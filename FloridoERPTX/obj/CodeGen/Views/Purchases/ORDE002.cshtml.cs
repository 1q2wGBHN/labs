#pragma warning disable 1591
//------------------------------------------------------------------------------
// <auto-generated>
//     Este código fue generado por una herramienta.
//     Versión de runtime:4.0.30319.42000
//
//     Los cambios en este archivo podrían causar un comportamiento incorrecto y se perderán si
//     se vuelve a generar el código.
// </auto-generated>
//------------------------------------------------------------------------------

namespace ASP
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using System.Net;
    using System.Text;
    using System.Web;
    using System.Web.Helpers;
    using System.Web.Mvc;
    using System.Web.Mvc.Ajax;
    using System.Web.Mvc.Html;
    using System.Web.Optimization;
    using System.Web.Routing;
    using System.Web.Security;
    using System.Web.UI;
    using System.Web.WebPages;
    using DevExpress.Utils;
    using DevExpress.Web;
    using DevExpress.Web.ASPxThemes;
    using DevExpress.Web.Mvc;
    using DevExpress.Web.Mvc.UI;
    using DevExpress.XtraReports;
    using DevExpress.XtraReports.UI;
    using DevExpress.XtraReports.Web;
    using DevExpress.XtraReports.Web.DocumentViewer;
    using FloridoERPTX;
    
    [System.CodeDom.Compiler.GeneratedCodeAttribute("RazorGenerator", "2.0.0.0")]
    [System.Web.WebPages.PageVirtualPathAttribute("~/Views/Purchases/ORDE002.cshtml")]
    public partial class _Views_Purchases_ORDE002_cshtml : System.Web.Mvc.WebViewPage<List<App.Entities.ViewModels.Order.OrderModel>>
    {
        public _Views_Purchases_ORDE002_cshtml()
        {
        }
        public override void Execute()
        {
            
            #line 2 "..\..\Views\Purchases\ORDE002.cshtml"
   ViewBag.Title = "ORDE002"; 
            
            #line default
            #line hidden
WriteLiteral("\r\n\r\n<style>\r\n    .textHidde input {\r\n        width: 80PX;\r\n    }\r\n</style>\r\n\r\n<di" +
"v");

WriteLiteral(" class=\"normalheader transition animated fadeIn\"");

WriteLiteral(">\r\n    <div");

WriteLiteral(" class=\"hpanel\"");

WriteLiteral(">\r\n        <div");

WriteLiteral(" class=\"panel-body\"");

WriteLiteral(">\r\n            <a");

WriteLiteral(" class=\"small-header-action\"");

WriteLiteral(" href=\"\"");

WriteLiteral(">\r\n                <div");

WriteLiteral(" class=\"clip-header\"");

WriteLiteral(">\r\n                    <i");

WriteLiteral(" id=\"fa\"");

WriteLiteral(" class=\"fa fa-arrow-up\"");

WriteLiteral("></i>\r\n                </div>\r\n            </a>\r\n            <div");

WriteLiteral(" id=\"hbreadcrumb\"");

WriteLiteral(" class=\"pull-right m-t-lg\"");

WriteLiteral(">\r\n                <ol");

WriteLiteral(" class=\"hbreadcrumb breadcrumb\"");

WriteLiteral(">\r\n                    <li><a");

WriteLiteral(" href=\"MENU000\"");

WriteLiteral(">Compras </a></li>\r\n                    <li>\r\n                        <a");

WriteLiteral(" href=\"MENU001\"");

WriteLiteral(">Pedidos </a>\r\n                    </li>\r\n                    <li");

WriteLiteral(" class=\"active\"");

WriteLiteral(">\r\n                        <span>Buscador formato para pedido a proveedor</span>\r" +
"\n                    </li>\r\n                </ol>\r\n            </div>\r\n         " +
"   <h2");

WriteLiteral(" class=\"font-light m-b-xs\"");

WriteLiteral(">\r\n                Pedidos\r\n            </h2>\r\n            <small>Buscador de ped" +
"idos de los proveedores </small>\r\n        </div>\r\n    </div>\r\n</div>\r\n<div");

WriteLiteral(" class=\"content animate-panel\"");

WriteLiteral(" id=\"search\"");

WriteLiteral(" style=\"padding-bottom:0\"");

WriteLiteral(">\r\n    <div");

WriteLiteral(" class=\"row\"");

WriteLiteral(">\r\n        <div");

WriteLiteral(" class=\"col-lg-12\"");

WriteLiteral(">\r\n            <div");

WriteLiteral(" class=\"hpanel\"");

WriteLiteral(">\r\n                <div");

WriteLiteral(" class=\"panel-body\"");

WriteLiteral(">\r\n                    <div");

WriteLiteral(" class=\"col-lg-12\"");

WriteLiteral(">\r\n                        <div");

WriteLiteral(" class=\"col-lg-1\"");

WriteLiteral(" style=\"padding-top:8px;\"");

WriteLiteral(">\r\n                            <p> <b>Folio:</b> </p>\r\n                        </" +
"div>\r\n                        <div");

WriteLiteral(" class=\"col-lg-9\"");

WriteLiteral(">\r\n                            <select");

WriteLiteral(" class=\"js-source-states-2 form-control\"");

WriteLiteral(" name=\"account\"");

WriteLiteral(" id=\"folioOrderSearch\"");

WriteLiteral(">\r\n");

            
            #line 47 "..\..\Views\Purchases\ORDE002.cshtml"
                                
            
            #line default
            #line hidden
            
            #line 47 "..\..\Views\Purchases\ORDE002.cshtml"
                                 foreach (var item in Model)
                                {

            
            #line default
            #line hidden
WriteLiteral("                                    <option");

WriteAttribute("value", Tuple.Create(" value=\"", 1855), Tuple.Create("\"", 1874)
            
            #line 49 "..\..\Views\Purchases\ORDE002.cshtml"
, Tuple.Create(Tuple.Create("", 1863), Tuple.Create<System.Object, System.Int32>(item.Folio
            
            #line default
            #line hidden
, 1863), false)
);

WriteLiteral(">");

            
            #line 49 "..\..\Views\Purchases\ORDE002.cshtml"
                                                           Write(item.Folio);

            
            #line default
            #line hidden
WriteLiteral(" - ");

            
            #line 49 "..\..\Views\Purchases\ORDE002.cshtml"
                                                                         Write(item.Supplier);

            
            #line default
            #line hidden
WriteLiteral("</option>\r\n");

            
            #line 50 "..\..\Views\Purchases\ORDE002.cshtml"
                                }

            
            #line default
            #line hidden
WriteLiteral("                            </select>\r\n                        </div>\r\n          " +
"              <div");

WriteLiteral(" class=\"col-lg-1\"");

WriteLiteral(">\r\n                            <button");

WriteLiteral(" class=\"btn btn-success\"");

WriteLiteral(" onclick=\"UpdateOrder()\"");

WriteLiteral("><i");

WriteLiteral(" class=\"fa fa-search\"");

WriteLiteral("></i> Buscar</button>\r\n\r\n                        </div>\r\n                        " +
"<div");

WriteLiteral(" class=\"col-lg-1\"");

WriteLiteral(">\r\n                            <button");

WriteLiteral(" class=\"btn btn-danger2\"");

WriteLiteral(" id=\"removeOrderList\"");

WriteLiteral("><i");

WriteLiteral(" class=\"fa fa-close\"");

WriteLiteral("></i> Cancelar</button>\r\n                        </div>\r\n                    </di" +
"v>\r\n                </div>\r\n            </div>\r\n        </div>\r\n    </div>\r\n</di" +
"v>\r\n\r\n\r\n<div");

WriteLiteral(" class=\"content animate-panel\"");

WriteLiteral(" id=\"orderSearch\"");

WriteLiteral(" data-effect=\"fadeInRight\"");

WriteLiteral(">\r\n    <div");

WriteLiteral(" class=\"row\"");

WriteLiteral(">\r\n        <div");

WriteLiteral(" class=\"col-lg-12\"");

WriteLiteral(">\r\n            <div");

WriteLiteral(" class=\"hpanel\"");

WriteLiteral(">\r\n                <div");

WriteLiteral(" class=\"panel-body\"");

WriteLiteral(">\r\n                    <div");

WriteLiteral(" class=\"back-link col-lg-12\"");

WriteLiteral(">\r\n                        <div");

WriteLiteral(" class=\"col-lg-2\"");

WriteLiteral(" style=\"text-align:left\"");

WriteLiteral(">\r\n                            <img");

WriteAttribute("src", Tuple.Create(" src=\"", 2924), Tuple.Create("\"", 2965)
, Tuple.Create(Tuple.Create("", 2930), Tuple.Create<System.Object, System.Int32>(Href("~/Content/img/logo")
, 2930), false)
, Tuple.Create(Tuple.Create(" ", 2948), Tuple.Create("florido", 2949), true)
, Tuple.Create(Tuple.Create(" ", 2956), Tuple.Create("azul.png", 2957), true)
);

WriteLiteral(" height=\"80\"");

WriteLiteral(" width=\"100\"");

WriteLiteral(">\r\n                        </div>\r\n                        <h1");

WriteLiteral(" class=\"col-lg-10  m-t-md\"");

WriteLiteral(">\r\n                            Formato para pedido a proveedor\r\n                 " +
"       </h1>\r\n                    </div>\r\n                    <div");

WriteLiteral(" class=\"col-lg-12\"");

WriteLiteral(">\r\n                        <div");

WriteLiteral(" class=\"col-lg-12\"");

WriteLiteral(">\r\n                            <p");

WriteLiteral(" class=\"col-lg-2\"");

WriteLiteral("> <b>Folio Formato:</b> </p>\r\n                            <p");

WriteLiteral(" class=\"col-lg-10\"");

WriteLiteral(" style=\"text-align:left\"");

WriteLiteral(" id=\"folio\"");

WriteLiteral("></p>\r\n                        </div>\r\n                        <div");

WriteLiteral(" class=\"col-lg-12\"");

WriteLiteral(">\r\n                            <p");

WriteLiteral(" style=\"padding-top:8px;font-weight: bold;\"");

WriteLiteral(" class=\"col-lg-2\"");

WriteLiteral("> Sucursal: </p>\r\n                            <div");

WriteLiteral(" class=\"col-lg-10\"");

WriteLiteral(">\r\n                                <select");

WriteLiteral(" class=\"form-control m-b\"");

WriteLiteral(" name=\"account\"");

WriteLiteral(" id=\"site\"");

WriteLiteral(">\r\n                                    <option");

WriteLiteral(" value=\"\"");

WriteLiteral(">*FLORIDO LAS AMERICAS</option>\r\n                                </select>\r\n     " +
"                       </div>\r\n                        </div>\r\n                 " +
"       <div");

WriteLiteral(" class=\"col-lg-12\"");

WriteLiteral(">\r\n                            <p");

WriteLiteral(" style=\"padding-top:8px;font-weight: bold;\"");

WriteLiteral(" class=\"col-lg-2\"");

WriteLiteral("> Proveedor:</p>\r\n                            <div");

WriteLiteral(" class=\"col-lg-10\"");

WriteLiteral(">\r\n                                <select");

WriteLiteral(" class=\"form-control m-b\"");

WriteLiteral(" name=\"account\"");

WriteLiteral(" id=\"supplier\"");

WriteLiteral(">\r\n                                    <option");

WriteLiteral(" value=\"\"");

WriteLiteral(">BIMBO S.A. DE C.V.</option>\r\n                                </select>\r\n        " +
"                    </div>\r\n                        </div>\r\n                    " +
"    <div");

WriteLiteral(" class=\"col-lg-12\"");

WriteLiteral(">\r\n                            <p");

WriteLiteral(" style=\"padding-top:8px;font-weight: bold;\"");

WriteLiteral(" class=\"col-lg-2\"");

WriteLiteral("> Dias Resurtido X 2: </p>\r\n                            <div");

WriteLiteral(" class=\"col-lg-10\"");

WriteLiteral(">\r\n                                <input");

WriteLiteral(" class=\"form-control\"");

WriteLiteral(" type=\"number\"");

WriteLiteral(" id=\"days\"");

WriteLiteral(" name=\"name\"");

WriteLiteral(" value=\"0\"");

WriteLiteral(" />\r\n                            </div>\r\n                        </div>\r\n        " +
"                <div");

WriteLiteral(" class=\"col-lg-12\"");

WriteLiteral(" style=\"margin-top:10px;\"");

WriteLiteral(">\r\n                            <p");

WriteLiteral(" style=\"padding-top:8px;font-weight: bold;\"");

WriteLiteral(" class=\"col-lg-2\"");

WriteLiteral("> Fecha de entrega: </p>\r\n                            <div");

WriteLiteral(" class=\"col-lg-3\"");

WriteLiteral(">\r\n                                <div");

WriteLiteral(" class=\"form-group\"");

WriteLiteral(">\r\n                                    <div");

WriteLiteral(" class=\'input-group date\'");

WriteLiteral(" id=\'datetimepicker\'");

WriteLiteral(">\r\n                                        <input");

WriteLiteral(" autocomplete=\"off\"");

WriteLiteral(" id=\"dateOrder\"");

WriteLiteral(" type=\'text\'");

WriteLiteral(" class=\"form-control\"");

WriteLiteral(" />\r\n                                        <span");

WriteLiteral(" class=\"input-group-addon\"");

WriteLiteral(">\r\n                                            <span");

WriteLiteral(" class=\"glyphicon glyphicon-calendar\"");

WriteLiteral("></span>\r\n                                        </span>\r\n                      " +
"              </div>\r\n                                    <label");

WriteLiteral(" id=\"OrderError\"");

WriteLiteral(" class=\"error\"");

WriteLiteral(" hidden>El Campo no puede ser nulo</label>\r\n                                </div" +
">\r\n                            </div>\r\n                        </div>\r\n         " +
"               <div");

WriteLiteral(" class=\"col-lg-12\"");

WriteLiteral(" style=\"padding-top:15px;padding-bottom:15px;\"");

WriteLiteral(">\r\n                            <div");

WriteLiteral(" class=\"col-lg-12\"");

WriteLiteral(">\r\n                                <button");

WriteLiteral(" id=\"buttonSaveTem\"");

WriteLiteral(" class=\"btn btn-success pull-right\"");

WriteLiteral(" onclick=\"TemporaryChanges()\"");

WriteLiteral("><i");

WriteLiteral(" class=\"fa fa-save\"");

WriteLiteral("></i> Guardar Cambios</button>\r\n                            </div>\r\n             " +
"           </div>\r\n\r\n                        <div");

WriteLiteral(" class=\"col-lg-12\"");

WriteLiteral(">\r\n                            <div");

WriteLiteral(" class=\"col-lg-12 table-responsive\"");

WriteLiteral(">\r\n                                <div");

WriteLiteral(" id=\"searchDiv\"");

WriteLiteral(" style=\"margin-bottom:20px;\"");

WriteLiteral(">\r\n                                    <p");

WriteLiteral(" style=\"padding-top:8px;font-weight: bold;\"");

WriteLiteral("> Buscador: </p>\r\n                                    <div>\r\n                    " +
"                    <input");

WriteLiteral(" style=\"width:25%\"");

WriteLiteral(" class=\"form-control\"");

WriteLiteral(" type=\"text\"");

WriteLiteral(" id=\"SearchInput\"");

WriteLiteral(" name=\"name\"");

WriteLiteral(" value=\"\"");

WriteLiteral(" />\r\n                                    </div>\r\n                                " +
"</div>\r\n                                <table");

WriteLiteral(" cellpadding=\"1\"");

WriteLiteral(" cellspacing=\"1\"");

WriteLiteral(" class=\"table table-bordered table-striped\"");

WriteLiteral(">\r\n                                    <thead>\r\n                                 " +
"       <tr>\r\n                                            <th");

WriteLiteral(" class=\'FolioHidden\'");

WriteLiteral(@">Folio No</th>
                                            <th>Codigo</th>
                                            <th>Description</th>
                                            <th>Existencia</th>
                                            <th>En PreCompra</th>
                                            <th>Vta Prom</th>
                                            <th>Sugerido Sist</th>
                                            <th");

WriteLiteral(" class=\"textHidde\"");

WriteLiteral(">Autorizado</th>\r\n                                            <th>Empaque</th>\r\n " +
"                                       </tr>\r\n                                  " +
"  </thead>\r\n                                    <tbody");

WriteLiteral(" id=\"tableOrdered\"");

WriteLiteral("></tbody>\r\n                                </table>\r\n                            " +
"</div>\r\n                        </div>\r\n                        <div");

WriteLiteral(" class=\"col-lg-6\"");

WriteLiteral(" style=\"padding-bottom:15px;\"");

WriteLiteral(">\r\n                            <p");

WriteLiteral(" style=\"padding-top:8px;\"");

WriteLiteral(" class=\"col-lg-3\"");

WriteLiteral("><b>Comentarios:</b></p>\r\n                            <div");

WriteLiteral(" class=\"col-lg-9\"");

WriteLiteral(">\r\n                                <textarea");

WriteLiteral(" rows=\"5\"");

WriteLiteral(" id=\"comment\"");

WriteLiteral(" placeholder=\"Comentarios\"");

WriteLiteral(" class=\"form-control\"");

WriteLiteral(" maxlength=\"150\"");

WriteLiteral(" style=\"max-width:100%;min-width:200px;width:100%;max-height:300px;min-height:100" +
"px;\"");

WriteLiteral("></textarea>\r\n                            </div>\r\n                        </div>\r" +
"\n                        <div");

WriteLiteral(" class=\"col-lg-4\"");

WriteLiteral(" style=\"padding-bottom:15px;\"");

WriteLiteral(">\r\n                            <p");

WriteLiteral(" style=\"padding-top:8px;\"");

WriteLiteral(" class=\"col-lg-3\"");

WriteLiteral("><b>Moneda:</b></p>\r\n                            <div");

WriteLiteral(" class=\"col-lg-9\"");

WriteLiteral(">\r\n                                <select");

WriteLiteral(" id=\"currency\"");

WriteLiteral(" class=\"form-control select2\"");

WriteLiteral("></select>\r\n                            </div>\r\n                        </div>\r\n " +
"                       <div");

WriteLiteral(" class=\"col-lg-2\"");

WriteLiteral(" style=\"padding-bottom:15px;\"");

WriteLiteral(">\r\n                            <div");

WriteLiteral(" class=\"col-lg-12\"");

WriteLiteral(">\r\n                                <button");

WriteLiteral(" id=\"buttonSave\"");

WriteLiteral(" class=\"btn btn-success pull-right\"");

WriteLiteral("><i");

WriteLiteral(" class=\"fa fa-check\"");

WriteLiteral("></i> Confirmar</button>\r\n                            </div>\r\n                   " +
"     </div>\r\n                    </div>\r\n                </div>\r\n            </d" +
"iv>\r\n        </div>\r\n    </div>\r\n</div>\r\n\r\n");

DefineSection("Scripts", () => {

WriteLiteral(" ");

            
            #line 178 "..\..\Views\Purchases\ORDE002.cshtml"
              Write(Scripts.Render("~/bundles/ORDE002/js"));

            
            #line default
            #line hidden
WriteLiteral(" ");

});

        }
    }
}
#pragma warning restore 1591
