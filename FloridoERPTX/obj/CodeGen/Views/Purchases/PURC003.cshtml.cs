#pragma warning disable 1591
//------------------------------------------------------------------------------
// <auto-generated>
//     Este código fue generado por una herramienta.
//     Versión de runtime:4.0.30319.42000
//
//     Los cambios en este archivo podrían causar un comportamiento incorrecto y se perderán si
//     se vuelve a generar el código.
// </auto-generated>
//------------------------------------------------------------------------------

namespace ASP
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using System.Net;
    using System.Text;
    using System.Web;
    using System.Web.Helpers;
    using System.Web.Mvc;
    using System.Web.Mvc.Ajax;
    using System.Web.Mvc.Html;
    using System.Web.Optimization;
    using System.Web.Routing;
    using System.Web.Security;
    using System.Web.UI;
    using System.Web.WebPages;
    using DevExpress.Utils;
    using DevExpress.Web;
    using DevExpress.Web.ASPxThemes;
    using DevExpress.Web.Mvc;
    using DevExpress.Web.Mvc.UI;
    using DevExpress.XtraReports;
    using DevExpress.XtraReports.UI;
    using DevExpress.XtraReports.Web;
    using DevExpress.XtraReports.Web.DocumentViewer;
    using FloridoERPTX;
    
    [System.CodeDom.Compiler.GeneratedCodeAttribute("RazorGenerator", "2.0.0.0")]
    [System.Web.WebPages.PageVirtualPathAttribute("~/Views/Purchases/PURC003.cshtml")]
    public partial class _Views_Purchases_PURC003_cshtml : System.Web.Mvc.WebViewPage<FloridoERPTX.ViewModels.Purchases.PurchasesOrderViewModel>
    {
        public _Views_Purchases_PURC003_cshtml()
        {
        }
        public override void Execute()
        {
            
            #line 2 "..\..\Views\Purchases\PURC003.cshtml"
   ViewBag.Title = "PURC003"; 
            
            #line default
            #line hidden
WriteLiteral("\r\n\r\n<style");

WriteLiteral(" type=\"text/css\"");

WriteLiteral(@">
    td.details-control:before {
        display: inline-block;
        font: normal normal normal 14px/1 FontAwesome;
        font-size: inherit;
        text-rendering: auto;
        -webkit-font-smoothing: antialiased;
        content: ""\f101"";
        cursor: pointer;
    }

    tr.details td.details-control:before {
        display: inline-block;
        font: normal normal normal 14px/1 FontAwesome;
        font-size: inherit;
        text-rendering: auto;
        -webkit-font-smoothing: antialiased;
        content: ""\f103"";
        cursor: pointer;
    }
</style>

<div");

WriteLiteral(" class=\"normalheader transition animated fadeIn\"");

WriteLiteral(">\r\n    <div");

WriteLiteral(" class=\"hpanel\"");

WriteLiteral(">\r\n        <div");

WriteLiteral(" class=\"panel-body\"");

WriteLiteral(">\r\n            <a");

WriteLiteral(" class=\"small-header-action\"");

WriteLiteral(" href=\"\"");

WriteLiteral(">\r\n                <div");

WriteLiteral(" class=\"clip-header\"");

WriteLiteral(">\r\n                    <i");

WriteLiteral(" id=\"fa\"");

WriteLiteral(" class=\"fa fa-arrow-up\"");

WriteLiteral("></i>\r\n                </div>\r\n            </a>\r\n            <div");

WriteLiteral(" id=\"hbreadcrumb\"");

WriteLiteral(" class=\"pull-right m-t-lg\"");

WriteLiteral(">\r\n                <ol");

WriteLiteral(" class=\"hbreadcrumb breadcrumb\"");

WriteLiteral(">\r\n                    <li><a");

WriteLiteral(" href=\"MENU000\"");

WriteLiteral(">Compras</a></li>\r\n                    <li>\r\n                        <span>Report" +
"es</span>\r\n                    </li>\r\n                    <li");

WriteLiteral(" class=\"active\"");

WriteLiteral(">\r\n                        <span>Eficiencia Proveedores</span>\r\n                 " +
"   </li>\r\n                </ol>\r\n            </div>\r\n            <h2");

WriteLiteral(" class=\"font-light m-b-xs\"");

WriteLiteral(">\r\n                Eficiencia Proveedores.\r\n            </h2>\r\n            <small" +
">Reporte de Eficiencia de Proveedores.</small>\r\n        </div>\r\n    </div>\r\n</di" +
"v>\r\n<div");

WriteLiteral(" class=\"content animate-panel\"");

WriteLiteral(">\r\n    <div");

WriteLiteral(" class=\"row\"");

WriteLiteral(">\r\n        <div");

WriteLiteral(" class=\"col-lg-12\"");

WriteLiteral(">\r\n            <div");

WriteLiteral(" class=\"hpanel hblue\"");

WriteLiteral(">\r\n                <div");

WriteLiteral(" class=\"panel-body\"");

WriteLiteral(">\r\n                    <div");

WriteLiteral(" class=\"row\"");

WriteLiteral(">\r\n                        <div");

WriteLiteral(" class=\"form-group col-lg-4\"");

WriteLiteral(">\r\n                            <label");

WriteLiteral(" title=\"Fecha Inicial\"");

WriteLiteral(" data-toggle=\"tooltip\"");

WriteLiteral(" data-placement=\"right\"");

WriteLiteral(">Periodo</label>\r\n                            <div");

WriteLiteral(" class=\"input-daterange input-group\"");

WriteLiteral(" id=\"datepicker\"");

WriteLiteral(">\r\n                                <div");

WriteLiteral(" class=\"input-group date\"");

WriteLiteral(">\r\n                                    <span");

WriteLiteral(" class=\"input-group-addon\"");

WriteLiteral(">\r\n                                        <span");

WriteLiteral(" class=\"fa fa-calendar\"");

WriteLiteral("></span>\r\n                                    </span>\r\n                          " +
"          <input");

WriteLiteral(" type=\"text\"");

WriteLiteral(" id=\"ValidDate1\"");

WriteLiteral(" class=\"form-control\"");

WriteLiteral(" autocomplete=\"off\"");

WriteLiteral(" />\r\n                                </div>\r\n                                <spa" +
"n");

WriteLiteral(" class=\"input-group-addon\"");

WriteLiteral(">A</span>\r\n                                <div");

WriteLiteral(" class=\"input-group date\"");

WriteLiteral(">\r\n                                    <input");

WriteLiteral(" type=\"text\"");

WriteLiteral(" id=\"ValidDate2\"");

WriteLiteral(" class=\"form-control\"");

WriteLiteral(" autocomplete=\"off\"");

WriteLiteral(" />\r\n                                </div>\r\n                            </div>\r\n" +
"                        </div>\r\n                        <div");

WriteLiteral(" class=\"col-md-4\"");

WriteLiteral(">\r\n                            <label");

WriteLiteral(" title=\"Proveedor\"");

WriteLiteral(" data-toggle=\"tooltip\"");

WriteLiteral(" data-placement=\"right\"");

WriteLiteral(">Proveedores</label>\r\n");

WriteLiteral("                            ");

            
            #line 75 "..\..\Views\Purchases\PURC003.cshtml"
                       Write(Html.DropDownListFor(m => m.Suppliers, Model.SuppliersList, new { @class = "form-control", @id = "SuppliersDrop" }));

            
            #line default
            #line hidden
WriteLiteral("\r\n                        </div>\r\n                        <div");

WriteLiteral(" class=\"form-group col-lg-1\"");

WriteLiteral(">\r\n                            <br />\r\n                            <button");

WriteLiteral(" class=\"btn btn-primary\"");

WriteLiteral(" style=\"margin-top:5px;\"");

WriteLiteral(" id=\"buscar\"");

WriteLiteral("><i");

WriteLiteral(" class=\"fa fa-search\"");

WriteLiteral("></i> Buscar</button>\r\n                        </div>\r\n                    </div>" +
"\r\n                    <div");

WriteLiteral(" id=\"TableDiv\"");

WriteLiteral(" class=\"animate-panel\"");

WriteLiteral(" data-effect=\"zoomIn\"");

WriteLiteral(">\r\n                        <div");

WriteLiteral(" class=\"row table-responsive\"");

WriteLiteral(">\r\n                            <table");

WriteLiteral(" id=\"TablePurchases\"");

WriteLiteral(" class=\"table table-striped table-bordered table-hover\"");

WriteLiteral(" style=\"width:100%\"");

WriteLiteral(@">
                                <thead>
                                    <tr>
                                        <th></th>
                                        <th># Orden</th>
                                        <th>Fecha Creacion</th>
                                        <th>Cantidad Solicitada</th>
                                        <th>Cantidad Surtida</th>
                                        <th>Monto Solicitado</th>
                                        <th>Monto Surtido</th>
                                        <th>Eficiencia Monto</th>
                                        <th>Eficiencia Cantidad</th>
                                    </tr>
                                </thead>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div");

WriteLiteral(" class=\"modal fade hmodal-info\"");

WriteLiteral(" id=\"ModalReport\"");

WriteLiteral(" tabindex=\"-1\"");

WriteLiteral(" role=\"dialog\"");

WriteLiteral(" aria-hidden=\"true\"");

WriteLiteral(">\r\n            <div");

WriteLiteral(" class=\"modal-dialog modal-lg\"");

WriteLiteral(">\r\n                <div");

WriteLiteral(" class=\"modal-content\"");

WriteLiteral(">\r\n                    <div");

WriteLiteral(" class=\"color-line\"");

WriteLiteral("></div>\r\n                    <div");

WriteLiteral(" class=\"modal-header\"");

WriteLiteral(">\r\n                        <h4");

WriteLiteral(" id=\"ModalTitle\"");

WriteLiteral(" class=\"modal-title\"");

WriteLiteral(">Orden de compra</h4>\r\n                        <small");

WriteLiteral(" id=\"ModalDescription\"");

WriteLiteral(" class=\"font-bold\"");

WriteLiteral(">Reporte de orden de compra Numero.</small>\r\n                    </div>\r\n        " +
"            <div");

WriteLiteral(" class=\"modal-body\"");

WriteLiteral(">\r\n                        <div");

WriteLiteral(" class=\"row\"");

WriteLiteral(">\r\n                            <iframe");

WriteLiteral(" id=\"iframe\"");

WriteLiteral(" style=\"width:100%\"");

WriteLiteral(" height=\"350\"");

WriteLiteral(" frameborder=\"0\"");

WriteLiteral("></iframe>\r\n                        </div>\r\n                    </div>\r\n         " +
"           <div");

WriteLiteral(" class=\"modal-footer\"");

WriteLiteral(">\r\n                        <button");

WriteLiteral(" type=\"button\"");

WriteLiteral(" class=\"btn btn-default\"");

WriteLiteral(" data-dismiss=\"modal\"");

WriteLiteral(">Cancelar</button>\r\n                    </div>\r\n                </div>\r\n         " +
"   </div>\r\n        </div>\r\n    </div>\r\n</div>\r\n\r\n");

DefineSection("Scripts", () => {

WriteLiteral(" ");

            
            #line 126 "..\..\Views\Purchases\PURC003.cshtml"
              Write(Scripts.Render("~/bundles/PURC003/js"));

            
            #line default
            #line hidden
WriteLiteral(" ");

});

        }
    }
}
#pragma warning restore 1591
