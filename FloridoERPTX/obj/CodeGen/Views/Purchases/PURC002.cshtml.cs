#pragma warning disable 1591
//------------------------------------------------------------------------------
// <auto-generated>
//     Este código fue generado por una herramienta.
//     Versión de runtime:4.0.30319.42000
//
//     Los cambios en este archivo podrían causar un comportamiento incorrecto y se perderán si
//     se vuelve a generar el código.
// </auto-generated>
//------------------------------------------------------------------------------

namespace ASP
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using System.Net;
    using System.Text;
    using System.Web;
    using System.Web.Helpers;
    using System.Web.Mvc;
    using System.Web.Mvc.Ajax;
    using System.Web.Mvc.Html;
    using System.Web.Optimization;
    using System.Web.Routing;
    using System.Web.Security;
    using System.Web.UI;
    using System.Web.WebPages;
    using DevExpress.Utils;
    using DevExpress.Web;
    using DevExpress.Web.ASPxThemes;
    using DevExpress.Web.Mvc;
    using DevExpress.Web.Mvc.UI;
    using DevExpress.XtraReports;
    using DevExpress.XtraReports.UI;
    using DevExpress.XtraReports.Web;
    using DevExpress.XtraReports.Web.DocumentViewer;
    using FloridoERPTX;
    
    [System.CodeDom.Compiler.GeneratedCodeAttribute("RazorGenerator", "2.0.0.0")]
    [System.Web.WebPages.PageVirtualPathAttribute("~/Views/Purchases/PURC002.cshtml")]
    public partial class _Views_Purchases_PURC002_cshtml : System.Web.Mvc.WebViewPage<FloridoERPTX.ViewModels.Purchases.SalesPriceChangeViewModel>
    {
        public _Views_Purchases_PURC002_cshtml()
        {
        }
        public override void Execute()
        {
            
            #line 2 "..\..\Views\Purchases\PURC002.cshtml"
   ViewBag.Title = "PURC002"; 
            
            #line default
            #line hidden
WriteLiteral("\r\n\r\n<div");

WriteLiteral(" class=\"normalheader transition animated fadeIn\"");

WriteLiteral(">\r\n    <div");

WriteLiteral(" class=\"hpanel\"");

WriteLiteral(">\r\n        <div");

WriteLiteral(" class=\"panel-body\"");

WriteLiteral(">\r\n            <a");

WriteLiteral(" class=\"small-header-action\"");

WriteLiteral(" href=\"\"");

WriteLiteral(">\r\n                <div");

WriteLiteral(" class=\"clip-header\"");

WriteLiteral(">\r\n                    <i");

WriteLiteral(" id=\"fa\"");

WriteLiteral(" class=\"fa fa-arrow-up\"");

WriteLiteral("></i>\r\n                </div>\r\n            </a>\r\n            <div");

WriteLiteral(" id=\"hbreadcrumb\"");

WriteLiteral(" class=\"pull-right m-t-lg\"");

WriteLiteral(">\r\n                <ol");

WriteLiteral(" class=\"hbreadcrumb breadcrumb\"");

WriteLiteral(">\r\n                    <li> <span>Compras</span></li>\r\n                    <li><a" +
"");

WriteLiteral(" href=\"\"");

WriteLiteral(">Reportes</a></li>\r\n                    <li");

WriteLiteral(" class=\"active\"");

WriteLiteral(">\r\n                        <span>Precio de Venta.</span>\r\n                    </l" +
"i>\r\n                </ol>\r\n            </div>\r\n            <h2");

WriteLiteral(" class=\"font-light m-b-xs\"");

WriteLiteral(">\r\n                Precio de Venta.\r\n            </h2>\r\n            <small>Report" +
"e y configuracion de basculas</small>\r\n        </div>\r\n    </div>\r\n</div>\r\n<div");

WriteLiteral(" class=\"content animate-panel\"");

WriteLiteral(">\r\n    <div");

WriteLiteral(" class=\"hpanel\"");

WriteLiteral(">\r\n        <ul");

WriteLiteral(" class=\"nav nav-tabs\"");

WriteLiteral(">\r\n            <li");

WriteLiteral(" class=\"active\"");

WriteLiteral("><a");

WriteLiteral(" data-toggle=\"tab\"");

WriteLiteral(" href=\"#tab-1\"");

WriteLiteral("><i");

WriteLiteral(" class=\"fa fa-area-chart\"");

WriteLiteral("></i> Reporte Cambio de Precio de Venta.</a></li>\r\n            <li");

WriteLiteral(" class=\"\"");

WriteLiteral("><a");

WriteLiteral(" data-toggle=\"tab\"");

WriteLiteral(" href=\"#tab-2\"");

WriteLiteral("><i");

WriteLiteral(" class=\"fa fa-shopping-cart\"");

WriteLiteral("></i> Listado de Precios</a></li>\r\n        </ul>\r\n        <div");

WriteLiteral(" class=\"tab-content\"");

WriteLiteral(">\r\n            <div");

WriteLiteral(" id=\"tab-1\"");

WriteLiteral(" class=\"tab-pane active\"");

WriteLiteral(">\r\n                <div");

WriteLiteral(" class=\"row\"");

WriteLiteral(">\r\n                    <div");

WriteLiteral(" class=\"col-lg-12\"");

WriteLiteral(">\r\n                        <div");

WriteLiteral(" class=\"hpanel blue\"");

WriteLiteral(">\r\n                            <div");

WriteLiteral(" class=\"panel-body\"");

WriteLiteral(">\r\n                                <div");

WriteLiteral(" class=\"row\"");

WriteLiteral(">\r\n                                    <div");

WriteLiteral(" class=\"col-lg-12\"");

WriteLiteral(">\r\n                                        <div");

WriteLiteral(" class=\"form-group col-lg-2\"");

WriteLiteral(">\r\n                                            <label");

WriteLiteral(" title=\"Fecha de Aplicacion de Precio\"");

WriteLiteral(" data-toggle=\"tooltip\"");

WriteLiteral(" data-placement=\"right\"");

WriteLiteral(">Fecha de Aplicacion</label>\r\n                                            <div");

WriteLiteral(" class=\"input-group date\"");

WriteLiteral(">\r\n                                                <span");

WriteLiteral(" class=\"input-group-addon\"");

WriteLiteral(">\r\n                                                    <span");

WriteLiteral(" class=\"fa fa-calendar\"");

WriteLiteral("></span>\r\n                                                </span>\r\n              " +
"                                  <input");

WriteLiteral(" id=\"fechaAppPrecio\"");

WriteLiteral(" type=\"text\"");

WriteLiteral(" class=\"form-control\"");

WriteLiteral(" autocomplete=\"off\"");

WriteLiteral(" />\r\n                                            </div>\r\n                        " +
"                </div>\r\n                                        <div");

WriteLiteral(" class=\"form-group col-lg-2\"");

WriteLiteral(">\r\n                                            <div");

WriteLiteral(" class=\"form-group col-md-12\"");

WriteLiteral(">\r\n                                                <label");

WriteLiteral(" title=\"\"");

WriteLiteral(" data-toggle=\"tooltip\"");

WriteLiteral(" data-placement=\"right\"");

WriteLiteral(">&nbsp;&nbsp;&nbsp;Existencias en 0</label>\r\n                                    " +
"            <br />\r\n                                                <div");

WriteLiteral(" class=\"icheckbox_square-green hover\"");

WriteLiteral(" style=\"position: relative; left: 40%; top:8px\"");

WriteLiteral(">\r\n                                                    <input");

WriteLiteral(" type=\"checkbox\"");

WriteLiteral(" class=\"i-checks\"");

WriteLiteral(" id=\"removeExist\"");

WriteLiteral(" name=\"removeExist\"");

WriteLiteral(" style=\"position: absolute; opacity: 0;\"");

WriteLiteral(">\r\n                                                </div>\r\n                      " +
"                      </div>\r\n                                        </div>\r\n  " +
"                                      <div");

WriteLiteral(" class=\"form-group col-lg-2\"");

WriteLiteral(">\r\n                                            <label");

WriteLiteral(" class=\"center\"");

WriteLiteral(" for=\"select_depto\"");

WriteLiteral(" data-toggle=\"tooltip\"");

WriteLiteral(" data-placement=\"right\"");

WriteLiteral(">Grupo</label>\r\n");

WriteLiteral("                                            ");

            
            #line 62 "..\..\Views\Purchases\PURC002.cshtml"
                                       Write(Html.DropDownListFor(m => m.Departamentos, Model.DeptosList, new { @class = "form-control select2", @id = "depto", @onchange = "GetFamily();" }));

            
            #line default
            #line hidden
WriteLiteral("\r\n                                        </div>\r\n                               " +
"         <div");

WriteLiteral(" class=\"form-group col-lg-3\"");

WriteLiteral(">\r\n                                            <label");

WriteLiteral(" class=\"center\"");

WriteLiteral(" for=\"select_family\"");

WriteLiteral(" data-toggle=\"tooltip\"");

WriteLiteral(" data-placement=\"right\"");

WriteLiteral(">Familia</label>\r\n                                            <select");

WriteLiteral(" id=\"family\"");

WriteLiteral(" class=\"form-control center select2\"");

WriteLiteral(">\r\n                                                <option");

WriteLiteral(" value=\"\"");

WriteLiteral(">Selecciona una Familia</option>\r\n                                            </s" +
"elect>\r\n                                        </div>\r\n                        " +
"                <div");

WriteLiteral(" class=\"form-group col-lg-1\"");

WriteLiteral(" style=\"margin-top:5px\"");

WriteLiteral(">\r\n                                            <br />\r\n                          " +
"                  <button");

WriteLiteral(" class=\"btn btn-primary\"");

WriteLiteral(" id=\"buscar\"");

WriteLiteral("><i");

WriteLiteral(" class=\"fa fa-search\"");

WriteLiteral("></i> Buscar</button>\r\n                                        </div>\r\n          " +
"                              <div");

WriteLiteral(" class=\"form-group col-lg-1\"");

WriteLiteral(" style=\"margin-top:5px\"");

WriteLiteral(">\r\n                                            <br />\r\n                          " +
"                  <button");

WriteLiteral(" class=\"btn btn-success\"");

WriteLiteral(" disabled");

WriteLiteral(" id=\"button\"");

WriteLiteral("><i");

WriteLiteral(" class=\"fa fa-print\"");

WriteLiteral("></i> Imprimir</button>\r\n                                        </div>\r\n        " +
"                            </div>\r\n                                    <div");

WriteLiteral(" class=\"col-lg-12\"");

WriteLiteral(">\r\n                                        <div");

WriteLiteral(" class=\"table-responsive\"");

WriteLiteral(">\r\n                                            <table");

WriteLiteral(" id=\"tableSalesPrice\"");

WriteLiteral(" class=\"table table-striped table-bordered table-hover\"");

WriteLiteral(" width=\"100%\"");

WriteLiteral(@">
                                                <thead>
                                                    <tr>
                                                        <th>Id</th>
                                                        <th>Codigo Producto</th>
                                                        <th>Descripcion</th>
                                                        <th>Precio</th>
                                                        <th>Fecha</th>
                                                        <th>Existencias</th>
                                                        <th>Grupo</th>
                                                        <th>Familia</th>
                                                    </tr>
                                                </thead>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div");

WriteLiteral(" id=\"tab-2\"");

WriteLiteral(" class=\"tab-pane\"");

WriteLiteral(">\r\n                <div");

WriteLiteral(" class=\"panel-body\"");

WriteLiteral(">\r\n                    <div");

WriteLiteral(" class=\"row\"");

WriteLiteral(">\r\n                        <div>\r\n                            <div");

WriteLiteral(" class=\"form-group col-lg-5\"");

WriteLiteral(">\r\n                                <label");

WriteLiteral(" class=\"center\"");

WriteLiteral(" for=\"select_depto\"");

WriteLiteral(" data-toggle=\"tooltip\"");

WriteLiteral(" data-placement=\"right\"");

WriteLiteral(">Departamento</label>\r\n");

WriteLiteral("                                ");

            
            #line 109 "..\..\Views\Purchases\PURC002.cshtml"
                           Write(Html.DropDownListFor(m => m.Departamentos, Model.DeptosList, new { @class = "form-control select2", @id = "departament", @onchange = "GetFamily();" }));

            
            #line default
            #line hidden
WriteLiteral("\r\n                            </div>\r\n                            <div");

WriteLiteral(" class=\"form-group col-lg-5\"");

WriteLiteral(">\r\n                                <label");

WriteLiteral(" class=\"center\"");

WriteLiteral(" for=\"select_family\"");

WriteLiteral(" data-toggle=\"tooltip\"");

WriteLiteral(" data-placement=\"right\"");

WriteLiteral(">Familia</label>\r\n                                <select");

WriteLiteral(" id=\"family2\"");

WriteLiteral(" class=\"form-control center select2\"");

WriteLiteral(">\r\n                                    <option");

WriteLiteral(" value=\"\"");

WriteLiteral(">Selecciona una Familia</option>\r\n                                </select>\r\n    " +
"                        </div>\r\n                            <div");

WriteLiteral(" class=\"form-group col-lg-1\"");

WriteLiteral(" style=\"margin-top:5px\"");

WriteLiteral(">\r\n                                <br />\r\n                                <butto" +
"n");

WriteLiteral(" class=\"btn btn-success\"");

WriteLiteral(" id=\"button\"");

WriteLiteral(" onclick=\"ShowReportPrice()\"");

WriteLiteral("><i");

WriteLiteral(" class=\"fa fa-print\"");

WriteLiteral("></i> Imprimir</button>\r\n                            </div>\r\n                    " +
"    </div>\r\n                    </div>\r\n                </div>\r\n            </di" +
"v>\r\n        </div>\r\n    </div>\r\n    <div");

WriteLiteral(" class=\"modal fade\"");

WriteLiteral(" id=\"modal-cenefa\"");

WriteLiteral(" tabindex=\"-1\"");

WriteLiteral(" role=\"dialog\"");

WriteLiteral(" aria-hidden=\"true\"");

WriteLiteral(">\r\n        <div");

WriteLiteral(" class=\"modal-dialog modal-lg\"");

WriteLiteral(">\r\n            <div");

WriteLiteral(" class=\"modal-content\"");

WriteLiteral(">\r\n                <div");

WriteLiteral(" class=\"color-line\"");

WriteLiteral("></div>\r\n                <div");

WriteLiteral(" class=\"modal-header\"");

WriteLiteral(">\r\n                    <h4");

WriteLiteral(" id=\"ModalTitle\"");

WriteLiteral(" class=\"modal-title\"");

WriteLiteral(">Impresion Cenefa.</h4>\r\n                </div>\r\n                <div");

WriteLiteral(" id=\"qwerty\"");

WriteLiteral(" class=\"modal-body\"");

WriteLiteral(" style=\"height:100%\"");

WriteLiteral(">\r\n\r\n                </div>\r\n                <div");

WriteLiteral(" class=\"modal-footer\"");

WriteLiteral(">\r\n                    <button");

WriteLiteral(" type=\"button\"");

WriteLiteral(" class=\"btn btn-default\"");

WriteLiteral(" data-dismiss=\"modal\"");

WriteLiteral(">Cancelar</button>\r\n                </div>\r\n            </div>\r\n        </div>\r\n " +
"   </div>\r\n    <div");

WriteLiteral(" class=\"modal fade hmodal-info\"");

WriteLiteral(" id=\"ModalReport\"");

WriteLiteral(" tabindex=\"-1\"");

WriteLiteral(" role=\"dialog\"");

WriteLiteral(" aria-hidden=\"true\"");

WriteLiteral(">\r\n        <div");

WriteLiteral(" class=\"modal-dialog modal-lg\"");

WriteLiteral(">\r\n            <div");

WriteLiteral(" class=\"modal-content\"");

WriteLiteral(">\r\n                <div");

WriteLiteral(" class=\"color-line\"");

WriteLiteral("></div>\r\n                <div");

WriteLiteral(" class=\"modal-header\"");

WriteLiteral(">\r\n                    <h4");

WriteLiteral(" id=\"ModalTitle\"");

WriteLiteral(" class=\"modal-title\"");

WriteLiteral(">Orden de compra</h4>\r\n                    <small");

WriteLiteral(" id=\"ModalDescription\"");

WriteLiteral(" class=\"font-bold\"");

WriteLiteral(">Reporte de orden de compra Numero.</small>\r\n                </div>\r\n            " +
"    <div");

WriteLiteral(" id=\"modalReferenceBody\"");

WriteLiteral(" class=\"modal-body\"");

WriteLiteral(" style=\"height:100%\"");

WriteLiteral(">\r\n                    <div");

WriteLiteral(" class=\"modal-footer\"");

WriteLiteral(">\r\n                        <button");

WriteLiteral(" type=\"button\"");

WriteLiteral(" class=\"btn btn-default\"");

WriteLiteral(" data-dismiss=\"modal\"");

WriteLiteral("><i");

WriteLiteral(" class=\"fa fa-close\"");

WriteLiteral("></i>Cancelar</button>\r\n                    </div>\r\n                </div>\r\n     " +
"       </div>\r\n        </div>\r\n    </div>\r\n</div>\r\n\r\n");

DefineSection("Scripts", () => {

WriteLiteral(" ");

            
            #line 161 "..\..\Views\Purchases\PURC002.cshtml"
              Write(Scripts.Render("~/bundles/PURC002/js"));

            
            #line default
            #line hidden
WriteLiteral(" ");

});

        }
    }
}
#pragma warning restore 1591
