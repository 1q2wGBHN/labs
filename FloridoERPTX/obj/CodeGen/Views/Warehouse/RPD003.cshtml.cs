#pragma warning disable 1591
//------------------------------------------------------------------------------
// <auto-generated>
//     Este código fue generado por una herramienta.
//     Versión de runtime:4.0.30319.42000
//
//     Los cambios en este archivo podrían causar un comportamiento incorrecto y se perderán si
//     se vuelve a generar el código.
// </auto-generated>
//------------------------------------------------------------------------------

namespace ASP
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using System.Net;
    using System.Text;
    using System.Web;
    using System.Web.Helpers;
    using System.Web.Mvc;
    using System.Web.Mvc.Ajax;
    using System.Web.Mvc.Html;
    using System.Web.Optimization;
    using System.Web.Routing;
    using System.Web.Security;
    using System.Web.UI;
    using System.Web.WebPages;
    using DevExpress.Utils;
    using DevExpress.Web;
    using DevExpress.Web.ASPxThemes;
    using DevExpress.Web.Mvc;
    using DevExpress.Web.Mvc.UI;
    using DevExpress.XtraReports;
    using DevExpress.XtraReports.UI;
    using DevExpress.XtraReports.Web;
    using DevExpress.XtraReports.Web.DocumentViewer;
    using FloridoERPTX;
    
    [System.CodeDom.Compiler.GeneratedCodeAttribute("RazorGenerator", "2.0.0.0")]
    [System.Web.WebPages.PageVirtualPathAttribute("~/Views/Warehouse/RPD003.cshtml")]
    public partial class _Views_Warehouse_RPD003_cshtml : System.Web.Mvc.WebViewPage<FloridoERPTX.ViewModels.Warehouse.RPD003Model>
    {
        public _Views_Warehouse_RPD003_cshtml()
        {
        }
        public override void Execute()
        {
            
            #line 2 "..\..\Views\Warehouse\RPD003.cshtml"
   ViewBag.Title = "RPD003"; 
            
            #line default
            #line hidden
WriteLiteral("\r\n\r\n<style");

WriteLiteral(" type=\"text/css\"");

WriteLiteral(@">
    td.details-control:before {
        display: inline-block;
        font: normal normal normal 14px/1 FontAwesome;
        font-size: inherit;
        text-rendering: auto;
        -webkit-font-smoothing: antialiased;
        content: ""\f101"";
        cursor: pointer;
    }

    tr.details td.details-control:before {
        display: inline-block;
        font: normal normal normal 14px/1 FontAwesome;
        font-size: inherit;
        text-rendering: auto;
        -webkit-font-smoothing: antialiased;
        content: ""\f103"";
        cursor: pointer;
    }
</style>

<div");

WriteLiteral(" class=\"normalheader transition animated fadeIn\"");

WriteLiteral(">\r\n    <div");

WriteLiteral(" class=\"hpanel\"");

WriteLiteral(">\r\n        <div");

WriteLiteral(" class=\"panel-body\"");

WriteLiteral(">\r\n            <a");

WriteLiteral(" class=\"small-header-action\"");

WriteLiteral(" href=\"\"");

WriteLiteral(">\r\n                <div");

WriteLiteral(" class=\"clip-header\"");

WriteLiteral(">\r\n                    <i");

WriteLiteral(" id=\"fa\"");

WriteLiteral(" class=\"fa fa-arrow-up\"");

WriteLiteral("></i>\r\n                </div>\r\n            </a>\r\n            <div");

WriteLiteral(" id=\"hbreadcrumb\"");

WriteLiteral(" class=\"pull-right m-t-lg\"");

WriteLiteral(">\r\n                <ol");

WriteLiteral(" class=\"hbreadcrumb breadcrumb\"");

WriteLiteral(">\r\n                    <li><a");

WriteLiteral(" href=\"WSM003\"");

WriteLiteral(">Almacen</a></li>\r\n                    <li>\r\n                        <span>Report" +
"es</span>\r\n                    </li>\r\n                    <li");

WriteLiteral(" class=\"active\"");

WriteLiteral(">\r\n                        <span>Reporte de Dia de Inventario y Excedentes</span>" +
"\r\n                    </li>\r\n                </ol>\r\n            </div>\r\n        " +
"    <h2");

WriteLiteral(" class=\"font-light m-b-xs\"");

WriteLiteral(">\r\n                Reporte Dia de Inventarios y Excedentes\r\n            </h2>\r\n  " +
"          <small></small>\r\n        </div>\r\n    </div>\r\n</div>\r\n<div");

WriteLiteral(" class=\"content animate-panel\"");

WriteLiteral(">\r\n    <div");

WriteLiteral(" class=\"row\"");

WriteLiteral(">\r\n        <div");

WriteLiteral(" class=\"col-lg-12\"");

WriteLiteral(">\r\n            <div");

WriteLiteral(" class=\"hpanel hblue\"");

WriteLiteral(">\r\n                <div");

WriteLiteral(" class=\"panel-heading\"");

WriteLiteral(">\r\n                    <div");

WriteLiteral(" class=\"panel-tools\"");

WriteLiteral(">\r\n                        <a");

WriteLiteral(" id=\"PanelGrid\"");

WriteLiteral(" class=\"showhide\"");

WriteLiteral("><i");

WriteLiteral(" class=\"fa fa-chevron-up\"");

WriteLiteral("></i></a>\r\n                    </div>\r\n                    <a");

WriteLiteral(" onclick=\"PanelGrid.click();\"");

WriteLiteral(">Reporte Dia de Inventarios y Excedentes</a>\r\n                </div>\r\n           " +
"     <div");

WriteLiteral(" class=\"panel-body\"");

WriteLiteral(">\r\n                    <div");

WriteLiteral(" class=\"row\"");

WriteLiteral(">\r\n                        <div");

WriteLiteral(" class=\"form-group col-lg-3 \"");

WriteLiteral(">\r\n                            <label");

WriteLiteral(" class=\"center\"");

WriteLiteral(" for=\"select_depto\"");

WriteLiteral(" data-toggle=\"tooltip\"");

WriteLiteral(" data-placement=\"right\"");

WriteLiteral(">Departamento</label>\r\n");

WriteLiteral("                            ");

            
            #line 66 "..\..\Views\Warehouse\RPD003.cshtml"
                       Write(Html.DropDownListFor(m => m.Departamentos, Model.DeptosList, new { @class = "form-control select2", @id = "depto", @onchange = "GetFamily();" }));

            
            #line default
            #line hidden
WriteLiteral("\r\n                        </div>\r\n                        <div");

WriteLiteral(" class=\"form-group col-lg-3 \"");

WriteLiteral(">\r\n                            <label");

WriteLiteral(" class=\"center\"");

WriteLiteral(" for=\"select_family\"");

WriteLiteral(" data-toggle=\"tooltip\"");

WriteLiteral(" data-placement=\"right\"");

WriteLiteral(">Familia</label>\r\n                            <select");

WriteLiteral(" id=\"family\"");

WriteLiteral(" class=\"form-control select2 center\"");

WriteLiteral(">\r\n                                <option");

WriteLiteral(" value=\"\"");

WriteLiteral(">Seleccione una familia</option>\r\n                            </select>\r\n        " +
"                </div>\r\n                        <div");

WriteLiteral(" class=\"form-group col-lg-3 \"");

WriteLiteral(">\r\n                            <label");

WriteLiteral(" class=\"center\"");

WriteLiteral(" for=\"select_supplier\"");

WriteLiteral(" data-toggle=\"tooltip\"");

WriteLiteral(" data-placement=\"right\"");

WriteLiteral(">Proveedor</label>\r\n                            <input");

WriteLiteral(" class=\"form-control\"");

WriteLiteral(" id=\"supplier\"");

WriteLiteral(" name=\"supplier\"");

WriteLiteral(" />\r\n                        </div>\r\n                        <div");

WriteLiteral(" class=\"form-group col-lg-3\"");

WriteLiteral(">\r\n                            <br />\r\n                            <button");

WriteLiteral(" class=\"btn btn-primary\"");

WriteLiteral(" id=\"btnSearch\"");

WriteLiteral(" onclick=\"search()\"");

WriteLiteral("><i");

WriteLiteral(" class=\"fa fa-eye\"");

WriteLiteral("></i> Mostrar</button>\r\n                            <button");

WriteLiteral(" class=\"btn btn-success\"");

WriteLiteral(" id=\"btnPrint\"");

WriteLiteral(" onclick=\"print()\"");

WriteLiteral("><i");

WriteLiteral(" class=\"fa fa-print\"");

WriteLiteral("></i> Imprimir</button>\r\n                        </div>\r\n                    </di" +
"v>\r\n                    <div");

WriteLiteral(" class=\"row\"");

WriteLiteral(">\r\n                        <div");

WriteLiteral(" class=\"form-group col-lg-6 \"");

WriteLiteral(">\r\n                            <label");

WriteLiteral(" class=\"center\"");

WriteLiteral(" data-toggle=\"tooltip\"");

WriteLiteral(" data-placement=\"left\"");

WriteLiteral(">Filtrar dias de excedentes</label>\r\n                            <label> <input");

WriteLiteral(" type=\"checkbox\"");

WriteLiteral(" checked");

WriteLiteral(" id=\"ckbox_excess\"");

WriteLiteral(" onclick=\"check()\"");

WriteLiteral(" class=\"i-checks \"");

WriteLiteral("></label>\r\n                            <label");

WriteLiteral(" class=\"center\"");

WriteLiteral(" id=\"idLblCheck\"");

WriteLiteral("><input");

WriteLiteral(" class=\"form-control\"");

WriteLiteral(" type=\"number\"");

WriteLiteral(" onkeypress=\"return event.charCode >= 48 && event.charCode <= 57\"");

WriteLiteral(" min=\"0\"");

WriteLiteral(" id=\"excessDays\"");

WriteLiteral(" value=\"0\"");

WriteLiteral(" style=\"width:75px\"");

WriteLiteral("></label>\r\n                            <label");

WriteLiteral(" class=\"center\"");

WriteLiteral(" id=\"idLblDays\"");

WriteLiteral(">Dias</label>\r\n                        </div>\r\n                    </div>\r\n      " +
"              <div");

WriteLiteral(" id=\"TableDiv\"");

WriteLiteral(" class=\"animate-panel\"");

WriteLiteral(" data-effect=\"zoomIn\"");

WriteLiteral(">\r\n                        <div");

WriteLiteral(" class=\"row table-responsive\"");

WriteLiteral(">\r\n                            <table");

WriteLiteral(" id=\"tableItems\"");

WriteLiteral(" class=\"table table-striped table-bordered table-hover\"");

WriteLiteral(" width=\"100%\"");

WriteLiteral(@">
                                <thead>
                                    <tr>
                                        <th>Código Producto</th>
                                        <th>Descripción</th>
                                        <th>Dias de Inventario</th>
                                        <th>Venta Promedio Diaria</th>
                                        <th>Ultima Venta</th>
                                        <th>Ultima Compra</th>
                                        <th>Stock Disponible</th>
                                        <th>Locación</th>
                                        <th>Monto</th>
                                    </tr>
                                </thead>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div");

WriteLiteral(" class=\"modal fade\"");

WriteLiteral(" id=\"modalReference\"");

WriteLiteral(" tabindex=\"-1\"");

WriteLiteral(" role=\"dialog\"");

WriteLiteral(" aria-hidden=\"true\"");

WriteLiteral(">\r\n    <div");

WriteLiteral(" class=\"modal-dialog modal-lg\"");

WriteLiteral(">\r\n        <div");

WriteLiteral(" class=\"modal-content\"");

WriteLiteral(">\r\n            <div");

WriteLiteral(" class=\"color-line\"");

WriteLiteral("></div>\r\n            <div");

WriteLiteral(" id=\"modalReferenceBody\"");

WriteLiteral(" class=\"modal-body\"");

WriteLiteral(" style=\"height:100%\"");

WriteLiteral(">\r\n            </div>\r\n            <div");

WriteLiteral(" class=\"modal-footer\"");

WriteLiteral(">\r\n                <button");

WriteLiteral(" type=\"button\"");

WriteLiteral(" class=\"btn btn-default\"");

WriteLiteral(" data-dismiss=\"modal\"");

WriteLiteral(">Cancelar</button>\r\n            </div>\r\n        </div>\r\n    </div>\r\n</div>\r\n\r\n");

DefineSection("Scripts", () => {

WriteLiteral(" ");

            
            #line 129 "..\..\Views\Warehouse\RPD003.cshtml"
             Write(Scripts.Render("~/bundles/RPD003/js"));

            
            #line default
            #line hidden
WriteLiteral(" ");

});

        }
    }
}
#pragma warning restore 1591
