#pragma warning disable 1591
//------------------------------------------------------------------------------
// <auto-generated>
//     Este código fue generado por una herramienta.
//     Versión de runtime:4.0.30319.42000
//
//     Los cambios en este archivo podrían causar un comportamiento incorrecto y se perderán si
//     se vuelve a generar el código.
// </auto-generated>
//------------------------------------------------------------------------------

namespace ASP
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using System.Net;
    using System.Text;
    using System.Web;
    using System.Web.Helpers;
    using System.Web.Mvc;
    using System.Web.Mvc.Ajax;
    using System.Web.Mvc.Html;
    using System.Web.Optimization;
    using System.Web.Routing;
    using System.Web.Security;
    using System.Web.UI;
    using System.Web.WebPages;
    using DevExpress.Utils;
    using DevExpress.Web;
    using DevExpress.Web.ASPxThemes;
    using DevExpress.Web.Mvc;
    using DevExpress.Web.Mvc.UI;
    using DevExpress.XtraReports;
    using DevExpress.XtraReports.UI;
    using DevExpress.XtraReports.Web;
    using DevExpress.XtraReports.Web.DocumentViewer;
    using FloridoERPTX;
    
    [System.CodeDom.Compiler.GeneratedCodeAttribute("RazorGenerator", "2.0.0.0")]
    [System.Web.WebPages.PageVirtualPathAttribute("~/Views/Warehouse/RPD005.cshtml")]
    public partial class _Views_Warehouse_RPD005_cshtml : System.Web.Mvc.WebViewPage<App.Entities.ViewModels.Item.ItemSalesInventoryViewModel>
    {
        public _Views_Warehouse_RPD005_cshtml()
        {
        }
        public override void Execute()
        {
            
            #line 2 "..\..\Views\Warehouse\RPD005.cshtml"
   ViewBag.Title = "RPD005"; 
            
            #line default
            #line hidden
WriteLiteral("\r\n\r\n<div");

WriteLiteral(" class=\"normalheader transition animated fadeIn\"");

WriteLiteral(">\r\n    <div");

WriteLiteral(" class=\"hpanel\"");

WriteLiteral(">\r\n        <div");

WriteLiteral(" class=\"panel-body\"");

WriteLiteral(">\r\n            <a");

WriteLiteral(" class=\"small-header-action\"");

WriteLiteral(" href=\"\"");

WriteLiteral(">\r\n                <div");

WriteLiteral(" class=\"clip-header\"");

WriteLiteral(">\r\n                    <i");

WriteLiteral(" id=\"fa\"");

WriteLiteral(" class=\"fa fa-arrow-up\"");

WriteLiteral("></i>\r\n                </div>\r\n            </a>\r\n            <div");

WriteLiteral(" id=\"hbreadcrumb\"");

WriteLiteral(" class=\"pull-right m-t-lg\"");

WriteLiteral(">\r\n                <ol");

WriteLiteral(" class=\"hbreadcrumb breadcrumb\"");

WriteLiteral(">\r\n                    <li><a");

WriteLiteral(" href=\"WSM003\"");

WriteLiteral(">Almacen </a></li>\r\n                    <li>\r\n                        <span>Repor" +
"tes</span>\r\n                    </li>\r\n                    <li");

WriteLiteral(" class=\"active\"");

WriteLiteral(">\r\n                        <span>Códigos Faltantes</span>\r\n                    </" +
"li>\r\n                </ol>\r\n            </div>\r\n            <h2");

WriteLiteral(" class=\"font-light m-b-xs\"");

WriteLiteral(">\r\n                Códigos Faltantes\r\n            </h2>\r\n            <small>Repor" +
"te de productos que no se han surtido</small>\r\n        </div>\r\n    </div>\r\n</div" +
">\r\n<div");

WriteLiteral(" class=\"content animate-panel\"");

WriteLiteral(">\r\n    <div");

WriteLiteral(" class=\"row\"");

WriteLiteral(">\r\n        <div");

WriteLiteral(" class=\"col-lg-12\"");

WriteLiteral(">\r\n            <div");

WriteLiteral(" class=\"hpanel hblue\"");

WriteLiteral(">\r\n                <div");

WriteLiteral(" class=\"panel-heading\"");

WriteLiteral(">\r\n                    <div");

WriteLiteral(" class=\"panel-tools\"");

WriteLiteral(">\r\n                        <a");

WriteLiteral(" id=\"PanelGrid\"");

WriteLiteral(" class=\"showhide\"");

WriteLiteral("><i");

WriteLiteral(" class=\"fa fa-chevron-up\"");

WriteLiteral("></i></a>\r\n                    </div>\r\n                    <a");

WriteLiteral(" onclick=\"PanelGrid.click();\"");

WriteLiteral(">Códigos Faltantes</a>\r\n                </div>\r\n                <div");

WriteLiteral(" class=\"panel-body\"");

WriteLiteral(">\r\n                    <div");

WriteLiteral(" class=\"row\"");

WriteLiteral(">\r\n                        <div");

WriteLiteral(" class=\"col-lg-3\"");

WriteLiteral(">\r\n                            <label>Proveedor</label>\r\n                        " +
"    <input");

WriteLiteral(" class=\"form-control\"");

WriteLiteral(" id=\"supplier\"");

WriteLiteral(" name=\"supplier\"");

WriteLiteral(" />\r\n                        </div>\r\n                        <div");

WriteLiteral(" class=\"col-lg-3\"");

WriteLiteral(">\r\n                            <label>Departamento</label>\r\n                     " +
"       <select");

WriteLiteral(" id=\"department\"");

WriteLiteral(" class=\"form-control select2\"");

WriteLiteral(" onchange=\"GetFamily();\"");

WriteLiteral(">\r\n                                <option");

WriteLiteral(" value=\"0\"");

WriteLiteral(" selected>Seleccione una departamento</option>\r\n");

            
            #line 50 "..\..\Views\Warehouse\RPD005.cshtml"
                                
            
            #line default
            #line hidden
            
            #line 50 "..\..\Views\Warehouse\RPD005.cshtml"
                                 foreach (var item in Model.Departments)
                                {

            
            #line default
            #line hidden
WriteLiteral("                                    <option");

WriteAttribute("value", Tuple.Create(" value=\"", 2274), Tuple.Create("\"", 2296)
            
            #line 52 "..\..\Views\Warehouse\RPD005.cshtml"
, Tuple.Create(Tuple.Create("", 2282), Tuple.Create<System.Object, System.Int32>(item.class_id
            
            #line default
            #line hidden
, 2282), false)
);

WriteLiteral(">");

            
            #line 52 "..\..\Views\Warehouse\RPD005.cshtml"
                                                              Write(item.class_name);

            
            #line default
            #line hidden
WriteLiteral("</option>\r\n");

            
            #line 53 "..\..\Views\Warehouse\RPD005.cshtml"
                                }

            
            #line default
            #line hidden
WriteLiteral("                            </select>\r\n                        </div>\r\n          " +
"              <div");

WriteLiteral(" class=\"col-lg-3\"");

WriteLiteral(">\r\n                            <label>Familia</label>\r\n                          " +
"  <select");

WriteLiteral(" id=\"family\"");

WriteLiteral(" class=\"form-control select2\"");

WriteLiteral(">\r\n                                <option");

WriteLiteral(" value=\"0\"");

WriteLiteral(" selected>Seleccione una familia</option>\r\n                            </select>\r" +
"\n                        </div>\r\n                        <div");

WriteLiteral(" class=\"col-lg-3\"");

WriteLiteral(">\r\n                            <br />\r\n                            <button");

WriteLiteral(" id=\"Search\"");

WriteLiteral(" class=\"btn btn-primary\"");

WriteLiteral("><i");

WriteLiteral(" class=\"fa fa-eye\"");

WriteLiteral("></i> Mostrar</button>\r\n                            <button");

WriteLiteral(" id=\"Print\"");

WriteLiteral(" class=\"btn btn-success\"");

WriteLiteral(" disabled><i");

WriteLiteral(" class=\"fa fa-print\"");

WriteLiteral("></i> Imprimir</button>\r\n                        </div>\r\n                    </di" +
"v>\r\n                    <br />\r\n                    <div");

WriteLiteral(" id=\"TableDiv\"");

WriteLiteral(" class=\"animate-panel\"");

WriteLiteral(" data-effect=\"zoomIn\"");

WriteLiteral(">\r\n                        <div");

WriteLiteral(" class=\"row table-responsive\"");

WriteLiteral(">\r\n                            <table");

WriteLiteral(" id=\"Items\"");

WriteLiteral(" class=\"table table-striped table-bordered table-hover\"");

WriteLiteral(" style=\"width:100%\"");

WriteLiteral(@">
                                <thead>
                                    <tr>
                                        <th>Código Producto</th>
                                        <th>Descripción</th>
                                        <th>Stock Disponible</th>
                                        <th>Ultima Compra</th>
                                    </tr>
                                </thead>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div");

WriteLiteral(" class=\"modal fade\"");

WriteLiteral(" id=\"modalReference\"");

WriteLiteral(" tabindex=\"-1\"");

WriteLiteral(" role=\"dialog\"");

WriteLiteral(" aria-hidden=\"true\"");

WriteLiteral(">\r\n    <div");

WriteLiteral(" class=\"modal-dialog modal-lg\"");

WriteLiteral(">\r\n        <div");

WriteLiteral(" class=\"modal-content\"");

WriteLiteral(">\r\n            <div");

WriteLiteral(" class=\"color-line\"");

WriteLiteral("></div>\r\n            <div");

WriteLiteral(" id=\"modalReferenceBody\"");

WriteLiteral(" class=\"modal-body\"");

WriteLiteral(" style=\"height:100%\"");

WriteLiteral(">\r\n            </div>\r\n            <div");

WriteLiteral(" class=\"modal-footer\"");

WriteLiteral(">\r\n                <button");

WriteLiteral(" type=\"button\"");

WriteLiteral(" class=\"btn btn-default\"");

WriteLiteral(" data-dismiss=\"modal\"");

WriteLiteral(">Cancelar</button>\r\n            </div>\r\n        </div>\r\n    </div>\r\n</div>\r\n\r\n");

            
            #line 102 "..\..\Views\Warehouse\RPD005.cshtml"
Write(Html.Hidden("model", Json.Encode(Model.ItemSalesInventory)));

            
            #line default
            #line hidden
WriteLiteral("\r\n");

DefineSection("Scripts", () => {

WriteLiteral(" ");

            
            #line 103 "..\..\Views\Warehouse\RPD005.cshtml"
              Write(Scripts.Render("~/bundles/RPD005/js"));

            
            #line default
            #line hidden
WriteLiteral(" ");

});

        }
    }
}
#pragma warning restore 1591
