#pragma warning disable 1591
//------------------------------------------------------------------------------
// <auto-generated>
//     Este código fue generado por una herramienta.
//     Versión de runtime:4.0.30319.42000
//
//     Los cambios en este archivo podrían causar un comportamiento incorrecto y se perderán si
//     se vuelve a generar el código.
// </auto-generated>
//------------------------------------------------------------------------------

namespace ASP
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using System.Net;
    using System.Text;
    using System.Web;
    using System.Web.Helpers;
    using System.Web.Mvc;
    using System.Web.Mvc.Ajax;
    using System.Web.Mvc.Html;
    using System.Web.Optimization;
    using System.Web.Routing;
    using System.Web.Security;
    using System.Web.UI;
    using System.Web.WebPages;
    using DevExpress.Utils;
    using DevExpress.Web;
    using DevExpress.Web.ASPxThemes;
    using DevExpress.Web.Mvc;
    using DevExpress.Web.Mvc.UI;
    using DevExpress.XtraReports;
    using DevExpress.XtraReports.UI;
    using DevExpress.XtraReports.Web;
    using DevExpress.XtraReports.Web.DocumentViewer;
    using FloridoERPTX;
    
    [System.CodeDom.Compiler.GeneratedCodeAttribute("RazorGenerator", "2.0.0.0")]
    [System.Web.WebPages.PageVirtualPathAttribute("~/Views/Warehouse/INVE008.cshtml")]
    public partial class _Views_Warehouse_INVE008_cshtml : System.Web.Mvc.WebViewPage<FloridoERPTX.ViewModels.Warehouse.INVE008ViewModel>
    {
        public _Views_Warehouse_INVE008_cshtml()
        {
        }
        public override void Execute()
        {
            
            #line 2 "..\..\Views\Warehouse\INVE008.cshtml"
   ViewBag.Title = "INVE008"; 
            
            #line default
            #line hidden
WriteLiteral("\r\n\r\n<div");

WriteLiteral(" class=\"normalheader transition animated fadeIn\"");

WriteLiteral(">\r\n    <div");

WriteLiteral(" class=\"hpanel\"");

WriteLiteral(">\r\n        <div");

WriteLiteral(" class=\"panel-body\"");

WriteLiteral(">\r\n            <a");

WriteLiteral(" class=\"small-header-action\"");

WriteLiteral(" href=\"\"");

WriteLiteral(">\r\n                <div");

WriteLiteral(" class=\"clip-header\"");

WriteLiteral(">\r\n                    <i");

WriteLiteral(" id=\"fa\"");

WriteLiteral(" class=\"fa fa-arrow-up\"");

WriteLiteral("></i>\r\n                </div>\r\n            </a>\r\n            <div");

WriteLiteral(" id=\"hbreadcrumb\"");

WriteLiteral(" class=\"pull-right m-t-lg\"");

WriteLiteral(">\r\n                <ol");

WriteLiteral(" class=\"hbreadcrumb breadcrumb\"");

WriteLiteral(">\r\n                    <li><a");

WriteLiteral(" href=\"WSM003\"");

WriteLiteral(">Almacen</a></li>\r\n                    <li>\r\n                        <a");

WriteLiteral(" href=\"INVE000\"");

WriteLiteral(">Inventarios</a>\r\n                    </li>\r\n                    <li");

WriteLiteral(" class=\"active\"");

WriteLiteral(">\r\n                        <span>Formatos para inventario</span>\r\n               " +
"     </li>\r\n                </ol>\r\n            </div>\r\n            <h2");

WriteLiteral(" class=\"font-light m-b-xs\"");

WriteLiteral(">\r\n                Formatos para inventario\r\n            </h2>\r\n            <smal" +
"l>Imprimir Formatos para inventario</small>\r\n        </div>\r\n    </div>\r\n</div>\r" +
"\n<div");

WriteLiteral(" class=\"content animate-panel\"");

WriteLiteral(">\r\n    <div");

WriteLiteral(" class=\"row\"");

WriteLiteral(">\r\n        <div");

WriteLiteral(" class=\"col-lg-12\"");

WriteLiteral(">\r\n            <div");

WriteLiteral(" class=\"hpanel hblue\"");

WriteLiteral(">\r\n                <div");

WriteLiteral(" class=\"panel-body\"");

WriteLiteral(">\r\n                    <div");

WriteLiteral(" class=\"row\"");

WriteLiteral(">\r\n                        <div");

WriteLiteral(" class=\"col-md-4\"");

WriteLiteral(" id=\"divSelectPrint\"");

WriteLiteral(">\r\n                            <label");

WriteLiteral(" title=\"Tipo Impresion\"");

WriteLiteral(" data-toggle=\"tooltip\"");

WriteLiteral(" data-placement=\"right\"");

WriteLiteral(">Impresion</label>\r\n                            <select");

WriteLiteral(" class=\"form-control\"");

WriteLiteral(" id=\"selectType\"");

WriteLiteral(" onchange=\"ChangeType()\"");

WriteLiteral(">\r\n                                <option");

WriteLiteral(" value=\"\"");

WriteLiteral(">Seleccione una opcion</option>\r\n                                <option");

WriteLiteral(" value=\"location\"");

WriteLiteral(">Ubicación</option>\r\n                                <option");

WriteLiteral(" value=\"supplier\"");

WriteLiteral(">Proveedor</option>\r\n                                <option");

WriteLiteral(" value=\"family\"");

WriteLiteral(">Familia</option>\r\n                            </select>\r\n                       " +
" </div>\r\n                    </div>\r\n                    <br />\r\n               " +
"     <div");

WriteLiteral(" class=\"row\"");

WriteLiteral(">\r\n                        <div");

WriteLiteral(" class=\"form-group col-lg-3\"");

WriteLiteral(" id=\"divLocation\"");

WriteLiteral(" hidden>\r\n                            <label");

WriteLiteral(" title=\"Locacion\"");

WriteLiteral(" data-toggle=\"tooltip\"");

WriteLiteral(" data-placement=\"right\"");

WriteLiteral(">Locación</label>\r\n                            <input");

WriteLiteral(" class=\"form-control\"");

WriteLiteral(" id=\"location\"");

WriteLiteral(" onchange=\"CheckExistList()\"");

WriteLiteral(" />\r\n                        </div>\r\n                        <div");

WriteLiteral(" class=\"col-md-4\"");

WriteLiteral(" id=\"divItems\"");

WriteLiteral(" hidden>\r\n                            <label");

WriteLiteral(" title=\"Producto\"");

WriteLiteral(" data-toggle=\"tooltip\"");

WriteLiteral(" data-placement=\"right\"");

WriteLiteral(">Productos</label>\r\n                            <input");

WriteLiteral(" class=\"form-control\"");

WriteLiteral(" id=\"ItemsDrop\"");

WriteLiteral(" name=\"ItemsDrop\"");

WriteLiteral(" />\r\n                        </div>\r\n                        <div");

WriteLiteral(" class=\"col-md-4\"");

WriteLiteral(" id=\"divSupplier\"");

WriteLiteral(" hidden>\r\n                            <label");

WriteLiteral(" title=\"Proveedor\"");

WriteLiteral(" data-toggle=\"tooltip\"");

WriteLiteral(" data-placement=\"right\"");

WriteLiteral(">Proveedor</label>\r\n                            <select");

WriteLiteral(" class=\"form-control\"");

WriteLiteral(" id=\"selectSupplier\"");

WriteLiteral(">\r\n                                <option");

WriteLiteral(" value=\"\"");

WriteLiteral(">Seleccione un Proveedor</option>\r\n");

            
            #line 60 "..\..\Views\Warehouse\INVE008.cshtml"
                                
            
            #line default
            #line hidden
            
            #line 60 "..\..\Views\Warehouse\INVE008.cshtml"
                                 foreach (var item in Model.ListSuppliers)
                                {

            
            #line default
            #line hidden
WriteLiteral("                                    <option");

WriteAttribute("value", Tuple.Create(" value=\"", 3193), Tuple.Create("\"", 3239)
            
            #line 62 "..\..\Views\Warehouse\INVE008.cshtml"
, Tuple.Create(Tuple.Create("", 3201), Tuple.Create<System.Object, System.Int32>(item.SupplierId
            
            #line default
            #line hidden
, 3201), false)
, Tuple.Create(Tuple.Create("", 3217), Tuple.Create("/*/*", 3217), true)
            
            #line 62 "..\..\Views\Warehouse\INVE008.cshtml"
, Tuple.Create(Tuple.Create("", 3221), Tuple.Create<System.Object, System.Int32>(item.BusinessName
            
            #line default
            #line hidden
, 3221), false)
);

WriteLiteral(">");

            
            #line 62 "..\..\Views\Warehouse\INVE008.cshtml"
                                                                                      Write(item.BusinessName);

            
            #line default
            #line hidden
WriteLiteral("</option>\r\n");

            
            #line 63 "..\..\Views\Warehouse\INVE008.cshtml"
                                }

            
            #line default
            #line hidden
WriteLiteral("                            </select>\r\n                        </div>\r\n          " +
"              <div");

WriteLiteral(" class=\"col-md-4\"");

WriteLiteral(" id=\"divFamily\"");

WriteLiteral(" hidden>\r\n                            <label");

WriteLiteral(" title=\"Familia\"");

WriteLiteral(" data-toggle=\"tooltip\"");

WriteLiteral(" data-placement=\"right\"");

WriteLiteral(">Familia</label>\r\n                            <select");

WriteLiteral(" class=\"form-control\"");

WriteLiteral(" id=\"selectFamily\"");

WriteLiteral(">\r\n                                <option");

WriteLiteral(" value=\"\"");

WriteLiteral(">Seleccione una Familia</option>\r\n");

            
            #line 70 "..\..\Views\Warehouse\INVE008.cshtml"
                                
            
            #line default
            #line hidden
            
            #line 70 "..\..\Views\Warehouse\INVE008.cshtml"
                                 foreach (var item in Model.ListMaClass)
                                {

            
            #line default
            #line hidden
WriteLiteral("                                    <option");

WriteAttribute("value", Tuple.Create(" value=\"", 3870), Tuple.Create("\"", 3912)
            
            #line 72 "..\..\Views\Warehouse\INVE008.cshtml"
, Tuple.Create(Tuple.Create("", 3878), Tuple.Create<System.Object, System.Int32>(item.class_id
            
            #line default
            #line hidden
, 3878), false)
, Tuple.Create(Tuple.Create("", 3892), Tuple.Create("/*/*", 3892), true)
            
            #line 72 "..\..\Views\Warehouse\INVE008.cshtml"
, Tuple.Create(Tuple.Create("", 3896), Tuple.Create<System.Object, System.Int32>(item.class_name
            
            #line default
            #line hidden
, 3896), false)
);

WriteLiteral(">");

            
            #line 72 "..\..\Views\Warehouse\INVE008.cshtml"
                                                                                  Write(item.class_name);

            
            #line default
            #line hidden
WriteLiteral("</option>\r\n");

            
            #line 73 "..\..\Views\Warehouse\INVE008.cshtml"
                                }

            
            #line default
            #line hidden
WriteLiteral("                            </select>\r\n                        </div>\r\n          " +
"              <div");

WriteLiteral(" class=\"form-group col-lg-3\"");

WriteLiteral(" id=\"divButtons\"");

WriteLiteral(" hidden>\r\n                            <br />\r\n                            <button" +
"");

WriteLiteral(" class=\"btn btn-success\"");

WriteLiteral(" id=\"btnAddProduct\"");

WriteLiteral(" onclick=\"AddItems()\"");

WriteLiteral("><i");

WriteLiteral(" class=\"fa fa-plus\"");

WriteLiteral("></i> Agregar</button>\r\n                            <button");

WriteLiteral(" class=\"btn btn-primary\"");

WriteLiteral(" id=\"btnPrintList\"");

WriteLiteral(" onclick=\"PrintList()\"");

WriteLiteral("><i");

WriteLiteral(" class=\"fa fa-print\"");

WriteLiteral("></i> Imprimir</button>\r\n                        </div>\r\n                    </di" +
"v>\r\n                    <div");

WriteLiteral(" id=\"divTable\"");

WriteLiteral(" class=\"animate-panel\"");

WriteLiteral(" data-effect=\"zoomIn\"");

WriteLiteral(" hidden>\r\n                        <div");

WriteLiteral(" class=\"row table-responsive\"");

WriteLiteral(">\r\n                            <table");

WriteLiteral(" id=\"TableProducts\"");

WriteLiteral(" class=\"table table-striped table-bordered table-hover\"");

WriteLiteral(" style=\"width:100%\"");

WriteLiteral(@">
                                <thead>
                                    <tr>
                                        <th>Código</th>
                                        <th>Descripcion</th>
                                        <th>Eliminar</th>
                                    </tr>
                                </thead>
                                <tbody></tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div");

WriteLiteral(" class=\"modal fade\"");

WriteLiteral(" id=\"modalReference\"");

WriteLiteral(" tabindex=\"-1\"");

WriteLiteral(" role=\"dialog\"");

WriteLiteral(" aria-hidden=\"true\"");

WriteLiteral(">\r\n    <div");

WriteLiteral(" class=\"modal-dialog modal-lg\"");

WriteLiteral(">\r\n        <div");

WriteLiteral(" class=\"modal-content\"");

WriteLiteral(">\r\n            <div");

WriteLiteral(" class=\"color-line\"");

WriteLiteral("></div>\r\n            <div");

WriteLiteral(" id=\"modalReferenceBody\"");

WriteLiteral(" class=\"modal-body\"");

WriteLiteral(" style=\"height:100%\"");

WriteLiteral(">\r\n            </div>\r\n            <div");

WriteLiteral(" class=\"modal-footer\"");

WriteLiteral(">\r\n                <button");

WriteLiteral(" type=\"button\"");

WriteLiteral(" class=\"btn btn-default\"");

WriteLiteral(" data-dismiss=\"modal\"");

WriteLiteral("><i");

WriteLiteral(" class=\"fa fa-close\"");

WriteLiteral("></i> Cancelar</button>\r\n            </div>\r\n        </div>\r\n    </div>\r\n</div>\r\n" +
"\r\n");

DefineSection("Scripts", () => {

WriteLiteral("\r\n    <script");

WriteLiteral(" type=\"text/javascript\"");

WriteLiteral(">\r\n\r\n        var listItems = [];\r\n        $(\"#selectFamily\").select2();\r\n        " +
"$(\"#selectSupplier\").select2();\r\n        $(\"#selectType\").select2();\r\n\r\n        " +
"$(\"#ItemsDrop\").select2({\r\n            minimumInputLength: 3,\r\n            allow" +
"Clear: true,\r\n            placeholder: \"Seleccione una producto\",\r\n            i" +
"nitSelection: function (element, callback) {\r\n                callback({ id: \"\"," +
" text: \"Seleccione una opcion\" });\r\n            },\r\n            ajax: {\r\n       " +
"         url: \"/Items/ActionSearchItemBarcode/\",\r\n                dataType: \'jso" +
"n\',\r\n                type: \"GET\",\r\n                quietMillis: 50,\r\n           " +
"     data: function (Filter) {\r\n                    return {\r\n                  " +
"      Filter: Filter\r\n                    };\r\n                },\r\n              " +
"  results: function (data) {\r\n                    return {\r\n                    " +
"    results: $.map(data.Json, function (item) {\r\n                            ret" +
"urn {\r\n                                text: item.Description,\r\n                " +
"                id: item.PathNumber + \'*/*/*/\' + item.Description\r\n             " +
"               }\r\n                        }),\r\n                    };\r\n         " +
"       }\r\n            }\r\n        });\r\n\r\n        $(document).ready(function () {\r" +
"\n            window.setTimeout(function () { $(\"#fa\").click(); }, 1000);\r\n      " +
"  });\r\n\r\n        var table = $(\'#TableProducts\').DataTable({\r\n            oLangu" +
"age:\r\n            {\r\n                \"sProcessing\": \"Procesando...\",\r\n          " +
"      \"sLengthMenu\": \"Mostrar _MENU_ registros\",\r\n                \"sZeroRecords\"" +
": \"No se encontraron resultados\",\r\n                \"sEmptyTable\": \"Ningún dato d" +
"isponible en esta tabla\",\r\n                \"sInfo\": \"Mostrando registros del _ST" +
"ART_ al _END_ de un total de _TOTAL_ registros\",\r\n                \"sInfoEmpty\": " +
"\"Mostrando registros del 0 al 0 de un total de 0 registros\",\r\n                \"s" +
"InfoFiltered\": \"(filtrado de un total de _MAX_ registros)\",\r\n                \"sI" +
"nfoPostFix\": \"\",\r\n                \"sSearch\": \"Buscar:\",\r\n                \"sUrl\":" +
" \"\",\r\n                \"sInfoThousands\": \",\",\r\n                \"sLoadingRecords\":" +
" \"Cargando...\",\r\n                \"oPaginate\": { \"sFirst\": \"Primero\", \"sLast\": \"Ú" +
"ltimo\", \"sNext\": \"Siguiente\", \"sPrevious\": \"Anterior\" },\r\n                \"oAria" +
"\": { \"sSortAscending\": \": Activar para ordenar la columna de manera ascendente\"," +
" \"sSortDescending\": \": Activar para ordenar la columna de manera descendente\" }\r" +
"\n            },\r\n            dom: \"<\'row\'<\'col-sm-4\'l><\'col-sm-4 text-left\'B><\'c" +
"ol-sm-4\'f>t<\'col-sm-6\'i><\'col-sm-6\'p>>\",\r\n            lengthMenu: [[10, 25, 50, " +
"-1], [10, 25, 50, \"Todos\"]],\r\n            buttons: [],\r\n            columns:\r\n  " +
"              [\r\n                    { data: \'part_number\' },\r\n                 " +
"   { data: \'part_description\' },\r\n                    { data: null }\r\n          " +
"      ],\r\n            columnDefs:\r\n                [\r\n                    {\r\n\r\n " +
"                       targets: [2],\r\n                        width: \"1%\",\r\n    " +
"                    //defaultContent: \'<button class=\"btn btn-xs btn-outline btn" +
"-danger\" id=\"btnDelete\" onclick=\"DeleteItem()\"><i class=\"fa fa-close\"></i> Elimi" +
"nar</button>\'\r\n                        render: function (data, type, full, meta)" +
" {\r\n                            return `<button class=\"btn btn-xs btn-outline bt" +
"n-danger\" id=\"btnDelete\" onclick=\"DeleteItem(\'` + full.part_number + `\')\"><i cla" +
"ss=\"fa fa-close\"></i> Eliminar</button>`;\r\n                        }\r\n          " +
"          },\r\n                    {\r\n                        targets: [0, 1],\r\n " +
"                       width: \"1%\"\r\n                    }\r\n                ]\r\n  " +
"      });\r\n\r\n        function AddItems() {\r\n            var type = $(\"#selectTyp" +
"e\").val();\r\n            if (type == \"location\") {\r\n                var item = $(" +
"\"#ItemsDrop\").val();\r\n                if (item == \"\") {\r\n                    toa" +
"str.warning(\"Ingrese un producto\");\r\n                    return;\r\n              " +
"  }\r\n                var part_number = item.split(\"*/*/*/\")[0];\r\n               " +
" var part_description = item.split(\"*/*/*/\")[1];\r\n                var newItem = " +
"{ part_number: part_number, part_description: part_description }\r\n\r\n            " +
"    if (listItems.findIndex(x => x.part_number == part_number) > 0) { toastr.err" +
"or(\"El producto ya existe en la lista\"); return;}\r\n                StartLoading(" +
");\r\n                $.ajax({\r\n                    type: \"POST\",\r\n               " +
"     url: \"/InventoryLocation/ActionAddInventoryFormat/\",\r\n                    d" +
"ata: { \"location\": $(\"#location\").val() , \"part_number\": part_number},\r\n        " +
"            success: function (json) {\r\n                        if (json.success" +
") {\r\n                            toastr.success(\"Producto Registrado correctamen" +
"te\");\r\n                            listItems = json.Data\r\n                      " +
"      fillTable(listItems);\r\n                            EndLoading();\r\n        " +
"                } else {\r\n                            if (json.Data == \"SF\") {\r\n" +
"                                SessionFalse(\"Terminó tu sesión\");\r\n            " +
"                } else {\r\n                                toastr.error(\'Alerta -" +
" Error inesperado contacte a sistemas\');\r\n                            }\r\n       " +
"                     EndLoading();\r\n                        }\r\n                 " +
"   },\r\n                    error: function (json) {\r\n                        toa" +
"str.error(\'Alerta - Error inesperado contactar a sistemas.\');\r\n                 " +
"       EndLoading();\r\n                    }\r\n                });\r\n\r\n            " +
"} else if (type == \"supplier\") {\r\n                listItems = [];\r\n             " +
"   var supplier = $(\"#selectSupplier\").val().split(\"/*/*\")[0];\r\n                " +
"if (supplier == \"\") {\r\n                    toastr.warning(\"Seleccione un proveed" +
"or\");\r\n                    return;\r\n                } else {\r\n                  " +
"  $.ajax({\r\n                        type: \"POST\",\r\n                        url: " +
"\"/ItemSuppliers/ActionGetAllItemsSuppliers/\",\r\n                        data: { \"" +
"supplier_id\": supplier },\r\n                        success: function (json) {\r\n " +
"                           if (json.success) {\r\n                                " +
"json.Data.forEach(x => { item = { part_number: x.PathNumber, part_description: x" +
".Description }; listItems.push(item); })\r\n                                fillTa" +
"ble(listItems);\r\n                                EndLoading();\r\n                " +
"            } else {\r\n                                if (json.data == \"SF\") {\r\n" +
"                                    SessionFalse(\"Terminó tu sesión\");\r\n        " +
"                        } else {\r\n                                    toastr.err" +
"or(\'Alerta - Error inesperado contacte a sistemas\');\r\n                          " +
"      }\r\n                            }\r\n                        },\r\n            " +
"            error: function (json) {\r\n                            toastr.error(\'" +
"Alerta - Error inesperado contactar a sistemas.\');\r\n                            " +
"EndLoading();\r\n                        }\r\n                    });\r\n             " +
"   }\r\n            } else if (type == \"family\") {\r\n                listItems = []" +
";\r\n                var family = $(\"#selectFamily\").val().split(\"/*/*\")[0];\r\n    " +
"            if (family == \"\") {\r\n                    toastr.warning(\"Seleccione " +
"una familia\");\r\n                } else {\r\n                    $.ajax({\r\n        " +
"                type: \"POST\",\r\n                        url: \"/Items/ActionGetIte" +
"msByDepartmentInventory/\",\r\n                        data: { \"id\": family },\r\n   " +
"                     success: function (json) {\r\n                            con" +
"sole.log(json)\r\n                            if (json.success) {\r\n               " +
"                 json.Data.forEach(x => { item = { part_number: x.PartNumber, pa" +
"rt_description: x.Description }; listItems.push(item); })\r\n                     " +
"           fillTable(listItems);\r\n                                EndLoading();\r" +
"\n                            } else {\r\n                                if (json." +
"data == \"SF\") {\r\n                                    SessionFalse(\"Terminó tu se" +
"sión\");\r\n                                } else {\r\n                             " +
"       toastr.error(\'Alerta - Error inesperado contacte a sistemas\');\r\n         " +
"                       }\r\n                            }\r\n                       " +
" },\r\n                        error: function (json) {\r\n                         " +
"   toastr.error(\'Alerta - Error inesperado contactar a sistemas.\');\r\n           " +
"                 EndLoading();\r\n                        }\r\n                    }" +
");\r\n                }\r\n            }\r\n        }\r\n\r\n        function DeleteItem(p" +
"art_number) {\r\n            StartLoading();\r\n            $.ajax({\r\n              " +
"  type: \"POST\",\r\n                url: \"/InventoryLocation/ActionDeleteInventoryF" +
"ormat/\",\r\n                data: { \"location\": $(\"#location\").val(), \"part_number" +
"\": part_number },\r\n                success: function (json) {\r\n                 " +
"   if (json.success) {\r\n                        toastr.success(\"Producto Elimina" +
"do correctamente\");\r\n                        listItems = json.Data\r\n            " +
"            fillTable(listItems);\r\n                        EndLoading();\r\n      " +
"              } else {\r\n                        if (json.Data == \"SF\") {\r\n      " +
"                      SessionFalse(\"Terminó tu sesión\");\r\n                      " +
"  } else {\r\n                            toastr.error(\'Alerta - Error inesperado " +
"contacte a sistemas\');\r\n                        }\r\n                        EndLo" +
"ading();\r\n                    }\r\n                },\r\n                error: func" +
"tion (json) {\r\n                    toastr.error(\'Alerta - Error inesperado conta" +
"ctar a sistemas.\');\r\n                    EndLoading();\r\n                }\r\n     " +
"       });\r\n        }\r\n\r\n        function PrintList() {\r\n            var locatio" +
"n = $(\"#location\").val();\r\n            var type = $(\"#selectType\").val();\r\n     " +
"       var desc;\r\n            var print = false;\r\n            if (type == \"locat" +
"ion\") {\r\n                desc = location;\r\n                if (location == \"\") {" +
"\r\n                    toastr.warning(\"Ingrese locación\");\r\n                    r" +
"eturn;\r\n                } else if (listItems.length == 0) {\r\n                   " +
" toastr.warning(\"Agregue por lo menos un producto\");\r\n                    return" +
";\r\n                }\r\n                StartLoading();\r\n                print = t" +
"rue;\r\n            } else if (type == \"supplier\") {\r\n                desc = $(\"#s" +
"electSupplier\").val().split(\"/*/*\")[1];\r\n                print = true;\r\n        " +
"    } else if (type == \"family\") {\r\n                desc = $(\"#selectFamily\").va" +
"l().split(\"/*/*\")[1];\r\n                print = true;\r\n            }\r\n\r\n         " +
"   if (print) {\r\n                StartLoading();\r\n                $.ajax({\r\n    " +
"                type: \"POST\",\r\n                    url: \"/InventoryHeader/Action" +
"GetReport/\",\r\n                    data: { \"items\": listItems, \"desc\": desc, \"typ" +
"e\": type },\r\n                    success: function (json) {\r\n                   " +
"     if (json.success) {\r\n                            $(\"#modalReferenceBody\").h" +
"tml(\"<iframe width=\'100%\' height=\'550px\' src=\'data:application/pdf;base64, \" + e" +
"ncodeURI(json.Data) + \"\'></iframe>\")\r\n                            $(\"#modalRefer" +
"ence\").modal(\'show\');\r\n                            EndLoading();\r\n              " +
"          } else {\r\n                            if (json.data == \"SF\") {\r\n      " +
"                          SessionFalse(\"Terminó tu sesión\");\r\n                  " +
"          } else {\r\n                                toastr.error(\'Alerta - Ha oc" +
"urrido un error al guardar el registro\');\r\n                            }\r\n      " +
"                  }\r\n                    },\r\n                    error: function" +
" (json) {\r\n                        toastr.error(\'Alerta - Error inesperado conta" +
"ctar a sistemas.\');\r\n                        EndLoading();\r\n                    " +
"}\r\n                });\r\n            }\r\n        }\r\n\r\n        function CheckExistL" +
"ist() {\r\n            var loc = $(\"#location\").val();\r\n            StartLoading()" +
";\r\n            $.ajax({\r\n                type: \"POST\",\r\n                url: \"/I" +
"nventoryLocation/ActionGetInventoryFormat/\",\r\n                data: { \"location\"" +
": loc},\r\n                success: function (json) {\r\n                    if (jso" +
"n.success) {\r\n                        listItems = json.Data\r\n                   " +
"    fillTable(listItems)\r\n                        EndLoading();\r\n               " +
"     } else {\r\n                        if (json.Data == \"SF\") {\r\n               " +
"             SessionFalse(\"Terminó tu sesión\");\r\n                        } else " +
"{\r\n                            toastr.error(\'Alerta - Ha ocurrido un error al gu" +
"ardar el registro\');\r\n                        }\r\n                    }\r\n        " +
"        },\r\n                error: function (json) {\r\n                    toastr" +
".error(\'Alerta - Error inesperado contactar a sistemas.\');\r\n                    " +
"EndLoading();\r\n                }\r\n            });\r\n        }\r\n\r\n        function" +
" ChangeType() {\r\n            listItems = [];\r\n            fillTable(listItems);\r" +
"\n            $(\"#location\").val(null);\r\n            var type = $(\"#selectType\")." +
"val();\r\n            if (type == \"location\") {\r\n                $(\"#divLocation\")" +
".show();\r\n                $(\"#divItems\").show();\r\n                ShowTableButto" +
"ns();\r\n                $(\"#divSupplier\").hide();\r\n                $(\"#divFamily\"" +
").hide();\r\n                ClearTable();\r\n            } else if (type == \"suppli" +
"er\") {\r\n                $(\"#divLocation\").hide();\r\n                $(\"#divItems\"" +
").hide();\r\n                $(\"#divFamily\").hide();\r\n                $(\"#divSuppl" +
"ier\").show();\r\n                ShowTableButtons();\r\n                divLocationH" +
"ide();\r\n                ClearTable();\r\n            } else if (type == \"family\") " +
"{\r\n                divLocationHide();\r\n                $(\"#divSupplier\").hide();" +
"\r\n                $(\"#divFamily\").show();\r\n                ShowTableButtons();\r\n" +
"            }\r\n        }\r\n\r\n        function ShowTableButtons() {\r\n            $" +
"(\"#divTable\").show();\r\n            $(\"#divButtons\").show();\r\n        }\r\n\r\n      " +
"  function ClearTable() {\r\n            table.clear().draw();\r\n            table." +
"columns.adjust().draw();\r\n        }\r\n\r\n        function divLocationHide() {\r\n   " +
"         $(\"#divLocation\").hide();\r\n            $(\"#divItems\").hide();\r\n        " +
"}\r\n\r\n        function fillTable(listItems) {\r\n            table.clear().draw();\r" +
"\n            table.rows.add(listItems)\r\n            table.columns.adjust().draw(" +
");\r\n        }\r\n    </script>\r\n");

});

        }
    }
}
#pragma warning restore 1591
