#pragma warning disable 1591
//------------------------------------------------------------------------------
// <auto-generated>
//     Este código fue generado por una herramienta.
//     Versión de runtime:4.0.30319.42000
//
//     Los cambios en este archivo podrían causar un comportamiento incorrecto y se perderán si
//     se vuelve a generar el código.
// </auto-generated>
//------------------------------------------------------------------------------

namespace ASP
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using System.Net;
    using System.Text;
    using System.Web;
    using System.Web.Helpers;
    using System.Web.Mvc;
    using System.Web.Mvc.Ajax;
    using System.Web.Mvc.Html;
    using System.Web.Optimization;
    using System.Web.Routing;
    using System.Web.Security;
    using System.Web.UI;
    using System.Web.WebPages;
    using DevExpress.Utils;
    using DevExpress.Web;
    using DevExpress.Web.ASPxThemes;
    using DevExpress.Web.Mvc;
    using DevExpress.Web.Mvc.UI;
    using DevExpress.XtraReports;
    using DevExpress.XtraReports.UI;
    using DevExpress.XtraReports.Web;
    using DevExpress.XtraReports.Web.DocumentViewer;
    using FloridoERPTX;
    
    [System.CodeDom.Compiler.GeneratedCodeAttribute("RazorGenerator", "2.0.0.0")]
    [System.Web.WebPages.PageVirtualPathAttribute("~/Views/Warehouse/RPD015.cshtml")]
    public partial class _Views_Warehouse_RPD015_cshtml : System.Web.Mvc.WebViewPage<FloridoERPTX.ViewModels.Warehouse.RPD003Model>
    {
        public _Views_Warehouse_RPD015_cshtml()
        {
        }
        public override void Execute()
        {
WriteLiteral("\r\n");

            
            #line 2 "..\..\Views\Warehouse\RPD015.cshtml"
   ViewBag.Title = "RPD015";


            
            #line default
            #line hidden
WriteLiteral("\r\n");

WriteLiteral("<div");

WriteLiteral(" class=\"normalheader transition animated fadeIn\"");

WriteLiteral(">\r\n    <div");

WriteLiteral(" class=\"hpanel\"");

WriteLiteral(">\r\n        <div");

WriteLiteral(" class=\"panel-body\"");

WriteLiteral(">\r\n            <a");

WriteLiteral(" class=\"small-header-action\"");

WriteLiteral(" href=\"\"");

WriteLiteral(">\r\n                <div");

WriteLiteral(" class=\"clip-header\"");

WriteLiteral(">\r\n                    <i");

WriteLiteral(" id=\"fa\"");

WriteLiteral(" class=\"fa fa-arrow-up\"");

WriteLiteral("></i>\r\n                </div>\r\n            </a>\r\n            <div");

WriteLiteral(" id=\"hbreadcrumb\"");

WriteLiteral(" class=\"pull-right m-t-lg\"");

WriteLiteral(">\r\n                <ol");

WriteLiteral(" class=\"hbreadcrumb breadcrumb\"");

WriteLiteral(">\r\n                    <li><a");

WriteLiteral(" href=\"WSM003\"");

WriteLiteral(">Almacen</a></li>\r\n                    <li>\r\n                        <span>Report" +
"es</span>\r\n                    </li>\r\n                    <li");

WriteLiteral(" class=\"active\"");

WriteLiteral(">\r\n                        <span>Kardex Horizontal</span>\r\n                    </" +
"li>\r\n                </ol>\r\n            </div>\r\n            <h2");

WriteLiteral(" class=\"font-light m-b-xs\"");

WriteLiteral(">\r\n                Kardex Horizontal.\r\n            </h2>\r\n            <small>Repo" +
"rte de Movimientos por Producto.</small>\r\n        </div>\r\n    </div>\r\n</div>\r\n<d" +
"iv");

WriteLiteral(" class=\"content animate-panel\"");

WriteLiteral(">\r\n    <div");

WriteLiteral(" class=\"row\"");

WriteLiteral(">\r\n        <div");

WriteLiteral(" class=\"col-lg-12\"");

WriteLiteral(">\r\n            <div");

WriteLiteral(" class=\"hpanel hblue\"");

WriteLiteral(">\r\n                <div");

WriteLiteral(" class=\"panel-body\"");

WriteLiteral(">\r\n                    <div");

WriteLiteral(" class=\"row\"");

WriteLiteral(">\r\n                        <div");

WriteLiteral(" class=\"form-group col-lg-3\"");

WriteLiteral(">\r\n                            <label");

WriteLiteral(" title=\"Fecha Inicial\"");

WriteLiteral(" data-toggle=\"tooltip\"");

WriteLiteral(" data-placement=\"right\"");

WriteLiteral(">Periodo</label>\r\n                            <div");

WriteLiteral(" class=\"input-daterange input-group\"");

WriteLiteral(" id=\"datepicker\"");

WriteLiteral(">\r\n                                <div");

WriteLiteral(" class=\"input-group date\"");

WriteLiteral(">\r\n                                    <span");

WriteLiteral(" class=\"input-group-addon\"");

WriteLiteral(">\r\n                                        <span");

WriteLiteral(" class=\"fa fa-calendar\"");

WriteLiteral("></span>\r\n                                    </span>\r\n");

WriteLiteral("                                    ");

            
            #line 46 "..\..\Views\Warehouse\RPD015.cshtml"
                               Write(Html.TextBox("ValidDate1", null, new { @onchange = "", @class = "form-control", @autocomplete = "off" }));

            
            #line default
            #line hidden
WriteLiteral("\r\n                                </div>\r\n                                <span");

WriteLiteral(" class=\"input-group-addon\"");

WriteLiteral(">A</span>\r\n                                <div");

WriteLiteral(" class=\"input-group date\"");

WriteLiteral(">\r\n");

WriteLiteral("                                    ");

            
            #line 50 "..\..\Views\Warehouse\RPD015.cshtml"
                               Write(Html.TextBox("ValidDate2", null, new { @onchange = "", @class = "form-control", @autocomplete = "off" }));

            
            #line default
            #line hidden
WriteLiteral("\r\n                                </div>\r\n                            </div>\r\n   " +
"                     </div>\r\n                        <div");

WriteLiteral(" class=\"form-group col-lg-3\"");

WriteLiteral(">\r\n                            <label");

WriteLiteral(" title=\"Producto\"");

WriteLiteral(" data-toggle=\"tooltip\"");

WriteLiteral(" data-placement=\"right\"");

WriteLiteral(">Productos</label>\r\n                            <input");

WriteLiteral(" class=\"form-control\"");

WriteLiteral(" id=\"ItemsDrop\"");

WriteLiteral(" name=\"ItemsDrop\"");

WriteLiteral(" />\r\n                        </div>\r\n                        <div");

WriteLiteral(" class=\"form-group col-lg-3 \"");

WriteLiteral(">\r\n                            <label");

WriteLiteral(" class=\"center\"");

WriteLiteral(" for=\"select_depto\"");

WriteLiteral(" data-toggle=\"tooltip\"");

WriteLiteral(" data-placement=\"right\"");

WriteLiteral(">Departamento</label>\r\n");

WriteLiteral("                            ");

            
            #line 60 "..\..\Views\Warehouse\RPD015.cshtml"
                       Write(Html.DropDownListFor(m => m.Departamentos, Model.DeptosList, new { @class = "form-control select2", @id = "depto", @onchange = "" }));

            
            #line default
            #line hidden
WriteLiteral("\r\n                        </div>\r\n                        <div");

WriteLiteral(" class=\"form-group col-lg-3\"");

WriteLiteral(">\r\n                            <br />\r\n                            <button");

WriteLiteral(" class=\"btn btn-primary\"");

WriteLiteral(" id=\"btnSearchMovements\"");

WriteLiteral("><i");

WriteLiteral(" class=\"fa fa-eye\"");

WriteLiteral("></i> Mostrar</button>\r\n                        </div>\r\n                    </div" +
">\r\n                    <div");

WriteLiteral(" id=\"TableDiv\"");

WriteLiteral(" class=\"animate-panel\"");

WriteLiteral(" data-effect=\"zoomIn\"");

WriteLiteral(">\r\n                        <div");

WriteLiteral(" class=\"row table-responsive\"");

WriteLiteral(">\r\n                            <table");

WriteLiteral(" id=\"TableProducts\"");

WriteLiteral(" class=\"table table-striped table-bordered table-hover\"");

WriteLiteral(" style=\"width:100%\"");

WriteLiteral(">\r\n                                <thead>\r\n                                    <" +
"tr>\r\n                                        <th>Codigo</th>\r\n                  " +
"                      <th>Descripcion</th>\r\n                                    " +
"    <th>Stock Inicial</th>\r\n                                        <th>Ajuste I" +
"nventario</th>\r\n                                        <th>$ Ajuste Inventario<" +
"/th>\r\n                                        <th>Entrada Negativo</th>\r\n       " +
"                                 <th>$ Entrada Negativo</th>\r\n                  " +
"                      <th>Salida Negativo</th>\r\n                                " +
"        <th>$ Salida Negativo</th>\r\n                                        <th>" +
"Salida Centro De Costos</th>\r\n                                        <th>$ Sali" +
"da Centro De Costos</th>\r\n                                        <th>Salida Por" +
" Venta</th>\r\n                                        <th>$ Salida Por Venta</th>" +
"\r\n                                        <th>Salida Remision</th>\r\n            " +
"                            <th>$ Salida Remision</th>\r\n                        " +
"                <th>Salida RMA</th>\r\n                                        <th" +
">$ Salida RMA</th>\r\n                                        <th>Salida Merma</th" +
">\r\n                                        <th>$ Salida Merma</th>\r\n            " +
"                            <th>Reversa Venta</th>\r\n                            " +
"            <th>$ Reversa Venta</th>\r\n                                        <t" +
"h>Reversa RMA</th>\r\n                                        <th>$ Reversa RMA</t" +
"h>\r\n                                        <th>Salida Combo</th>\r\n             " +
"                           <th>$ Salida Combo</th>\r\n                            " +
"            <th>Salida Packs</th>\r\n                                        <th>$" +
" Salida Packs</th>\r\n                                        <th>Salida Recetas</" +
"th>\r\n                                        <th>$ Salida Recetas</th>\r\n        " +
"                                <th>Salida Mat Prima</th>\r\n                     " +
"                   <th>$ Salida Mat Prima</th>\r\n                                " +
"        <th>Salida Transformacion Piezas</th>\r\n                                 " +
"       <th>$ Salida Transformacion Piezas</th>\r\n                                " +
"        <th>Salida Transferencia</th>\r\n                                        <" +
"th>$ Salida Transferencia</th>\r\n                                        <th>Entr" +
"ada Cancelacion OC</th>\r\n                                        <th>$ Entrada C" +
"ancelacion OC</th>\r\n                                        <th>Entrada Combo</t" +
"h>\r\n                                        <th>$ Entrada Combo</th>\r\n          " +
"                              <th>Entrada Packs</th>\r\n                          " +
"              <th>$ Entrada Packs</th>\r\n                                        " +
"<th>Entrada Recetas</th>\r\n                                        <th>$ Entrada " +
"Recetas</th>\r\n                                        <th>Entrada Transformacion" +
" Piezas</th>\r\n                                        <th>$ Entrada Transformaci" +
"on Piezas</th>\r\n                                        <th>Entrada Orden Compra" +
"</th>\r\n                                        <th>$ Entrada Orden Compra</th>\r\n" +
"                                        <th>Entrada Sin Costo</th>\r\n            " +
"                            <th>$ Entrada Sin Costo</th>\r\n                      " +
"                  <th>Entrada Nota Credito</th>\r\n                               " +
"         <th>$ Entrada Nota Credito</th>\r\n                                      " +
"  <th>Reversa Nota Credito</th>\r\n                                        <th>$ R" +
"eversa Nota Credito</th>\r\n                                        <th>Reversa Tr" +
"ansferencia</th>\r\n                                        <th>$ Reversa Transfer" +
"encia</th>\r\n                                        <th>Entrada Transferencia</t" +
"h>\r\n                                        <th>$ Entrada Transferencia</th>\r\n  " +
"                                      ");

WriteLiteral("\r\n                                        ");

WriteLiteral(@"
                                        <th>Stock Final</th>
                                </thead>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

");

DefineSection("Scripts", () => {

WriteLiteral("\r\n    <script");

WriteLiteral(" type=\"text/javascript\"");

WriteLiteral(">\r\n\r\n        var listItems = [];\r\n        $(\"#selectClass\").select2();\r\n        $" +
"(\"#selectType\").select2();\r\n        var id_location = 0;\r\n\r\n        $(\"#ValidDat" +
"e1\").datepicker({\r\n            autoclose: true,\r\n            todayHighlight: tru" +
"e,\r\n            showButtonPanel: true,\r\n        });\r\n\r\n        $(\"#ValidDate2\")." +
"datepicker({\r\n            autoclose: true,\r\n            startDate: \'01/01/1950\'," +
"\r\n            todayHighlight: true,\r\n        });\r\n        $(\"#ItemsDrop\").select" +
"2({\r\n            minimumInputLength: 3,\r\n            allowClear: true,\r\n        " +
"    placeholder: \"Seleccione un producto\",\r\n            initSelection: function " +
"(element, callback) {\r\n                callback({ id: \"\", text: \"Seleccione una " +
"opcion\" });\r\n            },\r\n            ajax: {\r\n                url: \"/Items/A" +
"ctionSearchItemBarcodeNoFlags/\",\r\n                dataType: \'json\',\r\n           " +
"     type: \"GET\",\r\n                quietMillis: 50,\r\n                data: funct" +
"ion (Filter) {\r\n                    return {\r\n                        Filter: Fi" +
"lter\r\n                    };\r\n                },\r\n                results: funct" +
"ion (data) {\r\n                    return {\r\n                        results: $.m" +
"ap(data.Json, function (item) {\r\n                            return {\r\n         " +
"                       text: `${item.PathNumber} - ${item.Description}`,\r\n      " +
"                          id: item.PathNumber\r\n                            }\r\n  " +
"                      }),\r\n                    };\r\n                }\r\n          " +
"  }\r\n        });\r\n\r\n\r\n        const formatmoney = money => decimal => new Intl.N" +
"umberFormat(\"en-US\", { style: \"currency\", currency: \"USD\", currencyDisplay: \"sym" +
"bol\", maximumFractionDigits: decimal }).format(money);\r\n\r\n\r\n        var columnas" +
" = 58\r\n        var sinSigno = [3, 5, 7, 9, 11, 13, 15, 17, 19, 21, 23, 25, 27, 2" +
"9, 31, 33, 35, 37, 39, 41, 43, 45, 47, 49, 51, 53, 55, 57]\r\n        var conSigno" +
" = [4, 6, 8, 10, 12, 14, 16, 18, 20, 22, 24, 26, 28, 30, 32, 34, 36, 38, 40, 42," +
" 44, 46, 48, 50, 52, 54, 56]\r\n        var par = false;\r\n\r\n\r\n\r\n        $(document" +
").ready(function () {\r\n            window.setTimeout(function () { $(\"#fa\").clic" +
"k(); }, 1000);\r\n            window.setTimeout(function () {\r\n\r\n                f" +
"or (var i = 3; i <= columnas; i++) {\r\n                    if (par) {\r\n          " +
"              conSigno.push(i)\r\n                        par = false;\r\n          " +
"          } else {\r\n                        sinSigno.push(i)\r\n                  " +
"      par = true;\r\n                    }\r\n                }\r\n            }, 500)" +
";\r\n\r\n\r\n        });\r\n        $(\"#TableProducts\").append(\'<tfoot><th colspan = \"3\"" +
">Totales</th><th></th><th></th><th></th><th></th><th></th><th></th><th></th><th>" +
"</th><th></th><th></th><th></th><th></th><th></th><th></th><th></th><th></th><th" +
"></th><th></th><th></th><th></th><th></th><th></th><th></th><th></th><th></th><t" +
"h></th><th></th><th></th><th></th><th></th><th></th><th></th><th></th><th></th><" +
"th></th><th></th><th></th><th></th><th></th><th></th><th></th><th></th><th></th>" +
"<th></th><th></th><th></th><th></th><th></th><th></th><th></th><th></th><th></th" +
"><th></th><th></th></tfoot>\');\r\n\r\n        var table = $(\'#TableProducts\').DataTa" +
"ble({\r\n            oLanguage:\r\n            {\r\n                \"sProcessing\": \"Pr" +
"ocesando...\",\r\n                \"sLengthMenu\": \"Mostrar _MENU_ registros\",\r\n     " +
"           \"sZeroRecords\": \"No se encontraron resultados\",\r\n                \"sEm" +
"ptyTable\": \"Ningún dato disponible en esta tabla\",\r\n                \"sInfo\": \"Mo" +
"strando registros del _START_ al _END_ de un total de _TOTAL_ registros\",\r\n     " +
"           \"sInfoEmpty\": \"Mostrando registros del 0 al 0 de un total de 0 regist" +
"ros\",\r\n                \"sInfoFiltered\": \"(filtrado de un total de _MAX_ registro" +
"s)\",\r\n                \"sInfoPostFix\": \"\",\r\n                \"sSearch\": \"Buscar:\"," +
"\r\n                \"sUrl\": \"\",\r\n                \"sInfoThousands\": \",\",\r\n         " +
"       \"sLoadingRecords\": \"Cargando...\",\r\n                \"oPaginate\": { \"sFirst" +
"\": \"Primero\", \"sLast\": \"Último\", \"sNext\": \"Siguiente\", \"sPrevious\": \"Anterior\" }" +
",\r\n                \"oAria\": { \"sSortAscending\": \": Activar para ordenar la colum" +
"na de manera ascendente\", \"sSortDescending\": \": Activar para ordenar la columna " +
"de manera descendente\" }\r\n            },\r\n            dom: \"<\'row\'<\'col-sm-4\'l><" +
"\'col-sm-4 text-left\'B><\'col-sm-4\'f>t<\'col-sm-6\'i><\'col-sm-6\'p>>\",\r\n            l" +
"engthMenu: [[10, 25, 50, -1], [10, 25, 50, \"Todos\"]],\r\n            buttons: [\r\n " +
"               {\r\n                    extend: \'csv\', text: \'Excel\', title: \'Kard" +
"ex Horizontal\', className: \'btn-sm\', footer: true\r\n                },\r\n         " +
"       {\r\n                    extend: \'pdf\', text: \'PDF\', title: \'Kardex Horizon" +
"tal\', className: \'btn-sm\', footer:true,\r\n                    customize: function" +
" (doc) {\r\n                        doc.content[1].table.widths =\r\n               " +
"             Array(doc.content[1].table.body[0].length + 1).join(\'*\').split(\'\');" +
"\r\n                    }\r\n                },\r\n                {\r\n                " +
"    extend: \'print\', text: \'Imprimir\', className: \'btn-sm\',\r\n                }\r\n" +
"            ], \"footerCallback\": function (row, data, start, end, display) {\r\n  " +
"              var api = this.api(), data;\r\n                // converting to inte" +
"rger to find total\r\n                var intVal = function (i) {\r\n               " +
"     return typeof i === \'string\' ?\r\n                        i.replace(/[\\$,]/g," +
" \'\') * 1 :\r\n                        typeof i === \'number\' ?\r\n                   " +
"         i : 0;\r\n                };\r\n\r\n                for (var i = 3; i < 57; i" +
"++) {\r\n                    if (sinSigno.indexOf(i) >= 0) {\r\n                    " +
"    var monTotal = api\r\n                            .column(i)\r\n                " +
"            .data()\r\n                            .reduce(function (a, b) {\r\n    " +
"                            return intVal(a) + intVal(b);\r\n                     " +
"       }, 0);\r\n                        $(api.column(i).footer()).html(`<span> ${" +
"monTotal.toFixed(2)}</span >`);\r\n                    } else {\r\n                 " +
"       var monTotal = api\r\n                            .column(i)\r\n             " +
"               .data()\r\n                            .reduce(function (a, b) {\r\n " +
"                               return intVal(a) + intVal(b);\r\n                  " +
"          }, 0);\r\n                        $(api.column(i).footer()).html(`<span>" +
" ${formatmoney(monTotal)(2)}</span >`);\r\n                    }\r\n\r\n\r\n\r\n          " +
"      }\r\n\r\n            },\r\n            columns:\r\n                [\r\n            " +
"        { data: \'part_number\' },\r\n                    { data: \'part_description\'" +
" },\r\n                    { data: \'Saldo_Inicial\' },\r\n                    { data:" +
" \'CC_INVENTORY\' },\r\n                    { data: \'CC_INVENTORY_A\' },\r\n           " +
"         { data: \'ENTRADA_AJUSTE_NEGATIVO\' },\r\n                    { data: \'ENTR" +
"ADA_AJUSTE_NEGATIVO_A\' },\r\n                    { data: \'SALIDA_AJUSTE_NEGATIVO\' " +
"},\r\n                    { data: \'SALIDA_AJUSTE_NEGATIVO_A\' },\r\n                 " +
"   { data: \'GI_COST_CENTER\' },\r\n                    { data: \'GI_COST_CENTER_A\' }" +
",\r\n                    { data: \'SALIDA_POR_PV\' },\r\n                    { data: \'" +
"SALIDA_POR_PV_A\' },\r\n                    { data: \'GI_FROM_BLOCKED_REMISION\' },\r\n" +
"                    { data: \'GI_FROM_BLOCKED_REMISION_A\' },\r\n                   " +
" { data: \'GI_FROM_BLOCKED_RMA\' },\r\n                    { data: \'GI_FROM_BLOCKED_" +
"RMA_A\' },\r\n                    { data: \'GI_FROM_BLOCKED_SCRAP\' },\r\n             " +
"       { data: \'GI_FROM_BLOCKED_SCRAP_A\' },\r\n                    { data: \'REVERS" +
"A_VENTA_TOTAL\' },\r\n                    { data: \'REVERSA_VENTA_TOTAL_A\' },\r\n     " +
"               { data: \'REVERSE_GI_FROM_BLOCKED_RMA\' },\r\n                    { d" +
"ata: \'REVERSE_GI_FROM_BLOCKED_RMA_A\' },\r\n                    { data: \'SALIDA_AJU" +
"STE_COMBO_TOTAL\' },\r\n                    { data: \'SALIDA_AJUSTE_COMBO_TOTAL_A\' }" +
",\r\n                    { data: \'SALIDA_PACKS_TOTAL\' },\r\n                    { da" +
"ta: \'SALIDA_PACKS_TOTAL_A\' },\r\n                    { data: \'SALIDA_AJUSTE_RECETA" +
"S\' },\r\n                    { data: \'SALIDA_AJUSTE_RECETAS_A\' },\r\n               " +
"     { data: \'SALIDA_MATERIA_PRIMA\' },\r\n                    { data: \'SALIDA_MATE" +
"RIA_PRIMA_A\' },\r\n                    { data: \'SALIDA_PACKS_TOTAL\' },\r\n          " +
"          { data: \'SALIDA_PACKS_TOTAL_A\' },\r\n                    { data: \'TRANSF" +
"ER_GI\' },\r\n                    { data: \'TRANSFER_GI_A\' },\r\n                    {" +
" data: \'CANCEL_GR_PO\' },\r\n                    { data: \'CANCEL_GR_PO_A\' },\r\n     " +
"               { data: \'ENTRADA_AJUSTE_COMBO_TOTAL\' },\r\n                    { da" +
"ta: \'ENTRADA_AJUSTE_COMBO_TOTAL_A\' },\r\n                    { data: \'ENTRADA_PACK" +
"S_TOTAL\' },\r\n                    { data: \'ENTRADA_PACKS_TOTAL_A\' },\r\n           " +
"         { data: \'ENTRADA_AJUSTE_RECETA\' },\r\n                    { data: \'ENTRAD" +
"A_AJUSTE_RECETA_A\' },\r\n                    { data: \'ENTRADA_PACKS_TOTAL\' },\r\n   " +
"                 { data: \'ENTRADA_PACKS_TOTAL_A\' },\r\n                    { data:" +
" \'GR_FROM_PO\' },\r\n                    { data: \'GR_FROM_PO_A\' },\r\n               " +
"     { data: \'GR_WITHOUT_COST\' },\r\n                    { data: \'GR_WITHOUT_COST_" +
"A\' },\r\n                    { data: \'NOTA_DE_CREDITO\' },\r\n                    { d" +
"ata: \'NOTA_DE_CREDITO_A\' },\r\n                    { data: \'REVERSA_NOTA_DE_CREDIT" +
"O\' },\r\n                    { data: \'REVERSA_NOTA_DE_CREDITO_A\' },\r\n             " +
"       { data: \'REVERSE_TRANSFER\' },\r\n                    { data: \'REVERSE_TRANS" +
"FER_A\' },\r\n                    { data: \'TRANSFER_GR\' },\r\n                    { d" +
"ata: \'TRANSFER_GR_A\' },\r\n\r\n                    //{ data: \'CC_INVENTORY_A\' },\r\n  " +
"                  //{ data: \'ENTRADA_AJUSTE_NEGATIVO_A\' },\r\n                    " +
"//{ data: \'SALIDA_AJUSTE_NEGATIVO_A\' },\r\n                    //{ data: \'GI_COST_" +
"CENTER_A\' },\r\n                    //{ data: \'SALIDA_POR_PV_A\' },\r\n              " +
"      //{ data: \'GI_FROM_BLOCKED_REMISION_A\' },\r\n                    //{ data: \'" +
"GI_FROM_BLOCKED_RMA_A\' },\r\n                    //{ data: \'GI_FROM_BLOCKED_SCRAP_" +
"A\' },\r\n                    //{ data: \'REVERSA_VENTA_TOTAL_A\' },\r\n               " +
"     //{ data: \'REVERSE_GI_FROM_BLOCKED_RMA_A\' },\r\n                    //{ data:" +
" \'SALIDA_AJUSTE_COMBO_TOTAL_A\' },\r\n                    //{ data: \'SALIDA_PACKS_T" +
"OTAL_A\' },\r\n                    //{ data: \'SALIDA_AJUSTE_RECETAS_A\' },\r\n        " +
"            //{ data: \'SALIDA_MATERIA_PRIMA_A\' },\r\n                    //{ data:" +
" \'SALIDA_PACKS_TOTAL_A\' },\r\n                    //{ data: \'TRANSFER_GI_A\' },\r\n  " +
"                  //{ data: \'CANCEL_GR_PO_A\' },\r\n                    //{ data: \'" +
"ENTRADA_AJUSTE_COMBO_TOTAL_A\' },\r\n                    //{ data: \'ENTRADA_PACKS_T" +
"OTAL_A\' },\r\n                    //{ data: \'ENTRADA_AJUSTE_RECETA_A\' },\r\n        " +
"            //{ data: \'ENTRADA_PACKS_TOTAL_A\' },\r\n                    //{ data: " +
"\'GR_FROM_PO_A\' },\r\n                    //{ data: \'GR_WITHOUT_COST_A\' },\r\n       " +
"             //{ data: \'NOTA_DE_CREDITO_A\' },\r\n                    //{ data: \'RE" +
"VERSA_NOTA_DE_CREDITO_A\' },\r\n                    //{ data: \'REVERSE_TRANSFER_A\' " +
"},\r\n                    //{ data: \'TRANSFER_GR_A\' },\r\n                    //////" +
"/\r\n                    { data: \'Saldo_Final\' }\r\n                ],\r\n            " +
"columnDefs:\r\n                [\r\n\r\n                    {\r\n                       " +
" width: \"1%\",\r\n                        targets: sinSigno,\r\n                     " +
"   render: function (data, type, full, name) {\r\n                            if (" +
"data == null)\r\n                                return \"0\";\r\n                    " +
"        else\r\n                                return parseFloat(data).toFixed(2)" +
";\r\n                        }\r\n                    },\r\n                    {\r\n   " +
"                     width: \"1%\",\r\n                        targets: conSigno,\r\n " +
"                       render: function (data, type, full, name) {\r\n            " +
"                if (data == null)\r\n                                return \"$0\";\r" +
"\n                            else\r\n                                return format" +
"money(data,2);\r\n                        }\r\n                    }\r\n              " +
"  ]\r\n        });\r\n\r\n\r\n        $(\'#btnSearchMovements\').on(\'click\', function (r, " +
"e) {\r\n            toastr.remove();\r\n            toastr.options =\r\n              " +
"  {\r\n                    \"debug\": false,\r\n                    \"newestOnTop\": fal" +
"se,\r\n                    \"positionClass\": \"toast-top-center\",\r\n                 " +
"   \"closeButton\": true,\r\n                    \"debug\": false,\r\n                  " +
"  \"toastClass\": \"animated fadeInDown\",\r\n                };\r\n            if ($(\"#" +
"ValidDate1\").val() == \"\") {\r\n                toastr.error(\'Seleccione una fecha\'" +
");\r\n                $(\"#ValidDate1\").val(\"\");\r\n                $(\"#ValidDate1\")." +
"focus();\r\n            }\r\n            else if ($(\"#ValidDate2\").val() == \"\") {\r\n " +
"               toastr.error(\'Seleccione una fecha\');\r\n                $(\"#ValidD" +
"ate2\").val(\"\");\r\n                $(\"#ValidDate2\").focus();\r\n            }\r\n     " +
"       else if (moment($(\"#ValidDate2\").val()) < moment($(\"#ValidDate1\").val()))" +
" {\r\n                toastr.error(\'Fecha Final no puede ser menor a Inicial\');\r\n " +
"               $(\"#ValidDate2\").val(\"\");\r\n                $(\"#ValidDate2\").focus" +
"();\r\n            }\r\n            else {\r\n                StartLoading();\r\n       " +
"         $.ajax({\r\n                    url: \"/Items/ActionGetItemsKardexHor\",\r\n " +
"                   type: \"POST\",\r\n                    data: {\r\n                 " +
"       \"DateInitial\": $(\"#ValidDate1\").val(), \"DateFinish\": $(\"#ValidDate2\").val" +
"(), \"department\": $(\"#depto\").val(), \"part_number\": $(\"#ItemsDrop\").val()  },\r\n " +
"                   success: function (returndata) {\r\n                        End" +
"Loading();\r\n                        if (returndata.data === \'Termino tu sesión.\'" +
")\r\n                            SessionFalse(returndata.responseText);\r\n         " +
"               else if (returndata) {\r\n                            console.log(r" +
"eturndata);\r\n                            table.clear().draw();\r\n                " +
"            table.rows.add(returndata)\r\n                            table.column" +
"s.adjust().draw();\r\n                        }\r\n                        else {\r\n " +
"                           toastr.warning(returndata.responseText);\r\n           " +
"                 table.clear().draw();\r\n                        }\r\n             " +
"       },\r\n                    error: function () {\r\n                        End" +
"Loading();\r\n                        toastr.error(\'Error inesperado contactar a s" +
"istemas.\');\r\n                    }\r\n                });\r\n            }\r\n        " +
"});\r\n\r\n\r\n    </script>\r\n");

});

WriteLiteral("\r\n");

        }
    }
}
#pragma warning restore 1591
