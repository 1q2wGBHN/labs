#pragma warning disable 1591
//------------------------------------------------------------------------------
// <auto-generated>
//     Este código fue generado por una herramienta.
//     Versión de runtime:4.0.30319.42000
//
//     Los cambios en este archivo podrían causar un comportamiento incorrecto y se perderán si
//     se vuelve a generar el código.
// </auto-generated>
//------------------------------------------------------------------------------

namespace ASP
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using System.Net;
    using System.Text;
    using System.Web;
    using System.Web.Helpers;
    using System.Web.Mvc;
    using System.Web.Mvc.Ajax;
    using System.Web.Mvc.Html;
    using System.Web.Optimization;
    using System.Web.Routing;
    using System.Web.Security;
    using System.Web.UI;
    using System.Web.WebPages;
    using DevExpress.Utils;
    using DevExpress.Web;
    using DevExpress.Web.ASPxThemes;
    using DevExpress.Web.Mvc;
    using DevExpress.Web.Mvc.UI;
    using DevExpress.XtraReports;
    using DevExpress.XtraReports.UI;
    using DevExpress.XtraReports.Web;
    using DevExpress.XtraReports.Web.DocumentViewer;
    using FloridoERPTX;
    
    [System.CodeDom.Compiler.GeneratedCodeAttribute("RazorGenerator", "2.0.0.0")]
    [System.Web.WebPages.PageVirtualPathAttribute("~/Views/SendDeposits/SEDE002.cshtml")]
    public partial class _Views_SendDeposits_SEDE002_cshtml : System.Web.Mvc.WebViewPage<App.Entities.ViewModels.SendDeposits.EditDeposits>
    {
        public _Views_SendDeposits_SEDE002_cshtml()
        {
        }
        public override void Execute()
        {
            
            #line 3 "..\..\Views\SendDeposits\SEDE002.cshtml"
  
    ViewBag.Title = "Editar Envío de Retiros";
    Layout = "~/Views/Shared/_Layout.cshtml";

            
            #line default
            #line hidden
WriteLiteral("\r\n\r\n");

WriteLiteral("\r\n<div");

WriteLiteral(" class=\"normalheader transition animated fadeIn\"");

WriteLiteral(">\r\n    <div");

WriteLiteral(" class=\"hpanel\"");

WriteLiteral(">\r\n        <div");

WriteLiteral(" class=\"panel-body\"");

WriteLiteral(">\r\n            <a");

WriteLiteral(" class=\"small-header-action\"");

WriteLiteral(" href=\"\"");

WriteLiteral(">\r\n                <div");

WriteLiteral(" class=\"clip-header\"");

WriteLiteral(">\r\n                    <i");

WriteLiteral(" id=\"fa\"");

WriteLiteral(" class=\"fa fa-arrow-up\"");

WriteLiteral("></i>\r\n                </div>\r\n            </a>\r\n            <div");

WriteLiteral(" id=\"hbreadcrumb\"");

WriteLiteral(" class=\"pull-right m-t-lg\"");

WriteLiteral(">\r\n                <ol");

WriteLiteral(" class=\"hbreadcrumb breadcrumb\"");

WriteLiteral(">\r\n                    <li><a");

WriteAttribute("href", Tuple.Create(" href=\"", 634), Tuple.Create("\"", 671)
            
            #line 19 "..\..\Views\SendDeposits\SEDE002.cshtml"
, Tuple.Create(Tuple.Create("", 641), Tuple.Create<System.Object, System.Int32>(Url.Action("SALE000","Sales")
            
            #line default
            #line hidden
, 641), false)
);

WriteLiteral(">Cajas</a></li>\r\n                    <li><a");

WriteAttribute("href", Tuple.Create(" href=\"", 715), Tuple.Create("\"", 752)
            
            #line 20 "..\..\Views\SendDeposits\SEDE002.cshtml"
, Tuple.Create(Tuple.Create("", 722), Tuple.Create<System.Object, System.Int32>(Url.Action("SALE004","Sales")
            
            #line default
            #line hidden
, 722), false)
);

WriteLiteral(">Movimientos</a></li>\r\n                    <li");

WriteLiteral(" class=\"active\"");

WriteLiteral(">\r\n                        <span>Editar Envío</span>\r\n                    </li>\r\n" +
"                </ol>\r\n            </div>\r\n            <h2");

WriteLiteral(" class=\"font-light m-b-xs\"");

WriteLiteral(">\r\n                Editar Envío de Retiros\r\n            </h2>\r\n            <small" +
">Módulo para editar envío de retiros</small><br />\r\n        </div>\r\n    </div>\r\n" +
"</div>\r\n\r\n<div");

WriteLiteral(" class=\"content animated-panel\"");

WriteLiteral(">\r\n    <div");

WriteLiteral(" class=\"row\"");

WriteLiteral(">\r\n        <div");

WriteLiteral(" class=\"col-lg-12\"");

WriteLiteral(">\r\n            <div");

WriteLiteral(" class=\"hpanel hblue\"");

WriteLiteral(">\r\n                <div");

WriteLiteral(" class=\"panel-heading hbuilt\"");

WriteLiteral(">\r\n                    Envío de Retiros\r\n                </div>\r\n                " +
"<div");

WriteLiteral(" class=\"panel-body\"");

WriteLiteral(">\r\n                    <div");

WriteLiteral(" class=\"form-group col-md-4\"");

WriteLiteral(">\r\n");

WriteLiteral("                        ");

            
            #line 43 "..\..\Views\SendDeposits\SEDE002.cshtml"
                   Write(Html.LabelFor(model => model.Date, htmlAttributes: new { @class = "control-label" }));

            
            #line default
            #line hidden
WriteLiteral("\r\n                        <div");

WriteLiteral(" class=\"col-md-10 input-group m-b\"");

WriteLiteral(">\r\n                            <span");

WriteLiteral(" class=\"input-group-addon\"");

WriteLiteral("><i");

WriteLiteral(" class=\"fa fa-calendar\"");

WriteLiteral("></i></span>\r\n");

WriteLiteral("                            ");

            
            #line 46 "..\..\Views\SendDeposits\SEDE002.cshtml"
                       Write(Html.EditorFor(model => model.Date, new { htmlAttributes = new { @class = "form-control datepicker" } }));

            
            #line default
            #line hidden
WriteLiteral("\r\n                        </div>\r\n");

WriteLiteral("                        ");

            
            #line 48 "..\..\Views\SendDeposits\SEDE002.cshtml"
                   Write(Html.ValidationMessageFor(model => model.Date, "", new { @class = "text-danger" }));

            
            #line default
            #line hidden
WriteLiteral("\r\n                    </div>\r\n                    <div");

WriteLiteral(" class=\"form-group col-md-4\"");

WriteLiteral(">\r\n                        <label");

WriteLiteral(" class=\"control-label\"");

WriteLiteral(">&nbsp;</label>\r\n                        <div");

WriteLiteral(" class=\"\"");

WriteLiteral(">\r\n                            <button");

WriteLiteral(" type=\"button\"");

WriteLiteral(" class=\"btn btn-success\"");

WriteLiteral(" id=\"SearchDeposits\"");

WriteLiteral(">Buscar</button>\r\n                        </div>\r\n                    </div>\r\n   " +
"             </div>\r\n                <div");

WriteLiteral(" class=\"content animated-panel\"");

WriteLiteral(" id=\"TableSection\"");

WriteLiteral(">\r\n                    <div");

WriteLiteral(" class=\"hpanel\"");

WriteLiteral(">\r\n                        <div");

WriteLiteral(" class=\"panel-body\"");

WriteLiteral(">\r\n                            <div");

WriteLiteral(" class=\"table-responsive m-t\"");

WriteLiteral(">\r\n                                <table");

WriteLiteral(" id=\"sendDeposits-table\"");

WriteLiteral(" class=\"table table-striped table-bordered table-hover\"");

WriteLiteral(" style=\"width:100%\"");

WriteLiteral(@">
                                    <thead>
                                        <tr>
                                            <th></th>
                                            <th>Fecha</th>
                                            <th>Usuario</th>
                                            <th>Tipo</th>
                                            <th>Origen</th>
                                            <th>Moneda</th>
                                            <th>Monto</th>
                                            <th>Comentarios</th>
                                            <th>Fecha y Hora de Creación</th>
                                            <th></th>
                                        </tr>
                                    </thead>                                    
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

");

WriteLiteral("\r\n<div");

WriteLiteral(" id=\"EditDeposit\"");

WriteLiteral(">\r\n</div>\r\n\r\n");

DefineSection("Scripts", () => {

WriteLiteral("\r\n");

            
            #line 91 "..\..\Views\SendDeposits\SEDE002.cshtml"
Write(Scripts.Render("~/bundles/datepicker/js"));

            
            #line default
            #line hidden
WriteLiteral("\r\n");

            
            #line 92 "..\..\Views\SendDeposits\SEDE002.cshtml"
Write(Scripts.Render("~/bundles/datatables/js"));

            
            #line default
            #line hidden
WriteLiteral("\r\n");

            
            #line 93 "..\..\Views\SendDeposits\SEDE002.cshtml"
Write(Scripts.Render("~/bundles/datatablesBootstrap/js"));

            
            #line default
            #line hidden
WriteLiteral("\r\n");

            
            #line 94 "..\..\Views\SendDeposits\SEDE002.cshtml"
Write(Scripts.Render("~/bundles/datatablesPlugins/js"));

            
            #line default
            #line hidden
WriteLiteral("\r\n<script");

WriteAttribute("src", Tuple.Create(" src=\"", 4142), Tuple.Create("\"", 4186)
, Tuple.Create(Tuple.Create("", 4148), Tuple.Create<System.Object, System.Int32>(Href("~/Scripts/SendDeposits/SendDeposits.js")
, 4148), false)
);

WriteLiteral("></script>\r\n<script");

WriteLiteral(" type=\"text/javascript\"");

WriteLiteral(">\r\n    var urlModal = \'");

            
            #line 97 "..\..\Views\SendDeposits\SEDE002.cshtml"
               Write(Url.Action("ActionGetDepositDetails", "SendDeposits"));

            
            #line default
            #line hidden
WriteLiteral("\';\r\n    var urlDeleteDeposit = \'");

            
            #line 98 "..\..\Views\SendDeposits\SEDE002.cshtml"
                       Write(Url.Action("ActionDeleteDeposit", "SendDeposits"));

            
            #line default
            #line hidden
WriteLiteral("\';\r\n    var urlSaveEdit = \'");

            
            #line 99 "..\..\Views\SendDeposits\SEDE002.cshtml"
                  Write(Url.Action("ActionSaveEditDeposit", "SendDeposits"));

            
            #line default
            #line hidden
WriteLiteral("\';\r\n    var urlDeposits = \'");

            
            #line 100 "..\..\Views\SendDeposits\SEDE002.cshtml"
                  Write(Url.Action("ActionGetDeposits", "SendDeposits"));

            
            #line default
            #line hidden
WriteLiteral("\';\r\n\r\n    $(function () {\r\n\r\n        var Deposits = ");

            
            #line 104 "..\..\Views\SendDeposits\SEDE002.cshtml"
                  Write(Html.Raw(Json.Encode(Model.DepositsList)));

            
            #line default
            #line hidden
WriteLiteral(@";
        table.clear();
        table.rows.add(jQuery(Deposits));
        table.columns.adjust().draw();

        $(""#Date"").datepicker({
            autoclose: true,
            format: 'dd/mm/yyyy',
            language: 'es',
            todayHighlight: true,
            endDate: new Date()
        });

        $(""#Date"").attr('readonly', 'readonly').css('background-color', '#fff');
    });    

    $('#sendDeposits-table').on('error.dt', function (e, settings, techNote, message) {
        console.log('An error has been reported by DataTables: ', message);
    });
    $.fn.dataTable.ext.errMode = 'none';

</script>
");

});

        }
    }
}
#pragma warning restore 1591
