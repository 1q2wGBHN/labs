#pragma warning disable 1591
//------------------------------------------------------------------------------
// <auto-generated>
//     Este código fue generado por una herramienta.
//     Versión de runtime:4.0.30319.42000
//
//     Los cambios en este archivo podrían causar un comportamiento incorrecto y se perderán si
//     se vuelve a generar el código.
// </auto-generated>
//------------------------------------------------------------------------------

namespace ASP
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using System.Net;
    using System.Text;
    using System.Web;
    using System.Web.Helpers;
    using System.Web.Mvc;
    using System.Web.Mvc.Ajax;
    using System.Web.Mvc.Html;
    using System.Web.Optimization;
    using System.Web.Routing;
    using System.Web.Security;
    using System.Web.UI;
    using System.Web.WebPages;
    using DevExpress.Utils;
    using DevExpress.Web;
    using DevExpress.Web.ASPxThemes;
    using DevExpress.Web.Mvc;
    using DevExpress.Web.Mvc.UI;
    using DevExpress.XtraReports;
    using DevExpress.XtraReports.UI;
    using DevExpress.XtraReports.Web;
    using DevExpress.XtraReports.Web.DocumentViewer;
    using FloridoERPTX;
    
    [System.CodeDom.Compiler.GeneratedCodeAttribute("RazorGenerator", "2.0.0.0")]
    [System.Web.WebPages.PageVirtualPathAttribute("~/Views/SendDeposits/SEDE001.cshtml")]
    public partial class _Views_SendDeposits_SEDE001_cshtml : System.Web.Mvc.WebViewPage<App.Entities.ViewModels.SendDeposits.SendDepositsModel>
    {
        public _Views_SendDeposits_SEDE001_cshtml()
        {
        }
        public override void Execute()
        {
            
            #line 3 "..\..\Views\SendDeposits\SEDE001.cshtml"
  
    ViewBag.Title = "Envío de Retiros";
    Layout = "~/Views/Shared/_Layout.cshtml";

            
            #line default
            #line hidden
WriteLiteral("\r\n\r\n");

WriteLiteral("\r\n<div");

WriteLiteral(" class=\"normalheader transition animated fadeIn\"");

WriteLiteral(">\r\n    <div");

WriteLiteral(" class=\"hpanel\"");

WriteLiteral(">\r\n        <div");

WriteLiteral(" class=\"panel-body\"");

WriteLiteral(">\r\n            <a");

WriteLiteral(" class=\"small-header-action\"");

WriteLiteral(" href=\"\"");

WriteLiteral(">\r\n                <div");

WriteLiteral(" class=\"clip-header\"");

WriteLiteral(">\r\n                    <i");

WriteLiteral(" id=\"fa\"");

WriteLiteral(" class=\"fa fa-arrow-up\"");

WriteLiteral("></i>\r\n                </div>\r\n            </a>\r\n            <div");

WriteLiteral(" id=\"hbreadcrumb\"");

WriteLiteral(" class=\"pull-right m-t-lg\"");

WriteLiteral(">\r\n                <ol");

WriteLiteral(" class=\"hbreadcrumb breadcrumb\"");

WriteLiteral(">\r\n                    <li><a");

WriteAttribute("href", Tuple.Create(" href=\"", 632), Tuple.Create("\"", 669)
            
            #line 19 "..\..\Views\SendDeposits\SEDE001.cshtml"
, Tuple.Create(Tuple.Create("", 639), Tuple.Create<System.Object, System.Int32>(Url.Action("SALE000","Sales")
            
            #line default
            #line hidden
, 639), false)
);

WriteLiteral(">Cajas</a></li>\r\n                    <li><a");

WriteAttribute("href", Tuple.Create(" href=\"", 713), Tuple.Create("\"", 750)
            
            #line 20 "..\..\Views\SendDeposits\SEDE001.cshtml"
, Tuple.Create(Tuple.Create("", 720), Tuple.Create<System.Object, System.Int32>(Url.Action("SALE004","Sales")
            
            #line default
            #line hidden
, 720), false)
);

WriteLiteral(">Movimientos</a></li>\r\n                    <li");

WriteLiteral(" class=\"active\"");

WriteLiteral(">\r\n                        <span>Envío</span>\r\n                    </li>\r\n       " +
"         </ol>\r\n            </div>\r\n            <h2");

WriteLiteral(" class=\"font-light m-b-xs\"");

WriteLiteral(">\r\n                Envío de Retiros\r\n            </h2>\r\n            <small>Módulo" +
" para envío de retiros</small><br />\r\n        </div>\r\n    </div>\r\n</div>\r\n\r\n<div" +
"");

WriteLiteral(" class=\"content animated-panel\"");

WriteLiteral(">\r\n    <div");

WriteLiteral(" class=\"hpanel hblue\"");

WriteLiteral(">\r\n        <div");

WriteLiteral(" class=\"panel-body\"");

WriteLiteral(">\r\n            <form");

WriteLiteral(" id=\"formDeposits\"");

WriteAttribute("action", Tuple.Create(" action=\"", 1266), Tuple.Create("\"", 1312)
            
            #line 37 "..\..\Views\SendDeposits\SEDE001.cshtml"
, Tuple.Create(Tuple.Create("", 1275), Tuple.Create<System.Object, System.Int32>(Url.Action("SEDE001","SendDeposits")
            
            #line default
            #line hidden
, 1275), false)
);

WriteLiteral(" method=\"post\"");

WriteLiteral(">\r\n                <div");

WriteLiteral(" class=\"row\"");

WriteLiteral(">\r\n                    <div");

WriteLiteral(" class=\"form-group col-md-6\"");

WriteLiteral(">\r\n");

WriteLiteral("                        ");

            
            #line 40 "..\..\Views\SendDeposits\SEDE001.cshtml"
                   Write(Html.LabelFor(model => model.Type, htmlAttributes: new { @class = "control-label" }));

            
            #line default
            #line hidden
WriteLiteral("\r\n                        <div");

WriteLiteral(" class=\"col-md-10 input-group m-b\"");

WriteLiteral(">\r\n                            <span");

WriteLiteral(" class=\"input-group-addon\"");

WriteLiteral("><i");

WriteLiteral(" class=\"fa fa-file-code-o\"");

WriteLiteral("></i></span>\r\n");

WriteLiteral("                            ");

            
            #line 43 "..\..\Views\SendDeposits\SEDE001.cshtml"
                       Write(Html.DropDownListFor(model => model.Type, null, "--Seleccionar Tipo--", htmlAttributes: new { @class = "form-control" }));

            
            #line default
            #line hidden
WriteLiteral("\r\n                        </div>\r\n");

WriteLiteral("                        ");

            
            #line 45 "..\..\Views\SendDeposits\SEDE001.cshtml"
                   Write(Html.ValidationMessageFor(model => model.Type, "", new { @class = "text-danger" }));

            
            #line default
            #line hidden
WriteLiteral("\r\n                    </div>\r\n                    <div");

WriteLiteral(" class=\"form-group col-md-6\"");

WriteLiteral(">\r\n");

WriteLiteral("                        ");

            
            #line 48 "..\..\Views\SendDeposits\SEDE001.cshtml"
                   Write(Html.LabelFor(model => model.Origin, htmlAttributes: new { @class = "control-label" }));

            
            #line default
            #line hidden
WriteLiteral("\r\n                        <div");

WriteLiteral(" class=\"col-md-10 input-group m-b\"");

WriteLiteral(">\r\n                            <span");

WriteLiteral(" class=\"input-group-addon\"");

WriteLiteral("><i");

WriteLiteral(" class=\"fa fa-file-code-o\"");

WriteLiteral("></i></span>\r\n");

WriteLiteral("                            ");

            
            #line 51 "..\..\Views\SendDeposits\SEDE001.cshtml"
                       Write(Html.DropDownListFor(model => model.Origin, null, "--Seleccionar Origen--", htmlAttributes: new { @class = "form-control" }));

            
            #line default
            #line hidden
WriteLiteral("\r\n                        </div>\r\n");

WriteLiteral("                        ");

            
            #line 53 "..\..\Views\SendDeposits\SEDE001.cshtml"
                   Write(Html.ValidationMessageFor(model => model.Origin, "", new { @class = "text-danger" }));

            
            #line default
            #line hidden
WriteLiteral("\r\n                    </div>\r\n                </div>\r\n                <div");

WriteLiteral(" class=\"row\"");

WriteLiteral(">\r\n                    <div");

WriteLiteral(" class=\"form-group col-md-6\"");

WriteLiteral(">\r\n");

WriteLiteral("                        ");

            
            #line 58 "..\..\Views\SendDeposits\SEDE001.cshtml"
                   Write(Html.LabelFor(model => model.Currency, htmlAttributes: new { @class = "control-label" }));

            
            #line default
            #line hidden
WriteLiteral("\r\n                        <div");

WriteLiteral(" class=\"col-md-10 input-group m-b\"");

WriteLiteral(">\r\n                            <span");

WriteLiteral(" class=\"input-group-addon\"");

WriteLiteral("><i");

WriteLiteral(" class=\"fa fa-file-code-o\"");

WriteLiteral("></i></span>\r\n");

WriteLiteral("                            ");

            
            #line 61 "..\..\Views\SendDeposits\SEDE001.cshtml"
                       Write(Html.DropDownListFor(model => model.Currency, null, "--Seleccionar Moneda--", htmlAttributes: new { @class = "form-control" }));

            
            #line default
            #line hidden
WriteLiteral("\r\n                        </div>\r\n");

WriteLiteral("                        ");

            
            #line 63 "..\..\Views\SendDeposits\SEDE001.cshtml"
                   Write(Html.ValidationMessageFor(model => model.Currency, "", new { @class = "text-danger" }));

            
            #line default
            #line hidden
WriteLiteral("\r\n                    </div>\r\n                    <div");

WriteLiteral(" class=\"form-group col-md-6\"");

WriteLiteral(">\r\n");

WriteLiteral("                        ");

            
            #line 66 "..\..\Views\SendDeposits\SEDE001.cshtml"
                   Write(Html.LabelFor(model => model.Amount, htmlAttributes: new { @class = "control-label" }));

            
            #line default
            #line hidden
WriteLiteral("\r\n                        <div");

WriteLiteral(" class=\"col-md-10 input-group m-b\"");

WriteLiteral(">\r\n                            <span");

WriteLiteral(" class=\"input-group-addon\"");

WriteLiteral("><i");

WriteLiteral(" class=\"fa fa-money\"");

WriteLiteral("></i></span>\r\n");

WriteLiteral("                            ");

            
            #line 69 "..\..\Views\SendDeposits\SEDE001.cshtml"
                       Write(Html.EditorFor(model => model.Amount, new { htmlAttributes = new { @class = "form-control" } }));

            
            #line default
            #line hidden
WriteLiteral("\r\n                        </div>\r\n");

WriteLiteral("                        ");

            
            #line 71 "..\..\Views\SendDeposits\SEDE001.cshtml"
                   Write(Html.ValidationMessageFor(model => model.Amount, "", new { @class = "text-danger" }));

            
            #line default
            #line hidden
WriteLiteral("\r\n                    </div>\r\n                </div>\r\n                <div");

WriteLiteral(" class=\"row\"");

WriteLiteral(">\r\n                    <div");

WriteLiteral(" class=\"form-group col-md-6\"");

WriteLiteral(">\r\n");

WriteLiteral("                        ");

            
            #line 76 "..\..\Views\SendDeposits\SEDE001.cshtml"
                   Write(Html.LabelFor(model => model.Comments, htmlAttributes: new { @class = "control-label" }));

            
            #line default
            #line hidden
WriteLiteral("\r\n                        <div");

WriteLiteral(" class=\"col-md-10 input-group m-b\"");

WriteLiteral(">\r\n                            <span");

WriteLiteral(" class=\"input-group-addon\"");

WriteLiteral("><i");

WriteLiteral(" class=\"pe-7s-map-marker\"");

WriteLiteral("></i></span>\r\n");

WriteLiteral("                            ");

            
            #line 79 "..\..\Views\SendDeposits\SEDE001.cshtml"
                       Write(Html.EditorFor(model => model.Comments, new { htmlAttributes = new { @class = "form-control" } }));

            
            #line default
            #line hidden
WriteLiteral("\r\n                        </div>\r\n");

WriteLiteral("                        ");

            
            #line 81 "..\..\Views\SendDeposits\SEDE001.cshtml"
                   Write(Html.ValidationMessageFor(model => model.Comments, "", new { @class = "text-danger" }));

            
            #line default
            #line hidden
WriteLiteral("\r\n                    </div>\r\n");

            
            #line 83 "..\..\Views\SendDeposits\SEDE001.cshtml"
                    
            
            #line default
            #line hidden
            
            #line 83 "..\..\Views\SendDeposits\SEDE001.cshtml"
                     if (Model.CanEdit)
                    {

            
            #line default
            #line hidden
WriteLiteral("                        <div");

WriteLiteral(" class=\"form-group col-md-6\"");

WriteLiteral(">\r\n");

WriteLiteral("                            ");

            
            #line 86 "..\..\Views\SendDeposits\SEDE001.cshtml"
                       Write(Html.LabelFor(model => model.Date, htmlAttributes: new { @class = "control-label" }));

            
            #line default
            #line hidden
WriteLiteral("\r\n                            <div");

WriteLiteral(" class=\"col-md-10 input-group m-b\"");

WriteLiteral(">\r\n                                <span");

WriteLiteral(" class=\"input-group-addon\"");

WriteLiteral("><i");

WriteLiteral(" class=\"pe-7s-map-marker\"");

WriteLiteral("></i></span>\r\n");

WriteLiteral("                                ");

            
            #line 89 "..\..\Views\SendDeposits\SEDE001.cshtml"
                           Write(Html.EditorFor(model => model.Date, new { htmlAttributes = new { @class = "form-control datepicker" } }));

            
            #line default
            #line hidden
WriteLiteral("\r\n                            </div>\r\n");

WriteLiteral("                            ");

            
            #line 91 "..\..\Views\SendDeposits\SEDE001.cshtml"
                       Write(Html.ValidationMessageFor(model => model.Date, "", new { @class = "text-danger" }));

            
            #line default
            #line hidden
WriteLiteral("\r\n                        </div>\r\n");

            
            #line 93 "..\..\Views\SendDeposits\SEDE001.cshtml"
                    }

            
            #line default
            #line hidden
WriteLiteral("                </div>\r\n                <div");

WriteLiteral(" class=\"row\"");

WriteLiteral(">\r\n                    <div");

WriteLiteral(" class=\"form-group col-md-12\"");

WriteLiteral(">\r\n                        <div");

WriteLiteral(" class=\"text-right\"");

WriteLiteral(">\r\n                            <label");

WriteLiteral(" class=\"control-label\"");

WriteLiteral(">&nbsp;</label>\r\n                            <div");

WriteLiteral(" class=\"\"");

WriteLiteral(">\r\n                                <button");

WriteLiteral(" type=\"button\"");

WriteLiteral(" class=\"btn btn-success\"");

WriteLiteral(" id=\"Process\"");

WriteLiteral(">Procesar</button>\r\n                            </div>\r\n                        <" +
"/div>\r\n                    </div>\r\n                </div>\r\n            </form>\r\n" +
"        </div>\r\n    </div>\r\n</div>\r\n\r\n");

DefineSection("scripts", () => {

WriteLiteral("\r\n");

            
            #line 111 "..\..\Views\SendDeposits\SEDE001.cshtml"
Write(Scripts.Render("~/bundles/datepicker/js"));

            
            #line default
            #line hidden
WriteLiteral("\r\n    <script");

WriteAttribute("src", Tuple.Create(" src=\"", 6127), Tuple.Create("\"", 6161)
, Tuple.Create(Tuple.Create("", 6133), Tuple.Create<System.Object, System.Int32>(Href("~/Scripts/jquery.validate.js")
, 6133), false)
);

WriteLiteral("></script>\r\n    <script");

WriteLiteral(" type=\"text/javascript\"");

WriteLiteral(">\r\n        $(document).ready(function () {\r\n\r\n            $(\".datepicker\").datepi" +
"cker({\r\n                autoclose: true,\r\n                todayHighlight: true,\r" +
"\n                format: \'dd/mm/yyyy\',\r\n                language: \'es\',\r\n       " +
"         endDate: new Date()\r\n            });\r\n\r\n            $(\".datepicker\").at" +
"tr(\'readonly\', \'readonly\').css(\'background-color\', \'#fff\');\r\n\r\n            $(\"#f" +
"ormDeposits\").validate({\r\n                errorClass: \'help-block animation-slid" +
"eDown\', // You can change the animation class for a different entrance animation" +
" - check animations page\r\n                errorElement: \"span\",\r\n               " +
" errorPlacement: function (error, element) {\r\n                    $(element)\r\n  " +
"                          .closest(\"form\")\r\n                            .find(\"s" +
"pan[data-valmsg-for=\'\" + element.attr(\"id\") + \"\']\")\r\n                           " +
" .append(error);\r\n                },\r\n                highlight: function (e) {\r" +
"\n                    $(\"#Process\").attr(\"disabled\", \"disabled\");\r\n              " +
"      $(e).closest(\'.form-group\').removeClass(\'has-success has-error\').addClass(" +
"\'has-error\');\r\n                    $(e).closest(\'.help-block\').remove();\r\n      " +
"          },\r\n                success: function (e) {\r\n                    $(\"#P" +
"rocess\").removeAttr(\"disabled\");\r\n                    e.closest(\'.form-group\').r" +
"emoveClass(\'has-success has-error\');\r\n                    e.closest(\'.help-block" +
"\').remove();\r\n                },\r\n                ignoreTitle: true,\r\n          " +
"      rules: {\r\n                    Type: {\r\n                        required: t" +
"rue\r\n                    },\r\n\r\n                    Origin: {\r\n                  " +
"      required: true\r\n                    },\r\n\r\n                    Currency: {\r" +
"\n                        required: true\r\n                    },\r\n\r\n             " +
"       Amount: {\r\n                        required: true,\r\n                     " +
"   number: true,\r\n                        greaterThanZero: true\r\n               " +
"     }\r\n                },\r\n                submitHandler: function (form) {\r\n  " +
"                  form.submit();\r\n                }\r\n            });\r\n        })" +
";\r\n\r\n        jQuery.validator.addMethod(\"greaterThanZero\", function (value, elem" +
"ent) {\r\n            return this.optional(element) || (parseFloat(value) > 0);\r\n " +
"       }, \"Monto debe ser mayor a 0\");\r\n\r\n        $(\"#Process\").click(function (" +
") {\r\n            var amount = $(\"#formDeposits\").validate().element(\"#Amount\");\r" +
"\n            var origin = $(\"#formDeposits\").validate().element(\"#Origin\");\r\n   " +
"         var type = $(\"#formDeposits\").validate().element(\"#Type\");\r\n           " +
" var currency = $(\"#formDeposits\").validate().element(\"#Currency\");\r\n           " +
" var validForm = amount && origin && type && currency;\r\n\r\n            if (validF" +
"orm) {\r\n                StartLoading();\r\n                $.post({\r\n             " +
"       url: \'");

            
            #line 185 "..\..\Views\SendDeposits\SEDE001.cshtml"
                     Write(Url.Action("SEDE001", "SendDeposits"));

            
            #line default
            #line hidden
WriteLiteral(@"',
                    dataType: 'json',
                    Type: 'POST',
                    data: $(""#formDeposits"").serialize(),
                    success: function (data) {
                        EndLoading();
                        if (data.success) {
                            swal({
                                title: 'Retiro',
                                text: ""Envío registrado exitosamente."",
                                type: ""success""
                            }, function () {
                                window.location.reload(true);
                            });
                        }
                        else {
                            swal({
                                title: 'Error',
                                text: ""Ocurrio un error, contacte a sistemas para solucionarlo."",
                                type: ""error""
                            });
                        }
                    }
                });
            }
        });

    </script>
");

});

        }
    }
}
#pragma warning restore 1591
