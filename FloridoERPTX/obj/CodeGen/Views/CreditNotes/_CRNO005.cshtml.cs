#pragma warning disable 1591
//------------------------------------------------------------------------------
// <auto-generated>
//     Este código fue generado por una herramienta.
//     Versión de runtime:4.0.30319.42000
//
//     Los cambios en este archivo podrían causar un comportamiento incorrecto y se perderán si
//     se vuelve a generar el código.
// </auto-generated>
//------------------------------------------------------------------------------

namespace ASP
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using System.Net;
    using System.Text;
    using System.Web;
    using System.Web.Helpers;
    using System.Web.Mvc;
    using System.Web.Mvc.Ajax;
    using System.Web.Mvc.Html;
    using System.Web.Optimization;
    using System.Web.Routing;
    using System.Web.Security;
    using System.Web.UI;
    using System.Web.WebPages;
    using DevExpress.Utils;
    using DevExpress.Web;
    using DevExpress.Web.ASPxThemes;
    using DevExpress.Web.Mvc;
    using DevExpress.Web.Mvc.UI;
    using DevExpress.XtraReports;
    using DevExpress.XtraReports.UI;
    using DevExpress.XtraReports.Web;
    using DevExpress.XtraReports.Web.DocumentViewer;
    using FloridoERPTX;
    
    [System.CodeDom.Compiler.GeneratedCodeAttribute("RazorGenerator", "2.0.0.0")]
    [System.Web.WebPages.PageVirtualPathAttribute("~/Views/CreditNotes/_CRNO005.cshtml")]
    public partial class _Views_CreditNotes__CRNO005_cshtml : System.Web.Mvc.WebViewPage<App.Entities.ViewModels.CreditNotes.CreditNoteDetails>
    {
        public _Views_CreditNotes__CRNO005_cshtml()
        {
        }
        public override void Execute()
        {
WriteLiteral("<div");

WriteLiteral(" class=\"form-horizontal\"");

WriteLiteral(">\r\n    <div");

WriteLiteral(" class=\"row\"");

WriteLiteral(">\r\n        <h4");

WriteLiteral(" class=\"text-center\"");

WriteLiteral(">DISTRIBUIDORA EL FLORIDO S.A. DE C.V.</h4>\r\n");

            
            #line 6 "..\..\Views\CreditNotes\_CRNO005.cshtml"
        
            
            #line default
            #line hidden
            
            #line 6 "..\..\Views\CreditNotes\_CRNO005.cshtml"
         if (Model.IsBonification)
        {
            
            #line default
            #line hidden
WriteLiteral("<h5");

WriteLiteral(" class=\"text-center\"");

WriteLiteral(">Detalle de Bonificación</h5>");

            
            #line 7 "..\..\Views\CreditNotes\_CRNO005.cshtml"
                                                             }
        else
        {
            
            #line default
            #line hidden
WriteLiteral(" <h5");

WriteLiteral(" class=\"text-center\"");

WriteLiteral(">Detalle de Nota de Crédito</h5>");

            
            #line 9 "..\..\Views\CreditNotes\_CRNO005.cshtml"
                                                                 }

            
            #line default
            #line hidden
WriteLiteral("    </div>\r\n    <hr");

WriteLiteral(" style=\"border-top: 1px solid #3498db !important;\"");

WriteLiteral(" />\r\n");

WriteLiteral("    ");

            
            #line 12 "..\..\Views\CreditNotes\_CRNO005.cshtml"
Write(Html.HiddenFor(model => Model.Uuid));

            
            #line default
            #line hidden
WriteLiteral("\r\n");

WriteLiteral("    ");

            
            #line 13 "..\..\Views\CreditNotes\_CRNO005.cshtml"
Write(Html.HiddenFor(model => Model.Serie));

            
            #line default
            #line hidden
WriteLiteral("\r\n");

WriteLiteral("    ");

            
            #line 14 "..\..\Views\CreditNotes\_CRNO005.cshtml"
Write(Html.HiddenFor(model => Model.Number));

            
            #line default
            #line hidden
WriteLiteral("\r\n");

WriteLiteral("    ");

            
            #line 15 "..\..\Views\CreditNotes\_CRNO005.cshtml"
Write(Html.HiddenFor(model => Model.DocumentType));

            
            #line default
            #line hidden
WriteLiteral("\r\n    <div");

WriteLiteral(" class=\"col-md-12 form-group\"");

WriteLiteral(">\r\n        <div");

WriteLiteral(" class=\"col-md-12\"");

WriteLiteral(">\r\n");

            
            #line 18 "..\..\Views\CreditNotes\_CRNO005.cshtml"
            
            
            #line default
            #line hidden
            
            #line 18 "..\..\Views\CreditNotes\_CRNO005.cshtml"
             if (Model.IsBonification)
            {
            
            #line default
            #line hidden
WriteLiteral("<label");

WriteLiteral(" class=\"control-label\"");

WriteLiteral(">Bonificación: </label> ");

            
            #line 19 "..\..\Views\CreditNotes\_CRNO005.cshtml"
                                                                 }
            else
            {
            
            #line default
            #line hidden
            
            #line 21 "..\..\Views\CreditNotes\_CRNO005.cshtml"
        Write(Html.LabelFor(model => model.CreditNote, htmlAttributes: new { @class = "control-label" }));

            
            #line default
            #line hidden
            
            #line 21 "..\..\Views\CreditNotes\_CRNO005.cshtml"
                                                                                                        }

            
            #line default
            #line hidden
WriteLiteral("            ");

            
            #line 22 "..\..\Views\CreditNotes\_CRNO005.cshtml"
       Write(Html.DisplayFor(model => model.CreditNote, new { htmlAttributes = new { @class = "form-control" } }));

            
            #line default
            #line hidden
WriteLiteral("\r\n        </div>\r\n        <div");

WriteLiteral(" class=\"col-md-12\"");

WriteLiteral(">\r\n");

WriteLiteral("            ");

            
            #line 25 "..\..\Views\CreditNotes\_CRNO005.cshtml"
       Write(Html.LabelFor(model => model.IssueDate, htmlAttributes: new { @class = "control-label" }));

            
            #line default
            #line hidden
WriteLiteral("\r\n");

WriteLiteral("            ");

            
            #line 26 "..\..\Views\CreditNotes\_CRNO005.cshtml"
       Write(Html.DisplayFor(model => model.IssueDate, new { htmlAttributes = new { @class = "form-control" } }));

            
            #line default
            #line hidden
WriteLiteral("\r\n        </div>        \r\n    </div>\r\n    <div");

WriteLiteral(" class=\"col-md-12 form-group\"");

WriteLiteral(">\r\n        <div");

WriteLiteral(" class=\"col-md-12\"");

WriteLiteral(">\r\n");

WriteLiteral("            ");

            
            #line 31 "..\..\Views\CreditNotes\_CRNO005.cshtml"
       Write(Html.LabelFor(model => model.Tax, htmlAttributes: new { @class = "control-label" }));

            
            #line default
            #line hidden
WriteLiteral("\r\n");

WriteLiteral("            ");

            
            #line 32 "..\..\Views\CreditNotes\_CRNO005.cshtml"
       Write(Html.DisplayFor(model => model.Tax, new { htmlAttributes = new { @class = "form-control" } }));

            
            #line default
            #line hidden
WriteLiteral("\r\n        </div>\r\n        <div");

WriteLiteral(" class=\"col-md-12\"");

WriteLiteral(">\r\n");

WriteLiteral("            ");

            
            #line 35 "..\..\Views\CreditNotes\_CRNO005.cshtml"
       Write(Html.LabelFor(model => model.Total, htmlAttributes: new { @class = "control-label" }));

            
            #line default
            #line hidden
WriteLiteral("\r\n");

WriteLiteral("            ");

            
            #line 36 "..\..\Views\CreditNotes\_CRNO005.cshtml"
       Write(Html.DisplayFor(model => model.Total, new { htmlAttributes = new { @class = "form-control" } }));

            
            #line default
            #line hidden
WriteLiteral("\r\n        </div>\r\n    </div>\r\n    <div");

WriteLiteral(" class=\"col-md-12 form-group\"");

WriteLiteral(">       \r\n        <div");

WriteLiteral(" class=\"col-md-12\"");

WriteLiteral(">\r\n");

WriteLiteral("            ");

            
            #line 41 "..\..\Views\CreditNotes\_CRNO005.cshtml"
       Write(Html.LabelFor(model => model.ClientName, htmlAttributes: new { @class = "control-label" }));

            
            #line default
            #line hidden
WriteLiteral("\r\n");

WriteLiteral("            ");

            
            #line 42 "..\..\Views\CreditNotes\_CRNO005.cshtml"
       Write(Html.DisplayFor(model => model.ClientName, new { htmlAttributes = new { @class = "form-control" } }));

            
            #line default
            #line hidden
WriteLiteral("\r\n        </div>\r\n        <div");

WriteLiteral(" class=\"col-md-12\"");

WriteLiteral(">\r\n");

WriteLiteral("            ");

            
            #line 45 "..\..\Views\CreditNotes\_CRNO005.cshtml"
       Write(Html.LabelFor(model => model.Invoice, htmlAttributes: new { @class = "control-label" }));

            
            #line default
            #line hidden
WriteLiteral("\r\n");

WriteLiteral("            ");

            
            #line 46 "..\..\Views\CreditNotes\_CRNO005.cshtml"
       Write(Html.DisplayFor(model => model.Invoice, new { htmlAttributes = new { @class = "form-control" } }));

            
            #line default
            #line hidden
WriteLiteral("\r\n        </div>\r\n    </div>\r\n");

            
            #line 49 "..\..\Views\CreditNotes\_CRNO005.cshtml"
    
            
            #line default
            #line hidden
            
            #line 49 "..\..\Views\CreditNotes\_CRNO005.cshtml"
     if (!Model.IsBonification)
    {

            
            #line default
            #line hidden
WriteLiteral("        <h5");

WriteLiteral(" class=\"text-center\"");

WriteLiteral(">Detalle</h5>\r\n");

WriteLiteral("        <hr");

WriteLiteral(" style=\"border-top: 1px solid #3498db !important;\"");

WriteLiteral(" />        \r\n");

WriteLiteral("        <div");

WriteLiteral(" class=\"table-responsive\"");

WriteLiteral(">\r\n            <table");

WriteLiteral(" id=\"document-table\"");

WriteLiteral(" class=\"table table-striped table-bordered table-hover\"");

WriteLiteral(" style=\"width:100%\"");

WriteLiteral(">\r\n                <thead>\r\n                    <tr>\r\n                        <th" +
">");

            
            #line 57 "..\..\Views\CreditNotes\_CRNO005.cshtml"
                       Write(Html.LabelFor(model => model.Products.First().ItemNumber));

            
            #line default
            #line hidden
WriteLiteral("</th>\r\n                        <th>");

            
            #line 58 "..\..\Views\CreditNotes\_CRNO005.cshtml"
                       Write(Html.LabelFor(model => model.Products.First().ItemName));

            
            #line default
            #line hidden
WriteLiteral("</th>\r\n                        <th>");

            
            #line 59 "..\..\Views\CreditNotes\_CRNO005.cshtml"
                       Write(Html.LabelFor(model => model.Products.First().Quantity));

            
            #line default
            #line hidden
WriteLiteral("</th>\r\n                        <th>");

            
            #line 60 "..\..\Views\CreditNotes\_CRNO005.cshtml"
                       Write(Html.LabelFor(model => model.Products.First().ItemPrice));

            
            #line default
            #line hidden
WriteLiteral("</th>\r\n                        <th>");

            
            #line 61 "..\..\Views\CreditNotes\_CRNO005.cshtml"
                       Write(Html.LabelFor(model => model.Products.First().TotalItemPrice));

            
            #line default
            #line hidden
WriteLiteral("</th>\r\n                    </tr>\r\n                </thead>\r\n                <tbod" +
"y>\r\n");

            
            #line 65 "..\..\Views\CreditNotes\_CRNO005.cshtml"
                    
            
            #line default
            #line hidden
            
            #line 65 "..\..\Views\CreditNotes\_CRNO005.cshtml"
                     foreach (var item in Model.Products)
                    {

            
            #line default
            #line hidden
WriteLiteral("                        <tr>\r\n                            <td>");

            
            #line 68 "..\..\Views\CreditNotes\_CRNO005.cshtml"
                           Write(Html.DisplayFor(modelItem => item.ItemNumber));

            
            #line default
            #line hidden
WriteLiteral("</td>\r\n                            <td>");

            
            #line 69 "..\..\Views\CreditNotes\_CRNO005.cshtml"
                           Write(Html.DisplayFor(modelItem => item.ItemName));

            
            #line default
            #line hidden
WriteLiteral("</td>\r\n                            <td>");

            
            #line 70 "..\..\Views\CreditNotes\_CRNO005.cshtml"
                           Write(Html.DisplayFor(modelItem => item.Quantity));

            
            #line default
            #line hidden
WriteLiteral("</td>\r\n                            <td>");

            
            #line 71 "..\..\Views\CreditNotes\_CRNO005.cshtml"
                           Write(Html.DisplayFor(modelItem => item.ItemPrice));

            
            #line default
            #line hidden
WriteLiteral("</td>\r\n                            <td>");

            
            #line 72 "..\..\Views\CreditNotes\_CRNO005.cshtml"
                           Write(Html.DisplayFor(modelItem => item.TotalItemPrice));

            
            #line default
            #line hidden
WriteLiteral("</td>\r\n                        </tr>\r\n");

            
            #line 74 "..\..\Views\CreditNotes\_CRNO005.cshtml"
                    }

            
            #line default
            #line hidden
WriteLiteral("                </tbody>\r\n            </table>\r\n        </div>\r\n");

            
            #line 78 "..\..\Views\CreditNotes\_CRNO005.cshtml"
    }

            
            #line default
            #line hidden
WriteLiteral("    <div");

WriteLiteral(" class=\"row\"");

WriteLiteral(">\r\n        <div");

WriteLiteral(" class=\"col-lg-12\"");

WriteLiteral(" style=\"\"");

WriteLiteral(">\r\n            <div");

WriteLiteral(" class=\"form-group col-lg-12\"");

WriteLiteral(">\r\n                <div");

WriteLiteral(" class=\"text-right\"");

WriteLiteral(">\r\n                    <button");

WriteLiteral(" type=\"button\"");

WriteLiteral(" class=\"btn btn-success\"");

WriteLiteral(" onclick=\"CancelDocument()\"");

WriteLiteral(">Cancelar</button>\r\n                </div>\r\n            </div>\r\n        </div>\r\n " +
"   </div>\r\n</div>");

        }
    }
}
#pragma warning restore 1591
