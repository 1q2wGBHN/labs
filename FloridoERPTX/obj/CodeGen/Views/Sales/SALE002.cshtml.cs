#pragma warning disable 1591
//------------------------------------------------------------------------------
// <auto-generated>
//     Este código fue generado por una herramienta.
//     Versión de runtime:4.0.30319.42000
//
//     Los cambios en este archivo podrían causar un comportamiento incorrecto y se perderán si
//     se vuelve a generar el código.
// </auto-generated>
//------------------------------------------------------------------------------

namespace ASP
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using System.Net;
    using System.Text;
    using System.Web;
    using System.Web.Helpers;
    using System.Web.Mvc;
    using System.Web.Mvc.Ajax;
    using System.Web.Mvc.Html;
    using System.Web.Optimization;
    using System.Web.Routing;
    using System.Web.Security;
    using System.Web.UI;
    using System.Web.WebPages;
    using DevExpress.Utils;
    using DevExpress.Web;
    using DevExpress.Web.ASPxThemes;
    using DevExpress.Web.Mvc;
    using DevExpress.Web.Mvc.UI;
    using DevExpress.XtraReports;
    using DevExpress.XtraReports.UI;
    using DevExpress.XtraReports.Web;
    using DevExpress.XtraReports.Web.DocumentViewer;
    using FloridoERPTX;
    
    [System.CodeDom.Compiler.GeneratedCodeAttribute("RazorGenerator", "2.0.0.0")]
    [System.Web.WebPages.PageVirtualPathAttribute("~/Views/Sales/SALE002.cshtml")]
    public partial class _Views_Sales_SALE002_cshtml : System.Web.Mvc.WebViewPage<App.Entities.ViewModels.Sales.SalesConcIndex>
    {
        public _Views_Sales_SALE002_cshtml()
        {
        }
        public override void Execute()
        {
            
            #line 1 "..\..\Views\Sales\SALE002.cshtml"
  
    Layout = "~/Views/Shared/_Layout.cshtml";


            
            #line default
            #line hidden
WriteLiteral("\r\n");

            
            #line 5 "..\..\Views\Sales\SALE002.cshtml"
Write(Styles.Render("~/bundles/datatables/css"));

            
            #line default
            #line hidden
WriteLiteral("\r\n<link");

WriteLiteral(" rel=\"stylesheet\"");

WriteAttribute("href", Tuple.Create(" href=\"", 122), Tuple.Create("\"", 184)
, Tuple.Create(Tuple.Create("", 129), Tuple.Create<System.Object, System.Int32>(Href("~/vendor/clockpicker/dist/bootstrap-clockpicker.min.css")
, 129), false)
);

WriteLiteral(" />\r\n<link");

WriteLiteral(" rel=\"stylesheet\"");

WriteAttribute("href", Tuple.Create(" href=\"", 212), Tuple.Create("\"", 294)
, Tuple.Create(Tuple.Create("", 219), Tuple.Create<System.Object, System.Int32>(Href("~/vendor/bootstrap-datepicker-master/dist/css/bootstrap-datepicker3.min.css")
, 219), false)
);

WriteLiteral(" />\r\n\r\n");

WriteLiteral("<style");

WriteLiteral(" type=\"text/css\"");

WriteLiteral(@">
    td.details-control:before {
        display: inline-block;
        font: normal normal normal 14px/1 FontAwesome;
        font-size: inherit;
        text-rendering: auto;
        -webkit-font-smoothing: antialiased;
        content: ""\f101"";
        cursor: pointer;
    }

    tr.details td.details-control:before {
        display: inline-block;
        font: normal normal normal 14px/1 FontAwesome;
        font-size: inherit;
        text-rendering: auto;
        -webkit-font-smoothing: antialiased;
        content: ""\f103"";
        cursor: pointer;
    }
</style>

<div");

WriteLiteral(" class=\"normalheader transition animated fadeIn\"");

WriteLiteral(">\r\n    <div");

WriteLiteral(" class=\"hpanel\"");

WriteLiteral(">\r\n        <div");

WriteLiteral(" class=\"panel-body\"");

WriteLiteral(">\r\n\r\n\r\n            <a");

WriteLiteral(" class=\"small-header-action\"");

WriteLiteral(" href=\"\"");

WriteLiteral(">\r\n                <div");

WriteLiteral(" class=\"clip-header\"");

WriteLiteral(">\r\n                    <i");

WriteLiteral(" id=\"fa\"");

WriteLiteral(" class=\"fa fa-arrow-up\"");

WriteLiteral("></i>\r\n                </div>\r\n            </a>\r\n            <div");

WriteLiteral(" id=\"hbreadcrumb\"");

WriteLiteral(" class=\"pull-right m-t-lg\"");

WriteLiteral(">\r\n                <ol");

WriteLiteral(" class=\"hbreadcrumb breadcrumb\"");

WriteLiteral(">\r\n                    <li><a");

WriteAttribute("href", Tuple.Create(" href=\"", 1439), Tuple.Create("\"", 1476)
            
            #line 45 "..\..\Views\Sales\SALE002.cshtml"
, Tuple.Create(Tuple.Create("", 1446), Tuple.Create<System.Object, System.Int32>(Url.Action("SALE000","Sales")
            
            #line default
            #line hidden
, 1446), false)
);

WriteLiteral(">Ventas</a></li>\r\n                    <li><a");

WriteAttribute("href", Tuple.Create(" href=\"", 1521), Tuple.Create("\"", 1558)
            
            #line 46 "..\..\Views\Sales\SALE002.cshtml"
, Tuple.Create(Tuple.Create("", 1528), Tuple.Create<System.Object, System.Int32>(Url.Action("SALE007","Sales")
            
            #line default
            #line hidden
, 1528), false)
);

WriteLiteral(">Reportes</a></li>\r\n                    <li");

WriteLiteral(" class=\"active\"");

WriteLiteral(">\r\n                        <span>Concentrado de Ventas</span>\r\n                  " +
"  </li>\r\n                </ol>\r\n            </div>\r\n            <h2");

WriteLiteral(" class=\"font-light m-b-xs\"");

WriteLiteral(">\r\n                CONCENTRADO DE VENTAS\r\n            </h2>\r\n            <small>R" +
"eporte Concentrado de Ventas.</small>\r\n        </div>\r\n    </div>\r\n</div>\r\n<div");

WriteLiteral(" class=\"content animate-panel\"");

WriteLiteral(">\r\n    <div");

WriteLiteral(" class=\"row\"");

WriteLiteral(">\r\n        <div");

WriteLiteral(" class=\"col-lg-12\"");

WriteLiteral(">\r\n            <div");

WriteLiteral(" class=\"hpanel hblue\"");

WriteLiteral(">\r\n                ");

WriteLiteral("\r\n                <div");

WriteLiteral(" class=\"panel-body\"");

WriteLiteral(">\r\n                    <form");

WriteLiteral(" id=\"formSales\"");

WriteAttribute("action", Tuple.Create(" action=\"", 2512), Tuple.Create("\"", 2551)
            
            #line 71 "..\..\Views\Sales\SALE002.cshtml"
, Tuple.Create(Tuple.Create("", 2521), Tuple.Create<System.Object, System.Int32>(Url.Action("SALE002","Sales")
            
            #line default
            #line hidden
, 2521), false)
);

WriteLiteral(" method=\"post\"");

WriteLiteral(">\r\n                        <div");

WriteLiteral(" class=\"row\"");

WriteLiteral(">\r\n                            <div");

WriteLiteral(" class=\"form-group col-md-3\"");

WriteLiteral(">\r\n");

WriteLiteral("                                ");

            
            #line 74 "..\..\Views\Sales\SALE002.cshtml"
                           Write(Html.LabelFor(model => model.StartDate, htmlAttributes: new { @class = "control-label" }));

            
            #line default
            #line hidden
WriteLiteral("\r\n                                <div");

WriteLiteral(" class=\"col-md-7 input-group m-b\"");

WriteLiteral(">\r\n                                    <span");

WriteLiteral(" class=\"input-group-addon\"");

WriteLiteral("><i");

WriteLiteral(" class=\"fa fa-calendar\"");

WriteLiteral("></i></span>\r\n");

WriteLiteral("                                    ");

            
            #line 77 "..\..\Views\Sales\SALE002.cshtml"
                               Write(Html.EditorFor(model => model.StartDate, new { htmlAttributes = new { @class = "form-control datepicker" } }));

            
            #line default
            #line hidden
WriteLiteral("\r\n                                </div>\r\n\r\n                            </div>\r\n " +
"                           <div");

WriteLiteral(" class=\"form-group col-md-3\"");

WriteLiteral(">\r\n");

WriteLiteral("                                ");

            
            #line 82 "..\..\Views\Sales\SALE002.cshtml"
                           Write(Html.LabelFor(model => model.EndDate, htmlAttributes: new { @class = "control-label" }));

            
            #line default
            #line hidden
WriteLiteral("\r\n                                <div");

WriteLiteral(" class=\"col-md-7 input-group m-b\"");

WriteLiteral(">\r\n                                    <span");

WriteLiteral(" class=\"input-group-addon\"");

WriteLiteral("><i");

WriteLiteral(" class=\"fa fa-calendar\"");

WriteLiteral("></i></span>\r\n");

WriteLiteral("                                    ");

            
            #line 85 "..\..\Views\Sales\SALE002.cshtml"
                               Write(Html.EditorFor(model => model.EndDate, new { htmlAttributes = new { @class = "form-control datepicker" } }));

            
            #line default
            #line hidden
WriteLiteral("\r\n\r\n                                </div>\r\n\r\n                            </div>\r" +
"\n                            <div");

WriteLiteral(" class=\"form-group col-md-3\"");

WriteLiteral(">\r\n");

WriteLiteral("                                ");

            
            #line 91 "..\..\Views\Sales\SALE002.cshtml"
                           Write(Html.LabelFor(model => model.Currency, htmlAttributes: new { @class = "control-label" }));

            
            #line default
            #line hidden
WriteLiteral("\r\n                                <div");

WriteLiteral(" class=\"col-md-10 input-group m-b\"");

WriteLiteral(">\r\n");

WriteLiteral("                                    ");

            
            #line 93 "..\..\Views\Sales\SALE002.cshtml"
                               Write(Html.EnumDropDownListFor(model => model.Currency, new { @class = "form-control" }));

            
            #line default
            #line hidden
WriteLiteral("\r\n                                </div>\r\n                            </div>\r\n   " +
"                         <div");

WriteLiteral(" class=\"text-left\"");

WriteLiteral(">\r\n                                <label");

WriteLiteral(" class=\"control-label\"");

WriteLiteral(">&nbsp;</label>\r\n                                <div");

WriteLiteral(" class=\"text-left\"");

WriteLiteral(">\r\n                                    <button");

WriteLiteral(" type=\"button\"");

WriteLiteral(" class=\"btn btn-success\"");

WriteLiteral(" id=\"CreditNoteButton\"");

WriteLiteral(" onclick=\"SearchSE()\"");

WriteLiteral(">Continuar</button>\r\n                                </div>\r\n                    " +
"        </div>\r\n                        </div>\r\n\r\n                    </form>\r\n");

            
            #line 105 "..\..\Views\Sales\SALE002.cshtml"
                    
            
            #line default
            #line hidden
            
            #line 105 "..\..\Views\Sales\SALE002.cshtml"
                     if (@Model.Sales.Count > 0)
                    {


            
            #line default
            #line hidden
WriteLiteral("                        <div");

WriteLiteral(" class=\"row\"");

WriteLiteral(">\r\n                            <div");

WriteLiteral(" class=\"text-center\"");

WriteLiteral(">\r\n                                <label");

WriteLiteral(" class=\"control-label\"");

WriteLiteral(">&nbsp;</label>\r\n                                <div");

WriteLiteral(" class=\"text-center\"");

WriteLiteral(">\r\n                                    <button");

WriteLiteral(" type=\"button\"");

WriteLiteral(" class=\"btn btn-success\"");

WriteLiteral(" id=\"ReportButton\"");

WriteLiteral(" >Reporte General</button>\r\n                                </div>\r\n             " +
"               </div>\r\n                        </div>\r\n");

WriteLiteral("                        <div");

WriteLiteral(" id=\"TableDiv\"");

WriteLiteral(" class=\"animate-panel\"");

WriteLiteral(" data-effect=\"zoomIn\"");

WriteLiteral(">\r\n                            <div");

WriteLiteral(" class=\"row table-responsive\"");

WriteLiteral(">\r\n\r\n                                <table");

WriteLiteral(" id=\"TableSales\"");

WriteLiteral(" class=\"table table-striped table-bordered table-hover \"");

WriteLiteral(" style=\"width:100%; font-size:10px\"");

WriteLiteral(">\r\n                                    <thead>\r\n\r\n                               " +
"         <tr>\r\n                                            <th");

WriteLiteral(" rowspan=\"2\"");

WriteLiteral(">FECHA</th>\r\n                                            <th");

WriteLiteral(" colspan=\"4\"");

WriteLiteral(" scope=\"colgroup\"");

WriteLiteral(" style=\"text-align:center; vertical-align:middle\"");

WriteLiteral(">NOTAS DE CREDITO</th>\r\n                                            <th");

WriteLiteral(" colspan=\"4\"");

WriteLiteral(" scope=\"colgroup\"");

WriteLiteral(" style=\"text-align:center; vertical-align:middle\"");

WriteLiteral(">BONIFICACIONES</th>\r\n                                            <th");

WriteLiteral(" colspan=\"4\"");

WriteLiteral(" scope=\"colgroup\"");

WriteLiteral(" style=\"text-align:center; vertical-align:middle\"");

WriteLiteral(">FACT CANCELADAS</th>\r\n                                            <th");

WriteLiteral(" colspan=\"1\"");

WriteLiteral(" scope=\"colgroup\"");

WriteLiteral(" style=\"text-align:center; vertical-align:middle\"");

WriteLiteral(">DESCUENTOS S/VENTAS</th>\r\n                                            <th");

WriteLiteral(" colspan=\"1\"");

WriteLiteral(" scope=\"colgroup\"");

WriteLiteral(" style=\"text-align:center; vertical-align:middle\"");

WriteLiteral(">CUPONES</th>\r\n                                            <th");

WriteLiteral(" colspan=\"4\"");

WriteLiteral(" scope=\"colgroup\"");

WriteLiteral(" style=\"text-align:center; vertical-align:middle\"");

WriteLiteral(">FACTURAS</th>\r\n                                            <th");

WriteLiteral(" colspan=\"4\"");

WriteLiteral(" scope=\"colgroup\"");

WriteLiteral(" style=\"text-align:center; vertical-align:middle\"");

WriteLiteral(">GRAN TOTAL</th>\r\n                                        </tr>\r\n                " +
"                        <tr>\r\n\r\n                                            <th");

WriteLiteral(" scope=\"col\"");

WriteLiteral(">SubTotal</th>\r\n                                            <th");

WriteLiteral(" scope=\"col\"");

WriteLiteral(">IVA</th>\r\n                                            <th");

WriteLiteral(" scope=\"col\"");

WriteLiteral(">IEPS</th>\r\n                                            <th");

WriteLiteral(" scope=\"col\"");

WriteLiteral(">Total</th>\r\n                                            <th");

WriteLiteral(" scope=\"col\"");

WriteLiteral(">SubTotal</th>\r\n                                            <th");

WriteLiteral(" scope=\"col\"");

WriteLiteral(">IVA</th>\r\n                                            <th");

WriteLiteral(" scope=\"col\"");

WriteLiteral(">IEPS</th>\r\n                                            <th");

WriteLiteral(" scope=\"col\"");

WriteLiteral(">Total</th>\r\n                                            <th");

WriteLiteral(" scope=\"col\"");

WriteLiteral(">SubTotal</th>\r\n                                            <th");

WriteLiteral(" scope=\"col\"");

WriteLiteral(">IVA</th>\r\n                                            <th");

WriteLiteral(" scope=\"col\"");

WriteLiteral(">IEPS</th>\r\n                                            <th");

WriteLiteral(" scope=\"col\"");

WriteLiteral(">Total</th>\r\n                                            <th");

WriteLiteral(" scope=\"col\"");

WriteLiteral(">Total</th>\r\n                                            <th");

WriteLiteral(" scope=\"col\"");

WriteLiteral(">Total</th>\r\n                                            <th");

WriteLiteral(" scope=\"col\"");

WriteLiteral(">SubTotal</th>\r\n                                            <th");

WriteLiteral(" scope=\"col\"");

WriteLiteral(">IVA</th>\r\n                                            <th");

WriteLiteral(" scope=\"col\"");

WriteLiteral(">IEPS</th>\r\n                                            <th");

WriteLiteral(" scope=\"col\"");

WriteLiteral(">Total</th>\r\n                                            <th");

WriteLiteral(" scope=\"col\"");

WriteLiteral(">SubTotal</th>\r\n                                            <th");

WriteLiteral(" scope=\"col\"");

WriteLiteral(">IVA</th>\r\n                                            <th");

WriteLiteral(" scope=\"col\"");

WriteLiteral(">IEPS</th>\r\n                                            <th");

WriteLiteral(" scope=\"col\"");

WriteLiteral(">Total</th>\r\n                                        </tr>\r\n                     " +
"               </thead>\r\n                                    <tbody></tbody>\r\n  " +
"                                  <tfoot>\r\n                                     " +
"   <tr>\r\n                                            <th></th>\r\n                " +
"                            <th></th>\r\n                                         " +
"   <th></th>\r\n                                            <th></th>\r\n           " +
"                                 <th></th>\r\n                                    " +
"        <th></th>\r\n                                            <th></th>\r\n      " +
"                                      <th></th>\r\n                               " +
"             <th></th>\r\n                                            <th></th>\r\n " +
"                                           <th></th>\r\n                          " +
"                  <th></th>\r\n                                            <th></t" +
"h>\r\n                                            <th></th>\r\n                     " +
"                       <th></th>\r\n                                            <t" +
"h></th>\r\n                                            <th></th>\r\n                " +
"                            <th></th>\r\n                                         " +
"   <th></th>\r\n                                            <th></th>\r\n           " +
"                                 <th></th>\r\n                                    " +
"        <th></th>\r\n                                            <th></th>\r\n\r\n    " +
"                                    </tr>\r\n                                    <" +
"/tfoot>\r\n                                </table>\r\n\r\n                           " +
" </div>\r\n                        </div>\r\n");

            
            #line 191 "..\..\Views\Sales\SALE002.cshtml"
                    }
                    else
                    {

            
            #line default
            #line hidden
WriteLiteral("                        <div");

WriteLiteral(" class=\"content animated-panel\"");

WriteLiteral(" id=\"report-section\"");

WriteLiteral(">\r\n                        <div");

WriteLiteral(" class=\"hpanel hblue\"");

WriteLiteral(">\r\n                            <div");

WriteLiteral(" class=\"panel-body\"");

WriteLiteral(">\r\n                                <div");

WriteLiteral(" class=\"row\"");

WriteLiteral(">\r\n                                    <h4");

WriteLiteral(" style=\"text-align:center;color:red;\"");

WriteLiteral(">No hay información con esos parámetros</h4>\r\n                                </d" +
"iv>\r\n                            </div>\r\n                        </div>\r\n       " +
"             </div>\r\n");

            
            #line 203 "..\..\Views\Sales\SALE002.cshtml"
                    }

            
            #line default
            #line hidden
WriteLiteral("                </div>\r\n            </div>\r\n        </div>\r\n        <div");

WriteLiteral(" class=\"modal fade hmodal-info\"");

WriteLiteral(" id=\"ModalReport\"");

WriteLiteral(" tabindex=\"-1\"");

WriteLiteral(" role=\"dialog\"");

WriteLiteral(" aria-hidden=\"true\"");

WriteLiteral(">\r\n            <div");

WriteLiteral(" class=\"modal-dialog modal-lg\"");

WriteLiteral(">\r\n                <div");

WriteLiteral(" class=\"modal-content\"");

WriteLiteral(">\r\n                    <div");

WriteLiteral(" class=\"color-line\"");

WriteLiteral("></div>\r\n                    <div");

WriteLiteral(" class=\"modal-header\"");

WriteLiteral(">\r\n                        <h4");

WriteLiteral(" id=\"ModalTitle\"");

WriteLiteral(" class=\"modal-title\"");

WriteLiteral(">Reporte de Ventas</h4>\r\n                        <small");

WriteLiteral(" id=\"ModalDescription\"");

WriteLiteral(" class=\"font-bold\"");

WriteLiteral("></small>\r\n                    </div>\r\n                    <div");

WriteLiteral(" class=\"modal-body\"");

WriteLiteral(">\r\n                        <div");

WriteLiteral(" class=\"row\"");

WriteLiteral(">\r\n                            <iframe");

WriteLiteral(" id=\"iframe\"");

WriteLiteral(" style=\"width:100%\"");

WriteLiteral(" height=\"350\"");

WriteLiteral(" frameborder=\"0\"");

WriteLiteral("></iframe>\r\n                        </div>\r\n                    </div>\r\n         " +
"           <div");

WriteLiteral(" class=\"modal-footer\"");

WriteLiteral(">\r\n                        <button");

WriteLiteral(" type=\"button\"");

WriteLiteral(" class=\"btn btn-default\"");

WriteLiteral(" data-dismiss=\"modal\"");

WriteLiteral(">Cancelar</button>\r\n                    </div>\r\n                </div>\r\n         " +
"   </div>\r\n        </div>\r\n    </div>\r\n    </div>\r\n\r\n");

DefineSection("Scripts", () => {

            
            #line 229 "..\..\Views\Sales\SALE002.cshtml"
    
            
            #line default
            #line hidden
WriteLiteral("\r\n\r\n<script");

WriteLiteral(" src=\"//cdn.datatables.net/plug-ins/1.10.11/sorting/date-eu.js\"");

WriteLiteral(" type=\"text/javascript\"");

WriteLiteral("></script>\r\n        <script");

WriteAttribute("src", Tuple.Create(" src=\"", 11967), Tuple.Create("\"", 11998)
, Tuple.Create(Tuple.Create("", 11973), Tuple.Create<System.Object, System.Int32>(Href("~/Vendor/moment/moment.js")
, 11973), false)
);

WriteLiteral("></script>\r\n        <script");

WriteLiteral(" type=\"text/javascript\"");

WriteLiteral(">\r\n\r\n            jQuery(document).ready(function () {\r\n\r\n                var Prod" +
"ucts = ");

            
            #line 237 "..\..\Views\Sales\SALE002.cshtml"
                          Write(Html.Raw(Json.Encode(Model.Sales)));

            
            #line default
            #line hidden
WriteLiteral(";\r\n                console.log(Products);\r\n                table.clear();\r\n      " +
"          table.rows.add(jQuery(Products));\r\n                table.columns.adjus" +
"t().draw();\r\n                $(\"#CreditNoteButton\").prop(\'textContent\', \"Continu" +
"ar\");\r\n                $(\"#CreditNoteButton\").prop(\'disabled\', false);\r\n        " +
"           \r\n                if(Products.length>0)\r\n                {\r\n         " +
"          \r\n                    footers();\r\n                }\r\n\r\n               " +
" else\r\n                {\r\n\r\n                   \r\n                    footers0();" +
"\r\n                }\r\n                $(\"#StartDate\").datepicker({\r\n             " +
"       autoclose: true,\r\n                    todayHighlight: true,\r\n            " +
"        format: \'dd/mm/yyyy\',\r\n                    language: \'es\'\r\n\r\n           " +
"     });\r\n                $(\"#EndDate\").datepicker({\r\n                    autocl" +
"ose: true,\r\n                    todayHighlight: true,\r\n                    forma" +
"t: \'dd/mm/yyyy\',\r\n                    language: \'es\'\r\n                });\r\n\r\n\r\n\r" +
"\n\r\n                $(\"#StartDate\").attr(\'readonly\', \'readonly\').css(\'background-" +
"color\', \'#fff\');\r\n                $(\"#EndDate\").attr(\'readonly\', \'readonly\').css" +
"(\'background-color\', \'#fff\');\r\n\r\n\r\n\r\n            });\r\n\r\n\r\n            var table " +
"= $(\'#TableSales\').DataTable({\r\n                oLanguage: {\r\n                  " +
"  \"sProcessing\": \"Procesando...\", \"sLengthMenu\": \"Mostrar _MENU_ registros\", \"sZ" +
"eroRecords\": \"No se encontraron resultados\", \"sEmptyTable\": \"Ningún dato disponi" +
"ble en esta tabla\", \"sInfo\": \"Mostrando registros del _START_ al _END_ de un tot" +
"al de _TOTAL_ registros\", \"sInfoEmpty\": \"Mostrando registros del 0 al 0 de un to" +
"tal de 0 registros\", \"sInfoFiltered\": \"(filtrado de un total de _MAX_ registros)" +
"\", \"sInfoPostFix\": \"\", \"sSearch\": \"Buscar:\", \"sUrl\": \"\", \"sInfoThousands\": \",\", " +
"\"sLoadingRecords\": \"Cargando...\", \"oPaginate\": { \"sFirst\": \"Primero\", \"sLast\": \"" +
"Último\", \"sNext\": \"Siguiente\", \"sPrevious\": \"Anterior\" }, \"oAria\": { \"sSortAscen" +
"ding\": \": Activar para ordenar la columna de manera ascendente\", \"sSortDescendin" +
"g\": \": Activar para ordenar la columna de manera descendente\" }\r\n               " +
" },\r\n                dom: \"<\'row\'<\'col-sm-4\'l><\'col-sm-4 text-left\'B><\'col-sm-4\'" +
"f>t<\'col-sm-6\'i><\'col-sm-6\'p>>\",\r\n                lengthMenu: [[10, 25, 50, -1]," +
" [10, 25, 50, \"Todos\"]],\r\n                order: [[ 0, \"asc\" ]],\r\n              " +
"  buttons: [\r\n\r\n                ],\r\n                columns: [\r\n\r\n              " +
"      { data: \'fecha\'},\r\n                    { data: \'cn_subtotal\'},\r\n          " +
"          { data: \'cn_iva\'},\r\n                    { data: \'cn_ieps\'},\r\n         " +
"           { data: \'cn_total\'},\r\n                    { data: \'bon_subtotal\'},\r\n " +
"                   { data: \'bon_iva\'},\r\n                    { data: \'bon_ieps\'}," +
"\r\n                    { data: \'bon_total\'},\r\n                    { data: \'fc_sub" +
"total\'},\r\n                    { data: \'fc_iva\'},\r\n                    { data: \'f" +
"c_ieps\'},\r\n                    { data: \'fc_total\'},\r\n                    { data:" +
" \'dv_total\'},\r\n                    { data: \'cu_total\'},\r\n                    { d" +
"ata: \'fac_subtotal\'},\r\n                    { data: \'fac_iva\'},\r\n                " +
"    { data: \'fac_ieps\'},\r\n                    { data: \'fac_total\'},\r\n           " +
"         { data: \'gt_subtotal\'},\r\n                    { data: \'gt_iva\'},\r\n      " +
"              { data: \'gt_ieps\'},\r\n                    { data: \'gt_total\'}\r\n    " +
"            ],\r\n\r\n                columnDefs: [\r\n                    {\r\n        " +
"                type: \'numeric-comma\',\r\n                        render: function" +
" (data, type, row) {\r\n                            return \'$\' + data.toFixed(2);\r" +
"\n                        },\r\n                        targets: [1,2,3,4,5,6,7,8,9" +
",10,11,12,13,14,15,16,17,18,19,20,21,22]\r\n                    },\r\n              " +
"      {targets:[0], type:\'date-eu\'},\r\n                    {\r\n                   " +
"     targets: [1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22],\r\n     " +
"                   className: \'text-center\'\r\n                    }]\r\n           " +
"   \r\n\r\n            });\r\n            var detailRows = [];\r\n\r\n\r\n            $(\"#Re" +
"portButton\").click(function () {\r\n                var obj = ");

            
            #line 339 "..\..\Views\Sales\SALE002.cshtml"
                     Write(Html.Raw(Json.Encode(Model)));

            
            #line default
            #line hidden
WriteLiteral(";\r\n                StartLoading();\r\n                $.ajax({\r\n\r\n                 " +
"   type: \"POST\",\r\n                    url: \"");

            
            #line 344 "..\..\Views\Sales\SALE002.cshtml"
                      Write(Url.Action("ActionReportGral", "Sales"));

            
            #line default
            #line hidden
WriteLiteral("\",\r\n                    dataType: \"html\",\r\n                    data: { \"model\":ob" +
"j },//{ \"Model\":  $(\"#formCreNoBo\").serialize()},\r\n                    success: " +
"function (returndates) {\r\n                        document.getElementById(\"ifram" +
"e\").srcdoc = returndates;\r\n                        EndLoading();\r\n              " +
"          $(\"#ModalDescription\").html(\'Concentrado de Ventas\');\r\n               " +
"         $(\'#ModalReport\').modal(\'show\');\r\n                    },\r\n             " +
"       error: function (returndates) {\r\n                        EndLoading();\r\n " +
"                       toastr.error(\'Alerta - Error inesperado  contactar a sist" +
"emas.\');\r\n\r\n                    }\r\n\r\n                });\r\n            });\r\n\r\n   " +
"         function SearchSE(){\r\n\r\n\r\n\r\n                var dateIni = stringToDate(" +
"$(\"#StartDate\").val());\r\n                var dateFin = stringToDate($(\"#EndDate\"" +
").val());\r\n\r\n                if (dateFin >= dateIni) {\r\n                   \r\n   " +
"                 $(\"#formSales\").submit();\r\n                    $(\"#CreditNoteBu" +
"tton\").prop(\'textContent\', \"Consultando...\");\r\n                    $(\"#CreditNot" +
"eButton\").prop(\'disabled\', true);\r\n                   \r\n\r\n                  \r\n  " +
"              }\r\n                else\r\n                    toastr.error(\'Alerta " +
"- Fecha Final debe ser mayor o igual a la fecha Inicial. (\' + $(\"#StartDate\").va" +
"l() + \')\');\r\n\r\n            }\r\n            function footers () {\r\n               " +
" for (var i = 19; i < 23; i++) {\r\n                    var foot = table.column( i" +
" );\r\n\r\n                    $( foot.footer() ).html(\r\n                           " +
"            \'$\'+foot.data().reduce( function (a,b) {\r\n                          " +
"                 return (a+b);\r\n                                       } ).toFix" +
"ed(2)\r\n                                   );\r\n                }\r\n\r\n            }" +
"\r\n            function footers0 () {\r\n                for (var i = 19; i < 23; i" +
"++) {\r\n                    var foot = table.column( i );\r\n\r\n                    " +
"$( foot.footer() ).html(\'\');\r\n                }\r\n\r\n            }\r\n\r\n\r\n        </" +
"script>\r\n    ");

});

        }
    }
}
#pragma warning restore 1591
