#pragma warning disable 1591
//------------------------------------------------------------------------------
// <auto-generated>
//     Este código fue generado por una herramienta.
//     Versión de runtime:4.0.30319.42000
//
//     Los cambios en este archivo podrían causar un comportamiento incorrecto y se perderán si
//     se vuelve a generar el código.
// </auto-generated>
//------------------------------------------------------------------------------

namespace ASP
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using System.Net;
    using System.Text;
    using System.Web;
    using System.Web.Helpers;
    using System.Web.Mvc;
    using System.Web.Mvc.Ajax;
    using System.Web.Mvc.Html;
    using System.Web.Optimization;
    using System.Web.Routing;
    using System.Web.Security;
    using System.Web.UI;
    using System.Web.WebPages;
    using DevExpress.Utils;
    using DevExpress.Web;
    using DevExpress.Web.ASPxThemes;
    using DevExpress.Web.Mvc;
    using DevExpress.Web.Mvc.UI;
    using DevExpress.XtraReports;
    using DevExpress.XtraReports.UI;
    using DevExpress.XtraReports.Web;
    using DevExpress.XtraReports.Web.DocumentViewer;
    using FloridoERPTX;
    
    [System.CodeDom.Compiler.GeneratedCodeAttribute("RazorGenerator", "2.0.0.0")]
    [System.Web.WebPages.PageVirtualPathAttribute("~/Views/Sanctions/SANC001.cshtml")]
    public partial class _Views_Sanctions_SANC001_cshtml : System.Web.Mvc.WebViewPage<App.Entities.ViewModels.Sanctions.SanctionIndex>
    {
        public _Views_Sanctions_SANC001_cshtml()
        {
        }
        public override void Execute()
        {
            
            #line 3 "..\..\Views\Sanctions\SANC001.cshtml"
  
    ViewBag.Title = "Sanciones";
    Layout = "~/Views/Shared/_Layout.cshtml";

            
            #line default
            #line hidden
WriteLiteral("\r\n<div");

WriteLiteral(" class=\"normalheader transition animated fadeIn\"");

WriteLiteral(">\r\n    <div");

WriteLiteral(" class=\"hpanel\"");

WriteLiteral(">\r\n        <div");

WriteLiteral(" class=\"panel-body\"");

WriteLiteral(">\r\n            <a");

WriteLiteral(" class=\"small-header-action\"");

WriteLiteral(" href=\"\"");

WriteLiteral(">\r\n                <div");

WriteLiteral(" class=\"clip-header\"");

WriteLiteral(">\r\n                    <i");

WriteLiteral(" id=\"fa\"");

WriteLiteral(" class=\"fa fa-arrow-up\"");

WriteLiteral("></i>\r\n                </div>\r\n            </a>\r\n            <div");

WriteLiteral(" id=\"hbreadcrumb\"");

WriteLiteral(" class=\"pull-right m-t-lg\"");

WriteLiteral(">\r\n                <ol");

WriteLiteral(" class=\"hbreadcrumb breadcrumb\"");

WriteLiteral(">\r\n                    <li><a");

WriteAttribute("href", Tuple.Create(" href=\"", 600), Tuple.Create("\"", 635)
            
            #line 17 "..\..\Views\Sanctions\SANC001.cshtml"
, Tuple.Create(Tuple.Create("", 607), Tuple.Create<System.Object, System.Int32>(Url.Action("POS000", "POS")
            
            #line default
            #line hidden
, 607), false)
);

WriteLiteral(">Cajas</a></li>\r\n                    <li><a");

WriteAttribute("href", Tuple.Create(" href=\"", 679), Tuple.Create("\"", 714)
            
            #line 18 "..\..\Views\Sanctions\SANC001.cshtml"
, Tuple.Create(Tuple.Create("", 686), Tuple.Create<System.Object, System.Int32>(Url.Action("POS001", "POS")
            
            #line default
            #line hidden
, 686), false)
);

WriteLiteral(">Movimientos</a></li>                   \r\n                        <li");

WriteLiteral(" class=\"active\"");

WriteLiteral(">\r\n                            <span>Agregar Sanciones</span>\r\n                  " +
"      </li>                   \r\n                </ol>\r\n            </div>       " +
"    \r\n                <h2");

WriteLiteral(" class=\"font-light m-b-xs\"");

WriteLiteral(">\r\n                    Sanciones\r\n                </h2>\r\n                <small>M" +
"ódulo para Agregar Sanciones</small><br />\r\n        </div>\r\n    </div>\r\n</div>\r\n" +
"\r\n<div");

WriteLiteral(" class=\"content animated-panel\"");

WriteLiteral(">\r\n    <div");

WriteLiteral(" class=\"hpanel hblue\"");

WriteLiteral(">\r\n        <div");

WriteLiteral(" class=\"panel-body\"");

WriteLiteral(">\r\n            <div");

WriteLiteral(" class=\"row\"");

WriteLiteral(">\r\n                <h3");

WriteLiteral(" class=\"text-center\"");

WriteLiteral(">DISTRIBUIDORA EL FLORIDO, S.A. DE C.V.</h3>\r\n            </div>\r\n            <di" +
"v");

WriteLiteral(" class=\"col-md-4\"");

WriteLiteral(">\r\n                <form");

WriteLiteral(" id=\"formSanctions\"");

WriteAttribute("action", Tuple.Create(" action=\"", 1490), Tuple.Create("\"", 1534)
            
            #line 39 "..\..\Views\Sanctions\SANC001.cshtml"
, Tuple.Create(Tuple.Create("", 1499), Tuple.Create<System.Object, System.Int32>(Url.Action("SANC001", "Sanctions")
            
            #line default
            #line hidden
, 1499), false)
);

WriteLiteral(" method=\"post\"");

WriteLiteral(">\r\n                    <div");

WriteLiteral(" class=\"form-group col-md-12\"");

WriteLiteral(">\r\n");

WriteLiteral("                        ");

            
            #line 41 "..\..\Views\Sanctions\SANC001.cshtml"
                   Write(Html.LabelFor(model => model.IssueDate, htmlAttributes: new { @class = "control-label" }));

            
            #line default
            #line hidden
WriteLiteral("\r\n                        <div");

WriteLiteral(" class=\"col-md-12 input-group m-b\"");

WriteLiteral(">\r\n                            <span");

WriteLiteral(" class=\"input-group-addon\"");

WriteLiteral("><i");

WriteLiteral(" class=\"fa fa-calendar\"");

WriteLiteral("></i></span>\r\n");

WriteLiteral("                            ");

            
            #line 44 "..\..\Views\Sanctions\SANC001.cshtml"
                       Write(Html.EditorFor(model => model.IssueDate, new { htmlAttributes = new { @class = "form-control datepicker" } }));

            
            #line default
            #line hidden
WriteLiteral("\r\n                        </div>\r\n                    </div>\r\n                   " +
" <div");

WriteLiteral(" class=\"form-group col-md-12\"");

WriteLiteral(">\r\n");

WriteLiteral("                        ");

            
            #line 48 "..\..\Views\Sanctions\SANC001.cshtml"
                   Write(Html.LabelFor(model => model.emp, htmlAttributes: new { @class = "control-label" }));

            
            #line default
            #line hidden
WriteLiteral("\r\n                        <div");

WriteLiteral(" class=\"col-md-12 input-group m-b\"");

WriteLiteral(">\r\n");

WriteLiteral("                            ");

            
            #line 50 "..\..\Views\Sanctions\SANC001.cshtml"
                       Write(Html.DropDownListFor(model => model.emp, null, "-- Seleccione el empleado--", htmlAttributes: new { @class = "form-control select2" }));

            
            #line default
            #line hidden
WriteLiteral("\r\n                        </div>\r\n                    </div>\r\n                   " +
" <div");

WriteLiteral(" class=\"form-group  col-md-6 \"");

WriteLiteral(">\r\n");

WriteLiteral("                        ");

            
            #line 54 "..\..\Views\Sanctions\SANC001.cshtml"
                   Write(Html.LabelFor(model => model.total, htmlAttributes: new { @class = "control-label" }));

            
            #line default
            #line hidden
WriteLiteral("\r\n                        <div");

WriteLiteral(" id=\"myDiv\"");

WriteLiteral(" class=\"col-md-12 input-group m-b\"");

WriteLiteral(">\r\n                            <span");

WriteLiteral(" class=\"input-group-addon\"");

WriteLiteral("><i");

WriteLiteral(" class=\"fa fa-usd\"");

WriteLiteral("></i></span>\r\n");

WriteLiteral("                            ");

            
            #line 57 "..\..\Views\Sanctions\SANC001.cshtml"
                       Write(Html.EditorFor(model => model.total, new { htmlAttributes = new { @class = "form-control", @type = "number", @min = "0", @onkeypress = "return event.charCode >= 48 && event.charCode <= 57 ||  event.charCode ==46" } }));

            
            #line default
            #line hidden
WriteLiteral("\r\n                        </div>\r\n                    </div>\r\n                   " +
" <div");

WriteLiteral(" class=\"form-group col-md-6\"");

WriteLiteral(">\r\n");

WriteLiteral("                        ");

            
            #line 61 "..\..\Views\Sanctions\SANC001.cshtml"
                   Write(Html.LabelFor(model => model.currency, htmlAttributes: new { @class = "control-label" }));

            
            #line default
            #line hidden
WriteLiteral("\r\n                        <div");

WriteLiteral(" class=\"col-md-12 input-group m-b\"");

WriteLiteral(">\r\n                            <span");

WriteLiteral(" class=\"input-group-addon\"");

WriteLiteral("><i");

WriteLiteral(" class=\"fa fa-usd\"");

WriteLiteral("></i></span>\r\n");

WriteLiteral("                            ");

            
            #line 64 "..\..\Views\Sanctions\SANC001.cshtml"
                       Write(Html.DropDownListFor(model => model.currency,null,  htmlAttributes: new { @class = "form-control select" }));

            
            #line default
            #line hidden
WriteLiteral("\r\n                        </div>\r\n                    </div>\r\n                   " +
" <div");

WriteLiteral(" class=\"form-group col-md-6\"");

WriteLiteral(">\r\n");

WriteLiteral("                        ");

            
            #line 68 "..\..\Views\Sanctions\SANC001.cshtml"
                   Write(Html.LabelFor(model => model.Motive, htmlAttributes: new { @class = "control-label" }));

            
            #line default
            #line hidden
WriteLiteral("\r\n");

WriteLiteral("                        ");

            
            #line 69 "..\..\Views\Sanctions\SANC001.cshtml"
                   Write(Html.DropDownListFor(model => model.Motive, null,  htmlAttributes: new { @class = "form-control select" }));

            
            #line default
            #line hidden
WriteLiteral("\r\n                    </div>\r\n                    <div");

WriteLiteral(" class=\"form-group col-md-6\"");

WriteLiteral(">\r\n");

WriteLiteral("                        ");

            
            #line 72 "..\..\Views\Sanctions\SANC001.cshtml"
                   Write(Html.HiddenFor(model => model.status));

            
            #line default
            #line hidden
WriteLiteral("\r\n");

WriteLiteral("                        ");

            
            #line 73 "..\..\Views\Sanctions\SANC001.cshtml"
                   Write(Html.LabelFor(model => model.status, htmlAttributes: new { @class = "control-label" }));

            
            #line default
            #line hidden
WriteLiteral("\r\n");

WriteLiteral("                        ");

            
            #line 74 "..\..\Views\Sanctions\SANC001.cshtml"
                   Write(Html.EnumDropDownListFor(model => model.status, null, new { @class = "form-control select", @disabled = "disabled" }));

            
            #line default
            #line hidden
WriteLiteral("\r\n                    </div>\r\n                    <div");

WriteLiteral(" class=\"form-group col-md-12\"");

WriteLiteral(">\r\n");

WriteLiteral("                        ");

            
            #line 77 "..\..\Views\Sanctions\SANC001.cshtml"
                   Write(Html.LabelFor(model => model.Comments, htmlAttributes: new { @class = "control-label" }));

            
            #line default
            #line hidden
WriteLiteral("\r\n");

WriteLiteral("                        ");

            
            #line 78 "..\..\Views\Sanctions\SANC001.cshtml"
                   Write(Html.EditorFor(model => model.Comments, new { htmlAttributes = new { @class = "form-control", @type = "text" } }));

            
            #line default
            #line hidden
WriteLiteral("\r\n                    </div>\r\n                    <div");

WriteLiteral(" class=\"col-md-4 \"");

WriteLiteral("></div>\r\n                    <div");

WriteLiteral(" class=\"col-md-4 \"");

WriteLiteral(">\r\n                        <button");

WriteLiteral(" type=\"button\"");

WriteLiteral(" class=\"btn btn-danger form-control\"");

WriteLiteral(" id=\"CancelButton\"");

WriteLiteral(">Cancelar</button>\r\n                    </div>\r\n                    <div");

WriteLiteral(" class=\"col-md-4 \"");

WriteLiteral(">\r\n                        <button");

WriteLiteral(" type=\"button\"");

WriteLiteral(" class=\"btn btn-success form-control\"");

WriteLiteral(" id=\"SanctionButton\"");

WriteLiteral(">Continuar</button>\r\n                    </div>\r\n                </form>\r\n       " +
"     </div>\r\n            <div");

WriteLiteral(" class=\"col-md-8\"");

WriteLiteral(">\r\n                <label");

WriteLiteral(" class=\"control-label\"");

WriteLiteral(">Seleccione la Sancion para cambiar su Estatus</label>\r\n                <div");

WriteLiteral(" class=\"table-responsive\"");

WriteLiteral(">\r\n                    <table");

WriteLiteral(" id=\"SanctionsTable\"");

WriteLiteral(" class=\"table table-striped table-bordered table-hover\"");

WriteLiteral(" style=\"width:100%\"");

WriteLiteral(@">
                        <thead>
                            <tr>
                                <th></th>
                                <th>FOLIO</th>                              
                                <th>FECHA</th>
                                <th>EMPLEADO</th>
                                <th>IMPORTE</th>
                                <th>MONEDA</th>
                                <th>MOTIVO</th>
                                <th>ESTATUS</th>                               
                                <th>COMENTARIOS</th>                               
                            </tr>
                        </thead>
                        <tbody>
");

            
            #line 107 "..\..\Views\Sanctions\SANC001.cshtml"
                            
            
            #line default
            #line hidden
            
            #line 107 "..\..\Views\Sanctions\SANC001.cshtml"
                             foreach (var item in Model.SanctionsList)
                            {
                                if (item.status!=0)
                                {

            
            #line default
            #line hidden
WriteLiteral("                                    <tr>\r\n                                       " +
" <td");

WriteLiteral(" style=\"width:57px;\"");

WriteLiteral("><button");

WriteLiteral(" class=\"btn btn-xs btn-outline btn-danger\"");

WriteLiteral(" data-toggle=\"modal\"");

WriteAttribute("onclick", Tuple.Create(" onclick=\"", 6650), Tuple.Create("\"", 6692)
, Tuple.Create(Tuple.Create("", 6660), Tuple.Create("EditSanction(\'", 6660), true)
            
            #line 112 "..\..\Views\Sanctions\SANC001.cshtml"
                                                                             , Tuple.Create(Tuple.Create("", 6674), Tuple.Create<System.Object, System.Int32>(item.SanctionId
            
            #line default
            #line hidden
, 6674), false)
, Tuple.Create(Tuple.Create("", 6690), Tuple.Create("\')", 6690), true)
);

WriteLiteral(">Editar</button></td>\r\n                                        <td>");

            
            #line 113 "..\..\Views\Sanctions\SANC001.cshtml"
                                       Write(Html.DisplayFor(modelItem => item.SanctionId));

            
            #line default
            #line hidden
WriteLiteral("</td>\r\n                                        <td>");

            
            #line 114 "..\..\Views\Sanctions\SANC001.cshtml"
                                       Write(Html.DisplayFor(modelItem => item.CreateDate));

            
            #line default
            #line hidden
WriteLiteral("</td>\r\n                                        <td>");

            
            #line 115 "..\..\Views\Sanctions\SANC001.cshtml"
                                       Write(Html.DisplayFor(modelItem => item.emp));

            
            #line default
            #line hidden
WriteLiteral("</td>\r\n                                        <td>");

            
            #line 116 "..\..\Views\Sanctions\SANC001.cshtml"
                                       Write(Html.DisplayFor(modelItem => item.total));

            
            #line default
            #line hidden
WriteLiteral("</td>\r\n                                        <td>");

            
            #line 117 "..\..\Views\Sanctions\SANC001.cshtml"
                                       Write(Html.DisplayFor(modelItem => item.currencyValue));

            
            #line default
            #line hidden
WriteLiteral("</td>\r\n                                        <td>");

            
            #line 118 "..\..\Views\Sanctions\SANC001.cshtml"
                                       Write(Html.DisplayFor(modelItem => item.Motive));

            
            #line default
            #line hidden
WriteLiteral("</td>\r\n                                        <td>");

            
            #line 119 "..\..\Views\Sanctions\SANC001.cshtml"
                                       Write(Html.DisplayFor(modelItem => item.statusVal));

            
            #line default
            #line hidden
WriteLiteral("</td>\r\n                                        <td>");

            
            #line 120 "..\..\Views\Sanctions\SANC001.cshtml"
                                       Write(Html.DisplayFor(modelItem => item.Comments));

            
            #line default
            #line hidden
WriteLiteral("</td>\r\n                                    </tr>\r\n");

            
            #line 122 "..\..\Views\Sanctions\SANC001.cshtml"
                                }
                            }

            
            #line default
            #line hidden
WriteLiteral("                        </tbody>\r\n                    </table>\r\n                <" +
"/div>\r\n            </div>\r\n        </div>\r\n    </div>   \r\n</div>\r\n\r\n<div");

WriteLiteral(" id=\"edit-sanction\"");

WriteLiteral(">\r\n</div>\r\n");

DefineSection("Scripts", () => {

WriteLiteral("\r\n\r\n    <script");

WriteLiteral(" type=\"text/javascript\"");

WriteLiteral(">\r\n        jQuery(document).ready(function () {\r\n            $(\"#status\").prop(\'s" +
"electedIndex\', 1);\r\n            $(\"#IssueDate\").datepicker({\r\n                au" +
"toclose: true,\r\n                todayHighlight: true,\r\n                format: \'" +
"dd/mm/yyyy\',\r\n                language: \'es\'\r\n            });\r\n\r\n            san" +
"csTable = $(\'#SanctionsTable\').DataTable({\r\n                oLanguage: {\r\n      " +
"              \"sProcessing\": \"Procesando...\", \"sLengthMenu\": \"Mostrar _MENU_ reg" +
"istros\", \"sZeroRecords\": \"No se encontraron resultados\", \"sEmptyTable\": \"Ningún " +
"dato disponible en esta tabla\", \"sInfo\": \"Mostrando registros del _START_ al _EN" +
"D_ de un total de _TOTAL_ registros\", \"sInfoEmpty\": \"Mostrando registros del 0 a" +
"l 0 de un total de 0 registros\", \"sInfoFiltered\": \"(filtrado de un total de _MAX" +
"_ registros)\", \"sInfoPostFix\": \"\", \"sSearch\": \"Buscar:\", \"sUrl\": \"\", \"sInfoThous" +
"ands\": \",\", \"sLoadingRecords\": \"Cargando...\", \"oPaginate\": { \"sFirst\": \"Primero\"" +
", \"sLast\": \"Último\", \"sNext\": \"Siguiente\", \"sPrevious\": \"Anterior\" }, \"oAria\": {" +
" \"sSortAscending\": \": Activar para ordenar la columna de manera ascendente\", \"sS" +
"ortDescending\": \": Activar para ordenar la columna de manera descendente\" }\r\n   " +
"             },\r\n                dom: \"<\'row\'<\'col-sm-4\'l><\'col-sm-4 text-left\'B" +
"><\'col-sm-4\'f>t<\'col-sm-6\'i><\'col-sm-6\'p>>\",\r\n                lengthMenu: [[10, " +
"25, 50, -1], [10, 25, 50, \"Todos\"]],\r\n                buttons: [  ],\r\n          " +
"      columns: [\r\n                    { data: null, defaultContent: \'<button cla" +
"ss=\"btn btn-xs btn-outline btn-danger\">Editar</button>\', width: \"1%\" },\r\n       " +
"             { data: \'SanctionId\' },\r\n                    { data: \'CreateDate\' }" +
",\r\n                    { data: \'emp\' },\r\n                    { data: \'total\' },\r" +
"\n                    { data: \'currencyValue\' },\r\n                    { data: \'Mo" +
"tive\' },\r\n                    { data: \'statusVal\' },\r\n                    { data" +
": \'Comments\' },\r\n                ],\r\n\r\n                columnDefs: [\r\n          " +
"          {\r\n                        targets: [0],\r\n                        orde" +
"rable: false\r\n                    },\r\n                ]\r\n            });\r\n      " +
"  });\r\n\r\n        $(\'#SanctionsTable tbody\').on(\'click\', \'button\', function () {\r" +
"\n            var data = sancsTable.row($(this).parents(\'tr\')).data();\r\n         " +
"   EditSanction(data.SanctionId);\r\n        });\r\n\r\n        $(\"#SanctionButton\").c" +
"lick(function () {\r\n\r\n            var empleado = $(\"#emp\").val();\r\n            v" +
"ar importe = $(\"#total\").val();\r\n            var motivo = $(\"#Motive\").val();\r\n " +
"           var moneda = $(\"#currency\").val();\r\n\r\n            if (empleado != \"\")" +
" {\r\n                if (importe != \"\" && importe > 0) {\r\n                    if " +
"(motivo != \"\") {\r\n                        if (moneda != \"\") {\r\n                 " +
"           CreateSanction();\r\n                        }\r\n                       " +
" else\r\n                            toastr.error(\'Alerta - Elige una Moneda.\');\r\n" +
"                    }\r\n                    else\r\n                        toastr." +
"error(\'Alerta - Elige un Motivo.\');\r\n                }\r\n                else {\r\n" +
"                    toastr.error(\'Alerta - Ingresa el importe.\');\r\n\r\n           " +
"     }\r\n            }\r\n            else\r\n                toastr.error(\'Alerta - " +
"Elige un empleado.\');\r\n        });\r\n\r\n        $(\"#CancelButton\").click(function " +
"() {\r\n            $(\"#emp\").prop(\'selectedIndex\', -1);\r\n            $(\"#currency" +
"\").prop(\'selectedIndex\', 0);\r\n            $(\"#Motive\").prop(\'selectedIndex\', 0);" +
"\r\n            $(\"#status\").prop(\'selectedIndex\', 1);\r\n            $(\"#total\").va" +
"l(\"\");\r\n            $(\"#Comments\").val(\"\");\r\n        });\r\n\r\n        function Cre" +
"ateSanction() {\r\n            StartLoading();\r\n            $.post({\r\n            " +
"    url: \'");

            
            #line 219 "..\..\Views\Sanctions\SANC001.cshtml"
                 Write(Url.Action("ActionCreateSanction", "Sanctions"));

            
            #line default
            #line hidden
WriteLiteral(@"',
                Type: 'POST',
                data: $(""#formSanctions"").serialize(),
                dataType: 'json',
                success: function (data) {
                    if (data.success)
                    {
                        swal({
                            title: 'Sanción',
                            text: ""Sanción creada con éxito"",
                            type: ""success""
                        }, function () { window.location.reload(true); });
                        EndLoading();

                    }
                    else {
                        swal({
                            title: 'Error',
                            text: ""Ocurrio un error, contacte a sistemas para solucionarlo."",
                            type: ""error""
                        });
                        EndLoading();
                    }
                }
            });
        }

        function EditSanction(idSanction) {
            $.ajax({
                url: '");

            
            #line 248 "..\..\Views\Sanctions\SANC001.cshtml"
                 Write(Url.Action("ActionEditCustomer", "Sanctions"));

            
            #line default
            #line hidden
WriteLiteral(@"',
                Type: 'GET',
                data: { SanctionId: idSanction },
                success: function (data) {
                    $(""#edit-sanction"").html(data);
                    $(""#ModalEditSanction"").modal('show');
                }
            });
        }

        //Function to save changes on modal edit
        function SaveChanges() {
            StartLoading();
            $.post({
                url: '");

            
            #line 262 "..\..\Views\Sanctions\SANC001.cshtml"
                 Write(Url.Action("ActionEditCustomer", "Sanctions"));

            
            #line default
            #line hidden
WriteLiteral(@"',
                dataType: 'json',
                Type: 'POST',
                data: $(""#formEditSanction"").serialize(),
                success: function (data) {
                    $('#ModalEditSanction').modal('hide');
                    EndLoading();
                    swal({
                        title: 'Sanción',
                        text: ""Sanción editada exitosamente."",
                        type: ""success""
                    }, function () { window.location.reload(true); });
                },
            });
        }
    </script>    
    
    ");

});

        }
    }
}
#pragma warning restore 1591
