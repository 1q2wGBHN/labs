#pragma warning disable 1591
//------------------------------------------------------------------------------
// <auto-generated>
//     Este código fue generado por una herramienta.
//     Versión de runtime:4.0.30319.42000
//
//     Los cambios en este archivo podrían causar un comportamiento incorrecto y se perderán si
//     se vuelve a generar el código.
// </auto-generated>
//------------------------------------------------------------------------------

namespace ASP
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using System.Net;
    using System.Text;
    using System.Web;
    using System.Web.Helpers;
    using System.Web.Mvc;
    using System.Web.Mvc.Ajax;
    using System.Web.Mvc.Html;
    using System.Web.Optimization;
    using System.Web.Routing;
    using System.Web.Security;
    using System.Web.UI;
    using System.Web.WebPages;
    using DevExpress.Utils;
    using DevExpress.Web;
    using DevExpress.Web.ASPxThemes;
    using DevExpress.Web.Mvc;
    using DevExpress.Web.Mvc.UI;
    using DevExpress.XtraReports;
    using DevExpress.XtraReports.UI;
    using DevExpress.XtraReports.Web;
    using DevExpress.XtraReports.Web.DocumentViewer;
    using FloridoERPTX;
    
    [System.CodeDom.Compiler.GeneratedCodeAttribute("RazorGenerator", "2.0.0.0")]
    [System.Web.WebPages.PageVirtualPathAttribute("~/Views/Customers/_CTMS003.cshtml")]
    public partial class _Views_Customers__CTMS003_cshtml : System.Web.Mvc.WebViewPage<App.Entities.ViewModels.Customers.CustomerViewModel>
    {
        public _Views_Customers__CTMS003_cshtml()
        {
        }
        public override void Execute()
        {
WriteLiteral("<div");

WriteLiteral(" class=\"modal fade\"");

WriteLiteral(" id=\"ModalEditCustomer\"");

WriteLiteral(" tabindex=\"-1\"");

WriteLiteral(" role=\"dialog\"");

WriteLiteral(" aria-hidden=\"true\"");

WriteLiteral(">\r\n    <div");

WriteLiteral(" class=\"modal-dialog modal-lg\"");

WriteLiteral(">\r\n        <div");

WriteLiteral(" class=\"modal-content\"");

WriteLiteral(">\r\n            <div");

WriteLiteral(" class=\"color-line\"");

WriteLiteral("></div>\r\n            <div");

WriteLiteral(" class=\"modal-header\"");

WriteLiteral(">\r\n                <h4");

WriteLiteral(" id=\"ModalTitle\"");

WriteLiteral(" class=\"modal-title\"");

WriteLiteral(">Editar Cliente</h4>\r\n                <small");

WriteLiteral(" id=\"ModalDescription\"");

WriteLiteral(" class=\"font-bold\"");

WriteLiteral(">Formulario para editar un cliente.</small>\r\n            </div>\r\n            <div" +
"");

WriteLiteral(" class=\"modal-body\"");

WriteLiteral(">\r\n");

            
            #line 12 "..\..\Views\Customers\_CTMS003.cshtml"
                
            
            #line default
            #line hidden
            
            #line 12 "..\..\Views\Customers\_CTMS003.cshtml"
                 using (Html.BeginForm("ActionEditCustomer", "Customers", FormMethod.Post, new { name = "formEditClient", id = "formEditClient" }))
                {
                    
            
            #line default
            #line hidden
            
            #line 14 "..\..\Views\Customers\_CTMS003.cshtml"
               Write(Html.AntiForgeryToken());

            
            #line default
            #line hidden
            
            #line 14 "..\..\Views\Customers\_CTMS003.cshtml"
                                            


            
            #line default
            #line hidden
WriteLiteral("                    <div");

WriteLiteral(" class=\"form-horizontal\"");

WriteLiteral(">\r\n                        <hr />\r\n");

WriteLiteral("                        ");

            
            #line 18 "..\..\Views\Customers\_CTMS003.cshtml"
                   Write(Html.ValidationSummary(true, "", new { @class = "text-danger" }));

            
            #line default
            #line hidden
WriteLiteral("\r\n                        <div");

WriteLiteral(" class=\"row\"");

WriteLiteral(">\r\n                            <div");

WriteLiteral(" class=\"col-md-6\"");

WriteLiteral(">\r\n                                <div");

WriteLiteral(" class=\"form-group\"");

WriteLiteral(">\r\n");

WriteLiteral("                                    ");

            
            #line 22 "..\..\Views\Customers\_CTMS003.cshtml"
                               Write(Html.LabelFor(model => model.RFC, htmlAttributes: new { @class = "control-label col-md-4" }));

            
            #line default
            #line hidden
WriteLiteral("\r\n                                    <div");

WriteLiteral(" class=\"col-md-7 input-group m-b\"");

WriteLiteral(">\r\n                                        ");

WriteLiteral("\r\n                                        <span");

WriteLiteral(" class=\"input-group-addon\"");

WriteLiteral("><i");

WriteLiteral(" class=\"fa fa-barcode\"");

WriteLiteral("></i></span>\r\n");

WriteLiteral("                                        ");

            
            #line 26 "..\..\Views\Customers\_CTMS003.cshtml"
                                   Write(Html.EditorFor(model => model.RFC, new { htmlAttributes = new { @class = "form-control" } }));

            
            #line default
            #line hidden
WriteLiteral("\r\n\r\n\r\n\r\n                                    </div>\r\n");

WriteLiteral("                                    ");

            
            #line 31 "..\..\Views\Customers\_CTMS003.cshtml"
                               Write(Html.ValidationMessageFor(model => model.RFC, "", new { @class = "text-danger, control-label col-md-10" }));

            
            #line default
            #line hidden
WriteLiteral("\r\n\r\n                                </div>\r\n                            </div>\r\n " +
"                           <div");

WriteLiteral(" class=\"col-md-6\"");

WriteLiteral(">\r\n                                <div");

WriteLiteral(" class=\"form-group\"");

WriteLiteral(">\r\n");

WriteLiteral("                                    ");

            
            #line 37 "..\..\Views\Customers\_CTMS003.cshtml"
                               Write(Html.LabelFor(model => model.Name, htmlAttributes: new { @class = "control-label col-md-4" }));

            
            #line default
            #line hidden
WriteLiteral("\r\n                                    <div");

WriteLiteral(" class=\"col-md-7 input-group m-b\"");

WriteLiteral(">\r\n                                        ");

WriteLiteral("\r\n                                        <span");

WriteLiteral(" class=\"input-group-addon\"");

WriteLiteral("><i");

WriteLiteral(" class=\"fa fa-user-plus\"");

WriteLiteral("></i></span>\r\n");

WriteLiteral("                                        ");

            
            #line 41 "..\..\Views\Customers\_CTMS003.cshtml"
                                   Write(Html.EditorFor(model => model.Name, new { htmlAttributes = new { @class = "form-control" } }));

            
            #line default
            #line hidden
WriteLiteral("\r\n                                    </div>\r\n                                </d" +
"iv>\r\n                            </div>\r\n                        </div>\r\n       " +
"                 <div");

WriteLiteral(" class=\"row\"");

WriteLiteral(">\r\n                            <div");

WriteLiteral(" class=\"col-md-6\"");

WriteLiteral(">\r\n                                <div");

WriteLiteral(" class=\"form-group\"");

WriteLiteral(">\r\n");

WriteLiteral("                                    ");

            
            #line 49 "..\..\Views\Customers\_CTMS003.cshtml"
                               Write(Html.LabelFor(model => model.Code, htmlAttributes: new { @class = "control-label col-md-4" }));

            
            #line default
            #line hidden
WriteLiteral("\r\n                                    <div");

WriteLiteral(" class=\"col-md-7 input-group m-b\"");

WriteLiteral(">\r\n");

WriteLiteral("                                        ");

            
            #line 51 "..\..\Views\Customers\_CTMS003.cshtml"
                                   Write(Html.HiddenFor(model => model.Code));

            
            #line default
            #line hidden
WriteLiteral("\r\n                                        <span");

WriteLiteral(" class=\"input-group-addon\"");

WriteLiteral("><i");

WriteLiteral(" class=\"pe-7s-id\"");

WriteLiteral("></i></span>\r\n");

WriteLiteral("                                        ");

            
            #line 53 "..\..\Views\Customers\_CTMS003.cshtml"
                                   Write(Html.EditorFor(model => model.Code, new { htmlAttributes = new { @class = "form-control", @readonly = "readonly" } }));

            
            #line default
            #line hidden
WriteLiteral("\r\n                                    </div>\r\n                                </d" +
"iv>\r\n                            </div>\r\n                            <div");

WriteLiteral(" class=\"col-md-6\"");

WriteLiteral(">\r\n                                <div");

WriteLiteral(" class=\"form-group\"");

WriteLiteral(">\r\n");

WriteLiteral("                                    ");

            
            #line 59 "..\..\Views\Customers\_CTMS003.cshtml"
                               Write(Html.LabelFor(model => model.StateId, htmlAttributes: new { @class = "control-label col-md-4" }));

            
            #line default
            #line hidden
WriteLiteral("\r\n                                    <div");

WriteLiteral(" class=\"col-md-7 input-group m-b\"");

WriteLiteral(">\r\n                                        <span");

WriteLiteral(" class=\"input-group-addon\"");

WriteLiteral("><i");

WriteLiteral(" class=\"pe-7s-map-marker\"");

WriteLiteral("></i></span>\r\n");

WriteLiteral("                                        ");

            
            #line 62 "..\..\Views\Customers\_CTMS003.cshtml"
                                   Write(Html.DropDownListFor(model => model.StateId, null, "--Seleccionar Estado--", htmlAttributes: new { @class = "form-control" }));

            
            #line default
            #line hidden
WriteLiteral("\r\n                                    </div>\r\n                                </d" +
"iv>\r\n                            </div>\r\n                        </div>\r\n       " +
"                 <div");

WriteLiteral(" class=\"row\"");

WriteLiteral(">\r\n                            <div");

WriteLiteral(" class=\"col-md-6\"");

WriteLiteral(">\r\n                                <div");

WriteLiteral(" class=\"form-group\"");

WriteLiteral(">\r\n");

WriteLiteral("                                    ");

            
            #line 70 "..\..\Views\Customers\_CTMS003.cshtml"
                               Write(Html.LabelFor(model => model.CityId, htmlAttributes: new { @class = "control-label col-md-4" }));

            
            #line default
            #line hidden
WriteLiteral("\r\n                                    <div");

WriteLiteral(" class=\"col-md-7 input-group m-b\"");

WriteLiteral(">\r\n                                        <span");

WriteLiteral(" class=\"input-group-addon\"");

WriteLiteral("><i");

WriteLiteral(" class=\"pe-7s-map-marker\"");

WriteLiteral("></i></span>\r\n");

WriteLiteral("                                        ");

            
            #line 73 "..\..\Views\Customers\_CTMS003.cshtml"
                                   Write(Html.DropDownListFor(model => model.CityId, null, "--Seleccionar Ciudad o Municipio--", htmlAttributes: new { @class = "form-control" }));

            
            #line default
            #line hidden
WriteLiteral("\r\n                                    </div>\r\n                                </d" +
"iv>\r\n                            </div>\r\n                            <div");

WriteLiteral(" class=\"col-md-6\"");

WriteLiteral(">\r\n                                <div");

WriteLiteral(" class=\"form-group\"");

WriteLiteral(">\r\n");

WriteLiteral("                                    ");

            
            #line 79 "..\..\Views\Customers\_CTMS003.cshtml"
                               Write(Html.LabelFor(model => model.Address, htmlAttributes: new { @class = "control-label col-md-4" }));

            
            #line default
            #line hidden
WriteLiteral("\r\n                                    <div");

WriteLiteral(" class=\"col-md-7 input-group m-b\"");

WriteLiteral(">\r\n                                        <span");

WriteLiteral(" class=\"input-group-addon\"");

WriteLiteral("><i");

WriteLiteral(" class=\"pe-7s-map-marker\"");

WriteLiteral("></i></span>\r\n");

WriteLiteral("                                        ");

            
            #line 82 "..\..\Views\Customers\_CTMS003.cshtml"
                                   Write(Html.EditorFor(model => model.Address, new { htmlAttributes = new { @class = "form-control" } }));

            
            #line default
            #line hidden
WriteLiteral("\r\n                                    </div>\r\n");

WriteLiteral("                                    ");

            
            #line 84 "..\..\Views\Customers\_CTMS003.cshtml"
                               Write(Html.ValidationMessageFor(model => model.Address, "", new { @class = "text-danger" }));

            
            #line default
            #line hidden
WriteLiteral("\r\n                                </div>\r\n                            </div>\r\n   " +
"                     </div>\r\n                        <div");

WriteLiteral(" class=\"row\"");

WriteLiteral(">\r\n                            <div");

WriteLiteral(" class=\"col-md-6\"");

WriteLiteral(">\r\n                                <div");

WriteLiteral(" class=\"form-group\"");

WriteLiteral(">\r\n");

WriteLiteral("                                    ");

            
            #line 91 "..\..\Views\Customers\_CTMS003.cshtml"
                               Write(Html.LabelFor(model => model.ZipCode, htmlAttributes: new { @class = "control-label col-md-4" }));

            
            #line default
            #line hidden
WriteLiteral("\r\n                                    <div");

WriteLiteral(" class=\"col-md-7 input-group m-b\"");

WriteLiteral(">\r\n                                        <span");

WriteLiteral(" class=\"input-group-addon\"");

WriteLiteral("><i");

WriteLiteral(" class=\"fa fa-map-marker\"");

WriteLiteral("></i></span>\r\n");

WriteLiteral("                                        ");

            
            #line 94 "..\..\Views\Customers\_CTMS003.cshtml"
                                   Write(Html.EditorFor(model => model.ZipCode, new { htmlAttributes = new { @class = "form-control" } }));

            
            #line default
            #line hidden
WriteLiteral("\r\n                                    </div>\r\n");

WriteLiteral("                                    ");

            
            #line 96 "..\..\Views\Customers\_CTMS003.cshtml"
                               Write(Html.ValidationMessageFor(model => model.ZipCode, "", new { @class = "text-danger" }));

            
            #line default
            #line hidden
WriteLiteral("\r\n                                </div>\r\n                            </div>\r\n   " +
"                         <div");

WriteLiteral(" class=\"col-md-6\"");

WriteLiteral(">\r\n                                <div");

WriteLiteral(" class=\"form-group\"");

WriteLiteral(">\r\n");

WriteLiteral("                                    ");

            
            #line 101 "..\..\Views\Customers\_CTMS003.cshtml"
                               Write(Html.LabelFor(model => model.Email, htmlAttributes: new { @class = "control-label col-md-4" }));

            
            #line default
            #line hidden
WriteLiteral("\r\n                                    <div");

WriteLiteral(" class=\"col-md-7 input-group m-b\"");

WriteLiteral(">\r\n                                        <span");

WriteLiteral(" class=\"input-group-addon\"");

WriteLiteral("><i");

WriteLiteral(" class=\"pe-7s-mail\"");

WriteLiteral("></i></span>\r\n");

WriteLiteral("                                        ");

            
            #line 104 "..\..\Views\Customers\_CTMS003.cshtml"
                                   Write(Html.EditorFor(model => model.Email, new { htmlAttributes = new { @class = "form-control" } }));

            
            #line default
            #line hidden
WriteLiteral("\r\n                                    </div>\r\n");

WriteLiteral("                                    ");

            
            #line 106 "..\..\Views\Customers\_CTMS003.cshtml"
                               Write(Html.ValidationMessageFor(model => model.Email, "", new { @class = "text-danger" }));

            
            #line default
            #line hidden
WriteLiteral("\r\n                                </div>\r\n                            </div>\r\n   " +
"                     </div>\r\n                        <div");

WriteLiteral(" class=\"row\"");

WriteLiteral(">\r\n                            <div");

WriteLiteral(" class=\"col-md-6\"");

WriteLiteral(">\r\n                                <div");

WriteLiteral(" class=\"form-group\"");

WriteLiteral(">\r\n");

WriteLiteral("                                    ");

            
            #line 113 "..\..\Views\Customers\_CTMS003.cshtml"
                               Write(Html.LabelFor(model => model.Phone, htmlAttributes: new { @class = "control-label col-md-4" }));

            
            #line default
            #line hidden
WriteLiteral("\r\n                                    <div");

WriteLiteral(" class=\"col-md-7 input-group m-b\"");

WriteLiteral(">\r\n                                        <span");

WriteLiteral(" class=\"input-group-addon\"");

WriteLiteral("><i");

WriteLiteral(" class=\"fa fa-mobile-phone\"");

WriteLiteral("></i></span>\r\n");

WriteLiteral("                                        ");

            
            #line 116 "..\..\Views\Customers\_CTMS003.cshtml"
                                   Write(Html.EditorFor(model => model.Phone, new { htmlAttributes = new { @class = "form-control" } }));

            
            #line default
            #line hidden
WriteLiteral("\r\n                                    </div>\r\n");

WriteLiteral("                                    ");

            
            #line 118 "..\..\Views\Customers\_CTMS003.cshtml"
                               Write(Html.ValidationMessageFor(model => model.Phone, "", new { @class = "text-danger" }));

            
            #line default
            #line hidden
WriteLiteral("\r\n                                </div>\r\n                            </div>\r\n   " +
"                         <div");

WriteLiteral(" class=\"col-md-6\"");

WriteLiteral(">\r\n                                <div");

WriteLiteral(" class=\"form-group\"");

WriteLiteral(">\r\n");

WriteLiteral("                                    ");

            
            #line 123 "..\..\Views\Customers\_CTMS003.cshtml"
                               Write(Html.LabelFor(model => model.Contact, htmlAttributes: new { @class = "control-label col-md-4" }));

            
            #line default
            #line hidden
WriteLiteral("\r\n                                    <div");

WriteLiteral(" class=\"col-md-7 input-group m-b\"");

WriteLiteral(">\r\n                                        <span");

WriteLiteral(" class=\"input-group-addon\"");

WriteLiteral("><i");

WriteLiteral(" class=\"fa fa-user\"");

WriteLiteral("></i></span>\r\n");

WriteLiteral("                                        ");

            
            #line 126 "..\..\Views\Customers\_CTMS003.cshtml"
                                   Write(Html.EditorFor(model => model.Contact, new { htmlAttributes = new { @class = "form-control" } }));

            
            #line default
            #line hidden
WriteLiteral("\r\n                                    </div>\r\n");

WriteLiteral("                                    ");

            
            #line 128 "..\..\Views\Customers\_CTMS003.cshtml"
                               Write(Html.ValidationMessageFor(model => model.Contact, "", new { @class = "text-danger" }));

            
            #line default
            #line hidden
WriteLiteral("\r\n                                </div>\r\n                            </div>\r\n   " +
"                     </div>\r\n                        <div");

WriteLiteral(" class=\"row\"");

WriteLiteral(">\r\n                            <div");

WriteLiteral(" class=\"col-md-6\"");

WriteLiteral(">\r\n                                <div");

WriteLiteral(" class=\"form-group\"");

WriteLiteral(">\r\n");

WriteLiteral("                                    ");

            
            #line 135 "..\..\Views\Customers\_CTMS003.cshtml"
                               Write(Html.LabelFor(model => model.Ship_To, htmlAttributes: new { @class = "control-label col-md-4" }));

            
            #line default
            #line hidden
WriteLiteral("\r\n                                    <div");

WriteLiteral(" class=\"col-md-7 input-group m-b\"");

WriteLiteral(">\r\n                                        <span");

WriteLiteral(" class=\"input-group-addon\"");

WriteLiteral("><i");

WriteLiteral(" class=\"fa fa-truck\"");

WriteLiteral("></i></span>\r\n");

WriteLiteral("                                        ");

            
            #line 138 "..\..\Views\Customers\_CTMS003.cshtml"
                                   Write(Html.EditorFor(model => model.Ship_To, new { htmlAttributes = new { @class = "form-control" } }));

            
            #line default
            #line hidden
WriteLiteral("\r\n                                    </div>\r\n");

WriteLiteral("                                    ");

            
            #line 140 "..\..\Views\Customers\_CTMS003.cshtml"
                               Write(Html.ValidationMessageFor(model => model.Ship_To, "", new { @class = "text-danger" }));

            
            #line default
            #line hidden
WriteLiteral("\r\n                                </div>\r\n                            </div>\r\n");

            
            #line 143 "..\..\Views\Customers\_CTMS003.cshtml"
                            
            
            #line default
            #line hidden
            
            #line 143 "..\..\Views\Customers\_CTMS003.cshtml"
                             if (Model.CanEdit)
                            {

            
            #line default
            #line hidden
WriteLiteral("                                <div");

WriteLiteral(" class=\"col-md-6\"");

WriteLiteral(">\r\n                                    <div");

WriteLiteral(" class=\"form-group\"");

WriteLiteral(">\r\n");

WriteLiteral("                                        ");

            
            #line 147 "..\..\Views\Customers\_CTMS003.cshtml"
                                   Write(Html.LabelFor(model => model.Credit_Limit, htmlAttributes: new { @class = "control-label col-md-4" }));

            
            #line default
            #line hidden
WriteLiteral("\r\n                                        <div");

WriteLiteral(" class=\"col-md-7 input-group m-b\"");

WriteLiteral(">\r\n                                            <span");

WriteLiteral(" class=\"input-group-addon\"");

WriteLiteral("><i");

WriteLiteral(" class=\"pe-7s-cash\"");

WriteLiteral("></i></span>\r\n");

WriteLiteral("                                            ");

            
            #line 150 "..\..\Views\Customers\_CTMS003.cshtml"
                                       Write(Html.EditorFor(model => model.Credit_Limit, new { htmlAttributes = new { @class = "form-control" } }));

            
            #line default
            #line hidden
WriteLiteral("\r\n                                        </div>\r\n");

WriteLiteral("                                        ");

            
            #line 152 "..\..\Views\Customers\_CTMS003.cshtml"
                                   Write(Html.ValidationMessageFor(model => model.Credit_Limit, "", new { @class = "text-danger" }));

            
            #line default
            #line hidden
WriteLiteral("\r\n                                    </div>\r\n                                </d" +
"iv>\r\n");

            
            #line 155 "..\..\Views\Customers\_CTMS003.cshtml"
                            }

            
            #line default
            #line hidden
WriteLiteral("                        </div>\r\n");

            
            #line 157 "..\..\Views\Customers\_CTMS003.cshtml"
                        
            
            #line default
            #line hidden
            
            #line 157 "..\..\Views\Customers\_CTMS003.cshtml"
                         if (Model.CanEdit)
                        {

            
            #line default
            #line hidden
WriteLiteral("                            <div");

WriteLiteral(" class=\"row\"");

WriteLiteral(">\r\n                                <div");

WriteLiteral(" class=\"col-md-6\"");

WriteLiteral(">\r\n                                    <div");

WriteLiteral(" class=\"form-group\"");

WriteLiteral(">\r\n");

WriteLiteral("                                        ");

            
            #line 162 "..\..\Views\Customers\_CTMS003.cshtml"
                                   Write(Html.LabelFor(model => model.Credit_Limit_Alt, htmlAttributes: new { @class = "control-label col-md-4" }));

            
            #line default
            #line hidden
WriteLiteral("\r\n                                        <div");

WriteLiteral(" class=\"col-md-7 input-group m-b\"");

WriteLiteral(">\r\n                                            <span");

WriteLiteral(" class=\"input-group-addon\"");

WriteLiteral("><i");

WriteLiteral(" class=\"pe-7s-cash\"");

WriteLiteral("></i></span>\r\n");

WriteLiteral("                                            ");

            
            #line 165 "..\..\Views\Customers\_CTMS003.cshtml"
                                       Write(Html.EditorFor(model => model.Credit_Limit_Alt, new { htmlAttributes = new { @class = "form-control" } }));

            
            #line default
            #line hidden
WriteLiteral("\r\n                                        </div>\r\n");

WriteLiteral("                                        ");

            
            #line 167 "..\..\Views\Customers\_CTMS003.cshtml"
                                   Write(Html.ValidationMessageFor(model => model.Credit_Limit_Alt, "", new { @class = "text-danger" }));

            
            #line default
            #line hidden
WriteLiteral("\r\n                                    </div>\r\n                                </d" +
"iv>\r\n                                <div");

WriteLiteral(" class=\"col-md-6\"");

WriteLiteral(">\r\n                                    <div");

WriteLiteral(" class=\"form-group\"");

WriteLiteral(">\r\n");

WriteLiteral("                                        ");

            
            #line 172 "..\..\Views\Customers\_CTMS003.cshtml"
                                   Write(Html.LabelFor(model => model.Credit_Days, htmlAttributes: new { @class = "control-label col-md-4" }));

            
            #line default
            #line hidden
WriteLiteral("\r\n                                        <div");

WriteLiteral(" class=\"col-md-7 input-group m-b\"");

WriteLiteral(">\r\n                                            <span");

WriteLiteral(" class=\"input-group-addon\"");

WriteLiteral("><i");

WriteLiteral(" class=\"pe-7s-cash\"");

WriteLiteral("></i></span>\r\n");

WriteLiteral("                                            ");

            
            #line 175 "..\..\Views\Customers\_CTMS003.cshtml"
                                       Write(Html.EditorFor(model => model.Credit_Days, new { htmlAttributes = new { @class = "form-control" } }));

            
            #line default
            #line hidden
WriteLiteral("\r\n                                        </div>\r\n");

WriteLiteral("                                        ");

            
            #line 177 "..\..\Views\Customers\_CTMS003.cshtml"
                                   Write(Html.ValidationMessageFor(model => model.Credit_Days, "", new { @class = "text-danger" }));

            
            #line default
            #line hidden
WriteLiteral("\r\n                                    </div>\r\n                                </d" +
"iv>\r\n                            </div>\r\n");

            
            #line 181 "..\..\Views\Customers\_CTMS003.cshtml"
                        }

            
            #line default
            #line hidden
WriteLiteral("                        <div");

WriteLiteral(" class=\"row\"");

WriteLiteral(">\r\n                            <div");

WriteLiteral(" class=\"col-md-6\"");

WriteLiteral(">\r\n                                <div");

WriteLiteral(" class=\"form-group\"");

WriteLiteral(">\r\n");

WriteLiteral("                                    ");

            
            #line 185 "..\..\Views\Customers\_CTMS003.cshtml"
                               Write(Html.LabelFor(model => model.Status, htmlAttributes: new { @class = "control-label col-md-4" }));

            
            #line default
            #line hidden
WriteLiteral("\r\n                                    <div");

WriteLiteral(" class=\"col-md-7 icheckbox_minimal-green\"");

WriteLiteral(">\r\n");

WriteLiteral("                                        ");

            
            #line 187 "..\..\Views\Customers\_CTMS003.cshtml"
                                   Write(Html.CheckBoxFor(model => model.Status, new { htmlAttributes = new { @class = "form-control" } }));

            
            #line default
            #line hidden
WriteLiteral("\r\n                                    </div>\r\n                                </d" +
"iv>\r\n                            </div>\r\n                        </div>\r\n       " +
"             </div>\r\n");

WriteLiteral("                    <div");

WriteLiteral(" class=\"form-group col-lg-12\"");

WriteLiteral(">\r\n                        <div");

WriteLiteral(" class=\"text-right\"");

WriteLiteral(">\r\n                            <button");

WriteLiteral(" type=\"button\"");

WriteLiteral(" class=\"btn btn-danger\"");

WriteLiteral(" data-dismiss=\"modal\"");

WriteLiteral(">Cancelar</button>\r\n                            <input");

WriteLiteral(" type=\"button\"");

WriteLiteral(" value=\"Guardar\"");

WriteLiteral(" class=\"btn btn-success\"");

WriteLiteral(" id=\"SaveEditCustomer\"");

WriteLiteral(" onclick=\"SaveChanges()\"");

WriteLiteral(" />\r\n                        </div>\r\n                    </div>\r\n");

WriteLiteral("                    <br>");

WriteLiteral("<br>\r\n");

            
            #line 200 "..\..\Views\Customers\_CTMS003.cshtml"
                }

            
            #line default
            #line hidden
WriteLiteral("            </div>\r\n        </div>\r\n    </div>\r\n</div>\r\n");

        }
    }
}
#pragma warning restore 1591
