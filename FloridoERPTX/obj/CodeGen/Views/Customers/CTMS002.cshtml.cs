#pragma warning disable 1591
//------------------------------------------------------------------------------
// <auto-generated>
//     Este código fue generado por una herramienta.
//     Versión de runtime:4.0.30319.42000
//
//     Los cambios en este archivo podrían causar un comportamiento incorrecto y se perderán si
//     se vuelve a generar el código.
// </auto-generated>
//------------------------------------------------------------------------------

namespace ASP
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using System.Net;
    using System.Text;
    using System.Web;
    using System.Web.Helpers;
    using System.Web.Mvc;
    using System.Web.Mvc.Ajax;
    using System.Web.Mvc.Html;
    using System.Web.Optimization;
    using System.Web.Routing;
    using System.Web.Security;
    using System.Web.UI;
    using System.Web.WebPages;
    using DevExpress.Utils;
    using DevExpress.Web;
    using DevExpress.Web.ASPxThemes;
    using DevExpress.Web.Mvc;
    using DevExpress.Web.Mvc.UI;
    using DevExpress.XtraReports;
    using DevExpress.XtraReports.UI;
    using DevExpress.XtraReports.Web;
    using DevExpress.XtraReports.Web.DocumentViewer;
    using FloridoERPTX;
    
    [System.CodeDom.Compiler.GeneratedCodeAttribute("RazorGenerator", "2.0.0.0")]
    [System.Web.WebPages.PageVirtualPathAttribute("~/Views/Customers/CTMS002.cshtml")]
    public partial class _Views_Customers_CTMS002_cshtml : System.Web.Mvc.WebViewPage<App.Entities.ViewModels.Customers.CustomerViewModel>
    {
        public _Views_Customers_CTMS002_cshtml()
        {
        }
        public override void Execute()
        {
            
            #line 3 "..\..\Views\Customers\CTMS002.cshtml"
  
    ViewBag.Title = "Agregar Cliente";
    Layout = "~/Views/Shared/_Layout.cshtml";

            
            #line default
            #line hidden
WriteLiteral("\r\n\r\n\r\n");

WriteLiteral("\r\n<div");

WriteLiteral(" class=\"normalheader transition animated fadeIn\"");

WriteLiteral(">\r\n    <div");

WriteLiteral(" class=\"hpanel\"");

WriteLiteral(">\r\n        <div");

WriteLiteral(" class=\"panel-body\"");

WriteLiteral(">\r\n            <a");

WriteLiteral(" class=\"small-header-action\"");

WriteLiteral(" href=\"\"");

WriteLiteral(">\r\n                <div");

WriteLiteral(" class=\"clip-header\"");

WriteLiteral(">\r\n                    <i");

WriteLiteral(" id=\"fa\"");

WriteLiteral(" class=\"fa fa-arrow-up\"");

WriteLiteral("></i>\r\n                </div>\r\n            </a>\r\n            <div");

WriteLiteral(" id=\"hbreadcrumb\"");

WriteLiteral(" class=\"pull-right m-t-lg\"");

WriteLiteral(">\r\n                <ol");

WriteLiteral(" class=\"hbreadcrumb breadcrumb\"");

WriteLiteral(">\r\n                    <li><a");

WriteAttribute("href", Tuple.Create(" href=\"", 630), Tuple.Create("\"", 671)
            
            #line 20 "..\..\Views\Customers\CTMS002.cshtml"
, Tuple.Create(Tuple.Create("", 637), Tuple.Create<System.Object, System.Int32>(Url.Action("CTMS000","Customers")
            
            #line default
            #line hidden
, 637), false)
);

WriteLiteral(">Clientes</a></li>\r\n                    <li><a");

WriteAttribute("href", Tuple.Create(" href=\"", 718), Tuple.Create("\"", 759)
            
            #line 21 "..\..\Views\Customers\CTMS002.cshtml"
, Tuple.Create(Tuple.Create("", 725), Tuple.Create<System.Object, System.Int32>(Url.Action("CTMS001","Customers")
            
            #line default
            #line hidden
, 725), false)
);

WriteLiteral(">Movimientos</a></li>\r\n                    <li");

WriteLiteral(" class=\"active\"");

WriteLiteral(">\r\n                        <span>Agregar</span>\r\n                    </li>\r\n     " +
"           </ol>\r\n            </div>\r\n            <h2");

WriteLiteral(" class=\"font-light m-b-xs\"");

WriteLiteral(">\r\n                Agregar Cliente\r\n            </h2>\r\n            <small>Módulo " +
"para registrar un nuevo cliente</small><br />\r\n        </div>\r\n    </div>\r\n</div" +
">\r\n\r\n<div");

WriteLiteral(" class=\"content animated-panel\"");

WriteLiteral(">\r\n    <div");

WriteLiteral(" class=\"hpanel hblue\"");

WriteLiteral(">\r\n");

            
            #line 37 "..\..\Views\Customers\CTMS002.cshtml"
        
            
            #line default
            #line hidden
            
            #line 37 "..\..\Views\Customers\CTMS002.cshtml"
         using (Html.BeginForm("CTMS002", "Customers", FormMethod.Post, new { name = "formCreateClient", id = "formCreateClient" }))
        {
            
            
            #line default
            #line hidden
            
            #line 39 "..\..\Views\Customers\CTMS002.cshtml"
       Write(Html.AntiForgeryToken());

            
            #line default
            #line hidden
            
            #line 39 "..\..\Views\Customers\CTMS002.cshtml"
                                    

            
            #line default
            #line hidden
WriteLiteral("            <div");

WriteLiteral(" class=\"panel-body\"");

WriteLiteral(">\r\n                <div");

WriteLiteral(" class=\"form-horizontal\"");

WriteLiteral(">\r\n");

WriteLiteral("                    ");

            
            #line 42 "..\..\Views\Customers\CTMS002.cshtml"
               Write(Html.ValidationSummary(true, "", new { @class = "text-danger" }));

            
            #line default
            #line hidden
WriteLiteral("\r\n                    <div");

WriteLiteral(" class=\"row\"");

WriteLiteral(">\r\n                        <div");

WriteLiteral(" class=\"form-group col-md-4\"");

WriteLiteral(">\r\n");

WriteLiteral("                            ");

            
            #line 45 "..\..\Views\Customers\CTMS002.cshtml"
                       Write(Html.LabelFor(model => model.Code, htmlAttributes: new { @class = "control-label" }));

            
            #line default
            #line hidden
WriteLiteral("\r\n                            <div");

WriteLiteral(" class=\"col-md-10 input-group m-b\"");

WriteLiteral(">\r\n                                <span");

WriteLiteral(" class=\"input-group-addon\"");

WriteLiteral("><i");

WriteLiteral(" class=\"pe-7s-id\"");

WriteLiteral("></i></span>\r\n");

WriteLiteral("                                ");

            
            #line 48 "..\..\Views\Customers\CTMS002.cshtml"
                           Write(Html.EditorFor(model => model.Code, new { htmlAttributes = new { @class = "form-control", Title = "Número de tarjeta El Florido" } }));

            
            #line default
            #line hidden
WriteLiteral("\r\n                            </div>\r\n");

WriteLiteral("                            ");

            
            #line 50 "..\..\Views\Customers\CTMS002.cshtml"
                       Write(Html.ValidationMessageFor(model => model.Code, "", new { @class = "text-danger" }));

            
            #line default
            #line hidden
WriteLiteral("\r\n                        </div>\r\n                    </div>\r\n                   " +
" <div");

WriteLiteral(" class=\"row\"");

WriteLiteral("></div>\r\n                    <div");

WriteLiteral(" class=\"collapse\"");

WriteLiteral(" id=\"collapseExample\"");

WriteLiteral(">\r\n                        <div");

WriteLiteral(" class=\"row\"");

WriteLiteral(">\r\n                            <div");

WriteLiteral(" class=\"form-group col-md-4\"");

WriteLiteral(">\r\n");

WriteLiteral("                                ");

            
            #line 57 "..\..\Views\Customers\CTMS002.cshtml"
                           Write(Html.LabelFor(model => model.Name, htmlAttributes: new { @class = "control-label" }));

            
            #line default
            #line hidden
WriteLiteral("\r\n                                <div");

WriteLiteral(" class=\"col-md-10 input-group m-b\"");

WriteLiteral(">\r\n                                    <span");

WriteLiteral(" class=\"input-group-addon\"");

WriteLiteral("><i");

WriteLiteral(" class=\"fa fa-user-plus\"");

WriteLiteral("></i></span>\r\n");

WriteLiteral("                                    ");

            
            #line 60 "..\..\Views\Customers\CTMS002.cshtml"
                               Write(Html.EditorFor(model => model.Name, new { htmlAttributes = new { @class = "form-control" } }));

            
            #line default
            #line hidden
WriteLiteral("\r\n                                </div>\r\n");

WriteLiteral("                                ");

            
            #line 62 "..\..\Views\Customers\CTMS002.cshtml"
                           Write(Html.ValidationMessageFor(model => model.Name, "", new { @class = "text-danger" }));

            
            #line default
            #line hidden
WriteLiteral("\r\n                            </div>\r\n\r\n                            <div");

WriteLiteral(" class=\"form-group col-md-4\"");

WriteLiteral(">\r\n");

WriteLiteral("                                ");

            
            #line 66 "..\..\Views\Customers\CTMS002.cshtml"
                           Write(Html.LabelFor(model => model.RFC, htmlAttributes: new { @class = "control-label" }));

            
            #line default
            #line hidden
WriteLiteral("\r\n                                <div");

WriteLiteral(" class=\"col-md-10 input-group m-b\"");

WriteLiteral(">\r\n                                    <span");

WriteLiteral(" class=\"input-group-addon\"");

WriteLiteral("><i");

WriteLiteral(" class=\"fa fa-barcode\"");

WriteLiteral("></i></span>\r\n");

WriteLiteral("                                    ");

            
            #line 69 "..\..\Views\Customers\CTMS002.cshtml"
                               Write(Html.EditorFor(model => model.RFC, new { htmlAttributes = new { @class = "form-control", Title = "Primeras letras en mayúsculas" } }));

            
            #line default
            #line hidden
WriteLiteral("\r\n                                </div>\r\n");

WriteLiteral("                                ");

            
            #line 71 "..\..\Views\Customers\CTMS002.cshtml"
                           Write(Html.ValidationMessageFor(model => model.RFC, "", new { @class = "text-danger" }));

            
            #line default
            #line hidden
WriteLiteral("\r\n                            </div>\r\n\r\n\r\n                            <div");

WriteLiteral(" class=\"form-group col-md-4\"");

WriteLiteral(">\r\n");

WriteLiteral("                                ");

            
            #line 76 "..\..\Views\Customers\CTMS002.cshtml"
                           Write(Html.LabelFor(model => model.Phone, htmlAttributes: new { @class = "control-label" }));

            
            #line default
            #line hidden
WriteLiteral("\r\n                                <div");

WriteLiteral(" class=\"col-md-10 input-group m-b\"");

WriteLiteral(">\r\n                                    <span");

WriteLiteral(" class=\"input-group-addon\"");

WriteLiteral("><i");

WriteLiteral(" class=\"fa fa-mobile-phone\"");

WriteLiteral("></i></span>\r\n");

WriteLiteral("                                    ");

            
            #line 79 "..\..\Views\Customers\CTMS002.cshtml"
                               Write(Html.EditorFor(model => model.Phone, new { htmlAttributes = new { @class = "form-control" } }));

            
            #line default
            #line hidden
WriteLiteral("\r\n                                </div>\r\n");

WriteLiteral("                                ");

            
            #line 81 "..\..\Views\Customers\CTMS002.cshtml"
                           Write(Html.ValidationMessageFor(model => model.Phone, "", new { @class = "text-danger" }));

            
            #line default
            #line hidden
WriteLiteral("\r\n                            </div>\r\n                        </div>\r\n           " +
"             <div");

WriteLiteral(" class=\"row\"");

WriteLiteral(">\r\n                            <div");

WriteLiteral(" class=\"form-group col-md-4\"");

WriteLiteral(">\r\n");

WriteLiteral("                                ");

            
            #line 86 "..\..\Views\Customers\CTMS002.cshtml"
                           Write(Html.LabelFor(model => model.StateId, htmlAttributes: new { @class = "control-label" }));

            
            #line default
            #line hidden
WriteLiteral("\r\n                                <div");

WriteLiteral(" class=\"col-md-10 input-group m-b\"");

WriteLiteral(">\r\n                                    <span");

WriteLiteral(" class=\"input-group-addon\"");

WriteLiteral("><i");

WriteLiteral(" class=\"pe-7s-map-marker\"");

WriteLiteral("></i></span>\r\n");

WriteLiteral("                                    ");

            
            #line 89 "..\..\Views\Customers\CTMS002.cshtml"
                               Write(Html.DropDownListFor(model => model.StateId, null, "--Seleccionar Estado--", htmlAttributes: new { @class = "form-control" }));

            
            #line default
            #line hidden
WriteLiteral("\r\n                                </div>\r\n                            </div>\r\n   " +
"                         <div");

WriteLiteral(" class=\"form-group col-md-4\"");

WriteLiteral(">\r\n");

WriteLiteral("                                ");

            
            #line 93 "..\..\Views\Customers\CTMS002.cshtml"
                           Write(Html.LabelFor(model => model.CityId, htmlAttributes: new { @class = "control-label" }));

            
            #line default
            #line hidden
WriteLiteral("\r\n                                <div");

WriteLiteral(" class=\"col-md-10 input-group m-b\"");

WriteLiteral(">\r\n                                    <span");

WriteLiteral(" class=\"input-group-addon\"");

WriteLiteral("><i");

WriteLiteral(" class=\"pe-7s-map-marker\"");

WriteLiteral("></i></span>\r\n");

WriteLiteral("                                    ");

            
            #line 96 "..\..\Views\Customers\CTMS002.cshtml"
                               Write(Html.DropDownListFor(model => model.CityId, null, "--Seleccionar Ciudad o Municipio--", htmlAttributes: new { @class = "form-control" }));

            
            #line default
            #line hidden
WriteLiteral("\r\n                                </div>\r\n                            </div>\r\n   " +
"                         <div");

WriteLiteral(" class=\"form-group col-md-4\"");

WriteLiteral(">\r\n");

WriteLiteral("                                ");

            
            #line 100 "..\..\Views\Customers\CTMS002.cshtml"
                           Write(Html.LabelFor(model => model.Address, htmlAttributes: new { @class = "control-label" }));

            
            #line default
            #line hidden
WriteLiteral("\r\n                                <div");

WriteLiteral(" class=\"col-md-10 input-group m-b\"");

WriteLiteral(">\r\n                                    <span");

WriteLiteral(" class=\"input-group-addon\"");

WriteLiteral("><i");

WriteLiteral(" class=\"pe-7s-map-marker\"");

WriteLiteral("></i></span>\r\n");

WriteLiteral("                                    ");

            
            #line 103 "..\..\Views\Customers\CTMS002.cshtml"
                               Write(Html.EditorFor(model => model.Address, new { htmlAttributes = new { @class = "form-control" } }));

            
            #line default
            #line hidden
WriteLiteral("\r\n                                </div>\r\n");

WriteLiteral("                                ");

            
            #line 105 "..\..\Views\Customers\CTMS002.cshtml"
                           Write(Html.ValidationMessageFor(model => model.Address, "", new { @class = "text-danger" }));

            
            #line default
            #line hidden
WriteLiteral("\r\n                            </div>\r\n                        </div>\r\n           " +
"             <div");

WriteLiteral(" class=\"row\"");

WriteLiteral(">\r\n                            <div");

WriteLiteral(" class=\"form-group col-md-4\"");

WriteLiteral(">\r\n");

WriteLiteral("                                ");

            
            #line 110 "..\..\Views\Customers\CTMS002.cshtml"
                           Write(Html.LabelFor(model => model.ZipCode, htmlAttributes: new { @class = "control-label" }));

            
            #line default
            #line hidden
WriteLiteral("\r\n                                <div");

WriteLiteral(" class=\"col-md-10 input-group m-b\"");

WriteLiteral(">\r\n                                    <span");

WriteLiteral(" class=\"input-group-addon\"");

WriteLiteral("><i");

WriteLiteral(" class=\"fa fa-map-marker\"");

WriteLiteral("></i></span>\r\n");

WriteLiteral("                                    ");

            
            #line 113 "..\..\Views\Customers\CTMS002.cshtml"
                               Write(Html.EditorFor(model => model.ZipCode, new { htmlAttributes = new { @class = "form-control" } }));

            
            #line default
            #line hidden
WriteLiteral("\r\n                                </div>\r\n");

WriteLiteral("                                ");

            
            #line 115 "..\..\Views\Customers\CTMS002.cshtml"
                           Write(Html.ValidationMessageFor(model => model.ZipCode, "", new { @class = "text-danger" }));

            
            #line default
            #line hidden
WriteLiteral("\r\n                            </div>\r\n                            <div");

WriteLiteral(" class=\"form-group col-md-4\"");

WriteLiteral(">\r\n");

WriteLiteral("                                ");

            
            #line 118 "..\..\Views\Customers\CTMS002.cshtml"
                           Write(Html.LabelFor(model => model.Email, htmlAttributes: new { @class = "control-label" }));

            
            #line default
            #line hidden
WriteLiteral("\r\n                                <div");

WriteLiteral(" class=\"col-md-10 input-group m-b\"");

WriteLiteral(">\r\n                                    <span");

WriteLiteral(" class=\"input-group-addon\"");

WriteLiteral("><i");

WriteLiteral(" class=\"pe-7s-mail\"");

WriteLiteral("></i></span>\r\n");

WriteLiteral("                                    ");

            
            #line 121 "..\..\Views\Customers\CTMS002.cshtml"
                               Write(Html.EditorFor(model => model.Email, new { htmlAttributes = new { @class = "form-control" } }));

            
            #line default
            #line hidden
WriteLiteral("\r\n                                </div>\r\n");

WriteLiteral("                                ");

            
            #line 123 "..\..\Views\Customers\CTMS002.cshtml"
                           Write(Html.ValidationMessageFor(model => model.Email, "", new { @class = "text-danger" }));

            
            #line default
            #line hidden
WriteLiteral("\r\n                            </div>\r\n                            <div");

WriteLiteral(" class=\"form-group col-md-4\"");

WriteLiteral(">\r\n");

WriteLiteral("                                ");

            
            #line 126 "..\..\Views\Customers\CTMS002.cshtml"
                           Write(Html.LabelFor(model => model.Ship_To, htmlAttributes: new { @class = "control-label" }));

            
            #line default
            #line hidden
WriteLiteral("\r\n                                <div");

WriteLiteral(" class=\"col-md-10 input-group m-b\"");

WriteLiteral(">\r\n                                    <span");

WriteLiteral(" class=\"input-group-addon\"");

WriteLiteral("><i");

WriteLiteral(" class=\"fa fa-truck\"");

WriteLiteral("></i></span>\r\n");

WriteLiteral("                                    ");

            
            #line 129 "..\..\Views\Customers\CTMS002.cshtml"
                               Write(Html.EditorFor(model => model.Ship_To, new { htmlAttributes = new { @class = "form-control" } }));

            
            #line default
            #line hidden
WriteLiteral("\r\n                                </div>\r\n");

WriteLiteral("                                ");

            
            #line 131 "..\..\Views\Customers\CTMS002.cshtml"
                           Write(Html.ValidationMessageFor(model => model.Ship_To, "", new { @class = "text-danger" }));

            
            #line default
            #line hidden
WriteLiteral("\r\n                            </div>\r\n                        </div>\r\n           " +
"             <div");

WriteLiteral(" class=\"row\"");

WriteLiteral(">\r\n                            <div");

WriteLiteral(" class=\"form-group col-md-4\"");

WriteLiteral(">\r\n");

WriteLiteral("                                ");

            
            #line 136 "..\..\Views\Customers\CTMS002.cshtml"
                           Write(Html.LabelFor(model => model.Contact, htmlAttributes: new { @class = "control-label" }));

            
            #line default
            #line hidden
WriteLiteral("\r\n                                <div");

WriteLiteral(" class=\"col-md-10 input-group m-b\"");

WriteLiteral(">\r\n                                    <span");

WriteLiteral(" class=\"input-group-addon\"");

WriteLiteral("><i");

WriteLiteral(" class=\"fa fa-user\"");

WriteLiteral("></i></span>\r\n");

WriteLiteral("                                    ");

            
            #line 139 "..\..\Views\Customers\CTMS002.cshtml"
                               Write(Html.EditorFor(model => model.Contact, new { htmlAttributes = new { @class = "form-control" } }));

            
            #line default
            #line hidden
WriteLiteral("\r\n                                </div>\r\n");

WriteLiteral("                                ");

            
            #line 141 "..\..\Views\Customers\CTMS002.cshtml"
                           Write(Html.ValidationMessageFor(model => model.Contact, "", new { @class = "text-danger" }));

            
            #line default
            #line hidden
WriteLiteral("\r\n                            </div>\r\n");

            
            #line 143 "..\..\Views\Customers\CTMS002.cshtml"
                            
            
            #line default
            #line hidden
            
            #line 143 "..\..\Views\Customers\CTMS002.cshtml"
                             if (Model.CanEdit)
                            {

            
            #line default
            #line hidden
WriteLiteral("                                <div");

WriteLiteral(" class=\"form-group col-md-4\"");

WriteLiteral(">\r\n");

WriteLiteral("                                    ");

            
            #line 146 "..\..\Views\Customers\CTMS002.cshtml"
                               Write(Html.LabelFor(model => model.Credit_Limit, htmlAttributes: new { @class = "control-label" }));

            
            #line default
            #line hidden
WriteLiteral("\r\n                                    <div");

WriteLiteral(" class=\"col-md-10 input-group m-b\"");

WriteLiteral(">\r\n                                        <span");

WriteLiteral(" class=\"input-group-addon\"");

WriteLiteral("><i");

WriteLiteral(" class=\"pe-7s-cash\"");

WriteLiteral("></i></span>\r\n");

WriteLiteral("                                        ");

            
            #line 149 "..\..\Views\Customers\CTMS002.cshtml"
                                   Write(Html.EditorFor(model => model.Credit_Limit, new { htmlAttributes = new { @class = "form-control" } }));

            
            #line default
            #line hidden
WriteLiteral("\r\n                                    </div>\r\n");

WriteLiteral("                                    ");

            
            #line 151 "..\..\Views\Customers\CTMS002.cshtml"
                               Write(Html.ValidationMessageFor(model => model.Credit_Limit, "", new { @class = "text-danger" }));

            
            #line default
            #line hidden
WriteLiteral("\r\n                                </div>\r\n");

WriteLiteral("                                <div");

WriteLiteral(" class=\"form-group col-md-4\"");

WriteLiteral(">\r\n");

WriteLiteral("                                    ");

            
            #line 154 "..\..\Views\Customers\CTMS002.cshtml"
                               Write(Html.LabelFor(model => model.Credit_Limit_Alt, htmlAttributes: new { @class = "control-label" }));

            
            #line default
            #line hidden
WriteLiteral("\r\n                                    <div");

WriteLiteral(" class=\"col-md-10 input-group m-b\"");

WriteLiteral(">\r\n                                        <span");

WriteLiteral(" class=\"input-group-addon\"");

WriteLiteral("><i");

WriteLiteral(" class=\"pe-7s-cash\"");

WriteLiteral("></i></span>\r\n");

WriteLiteral("                                        ");

            
            #line 157 "..\..\Views\Customers\CTMS002.cshtml"
                                   Write(Html.EditorFor(model => model.Credit_Limit_Alt, new { htmlAttributes = new { @class = "form-control" } }));

            
            #line default
            #line hidden
WriteLiteral("\r\n                                    </div>\r\n");

WriteLiteral("                                    ");

            
            #line 159 "..\..\Views\Customers\CTMS002.cshtml"
                               Write(Html.ValidationMessageFor(model => model.Credit_Limit_Alt, "", new { @class = "text-danger" }));

            
            #line default
            #line hidden
WriteLiteral("\r\n                                </div>\r\n");

            
            #line 161 "..\..\Views\Customers\CTMS002.cshtml"
                            }

            
            #line default
            #line hidden
WriteLiteral("                        </div>\r\n                        <div");

WriteLiteral(" class=\"row\"");

WriteLiteral(">\r\n");

            
            #line 164 "..\..\Views\Customers\CTMS002.cshtml"
                            
            
            #line default
            #line hidden
            
            #line 164 "..\..\Views\Customers\CTMS002.cshtml"
                             if (Model.CanEdit)
                            {

            
            #line default
            #line hidden
WriteLiteral("                                <div");

WriteLiteral(" class=\"form-group col-md-4\"");

WriteLiteral(">\r\n");

WriteLiteral("                                    ");

            
            #line 167 "..\..\Views\Customers\CTMS002.cshtml"
                               Write(Html.LabelFor(model => model.Credit_Days, htmlAttributes: new { @class = "control-label" }));

            
            #line default
            #line hidden
WriteLiteral("\r\n                                    <div");

WriteLiteral(" class=\"col-md-10 input-group m-b\"");

WriteLiteral(">\r\n                                        <span");

WriteLiteral(" class=\"input-group-addon\"");

WriteLiteral("><i");

WriteLiteral(" class=\"pe-7s-cash\"");

WriteLiteral("></i></span>\r\n");

WriteLiteral("                                        ");

            
            #line 170 "..\..\Views\Customers\CTMS002.cshtml"
                                   Write(Html.EditorFor(model => model.Credit_Days, new { htmlAttributes = new { @class = "form-control" } }));

            
            #line default
            #line hidden
WriteLiteral("\r\n                                    </div>\r\n");

WriteLiteral("                                    ");

            
            #line 172 "..\..\Views\Customers\CTMS002.cshtml"
                               Write(Html.ValidationMessageFor(model => model.Credit_Days, "", new { @class = "text-danger" }));

            
            #line default
            #line hidden
WriteLiteral("\r\n                                </div>\r\n");

            
            #line 174 "..\..\Views\Customers\CTMS002.cshtml"
                            }

            
            #line default
            #line hidden
WriteLiteral("\r\n                        </div>\r\n\r\n                        <div");

WriteLiteral(" class=\"col-lg-12\"");

WriteLiteral(" style=\"\"");

WriteLiteral(">\r\n                            <div");

WriteLiteral(" class=\"form-group col-lg-12\"");

WriteLiteral(">\r\n                                <div");

WriteLiteral(" class=\"text-right\"");

WriteLiteral(">\r\n                                    <button");

WriteLiteral(" type=\"button\"");

WriteLiteral(" class=\"btn btn-danger\"");

WriteLiteral(" onclick=\"ResetForm()\"");

WriteLiteral(">Cancelar</button>\r\n                                    <button");

WriteLiteral(" type=\"button\"");

WriteLiteral(" class=\"btn btn-success\"");

WriteLiteral(" id=\"CreateCustomer\"");

WriteLiteral(">Guardar</button>\r\n                                </div>\r\n                      " +
"      </div>\r\n                        </div>\r\n                    </div>\r\n      " +
"          </div>\r\n            </div>\r\n");

            
            #line 189 "..\..\Views\Customers\CTMS002.cshtml"
        }

            
            #line default
            #line hidden
WriteLiteral("    </div>\r\n</div>\r\n\r\n\r\n\r\n");

DefineSection("Scripts", () => {

WriteLiteral("\r\n    <script");

WriteAttribute("src", Tuple.Create(" src=\"", 12738), Tuple.Create("\"", 12776)
, Tuple.Create(Tuple.Create("", 12744), Tuple.Create<System.Object, System.Int32>(Href("~/Scripts/Costumers/Costumers.js")
, 12744), false)
);

WriteLiteral("></script>\r\n    <script");

WriteAttribute("src", Tuple.Create(" src=\"", 12800), Tuple.Create("\"", 12834)
, Tuple.Create(Tuple.Create("", 12806), Tuple.Create<System.Object, System.Int32>(Href("~/Scripts/jquery.validate.js")
, 12806), false)
);

WriteLiteral("></script>\r\n    <script");

WriteLiteral(" type=\"text/javascript\"");

WriteLiteral(">\r\n        $(document).ready(function () {\r\n\r\n            $(\"#StateId\").change(fu" +
"nction () {\r\n                var url = \'");

            
            #line 202 "..\..\Views\Customers\CTMS002.cshtml"
                      Write(Url.Action("ActionGetCityListBasedOnStateId","Customers"));

            
            #line default
            #line hidden
WriteLiteral("\';\r\n                CallCities(url);\r\n            });\r\n\r\n            $(\'.row\').cs" +
"s({ \'margin-left\': \'0px\' });\r\n            $(\'.row\').css({ \'margin-right\': \'15px\'" +
" });\r\n\r\n\r\n\r\n            crateFormClient = $(\"#formCreateClient\").validate({\r\n   " +
"             errorClass: \'help-block animation-slideDown\', // You can change the" +
" animation class for a different entrance animation - check animations page\r\n   " +
"             errorElement: \"span\",\r\n                errorPlacement: function (er" +
"ror, element) {\r\n                    $(element)\r\n                            .cl" +
"osest(\"form\")\r\n                            .find(\"span[data-valmsg-for=\'\" + elem" +
"ent.attr(\"id\") + \"\']\")\r\n                            .append(error);\r\n           " +
"     },\r\n                highlight: function (e) {\r\n                    $(\"#Crea" +
"teCustomer\").attr(\"disabled\", \"disabled\");\r\n                    $(e).closest(\'.f" +
"orm-group\').removeClass(\'has-success has-error\').addClass(\'has-error\');\r\n       " +
"             $(e).closest(\'.help-block\').remove();\r\n                },\r\n        " +
"        success: function (e) {\r\n                    $(\"#collapseExample\").show(" +
"\"show\");\r\n                    $(\"#CreateCustomer\").removeAttr(\"disabled\");\r\n    " +
"                e.closest(\'.form-group\').removeClass(\'has-success has-error\');\r\n" +
"                    e.closest(\'.help-block\').remove();\r\n                },\r\n    " +
"            ignoreTitle: true,\r\n                rules: {\r\n                    RF" +
"C: {\r\n                        required: true,\r\n                        minlength" +
": 12,\r\n                        maxlength: 13,\r\n                        rfc: true" +
",\r\n                        ");

WriteLiteral(@"
                    },

                    Code: {
                        digits: true,
                        required: true,
                        maxlength: 11,
                        remote: function () {
                            var codeData = $('#Code').val();
                            var exist = {
                                url: """);

            
            #line 267 "..\..\Views\Customers\CTMS002.cshtml"
                                  Write(Url.Action("ActionExistCode", "Customers"));

            
            #line default
            #line hidden
WriteLiteral("\",\r\n                                type: \"post\",\r\n                              " +
"  data: { \"Code\": codeData },\r\n                                dataFilter: funct" +
"ion (response) {\r\n                                    if (response === \"False\")\r" +
"\n                                        return false;\r\n                        " +
"            else\r\n                                        return true;\r\n        " +
"                        }\r\n                            };\r\n                     " +
"       return exist;\r\n                        }\r\n                    },\r\n\r\n     " +
"               Name: {\r\n                        required: true\r\n                " +
"    },\r\n\r\n                    ZipCode: {\r\n                        digits: true\r\n" +
"                    },\r\n\r\n                    Email: {\r\n                        " +
"email: true\r\n                    },\r\n\r\n                    Phone: {\r\n           " +
"             number: true\r\n                    },\r\n\r\n                    Credit_" +
"Limit: {\r\n                        number: true\r\n                    },\r\n\r\n      " +
"              Credit_Limit_Alt: {\r\n                        number: true\r\n       " +
"             }\r\n                },\r\n                messages:\r\n                {" +
"\r\n                    RFC: {\r\n                        required: function () { re" +
"turn \"RFC requerido\"; },\r\n                        minlength: function () { retur" +
"n \"RFC debe ser de 12 o 13 caracteres\"; },\r\n                        maxlength: f" +
"unction () { return \"RFC debe ser de 12 o 13 caracteres\"; },\r\n                  " +
"      rfc: function () { return \"Formato RFC inválido\"; }//,\r\n                  " +
"      // remote: jQuery.validator.format(\"RFC {0} ya esta registrado en el siste" +
"ma.\")\r\n                    },\r\n                    Code: {\r\n                    " +
"    remote: jQuery.validator.format(\"No. Tarjeta {0} ya esta registrado en el si" +
"stema.\"),\r\n                        maxlength: \"El No de tarjeta no debe ser mayo" +
"r a 11 dígitos.\"\r\n                    },\r\n                },\r\n                su" +
"bmitHandler: function (form) {\r\n                    form.submit();\r\n            " +
"    }\r\n            })\r\n        });\r\n\r\n        $(\"#Code\").on(\'keydown\', function " +
"(e) {\r\n            if (e.which == 13) {\r\n                $(\"#formCreateClient\")." +
"validate().element(\"#Code\");\r\n            }\r\n        })\r\n        $(\"#CreateCusto" +
"mer\").on(\'click\', function (e) {\r\n            var code = $(\"#formCreateClient\")." +
"validate().element(\"#RFC\");\r\n            var name = $(\"#formCreateClient\").valid" +
"ate().element(\"#Name\");\r\n            if (code && name) {\r\n                $.post" +
"({\r\n                    url: \'");

            
            #line 335 "..\..\Views\Customers\CTMS002.cshtml"
                     Write(Url.Action("ActionCreateCustomer", "Customers"));

            
            #line default
            #line hidden
WriteLiteral(@"',
                    dataType: 'json',
                    Type: 'POST',
                    data: $(""#formCreateClient"").serialize(),
                    success: function (data) {
                        swal({
                            title: 'Cliente',
                            text: ""Cliente creado exitosamente."",
                            type: ""success""
                        }, function () {
                            window.location.reload(true);
                        });
                    }
                });
            }
        });

        function ResetForm() {
            window.location.reload(true);
        }

    </script>
");

});

        }
    }
}
#pragma warning restore 1591
