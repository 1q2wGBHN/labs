#pragma warning disable 1591
//------------------------------------------------------------------------------
// <auto-generated>
//     Este código fue generado por una herramienta.
//     Versión de runtime:4.0.30319.42000
//
//     Los cambios en este archivo podrían causar un comportamiento incorrecto y se perderán si
//     se vuelve a generar el código.
// </auto-generated>
//------------------------------------------------------------------------------

namespace ASP
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using System.Net;
    using System.Text;
    using System.Web;
    using System.Web.Helpers;
    using System.Web.Mvc;
    using System.Web.Mvc.Ajax;
    using System.Web.Mvc.Html;
    using System.Web.Optimization;
    using System.Web.Routing;
    using System.Web.Security;
    using System.Web.UI;
    using System.Web.WebPages;
    using DevExpress.Utils;
    using DevExpress.Web;
    using DevExpress.Web.ASPxThemes;
    using DevExpress.Web.Mvc;
    using DevExpress.Web.Mvc.UI;
    using DevExpress.XtraReports;
    using DevExpress.XtraReports.UI;
    using DevExpress.XtraReports.Web;
    using DevExpress.XtraReports.Web.DocumentViewer;
    using FloridoERPTX;
    
    [System.CodeDom.Compiler.GeneratedCodeAttribute("RazorGenerator", "2.0.0.0")]
    [System.Web.WebPages.PageVirtualPathAttribute("~/Views/Items/ITEM002.cshtml")]
    public partial class _Views_Items_ITEM002_cshtml : System.Web.Mvc.WebViewPage<App.Entities.ViewModels.Item.ItemQuickCheckIndex>
    {
        public _Views_Items_ITEM002_cshtml()
        {
        }
        public override void Execute()
        {
            
            #line 2 "..\..\Views\Items\ITEM002.cshtml"
   ViewBag.Title = "ITEM001"; 
            
            #line default
            #line hidden
WriteLiteral("\r\n\r\n<div");

WriteLiteral(" class=\"normalheader transition animated fadeIn\"");

WriteLiteral(">\r\n    <div");

WriteLiteral(" class=\"hpanel\"");

WriteLiteral(">\r\n        <div");

WriteLiteral(" class=\"panel-body\"");

WriteLiteral(">\r\n            <a");

WriteLiteral(" class=\"small-header-action\"");

WriteLiteral(" href=\"\"");

WriteLiteral(">\r\n                <div");

WriteLiteral(" class=\"clip-header\"");

WriteLiteral(">\r\n                    <i");

WriteLiteral(" id=\"fa\"");

WriteLiteral(" class=\"fa fa-arrow-up\"");

WriteLiteral("></i>\r\n                </div>\r\n            </a>\r\n            <div");

WriteLiteral(" id=\"hbreadcrumb\"");

WriteLiteral(" class=\"pull-right m-t-lg\"");

WriteLiteral(">\r\n                <ol");

WriteLiteral(" class=\"hbreadcrumb breadcrumb\"");

WriteLiteral(">\r\n                    <li><a");

WriteAttribute("href", Tuple.Create(" href=\"", 546), Tuple.Create("\"", 583)
            
            #line 14 "..\..\Views\Items\ITEM002.cshtml"
, Tuple.Create(Tuple.Create("", 553), Tuple.Create<System.Object, System.Int32>(Url.Action("SALE000","Sales")
            
            #line default
            #line hidden
, 553), false)
);

WriteLiteral(">Ventas</a></li>\r\n                    <li><a");

WriteAttribute("href", Tuple.Create(" href=\"", 628), Tuple.Create("\"", 665)
            
            #line 15 "..\..\Views\Items\ITEM002.cshtml"
, Tuple.Create(Tuple.Create("", 635), Tuple.Create<System.Object, System.Int32>(Url.Action("SALE004","Sales")
            
            #line default
            #line hidden
, 635), false)
);

WriteLiteral(">Movimientos</a></li>\r\n                    <li");

WriteLiteral(" class=\"active\"");

WriteLiteral(">\r\n                        <span>Cotizaciones/Pedidos</span>\r\n                   " +
" </li>\r\n                </ol>\r\n            </div>\r\n            <h2");

WriteLiteral(" class=\"font-light m-b-xs\"");

WriteLiteral(">\r\n                Consulta Rápida\r\n            </h2>\r\n            <small>Consult" +
"ar Artículos</small><br />\r\n        </div>\r\n    </div>\r\n</div>\r\n\r\n");

            
            #line 29 "..\..\Views\Items\ITEM002.cshtml"
 if (Model.IsFromMenu)
{

            
            #line default
            #line hidden
WriteLiteral("    <div");

WriteLiteral(" class=\"content animated-panel\"");

WriteLiteral(" id=\"SelectClient\"");

WriteLiteral(">\r\n        <div");

WriteLiteral(" class=\"hpanel hblue\"");

WriteLiteral(">\r\n            <div");

WriteLiteral(" class=\"panel-body\"");

WriteLiteral(">\r\n                <form");

WriteLiteral(" id=\"formQuoteClient\"");

WriteAttribute("action", Tuple.Create(" action=\"", 1250), Tuple.Create("\"", 1290)
            
            #line 34 "..\..\Views\Items\ITEM002.cshtml"
, Tuple.Create(Tuple.Create("", 1259), Tuple.Create<System.Object, System.Int32>(Url.Action("ITEM001", "Items")
            
            #line default
            #line hidden
, 1259), false)
);

WriteLiteral(" method=\"post\"");

WriteLiteral(">\r\n");

WriteLiteral("                    ");

            
            #line 35 "..\..\Views\Items\ITEM002.cshtml"
               Write(Html.HiddenFor(model => model.IsFromMenu));

            
            #line default
            #line hidden
WriteLiteral("\r\n                    <div");

WriteLiteral(" class=\"form-group col-md-12\"");

WriteLiteral(" id=\"SearchClient\"");

WriteLiteral(">\r\n");

WriteLiteral("                        ");

            
            #line 37 "..\..\Views\Items\ITEM002.cshtml"
                   Write(Html.LabelFor(model => model.ItemSearch, htmlAttributes: new { @class = "control-label" }));

            
            #line default
            #line hidden
WriteLiteral("\r\n                        <div");

WriteLiteral(" class=\"col-md-10 input-group m-b\"");

WriteLiteral(">\r\n");

WriteLiteral("                            ");

            
            #line 39 "..\..\Views\Items\ITEM002.cshtml"
                       Write(Html.EditorFor(model => model.ItemSearch, new { htmlAttributes = new { @class = "form-control" } }));

            
            #line default
            #line hidden
WriteLiteral("\r\n                        </div>\r\n                    </div>\r\n                   " +
" <div");

WriteLiteral(" class=\"col-lg-12\"");

WriteLiteral(" style=\"\"");

WriteLiteral(">\r\n                        <div");

WriteLiteral(" class=\"form-group col-lg-12\"");

WriteLiteral(">\r\n                            <div");

WriteLiteral(" class=\"text-right\"");

WriteLiteral(">\r\n                                <button");

WriteLiteral(" type=\"submit\"");

WriteLiteral(" class=\"btn btn-success\"");

WriteLiteral(" id=\"SearchItem\"");

WriteLiteral(">Continuar</button>\r\n                            </div>\r\n                        " +
"</div>\r\n                    </div>\r\n                </form>\r\n            </div>\r" +
"\n        </div>\r\n    </div>\r\n");

            
            #line 53 "..\..\Views\Items\ITEM002.cshtml"
}

            
            #line default
            #line hidden
            
            #line 54 "..\..\Views\Items\ITEM002.cshtml"
 if (!Model.IsFromMenu && Model.ItemList.Count > 0)
{

            
            #line default
            #line hidden
WriteLiteral("    <div");

WriteLiteral(" class=\"content animated-panel\"");

WriteLiteral(">\r\n        <div");

WriteLiteral(" class=\"hpanel hblue\"");

WriteLiteral(">\r\n            <div");

WriteLiteral(" class=\"panel-body\"");

WriteLiteral(">\r\n                <div");

WriteLiteral(" class=\"table-responsive m-t\"");

WriteLiteral(">\r\n                    <table");

WriteLiteral(" id=\"ItemTable\"");

WriteLiteral(" class=\"table table-striped table-bordered table-hover\"");

WriteLiteral(" style=\"width:100%\"");

WriteLiteral(@">
                        <thead>
                            <tr>
                                <th>Número de Artículo</th>
                                <th>Descripción</th>
                                <th>Existencia</th>
                                <th>Precio</th>
                                <th>Precio c/IVA</th>
                                <th>Presentación</th>
                                <th>Presentación</th>
                                <th>Presentación</th>
                            </tr>
                        </thead>
                        <tbody>
");

            
            #line 74 "..\..\Views\Items\ITEM002.cshtml"
                            
            
            #line default
            #line hidden
            
            #line 74 "..\..\Views\Items\ITEM002.cshtml"
                             foreach (var item in Model.ItemList)
                            {

            
            #line default
            #line hidden
WriteLiteral("                                <tr>\r\n                                    <td>");

            
            #line 77 "..\..\Views\Items\ITEM002.cshtml"
                                   Write(item.PartNumber);

            
            #line default
            #line hidden
WriteLiteral("</td>\r\n                                    <td>");

            
            #line 78 "..\..\Views\Items\ITEM002.cshtml"
                                   Write(item.Description);

            
            #line default
            #line hidden
WriteLiteral("</td>\r\n                                    <td>");

            
            #line 79 "..\..\Views\Items\ITEM002.cshtml"
                                   Write(item.Stock);

            
            #line default
            #line hidden
WriteLiteral("</td>\r\n                                    <td>");

            
            #line 80 "..\..\Views\Items\ITEM002.cshtml"
                                   Write(item.BasePrice.ToString("C"));

            
            #line default
            #line hidden
WriteLiteral("</td>\r\n                                    <td>");

            
            #line 81 "..\..\Views\Items\ITEM002.cshtml"
                                   Write(item.TaxPrice.ToString("C"));

            
            #line default
            #line hidden
WriteLiteral("</td>\r\n");

            
            #line 82 "..\..\Views\Items\ITEM002.cshtml"
                                    
            
            #line default
            #line hidden
            
            #line 82 "..\..\Views\Items\ITEM002.cshtml"
                                      var p = item.Presentations;
            
            #line default
            #line hidden
WriteLiteral("\r\n");

            
            #line 83 "..\..\Views\Items\ITEM002.cshtml"
                                    
            
            #line default
            #line hidden
            
            #line 83 "..\..\Views\Items\ITEM002.cshtml"
                                     for (var i = 0; i < 3; i++)
                                    {

            
            #line default
            #line hidden
WriteLiteral("                                        <td>\r\n");

            
            #line 86 "..\..\Views\Items\ITEM002.cshtml"
                                            
            
            #line default
            #line hidden
            
            #line 86 "..\..\Views\Items\ITEM002.cshtml"
                                             if (p.Count > i)
                                            {

            
            #line default
            #line hidden
WriteLiteral("                                                <span");

WriteLiteral(" style=\"white-space: nowrap; font-size: 0.85em;\"");

WriteLiteral("> ");

            
            #line 88 "..\..\Views\Items\ITEM002.cshtml"
                                                                                                  Write(p[i].Description);

            
            #line default
            #line hidden
WriteLiteral(" </span>");

WriteLiteral("<br />");

            
            #line 88 "..\..\Views\Items\ITEM002.cshtml"
                                                                                                                                      
            
            #line default
            #line hidden
            
            #line 88 "..\..\Views\Items\ITEM002.cshtml"
                                                                                                                                 Write(p[i].TaxPrice.ToString("C"));

            
            #line default
            #line hidden
            
            #line 88 "..\..\Views\Items\ITEM002.cshtml"
                                                                                                                                                                  
                                            }

            
            #line default
            #line hidden
WriteLiteral("                                        </td>\r\n");

            
            #line 91 "..\..\Views\Items\ITEM002.cshtml"
                                    }

            
            #line default
            #line hidden
WriteLiteral("                                </tr>\r\n");

            
            #line 93 "..\..\Views\Items\ITEM002.cshtml"
                                        }

            
            #line default
            #line hidden
WriteLiteral("                        </tbody>\r\n                    </table>\r\n                <" +
"/div>\r\n            </div>\r\n        </div>\r\n    </div>\r\n");

            
            #line 100 "..\..\Views\Items\ITEM002.cshtml"
                                        }
                                        else if (!Model.IsFromMenu)
                                        {

            
            #line default
            #line hidden
WriteLiteral("                                            <div");

WriteLiteral(" class=\"content animated-panel\"");

WriteLiteral(" id=\"report-section\"");

WriteLiteral(">\r\n                                                <div");

WriteLiteral(" class=\"hpanel hblue\"");

WriteLiteral(">\r\n                                                    <div");

WriteLiteral(" class=\"panel-body\"");

WriteLiteral(">\r\n                                                        <div");

WriteLiteral(" class=\"row\"");

WriteLiteral(">\r\n                                                            <h4");

WriteLiteral(" style=\"text-align:center;color:red;\"");

WriteLiteral(@">No hay información con esos parámetros</h4>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
");

            
            #line 112 "..\..\Views\Items\ITEM002.cshtml"
                                        }

            
            #line default
            #line hidden
WriteLiteral("\r\n");

DefineSection("Scripts", () => {

WriteLiteral(" ");

            
            #line 114 "..\..\Views\Items\ITEM002.cshtml"
              Write(Scripts.Render("~/bundles/ITEM001/js"));

            
            #line default
            #line hidden
WriteLiteral(" ");

});

        }
    }
}
#pragma warning restore 1591
