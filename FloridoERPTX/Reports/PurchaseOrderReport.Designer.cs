﻿namespace FloridoERPTX.Reports
{
    partial class PurchaseOrderReport
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(PurchaseOrderReport));
            this.Detail = new DevExpress.XtraReports.UI.DetailBand();
            this.xrTablePurchase = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow1 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrCellPO = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrCellProvider = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrCellETA = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrCellInvoice = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrCellCurrency = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrCellImport = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrCellIVA = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrCellIEPS = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrCellTotal = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrCellStatus = new DevExpress.XtraReports.UI.XRTableCell();
            this.TopMargin = new DevExpress.XtraReports.UI.TopMarginBand();
            this.BottomMargin = new DevExpress.XtraReports.UI.BottomMarginBand();
            this.PageHeader = new DevExpress.XtraReports.UI.PageHeaderBand();
            this.xrlblEndDate = new DevExpress.XtraReports.UI.XRLabel();
            this.xrlblStartDate = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel2 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel1 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrlblStatus = new DevExpress.XtraReports.UI.XRLabel();
            this.xrlblTotal = new DevExpress.XtraReports.UI.XRLabel();
            this.xrlblIEPS = new DevExpress.XtraReports.UI.XRLabel();
            this.xrlblIVA = new DevExpress.XtraReports.UI.XRLabel();
            this.xrlblImport = new DevExpress.XtraReports.UI.XRLabel();
            this.xrlblCurrency = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel3 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrlblETA = new DevExpress.XtraReports.UI.XRLabel();
            this.xrlblProvider = new DevExpress.XtraReports.UI.XRLabel();
            this.xrlblPO = new DevExpress.XtraReports.UI.XRLabel();
            this.xrDateShow = new DevExpress.XtraReports.UI.XRLabel();
            this.xrlblDate = new DevExpress.XtraReports.UI.XRLabel();
            this.xrPictureBox2 = new DevExpress.XtraReports.UI.XRPictureBox();
            this.xrLabel21 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrlblUser = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel6 = new DevExpress.XtraReports.UI.XRLabel();
            this.siteLabel = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel8 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrlblCreate = new DevExpress.XtraReports.UI.XRLabel();
            this.xraddress = new DevExpress.XtraReports.UI.XRLabel();
            this.xrState = new DevExpress.XtraReports.UI.XRLabel();
            this.GroupFooter1 = new DevExpress.XtraReports.UI.GroupFooterBand();
            this.xrlblUsdImport = new DevExpress.XtraReports.UI.XRLabel();
            this.xrlblMxnImport = new DevExpress.XtraReports.UI.XRLabel();
            this.xrlblMxnTotal = new DevExpress.XtraReports.UI.XRLabel();
            this.xrlblUsdTotal = new DevExpress.XtraReports.UI.XRLabel();
            this.xrlblMxnIEPS = new DevExpress.XtraReports.UI.XRLabel();
            this.xrlblUsdIEPS = new DevExpress.XtraReports.UI.XRLabel();
            this.xrlblUsdIVA = new DevExpress.XtraReports.UI.XRLabel();
            this.xrlblMxnIVA = new DevExpress.XtraReports.UI.XRLabel();
            this.xrlblUSD = new DevExpress.XtraReports.UI.XRLabel();
            this.xrlblMXN = new DevExpress.XtraReports.UI.XRLabel();
            this.xrlblTotalUSD = new DevExpress.XtraReports.UI.XRLabel();
            this.xrlblTotalMXN = new DevExpress.XtraReports.UI.XRLabel();
            this.PageFooter = new DevExpress.XtraReports.UI.PageFooterBand();
            this.xrPageInfo1 = new DevExpress.XtraReports.UI.XRPageInfo();
            this.DetailReport = new DevExpress.XtraReports.UI.DetailReportBand();
            this.Detail1 = new DevExpress.XtraReports.UI.DetailBand();
            ((System.ComponentModel.ISupportInitialize)(this.xrTablePurchase)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // Detail
            // 
            this.Detail.Dpi = 100F;
            this.Detail.HeightF = 0F;
            this.Detail.Name = "Detail";
            this.Detail.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.Detail.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrTablePurchase
            // 
            this.xrTablePurchase.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTablePurchase.Dpi = 100F;
            this.xrTablePurchase.LocationFloat = new DevExpress.Utils.PointFloat(0F, 0F);
            this.xrTablePurchase.Name = "xrTablePurchase";
            this.xrTablePurchase.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow1});
            this.xrTablePurchase.SizeF = new System.Drawing.SizeF(1049F, 50.09805F);
            this.xrTablePurchase.StylePriority.UseBorders = false;
            // 
            // xrTableRow1
            // 
            this.xrTableRow1.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrCellPO,
            this.xrCellProvider,
            this.xrCellETA,
            this.xrCellInvoice,
            this.xrCellCurrency,
            this.xrCellImport,
            this.xrCellIVA,
            this.xrCellIEPS,
            this.xrCellTotal,
            this.xrCellStatus});
            this.xrTableRow1.Dpi = 100F;
            this.xrTableRow1.Name = "xrTableRow1";
            this.xrTableRow1.Weight = 1D;
            // 
            // xrCellPO
            // 
            this.xrCellPO.Dpi = 100F;
            this.xrCellPO.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrCellPO.Name = "xrCellPO";
            this.xrCellPO.StylePriority.UseFont = false;
            this.xrCellPO.StylePriority.UseTextAlignment = false;
            this.xrCellPO.Text = "xrCellPO";
            this.xrCellPO.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrCellPO.Weight = 0.77816393725652366D;
            // 
            // xrCellProvider
            // 
            this.xrCellProvider.Dpi = 100F;
            this.xrCellProvider.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrCellProvider.Name = "xrCellProvider";
            this.xrCellProvider.StylePriority.UseFont = false;
            this.xrCellProvider.StylePriority.UseTextAlignment = false;
            this.xrCellProvider.Text = "xrCellProvider";
            this.xrCellProvider.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrCellProvider.Weight = 1.5567622919650435D;
            // 
            // xrCellETA
            // 
            this.xrCellETA.Dpi = 100F;
            this.xrCellETA.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrCellETA.Name = "xrCellETA";
            this.xrCellETA.StylePriority.UseFont = false;
            this.xrCellETA.StylePriority.UseTextAlignment = false;
            this.xrCellETA.Text = "xrCellETA";
            this.xrCellETA.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrCellETA.Weight = 0.76543557648008886D;
            // 
            // xrCellInvoice
            // 
            this.xrCellInvoice.Dpi = 100F;
            this.xrCellInvoice.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrCellInvoice.Name = "xrCellInvoice";
            this.xrCellInvoice.StylePriority.UseFont = false;
            this.xrCellInvoice.StylePriority.UseTextAlignment = false;
            this.xrCellInvoice.Text = "xrCellInvoice";
            this.xrCellInvoice.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrCellInvoice.Weight = 1.2124864208856425D;
            // 
            // xrCellCurrency
            // 
            this.xrCellCurrency.Dpi = 100F;
            this.xrCellCurrency.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrCellCurrency.Name = "xrCellCurrency";
            this.xrCellCurrency.StylePriority.UseFont = false;
            this.xrCellCurrency.StylePriority.UseTextAlignment = false;
            this.xrCellCurrency.Text = "xrCellCurrency";
            this.xrCellCurrency.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrCellCurrency.Weight = 0.73189717029233148D;
            // 
            // xrCellImport
            // 
            this.xrCellImport.Dpi = 100F;
            this.xrCellImport.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrCellImport.Name = "xrCellImport";
            this.xrCellImport.StylePriority.UseFont = false;
            this.xrCellImport.StylePriority.UseTextAlignment = false;
            this.xrCellImport.Text = "xrCellImport";
            this.xrCellImport.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrCellImport.Weight = 0.78161843641642326D;
            // 
            // xrCellIVA
            // 
            this.xrCellIVA.Dpi = 100F;
            this.xrCellIVA.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrCellIVA.Name = "xrCellIVA";
            this.xrCellIVA.StylePriority.UseFont = false;
            this.xrCellIVA.StylePriority.UseTextAlignment = false;
            this.xrCellIVA.Text = "xrCellIVA";
            this.xrCellIVA.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrCellIVA.Weight = 0.864413100350411D;
            // 
            // xrCellIEPS
            // 
            this.xrCellIEPS.Dpi = 100F;
            this.xrCellIEPS.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrCellIEPS.Name = "xrCellIEPS";
            this.xrCellIEPS.StylePriority.UseFont = false;
            this.xrCellIEPS.StylePriority.UseTextAlignment = false;
            this.xrCellIEPS.Text = "xrCellIEPS";
            this.xrCellIEPS.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrCellIEPS.Weight = 0.98553691556562162D;
            // 
            // xrCellTotal
            // 
            this.xrCellTotal.Dpi = 100F;
            this.xrCellTotal.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrCellTotal.Name = "xrCellTotal";
            this.xrCellTotal.StylePriority.UseFont = false;
            this.xrCellTotal.StylePriority.UseTextAlignment = false;
            this.xrCellTotal.Text = "xrCellTotal";
            this.xrCellTotal.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrCellTotal.Weight = 0.949708617877022D;
            // 
            // xrCellStatus
            // 
            this.xrCellStatus.Dpi = 100F;
            this.xrCellStatus.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrCellStatus.Name = "xrCellStatus";
            this.xrCellStatus.StylePriority.UseFont = false;
            this.xrCellStatus.StylePriority.UseTextAlignment = false;
            this.xrCellStatus.Text = "xrCellStatus";
            this.xrCellStatus.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrCellStatus.Weight = 0.80390947003884172D;
            // 
            // TopMargin
            // 
            this.TopMargin.Dpi = 100F;
            this.TopMargin.HeightF = 11F;
            this.TopMargin.Name = "TopMargin";
            this.TopMargin.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.TopMargin.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // BottomMargin
            // 
            this.BottomMargin.Dpi = 100F;
            this.BottomMargin.HeightF = 29.40456F;
            this.BottomMargin.Name = "BottomMargin";
            this.BottomMargin.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.BottomMargin.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // PageHeader
            // 
            this.PageHeader.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrlblEndDate,
            this.xrlblStartDate,
            this.xrLabel2,
            this.xrLabel1,
            this.xrlblStatus,
            this.xrlblTotal,
            this.xrlblIEPS,
            this.xrlblIVA,
            this.xrlblImport,
            this.xrlblCurrency,
            this.xrLabel3,
            this.xrlblETA,
            this.xrlblProvider,
            this.xrlblPO,
            this.xrDateShow,
            this.xrlblDate,
            this.xrPictureBox2,
            this.xrLabel21,
            this.xrlblUser,
            this.xrLabel6,
            this.siteLabel,
            this.xrLabel8,
            this.xrlblCreate,
            this.xraddress,
            this.xrState});
            this.PageHeader.Dpi = 100F;
            this.PageHeader.HeightF = 144.7211F;
            this.PageHeader.Name = "PageHeader";
            // 
            // xrlblEndDate
            // 
            this.xrlblEndDate.Dpi = 100F;
            this.xrlblEndDate.Font = new System.Drawing.Font("Arial", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrlblEndDate.LocationFloat = new DevExpress.Utils.PointFloat(450.7823F, 70F);
            this.xrlblEndDate.Name = "xrlblEndDate";
            this.xrlblEndDate.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrlblEndDate.SizeF = new System.Drawing.SizeF(141.653F, 15F);
            this.xrlblEndDate.StylePriority.UseFont = false;
            // 
            // xrlblStartDate
            // 
            this.xrlblStartDate.Dpi = 100F;
            this.xrlblStartDate.Font = new System.Drawing.Font("Arial", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrlblStartDate.LocationFloat = new DevExpress.Utils.PointFloat(163.228F, 69.99998F);
            this.xrlblStartDate.Name = "xrlblStartDate";
            this.xrlblStartDate.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrlblStartDate.SizeF = new System.Drawing.SizeF(141.653F, 15F);
            this.xrlblStartDate.StylePriority.UseFont = false;
            // 
            // xrLabel2
            // 
            this.xrLabel2.Dpi = 100F;
            this.xrLabel2.Font = new System.Drawing.Font("Arial", 7F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel2.LocationFloat = new DevExpress.Utils.PointFloat(334.038F, 69.99998F);
            this.xrLabel2.Multiline = true;
            this.xrLabel2.Name = "xrLabel2";
            this.xrLabel2.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel2.SizeF = new System.Drawing.SizeF(97.99426F, 15F);
            this.xrLabel2.StylePriority.UseFont = false;
            this.xrLabel2.Text = "Fecha Final:\r\n";
            // 
            // xrLabel1
            // 
            this.xrLabel1.Dpi = 100F;
            this.xrLabel1.Font = new System.Drawing.Font("Arial", 7F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel1.LocationFloat = new DevExpress.Utils.PointFloat(51.40792F, 69.99999F);
            this.xrLabel1.Name = "xrLabel1";
            this.xrLabel1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel1.SizeF = new System.Drawing.SizeF(97.99426F, 15F);
            this.xrLabel1.StylePriority.UseFont = false;
            this.xrLabel1.Text = "Fecha Inicio:";
            // 
            // xrlblStatus
            // 
            this.xrlblStatus.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrlblStatus.Dpi = 100F;
            this.xrlblStatus.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrlblStatus.LocationFloat = new DevExpress.Utils.PointFloat(962.5193F, 121.7211F);
            this.xrlblStatus.Name = "xrlblStatus";
            this.xrlblStatus.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrlblStatus.SizeF = new System.Drawing.SizeF(86.48071F, 22.99999F);
            this.xrlblStatus.StylePriority.UseBorders = false;
            this.xrlblStatus.StylePriority.UseFont = false;
            this.xrlblStatus.StylePriority.UseTextAlignment = false;
            this.xrlblStatus.Text = "Estatus";
            this.xrlblStatus.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrlblTotal
            // 
            this.xrlblTotal.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrlblTotal.Dpi = 100F;
            this.xrlblTotal.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrlblTotal.LocationFloat = new DevExpress.Utils.PointFloat(853.9249F, 121.7211F);
            this.xrlblTotal.Name = "xrlblTotal";
            this.xrlblTotal.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrlblTotal.SizeF = new System.Drawing.SizeF(108.5944F, 22.99998F);
            this.xrlblTotal.StylePriority.UseBorders = false;
            this.xrlblTotal.StylePriority.UseFont = false;
            this.xrlblTotal.StylePriority.UseTextAlignment = false;
            this.xrlblTotal.Text = "Total";
            this.xrlblTotal.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrlblIEPS
            // 
            this.xrlblIEPS.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrlblIEPS.Dpi = 100F;
            this.xrlblIEPS.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrlblIEPS.LocationFloat = new DevExpress.Utils.PointFloat(744.2922F, 121.7211F);
            this.xrlblIEPS.Name = "xrlblIEPS";
            this.xrlblIEPS.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrlblIEPS.SizeF = new System.Drawing.SizeF(109.6326F, 22.99998F);
            this.xrlblIEPS.StylePriority.UseBorders = false;
            this.xrlblIEPS.StylePriority.UseFont = false;
            this.xrlblIEPS.StylePriority.UseTextAlignment = false;
            this.xrlblIEPS.Text = "IEPS";
            this.xrlblIEPS.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrlblIVA
            // 
            this.xrlblIVA.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrlblIVA.Dpi = 100F;
            this.xrlblIVA.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrlblIVA.LocationFloat = new DevExpress.Utils.PointFloat(648.1336F, 121.7211F);
            this.xrlblIVA.Name = "xrlblIVA";
            this.xrlblIVA.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrlblIVA.SizeF = new System.Drawing.SizeF(96.15863F, 22.99998F);
            this.xrlblIVA.StylePriority.UseBorders = false;
            this.xrlblIVA.StylePriority.UseFont = false;
            this.xrlblIVA.StylePriority.UseTextAlignment = false;
            this.xrlblIVA.Text = "IVA";
            this.xrlblIVA.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrlblImport
            // 
            this.xrlblImport.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrlblImport.Dpi = 100F;
            this.xrlblImport.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrlblImport.LocationFloat = new DevExpress.Utils.PointFloat(561.1852F, 121.7211F);
            this.xrlblImport.Name = "xrlblImport";
            this.xrlblImport.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrlblImport.SizeF = new System.Drawing.SizeF(86.94843F, 22.99998F);
            this.xrlblImport.StylePriority.UseBorders = false;
            this.xrlblImport.StylePriority.UseFont = false;
            this.xrlblImport.StylePriority.UseTextAlignment = false;
            this.xrlblImport.Text = "Importe";
            this.xrlblImport.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrlblCurrency
            // 
            this.xrlblCurrency.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrlblCurrency.Dpi = 100F;
            this.xrlblCurrency.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrlblCurrency.LocationFloat = new DevExpress.Utils.PointFloat(479.7678F, 121.7211F);
            this.xrlblCurrency.Name = "xrlblCurrency";
            this.xrlblCurrency.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrlblCurrency.SizeF = new System.Drawing.SizeF(81.41736F, 22.99998F);
            this.xrlblCurrency.StylePriority.UseBorders = false;
            this.xrlblCurrency.StylePriority.UseFont = false;
            this.xrlblCurrency.StylePriority.UseTextAlignment = false;
            this.xrlblCurrency.Text = "Currency";
            this.xrlblCurrency.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrLabel3
            // 
            this.xrLabel3.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel3.Dpi = 100F;
            this.xrLabel3.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel3.LocationFloat = new DevExpress.Utils.PointFloat(344.8891F, 121.7211F);
            this.xrLabel3.Name = "xrLabel3";
            this.xrLabel3.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel3.SizeF = new System.Drawing.SizeF(134.8787F, 22.99998F);
            this.xrLabel3.StylePriority.UseBorders = false;
            this.xrLabel3.StylePriority.UseFont = false;
            this.xrLabel3.StylePriority.UseTextAlignment = false;
            this.xrLabel3.Text = "Factura";
            this.xrLabel3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrlblETA
            // 
            this.xrlblETA.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrlblETA.Dpi = 100F;
            this.xrlblETA.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrlblETA.LocationFloat = new DevExpress.Utils.PointFloat(259.7407F, 121.7211F);
            this.xrlblETA.Name = "xrlblETA";
            this.xrlblETA.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrlblETA.SizeF = new System.Drawing.SizeF(85.14838F, 22.99998F);
            this.xrlblETA.StylePriority.UseBorders = false;
            this.xrlblETA.StylePriority.UseFont = false;
            this.xrlblETA.StylePriority.UseTextAlignment = false;
            this.xrlblETA.Text = "Fecha Recibida";
            this.xrlblETA.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrlblProvider
            // 
            this.xrlblProvider.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrlblProvider.Dpi = 100F;
            this.xrlblProvider.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrlblProvider.LocationFloat = new DevExpress.Utils.PointFloat(86.56407F, 121.7211F);
            this.xrlblProvider.Name = "xrlblProvider";
            this.xrlblProvider.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrlblProvider.SizeF = new System.Drawing.SizeF(173.1767F, 22.99998F);
            this.xrlblProvider.StylePriority.UseBorders = false;
            this.xrlblProvider.StylePriority.UseFont = false;
            this.xrlblProvider.StylePriority.UseTextAlignment = false;
            this.xrlblProvider.Text = "Proveedor";
            this.xrlblProvider.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrlblPO
            // 
            this.xrlblPO.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrlblPO.Dpi = 100F;
            this.xrlblPO.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrlblPO.LocationFloat = new DevExpress.Utils.PointFloat(0F, 121.7211F);
            this.xrlblPO.Name = "xrlblPO";
            this.xrlblPO.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrlblPO.SizeF = new System.Drawing.SizeF(86.56407F, 22.99998F);
            this.xrlblPO.StylePriority.UseBorders = false;
            this.xrlblPO.StylePriority.UseFont = false;
            this.xrlblPO.StylePriority.UseTextAlignment = false;
            this.xrlblPO.Text = "PO";
            this.xrlblPO.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrDateShow
            // 
            this.xrDateShow.Dpi = 100F;
            this.xrDateShow.Font = new System.Drawing.Font("Arial", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrDateShow.LocationFloat = new DevExpress.Utils.PointFloat(163.228F, 96.36314F);
            this.xrDateShow.Name = "xrDateShow";
            this.xrDateShow.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrDateShow.SizeF = new System.Drawing.SizeF(141.653F, 15F);
            this.xrDateShow.StylePriority.UseFont = false;
            // 
            // xrlblDate
            // 
            this.xrlblDate.Dpi = 100F;
            this.xrlblDate.Font = new System.Drawing.Font("Arial", 7F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrlblDate.LocationFloat = new DevExpress.Utils.PointFloat(51.40792F, 96.36314F);
            this.xrlblDate.Name = "xrlblDate";
            this.xrlblDate.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrlblDate.SizeF = new System.Drawing.SizeF(97.99426F, 15F);
            this.xrlblDate.StylePriority.UseFont = false;
            this.xrlblDate.Text = "Fecha Impresión:";
            // 
            // xrPictureBox2
            // 
            this.xrPictureBox2.Dpi = 100F;
            this.xrPictureBox2.Image = ((System.Drawing.Image)(resources.GetObject("xrPictureBox2.Image")));
            this.xrPictureBox2.LocationFloat = new DevExpress.Utils.PointFloat(873.5367F, 10F);
            this.xrPictureBox2.Name = "xrPictureBox2";
            this.xrPictureBox2.SizeF = new System.Drawing.SizeF(116.4632F, 75F);
            this.xrPictureBox2.Sizing = DevExpress.XtraPrinting.ImageSizeMode.ZoomImage;
            // 
            // xrLabel21
            // 
            this.xrLabel21.Dpi = 100F;
            this.xrLabel21.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel21.LocationFloat = new DevExpress.Utils.PointFloat(51.40792F, 5.000001F);
            this.xrLabel21.Name = "xrLabel21";
            this.xrLabel21.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel21.SizeF = new System.Drawing.SizeF(293.4811F, 20F);
            this.xrLabel21.StylePriority.UseBorders = false;
            this.xrLabel21.StylePriority.UseBorderWidth = false;
            this.xrLabel21.StylePriority.UseFont = false;
            this.xrLabel21.StylePriority.UseTextAlignment = false;
            this.xrLabel21.Text = "Reporte de Ordenes de Compra";
            // 
            // xrlblUser
            // 
            this.xrlblUser.Dpi = 100F;
            this.xrlblUser.Font = new System.Drawing.Font("Arial", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrlblUser.LocationFloat = new DevExpress.Utils.PointFloat(163.228F, 39.99998F);
            this.xrlblUser.Name = "xrlblUser";
            this.xrlblUser.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrlblUser.SizeF = new System.Drawing.SizeF(246.3848F, 15F);
            this.xrlblUser.StylePriority.UseFont = false;
            // 
            // xrLabel6
            // 
            this.xrLabel6.Dpi = 100F;
            this.xrLabel6.Font = new System.Drawing.Font("Arial", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel6.LocationFloat = new DevExpress.Utils.PointFloat(638.239F, 10F);
            this.xrLabel6.Name = "xrLabel6";
            this.xrLabel6.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel6.SizeF = new System.Drawing.SizeF(204.1053F, 15F);
            this.xrLabel6.StylePriority.UseFont = false;
            this.xrLabel6.StylePriority.UseTextAlignment = false;
            this.xrLabel6.Text = "DISTRIBUIDORA EL FLORIDO S.A. DE C.V.";
            this.xrLabel6.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // siteLabel
            // 
            this.siteLabel.Dpi = 100F;
            this.siteLabel.Font = new System.Drawing.Font("Arial", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.siteLabel.LocationFloat = new DevExpress.Utils.PointFloat(641.3443F, 24.99999F);
            this.siteLabel.Multiline = true;
            this.siteLabel.Name = "siteLabel";
            this.siteLabel.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.siteLabel.SizeF = new System.Drawing.SizeF(201.0001F, 15F);
            this.siteLabel.StylePriority.UseFont = false;
            this.siteLabel.StylePriority.UseTextAlignment = false;
            this.siteLabel.Text = "*FLORIDO\r\n";
            this.siteLabel.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // xrLabel8
            // 
            this.xrLabel8.Dpi = 100F;
            this.xrLabel8.Font = new System.Drawing.Font("Arial", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel8.LocationFloat = new DevExpress.Utils.PointFloat(755.5186F, 39.99998F);
            this.xrLabel8.Name = "xrLabel8";
            this.xrLabel8.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel8.SizeF = new System.Drawing.SizeF(86.82587F, 15F);
            this.xrLabel8.StylePriority.UseFont = false;
            this.xrLabel8.StylePriority.UseTextAlignment = false;
            this.xrLabel8.Text = "DFL-950802-5N4";
            this.xrLabel8.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // xrlblCreate
            // 
            this.xrlblCreate.Dpi = 100F;
            this.xrlblCreate.Font = new System.Drawing.Font("Arial", 7F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrlblCreate.LocationFloat = new DevExpress.Utils.PointFloat(51.40792F, 39.99998F);
            this.xrlblCreate.Name = "xrlblCreate";
            this.xrlblCreate.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrlblCreate.SizeF = new System.Drawing.SizeF(97.99426F, 15F);
            this.xrlblCreate.StylePriority.UseFont = false;
            this.xrlblCreate.Text = "Realizó:";
            // 
            // xraddress
            // 
            this.xraddress.Dpi = 100F;
            this.xraddress.Font = new System.Drawing.Font("Arial", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xraddress.LocationFloat = new DevExpress.Utils.PointFloat(641.3443F, 54.99997F);
            this.xraddress.Name = "xraddress";
            this.xraddress.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xraddress.SizeF = new System.Drawing.SizeF(201.0001F, 15F);
            this.xraddress.StylePriority.UseFont = false;
            this.xraddress.StylePriority.UseTextAlignment = false;
            this.xraddress.Text = "- Dirección";
            this.xraddress.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // xrState
            // 
            this.xrState.Dpi = 100F;
            this.xrState.Font = new System.Drawing.Font("Arial", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrState.LocationFloat = new DevExpress.Utils.PointFloat(641.344F, 69.99999F);
            this.xrState.Name = "xrState";
            this.xrState.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrState.SizeF = new System.Drawing.SizeF(201.0001F, 15F);
            this.xrState.StylePriority.UseFont = false;
            this.xrState.StylePriority.UseTextAlignment = false;
            this.xrState.Text = "- Estado";
            this.xrState.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // GroupFooter1
            // 
            this.GroupFooter1.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrlblUsdImport,
            this.xrlblMxnImport,
            this.xrlblMxnTotal,
            this.xrlblUsdTotal,
            this.xrlblMxnIEPS,
            this.xrlblUsdIEPS,
            this.xrlblUsdIVA,
            this.xrlblMxnIVA,
            this.xrlblUSD,
            this.xrlblMXN,
            this.xrlblTotalUSD,
            this.xrlblTotalMXN});
            this.GroupFooter1.Dpi = 100F;
            this.GroupFooter1.HeightF = 78.34447F;
            this.GroupFooter1.Name = "GroupFooter1";
            // 
            // xrlblUsdImport
            // 
            this.xrlblUsdImport.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrlblUsdImport.Dpi = 100F;
            this.xrlblUsdImport.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrlblUsdImport.LocationFloat = new DevExpress.Utils.PointFloat(561.1852F, 22.99997F);
            this.xrlblUsdImport.Name = "xrlblUsdImport";
            this.xrlblUsdImport.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrlblUsdImport.SizeF = new System.Drawing.SizeF(86.94849F, 22.99998F);
            this.xrlblUsdImport.StylePriority.UseBorders = false;
            this.xrlblUsdImport.StylePriority.UseFont = false;
            this.xrlblUsdImport.StylePriority.UseTextAlignment = false;
            this.xrlblUsdImport.Text = "$ 0";
            this.xrlblUsdImport.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrlblMxnImport
            // 
            this.xrlblMxnImport.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrlblMxnImport.Dpi = 100F;
            this.xrlblMxnImport.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrlblMxnImport.LocationFloat = new DevExpress.Utils.PointFloat(561.1851F, 0F);
            this.xrlblMxnImport.Name = "xrlblMxnImport";
            this.xrlblMxnImport.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrlblMxnImport.SizeF = new System.Drawing.SizeF(86.94849F, 22.99998F);
            this.xrlblMxnImport.StylePriority.UseBorders = false;
            this.xrlblMxnImport.StylePriority.UseFont = false;
            this.xrlblMxnImport.StylePriority.UseTextAlignment = false;
            this.xrlblMxnImport.Text = "$ 0";
            this.xrlblMxnImport.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrlblMxnTotal
            // 
            this.xrlblMxnTotal.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrlblMxnTotal.Dpi = 100F;
            this.xrlblMxnTotal.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrlblMxnTotal.LocationFloat = new DevExpress.Utils.PointFloat(853.9249F, 0F);
            this.xrlblMxnTotal.Name = "xrlblMxnTotal";
            this.xrlblMxnTotal.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrlblMxnTotal.SizeF = new System.Drawing.SizeF(195.0751F, 22.99998F);
            this.xrlblMxnTotal.StylePriority.UseBorders = false;
            this.xrlblMxnTotal.StylePriority.UseFont = false;
            this.xrlblMxnTotal.StylePriority.UseTextAlignment = false;
            this.xrlblMxnTotal.Text = "$ 0";
            this.xrlblMxnTotal.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrlblUsdTotal
            // 
            this.xrlblUsdTotal.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrlblUsdTotal.Dpi = 100F;
            this.xrlblUsdTotal.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrlblUsdTotal.LocationFloat = new DevExpress.Utils.PointFloat(853.9248F, 23.05278F);
            this.xrlblUsdTotal.Name = "xrlblUsdTotal";
            this.xrlblUsdTotal.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrlblUsdTotal.SizeF = new System.Drawing.SizeF(195.0752F, 22.99998F);
            this.xrlblUsdTotal.StylePriority.UseBorders = false;
            this.xrlblUsdTotal.StylePriority.UseFont = false;
            this.xrlblUsdTotal.StylePriority.UseTextAlignment = false;
            this.xrlblUsdTotal.Text = "$ 0";
            this.xrlblUsdTotal.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrlblMxnIEPS
            // 
            this.xrlblMxnIEPS.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrlblMxnIEPS.Dpi = 100F;
            this.xrlblMxnIEPS.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrlblMxnIEPS.LocationFloat = new DevExpress.Utils.PointFloat(744.2922F, 0.05280344F);
            this.xrlblMxnIEPS.Name = "xrlblMxnIEPS";
            this.xrlblMxnIEPS.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrlblMxnIEPS.SizeF = new System.Drawing.SizeF(109.6326F, 22.99998F);
            this.xrlblMxnIEPS.StylePriority.UseBorders = false;
            this.xrlblMxnIEPS.StylePriority.UseFont = false;
            this.xrlblMxnIEPS.StylePriority.UseTextAlignment = false;
            this.xrlblMxnIEPS.Text = "$ 0";
            this.xrlblMxnIEPS.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrlblUsdIEPS
            // 
            this.xrlblUsdIEPS.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrlblUsdIEPS.Dpi = 100F;
            this.xrlblUsdIEPS.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrlblUsdIEPS.LocationFloat = new DevExpress.Utils.PointFloat(744.2922F, 23.05278F);
            this.xrlblUsdIEPS.Name = "xrlblUsdIEPS";
            this.xrlblUsdIEPS.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrlblUsdIEPS.SizeF = new System.Drawing.SizeF(109.6326F, 22.99998F);
            this.xrlblUsdIEPS.StylePriority.UseBorders = false;
            this.xrlblUsdIEPS.StylePriority.UseFont = false;
            this.xrlblUsdIEPS.StylePriority.UseTextAlignment = false;
            this.xrlblUsdIEPS.Text = "$ 0";
            this.xrlblUsdIEPS.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrlblUsdIVA
            // 
            this.xrlblUsdIVA.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrlblUsdIVA.Dpi = 100F;
            this.xrlblUsdIVA.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrlblUsdIVA.LocationFloat = new DevExpress.Utils.PointFloat(648.1336F, 22.99998F);
            this.xrlblUsdIVA.Name = "xrlblUsdIVA";
            this.xrlblUsdIVA.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrlblUsdIVA.SizeF = new System.Drawing.SizeF(96.15863F, 22.99998F);
            this.xrlblUsdIVA.StylePriority.UseBorders = false;
            this.xrlblUsdIVA.StylePriority.UseFont = false;
            this.xrlblUsdIVA.StylePriority.UseTextAlignment = false;
            this.xrlblUsdIVA.Text = "$ 0";
            this.xrlblUsdIVA.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrlblMxnIVA
            // 
            this.xrlblMxnIVA.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrlblMxnIVA.Dpi = 100F;
            this.xrlblMxnIVA.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrlblMxnIVA.LocationFloat = new DevExpress.Utils.PointFloat(648.1336F, 0F);
            this.xrlblMxnIVA.Name = "xrlblMxnIVA";
            this.xrlblMxnIVA.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrlblMxnIVA.SizeF = new System.Drawing.SizeF(96.15863F, 22.99998F);
            this.xrlblMxnIVA.StylePriority.UseBorders = false;
            this.xrlblMxnIVA.StylePriority.UseFont = false;
            this.xrlblMxnIVA.StylePriority.UseTextAlignment = false;
            this.xrlblMxnIVA.Text = "$ 0";
            this.xrlblMxnIVA.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrlblUSD
            // 
            this.xrlblUSD.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrlblUSD.Dpi = 100F;
            this.xrlblUSD.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrlblUSD.LocationFloat = new DevExpress.Utils.PointFloat(86.56415F, 23.05276F);
            this.xrlblUSD.Name = "xrlblUSD";
            this.xrlblUSD.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrlblUSD.SizeF = new System.Drawing.SizeF(474.6209F, 22.99998F);
            this.xrlblUSD.StylePriority.UseBorders = false;
            this.xrlblUSD.StylePriority.UseFont = false;
            this.xrlblUSD.StylePriority.UseTextAlignment = false;
            this.xrlblUSD.Text = "USD";
            this.xrlblUSD.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrlblMXN
            // 
            this.xrlblMXN.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrlblMXN.Dpi = 100F;
            this.xrlblMXN.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrlblMXN.LocationFloat = new DevExpress.Utils.PointFloat(86.56409F, 0F);
            this.xrlblMXN.Name = "xrlblMXN";
            this.xrlblMXN.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrlblMXN.SizeF = new System.Drawing.SizeF(474.621F, 22.99998F);
            this.xrlblMXN.StylePriority.UseBorders = false;
            this.xrlblMXN.StylePriority.UseFont = false;
            this.xrlblMXN.StylePriority.UseTextAlignment = false;
            this.xrlblMXN.Text = "MXN";
            this.xrlblMXN.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrlblTotalUSD
            // 
            this.xrlblTotalUSD.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrlblTotalUSD.Dpi = 100F;
            this.xrlblTotalUSD.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrlblTotalUSD.LocationFloat = new DevExpress.Utils.PointFloat(0F, 23.05278F);
            this.xrlblTotalUSD.Name = "xrlblTotalUSD";
            this.xrlblTotalUSD.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrlblTotalUSD.SizeF = new System.Drawing.SizeF(86.56407F, 22.99998F);
            this.xrlblTotalUSD.StylePriority.UseBorders = false;
            this.xrlblTotalUSD.StylePriority.UseFont = false;
            this.xrlblTotalUSD.StylePriority.UseTextAlignment = false;
            this.xrlblTotalUSD.Text = "Total";
            this.xrlblTotalUSD.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrlblTotalMXN
            // 
            this.xrlblTotalMXN.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrlblTotalMXN.Dpi = 100F;
            this.xrlblTotalMXN.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrlblTotalMXN.LocationFloat = new DevExpress.Utils.PointFloat(0F, 0F);
            this.xrlblTotalMXN.Name = "xrlblTotalMXN";
            this.xrlblTotalMXN.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrlblTotalMXN.SizeF = new System.Drawing.SizeF(86.56407F, 22.99998F);
            this.xrlblTotalMXN.StylePriority.UseBorders = false;
            this.xrlblTotalMXN.StylePriority.UseFont = false;
            this.xrlblTotalMXN.StylePriority.UseTextAlignment = false;
            this.xrlblTotalMXN.Text = "Total";
            this.xrlblTotalMXN.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // PageFooter
            // 
            this.PageFooter.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrPageInfo1});
            this.PageFooter.Dpi = 100F;
            this.PageFooter.HeightF = 36.03925F;
            this.PageFooter.Name = "PageFooter";
            // 
            // xrPageInfo1
            // 
            this.xrPageInfo1.Dpi = 100F;
            this.xrPageInfo1.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Bold);
            this.xrPageInfo1.Format = "Página {0} de {1}";
            this.xrPageInfo1.LocationFloat = new DevExpress.Utils.PointFloat(9.999974F, 9.999974F);
            this.xrPageInfo1.Name = "xrPageInfo1";
            this.xrPageInfo1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrPageInfo1.SizeF = new System.Drawing.SizeF(100F, 15F);
            this.xrPageInfo1.StylePriority.UseFont = false;
            // 
            // DetailReport
            // 
            this.DetailReport.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.Detail1});
            this.DetailReport.Dpi = 100F;
            this.DetailReport.Level = 0;
            this.DetailReport.Name = "DetailReport";
            // 
            // Detail1
            // 
            this.Detail1.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTablePurchase});
            this.Detail1.Dpi = 100F;
            this.Detail1.HeightF = 50.09805F;
            this.Detail1.Name = "Detail1";
            // 
            // PurchaseOrderReport
            // 
            this.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.Detail,
            this.TopMargin,
            this.BottomMargin,
            this.PageHeader,
            this.GroupFooter1,
            this.PageFooter,
            this.DetailReport});
            this.Landscape = true;
            this.Margins = new System.Drawing.Printing.Margins(26, 25, 11, 29);
            this.PageHeight = 850;
            this.PageWidth = 1100;
            this.Version = "16.2";
            ((System.ComponentModel.ISupportInitialize)(this.xrTablePurchase)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

        }

        #endregion

        private DevExpress.XtraReports.UI.DetailBand Detail;
        private DevExpress.XtraReports.UI.TopMarginBand TopMargin;
        private DevExpress.XtraReports.UI.BottomMarginBand BottomMargin;
        private DevExpress.XtraReports.UI.PageHeaderBand PageHeader;
        private DevExpress.XtraReports.UI.XRLabel xrDateShow;
        private DevExpress.XtraReports.UI.XRLabel xrlblDate;
        private DevExpress.XtraReports.UI.XRPictureBox xrPictureBox2;
        private DevExpress.XtraReports.UI.XRLabel xrLabel21;
        private DevExpress.XtraReports.UI.XRLabel xrlblUser;
        private DevExpress.XtraReports.UI.XRLabel xrLabel6;
        private DevExpress.XtraReports.UI.XRLabel siteLabel;
        private DevExpress.XtraReports.UI.XRLabel xrLabel8;
        private DevExpress.XtraReports.UI.XRLabel xrlblCreate;
        private DevExpress.XtraReports.UI.XRLabel xraddress;
        private DevExpress.XtraReports.UI.XRLabel xrState;
        private DevExpress.XtraReports.UI.XRTable xrTablePurchase;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow1;
        private DevExpress.XtraReports.UI.XRTableCell xrCellPO;
        private DevExpress.XtraReports.UI.XRTableCell xrCellProvider;
        private DevExpress.XtraReports.UI.XRTableCell xrCellETA;
        private DevExpress.XtraReports.UI.XRTableCell xrCellInvoice;
        private DevExpress.XtraReports.UI.XRTableCell xrCellCurrency;
        private DevExpress.XtraReports.UI.XRTableCell xrCellImport;
        private DevExpress.XtraReports.UI.XRTableCell xrCellIVA;
        private DevExpress.XtraReports.UI.XRTableCell xrCellIEPS;
        private DevExpress.XtraReports.UI.XRTableCell xrCellTotal;
        private DevExpress.XtraReports.UI.XRTableCell xrCellStatus;
        private DevExpress.XtraReports.UI.XRLabel xrlblStatus;
        private DevExpress.XtraReports.UI.XRLabel xrlblTotal;
        private DevExpress.XtraReports.UI.XRLabel xrlblIEPS;
        private DevExpress.XtraReports.UI.XRLabel xrlblIVA;
        private DevExpress.XtraReports.UI.XRLabel xrlblImport;
        private DevExpress.XtraReports.UI.XRLabel xrlblCurrency;
        private DevExpress.XtraReports.UI.XRLabel xrLabel3;
        private DevExpress.XtraReports.UI.XRLabel xrlblETA;
        private DevExpress.XtraReports.UI.XRLabel xrlblProvider;
        private DevExpress.XtraReports.UI.XRLabel xrlblPO;
        private DevExpress.XtraReports.UI.GroupFooterBand GroupFooter1;
        private DevExpress.XtraReports.UI.PageFooterBand PageFooter;
        private DevExpress.XtraReports.UI.XRPageInfo xrPageInfo1;
        private DevExpress.XtraReports.UI.XRLabel xrlblUSD;
        private DevExpress.XtraReports.UI.XRLabel xrlblMXN;
        private DevExpress.XtraReports.UI.XRLabel xrlblTotalUSD;
        private DevExpress.XtraReports.UI.XRLabel xrlblTotalMXN;
        private DevExpress.XtraReports.UI.XRLabel xrlblMxnTotal;
        private DevExpress.XtraReports.UI.XRLabel xrlblUsdTotal;
        private DevExpress.XtraReports.UI.XRLabel xrlblMxnIEPS;
        private DevExpress.XtraReports.UI.XRLabel xrlblUsdIEPS;
        private DevExpress.XtraReports.UI.XRLabel xrlblUsdIVA;
        private DevExpress.XtraReports.UI.XRLabel xrlblMxnIVA;
        private DevExpress.XtraReports.UI.XRLabel xrlblEndDate;
        private DevExpress.XtraReports.UI.XRLabel xrlblStartDate;
        private DevExpress.XtraReports.UI.XRLabel xrLabel2;
        private DevExpress.XtraReports.UI.XRLabel xrLabel1;
        private DevExpress.XtraReports.UI.XRLabel xrlblUsdImport;
        private DevExpress.XtraReports.UI.XRLabel xrlblMxnImport;
        private DevExpress.XtraReports.UI.DetailReportBand DetailReport;
        private DevExpress.XtraReports.UI.DetailBand Detail1;
    }
}
