﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
using App.Entities.ViewModels.Item;
using System.Collections.Generic;
using System.Data;

namespace FloridoERPTX.Reports
{
    public partial class InventoryWithoutSales : DevExpress.XtraReports.UI.XtraReport
    {
        public DataSet DataSetTableMovements = null;
        public InventoryWithoutSales()
        {
            InitializeComponent();
        }
        public DataSet printTable(List<ItemSalesInventory> items, string siteName)
        {
            try
            {
                xrLabelSiteName.Text = siteName;
                xrLabelDate.Text = System.DateTime.Now.ToString();
                DataSetTableMovements = new DataSet();

                DataTable table = new DataTable();
                DataSetTableMovements.Tables.Add(table);

                table.Columns.Add(new DataColumn("cellPartNumber"));
                table.Columns.Add(new DataColumn("cellPartDescription"));
                table.Columns.Add(new DataColumn("cellUnrestricted"));
                table.Columns.Add(new DataColumn("cellAmount"));
                table.Columns.Add(new DataColumn("cellStorageLocation"));
                table.Columns.Add(new DataColumn("cellLastPurchase"));
                table.Columns.Add(new DataColumn("cellDays"));

                foreach (var item in items)
                {
                    DataRow DataRowOrdered = DataSetTableMovements.Tables[0].NewRow();
                    DataRowOrdered["cellPartNumber"] = item.PartNumber;
                    DataRowOrdered["cellPartDescription"] = item.PartDescription;
                    DataRowOrdered["cellUnrestricted"] = item.Unrestricted;
                    DataRowOrdered["cellAmount"] = item.MapPrice;
                    DataRowOrdered["cellStorageLocation"] = item.StorageLocation;                    
                    DataRowOrdered["cellLastPurchase"] = Convert.ToDateTime(item.LastPurchase).ToString("dd/MM/yyyy") == "01/01/0001" ? "" : Convert.ToDateTime(item.LastPurchase).ToString("dd/MM/yyyy");
                    DataRowOrdered["cellDays"] = item.Days;
                    DataSetTableMovements.Tables[0].Rows.Add(DataRowOrdered);
                }

                cellPartNumber.DataBindings.Add(new XRBinding("Text", null, "xrTableRow1.cellPartNumber"));
                cellPartDescription.DataBindings.Add(new XRBinding("Text", null, "xrTableRow1.cellPartDescription"));
                cellUnrestricted.DataBindings.Add(new XRBinding("Text", null, "xrTableRow1.cellUnrestricted"));
                cellAmount.DataBindings.Add(new XRBinding("Text", null, "xrTableRow1.cellAmount"));
                cellStorageLocation.DataBindings.Add(new XRBinding("Text", null, "xrTableRow1.cellStorageLocation"));
                cellLastPurchase.DataBindings.Add(new XRBinding("Text", null, "xrTableRow1.cellLastPurchase"));
                cellDays.DataBindings.Add(new XRBinding("Text", null, "xrTableRow1.cellDays"));

                return DataSetTableMovements;
            }
            catch (Exception ex)
            {
                string d = ex.Message;
                throw;
            }
        }
    }
}
