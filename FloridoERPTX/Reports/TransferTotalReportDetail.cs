﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
using System.Data;
using System.Collections.Generic;
using App.Entities;
using App.Entities.ViewModels.PurchaseOrder;

namespace FloridoERPTX.Reports
{
    public partial class TransferTotalReportDetail : DevExpress.XtraReports.UI.XtraReport
    {
        public DataSet DataSetTableMovements = null;
        public decimal iva, subtotal, ieps, total, miniTotal = 0;
        public TransferTotalReportDetail()
        {
            InitializeComponent();
        }

        public DataSet printTable(List<PurchaseOrderItemDetailReportGeneral> POs, SITES site_info, DateTime? date1, DateTime? date2, string name, string transferType)
        {
            try
            {
                DataSetTableMovements = new DataSet();
                DataTable table = new DataTable();
                DataSetTableMovements.Tables.Add(table);
                table.Columns.Add(new DataColumn("xrCellTransfer"));
                table.Columns.Add(new DataColumn("xrCellPartNumber"));
                table.Columns.Add(new DataColumn("xrCellDescription"));
                table.Columns.Add(new DataColumn("xrCellDeparment"));
                table.Columns.Add(new DataColumn("xrCellFamily"));
                table.Columns.Add(new DataColumn("xrCellETA"));
                table.Columns.Add(new DataColumn("xrCellOriginDestinationSite"));
                table.Columns.Add(new DataColumn("xrCellQuantity"));
                table.Columns.Add(new DataColumn("xrCellCostUnit"));
                table.Columns.Add(new DataColumn("xrCellImport"));
                table.Columns.Add(new DataColumn("xrCellIVA"));
                table.Columns.Add(new DataColumn("xrCellIEPS"));
                table.Columns.Add(new DataColumn("xrCellTotal"));
                table.Columns.Add(new DataColumn("xrCellStatus"));


                DateTime date = DateTime.Today;
                var firstDayOfMonth = new DateTime(date.Year, date.Month, 1);
                var lastDayOfMonth = firstDayOfMonth.AddMonths(1).AddDays(-1);
                DateTime BeginDate2 = date1 ?? firstDayOfMonth;
                DateTime EndDate2 = date2 ?? lastDayOfMonth;

                xrlblTransfTypeShow.Text = transferType.ToUpper();
                xrlblUser.Text = name.ToUpper();
                xrlblEndDate.Text = EndDate2.ToShortDateString();
                xrlblStartDate.Text = BeginDate2.ToShortDateString();
                xrDateShow.Text = DateTime.Now.ToShortDateString();

                //Info Tienda
                xraddress.Text = site_info.address + " C.P." + site_info.postal_code;
                xrState.Text = site_info.city + ", " + site_info.state;
                siteLabel.Text = "*" + site_info.site_name;
                xrDateShow.Text = DateTime.Now.ToShortDateString();

                if (transferType == "salida")
                    xrlblDateTransfer.Text = "Salida";
                else
                    xrlblDateTransfer.Text = "Entrada";
                foreach (var item in POs)
                {
                    DataRow DataRowOrdered = DataSetTableMovements.Tables[0].NewRow();
                    DataRowOrdered["xrCellTransfer"] = item.Folio;
                    DataRowOrdered["xrCellPartNumber"] = item.PartNumber;
                    DataRowOrdered["xrCellDescription"] = item.DescriptionC;
                    DataRowOrdered["xrCellDeparment"] = item.Department;
                    DataRowOrdered["xrCellFamily"] = item.Family;
                    DataRowOrdered["xrCellETA"] = item.MerchandiseEntry == "" || item.MerchandiseEntry == null ? "LLego en " + item.MerchandiseEntryEta : item.MerchandiseEntry;
                    DataRowOrdered["xrCellOriginDestinationSite"] = item.Supplier;
                    DataRowOrdered["xrCellQuantity"] = item.Quantity;
                    DataRowOrdered["xrCellCostUnit"] = item.CostUnit.ToString("C4");
                    DataRowOrdered["xrCellImport"] = item.SubTotal.ToString("C4");
                    DataRowOrdered["xrCellIVA"] = item.IVA.ToString("C4");
                    DataRowOrdered["xrCellIEPS"] = item.IEPS.ToString("C4");
                    DataRowOrdered["xrCellTotal"] = item.Amount.ToString("C4");
                    DataRowOrdered["xrCellStatus"] = item.Item_Status;
                    DataSetTableMovements.Tables[0].Rows.Add(DataRowOrdered);

                    miniTotal = item.IVA + item.IEPS + item.SubTotal;
                    iva += item.IVA;
                    ieps += item.IEPS;
                    subtotal += item.SubTotal;
                    total += miniTotal;
                }
                xrlblMxnImport.Text = subtotal.ToString("C4");
                xrlblMxnIVA.Text = iva.ToString("C4");
                xrlblMxnIEPS.Text = ieps.ToString("C4");
                xrlblMxnTotal.Text = total.ToString("C4");

                xrCellTransfer.DataBindings.Add(new XRBinding("Text", null, "xrTablePurchase.xrCellTransfer"));
                xrCellPartNumber.DataBindings.Add(new XRBinding("Text", null, "xrTablePurchase.xrCellPartNumber"));
                xrCellDescription.DataBindings.Add(new XRBinding("Text", null, "xrTablePurchase.xrCellDescription"));
                xrCellDeparment.DataBindings.Add(new XRBinding("Text", null, "xrTablePurchase.xrCellDeparment"));
                xrCellFamily.DataBindings.Add(new XRBinding("Text", null, "xrTablePurchase.xrCellFamily"));
                xrCellETA.DataBindings.Add(new XRBinding("Text", null, "xrTablePurchase.xrCellETA"));
                xrCellOriginDestinationSite.DataBindings.Add(new XRBinding("Text", null, "xrTablePurchase.xrCellOriginDestinationSite"));
                xrCellQuantity.DataBindings.Add(new XRBinding("Text", null, "xrTablePurchase.xrCellQuantity"));
                xrCellCostUnit.DataBindings.Add(new XRBinding("Text", null, "xrTablePurchase.xrCellCostUnit"));
                xrCellImport.DataBindings.Add(new XRBinding("Text", null, "xrTablePurchase.xrCellImport"));
                xrCellIVA.DataBindings.Add(new XRBinding("Text", null, "xrTablePurchase.xrCellIVA"));
                xrCellIEPS.DataBindings.Add(new XRBinding("Text", null, "xrTablePurchase.xrCellIEPS"));
                xrCellTotal.DataBindings.Add(new XRBinding("Text", null, "xrTablePurchase.xrCellTotal"));
                xrCellStatus.DataBindings.Add(new XRBinding("Text", null, "xrTablePurchase.xrCellStatus"));
                return DataSetTableMovements;
            }
            catch (Exception e)
            {
                var msg = e.Message;
                throw;
            }
        }
    }
}
