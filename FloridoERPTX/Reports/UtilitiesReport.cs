﻿using System;
using System.Data;
using DevExpress.XtraReports.UI;
using System.Collections.Generic;
using System.Linq;
using App.BLL.Site;
using App.Entities.ViewModels.Sales;

namespace FloridoERPTX.Reports
{
    public partial class UtilitiesReport : DevExpress.XtraReports.UI.XtraReport
    {
        private SiteConfigBusiness _SiteConfigBusiness;
        public DataSet DataSetTableOrdered = null;        
        public UtilitiesReport()
        {
            InitializeComponent();
            _SiteConfigBusiness = new SiteConfigBusiness();
        }
        public DataSet printTable(UtilitySalesIndex Model) 
        {
            try
            {
                decimal greatTotal = 0, greatCost = 0, greatUtility = 0, greatPercentage = 0;
                Date.Text = string.Format("{0} - {1}", Model.StartDate, Model.EndDate);
                siteLabel.Text = _SiteConfigBusiness.GetSiteCodeName();
                DataSetTableOrdered = new DataSet();

                DataTable DataTableOrdered = new DataTable();
                DataSetTableOrdered.Tables.Add(DataTableOrdered);
                
                DataTableOrdered.Columns.Add(new DataColumn("Item"));
                DataTableOrdered.Columns.Add(new DataColumn("Description"));
                DataTableOrdered.Columns.Add(new DataColumn("Qty"));
                DataTableOrdered.Columns.Add(new DataColumn("Sales"));
                DataTableOrdered.Columns.Add(new DataColumn("IEPS"));
                DataTableOrdered.Columns.Add(new DataColumn("Total"));
                DataTableOrdered.Columns.Add(new DataColumn("Cost"));
                DataTableOrdered.Columns.Add(new DataColumn("Utility"));
                DataTableOrdered.Columns.Add(new DataColumn("Percentage"));

                foreach (var item in Model.UtilitySales)
                {
                    decimal Cost = 0;
                    if (Model.GroupByDepartment)
                    {
                        greatTotal = 0; greatCost = 0; greatUtility = 0; greatPercentage = 0;
                        Cost = item.COSTO_STD;
                    }
                    else
                    {
                        Cost = @item.COSTO_STD * @item.QTY;
                    }
                    decimal Total = @item.VENTA + @item.IEPS + @item.IVA;
                    decimal TotalNoTax = @item.VENTA + @item.IEPS;
                    decimal Utility = (item.VENTA + item.IEPS) - Cost;
                    decimal Percentage = Math.Round((Utility * 100) / TotalNoTax, 2);
                    greatTotal += TotalNoTax;
                    greatCost += Cost;
                    greatUtility += Utility;
                    DataRow DataRowOrdered = DataSetTableOrdered.Tables[0].NewRow();
                    DataRowOrdered["Item"] = item.part_number;
                    DataRowOrdered["Description"] = item.part_description;
                    DataRowOrdered["Qty"] = item.QTY.ToString("0.00");
                    DataRowOrdered["Sales"] = item.VENTA.ToString("C");
                    DataRowOrdered["IEPS"] = item.IEPS.ToString("C");
                    DataRowOrdered["Total"] = Total.ToString("C");
                    DataRowOrdered["Cost"] = Cost.ToString("C");
                    DataRowOrdered["Utility"] = Utility.ToString("C");
                    DataRowOrdered["Percentage"] = Percentage.ToString() + "%";


                    DataSetTableOrdered.Tables[0].Rows.Add(DataRowOrdered);                                                        
                }
                greatPercentage = Math.Round((greatUtility * 100) / greatTotal, 2);
                GreatCost.Text = greatCost.ToString("C");
                GreatUtility.Text = greatUtility.ToString("C");
                GreatTotal.Text = greatTotal.ToString("C");
                GreatPercentage.Text = string.Format("{0}%", greatPercentage);
                GreatIeps.Text = Model.UtilitySales.Sum(c => c.IEPS).ToString("C");
                GreatSales.Text = Model.UtilitySales.Sum(c => c.VENTA).ToString("C");                

                Item.DataBindings.Add(new XRBinding("Text", null, "xrTableRowFormat.Item"));
                Description.DataBindings.Add(new XRBinding("Text", null, "xrTableRowFormat.Description"));
                Qty.DataBindings.Add(new XRBinding("Text", null, "xrTableRowFormat.Qty"));
                Sales.DataBindings.Add(new XRBinding("Text", null, "xrTableRowFormat.Sales"));
                IEPS.DataBindings.Add(new XRBinding("Text", null, "xrTableRowFormat.IEPS"));
                Total.DataBindings.Add(new XRBinding("Text", null, "xrTableRowFormat.Total"));
                Cost.DataBindings.Add(new XRBinding("Text", null, "xrTableRowFormat.Cost"));
                Utility.DataBindings.Add(new XRBinding("Text", null, "xrTableRowFormat.Utility"));
                Percentage.DataBindings.Add(new XRBinding("Text", null, "xrTableRowFormat.Percentage"));                

                return DataSetTableOrdered;
            }
            catch (Exception ex)
            {
                string d=ex.Message;
                throw;
            }
          
        }       
    }
}
