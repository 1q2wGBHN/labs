﻿using System;
using System.Data;
using DevExpress.XtraReports.UI;
using System.Linq;
using App.Entities.ViewModels.Donations;
using FloridoERPTX.Extensions;

namespace FloridoERPTX.Reports
{
    public partial class DonationsReport : DevExpress.XtraReports.UI.XtraReport
    {

        public DataSet DataSetTableOrdered = null;

        public DonationsReport()
        {
            InitializeComponent();
        }

        public DataSet printTable(DonationIndex Model, string siteName)
        {
            try
            {
                decimal greatTotal = 0, totalCashiers = 0;
                int cont = 0;                

                DataSetTableOrdered = new DataSet();
                DataTable DataTableOrdered = new DataTable();
                DataTable TotalCashierTable = new DataTable();
                DataSetTableOrdered.Tables.Add(DataTableOrdered);
                DataSetTableOrdered.Tables.Add(TotalCashierTable);
                siteLabel.Text = siteName;

                Date.Text = string.Format("{0}-{1}", Model.StartDate, Model.EndDate);
                Cashier.Text = Model.CashierName;
                Campaign.Text = Model.CampaignName;

                DataTableOrdered.Columns.Add(new DataColumn("DonationDate"));
                DataTableOrdered.Columns.Add(new DataColumn("CashierName"));
                DataTableOrdered.Columns.Add(new DataColumn("AmountMXN"));
                DataTableOrdered.Columns.Add(new DataColumn("AmountUSD"));
                DataTableOrdered.Columns.Add(new DataColumn("CurrencyRate"));
                DataTableOrdered.Columns.Add(new DataColumn("Total"));
                DataTableOrdered.Columns.Add(new DataColumn("EmptyCell"));
                DataTableOrdered.Columns.Add(new DataColumn("CashierName2"));
                DataTableOrdered.Columns.Add(new DataColumn("CashierTotal"));

                foreach (var item in Model.DataReport.OrderBy(c => c.Date).ThenBy(x => x.Cashier))
                {
                    var total = item.AmountMXN + (item.AmountUSD * item.CurrencyRate);
                    greatTotal += total;

                    DataRow DataRowOrdered = DataSetTableOrdered.Tables[0].NewRow();
                    DataRowOrdered["DonationDate"] = item.Date;
                    DataRowOrdered["CashierName"] = item.Cashier.ToTitleCase();
                    DataRowOrdered["AmountMXN"] = item.AmountMXN.ToString("C");
                    DataRowOrdered["AmountUSD"] = item.AmountUSD.ToString("C");
                    DataRowOrdered["CurrencyRate"] = item.CurrencyRate.ToString("C");
                    DataRowOrdered["Total"] = total.ToString("C");
                    DataRowOrdered["EmptyCell"] = string.Empty;
                    if (cont < Model.CashierTotals.Count)
                    {
                        totalCashiers += Model.CashierTotals[cont].Amount;                        
                        DataRowOrdered["CashierName2"] = Model.CashierTotals[cont].Cashier.ToTitleCase();
                        DataRowOrdered["CashierTotal"] = Model.CashierTotals[cont].Amount.ToString("C");
                    }
                    else
                    {
                        DataRowOrdered["CashierName2"] = string.Empty;
                        DataRowOrdered["CashierTotal"] = string.Empty;
                    }
                    cont++;
                    DataSetTableOrdered.Tables[0].Rows.Add(DataRowOrdered);
                }

                DonationDate.DataBindings.Add(new XRBinding("Text", null, "xrTableRowFormat.DonationDate"));
                CashierName.DataBindings.Add(new XRBinding("Text", null, "xrTableRowFormat.CashierName"));
                AmountMXN.DataBindings.Add(new XRBinding("Text", null, "xrTableRowFormat.AmountMXN"));
                AmountUSD.DataBindings.Add(new XRBinding("Text", null, "xrTableRowFormat.AmountUSD"));
                CurrencyRate.DataBindings.Add(new XRBinding("Text", null, "xrTableRowFormat.CurrencyRate"));
                Total.DataBindings.Add(new XRBinding("Text", null, "xrTableRowFormat.Total"));
                EmptyCell.DataBindings.Add(new XRBinding("Text", null, "xrTableRowFormat.EmptyCell"));
                CashierName2.DataBindings.Add(new XRBinding("Text", null, "xrTableRowFormat.CashierName2"));
                CashierTotal.DataBindings.Add(new XRBinding("Text", null, "xrTableRowFormat.CashierTotal"));                               

                totalDonations.Text = greatTotal.ToString("C");
                totalMXN.Text = Model.DataReport.Sum(c => c.AmountMXN).ToString("C");
                totalUSD.Text = Model.DataReport.Sum(c => c.AmountUSD).ToString("C");
                CashiersTotal.Text = totalCashiers.ToString("C");

                return DataSetTableOrdered;
            }
            catch (Exception ex)
            {
                string d = ex.Message;
                throw;
            }

        }
    }
}
