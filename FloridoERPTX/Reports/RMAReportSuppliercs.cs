﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
using App.Entities.ViewModels.DamagedsGoods;
using System.Data;
using System.Collections.Generic;

namespace FloridoERPTX.Reports
{
    public partial class RMAReportSuppliercs : DevExpress.XtraReports.UI.XtraReport
    {
        public DataSet DataSetTableOrdered = null;
        public RMAReportSuppliercs()
        {
            InitializeComponent();
        }
        public DataSet printTable(string document ,DamagedGoodsHeadModel damaged)
        {
            try
            {
                DataSetTableOrdered = new DataSet();

                DataTable DataTableOrdered = new DataTable();
                DataSetTableOrdered.Tables.Add(DataTableOrdered);

                

                if (damaged.processDate == null)
                {
                    xrLabelDateFin.Visible = false;
                    xrLabel8.Visible = false;
                }
                else
                {
                    xrLabelDateFin.Text = damaged.processDate.Value.ToShortDateString();
                    xrLabelDateFin.Visible = true;
                    xrLabel8.Visible = true;
                }
                xrLabelSupplier.Text = damaged.Supplier;
                xrLabelData.Text = DateTime.Now.ToString("dd/MM/yyyy");
                xrlblStatus.Text = StatusDamageds(damaged.ProcessStatus);
                xrLabelComment.Text = damaged.Comment;
                xrLabelSite.Text = damaged.SiteName;
                xrLabelRMA.Text = damaged.DamagedGoodsDoc;
                if (damaged.Weight == null)
                {
                    xLabelWeight.Text = "No aplica";
                    xrLabel11.Visible = false;
                    xLabelWeight.Visible = false;
                }
                else
                {
                    xLabelWeight.Text = damaged.Weight.ToString() + " Kg(s).";
                    xLabelWeight.Visible = true;
                    xrLabel11.Visible = true;

                }
                //DataTableOrdered.Columns.Add(new DataColumn("xrTableCellSite"));
                DataTableOrdered.Columns.Add(new DataColumn("xrTableCellPartNumber"));
                DataTableOrdered.Columns.Add(new DataColumn("xrTableCellDescription"));
                DataTableOrdered.Columns.Add(new DataColumn("xrTableCellQuantity"));
                DataTableOrdered.Columns.Add(new DataColumn("xrTableCellPrice"));
                DataTableOrdered.Columns.Add(new DataColumn("xrTableCellamount"));
                DataTableOrdered.Columns.Add(new DataColumn("xrTableCellIEPS"));
                DataTableOrdered.Columns.Add(new DataColumn("xrTableCellIVA"));
                DataTableOrdered.Columns.Add(new DataColumn("xrTableCellTotal"));
                DataTableOrdered.Columns.Add(new DataColumn("xLabelWeight"));

                decimal subtotal = 0;
                decimal ivatotal = 0;
                decimal iepstotal = 0;
                foreach (var damagedsGoods in damaged.DamageGoodsItem)
                {
                    DataRow DataRowOrdered = DataSetTableOrdered.Tables[0].NewRow();
                    //DataRowOrdered["xrTableCellSite"] = item.SiteName;
                    DataRowOrdered["xrTableCellPartNumber"] = damagedsGoods.PartNumber;
                    DataRowOrdered["xrTableCellDescription"] = damagedsGoods.Description;
                    DataRowOrdered["xrTableCellQuantity"] = damagedsGoods.Quantity;
                    DataRowOrdered["xrTableCellPrice"] = "$" + damagedsGoods.Price.ToString("N4");
                    DataRowOrdered["xrTableCellamount"] = "$" + damagedsGoods.Amount.ToString("N4");
                    DataRowOrdered["xrTableCellIEPS"] = "$" + damagedsGoods.IEPS.ToString("N4");
                    DataRowOrdered["xrTableCellIVA"] = "$" + damagedsGoods.IVA.ToString("N4");
                    DataRowOrdered["xrTableCellTotal"] = "$" + float.Parse(Math.Round((damagedsGoods.Amount + damagedsGoods.IEPS + damagedsGoods.IVA), 4).ToString());
                    subtotal += damagedsGoods.Amount;
                    ivatotal += damagedsGoods.IVA;
                    iepstotal += damagedsGoods.IEPS;
                        
                    DataSetTableOrdered.Tables[0].Rows.Add(DataRowOrdered);
                }

                xrLabelAmount.Text = "$ " + (subtotal + ivatotal + iepstotal).ToString("N4");
                xrLabelIEPS.Text = "$ " + iepstotal.ToString("N4");
                xrLabelIVA.Text = "$ " + ivatotal.ToString("N4");
                xrLabelSub.Text = "$ " + subtotal.ToString("N4");

                //xrTableCellSite.DataBindings.Add(new XRBinding("Text", null, "xrTableRow1.xrTableCellSite"));
                xrTableCellPartNumber.DataBindings.Add(new XRBinding("Text", null, "xrTableRow1.xrTableCellPartNumber"));
                xrTableCellDescription.DataBindings.Add(new XRBinding("Text", null, "xrTableRow1.xrTableCellDescription"));
                xrTableCellQuantity.DataBindings.Add(new XRBinding("Text", null, "xrTableRow1.xrTableCellQuantity"));
                xrTableCellPrice.DataBindings.Add(new XRBinding("Text", null, "xrTableRow1.xrTableCellPrice"));
                xrTableCellamount.DataBindings.Add(new XRBinding("Text", null, "xrTableRow1.xrTableCellamount"));
                xrTableCellIEPS.DataBindings.Add(new XRBinding("Text", null, "xrTableRow1.xrTableCellIEPS"));
                xrTableCellIVA.DataBindings.Add(new XRBinding("Text", null, "xrTableRow1.xrTableCellIVA"));
                xrTableCellTotal.DataBindings.Add(new XRBinding("Text", null, "xrTableRow1.xrTableCellTotal"));
                return DataSetTableOrdered;
            }
            catch (Exception ex)
            {
                string d = ex.Message;
                throw;
            }
        }

        public string StatusDamageds(int status)
        {
            if (status == 1)
                return "Pendiente";
            else if (status == 0)
                return "Inicial";
            else if (status == 2)
                return "Pendiente";
            else if (status == 3)
                return "Rechazado";
            else if (status == 8)
                return "Cancelada";
            else if (status == 9)
                return "Terminado";
            else
                return "Inicial";
        }
    }
}
