﻿using System;
using System.Data;
using System.Collections.Generic;
using App.DAL.PurchaseOrder;
using App.Entities;
using DevExpress.XtraReports.UI;
using App.BLL.Supplier;
using App.Entities.ViewModels.PurchaseOrder;
using App.Entities.ViewModels.Sales;
using App.Entities.ViewModels.Sanctions;
using FloridoERPTX.ViewModels;

namespace FloridoERPTX.Reports
{
    public partial class ProductsWithSales : DevExpress.XtraReports.UI.XtraReport
    {
       
        public DataSet DataSetTableOrdered = null;        
        public ProductsWithSales()
        {
            InitializeComponent();
          
        }
        public DataSet printTable(ItemsWithSales Model, string siteName) //se encarga de imprimir la orden del proveedor
        {
            try
            {

                labelDate.Text = DateTime.Now.Date.ToShortDateString();
                labelDate1.Text = Model.Date;
                labelDate2.Text = Model.Date2;
                siteLabel.Text = siteName;
                DataSetTableOrdered = new DataSet();

                DataTable DataTableOrdered = new DataTable();
                DataSetTableOrdered.Tables.Add(DataTableOrdered);

             


                DataTableOrdered.Columns.Add(new DataColumn("codigo"));
                DataTableOrdered.Columns.Add(new DataColumn("descripcion"));
                DataTableOrdered.Columns.Add(new DataColumn("importe"));
                DataTableOrdered.Columns.Add(new DataColumn("iva"));
                DataTableOrdered.Columns.Add(new DataColumn("proveedor"));
                DataTableOrdered.Columns.Add(new DataColumn("depto"));
                DataTableOrdered.Columns.Add(new DataColumn("cantidad"));
                



                foreach (var item in Model.ItemsList)
                {
                    DataRow DataRowOrdered = DataSetTableOrdered.Tables[0].NewRow();
                    DataRowOrdered["codigo"] = item.part_number;
                    DataRowOrdered["descripcion"] = item.part_description;
                    DataRowOrdered["importe"] = item.mount.ToString("C");
                    DataRowOrdered["iva"] = item.iva.ToString("C");
                    DataRowOrdered["proveedor"] = item.supplier;
                    DataRowOrdered["depto"] = item.department;
                    DataRowOrdered["cantidad"] = item.quantity;

                    DataSetTableOrdered.Tables[0].Rows.Add(DataRowOrdered);
                }
               
               
               
                cantidad.DataBindings.Add(new XRBinding("Text", null, "xrTableRowFormat.cantidad"));
                codigo.DataBindings.Add(new XRBinding("Text", null, "xrTableRowFormat.codigo"));
                importe.DataBindings.Add(new XRBinding("Text", null, "xrTableRowFormat.importe"));
                iva.DataBindings.Add(new XRBinding("Text", null, "xrTableRowFormat.iva"));
                descripcion.DataBindings.Add(new XRBinding("Text", null, "xrTableRowFormat.descripcion"));
                proveedor.DataBindings.Add(new XRBinding("Text", null, "xrTableRowFormat.proveedor"));
                depto.DataBindings.Add(new XRBinding("Text", null, "xrTableRowFormat.depto"));






                return DataSetTableOrdered;
            }
            catch (Exception ex)
            {
                string d=ex.Message;
                throw;
            }
          
        }

       


    }
}
