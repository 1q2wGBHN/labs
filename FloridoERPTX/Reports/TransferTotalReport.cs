﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
using System.Data;
using System.Collections.Generic;
using App.Entities;
using App.Entities.ViewModels.Transfer;

namespace FloridoERPTX.Reports
{
    public partial class TransferTotalReport : DevExpress.XtraReports.UI.XtraReport
    {
        public DataSet DataSetTableMovements = null;
        public decimal iva, subtotal, ieps, total, miniTotal = 0;
        public TransferTotalReport()
        {
            InitializeComponent();
        }

        public DataSet printTable(List<TransferHeaderModel> POs, SITES site_info, DateTime? date1, DateTime? date2, string name, string transferType)
        {
            try
            {
                DataSetTableMovements = new DataSet();
                DataTable table = new DataTable();
                DataSetTableMovements.Tables.Add(table);
                table.Columns.Add(new DataColumn("xrCellTransfer"));
                table.Columns.Add(new DataColumn("xrCellSiteOrigin"));
                table.Columns.Add(new DataColumn("xrCellDestinationSite"));
                table.Columns.Add(new DataColumn("xrCellDate"));
                table.Columns.Add(new DataColumn("xrCellComment"));
                table.Columns.Add(new DataColumn("xrCellImport"));
                table.Columns.Add(new DataColumn("xrCellIVA"));
                table.Columns.Add(new DataColumn("xrCellIEPS"));
                table.Columns.Add(new DataColumn("xrCellTotal"));
                table.Columns.Add(new DataColumn("xrCellStatus"));


                DateTime date = DateTime.Today;
                var firstDayOfMonth = new DateTime(date.Year, date.Month, 1);
                var lastDayOfMonth = firstDayOfMonth.AddMonths(1).AddDays(-1);
                DateTime BeginDate2 = date1 ?? firstDayOfMonth;
                DateTime EndDate2 = date2 ?? lastDayOfMonth;

                xrlblTransfTypeShow.Text = transferType.ToUpper();
                xrlblUser.Text = name.ToUpper();
                xrlblEndDate.Text = EndDate2.ToShortDateString();
                xrlblStartDate.Text = BeginDate2.ToShortDateString();
                xrDateShow.Text = DateTime.Now.ToShortDateString();

                //Info Tienda
                xraddress.Text = site_info.address + " C.P." + site_info.postal_code;
                xrState.Text = site_info.city + ", " + site_info.state;
                siteLabel.Text = "*" + site_info.site_name;
                xrDateShow.Text = DateTime.Now.ToShortDateString();

                foreach (var item in POs)
                {
                    miniTotal = item.TotalIva + item.TotalIEPS + item.TotalAmount;

                    DataRow DataRowOrdered = DataSetTableMovements.Tables[0].NewRow();
                    DataRowOrdered["xrCellTransfer"] = item.TransferDocument;
                    DataRowOrdered["xrCellSiteOrigin"] = item.TransferSiteName;
                    DataRowOrdered["xrCellDestinationSite"] = item.DestinationSiteName;
                    DataRowOrdered["xrCellDate"] = item.TransferDate == "" || item.TransferDate == null ? "-" : item.TransferDate;
                    DataRowOrdered["xrCellComment"] = item.Comment;
                    DataRowOrdered["xrCellImport"] = item.TotalAmount.ToString("C4");
                    DataRowOrdered["xrCellIVA"] = item.TotalIva.ToString("C4");
                    DataRowOrdered["xrCellIEPS"] = item.TotalIEPS.ToString("C4");
                    DataRowOrdered["xrCellTotal"] = miniTotal.ToString("C4");
                    DataRowOrdered["xrCellStatus"] = item.Status;
                    DataSetTableMovements.Tables[0].Rows.Add(DataRowOrdered);

                    iva += item.TotalIva;
                    ieps += item.TotalIEPS;
                    subtotal += item.TotalAmount;
                    total += miniTotal;
                }
                xrlblMxnImport.Text = subtotal.ToString("C4");
                xrlblMxnIVA.Text = iva.ToString("C4");
                xrlblMxnIEPS.Text = ieps.ToString("C4");
                xrlblMxnTotal.Text = total.ToString("C4");

                xrCellTransfer.DataBindings.Add(new XRBinding("Text", null, "xrTablePurchase.xrCellTransfer"));
                xrCellSiteOrigin.DataBindings.Add(new XRBinding("Text", null, "xrTablePurchase.xrCellSiteOrigin"));
                xrCellDestinationSite.DataBindings.Add(new XRBinding("Text", null, "xrTablePurchase.xrCellDestinationSite"));
                xrCellDate.DataBindings.Add(new XRBinding("Text", null, "xrTablePurchase.xrCellDate"));
                xrCellComment.DataBindings.Add(new XRBinding("Text", null, "xrTablePurchase.xrCellComment"));
                xrCellImport.DataBindings.Add(new XRBinding("Text", null, "xrTablePurchase.xrCellImport"));
                xrCellIVA.DataBindings.Add(new XRBinding("Text", null, "xrTablePurchase.xrCellIVA"));
                xrCellIEPS.DataBindings.Add(new XRBinding("Text", null, "xrTablePurchase.xrCellIEPS"));
                xrCellTotal.DataBindings.Add(new XRBinding("Text", null, "xrTablePurchase.xrCellTotal"));
                xrCellStatus.DataBindings.Add(new XRBinding("Text", null, "xrTablePurchase.xrCellStatus"));
                return DataSetTableMovements;
            }
            catch (Exception e)
            {
                var msg = e.Message;
                throw;
            }
        }

    }
}
