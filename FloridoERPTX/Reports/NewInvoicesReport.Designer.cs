﻿namespace FloridoERPTX.Reports
{
    partial class NewInvoicesReport
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(NewInvoicesReport));
            this.Detail = new DevExpress.XtraReports.UI.DetailBand();
            this.TopMargin = new DevExpress.XtraReports.UI.TopMarginBand();
            this.BottomMargin = new DevExpress.XtraReports.UI.BottomMarginBand();
            this.ReportHeader = new DevExpress.XtraReports.UI.ReportHeaderBand();
            this.labelDate1 = new DevExpress.XtraReports.UI.XRLabel();
            this.labelDate2 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel7 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel5 = new DevExpress.XtraReports.UI.XRLabel();
            this.labelDate = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel9 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLine1 = new DevExpress.XtraReports.UI.XRLine();
            this._labelReporte = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel4 = new DevExpress.XtraReports.UI.XRLabel();
            this.siteLabel = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel3 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrPictureBox1 = new DevExpress.XtraReports.UI.XRPictureBox();
            this.GroupFooter1 = new DevExpress.XtraReports.UI.GroupFooterBand();
            this._exeTotal = new DevExpress.XtraReports.UI.XRLabel();
            this._exeCash = new DevExpress.XtraReports.UI.XRLabel();
            this._exeCredit = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel38 = new DevExpress.XtraReports.UI.XRLabel();
            this.lbl_header_credito = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel16 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel20 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel21 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel22 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel23 = new DevExpress.XtraReports.UI.XRLabel();
            this.lbl_credit_zrate = new DevExpress.XtraReports.UI.XRLabel();
            this.lbl_credit_taxed = new DevExpress.XtraReports.UI.XRLabel();
            this.lbl_credit_tax = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel26 = new DevExpress.XtraReports.UI.XRLabel();
            this.lbl_cash_zrate = new DevExpress.XtraReports.UI.XRLabel();
            this.lbl_cash_taxed = new DevExpress.XtraReports.UI.XRLabel();
            this.lbl_cash_tax = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel27 = new DevExpress.XtraReports.UI.XRLabel();
            this.lbl_total_zrate = new DevExpress.XtraReports.UI.XRLabel();
            this.lbl_total_taxed = new DevExpress.XtraReports.UI.XRLabel();
            this.lbl_credit_ieps = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel28 = new DevExpress.XtraReports.UI.XRLabel();
            this.lbl_total_tax = new DevExpress.XtraReports.UI.XRLabel();
            this.lbl_total_ieps = new DevExpress.XtraReports.UI.XRLabel();
            this.lbl_cash_ieps = new DevExpress.XtraReports.UI.XRLabel();
            this.lbl_total_cash = new DevExpress.XtraReports.UI.XRLabel();
            this.lbl_total_global = new DevExpress.XtraReports.UI.XRLabel();
            this.lbl_total_credit = new DevExpress.XtraReports.UI.XRLabel();
            this.iepsLabel = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel31 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel32 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel33 = new DevExpress.XtraReports.UI.XRLabel();
            this.subtotalLabel = new DevExpress.XtraReports.UI.XRLabel();
            this.totalLabel = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel29 = new DevExpress.XtraReports.UI.XRLabel();
            this.ivaLabel = new DevExpress.XtraReports.UI.XRLabel();
            this.PageFooter = new DevExpress.XtraReports.UI.PageFooterBand();
            this.xrPageInfo1 = new DevExpress.XtraReports.UI.XRPageInfo();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // Detail
            // 
            this.Detail.Dpi = 100F;
            this.Detail.HeightF = 110.7075F;
            this.Detail.Name = "Detail";
            this.Detail.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.Detail.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // TopMargin
            // 
            this.TopMargin.Dpi = 100F;
            this.TopMargin.HeightF = 30F;
            this.TopMargin.Name = "TopMargin";
            this.TopMargin.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.TopMargin.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // BottomMargin
            // 
            this.BottomMargin.Dpi = 100F;
            this.BottomMargin.HeightF = 55F;
            this.BottomMargin.Name = "BottomMargin";
            this.BottomMargin.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.BottomMargin.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // ReportHeader
            // 
            this.ReportHeader.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.labelDate1,
            this.labelDate2,
            this.xrLabel7,
            this.xrLabel5,
            this.labelDate,
            this.xrLabel9,
            this.xrLine1,
            this._labelReporte,
            this.xrLabel4,
            this.siteLabel,
            this.xrLabel3,
            this.xrPictureBox1});
            this.ReportHeader.Dpi = 100F;
            this.ReportHeader.HeightF = 117.0793F;
            this.ReportHeader.Name = "ReportHeader";
            // 
            // labelDate1
            // 
            this.labelDate1.Dpi = 100F;
            this.labelDate1.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold);
            this.labelDate1.LocationFloat = new DevExpress.Utils.PointFloat(36.88701F, 25F);
            this.labelDate1.Name = "labelDate1";
            this.labelDate1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.labelDate1.SizeF = new System.Drawing.SizeF(100F, 20.00001F);
            this.labelDate1.StylePriority.UseFont = false;
            this.labelDate1.StylePriority.UseTextAlignment = false;
            this.labelDate1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // labelDate2
            // 
            this.labelDate2.Dpi = 100F;
            this.labelDate2.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold);
            this.labelDate2.LocationFloat = new DevExpress.Utils.PointFloat(173.774F, 25F);
            this.labelDate2.Name = "labelDate2";
            this.labelDate2.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.labelDate2.SizeF = new System.Drawing.SizeF(100F, 19.99998F);
            this.labelDate2.StylePriority.UseFont = false;
            this.labelDate2.StylePriority.UseTextAlignment = false;
            this.labelDate2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrLabel7
            // 
            this.xrLabel7.Dpi = 100F;
            this.xrLabel7.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel7.LocationFloat = new DevExpress.Utils.PointFloat(136.887F, 25.00002F);
            this.xrLabel7.Name = "xrLabel7";
            this.xrLabel7.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel7.SizeF = new System.Drawing.SizeF(36.88701F, 20F);
            this.xrLabel7.StylePriority.UseFont = false;
            this.xrLabel7.StylePriority.UseTextAlignment = false;
            this.xrLabel7.Text = "Al:";
            this.xrLabel7.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLabel5
            // 
            this.xrLabel5.Dpi = 100F;
            this.xrLabel5.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel5.LocationFloat = new DevExpress.Utils.PointFloat(0F, 25F);
            this.xrLabel5.Name = "xrLabel5";
            this.xrLabel5.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel5.SizeF = new System.Drawing.SizeF(36.88701F, 20F);
            this.xrLabel5.StylePriority.UseFont = false;
            this.xrLabel5.StylePriority.UseTextAlignment = false;
            this.xrLabel5.Text = "Del:";
            this.xrLabel5.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // labelDate
            // 
            this.labelDate.Dpi = 100F;
            this.labelDate.Font = new System.Drawing.Font("Arial", 7F);
            this.labelDate.LocationFloat = new DevExpress.Utils.PointFloat(922F, 86.58339F);
            this.labelDate.Name = "labelDate";
            this.labelDate.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.labelDate.SizeF = new System.Drawing.SizeF(100F, 15F);
            this.labelDate.StylePriority.UseFont = false;
            // 
            // xrLabel9
            // 
            this.xrLabel9.Dpi = 100F;
            this.xrLabel9.Font = new System.Drawing.Font("Arial", 7F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel9.LocationFloat = new DevExpress.Utils.PointFloat(853.1276F, 86.58339F);
            this.xrLabel9.Name = "xrLabel9";
            this.xrLabel9.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel9.SizeF = new System.Drawing.SizeF(68.87257F, 15F);
            this.xrLabel9.StylePriority.UseFont = false;
            this.xrLabel9.Text = "Fecha:";
            // 
            // xrLine1
            // 
            this.xrLine1.Dpi = 100F;
            this.xrLine1.LocationFloat = new DevExpress.Utils.PointFloat(0F, 76.5834F);
            this.xrLine1.Name = "xrLine1";
            this.xrLine1.SizeF = new System.Drawing.SizeF(1068.8F, 9.999992F);
            this.xrLine1.StylePriority.UseBorderDashStyle = false;
            // 
            // _labelReporte
            // 
            this._labelReporte.Dpi = 100F;
            this._labelReporte.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._labelReporte.LocationFloat = new DevExpress.Utils.PointFloat(0F, 0F);
            this._labelReporte.Name = "_labelReporte";
            this._labelReporte.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this._labelReporte.SizeF = new System.Drawing.SizeF(406.2806F, 20F);
            this._labelReporte.StylePriority.UseFont = false;
            this._labelReporte.Text = "Reporte de Facturas";
            // 
            // xrLabel4
            // 
            this.xrLabel4.Dpi = 100F;
            this.xrLabel4.Font = new System.Drawing.Font("Arial", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel4.LocationFloat = new DevExpress.Utils.PointFloat(835.1743F, 30.00002F);
            this.xrLabel4.Name = "xrLabel4";
            this.xrLabel4.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel4.SizeF = new System.Drawing.SizeF(86.82587F, 15F);
            this.xrLabel4.StylePriority.UseFont = false;
            this.xrLabel4.StylePriority.UseTextAlignment = false;
            this.xrLabel4.Text = "DFL-950802-5N4";
            this.xrLabel4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // siteLabel
            // 
            this.siteLabel.Dpi = 100F;
            this.siteLabel.Font = new System.Drawing.Font("Arial", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.siteLabel.LocationFloat = new DevExpress.Utils.PointFloat(721.0001F, 15.00003F);
            this.siteLabel.Name = "siteLabel";
            this.siteLabel.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.siteLabel.SizeF = new System.Drawing.SizeF(201.0001F, 15F);
            this.siteLabel.StylePriority.UseFont = false;
            this.siteLabel.StylePriority.UseTextAlignment = false;
            this.siteLabel.Text = "*FLORIDO CEDIS";
            this.siteLabel.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // xrLabel3
            // 
            this.xrLabel3.Dpi = 100F;
            this.xrLabel3.Font = new System.Drawing.Font("Arial", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel3.LocationFloat = new DevExpress.Utils.PointFloat(717.8945F, 1.589457E-05F);
            this.xrLabel3.Name = "xrLabel3";
            this.xrLabel3.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel3.SizeF = new System.Drawing.SizeF(204.1053F, 15F);
            this.xrLabel3.StylePriority.UseFont = false;
            this.xrLabel3.StylePriority.UseTextAlignment = false;
            this.xrLabel3.Text = "DISTRIBUIDORA EL FLORIDO S.A. DE C.V.";
            this.xrLabel3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // xrPictureBox1
            // 
            this.xrPictureBox1.Dpi = 100F;
            this.xrPictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("xrPictureBox1.Image")));
            this.xrPictureBox1.LocationFloat = new DevExpress.Utils.PointFloat(926.7941F, 1.589457E-05F);
            this.xrPictureBox1.Name = "xrPictureBox1";
            this.xrPictureBox1.SizeF = new System.Drawing.SizeF(92.58575F, 61.66666F);
            this.xrPictureBox1.Sizing = DevExpress.XtraPrinting.ImageSizeMode.ZoomImage;
            // 
            // GroupFooter1
            // 
            this.GroupFooter1.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this._exeTotal,
            this._exeCash,
            this._exeCredit,
            this.xrLabel38,
            this.lbl_header_credito,
            this.xrLabel16,
            this.xrLabel20,
            this.xrLabel21,
            this.xrLabel22,
            this.xrLabel23,
            this.lbl_credit_zrate,
            this.lbl_credit_taxed,
            this.lbl_credit_tax,
            this.xrLabel26,
            this.lbl_cash_zrate,
            this.lbl_cash_taxed,
            this.lbl_cash_tax,
            this.xrLabel27,
            this.lbl_total_zrate,
            this.lbl_total_taxed,
            this.lbl_credit_ieps,
            this.xrLabel28,
            this.lbl_total_tax,
            this.lbl_total_ieps,
            this.lbl_cash_ieps,
            this.lbl_total_cash,
            this.lbl_total_global,
            this.lbl_total_credit,
            this.iepsLabel,
            this.xrLabel31,
            this.xrLabel32,
            this.xrLabel33,
            this.subtotalLabel,
            this.totalLabel,
            this.xrLabel29,
            this.ivaLabel});
            this.GroupFooter1.Dpi = 100F;
            this.GroupFooter1.HeightF = 229.1506F;
            this.GroupFooter1.Name = "GroupFooter1";
            // 
            // _exeTotal
            // 
            this._exeTotal.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this._exeTotal.Dpi = 100F;
            this._exeTotal.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._exeTotal.LocationFloat = new DevExpress.Utils.PointFloat(276.4702F, 101.8426F);
            this._exeTotal.Name = "_exeTotal";
            this._exeTotal.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this._exeTotal.SizeF = new System.Drawing.SizeF(120F, 20F);
            this._exeTotal.StylePriority.UseBorders = false;
            this._exeTotal.StylePriority.UseFont = false;
            this._exeTotal.StylePriority.UseTextAlignment = false;
            this._exeTotal.Text = "$999,999,999.99";
            this._exeTotal.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // _exeCash
            // 
            this._exeCash.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this._exeCash.Dpi = 100F;
            this._exeCash.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._exeCash.LocationFloat = new DevExpress.Utils.PointFloat(176.4702F, 101.8426F);
            this._exeCash.Name = "_exeCash";
            this._exeCash.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this._exeCash.SizeF = new System.Drawing.SizeF(100F, 20F);
            this._exeCash.StylePriority.UseBorders = false;
            this._exeCash.StylePriority.UseFont = false;
            this._exeCash.StylePriority.UseTextAlignment = false;
            this._exeCash.Text = "$999,999.99";
            this._exeCash.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // _exeCredit
            // 
            this._exeCredit.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this._exeCredit.Dpi = 100F;
            this._exeCredit.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._exeCredit.LocationFloat = new DevExpress.Utils.PointFloat(76.47015F, 101.8426F);
            this._exeCredit.Name = "_exeCredit";
            this._exeCredit.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this._exeCredit.SizeF = new System.Drawing.SizeF(100F, 20F);
            this._exeCredit.StylePriority.UseBorders = false;
            this._exeCredit.StylePriority.UseFont = false;
            this._exeCredit.StylePriority.UseTextAlignment = false;
            this._exeCredit.Text = "$999,999.99";
            this._exeCredit.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // xrLabel38
            // 
            this.xrLabel38.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel38.Dpi = 100F;
            this.xrLabel38.Font = new System.Drawing.Font("Arial", 8.25F, System.Drawing.FontStyle.Bold);
            this.xrLabel38.LocationFloat = new DevExpress.Utils.PointFloat(0F, 101.8426F);
            this.xrLabel38.Name = "xrLabel38";
            this.xrLabel38.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel38.SizeF = new System.Drawing.SizeF(76.47031F, 20F);
            this.xrLabel38.StylePriority.UseBorders = false;
            this.xrLabel38.StylePriority.UseFont = false;
            this.xrLabel38.StylePriority.UseTextAlignment = false;
            this.xrLabel38.Text = "Exento:";
            this.xrLabel38.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // lbl_header_credito
            // 
            this.lbl_header_credito.BackColor = System.Drawing.Color.White;
            this.lbl_header_credito.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Top | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.lbl_header_credito.Dpi = 100F;
            this.lbl_header_credito.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_header_credito.ForeColor = System.Drawing.Color.Black;
            this.lbl_header_credito.LocationFloat = new DevExpress.Utils.PointFloat(76.47031F, 41.84253F);
            this.lbl_header_credito.Name = "lbl_header_credito";
            this.lbl_header_credito.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lbl_header_credito.SizeF = new System.Drawing.SizeF(99.99999F, 20F);
            this.lbl_header_credito.StylePriority.UseBackColor = false;
            this.lbl_header_credito.StylePriority.UseBorders = false;
            this.lbl_header_credito.StylePriority.UseFont = false;
            this.lbl_header_credito.StylePriority.UseForeColor = false;
            this.lbl_header_credito.StylePriority.UseTextAlignment = false;
            this.lbl_header_credito.Text = "Crédito";
            this.lbl_header_credito.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            // 
            // xrLabel16
            // 
            this.xrLabel16.BackColor = System.Drawing.Color.DarkGray;
            this.xrLabel16.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel16.Dpi = 100F;
            this.xrLabel16.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel16.ForeColor = System.Drawing.Color.White;
            this.xrLabel16.LocationFloat = new DevExpress.Utils.PointFloat(0F, 41.84253F);
            this.xrLabel16.Name = "xrLabel16";
            this.xrLabel16.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel16.SizeF = new System.Drawing.SizeF(76.47031F, 20F);
            this.xrLabel16.StylePriority.UseBackColor = false;
            this.xrLabel16.StylePriority.UseBorders = false;
            this.xrLabel16.StylePriority.UseFont = false;
            this.xrLabel16.StylePriority.UseForeColor = false;
            // 
            // xrLabel20
            // 
            this.xrLabel20.BackColor = System.Drawing.Color.Transparent;
            this.xrLabel20.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Top | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel20.Dpi = 100F;
            this.xrLabel20.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel20.ForeColor = System.Drawing.Color.Black;
            this.xrLabel20.LocationFloat = new DevExpress.Utils.PointFloat(176.4703F, 41.84253F);
            this.xrLabel20.Name = "xrLabel20";
            this.xrLabel20.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel20.SizeF = new System.Drawing.SizeF(100F, 20F);
            this.xrLabel20.StylePriority.UseBackColor = false;
            this.xrLabel20.StylePriority.UseBorders = false;
            this.xrLabel20.StylePriority.UseFont = false;
            this.xrLabel20.StylePriority.UseForeColor = false;
            this.xrLabel20.StylePriority.UseTextAlignment = false;
            this.xrLabel20.Text = "Contado";
            this.xrLabel20.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            // 
            // xrLabel21
            // 
            this.xrLabel21.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel21.Dpi = 100F;
            this.xrLabel21.Font = new System.Drawing.Font("Arial", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel21.LocationFloat = new DevExpress.Utils.PointFloat(0F, 61.84247F);
            this.xrLabel21.Name = "xrLabel21";
            this.xrLabel21.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel21.SizeF = new System.Drawing.SizeF(76.47031F, 20F);
            this.xrLabel21.StylePriority.UseBorders = false;
            this.xrLabel21.StylePriority.UseFont = false;
            this.xrLabel21.StylePriority.UseTextAlignment = false;
            this.xrLabel21.Text = "Tasa Cero:";
            this.xrLabel21.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // xrLabel22
            // 
            this.xrLabel22.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel22.Dpi = 100F;
            this.xrLabel22.Font = new System.Drawing.Font("Arial", 8.25F, System.Drawing.FontStyle.Bold);
            this.xrLabel22.LocationFloat = new DevExpress.Utils.PointFloat(0F, 81.84255F);
            this.xrLabel22.Name = "xrLabel22";
            this.xrLabel22.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel22.SizeF = new System.Drawing.SizeF(76.47031F, 20F);
            this.xrLabel22.StylePriority.UseBorders = false;
            this.xrLabel22.StylePriority.UseFont = false;
            this.xrLabel22.StylePriority.UseTextAlignment = false;
            this.xrLabel22.Text = "Gravado:";
            this.xrLabel22.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // xrLabel23
            // 
            this.xrLabel23.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel23.Dpi = 100F;
            this.xrLabel23.Font = new System.Drawing.Font("Arial", 8.25F, System.Drawing.FontStyle.Bold);
            this.xrLabel23.LocationFloat = new DevExpress.Utils.PointFloat(0F, 121.8426F);
            this.xrLabel23.Name = "xrLabel23";
            this.xrLabel23.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel23.SizeF = new System.Drawing.SizeF(76.47031F, 20F);
            this.xrLabel23.StylePriority.UseBorders = false;
            this.xrLabel23.StylePriority.UseFont = false;
            this.xrLabel23.StylePriority.UseTextAlignment = false;
            this.xrLabel23.Text = "IVA:";
            this.xrLabel23.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // lbl_credit_zrate
            // 
            this.lbl_credit_zrate.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.lbl_credit_zrate.Dpi = 100F;
            this.lbl_credit_zrate.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_credit_zrate.LocationFloat = new DevExpress.Utils.PointFloat(76.47025F, 61.84247F);
            this.lbl_credit_zrate.Name = "lbl_credit_zrate";
            this.lbl_credit_zrate.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lbl_credit_zrate.SizeF = new System.Drawing.SizeF(100F, 20F);
            this.lbl_credit_zrate.StylePriority.UseBorders = false;
            this.lbl_credit_zrate.StylePriority.UseFont = false;
            this.lbl_credit_zrate.StylePriority.UseTextAlignment = false;
            this.lbl_credit_zrate.Text = "$999,999.99";
            this.lbl_credit_zrate.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // lbl_credit_taxed
            // 
            this.lbl_credit_taxed.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.lbl_credit_taxed.Dpi = 100F;
            this.lbl_credit_taxed.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_credit_taxed.LocationFloat = new DevExpress.Utils.PointFloat(76.47025F, 81.84255F);
            this.lbl_credit_taxed.Name = "lbl_credit_taxed";
            this.lbl_credit_taxed.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lbl_credit_taxed.SizeF = new System.Drawing.SizeF(100F, 20F);
            this.lbl_credit_taxed.StylePriority.UseBorders = false;
            this.lbl_credit_taxed.StylePriority.UseFont = false;
            this.lbl_credit_taxed.StylePriority.UseTextAlignment = false;
            this.lbl_credit_taxed.Text = "$999,999.99";
            this.lbl_credit_taxed.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // lbl_credit_tax
            // 
            this.lbl_credit_tax.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.lbl_credit_tax.Dpi = 100F;
            this.lbl_credit_tax.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_credit_tax.LocationFloat = new DevExpress.Utils.PointFloat(76.47031F, 121.8426F);
            this.lbl_credit_tax.Name = "lbl_credit_tax";
            this.lbl_credit_tax.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lbl_credit_tax.SizeF = new System.Drawing.SizeF(100F, 20F);
            this.lbl_credit_tax.StylePriority.UseBorders = false;
            this.lbl_credit_tax.StylePriority.UseFont = false;
            this.lbl_credit_tax.StylePriority.UseTextAlignment = false;
            this.lbl_credit_tax.Text = "$999,999.99";
            this.lbl_credit_tax.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // xrLabel26
            // 
            this.xrLabel26.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel26.Dpi = 100F;
            this.xrLabel26.Font = new System.Drawing.Font("Arial", 8.25F, System.Drawing.FontStyle.Bold);
            this.xrLabel26.LocationFloat = new DevExpress.Utils.PointFloat(6.357829E-05F, 141.8426F);
            this.xrLabel26.Name = "xrLabel26";
            this.xrLabel26.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel26.SizeF = new System.Drawing.SizeF(76.47025F, 20F);
            this.xrLabel26.StylePriority.UseBorders = false;
            this.xrLabel26.StylePriority.UseFont = false;
            this.xrLabel26.StylePriority.UseTextAlignment = false;
            this.xrLabel26.Text = "IEPS:";
            this.xrLabel26.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // lbl_cash_zrate
            // 
            this.lbl_cash_zrate.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.lbl_cash_zrate.Dpi = 100F;
            this.lbl_cash_zrate.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_cash_zrate.LocationFloat = new DevExpress.Utils.PointFloat(176.4703F, 61.84247F);
            this.lbl_cash_zrate.Name = "lbl_cash_zrate";
            this.lbl_cash_zrate.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lbl_cash_zrate.SizeF = new System.Drawing.SizeF(100F, 20F);
            this.lbl_cash_zrate.StylePriority.UseBorders = false;
            this.lbl_cash_zrate.StylePriority.UseFont = false;
            this.lbl_cash_zrate.StylePriority.UseTextAlignment = false;
            this.lbl_cash_zrate.Text = "$999,999.99";
            this.lbl_cash_zrate.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // lbl_cash_taxed
            // 
            this.lbl_cash_taxed.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.lbl_cash_taxed.Dpi = 100F;
            this.lbl_cash_taxed.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_cash_taxed.LocationFloat = new DevExpress.Utils.PointFloat(176.4703F, 81.84255F);
            this.lbl_cash_taxed.Name = "lbl_cash_taxed";
            this.lbl_cash_taxed.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lbl_cash_taxed.SizeF = new System.Drawing.SizeF(100F, 20F);
            this.lbl_cash_taxed.StylePriority.UseBorders = false;
            this.lbl_cash_taxed.StylePriority.UseFont = false;
            this.lbl_cash_taxed.StylePriority.UseTextAlignment = false;
            this.lbl_cash_taxed.Text = "$999,999.99";
            this.lbl_cash_taxed.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // lbl_cash_tax
            // 
            this.lbl_cash_tax.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.lbl_cash_tax.Dpi = 100F;
            this.lbl_cash_tax.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_cash_tax.LocationFloat = new DevExpress.Utils.PointFloat(176.4703F, 121.8426F);
            this.lbl_cash_tax.Name = "lbl_cash_tax";
            this.lbl_cash_tax.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lbl_cash_tax.SizeF = new System.Drawing.SizeF(100F, 20F);
            this.lbl_cash_tax.StylePriority.UseBorders = false;
            this.lbl_cash_tax.StylePriority.UseFont = false;
            this.lbl_cash_tax.StylePriority.UseTextAlignment = false;
            this.lbl_cash_tax.Text = "$999,999.99";
            this.lbl_cash_tax.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // xrLabel27
            // 
            this.xrLabel27.BackColor = System.Drawing.Color.Transparent;
            this.xrLabel27.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Top | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel27.Dpi = 100F;
            this.xrLabel27.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel27.ForeColor = System.Drawing.Color.Black;
            this.xrLabel27.LocationFloat = new DevExpress.Utils.PointFloat(276.4703F, 41.84253F);
            this.xrLabel27.Name = "xrLabel27";
            this.xrLabel27.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel27.SizeF = new System.Drawing.SizeF(120F, 20F);
            this.xrLabel27.StylePriority.UseBackColor = false;
            this.xrLabel27.StylePriority.UseBorders = false;
            this.xrLabel27.StylePriority.UseFont = false;
            this.xrLabel27.StylePriority.UseForeColor = false;
            this.xrLabel27.StylePriority.UseTextAlignment = false;
            this.xrLabel27.Text = "Totales";
            this.xrLabel27.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            // 
            // lbl_total_zrate
            // 
            this.lbl_total_zrate.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.lbl_total_zrate.Dpi = 100F;
            this.lbl_total_zrate.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_total_zrate.LocationFloat = new DevExpress.Utils.PointFloat(276.4703F, 61.84247F);
            this.lbl_total_zrate.Name = "lbl_total_zrate";
            this.lbl_total_zrate.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lbl_total_zrate.SizeF = new System.Drawing.SizeF(120F, 20F);
            this.lbl_total_zrate.StylePriority.UseBorders = false;
            this.lbl_total_zrate.StylePriority.UseFont = false;
            this.lbl_total_zrate.StylePriority.UseTextAlignment = false;
            this.lbl_total_zrate.Text = "$999,999,999.99";
            this.lbl_total_zrate.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // lbl_total_taxed
            // 
            this.lbl_total_taxed.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.lbl_total_taxed.Dpi = 100F;
            this.lbl_total_taxed.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_total_taxed.LocationFloat = new DevExpress.Utils.PointFloat(276.4703F, 81.84255F);
            this.lbl_total_taxed.Name = "lbl_total_taxed";
            this.lbl_total_taxed.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lbl_total_taxed.SizeF = new System.Drawing.SizeF(120F, 20F);
            this.lbl_total_taxed.StylePriority.UseBorders = false;
            this.lbl_total_taxed.StylePriority.UseFont = false;
            this.lbl_total_taxed.StylePriority.UseTextAlignment = false;
            this.lbl_total_taxed.Text = "$999,999,999.99";
            this.lbl_total_taxed.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // lbl_credit_ieps
            // 
            this.lbl_credit_ieps.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.lbl_credit_ieps.Dpi = 100F;
            this.lbl_credit_ieps.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_credit_ieps.LocationFloat = new DevExpress.Utils.PointFloat(76.47031F, 141.8426F);
            this.lbl_credit_ieps.Name = "lbl_credit_ieps";
            this.lbl_credit_ieps.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lbl_credit_ieps.SizeF = new System.Drawing.SizeF(99.99999F, 20F);
            this.lbl_credit_ieps.StylePriority.UseBorders = false;
            this.lbl_credit_ieps.StylePriority.UseFont = false;
            this.lbl_credit_ieps.StylePriority.UseTextAlignment = false;
            this.lbl_credit_ieps.Text = "$999,999.99";
            this.lbl_credit_ieps.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // xrLabel28
            // 
            this.xrLabel28.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel28.Dpi = 100F;
            this.xrLabel28.Font = new System.Drawing.Font("Arial", 8.25F, System.Drawing.FontStyle.Bold);
            this.xrLabel28.LocationFloat = new DevExpress.Utils.PointFloat(6.357829E-05F, 161.8427F);
            this.xrLabel28.Name = "xrLabel28";
            this.xrLabel28.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel28.SizeF = new System.Drawing.SizeF(76.47025F, 20F);
            this.xrLabel28.StylePriority.UseBorders = false;
            this.xrLabel28.StylePriority.UseFont = false;
            this.xrLabel28.StylePriority.UseTextAlignment = false;
            this.xrLabel28.Text = "Totales:";
            this.xrLabel28.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // lbl_total_tax
            // 
            this.lbl_total_tax.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.lbl_total_tax.Dpi = 100F;
            this.lbl_total_tax.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_total_tax.LocationFloat = new DevExpress.Utils.PointFloat(276.4702F, 121.8426F);
            this.lbl_total_tax.Name = "lbl_total_tax";
            this.lbl_total_tax.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lbl_total_tax.SizeF = new System.Drawing.SizeF(120.0001F, 20F);
            this.lbl_total_tax.StylePriority.UseBorders = false;
            this.lbl_total_tax.StylePriority.UseFont = false;
            this.lbl_total_tax.StylePriority.UseTextAlignment = false;
            this.lbl_total_tax.Text = "$999,999,999.99";
            this.lbl_total_tax.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // lbl_total_ieps
            // 
            this.lbl_total_ieps.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.lbl_total_ieps.Dpi = 100F;
            this.lbl_total_ieps.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_total_ieps.LocationFloat = new DevExpress.Utils.PointFloat(276.4702F, 141.8426F);
            this.lbl_total_ieps.Name = "lbl_total_ieps";
            this.lbl_total_ieps.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lbl_total_ieps.SizeF = new System.Drawing.SizeF(120.0001F, 20F);
            this.lbl_total_ieps.StylePriority.UseBorders = false;
            this.lbl_total_ieps.StylePriority.UseFont = false;
            this.lbl_total_ieps.StylePriority.UseTextAlignment = false;
            this.lbl_total_ieps.Text = "$999,999,999.99";
            this.lbl_total_ieps.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // lbl_cash_ieps
            // 
            this.lbl_cash_ieps.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.lbl_cash_ieps.Dpi = 100F;
            this.lbl_cash_ieps.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_cash_ieps.LocationFloat = new DevExpress.Utils.PointFloat(176.4703F, 141.8426F);
            this.lbl_cash_ieps.Name = "lbl_cash_ieps";
            this.lbl_cash_ieps.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lbl_cash_ieps.SizeF = new System.Drawing.SizeF(100F, 20F);
            this.lbl_cash_ieps.StylePriority.UseBorders = false;
            this.lbl_cash_ieps.StylePriority.UseFont = false;
            this.lbl_cash_ieps.StylePriority.UseTextAlignment = false;
            this.lbl_cash_ieps.Text = "$999,999.99";
            this.lbl_cash_ieps.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // lbl_total_cash
            // 
            this.lbl_total_cash.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.lbl_total_cash.Dpi = 100F;
            this.lbl_total_cash.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_total_cash.LocationFloat = new DevExpress.Utils.PointFloat(176.4704F, 161.8426F);
            this.lbl_total_cash.Name = "lbl_total_cash";
            this.lbl_total_cash.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lbl_total_cash.SizeF = new System.Drawing.SizeF(99.99994F, 20F);
            this.lbl_total_cash.StylePriority.UseBorders = false;
            this.lbl_total_cash.StylePriority.UseFont = false;
            this.lbl_total_cash.StylePriority.UseTextAlignment = false;
            this.lbl_total_cash.Text = "$999,999.99";
            this.lbl_total_cash.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // lbl_total_global
            // 
            this.lbl_total_global.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.lbl_total_global.Dpi = 100F;
            this.lbl_total_global.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_total_global.LocationFloat = new DevExpress.Utils.PointFloat(276.4702F, 161.8426F);
            this.lbl_total_global.Name = "lbl_total_global";
            this.lbl_total_global.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lbl_total_global.SizeF = new System.Drawing.SizeF(120.0001F, 20F);
            this.lbl_total_global.StylePriority.UseBorders = false;
            this.lbl_total_global.StylePriority.UseFont = false;
            this.lbl_total_global.StylePriority.UseTextAlignment = false;
            this.lbl_total_global.Text = "$999,999,999.99";
            this.lbl_total_global.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // lbl_total_credit
            // 
            this.lbl_total_credit.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.lbl_total_credit.Dpi = 100F;
            this.lbl_total_credit.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_total_credit.LocationFloat = new DevExpress.Utils.PointFloat(76.47038F, 161.8426F);
            this.lbl_total_credit.Name = "lbl_total_credit";
            this.lbl_total_credit.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lbl_total_credit.SizeF = new System.Drawing.SizeF(99.99993F, 20F);
            this.lbl_total_credit.StylePriority.UseBorders = false;
            this.lbl_total_credit.StylePriority.UseFont = false;
            this.lbl_total_credit.StylePriority.UseTextAlignment = false;
            this.lbl_total_credit.Text = "$999,999.99";
            this.lbl_total_credit.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // iepsLabel
            // 
            this.iepsLabel.Dpi = 100F;
            this.iepsLabel.Font = new System.Drawing.Font("Arial", 8F);
            this.iepsLabel.LocationFloat = new DevExpress.Utils.PointFloat(903.8048F, 71.84258F);
            this.iepsLabel.Name = "iepsLabel";
            this.iepsLabel.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.iepsLabel.SizeF = new System.Drawing.SizeF(118.1951F, 15F);
            this.iepsLabel.StylePriority.UseFont = false;
            this.iepsLabel.StylePriority.UseTextAlignment = false;
            this.iepsLabel.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // xrLabel31
            // 
            this.xrLabel31.Dpi = 100F;
            this.xrLabel31.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Bold);
            this.xrLabel31.LocationFloat = new DevExpress.Utils.PointFloat(875.3879F, 56.8426F);
            this.xrLabel31.Name = "xrLabel31";
            this.xrLabel31.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel31.SizeF = new System.Drawing.SizeF(27F, 15F);
            this.xrLabel31.StylePriority.UseFont = false;
            this.xrLabel31.Text = "IVA:";
            // 
            // xrLabel32
            // 
            this.xrLabel32.Dpi = 100F;
            this.xrLabel32.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Bold);
            this.xrLabel32.LocationFloat = new DevExpress.Utils.PointFloat(868.3881F, 71.84258F);
            this.xrLabel32.Name = "xrLabel32";
            this.xrLabel32.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel32.SizeF = new System.Drawing.SizeF(34F, 15F);
            this.xrLabel32.StylePriority.UseFont = false;
            this.xrLabel32.Text = "IEPS:";
            // 
            // xrLabel33
            // 
            this.xrLabel33.Dpi = 100F;
            this.xrLabel33.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold);
            this.xrLabel33.LocationFloat = new DevExpress.Utils.PointFloat(849.388F, 86.84254F);
            this.xrLabel33.Name = "xrLabel33";
            this.xrLabel33.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel33.SizeF = new System.Drawing.SizeF(54.41675F, 15F);
            this.xrLabel33.StylePriority.UseFont = false;
            this.xrLabel33.StylePriority.UseTextAlignment = false;
            this.xrLabel33.Text = "Total:";
            this.xrLabel33.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
            // 
            // subtotalLabel
            // 
            this.subtotalLabel.Dpi = 100F;
            this.subtotalLabel.Font = new System.Drawing.Font("Arial", 8F);
            this.subtotalLabel.LocationFloat = new DevExpress.Utils.PointFloat(903.8047F, 41.84253F);
            this.subtotalLabel.Name = "subtotalLabel";
            this.subtotalLabel.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.subtotalLabel.SizeF = new System.Drawing.SizeF(118.1951F, 15F);
            this.subtotalLabel.StylePriority.UseFont = false;
            this.subtotalLabel.StylePriority.UseTextAlignment = false;
            this.subtotalLabel.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // totalLabel
            // 
            this.totalLabel.Dpi = 100F;
            this.totalLabel.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold);
            this.totalLabel.LocationFloat = new DevExpress.Utils.PointFloat(903.8048F, 86.8426F);
            this.totalLabel.Name = "totalLabel";
            this.totalLabel.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.totalLabel.SizeF = new System.Drawing.SizeF(118.1951F, 15F);
            this.totalLabel.StylePriority.UseFont = false;
            this.totalLabel.StylePriority.UseTextAlignment = false;
            this.totalLabel.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // xrLabel29
            // 
            this.xrLabel29.Dpi = 100F;
            this.xrLabel29.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Bold);
            this.xrLabel29.LocationFloat = new DevExpress.Utils.PointFloat(849.388F, 41.84248F);
            this.xrLabel29.Name = "xrLabel29";
            this.xrLabel29.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel29.SizeF = new System.Drawing.SizeF(53F, 15F);
            this.xrLabel29.StylePriority.UseFont = false;
            this.xrLabel29.Text = "Subtotal:";
            // 
            // ivaLabel
            // 
            this.ivaLabel.Dpi = 100F;
            this.ivaLabel.Font = new System.Drawing.Font("Arial", 8F);
            this.ivaLabel.LocationFloat = new DevExpress.Utils.PointFloat(903.8047F, 56.84268F);
            this.ivaLabel.Name = "ivaLabel";
            this.ivaLabel.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.ivaLabel.SizeF = new System.Drawing.SizeF(118.1953F, 15F);
            this.ivaLabel.StylePriority.UseFont = false;
            this.ivaLabel.StylePriority.UseTextAlignment = false;
            this.ivaLabel.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // PageFooter
            // 
            this.PageFooter.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrPageInfo1});
            this.PageFooter.Dpi = 100F;
            this.PageFooter.HeightF = 15.625F;
            this.PageFooter.Name = "PageFooter";
            // 
            // xrPageInfo1
            // 
            this.xrPageInfo1.Dpi = 100F;
            this.xrPageInfo1.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Bold);
            this.xrPageInfo1.Format = "Página {0} de {1}";
            this.xrPageInfo1.LocationFloat = new DevExpress.Utils.PointFloat(0F, 0F);
            this.xrPageInfo1.Name = "xrPageInfo1";
            this.xrPageInfo1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrPageInfo1.SizeF = new System.Drawing.SizeF(100F, 15F);
            this.xrPageInfo1.StylePriority.UseFont = false;
            // 
            // NewInvoicesReport
            // 
            this.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.Detail,
            this.TopMargin,
            this.BottomMargin,
            this.ReportHeader,
            this.GroupFooter1,
            this.PageFooter});
            this.Landscape = true;
            this.Margins = new System.Drawing.Printing.Margins(15, 10, 30, 55);
            this.PageHeight = 850;
            this.PageWidth = 1100;
            this.Version = "16.2";
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

        }

        #endregion

        private DevExpress.XtraReports.UI.DetailBand Detail;
        private DevExpress.XtraReports.UI.TopMarginBand TopMargin;
        private DevExpress.XtraReports.UI.BottomMarginBand BottomMargin;
        private DevExpress.XtraReports.UI.ReportHeaderBand ReportHeader;
        private DevExpress.XtraReports.UI.GroupFooterBand GroupFooter1;
        private DevExpress.XtraReports.UI.PageFooterBand PageFooter;
        private DevExpress.XtraReports.UI.XRPageInfo xrPageInfo1;
        private DevExpress.XtraReports.UI.XRLabel labelDate;
        private DevExpress.XtraReports.UI.XRLabel xrLabel9;
        private DevExpress.XtraReports.UI.XRLine xrLine1;
        private DevExpress.XtraReports.UI.XRLabel _labelReporte;
        private DevExpress.XtraReports.UI.XRLabel xrLabel4;
        private DevExpress.XtraReports.UI.XRLabel siteLabel;
        private DevExpress.XtraReports.UI.XRLabel xrLabel3;
        private DevExpress.XtraReports.UI.XRPictureBox xrPictureBox1;
        private DevExpress.XtraReports.UI.XRLabel iepsLabel;
        private DevExpress.XtraReports.UI.XRLabel xrLabel31;
        private DevExpress.XtraReports.UI.XRLabel xrLabel32;
        private DevExpress.XtraReports.UI.XRLabel xrLabel33;
        private DevExpress.XtraReports.UI.XRLabel subtotalLabel;
        private DevExpress.XtraReports.UI.XRLabel totalLabel;
        private DevExpress.XtraReports.UI.XRLabel xrLabel29;
        private DevExpress.XtraReports.UI.XRLabel ivaLabel;
        private DevExpress.XtraReports.UI.XRLabel labelDate1;
        private DevExpress.XtraReports.UI.XRLabel labelDate2;
        private DevExpress.XtraReports.UI.XRLabel xrLabel7;
        private DevExpress.XtraReports.UI.XRLabel xrLabel5;
        private DevExpress.XtraReports.UI.XRLabel lbl_header_credito;
        private DevExpress.XtraReports.UI.XRLabel xrLabel16;
        private DevExpress.XtraReports.UI.XRLabel xrLabel20;
        private DevExpress.XtraReports.UI.XRLabel xrLabel21;
        private DevExpress.XtraReports.UI.XRLabel xrLabel22;
        private DevExpress.XtraReports.UI.XRLabel xrLabel23;
        private DevExpress.XtraReports.UI.XRLabel lbl_credit_zrate;
        private DevExpress.XtraReports.UI.XRLabel lbl_credit_taxed;
        private DevExpress.XtraReports.UI.XRLabel lbl_credit_tax;
        private DevExpress.XtraReports.UI.XRLabel xrLabel26;
        private DevExpress.XtraReports.UI.XRLabel lbl_cash_zrate;
        private DevExpress.XtraReports.UI.XRLabel lbl_cash_taxed;
        private DevExpress.XtraReports.UI.XRLabel lbl_cash_tax;
        private DevExpress.XtraReports.UI.XRLabel xrLabel27;
        private DevExpress.XtraReports.UI.XRLabel lbl_total_zrate;
        private DevExpress.XtraReports.UI.XRLabel lbl_total_taxed;
        private DevExpress.XtraReports.UI.XRLabel lbl_credit_ieps;
        private DevExpress.XtraReports.UI.XRLabel xrLabel28;
        private DevExpress.XtraReports.UI.XRLabel lbl_total_tax;
        private DevExpress.XtraReports.UI.XRLabel lbl_total_ieps;
        private DevExpress.XtraReports.UI.XRLabel lbl_cash_ieps;
        private DevExpress.XtraReports.UI.XRLabel lbl_total_cash;
        private DevExpress.XtraReports.UI.XRLabel lbl_total_global;
        private DevExpress.XtraReports.UI.XRLabel lbl_total_credit;
        private DevExpress.XtraReports.UI.XRLabel _exeTotal;
        private DevExpress.XtraReports.UI.XRLabel _exeCash;
        private DevExpress.XtraReports.UI.XRLabel _exeCredit;
        private DevExpress.XtraReports.UI.XRLabel xrLabel38;
    }
}
