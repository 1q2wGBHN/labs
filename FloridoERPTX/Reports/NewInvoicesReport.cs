﻿using System;
using System.Data;
using DevExpress.XtraReports.UI;
using App.Entities.ViewModels.Invoices;
using DevExpress.XtraPrinting;
using System.Drawing;


namespace FloridoERPTX.Reports
{
    public partial class NewInvoicesReport : DevExpress.XtraReports.UI.XtraReport
    {

        public DataSet DataSetTableOrdered = null;
        decimal ivaValue, iepsValue, totalValue, subTotal = 0;
        public NewInvoicesReport()
        {
            InitializeComponent();

        }

        public DataSet printTable(InvoiceIndex model, string siteName) //se encarga de imprimir la orden del proveedor
        {
            try
            {
                labelDate.Text = DateTime.Now.Date.ToShortDateString();
                labelDate1.Text = model.StartDate;
                labelDate2.Text = model.EndDate;
                siteLabel.Text = siteName;

                lbl_credit_zrate.Text = model.Totales[0, 0].ToString("C");
                lbl_credit_taxed.Text = model.Totales[1, 0].ToString("C");
                _exeCredit.Text = model.Totales[2, 0].ToString("C");
                lbl_credit_tax.Text = model.Totales[3, 0].ToString("C");
                lbl_credit_ieps.Text = model.Totales[4, 0].ToString("C");
                lbl_total_credit.Text = model.Totales[5, 0].ToString("C");

                lbl_cash_zrate.Text = model.Totales[0, 1].ToString("C");
                lbl_cash_taxed.Text = model.Totales[1, 1].ToString("C");
                _exeCash.Text = model.Totales[2, 1].ToString("C");
                lbl_cash_tax.Text = model.Totales[3, 1].ToString("C");
                lbl_cash_ieps.Text = model.Totales[4, 1].ToString("C");
                lbl_total_cash.Text = model.Totales[5, 1].ToString("C");

                lbl_total_zrate.Text = model.Totales[0, 2].ToString("C");
                lbl_total_taxed.Text = model.Totales[1, 2].ToString("C");
                _exeTotal.Text = model.Totales[2, 2].ToString("C");
                lbl_total_tax.Text = model.Totales[3, 2].ToString("C");
                lbl_total_ieps.Text = model.Totales[4, 2].ToString("C");
                lbl_total_global.Text = model.Totales[5, 2].ToString("C");

                float InvoiceNoWidth = 50F, DateWidth = 70F, ClientCodeWidth = 80F, ClientNameWidth = 145F, DueDateWidth = 80F, AmountWidth = 60F, IEPSWidth = 55F;
                float TaxWidth = 50F, TotalWidth = 50F, CreditWidth = 50F, CashWidth = 55F, PaymentWidth = 55F, BalanceWidht = 50F;
                float CurrencyWidht = 50F, StampWidth = 70F, StatusWidth = 50F;

                // Create an empty table and set its size.
                XRTable dynamicTable = new XRTable();
                dynamicTable.SizeF = new SizeF(1075, 18);

                // Start table initialization.
                dynamicTable.BeginInit();

                // Enable table borders to see its boundaries.
                dynamicTable.BorderWidth = .5F;
                dynamicTable.Borders = BorderSide.Bottom;

                XRTableRow HeaderRow = new XRTableRow
                {
                    HeightF = 15F,
                    BackColor = Color.DarkGray,
                    Borders = BorderSide.Bottom
                };

                var InvoiceNoHeaderCell = new XRTableCell
                {
                    TextAlignment = TextAlignment.MiddleCenter,
                    Font = new Font(Font.FontFamily, Font.Size, FontStyle.Bold),
                    Text = "No. Factura",
                    WidthF = InvoiceNoWidth
                };
                HeaderRow.Cells.Add(InvoiceNoHeaderCell);

                var DateHeaderCell = new XRTableCell
                {
                    TextAlignment = TextAlignment.MiddleCenter,
                    Font = new Font(Font.FontFamily, Font.Size, FontStyle.Bold),
                    Text = "Fecha",
                    WidthF = DateWidth
                };
                HeaderRow.Cells.Add(DateHeaderCell);

                var ClientCodeHeaderCell = new XRTableCell
                {
                    TextAlignment = TextAlignment.MiddleCenter,
                    Font = new Font(Font.FontFamily, Font.Size, FontStyle.Bold),
                    Text = "Código de Cliente",
                    WidthF = ClientCodeWidth
                };
                HeaderRow.Cells.Add(ClientCodeHeaderCell);

                var ClientNameHeaderCell = new XRTableCell
                {
                    TextAlignment = TextAlignment.MiddleCenter,
                    Font = new Font(Font.FontFamily, Font.Size, FontStyle.Bold),
                    Text = "Cliente",
                    WidthF = ClientNameWidth
                };
                HeaderRow.Cells.Add(ClientNameHeaderCell);

                var DueDateHeaderCell = new XRTableCell
                {
                    TextAlignment = TextAlignment.MiddleCenter,
                    Font = new Font(Font.FontFamily, Font.Size, FontStyle.Bold),
                    Text = "Fecha Vencimiento",
                    WidthF = DueDateWidth
                };
                HeaderRow.Cells.Add(DueDateHeaderCell);

                var AmountHeaderCell = new XRTableCell
                {
                    TextAlignment = TextAlignment.MiddleCenter,
                    Font = new Font(Font.FontFamily, Font.Size, FontStyle.Bold),
                    Text = "Monto",
                    WidthF = AmountWidth
                };
                HeaderRow.Cells.Add(AmountHeaderCell);

                foreach(var IEPSName in model.IepsNamesList)
                {
                    var IEPSHeaderCell = new XRTableCell
                    {
                        TextAlignment = TextAlignment.MiddleCenter,
                        Font = new Font(Font.FontFamily, Font.Size, FontStyle.Bold),
                        Text = IEPSName,
                        WidthF = IEPSWidth,
                        Name = IEPSName
                    };
                    HeaderRow.Cells.Add(IEPSHeaderCell);
                }

                var TaxHeaderCell = new XRTableCell
                {
                    TextAlignment = TextAlignment.MiddleCenter,
                    Font = new Font(Font.FontFamily, Font.Size, FontStyle.Bold),
                    Text = "IVA",
                    WidthF = TaxWidth
                };
                HeaderRow.Cells.Add(TaxHeaderCell);

                var TotalHeaderCell = new XRTableCell
                {
                    TextAlignment = TextAlignment.MiddleCenter,
                    Font = new Font(Font.FontFamily, Font.Size, FontStyle.Bold),
                    Text = "Total",
                    WidthF = TotalWidth
                };
                HeaderRow.Cells.Add(TotalHeaderCell);

                var CreditHeaderCell = new XRTableCell
                {
                    TextAlignment = TextAlignment.MiddleCenter,
                    Font = new Font(Font.FontFamily, Font.Size, FontStyle.Bold),
                    Text = "Crédito",
                    WidthF = CreditWidth
                };
                HeaderRow.Cells.Add(CreditHeaderCell);

                var CashHeaderCell = new XRTableCell
                {
                    TextAlignment = TextAlignment.MiddleCenter,
                    Font = new Font(Font.FontFamily, Font.Size, FontStyle.Bold),
                    Text = "Contado",
                    WidthF = CashWidth
                };
                HeaderRow.Cells.Add(CashHeaderCell);

                var PaymentHeaderCell = new XRTableCell
                {
                    TextAlignment = TextAlignment.MiddleCenter,
                    Font = new Font(Font.FontFamily, Font.Size, FontStyle.Bold),
                    Text = "Pago",
                    WidthF = PaymentWidth
                };
                HeaderRow.Cells.Add(PaymentHeaderCell);

                var BalanceHeaderCell = new XRTableCell
                {
                    TextAlignment = TextAlignment.MiddleCenter,
                    Font = new Font(Font.FontFamily, Font.Size, FontStyle.Bold),
                    Text = "Saldo",
                    WidthF = BalanceWidht
                };
                HeaderRow.Cells.Add(BalanceHeaderCell);

                var CurrencyHeaderCell = new XRTableCell
                {
                    TextAlignment = TextAlignment.MiddleCenter,
                    Font = new Font(Font.FontFamily, Font.Size, FontStyle.Bold),
                    Text = "Moneda",
                    WidthF = CurrencyWidht
                };
                HeaderRow.Cells.Add(CurrencyHeaderCell);

                var StampNumberHeaderCell = new XRTableCell
                {
                    TextAlignment = TextAlignment.MiddleCenter,
                    Font = new Font(Font.FontFamily, Font.Size, FontStyle.Bold),
                    Text = "Folio Fiscal",
                    WidthF = StampWidth
                };
                HeaderRow.Cells.Add(StampNumberHeaderCell);

                var StatusHeaderCell = new XRTableCell
                {
                    TextAlignment = TextAlignment.MiddleCenter,
                    Font = new Font(Font.FontFamily, Font.Size, FontStyle.Bold),
                    Text = "Estado",
                    WidthF = StatusWidth
                };
                HeaderRow.Cells.Add(StatusHeaderCell);

                dynamicTable.Rows.Add(HeaderRow);

                foreach (var Item in model.InvoiceList)
                {
                    int cont = 0;
                    var dataRow = new XRTableRow { HeightF = 15F, BackColor = Color.White, Borders = BorderSide.Bottom };                    

                    var InvoiceNoDataCell = new XRTableCell
                    {
                        TextAlignment = TextAlignment.MiddleCenter,
                        Font = new Font(Font.FontFamily, Font.Size, FontStyle.Regular),
                        Text = Item.SerieAndNumber,
                        WidthF = InvoiceNoWidth
                    };
                    cont++;
                    dataRow.Cells.Add(InvoiceNoDataCell);

                    var DateDataCell = new XRTableCell
                    {
                        TextAlignment = TextAlignment.MiddleCenter,
                        Font = new Font(Font.FontFamily, Font.Size, FontStyle.Regular),
                        Text = Item.CreateDate,
                        WidthF = DateWidth
                    };
                    cont++;
                    dataRow.Cells.Add(DateDataCell);

                    var ClientCodeDataCell = new XRTableCell
                    {
                        TextAlignment = TextAlignment.MiddleCenter,
                        Font = new Font(Font.FontFamily, Font.Size, FontStyle.Regular),
                        Text = Item.CustomerCode,
                        WidthF = ClientCodeWidth
                    };
                    cont++;
                    dataRow.Cells.Add(ClientCodeDataCell);

                    var ClientNameDataCell = new XRTableCell
                    {
                        TextAlignment = TextAlignment.MiddleCenter,
                        Font = new Font(Font.FontFamily, Font.Size, FontStyle.Regular),
                        Text = Item.CustomerName,
                        WidthF = ClientNameWidth
                    };
                    cont++;
                    dataRow.Cells.Add(ClientNameDataCell);

                    var DueDateDataCell = new XRTableCell
                    {
                        TextAlignment = TextAlignment.MiddleCenter,
                        Font = new Font(Font.FontFamily, Font.Size, FontStyle.Regular),
                        Text = Item.DueDate,
                        WidthF = DueDateWidth
                    };
                    cont++;
                    dataRow.Cells.Add(DueDateDataCell);

                    var AmountDataCell = new XRTableCell
                    {
                        TextAlignment = TextAlignment.MiddleCenter,
                        Font = new Font(Font.FontFamily, Font.Size, FontStyle.Regular),
                        Text = Item.Amount.ToString("C"),
                        WidthF = AmountWidth
                    };
                    cont++;
                    dataRow.Cells.Add(AmountDataCell);                    

                    for (int i = 0; i < model.IepsNamesList.Count; i++)
                    {
                        string text = "$0.00";
                        var headerName = HeaderRow.Cells[cont].Name;
                        if (Item.IepsList.Count > 0)
                        {
                            foreach (var ieps in Item.IepsList)
                            {
                                if (ieps.nameIeps == headerName)
                                    text = ieps.value.ToString("C");
                            }
                        }

                        var IEPSDataCell = new XRTableCell
                        {
                            TextAlignment = TextAlignment.MiddleCenter,
                            Font = new Font(Font.FontFamily, Font.Size, FontStyle.Regular),
                            Text = text,
                            WidthF = IEPSWidth
                        };
                        cont++;
                        dataRow.Cells.Add(IEPSDataCell);
                    }

                    var TaxDataCell = new XRTableCell
                    {
                        TextAlignment = TextAlignment.MiddleCenter,
                        Font = new Font(Font.FontFamily, Font.Size, FontStyle.Regular),
                        Text = Item.Tax.ToString("C"),
                        WidthF = TaxWidth
                    };
                    cont++;
                    dataRow.Cells.Add(TaxDataCell);

                    var TotalDataCell = new XRTableCell
                    {
                        TextAlignment = TextAlignment.MiddleCenter,
                        Font = new Font(Font.FontFamily, Font.Size, FontStyle.Regular),
                        Text = Item.TotalAmount.ToString("C"),
                        WidthF = TotalWidth
                    };
                    cont++;
                    dataRow.Cells.Add(TotalDataCell);

                    var CreditDataCell = new XRTableCell
                    {
                        TextAlignment = TextAlignment.MiddleCenter,
                        Font = new Font(Font.FontFamily, Font.Size, FontStyle.Regular),
                        Text = Item.TotalCredit.ToString("C"),
                        WidthF = CreditWidth
                    };
                    cont++;
                    dataRow.Cells.Add(CreditDataCell);

                    var CashDataCell = new XRTableCell
                    {
                        TextAlignment = TextAlignment.MiddleCenter,
                        Font = new Font(Font.FontFamily, Font.Size, FontStyle.Regular),
                        Text = Item.TotalCash.ToString("C"),
                        WidthF = CashWidth
                    };
                    cont++;
                    dataRow.Cells.Add(CashDataCell);

                    var PaymentDataCell = new XRTableCell
                    {
                        TextAlignment = TextAlignment.MiddleCenter,
                        Font = new Font(Font.FontFamily, Font.Size, FontStyle.Regular),
                        Text = Item.CreditOrCash,
                        WidthF = PaymentWidth
                    };
                    cont++;
                    dataRow.Cells.Add(PaymentDataCell);

                    var BalanceDataCell = new XRTableCell
                    {
                        TextAlignment = TextAlignment.MiddleCenter,
                        Font = new Font(Font.FontFamily, Font.Size, FontStyle.Regular),
                        Text = Item.Balance.ToString("C"),
                        WidthF = BalanceWidht
                    };
                    cont++;
                    dataRow.Cells.Add(BalanceDataCell);

                    var CurrencyDataCell = new XRTableCell
                    {
                        TextAlignment = TextAlignment.MiddleCenter,
                        Font = new Font(Font.FontFamily, Font.Size, FontStyle.Regular),
                        Text = Item.CurrencyCode,
                        WidthF = CurrencyWidht,
                    };
                    cont++;
                    dataRow.Cells.Add(CurrencyDataCell);

                    var StampNumberDataCell = new XRTableCell
                    {
                        TextAlignment = TextAlignment.MiddleCenter,
                        Font = new Font(Font.FontFamily, Font.Size, FontStyle.Regular),
                        Text = !string.IsNullOrEmpty(Item.FiscalNumberReport) ? Item.FiscalNumberReport.Replace("@", Environment.NewLine) : string.Empty,
                        WidthF = StampWidth
                    };
                    cont++;
                    dataRow.Cells.Add(StampNumberDataCell);

                    var StatusDataCell = new XRTableCell
                    {
                        TextAlignment = TextAlignment.MiddleCenter,
                        Font = new Font(Font.FontFamily, Font.Size, FontStyle.Regular),
                        Text = Item.Status,
                        WidthF = StatusWidth
                    };
                    cont++;
                    dataRow.Cells.Add(StatusDataCell);


                    ivaValue += Item.Tax;
                    subTotal += Item.Amount;
                    totalValue += Item.TotalAmount;
                    iepsValue += Item.Ieps;

                    dynamicTable.Rows.Add(dataRow);
                }

                dynamicTable.EndInit();

                this.Detail.Controls.Add(dynamicTable);                               

                ivaLabel.Text = ivaValue.ToString("C");
                iepsLabel.Text = iepsValue.ToString("C");
                subtotalLabel.Text = subTotal.ToString("C");
                totalLabel.Text = totalValue.ToString("C");                

                return DataSetTableOrdered;
            }
            catch (Exception ex)
            {
                string d = ex.Message;
                throw;
            }
        }
    }
}
