﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
using System.Data;
using App.Entities.ViewModels.DamagedsGoods;

namespace FloridoERPTX.Reports
{
    public partial class RMABanati : DevExpress.XtraReports.UI.XtraReport
    {
        public DataSet DataSetTableOrdered = null;
        public RMABanati()
        {
            InitializeComponent();
        }

        public DataSet printTable(DamagedGoodsHeadModel damaged)
        {
            try
            {
                DataSetTableOrdered = new DataSet();
                
                DataTable DataTableOrdered = new DataTable();
                DataSetTableOrdered.Tables.Add(DataTableOrdered);

                xrLabelSupplier.Text = damaged.Supplier;
                xrLabelDate.Text = DateTime.Now.ToString("dd/MM/yyyy");
                xrLabelFolio.Text = damaged.DamagedGoodsDoc.ToString();
                xrLabelSite.Text = damaged.SiteName;

                DataTableOrdered.Columns.Add(new DataColumn("xrTableCellPartNumber"));
                DataTableOrdered.Columns.Add(new DataColumn("xrTableCellName"));                
                DataTableOrdered.Columns.Add(new DataColumn("xrTableCellQuantity"));                
                DataTableOrdered.Columns.Add(new DataColumn("xrTableCellStorage"));
                DataTableOrdered.Columns.Add(new DataColumn("xrTableCellQuantityRefund"));                
                
                foreach (var item in damaged.DamageGoodsItem)
                {
                    DataRow DataRowOrdered = DataSetTableOrdered.Tables[0].NewRow();
                    DataRowOrdered["xrTableCellPartNumber"] = item.PartNumber;
                    DataRowOrdered["xrTableCellName"] = item.Description;                    
                    DataRowOrdered["xrTableCellQuantity"] = item.Quantity;
                    DataRowOrdered["xrTableCellStorage"] = item.Storage;                    
                    DataSetTableOrdered.Tables[0].Rows.Add(DataRowOrdered);
                }

                xrTableCellPartNumber.DataBindings.Add(new XRBinding("Text", null, "xrTableRow1.xrTableCellPartNumber"));
                xrTableCellName.DataBindings.Add(new XRBinding("Text", null, "xrTableRow1.xrTableCellName"));                
                xrTableCellQuantity.DataBindings.Add(new XRBinding("Text", null, "xrTableRow1.xrTableCellQuantity"));                
                xrTableCellStorage.DataBindings.Add(new XRBinding("Text", null, "xrTableRow1.xrTableCellStorage"));                

                return DataSetTableOrdered;
            }
            catch (Exception ex)
            {
                string d = ex.Message;
                throw;
            }
        }
    }
}
