﻿using System;
using System.Data;
using System.Collections.Generic;
using App.DAL.PurchaseOrder;
using App.Entities;
using DevExpress.XtraReports.UI;
using App.BLL.Supplier;
using App.Entities.ViewModels.PurchaseOrder;
using App.Entities.ViewModels.Sales;

namespace FloridoERPTX.Reports
{
    public partial class SalesByHourReport : DevExpress.XtraReports.UI.XtraReport
    {
       
        public DataSet DataSetTableOrdered = null;
        decimal ivaValue, iepsValue,totalValue,subTotal, clientesValue,
        articulosValue ,
        articulosPromedioValue ,
        clientesPromedioValue;
        public SalesByHourReport()
        {
            InitializeComponent();
          
        }
        public DataSet printTable(List<CustomerSalesViewModel> salesList, string date1, string date2,string siteName="Florido") //se encarga de imprimir la orden del proveedor
        {
            try
            {
                siteLabel.Text = siteName;
                labelDate.Text = DateTime.Now.Date.ToShortDateString();
                labelDate1.Text = date1;
                labelDate2.Text = date2;

                DataSetTableOrdered = new DataSet();

                DataTable DataTableOrdered = new DataTable();
                DataSetTableOrdered.Tables.Add(DataTableOrdered);

                
               
                 
                DataTableOrdered.Columns.Add(new DataColumn("Hora"));
                DataTableOrdered.Columns.Add(new DataColumn("Articulos"));
                DataTableOrdered.Columns.Add(new DataColumn("ArticulosPromedio"));
                DataTableOrdered.Columns.Add(new DataColumn("Subtotal"));
                DataTableOrdered.Columns.Add(new DataColumn("Ieps"));
                DataTableOrdered.Columns.Add(new DataColumn("Iva"));
                DataTableOrdered.Columns.Add(new DataColumn("Ventas"));
                DataTableOrdered.Columns.Add(new DataColumn("Clientes"));
                DataTableOrdered.Columns.Add(new DataColumn("PromedioPorCliente"));




                foreach (var item in salesList)
                {
                    DataRow DataRowOrdered = DataSetTableOrdered.Tables[0].NewRow();
                    DataRowOrdered["Hora"] = item.Hora;
                    DataRowOrdered["Articulos"] = item.Articulos;
                    DataRowOrdered["ArticulosPromedio"] = item.ArticulosPromedio.ToString("0.00");
                    DataRowOrdered["Subtotal"] = "$" + item.Subtotal.ToString("0.00");
                    DataRowOrdered["Ieps"] = "$" + item.Ieps.ToString("0.00");
                    DataRowOrdered["Iva"] = "$" + item.Iva.ToString("0.00");
                    DataRowOrdered["Ventas"] = "$" + item.Ventas.ToString("0.00");
                    DataRowOrdered["Clientes"] = item.Clientes;
                    DataRowOrdered["PromedioPorCliente"] = "$"+item.PromedioPorCliente.ToString("0.00");
                   

                    DataSetTableOrdered.Tables[0].Rows.Add(DataRowOrdered);
                    clientesValue += item.Clientes;
                    articulosValue += item.Articulos;
                    articulosPromedioValue += item.ArticulosPromedio;
                    clientesPromedioValue += item.PromedioPorCliente;
                    ivaValue += item.Iva;
                    iepsValue += item.Ieps;
                    subTotal += item.Subtotal;
                    totalValue += item.Ventas;



                }
               
                ivaLabel.Text = "$"+ivaValue.ToString("0.00");
                iepsLabel.Text = "$" + iepsValue.ToString("0.00");
                subtotalLabel.Text = "$" + subTotal.ToString("0.00");
                totalLabel.Text = "$" + totalValue.ToString("0.00");
                Hora.DataBindings.Add(new XRBinding("Text", null, "xrTableRowFormat.Hora"));
                Articulos.DataBindings.Add(new XRBinding("Text", null, "xrTableRowFormat.Articulos"));
                ArticulosPromedio.DataBindings.Add(new XRBinding("Text", null, "xrTableRowFormat.ArticulosPromedio"));
                lSubtotal.DataBindings.Add(new XRBinding("Text", null, "xrTableRowFormat.Subtotal"));
                Ventas.DataBindings.Add(new XRBinding("Text", null, "xrTableRowFormat.Ventas"));
                Iva.DataBindings.Add(new XRBinding("Text", null, "xrTableRowFormat.Iva"));
                Ieps.DataBindings.Add(new XRBinding("Text", null, "xrTableRowFormat.Ieps"));
                PromedioPorCliente.DataBindings.Add(new XRBinding("Text", null, "xrTableRowFormat.PromedioPorCliente"));
                Clientes.DataBindings.Add(new XRBinding("Text", null, "xrTableRowFormat.Clientes"));
              
               


                return DataSetTableOrdered;
            }
            catch (Exception ex)
            {
                string d=ex.Message;
                throw;
            }
          
        }

       


    }
}
