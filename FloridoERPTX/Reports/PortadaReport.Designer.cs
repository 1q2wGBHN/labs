﻿namespace FloridoERPTX.Reports
{
    partial class Portada
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Portada));
            this.Detail = new DevExpress.XtraReports.UI.DetailBand();
            this.xrLabel2 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel6 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel1 = new DevExpress.XtraReports.UI.XRLabel();
            this.Detalle = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel17 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel18 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel19 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel24 = new DevExpress.XtraReports.UI.XRLabel();
            this.TopMargin = new DevExpress.XtraReports.UI.TopMarginBand();
            this.BottomMargin = new DevExpress.XtraReports.UI.BottomMarginBand();
            this.ReportHeader = new DevExpress.XtraReports.UI.ReportHeaderBand();
            this.xrLabel35 = new DevExpress.XtraReports.UI.XRLabel();
            this._dllsEXEcre = new DevExpress.XtraReports.UI.XRLabel();
            this._dllsEXEcash = new DevExpress.XtraReports.UI.XRLabel();
            this._dllsEXEtotal = new DevExpress.XtraReports.UI.XRLabel();
            this._EXEcreGranTotal = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel27 = new DevExpress.XtraReports.UI.XRLabel();
            this._mnEXEcre = new DevExpress.XtraReports.UI.XRLabel();
            this._mnEXEcash = new DevExpress.XtraReports.UI.XRLabel();
            this._mnEXEtotal = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel9 = new DevExpress.XtraReports.UI.XRLabel();
            this._LTotalTOTAL = new DevExpress.XtraReports.UI.XRLabel();
            this._LTotalIEPS = new DevExpress.XtraReports.UI.XRLabel();
            this._LTotalIVA = new DevExpress.XtraReports.UI.XRLabel();
            this._LTotalIEPS0 = new DevExpress.XtraReports.UI.XRLabel();
            this._LTotalGravado = new DevExpress.XtraReports.UI.XRLabel();
            this._LTotalTasa0 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel37 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel22 = new DevExpress.XtraReports.UI.XRLabel();
            this._Ldiferencia = new DevExpress.XtraReports.UI.XRLabel();
            this._LTipoCambio = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel20 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel16 = new DevExpress.XtraReports.UI.XRLabel();
            this._LprintDate = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel105 = new DevExpress.XtraReports.UI.XRLabel();
            this._CASHBACK = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel104 = new DevExpress.XtraReports.UI.XRLabel();
            this._DONMXN = new DevExpress.XtraReports.UI.XRLabel();
            this._DONDLLS = new DevExpress.XtraReports.UI.XRLabel();
            this._CANelect = new DevExpress.XtraReports.UI.XRLabel();
            this._CANefectivo = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel98 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel99 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel100 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel101 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel102 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel103 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel94 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel73 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel74 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel75 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel76 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel77 = new DevExpress.XtraReports.UI.XRLabel();
            this._BONDLLS = new DevExpress.XtraReports.UI.XRLabel();
            this._BONMXN = new DevExpress.XtraReports.UI.XRLabel();
            this._NCDLLS = new DevExpress.XtraReports.UI.XRLabel();
            this._NCMXN = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel82 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel83 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel84 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel86 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel87 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel88 = new DevExpress.XtraReports.UI.XRLabel();
            this._DESCDLLS = new DevExpress.XtraReports.UI.XRLabel();
            this._DESDLLS = new DevExpress.XtraReports.UI.XRLabel();
            this._DESCMXN = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel92 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel93 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel44 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel45 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel46 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel47 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel48 = new DevExpress.XtraReports.UI.XRLabel();
            this._USD02 = new DevExpress.XtraReports.UI.XRLabel();
            this._USD01 = new DevExpress.XtraReports.UI.XRLabel();
            this._USD00 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel52 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel53 = new DevExpress.XtraReports.UI.XRLabel();
            this._USD10 = new DevExpress.XtraReports.UI.XRLabel();
            this._USD11 = new DevExpress.XtraReports.UI.XRLabel();
            this._USD12 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel57 = new DevExpress.XtraReports.UI.XRLabel();
            this._USD20 = new DevExpress.XtraReports.UI.XRLabel();
            this._USD21 = new DevExpress.XtraReports.UI.XRLabel();
            this._USD22 = new DevExpress.XtraReports.UI.XRLabel();
            this._USD42 = new DevExpress.XtraReports.UI.XRLabel();
            this._USD41 = new DevExpress.XtraReports.UI.XRLabel();
            this._USD40 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel64 = new DevExpress.XtraReports.UI.XRLabel();
            this._USD32 = new DevExpress.XtraReports.UI.XRLabel();
            this._USD31 = new DevExpress.XtraReports.UI.XRLabel();
            this._USD30 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel68 = new DevExpress.XtraReports.UI.XRLabel();
            this._USD52 = new DevExpress.XtraReports.UI.XRLabel();
            this._USD51 = new DevExpress.XtraReports.UI.XRLabel();
            this._USD50 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel72 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel40 = new DevExpress.XtraReports.UI.XRLabel();
            this._MN50 = new DevExpress.XtraReports.UI.XRLabel();
            this._MN51 = new DevExpress.XtraReports.UI.XRLabel();
            this._MN52 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel31 = new DevExpress.XtraReports.UI.XRLabel();
            this._MN30 = new DevExpress.XtraReports.UI.XRLabel();
            this._MN31 = new DevExpress.XtraReports.UI.XRLabel();
            this._MN32 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel36 = new DevExpress.XtraReports.UI.XRLabel();
            this._MN40 = new DevExpress.XtraReports.UI.XRLabel();
            this._MN41 = new DevExpress.XtraReports.UI.XRLabel();
            this._MN42 = new DevExpress.XtraReports.UI.XRLabel();
            this._MN22 = new DevExpress.XtraReports.UI.XRLabel();
            this._MN21 = new DevExpress.XtraReports.UI.XRLabel();
            this._MN20 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel30 = new DevExpress.XtraReports.UI.XRLabel();
            this._MN12 = new DevExpress.XtraReports.UI.XRLabel();
            this._MN11 = new DevExpress.XtraReports.UI.XRLabel();
            this._MN10 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel26 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel14 = new DevExpress.XtraReports.UI.XRLabel();
            this._MN00 = new DevExpress.XtraReports.UI.XRLabel();
            this._MN01 = new DevExpress.XtraReports.UI.XRLabel();
            this._MN02 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel13 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel12 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel11 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel10 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel8 = new DevExpress.XtraReports.UI.XRLabel();
            this.labelDate = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLine1 = new DevExpress.XtraReports.UI.XRLine();
            this._labelReporte = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel4 = new DevExpress.XtraReports.UI.XRLabel();
            this.siteLabel = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel3 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrPictureBox1 = new DevExpress.XtraReports.UI.XRPictureBox();
            this.PageFooter = new DevExpress.XtraReports.UI.PageFooterBand();
            this.xrPageInfo1 = new DevExpress.XtraReports.UI.XRPageInfo();
            this.DetailReport = new DevExpress.XtraReports.UI.DetailReportBand();
            this.Detail1 = new DevExpress.XtraReports.UI.DetailBand();
            this.xrTable4 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow4 = new DevExpress.XtraReports.UI.XRTableRow();
            this._DepDLLScc = new DevExpress.XtraReports.UI.XRTableCell();
            this._DepDLLSCcTipo = new DevExpress.XtraReports.UI.XRTableCell();
            this._DepDLLSMXNCc = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable3 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow3 = new DevExpress.XtraReports.UI.XRTableRow();
            this._DepDLLScajas = new DevExpress.XtraReports.UI.XRTableCell();
            this._DepDLLSCajasTipo = new DevExpress.XtraReports.UI.XRTableCell();
            this._DepDLLSMXNCajas = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable2 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow2 = new DevExpress.XtraReports.UI.XRTableRow();
            this._DepMXNcc = new DevExpress.XtraReports.UI.XRTableCell();
            this._DepTipoCc = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable1 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow1 = new DevExpress.XtraReports.UI.XRTableRow();
            this._DepMXNcajas = new DevExpress.XtraReports.UI.XRTableCell();
            this._DepMXNCajaTipo = new DevExpress.XtraReports.UI.XRTableCell();
            this.ReportHeader1 = new DevExpress.XtraReports.UI.ReportHeaderBand();
            this.xrLabel111 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel110 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel109 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel25 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel107 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel108 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel112 = new DevExpress.XtraReports.UI.XRLabel();
            this.ReportFooter = new DevExpress.XtraReports.UI.ReportFooterBand();
            this._RENTAdlls = new DevExpress.XtraReports.UI.XRLabel();
            this._RENTAiva = new DevExpress.XtraReports.UI.XRLabel();
            this._RENTAtotalDlls = new DevExpress.XtraReports.UI.XRLabel();
            this._RENTAtotalDllsMxn = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel146 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel147 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel148 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel149 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel150 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel127 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel130 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel131 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel132 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel133 = new DevExpress.XtraReports.UI.XRLabel();
            this._COMStotal = new DevExpress.XtraReports.UI.XRLabel();
            this._COMSdllsMxn = new DevExpress.XtraReports.UI.XRLabel();
            this._COMSdlls = new DevExpress.XtraReports.UI.XRLabel();
            this._COMSmxn = new DevExpress.XtraReports.UI.XRLabel();
            this._PAYSmxn = new DevExpress.XtraReports.UI.XRLabel();
            this._PAYSdlls = new DevExpress.XtraReports.UI.XRLabel();
            this._PAYSdllsMxn = new DevExpress.XtraReports.UI.XRLabel();
            this._PAYStotal = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel116 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel117 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel120 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel121 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel126 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel5 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel118 = new DevExpress.XtraReports.UI.XRLabel();
            this._TDvalesElec = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel122 = new DevExpress.XtraReports.UI.XRLabel();
            this._TDtotal = new DevExpress.XtraReports.UI.XRLabel();
            this._TDvales = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel125 = new DevExpress.XtraReports.UI.XRLabel();
            this._TDcards = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel129 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel134 = new DevExpress.XtraReports.UI.XRLabel();
            this._TDdllsmxn = new DevExpress.XtraReports.UI.XRLabel();
            this._TDmxn = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel137 = new DevExpress.XtraReports.UI.XRLabel();
            this.backgroundWorker1 = new System.ComponentModel.BackgroundWorker();
            this.DetailReport1 = new DevExpress.XtraReports.UI.DetailReportBand();
            this.Detail2 = new DevExpress.XtraReports.UI.DetailBand();
            this._TableTipoPago = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow9 = new DevExpress.XtraReports.UI.XRTableRow();
            this._PAYMETname = new DevExpress.XtraReports.UI.XRTableCell();
            this._PAYMETimporte = new DevExpress.XtraReports.UI.XRTableCell();
            this._TableIEPS = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow8 = new DevExpress.XtraReports.UI.XRTableRow();
            this._IEPSperc = new DevExpress.XtraReports.UI.XRTableCell();
            this._IEPSimporte = new DevExpress.XtraReports.UI.XRTableCell();
            this._TableServ = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow7 = new DevExpress.XtraReports.UI.XRTableRow();
            this._SERVname = new DevExpress.XtraReports.UI.XRTableCell();
            this._SERVimporte = new DevExpress.XtraReports.UI.XRTableCell();
            this._TablesDesc = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow6 = new DevExpress.XtraReports.UI.XRTableRow();
            this._DESCcliente = new DevExpress.XtraReports.UI.XRTableCell();
            this._DESCimporte = new DevExpress.XtraReports.UI.XRTableCell();
            this._TableSanctions = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow5 = new DevExpress.XtraReports.UI.XRTableRow();
            this._SANCcajero = new DevExpress.XtraReports.UI.XRTableCell();
            this._SANCimporte = new DevExpress.XtraReports.UI.XRTableCell();
            this.ReportHeader2 = new DevExpress.XtraReports.UI.ReportHeaderBand();
            this.xrLabel162 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel163 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel164 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel159 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel160 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel161 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel156 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel157 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel158 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel153 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel154 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel155 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel33 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel151 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel152 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel29 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell1 = new DevExpress.XtraReports.UI.XRTableCell();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._TableTipoPago)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._TableIEPS)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._TableServ)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._TablesDesc)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._TableSanctions)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // Detail
            // 
            this.Detail.Dpi = 100F;
            this.Detail.Expanded = false;
            this.Detail.HeightF = 0F;
            this.Detail.Name = "Detail";
            this.Detail.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.Detail.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel2
            // 
            this.xrLabel2.BackColor = System.Drawing.Color.Silver;
            this.xrLabel2.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel2.Dpi = 100F;
            this.xrLabel2.Font = new System.Drawing.Font("Arial", 7F, System.Drawing.FontStyle.Bold);
            this.xrLabel2.LocationFloat = new DevExpress.Utils.PointFloat(523.8878F, 42.47393F);
            this.xrLabel2.Name = "xrLabel2";
            this.xrLabel2.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel2.SizeF = new System.Drawing.SizeF(94.56927F, 17.55346F);
            this.xrLabel2.StylePriority.UseBackColor = false;
            this.xrLabel2.StylePriority.UseBorders = false;
            this.xrLabel2.StylePriority.UseFont = false;
            this.xrLabel2.StylePriority.UseTextAlignment = false;
            this.xrLabel2.Text = "TIPO DEPOSITO";
            this.xrLabel2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrLabel6
            // 
            this.xrLabel6.BackColor = System.Drawing.Color.Silver;
            this.xrLabel6.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel6.Dpi = 100F;
            this.xrLabel6.Font = new System.Drawing.Font("Arial", 7F, System.Drawing.FontStyle.Bold);
            this.xrLabel6.LocationFloat = new DevExpress.Utils.PointFloat(618.4569F, 42.47393F);
            this.xrLabel6.Name = "xrLabel6";
            this.xrLabel6.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel6.SizeF = new System.Drawing.SizeF(94.5694F, 17.55346F);
            this.xrLabel6.StylePriority.UseBackColor = false;
            this.xrLabel6.StylePriority.UseBorders = false;
            this.xrLabel6.StylePriority.UseFont = false;
            this.xrLabel6.StylePriority.UseTextAlignment = false;
            this.xrLabel6.Text = "EN M.N.";
            this.xrLabel6.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrLabel1
            // 
            this.xrLabel1.BackColor = System.Drawing.Color.Silver;
            this.xrLabel1.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel1.Dpi = 100F;
            this.xrLabel1.Font = new System.Drawing.Font("Arial", 7F, System.Drawing.FontStyle.Bold);
            this.xrLabel1.LocationFloat = new DevExpress.Utils.PointFloat(427.6086F, 42.47393F);
            this.xrLabel1.Name = "xrLabel1";
            this.xrLabel1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel1.SizeF = new System.Drawing.SizeF(96.27917F, 17.55346F);
            this.xrLabel1.StylePriority.UseBackColor = false;
            this.xrLabel1.StylePriority.UseBorders = false;
            this.xrLabel1.StylePriority.UseFont = false;
            this.xrLabel1.StylePriority.UseTextAlignment = false;
            this.xrLabel1.Text = "IMPORTE";
            this.xrLabel1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // Detalle
            // 
            this.Detalle.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.Detalle.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.Detalle.Dpi = 100F;
            this.Detalle.Font = new System.Drawing.Font("Arial", 7F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Detalle.LocationFloat = new DevExpress.Utils.PointFloat(9.000015F, 28.00001F);
            this.Detalle.Name = "Detalle";
            this.Detalle.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.Detalle.SizeF = new System.Drawing.SizeF(184.5693F, 14.47391F);
            this.Detalle.StylePriority.UseBackColor = false;
            this.Detalle.StylePriority.UseBorders = false;
            this.Detalle.StylePriority.UseFont = false;
            this.Detalle.StylePriority.UseTextAlignment = false;
            this.Detalle.Text = "PESOS AREA DE CAJAS";
            this.Detalle.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrLabel17
            // 
            this.xrLabel17.BackColor = System.Drawing.Color.Silver;
            this.xrLabel17.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel17.Dpi = 100F;
            this.xrLabel17.Font = new System.Drawing.Font("Arial", 7F, System.Drawing.FontStyle.Bold);
            this.xrLabel17.LocationFloat = new DevExpress.Utils.PointFloat(9.000015F, 42.47392F);
            this.xrLabel17.Name = "xrLabel17";
            this.xrLabel17.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel17.SizeF = new System.Drawing.SizeF(89.9999F, 17.55346F);
            this.xrLabel17.StylePriority.UseBackColor = false;
            this.xrLabel17.StylePriority.UseBorders = false;
            this.xrLabel17.StylePriority.UseFont = false;
            this.xrLabel17.StylePriority.UseTextAlignment = false;
            this.xrLabel17.Text = "IMPORTE";
            this.xrLabel17.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrLabel18
            // 
            this.xrLabel18.BackColor = System.Drawing.Color.Silver;
            this.xrLabel18.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel18.Dpi = 100F;
            this.xrLabel18.Font = new System.Drawing.Font("Arial", 7F, System.Drawing.FontStyle.Bold);
            this.xrLabel18.LocationFloat = new DevExpress.Utils.PointFloat(100F, 42.47392F);
            this.xrLabel18.Name = "xrLabel18";
            this.xrLabel18.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel18.SizeF = new System.Drawing.SizeF(93.56924F, 17.55346F);
            this.xrLabel18.StylePriority.UseBackColor = false;
            this.xrLabel18.StylePriority.UseBorders = false;
            this.xrLabel18.StylePriority.UseFont = false;
            this.xrLabel18.StylePriority.UseTextAlignment = false;
            this.xrLabel18.Text = "TIPO DEPOSITO";
            this.xrLabel18.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrLabel19
            // 
            this.xrLabel19.BackColor = System.Drawing.Color.Silver;
            this.xrLabel19.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel19.Dpi = 100F;
            this.xrLabel19.Font = new System.Drawing.Font("Arial", 7F, System.Drawing.FontStyle.Bold);
            this.xrLabel19.LocationFloat = new DevExpress.Utils.PointFloat(219.7342F, 42.47392F);
            this.xrLabel19.Name = "xrLabel19";
            this.xrLabel19.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel19.SizeF = new System.Drawing.SizeF(89.99988F, 17.60773F);
            this.xrLabel19.StylePriority.UseBackColor = false;
            this.xrLabel19.StylePriority.UseBorders = false;
            this.xrLabel19.StylePriority.UseFont = false;
            this.xrLabel19.StylePriority.UseTextAlignment = false;
            this.xrLabel19.Text = "IMPORTE";
            this.xrLabel19.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrLabel24
            // 
            this.xrLabel24.BackColor = System.Drawing.Color.Silver;
            this.xrLabel24.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel24.Dpi = 100F;
            this.xrLabel24.Font = new System.Drawing.Font("Arial", 7F, System.Drawing.FontStyle.Bold);
            this.xrLabel24.LocationFloat = new DevExpress.Utils.PointFloat(310.7341F, 42.47392F);
            this.xrLabel24.Name = "xrLabel24";
            this.xrLabel24.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel24.SizeF = new System.Drawing.SizeF(93.5694F, 17.55346F);
            this.xrLabel24.StylePriority.UseBackColor = false;
            this.xrLabel24.StylePriority.UseBorders = false;
            this.xrLabel24.StylePriority.UseFont = false;
            this.xrLabel24.StylePriority.UseTextAlignment = false;
            this.xrLabel24.Text = "TIPO DEPOSITO";
            this.xrLabel24.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // TopMargin
            // 
            this.TopMargin.Dpi = 100F;
            this.TopMargin.HeightF = 30F;
            this.TopMargin.Name = "TopMargin";
            this.TopMargin.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.TopMargin.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // BottomMargin
            // 
            this.BottomMargin.Dpi = 100F;
            this.BottomMargin.HeightF = 55F;
            this.BottomMargin.Name = "BottomMargin";
            this.BottomMargin.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.BottomMargin.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // ReportHeader
            // 
            this.ReportHeader.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel35,
            this._dllsEXEcre,
            this._dllsEXEcash,
            this._dllsEXEtotal,
            this._EXEcreGranTotal,
            this.xrLabel27,
            this._mnEXEcre,
            this._mnEXEcash,
            this._mnEXEtotal,
            this.xrLabel9,
            this._LTotalTOTAL,
            this._LTotalIEPS,
            this._LTotalIVA,
            this._LTotalIEPS0,
            this._LTotalGravado,
            this._LTotalTasa0,
            this.xrLabel37,
            this.xrLabel22,
            this._Ldiferencia,
            this._LTipoCambio,
            this.xrLabel20,
            this.xrLabel16,
            this._LprintDate,
            this.xrLabel105,
            this._CASHBACK,
            this.xrLabel104,
            this._DONMXN,
            this._DONDLLS,
            this._CANelect,
            this._CANefectivo,
            this.xrLabel98,
            this.xrLabel99,
            this.xrLabel100,
            this.xrLabel101,
            this.xrLabel102,
            this.xrLabel103,
            this.xrLabel94,
            this.xrLabel73,
            this.xrLabel74,
            this.xrLabel75,
            this.xrLabel76,
            this.xrLabel77,
            this._BONDLLS,
            this._BONMXN,
            this._NCDLLS,
            this._NCMXN,
            this.xrLabel82,
            this.xrLabel83,
            this.xrLabel84,
            this.xrLabel86,
            this.xrLabel87,
            this.xrLabel88,
            this._DESCDLLS,
            this._DESDLLS,
            this._DESCMXN,
            this.xrLabel92,
            this.xrLabel93,
            this.xrLabel44,
            this.xrLabel45,
            this.xrLabel46,
            this.xrLabel47,
            this.xrLabel48,
            this._USD02,
            this._USD01,
            this._USD00,
            this.xrLabel52,
            this.xrLabel53,
            this._USD10,
            this._USD11,
            this._USD12,
            this.xrLabel57,
            this._USD20,
            this._USD21,
            this._USD22,
            this._USD42,
            this._USD41,
            this._USD40,
            this.xrLabel64,
            this._USD32,
            this._USD31,
            this._USD30,
            this.xrLabel68,
            this._USD52,
            this._USD51,
            this._USD50,
            this.xrLabel72,
            this.xrLabel40,
            this._MN50,
            this._MN51,
            this._MN52,
            this.xrLabel31,
            this._MN30,
            this._MN31,
            this._MN32,
            this.xrLabel36,
            this._MN40,
            this._MN41,
            this._MN42,
            this._MN22,
            this._MN21,
            this._MN20,
            this.xrLabel30,
            this._MN12,
            this._MN11,
            this._MN10,
            this.xrLabel26,
            this.xrLabel14,
            this._MN00,
            this._MN01,
            this._MN02,
            this.xrLabel13,
            this.xrLabel12,
            this.xrLabel11,
            this.xrLabel10,
            this.xrLabel8,
            this.labelDate,
            this.xrLine1,
            this._labelReporte,
            this.xrLabel4,
            this.siteLabel,
            this.xrLabel3,
            this.xrPictureBox1});
            this.ReportHeader.Dpi = 100F;
            this.ReportHeader.HeightF = 360.7241F;
            this.ReportHeader.Name = "ReportHeader";
            // 
            // xrLabel35
            // 
            this.xrLabel35.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel35.Dpi = 100F;
            this.xrLabel35.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel35.LocationFloat = new DevExpress.Utils.PointFloat(347.3087F, 218.1565F);
            this.xrLabel35.Name = "xrLabel35";
            this.xrLabel35.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel35.SizeF = new System.Drawing.SizeF(70.99997F, 23F);
            this.xrLabel35.StylePriority.UseBorders = false;
            this.xrLabel35.StylePriority.UseFont = false;
            this.xrLabel35.StylePriority.UseTextAlignment = false;
            this.xrLabel35.Text = "EXENTO:";
            this.xrLabel35.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // _dllsEXEcre
            // 
            this._dllsEXEcre.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this._dllsEXEcre.Dpi = 100F;
            this._dllsEXEcre.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._dllsEXEcre.LocationFloat = new DevExpress.Utils.PointFloat(418.3087F, 218.1565F);
            this._dllsEXEcre.Name = "_dllsEXEcre";
            this._dllsEXEcre.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this._dllsEXEcre.SizeF = new System.Drawing.SizeF(70.99991F, 23F);
            this._dllsEXEcre.StylePriority.UseBorders = false;
            this._dllsEXEcre.StylePriority.UseFont = false;
            this._dllsEXEcre.StylePriority.UseTextAlignment = false;
            this._dllsEXEcre.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // _dllsEXEcash
            // 
            this._dllsEXEcash.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this._dllsEXEcash.Dpi = 100F;
            this._dllsEXEcash.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._dllsEXEcash.LocationFloat = new DevExpress.Utils.PointFloat(489.3087F, 218.1565F);
            this._dllsEXEcash.Name = "_dllsEXEcash";
            this._dllsEXEcash.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this._dllsEXEcash.SizeF = new System.Drawing.SizeF(70.99997F, 23F);
            this._dllsEXEcash.StylePriority.UseBorders = false;
            this._dllsEXEcash.StylePriority.UseFont = false;
            this._dllsEXEcash.StylePriority.UseTextAlignment = false;
            this._dllsEXEcash.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // _dllsEXEtotal
            // 
            this._dllsEXEtotal.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this._dllsEXEtotal.Dpi = 100F;
            this._dllsEXEtotal.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._dllsEXEtotal.LocationFloat = new DevExpress.Utils.PointFloat(560.3087F, 218.1565F);
            this._dllsEXEtotal.Name = "_dllsEXEtotal";
            this._dllsEXEtotal.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this._dllsEXEtotal.SizeF = new System.Drawing.SizeF(70.99969F, 23F);
            this._dllsEXEtotal.StylePriority.UseBorders = false;
            this._dllsEXEtotal.StylePriority.UseFont = false;
            this._dllsEXEtotal.StylePriority.UseTextAlignment = false;
            this._dllsEXEtotal.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // _EXEcreGranTotal
            // 
            this._EXEcreGranTotal.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this._EXEcreGranTotal.Dpi = 100F;
            this._EXEcreGranTotal.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._EXEcreGranTotal.LocationFloat = new DevExpress.Utils.PointFloat(631.309F, 218.1565F);
            this._EXEcreGranTotal.Name = "_EXEcreGranTotal";
            this._EXEcreGranTotal.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this._EXEcreGranTotal.SizeF = new System.Drawing.SizeF(70.99969F, 23F);
            this._EXEcreGranTotal.StylePriority.UseBorders = false;
            this._EXEcreGranTotal.StylePriority.UseFont = false;
            this._EXEcreGranTotal.StylePriority.UseTextAlignment = false;
            this._EXEcreGranTotal.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrLabel27
            // 
            this.xrLabel27.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel27.Dpi = 100F;
            this.xrLabel27.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel27.LocationFloat = new DevExpress.Utils.PointFloat(10.44257F, 218.1565F);
            this.xrLabel27.Name = "xrLabel27";
            this.xrLabel27.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel27.SizeF = new System.Drawing.SizeF(77.33333F, 23F);
            this.xrLabel27.StylePriority.UseBorders = false;
            this.xrLabel27.StylePriority.UseFont = false;
            this.xrLabel27.StylePriority.UseTextAlignment = false;
            this.xrLabel27.Text = "EXENTO:";
            this.xrLabel27.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // _mnEXEcre
            // 
            this._mnEXEcre.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this._mnEXEcre.Dpi = 100F;
            this._mnEXEcre.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._mnEXEcre.LocationFloat = new DevExpress.Utils.PointFloat(87.77602F, 218.1565F);
            this._mnEXEcre.Name = "_mnEXEcre";
            this._mnEXEcre.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this._mnEXEcre.SizeF = new System.Drawing.SizeF(79.58278F, 23F);
            this._mnEXEcre.StylePriority.UseBorders = false;
            this._mnEXEcre.StylePriority.UseFont = false;
            this._mnEXEcre.StylePriority.UseTextAlignment = false;
            this._mnEXEcre.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // _mnEXEcash
            // 
            this._mnEXEcash.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this._mnEXEcash.Dpi = 100F;
            this._mnEXEcash.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._mnEXEcash.LocationFloat = new DevExpress.Utils.PointFloat(167.3587F, 218.1565F);
            this._mnEXEcash.Name = "_mnEXEcash";
            this._mnEXEcash.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this._mnEXEcash.SizeF = new System.Drawing.SizeF(82.52768F, 23F);
            this._mnEXEcash.StylePriority.UseBorders = false;
            this._mnEXEcash.StylePriority.UseFont = false;
            this._mnEXEcash.StylePriority.UseTextAlignment = false;
            this._mnEXEcash.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // _mnEXEtotal
            // 
            this._mnEXEtotal.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this._mnEXEtotal.Dpi = 100F;
            this._mnEXEtotal.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._mnEXEtotal.LocationFloat = new DevExpress.Utils.PointFloat(249.8865F, 218.1565F);
            this._mnEXEtotal.Name = "_mnEXEtotal";
            this._mnEXEtotal.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this._mnEXEtotal.SizeF = new System.Drawing.SizeF(81.49771F, 23F);
            this._mnEXEtotal.StylePriority.UseBorders = false;
            this._mnEXEtotal.StylePriority.UseFont = false;
            this._mnEXEtotal.StylePriority.UseTextAlignment = false;
            this._mnEXEtotal.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrLabel9
            // 
            this.xrLabel9.Dpi = 100F;
            this.xrLabel9.Font = new System.Drawing.Font("Times New Roman", 11F, System.Drawing.FontStyle.Bold);
            this.xrLabel9.LocationFloat = new DevExpress.Utils.PointFloat(427.6086F, 0F);
            this.xrLabel9.Name = "xrLabel9";
            this.xrLabel9.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel9.SizeF = new System.Drawing.SizeF(132.7001F, 23F);
            this.xrLabel9.StylePriority.UseFont = false;
            this.xrLabel9.Text = "Resumen de Ventas";
            // 
            // _LTotalTOTAL
            // 
            this._LTotalTOTAL.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this._LTotalTOTAL.Dpi = 100F;
            this._LTotalTOTAL.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._LTotalTOTAL.LocationFloat = new DevExpress.Utils.PointFloat(631.3091F, 310.1565F);
            this._LTotalTOTAL.Name = "_LTotalTOTAL";
            this._LTotalTOTAL.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this._LTotalTOTAL.SizeF = new System.Drawing.SizeF(70.99969F, 23F);
            this._LTotalTOTAL.StylePriority.UseBorders = false;
            this._LTotalTOTAL.StylePriority.UseFont = false;
            this._LTotalTOTAL.StylePriority.UseTextAlignment = false;
            this._LTotalTOTAL.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // _LTotalIEPS
            // 
            this._LTotalIEPS.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this._LTotalIEPS.Dpi = 100F;
            this._LTotalIEPS.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._LTotalIEPS.LocationFloat = new DevExpress.Utils.PointFloat(631.3091F, 264.1565F);
            this._LTotalIEPS.Name = "_LTotalIEPS";
            this._LTotalIEPS.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this._LTotalIEPS.SizeF = new System.Drawing.SizeF(70.99969F, 23F);
            this._LTotalIEPS.StylePriority.UseBorders = false;
            this._LTotalIEPS.StylePriority.UseFont = false;
            this._LTotalIEPS.StylePriority.UseTextAlignment = false;
            this._LTotalIEPS.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // _LTotalIVA
            // 
            this._LTotalIVA.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this._LTotalIVA.Dpi = 100F;
            this._LTotalIVA.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._LTotalIVA.LocationFloat = new DevExpress.Utils.PointFloat(631.3091F, 287.1563F);
            this._LTotalIVA.Name = "_LTotalIVA";
            this._LTotalIVA.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this._LTotalIVA.SizeF = new System.Drawing.SizeF(70.99969F, 23.00003F);
            this._LTotalIVA.StylePriority.UseBorders = false;
            this._LTotalIVA.StylePriority.UseFont = false;
            this._LTotalIVA.StylePriority.UseTextAlignment = false;
            this._LTotalIVA.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // _LTotalIEPS0
            // 
            this._LTotalIEPS0.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this._LTotalIEPS0.Dpi = 100F;
            this._LTotalIEPS0.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._LTotalIEPS0.LocationFloat = new DevExpress.Utils.PointFloat(631.3091F, 241.1565F);
            this._LTotalIEPS0.Name = "_LTotalIEPS0";
            this._LTotalIEPS0.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this._LTotalIEPS0.SizeF = new System.Drawing.SizeF(70.99969F, 23F);
            this._LTotalIEPS0.StylePriority.UseBorders = false;
            this._LTotalIEPS0.StylePriority.UseFont = false;
            this._LTotalIEPS0.StylePriority.UseTextAlignment = false;
            this._LTotalIEPS0.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // _LTotalGravado
            // 
            this._LTotalGravado.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this._LTotalGravado.Dpi = 100F;
            this._LTotalGravado.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._LTotalGravado.LocationFloat = new DevExpress.Utils.PointFloat(631.3091F, 195.1566F);
            this._LTotalGravado.Name = "_LTotalGravado";
            this._LTotalGravado.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this._LTotalGravado.SizeF = new System.Drawing.SizeF(70.99969F, 23F);
            this._LTotalGravado.StylePriority.UseBorders = false;
            this._LTotalGravado.StylePriority.UseFont = false;
            this._LTotalGravado.StylePriority.UseTextAlignment = false;
            this._LTotalGravado.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // _LTotalTasa0
            // 
            this._LTotalTasa0.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this._LTotalTasa0.Dpi = 100F;
            this._LTotalTasa0.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._LTotalTasa0.LocationFloat = new DevExpress.Utils.PointFloat(631.3093F, 172.1567F);
            this._LTotalTasa0.Name = "_LTotalTasa0";
            this._LTotalTasa0.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this._LTotalTasa0.SizeF = new System.Drawing.SizeF(70.99963F, 23F);
            this._LTotalTasa0.StylePriority.UseBorders = false;
            this._LTotalTasa0.StylePriority.UseFont = false;
            this._LTotalTasa0.StylePriority.UseTextAlignment = false;
            this._LTotalTasa0.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrLabel37
            // 
            this.xrLabel37.BackColor = System.Drawing.Color.Silver;
            this.xrLabel37.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel37.Dpi = 100F;
            this.xrLabel37.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel37.LocationFloat = new DevExpress.Utils.PointFloat(631.3089F, 149.1567F);
            this.xrLabel37.Name = "xrLabel37";
            this.xrLabel37.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel37.SizeF = new System.Drawing.SizeF(71F, 23F);
            this.xrLabel37.StylePriority.UseBackColor = false;
            this.xrLabel37.StylePriority.UseBorders = false;
            this.xrLabel37.StylePriority.UseFont = false;
            this.xrLabel37.StylePriority.UseTextAlignment = false;
            this.xrLabel37.Text = "TOTAL";
            this.xrLabel37.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrLabel22
            // 
            this.xrLabel22.Dpi = 100F;
            this.xrLabel22.Font = new System.Drawing.Font("Arial Narrow", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel22.LocationFloat = new DevExpress.Utils.PointFloat(368.7256F, 86.58339F);
            this.xrLabel22.Name = "xrLabel22";
            this.xrLabel22.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel22.SizeF = new System.Drawing.SizeF(68.87256F, 20.00001F);
            this.xrLabel22.StylePriority.UseFont = false;
            this.xrLabel22.Text = "Diferencia";
            // 
            // _Ldiferencia
            // 
            this._Ldiferencia.Dpi = 100F;
            this._Ldiferencia.Font = new System.Drawing.Font("Arial Narrow", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._Ldiferencia.LocationFloat = new DevExpress.Utils.PointFloat(437.5981F, 86.58339F);
            this._Ldiferencia.Name = "_Ldiferencia";
            this._Ldiferencia.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this._Ldiferencia.SizeF = new System.Drawing.SizeF(100F, 20.00001F);
            this._Ldiferencia.StylePriority.UseFont = false;
            // 
            // _LTipoCambio
            // 
            this._LTipoCambio.Dpi = 100F;
            this._LTipoCambio.Font = new System.Drawing.Font("Arial Narrow", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._LTipoCambio.LocationFloat = new DevExpress.Utils.PointFloat(94.56921F, 86.58339F);
            this._LTipoCambio.Name = "_LTipoCambio";
            this._LTipoCambio.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this._LTipoCambio.SizeF = new System.Drawing.SizeF(100F, 20.00001F);
            this._LTipoCambio.StylePriority.UseFont = false;
            // 
            // xrLabel20
            // 
            this.xrLabel20.Dpi = 100F;
            this.xrLabel20.Font = new System.Drawing.Font("Arial Narrow", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel20.LocationFloat = new DevExpress.Utils.PointFloat(18.44257F, 86.58339F);
            this.xrLabel20.Name = "xrLabel20";
            this.xrLabel20.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel20.SizeF = new System.Drawing.SizeF(75.87256F, 20.00001F);
            this.xrLabel20.StylePriority.UseFont = false;
            this.xrLabel20.Text = "Tipo Cambio";
            // 
            // xrLabel16
            // 
            this.xrLabel16.Dpi = 100F;
            this.xrLabel16.Font = new System.Drawing.Font("Arial Narrow", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel16.LocationFloat = new DevExpress.Utils.PointFloat(677.3865F, 56.58337F);
            this.xrLabel16.Name = "xrLabel16";
            this.xrLabel16.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel16.SizeF = new System.Drawing.SizeF(120.3587F, 20.00002F);
            this.xrLabel16.StylePriority.UseFont = false;
            this.xrLabel16.Text = "Fecha de impresión:";
            // 
            // _LprintDate
            // 
            this._LprintDate.Dpi = 100F;
            this._LprintDate.Font = new System.Drawing.Font("Arial Narrow", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._LprintDate.LocationFloat = new DevExpress.Utils.PointFloat(797.7451F, 56.58337F);
            this._LprintDate.Name = "_LprintDate";
            this._LprintDate.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this._LprintDate.SizeF = new System.Drawing.SizeF(100F, 20.00001F);
            this._LprintDate.StylePriority.UseFont = false;
            // 
            // xrLabel105
            // 
            this.xrLabel105.BackColor = System.Drawing.Color.Silver;
            this.xrLabel105.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel105.Dpi = 100F;
            this.xrLabel105.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel105.LocationFloat = new DevExpress.Utils.PointFloat(956.014F, 163.8073F);
            this.xrLabel105.Name = "xrLabel105";
            this.xrLabel105.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel105.SizeF = new System.Drawing.SizeF(65.00885F, 23F);
            this.xrLabel105.StylePriority.UseBackColor = false;
            this.xrLabel105.StylePriority.UseBorders = false;
            this.xrLabel105.StylePriority.UseFont = false;
            this.xrLabel105.StylePriority.UseTextAlignment = false;
            this.xrLabel105.Text = "TOTAL";
            this.xrLabel105.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // _CASHBACK
            // 
            this._CASHBACK.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this._CASHBACK.Dpi = 100F;
            this._CASHBACK.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._CASHBACK.LocationFloat = new DevExpress.Utils.PointFloat(956.014F, 186.8073F);
            this._CASHBACK.Name = "_CASHBACK";
            this._CASHBACK.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this._CASHBACK.SizeF = new System.Drawing.SizeF(65.00885F, 23F);
            this._CASHBACK.StylePriority.UseBorders = false;
            this._CASHBACK.StylePriority.UseFont = false;
            this._CASHBACK.StylePriority.UseTextAlignment = false;
            this._CASHBACK.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrLabel104
            // 
            this.xrLabel104.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.xrLabel104.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel104.Dpi = 100F;
            this.xrLabel104.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel104.LocationFloat = new DevExpress.Utils.PointFloat(956.014F, 126.1567F);
            this.xrLabel104.Multiline = true;
            this.xrLabel104.Name = "xrLabel104";
            this.xrLabel104.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel104.SizeF = new System.Drawing.SizeF(65.00885F, 37.65073F);
            this.xrLabel104.StylePriority.UseBackColor = false;
            this.xrLabel104.StylePriority.UseBorders = false;
            this.xrLabel104.StylePriority.UseFont = false;
            this.xrLabel104.StylePriority.UseTextAlignment = false;
            this.xrLabel104.Text = "CASH\r\nBACK";
            this.xrLabel104.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // _DONMXN
            // 
            this._DONMXN.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this._DONMXN.Dpi = 100F;
            this._DONMXN.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._DONMXN.LocationFloat = new DevExpress.Utils.PointFloat(713.9698F, 186.8073F);
            this._DONMXN.Name = "_DONMXN";
            this._DONMXN.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this._DONMXN.SizeF = new System.Drawing.SizeF(50.37497F, 23F);
            this._DONMXN.StylePriority.UseBorders = false;
            this._DONMXN.StylePriority.UseFont = false;
            this._DONMXN.StylePriority.UseTextAlignment = false;
            this._DONMXN.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // _DONDLLS
            // 
            this._DONDLLS.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this._DONDLLS.Dpi = 100F;
            this._DONDLLS.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._DONDLLS.LocationFloat = new DevExpress.Utils.PointFloat(764.3448F, 186.8073F);
            this._DONDLLS.Name = "_DONDLLS";
            this._DONDLLS.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this._DONDLLS.SizeF = new System.Drawing.SizeF(50.4162F, 23F);
            this._DONDLLS.StylePriority.UseBorders = false;
            this._DONDLLS.StylePriority.UseFont = false;
            this._DONDLLS.StylePriority.UseTextAlignment = false;
            this._DONDLLS.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // _CANelect
            // 
            this._CANelect.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this._CANelect.Dpi = 100F;
            this._CANelect.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._CANelect.LocationFloat = new DevExpress.Utils.PointFloat(814.7609F, 186.8074F);
            this._CANelect.Name = "_CANelect";
            this._CANelect.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this._CANelect.SizeF = new System.Drawing.SizeF(82.52769F, 23F);
            this._CANelect.StylePriority.UseBorders = false;
            this._CANelect.StylePriority.UseFont = false;
            this._CANelect.StylePriority.UseTextAlignment = false;
            this._CANelect.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // _CANefectivo
            // 
            this._CANefectivo.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this._CANefectivo.Dpi = 100F;
            this._CANefectivo.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._CANefectivo.LocationFloat = new DevExpress.Utils.PointFloat(897.2885F, 186.8074F);
            this._CANefectivo.Name = "_CANefectivo";
            this._CANefectivo.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this._CANefectivo.SizeF = new System.Drawing.SizeF(58.72565F, 23F);
            this._CANefectivo.StylePriority.UseBorders = false;
            this._CANefectivo.StylePriority.UseFont = false;
            this._CANefectivo.StylePriority.UseTextAlignment = false;
            this._CANefectivo.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrLabel98
            // 
            this.xrLabel98.BackColor = System.Drawing.Color.Silver;
            this.xrLabel98.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel98.Dpi = 100F;
            this.xrLabel98.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel98.LocationFloat = new DevExpress.Utils.PointFloat(897.2885F, 163.8074F);
            this.xrLabel98.Name = "xrLabel98";
            this.xrLabel98.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel98.SizeF = new System.Drawing.SizeF(58.72565F, 23F);
            this.xrLabel98.StylePriority.UseBackColor = false;
            this.xrLabel98.StylePriority.UseBorders = false;
            this.xrLabel98.StylePriority.UseFont = false;
            this.xrLabel98.StylePriority.UseTextAlignment = false;
            this.xrLabel98.Text = "EFECT";
            this.xrLabel98.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrLabel99
            // 
            this.xrLabel99.BackColor = System.Drawing.Color.Silver;
            this.xrLabel99.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel99.Dpi = 100F;
            this.xrLabel99.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel99.LocationFloat = new DevExpress.Utils.PointFloat(814.7609F, 163.8074F);
            this.xrLabel99.Name = "xrLabel99";
            this.xrLabel99.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel99.SizeF = new System.Drawing.SizeF(82.52769F, 23F);
            this.xrLabel99.StylePriority.UseBackColor = false;
            this.xrLabel99.StylePriority.UseBorders = false;
            this.xrLabel99.StylePriority.UseFont = false;
            this.xrLabel99.StylePriority.UseTextAlignment = false;
            this.xrLabel99.Text = "ELECT";
            this.xrLabel99.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrLabel100
            // 
            this.xrLabel100.BackColor = System.Drawing.Color.Silver;
            this.xrLabel100.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel100.Dpi = 100F;
            this.xrLabel100.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel100.LocationFloat = new DevExpress.Utils.PointFloat(764.3448F, 163.8073F);
            this.xrLabel100.Name = "xrLabel100";
            this.xrLabel100.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel100.SizeF = new System.Drawing.SizeF(50.4162F, 23F);
            this.xrLabel100.StylePriority.UseBackColor = false;
            this.xrLabel100.StylePriority.UseBorders = false;
            this.xrLabel100.StylePriority.UseFont = false;
            this.xrLabel100.StylePriority.UseTextAlignment = false;
            this.xrLabel100.Text = "DLLS";
            this.xrLabel100.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrLabel101
            // 
            this.xrLabel101.BackColor = System.Drawing.Color.Silver;
            this.xrLabel101.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel101.Dpi = 100F;
            this.xrLabel101.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel101.LocationFloat = new DevExpress.Utils.PointFloat(713.9698F, 163.8073F);
            this.xrLabel101.Name = "xrLabel101";
            this.xrLabel101.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel101.SizeF = new System.Drawing.SizeF(50.37497F, 23F);
            this.xrLabel101.StylePriority.UseBackColor = false;
            this.xrLabel101.StylePriority.UseBorders = false;
            this.xrLabel101.StylePriority.UseFont = false;
            this.xrLabel101.StylePriority.UseTextAlignment = false;
            this.xrLabel101.Text = "MXN";
            this.xrLabel101.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrLabel102
            // 
            this.xrLabel102.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.xrLabel102.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel102.Dpi = 100F;
            this.xrLabel102.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel102.LocationFloat = new DevExpress.Utils.PointFloat(713.9698F, 126.1567F);
            this.xrLabel102.Name = "xrLabel102";
            this.xrLabel102.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel102.SizeF = new System.Drawing.SizeF(100.7911F, 37.65073F);
            this.xrLabel102.StylePriority.UseBackColor = false;
            this.xrLabel102.StylePriority.UseBorders = false;
            this.xrLabel102.StylePriority.UseFont = false;
            this.xrLabel102.StylePriority.UseTextAlignment = false;
            this.xrLabel102.Text = "DONATIVOS";
            this.xrLabel102.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrLabel103
            // 
            this.xrLabel103.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.xrLabel103.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel103.Dpi = 100F;
            this.xrLabel103.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel103.LocationFloat = new DevExpress.Utils.PointFloat(814.7609F, 126.1567F);
            this.xrLabel103.Name = "xrLabel103";
            this.xrLabel103.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel103.SizeF = new System.Drawing.SizeF(141.2533F, 37.65073F);
            this.xrLabel103.StylePriority.UseBackColor = false;
            this.xrLabel103.StylePriority.UseBorders = false;
            this.xrLabel103.StylePriority.UseFont = false;
            this.xrLabel103.StylePriority.UseTextAlignment = false;
            this.xrLabel103.Text = "CANCELACIONES";
            this.xrLabel103.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrLabel94
            // 
            this.xrLabel94.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.xrLabel94.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel94.Dpi = 100F;
            this.xrLabel94.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel94.LocationFloat = new DevExpress.Utils.PointFloat(868.4142F, 218.1567F);
            this.xrLabel94.Name = "xrLabel94";
            this.xrLabel94.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel94.SizeF = new System.Drawing.SizeF(152.6089F, 23F);
            this.xrLabel94.StylePriority.UseBackColor = false;
            this.xrLabel94.StylePriority.UseBorders = false;
            this.xrLabel94.StylePriority.UseFont = false;
            this.xrLabel94.StylePriority.UseTextAlignment = false;
            this.xrLabel94.Text = "BONIFICACIONES";
            this.xrLabel94.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrLabel73
            // 
            this.xrLabel73.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.xrLabel73.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel73.Dpi = 100F;
            this.xrLabel73.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel73.LocationFloat = new DevExpress.Utils.PointFloat(713.9698F, 218.1567F);
            this.xrLabel73.Name = "xrLabel73";
            this.xrLabel73.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel73.SizeF = new System.Drawing.SizeF(153.9161F, 23F);
            this.xrLabel73.StylePriority.UseBackColor = false;
            this.xrLabel73.StylePriority.UseBorders = false;
            this.xrLabel73.StylePriority.UseFont = false;
            this.xrLabel73.StylePriority.UseTextAlignment = false;
            this.xrLabel73.Text = "NOTAS DE CREDITO";
            this.xrLabel73.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrLabel74
            // 
            this.xrLabel74.BackColor = System.Drawing.Color.Silver;
            this.xrLabel74.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel74.Dpi = 100F;
            this.xrLabel74.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel74.LocationFloat = new DevExpress.Utils.PointFloat(713.9698F, 241.1567F);
            this.xrLabel74.Name = "xrLabel74";
            this.xrLabel74.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel74.SizeF = new System.Drawing.SizeF(74.33333F, 23F);
            this.xrLabel74.StylePriority.UseBackColor = false;
            this.xrLabel74.StylePriority.UseBorders = false;
            this.xrLabel74.StylePriority.UseFont = false;
            this.xrLabel74.StylePriority.UseTextAlignment = false;
            this.xrLabel74.Text = "MXN";
            this.xrLabel74.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrLabel75
            // 
            this.xrLabel75.BackColor = System.Drawing.Color.Silver;
            this.xrLabel75.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel75.Dpi = 100F;
            this.xrLabel75.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel75.LocationFloat = new DevExpress.Utils.PointFloat(788.3031F, 241.1567F);
            this.xrLabel75.Name = "xrLabel75";
            this.xrLabel75.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel75.SizeF = new System.Drawing.SizeF(79.58279F, 23F);
            this.xrLabel75.StylePriority.UseBackColor = false;
            this.xrLabel75.StylePriority.UseBorders = false;
            this.xrLabel75.StylePriority.UseFont = false;
            this.xrLabel75.StylePriority.UseTextAlignment = false;
            this.xrLabel75.Text = "DLLS";
            this.xrLabel75.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrLabel76
            // 
            this.xrLabel76.BackColor = System.Drawing.Color.Silver;
            this.xrLabel76.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel76.Dpi = 100F;
            this.xrLabel76.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel76.LocationFloat = new DevExpress.Utils.PointFloat(868.4142F, 241.1567F);
            this.xrLabel76.Name = "xrLabel76";
            this.xrLabel76.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel76.SizeF = new System.Drawing.SizeF(82.52769F, 23F);
            this.xrLabel76.StylePriority.UseBackColor = false;
            this.xrLabel76.StylePriority.UseBorders = false;
            this.xrLabel76.StylePriority.UseFont = false;
            this.xrLabel76.StylePriority.UseTextAlignment = false;
            this.xrLabel76.Text = "MXN";
            this.xrLabel76.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrLabel77
            // 
            this.xrLabel77.BackColor = System.Drawing.Color.Silver;
            this.xrLabel77.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel77.Dpi = 100F;
            this.xrLabel77.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel77.LocationFloat = new DevExpress.Utils.PointFloat(950.9417F, 241.1566F);
            this.xrLabel77.Name = "xrLabel77";
            this.xrLabel77.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel77.SizeF = new System.Drawing.SizeF(70.08122F, 23F);
            this.xrLabel77.StylePriority.UseBackColor = false;
            this.xrLabel77.StylePriority.UseBorders = false;
            this.xrLabel77.StylePriority.UseFont = false;
            this.xrLabel77.StylePriority.UseTextAlignment = false;
            this.xrLabel77.Text = "DLLS";
            this.xrLabel77.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // _BONDLLS
            // 
            this._BONDLLS.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this._BONDLLS.Dpi = 100F;
            this._BONDLLS.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._BONDLLS.LocationFloat = new DevExpress.Utils.PointFloat(950.9417F, 264.1566F);
            this._BONDLLS.Name = "_BONDLLS";
            this._BONDLLS.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this._BONDLLS.SizeF = new System.Drawing.SizeF(70.08122F, 23F);
            this._BONDLLS.StylePriority.UseBorders = false;
            this._BONDLLS.StylePriority.UseFont = false;
            this._BONDLLS.StylePriority.UseTextAlignment = false;
            this._BONDLLS.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // _BONMXN
            // 
            this._BONMXN.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this._BONMXN.Dpi = 100F;
            this._BONMXN.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._BONMXN.LocationFloat = new DevExpress.Utils.PointFloat(868.4142F, 264.1566F);
            this._BONMXN.Name = "_BONMXN";
            this._BONMXN.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this._BONMXN.SizeF = new System.Drawing.SizeF(82.52769F, 23F);
            this._BONMXN.StylePriority.UseBorders = false;
            this._BONMXN.StylePriority.UseFont = false;
            this._BONMXN.StylePriority.UseTextAlignment = false;
            this._BONMXN.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // _NCDLLS
            // 
            this._NCDLLS.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this._NCDLLS.Dpi = 100F;
            this._NCDLLS.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._NCDLLS.LocationFloat = new DevExpress.Utils.PointFloat(788.3031F, 264.1567F);
            this._NCDLLS.Name = "_NCDLLS";
            this._NCDLLS.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this._NCDLLS.SizeF = new System.Drawing.SizeF(79.58279F, 23F);
            this._NCDLLS.StylePriority.UseBorders = false;
            this._NCDLLS.StylePriority.UseFont = false;
            this._NCDLLS.StylePriority.UseTextAlignment = false;
            this._NCDLLS.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // _NCMXN
            // 
            this._NCMXN.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this._NCMXN.Dpi = 100F;
            this._NCMXN.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._NCMXN.LocationFloat = new DevExpress.Utils.PointFloat(713.9698F, 264.1567F);
            this._NCMXN.Name = "_NCMXN";
            this._NCMXN.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this._NCMXN.SizeF = new System.Drawing.SizeF(74.33333F, 23F);
            this._NCMXN.StylePriority.UseBorders = false;
            this._NCMXN.StylePriority.UseFont = false;
            this._NCMXN.StylePriority.UseTextAlignment = false;
            this._NCMXN.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLabel82
            // 
            this.xrLabel82.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel82.Dpi = 100F;
            this.xrLabel82.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel82.LocationFloat = new DevExpress.Utils.PointFloat(713.9698F, 287.1566F);
            this.xrLabel82.Name = "xrLabel82";
            this.xrLabel82.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel82.SizeF = new System.Drawing.SizeF(74.33333F, 23F);
            this.xrLabel82.StylePriority.UseBorders = false;
            this.xrLabel82.StylePriority.UseFont = false;
            this.xrLabel82.StylePriority.UseTextAlignment = false;
            this.xrLabel82.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLabel83
            // 
            this.xrLabel83.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrLabel83.Dpi = 100F;
            this.xrLabel83.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel83.LocationFloat = new DevExpress.Utils.PointFloat(788.3032F, 287.1567F);
            this.xrLabel83.Name = "xrLabel83";
            this.xrLabel83.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel83.SizeF = new System.Drawing.SizeF(79.58278F, 23F);
            this.xrLabel83.StylePriority.UseBorders = false;
            this.xrLabel83.StylePriority.UseFont = false;
            this.xrLabel83.StylePriority.UseTextAlignment = false;
            this.xrLabel83.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrLabel84
            // 
            this.xrLabel84.BackColor = System.Drawing.Color.Silver;
            this.xrLabel84.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel84.Dpi = 100F;
            this.xrLabel84.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel84.LocationFloat = new DevExpress.Utils.PointFloat(868.4142F, 287.1567F);
            this.xrLabel84.Name = "xrLabel84";
            this.xrLabel84.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel84.SizeF = new System.Drawing.SizeF(152.6088F, 23F);
            this.xrLabel84.StylePriority.UseBackColor = false;
            this.xrLabel84.StylePriority.UseBorders = false;
            this.xrLabel84.StylePriority.UseFont = false;
            this.xrLabel84.StylePriority.UseTextAlignment = false;
            this.xrLabel84.Text = "CUPON DE DESCUENTO";
            this.xrLabel84.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrLabel86
            // 
            this.xrLabel86.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel86.Dpi = 100F;
            this.xrLabel86.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel86.LocationFloat = new DevExpress.Utils.PointFloat(713.9698F, 310.1567F);
            this.xrLabel86.Name = "xrLabel86";
            this.xrLabel86.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel86.SizeF = new System.Drawing.SizeF(74.33333F, 23F);
            this.xrLabel86.StylePriority.UseBorders = false;
            this.xrLabel86.StylePriority.UseFont = false;
            this.xrLabel86.StylePriority.UseTextAlignment = false;
            this.xrLabel86.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLabel87
            // 
            this.xrLabel87.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel87.Dpi = 100F;
            this.xrLabel87.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel87.LocationFloat = new DevExpress.Utils.PointFloat(788.3032F, 310.1567F);
            this.xrLabel87.Name = "xrLabel87";
            this.xrLabel87.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel87.SizeF = new System.Drawing.SizeF(79.58278F, 23F);
            this.xrLabel87.StylePriority.UseBorders = false;
            this.xrLabel87.StylePriority.UseFont = false;
            this.xrLabel87.StylePriority.UseTextAlignment = false;
            this.xrLabel87.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrLabel88
            // 
            this.xrLabel88.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel88.Dpi = 100F;
            this.xrLabel88.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel88.LocationFloat = new DevExpress.Utils.PointFloat(868.4142F, 310.1567F);
            this.xrLabel88.Name = "xrLabel88";
            this.xrLabel88.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel88.SizeF = new System.Drawing.SizeF(82.52768F, 23F);
            this.xrLabel88.StylePriority.UseBorders = false;
            this.xrLabel88.StylePriority.UseFont = false;
            this.xrLabel88.StylePriority.UseTextAlignment = false;
            this.xrLabel88.Text = "MXN";
            this.xrLabel88.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // _DESCDLLS
            // 
            this._DESCDLLS.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this._DESCDLLS.Dpi = 100F;
            this._DESCDLLS.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._DESCDLLS.LocationFloat = new DevExpress.Utils.PointFloat(950.9417F, 310.1567F);
            this._DESCDLLS.Name = "_DESCDLLS";
            this._DESCDLLS.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this._DESCDLLS.SizeF = new System.Drawing.SizeF(70.08109F, 23F);
            this._DESCDLLS.StylePriority.UseBorders = false;
            this._DESCDLLS.StylePriority.UseFont = false;
            this._DESCDLLS.StylePriority.UseTextAlignment = false;
            this._DESCDLLS.Text = "DLLS";
            this._DESCDLLS.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // _DESDLLS
            // 
            this._DESDLLS.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this._DESDLLS.Dpi = 100F;
            this._DESDLLS.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._DESDLLS.LocationFloat = new DevExpress.Utils.PointFloat(950.9417F, 333.1566F);
            this._DESDLLS.Name = "_DESDLLS";
            this._DESDLLS.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this._DESDLLS.SizeF = new System.Drawing.SizeF(70.08109F, 23F);
            this._DESDLLS.StylePriority.UseBorders = false;
            this._DESDLLS.StylePriority.UseFont = false;
            this._DESDLLS.StylePriority.UseTextAlignment = false;
            this._DESDLLS.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // _DESCMXN
            // 
            this._DESCMXN.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this._DESCMXN.Dpi = 100F;
            this._DESCMXN.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._DESCMXN.LocationFloat = new DevExpress.Utils.PointFloat(868.4142F, 333.1566F);
            this._DESCMXN.Name = "_DESCMXN";
            this._DESCMXN.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this._DESCMXN.SizeF = new System.Drawing.SizeF(82.52769F, 23F);
            this._DESCMXN.StylePriority.UseBorders = false;
            this._DESCMXN.StylePriority.UseFont = false;
            this._DESCMXN.StylePriority.UseTextAlignment = false;
            this._DESCMXN.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrLabel92
            // 
            this.xrLabel92.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel92.Dpi = 100F;
            this.xrLabel92.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel92.LocationFloat = new DevExpress.Utils.PointFloat(788.3031F, 333.1567F);
            this.xrLabel92.Name = "xrLabel92";
            this.xrLabel92.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel92.SizeF = new System.Drawing.SizeF(79.58279F, 23F);
            this.xrLabel92.StylePriority.UseBorders = false;
            this.xrLabel92.StylePriority.UseFont = false;
            this.xrLabel92.StylePriority.UseTextAlignment = false;
            this.xrLabel92.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrLabel93
            // 
            this.xrLabel93.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel93.Dpi = 100F;
            this.xrLabel93.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel93.LocationFloat = new DevExpress.Utils.PointFloat(713.9698F, 333.1567F);
            this.xrLabel93.Name = "xrLabel93";
            this.xrLabel93.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel93.SizeF = new System.Drawing.SizeF(74.33333F, 23F);
            this.xrLabel93.StylePriority.UseBorders = false;
            this.xrLabel93.StylePriority.UseFont = false;
            this.xrLabel93.StylePriority.UseTextAlignment = false;
            this.xrLabel93.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLabel44
            // 
            this.xrLabel44.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.xrLabel44.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel44.Dpi = 100F;
            this.xrLabel44.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel44.LocationFloat = new DevExpress.Utils.PointFloat(347.3089F, 126.1567F);
            this.xrLabel44.Name = "xrLabel44";
            this.xrLabel44.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel44.SizeF = new System.Drawing.SizeF(355F, 23.00002F);
            this.xrLabel44.StylePriority.UseBackColor = false;
            this.xrLabel44.StylePriority.UseBorders = false;
            this.xrLabel44.StylePriority.UseFont = false;
            this.xrLabel44.StylePriority.UseTextAlignment = false;
            this.xrLabel44.Text = "VENTAS MONEDA AMERICANA";
            this.xrLabel44.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrLabel45
            // 
            this.xrLabel45.BackColor = System.Drawing.Color.Silver;
            this.xrLabel45.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel45.Dpi = 100F;
            this.xrLabel45.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel45.LocationFloat = new DevExpress.Utils.PointFloat(347.3088F, 149.1567F);
            this.xrLabel45.Name = "xrLabel45";
            this.xrLabel45.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel45.SizeF = new System.Drawing.SizeF(71F, 23F);
            this.xrLabel45.StylePriority.UseBackColor = false;
            this.xrLabel45.StylePriority.UseBorders = false;
            this.xrLabel45.StylePriority.UseFont = false;
            this.xrLabel45.StylePriority.UseTextAlignment = false;
            this.xrLabel45.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrLabel46
            // 
            this.xrLabel46.BackColor = System.Drawing.Color.Silver;
            this.xrLabel46.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel46.Dpi = 100F;
            this.xrLabel46.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel46.LocationFloat = new DevExpress.Utils.PointFloat(418.3087F, 149.1567F);
            this.xrLabel46.Name = "xrLabel46";
            this.xrLabel46.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel46.SizeF = new System.Drawing.SizeF(71F, 23F);
            this.xrLabel46.StylePriority.UseBackColor = false;
            this.xrLabel46.StylePriority.UseBorders = false;
            this.xrLabel46.StylePriority.UseFont = false;
            this.xrLabel46.StylePriority.UseTextAlignment = false;
            this.xrLabel46.Text = "CREDITO";
            this.xrLabel46.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrLabel47
            // 
            this.xrLabel47.BackColor = System.Drawing.Color.Silver;
            this.xrLabel47.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel47.Dpi = 100F;
            this.xrLabel47.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel47.LocationFloat = new DevExpress.Utils.PointFloat(489.3087F, 149.1566F);
            this.xrLabel47.Name = "xrLabel47";
            this.xrLabel47.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel47.SizeF = new System.Drawing.SizeF(71F, 23F);
            this.xrLabel47.StylePriority.UseBackColor = false;
            this.xrLabel47.StylePriority.UseBorders = false;
            this.xrLabel47.StylePriority.UseFont = false;
            this.xrLabel47.StylePriority.UseTextAlignment = false;
            this.xrLabel47.Text = "CONTADO";
            this.xrLabel47.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrLabel48
            // 
            this.xrLabel48.BackColor = System.Drawing.Color.Silver;
            this.xrLabel48.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel48.Dpi = 100F;
            this.xrLabel48.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel48.LocationFloat = new DevExpress.Utils.PointFloat(560.3085F, 149.1567F);
            this.xrLabel48.Name = "xrLabel48";
            this.xrLabel48.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel48.SizeF = new System.Drawing.SizeF(71F, 23F);
            this.xrLabel48.StylePriority.UseBackColor = false;
            this.xrLabel48.StylePriority.UseBorders = false;
            this.xrLabel48.StylePriority.UseFont = false;
            this.xrLabel48.StylePriority.UseTextAlignment = false;
            this.xrLabel48.Text = "SUMA";
            this.xrLabel48.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // _USD02
            // 
            this._USD02.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this._USD02.Dpi = 100F;
            this._USD02.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._USD02.LocationFloat = new DevExpress.Utils.PointFloat(560.3088F, 172.1567F);
            this._USD02.Name = "_USD02";
            this._USD02.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this._USD02.SizeF = new System.Drawing.SizeF(70.99963F, 23F);
            this._USD02.StylePriority.UseBorders = false;
            this._USD02.StylePriority.UseFont = false;
            this._USD02.StylePriority.UseTextAlignment = false;
            this._USD02.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // _USD01
            // 
            this._USD01.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this._USD01.Dpi = 100F;
            this._USD01.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._USD01.LocationFloat = new DevExpress.Utils.PointFloat(489.3087F, 172.1566F);
            this._USD01.Name = "_USD01";
            this._USD01.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this._USD01.SizeF = new System.Drawing.SizeF(71.00015F, 23F);
            this._USD01.StylePriority.UseBorders = false;
            this._USD01.StylePriority.UseFont = false;
            this._USD01.StylePriority.UseTextAlignment = false;
            this._USD01.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // _USD00
            // 
            this._USD00.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this._USD00.Dpi = 100F;
            this._USD00.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._USD00.LocationFloat = new DevExpress.Utils.PointFloat(418.3087F, 172.1567F);
            this._USD00.Name = "_USD00";
            this._USD00.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this._USD00.SizeF = new System.Drawing.SizeF(70.99994F, 23F);
            this._USD00.StylePriority.UseBorders = false;
            this._USD00.StylePriority.UseFont = false;
            this._USD00.StylePriority.UseTextAlignment = false;
            this._USD00.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrLabel52
            // 
            this.xrLabel52.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel52.Dpi = 100F;
            this.xrLabel52.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel52.LocationFloat = new DevExpress.Utils.PointFloat(347.3088F, 172.1567F);
            this.xrLabel52.Name = "xrLabel52";
            this.xrLabel52.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel52.SizeF = new System.Drawing.SizeF(70.99991F, 23F);
            this.xrLabel52.StylePriority.UseBorders = false;
            this.xrLabel52.StylePriority.UseFont = false;
            this.xrLabel52.StylePriority.UseTextAlignment = false;
            this.xrLabel52.Text = "TASA 0:";
            this.xrLabel52.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLabel53
            // 
            this.xrLabel53.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel53.Dpi = 100F;
            this.xrLabel53.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel53.LocationFloat = new DevExpress.Utils.PointFloat(347.3088F, 195.1566F);
            this.xrLabel53.Name = "xrLabel53";
            this.xrLabel53.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel53.SizeF = new System.Drawing.SizeF(70.99997F, 23F);
            this.xrLabel53.StylePriority.UseBorders = false;
            this.xrLabel53.StylePriority.UseFont = false;
            this.xrLabel53.StylePriority.UseTextAlignment = false;
            this.xrLabel53.Text = "GRAVADO:";
            this.xrLabel53.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // _USD10
            // 
            this._USD10.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this._USD10.Dpi = 100F;
            this._USD10.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._USD10.LocationFloat = new DevExpress.Utils.PointFloat(418.3088F, 195.1566F);
            this._USD10.Name = "_USD10";
            this._USD10.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this._USD10.SizeF = new System.Drawing.SizeF(70.99991F, 23F);
            this._USD10.StylePriority.UseBorders = false;
            this._USD10.StylePriority.UseFont = false;
            this._USD10.StylePriority.UseTextAlignment = false;
            this._USD10.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // _USD11
            // 
            this._USD11.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this._USD11.Dpi = 100F;
            this._USD11.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._USD11.LocationFloat = new DevExpress.Utils.PointFloat(489.3087F, 195.1566F);
            this._USD11.Name = "_USD11";
            this._USD11.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this._USD11.SizeF = new System.Drawing.SizeF(70.99997F, 23F);
            this._USD11.StylePriority.UseBorders = false;
            this._USD11.StylePriority.UseFont = false;
            this._USD11.StylePriority.UseTextAlignment = false;
            this._USD11.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // _USD12
            // 
            this._USD12.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this._USD12.Dpi = 100F;
            this._USD12.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._USD12.LocationFloat = new DevExpress.Utils.PointFloat(560.3087F, 195.1566F);
            this._USD12.Name = "_USD12";
            this._USD12.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this._USD12.SizeF = new System.Drawing.SizeF(70.99969F, 23F);
            this._USD12.StylePriority.UseBorders = false;
            this._USD12.StylePriority.UseFont = false;
            this._USD12.StylePriority.UseTextAlignment = false;
            this._USD12.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrLabel57
            // 
            this.xrLabel57.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel57.Dpi = 100F;
            this.xrLabel57.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel57.LocationFloat = new DevExpress.Utils.PointFloat(347.3088F, 241.1565F);
            this.xrLabel57.Name = "xrLabel57";
            this.xrLabel57.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel57.SizeF = new System.Drawing.SizeF(70.99991F, 23F);
            this.xrLabel57.StylePriority.UseBorders = false;
            this.xrLabel57.StylePriority.UseFont = false;
            this.xrLabel57.StylePriority.UseTextAlignment = false;
            this.xrLabel57.Text = "IEPS 0:";
            this.xrLabel57.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // _USD20
            // 
            this._USD20.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this._USD20.Dpi = 100F;
            this._USD20.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._USD20.LocationFloat = new DevExpress.Utils.PointFloat(418.3088F, 241.1565F);
            this._USD20.Name = "_USD20";
            this._USD20.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this._USD20.SizeF = new System.Drawing.SizeF(70.99991F, 23F);
            this._USD20.StylePriority.UseBorders = false;
            this._USD20.StylePriority.UseFont = false;
            this._USD20.StylePriority.UseTextAlignment = false;
            this._USD20.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // _USD21
            // 
            this._USD21.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this._USD21.Dpi = 100F;
            this._USD21.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._USD21.LocationFloat = new DevExpress.Utils.PointFloat(489.3087F, 241.1565F);
            this._USD21.Name = "_USD21";
            this._USD21.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this._USD21.SizeF = new System.Drawing.SizeF(70.99997F, 23F);
            this._USD21.StylePriority.UseBorders = false;
            this._USD21.StylePriority.UseFont = false;
            this._USD21.StylePriority.UseTextAlignment = false;
            this._USD21.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // _USD22
            // 
            this._USD22.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this._USD22.Dpi = 100F;
            this._USD22.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._USD22.LocationFloat = new DevExpress.Utils.PointFloat(560.3087F, 241.1565F);
            this._USD22.Name = "_USD22";
            this._USD22.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this._USD22.SizeF = new System.Drawing.SizeF(70.99969F, 23F);
            this._USD22.StylePriority.UseBorders = false;
            this._USD22.StylePriority.UseFont = false;
            this._USD22.StylePriority.UseTextAlignment = false;
            this._USD22.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // _USD42
            // 
            this._USD42.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this._USD42.Dpi = 100F;
            this._USD42.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._USD42.LocationFloat = new DevExpress.Utils.PointFloat(560.3087F, 287.1563F);
            this._USD42.Name = "_USD42";
            this._USD42.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this._USD42.SizeF = new System.Drawing.SizeF(70.99969F, 23.00003F);
            this._USD42.StylePriority.UseBorders = false;
            this._USD42.StylePriority.UseFont = false;
            this._USD42.StylePriority.UseTextAlignment = false;
            this._USD42.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // _USD41
            // 
            this._USD41.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this._USD41.Dpi = 100F;
            this._USD41.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._USD41.LocationFloat = new DevExpress.Utils.PointFloat(489.3087F, 287.1563F);
            this._USD41.Name = "_USD41";
            this._USD41.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this._USD41.SizeF = new System.Drawing.SizeF(71.00015F, 23.00003F);
            this._USD41.StylePriority.UseBorders = false;
            this._USD41.StylePriority.UseFont = false;
            this._USD41.StylePriority.UseTextAlignment = false;
            this._USD41.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // _USD40
            // 
            this._USD40.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this._USD40.Dpi = 100F;
            this._USD40.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._USD40.LocationFloat = new DevExpress.Utils.PointFloat(418.3088F, 287.1563F);
            this._USD40.Name = "_USD40";
            this._USD40.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this._USD40.SizeF = new System.Drawing.SizeF(70.99991F, 23.00003F);
            this._USD40.StylePriority.UseBorders = false;
            this._USD40.StylePriority.UseFont = false;
            this._USD40.StylePriority.UseTextAlignment = false;
            this._USD40.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrLabel64
            // 
            this.xrLabel64.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel64.Dpi = 100F;
            this.xrLabel64.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel64.LocationFloat = new DevExpress.Utils.PointFloat(347.3088F, 287.1563F);
            this.xrLabel64.Name = "xrLabel64";
            this.xrLabel64.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel64.SizeF = new System.Drawing.SizeF(70.99988F, 23.00003F);
            this.xrLabel64.StylePriority.UseBorders = false;
            this.xrLabel64.StylePriority.UseFont = false;
            this.xrLabel64.StylePriority.UseTextAlignment = false;
            this.xrLabel64.Text = "IVA:";
            this.xrLabel64.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // _USD32
            // 
            this._USD32.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this._USD32.Dpi = 100F;
            this._USD32.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._USD32.LocationFloat = new DevExpress.Utils.PointFloat(560.3087F, 264.1565F);
            this._USD32.Name = "_USD32";
            this._USD32.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this._USD32.SizeF = new System.Drawing.SizeF(70.99969F, 23F);
            this._USD32.StylePriority.UseBorders = false;
            this._USD32.StylePriority.UseFont = false;
            this._USD32.StylePriority.UseTextAlignment = false;
            this._USD32.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // _USD31
            // 
            this._USD31.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this._USD31.Dpi = 100F;
            this._USD31.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._USD31.LocationFloat = new DevExpress.Utils.PointFloat(489.3087F, 264.1563F);
            this._USD31.Name = "_USD31";
            this._USD31.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this._USD31.SizeF = new System.Drawing.SizeF(71.00015F, 23.00002F);
            this._USD31.StylePriority.UseBorders = false;
            this._USD31.StylePriority.UseFont = false;
            this._USD31.StylePriority.UseTextAlignment = false;
            this._USD31.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // _USD30
            // 
            this._USD30.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this._USD30.Dpi = 100F;
            this._USD30.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._USD30.LocationFloat = new DevExpress.Utils.PointFloat(418.3087F, 264.1565F);
            this._USD30.Name = "_USD30";
            this._USD30.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this._USD30.SizeF = new System.Drawing.SizeF(70.99994F, 23F);
            this._USD30.StylePriority.UseBorders = false;
            this._USD30.StylePriority.UseFont = false;
            this._USD30.StylePriority.UseTextAlignment = false;
            this._USD30.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrLabel68
            // 
            this.xrLabel68.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel68.Dpi = 100F;
            this.xrLabel68.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel68.LocationFloat = new DevExpress.Utils.PointFloat(347.3088F, 264.1565F);
            this.xrLabel68.Name = "xrLabel68";
            this.xrLabel68.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel68.SizeF = new System.Drawing.SizeF(70.99991F, 23F);
            this.xrLabel68.StylePriority.UseBorders = false;
            this.xrLabel68.StylePriority.UseFont = false;
            this.xrLabel68.StylePriority.UseTextAlignment = false;
            this.xrLabel68.Text = "IEPS:";
            this.xrLabel68.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // _USD52
            // 
            this._USD52.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this._USD52.Dpi = 100F;
            this._USD52.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._USD52.LocationFloat = new DevExpress.Utils.PointFloat(560.3087F, 310.1565F);
            this._USD52.Name = "_USD52";
            this._USD52.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this._USD52.SizeF = new System.Drawing.SizeF(70.99969F, 23F);
            this._USD52.StylePriority.UseBorders = false;
            this._USD52.StylePriority.UseFont = false;
            this._USD52.StylePriority.UseTextAlignment = false;
            this._USD52.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // _USD51
            // 
            this._USD51.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this._USD51.Dpi = 100F;
            this._USD51.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._USD51.LocationFloat = new DevExpress.Utils.PointFloat(489.3087F, 310.1565F);
            this._USD51.Name = "_USD51";
            this._USD51.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this._USD51.SizeF = new System.Drawing.SizeF(70.99997F, 23F);
            this._USD51.StylePriority.UseBorders = false;
            this._USD51.StylePriority.UseFont = false;
            this._USD51.StylePriority.UseTextAlignment = false;
            this._USD51.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // _USD50
            // 
            this._USD50.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this._USD50.Dpi = 100F;
            this._USD50.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._USD50.LocationFloat = new DevExpress.Utils.PointFloat(418.3088F, 310.1565F);
            this._USD50.Name = "_USD50";
            this._USD50.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this._USD50.SizeF = new System.Drawing.SizeF(70.99991F, 23F);
            this._USD50.StylePriority.UseBorders = false;
            this._USD50.StylePriority.UseFont = false;
            this._USD50.StylePriority.UseTextAlignment = false;
            this._USD50.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrLabel72
            // 
            this.xrLabel72.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel72.Dpi = 100F;
            this.xrLabel72.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel72.LocationFloat = new DevExpress.Utils.PointFloat(347.3089F, 310.1565F);
            this.xrLabel72.Name = "xrLabel72";
            this.xrLabel72.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel72.SizeF = new System.Drawing.SizeF(70.99991F, 23F);
            this.xrLabel72.StylePriority.UseBorders = false;
            this.xrLabel72.StylePriority.UseFont = false;
            this.xrLabel72.StylePriority.UseTextAlignment = false;
            this.xrLabel72.Text = "TOTAL:";
            this.xrLabel72.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLabel40
            // 
            this.xrLabel40.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel40.Dpi = 100F;
            this.xrLabel40.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel40.LocationFloat = new DevExpress.Utils.PointFloat(10.44257F, 310.1565F);
            this.xrLabel40.Name = "xrLabel40";
            this.xrLabel40.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel40.SizeF = new System.Drawing.SizeF(77.33333F, 23F);
            this.xrLabel40.StylePriority.UseBorders = false;
            this.xrLabel40.StylePriority.UseFont = false;
            this.xrLabel40.StylePriority.UseTextAlignment = false;
            this.xrLabel40.Text = "TOTAL:";
            this.xrLabel40.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // _MN50
            // 
            this._MN50.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this._MN50.Dpi = 100F;
            this._MN50.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._MN50.LocationFloat = new DevExpress.Utils.PointFloat(87.77602F, 310.1565F);
            this._MN50.Name = "_MN50";
            this._MN50.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this._MN50.SizeF = new System.Drawing.SizeF(79.58278F, 23F);
            this._MN50.StylePriority.UseBorders = false;
            this._MN50.StylePriority.UseFont = false;
            this._MN50.StylePriority.UseTextAlignment = false;
            this._MN50.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // _MN51
            // 
            this._MN51.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this._MN51.Dpi = 100F;
            this._MN51.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._MN51.LocationFloat = new DevExpress.Utils.PointFloat(167.3587F, 310.1565F);
            this._MN51.Name = "_MN51";
            this._MN51.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this._MN51.SizeF = new System.Drawing.SizeF(82.52768F, 23F);
            this._MN51.StylePriority.UseBorders = false;
            this._MN51.StylePriority.UseFont = false;
            this._MN51.StylePriority.UseTextAlignment = false;
            this._MN51.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // _MN52
            // 
            this._MN52.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this._MN52.Dpi = 100F;
            this._MN52.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._MN52.LocationFloat = new DevExpress.Utils.PointFloat(249.8865F, 310.1565F);
            this._MN52.Name = "_MN52";
            this._MN52.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this._MN52.SizeF = new System.Drawing.SizeF(81.49771F, 23F);
            this._MN52.StylePriority.UseBorders = false;
            this._MN52.StylePriority.UseFont = false;
            this._MN52.StylePriority.UseTextAlignment = false;
            this._MN52.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrLabel31
            // 
            this.xrLabel31.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel31.Dpi = 100F;
            this.xrLabel31.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel31.LocationFloat = new DevExpress.Utils.PointFloat(10.44257F, 264.1565F);
            this.xrLabel31.Name = "xrLabel31";
            this.xrLabel31.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel31.SizeF = new System.Drawing.SizeF(77.33333F, 23F);
            this.xrLabel31.StylePriority.UseBorders = false;
            this.xrLabel31.StylePriority.UseFont = false;
            this.xrLabel31.StylePriority.UseTextAlignment = false;
            this.xrLabel31.Text = "IEPS:";
            this.xrLabel31.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // _MN30
            // 
            this._MN30.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this._MN30.Dpi = 100F;
            this._MN30.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._MN30.LocationFloat = new DevExpress.Utils.PointFloat(87.77593F, 264.1565F);
            this._MN30.Name = "_MN30";
            this._MN30.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this._MN30.SizeF = new System.Drawing.SizeF(79.58279F, 23F);
            this._MN30.StylePriority.UseBorders = false;
            this._MN30.StylePriority.UseFont = false;
            this._MN30.StylePriority.UseTextAlignment = false;
            this._MN30.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // _MN31
            // 
            this._MN31.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this._MN31.Dpi = 100F;
            this._MN31.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._MN31.LocationFloat = new DevExpress.Utils.PointFloat(167.3587F, 264.1563F);
            this._MN31.Name = "_MN31";
            this._MN31.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this._MN31.SizeF = new System.Drawing.SizeF(82.52769F, 23F);
            this._MN31.StylePriority.UseBorders = false;
            this._MN31.StylePriority.UseFont = false;
            this._MN31.StylePriority.UseTextAlignment = false;
            this._MN31.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // _MN32
            // 
            this._MN32.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this._MN32.Dpi = 100F;
            this._MN32.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._MN32.LocationFloat = new DevExpress.Utils.PointFloat(249.8865F, 264.1563F);
            this._MN32.Name = "_MN32";
            this._MN32.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this._MN32.SizeF = new System.Drawing.SizeF(81.49771F, 23F);
            this._MN32.StylePriority.UseBorders = false;
            this._MN32.StylePriority.UseFont = false;
            this._MN32.StylePriority.UseTextAlignment = false;
            this._MN32.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrLabel36
            // 
            this.xrLabel36.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel36.Dpi = 100F;
            this.xrLabel36.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel36.LocationFloat = new DevExpress.Utils.PointFloat(10.44257F, 287.1564F);
            this.xrLabel36.Name = "xrLabel36";
            this.xrLabel36.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel36.SizeF = new System.Drawing.SizeF(77.33333F, 23F);
            this.xrLabel36.StylePriority.UseBorders = false;
            this.xrLabel36.StylePriority.UseFont = false;
            this.xrLabel36.StylePriority.UseTextAlignment = false;
            this.xrLabel36.Text = "IVA:";
            this.xrLabel36.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // _MN40
            // 
            this._MN40.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this._MN40.Dpi = 100F;
            this._MN40.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._MN40.LocationFloat = new DevExpress.Utils.PointFloat(87.77602F, 287.1564F);
            this._MN40.Name = "_MN40";
            this._MN40.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this._MN40.SizeF = new System.Drawing.SizeF(79.58278F, 23.00003F);
            this._MN40.StylePriority.UseBorders = false;
            this._MN40.StylePriority.UseFont = false;
            this._MN40.StylePriority.UseTextAlignment = false;
            this._MN40.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // _MN41
            // 
            this._MN41.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this._MN41.Dpi = 100F;
            this._MN41.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._MN41.LocationFloat = new DevExpress.Utils.PointFloat(167.3587F, 287.1564F);
            this._MN41.Name = "_MN41";
            this._MN41.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this._MN41.SizeF = new System.Drawing.SizeF(82.52769F, 23.00003F);
            this._MN41.StylePriority.UseBorders = false;
            this._MN41.StylePriority.UseFont = false;
            this._MN41.StylePriority.UseTextAlignment = false;
            this._MN41.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // _MN42
            // 
            this._MN42.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this._MN42.Dpi = 100F;
            this._MN42.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._MN42.LocationFloat = new DevExpress.Utils.PointFloat(249.8865F, 287.1564F);
            this._MN42.Name = "_MN42";
            this._MN42.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this._MN42.SizeF = new System.Drawing.SizeF(81.49771F, 23.00003F);
            this._MN42.StylePriority.UseBorders = false;
            this._MN42.StylePriority.UseFont = false;
            this._MN42.StylePriority.UseTextAlignment = false;
            this._MN42.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // _MN22
            // 
            this._MN22.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this._MN22.Dpi = 100F;
            this._MN22.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._MN22.LocationFloat = new DevExpress.Utils.PointFloat(249.8865F, 241.1564F);
            this._MN22.Name = "_MN22";
            this._MN22.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this._MN22.SizeF = new System.Drawing.SizeF(81.49771F, 23F);
            this._MN22.StylePriority.UseBorders = false;
            this._MN22.StylePriority.UseFont = false;
            this._MN22.StylePriority.UseTextAlignment = false;
            this._MN22.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // _MN21
            // 
            this._MN21.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this._MN21.Dpi = 100F;
            this._MN21.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._MN21.LocationFloat = new DevExpress.Utils.PointFloat(167.3587F, 241.1565F);
            this._MN21.Name = "_MN21";
            this._MN21.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this._MN21.SizeF = new System.Drawing.SizeF(82.52768F, 23F);
            this._MN21.StylePriority.UseBorders = false;
            this._MN21.StylePriority.UseFont = false;
            this._MN21.StylePriority.UseTextAlignment = false;
            this._MN21.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // _MN20
            // 
            this._MN20.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this._MN20.Dpi = 100F;
            this._MN20.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._MN20.LocationFloat = new DevExpress.Utils.PointFloat(87.77602F, 241.1565F);
            this._MN20.Name = "_MN20";
            this._MN20.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this._MN20.SizeF = new System.Drawing.SizeF(79.58278F, 23F);
            this._MN20.StylePriority.UseBorders = false;
            this._MN20.StylePriority.UseFont = false;
            this._MN20.StylePriority.UseTextAlignment = false;
            this._MN20.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrLabel30
            // 
            this.xrLabel30.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel30.Dpi = 100F;
            this.xrLabel30.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel30.LocationFloat = new DevExpress.Utils.PointFloat(10.44257F, 241.1565F);
            this.xrLabel30.Name = "xrLabel30";
            this.xrLabel30.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel30.SizeF = new System.Drawing.SizeF(77.33333F, 23F);
            this.xrLabel30.StylePriority.UseBorders = false;
            this.xrLabel30.StylePriority.UseFont = false;
            this.xrLabel30.StylePriority.UseTextAlignment = false;
            this.xrLabel30.Text = "IEPS 0:";
            this.xrLabel30.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // _MN12
            // 
            this._MN12.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this._MN12.Dpi = 100F;
            this._MN12.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._MN12.LocationFloat = new DevExpress.Utils.PointFloat(249.8865F, 195.1566F);
            this._MN12.Name = "_MN12";
            this._MN12.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this._MN12.SizeF = new System.Drawing.SizeF(81.49771F, 23F);
            this._MN12.StylePriority.UseBorders = false;
            this._MN12.StylePriority.UseFont = false;
            this._MN12.StylePriority.UseTextAlignment = false;
            this._MN12.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // _MN11
            // 
            this._MN11.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this._MN11.Dpi = 100F;
            this._MN11.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._MN11.LocationFloat = new DevExpress.Utils.PointFloat(167.3587F, 195.1566F);
            this._MN11.Name = "_MN11";
            this._MN11.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this._MN11.SizeF = new System.Drawing.SizeF(82.52768F, 23F);
            this._MN11.StylePriority.UseBorders = false;
            this._MN11.StylePriority.UseFont = false;
            this._MN11.StylePriority.UseTextAlignment = false;
            this._MN11.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // _MN10
            // 
            this._MN10.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this._MN10.Dpi = 100F;
            this._MN10.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._MN10.LocationFloat = new DevExpress.Utils.PointFloat(87.77602F, 195.1566F);
            this._MN10.Name = "_MN10";
            this._MN10.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this._MN10.SizeF = new System.Drawing.SizeF(79.58278F, 23F);
            this._MN10.StylePriority.UseBorders = false;
            this._MN10.StylePriority.UseFont = false;
            this._MN10.StylePriority.UseTextAlignment = false;
            this._MN10.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrLabel26
            // 
            this.xrLabel26.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel26.Dpi = 100F;
            this.xrLabel26.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel26.LocationFloat = new DevExpress.Utils.PointFloat(10.44257F, 195.1566F);
            this.xrLabel26.Name = "xrLabel26";
            this.xrLabel26.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel26.SizeF = new System.Drawing.SizeF(77.33333F, 23F);
            this.xrLabel26.StylePriority.UseBorders = false;
            this.xrLabel26.StylePriority.UseFont = false;
            this.xrLabel26.StylePriority.UseTextAlignment = false;
            this.xrLabel26.Text = "GRAVADO:";
            this.xrLabel26.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLabel14
            // 
            this.xrLabel14.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel14.Dpi = 100F;
            this.xrLabel14.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel14.LocationFloat = new DevExpress.Utils.PointFloat(10.44257F, 172.1567F);
            this.xrLabel14.Name = "xrLabel14";
            this.xrLabel14.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel14.SizeF = new System.Drawing.SizeF(77.33333F, 23F);
            this.xrLabel14.StylePriority.UseBorders = false;
            this.xrLabel14.StylePriority.UseFont = false;
            this.xrLabel14.StylePriority.UseTextAlignment = false;
            this.xrLabel14.Text = "TASA 0:";
            this.xrLabel14.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // _MN00
            // 
            this._MN00.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this._MN00.Dpi = 100F;
            this._MN00.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._MN00.LocationFloat = new DevExpress.Utils.PointFloat(87.77589F, 172.1567F);
            this._MN00.Name = "_MN00";
            this._MN00.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this._MN00.SizeF = new System.Drawing.SizeF(79.58279F, 23F);
            this._MN00.StylePriority.UseBorders = false;
            this._MN00.StylePriority.UseFont = false;
            this._MN00.StylePriority.UseTextAlignment = false;
            this._MN00.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // _MN01
            // 
            this._MN01.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this._MN01.Dpi = 100F;
            this._MN01.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._MN01.LocationFloat = new DevExpress.Utils.PointFloat(167.3587F, 172.1566F);
            this._MN01.Name = "_MN01";
            this._MN01.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this._MN01.SizeF = new System.Drawing.SizeF(82.52769F, 23F);
            this._MN01.StylePriority.UseBorders = false;
            this._MN01.StylePriority.UseFont = false;
            this._MN01.StylePriority.UseTextAlignment = false;
            this._MN01.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // _MN02
            // 
            this._MN02.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this._MN02.Dpi = 100F;
            this._MN02.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._MN02.LocationFloat = new DevExpress.Utils.PointFloat(249.8864F, 172.1566F);
            this._MN02.Name = "_MN02";
            this._MN02.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this._MN02.SizeF = new System.Drawing.SizeF(81.49771F, 23F);
            this._MN02.StylePriority.UseBorders = false;
            this._MN02.StylePriority.UseFont = false;
            this._MN02.StylePriority.UseTextAlignment = false;
            this._MN02.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrLabel13
            // 
            this.xrLabel13.BackColor = System.Drawing.Color.Silver;
            this.xrLabel13.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel13.Dpi = 100F;
            this.xrLabel13.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel13.LocationFloat = new DevExpress.Utils.PointFloat(249.8864F, 149.1566F);
            this.xrLabel13.Name = "xrLabel13";
            this.xrLabel13.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel13.SizeF = new System.Drawing.SizeF(81.49771F, 23F);
            this.xrLabel13.StylePriority.UseBackColor = false;
            this.xrLabel13.StylePriority.UseBorders = false;
            this.xrLabel13.StylePriority.UseFont = false;
            this.xrLabel13.StylePriority.UseTextAlignment = false;
            this.xrLabel13.Text = "SUMA";
            this.xrLabel13.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrLabel12
            // 
            this.xrLabel12.BackColor = System.Drawing.Color.Silver;
            this.xrLabel12.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel12.Dpi = 100F;
            this.xrLabel12.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel12.LocationFloat = new DevExpress.Utils.PointFloat(167.3587F, 149.1567F);
            this.xrLabel12.Name = "xrLabel12";
            this.xrLabel12.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel12.SizeF = new System.Drawing.SizeF(82.52769F, 23F);
            this.xrLabel12.StylePriority.UseBackColor = false;
            this.xrLabel12.StylePriority.UseBorders = false;
            this.xrLabel12.StylePriority.UseFont = false;
            this.xrLabel12.StylePriority.UseTextAlignment = false;
            this.xrLabel12.Text = "CONTADO";
            this.xrLabel12.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrLabel11
            // 
            this.xrLabel11.BackColor = System.Drawing.Color.Silver;
            this.xrLabel11.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel11.Dpi = 100F;
            this.xrLabel11.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel11.LocationFloat = new DevExpress.Utils.PointFloat(87.77589F, 149.1567F);
            this.xrLabel11.Name = "xrLabel11";
            this.xrLabel11.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel11.SizeF = new System.Drawing.SizeF(79.58279F, 23F);
            this.xrLabel11.StylePriority.UseBackColor = false;
            this.xrLabel11.StylePriority.UseBorders = false;
            this.xrLabel11.StylePriority.UseFont = false;
            this.xrLabel11.StylePriority.UseTextAlignment = false;
            this.xrLabel11.Text = "CREDITO";
            this.xrLabel11.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrLabel10
            // 
            this.xrLabel10.BackColor = System.Drawing.Color.Silver;
            this.xrLabel10.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel10.Dpi = 100F;
            this.xrLabel10.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel10.LocationFloat = new DevExpress.Utils.PointFloat(10.44257F, 149.1567F);
            this.xrLabel10.Name = "xrLabel10";
            this.xrLabel10.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel10.SizeF = new System.Drawing.SizeF(77.33333F, 23F);
            this.xrLabel10.StylePriority.UseBackColor = false;
            this.xrLabel10.StylePriority.UseBorders = false;
            this.xrLabel10.StylePriority.UseFont = false;
            this.xrLabel10.StylePriority.UseTextAlignment = false;
            this.xrLabel10.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrLabel8
            // 
            this.xrLabel8.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.xrLabel8.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel8.Dpi = 100F;
            this.xrLabel8.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel8.LocationFloat = new DevExpress.Utils.PointFloat(10.44257F, 126.1567F);
            this.xrLabel8.Name = "xrLabel8";
            this.xrLabel8.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel8.SizeF = new System.Drawing.SizeF(320.9416F, 23F);
            this.xrLabel8.StylePriority.UseBackColor = false;
            this.xrLabel8.StylePriority.UseBorders = false;
            this.xrLabel8.StylePriority.UseFont = false;
            this.xrLabel8.StylePriority.UseTextAlignment = false;
            this.xrLabel8.Text = "VENTAS MONEDA NACIONAL";
            this.xrLabel8.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // labelDate
            // 
            this.labelDate.Dpi = 100F;
            this.labelDate.Font = new System.Drawing.Font("Arial Narrow", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelDate.LocationFloat = new DevExpress.Utils.PointFloat(94.31512F, 30F);
            this.labelDate.Name = "labelDate";
            this.labelDate.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.labelDate.SizeF = new System.Drawing.SizeF(100F, 20.00001F);
            this.labelDate.StylePriority.UseFont = false;
            // 
            // xrLine1
            // 
            this.xrLine1.Dpi = 100F;
            this.xrLine1.LocationFloat = new DevExpress.Utils.PointFloat(0F, 76.58339F);
            this.xrLine1.Name = "xrLine1";
            this.xrLine1.SizeF = new System.Drawing.SizeF(1022F, 10F);
            this.xrLine1.StylePriority.UseBorderDashStyle = false;
            // 
            // _labelReporte
            // 
            this._labelReporte.Dpi = 100F;
            this._labelReporte.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._labelReporte.LocationFloat = new DevExpress.Utils.PointFloat(2.000109F, 30.00002F);
            this._labelReporte.Name = "_labelReporte";
            this._labelReporte.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this._labelReporte.SizeF = new System.Drawing.SizeF(92.31501F, 20F);
            this._labelReporte.StylePriority.UseFont = false;
            this._labelReporte.Text = "Portada del:";
            // 
            // xrLabel4
            // 
            this.xrLabel4.Dpi = 100F;
            this.xrLabel4.Font = new System.Drawing.Font("Arial", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel4.LocationFloat = new DevExpress.Utils.PointFloat(843.0468F, 35.00001F);
            this.xrLabel4.Name = "xrLabel4";
            this.xrLabel4.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel4.SizeF = new System.Drawing.SizeF(86.82587F, 15F);
            this.xrLabel4.StylePriority.UseFont = false;
            this.xrLabel4.StylePriority.UseTextAlignment = false;
            this.xrLabel4.Text = "DFL-950802-5N4";
            this.xrLabel4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // siteLabel
            // 
            this.siteLabel.Dpi = 100F;
            this.siteLabel.Font = new System.Drawing.Font("Arial", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.siteLabel.LocationFloat = new DevExpress.Utils.PointFloat(728.8726F, 20.00001F);
            this.siteLabel.Name = "siteLabel";
            this.siteLabel.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.siteLabel.SizeF = new System.Drawing.SizeF(201.0001F, 15F);
            this.siteLabel.StylePriority.UseFont = false;
            this.siteLabel.StylePriority.UseTextAlignment = false;
            this.siteLabel.Text = "*FLORIDO CEDIS";
            this.siteLabel.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // xrLabel3
            // 
            this.xrLabel3.Dpi = 100F;
            this.xrLabel3.Font = new System.Drawing.Font("Arial", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel3.LocationFloat = new DevExpress.Utils.PointFloat(725.7672F, 5.000003F);
            this.xrLabel3.Name = "xrLabel3";
            this.xrLabel3.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel3.SizeF = new System.Drawing.SizeF(204.1053F, 15F);
            this.xrLabel3.StylePriority.UseFont = false;
            this.xrLabel3.StylePriority.UseTextAlignment = false;
            this.xrLabel3.Text = "DISTRIBUIDORA EL FLORIDO S.A. DE C.V.";
            this.xrLabel3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // xrPictureBox1
            // 
            this.xrPictureBox1.Dpi = 100F;
            this.xrPictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("xrPictureBox1.Image")));
            this.xrPictureBox1.LocationFloat = new DevExpress.Utils.PointFloat(934.6667F, 5.000003F);
            this.xrPictureBox1.Name = "xrPictureBox1";
            this.xrPictureBox1.SizeF = new System.Drawing.SizeF(92.58575F, 61.66666F);
            this.xrPictureBox1.Sizing = DevExpress.XtraPrinting.ImageSizeMode.ZoomImage;
            // 
            // PageFooter
            // 
            this.PageFooter.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrPageInfo1});
            this.PageFooter.Dpi = 100F;
            this.PageFooter.Expanded = false;
            this.PageFooter.HeightF = 15.625F;
            this.PageFooter.Name = "PageFooter";
            // 
            // xrPageInfo1
            // 
            this.xrPageInfo1.Dpi = 100F;
            this.xrPageInfo1.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Bold);
            this.xrPageInfo1.Format = "Página {0} de {1}";
            this.xrPageInfo1.LocationFloat = new DevExpress.Utils.PointFloat(0F, 0F);
            this.xrPageInfo1.Name = "xrPageInfo1";
            this.xrPageInfo1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrPageInfo1.SizeF = new System.Drawing.SizeF(100F, 15F);
            this.xrPageInfo1.StylePriority.UseFont = false;
            // 
            // DetailReport
            // 
            this.DetailReport.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.Detail1,
            this.ReportHeader1,
            this.ReportFooter});
            this.DetailReport.Dpi = 100F;
            this.DetailReport.Level = 0;
            this.DetailReport.Name = "DetailReport";
            // 
            // Detail1
            // 
            this.Detail1.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable4,
            this.xrTable3,
            this.xrTable2,
            this.xrTable1});
            this.Detail1.Dpi = 100F;
            this.Detail1.HeightF = 27.29168F;
            this.Detail1.Name = "Detail1";
            // 
            // xrTable4
            // 
            this.xrTable4.BackColor = System.Drawing.Color.White;
            this.xrTable4.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTable4.Dpi = 100F;
            this.xrTable4.Font = new System.Drawing.Font("Arial", 6F);
            this.xrTable4.LocationFloat = new DevExpress.Utils.PointFloat(736.5819F, 0F);
            this.xrTable4.Name = "xrTable4";
            this.xrTable4.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow4});
            this.xrTable4.SizeF = new System.Drawing.SizeF(285.4178F, 20F);
            this.xrTable4.StylePriority.UseBackColor = false;
            this.xrTable4.StylePriority.UseBorders = false;
            this.xrTable4.StylePriority.UseFont = false;
            // 
            // xrTableRow4
            // 
            this.xrTableRow4.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this._DepDLLScc,
            this._DepDLLSCcTipo,
            this._DepDLLSMXNCc});
            this.xrTableRow4.Dpi = 100F;
            this.xrTableRow4.Name = "xrTableRow4";
            this.xrTableRow4.Weight = 1D;
            // 
            // _DepDLLScc
            // 
            this._DepDLLScc.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this._DepDLLScc.Dpi = 100F;
            this._DepDLLScc.Font = new System.Drawing.Font("Arial", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._DepDLLScc.Name = "_DepDLLScc";
            this._DepDLLScc.StylePriority.UseBorders = false;
            this._DepDLLScc.StylePriority.UseFont = false;
            this._DepDLLScc.StylePriority.UseTextAlignment = false;
            this._DepDLLScc.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this._DepDLLScc.Weight = 0.52791723271099933D;
            // 
            // _DepDLLSCcTipo
            // 
            this._DepDLLSCcTipo.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this._DepDLLSCcTipo.Dpi = 100F;
            this._DepDLLSCcTipo.Font = new System.Drawing.Font("Arial", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._DepDLLSCcTipo.Name = "_DepDLLSCcTipo";
            this._DepDLLSCcTipo.StylePriority.UseBorders = false;
            this._DepDLLSCcTipo.StylePriority.UseFont = false;
            this._DepDLLSCcTipo.StylePriority.UseTextAlignment = false;
            this._DepDLLSCcTipo.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this._DepDLLSCcTipo.Weight = 0.51854221962862557D;
            // 
            // _DepDLLSMXNCc
            // 
            this._DepDLLSMXNCc.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this._DepDLLSMXNCc.Dpi = 100F;
            this._DepDLLSMXNCc.Font = new System.Drawing.Font("Arial", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._DepDLLSMXNCc.Name = "_DepDLLSMXNCc";
            this._DepDLLSMXNCc.StylePriority.UseBorders = false;
            this._DepDLLSMXNCc.StylePriority.UseFont = false;
            this._DepDLLSMXNCc.StylePriority.UseTextAlignment = false;
            this._DepDLLSMXNCc.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this._DepDLLSMXNCc.Weight = 0.51854155029360083D;
            // 
            // xrTable3
            // 
            this.xrTable3.BackColor = System.Drawing.Color.White;
            this.xrTable3.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTable3.Dpi = 100F;
            this.xrTable3.Font = new System.Drawing.Font("Arial", 6F);
            this.xrTable3.LocationFloat = new DevExpress.Utils.PointFloat(427.6086F, 6.67572E-06F);
            this.xrTable3.Name = "xrTable3";
            this.xrTable3.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow3});
            this.xrTable3.SizeF = new System.Drawing.SizeF(285.4177F, 20F);
            this.xrTable3.StylePriority.UseBackColor = false;
            this.xrTable3.StylePriority.UseBorders = false;
            this.xrTable3.StylePriority.UseFont = false;
            // 
            // xrTableRow3
            // 
            this.xrTableRow3.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this._DepDLLScajas,
            this._DepDLLSCajasTipo,
            this._DepDLLSMXNCajas});
            this.xrTableRow3.Dpi = 100F;
            this.xrTableRow3.Name = "xrTableRow3";
            this.xrTableRow3.Weight = 1D;
            // 
            // _DepDLLScajas
            // 
            this._DepDLLScajas.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this._DepDLLScajas.Dpi = 100F;
            this._DepDLLScajas.Font = new System.Drawing.Font("Arial", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._DepDLLScajas.Name = "_DepDLLScajas";
            this._DepDLLScajas.StylePriority.UseBorders = false;
            this._DepDLLScajas.StylePriority.UseFont = false;
            this._DepDLLScajas.StylePriority.UseTextAlignment = false;
            this._DepDLLScajas.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this._DepDLLScajas.Weight = 0.52791726877090128D;
            // 
            // _DepDLLSCajasTipo
            // 
            this._DepDLLSCajasTipo.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this._DepDLLSCajasTipo.Dpi = 100F;
            this._DepDLLSCajasTipo.Font = new System.Drawing.Font("Arial", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._DepDLLSCajasTipo.Name = "_DepDLLSCajasTipo";
            this._DepDLLSCajasTipo.StylePriority.UseBorders = false;
            this._DepDLLSCajasTipo.StylePriority.UseFont = false;
            this._DepDLLSCajasTipo.StylePriority.UseTextAlignment = false;
            this._DepDLLSCajasTipo.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this._DepDLLSCajasTipo.Weight = 0.51854088095858109D;
            // 
            // _DepDLLSMXNCajas
            // 
            this._DepDLLSMXNCajas.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this._DepDLLSMXNCajas.Dpi = 100F;
            this._DepDLLSMXNCajas.Font = new System.Drawing.Font("Arial", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._DepDLLSMXNCajas.Name = "_DepDLLSMXNCajas";
            this._DepDLLSMXNCajas.StylePriority.UseBorders = false;
            this._DepDLLSMXNCajas.StylePriority.UseFont = false;
            this._DepDLLSMXNCajas.StylePriority.UseTextAlignment = false;
            this._DepDLLSMXNCajas.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this._DepDLLSMXNCajas.Weight = 0.51854184312767559D;
            // 
            // xrTable2
            // 
            this.xrTable2.BackColor = System.Drawing.Color.White;
            this.xrTable2.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrTable2.Dpi = 100F;
            this.xrTable2.Font = new System.Drawing.Font("Arial", 6F);
            this.xrTable2.LocationFloat = new DevExpress.Utils.PointFloat(219.7342F, 0F);
            this.xrTable2.Name = "xrTable2";
            this.xrTable2.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow2});
            this.xrTable2.SizeF = new System.Drawing.SizeF(184.5694F, 20F);
            this.xrTable2.StylePriority.UseBackColor = false;
            this.xrTable2.StylePriority.UseBorders = false;
            this.xrTable2.StylePriority.UseFont = false;
            // 
            // xrTableRow2
            // 
            this.xrTableRow2.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this._DepMXNcc,
            this._DepTipoCc});
            this.xrTableRow2.Dpi = 100F;
            this.xrTableRow2.Name = "xrTableRow2";
            this.xrTableRow2.Weight = 1D;
            // 
            // _DepMXNcc
            // 
            this._DepMXNcc.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this._DepMXNcc.Dpi = 100F;
            this._DepMXNcc.Font = new System.Drawing.Font("Arial", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._DepMXNcc.Name = "_DepMXNcc";
            this._DepMXNcc.StylePriority.UseBorders = false;
            this._DepMXNcc.StylePriority.UseFont = false;
            this._DepMXNcc.StylePriority.UseTextAlignment = false;
            this._DepMXNcc.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this._DepMXNcc.Weight = 0.49348679905340437D;
            // 
            // _DepTipoCc
            // 
            this._DepTipoCc.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this._DepTipoCc.Dpi = 100F;
            this._DepTipoCc.Font = new System.Drawing.Font("Arial", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._DepTipoCc.Name = "_DepTipoCc";
            this._DepTipoCc.StylePriority.UseBorders = false;
            this._DepTipoCc.StylePriority.UseFont = false;
            this._DepTipoCc.StylePriority.UseTextAlignment = false;
            this._DepTipoCc.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this._DepTipoCc.Weight = 0.51854255429613239D;
            // 
            // xrTable1
            // 
            this.xrTable1.BackColor = System.Drawing.Color.White;
            this.xrTable1.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable1.Dpi = 100F;
            this.xrTable1.Font = new System.Drawing.Font("Arial", 6F);
            this.xrTable1.LocationFloat = new DevExpress.Utils.PointFloat(3.720917F, 0F);
            this.xrTable1.Name = "xrTable1";
            this.xrTable1.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow1});
            this.xrTable1.SizeF = new System.Drawing.SizeF(189.8483F, 20F);
            this.xrTable1.StylePriority.UseBackColor = false;
            this.xrTable1.StylePriority.UseBorders = false;
            this.xrTable1.StylePriority.UseFont = false;
            // 
            // xrTableRow1
            // 
            this.xrTableRow1.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this._DepMXNcajas,
            this._DepMXNCajaTipo});
            this.xrTableRow1.Dpi = 100F;
            this.xrTableRow1.Name = "xrTableRow1";
            this.xrTableRow1.Weight = 1D;
            // 
            // _DepMXNcajas
            // 
            this._DepMXNcajas.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this._DepMXNcajas.Dpi = 100F;
            this._DepMXNcajas.Font = new System.Drawing.Font("Arial", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._DepMXNcajas.Name = "_DepMXNcajas";
            this._DepMXNcajas.StylePriority.UseBorders = false;
            this._DepMXNcajas.StylePriority.UseFont = false;
            this._DepMXNcajas.StylePriority.UseTextAlignment = false;
            this._DepMXNcajas.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this._DepMXNcajas.Weight = 0.52791680648936579D;
            // 
            // _DepMXNCajaTipo
            // 
            this._DepMXNCajaTipo.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this._DepMXNCajaTipo.Dpi = 100F;
            this._DepMXNCajaTipo.Font = new System.Drawing.Font("Arial", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._DepMXNCajaTipo.Name = "_DepMXNCajaTipo";
            this._DepMXNCajaTipo.StylePriority.UseBorders = false;
            this._DepMXNCajaTipo.StylePriority.UseFont = false;
            this._DepMXNCajaTipo.StylePriority.UseTextAlignment = false;
            this._DepMXNCajaTipo.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this._DepMXNCajaTipo.Weight = 0.51305819052772828D;
            // 
            // ReportHeader1
            // 
            this.ReportHeader1.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel24,
            this.xrLabel111,
            this.xrLabel110,
            this.xrLabel109,
            this.xrLabel25,
            this.xrLabel107,
            this.xrLabel108,
            this.xrLabel2,
            this.xrLabel6,
            this.xrLabel1,
            this.Detalle,
            this.xrLabel17,
            this.xrLabel18,
            this.xrLabel19,
            this.xrLabel112});
            this.ReportHeader1.Dpi = 100F;
            this.ReportHeader1.HeightF = 60.08165F;
            this.ReportHeader1.Name = "ReportHeader1";
            // 
            // xrLabel111
            // 
            this.xrLabel111.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.xrLabel111.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel111.Dpi = 100F;
            this.xrLabel111.Font = new System.Drawing.Font("Arial", 7F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel111.LocationFloat = new DevExpress.Utils.PointFloat(736.5822F, 28.00005F);
            this.xrLabel111.Name = "xrLabel111";
            this.xrLabel111.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel111.SizeF = new System.Drawing.SizeF(285.4178F, 14.47391F);
            this.xrLabel111.StylePriority.UseBackColor = false;
            this.xrLabel111.StylePriority.UseBorders = false;
            this.xrLabel111.StylePriority.UseFont = false;
            this.xrLabel111.StylePriority.UseTextAlignment = false;
            this.xrLabel111.Text = "DOALRES  EN CREDITO Y COBRANZA";
            this.xrLabel111.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrLabel110
            // 
            this.xrLabel110.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.xrLabel110.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel110.Dpi = 100F;
            this.xrLabel110.Font = new System.Drawing.Font("Arial", 7F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel110.LocationFloat = new DevExpress.Utils.PointFloat(427.6086F, 28.00005F);
            this.xrLabel110.Name = "xrLabel110";
            this.xrLabel110.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel110.SizeF = new System.Drawing.SizeF(285.4178F, 14.47391F);
            this.xrLabel110.StylePriority.UseBackColor = false;
            this.xrLabel110.StylePriority.UseBorders = false;
            this.xrLabel110.StylePriority.UseFont = false;
            this.xrLabel110.StylePriority.UseTextAlignment = false;
            this.xrLabel110.Text = "DOLARES AREA DE CAJAS";
            this.xrLabel110.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrLabel109
            // 
            this.xrLabel109.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.xrLabel109.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel109.Dpi = 100F;
            this.xrLabel109.Font = new System.Drawing.Font("Arial", 7F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel109.LocationFloat = new DevExpress.Utils.PointFloat(219.7342F, 28.00001F);
            this.xrLabel109.Name = "xrLabel109";
            this.xrLabel109.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel109.SizeF = new System.Drawing.SizeF(184.5693F, 14.47391F);
            this.xrLabel109.StylePriority.UseBackColor = false;
            this.xrLabel109.StylePriority.UseBorders = false;
            this.xrLabel109.StylePriority.UseFont = false;
            this.xrLabel109.StylePriority.UseTextAlignment = false;
            this.xrLabel109.Text = "PESOS CREDITO Y COBRANZA";
            this.xrLabel109.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrLabel25
            // 
            this.xrLabel25.BackColor = System.Drawing.Color.Silver;
            this.xrLabel25.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel25.Dpi = 100F;
            this.xrLabel25.Font = new System.Drawing.Font("Arial", 7F, System.Drawing.FontStyle.Bold);
            this.xrLabel25.LocationFloat = new DevExpress.Utils.PointFloat(736.582F, 42.47393F);
            this.xrLabel25.Name = "xrLabel25";
            this.xrLabel25.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel25.SizeF = new System.Drawing.SizeF(96.27917F, 17.55346F);
            this.xrLabel25.StylePriority.UseBackColor = false;
            this.xrLabel25.StylePriority.UseBorders = false;
            this.xrLabel25.StylePriority.UseFont = false;
            this.xrLabel25.StylePriority.UseTextAlignment = false;
            this.xrLabel25.Text = "IMPORTE";
            this.xrLabel25.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrLabel107
            // 
            this.xrLabel107.BackColor = System.Drawing.Color.Silver;
            this.xrLabel107.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel107.Dpi = 100F;
            this.xrLabel107.Font = new System.Drawing.Font("Arial", 7F, System.Drawing.FontStyle.Bold);
            this.xrLabel107.LocationFloat = new DevExpress.Utils.PointFloat(927.4305F, 42.47393F);
            this.xrLabel107.Name = "xrLabel107";
            this.xrLabel107.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel107.SizeF = new System.Drawing.SizeF(94.5694F, 17.55346F);
            this.xrLabel107.StylePriority.UseBackColor = false;
            this.xrLabel107.StylePriority.UseBorders = false;
            this.xrLabel107.StylePriority.UseFont = false;
            this.xrLabel107.StylePriority.UseTextAlignment = false;
            this.xrLabel107.Text = "EN M.N.";
            this.xrLabel107.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrLabel108
            // 
            this.xrLabel108.BackColor = System.Drawing.Color.Silver;
            this.xrLabel108.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel108.Dpi = 100F;
            this.xrLabel108.Font = new System.Drawing.Font("Arial", 7F, System.Drawing.FontStyle.Bold);
            this.xrLabel108.LocationFloat = new DevExpress.Utils.PointFloat(832.8613F, 42.47393F);
            this.xrLabel108.Name = "xrLabel108";
            this.xrLabel108.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel108.SizeF = new System.Drawing.SizeF(94.56927F, 17.55346F);
            this.xrLabel108.StylePriority.UseBackColor = false;
            this.xrLabel108.StylePriority.UseBorders = false;
            this.xrLabel108.StylePriority.UseFont = false;
            this.xrLabel108.StylePriority.UseTextAlignment = false;
            this.xrLabel108.Text = "TIPO DEPOSITO";
            this.xrLabel108.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrLabel112
            // 
            this.xrLabel112.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.xrLabel112.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel112.Dpi = 100F;
            this.xrLabel112.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel112.LocationFloat = new DevExpress.Utils.PointFloat(9.000015F, 13.52609F);
            this.xrLabel112.Name = "xrLabel112";
            this.xrLabel112.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel112.SizeF = new System.Drawing.SizeF(1013F, 14.47391F);
            this.xrLabel112.StylePriority.UseBackColor = false;
            this.xrLabel112.StylePriority.UseBorders = false;
            this.xrLabel112.StylePriority.UseFont = false;
            this.xrLabel112.StylePriority.UseTextAlignment = false;
            this.xrLabel112.Text = "DETALLE DE DEPOSITOS";
            this.xrLabel112.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // ReportFooter
            // 
            this.ReportFooter.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this._RENTAdlls,
            this._RENTAiva,
            this._RENTAtotalDlls,
            this._RENTAtotalDllsMxn,
            this.xrLabel146,
            this.xrLabel147,
            this.xrLabel148,
            this.xrLabel149,
            this.xrLabel150,
            this.xrLabel127,
            this.xrLabel130,
            this.xrLabel131,
            this.xrLabel132,
            this.xrLabel133,
            this._COMStotal,
            this._COMSdllsMxn,
            this._COMSdlls,
            this._COMSmxn,
            this._PAYSmxn,
            this._PAYSdlls,
            this._PAYSdllsMxn,
            this._PAYStotal,
            this.xrLabel116,
            this.xrLabel117,
            this.xrLabel120,
            this.xrLabel121,
            this.xrLabel126,
            this.xrLabel5,
            this.xrLabel118,
            this._TDvalesElec,
            this.xrLabel122,
            this._TDtotal,
            this._TDvales,
            this.xrLabel125,
            this._TDcards,
            this.xrLabel129,
            this.xrLabel134,
            this._TDdllsmxn,
            this._TDmxn,
            this.xrLabel137});
            this.ReportFooter.Dpi = 100F;
            this.ReportFooter.HeightF = 202.3445F;
            this.ReportFooter.Name = "ReportFooter";
            // 
            // _RENTAdlls
            // 
            this._RENTAdlls.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this._RENTAdlls.Dpi = 100F;
            this._RENTAdlls.Font = new System.Drawing.Font("Times New Roman", 8F, System.Drawing.FontStyle.Bold);
            this._RENTAdlls.LocationFloat = new DevExpress.Utils.PointFloat(689.8865F, 55.99995F);
            this._RENTAdlls.Name = "_RENTAdlls";
            this._RENTAdlls.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this._RENTAdlls.SizeF = new System.Drawing.SizeF(74.33333F, 23F);
            this._RENTAdlls.StylePriority.UseBorders = false;
            this._RENTAdlls.StylePriority.UseFont = false;
            this._RENTAdlls.StylePriority.UseTextAlignment = false;
            this._RENTAdlls.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // _RENTAiva
            // 
            this._RENTAiva.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this._RENTAiva.Dpi = 100F;
            this._RENTAiva.Font = new System.Drawing.Font("Times New Roman", 8F);
            this._RENTAiva.LocationFloat = new DevExpress.Utils.PointFloat(764.2197F, 55.99995F);
            this._RENTAiva.Name = "_RENTAiva";
            this._RENTAiva.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this._RENTAiva.SizeF = new System.Drawing.SizeF(79.58279F, 23F);
            this._RENTAiva.StylePriority.UseBorders = false;
            this._RENTAiva.StylePriority.UseFont = false;
            this._RENTAiva.StylePriority.UseTextAlignment = false;
            this._RENTAiva.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // _RENTAtotalDlls
            // 
            this._RENTAtotalDlls.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this._RENTAtotalDlls.Dpi = 100F;
            this._RENTAtotalDlls.Font = new System.Drawing.Font("Times New Roman", 8F);
            this._RENTAtotalDlls.LocationFloat = new DevExpress.Utils.PointFloat(843.8024F, 55.99995F);
            this._RENTAtotalDlls.Name = "_RENTAtotalDlls";
            this._RENTAtotalDlls.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this._RENTAtotalDlls.SizeF = new System.Drawing.SizeF(67.94434F, 23F);
            this._RENTAtotalDlls.StylePriority.UseBorders = false;
            this._RENTAtotalDlls.StylePriority.UseFont = false;
            this._RENTAtotalDlls.StylePriority.UseTextAlignment = false;
            this._RENTAtotalDlls.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // _RENTAtotalDllsMxn
            // 
            this._RENTAtotalDllsMxn.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this._RENTAtotalDllsMxn.Dpi = 100F;
            this._RENTAtotalDllsMxn.Font = new System.Drawing.Font("Times New Roman", 8F);
            this._RENTAtotalDllsMxn.LocationFloat = new DevExpress.Utils.PointFloat(911.7467F, 55.99982F);
            this._RENTAtotalDllsMxn.Name = "_RENTAtotalDllsMxn";
            this._RENTAtotalDllsMxn.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this._RENTAtotalDllsMxn.SizeF = new System.Drawing.SizeF(84.66467F, 23F);
            this._RENTAtotalDllsMxn.StylePriority.UseBorders = false;
            this._RENTAtotalDllsMxn.StylePriority.UseFont = false;
            this._RENTAtotalDllsMxn.StylePriority.UseTextAlignment = false;
            this._RENTAtotalDllsMxn.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrLabel146
            // 
            this.xrLabel146.BackColor = System.Drawing.Color.Silver;
            this.xrLabel146.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel146.Dpi = 100F;
            this.xrLabel146.Font = new System.Drawing.Font("Times New Roman", 8F, System.Drawing.FontStyle.Bold);
            this.xrLabel146.LocationFloat = new DevExpress.Utils.PointFloat(911.7467F, 32.99987F);
            this.xrLabel146.Name = "xrLabel146";
            this.xrLabel146.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel146.SizeF = new System.Drawing.SizeF(84.66467F, 23F);
            this.xrLabel146.StylePriority.UseBackColor = false;
            this.xrLabel146.StylePriority.UseBorders = false;
            this.xrLabel146.StylePriority.UseFont = false;
            this.xrLabel146.StylePriority.UseTextAlignment = false;
            this.xrLabel146.Text = "TOTAL M.N";
            this.xrLabel146.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrLabel147
            // 
            this.xrLabel147.BackColor = System.Drawing.Color.Silver;
            this.xrLabel147.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel147.Dpi = 100F;
            this.xrLabel147.Font = new System.Drawing.Font("Times New Roman", 8F, System.Drawing.FontStyle.Bold);
            this.xrLabel147.LocationFloat = new DevExpress.Utils.PointFloat(843.8024F, 32.99987F);
            this.xrLabel147.Name = "xrLabel147";
            this.xrLabel147.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel147.SizeF = new System.Drawing.SizeF(67.94434F, 23F);
            this.xrLabel147.StylePriority.UseBackColor = false;
            this.xrLabel147.StylePriority.UseBorders = false;
            this.xrLabel147.StylePriority.UseFont = false;
            this.xrLabel147.StylePriority.UseTextAlignment = false;
            this.xrLabel147.Text = "TOTAL";
            this.xrLabel147.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrLabel148
            // 
            this.xrLabel148.BackColor = System.Drawing.Color.Silver;
            this.xrLabel148.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel148.Dpi = 100F;
            this.xrLabel148.Font = new System.Drawing.Font("Times New Roman", 8F, System.Drawing.FontStyle.Bold);
            this.xrLabel148.LocationFloat = new DevExpress.Utils.PointFloat(764.2197F, 32.99987F);
            this.xrLabel148.Name = "xrLabel148";
            this.xrLabel148.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel148.SizeF = new System.Drawing.SizeF(79.58279F, 23F);
            this.xrLabel148.StylePriority.UseBackColor = false;
            this.xrLabel148.StylePriority.UseBorders = false;
            this.xrLabel148.StylePriority.UseFont = false;
            this.xrLabel148.StylePriority.UseTextAlignment = false;
            this.xrLabel148.Text = "IVA";
            this.xrLabel148.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrLabel149
            // 
            this.xrLabel149.BackColor = System.Drawing.Color.Silver;
            this.xrLabel149.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel149.Dpi = 100F;
            this.xrLabel149.Font = new System.Drawing.Font("Times New Roman", 8F, System.Drawing.FontStyle.Bold);
            this.xrLabel149.LocationFloat = new DevExpress.Utils.PointFloat(689.8865F, 32.99987F);
            this.xrLabel149.Name = "xrLabel149";
            this.xrLabel149.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel149.SizeF = new System.Drawing.SizeF(74.33333F, 23F);
            this.xrLabel149.StylePriority.UseBackColor = false;
            this.xrLabel149.StylePriority.UseBorders = false;
            this.xrLabel149.StylePriority.UseFont = false;
            this.xrLabel149.StylePriority.UseTextAlignment = false;
            this.xrLabel149.Text = "DLLS";
            this.xrLabel149.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrLabel150
            // 
            this.xrLabel150.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.xrLabel150.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel150.Dpi = 100F;
            this.xrLabel150.Font = new System.Drawing.Font("Times New Roman", 8.5F, System.Drawing.FontStyle.Bold);
            this.xrLabel150.LocationFloat = new DevExpress.Utils.PointFloat(689.8865F, 9.999911F);
            this.xrLabel150.Name = "xrLabel150";
            this.xrLabel150.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel150.SizeF = new System.Drawing.SizeF(306.5249F, 23F);
            this.xrLabel150.StylePriority.UseBackColor = false;
            this.xrLabel150.StylePriority.UseBorders = false;
            this.xrLabel150.StylePriority.UseFont = false;
            this.xrLabel150.StylePriority.UseTextAlignment = false;
            this.xrLabel150.Text = "RENTA";
            this.xrLabel150.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrLabel127
            // 
            this.xrLabel127.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.xrLabel127.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel127.Dpi = 100F;
            this.xrLabel127.Font = new System.Drawing.Font("Times New Roman", 8.5F, System.Drawing.FontStyle.Bold);
            this.xrLabel127.LocationFloat = new DevExpress.Utils.PointFloat(357.2752F, 102F);
            this.xrLabel127.Name = "xrLabel127";
            this.xrLabel127.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel127.SizeF = new System.Drawing.SizeF(306.5249F, 23F);
            this.xrLabel127.StylePriority.UseBackColor = false;
            this.xrLabel127.StylePriority.UseBorders = false;
            this.xrLabel127.StylePriority.UseFont = false;
            this.xrLabel127.StylePriority.UseTextAlignment = false;
            this.xrLabel127.Text = "COMISIONES";
            this.xrLabel127.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrLabel130
            // 
            this.xrLabel130.BackColor = System.Drawing.Color.Silver;
            this.xrLabel130.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel130.Dpi = 100F;
            this.xrLabel130.Font = new System.Drawing.Font("Times New Roman", 8F, System.Drawing.FontStyle.Bold);
            this.xrLabel130.LocationFloat = new DevExpress.Utils.PointFloat(357.2752F, 125F);
            this.xrLabel130.Name = "xrLabel130";
            this.xrLabel130.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel130.SizeF = new System.Drawing.SizeF(74.33333F, 23F);
            this.xrLabel130.StylePriority.UseBackColor = false;
            this.xrLabel130.StylePriority.UseBorders = false;
            this.xrLabel130.StylePriority.UseFont = false;
            this.xrLabel130.StylePriority.UseTextAlignment = false;
            this.xrLabel130.Text = "PESOS";
            this.xrLabel130.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrLabel131
            // 
            this.xrLabel131.BackColor = System.Drawing.Color.Silver;
            this.xrLabel131.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel131.Dpi = 100F;
            this.xrLabel131.Font = new System.Drawing.Font("Times New Roman", 8F, System.Drawing.FontStyle.Bold);
            this.xrLabel131.LocationFloat = new DevExpress.Utils.PointFloat(431.6086F, 125F);
            this.xrLabel131.Name = "xrLabel131";
            this.xrLabel131.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel131.SizeF = new System.Drawing.SizeF(79.58279F, 23F);
            this.xrLabel131.StylePriority.UseBackColor = false;
            this.xrLabel131.StylePriority.UseBorders = false;
            this.xrLabel131.StylePriority.UseFont = false;
            this.xrLabel131.StylePriority.UseTextAlignment = false;
            this.xrLabel131.Text = "DLLS";
            this.xrLabel131.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrLabel132
            // 
            this.xrLabel132.BackColor = System.Drawing.Color.Silver;
            this.xrLabel132.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel132.Dpi = 100F;
            this.xrLabel132.Font = new System.Drawing.Font("Times New Roman", 8F, System.Drawing.FontStyle.Bold);
            this.xrLabel132.LocationFloat = new DevExpress.Utils.PointFloat(511.1913F, 125F);
            this.xrLabel132.Name = "xrLabel132";
            this.xrLabel132.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel132.SizeF = new System.Drawing.SizeF(82.52769F, 23F);
            this.xrLabel132.StylePriority.UseBackColor = false;
            this.xrLabel132.StylePriority.UseBorders = false;
            this.xrLabel132.StylePriority.UseFont = false;
            this.xrLabel132.StylePriority.UseTextAlignment = false;
            this.xrLabel132.Text = "DLLS EN M.N.";
            this.xrLabel132.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrLabel133
            // 
            this.xrLabel133.BackColor = System.Drawing.Color.Silver;
            this.xrLabel133.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel133.Dpi = 100F;
            this.xrLabel133.Font = new System.Drawing.Font("Times New Roman", 8F, System.Drawing.FontStyle.Bold);
            this.xrLabel133.LocationFloat = new DevExpress.Utils.PointFloat(593.7189F, 125F);
            this.xrLabel133.Name = "xrLabel133";
            this.xrLabel133.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel133.SizeF = new System.Drawing.SizeF(70.08122F, 23F);
            this.xrLabel133.StylePriority.UseBackColor = false;
            this.xrLabel133.StylePriority.UseBorders = false;
            this.xrLabel133.StylePriority.UseFont = false;
            this.xrLabel133.StylePriority.UseTextAlignment = false;
            this.xrLabel133.Text = "TOTAL";
            this.xrLabel133.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // _COMStotal
            // 
            this._COMStotal.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this._COMStotal.Dpi = 100F;
            this._COMStotal.Font = new System.Drawing.Font("Times New Roman", 8F);
            this._COMStotal.LocationFloat = new DevExpress.Utils.PointFloat(593.7189F, 148F);
            this._COMStotal.Name = "_COMStotal";
            this._COMStotal.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this._COMStotal.SizeF = new System.Drawing.SizeF(70.08122F, 23F);
            this._COMStotal.StylePriority.UseBorders = false;
            this._COMStotal.StylePriority.UseFont = false;
            this._COMStotal.StylePriority.UseTextAlignment = false;
            this._COMStotal.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // _COMSdllsMxn
            // 
            this._COMSdllsMxn.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this._COMSdllsMxn.Dpi = 100F;
            this._COMSdllsMxn.Font = new System.Drawing.Font("Times New Roman", 8F);
            this._COMSdllsMxn.LocationFloat = new DevExpress.Utils.PointFloat(511.1913F, 148F);
            this._COMSdllsMxn.Name = "_COMSdllsMxn";
            this._COMSdllsMxn.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this._COMSdllsMxn.SizeF = new System.Drawing.SizeF(82.52769F, 23F);
            this._COMSdllsMxn.StylePriority.UseBorders = false;
            this._COMSdllsMxn.StylePriority.UseFont = false;
            this._COMSdllsMxn.StylePriority.UseTextAlignment = false;
            this._COMSdllsMxn.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // _COMSdlls
            // 
            this._COMSdlls.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this._COMSdlls.Dpi = 100F;
            this._COMSdlls.Font = new System.Drawing.Font("Times New Roman", 8F);
            this._COMSdlls.LocationFloat = new DevExpress.Utils.PointFloat(431.6086F, 148F);
            this._COMSdlls.Name = "_COMSdlls";
            this._COMSdlls.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this._COMSdlls.SizeF = new System.Drawing.SizeF(79.58279F, 23F);
            this._COMSdlls.StylePriority.UseBorders = false;
            this._COMSdlls.StylePriority.UseFont = false;
            this._COMSdlls.StylePriority.UseTextAlignment = false;
            this._COMSdlls.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // _COMSmxn
            // 
            this._COMSmxn.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this._COMSmxn.Dpi = 100F;
            this._COMSmxn.Font = new System.Drawing.Font("Times New Roman", 8F, System.Drawing.FontStyle.Bold);
            this._COMSmxn.LocationFloat = new DevExpress.Utils.PointFloat(357.2752F, 148F);
            this._COMSmxn.Name = "_COMSmxn";
            this._COMSmxn.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this._COMSmxn.SizeF = new System.Drawing.SizeF(74.33333F, 23F);
            this._COMSmxn.StylePriority.UseBorders = false;
            this._COMSmxn.StylePriority.UseFont = false;
            this._COMSmxn.StylePriority.UseTextAlignment = false;
            this._COMSmxn.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // _PAYSmxn
            // 
            this._PAYSmxn.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this._PAYSmxn.Dpi = 100F;
            this._PAYSmxn.Font = new System.Drawing.Font("Times New Roman", 8F, System.Drawing.FontStyle.Bold);
            this._PAYSmxn.LocationFloat = new DevExpress.Utils.PointFloat(357.7255F, 56.00001F);
            this._PAYSmxn.Name = "_PAYSmxn";
            this._PAYSmxn.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this._PAYSmxn.SizeF = new System.Drawing.SizeF(74.33333F, 23F);
            this._PAYSmxn.StylePriority.UseBorders = false;
            this._PAYSmxn.StylePriority.UseFont = false;
            this._PAYSmxn.StylePriority.UseTextAlignment = false;
            this._PAYSmxn.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // _PAYSdlls
            // 
            this._PAYSdlls.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this._PAYSdlls.Dpi = 100F;
            this._PAYSdlls.Font = new System.Drawing.Font("Times New Roman", 8F);
            this._PAYSdlls.LocationFloat = new DevExpress.Utils.PointFloat(432.0588F, 56.00001F);
            this._PAYSdlls.Name = "_PAYSdlls";
            this._PAYSdlls.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this._PAYSdlls.SizeF = new System.Drawing.SizeF(79.58279F, 23F);
            this._PAYSdlls.StylePriority.UseBorders = false;
            this._PAYSdlls.StylePriority.UseFont = false;
            this._PAYSdlls.StylePriority.UseTextAlignment = false;
            this._PAYSdlls.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // _PAYSdllsMxn
            // 
            this._PAYSdllsMxn.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this._PAYSdllsMxn.Dpi = 100F;
            this._PAYSdllsMxn.Font = new System.Drawing.Font("Times New Roman", 8F);
            this._PAYSdllsMxn.LocationFloat = new DevExpress.Utils.PointFloat(511.6415F, 56.00001F);
            this._PAYSdllsMxn.Name = "_PAYSdllsMxn";
            this._PAYSdllsMxn.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this._PAYSdllsMxn.SizeF = new System.Drawing.SizeF(82.52769F, 23F);
            this._PAYSdllsMxn.StylePriority.UseBorders = false;
            this._PAYSdllsMxn.StylePriority.UseFont = false;
            this._PAYSdllsMxn.StylePriority.UseTextAlignment = false;
            this._PAYSdllsMxn.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // _PAYStotal
            // 
            this._PAYStotal.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this._PAYStotal.Dpi = 100F;
            this._PAYStotal.Font = new System.Drawing.Font("Times New Roman", 8F);
            this._PAYStotal.LocationFloat = new DevExpress.Utils.PointFloat(594.1691F, 55.99995F);
            this._PAYStotal.Name = "_PAYStotal";
            this._PAYStotal.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this._PAYStotal.SizeF = new System.Drawing.SizeF(70.08122F, 23F);
            this._PAYStotal.StylePriority.UseBorders = false;
            this._PAYStotal.StylePriority.UseFont = false;
            this._PAYStotal.StylePriority.UseTextAlignment = false;
            this._PAYStotal.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrLabel116
            // 
            this.xrLabel116.BackColor = System.Drawing.Color.Silver;
            this.xrLabel116.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel116.Dpi = 100F;
            this.xrLabel116.Font = new System.Drawing.Font("Times New Roman", 8F, System.Drawing.FontStyle.Bold);
            this.xrLabel116.LocationFloat = new DevExpress.Utils.PointFloat(594.1691F, 32.99996F);
            this.xrLabel116.Name = "xrLabel116";
            this.xrLabel116.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel116.SizeF = new System.Drawing.SizeF(70.08122F, 23F);
            this.xrLabel116.StylePriority.UseBackColor = false;
            this.xrLabel116.StylePriority.UseBorders = false;
            this.xrLabel116.StylePriority.UseFont = false;
            this.xrLabel116.StylePriority.UseTextAlignment = false;
            this.xrLabel116.Text = "TOTAL";
            this.xrLabel116.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrLabel117
            // 
            this.xrLabel117.BackColor = System.Drawing.Color.Silver;
            this.xrLabel117.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel117.Dpi = 100F;
            this.xrLabel117.Font = new System.Drawing.Font("Times New Roman", 8F, System.Drawing.FontStyle.Bold);
            this.xrLabel117.LocationFloat = new DevExpress.Utils.PointFloat(511.6415F, 32.99999F);
            this.xrLabel117.Name = "xrLabel117";
            this.xrLabel117.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel117.SizeF = new System.Drawing.SizeF(82.52769F, 23F);
            this.xrLabel117.StylePriority.UseBackColor = false;
            this.xrLabel117.StylePriority.UseBorders = false;
            this.xrLabel117.StylePriority.UseFont = false;
            this.xrLabel117.StylePriority.UseTextAlignment = false;
            this.xrLabel117.Text = "DLLS EN M.N.";
            this.xrLabel117.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrLabel120
            // 
            this.xrLabel120.BackColor = System.Drawing.Color.Silver;
            this.xrLabel120.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel120.Dpi = 100F;
            this.xrLabel120.Font = new System.Drawing.Font("Times New Roman", 8F, System.Drawing.FontStyle.Bold);
            this.xrLabel120.LocationFloat = new DevExpress.Utils.PointFloat(432.0588F, 33.00002F);
            this.xrLabel120.Name = "xrLabel120";
            this.xrLabel120.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel120.SizeF = new System.Drawing.SizeF(79.58279F, 23F);
            this.xrLabel120.StylePriority.UseBackColor = false;
            this.xrLabel120.StylePriority.UseBorders = false;
            this.xrLabel120.StylePriority.UseFont = false;
            this.xrLabel120.StylePriority.UseTextAlignment = false;
            this.xrLabel120.Text = "DLLS";
            this.xrLabel120.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrLabel121
            // 
            this.xrLabel121.BackColor = System.Drawing.Color.Silver;
            this.xrLabel121.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel121.Dpi = 100F;
            this.xrLabel121.Font = new System.Drawing.Font("Times New Roman", 8F, System.Drawing.FontStyle.Bold);
            this.xrLabel121.LocationFloat = new DevExpress.Utils.PointFloat(357.7255F, 32.99999F);
            this.xrLabel121.Name = "xrLabel121";
            this.xrLabel121.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel121.SizeF = new System.Drawing.SizeF(74.33333F, 23F);
            this.xrLabel121.StylePriority.UseBackColor = false;
            this.xrLabel121.StylePriority.UseBorders = false;
            this.xrLabel121.StylePriority.UseFont = false;
            this.xrLabel121.StylePriority.UseTextAlignment = false;
            this.xrLabel121.Text = "PESOS";
            this.xrLabel121.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrLabel126
            // 
            this.xrLabel126.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.xrLabel126.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel126.Dpi = 100F;
            this.xrLabel126.Font = new System.Drawing.Font("Times New Roman", 8.5F, System.Drawing.FontStyle.Bold);
            this.xrLabel126.LocationFloat = new DevExpress.Utils.PointFloat(357.7255F, 9.999974F);
            this.xrLabel126.Name = "xrLabel126";
            this.xrLabel126.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel126.SizeF = new System.Drawing.SizeF(306.5249F, 23F);
            this.xrLabel126.StylePriority.UseBackColor = false;
            this.xrLabel126.StylePriority.UseBorders = false;
            this.xrLabel126.StylePriority.UseFont = false;
            this.xrLabel126.StylePriority.UseTextAlignment = false;
            this.xrLabel126.Text = "PAGOS Y ABONOS DE CLIENTES";
            this.xrLabel126.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrLabel5
            // 
            this.xrLabel5.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.xrLabel5.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel5.Dpi = 100F;
            this.xrLabel5.Font = new System.Drawing.Font("Times New Roman", 8.5F, System.Drawing.FontStyle.Bold);
            this.xrLabel5.LocationFloat = new DevExpress.Utils.PointFloat(10.44256F, 9.999997F);
            this.xrLabel5.Name = "xrLabel5";
            this.xrLabel5.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel5.SizeF = new System.Drawing.SizeF(264.3315F, 23F);
            this.xrLabel5.StylePriority.UseBackColor = false;
            this.xrLabel5.StylePriority.UseBorders = false;
            this.xrLabel5.StylePriority.UseFont = false;
            this.xrLabel5.StylePriority.UseTextAlignment = false;
            this.xrLabel5.Text = "TOTAL DE DEPOSITOS";
            this.xrLabel5.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrLabel118
            // 
            this.xrLabel118.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel118.Dpi = 100F;
            this.xrLabel118.Font = new System.Drawing.Font("Times New Roman", 8F, System.Drawing.FontStyle.Bold);
            this.xrLabel118.LocationFloat = new DevExpress.Utils.PointFloat(9.999997F, 125.0002F);
            this.xrLabel118.Name = "xrLabel118";
            this.xrLabel118.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel118.SizeF = new System.Drawing.SizeF(163.7324F, 23.00001F);
            this.xrLabel118.StylePriority.UseBorders = false;
            this.xrLabel118.StylePriority.UseFont = false;
            this.xrLabel118.StylePriority.UseTextAlignment = false;
            this.xrLabel118.Text = "VALES ELECTRONICOS";
            this.xrLabel118.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // _TDvalesElec
            // 
            this._TDvalesElec.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this._TDvalesElec.Dpi = 100F;
            this._TDvalesElec.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._TDvalesElec.LocationFloat = new DevExpress.Utils.PointFloat(173.7324F, 125.0003F);
            this._TDvalesElec.Name = "_TDvalesElec";
            this._TDvalesElec.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this._TDvalesElec.SizeF = new System.Drawing.SizeF(101.0416F, 23F);
            this._TDvalesElec.StylePriority.UseBorders = false;
            this._TDvalesElec.StylePriority.UseFont = false;
            this._TDvalesElec.StylePriority.UseTextAlignment = false;
            this._TDvalesElec.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // xrLabel122
            // 
            this.xrLabel122.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel122.Dpi = 100F;
            this.xrLabel122.Font = new System.Drawing.Font("Times New Roman", 8F, System.Drawing.FontStyle.Bold);
            this.xrLabel122.LocationFloat = new DevExpress.Utils.PointFloat(9.999997F, 148.0003F);
            this.xrLabel122.Name = "xrLabel122";
            this.xrLabel122.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel122.SizeF = new System.Drawing.SizeF(163.7324F, 23.00003F);
            this.xrLabel122.StylePriority.UseBorders = false;
            this.xrLabel122.StylePriority.UseFont = false;
            this.xrLabel122.StylePriority.UseTextAlignment = false;
            this.xrLabel122.Text = "GRAN TOTAL";
            this.xrLabel122.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // _TDtotal
            // 
            this._TDtotal.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this._TDtotal.Dpi = 100F;
            this._TDtotal.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold);
            this._TDtotal.LocationFloat = new DevExpress.Utils.PointFloat(173.7324F, 148.0001F);
            this._TDtotal.Name = "_TDtotal";
            this._TDtotal.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this._TDtotal.SizeF = new System.Drawing.SizeF(101.0416F, 23.00003F);
            this._TDtotal.StylePriority.UseBorders = false;
            this._TDtotal.StylePriority.UseFont = false;
            this._TDtotal.StylePriority.UseTextAlignment = false;
            this._TDtotal.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // _TDvales
            // 
            this._TDvales.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this._TDvales.Dpi = 100F;
            this._TDvales.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._TDvales.LocationFloat = new DevExpress.Utils.PointFloat(173.7324F, 102.0001F);
            this._TDvales.Name = "_TDvales";
            this._TDvales.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this._TDvales.SizeF = new System.Drawing.SizeF(101.0416F, 22.99999F);
            this._TDvales.StylePriority.UseBorders = false;
            this._TDvales.StylePriority.UseFont = false;
            this._TDvales.StylePriority.UseTextAlignment = false;
            this._TDvales.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // xrLabel125
            // 
            this.xrLabel125.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel125.Dpi = 100F;
            this.xrLabel125.Font = new System.Drawing.Font("Times New Roman", 8F, System.Drawing.FontStyle.Bold);
            this.xrLabel125.LocationFloat = new DevExpress.Utils.PointFloat(10.00011F, 102F);
            this.xrLabel125.Name = "xrLabel125";
            this.xrLabel125.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel125.SizeF = new System.Drawing.SizeF(163.7323F, 22.99999F);
            this.xrLabel125.StylePriority.UseBorders = false;
            this.xrLabel125.StylePriority.UseFont = false;
            this.xrLabel125.StylePriority.UseTextAlignment = false;
            this.xrLabel125.Text = "VALES:";
            this.xrLabel125.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // _TDcards
            // 
            this._TDcards.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this._TDcards.Dpi = 100F;
            this._TDcards.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._TDcards.LocationFloat = new DevExpress.Utils.PointFloat(173.7324F, 79.00006F);
            this._TDcards.Name = "_TDcards";
            this._TDcards.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this._TDcards.SizeF = new System.Drawing.SizeF(101.0416F, 23F);
            this._TDcards.StylePriority.UseBorders = false;
            this._TDcards.StylePriority.UseFont = false;
            this._TDcards.StylePriority.UseTextAlignment = false;
            this._TDcards.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // xrLabel129
            // 
            this.xrLabel129.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel129.Dpi = 100F;
            this.xrLabel129.Font = new System.Drawing.Font("Times New Roman", 8F, System.Drawing.FontStyle.Bold);
            this.xrLabel129.LocationFloat = new DevExpress.Utils.PointFloat(10.00011F, 79.00006F);
            this.xrLabel129.Name = "xrLabel129";
            this.xrLabel129.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel129.SizeF = new System.Drawing.SizeF(163.7323F, 23F);
            this.xrLabel129.StylePriority.UseBorders = false;
            this.xrLabel129.StylePriority.UseFont = false;
            this.xrLabel129.StylePriority.UseTextAlignment = false;
            this.xrLabel129.Text = "T/CREDITO/DEBITO";
            this.xrLabel129.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLabel134
            // 
            this.xrLabel134.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel134.Dpi = 100F;
            this.xrLabel134.Font = new System.Drawing.Font("Times New Roman", 8F, System.Drawing.FontStyle.Bold);
            this.xrLabel134.LocationFloat = new DevExpress.Utils.PointFloat(9.999997F, 55.99998F);
            this.xrLabel134.Name = "xrLabel134";
            this.xrLabel134.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel134.SizeF = new System.Drawing.SizeF(163.7324F, 23F);
            this.xrLabel134.StylePriority.UseBorders = false;
            this.xrLabel134.StylePriority.UseFont = false;
            this.xrLabel134.StylePriority.UseTextAlignment = false;
            this.xrLabel134.Text = "DLLS EN M.N.";
            this.xrLabel134.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // _TDdllsmxn
            // 
            this._TDdllsmxn.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this._TDdllsmxn.Dpi = 100F;
            this._TDdllsmxn.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._TDdllsmxn.LocationFloat = new DevExpress.Utils.PointFloat(173.7324F, 55.99998F);
            this._TDdllsmxn.Name = "_TDdllsmxn";
            this._TDdllsmxn.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this._TDdllsmxn.SizeF = new System.Drawing.SizeF(101.0416F, 23F);
            this._TDdllsmxn.StylePriority.UseBorders = false;
            this._TDdllsmxn.StylePriority.UseFont = false;
            this._TDdllsmxn.StylePriority.UseTextAlignment = false;
            this._TDdllsmxn.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // _TDmxn
            // 
            this._TDmxn.BackColor = System.Drawing.Color.Transparent;
            this._TDmxn.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this._TDmxn.Dpi = 100F;
            this._TDmxn.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._TDmxn.LocationFloat = new DevExpress.Utils.PointFloat(173.7324F, 33.00002F);
            this._TDmxn.Name = "_TDmxn";
            this._TDmxn.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this._TDmxn.SizeF = new System.Drawing.SizeF(101.0416F, 23F);
            this._TDmxn.StylePriority.UseBackColor = false;
            this._TDmxn.StylePriority.UseBorders = false;
            this._TDmxn.StylePriority.UseFont = false;
            this._TDmxn.StylePriority.UseTextAlignment = false;
            this._TDmxn.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // xrLabel137
            // 
            this.xrLabel137.BackColor = System.Drawing.Color.Transparent;
            this.xrLabel137.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel137.Dpi = 100F;
            this.xrLabel137.Font = new System.Drawing.Font("Times New Roman", 8F, System.Drawing.FontStyle.Bold);
            this.xrLabel137.LocationFloat = new DevExpress.Utils.PointFloat(9.999997F, 33.00002F);
            this.xrLabel137.Name = "xrLabel137";
            this.xrLabel137.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel137.SizeF = new System.Drawing.SizeF(163.7324F, 23F);
            this.xrLabel137.StylePriority.UseBackColor = false;
            this.xrLabel137.StylePriority.UseBorders = false;
            this.xrLabel137.StylePriority.UseFont = false;
            this.xrLabel137.StylePriority.UseTextAlignment = false;
            this.xrLabel137.Text = "MONEDA NACIONAL";
            this.xrLabel137.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // DetailReport1
            // 
            this.DetailReport1.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.Detail2,
            this.ReportHeader2});
            this.DetailReport1.Dpi = 100F;
            this.DetailReport1.Level = 1;
            this.DetailReport1.Name = "DetailReport1";
            // 
            // Detail2
            // 
            this.Detail2.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this._TableTipoPago,
            this._TableIEPS,
            this._TableServ,
            this._TablesDesc,
            this._TableSanctions});
            this.Detail2.Dpi = 100F;
            this.Detail2.HeightF = 20F;
            this.Detail2.Name = "Detail2";
            // 
            // _TableTipoPago
            // 
            this._TableTipoPago.BackColor = System.Drawing.Color.White;
            this._TableTipoPago.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this._TableTipoPago.Dpi = 100F;
            this._TableTipoPago.Font = new System.Drawing.Font("Arial", 6F);
            this._TableTipoPago.LocationFloat = new DevExpress.Utils.PointFloat(841.1515F, 0F);
            this._TableTipoPago.Name = "_TableTipoPago";
            this._TableTipoPago.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow9});
            this._TableTipoPago.SizeF = new System.Drawing.SizeF(190.8485F, 20F);
            this._TableTipoPago.StylePriority.UseBackColor = false;
            this._TableTipoPago.StylePriority.UseBorders = false;
            this._TableTipoPago.StylePriority.UseFont = false;
            // 
            // xrTableRow9
            // 
            this.xrTableRow9.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this._PAYMETname,
            this._PAYMETimporte});
            this.xrTableRow9.Dpi = 100F;
            this.xrTableRow9.Name = "xrTableRow9";
            this.xrTableRow9.Weight = 1D;
            // 
            // _PAYMETname
            // 
            this._PAYMETname.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this._PAYMETname.Dpi = 100F;
            this._PAYMETname.Font = new System.Drawing.Font("Arial", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._PAYMETname.Name = "_PAYMETname";
            this._PAYMETname.StylePriority.UseBorders = false;
            this._PAYMETname.StylePriority.UseFont = false;
            this._PAYMETname.StylePriority.UseTextAlignment = false;
            this._PAYMETname.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this._PAYMETname.Weight = 0.52791680648936579D;
            // 
            // _PAYMETimporte
            // 
            this._PAYMETimporte.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this._PAYMETimporte.Dpi = 100F;
            this._PAYMETimporte.Font = new System.Drawing.Font("Arial", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._PAYMETimporte.Name = "_PAYMETimporte";
            this._PAYMETimporte.StylePriority.UseBorders = false;
            this._PAYMETimporte.StylePriority.UseFont = false;
            this._PAYMETimporte.StylePriority.UseTextAlignment = false;
            this._PAYMETimporte.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this._PAYMETimporte.Weight = 0.51854221962862557D;
            // 
            // _TableIEPS
            // 
            this._TableIEPS.BackColor = System.Drawing.Color.White;
            this._TableIEPS.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this._TableIEPS.Dpi = 100F;
            this._TableIEPS.Font = new System.Drawing.Font("Arial", 6F);
            this._TableIEPS.LocationFloat = new DevExpress.Utils.PointFloat(642.0124F, 0F);
            this._TableIEPS.Name = "_TableIEPS";
            this._TableIEPS.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow8});
            this._TableIEPS.SizeF = new System.Drawing.SizeF(190.8485F, 20F);
            this._TableIEPS.StylePriority.UseBackColor = false;
            this._TableIEPS.StylePriority.UseBorders = false;
            this._TableIEPS.StylePriority.UseFont = false;
            // 
            // xrTableRow8
            // 
            this.xrTableRow8.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this._IEPSperc,
            this._IEPSimporte});
            this.xrTableRow8.Dpi = 100F;
            this.xrTableRow8.Name = "xrTableRow8";
            this.xrTableRow8.Weight = 1D;
            // 
            // _IEPSperc
            // 
            this._IEPSperc.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this._IEPSperc.Dpi = 100F;
            this._IEPSperc.Font = new System.Drawing.Font("Arial", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._IEPSperc.Name = "_IEPSperc";
            this._IEPSperc.StylePriority.UseBorders = false;
            this._IEPSperc.StylePriority.UseFont = false;
            this._IEPSperc.StylePriority.UseTextAlignment = false;
            this._IEPSperc.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this._IEPSperc.Weight = 0.52791680648936579D;
            // 
            // _IEPSimporte
            // 
            this._IEPSimporte.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this._IEPSimporte.Dpi = 100F;
            this._IEPSimporte.Font = new System.Drawing.Font("Arial", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._IEPSimporte.Name = "_IEPSimporte";
            this._IEPSimporte.StylePriority.UseBorders = false;
            this._IEPSimporte.StylePriority.UseFont = false;
            this._IEPSimporte.StylePriority.UseTextAlignment = false;
            this._IEPSimporte.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this._IEPSimporte.Weight = 0.51854221962862557D;
            // 
            // _TableServ
            // 
            this._TableServ.BackColor = System.Drawing.Color.White;
            this._TableServ.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this._TableServ.Dpi = 100F;
            this._TableServ.Font = new System.Drawing.Font("Arial", 6F);
            this._TableServ.LocationFloat = new DevExpress.Utils.PointFloat(440.4599F, 0F);
            this._TableServ.Name = "_TableServ";
            this._TableServ.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow7});
            this._TableServ.SizeF = new System.Drawing.SizeF(190.8485F, 20F);
            this._TableServ.StylePriority.UseBackColor = false;
            this._TableServ.StylePriority.UseBorders = false;
            this._TableServ.StylePriority.UseFont = false;
            // 
            // xrTableRow7
            // 
            this.xrTableRow7.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this._SERVname,
            this._SERVimporte});
            this.xrTableRow7.Dpi = 100F;
            this.xrTableRow7.Name = "xrTableRow7";
            this.xrTableRow7.Weight = 1D;
            // 
            // _SERVname
            // 
            this._SERVname.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this._SERVname.Dpi = 100F;
            this._SERVname.Font = new System.Drawing.Font("Arial", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._SERVname.Name = "_SERVname";
            this._SERVname.StylePriority.UseBorders = false;
            this._SERVname.StylePriority.UseFont = false;
            this._SERVname.StylePriority.UseTextAlignment = false;
            this._SERVname.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this._SERVname.Weight = 0.52791680648936579D;
            // 
            // _SERVimporte
            // 
            this._SERVimporte.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this._SERVimporte.Dpi = 100F;
            this._SERVimporte.Font = new System.Drawing.Font("Arial", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._SERVimporte.Name = "_SERVimporte";
            this._SERVimporte.StylePriority.UseBorders = false;
            this._SERVimporte.StylePriority.UseFont = false;
            this._SERVimporte.StylePriority.UseTextAlignment = false;
            this._SERVimporte.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this._SERVimporte.Weight = 0.51854221962862557D;
            // 
            // _TablesDesc
            // 
            this._TablesDesc.BackColor = System.Drawing.Color.White;
            this._TablesDesc.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this._TablesDesc.Dpi = 100F;
            this._TablesDesc.Font = new System.Drawing.Font("Arial", 6F);
            this._TablesDesc.LocationFloat = new DevExpress.Utils.PointFloat(240.76F, 0F);
            this._TablesDesc.Name = "_TablesDesc";
            this._TablesDesc.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow6});
            this._TablesDesc.SizeF = new System.Drawing.SizeF(190.8485F, 20F);
            this._TablesDesc.StylePriority.UseBackColor = false;
            this._TablesDesc.StylePriority.UseBorders = false;
            this._TablesDesc.StylePriority.UseFont = false;
            // 
            // xrTableRow6
            // 
            this.xrTableRow6.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this._DESCcliente,
            this._DESCimporte});
            this.xrTableRow6.Dpi = 100F;
            this.xrTableRow6.Name = "xrTableRow6";
            this.xrTableRow6.Weight = 1D;
            // 
            // _DESCcliente
            // 
            this._DESCcliente.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this._DESCcliente.Dpi = 100F;
            this._DESCcliente.Font = new System.Drawing.Font("Arial", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._DESCcliente.Name = "_DESCcliente";
            this._DESCcliente.StylePriority.UseBorders = false;
            this._DESCcliente.StylePriority.UseFont = false;
            this._DESCcliente.StylePriority.UseTextAlignment = false;
            this._DESCcliente.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this._DESCcliente.Weight = 0.52791680648936579D;
            // 
            // _DESCimporte
            // 
            this._DESCimporte.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this._DESCimporte.Dpi = 100F;
            this._DESCimporte.Font = new System.Drawing.Font("Arial", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._DESCimporte.Name = "_DESCimporte";
            this._DESCimporte.StylePriority.UseBorders = false;
            this._DESCimporte.StylePriority.UseFont = false;
            this._DESCimporte.StylePriority.UseTextAlignment = false;
            this._DESCimporte.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this._DESCimporte.Weight = 0.51854221962862557D;
            // 
            // _TableSanctions
            // 
            this._TableSanctions.BackColor = System.Drawing.Color.White;
            this._TableSanctions.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this._TableSanctions.Dpi = 100F;
            this._TableSanctions.Font = new System.Drawing.Font("Arial", 6F);
            this._TableSanctions.LocationFloat = new DevExpress.Utils.PointFloat(0F, 0F);
            this._TableSanctions.Name = "_TableSanctions";
            this._TableSanctions.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow5});
            this._TableSanctions.SizeF = new System.Drawing.SizeF(233.4064F, 20F);
            this._TableSanctions.StylePriority.UseBackColor = false;
            this._TableSanctions.StylePriority.UseBorders = false;
            this._TableSanctions.StylePriority.UseFont = false;
            // 
            // xrTableRow5
            // 
            this.xrTableRow5.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this._SANCcajero,
            this.xrTableCell1,
            this._SANCimporte});
            this.xrTableRow5.Dpi = 100F;
            this.xrTableRow5.Name = "xrTableRow5";
            this.xrTableRow5.Weight = 1D;
            // 
            // _SANCcajero
            // 
            this._SANCcajero.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this._SANCcajero.Dpi = 100F;
            this._SANCcajero.Font = new System.Drawing.Font("Arial", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._SANCcajero.Name = "_SANCcajero";
            this._SANCcajero.StylePriority.UseBorders = false;
            this._SANCcajero.StylePriority.UseFont = false;
            this._SANCcajero.StylePriority.UseTextAlignment = false;
            this._SANCcajero.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this._SANCcajero.Weight = 0.54831928186698742D;
            // 
            // _SANCimporte
            // 
            this._SANCimporte.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this._SANCimporte.Dpi = 100F;
            this._SANCimporte.Font = new System.Drawing.Font("Arial", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._SANCimporte.Name = "_SANCimporte";
            this._SANCimporte.StylePriority.UseBorders = false;
            this._SANCimporte.StylePriority.UseFont = false;
            this._SANCimporte.StylePriority.UseTextAlignment = false;
            this._SANCimporte.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this._SANCimporte.Weight = 0.32720423029732415D;
            // 
            // ReportHeader2
            // 
            this.ReportHeader2.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel29,
            this.xrLabel162,
            this.xrLabel163,
            this.xrLabel164,
            this.xrLabel159,
            this.xrLabel160,
            this.xrLabel161,
            this.xrLabel156,
            this.xrLabel157,
            this.xrLabel158,
            this.xrLabel153,
            this.xrLabel154,
            this.xrLabel155,
            this.xrLabel33,
            this.xrLabel151,
            this.xrLabel152});
            this.ReportHeader2.Dpi = 100F;
            this.ReportHeader2.HeightF = 32.02744F;
            this.ReportHeader2.Name = "ReportHeader2";
            // 
            // xrLabel162
            // 
            this.xrLabel162.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.xrLabel162.Dpi = 100F;
            this.xrLabel162.Font = new System.Drawing.Font("Arial", 7F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel162.LocationFloat = new DevExpress.Utils.PointFloat(841.1516F, 0F);
            this.xrLabel162.Name = "xrLabel162";
            this.xrLabel162.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel162.SizeF = new System.Drawing.SizeF(190.8484F, 14.47391F);
            this.xrLabel162.StylePriority.UseBackColor = false;
            this.xrLabel162.StylePriority.UseFont = false;
            this.xrLabel162.StylePriority.UseTextAlignment = false;
            this.xrLabel162.Text = "TIPOS DE PAGO";
            this.xrLabel162.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrLabel163
            // 
            this.xrLabel163.BackColor = System.Drawing.Color.Silver;
            this.xrLabel163.Dpi = 100F;
            this.xrLabel163.Font = new System.Drawing.Font("Arial", 7F, System.Drawing.FontStyle.Bold);
            this.xrLabel163.LocationFloat = new DevExpress.Utils.PointFloat(841.1516F, 14.47398F);
            this.xrLabel163.Name = "xrLabel163";
            this.xrLabel163.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel163.SizeF = new System.Drawing.SizeF(96.27908F, 17.55346F);
            this.xrLabel163.StylePriority.UseBackColor = false;
            this.xrLabel163.StylePriority.UseFont = false;
            this.xrLabel163.StylePriority.UseTextAlignment = false;
            this.xrLabel163.Text = "TIPO DE PAGO";
            this.xrLabel163.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrLabel164
            // 
            this.xrLabel164.BackColor = System.Drawing.Color.Silver;
            this.xrLabel164.Dpi = 100F;
            this.xrLabel164.Font = new System.Drawing.Font("Arial", 7F, System.Drawing.FontStyle.Bold);
            this.xrLabel164.LocationFloat = new DevExpress.Utils.PointFloat(937.4308F, 14.47398F);
            this.xrLabel164.Name = "xrLabel164";
            this.xrLabel164.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel164.SizeF = new System.Drawing.SizeF(94.56937F, 17.55346F);
            this.xrLabel164.StylePriority.UseBackColor = false;
            this.xrLabel164.StylePriority.UseFont = false;
            this.xrLabel164.StylePriority.UseTextAlignment = false;
            this.xrLabel164.Text = "IMPORTE";
            this.xrLabel164.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrLabel159
            // 
            this.xrLabel159.BackColor = System.Drawing.Color.Silver;
            this.xrLabel159.Dpi = 100F;
            this.xrLabel159.Font = new System.Drawing.Font("Arial", 7F, System.Drawing.FontStyle.Bold);
            this.xrLabel159.LocationFloat = new DevExpress.Utils.PointFloat(738.2917F, 14.47395F);
            this.xrLabel159.Name = "xrLabel159";
            this.xrLabel159.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel159.SizeF = new System.Drawing.SizeF(94.56937F, 17.55346F);
            this.xrLabel159.StylePriority.UseBackColor = false;
            this.xrLabel159.StylePriority.UseFont = false;
            this.xrLabel159.StylePriority.UseTextAlignment = false;
            this.xrLabel159.Text = "IMPORTE";
            this.xrLabel159.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrLabel160
            // 
            this.xrLabel160.BackColor = System.Drawing.Color.Silver;
            this.xrLabel160.Dpi = 100F;
            this.xrLabel160.Font = new System.Drawing.Font("Arial", 7F, System.Drawing.FontStyle.Bold);
            this.xrLabel160.LocationFloat = new DevExpress.Utils.PointFloat(642.0125F, 14.47395F);
            this.xrLabel160.Name = "xrLabel160";
            this.xrLabel160.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel160.SizeF = new System.Drawing.SizeF(96.27908F, 17.55346F);
            this.xrLabel160.StylePriority.UseBackColor = false;
            this.xrLabel160.StylePriority.UseFont = false;
            this.xrLabel160.StylePriority.UseTextAlignment = false;
            this.xrLabel160.Text = "I.E.P.S. %";
            this.xrLabel160.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrLabel161
            // 
            this.xrLabel161.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.xrLabel161.Dpi = 100F;
            this.xrLabel161.Font = new System.Drawing.Font("Arial", 7F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel161.LocationFloat = new DevExpress.Utils.PointFloat(642.0125F, 3.178914E-05F);
            this.xrLabel161.Name = "xrLabel161";
            this.xrLabel161.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel161.SizeF = new System.Drawing.SizeF(190.8484F, 14.47391F);
            this.xrLabel161.StylePriority.UseBackColor = false;
            this.xrLabel161.StylePriority.UseFont = false;
            this.xrLabel161.StylePriority.UseTextAlignment = false;
            this.xrLabel161.Text = "I.E.P.S.";
            this.xrLabel161.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrLabel156
            // 
            this.xrLabel156.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.xrLabel156.Dpi = 100F;
            this.xrLabel156.Font = new System.Drawing.Font("Arial", 7F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel156.LocationFloat = new DevExpress.Utils.PointFloat(440.4601F, 0F);
            this.xrLabel156.Name = "xrLabel156";
            this.xrLabel156.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel156.SizeF = new System.Drawing.SizeF(190.8484F, 14.47391F);
            this.xrLabel156.StylePriority.UseBackColor = false;
            this.xrLabel156.StylePriority.UseFont = false;
            this.xrLabel156.StylePriority.UseTextAlignment = false;
            this.xrLabel156.Text = "DETALLE DE SERVCIOS";
            this.xrLabel156.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrLabel157
            // 
            this.xrLabel157.BackColor = System.Drawing.Color.Silver;
            this.xrLabel157.Dpi = 100F;
            this.xrLabel157.Font = new System.Drawing.Font("Arial", 7F, System.Drawing.FontStyle.Bold);
            this.xrLabel157.LocationFloat = new DevExpress.Utils.PointFloat(440.4601F, 14.47392F);
            this.xrLabel157.Name = "xrLabel157";
            this.xrLabel157.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel157.SizeF = new System.Drawing.SizeF(96.27908F, 17.55346F);
            this.xrLabel157.StylePriority.UseBackColor = false;
            this.xrLabel157.StylePriority.UseFont = false;
            this.xrLabel157.StylePriority.UseTextAlignment = false;
            this.xrLabel157.Text = "SERVICIO";
            this.xrLabel157.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrLabel158
            // 
            this.xrLabel158.BackColor = System.Drawing.Color.Silver;
            this.xrLabel158.Dpi = 100F;
            this.xrLabel158.Font = new System.Drawing.Font("Arial", 7F, System.Drawing.FontStyle.Bold);
            this.xrLabel158.LocationFloat = new DevExpress.Utils.PointFloat(536.7392F, 14.47392F);
            this.xrLabel158.Name = "xrLabel158";
            this.xrLabel158.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel158.SizeF = new System.Drawing.SizeF(94.56937F, 17.55346F);
            this.xrLabel158.StylePriority.UseBackColor = false;
            this.xrLabel158.StylePriority.UseFont = false;
            this.xrLabel158.StylePriority.UseTextAlignment = false;
            this.xrLabel158.Text = "IMPORTE";
            this.xrLabel158.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrLabel153
            // 
            this.xrLabel153.BackColor = System.Drawing.Color.Silver;
            this.xrLabel153.Dpi = 100F;
            this.xrLabel153.Font = new System.Drawing.Font("Arial", 7F, System.Drawing.FontStyle.Bold);
            this.xrLabel153.LocationFloat = new DevExpress.Utils.PointFloat(337.4895F, 14.47392F);
            this.xrLabel153.Name = "xrLabel153";
            this.xrLabel153.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel153.SizeF = new System.Drawing.SizeF(94.56937F, 17.55346F);
            this.xrLabel153.StylePriority.UseBackColor = false;
            this.xrLabel153.StylePriority.UseFont = false;
            this.xrLabel153.StylePriority.UseTextAlignment = false;
            this.xrLabel153.Text = "IMPORTE";
            this.xrLabel153.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrLabel154
            // 
            this.xrLabel154.BackColor = System.Drawing.Color.Silver;
            this.xrLabel154.Dpi = 100F;
            this.xrLabel154.Font = new System.Drawing.Font("Arial", 7F, System.Drawing.FontStyle.Bold);
            this.xrLabel154.LocationFloat = new DevExpress.Utils.PointFloat(241.2104F, 14.47392F);
            this.xrLabel154.Name = "xrLabel154";
            this.xrLabel154.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel154.SizeF = new System.Drawing.SizeF(96.27908F, 17.55346F);
            this.xrLabel154.StylePriority.UseBackColor = false;
            this.xrLabel154.StylePriority.UseFont = false;
            this.xrLabel154.StylePriority.UseTextAlignment = false;
            this.xrLabel154.Text = "CLIENTE";
            this.xrLabel154.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrLabel155
            // 
            this.xrLabel155.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.xrLabel155.Dpi = 100F;
            this.xrLabel155.Font = new System.Drawing.Font("Arial", 7F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel155.LocationFloat = new DevExpress.Utils.PointFloat(241.2104F, 0F);
            this.xrLabel155.Name = "xrLabel155";
            this.xrLabel155.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel155.SizeF = new System.Drawing.SizeF(190.8484F, 14.47391F);
            this.xrLabel155.StylePriority.UseBackColor = false;
            this.xrLabel155.StylePriority.UseFont = false;
            this.xrLabel155.StylePriority.UseTextAlignment = false;
            this.xrLabel155.Text = "DESCUENTOS SOBRE VENTA";
            this.xrLabel155.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrLabel33
            // 
            this.xrLabel33.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.xrLabel33.Dpi = 100F;
            this.xrLabel33.Font = new System.Drawing.Font("Arial", 7F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel33.LocationFloat = new DevExpress.Utils.PointFloat(0F, 3.147125E-05F);
            this.xrLabel33.Name = "xrLabel33";
            this.xrLabel33.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel33.SizeF = new System.Drawing.SizeF(233.4064F, 14.47391F);
            this.xrLabel33.StylePriority.UseBackColor = false;
            this.xrLabel33.StylePriority.UseFont = false;
            this.xrLabel33.StylePriority.UseTextAlignment = false;
            this.xrLabel33.Text = "SANCIONES";
            this.xrLabel33.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrLabel151
            // 
            this.xrLabel151.BackColor = System.Drawing.Color.Silver;
            this.xrLabel151.Dpi = 100F;
            this.xrLabel151.Font = new System.Drawing.Font("Arial", 7F, System.Drawing.FontStyle.Bold);
            this.xrLabel151.LocationFloat = new DevExpress.Utils.PointFloat(0F, 14.47394F);
            this.xrLabel151.Name = "xrLabel151";
            this.xrLabel151.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel151.SizeF = new System.Drawing.SizeF(99.99999F, 17.55346F);
            this.xrLabel151.StylePriority.UseBackColor = false;
            this.xrLabel151.StylePriority.UseFont = false;
            this.xrLabel151.StylePriority.UseTextAlignment = false;
            this.xrLabel151.Text = "CAJERO(A)";
            this.xrLabel151.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLabel152
            // 
            this.xrLabel152.BackColor = System.Drawing.Color.Silver;
            this.xrLabel152.Dpi = 100F;
            this.xrLabel152.Font = new System.Drawing.Font("Arial", 7F, System.Drawing.FontStyle.Bold);
            this.xrLabel152.LocationFloat = new DevExpress.Utils.PointFloat(173.7324F, 14.47394F);
            this.xrLabel152.Name = "xrLabel152";
            this.xrLabel152.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel152.SizeF = new System.Drawing.SizeF(59.67404F, 17.55346F);
            this.xrLabel152.StylePriority.UseBackColor = false;
            this.xrLabel152.StylePriority.UseFont = false;
            this.xrLabel152.StylePriority.UseTextAlignment = false;
            this.xrLabel152.Text = "IMPORTE";
            this.xrLabel152.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // xrLabel29
            // 
            this.xrLabel29.BackColor = System.Drawing.Color.Silver;
            this.xrLabel29.Dpi = 100F;
            this.xrLabel29.Font = new System.Drawing.Font("Arial", 7F, System.Drawing.FontStyle.Bold);
            this.xrLabel29.LocationFloat = new DevExpress.Utils.PointFloat(99.99999F, 14.47391F);
            this.xrLabel29.Name = "xrLabel29";
            this.xrLabel29.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel29.SizeF = new System.Drawing.SizeF(73.7324F, 17.55346F);
            this.xrLabel29.StylePriority.UseBackColor = false;
            this.xrLabel29.StylePriority.UseFont = false;
            this.xrLabel29.StylePriority.UseTextAlignment = false;
            this.xrLabel29.Text = "MOTIVO";
            this.xrLabel29.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrTableCell1
            // 
            this.xrTableCell1.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrTableCell1.Dpi = 100F;
            this.xrTableCell1.Font = new System.Drawing.Font("Arial", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTableCell1.Name = "xrTableCell1";
            this.xrTableCell1.StylePriority.UseBorders = false;
            this.xrTableCell1.StylePriority.UseFont = false;
            this.xrTableCell1.StylePriority.UseTextAlignment = false;
            this.xrTableCell1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell1.Weight = 0.40428890506128756D;
            // 
            // Portada
            // 
            this.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.Detail,
            this.TopMargin,
            this.BottomMargin,
            this.ReportHeader,
            this.PageFooter,
            this.DetailReport,
            this.DetailReport1});
            this.Landscape = true;
            this.Margins = new System.Drawing.Printing.Margins(40, 28, 30, 55);
            this.PageHeight = 850;
            this.PageWidth = 1100;
            this.Version = "16.2";
            ((System.ComponentModel.ISupportInitialize)(this.xrTable4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._TableTipoPago)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._TableIEPS)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._TableServ)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._TablesDesc)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._TableSanctions)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

        }

        #endregion

        private DevExpress.XtraReports.UI.DetailBand Detail;
        private DevExpress.XtraReports.UI.TopMarginBand TopMargin;
        private DevExpress.XtraReports.UI.BottomMarginBand BottomMargin;
        private DevExpress.XtraReports.UI.ReportHeaderBand ReportHeader;
        private DevExpress.XtraReports.UI.PageFooterBand PageFooter;
        private DevExpress.XtraReports.UI.XRPageInfo xrPageInfo1;
        private DevExpress.XtraReports.UI.DetailReportBand DetailReport;
        private DevExpress.XtraReports.UI.DetailBand Detail1;
        private DevExpress.XtraReports.UI.XRTable xrTable1;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow1;
        private DevExpress.XtraReports.UI.XRTableCell _DepMXNCajaTipo;
        private DevExpress.XtraReports.UI.XRLabel Detalle;
        private DevExpress.XtraReports.UI.XRLabel labelDate;
        private DevExpress.XtraReports.UI.XRLine xrLine1;
        private DevExpress.XtraReports.UI.XRLabel _labelReporte;
        private DevExpress.XtraReports.UI.XRLabel xrLabel4;
        private DevExpress.XtraReports.UI.XRLabel siteLabel;
        private DevExpress.XtraReports.UI.XRLabel xrLabel3;
        private DevExpress.XtraReports.UI.XRPictureBox xrPictureBox1;
        private DevExpress.XtraReports.UI.XRLabel xrLabel17;
        private DevExpress.XtraReports.UI.XRLabel xrLabel18;
        private DevExpress.XtraReports.UI.XRLabel xrLabel19;
        private DevExpress.XtraReports.UI.XRLabel xrLabel24;
        private DevExpress.XtraReports.UI.XRLabel xrLabel1;
        private DevExpress.XtraReports.UI.XRLabel xrLabel6;
        private DevExpress.XtraReports.UI.XRLabel xrLabel2;
        private DevExpress.XtraReports.UI.XRTableCell _DepMXNcajas;
        private DevExpress.XtraReports.UI.XRLabel xrLabel44;
        private DevExpress.XtraReports.UI.XRLabel xrLabel45;
        private DevExpress.XtraReports.UI.XRLabel xrLabel46;
        private DevExpress.XtraReports.UI.XRLabel xrLabel47;
        private DevExpress.XtraReports.UI.XRLabel xrLabel48;
        private DevExpress.XtraReports.UI.XRLabel _USD02;
        private DevExpress.XtraReports.UI.XRLabel _USD01;
        private DevExpress.XtraReports.UI.XRLabel _USD00;
        private DevExpress.XtraReports.UI.XRLabel xrLabel52;
        private DevExpress.XtraReports.UI.XRLabel xrLabel53;
        private DevExpress.XtraReports.UI.XRLabel _USD10;
        private DevExpress.XtraReports.UI.XRLabel _USD11;
        private DevExpress.XtraReports.UI.XRLabel _USD12;
        private DevExpress.XtraReports.UI.XRLabel xrLabel57;
        private DevExpress.XtraReports.UI.XRLabel _USD20;
        private DevExpress.XtraReports.UI.XRLabel _USD21;
        private DevExpress.XtraReports.UI.XRLabel _USD22;
        private DevExpress.XtraReports.UI.XRLabel _USD42;
        private DevExpress.XtraReports.UI.XRLabel _USD41;
        private DevExpress.XtraReports.UI.XRLabel _USD40;
        private DevExpress.XtraReports.UI.XRLabel xrLabel64;
        private DevExpress.XtraReports.UI.XRLabel _USD32;
        private DevExpress.XtraReports.UI.XRLabel _USD31;
        private DevExpress.XtraReports.UI.XRLabel _USD30;
        private DevExpress.XtraReports.UI.XRLabel xrLabel68;
        private DevExpress.XtraReports.UI.XRLabel _USD52;
        private DevExpress.XtraReports.UI.XRLabel _USD51;
        private DevExpress.XtraReports.UI.XRLabel _USD50;
        private DevExpress.XtraReports.UI.XRLabel xrLabel72;
        private DevExpress.XtraReports.UI.XRLabel xrLabel40;
        private DevExpress.XtraReports.UI.XRLabel _MN50;
        private DevExpress.XtraReports.UI.XRLabel _MN51;
        private DevExpress.XtraReports.UI.XRLabel _MN52;
        private DevExpress.XtraReports.UI.XRLabel xrLabel31;
        private DevExpress.XtraReports.UI.XRLabel _MN30;
        private DevExpress.XtraReports.UI.XRLabel _MN31;
        private DevExpress.XtraReports.UI.XRLabel _MN32;
        private DevExpress.XtraReports.UI.XRLabel xrLabel36;
        private DevExpress.XtraReports.UI.XRLabel _MN40;
        private DevExpress.XtraReports.UI.XRLabel _MN41;
        private DevExpress.XtraReports.UI.XRLabel _MN42;
        private DevExpress.XtraReports.UI.XRLabel _MN22;
        private DevExpress.XtraReports.UI.XRLabel _MN21;
        private DevExpress.XtraReports.UI.XRLabel _MN20;
        private DevExpress.XtraReports.UI.XRLabel xrLabel30;
        private DevExpress.XtraReports.UI.XRLabel _MN12;
        private DevExpress.XtraReports.UI.XRLabel _MN11;
        private DevExpress.XtraReports.UI.XRLabel _MN10;
        private DevExpress.XtraReports.UI.XRLabel xrLabel26;
        private DevExpress.XtraReports.UI.XRLabel xrLabel14;
        private DevExpress.XtraReports.UI.XRLabel _MN00;
        private DevExpress.XtraReports.UI.XRLabel _MN01;
        private DevExpress.XtraReports.UI.XRLabel _MN02;
        private DevExpress.XtraReports.UI.XRLabel xrLabel13;
        private DevExpress.XtraReports.UI.XRLabel xrLabel12;
        private DevExpress.XtraReports.UI.XRLabel xrLabel11;
        private DevExpress.XtraReports.UI.XRLabel xrLabel10;
        private DevExpress.XtraReports.UI.XRLabel xrLabel8;
        private DevExpress.XtraReports.UI.XRLabel xrLabel94;
        private DevExpress.XtraReports.UI.XRLabel xrLabel73;
        private DevExpress.XtraReports.UI.XRLabel xrLabel74;
        private DevExpress.XtraReports.UI.XRLabel xrLabel75;
        private DevExpress.XtraReports.UI.XRLabel xrLabel76;
        private DevExpress.XtraReports.UI.XRLabel xrLabel77;
        private DevExpress.XtraReports.UI.XRLabel _BONDLLS;
        private DevExpress.XtraReports.UI.XRLabel _BONMXN;
        private DevExpress.XtraReports.UI.XRLabel _NCDLLS;
        private DevExpress.XtraReports.UI.XRLabel _NCMXN;
        private DevExpress.XtraReports.UI.XRLabel xrLabel82;
        private DevExpress.XtraReports.UI.XRLabel xrLabel83;
        private DevExpress.XtraReports.UI.XRLabel xrLabel84;
        private DevExpress.XtraReports.UI.XRLabel xrLabel86;
        private DevExpress.XtraReports.UI.XRLabel xrLabel87;
        private DevExpress.XtraReports.UI.XRLabel xrLabel88;
        private DevExpress.XtraReports.UI.XRLabel _DESCDLLS;
        private DevExpress.XtraReports.UI.XRLabel _DESDLLS;
        private DevExpress.XtraReports.UI.XRLabel _DESCMXN;
        private DevExpress.XtraReports.UI.XRLabel xrLabel92;
        private DevExpress.XtraReports.UI.XRLabel xrLabel93;
        private DevExpress.XtraReports.UI.XRLabel _DONMXN;
        private DevExpress.XtraReports.UI.XRLabel _DONDLLS;
        private DevExpress.XtraReports.UI.XRLabel _CANelect;
        private DevExpress.XtraReports.UI.XRLabel _CANefectivo;
        private DevExpress.XtraReports.UI.XRLabel xrLabel98;
        private DevExpress.XtraReports.UI.XRLabel xrLabel99;
        private DevExpress.XtraReports.UI.XRLabel xrLabel100;
        private DevExpress.XtraReports.UI.XRLabel xrLabel101;
        private DevExpress.XtraReports.UI.XRLabel xrLabel102;
        private DevExpress.XtraReports.UI.XRLabel xrLabel103;
        private DevExpress.XtraReports.UI.XRLabel xrLabel105;
        private DevExpress.XtraReports.UI.XRLabel _CASHBACK;
        private DevExpress.XtraReports.UI.XRLabel xrLabel104;
        private DevExpress.XtraReports.UI.XRLabel xrLabel112;
        private DevExpress.XtraReports.UI.XRLabel xrLabel111;
        private DevExpress.XtraReports.UI.XRLabel xrLabel110;
        private DevExpress.XtraReports.UI.XRLabel xrLabel109;
        private DevExpress.XtraReports.UI.XRLabel xrLabel25;
        private DevExpress.XtraReports.UI.XRLabel xrLabel107;
        private DevExpress.XtraReports.UI.XRLabel xrLabel108;
        private DevExpress.XtraReports.UI.XRTable xrTable4;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow4;
        private DevExpress.XtraReports.UI.XRTableCell _DepDLLScc;
        private DevExpress.XtraReports.UI.XRTableCell _DepDLLSCcTipo;
        private DevExpress.XtraReports.UI.XRTableCell _DepDLLSMXNCc;
        private DevExpress.XtraReports.UI.XRTable xrTable3;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow3;
        private DevExpress.XtraReports.UI.XRTableCell _DepDLLScajas;
        private DevExpress.XtraReports.UI.XRTableCell _DepDLLSCajasTipo;
        private DevExpress.XtraReports.UI.XRTableCell _DepDLLSMXNCajas;
        private DevExpress.XtraReports.UI.XRTable xrTable2;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow2;
        private DevExpress.XtraReports.UI.XRTableCell _DepMXNcc;
        private DevExpress.XtraReports.UI.XRTableCell _DepTipoCc;
        private System.ComponentModel.BackgroundWorker backgroundWorker1;
        private DevExpress.XtraReports.UI.ReportHeaderBand ReportHeader1;
        private DevExpress.XtraReports.UI.ReportFooterBand ReportFooter;
        private DevExpress.XtraReports.UI.XRLabel _RENTAdlls;
        private DevExpress.XtraReports.UI.XRLabel _RENTAiva;
        private DevExpress.XtraReports.UI.XRLabel _RENTAtotalDlls;
        private DevExpress.XtraReports.UI.XRLabel _RENTAtotalDllsMxn;
        private DevExpress.XtraReports.UI.XRLabel xrLabel146;
        private DevExpress.XtraReports.UI.XRLabel xrLabel147;
        private DevExpress.XtraReports.UI.XRLabel xrLabel148;
        private DevExpress.XtraReports.UI.XRLabel xrLabel149;
        private DevExpress.XtraReports.UI.XRLabel xrLabel150;
        private DevExpress.XtraReports.UI.XRLabel xrLabel127;
        private DevExpress.XtraReports.UI.XRLabel xrLabel130;
        private DevExpress.XtraReports.UI.XRLabel xrLabel131;
        private DevExpress.XtraReports.UI.XRLabel xrLabel132;
        private DevExpress.XtraReports.UI.XRLabel xrLabel133;
        private DevExpress.XtraReports.UI.XRLabel _COMStotal;
        private DevExpress.XtraReports.UI.XRLabel _COMSdllsMxn;
        private DevExpress.XtraReports.UI.XRLabel _COMSdlls;
        private DevExpress.XtraReports.UI.XRLabel _COMSmxn;
        private DevExpress.XtraReports.UI.XRLabel _PAYSmxn;
        private DevExpress.XtraReports.UI.XRLabel _PAYSdlls;
        private DevExpress.XtraReports.UI.XRLabel _PAYSdllsMxn;
        private DevExpress.XtraReports.UI.XRLabel _PAYStotal;
        private DevExpress.XtraReports.UI.XRLabel xrLabel116;
        private DevExpress.XtraReports.UI.XRLabel xrLabel117;
        private DevExpress.XtraReports.UI.XRLabel xrLabel120;
        private DevExpress.XtraReports.UI.XRLabel xrLabel121;
        private DevExpress.XtraReports.UI.XRLabel xrLabel126;
        private DevExpress.XtraReports.UI.XRLabel xrLabel5;
        private DevExpress.XtraReports.UI.XRLabel xrLabel118;
        private DevExpress.XtraReports.UI.XRLabel _TDvalesElec;
        private DevExpress.XtraReports.UI.XRLabel xrLabel122;
        private DevExpress.XtraReports.UI.XRLabel _TDtotal;
        private DevExpress.XtraReports.UI.XRLabel _TDvales;
        private DevExpress.XtraReports.UI.XRLabel xrLabel125;
        private DevExpress.XtraReports.UI.XRLabel _TDcards;
        private DevExpress.XtraReports.UI.XRLabel xrLabel129;
        private DevExpress.XtraReports.UI.XRLabel xrLabel134;
        private DevExpress.XtraReports.UI.XRLabel _TDdllsmxn;
        private DevExpress.XtraReports.UI.XRLabel _TDmxn;
        private DevExpress.XtraReports.UI.XRLabel xrLabel137;
        private DevExpress.XtraReports.UI.DetailReportBand DetailReport1;
        private DevExpress.XtraReports.UI.DetailBand Detail2;
        private DevExpress.XtraReports.UI.XRTable _TableTipoPago;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow9;
        private DevExpress.XtraReports.UI.XRTableCell _PAYMETname;
        private DevExpress.XtraReports.UI.XRTableCell _PAYMETimporte;
        private DevExpress.XtraReports.UI.XRTable _TableIEPS;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow8;
        private DevExpress.XtraReports.UI.XRTableCell _IEPSperc;
        private DevExpress.XtraReports.UI.XRTableCell _IEPSimporte;
        private DevExpress.XtraReports.UI.XRTable _TableServ;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow7;
        private DevExpress.XtraReports.UI.XRTableCell _SERVname;
        private DevExpress.XtraReports.UI.XRTableCell _SERVimporte;
        private DevExpress.XtraReports.UI.XRTable _TablesDesc;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow6;
        private DevExpress.XtraReports.UI.XRTableCell _DESCcliente;
        private DevExpress.XtraReports.UI.XRTableCell _DESCimporte;
        private DevExpress.XtraReports.UI.XRTable _TableSanctions;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow5;
        private DevExpress.XtraReports.UI.XRTableCell _SANCcajero;
        private DevExpress.XtraReports.UI.XRTableCell _SANCimporte;
        private DevExpress.XtraReports.UI.ReportHeaderBand ReportHeader2;
        private DevExpress.XtraReports.UI.XRLabel xrLabel162;
        private DevExpress.XtraReports.UI.XRLabel xrLabel163;
        private DevExpress.XtraReports.UI.XRLabel xrLabel164;
        private DevExpress.XtraReports.UI.XRLabel xrLabel159;
        private DevExpress.XtraReports.UI.XRLabel xrLabel160;
        private DevExpress.XtraReports.UI.XRLabel xrLabel161;
        private DevExpress.XtraReports.UI.XRLabel xrLabel156;
        private DevExpress.XtraReports.UI.XRLabel xrLabel157;
        private DevExpress.XtraReports.UI.XRLabel xrLabel158;
        private DevExpress.XtraReports.UI.XRLabel xrLabel153;
        private DevExpress.XtraReports.UI.XRLabel xrLabel154;
        private DevExpress.XtraReports.UI.XRLabel xrLabel155;
        private DevExpress.XtraReports.UI.XRLabel xrLabel33;
        private DevExpress.XtraReports.UI.XRLabel xrLabel151;
        private DevExpress.XtraReports.UI.XRLabel xrLabel152;
        private DevExpress.XtraReports.UI.XRLabel xrLabel16;
        private DevExpress.XtraReports.UI.XRLabel _LprintDate;
        private DevExpress.XtraReports.UI.XRLabel xrLabel22;
        private DevExpress.XtraReports.UI.XRLabel _Ldiferencia;
        private DevExpress.XtraReports.UI.XRLabel _LTipoCambio;
        private DevExpress.XtraReports.UI.XRLabel xrLabel20;
        private DevExpress.XtraReports.UI.XRLabel _LTotalTOTAL;
        private DevExpress.XtraReports.UI.XRLabel _LTotalIEPS;
        private DevExpress.XtraReports.UI.XRLabel _LTotalIVA;
        private DevExpress.XtraReports.UI.XRLabel _LTotalIEPS0;
        private DevExpress.XtraReports.UI.XRLabel _LTotalGravado;
        private DevExpress.XtraReports.UI.XRLabel _LTotalTasa0;
        private DevExpress.XtraReports.UI.XRLabel xrLabel37;
        private DevExpress.XtraReports.UI.XRLabel xrLabel9;
        private DevExpress.XtraReports.UI.XRLabel xrLabel35;
        private DevExpress.XtraReports.UI.XRLabel _dllsEXEcre;
        private DevExpress.XtraReports.UI.XRLabel _dllsEXEcash;
        private DevExpress.XtraReports.UI.XRLabel _dllsEXEtotal;
        private DevExpress.XtraReports.UI.XRLabel _EXEcreGranTotal;
        private DevExpress.XtraReports.UI.XRLabel xrLabel27;
        private DevExpress.XtraReports.UI.XRLabel _mnEXEcre;
        private DevExpress.XtraReports.UI.XRLabel _mnEXEcash;
        private DevExpress.XtraReports.UI.XRLabel _mnEXEtotal;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell1;
        private DevExpress.XtraReports.UI.XRLabel xrLabel29;
    }
}
