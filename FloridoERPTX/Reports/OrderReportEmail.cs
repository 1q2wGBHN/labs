﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
using System.Data;
using App.Entities.ViewModels.Android;
using System.Collections.Generic;
using System.Globalization;

namespace FloridoERPTX.Reports
{
    public partial class OrderReportEmail : DevExpress.XtraReports.UI.XtraReport
    {
        public DataSet DataSetTableOrdered = null;
        public OrderReportEmail()
        {
            InitializeComponent();
        }
        public DataSet printTable(List<App.Entities.ViewModels.PurchaseOrder.PurchaseOrderModel> Purchase,string site,string currency,DateTime date) //se encarga de imprimir la orden del proveedor
        {
            DataSetTableOrdered = new DataSet();

            DataTable DataTableOrdered = new DataTable();
            DataSetTableOrdered.Tables.Add(DataTableOrdered);

            xrLabelDate.Text = DateTime.Now.ToString("dd/MM/yyyy");
            xrLabelFolioFormat.Text = Purchase[0].PurchaseNo.ToString();//"38679";
            xrLabelBranchOffice.Text =  site;//"*FLORIDO LAS AMARICAS 2";
            xrLabelMoneda.Text = currency;//"BIMBO S.A. DE C.V. 2";
            xrLabelDays.Text = date.ToString("dd/MM/yyyy");
            DataTableOrdered.Columns.Add(new DataColumn("xrTableCellUPC"));
            DataTableOrdered.Columns.Add(new DataColumn("xrTableCellProducto"));
            DataTableOrdered.Columns.Add(new DataColumn("xrTableCellPiezas"));
            DataTableOrdered.Columns.Add(new DataColumn("xrTableCellUnidad"));
            DataTableOrdered.Columns.Add(new DataColumn("xrTableCellPrecio"));
            DataTableOrdered.Columns.Add(new DataColumn("xrTableCellIva"));
            DataTableOrdered.Columns.Add(new DataColumn("xrTableCellIeps"));
            DataTableOrdered.Columns.Add(new DataColumn("xrTableCellSubTotal"));
            DataTableOrdered.Columns.Add(new DataColumn("xrTableCellTotal"));

            var cultureInfo = CultureInfo.GetCultureInfo("en-US");
            decimal subtotal = 0;
            decimal ivatotal = 0;
            decimal iepstotal = 0;
            decimal Total = 0;

            foreach (var item in Purchase)
            {
                DataRow DataRowOrdered = DataSetTableOrdered.Tables[0].NewRow();
                DataRowOrdered["xrTableCellUPC"] = item.PartNumber;
                DataRowOrdered["xrTableCellProducto"] = item.Description;
                DataRowOrdered["xrTableCellPiezas"] = item.Quantity;
                DataRowOrdered["xrTableCellUnidad"] = item.UnitSize;
                DataRowOrdered["xrTableCellPrecio"] = item.PurchasePrice;
                DataRowOrdered["xrTableCellIva"] =  item.Iva.ToString("C4"); 
                DataRowOrdered["xrTableCellIeps"] = item.Ieps.ToString("C4"); 
                DataRowOrdered["xrTableCellSubTotal"] = (item.PurchasePrice * item.Quantity).ToString("C4"); 
                DataRowOrdered["xrTableCellTotal"] = ((item.PurchasePrice * item.Quantity)+item.Iva+item.Ieps).ToString("C4");

                subtotal += (item.PurchasePrice * item.Quantity);
                ivatotal += item.Iva;
                iepstotal += item.Ieps;
                Total += ((item.PurchasePrice * item.Quantity) + item.Iva + item.Ieps);
                DataSetTableOrdered.Tables[0].Rows.Add(DataRowOrdered);
            }

            xrLabelAmount.Text = String.Format(cultureInfo, "{0:C4}", Total);
            xrLabelIEPS.Text = String.Format(cultureInfo, "{0:C4}", iepstotal);
            xrLabelIVA.Text = String.Format(cultureInfo, "{0:C4}", ivatotal);
            xrLabelSub.Text = String.Format(cultureInfo, "{0:C4}", subtotal);

            xrTableCellUPC.DataBindings.Add(new XRBinding("Text", null, "xrTable1.xrTableCellUPC"));
            xrTableCellProducto.DataBindings.Add(new XRBinding("Text", null, "xrTable1.xrTableCellProducto"));
            xrTableCellPiezas.DataBindings.Add(new XRBinding("Text", null, "xrTable1.xrTableCellPiezas"));
            xrTableCellUnidad.DataBindings.Add(new XRBinding("Text", null, "xrTable1.xrTableCellUnidad"));
            xrTableCellPrecio.DataBindings.Add(new XRBinding("Text", null, "xrTable1.xrTableCellPrecio"));
            xrTableCellIva.DataBindings.Add(new XRBinding("Text", null, "xrTable1.xrTableCellIva"));
            xrTableCellIeps.DataBindings.Add(new XRBinding("Text", null, "xrTable1.xrTableCellIeps"));
            xrTableCellSubTotal.DataBindings.Add(new XRBinding("Text", null, "xrTable1.xrTableCellSubTotal"));
            xrTableCellTotal.DataBindings.Add(new XRBinding("Text", null, "xrTable1.xrTableCellTotal"));
            return DataSetTableOrdered;
        }
        //public string ReturnFormatMoney(decimal value)
        //{
        //    var c = value.ToString().Split('.');
        //    int decimales = Int32.Parse(c[1]);
        //    var total = "";
        //    if (decimales > 0)
        //    {
        //        total = value.ToString("C4");
        //    }
        //    else
        //    {
        //        total = Int32.Parse(c[0]).ToString("C0");
        //    }
        //    return total;
        //}
        internal object printTable(object p, List<App.Entities.ViewModels.PurchaseOrder.PurchaseOrderModel> list)
        {
            throw new NotImplementedException();
        }
    }
}
