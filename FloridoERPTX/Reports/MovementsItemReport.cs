﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
using App.Entities.ViewModels.Item;
using System.Collections.Generic;
using System.Data;

namespace FloridoERPTX.Reports
{
    public partial class MovementsItemReport : DevExpress.XtraReports.UI.XtraReport
    {
        public DataSet DataSetTableMovements = null;
        public MovementsItemReport()
        {
            InitializeComponent();
        }

        public DataSet printTable (List<ItemKardexModel> items, string site, string partNumber, string partDescription, string dateRange)
        {
            try
            {
                DataSetTableMovements = new DataSet();

                DataTable tableMovements = new DataTable();
                DataSetTableMovements.Tables.Add(tableMovements);

                xrLabelSiteName.Text = site;
                xrLabelPartNumber.Text = partNumber;
                xrLabelPartDescription.Text = partDescription;
                xrLabelDateRange.Text = dateRange;

                tableMovements.Columns.Add(new DataColumn("cellDate"));
                tableMovements.Columns.Add(new DataColumn("cellDocument"));
                tableMovements.Columns.Add(new DataColumn("cellReference"));
                tableMovements.Columns.Add(new DataColumn("cellTypeReference"));
                tableMovements.Columns.Add(new DataColumn("cellTypeInven"));
                tableMovements.Columns.Add(new DataColumn("cellStock"));
                tableMovements.Columns.Add(new DataColumn("cellMovement"));
                tableMovements.Columns.Add(new DataColumn("cellQuantity"));
                tableMovements.Columns.Add(new DataColumn("cellFinalStock"));

                foreach (var item in items)
                {
                    DataRow DataRowOrdered = DataSetTableMovements.Tables[0].NewRow();
                    DataRowOrdered["cellDate"] = item.MovementDate;
                    DataRowOrdered["cellDocument"] = item.MovementDocument;
                    DataRowOrdered["cellReference"] = item.MovementReference;
                    DataRowOrdered["cellTypeReference"] = item.MovementReferenceType;
                    DataRowOrdered["cellTypeInven"] = item.StockType;
                    DataRowOrdered["cellStock"] = item.StorageLocationStock;
                    DataRowOrdered["cellMovement"] = item.DebitCredit;
                    DataRowOrdered["cellQuantity"] = item.MovementQuantity;
                    DataRowOrdered["cellFinalStock"] = item.TotalStock;
                    DataSetTableMovements.Tables[0].Rows.Add(DataRowOrdered);
                }

                cellDate.DataBindings.Add(new XRBinding("Text", null, "xrTableRow1.cellDate"));
                cellDocument.DataBindings.Add(new XRBinding("Text", null, "xrTableRow1.cellDocument"));
                cellReference.DataBindings.Add(new XRBinding("Text", null, "xrTableRow1.cellReference"));
                cellTypeReference.DataBindings.Add(new XRBinding("Text", null, "xrTableRow1.cellTypeReference"));
                cellTypeInven.DataBindings.Add(new XRBinding("Text", null, "xrTableRow1.cellTypeInven"));
                cellStock.DataBindings.Add(new XRBinding("Text", null, "xrTableRow1.cellStock"));
                cellMovement.DataBindings.Add(new XRBinding("Text", null, "xrTableRow1.cellMovement"));
                cellQuantity.DataBindings.Add(new XRBinding("Text", null, "xrTableRow1.cellQuantity"));
                cellFinalStock.DataBindings.Add(new XRBinding("Text", null, "xrTableRow1.cellFinalStock"));

                return DataSetTableMovements;
            }
            catch (Exception ex)
            {
                string d = ex.Message;
                throw;
            }
        }
    }
}
