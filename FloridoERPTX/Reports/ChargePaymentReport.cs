﻿using System;
using DevExpress.XtraReports.UI;
using System.Data;
using System.Linq;
using App.Entities.ViewModels.TransactionsCustomers;

namespace FloridoERP.Report
{
    public partial class ChargePaymentReport : DevExpress.XtraReports.UI.XtraReport
    {
        public DataSet DataSetTableServices = null;       
        public ChargePaymentReport()
        {
            InitializeComponent();
        }

        public DataSet printTable(TransactionsCustomersReport Model)
        {
            try
            {
                DataSetTableServices = new DataSet();
                DataTable DataTableServices = new DataTable();
                DataSetTableServices.Tables.Add(DataTableServices);
                StartDate.Text = Model.StartDate;
                EndDate.Text = Model.EndDate;
                totalCharge.Text = "$" + Model.ReportList.Sum(c => c.Charge).ToString("0.00");
                totalPayment.Text = "$" + Model.ReportList.Sum(c => c.Payment).ToString("0.00");


                DataTableServices.Columns.Add(new DataColumn("clientNumber"));
                DataTableServices.Columns.Add(new DataColumn("clientName"));
                DataTableServices.Columns.Add(new DataColumn("date"));
                DataTableServices.Columns.Add(new DataColumn("charge"));
                DataTableServices.Columns.Add(new DataColumn("payment"));
                DataTableServices.Columns.Add(new DataColumn("reference"));                

                foreach (var item in Model.ReportList)
                {
                    DataRow DataRowOrdered = DataSetTableServices.Tables[0].NewRow();
                    DataRowOrdered["clientNumber"] = item.ClientNumber;
                    DataRowOrdered["clientName"] = item.ClientName;
                    DataRowOrdered["date"] = item.Date;
                    DataRowOrdered["charge"] = item.ChargeText;
                    DataRowOrdered["payment"] = item.PaymentText;
                    DataRowOrdered["reference"] = item.Reference;    
                                    
                    DataSetTableServices.Tables[0].Rows.Add(DataRowOrdered);
                }

                clientNumber.DataBindings.Add(new XRBinding("Text", null, "xrTableRowFormat.clientNumber"));
                clientName.DataBindings.Add(new XRBinding("Text", null, "xrTableRowFormat.clientName"));
                date.DataBindings.Add(new XRBinding("Text", null, "xrTableRowFormat.date"));
                charge.DataBindings.Add(new XRBinding("Text", null, "xrTableRowFormat.charge"));
                payment.DataBindings.Add(new XRBinding("Text", null, "xrTableRowFormat.payment"));
                reference.DataBindings.Add(new XRBinding("Text", null, "xrTableRowFormat.reference"));                

                return DataSetTableServices;
            }
            catch (Exception ex)
            {
                string d = ex.Message;
                throw;
            }
        }        
    }
}
