﻿namespace FloridoERPTX.Reports
{
    partial class EmptySpaceScanReport
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(EmptySpaceScanReport));
            this.Detail = new DevExpress.XtraReports.UI.DetailBand();
            this.xrTable1 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow1 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCellPartNumber = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCellPartDescription = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCellPlanogram = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCellStock = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCellDateEntry = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCellAverageSale = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCellIncidence = new DevExpress.XtraReports.UI.XRTableCell();
            this.TopMargin = new DevExpress.XtraReports.UI.TopMarginBand();
            this.BottomMargin = new DevExpress.XtraReports.UI.BottomMarginBand();
            this.ReportHeader = new DevExpress.XtraReports.UI.ReportHeaderBand();
            this.xrLabelIncidencia = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabelNo = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabelFolio = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabelIncidenceNow = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabelincidence = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabelCodigo = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabeldescrip = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabelplanograma = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabelstock = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabelUltimaEntrada = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabelVentaPromedio = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabelDateCreate = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabelDate = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabelUserCreate = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabelUser = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabelTitlw = new DevExpress.XtraReports.UI.XRLabel();
            this.xrPictureBox1 = new DevExpress.XtraReports.UI.XRPictureBox();
            this.PageFooter = new DevExpress.XtraReports.UI.PageFooterBand();
            this.xrPageInfo1 = new DevExpress.XtraReports.UI.XRPageInfo();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // Detail
            // 
            this.Detail.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable1});
            this.Detail.Dpi = 100F;
            this.Detail.HeightF = 32.29167F;
            this.Detail.Name = "Detail";
            this.Detail.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.Detail.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrTable1
            // 
            this.xrTable1.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable1.Dpi = 100F;
            this.xrTable1.Font = new System.Drawing.Font("Times New Roman", 9F);
            this.xrTable1.LocationFloat = new DevExpress.Utils.PointFloat(0F, 0F);
            this.xrTable1.Name = "xrTable1";
            this.xrTable1.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow1});
            this.xrTable1.SizeF = new System.Drawing.SizeF(677F, 32.29167F);
            this.xrTable1.StylePriority.UseBorders = false;
            this.xrTable1.StylePriority.UseFont = false;
            // 
            // xrTableRow1
            // 
            this.xrTableRow1.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCellPartNumber,
            this.xrTableCellPartDescription,
            this.xrTableCellPlanogram,
            this.xrTableCellStock,
            this.xrTableCellDateEntry,
            this.xrTableCellAverageSale,
            this.xrTableCellIncidence});
            this.xrTableRow1.Dpi = 100F;
            this.xrTableRow1.Name = "xrTableRow1";
            this.xrTableRow1.Weight = 1D;
            // 
            // xrTableCellPartNumber
            // 
            this.xrTableCellPartNumber.Dpi = 100F;
            this.xrTableCellPartNumber.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCellPartNumber.Name = "xrTableCellPartNumber";
            this.xrTableCellPartNumber.StylePriority.UseFont = false;
            this.xrTableCellPartNumber.StylePriority.UseTextAlignment = false;
            this.xrTableCellPartNumber.Text = "xrTableCellPartNumber";
            this.xrTableCellPartNumber.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCellPartNumber.Weight = 0.77974358053133075D;
            // 
            // xrTableCellPartDescription
            // 
            this.xrTableCellPartDescription.Dpi = 100F;
            this.xrTableCellPartDescription.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCellPartDescription.Name = "xrTableCellPartDescription";
            this.xrTableCellPartDescription.StylePriority.UseFont = false;
            this.xrTableCellPartDescription.StylePriority.UseTextAlignment = false;
            this.xrTableCellPartDescription.Text = "xrTableCellPartDescription";
            this.xrTableCellPartDescription.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCellPartDescription.Weight = 1.515394706834106D;
            // 
            // xrTableCellPlanogram
            // 
            this.xrTableCellPlanogram.Dpi = 100F;
            this.xrTableCellPlanogram.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCellPlanogram.Name = "xrTableCellPlanogram";
            this.xrTableCellPlanogram.StylePriority.UseFont = false;
            this.xrTableCellPlanogram.StylePriority.UseTextAlignment = false;
            this.xrTableCellPlanogram.Text = "xrTableCellPlanogram";
            this.xrTableCellPlanogram.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCellPlanogram.Weight = 0.585812801221629D;
            // 
            // xrTableCellStock
            // 
            this.xrTableCellStock.Dpi = 100F;
            this.xrTableCellStock.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCellStock.Name = "xrTableCellStock";
            this.xrTableCellStock.StylePriority.UseFont = false;
            this.xrTableCellStock.StylePriority.UseTextAlignment = false;
            this.xrTableCellStock.Text = "xrTableCellStock";
            this.xrTableCellStock.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCellStock.Weight = 0.53442830552720044D;
            // 
            // xrTableCellDateEntry
            // 
            this.xrTableCellDateEntry.Dpi = 100F;
            this.xrTableCellDateEntry.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCellDateEntry.Name = "xrTableCellDateEntry";
            this.xrTableCellDateEntry.StylePriority.UseFont = false;
            this.xrTableCellDateEntry.StylePriority.UseTextAlignment = false;
            this.xrTableCellDateEntry.Text = "xrTableCellDateEntry";
            this.xrTableCellDateEntry.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCellDateEntry.Weight = 0.49623437382559465D;
            // 
            // xrTableCellAverageSale
            // 
            this.xrTableCellAverageSale.Dpi = 100F;
            this.xrTableCellAverageSale.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCellAverageSale.Name = "xrTableCellAverageSale";
            this.xrTableCellAverageSale.StylePriority.UseFont = false;
            this.xrTableCellAverageSale.StylePriority.UseTextAlignment = false;
            this.xrTableCellAverageSale.Text = "xrTableCellAverageSale";
            this.xrTableCellAverageSale.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCellAverageSale.Weight = 0.46463776234738324D;
            // 
            // xrTableCellIncidence
            // 
            this.xrTableCellIncidence.Dpi = 100F;
            this.xrTableCellIncidence.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTableCellIncidence.Name = "xrTableCellIncidence";
            this.xrTableCellIncidence.StylePriority.UseFont = false;
            this.xrTableCellIncidence.StylePriority.UseTextAlignment = false;
            this.xrTableCellIncidence.Text = "xrTableCellIncidence";
            this.xrTableCellIncidence.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCellIncidence.Weight = 0.49654512856313326D;
            // 
            // TopMargin
            // 
            this.TopMargin.Dpi = 100F;
            this.TopMargin.HeightF = 36F;
            this.TopMargin.Name = "TopMargin";
            this.TopMargin.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.TopMargin.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // BottomMargin
            // 
            this.BottomMargin.Dpi = 100F;
            this.BottomMargin.HeightF = 50F;
            this.BottomMargin.Name = "BottomMargin";
            this.BottomMargin.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.BottomMargin.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // ReportHeader
            // 
            this.ReportHeader.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabelIncidencia,
            this.xrLabelNo,
            this.xrLabelFolio,
            this.xrLabelIncidenceNow,
            this.xrLabelincidence,
            this.xrLabelCodigo,
            this.xrLabeldescrip,
            this.xrLabelplanograma,
            this.xrLabelstock,
            this.xrLabelUltimaEntrada,
            this.xrLabelVentaPromedio,
            this.xrLabelDateCreate,
            this.xrLabelDate,
            this.xrLabelUserCreate,
            this.xrLabelUser,
            this.xrLabelTitlw,
            this.xrPictureBox1});
            this.ReportHeader.Dpi = 100F;
            this.ReportHeader.HeightF = 185.4168F;
            this.ReportHeader.Name = "ReportHeader";
            // 
            // xrLabelIncidencia
            // 
            this.xrLabelIncidencia.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabelIncidencia.Dpi = 100F;
            this.xrLabelIncidencia.Font = new System.Drawing.Font("Times New Roman", 9.75F);
            this.xrLabelIncidencia.LocationFloat = new DevExpress.Utils.PointFloat(608.0127F, 145.7501F);
            this.xrLabelIncidencia.Multiline = true;
            this.xrLabelIncidencia.Name = "xrLabelIncidencia";
            this.xrLabelIncidencia.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabelIncidencia.SizeF = new System.Drawing.SizeF(68.98737F, 39.6667F);
            this.xrLabelIncidencia.StylePriority.UseBorders = false;
            this.xrLabelIncidencia.StylePriority.UseFont = false;
            this.xrLabelIncidencia.Text = "Incidencas";
            // 
            // xrLabelNo
            // 
            this.xrLabelNo.Dpi = 100F;
            this.xrLabelNo.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabelNo.LocationFloat = new DevExpress.Utils.PointFloat(248.2487F, 62.83334F);
            this.xrLabelNo.Multiline = true;
            this.xrLabelNo.Name = "xrLabelNo";
            this.xrLabelNo.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabelNo.SizeF = new System.Drawing.SizeF(185.3483F, 22.99998F);
            this.xrLabelNo.SnapLineMargin = new DevExpress.XtraPrinting.PaddingInfo(2, 3, 0, 0, 100F);
            this.xrLabelNo.StylePriority.UseFont = false;
            this.xrLabelNo.StylePriority.UseTextAlignment = false;
            this.xrLabelNo.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLabelFolio
            // 
            this.xrLabelFolio.Dpi = 100F;
            this.xrLabelFolio.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabelFolio.LocationFloat = new DevExpress.Utils.PointFloat(144.9165F, 62.83334F);
            this.xrLabelFolio.Name = "xrLabelFolio";
            this.xrLabelFolio.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabelFolio.SizeF = new System.Drawing.SizeF(98.33221F, 22.99998F);
            this.xrLabelFolio.SnapLineMargin = new DevExpress.XtraPrinting.PaddingInfo(2, 3, 0, 0, 100F);
            this.xrLabelFolio.StylePriority.UseFont = false;
            this.xrLabelFolio.StylePriority.UseTextAlignment = false;
            this.xrLabelFolio.Text = "Folio:";
            this.xrLabelFolio.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLabelIncidenceNow
            // 
            this.xrLabelIncidenceNow.Dpi = 100F;
            this.xrLabelIncidenceNow.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabelIncidenceNow.LocationFloat = new DevExpress.Utils.PointFloat(537F, 104.0001F);
            this.xrLabelIncidenceNow.Multiline = true;
            this.xrLabelIncidenceNow.Name = "xrLabelIncidenceNow";
            this.xrLabelIncidenceNow.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabelIncidenceNow.SizeF = new System.Drawing.SizeF(100F, 22.99998F);
            this.xrLabelIncidenceNow.SnapLineMargin = new DevExpress.XtraPrinting.PaddingInfo(2, 3, 0, 0, 100F);
            this.xrLabelIncidenceNow.StylePriority.UseFont = false;
            this.xrLabelIncidenceNow.StylePriority.UseTextAlignment = false;
            this.xrLabelIncidenceNow.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLabelincidence
            // 
            this.xrLabelincidence.Dpi = 100F;
            this.xrLabelincidence.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabelincidence.LocationFloat = new DevExpress.Utils.PointFloat(446.0969F, 104.0001F);
            this.xrLabelincidence.Name = "xrLabelincidence";
            this.xrLabelincidence.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabelincidence.SizeF = new System.Drawing.SizeF(85.90308F, 22.99998F);
            this.xrLabelincidence.SnapLineMargin = new DevExpress.XtraPrinting.PaddingInfo(2, 3, 0, 0, 100F);
            this.xrLabelincidence.StylePriority.UseFont = false;
            this.xrLabelincidence.StylePriority.UseTextAlignment = false;
            this.xrLabelincidence.Text = "Incidencias:";
            this.xrLabelincidence.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLabelCodigo
            // 
            this.xrLabelCodigo.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabelCodigo.Dpi = 100F;
            this.xrLabelCodigo.LocationFloat = new DevExpress.Utils.PointFloat(1.04173F, 145.75F);
            this.xrLabelCodigo.Name = "xrLabelCodigo";
            this.xrLabelCodigo.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabelCodigo.SizeF = new System.Drawing.SizeF(107.2916F, 39.66672F);
            this.xrLabelCodigo.StylePriority.UseBorders = false;
            this.xrLabelCodigo.Text = "Código";
            // 
            // xrLabeldescrip
            // 
            this.xrLabeldescrip.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabeldescrip.Dpi = 100F;
            this.xrLabeldescrip.LocationFloat = new DevExpress.Utils.PointFloat(108.3333F, 145.75F);
            this.xrLabeldescrip.Name = "xrLabeldescrip";
            this.xrLabeldescrip.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabeldescrip.SizeF = new System.Drawing.SizeF(210.5407F, 39.66672F);
            this.xrLabeldescrip.StylePriority.UseBorders = false;
            this.xrLabeldescrip.Text = "Descripción";
            // 
            // xrLabelplanograma
            // 
            this.xrLabelplanograma.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabelplanograma.Dpi = 100F;
            this.xrLabelplanograma.Font = new System.Drawing.Font("Times New Roman", 9.75F);
            this.xrLabelplanograma.LocationFloat = new DevExpress.Utils.PointFloat(318.8741F, 145.75F);
            this.xrLabelplanograma.Name = "xrLabelplanograma";
            this.xrLabelplanograma.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabelplanograma.SizeF = new System.Drawing.SizeF(81.38956F, 39.66669F);
            this.xrLabelplanograma.StylePriority.UseBorders = false;
            this.xrLabelplanograma.StylePriority.UseFont = false;
            this.xrLabelplanograma.Text = "Planograma";
            // 
            // xrLabelstock
            // 
            this.xrLabelstock.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabelstock.Dpi = 100F;
            this.xrLabelstock.LocationFloat = new DevExpress.Utils.PointFloat(400.2637F, 145.75F);
            this.xrLabelstock.Multiline = true;
            this.xrLabelstock.Name = "xrLabelstock";
            this.xrLabelstock.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabelstock.SizeF = new System.Drawing.SizeF(74.25061F, 39.66673F);
            this.xrLabelstock.StylePriority.UseBorders = false;
            this.xrLabelstock.Text = "Stock\r\nActual";
            // 
            // xrLabelUltimaEntrada
            // 
            this.xrLabelUltimaEntrada.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabelUltimaEntrada.Dpi = 100F;
            this.xrLabelUltimaEntrada.LocationFloat = new DevExpress.Utils.PointFloat(474.5143F, 145.75F);
            this.xrLabelUltimaEntrada.Multiline = true;
            this.xrLabelUltimaEntrada.Name = "xrLabelUltimaEntrada";
            this.xrLabelUltimaEntrada.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabelUltimaEntrada.SizeF = new System.Drawing.SizeF(68.94406F, 39.6667F);
            this.xrLabelUltimaEntrada.StylePriority.UseBorders = false;
            this.xrLabelUltimaEntrada.Text = "Última\r\nEntrada";
            // 
            // xrLabelVentaPromedio
            // 
            this.xrLabelVentaPromedio.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabelVentaPromedio.Dpi = 100F;
            this.xrLabelVentaPromedio.LocationFloat = new DevExpress.Utils.PointFloat(543.4584F, 145.75F);
            this.xrLabelVentaPromedio.Multiline = true;
            this.xrLabelVentaPromedio.Name = "xrLabelVentaPromedio";
            this.xrLabelVentaPromedio.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabelVentaPromedio.SizeF = new System.Drawing.SizeF(64.55432F, 39.6667F);
            this.xrLabelVentaPromedio.StylePriority.UseBorders = false;
            this.xrLabelVentaPromedio.Text = "Venta\r\nPromedio";
            // 
            // xrLabelDateCreate
            // 
            this.xrLabelDateCreate.Dpi = 100F;
            this.xrLabelDateCreate.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabelDateCreate.LocationFloat = new DevExpress.Utils.PointFloat(537F, 62.83334F);
            this.xrLabelDateCreate.Multiline = true;
            this.xrLabelDateCreate.Name = "xrLabelDateCreate";
            this.xrLabelDateCreate.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabelDateCreate.SizeF = new System.Drawing.SizeF(100F, 22.99998F);
            this.xrLabelDateCreate.SnapLineMargin = new DevExpress.XtraPrinting.PaddingInfo(2, 3, 0, 0, 100F);
            this.xrLabelDateCreate.StylePriority.UseFont = false;
            this.xrLabelDateCreate.StylePriority.UseTextAlignment = false;
            this.xrLabelDateCreate.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLabelDate
            // 
            this.xrLabelDate.Dpi = 100F;
            this.xrLabelDate.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabelDate.LocationFloat = new DevExpress.Utils.PointFloat(451.0971F, 62.83334F);
            this.xrLabelDate.Name = "xrLabelDate";
            this.xrLabelDate.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabelDate.SizeF = new System.Drawing.SizeF(56.75049F, 22.99998F);
            this.xrLabelDate.SnapLineMargin = new DevExpress.XtraPrinting.PaddingInfo(2, 3, 0, 0, 100F);
            this.xrLabelDate.StylePriority.UseFont = false;
            this.xrLabelDate.StylePriority.UseTextAlignment = false;
            this.xrLabelDate.Text = "Fecha:";
            this.xrLabelDate.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLabelUserCreate
            // 
            this.xrLabelUserCreate.Dpi = 100F;
            this.xrLabelUserCreate.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabelUserCreate.LocationFloat = new DevExpress.Utils.PointFloat(248.2487F, 104.0001F);
            this.xrLabelUserCreate.Multiline = true;
            this.xrLabelUserCreate.Name = "xrLabelUserCreate";
            this.xrLabelUserCreate.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabelUserCreate.SizeF = new System.Drawing.SizeF(185.3483F, 22.99998F);
            this.xrLabelUserCreate.SnapLineMargin = new DevExpress.XtraPrinting.PaddingInfo(2, 3, 0, 0, 100F);
            this.xrLabelUserCreate.StylePriority.UseFont = false;
            this.xrLabelUserCreate.StylePriority.UseTextAlignment = false;
            this.xrLabelUserCreate.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLabelUser
            // 
            this.xrLabelUser.Dpi = 100F;
            this.xrLabelUser.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabelUser.LocationFloat = new DevExpress.Utils.PointFloat(144.9165F, 104.0001F);
            this.xrLabelUser.Name = "xrLabelUser";
            this.xrLabelUser.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabelUser.SizeF = new System.Drawing.SizeF(98.33221F, 22.99998F);
            this.xrLabelUser.SnapLineMargin = new DevExpress.XtraPrinting.PaddingInfo(2, 3, 0, 0, 100F);
            this.xrLabelUser.StylePriority.UseFont = false;
            this.xrLabelUser.StylePriority.UseTextAlignment = false;
            this.xrLabelUser.Text = "Elaborada por:";
            this.xrLabelUser.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLabelTitlw
            // 
            this.xrLabelTitlw.Dpi = 100F;
            this.xrLabelTitlw.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Bold);
            this.xrLabelTitlw.LocationFloat = new DevExpress.Utils.PointFloat(173.5417F, 10.00001F);
            this.xrLabelTitlw.Name = "xrLabelTitlw";
            this.xrLabelTitlw.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabelTitlw.SizeF = new System.Drawing.SizeF(466.4583F, 38.62498F);
            this.xrLabelTitlw.StylePriority.UseFont = false;
            this.xrLabelTitlw.StylePriority.UseTextAlignment = false;
            this.xrLabelTitlw.Text = "REPORTE DE ESPACIOS VACÍOS";
            this.xrLabelTitlw.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrPictureBox1
            // 
            this.xrPictureBox1.Dpi = 100F;
            this.xrPictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("xrPictureBox1.Image")));
            this.xrPictureBox1.ImageAlignment = DevExpress.XtraPrinting.ImageAlignment.TopLeft;
            this.xrPictureBox1.LocationFloat = new DevExpress.Utils.PointFloat(1.04173F, 10.00001F);
            this.xrPictureBox1.Name = "xrPictureBox1";
            this.xrPictureBox1.SizeF = new System.Drawing.SizeF(127.5F, 108.75F);
            // 
            // PageFooter
            // 
            this.PageFooter.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrPageInfo1});
            this.PageFooter.Dpi = 100F;
            this.PageFooter.HeightF = 39.58333F;
            this.PageFooter.Name = "PageFooter";
            // 
            // xrPageInfo1
            // 
            this.xrPageInfo1.Dpi = 100F;
            this.xrPageInfo1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrPageInfo1.Format = "Página {0} de {1}";
            this.xrPageInfo1.LocationFloat = new DevExpress.Utils.PointFloat(537F, 14.58333F);
            this.xrPageInfo1.Name = "xrPageInfo1";
            this.xrPageInfo1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrPageInfo1.SizeF = new System.Drawing.SizeF(100F, 15F);
            this.xrPageInfo1.StylePriority.UseFont = false;
            // 
            // EmptySpaceScanReport
            // 
            this.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.Detail,
            this.TopMargin,
            this.BottomMargin,
            this.ReportHeader,
            this.PageFooter});
            this.Margins = new System.Drawing.Printing.Margins(100, 73, 36, 50);
            this.Version = "16.2";
            ((System.ComponentModel.ISupportInitialize)(this.xrTable1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

        }

        #endregion

        private DevExpress.XtraReports.UI.DetailBand Detail;
        private DevExpress.XtraReports.UI.TopMarginBand TopMargin;
        private DevExpress.XtraReports.UI.BottomMarginBand BottomMargin;
        private DevExpress.XtraReports.UI.ReportHeaderBand ReportHeader;
        private DevExpress.XtraReports.UI.XRPictureBox xrPictureBox1;
        private DevExpress.XtraReports.UI.XRLabel xrLabelTitlw;
        private DevExpress.XtraReports.UI.XRLabel xrLabelDateCreate;
        private DevExpress.XtraReports.UI.XRLabel xrLabelDate;
        private DevExpress.XtraReports.UI.XRLabel xrLabelUserCreate;
        private DevExpress.XtraReports.UI.XRLabel xrLabelUser;
        private DevExpress.XtraReports.UI.PageFooterBand PageFooter;
        private DevExpress.XtraReports.UI.XRPageInfo xrPageInfo1;
        private DevExpress.XtraReports.UI.XRLabel xrLabelCodigo;
        private DevExpress.XtraReports.UI.XRLabel xrLabeldescrip;
        private DevExpress.XtraReports.UI.XRLabel xrLabelplanograma;
        private DevExpress.XtraReports.UI.XRLabel xrLabelstock;
        private DevExpress.XtraReports.UI.XRLabel xrLabelUltimaEntrada;
        private DevExpress.XtraReports.UI.XRLabel xrLabelVentaPromedio;
        private DevExpress.XtraReports.UI.XRTable xrTable1;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow1;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCellPartNumber;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCellPartDescription;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCellPlanogram;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCellStock;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCellDateEntry;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCellAverageSale;
        private DevExpress.XtraReports.UI.XRLabel xrLabelIncidenceNow;
        private DevExpress.XtraReports.UI.XRLabel xrLabelincidence;
        private DevExpress.XtraReports.UI.XRLabel xrLabelNo;
        private DevExpress.XtraReports.UI.XRLabel xrLabelFolio;
        private DevExpress.XtraReports.UI.XRLabel xrLabelIncidencia;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCellIncidence;
    }
}
