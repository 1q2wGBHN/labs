﻿namespace FloridoERPTX.Reports
{
    partial class InvoicesReport
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(InvoicesReport));
            this.Detail = new DevExpress.XtraReports.UI.DetailBand();
            this.xrLabel34 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel30 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel15 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel8 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel10 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel11 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel12 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel13 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel14 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel2 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel6 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel1 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLine4 = new DevExpress.XtraReports.UI.XRLine();
            this.xrLabel17 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel18 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel19 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel24 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel25 = new DevExpress.XtraReports.UI.XRLabel();
            this.TopMargin = new DevExpress.XtraReports.UI.TopMarginBand();
            this.BottomMargin = new DevExpress.XtraReports.UI.BottomMarginBand();
            this.ReportHeader = new DevExpress.XtraReports.UI.ReportHeaderBand();
            this.labelDate1 = new DevExpress.XtraReports.UI.XRLabel();
            this.labelDate2 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel7 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel5 = new DevExpress.XtraReports.UI.XRLabel();
            this.labelDate = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel9 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLine1 = new DevExpress.XtraReports.UI.XRLine();
            this._labelReporte = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel4 = new DevExpress.XtraReports.UI.XRLabel();
            this.siteLabel = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel3 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrPictureBox1 = new DevExpress.XtraReports.UI.XRPictureBox();
            this.GroupFooter1 = new DevExpress.XtraReports.UI.GroupFooterBand();
            this._exeTotal = new DevExpress.XtraReports.UI.XRLabel();
            this._exeCash = new DevExpress.XtraReports.UI.XRLabel();
            this._exeCredit = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel38 = new DevExpress.XtraReports.UI.XRLabel();
            this.lbl_header_credito = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel16 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel20 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel21 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel22 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel23 = new DevExpress.XtraReports.UI.XRLabel();
            this.lbl_credit_zrate = new DevExpress.XtraReports.UI.XRLabel();
            this.lbl_credit_taxed = new DevExpress.XtraReports.UI.XRLabel();
            this.lbl_credit_tax = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel26 = new DevExpress.XtraReports.UI.XRLabel();
            this.lbl_cash_zrate = new DevExpress.XtraReports.UI.XRLabel();
            this.lbl_cash_taxed = new DevExpress.XtraReports.UI.XRLabel();
            this.lbl_cash_tax = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel27 = new DevExpress.XtraReports.UI.XRLabel();
            this.lbl_total_zrate = new DevExpress.XtraReports.UI.XRLabel();
            this.lbl_total_taxed = new DevExpress.XtraReports.UI.XRLabel();
            this.lbl_credit_ieps = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel28 = new DevExpress.XtraReports.UI.XRLabel();
            this.lbl_total_tax = new DevExpress.XtraReports.UI.XRLabel();
            this.lbl_total_ieps = new DevExpress.XtraReports.UI.XRLabel();
            this.lbl_cash_ieps = new DevExpress.XtraReports.UI.XRLabel();
            this.lbl_total_cash = new DevExpress.XtraReports.UI.XRLabel();
            this.lbl_total_global = new DevExpress.XtraReports.UI.XRLabel();
            this.lbl_total_credit = new DevExpress.XtraReports.UI.XRLabel();
            this.iepsLabel = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel31 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel32 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel33 = new DevExpress.XtraReports.UI.XRLabel();
            this.subtotalLabel = new DevExpress.XtraReports.UI.XRLabel();
            this.totalLabel = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel29 = new DevExpress.XtraReports.UI.XRLabel();
            this.ivaLabel = new DevExpress.XtraReports.UI.XRLabel();
            this.PageFooter = new DevExpress.XtraReports.UI.PageFooterBand();
            this.xrPageInfo1 = new DevExpress.XtraReports.UI.XRPageInfo();
            this.DetailReport = new DevExpress.XtraReports.UI.DetailReportBand();
            this.Detail1 = new DevExpress.XtraReports.UI.DetailBand();
            this.xrTable1 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow1 = new DevExpress.XtraReports.UI.XRTableRow();
            this.InvoiceNumber = new DevExpress.XtraReports.UI.XRTableCell();
            this.CreateDate = new DevExpress.XtraReports.UI.XRTableCell();
            this.CustomerCode = new DevExpress.XtraReports.UI.XRTableCell();
            this.CustomerName = new DevExpress.XtraReports.UI.XRTableCell();
            this.DueDate = new DevExpress.XtraReports.UI.XRTableCell();
            this.Amount = new DevExpress.XtraReports.UI.XRTableCell();
            this.Ieps = new DevExpress.XtraReports.UI.XRTableCell();
            this.IepsPredicate = new DevExpress.XtraReports.UI.XRTableCell();
            this.Tax = new DevExpress.XtraReports.UI.XRTableCell();
            this.TotalAmount = new DevExpress.XtraReports.UI.XRTableCell();
            this.TotalCredit = new DevExpress.XtraReports.UI.XRTableCell();
            this.TotalCash = new DevExpress.XtraReports.UI.XRTableCell();
            this.PaymentMethod = new DevExpress.XtraReports.UI.XRTableCell();
            this.Balance = new DevExpress.XtraReports.UI.XRTableCell();
            this.CurrencyCode = new DevExpress.XtraReports.UI.XRTableCell();
            this.FiscalNumber = new DevExpress.XtraReports.UI.XRTableCell();
            this.Status = new DevExpress.XtraReports.UI.XRTableCell();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // Detail
            // 
            this.Detail.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel34,
            this.xrLabel30,
            this.xrLabel15,
            this.xrLabel8,
            this.xrLabel10,
            this.xrLabel11,
            this.xrLabel12,
            this.xrLabel13,
            this.xrLabel14,
            this.xrLabel2,
            this.xrLabel6,
            this.xrLabel1,
            this.xrLine4,
            this.xrLabel17,
            this.xrLabel18,
            this.xrLabel19,
            this.xrLabel24,
            this.xrLabel25});
            this.Detail.Dpi = 100F;
            this.Detail.HeightF = 60.7075F;
            this.Detail.Name = "Detail";
            this.Detail.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.Detail.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel34
            // 
            this.xrLabel34.Dpi = 100F;
            this.xrLabel34.Font = new System.Drawing.Font("Arial", 7F, System.Drawing.FontStyle.Bold);
            this.xrLabel34.LocationFloat = new DevExpress.Utils.PointFloat(730.452F, 32.15405F);
            this.xrLabel34.Multiline = true;
            this.xrLabel34.Name = "xrLabel34";
            this.xrLabel34.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel34.SizeF = new System.Drawing.SizeF(54.39557F, 17.55346F);
            this.xrLabel34.StylePriority.UseFont = false;
            this.xrLabel34.StylePriority.UseTextAlignment = false;
            this.xrLabel34.Text = "CONTADO";
            this.xrLabel34.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrLabel30
            // 
            this.xrLabel30.Dpi = 100F;
            this.xrLabel30.Font = new System.Drawing.Font("Arial", 7F, System.Drawing.FontStyle.Bold);
            this.xrLabel30.LocationFloat = new DevExpress.Utils.PointFloat(784.8475F, 32.52821F);
            this.xrLabel30.Multiline = true;
            this.xrLabel30.Name = "xrLabel30";
            this.xrLabel30.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel30.SizeF = new System.Drawing.SizeF(50.45331F, 17.55346F);
            this.xrLabel30.StylePriority.UseFont = false;
            this.xrLabel30.StylePriority.UseTextAlignment = false;
            this.xrLabel30.Text = "PAGO";
            this.xrLabel30.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrLabel15
            // 
            this.xrLabel15.Dpi = 100F;
            this.xrLabel15.Font = new System.Drawing.Font("Arial", 7F, System.Drawing.FontStyle.Bold);
            this.xrLabel15.LocationFloat = new DevExpress.Utils.PointFloat(1012.362F, 33.15404F);
            this.xrLabel15.Name = "xrLabel15";
            this.xrLabel15.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel15.SizeF = new System.Drawing.SizeF(56.43756F, 16.55346F);
            this.xrLabel15.StylePriority.UseFont = false;
            this.xrLabel15.StylePriority.UseTextAlignment = false;
            this.xrLabel15.Text = "Estado";
            this.xrLabel15.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrLabel8
            // 
            this.xrLabel8.Dpi = 100F;
            this.xrLabel8.Font = new System.Drawing.Font("Arial", 7F, System.Drawing.FontStyle.Bold);
            this.xrLabel8.LocationFloat = new DevExpress.Utils.PointFloat(955.9243F, 32.41962F);
            this.xrLabel8.Name = "xrLabel8";
            this.xrLabel8.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel8.SizeF = new System.Drawing.SizeF(56.43756F, 17.55346F);
            this.xrLabel8.StylePriority.UseFont = false;
            this.xrLabel8.StylePriority.UseTextAlignment = false;
            this.xrLabel8.Text = "Folio Fiscal";
            this.xrLabel8.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrLabel10
            // 
            this.xrLabel10.Dpi = 100F;
            this.xrLabel10.Font = new System.Drawing.Font("Arial", 7F, System.Drawing.FontStyle.Bold);
            this.xrLabel10.LocationFloat = new DevExpress.Utils.PointFloat(899.4868F, 32.47392F);
            this.xrLabel10.Name = "xrLabel10";
            this.xrLabel10.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel10.SizeF = new System.Drawing.SizeF(56.43756F, 17.55346F);
            this.xrLabel10.StylePriority.UseFont = false;
            this.xrLabel10.StylePriority.UseTextAlignment = false;
            this.xrLabel10.Text = "Moneda";
            this.xrLabel10.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrLabel11
            // 
            this.xrLabel11.Dpi = 100F;
            this.xrLabel11.Font = new System.Drawing.Font("Arial", 7F, System.Drawing.FontStyle.Bold);
            this.xrLabel11.LocationFloat = new DevExpress.Utils.PointFloat(835.3008F, 32.47392F);
            this.xrLabel11.Name = "xrLabel11";
            this.xrLabel11.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel11.SizeF = new System.Drawing.SizeF(64.18604F, 17.55346F);
            this.xrLabel11.StylePriority.UseFont = false;
            this.xrLabel11.StylePriority.UseTextAlignment = false;
            this.xrLabel11.Text = "Saldo";
            this.xrLabel11.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrLabel12
            // 
            this.xrLabel12.Dpi = 100F;
            this.xrLabel12.Font = new System.Drawing.Font("Arial", 7F, System.Drawing.FontStyle.Bold);
            this.xrLabel12.LocationFloat = new DevExpress.Utils.PointFloat(676.0564F, 32.47392F);
            this.xrLabel12.Multiline = true;
            this.xrLabel12.Name = "xrLabel12";
            this.xrLabel12.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel12.SizeF = new System.Drawing.SizeF(54.39557F, 17.55346F);
            this.xrLabel12.StylePriority.UseFont = false;
            this.xrLabel12.StylePriority.UseTextAlignment = false;
            this.xrLabel12.Text = "CRÉDITO";
            this.xrLabel12.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrLabel13
            // 
            this.xrLabel13.Dpi = 100F;
            this.xrLabel13.Font = new System.Drawing.Font("Arial", 7F, System.Drawing.FontStyle.Bold);
            this.xrLabel13.LocationFloat = new DevExpress.Utils.PointFloat(622.5681F, 32.47392F);
            this.xrLabel13.Name = "xrLabel13";
            this.xrLabel13.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel13.SizeF = new System.Drawing.SizeF(53.48831F, 17.55346F);
            this.xrLabel13.StylePriority.UseFont = false;
            this.xrLabel13.StylePriority.UseTextAlignment = false;
            this.xrLabel13.Text = "TOTAL";
            this.xrLabel13.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrLabel14
            // 
            this.xrLabel14.Dpi = 100F;
            this.xrLabel14.Font = new System.Drawing.Font("Arial", 7F, System.Drawing.FontStyle.Bold);
            this.xrLabel14.LocationFloat = new DevExpress.Utils.PointFloat(567.946F, 32.41962F);
            this.xrLabel14.Name = "xrLabel14";
            this.xrLabel14.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel14.SizeF = new System.Drawing.SizeF(54.62207F, 17.60775F);
            this.xrLabel14.StylePriority.UseFont = false;
            this.xrLabel14.StylePriority.UseTextAlignment = false;
            this.xrLabel14.Text = "IVA";
            this.xrLabel14.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrLabel2
            // 
            this.xrLabel2.Dpi = 100F;
            this.xrLabel2.Font = new System.Drawing.Font("Arial", 7F, System.Drawing.FontStyle.Bold);
            this.xrLabel2.LocationFloat = new DevExpress.Utils.PointFloat(426.0338F, 32.52821F);
            this.xrLabel2.Name = "xrLabel2";
            this.xrLabel2.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel2.SizeF = new System.Drawing.SizeF(37.68756F, 17.55346F);
            this.xrLabel2.StylePriority.UseFont = false;
            this.xrLabel2.StylePriority.UseTextAlignment = false;
            this.xrLabel2.Text = "IEPS";
            this.xrLabel2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrLabel6
            // 
            this.xrLabel6.Dpi = 100F;
            this.xrLabel6.Font = new System.Drawing.Font("Arial", 7F, System.Drawing.FontStyle.Bold);
            this.xrLabel6.LocationFloat = new DevExpress.Utils.PointFloat(463.7213F, 32.47392F);
            this.xrLabel6.Name = "xrLabel6";
            this.xrLabel6.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel6.SizeF = new System.Drawing.SizeF(104.2247F, 17.55346F);
            this.xrLabel6.StylePriority.UseFont = false;
            this.xrLabel6.StylePriority.UseTextAlignment = false;
            this.xrLabel6.Text = "IEPS PORCENTAJES";
            this.xrLabel6.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrLabel1
            // 
            this.xrLabel1.Dpi = 100F;
            this.xrLabel1.Font = new System.Drawing.Font("Arial", 7F, System.Drawing.FontStyle.Bold);
            this.xrLabel1.LocationFloat = new DevExpress.Utils.PointFloat(376.4311F, 32.52821F);
            this.xrLabel1.Name = "xrLabel1";
            this.xrLabel1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel1.SizeF = new System.Drawing.SizeF(49.60269F, 17.55346F);
            this.xrLabel1.StylePriority.UseFont = false;
            this.xrLabel1.StylePriority.UseTextAlignment = false;
            this.xrLabel1.Text = "Importe";
            this.xrLabel1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrLine4
            // 
            this.xrLine4.Dpi = 100F;
            this.xrLine4.LocationFloat = new DevExpress.Utils.PointFloat(0F, 50.13597F);
            this.xrLine4.Name = "xrLine4";
            this.xrLine4.SizeF = new System.Drawing.SizeF(1068.8F, 10F);
            // 
            // xrLabel17
            // 
            this.xrLabel17.Dpi = 100F;
            this.xrLabel17.Font = new System.Drawing.Font("Arial", 7F, System.Drawing.FontStyle.Bold);
            this.xrLabel17.LocationFloat = new DevExpress.Utils.PointFloat(6.896504F, 32.47392F);
            this.xrLabel17.Name = "xrLabel17";
            this.xrLabel17.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel17.SizeF = new System.Drawing.SizeF(49.40409F, 17.55346F);
            this.xrLabel17.StylePriority.UseFont = false;
            this.xrLabel17.StylePriority.UseTextAlignment = false;
            this.xrLabel17.Text = "No. ";
            this.xrLabel17.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrLabel18
            // 
            this.xrLabel18.Dpi = 100F;
            this.xrLabel18.Font = new System.Drawing.Font("Arial", 7F, System.Drawing.FontStyle.Bold);
            this.xrLabel18.LocationFloat = new DevExpress.Utils.PointFloat(56.30059F, 32.47392F);
            this.xrLabel18.Name = "xrLabel18";
            this.xrLabel18.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel18.SizeF = new System.Drawing.SizeF(83.11104F, 17.55346F);
            this.xrLabel18.StylePriority.UseFont = false;
            this.xrLabel18.StylePriority.UseTextAlignment = false;
            this.xrLabel18.Text = "Fecha";
            this.xrLabel18.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrLabel19
            // 
            this.xrLabel19.Dpi = 100F;
            this.xrLabel19.Font = new System.Drawing.Font("Arial", 7F, System.Drawing.FontStyle.Bold);
            this.xrLabel19.LocationFloat = new DevExpress.Utils.PointFloat(139.4116F, 32.47392F);
            this.xrLabel19.Name = "xrLabel19";
            this.xrLabel19.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel19.SizeF = new System.Drawing.SizeF(59.61139F, 17.60773F);
            this.xrLabel19.StylePriority.UseFont = false;
            this.xrLabel19.StylePriority.UseTextAlignment = false;
            this.xrLabel19.Text = "Cód Cliente";
            this.xrLabel19.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrLabel24
            // 
            this.xrLabel24.Dpi = 100F;
            this.xrLabel24.Font = new System.Drawing.Font("Arial", 7F, System.Drawing.FontStyle.Bold);
            this.xrLabel24.LocationFloat = new DevExpress.Utils.PointFloat(199.023F, 32.52821F);
            this.xrLabel24.Name = "xrLabel24";
            this.xrLabel24.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel24.SizeF = new System.Drawing.SizeF(113.222F, 17.55346F);
            this.xrLabel24.StylePriority.UseFont = false;
            this.xrLabel24.StylePriority.UseTextAlignment = false;
            this.xrLabel24.Text = "Cliente";
            this.xrLabel24.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrLabel25
            // 
            this.xrLabel25.Dpi = 100F;
            this.xrLabel25.Font = new System.Drawing.Font("Arial", 7F, System.Drawing.FontStyle.Bold);
            this.xrLabel25.LocationFloat = new DevExpress.Utils.PointFloat(312.245F, 32.52821F);
            this.xrLabel25.Name = "xrLabel25";
            this.xrLabel25.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel25.SizeF = new System.Drawing.SizeF(64.18604F, 17.55346F);
            this.xrLabel25.StylePriority.UseFont = false;
            this.xrLabel25.StylePriority.UseTextAlignment = false;
            this.xrLabel25.Text = "Vence";
            this.xrLabel25.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // TopMargin
            // 
            this.TopMargin.Dpi = 100F;
            this.TopMargin.HeightF = 30F;
            this.TopMargin.Name = "TopMargin";
            this.TopMargin.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.TopMargin.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // BottomMargin
            // 
            this.BottomMargin.Dpi = 100F;
            this.BottomMargin.HeightF = 55F;
            this.BottomMargin.Name = "BottomMargin";
            this.BottomMargin.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.BottomMargin.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // ReportHeader
            // 
            this.ReportHeader.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.labelDate1,
            this.labelDate2,
            this.xrLabel7,
            this.xrLabel5,
            this.labelDate,
            this.xrLabel9,
            this.xrLine1,
            this._labelReporte,
            this.xrLabel4,
            this.siteLabel,
            this.xrLabel3,
            this.xrPictureBox1});
            this.ReportHeader.Dpi = 100F;
            this.ReportHeader.HeightF = 117.0793F;
            this.ReportHeader.Name = "ReportHeader";
            // 
            // labelDate1
            // 
            this.labelDate1.Dpi = 100F;
            this.labelDate1.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold);
            this.labelDate1.LocationFloat = new DevExpress.Utils.PointFloat(36.88701F, 25F);
            this.labelDate1.Name = "labelDate1";
            this.labelDate1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.labelDate1.SizeF = new System.Drawing.SizeF(100F, 20.00001F);
            this.labelDate1.StylePriority.UseFont = false;
            this.labelDate1.StylePriority.UseTextAlignment = false;
            this.labelDate1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // labelDate2
            // 
            this.labelDate2.Dpi = 100F;
            this.labelDate2.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold);
            this.labelDate2.LocationFloat = new DevExpress.Utils.PointFloat(173.774F, 25F);
            this.labelDate2.Name = "labelDate2";
            this.labelDate2.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.labelDate2.SizeF = new System.Drawing.SizeF(100F, 19.99998F);
            this.labelDate2.StylePriority.UseFont = false;
            this.labelDate2.StylePriority.UseTextAlignment = false;
            this.labelDate2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrLabel7
            // 
            this.xrLabel7.Dpi = 100F;
            this.xrLabel7.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel7.LocationFloat = new DevExpress.Utils.PointFloat(136.887F, 25.00002F);
            this.xrLabel7.Name = "xrLabel7";
            this.xrLabel7.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel7.SizeF = new System.Drawing.SizeF(36.88701F, 20F);
            this.xrLabel7.StylePriority.UseFont = false;
            this.xrLabel7.StylePriority.UseTextAlignment = false;
            this.xrLabel7.Text = "Al:";
            this.xrLabel7.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLabel5
            // 
            this.xrLabel5.Dpi = 100F;
            this.xrLabel5.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel5.LocationFloat = new DevExpress.Utils.PointFloat(0F, 25F);
            this.xrLabel5.Name = "xrLabel5";
            this.xrLabel5.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel5.SizeF = new System.Drawing.SizeF(36.88701F, 20F);
            this.xrLabel5.StylePriority.UseFont = false;
            this.xrLabel5.StylePriority.UseTextAlignment = false;
            this.xrLabel5.Text = "Del:";
            this.xrLabel5.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // labelDate
            // 
            this.labelDate.Dpi = 100F;
            this.labelDate.Font = new System.Drawing.Font("Arial", 7F);
            this.labelDate.LocationFloat = new DevExpress.Utils.PointFloat(922F, 86.58339F);
            this.labelDate.Name = "labelDate";
            this.labelDate.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.labelDate.SizeF = new System.Drawing.SizeF(100F, 15F);
            this.labelDate.StylePriority.UseFont = false;
            // 
            // xrLabel9
            // 
            this.xrLabel9.Dpi = 100F;
            this.xrLabel9.Font = new System.Drawing.Font("Arial", 7F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel9.LocationFloat = new DevExpress.Utils.PointFloat(853.1276F, 86.58339F);
            this.xrLabel9.Name = "xrLabel9";
            this.xrLabel9.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel9.SizeF = new System.Drawing.SizeF(68.87257F, 15F);
            this.xrLabel9.StylePriority.UseFont = false;
            this.xrLabel9.Text = "Fecha:";
            // 
            // xrLine1
            // 
            this.xrLine1.Dpi = 100F;
            this.xrLine1.LocationFloat = new DevExpress.Utils.PointFloat(0F, 76.5834F);
            this.xrLine1.Name = "xrLine1";
            this.xrLine1.SizeF = new System.Drawing.SizeF(1068.8F, 9.999992F);
            this.xrLine1.StylePriority.UseBorderDashStyle = false;
            // 
            // _labelReporte
            // 
            this._labelReporte.Dpi = 100F;
            this._labelReporte.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._labelReporte.LocationFloat = new DevExpress.Utils.PointFloat(0F, 0F);
            this._labelReporte.Name = "_labelReporte";
            this._labelReporte.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this._labelReporte.SizeF = new System.Drawing.SizeF(406.2806F, 20F);
            this._labelReporte.StylePriority.UseFont = false;
            this._labelReporte.Text = "Reporte de Facturas";
            // 
            // xrLabel4
            // 
            this.xrLabel4.Dpi = 100F;
            this.xrLabel4.Font = new System.Drawing.Font("Arial", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel4.LocationFloat = new DevExpress.Utils.PointFloat(835.1743F, 30.00002F);
            this.xrLabel4.Name = "xrLabel4";
            this.xrLabel4.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel4.SizeF = new System.Drawing.SizeF(86.82587F, 15F);
            this.xrLabel4.StylePriority.UseFont = false;
            this.xrLabel4.StylePriority.UseTextAlignment = false;
            this.xrLabel4.Text = "DFL-950802-5N4";
            this.xrLabel4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // siteLabel
            // 
            this.siteLabel.Dpi = 100F;
            this.siteLabel.Font = new System.Drawing.Font("Arial", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.siteLabel.LocationFloat = new DevExpress.Utils.PointFloat(721.0001F, 15.00003F);
            this.siteLabel.Name = "siteLabel";
            this.siteLabel.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.siteLabel.SizeF = new System.Drawing.SizeF(201.0001F, 15F);
            this.siteLabel.StylePriority.UseFont = false;
            this.siteLabel.StylePriority.UseTextAlignment = false;
            this.siteLabel.Text = "*FLORIDO CEDIS";
            this.siteLabel.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // xrLabel3
            // 
            this.xrLabel3.Dpi = 100F;
            this.xrLabel3.Font = new System.Drawing.Font("Arial", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel3.LocationFloat = new DevExpress.Utils.PointFloat(717.8945F, 1.589457E-05F);
            this.xrLabel3.Name = "xrLabel3";
            this.xrLabel3.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel3.SizeF = new System.Drawing.SizeF(204.1053F, 15F);
            this.xrLabel3.StylePriority.UseFont = false;
            this.xrLabel3.StylePriority.UseTextAlignment = false;
            this.xrLabel3.Text = "DISTRIBUIDORA EL FLORIDO S.A. DE C.V.";
            this.xrLabel3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // xrPictureBox1
            // 
            this.xrPictureBox1.Dpi = 100F;
            this.xrPictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("xrPictureBox1.Image")));
            this.xrPictureBox1.LocationFloat = new DevExpress.Utils.PointFloat(926.7941F, 1.589457E-05F);
            this.xrPictureBox1.Name = "xrPictureBox1";
            this.xrPictureBox1.SizeF = new System.Drawing.SizeF(92.58575F, 61.66666F);
            this.xrPictureBox1.Sizing = DevExpress.XtraPrinting.ImageSizeMode.ZoomImage;
            // 
            // GroupFooter1
            // 
            this.GroupFooter1.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this._exeTotal,
            this._exeCash,
            this._exeCredit,
            this.xrLabel38,
            this.lbl_header_credito,
            this.xrLabel16,
            this.xrLabel20,
            this.xrLabel21,
            this.xrLabel22,
            this.xrLabel23,
            this.lbl_credit_zrate,
            this.lbl_credit_taxed,
            this.lbl_credit_tax,
            this.xrLabel26,
            this.lbl_cash_zrate,
            this.lbl_cash_taxed,
            this.lbl_cash_tax,
            this.xrLabel27,
            this.lbl_total_zrate,
            this.lbl_total_taxed,
            this.lbl_credit_ieps,
            this.xrLabel28,
            this.lbl_total_tax,
            this.lbl_total_ieps,
            this.lbl_cash_ieps,
            this.lbl_total_cash,
            this.lbl_total_global,
            this.lbl_total_credit,
            this.iepsLabel,
            this.xrLabel31,
            this.xrLabel32,
            this.xrLabel33,
            this.subtotalLabel,
            this.totalLabel,
            this.xrLabel29,
            this.ivaLabel});
            this.GroupFooter1.Dpi = 100F;
            this.GroupFooter1.HeightF = 229.1506F;
            this.GroupFooter1.Name = "GroupFooter1";
            // 
            // _exeTotal
            // 
            this._exeTotal.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this._exeTotal.Dpi = 100F;
            this._exeTotal.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._exeTotal.LocationFloat = new DevExpress.Utils.PointFloat(276.4702F, 101.8426F);
            this._exeTotal.Name = "_exeTotal";
            this._exeTotal.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this._exeTotal.SizeF = new System.Drawing.SizeF(120F, 20F);
            this._exeTotal.StylePriority.UseBorders = false;
            this._exeTotal.StylePriority.UseFont = false;
            this._exeTotal.StylePriority.UseTextAlignment = false;
            this._exeTotal.Text = "$999,999,999.99";
            this._exeTotal.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // _exeCash
            // 
            this._exeCash.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this._exeCash.Dpi = 100F;
            this._exeCash.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._exeCash.LocationFloat = new DevExpress.Utils.PointFloat(176.4702F, 101.8426F);
            this._exeCash.Name = "_exeCash";
            this._exeCash.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this._exeCash.SizeF = new System.Drawing.SizeF(100F, 20F);
            this._exeCash.StylePriority.UseBorders = false;
            this._exeCash.StylePriority.UseFont = false;
            this._exeCash.StylePriority.UseTextAlignment = false;
            this._exeCash.Text = "$999,999.99";
            this._exeCash.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // _exeCredit
            // 
            this._exeCredit.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this._exeCredit.Dpi = 100F;
            this._exeCredit.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._exeCredit.LocationFloat = new DevExpress.Utils.PointFloat(76.47015F, 101.8426F);
            this._exeCredit.Name = "_exeCredit";
            this._exeCredit.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this._exeCredit.SizeF = new System.Drawing.SizeF(100F, 20F);
            this._exeCredit.StylePriority.UseBorders = false;
            this._exeCredit.StylePriority.UseFont = false;
            this._exeCredit.StylePriority.UseTextAlignment = false;
            this._exeCredit.Text = "$999,999.99";
            this._exeCredit.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // xrLabel38
            // 
            this.xrLabel38.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel38.Dpi = 100F;
            this.xrLabel38.Font = new System.Drawing.Font("Arial", 8.25F, System.Drawing.FontStyle.Bold);
            this.xrLabel38.LocationFloat = new DevExpress.Utils.PointFloat(0F, 101.8426F);
            this.xrLabel38.Name = "xrLabel38";
            this.xrLabel38.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel38.SizeF = new System.Drawing.SizeF(76.47031F, 20F);
            this.xrLabel38.StylePriority.UseBorders = false;
            this.xrLabel38.StylePriority.UseFont = false;
            this.xrLabel38.StylePriority.UseTextAlignment = false;
            this.xrLabel38.Text = "Exento:";
            this.xrLabel38.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // lbl_header_credito
            // 
            this.lbl_header_credito.BackColor = System.Drawing.Color.White;
            this.lbl_header_credito.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Top | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.lbl_header_credito.Dpi = 100F;
            this.lbl_header_credito.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_header_credito.ForeColor = System.Drawing.Color.Black;
            this.lbl_header_credito.LocationFloat = new DevExpress.Utils.PointFloat(76.47031F, 41.84253F);
            this.lbl_header_credito.Name = "lbl_header_credito";
            this.lbl_header_credito.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lbl_header_credito.SizeF = new System.Drawing.SizeF(99.99999F, 20F);
            this.lbl_header_credito.StylePriority.UseBackColor = false;
            this.lbl_header_credito.StylePriority.UseBorders = false;
            this.lbl_header_credito.StylePriority.UseFont = false;
            this.lbl_header_credito.StylePriority.UseForeColor = false;
            this.lbl_header_credito.StylePriority.UseTextAlignment = false;
            this.lbl_header_credito.Text = "Crédito";
            this.lbl_header_credito.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            // 
            // xrLabel16
            // 
            this.xrLabel16.BackColor = System.Drawing.Color.DarkGray;
            this.xrLabel16.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel16.Dpi = 100F;
            this.xrLabel16.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel16.ForeColor = System.Drawing.Color.White;
            this.xrLabel16.LocationFloat = new DevExpress.Utils.PointFloat(0F, 41.84253F);
            this.xrLabel16.Name = "xrLabel16";
            this.xrLabel16.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel16.SizeF = new System.Drawing.SizeF(76.47031F, 20F);
            this.xrLabel16.StylePriority.UseBackColor = false;
            this.xrLabel16.StylePriority.UseBorders = false;
            this.xrLabel16.StylePriority.UseFont = false;
            this.xrLabel16.StylePriority.UseForeColor = false;
            // 
            // xrLabel20
            // 
            this.xrLabel20.BackColor = System.Drawing.Color.Transparent;
            this.xrLabel20.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Top | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel20.Dpi = 100F;
            this.xrLabel20.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel20.ForeColor = System.Drawing.Color.Black;
            this.xrLabel20.LocationFloat = new DevExpress.Utils.PointFloat(176.4703F, 41.84253F);
            this.xrLabel20.Name = "xrLabel20";
            this.xrLabel20.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel20.SizeF = new System.Drawing.SizeF(100F, 20F);
            this.xrLabel20.StylePriority.UseBackColor = false;
            this.xrLabel20.StylePriority.UseBorders = false;
            this.xrLabel20.StylePriority.UseFont = false;
            this.xrLabel20.StylePriority.UseForeColor = false;
            this.xrLabel20.StylePriority.UseTextAlignment = false;
            this.xrLabel20.Text = "Contado";
            this.xrLabel20.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            // 
            // xrLabel21
            // 
            this.xrLabel21.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel21.Dpi = 100F;
            this.xrLabel21.Font = new System.Drawing.Font("Arial", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel21.LocationFloat = new DevExpress.Utils.PointFloat(0F, 61.84247F);
            this.xrLabel21.Name = "xrLabel21";
            this.xrLabel21.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel21.SizeF = new System.Drawing.SizeF(76.47031F, 20F);
            this.xrLabel21.StylePriority.UseBorders = false;
            this.xrLabel21.StylePriority.UseFont = false;
            this.xrLabel21.StylePriority.UseTextAlignment = false;
            this.xrLabel21.Text = "Tasa Cero:";
            this.xrLabel21.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // xrLabel22
            // 
            this.xrLabel22.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel22.Dpi = 100F;
            this.xrLabel22.Font = new System.Drawing.Font("Arial", 8.25F, System.Drawing.FontStyle.Bold);
            this.xrLabel22.LocationFloat = new DevExpress.Utils.PointFloat(0F, 81.84255F);
            this.xrLabel22.Name = "xrLabel22";
            this.xrLabel22.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel22.SizeF = new System.Drawing.SizeF(76.47031F, 20F);
            this.xrLabel22.StylePriority.UseBorders = false;
            this.xrLabel22.StylePriority.UseFont = false;
            this.xrLabel22.StylePriority.UseTextAlignment = false;
            this.xrLabel22.Text = "Gravado:";
            this.xrLabel22.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // xrLabel23
            // 
            this.xrLabel23.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel23.Dpi = 100F;
            this.xrLabel23.Font = new System.Drawing.Font("Arial", 8.25F, System.Drawing.FontStyle.Bold);
            this.xrLabel23.LocationFloat = new DevExpress.Utils.PointFloat(0F, 121.8426F);
            this.xrLabel23.Name = "xrLabel23";
            this.xrLabel23.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel23.SizeF = new System.Drawing.SizeF(76.47031F, 20F);
            this.xrLabel23.StylePriority.UseBorders = false;
            this.xrLabel23.StylePriority.UseFont = false;
            this.xrLabel23.StylePriority.UseTextAlignment = false;
            this.xrLabel23.Text = "IVA:";
            this.xrLabel23.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // lbl_credit_zrate
            // 
            this.lbl_credit_zrate.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.lbl_credit_zrate.Dpi = 100F;
            this.lbl_credit_zrate.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_credit_zrate.LocationFloat = new DevExpress.Utils.PointFloat(76.47025F, 61.84247F);
            this.lbl_credit_zrate.Name = "lbl_credit_zrate";
            this.lbl_credit_zrate.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lbl_credit_zrate.SizeF = new System.Drawing.SizeF(100F, 20F);
            this.lbl_credit_zrate.StylePriority.UseBorders = false;
            this.lbl_credit_zrate.StylePriority.UseFont = false;
            this.lbl_credit_zrate.StylePriority.UseTextAlignment = false;
            this.lbl_credit_zrate.Text = "$999,999.99";
            this.lbl_credit_zrate.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // lbl_credit_taxed
            // 
            this.lbl_credit_taxed.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.lbl_credit_taxed.Dpi = 100F;
            this.lbl_credit_taxed.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_credit_taxed.LocationFloat = new DevExpress.Utils.PointFloat(76.47025F, 81.84255F);
            this.lbl_credit_taxed.Name = "lbl_credit_taxed";
            this.lbl_credit_taxed.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lbl_credit_taxed.SizeF = new System.Drawing.SizeF(100F, 20F);
            this.lbl_credit_taxed.StylePriority.UseBorders = false;
            this.lbl_credit_taxed.StylePriority.UseFont = false;
            this.lbl_credit_taxed.StylePriority.UseTextAlignment = false;
            this.lbl_credit_taxed.Text = "$999,999.99";
            this.lbl_credit_taxed.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // lbl_credit_tax
            // 
            this.lbl_credit_tax.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.lbl_credit_tax.Dpi = 100F;
            this.lbl_credit_tax.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_credit_tax.LocationFloat = new DevExpress.Utils.PointFloat(76.47031F, 121.8426F);
            this.lbl_credit_tax.Name = "lbl_credit_tax";
            this.lbl_credit_tax.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lbl_credit_tax.SizeF = new System.Drawing.SizeF(100F, 20F);
            this.lbl_credit_tax.StylePriority.UseBorders = false;
            this.lbl_credit_tax.StylePriority.UseFont = false;
            this.lbl_credit_tax.StylePriority.UseTextAlignment = false;
            this.lbl_credit_tax.Text = "$999,999.99";
            this.lbl_credit_tax.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // xrLabel26
            // 
            this.xrLabel26.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel26.Dpi = 100F;
            this.xrLabel26.Font = new System.Drawing.Font("Arial", 8.25F, System.Drawing.FontStyle.Bold);
            this.xrLabel26.LocationFloat = new DevExpress.Utils.PointFloat(6.357829E-05F, 141.8426F);
            this.xrLabel26.Name = "xrLabel26";
            this.xrLabel26.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel26.SizeF = new System.Drawing.SizeF(76.47025F, 20F);
            this.xrLabel26.StylePriority.UseBorders = false;
            this.xrLabel26.StylePriority.UseFont = false;
            this.xrLabel26.StylePriority.UseTextAlignment = false;
            this.xrLabel26.Text = "IEPS:";
            this.xrLabel26.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // lbl_cash_zrate
            // 
            this.lbl_cash_zrate.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.lbl_cash_zrate.Dpi = 100F;
            this.lbl_cash_zrate.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_cash_zrate.LocationFloat = new DevExpress.Utils.PointFloat(176.4703F, 61.84247F);
            this.lbl_cash_zrate.Name = "lbl_cash_zrate";
            this.lbl_cash_zrate.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lbl_cash_zrate.SizeF = new System.Drawing.SizeF(100F, 20F);
            this.lbl_cash_zrate.StylePriority.UseBorders = false;
            this.lbl_cash_zrate.StylePriority.UseFont = false;
            this.lbl_cash_zrate.StylePriority.UseTextAlignment = false;
            this.lbl_cash_zrate.Text = "$999,999.99";
            this.lbl_cash_zrate.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // lbl_cash_taxed
            // 
            this.lbl_cash_taxed.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.lbl_cash_taxed.Dpi = 100F;
            this.lbl_cash_taxed.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_cash_taxed.LocationFloat = new DevExpress.Utils.PointFloat(176.4703F, 81.84255F);
            this.lbl_cash_taxed.Name = "lbl_cash_taxed";
            this.lbl_cash_taxed.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lbl_cash_taxed.SizeF = new System.Drawing.SizeF(100F, 20F);
            this.lbl_cash_taxed.StylePriority.UseBorders = false;
            this.lbl_cash_taxed.StylePriority.UseFont = false;
            this.lbl_cash_taxed.StylePriority.UseTextAlignment = false;
            this.lbl_cash_taxed.Text = "$999,999.99";
            this.lbl_cash_taxed.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // lbl_cash_tax
            // 
            this.lbl_cash_tax.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.lbl_cash_tax.Dpi = 100F;
            this.lbl_cash_tax.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_cash_tax.LocationFloat = new DevExpress.Utils.PointFloat(176.4703F, 121.8426F);
            this.lbl_cash_tax.Name = "lbl_cash_tax";
            this.lbl_cash_tax.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lbl_cash_tax.SizeF = new System.Drawing.SizeF(100F, 20F);
            this.lbl_cash_tax.StylePriority.UseBorders = false;
            this.lbl_cash_tax.StylePriority.UseFont = false;
            this.lbl_cash_tax.StylePriority.UseTextAlignment = false;
            this.lbl_cash_tax.Text = "$999,999.99";
            this.lbl_cash_tax.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // xrLabel27
            // 
            this.xrLabel27.BackColor = System.Drawing.Color.Transparent;
            this.xrLabel27.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Top | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel27.Dpi = 100F;
            this.xrLabel27.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel27.ForeColor = System.Drawing.Color.Black;
            this.xrLabel27.LocationFloat = new DevExpress.Utils.PointFloat(276.4703F, 41.84253F);
            this.xrLabel27.Name = "xrLabel27";
            this.xrLabel27.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel27.SizeF = new System.Drawing.SizeF(120F, 20F);
            this.xrLabel27.StylePriority.UseBackColor = false;
            this.xrLabel27.StylePriority.UseBorders = false;
            this.xrLabel27.StylePriority.UseFont = false;
            this.xrLabel27.StylePriority.UseForeColor = false;
            this.xrLabel27.StylePriority.UseTextAlignment = false;
            this.xrLabel27.Text = "Totales";
            this.xrLabel27.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            // 
            // lbl_total_zrate
            // 
            this.lbl_total_zrate.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.lbl_total_zrate.Dpi = 100F;
            this.lbl_total_zrate.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_total_zrate.LocationFloat = new DevExpress.Utils.PointFloat(276.4703F, 61.84247F);
            this.lbl_total_zrate.Name = "lbl_total_zrate";
            this.lbl_total_zrate.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lbl_total_zrate.SizeF = new System.Drawing.SizeF(120F, 20F);
            this.lbl_total_zrate.StylePriority.UseBorders = false;
            this.lbl_total_zrate.StylePriority.UseFont = false;
            this.lbl_total_zrate.StylePriority.UseTextAlignment = false;
            this.lbl_total_zrate.Text = "$999,999,999.99";
            this.lbl_total_zrate.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // lbl_total_taxed
            // 
            this.lbl_total_taxed.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.lbl_total_taxed.Dpi = 100F;
            this.lbl_total_taxed.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_total_taxed.LocationFloat = new DevExpress.Utils.PointFloat(276.4703F, 81.84255F);
            this.lbl_total_taxed.Name = "lbl_total_taxed";
            this.lbl_total_taxed.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lbl_total_taxed.SizeF = new System.Drawing.SizeF(120F, 20F);
            this.lbl_total_taxed.StylePriority.UseBorders = false;
            this.lbl_total_taxed.StylePriority.UseFont = false;
            this.lbl_total_taxed.StylePriority.UseTextAlignment = false;
            this.lbl_total_taxed.Text = "$999,999,999.99";
            this.lbl_total_taxed.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // lbl_credit_ieps
            // 
            this.lbl_credit_ieps.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.lbl_credit_ieps.Dpi = 100F;
            this.lbl_credit_ieps.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_credit_ieps.LocationFloat = new DevExpress.Utils.PointFloat(76.47031F, 141.8426F);
            this.lbl_credit_ieps.Name = "lbl_credit_ieps";
            this.lbl_credit_ieps.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lbl_credit_ieps.SizeF = new System.Drawing.SizeF(99.99999F, 20F);
            this.lbl_credit_ieps.StylePriority.UseBorders = false;
            this.lbl_credit_ieps.StylePriority.UseFont = false;
            this.lbl_credit_ieps.StylePriority.UseTextAlignment = false;
            this.lbl_credit_ieps.Text = "$999,999.99";
            this.lbl_credit_ieps.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // xrLabel28
            // 
            this.xrLabel28.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel28.Dpi = 100F;
            this.xrLabel28.Font = new System.Drawing.Font("Arial", 8.25F, System.Drawing.FontStyle.Bold);
            this.xrLabel28.LocationFloat = new DevExpress.Utils.PointFloat(6.357829E-05F, 161.8427F);
            this.xrLabel28.Name = "xrLabel28";
            this.xrLabel28.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel28.SizeF = new System.Drawing.SizeF(76.47025F, 20F);
            this.xrLabel28.StylePriority.UseBorders = false;
            this.xrLabel28.StylePriority.UseFont = false;
            this.xrLabel28.StylePriority.UseTextAlignment = false;
            this.xrLabel28.Text = "Totales:";
            this.xrLabel28.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // lbl_total_tax
            // 
            this.lbl_total_tax.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.lbl_total_tax.Dpi = 100F;
            this.lbl_total_tax.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_total_tax.LocationFloat = new DevExpress.Utils.PointFloat(276.4702F, 121.8426F);
            this.lbl_total_tax.Name = "lbl_total_tax";
            this.lbl_total_tax.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lbl_total_tax.SizeF = new System.Drawing.SizeF(120.0001F, 20F);
            this.lbl_total_tax.StylePriority.UseBorders = false;
            this.lbl_total_tax.StylePriority.UseFont = false;
            this.lbl_total_tax.StylePriority.UseTextAlignment = false;
            this.lbl_total_tax.Text = "$999,999,999.99";
            this.lbl_total_tax.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // lbl_total_ieps
            // 
            this.lbl_total_ieps.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.lbl_total_ieps.Dpi = 100F;
            this.lbl_total_ieps.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_total_ieps.LocationFloat = new DevExpress.Utils.PointFloat(276.4702F, 141.8426F);
            this.lbl_total_ieps.Name = "lbl_total_ieps";
            this.lbl_total_ieps.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lbl_total_ieps.SizeF = new System.Drawing.SizeF(120.0001F, 20F);
            this.lbl_total_ieps.StylePriority.UseBorders = false;
            this.lbl_total_ieps.StylePriority.UseFont = false;
            this.lbl_total_ieps.StylePriority.UseTextAlignment = false;
            this.lbl_total_ieps.Text = "$999,999,999.99";
            this.lbl_total_ieps.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // lbl_cash_ieps
            // 
            this.lbl_cash_ieps.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.lbl_cash_ieps.Dpi = 100F;
            this.lbl_cash_ieps.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_cash_ieps.LocationFloat = new DevExpress.Utils.PointFloat(176.4703F, 141.8426F);
            this.lbl_cash_ieps.Name = "lbl_cash_ieps";
            this.lbl_cash_ieps.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lbl_cash_ieps.SizeF = new System.Drawing.SizeF(100F, 20F);
            this.lbl_cash_ieps.StylePriority.UseBorders = false;
            this.lbl_cash_ieps.StylePriority.UseFont = false;
            this.lbl_cash_ieps.StylePriority.UseTextAlignment = false;
            this.lbl_cash_ieps.Text = "$999,999.99";
            this.lbl_cash_ieps.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // lbl_total_cash
            // 
            this.lbl_total_cash.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.lbl_total_cash.Dpi = 100F;
            this.lbl_total_cash.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_total_cash.LocationFloat = new DevExpress.Utils.PointFloat(176.4704F, 161.8426F);
            this.lbl_total_cash.Name = "lbl_total_cash";
            this.lbl_total_cash.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lbl_total_cash.SizeF = new System.Drawing.SizeF(99.99994F, 20F);
            this.lbl_total_cash.StylePriority.UseBorders = false;
            this.lbl_total_cash.StylePriority.UseFont = false;
            this.lbl_total_cash.StylePriority.UseTextAlignment = false;
            this.lbl_total_cash.Text = "$999,999.99";
            this.lbl_total_cash.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // lbl_total_global
            // 
            this.lbl_total_global.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.lbl_total_global.Dpi = 100F;
            this.lbl_total_global.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_total_global.LocationFloat = new DevExpress.Utils.PointFloat(276.4702F, 161.8426F);
            this.lbl_total_global.Name = "lbl_total_global";
            this.lbl_total_global.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lbl_total_global.SizeF = new System.Drawing.SizeF(120.0001F, 20F);
            this.lbl_total_global.StylePriority.UseBorders = false;
            this.lbl_total_global.StylePriority.UseFont = false;
            this.lbl_total_global.StylePriority.UseTextAlignment = false;
            this.lbl_total_global.Text = "$999,999,999.99";
            this.lbl_total_global.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // lbl_total_credit
            // 
            this.lbl_total_credit.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.lbl_total_credit.Dpi = 100F;
            this.lbl_total_credit.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_total_credit.LocationFloat = new DevExpress.Utils.PointFloat(76.47038F, 161.8426F);
            this.lbl_total_credit.Name = "lbl_total_credit";
            this.lbl_total_credit.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lbl_total_credit.SizeF = new System.Drawing.SizeF(99.99993F, 20F);
            this.lbl_total_credit.StylePriority.UseBorders = false;
            this.lbl_total_credit.StylePriority.UseFont = false;
            this.lbl_total_credit.StylePriority.UseTextAlignment = false;
            this.lbl_total_credit.Text = "$999,999.99";
            this.lbl_total_credit.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // iepsLabel
            // 
            this.iepsLabel.Dpi = 100F;
            this.iepsLabel.Font = new System.Drawing.Font("Arial", 8F);
            this.iepsLabel.LocationFloat = new DevExpress.Utils.PointFloat(903.8048F, 71.84258F);
            this.iepsLabel.Name = "iepsLabel";
            this.iepsLabel.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.iepsLabel.SizeF = new System.Drawing.SizeF(118.1951F, 15F);
            this.iepsLabel.StylePriority.UseFont = false;
            this.iepsLabel.StylePriority.UseTextAlignment = false;
            this.iepsLabel.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // xrLabel31
            // 
            this.xrLabel31.Dpi = 100F;
            this.xrLabel31.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Bold);
            this.xrLabel31.LocationFloat = new DevExpress.Utils.PointFloat(875.3879F, 56.8426F);
            this.xrLabel31.Name = "xrLabel31";
            this.xrLabel31.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel31.SizeF = new System.Drawing.SizeF(27F, 15F);
            this.xrLabel31.StylePriority.UseFont = false;
            this.xrLabel31.Text = "IVA:";
            // 
            // xrLabel32
            // 
            this.xrLabel32.Dpi = 100F;
            this.xrLabel32.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Bold);
            this.xrLabel32.LocationFloat = new DevExpress.Utils.PointFloat(868.3881F, 71.84258F);
            this.xrLabel32.Name = "xrLabel32";
            this.xrLabel32.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel32.SizeF = new System.Drawing.SizeF(34F, 15F);
            this.xrLabel32.StylePriority.UseFont = false;
            this.xrLabel32.Text = "IEPS:";
            // 
            // xrLabel33
            // 
            this.xrLabel33.Dpi = 100F;
            this.xrLabel33.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold);
            this.xrLabel33.LocationFloat = new DevExpress.Utils.PointFloat(849.388F, 86.84254F);
            this.xrLabel33.Name = "xrLabel33";
            this.xrLabel33.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel33.SizeF = new System.Drawing.SizeF(54.41675F, 15F);
            this.xrLabel33.StylePriority.UseFont = false;
            this.xrLabel33.StylePriority.UseTextAlignment = false;
            this.xrLabel33.Text = "Total:";
            this.xrLabel33.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
            // 
            // subtotalLabel
            // 
            this.subtotalLabel.Dpi = 100F;
            this.subtotalLabel.Font = new System.Drawing.Font("Arial", 8F);
            this.subtotalLabel.LocationFloat = new DevExpress.Utils.PointFloat(903.8047F, 41.84253F);
            this.subtotalLabel.Name = "subtotalLabel";
            this.subtotalLabel.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.subtotalLabel.SizeF = new System.Drawing.SizeF(118.1951F, 15F);
            this.subtotalLabel.StylePriority.UseFont = false;
            this.subtotalLabel.StylePriority.UseTextAlignment = false;
            this.subtotalLabel.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // totalLabel
            // 
            this.totalLabel.Dpi = 100F;
            this.totalLabel.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold);
            this.totalLabel.LocationFloat = new DevExpress.Utils.PointFloat(903.8048F, 86.8426F);
            this.totalLabel.Name = "totalLabel";
            this.totalLabel.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.totalLabel.SizeF = new System.Drawing.SizeF(118.1951F, 15F);
            this.totalLabel.StylePriority.UseFont = false;
            this.totalLabel.StylePriority.UseTextAlignment = false;
            this.totalLabel.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // xrLabel29
            // 
            this.xrLabel29.Dpi = 100F;
            this.xrLabel29.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Bold);
            this.xrLabel29.LocationFloat = new DevExpress.Utils.PointFloat(849.388F, 41.84248F);
            this.xrLabel29.Name = "xrLabel29";
            this.xrLabel29.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel29.SizeF = new System.Drawing.SizeF(53F, 15F);
            this.xrLabel29.StylePriority.UseFont = false;
            this.xrLabel29.Text = "Subtotal:";
            // 
            // ivaLabel
            // 
            this.ivaLabel.Dpi = 100F;
            this.ivaLabel.Font = new System.Drawing.Font("Arial", 8F);
            this.ivaLabel.LocationFloat = new DevExpress.Utils.PointFloat(903.8047F, 56.84268F);
            this.ivaLabel.Name = "ivaLabel";
            this.ivaLabel.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.ivaLabel.SizeF = new System.Drawing.SizeF(118.1953F, 15F);
            this.ivaLabel.StylePriority.UseFont = false;
            this.ivaLabel.StylePriority.UseTextAlignment = false;
            this.ivaLabel.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // PageFooter
            // 
            this.PageFooter.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrPageInfo1});
            this.PageFooter.Dpi = 100F;
            this.PageFooter.HeightF = 15.625F;
            this.PageFooter.Name = "PageFooter";
            // 
            // xrPageInfo1
            // 
            this.xrPageInfo1.Dpi = 100F;
            this.xrPageInfo1.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Bold);
            this.xrPageInfo1.Format = "Página {0} de {1}";
            this.xrPageInfo1.LocationFloat = new DevExpress.Utils.PointFloat(0F, 0F);
            this.xrPageInfo1.Name = "xrPageInfo1";
            this.xrPageInfo1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrPageInfo1.SizeF = new System.Drawing.SizeF(100F, 15F);
            this.xrPageInfo1.StylePriority.UseFont = false;
            // 
            // DetailReport
            // 
            this.DetailReport.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.Detail1});
            this.DetailReport.Dpi = 100F;
            this.DetailReport.Level = 0;
            this.DetailReport.Name = "DetailReport";
            // 
            // Detail1
            // 
            this.Detail1.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable1});
            this.Detail1.Dpi = 100F;
            this.Detail1.HeightF = 20F;
            this.Detail1.Name = "Detail1";
            // 
            // xrTable1
            // 
            this.xrTable1.BackColor = System.Drawing.Color.White;
            this.xrTable1.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrTable1.Dpi = 100F;
            this.xrTable1.Font = new System.Drawing.Font("Arial", 6F);
            this.xrTable1.LocationFloat = new DevExpress.Utils.PointFloat(6.8965F, 0F);
            this.xrTable1.Name = "xrTable1";
            this.xrTable1.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow1});
            this.xrTable1.SizeF = new System.Drawing.SizeF(1061.903F, 20F);
            this.xrTable1.StylePriority.UseBackColor = false;
            this.xrTable1.StylePriority.UseBorders = false;
            this.xrTable1.StylePriority.UseFont = false;
            // 
            // xrTableRow1
            // 
            this.xrTableRow1.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.InvoiceNumber,
            this.CreateDate,
            this.CustomerCode,
            this.CustomerName,
            this.DueDate,
            this.Amount,
            this.Ieps,
            this.IepsPredicate,
            this.Tax,
            this.TotalAmount,
            this.TotalCredit,
            this.TotalCash,
            this.PaymentMethod,
            this.Balance,
            this.CurrencyCode,
            this.FiscalNumber,
            this.Status});
            this.xrTableRow1.Dpi = 100F;
            this.xrTableRow1.Name = "xrTableRow1";
            this.xrTableRow1.Weight = 1D;
            // 
            // InvoiceNumber
            // 
            this.InvoiceNumber.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.InvoiceNumber.Dpi = 100F;
            this.InvoiceNumber.Font = new System.Drawing.Font("Arial", 6F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.InvoiceNumber.Name = "InvoiceNumber";
            this.InvoiceNumber.StylePriority.UseBorders = false;
            this.InvoiceNumber.StylePriority.UseFont = false;
            this.InvoiceNumber.StylePriority.UseTextAlignment = false;
            this.InvoiceNumber.Text = "InvoiceNumber";
            this.InvoiceNumber.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.InvoiceNumber.Weight = 0.2708920665990211D;
            // 
            // CreateDate
            // 
            this.CreateDate.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.CreateDate.Dpi = 100F;
            this.CreateDate.Font = new System.Drawing.Font("Arial", 6F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.CreateDate.Name = "CreateDate";
            this.CreateDate.StylePriority.UseBorders = false;
            this.CreateDate.StylePriority.UseFont = false;
            this.CreateDate.StylePriority.UseTextAlignment = false;
            this.CreateDate.Text = "CreateDate";
            this.CreateDate.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.CreateDate.Weight = 0.45571364724736446D;
            // 
            // CustomerCode
            // 
            this.CustomerCode.Dpi = 100F;
            this.CustomerCode.Font = new System.Drawing.Font("Arial", 6F);
            this.CustomerCode.Name = "CustomerCode";
            this.CustomerCode.StylePriority.UseFont = false;
            this.CustomerCode.StylePriority.UseTextAlignment = false;
            this.CustomerCode.Text = "CustomerCode";
            this.CustomerCode.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.CustomerCode.Weight = 0.32686082392481541D;
            // 
            // CustomerName
            // 
            this.CustomerName.Dpi = 100F;
            this.CustomerName.Font = new System.Drawing.Font("Arial", 6F);
            this.CustomerName.Name = "CustomerName";
            this.CustomerName.StylePriority.UseFont = false;
            this.CustomerName.StylePriority.UseTextAlignment = false;
            this.CustomerName.Text = "CustomerName";
            this.CustomerName.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.CustomerName.Weight = 0.62081785193508288D;
            // 
            // DueDate
            // 
            this.DueDate.Dpi = 100F;
            this.DueDate.Font = new System.Drawing.Font("Arial", 6F);
            this.DueDate.Name = "DueDate";
            this.DueDate.StylePriority.UseFont = false;
            this.DueDate.StylePriority.UseTextAlignment = false;
            this.DueDate.Text = "DueDate";
            this.DueDate.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.DueDate.Weight = 0.35194445800213181D;
            // 
            // Amount
            // 
            this.Amount.Dpi = 100F;
            this.Amount.Font = new System.Drawing.Font("Arial", 6F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Amount.Name = "Amount";
            this.Amount.StylePriority.UseFont = false;
            this.Amount.StylePriority.UseTextAlignment = false;
            this.Amount.Text = "Amount";
            this.Amount.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.Amount.Weight = 0.27198087503448232D;
            // 
            // Ieps
            // 
            this.Ieps.Dpi = 100F;
            this.Ieps.Font = new System.Drawing.Font("Arial", 6F);
            this.Ieps.Name = "Ieps";
            this.Ieps.StylePriority.UseFont = false;
            this.Ieps.StylePriority.UseTextAlignment = false;
            this.Ieps.Text = "Ieps";
            this.Ieps.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.Ieps.Weight = 0.20664810988261378D;
            // 
            // IepsPredicate
            // 
            this.IepsPredicate.Dpi = 100F;
            this.IepsPredicate.Font = new System.Drawing.Font("Arial", 6F);
            this.IepsPredicate.Multiline = true;
            this.IepsPredicate.Name = "IepsPredicate";
            this.IepsPredicate.StylePriority.UseFont = false;
            this.IepsPredicate.StylePriority.UseTextAlignment = false;
            this.IepsPredicate.Text = "IepsPredicate";
            this.IepsPredicate.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.IepsPredicate.Weight = 0.57148397141923335D;
            // 
            // Tax
            // 
            this.Tax.Dpi = 100F;
            this.Tax.Font = new System.Drawing.Font("Arial", 6F);
            this.Tax.Name = "Tax";
            this.Tax.StylePriority.UseFont = false;
            this.Tax.StylePriority.UseTextAlignment = false;
            this.Tax.Text = "Tax";
            this.Tax.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.Tax.Weight = 0.29950391170120544D;
            // 
            // TotalAmount
            // 
            this.TotalAmount.Dpi = 100F;
            this.TotalAmount.Font = new System.Drawing.Font("Arial", 6F);
            this.TotalAmount.Name = "TotalAmount";
            this.TotalAmount.StylePriority.UseFont = false;
            this.TotalAmount.StylePriority.UseTextAlignment = false;
            this.TotalAmount.Text = "TotalAmount";
            this.TotalAmount.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.TotalAmount.Weight = 0.29328612431405043D;
            // 
            // TotalCredit
            // 
            this.TotalCredit.Dpi = 100F;
            this.TotalCredit.Font = new System.Drawing.Font("Arial", 6F);
            this.TotalCredit.Name = "TotalCredit";
            this.TotalCredit.StylePriority.UseFont = false;
            this.TotalCredit.StylePriority.UseTextAlignment = false;
            this.TotalCredit.Text = "TotalCredit";
            this.TotalCredit.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.TotalCredit.Weight = 0.29826126966087946D;
            // 
            // TotalCash
            // 
            this.TotalCash.Dpi = 100F;
            this.TotalCash.Font = new System.Drawing.Font("Arial", 6F);
            this.TotalCash.Name = "TotalCash";
            this.TotalCash.StylePriority.UseFont = false;
            this.TotalCash.StylePriority.UseTextAlignment = false;
            this.TotalCash.Text = "TotalCash";
            this.TotalCash.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.TotalCash.Weight = 0.29826097682686353D;
            // 
            // PaymentMethod
            // 
            this.PaymentMethod.Dpi = 100F;
            this.PaymentMethod.Font = new System.Drawing.Font("Arial", 6F);
            this.PaymentMethod.Name = "PaymentMethod";
            this.PaymentMethod.StylePriority.UseFont = false;
            this.PaymentMethod.StylePriority.UseTextAlignment = false;
            this.PaymentMethod.Text = "PaymentMethod";
            this.PaymentMethod.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.PaymentMethod.Weight = 0.27664549083210294D;
            // 
            // Balance
            // 
            this.Balance.Dpi = 100F;
            this.Balance.Font = new System.Drawing.Font("Arial", 6F);
            this.Balance.Name = "Balance";
            this.Balance.StylePriority.UseFont = false;
            this.Balance.StylePriority.UseTextAlignment = false;
            this.Balance.Text = "Balance";
            this.Balance.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.Balance.Weight = 0.35194425101222981D;
            // 
            // CurrencyCode
            // 
            this.CurrencyCode.Dpi = 100F;
            this.CurrencyCode.Font = new System.Drawing.Font("Arial", 6F);
            this.CurrencyCode.Name = "CurrencyCode";
            this.CurrencyCode.StylePriority.UseFont = false;
            this.CurrencyCode.StylePriority.UseTextAlignment = false;
            this.CurrencyCode.Text = "CurrencyCode";
            this.CurrencyCode.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.CurrencyCode.Weight = 0.30945763589536496D;
            // 
            // FiscalNumber
            // 
            this.FiscalNumber.Dpi = 100F;
            this.FiscalNumber.Font = new System.Drawing.Font("Arial", 6F);
            this.FiscalNumber.Multiline = true;
            this.FiscalNumber.Name = "FiscalNumber";
            this.FiscalNumber.StylePriority.UseFont = false;
            this.FiscalNumber.StylePriority.UseTextAlignment = false;
            this.FiscalNumber.Text = "FiscalNumber";
            this.FiscalNumber.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.FiscalNumber.Weight = 0.30945830523025841D;
            // 
            // Status
            // 
            this.Status.Dpi = 100F;
            this.Status.Font = new System.Drawing.Font("Arial", 6F);
            this.Status.Name = "Status";
            this.Status.StylePriority.UseFont = false;
            this.Status.StylePriority.UseTextAlignment = false;
            this.Status.Text = "Status";
            this.Status.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.Status.Weight = 0.30945743991629304D;
            // 
            // InvoicesReport
            // 
            this.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.Detail,
            this.TopMargin,
            this.BottomMargin,
            this.ReportHeader,
            this.GroupFooter1,
            this.PageFooter,
            this.DetailReport});
            this.Landscape = true;
            this.Margins = new System.Drawing.Printing.Margins(18, 10, 30, 55);
            this.PageHeight = 850;
            this.PageWidth = 1100;
            this.Version = "16.2";
            ((System.ComponentModel.ISupportInitialize)(this.xrTable1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

        }

        #endregion

        private DevExpress.XtraReports.UI.DetailBand Detail;
        private DevExpress.XtraReports.UI.TopMarginBand TopMargin;
        private DevExpress.XtraReports.UI.BottomMarginBand BottomMargin;
        private DevExpress.XtraReports.UI.ReportHeaderBand ReportHeader;
        private DevExpress.XtraReports.UI.GroupFooterBand GroupFooter1;
        private DevExpress.XtraReports.UI.PageFooterBand PageFooter;
        private DevExpress.XtraReports.UI.XRPageInfo xrPageInfo1;
        private DevExpress.XtraReports.UI.DetailReportBand DetailReport;
        private DevExpress.XtraReports.UI.DetailBand Detail1;
        private DevExpress.XtraReports.UI.XRTable xrTable1;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow1;
        private DevExpress.XtraReports.UI.XRTableCell CreateDate;
        private DevExpress.XtraReports.UI.XRTableCell CustomerCode;
        private DevExpress.XtraReports.UI.XRTableCell CustomerName;
        private DevExpress.XtraReports.UI.XRTableCell DueDate;
        private DevExpress.XtraReports.UI.XRLabel labelDate;
        private DevExpress.XtraReports.UI.XRLabel xrLabel9;
        private DevExpress.XtraReports.UI.XRLine xrLine1;
        private DevExpress.XtraReports.UI.XRLabel _labelReporte;
        private DevExpress.XtraReports.UI.XRLabel xrLabel4;
        private DevExpress.XtraReports.UI.XRLabel siteLabel;
        private DevExpress.XtraReports.UI.XRLabel xrLabel3;
        private DevExpress.XtraReports.UI.XRPictureBox xrPictureBox1;
        private DevExpress.XtraReports.UI.XRLabel xrLabel17;
        private DevExpress.XtraReports.UI.XRLabel xrLabel18;
        private DevExpress.XtraReports.UI.XRLabel xrLabel19;
        private DevExpress.XtraReports.UI.XRLabel xrLabel24;
        private DevExpress.XtraReports.UI.XRLabel xrLabel25;
        private DevExpress.XtraReports.UI.XRLabel iepsLabel;
        private DevExpress.XtraReports.UI.XRLabel xrLabel31;
        private DevExpress.XtraReports.UI.XRLabel xrLabel32;
        private DevExpress.XtraReports.UI.XRLabel xrLabel33;
        private DevExpress.XtraReports.UI.XRLabel subtotalLabel;
        private DevExpress.XtraReports.UI.XRLabel totalLabel;
        private DevExpress.XtraReports.UI.XRLabel xrLabel29;
        private DevExpress.XtraReports.UI.XRLabel ivaLabel;
        private DevExpress.XtraReports.UI.XRLine xrLine4;
        private DevExpress.XtraReports.UI.XRLabel xrLabel1;
        private DevExpress.XtraReports.UI.XRTableCell Amount;
        private DevExpress.XtraReports.UI.XRLabel xrLabel6;
        private DevExpress.XtraReports.UI.XRTableCell Ieps;
        private DevExpress.XtraReports.UI.XRLabel xrLabel2;
        private DevExpress.XtraReports.UI.XRTableCell InvoiceNumber;
        private DevExpress.XtraReports.UI.XRTableCell IepsPredicate;
        private DevExpress.XtraReports.UI.XRLabel labelDate1;
        private DevExpress.XtraReports.UI.XRLabel labelDate2;
        private DevExpress.XtraReports.UI.XRLabel xrLabel7;
        private DevExpress.XtraReports.UI.XRLabel xrLabel5;
        private DevExpress.XtraReports.UI.XRLabel xrLabel15;
        private DevExpress.XtraReports.UI.XRLabel xrLabel8;
        private DevExpress.XtraReports.UI.XRLabel xrLabel10;
        private DevExpress.XtraReports.UI.XRLabel xrLabel11;
        private DevExpress.XtraReports.UI.XRLabel xrLabel12;
        private DevExpress.XtraReports.UI.XRLabel xrLabel13;
        private DevExpress.XtraReports.UI.XRLabel xrLabel14;
        private DevExpress.XtraReports.UI.XRTableCell Tax;
        private DevExpress.XtraReports.UI.XRTableCell TotalAmount;
        private DevExpress.XtraReports.UI.XRTableCell PaymentMethod;
        private DevExpress.XtraReports.UI.XRTableCell Balance;
        private DevExpress.XtraReports.UI.XRTableCell CurrencyCode;
        private DevExpress.XtraReports.UI.XRTableCell FiscalNumber;
        private DevExpress.XtraReports.UI.XRTableCell Status;
        private DevExpress.XtraReports.UI.XRLabel lbl_header_credito;
        private DevExpress.XtraReports.UI.XRLabel xrLabel16;
        private DevExpress.XtraReports.UI.XRLabel xrLabel20;
        private DevExpress.XtraReports.UI.XRLabel xrLabel21;
        private DevExpress.XtraReports.UI.XRLabel xrLabel22;
        private DevExpress.XtraReports.UI.XRLabel xrLabel23;
        private DevExpress.XtraReports.UI.XRLabel lbl_credit_zrate;
        private DevExpress.XtraReports.UI.XRLabel lbl_credit_taxed;
        private DevExpress.XtraReports.UI.XRLabel lbl_credit_tax;
        private DevExpress.XtraReports.UI.XRLabel xrLabel26;
        private DevExpress.XtraReports.UI.XRLabel lbl_cash_zrate;
        private DevExpress.XtraReports.UI.XRLabel lbl_cash_taxed;
        private DevExpress.XtraReports.UI.XRLabel lbl_cash_tax;
        private DevExpress.XtraReports.UI.XRLabel xrLabel27;
        private DevExpress.XtraReports.UI.XRLabel lbl_total_zrate;
        private DevExpress.XtraReports.UI.XRLabel lbl_total_taxed;
        private DevExpress.XtraReports.UI.XRLabel lbl_credit_ieps;
        private DevExpress.XtraReports.UI.XRLabel xrLabel28;
        private DevExpress.XtraReports.UI.XRLabel lbl_total_tax;
        private DevExpress.XtraReports.UI.XRLabel lbl_total_ieps;
        private DevExpress.XtraReports.UI.XRLabel lbl_cash_ieps;
        private DevExpress.XtraReports.UI.XRLabel lbl_total_cash;
        private DevExpress.XtraReports.UI.XRLabel lbl_total_global;
        private DevExpress.XtraReports.UI.XRLabel lbl_total_credit;
        private DevExpress.XtraReports.UI.XRLabel xrLabel34;
        private DevExpress.XtraReports.UI.XRLabel xrLabel30;
        private DevExpress.XtraReports.UI.XRTableCell TotalCredit;
        private DevExpress.XtraReports.UI.XRTableCell TotalCash;
        private DevExpress.XtraReports.UI.XRLabel _exeTotal;
        private DevExpress.XtraReports.UI.XRLabel _exeCash;
        private DevExpress.XtraReports.UI.XRLabel _exeCredit;
        private DevExpress.XtraReports.UI.XRLabel xrLabel38;
    }
}
