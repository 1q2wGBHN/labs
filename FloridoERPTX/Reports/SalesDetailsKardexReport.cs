﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
using System.Data;
using App.Entities.ViewModels.Sales;
using System.Collections.Generic;

namespace FloridoERPTX.Reports
{
    public partial class SalesDetailsKardexReport : DevExpress.XtraReports.UI.XtraReport
    {
        public DataSet DataSetTableOrdered = null;
        decimal ivaValue, iepsValue, totalValue, subTotal = 0;
        public SalesDetailsKardexReport()
        {
            InitializeComponent();
        }
        public DataSet printTable(List<SalesDetailReportModel> saleDetails, string saId, string saDate, string saInvoice, string siteName)
        {
            try
            {
                siteLabel.Text = siteName;
                DataSetTableOrdered = new DataSet();

                DataTable DataTableOrdered = new DataTable();
                DataSetTableOrdered.Tables.Add(DataTableOrdered);
                saNoLabel.Text = saId;
                saDaLabel.Text = saDate;
                saInLabel.Text = saInvoice;

                _labelFactura.Visible = false;
                saInLabel.Visible = false;


                DataTableOrdered.Columns.Add(new DataColumn("part_number"));
                DataTableOrdered.Columns.Add(new DataColumn("part_name"));
                DataTableOrdered.Columns.Add(new DataColumn("part_quantity"));
                DataTableOrdered.Columns.Add(new DataColumn("part_price"));
                DataTableOrdered.Columns.Add(new DataColumn("part_importe"));
                DataTableOrdered.Columns.Add(new DataColumn("part_iva"));
                DataTableOrdered.Columns.Add(new DataColumn("part_iva_percentage"));
                DataTableOrdered.Columns.Add(new DataColumn("part_ieps"));
                DataTableOrdered.Columns.Add(new DataColumn("part_ieps_percentage"));
                DataTableOrdered.Columns.Add(new DataColumn("part_total"));



                foreach (var item in saleDetails)
                {
                    DataRow DataRowOrdered = DataSetTableOrdered.Tables[0].NewRow();
                    DataRowOrdered["part_number"] = item.part_number;
                    DataRowOrdered["part_name"] = item.part_name;
                    DataRowOrdered["part_quantity"] = item.part_quantity.ToString("0.0000");
                    DataRowOrdered["part_price"] = "$" + item.part_price.ToString("0.00");
                    DataRowOrdered["part_importe"] = "$" + item.part_importe.ToString("0.00");
                    DataRowOrdered["part_iva"] = "$" + item.part_iva.ToString("0.00");
                    DataRowOrdered["part_iva_percentage"] = item.part_iva_percentage.ToString("0");
                    DataRowOrdered["part_ieps"] = "$" + item.part_ieps.ToString("0.00");
                    DataRowOrdered["part_ieps_percentage"] = item.part_ieps_percentage.ToString("0");
                    DataRowOrdered["part_total"] = "$" + item.part_total.ToString("0.00");

                    DataSetTableOrdered.Tables[0].Rows.Add(DataRowOrdered);
                    ivaValue += item.part_iva;
                    iepsValue += item.part_ieps;
                    subTotal += item.part_importe;


                }
                totalValue = ivaValue + iepsValue + subTotal;
                ivaLabel.Text = "$" + ivaValue.ToString("0.00");
                iepsLabel.Text = "$" + iepsValue.ToString("0.00");
                subtotalLabel.Text = "$" + subTotal.ToString("0.00");
                totalLabel.Text = "$" + totalValue.ToString("0.00");
                part_number.DataBindings.Add(new XRBinding("Text", null, "xrTableRowFormat.part_number"));
                part_name.DataBindings.Add(new XRBinding("Text", null, "xrTableRowFormat.part_name"));
                part_quantity.DataBindings.Add(new XRBinding("Text", null, "xrTableRowFormat.part_quantity"));
                part_price.DataBindings.Add(new XRBinding("Text", null, "xrTableRowFormat.part_price"));
                part_importe.DataBindings.Add(new XRBinding("Text", null, "xrTableRowFormat.part_importe"));
                part_iva.DataBindings.Add(new XRBinding("Text", null, "xrTableRowFormat.part_iva"));
                part_iva_percentage.DataBindings.Add(new XRBinding("Text", null, "xrTableRowFormat.part_iva_percentage"));
                part_ieps.DataBindings.Add(new XRBinding("Text", null, "xrTableRowFormat.part_ieps"));
                part_ieps_percentage.DataBindings.Add(new XRBinding("Text", null, "xrTableRowFormat.part_ieps_percentage"));
                part_total.DataBindings.Add(new XRBinding("Text", null, "xrTableRowFormat.part_total"));


                return DataSetTableOrdered;
            }
            catch (Exception ex)
            {
                string d = ex.Message;
                throw;
            }

        }

    }
}
