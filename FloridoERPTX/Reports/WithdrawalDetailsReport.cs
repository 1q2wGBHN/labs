﻿using System;
using System.Data;
using DevExpress.XtraReports.UI;
using App.Entities.ViewModels.Withdrawal;
using System.Collections.Generic;
using System.Linq;

namespace FloridoERPTX.Reports
{
    public partial class WithdrawalDetailsReport : DevExpress.XtraReports.UI.XtraReport
    {
       
        public DataSet DataSetTableOrdered = null;        
        public WithdrawalDetailsReport()
        {
            InitializeComponent();
          
        }
        public DataSet printTable(List<WithdrawalDetails> Model, string DateValue, string CurrencyValue, string EmployeeName, string SiteName) 
        {
            try
            {
                Date.Text = DateValue;
                Cashier.Text = EmployeeName;
                Currency.Text = CurrencyValue;
                siteLabel.Text = SiteName;

                DataSetTableOrdered = new DataSet();

                DataTable DataTableOrdered = new DataTable();
                DataSetTableOrdered.Tables.Add(DataTableOrdered);
                
                DataTableOrdered.Columns.Add(new DataColumn("DateTime"));
                DataTableOrdered.Columns.Add(new DataColumn("SalePoint"));
                DataTableOrdered.Columns.Add(new DataColumn("Amount"));
                DataTableOrdered.Columns.Add(new DataColumn("WithdrawalType"));

                foreach (var item in Model)
                {
                    DataRow DataRowOrdered = DataSetTableOrdered.Tables[0].NewRow();
                    DataRowOrdered["DateTime"] = item.Date;
                    DataRowOrdered["SalePoint"] = item.Site;
                    DataRowOrdered["Amount"] = string.Format("${0}", item.Amount.ToString("0.00"));
                    DataRowOrdered["WithdrawalType"] = item.WithdrawalType;


                    DataSetTableOrdered.Tables[0].Rows.Add(DataRowOrdered);                                                        
                }

                total.Text = string.Format("{0}{1}", "$", Model.Sum(c => c.Amount).ToString("0.00"));

                DateTime.DataBindings.Add(new XRBinding("Text", null, "xrTableRowFormat.DateTime"));
                SalePoint.DataBindings.Add(new XRBinding("Text", null, "xrTableRowFormat.SalePoint"));
                Amount.DataBindings.Add(new XRBinding("Text", null, "xrTableRowFormat.Amount"));
                WithdrawalType.DataBindings.Add(new XRBinding("Text", null, "xrTableRowFormat.WithdrawalType"));

                return DataSetTableOrdered;
            }
            catch (Exception ex)
            {
                string d=ex.Message;
                throw;
            }
          
        }       
    }
}
