﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
using App.Entities.ViewModels.Invoices;
using FloridoERPTX.ViewModels.Invoices;
using System.Text;

namespace FloridoERPTX.Reports
{
    public partial class XRPTInvoices : DevExpress.XtraReports.UI.XtraReport
    {
        public XRPTInvoices(object par_RM)
        {
            InitializeComponent();

            var RM = (InvoiceReportModel)par_RM;
            ods_invoices.DataSource = RM;

            string predicate = null;
            //     F I L T R O S



            // POR CODIGO DE CLIENTE
            if (!string.IsNullOrEmpty(RM.CustomerCode))
            {
                predicate = "FILTRADO POR CODIGO DE CLIENTE " + RM.CustomerCode;
            }
            // POR CODIGO DE MONEDA
            
            if (RM.CurrencyCodeOption != "SIN_FILTRAR")
            {
                if (predicate == null)
                    predicate = "FILTRADO POR ";
                else
                    predicate += ", ";
                predicate += "CODIGO DE MONEDA " + RM.CurrencyCodeOption;
                
            }
            // POR FECHAS
            if (RM.StartDate != null)
            {
                DateTime start_date_no_null = (RM.StartDate ?? DateTime.Now).Date;
                DateTime finish_date_no_null = (RM.FinishDate ?? DateTime.Now).Date;
                if (predicate == null)
                    predicate = "FILTRADO POR ";
                else
                    predicate += ", ";

                if (RM.FinishDate != null)
                    predicate += "PERIODO " + start_date_no_null.ToShortDateString() + " AL " +
                        finish_date_no_null.ToShortDateString();
                else
                    predicate += "FECHA " + start_date_no_null.ToShortDateString();
            }
            // POR MONTO DE FACTURA
            if(RM.AmountOption != "SIN_FILTRAR")
            {
                if (predicate == null)
                    predicate = "FILTRADO POR ";
                else
                    predicate += ", ";

                switch (RM.AmountOption)
                {
                    case "IGUAL_A":
                        predicate += "MONTO IGUAL A " + RM.InvoiceTotal.ToString();
                        break;
                    case "MAYOR_A":
                        predicate += "MONTO MAYOR A " + RM.InvoiceTotal.ToString();
                        break;
                    case "MENOR_A":
                        predicate += "MONTO MENOR A " + RM.InvoiceTotal.ToString();
                        break;
                    default:
                        break;
                }
            }
            
            // POR ESTADO DE FACTURA
            if(RM.StatusOption != "SIN_FILTRAR")
            {
                if (predicate == null)
                    predicate = "FILTRADO POR ";
                else
                    predicate += ", ";
                switch (RM.StatusOption)
                {
                    case "ACTIVAS":
                        predicate += "FACTURAS ACTIVAS";
                        break;
                    case "CANCELADAS":
                        predicate += "FACTURAS CANCELADAS"; ;
                        break;
                    case "SIN_FILTRAR":
                        break;
                    default:
                        break;
                }
            }
            if(predicate != null)
            {
                predicate += ".";

                int index = predicate.LastIndexOf(',');
                
                if (index != -1)
                {
                    var predicate_sb = new StringBuilder(predicate);
                    predicate_sb.Insert(index, ' ');
                    predicate_sb[index + 1] = 'Y'; // index starts at 0!
                    predicate = predicate_sb.ToString();
                }
            }else
            {
                predicate = "SIN FILTROS.";
            }
            

            lbl_predicate.Text = predicate;

            // B I N D     T O T A L E S 

            lbl_credit_zrate.Text = RM.Totales[0, 0].ToString("C");
            lbl_credit_taxed.Text = RM.Totales[1, 0].ToString("C");
            lbl_credit_tax.Text = RM.Totales[2, 0].ToString("C");
            lbl_credit_ieps.Text = RM.Totales[3, 0].ToString("C");
            lbl_total_credit.Text = RM.Totales[4, 0].ToString("C");

            lbl_cash_zrate.Text = RM.Totales[0, 1].ToString("C");
            lbl_cash_taxed.Text = RM.Totales[1, 1].ToString("C");
            lbl_cash_tax.Text = RM.Totales[2, 1].ToString("C");
            lbl_cash_ieps.Text = RM.Totales[3, 1].ToString("C");
            lbl_total_cash.Text = RM.Totales[4, 1].ToString("C");

            lbl_total_zrate.Text = RM.Totales[0, 2].ToString("C");
            lbl_total_taxed.Text = RM.Totales[1, 2].ToString("C");
            lbl_total_tax.Text = RM.Totales[2, 2].ToString("C");
            lbl_total_ieps.Text = RM.Totales[3, 2].ToString("C");
            lbl_total_global.Text = RM.Totales[4, 2].ToString("C");

        }

    }
}
