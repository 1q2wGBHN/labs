﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
using System.Data;
using System.Collections.Generic;
using App.Entities.ViewModels.Item;
using App.Entities;

namespace FloridoERPTX.Reports
{
    public partial class AjusteInventario : DevExpress.XtraReports.UI.XtraReport
    {
        public DataSet DataSetTableMovements = null;

        public AjusteInventario()
        {
            InitializeComponent();
        }

        public DataSet printTable(List<ItemModel> items, string siteName, USER_MASTER usuario)
        {
            try
            {
                DataSetTableMovements = new DataSet();

                DataTable table = new DataTable();
                DataSetTableMovements.Tables.Add(table);

                xrLabelSiteName.Text = siteName;
                xrLabelDocumentNumber.Text = items[0].Document;
                xrLabelUser.Text = usuario.first_name + " " + usuario.last_name;

                table.Columns.Add(new DataColumn("xrTableCellPartNumber"));
                table.Columns.Add(new DataColumn("xrTableCellPartDescription"));
                table.Columns.Add(new DataColumn("xrTableCellStorageLocation"));
                table.Columns.Add(new DataColumn("xrTableCellInitialStock"));
                table.Columns.Add(new DataColumn("xrTableCellMovement"));
                table.Columns.Add(new DataColumn("xrTableCellQuantity"));
                table.Columns.Add(new DataColumn("xrTableCellFinalStock"));
                table.Columns.Add(new DataColumn("xrTableCellComment"));

                foreach (var item in items)
                {
                    DataRow DataRowOrdered = DataSetTableMovements.Tables[0].NewRow();
                    DataRowOrdered["xrTableCellPartNumber"] = item.PathNumber;
                    DataRowOrdered["xrTableCellPartDescription"] = item.Description;
                    DataRowOrdered["xrTableCellStorageLocation"] = item.StorageLocation;
                    DataRowOrdered["xrTableCellInitialStock"] = item.FStock;
                    DataRowOrdered["xrTableCellMovement"] = item.SMovementType;
                    DataRowOrdered["xrTableCellQuantity"] =  item.Quantity; //cambie por quantity
                    DataRowOrdered["xrTableCellFinalStock"] = item.Stock;
                    DataRowOrdered["xrTableCellComment"] = item.Comment;
                    DataSetTableMovements.Tables[0].Rows.Add(DataRowOrdered);
                }

                xrTableCellPartNumber.DataBindings.Add(new XRBinding("Text", null, "xrTableRow1.xrTableCellPartNumber"));
                xrTableCellPartDescription.DataBindings.Add(new XRBinding("Text", null, "xrTableRow1.xrTableCellPartDescription"));
                xrTableCellStorageLocation.DataBindings.Add(new XRBinding("Text", null, "xrTableRow1.xrTableCellStorageLocation"));
                xrTableCellInitialStock.DataBindings.Add(new XRBinding("Text", null, "xrTableRow1.xrTableCellInitialStock"));
                xrTableCellMovement.DataBindings.Add(new XRBinding("Text", null, "xrTableRow1.xrTableCellMovement"));
                xrTableCellQuantity.DataBindings.Add(new XRBinding("Text", null, "xrTableRow1.xrTableCellQuantity"));
                xrTableCellFinalStock.DataBindings.Add(new XRBinding("Text", null, "xrTableRow1.xrTableCellFinalStock"));
                xrTableCellComment.DataBindings.Add(new XRBinding("Text", null, "xrTableRow1.xrTableCellComment"));

                return DataSetTableMovements;
            }
            catch (Exception ex)
            {
                string d = ex.Message;
                throw;
            }
        }

    }
}
