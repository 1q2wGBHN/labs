﻿using System;
using System.Data;
using DevExpress.XtraReports.UI;
using App.Entities.ViewModels.Sales;
using FloridoERPTX.Extensions;
using System.ComponentModel.DataAnnotations;
using DevExpress.XtraPrinting;
using System.Drawing;
using System.Linq;

namespace FloridoERPTX.Reports
{
    public partial class SalesIEPS : DevExpress.XtraReports.UI.XtraReport
    {
       
        public DataSet DataSetTableOrdered = null;       
        public SalesIEPS()
        {
            InitializeComponent();                      
        }
        public DataSet printTable(SalesIndex Model, string siteName) //se encarga de imprimir la orden del proveedor
        {
            try
            {
                var typeDisplay = Model.Type.GetAttribute<DisplayAttribute>();
                var searchDisplay = Model.Search.GetAttribute<DisplayAttribute>();

                labelDate.Text = DateTime.Now.Date.ToShortDateString();
                labelDate1.Text = Model.StartDate;
                labelDate2.Text = Model.EndDate;
                siteLabel.Text = siteName;
                _labelReporte.Text = string.Format("Relación de ventas {0} con IEPS por {1}", typeDisplay.Name.ToLower(), searchDisplay.Name.ToLower());


                #region TableRate 8
                XRTable _tableRate8 = new XRTable();
                _tableRate8.SizeF = new SizeF(1032, 20);
                _tableRate8.BeginInit();
                XRTableRow HeaderRow;
                HeaderRow = new XRTableRow();
                HeaderRow.HeightF = 15f;

                var DateCell = new XRTableCell();
                DateCell.TextAlignment = TextAlignment.MiddleCenter;
                DateCell.Text = "Fecha";
                DateCell.Font = new Font(DateCell.Font.FontFamily, DateCell.Font.Size, FontStyle.Bold);

                var DepartmentCell = new XRTableCell();
                DepartmentCell.TextAlignment = TextAlignment.MiddleCenter;
                DepartmentCell.Text = "Departamento";
                DepartmentCell.Font = new Font(DepartmentCell.Font.FontFamily, DepartmentCell.Font.Size, FontStyle.Bold);

                var CodeCell = new XRTableCell();
                CodeCell.TextAlignment = TextAlignment.MiddleCenter;
                CodeCell.Text = "Código";
                CodeCell.Font = new Font(CodeCell.Font.FontFamily, CodeCell.Font.Size, FontStyle.Bold);

                var FamilyCell = new XRTableCell();
                FamilyCell.TextAlignment = TextAlignment.MiddleCenter;
                FamilyCell.Text = "Familia";
                FamilyCell.Font = new Font(FamilyCell.Font.FontFamily, FamilyCell.Font.Size, FontStyle.Bold);

                var DescriptionCell = new XRTableCell();
                DescriptionCell.TextAlignment = TextAlignment.MiddleCenter;
                DescriptionCell.Text = "Descripción";
                DescriptionCell.Font = new Font(DescriptionCell.Font.FontFamily, DescriptionCell.Font.Size, FontStyle.Bold);

                var IEPSRateCell = new XRTableCell();
                IEPSRateCell.TextAlignment = TextAlignment.MiddleCenter;
                IEPSRateCell.Text = "Tasa IEPS";
                IEPSRateCell.Font = new Font(IEPSRateCell.Font.FontFamily, IEPSRateCell.Font.Size, FontStyle.Bold);

                var SalesCell = new XRTableCell();
                SalesCell.TextAlignment = TextAlignment.MiddleCenter;
                SalesCell.Text = "Ventas";
                SalesCell.Font = new Font(SalesCell.Font.FontFamily, SalesCell.Font.Size, FontStyle.Bold);

                var IEPSAmountCell = new XRTableCell();
                IEPSAmountCell.TextAlignment = TextAlignment.MiddleCenter;
                IEPSAmountCell.Text = "Importe IEPS";
                IEPSAmountCell.Font = new Font(IEPSAmountCell.Font.FontFamily, IEPSAmountCell.Font.Size, FontStyle.Bold);

                var TaxCell = new XRTableCell();
                TaxCell.TextAlignment = TextAlignment.MiddleCenter;
                TaxCell.Text = "IVA";
                TaxCell.Font = new Font(TaxCell.Font.FontFamily, TaxCell.Font.Size, FontStyle.Bold);

                var TotalCell = new XRTableCell();
                TotalCell.TextAlignment = TextAlignment.MiddleCenter;
                TotalCell.Text = "TOTAL";
                TotalCell.Font = new Font(TotalCell.Font.FontFamily, TotalCell.Font.Size, FontStyle.Bold);

                HeaderRow.Cells.Add(DateCell);
                HeaderRow.Cells.Add(DepartmentCell);

                if (Model.Search == SearchType.Family)
                    HeaderRow.Cells.Add(FamilyCell);
                else if (Model.Search == SearchType.Item)
                {
                    HeaderRow.Cells.Add(CodeCell);
                    HeaderRow.Cells.Add(DescriptionCell);
                }

                HeaderRow.Cells.Add(IEPSRateCell);
                HeaderRow.Cells.Add(SalesCell);
                HeaderRow.Cells.Add(IEPSAmountCell);
                HeaderRow.Cells.Add(TaxCell);
                HeaderRow.Cells.Add(TotalCell);

                HeaderRow.Borders = BorderSide.Bottom;
                HeaderRow.BackColor = Color.Silver;
                    
                _tableRate8.Rows.Add(HeaderRow);
                foreach (var item in Model.IEPSReport.SalesIepsTax)
                {
                    var DetailRow = new XRTableRow();                    
                    DetailRow.HeightF = 15f;

                    var DateDetailCell = new XRTableCell();
                    DateDetailCell.Text = item.saleDate;
                    DateDetailCell.TextAlignment = TextAlignment.MiddleCenter;

                    var DepartmentDetailCell = new XRTableCell();
                    DepartmentDetailCell.Text = item.departamento;
                    DepartmentDetailCell.TextAlignment = TextAlignment.MiddleCenter;

                    var CodeDetailCell = new XRTableCell();
                    CodeDetailCell.Text = item.codigo;
                    CodeDetailCell.TextAlignment = TextAlignment.MiddleCenter;

                    var FamilyDetailCell = new XRTableCell();
                    FamilyDetailCell.Text = item.familia;
                    FamilyDetailCell.TextAlignment = TextAlignment.MiddleCenter;

                    var DescriptionDetailCell = new XRTableCell();
                    DescriptionDetailCell.Text = item.descripcion;
                    DescriptionDetailCell.TextAlignment = TextAlignment.MiddleCenter;

                    var IEPSRateDetailCell = new XRTableCell();
                    IEPSRateDetailCell.Text = item.tasa_ieps;
                    IEPSRateDetailCell.TextAlignment = TextAlignment.MiddleCenter;

                    var SalesDetailCell = new XRTableCell();
                    SalesDetailCell.Text = item.venta.ToString("C");
                    SalesDetailCell.TextAlignment = TextAlignment.MiddleCenter;

                    var IEPSAmountDetailCell = new XRTableCell();
                    IEPSAmountDetailCell.Text = item.importe_ieps.ToString("C");
                    IEPSAmountDetailCell.TextAlignment = TextAlignment.MiddleCenter;

                    var TaxDetailCell = new XRTableCell();
                    TaxDetailCell.Text = item.iva.ToString("C");
                    TaxDetailCell.TextAlignment = TextAlignment.MiddleCenter;

                    var TotalDetailCell = new XRTableCell();
                    TotalDetailCell.Text = item.total.ToString("C");
                    TotalDetailCell.TextAlignment = TextAlignment.MiddleCenter;

                    DetailRow.Cells.Add(DateDetailCell);
                    DetailRow.Cells.Add(DepartmentDetailCell);                                                          
                    
                    if (Model.Search == SearchType.Family)
                        DetailRow.Cells.Add(FamilyDetailCell);
                    else if (Model.Search == SearchType.Item)
                    {
                        DetailRow.Cells.Add(CodeDetailCell);
                        DetailRow.Cells.Add(DescriptionDetailCell);
                    }
                    DetailRow.Cells.Add(IEPSRateDetailCell);
                    DetailRow.Cells.Add(SalesDetailCell);
                    DetailRow.Cells.Add(IEPSAmountDetailCell);
                    DetailRow.Cells.Add(TaxDetailCell);
                    DetailRow.Cells.Add(TotalDetailCell);

                    DetailRow.Borders = BorderSide.Bottom;
                    _tableRate8.Rows.Add(DetailRow);
                }
                _tableRate8.EndInit();
                this.Detail1.Controls.Add(_tableRate8);
                _ventas.Text = Model.IEPSReport.SalesIepsTax.Sum(c => c.venta).ToString("C");
                _importeIEPS.Text = Model.IEPSReport.SalesIepsTax.Sum(c => c.importe_ieps).ToString("C");
                _IVA.Text = Model.IEPSReport.SalesIepsTax.Sum(c => c.iva).ToString("C");
                _total.Text = Model.IEPSReport.SalesIepsTax.Sum(c => c.total).ToString("C");
                #endregion

                #region TableRate 0
                XRTable _tableRate0 = new XRTable();
                _tableRate0.SizeF = new SizeF(1032, 20);
                _tableRate0.BeginInit();
                var HeaderRow2 = new XRTableRow();
                HeaderRow2.HeightF = 15f;

                var DateCell2 = new XRTableCell();
                DateCell2.TextAlignment = TextAlignment.MiddleCenter;
                DateCell2.Text = "Fecha";
                DateCell2.Font = new Font(DateCell2.Font.FontFamily, DateCell2.Font.Size, FontStyle.Bold);

                var DepartmentCell2 = new XRTableCell();
                DepartmentCell2.TextAlignment = TextAlignment.MiddleCenter;
                DepartmentCell2.Text = "Departamento";
                DepartmentCell2.Font = new Font(DepartmentCell2.Font.FontFamily, DepartmentCell2.Font.Size, FontStyle.Bold);

                var CodeCell2 = new XRTableCell();
                CodeCell2.TextAlignment = TextAlignment.MiddleCenter;
                CodeCell2.Text = "Código";
                CodeCell2.Font = new Font(CodeCell2.Font.FontFamily, CodeCell2.Font.Size, FontStyle.Bold);

                var FamilyCell2 = new XRTableCell();
                FamilyCell2.TextAlignment = TextAlignment.MiddleCenter;
                FamilyCell2.Text = "Familia";
                FamilyCell2.Font = new Font(FamilyCell2.Font.FontFamily, FamilyCell2.Font.Size, FontStyle.Bold);

                var DescriptionCell2 = new XRTableCell();
                DescriptionCell2.TextAlignment = TextAlignment.MiddleCenter;
                DescriptionCell2.Text = "Descripción";
                DescriptionCell2.Font = new Font(DescriptionCell2.Font.FontFamily, DescriptionCell2.Font.Size, FontStyle.Bold);

                var IEPSRateCell2 = new XRTableCell();
                IEPSRateCell2.TextAlignment = TextAlignment.MiddleCenter;
                IEPSRateCell2.Text = "Tasa IEPS";
                IEPSRateCell2.Font = new Font(IEPSRateCell2.Font.FontFamily, IEPSRateCell2.Font.Size, FontStyle.Bold);

                var SalesCell2 = new XRTableCell();
                SalesCell2.TextAlignment = TextAlignment.MiddleCenter;
                SalesCell2.Text = "Ventas";
                SalesCell2.Font = new Font(SalesCell2.Font.FontFamily, SalesCell2.Font.Size, FontStyle.Bold);

                var IEPSAmountCell2 = new XRTableCell();
                IEPSAmountCell2.TextAlignment = TextAlignment.MiddleCenter;
                IEPSAmountCell2.Text = "Importe IEPS";
                IEPSAmountCell2.Font = new Font(IEPSAmountCell2.Font.FontFamily, IEPSAmountCell2.Font.Size, FontStyle.Bold);

                var TaxCell2 = new XRTableCell();
                TaxCell2.TextAlignment = TextAlignment.MiddleCenter;
                TaxCell2.Text = "IVA";
                TaxCell2.Font = new Font(TaxCell2.Font.FontFamily, TaxCell2.Font.Size, FontStyle.Bold);

                var TotalCell2 = new XRTableCell();
                TotalCell2.TextAlignment = TextAlignment.MiddleCenter;
                TotalCell2.Text = "TOTAL";
                TotalCell2.Font = new Font(TotalCell2.Font.FontFamily, TotalCell2.Font.Size, FontStyle.Bold);

                HeaderRow2.Cells.Add(DateCell2);
                HeaderRow2.Cells.Add(DepartmentCell2);

                if (Model.Search == SearchType.Family)
                    HeaderRow2.Cells.Add(FamilyCell2);
                else if (Model.Search == SearchType.Item)
                {
                    HeaderRow2.Cells.Add(CodeCell2);
                    HeaderRow2.Cells.Add(DescriptionCell2);
                }

                HeaderRow2.Cells.Add(IEPSRateCell2);
                HeaderRow2.Cells.Add(SalesCell2);
                HeaderRow2.Cells.Add(IEPSAmountCell2);
                HeaderRow2.Cells.Add(TaxCell2);
                HeaderRow2.Cells.Add(TotalCell2);

                HeaderRow2.Borders = BorderSide.Bottom;
                HeaderRow2.BackColor = Color.Silver;

                _tableRate0.Rows.Add(HeaderRow2);
                foreach (var item in Model.IEPSReport.SalesIepsTaxZero)
                {
                    var DetailRow = new XRTableRow();
                    DetailRow.HeightF = 15f;

                    var DateDetailCell = new XRTableCell();
                    DateDetailCell.Text = item.saleDate;
                    DateDetailCell.TextAlignment = TextAlignment.MiddleCenter;

                    var DepartmentDetailCell = new XRTableCell();
                    DepartmentDetailCell.Text = item.departamento;
                    DepartmentDetailCell.TextAlignment = TextAlignment.MiddleCenter;

                    var CodeDetailCell = new XRTableCell();
                    CodeDetailCell.Text = item.codigo;
                    CodeDetailCell.TextAlignment = TextAlignment.MiddleCenter;

                    var FamilyDetailCell = new XRTableCell();
                    FamilyDetailCell.Text = item.familia;
                    FamilyDetailCell.TextAlignment = TextAlignment.MiddleCenter;

                    var DescriptionDetailCell = new XRTableCell();
                    DescriptionDetailCell.Text = item.descripcion;
                    DescriptionDetailCell.TextAlignment = TextAlignment.MiddleCenter;

                    var IEPSRateDetailCell = new XRTableCell();
                    IEPSRateDetailCell.Text = item.tasa_ieps;
                    IEPSRateDetailCell.TextAlignment = TextAlignment.MiddleCenter;

                    var SalesDetailCell = new XRTableCell();
                    SalesDetailCell.Text = item.venta.ToString("C");
                    SalesDetailCell.TextAlignment = TextAlignment.MiddleCenter;

                    var IEPSAmountDetailCell = new XRTableCell();
                    IEPSAmountDetailCell.Text = item.importe_ieps.ToString("C");
                    IEPSAmountDetailCell.TextAlignment = TextAlignment.MiddleCenter;

                    var TaxDetailCell = new XRTableCell();
                    TaxDetailCell.Text = item.iva.ToString("C");
                    TaxDetailCell.TextAlignment = TextAlignment.MiddleCenter;

                    var TotalDetailCell = new XRTableCell();
                    TotalDetailCell.Text = item.total.ToString("C");
                    TotalDetailCell.TextAlignment = TextAlignment.MiddleCenter;

                    DetailRow.Cells.Add(DateDetailCell);
                    DetailRow.Cells.Add(DepartmentDetailCell);

                    if (Model.Search == SearchType.Family)
                        DetailRow.Cells.Add(FamilyDetailCell);
                    else if (Model.Search == SearchType.Item)
                    {
                        DetailRow.Cells.Add(CodeDetailCell);
                        DetailRow.Cells.Add(DescriptionDetailCell);
                    }
                    DetailRow.Cells.Add(IEPSRateDetailCell);
                    DetailRow.Cells.Add(SalesDetailCell);
                    DetailRow.Cells.Add(IEPSAmountDetailCell);
                    DetailRow.Cells.Add(TaxDetailCell);
                    DetailRow.Cells.Add(TotalDetailCell);

                    DetailRow.Borders = BorderSide.Bottom;
                    _tableRate0.Rows.Add(DetailRow);
                }
                _tableRate0.EndInit();
                this.Detail2.Controls.Add(_tableRate0);                
                _ventas0.Text = Model.IEPSReport.SalesIepsTaxZero.Sum(c => c.venta).ToString("C");
                _importeIEPS0.Text = Model.IEPSReport.SalesIepsTaxZero.Sum(c => c.importe_ieps).ToString("C");
                _IVA0.Text = Model.IEPSReport.SalesIepsTaxZero.Sum(c => c.iva).ToString("C");
                _total0.Text = Model.IEPSReport.SalesIepsTaxZero.Sum(c => c.total).ToString("C");
                #endregion

                return DataSetTableOrdered;
            }
            catch (Exception ex)
            {
                string d=ex.Message;
                throw;
            }
          
        }

       


    }
}
