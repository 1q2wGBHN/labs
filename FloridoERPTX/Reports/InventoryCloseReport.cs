﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
using App.Entities.ViewModels.Inventory;
using System.Collections.Generic;
using System.Data;

namespace FloridoERPTX.Reports
{
    public partial class InventoryCloseReport : DevExpress.XtraReports.UI.XtraReport
    {
        public DataSet DataSetTableMovements = null;
        public InventoryCloseReport()
        {
            InitializeComponent();
        }
        public DataSet printTable(List<InventoryReportModel> items, string siteName , string id , string endDate)
        {
            try
            {
                xrLabelSiteName.Text = siteName;
                xrInventory.Text = id.ToString();
                xrLabelDate.Text = endDate;
                DataSetTableMovements = new DataSet();
                DataTable table = new DataTable();
                DataSetTableMovements.Tables.Add(table);
                table.Columns.Add(new DataColumn("cellPartNumber"));
                table.Columns.Add(new DataColumn("cellPartDescription"));
                table.Columns.Add(new DataColumn("cellCurrent"));
                table.Columns.Add(new DataColumn("cellDetail"));
                table.Columns.Add(new DataColumn("cellDifference"));
                table.Columns.Add(new DataColumn("cellCurrentAmount"));
                table.Columns.Add(new DataColumn("cellDetailAmount"));
                table.Columns.Add(new DataColumn("cellDifferenceAmount"));
                decimal difference = 0;
                decimal current = 0;
                decimal detail = 0;
                int correct = 0;
                foreach (var item in items)
                {
                    DataRow DataRowOrdered = DataSetTableMovements.Tables[0].NewRow();
                    DataRowOrdered["cellPartNumber"] = item.part_number;
                    DataRowOrdered["cellPartDescription"] = item.part_description;
                    DataRowOrdered["cellCurrent"] = Decimal.Round(item.quantity_current,2).ToString("N2");
                    DataRowOrdered["cellDetail"] = Decimal.Round(item.quantity_detail,2).ToString("N2");
                    DataRowOrdered["cellDifference"] = Decimal.Round(item.quantity_difference, 4).ToString("N2");
                    DataRowOrdered["cellCurrentAmount"] = "$" + Decimal.Round(item.currentAmount , 4).ToString("N4");
                    DataRowOrdered["cellDetailAmount"] = "$" + Decimal.Round(item.detailAmount, 4).ToString("N4");
                    DataRowOrdered["cellDifferenceAmount"] = "$" + Decimal.Round(item.differenceAmount, 4).ToString("N4"); 
                    DataSetTableMovements.Tables[0].Rows.Add(DataRowOrdered);
                    difference = difference + item.differenceAmount;
                    current = current + item.currentAmount;
                    detail = detail + item.detailAmount;
                    if (item.quantity_current == item.quantity_detail)
                    {
                        correct++;
                    }
                }

                cellPartNumber.DataBindings.Add(new XRBinding("Text", null, "xrTableRow1.cellPartNumber"));
                cellPartDescription.DataBindings.Add(new XRBinding("Text", null, "xrTableRow1.cellPartDescription"));
                cellCurrent.DataBindings.Add(new XRBinding("Text", null, "xrTableRow1.cellCurrent"));
                cellCurrentAmount.DataBindings.Add(new XRBinding("Text", null, "xrTableRow1.cellStorageLocation"));
                cellDetail.DataBindings.Add(new XRBinding("Text", null, "xrTableRow1.cellDetail"));
                cellDifference.DataBindings.Add(new XRBinding("Text", null, "xrTableRow1.cellDifference"));
                cellCurrentAmount.DataBindings.Add(new XRBinding("Text", null, "xrTableRow1.cellCurrentAmount"));
                cellDetailAmount.DataBindings.Add(new XRBinding("Text", null, "xrTableRow1.cellDetailAmount"));
                cellDifferenceAmount.DataBindings.Add(new XRBinding("Text", null, "xrTableRow1.cellDifferenceAmount"));
                xrLabelDifferenceTotal.Text = "$"+ Decimal.Round(difference,4).ToString("N4");
                xrLabelCurrentTotal.Text = "$"+ Decimal.Round(current, 4).ToString("N4");
                xrLabelDetailTotal.Text = "$" +Decimal.Round(detail, 4).ToString("N4");
                xrLabelCount.Text = correct.ToString() + " DE " + items.Count.ToString();
            
                return DataSetTableMovements;
            }
            catch (Exception ex)
            {
                string d = ex.Message;
                throw;
            }
        }

    }
}
