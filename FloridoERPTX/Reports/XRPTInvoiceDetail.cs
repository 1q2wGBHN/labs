﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
using App.Entities.ViewModels.Invoices;
using FloridoERPTX.ViewModels.Invoices;
namespace FloridoERPTX.Reports
{
    public partial class XRPTInvoiceDetail : DevExpress.XtraReports.UI.XtraReport
    {
        public XRPTInvoiceDetail(object IR)
        {

            InitializeComponent();
            ods_invoice.DataSource = (InvoiceReport)IR;
        }

    }
}
