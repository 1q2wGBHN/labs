﻿namespace FloridoERPTX.Reports
{
    partial class UtilitiesReport
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(UtilitiesReport));
            this.Detail = new DevExpress.XtraReports.UI.DetailBand();
            this.xrLabel1 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel7 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel6 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel2 = new DevExpress.XtraReports.UI.XRLabel();
            this.Descripcion = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel5 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLine3 = new DevExpress.XtraReports.UI.XRLine();
            this.xrLabel19 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel18 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel17 = new DevExpress.XtraReports.UI.XRLabel();
            this.TopMargin = new DevExpress.XtraReports.UI.TopMarginBand();
            this.BottomMargin = new DevExpress.XtraReports.UI.BottomMarginBand();
            this.ReportHeader = new DevExpress.XtraReports.UI.ReportHeaderBand();
            this.xrLabel10 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrPictureBox2 = new DevExpress.XtraReports.UI.XRPictureBox();
            this.Date = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel9 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel4 = new DevExpress.XtraReports.UI.XRLabel();
            this.siteLabel = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel3 = new DevExpress.XtraReports.UI.XRLabel();
            this.GroupFooter1 = new DevExpress.XtraReports.UI.GroupFooterBand();
            this.GreatPercentage = new DevExpress.XtraReports.UI.XRLabel();
            this.GreatUtility = new DevExpress.XtraReports.UI.XRLabel();
            this.GreatCost = new DevExpress.XtraReports.UI.XRLabel();
            this.GreatTotal = new DevExpress.XtraReports.UI.XRLabel();
            this.GreatIeps = new DevExpress.XtraReports.UI.XRLabel();
            this.GreatSales = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel11 = new DevExpress.XtraReports.UI.XRLabel();
            this.PageFooter = new DevExpress.XtraReports.UI.PageFooterBand();
            this.xrPageInfo1 = new DevExpress.XtraReports.UI.XRPageInfo();
            this.DetailReport = new DevExpress.XtraReports.UI.DetailReportBand();
            this.Detail1 = new DevExpress.XtraReports.UI.DetailBand();
            this.xrTable1 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow1 = new DevExpress.XtraReports.UI.XRTableRow();
            this.Item = new DevExpress.XtraReports.UI.XRTableCell();
            this.Description = new DevExpress.XtraReports.UI.XRTableCell();
            this.Qty = new DevExpress.XtraReports.UI.XRTableCell();
            this.Sales = new DevExpress.XtraReports.UI.XRTableCell();
            this.IEPS = new DevExpress.XtraReports.UI.XRTableCell();
            this.Total = new DevExpress.XtraReports.UI.XRTableCell();
            this.Cost = new DevExpress.XtraReports.UI.XRTableCell();
            this.Utility = new DevExpress.XtraReports.UI.XRTableCell();
            this.Percentage = new DevExpress.XtraReports.UI.XRTableCell();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // Detail
            // 
            this.Detail.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel1,
            this.xrLabel7,
            this.xrLabel6,
            this.xrLabel2,
            this.Descripcion,
            this.xrLabel5,
            this.xrLine3,
            this.xrLabel19,
            this.xrLabel18,
            this.xrLabel17});
            this.Detail.Dpi = 100F;
            this.Detail.HeightF = 27.55347F;
            this.Detail.Name = "Detail";
            this.Detail.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.Detail.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel1
            // 
            this.xrLabel1.Dpi = 100F;
            this.xrLabel1.Font = new System.Drawing.Font("Arial", 7F, System.Drawing.FontStyle.Bold);
            this.xrLabel1.LocationFloat = new DevExpress.Utils.PointFloat(191.6786F, 0F);
            this.xrLabel1.Name = "xrLabel1";
            this.xrLabel1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel1.SizeF = new System.Drawing.SizeF(84.37503F, 17.60772F);
            this.xrLabel1.StylePriority.UseFont = false;
            this.xrLabel1.StylePriority.UseTextAlignment = false;
            this.xrLabel1.Text = "Cantidad";
            this.xrLabel1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrLabel7
            // 
            this.xrLabel7.Dpi = 100F;
            this.xrLabel7.Font = new System.Drawing.Font("Arial", 7F, System.Drawing.FontStyle.Bold);
            this.xrLabel7.LocationFloat = new DevExpress.Utils.PointFloat(631.7451F, 0F);
            this.xrLabel7.Multiline = true;
            this.xrLabel7.Name = "xrLabel7";
            this.xrLabel7.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel7.SizeF = new System.Drawing.SizeF(49.76282F, 17.60773F);
            this.xrLabel7.StylePriority.UseFont = false;
            this.xrLabel7.StylePriority.UseTextAlignment = false;
            this.xrLabel7.Text = "%";
            this.xrLabel7.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrLabel6
            // 
            this.xrLabel6.Dpi = 100F;
            this.xrLabel6.Font = new System.Drawing.Font("Arial", 7F, System.Drawing.FontStyle.Bold);
            this.xrLabel6.LocationFloat = new DevExpress.Utils.PointFloat(550.7323F, 0F);
            this.xrLabel6.Multiline = true;
            this.xrLabel6.Name = "xrLabel6";
            this.xrLabel6.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel6.SizeF = new System.Drawing.SizeF(81.01279F, 17.60773F);
            this.xrLabel6.StylePriority.UseFont = false;
            this.xrLabel6.StylePriority.UseTextAlignment = false;
            this.xrLabel6.Text = "Utilidad";
            this.xrLabel6.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrLabel2
            // 
            this.xrLabel2.Dpi = 100F;
            this.xrLabel2.Font = new System.Drawing.Font("Arial", 7F, System.Drawing.FontStyle.Bold);
            this.xrLabel2.LocationFloat = new DevExpress.Utils.PointFloat(469.7195F, 0F);
            this.xrLabel2.Multiline = true;
            this.xrLabel2.Name = "xrLabel2";
            this.xrLabel2.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel2.SizeF = new System.Drawing.SizeF(81.01279F, 17.60773F);
            this.xrLabel2.StylePriority.UseFont = false;
            this.xrLabel2.StylePriority.UseTextAlignment = false;
            this.xrLabel2.Text = "Costo";
            this.xrLabel2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // Descripcion
            // 
            this.Descripcion.Dpi = 100F;
            this.Descripcion.Font = new System.Drawing.Font("Arial", 7F, System.Drawing.FontStyle.Bold);
            this.Descripcion.LocationFloat = new DevExpress.Utils.PointFloat(78.87255F, 0F);
            this.Descripcion.Name = "Descripcion";
            this.Descripcion.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.Descripcion.SizeF = new System.Drawing.SizeF(112.8061F, 17.55346F);
            this.Descripcion.StylePriority.UseFont = false;
            this.Descripcion.StylePriority.UseTextAlignment = false;
            this.Descripcion.Text = "Descripción";
            this.Descripcion.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrLabel5
            // 
            this.xrLabel5.Dpi = 100F;
            this.xrLabel5.Font = new System.Drawing.Font("Arial", 7F, System.Drawing.FontStyle.Bold);
            this.xrLabel5.LocationFloat = new DevExpress.Utils.PointFloat(383.7161F, 0F);
            this.xrLabel5.Multiline = true;
            this.xrLabel5.Name = "xrLabel5";
            this.xrLabel5.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel5.SizeF = new System.Drawing.SizeF(86.00339F, 17.60773F);
            this.xrLabel5.StylePriority.UseFont = false;
            this.xrLabel5.StylePriority.UseTextAlignment = false;
            this.xrLabel5.Text = "Total";
            this.xrLabel5.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrLine3
            // 
            this.xrLine3.Dpi = 100F;
            this.xrLine3.LocationFloat = new DevExpress.Utils.PointFloat(0F, 17.60773F);
            this.xrLine3.Name = "xrLine3";
            this.xrLine3.SizeF = new System.Drawing.SizeF(681.5078F, 9.945738F);
            this.xrLine3.StylePriority.UseBorderDashStyle = false;
            // 
            // xrLabel19
            // 
            this.xrLabel19.Dpi = 100F;
            this.xrLabel19.Font = new System.Drawing.Font("Arial", 7F, System.Drawing.FontStyle.Bold);
            this.xrLabel19.LocationFloat = new DevExpress.Utils.PointFloat(341.245F, 0F);
            this.xrLabel19.Multiline = true;
            this.xrLabel19.Name = "xrLabel19";
            this.xrLabel19.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel19.SizeF = new System.Drawing.SizeF(42.47113F, 17.60773F);
            this.xrLabel19.StylePriority.UseFont = false;
            this.xrLabel19.StylePriority.UseTextAlignment = false;
            this.xrLabel19.Text = "IEPS";
            this.xrLabel19.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrLabel18
            // 
            this.xrLabel18.Dpi = 100F;
            this.xrLabel18.Font = new System.Drawing.Font("Arial", 7F, System.Drawing.FontStyle.Bold);
            this.xrLabel18.LocationFloat = new DevExpress.Utils.PointFloat(276.0537F, 0F);
            this.xrLabel18.Name = "xrLabel18";
            this.xrLabel18.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel18.SizeF = new System.Drawing.SizeF(65.19127F, 17.55346F);
            this.xrLabel18.StylePriority.UseFont = false;
            this.xrLabel18.StylePriority.UseTextAlignment = false;
            this.xrLabel18.Text = "Ventas";
            this.xrLabel18.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrLabel17
            // 
            this.xrLabel17.Dpi = 100F;
            this.xrLabel17.Font = new System.Drawing.Font("Arial", 7F, System.Drawing.FontStyle.Bold);
            this.xrLabel17.LocationFloat = new DevExpress.Utils.PointFloat(0F, 0F);
            this.xrLabel17.Name = "xrLabel17";
            this.xrLabel17.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel17.SizeF = new System.Drawing.SizeF(78.87255F, 17.55346F);
            this.xrLabel17.StylePriority.UseFont = false;
            this.xrLabel17.StylePriority.UseTextAlignment = false;
            this.xrLabel17.Text = "Artículo";
            this.xrLabel17.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // TopMargin
            // 
            this.TopMargin.Dpi = 100F;
            this.TopMargin.HeightF = 30F;
            this.TopMargin.Name = "TopMargin";
            this.TopMargin.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.TopMargin.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // BottomMargin
            // 
            this.BottomMargin.Dpi = 100F;
            this.BottomMargin.HeightF = 55F;
            this.BottomMargin.Name = "BottomMargin";
            this.BottomMargin.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.BottomMargin.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // ReportHeader
            // 
            this.ReportHeader.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel10,
            this.xrPictureBox2,
            this.Date,
            this.xrLabel9,
            this.xrLabel4,
            this.siteLabel,
            this.xrLabel3});
            this.ReportHeader.Dpi = 100F;
            this.ReportHeader.HeightF = 120.9483F;
            this.ReportHeader.Name = "ReportHeader";
            // 
            // xrLabel10
            // 
            this.xrLabel10.Dpi = 100F;
            this.xrLabel10.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel10.LocationFloat = new DevExpress.Utils.PointFloat(9.45836F, 10.00002F);
            this.xrLabel10.Name = "xrLabel10";
            this.xrLabel10.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel10.SizeF = new System.Drawing.SizeF(357.1429F, 56.16665F);
            this.xrLabel10.StylePriority.UseFont = false;
            this.xrLabel10.StylePriority.UseTextAlignment = false;
            this.xrLabel10.Text = "REPORTE DE UTILIDAD POR DEPARTAMENTO DETALLADO PV";
            this.xrLabel10.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrPictureBox2
            // 
            this.xrPictureBox2.Dpi = 100F;
            this.xrPictureBox2.Image = ((System.Drawing.Image)(resources.GetObject("xrPictureBox2.Image")));
            this.xrPictureBox2.ImageAlignment = DevExpress.XtraPrinting.ImageAlignment.TopLeft;
            this.xrPictureBox2.LocationFloat = new DevExpress.Utils.PointFloat(632.814F, 0F);
            this.xrPictureBox2.Name = "xrPictureBox2";
            this.xrPictureBox2.SizeF = new System.Drawing.SizeF(91.18595F, 73.16666F);
            this.xrPictureBox2.Sizing = DevExpress.XtraPrinting.ImageSizeMode.StretchImage;
            // 
            // Date
            // 
            this.Date.Dpi = 100F;
            this.Date.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Date.LocationFloat = new DevExpress.Utils.PointFloat(78.87258F, 74.27379F);
            this.Date.Name = "Date";
            this.Date.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.Date.SizeF = new System.Drawing.SizeF(220.7245F, 16.75002F);
            this.Date.StylePriority.UseFont = false;
            // 
            // xrLabel9
            // 
            this.xrLabel9.Dpi = 100F;
            this.xrLabel9.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel9.LocationFloat = new DevExpress.Utils.PointFloat(9.999956F, 74.27379F);
            this.xrLabel9.Name = "xrLabel9";
            this.xrLabel9.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel9.SizeF = new System.Drawing.SizeF(68.87256F, 16.75002F);
            this.xrLabel9.StylePriority.UseFont = false;
            this.xrLabel9.Text = "Fecha:";
            // 
            // xrLabel4
            // 
            this.xrLabel4.Dpi = 100F;
            this.xrLabel4.Font = new System.Drawing.Font("Arial", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel4.LocationFloat = new DevExpress.Utils.PointFloat(544.5886F, 40.00001F);
            this.xrLabel4.Name = "xrLabel4";
            this.xrLabel4.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel4.SizeF = new System.Drawing.SizeF(86.82587F, 15F);
            this.xrLabel4.StylePriority.UseFont = false;
            this.xrLabel4.StylePriority.UseTextAlignment = false;
            this.xrLabel4.Text = "DFL-950802-5N4";
            this.xrLabel4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // siteLabel
            // 
            this.siteLabel.Dpi = 100F;
            this.siteLabel.Font = new System.Drawing.Font("Arial", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.siteLabel.LocationFloat = new DevExpress.Utils.PointFloat(427.3088F, 25.00002F);
            this.siteLabel.Name = "siteLabel";
            this.siteLabel.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.siteLabel.SizeF = new System.Drawing.SizeF(204.1057F, 15F);
            this.siteLabel.StylePriority.UseFont = false;
            this.siteLabel.StylePriority.UseTextAlignment = false;
            this.siteLabel.Text = "*FLORIDO CEDIS";
            this.siteLabel.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // xrLabel3
            // 
            this.xrLabel3.Dpi = 100F;
            this.xrLabel3.Font = new System.Drawing.Font("Arial", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel3.LocationFloat = new DevExpress.Utils.PointFloat(427.3088F, 10.00002F);
            this.xrLabel3.Name = "xrLabel3";
            this.xrLabel3.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel3.SizeF = new System.Drawing.SizeF(204.1053F, 15F);
            this.xrLabel3.StylePriority.UseFont = false;
            this.xrLabel3.StylePriority.UseTextAlignment = false;
            this.xrLabel3.Text = "DISTRIBUIDORA EL FLORIDO S.A. DE C.V.";
            this.xrLabel3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // GroupFooter1
            // 
            this.GroupFooter1.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.GreatPercentage,
            this.GreatUtility,
            this.GreatCost,
            this.GreatTotal,
            this.GreatIeps,
            this.GreatSales,
            this.xrLabel11});
            this.GroupFooter1.Dpi = 100F;
            this.GroupFooter1.HeightF = 28.70834F;
            this.GroupFooter1.Name = "GroupFooter1";
            // 
            // GreatPercentage
            // 
            this.GreatPercentage.Dpi = 100F;
            this.GreatPercentage.Font = new System.Drawing.Font("Arial", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.GreatPercentage.LocationFloat = new DevExpress.Utils.PointFloat(600.4951F, 10F);
            this.GreatPercentage.Name = "GreatPercentage";
            this.GreatPercentage.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.GreatPercentage.SizeF = new System.Drawing.SizeF(81.0127F, 15F);
            this.GreatPercentage.StylePriority.UseFont = false;
            this.GreatPercentage.StylePriority.UseTextAlignment = false;
            this.GreatPercentage.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // GreatUtility
            // 
            this.GreatUtility.Dpi = 100F;
            this.GreatUtility.Font = new System.Drawing.Font("Arial", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.GreatUtility.LocationFloat = new DevExpress.Utils.PointFloat(519.4822F, 10F);
            this.GreatUtility.Name = "GreatUtility";
            this.GreatUtility.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.GreatUtility.SizeF = new System.Drawing.SizeF(81.01282F, 15F);
            this.GreatUtility.StylePriority.UseFont = false;
            this.GreatUtility.StylePriority.UseTextAlignment = false;
            this.GreatUtility.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // GreatCost
            // 
            this.GreatCost.Dpi = 100F;
            this.GreatCost.Font = new System.Drawing.Font("Arial", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.GreatCost.LocationFloat = new DevExpress.Utils.PointFloat(438.4695F, 10F);
            this.GreatCost.Name = "GreatCost";
            this.GreatCost.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.GreatCost.SizeF = new System.Drawing.SizeF(81.01282F, 15F);
            this.GreatCost.StylePriority.UseFont = false;
            this.GreatCost.StylePriority.UseTextAlignment = false;
            this.GreatCost.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // GreatTotal
            // 
            this.GreatTotal.Dpi = 100F;
            this.GreatTotal.Font = new System.Drawing.Font("Arial", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.GreatTotal.LocationFloat = new DevExpress.Utils.PointFloat(352.4661F, 10F);
            this.GreatTotal.Name = "GreatTotal";
            this.GreatTotal.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.GreatTotal.SizeF = new System.Drawing.SizeF(86.00333F, 15F);
            this.GreatTotal.StylePriority.UseFont = false;
            this.GreatTotal.StylePriority.UseTextAlignment = false;
            this.GreatTotal.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // GreatIeps
            // 
            this.GreatIeps.Dpi = 100F;
            this.GreatIeps.Font = new System.Drawing.Font("Arial", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.GreatIeps.LocationFloat = new DevExpress.Utils.PointFloat(270.4115F, 10F);
            this.GreatIeps.Name = "GreatIeps";
            this.GreatIeps.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.GreatIeps.SizeF = new System.Drawing.SizeF(82.05453F, 15F);
            this.GreatIeps.StylePriority.UseFont = false;
            this.GreatIeps.StylePriority.UseTextAlignment = false;
            this.GreatIeps.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // GreatSales
            // 
            this.GreatSales.Dpi = 100F;
            this.GreatSales.Font = new System.Drawing.Font("Arial", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.GreatSales.LocationFloat = new DevExpress.Utils.PointFloat(191.6786F, 10F);
            this.GreatSales.Name = "GreatSales";
            this.GreatSales.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.GreatSales.SizeF = new System.Drawing.SizeF(78.73294F, 15F);
            this.GreatSales.StylePriority.UseFont = false;
            this.GreatSales.StylePriority.UseTextAlignment = false;
            this.GreatSales.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // xrLabel11
            // 
            this.xrLabel11.Dpi = 100F;
            this.xrLabel11.Font = new System.Drawing.Font("Arial", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel11.LocationFloat = new DevExpress.Utils.PointFloat(119.1592F, 10F);
            this.xrLabel11.Name = "xrLabel11";
            this.xrLabel11.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel11.SizeF = new System.Drawing.SizeF(72.51935F, 15F);
            this.xrLabel11.StylePriority.UseFont = false;
            this.xrLabel11.StylePriority.UseTextAlignment = false;
            this.xrLabel11.Text = "Total:";
            this.xrLabel11.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
            // 
            // PageFooter
            // 
            this.PageFooter.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrPageInfo1});
            this.PageFooter.Dpi = 100F;
            this.PageFooter.HeightF = 15.625F;
            this.PageFooter.Name = "PageFooter";
            // 
            // xrPageInfo1
            // 
            this.xrPageInfo1.Dpi = 100F;
            this.xrPageInfo1.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Bold);
            this.xrPageInfo1.Format = "Página {0} de {1}";
            this.xrPageInfo1.LocationFloat = new DevExpress.Utils.PointFloat(0F, 0F);
            this.xrPageInfo1.Name = "xrPageInfo1";
            this.xrPageInfo1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrPageInfo1.SizeF = new System.Drawing.SizeF(100F, 15F);
            this.xrPageInfo1.StylePriority.UseFont = false;
            // 
            // DetailReport
            // 
            this.DetailReport.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.Detail1});
            this.DetailReport.Dpi = 100F;
            this.DetailReport.Level = 0;
            this.DetailReport.Name = "DetailReport";
            // 
            // Detail1
            // 
            this.Detail1.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable1});
            this.Detail1.Dpi = 100F;
            this.Detail1.HeightF = 20F;
            this.Detail1.Name = "Detail1";
            // 
            // xrTable1
            // 
            this.xrTable1.BackColor = System.Drawing.Color.White;
            this.xrTable1.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrTable1.Dpi = 100F;
            this.xrTable1.Font = new System.Drawing.Font("Arial", 6F);
            this.xrTable1.LocationFloat = new DevExpress.Utils.PointFloat(0F, 0F);
            this.xrTable1.Name = "xrTable1";
            this.xrTable1.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow1});
            this.xrTable1.SizeF = new System.Drawing.SizeF(681.5078F, 20F);
            this.xrTable1.StylePriority.UseBackColor = false;
            this.xrTable1.StylePriority.UseBorders = false;
            this.xrTable1.StylePriority.UseFont = false;
            // 
            // xrTableRow1
            // 
            this.xrTableRow1.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.Item,
            this.Description,
            this.Qty,
            this.Sales,
            this.IEPS,
            this.Total,
            this.Cost,
            this.Utility,
            this.Percentage});
            this.xrTableRow1.Dpi = 100F;
            this.xrTableRow1.Name = "xrTableRow1";
            this.xrTableRow1.Weight = 1D;
            // 
            // Item
            // 
            this.Item.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.Item.Dpi = 100F;
            this.Item.Font = new System.Drawing.Font("Arial", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Item.Name = "Item";
            this.Item.StylePriority.UseBorders = false;
            this.Item.StylePriority.UseFont = false;
            this.Item.StylePriority.UseTextAlignment = false;
            this.Item.Text = "Item";
            this.Item.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.Item.Weight = 0.43247360025307896D;
            // 
            // Description
            // 
            this.Description.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.Description.Dpi = 100F;
            this.Description.Font = new System.Drawing.Font("Arial", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Description.Name = "Description";
            this.Description.StylePriority.UseBorders = false;
            this.Description.StylePriority.UseFont = false;
            this.Description.StylePriority.UseTextAlignment = false;
            this.Description.Text = "Description";
            this.Description.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.Description.Weight = 0.618537760849788D;
            // 
            // Qty
            // 
            this.Qty.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.Qty.Dpi = 100F;
            this.Qty.Font = new System.Drawing.Font("Arial", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Qty.Name = "Qty";
            this.Qty.StylePriority.UseBorders = false;
            this.Qty.StylePriority.UseFont = false;
            this.Qty.StylePriority.UseTextAlignment = false;
            this.Qty.Text = "Qty";
            this.Qty.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.Qty.Weight = 0.46264465414436723D;
            // 
            // Sales
            // 
            this.Sales.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.Sales.Dpi = 100F;
            this.Sales.Font = new System.Drawing.Font("Arial", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Sales.Multiline = true;
            this.Sales.Name = "Sales";
            this.Sales.StylePriority.UseBorders = false;
            this.Sales.StylePriority.UseFont = false;
            this.Sales.StylePriority.UseTextAlignment = false;
            this.Sales.Text = "Sales";
            this.Sales.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.Sales.Weight = 0.35745648595554508D;
            // 
            // IEPS
            // 
            this.IEPS.Dpi = 100F;
            this.IEPS.Font = new System.Drawing.Font("Arial", 7F);
            this.IEPS.Multiline = true;
            this.IEPS.Name = "IEPS";
            this.IEPS.StylePriority.UseFont = false;
            this.IEPS.StylePriority.UseTextAlignment = false;
            this.IEPS.Text = "IEPS";
            this.IEPS.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.IEPS.Weight = 0.23287764148147389D;
            // 
            // Total
            // 
            this.Total.Dpi = 100F;
            this.Total.Font = new System.Drawing.Font("Arial", 7F);
            this.Total.Name = "Total";
            this.Total.StylePriority.UseFont = false;
            this.Total.StylePriority.UseTextAlignment = false;
            this.Total.Text = "Total";
            this.Total.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.Total.Weight = 0.47157329702489376D;
            // 
            // Cost
            // 
            this.Cost.Dpi = 100F;
            this.Cost.Font = new System.Drawing.Font("Arial", 7F);
            this.Cost.Name = "Cost";
            this.Cost.StylePriority.UseFont = false;
            this.Cost.StylePriority.UseTextAlignment = false;
            this.Cost.Text = "Cost";
            this.Cost.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.Cost.Weight = 0.44420886904056023D;
            // 
            // Utility
            // 
            this.Utility.Dpi = 100F;
            this.Utility.Font = new System.Drawing.Font("Arial", 7F);
            this.Utility.Name = "Utility";
            this.Utility.StylePriority.UseFont = false;
            this.Utility.StylePriority.UseTextAlignment = false;
            this.Utility.Text = "Utility";
            this.Utility.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.Utility.Weight = 0.44420870170674787D;
            // 
            // Percentage
            // 
            this.Percentage.Dpi = 100F;
            this.Percentage.Font = new System.Drawing.Font("Arial", 7F);
            this.Percentage.Name = "Percentage";
            this.Percentage.StylePriority.UseFont = false;
            this.Percentage.StylePriority.UseTextAlignment = false;
            this.Percentage.Text = "Percentage";
            this.Percentage.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.Percentage.Weight = 0.27285845928264463D;
            // 
            // UtilitiesReport
            // 
            this.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.Detail,
            this.TopMargin,
            this.BottomMargin,
            this.ReportHeader,
            this.GroupFooter1,
            this.PageFooter,
            this.DetailReport});
            this.Margins = new System.Drawing.Printing.Margins(92, 34, 30, 55);
            this.Version = "16.2";
            ((System.ComponentModel.ISupportInitialize)(this.xrTable1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

        }

        #endregion

        private DevExpress.XtraReports.UI.DetailBand Detail;
        private DevExpress.XtraReports.UI.TopMarginBand TopMargin;
        private DevExpress.XtraReports.UI.BottomMarginBand BottomMargin;
        private DevExpress.XtraReports.UI.ReportHeaderBand ReportHeader;
        private DevExpress.XtraReports.UI.GroupFooterBand GroupFooter1;
        private DevExpress.XtraReports.UI.PageFooterBand PageFooter;
        private DevExpress.XtraReports.UI.XRPageInfo xrPageInfo1;
        private DevExpress.XtraReports.UI.DetailReportBand DetailReport;
        private DevExpress.XtraReports.UI.DetailBand Detail1;
        private DevExpress.XtraReports.UI.XRTable xrTable1;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow1;
        private DevExpress.XtraReports.UI.XRTableCell Sales;
        private DevExpress.XtraReports.UI.XRTableCell IEPS;
        private DevExpress.XtraReports.UI.XRTableCell Item;
        private DevExpress.XtraReports.UI.XRLabel xrLabel4;
        private DevExpress.XtraReports.UI.XRLabel siteLabel;
        private DevExpress.XtraReports.UI.XRLabel xrLabel3;
        private DevExpress.XtraReports.UI.XRLabel Date;
        private DevExpress.XtraReports.UI.XRLabel xrLabel9;
        private DevExpress.XtraReports.UI.XRLabel xrLabel19;
        private DevExpress.XtraReports.UI.XRLabel xrLabel18;
        private DevExpress.XtraReports.UI.XRLabel xrLabel17;
        private DevExpress.XtraReports.UI.XRLine xrLine3;
        private DevExpress.XtraReports.UI.XRLabel GreatSales;
        private DevExpress.XtraReports.UI.XRLabel xrLabel11;
        private DevExpress.XtraReports.UI.XRPictureBox xrPictureBox2;
        private DevExpress.XtraReports.UI.XRLabel xrLabel10;
        private DevExpress.XtraReports.UI.XRLabel xrLabel5;
        private DevExpress.XtraReports.UI.XRTableCell Description;
        private DevExpress.XtraReports.UI.XRLabel xrLabel7;
        private DevExpress.XtraReports.UI.XRLabel xrLabel6;
        private DevExpress.XtraReports.UI.XRLabel xrLabel2;
        private DevExpress.XtraReports.UI.XRLabel Descripcion;
        private DevExpress.XtraReports.UI.XRTableCell Total;
        private DevExpress.XtraReports.UI.XRTableCell Cost;
        private DevExpress.XtraReports.UI.XRTableCell Utility;
        private DevExpress.XtraReports.UI.XRTableCell Percentage;
        private DevExpress.XtraReports.UI.XRLabel GreatPercentage;
        private DevExpress.XtraReports.UI.XRLabel GreatUtility;
        private DevExpress.XtraReports.UI.XRLabel GreatCost;
        private DevExpress.XtraReports.UI.XRLabel GreatTotal;
        private DevExpress.XtraReports.UI.XRLabel GreatIeps;
        private DevExpress.XtraReports.UI.XRLabel xrLabel1;
        private DevExpress.XtraReports.UI.XRTableCell Qty;
    }
}
