﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
using System.Data;
using App.Entities.ViewModels.Item;
using System.Collections.Generic;
using App.Entities;

namespace FloridoERPTX.Reports
{
    public partial class SalesPriceChangeCurrent : DevExpress.XtraReports.UI.XtraReport
    {
        public DataSet DataSetTableMovements = null;
        public SalesPriceChangeCurrent()
        {
            InitializeComponent();
        }

        public DataSet printTable(List<ItemCurrent> items, SITES site_info, string name, string dep, string fam)
        {
            try
            {
                DataSetTableMovements = new DataSet();

                DataTable table = new DataTable();
                DataSetTableMovements.Tables.Add(table);
                table.Columns.Add(new DataColumn("xrTableCellPartNumber"));
                table.Columns.Add(new DataColumn("xrTableCellPartDescription"));
                table.Columns.Add(new DataColumn("xrTableCellUnitSize"));
                table.Columns.Add(new DataColumn("xrBarCode1"));
                table.Columns.Add(new DataColumn("xrTableCellPrice"));

                xrlblDepartment.Text = dep;
                xrlblFamily.Text = fam;
                xrlblUser.Text = name;
                //Info Tienda
                xraddress.Text = site_info.address + " C.P." + site_info.postal_code;
                xrState.Text = site_info.city + ", " + site_info.state;
                siteLabel.Text = "*" + site_info.site_name;
                xrDateShow.Text = DateTime.Now.ToShortDateString();

                foreach (var item in items)
                {
                    DataRow DataRowOrdered = DataSetTableMovements.Tables[0].NewRow();
                    DataRowOrdered["xrTableCellPartNumber"] = item.PartNumber;
                    DataRowOrdered["xrTableCellPartDescription"] = item.DescriptionReal;
                    DataRowOrdered["xrTableCellUnitSize"] = item.FactorUnitSize;
                    DataRowOrdered["xrBarCode1"] = item.PartNumber;
                    DataRowOrdered["xrTableCellPrice"] = "$" + item.TaxPrice; //Con Iva
                    DataSetTableMovements.Tables[0].Rows.Add(DataRowOrdered);
                }
                xrTableCellPartNumber.DataBindings.Add(new XRBinding("Text", null, "xrTableRow1.xrTableCellPartNumber"));
                xrTableCellPartDescription.DataBindings.Add(new XRBinding("Text", null, "xrTableRow1.xrTableCellPartDescription"));
                xrTableCellUnitSize.DataBindings.Add(new XRBinding("Text", null, "xrTableRow1.xrTableCellUnitSize"));
                xrBarCode1.DataBindings.Add(new XRBinding("Text", null, "xrTableRow1.xrBarCode1"));
                xrTableCellPrice.DataBindings.Add(new XRBinding("Text", null, "xrTableRow1.xrTableCellPrice"));
                return DataSetTableMovements;
            }
            catch (Exception e)
            {
                var msg = e.Message;
                throw;
            }
        }
    }
}
