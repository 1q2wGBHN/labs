﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
using System.Data;
using App.Entities.ViewModels.DamagedsGoods;

namespace FloridoERPTX.Reports
{
    public partial class RMAReport : DevExpress.XtraReports.UI.XtraReport
    {
        public DataSet DataSetTableOrdered = null;
        public RMAReport()
        {
            InitializeComponent();
        }
        public DataSet printTable(DamagedGoodsHeadModel damaged)
        {
            try
            {
                DataSetTableOrdered = new DataSet();

                DataTable DataTableOrdered = new DataTable();
                DataSetTableOrdered.Tables.Add(DataTableOrdered);

                xrLabelSupplier.Text = damaged.Supplier;
                xrLabelDate.Text = DateTime.Now.ToString("dd/MM/yyyy");
                xrLabelFolio.Text = damaged.DamagedGoodsDoc.ToString();
                xrLabelSite.Text = damaged.SiteName;

                DataTableOrdered.Columns.Add(new DataColumn("xrTableCellPartNumber"));
                DataTableOrdered.Columns.Add(new DataColumn("xrTableCellName"));
                DataTableOrdered.Columns.Add(new DataColumn("xrTableCellSize"));
                DataTableOrdered.Columns.Add(new DataColumn("xrTableCellQuantity"));
                DataTableOrdered.Columns.Add(new DataColumn("xrTableCellPrice"));
                DataTableOrdered.Columns.Add(new DataColumn("xrTableCellStorage"));
                DataTableOrdered.Columns.Add(new DataColumn("xrTableCellQuantityRefund"));
                DataTableOrdered.Columns.Add(new DataColumn("xrTableCellAmountRefund"));

                //decimal subtotal = 0;
                //decimal ivatotal = 0;
                //decimal iepstotal = 0;
                foreach (var item in damaged.DamageGoodsItem)
                {
                    DataRow DataRowOrdered = DataSetTableOrdered.Tables[0].NewRow();
                    DataRowOrdered["xrTableCellPartNumber"] = item.PartNumber;
                    DataRowOrdered["xrTableCellName"] = item.Description;
                    DataRowOrdered["xrTableCellSize"] = item.Size;
                    DataRowOrdered["xrTableCellQuantity"] = item.Block.ToString("N4");
                    DataRowOrdered["xrTableCellPrice"] = item.Price.ToString("C4");
                    DataRowOrdered["xrTableCellStorage"] = item.Storage;
                    DataRowOrdered["xrTableCellQuantityRefund"] = item.Quantity;
                    DataRowOrdered["xrTableCellAmountRefund"] = (item.Quantity * item.Price).ToString("C4");
                    //subtotal += item.Import;
                    //ivatotal += item.Iva;
                    //iepstotal += item.Ieps;
                    DataSetTableOrdered.Tables[0].Rows.Add(DataRowOrdered);
                }


                xrTableCellPartNumber.DataBindings.Add(new XRBinding("Text", null, "xrTableRow1.xrTableCellPartNumber"));
                xrTableCellName.DataBindings.Add(new XRBinding("Text", null, "xrTableRow1.xrTableCellName"));
                xrTableCellSize.DataBindings.Add(new XRBinding("Text", null, "xrTableRow1.xrTableCellSize"));
                xrTableCellQuantity.DataBindings.Add(new XRBinding("Text", null, "xrTableRow1.xrTableCellQuantity"));
                xrTableCellPrice.DataBindings.Add(new XRBinding("Text", null, "xrTableRow1.xrTableCellPrice"));
                xrTableCellStorage.DataBindings.Add(new XRBinding("Text", null, "xrTableRow1.xrTableCellStorage"));
                xrTableCellQuantityRefund.DataBindings.Add(new XRBinding("Text", null, "xrTableRow1.xrTableCellQuantityRefund"));
                xrTableCellAmountRefund.DataBindings.Add(new XRBinding("Text", null, "xrTableRow1.xrTableCellAmountRefund"));

                return DataSetTableOrdered;
            }
            catch (Exception ex)
            {
                string d = ex.Message;
                throw;
            }

        }
    }
}
