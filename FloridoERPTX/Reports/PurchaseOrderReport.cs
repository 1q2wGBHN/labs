﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
using System.Data;
using App.Entities;
using App.Entities.ViewModels.PurchaseOrder;
using System.Collections.Generic;

namespace FloridoERPTX.Reports
{
    public partial class PurchaseOrderReport : DevExpress.XtraReports.UI.XtraReport
    {
        public DataSet DataSetTableMovements = null;
        public decimal iva, subtotal, ieps, total, ivausd, iepsusd, subtotalusd, totalusd = 0;
        public PurchaseOrderReport()
        {
            InitializeComponent();
        }


        public DataSet printTable(List<PurchaseOrderModel> POs, SITES site_info, DateTime? date1, DateTime? date2, string name)
        {
            try
            {
                DataSetTableMovements = new DataSet();
                DataTable table = new DataTable();
                DataSetTableMovements.Tables.Add(table);
                table.Columns.Add(new DataColumn("xrCellPO"));
                table.Columns.Add(new DataColumn("xrCellProvider"));
                table.Columns.Add(new DataColumn("xrCellETA"));
                table.Columns.Add(new DataColumn("xrCellInvoice"));
                table.Columns.Add(new DataColumn("xrCellCurrency"));
                table.Columns.Add(new DataColumn("xrCellImport"));
                table.Columns.Add(new DataColumn("xrCellIVA"));
                table.Columns.Add(new DataColumn("xrCellIEPS"));
                table.Columns.Add(new DataColumn("xrCellTotal"));
                table.Columns.Add(new DataColumn("xrCellStatus"));


                DateTime date = DateTime.Today;
                var firstDayOfMonth = new DateTime(date.Year, date.Month, 1);
                var lastDayOfMonth = firstDayOfMonth.AddMonths(1).AddDays(-1);
                DateTime BeginDate2 = date1 ?? firstDayOfMonth;
                DateTime EndDate2 = date2 ?? lastDayOfMonth;

                xrlblUser.Text = name;
                xrlblEndDate.Text = EndDate2.ToShortDateString();
                xrlblStartDate.Text = BeginDate2.ToShortDateString();
                xrDateShow.Text = DateTime.Now.ToShortDateString();

                //Info Tienda
                xraddress.Text = site_info.address + " C.P." + site_info.postal_code;
                xrState.Text = site_info.city + ", " + site_info.state;
                siteLabel.Text = "*" + site_info.site_name;
                xrDateShow.Text = DateTime.Now.ToShortDateString();

                foreach (var item in POs)
                {
                    DataRow DataRowOrdered = DataSetTableMovements.Tables[0].NewRow();
                    DataRowOrdered["xrCellPO"] = item.PurchaseNo;
                    DataRowOrdered["xrCellProvider"] = item.supplier_name;
                    DataRowOrdered["xrCellETA"] = item.Eta.ToShortDateString();
                    DataRowOrdered["xrCellInvoice"] = item.invoice;
                    DataRowOrdered["xrCellCurrency"] = item.Currency;
                    DataRowOrdered["xrCellImport"] = item.ItemAmount.ToString("C4");
                    DataRowOrdered["xrCellIVA"] = item.Iva.ToString("C4");
                    DataRowOrdered["xrCellIEPS"] = item.Ieps.ToString("C4");
                    DataRowOrdered["xrCellTotal"] = item.ItemTotalAmount.ToString("C4"); ;
                    DataRowOrdered["xrCellStatus"] = item.purchase_status_description;
                    DataSetTableMovements.Tables[0].Rows.Add(DataRowOrdered);

                    if(item.Currency == "MXN")
                    {
                        iva += item.Iva;
                        ieps += item.Ieps;
                        subtotal += item.ItemAmount;
                        total += item.ItemTotalAmount;
                    }
                    else
                    {
                        ivausd += item.Iva;
                        iepsusd += item.Ieps;
                        subtotalusd += item.ItemAmount;
                        totalusd += item.ItemTotalAmount;
                    }

                }
                xrlblMxnImport.Text = subtotal.ToString("C4");
                xrlblMxnIVA.Text = iva.ToString("C4");
                xrlblMxnIEPS.Text = ieps.ToString("C4");
                xrlblMxnTotal.Text = total.ToString("C4");

                xrlblUsdImport.Text =subtotalusd.ToString("C4");
                xrlblUsdIVA.Text = ivausd.ToString("C4");
                xrlblUsdIVA.Text = iepsusd.ToString("C4");
                xrlblUsdTotal.Text = totalusd.ToString("C4");

                xrCellPO.DataBindings.Add(new XRBinding("Text", null, "xrTablePurchase.xrCellPO"));
                xrCellProvider.DataBindings.Add(new XRBinding("Text", null, "xrTablePurchase.xrCellProvider"));
                xrCellETA.DataBindings.Add(new XRBinding("Text", null, "xrTablePurchase.xrCellETA"));
                xrCellInvoice.DataBindings.Add(new XRBinding("Text", null, "xrTablePurchase.xrCellInvoice"));
                xrCellCurrency.DataBindings.Add(new XRBinding("Text", null, "xrTablePurchase.xrCellCurrency"));
                xrCellImport.DataBindings.Add(new XRBinding("Text", null, "xrTablePurchase.xrCellImport"));
                xrCellIVA.DataBindings.Add(new XRBinding("Text", null, "xrTablePurchase.xrCellIVA"));
                xrCellIEPS.DataBindings.Add(new XRBinding("Text", null, "xrTablePurchase.xrCellIEPS"));
                xrCellTotal.DataBindings.Add(new XRBinding("Text", null, "xrTablePurchase.xrCellTotal"));
                xrCellStatus.DataBindings.Add(new XRBinding("Text", null, "xrTablePurchase.xrCellStatus"));
                return DataSetTableMovements;
            }
            catch (Exception e)
            {
                var msg = e.Message;
                throw;
            }
        }
    }
}
