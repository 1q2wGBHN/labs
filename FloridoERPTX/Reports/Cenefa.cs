﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
using System.Data;
using FloridoERPTX.ViewModels.Purchases;
using System.Collections.Generic;
using DevExpress.XtraReports.Configuration;

namespace FloridoERPTX.Reports
{
    public partial class Cenefa : DevExpress.XtraReports.UI.XtraReport
    {
        public Cenefa()
        {
            InitializeComponent();
        }
        public void Imprimir(string bindingMember)
        {
            if(bindingMember == "PART_NUMBER")
            {
                labelCode.DataBindings.Add("Text", null, bindingMember);
            }
            else if (bindingMember == "PART_DESCRIPTION")
            {
                labelDescription.DataBindings.Add("Text", null, bindingMember);
            }
            else
            {
                labelPrice.DataBindings.Add("Text", null, bindingMember);
            }      
        }
    }
}
