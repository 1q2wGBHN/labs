﻿namespace FloridoERPTX.Reports
{
    partial class InventoryLabelsReport
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            DevExpress.XtraPrinting.BarCode.Code93Generator code93Generator1 = new DevExpress.XtraPrinting.BarCode.Code93Generator();
            this.Detail = new DevExpress.XtraReports.UI.DetailBand();
            this.xrBarCode = new DevExpress.XtraReports.UI.XRBarCode();
            this.TopMargin = new DevExpress.XtraReports.UI.TopMarginBand();
            this.xrLabelDia = new DevExpress.XtraReports.UI.XRLabel();
            this.xrInventory = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabelTitle = new DevExpress.XtraReports.UI.XRLabel();
            this.BottomMargin = new DevExpress.XtraReports.UI.BottomMarginBand();
            this.xr = new DevExpress.XtraReports.UI.XRLabel();
            this.xrFolio = new DevExpress.XtraReports.UI.XRLabel();
            this.xrPictureBox1 = new DevExpress.XtraReports.UI.XRPictureBox();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // Detail
            // 
            this.Detail.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrPictureBox1,
            this.xrBarCode});
            this.Detail.Dpi = 100F;
            this.Detail.HeightF = 96.875F;
            this.Detail.Name = "Detail";
            this.Detail.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.Detail.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrBarCode
            // 
            this.xrBarCode.AutoModule = true;
            this.xrBarCode.Dpi = 100F;
            this.xrBarCode.LocationFloat = new DevExpress.Utils.PointFloat(154.2709F, 21.83336F);
            this.xrBarCode.Name = "xrBarCode";
            this.xrBarCode.Padding = new DevExpress.XtraPrinting.PaddingInfo(10, 10, 0, 0, 100F);
            this.xrBarCode.SizeF = new System.Drawing.SizeF(329.1667F, 65.04163F);
            this.xrBarCode.StylePriority.UseTextAlignment = false;
            this.xrBarCode.Symbology = code93Generator1;
            this.xrBarCode.Text = "1000002N000001";
            this.xrBarCode.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // TopMargin
            // 
            this.TopMargin.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrFolio,
            this.xr,
            this.xrLabelDia,
            this.xrInventory,
            this.xrLabelTitle});
            this.TopMargin.Dpi = 100F;
            this.TopMargin.HeightF = 76.18752F;
            this.TopMargin.Name = "TopMargin";
            this.TopMargin.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.TopMargin.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabelDia
            // 
            this.xrLabelDia.Dpi = 100F;
            this.xrLabelDia.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabelDia.LocationFloat = new DevExpress.Utils.PointFloat(189.9998F, 57.35419F);
            this.xrLabelDia.Name = "xrLabelDia";
            this.xrLabelDia.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabelDia.SizeF = new System.Drawing.SizeF(84.37497F, 18.83331F);
            this.xrLabelDia.StylePriority.UseFont = false;
            this.xrLabelDia.Text = "Inventario :";
            // 
            // xrInventory
            // 
            this.xrInventory.Dpi = 100F;
            this.xrInventory.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrInventory.LocationFloat = new DevExpress.Utils.PointFloat(48.95829F, 57.35419F);
            this.xrInventory.Name = "xrInventory";
            this.xrInventory.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrInventory.SizeF = new System.Drawing.SizeF(141.0416F, 18.83331F);
            this.xrInventory.StylePriority.UseFont = false;
            // 
            // xrLabelTitle
            // 
            this.xrLabelTitle.Dpi = 100F;
            this.xrLabelTitle.Font = new System.Drawing.Font("Times New Roman", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabelTitle.LocationFloat = new DevExpress.Utils.PointFloat(120.1459F, 12.64582F);
            this.xrLabelTitle.Name = "xrLabelTitle";
            this.xrLabelTitle.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabelTitle.SizeF = new System.Drawing.SizeF(396.25F, 26.125F);
            this.xrLabelTitle.StylePriority.UseFont = false;
            this.xrLabelTitle.StylePriority.UseTextAlignment = false;
            this.xrLabelTitle.Text = "Reporte de Etiquetas de Inventario";
            this.xrLabelTitle.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // BottomMargin
            // 
            this.BottomMargin.Dpi = 100F;
            this.BottomMargin.HeightF = 28.125F;
            this.BottomMargin.Name = "BottomMargin";
            this.BottomMargin.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.BottomMargin.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xr
            // 
            this.xr.Dpi = 100F;
            this.xr.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xr.LocationFloat = new DevExpress.Utils.PointFloat(0F, 57.35419F);
            this.xr.Name = "xr";
            this.xr.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xr.SizeF = new System.Drawing.SizeF(48.95828F, 18.83331F);
            this.xr.StylePriority.UseFont = false;
            this.xr.Text = "Folio :";
            // 
            // xrFolio
            // 
            this.xrFolio.Dpi = 100F;
            this.xrFolio.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrFolio.LocationFloat = new DevExpress.Utils.PointFloat(274.3748F, 57.35419F);
            this.xrFolio.Name = "xrFolio";
            this.xrFolio.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrFolio.SizeF = new System.Drawing.SizeF(190.2086F, 18.83331F);
            this.xrFolio.StylePriority.UseFont = false;
            // 
            // xrPictureBox1
            // 
            this.xrPictureBox1.Dpi = 100F;
            this.xrPictureBox1.LocationFloat = new DevExpress.Utils.PointFloat(0F, 0F);
            this.xrPictureBox1.Name = "xrPictureBox1";
            this.xrPictureBox1.SizeF = new System.Drawing.SizeF(100F, 23F);
            // 
            // InventoryLabelsReport
            // 
            this.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.Detail,
            this.TopMargin,
            this.BottomMargin});
            this.Margins = new System.Drawing.Printing.Margins(100, 100, 76, 28);
            this.Version = "16.2";
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

        }

        #endregion

        private DevExpress.XtraReports.UI.DetailBand Detail;
        private DevExpress.XtraReports.UI.TopMarginBand TopMargin;
        private DevExpress.XtraReports.UI.BottomMarginBand BottomMargin;
        private DevExpress.XtraReports.UI.XRLabel xrLabelDia;
        private DevExpress.XtraReports.UI.XRLabel xrInventory;
        private DevExpress.XtraReports.UI.XRLabel xrLabelTitle;
        private DevExpress.XtraReports.UI.XRBarCode xrBarCode;
        private DevExpress.XtraReports.UI.XRLabel xrFolio;
        private DevExpress.XtraReports.UI.XRLabel xr;
        private DevExpress.XtraReports.UI.XRPictureBox xrPictureBox1;
    }
}
