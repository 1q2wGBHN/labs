﻿using System;
using System.Data;
using DevExpress.XtraReports.UI;
using App.Entities.ViewModels.Invoices;
using System.Globalization;
using System.Linq;

namespace FloridoERPTX.Reports
{
    public partial class OldBalanceReportByCustomer : DevExpress.XtraReports.UI.XtraReport
    {
       
        public DataSet DataSetTableOrdered = null;         
        public OldBalanceReportByCustomer()
        {
            InitializeComponent();
          
        }
        public DataSet printTable(InvoiceOldBalanceReport Model, string CurrencyValue, string StoreValue) 
        {
            try
            {
                Date.Text = DateTime.Now.ToString("dd/MMMM/yyyy", CultureInfo.CreateSpecificCulture("es-ES"));
                Currency.Text = CurrencyValue;
                Store.Text = StoreValue;
                ClientName.Text = Model.CustomerName;
                Phone.Text = Model.Phone;
                Email.Text = Model.Email;
                TotalBalance.Text = "$" + Model.InvoicesList.Sum(c => c.Balance).ToString("0.00");

                DataSetTableOrdered = new DataSet();                              

                DataTable DataTableOrdered = new DataTable();
                DataSetTableOrdered.Tables.Add(DataTableOrdered);

                DataTableOrdered.Columns.Add(new DataColumn("InvoiceNo"));
                DataTableOrdered.Columns.Add(new DataColumn("InvoiceDate"));
                DataTableOrdered.Columns.Add(new DataColumn("DueDate"));
                DataTableOrdered.Columns.Add(new DataColumn("Days"));
                DataTableOrdered.Columns.Add(new DataColumn("TotalInvoice"));
                DataTableOrdered.Columns.Add(new DataColumn("SubTotal"));
                DataTableOrdered.Columns.Add(new DataColumn("IEPS"));
                DataTableOrdered.Columns.Add(new DataColumn("Tax"));
                DataTableOrdered.Columns.Add(new DataColumn("Balance"));               

                foreach(var item in Model.InvoicesList)
                {
                    DataRow DataRowOrdered = DataSetTableOrdered.Tables[0].NewRow();                    
                    DataRowOrdered["InvoiceNo"] = item.Number;
                    DataRowOrdered["InvoiceDate"] = item.Date;
                    DataRowOrdered["DueDate"] = item.DueDate;
                    DataRowOrdered["Days"] = item.Days;
                    DataRowOrdered["TotalInvoice"] = item.Total;
                    DataRowOrdered["SubTotal"] = item.SubTotal;
                    DataRowOrdered["IEPS"] = item.IEPS;
                    DataRowOrdered["Tax"] = item.Tax;
                    DataRowOrdered["Balance"] = item.Balance;

                    DataSetTableOrdered.Tables[0].Rows.Add(DataRowOrdered);
                }

                InvoiceTotal.Text = "$" + Model.InvoicesList.Sum(c => c.Total).ToString("0.00"); 
                InvoiceSubTotal.Text = "$" + Model.InvoicesList.Sum(c => c.SubTotal).ToString("0.00");
                IEPSTotal.Text = "$" + Model.InvoicesList.Sum(c => c.IEPS).ToString("0.00");
                TaxTotal.Text = "$" + Model.InvoicesList.Sum(c => c.Tax).ToString("0.00");
                BalanceTotal.Text = "$" + Model.InvoicesList.Sum(c => c.Balance).ToString("0.00");

                InvoiceNo.DataBindings.Add(new XRBinding("Text", null, "xrTableRowFormat.InvoiceNo"));
                InvoiceDate.DataBindings.Add(new XRBinding("Text", null, "xrTableRowFormat.InvoiceDate"));
                DueDate.DataBindings.Add(new XRBinding("Text", null, "xrTableRowFormat.DueDate"));
                Days.DataBindings.Add(new XRBinding("Text", null, "xrTableRowFormat.Days"));
                TotalInvoice.DataBindings.Add(new XRBinding("Text", null, "xrTableRowFormat.TotalInvoice"));
                SubTotal.DataBindings.Add(new XRBinding("Text", null, "xrTableRowFormat.SubTotal"));
                IEPS.DataBindings.Add(new XRBinding("Text", null, "xrTableRowFormat.IEPS"));
                Tax.DataBindings.Add(new XRBinding("Text", null, "xrTableRowFormat.Tax"));
                Balance.DataBindings.Add(new XRBinding("Text", null, "xrTableRowFormat.Balance"));

                return DataSetTableOrdered;
            }
            catch (Exception ex)
            {
                string d=ex.Message;
                throw;
            }          
        }       
    }
}
