﻿using System;
using System.Data;
using DevExpress.XtraReports.UI;
using App.Entities.ViewModels.Withdrawal;
using System.Linq;

namespace FloridoERPTX.Reports
{
    public partial class WithdrawalHeaderTotalReport : DevExpress.XtraReports.UI.XtraReport
    {
       
        public DataSet DataSetTableOrdered = null;        
        public WithdrawalHeaderTotalReport()
        {
            InitializeComponent();
          
        }
        public DataSet printTable(WithdrawalIndex Model) 
        {
            try
            {
                siteLabel.Text = Model.SiteName;
                dateLabel.Text = Model.Date;
                DataSetTableOrdered = new DataSet();

                DataTable DataTableOrdered = new DataTable();
                DataSetTableOrdered.Tables.Add(DataTableOrdered);

                DataTableOrdered.Columns.Add(new DataColumn("Date"));
                DataTableOrdered.Columns.Add(new DataColumn("Cashier"));                
                DataTableOrdered.Columns.Add(new DataColumn("Amount"));               

                foreach (var item in Model.AcumulateData)
                {
                    DataRow DataRowOrdered = DataSetTableOrdered.Tables[0].NewRow();
                    DataRowOrdered["Cashier"] = item.Cashier;
                    DataRowOrdered["Date"] = Model.Date;                   
                    DataRowOrdered["Amount"] = string.Format("${0}", item.Amount.ToString("0.00"));                    
                    
                    
                    DataSetTableOrdered.Tables[0].Rows.Add(DataRowOrdered);                                                        
                }

                total.Text = string.Format("{0}{1}", "$", Model.AcumulateData.Sum(c => c.Amount).ToString("0.00"));

                Date.DataBindings.Add(new XRBinding("Text", null, "xrTableRowFormat.Date"));
                Cashier.DataBindings.Add(new XRBinding("Text", null, "xrTableRowFormat.Cashier"));
                Amount.DataBindings.Add(new XRBinding("Text", null, "xrTableRowFormat.Amount"));               
                
                return DataSetTableOrdered;
            }
            catch (Exception ex)
            {
                string d=ex.Message;
                throw;
            }
          
        }       
    }
}
