﻿using System;
using System.Data;
using DevExpress.XtraReports.UI;
using System.Linq;
using App.Entities.ViewModels.Sales;

namespace FloridoERPTX.Reports
{
    public partial class ChargeSaleRelationReport : DevExpress.XtraReports.UI.XtraReport
    {
       
        public DataSet DataSetTableOrdered = null;        
        public ChargeSaleRelationReport()
        {
            InitializeComponent();
          
        }
        public DataSet printTable(ChargeRelationSalesIndex Model) 
        {
            try
            {
                decimal TotalMXN = Model.DataReport.Where(c => c.Currency == "MXN").Sum(d => d.Amount);
                decimal TotalUSD = Model.DataReport.Where(c => c.Currency == "USD").Count() > 0 ? Model.DataReport.Where(c => c.Currency == "USD").Sum(d => d.Amount) : 0;
                string TotalValue = (TotalMXN + (TotalUSD * Model.CurrencyRate)).ToString("0.00");

                DateRange.Text = string.Format("{0}-{1}", Model.Date, Model.EndDate);
                DataSetTableOrdered = new DataSet();
                siteLabel.Text = Model.SiteName;

                DataTable DataTableOrdered = new DataTable();
                DataSetTableOrdered.Tables.Add(DataTableOrdered);

                DataTableOrdered.Columns.Add(new DataColumn("Date"));
                DataTableOrdered.Columns.Add(new DataColumn("Cashier"));
                DataTableOrdered.Columns.Add(new DataColumn("Amount"));
                DataTableOrdered.Columns.Add(new DataColumn("PaymentType"));

                foreach (var item in Model.DataReport)
                {
                    DataRow DataRowOrdered = DataSetTableOrdered.Tables[0].NewRow();
                    DataRowOrdered["Date"] = item.Date.ToString("dd/MM/yyyy");
                    DataRowOrdered["Cashier"] = item.Cashier;
                    DataRowOrdered["Amount"] = string.Format("${0}", item.Amount.ToString("0.00"));
                    DataRowOrdered["PaymentType"] = item.PaymentType;
                    
                    
                    DataSetTableOrdered.Tables[0].Rows.Add(DataRowOrdered);                                                        
                }

                total.Text = string.Format("{0}{1}", "$", TotalValue);

                Date.DataBindings.Add(new XRBinding("Text", null, "xrTableRowFormat.Date"));
                Cashier.DataBindings.Add(new XRBinding("Text", null, "xrTableRowFormat.Cashier"));
                Amount.DataBindings.Add(new XRBinding("Text", null, "xrTableRowFormat.Amount"));
                PaymentType.DataBindings.Add(new XRBinding("Text", null, "xrTableRowFormat.PaymentType"));               
                
                return DataSetTableOrdered;
            }
            catch (Exception ex)
            {
                string d=ex.Message;
                throw;
            }
          
        }       
    }
}
