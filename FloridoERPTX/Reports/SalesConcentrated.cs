﻿using System;
using System.Data;
using System.Collections.Generic;
using App.DAL.PurchaseOrder;
using App.Entities;
using DevExpress.XtraReports.UI;
using App.BLL.Supplier;
using App.Entities.ViewModels.PurchaseOrder;
using App.Entities.ViewModels.Sales;

namespace FloridoERPTX.Reports
{
    public partial class SalesConcentrated : DevExpress.XtraReports.UI.XtraReport
    {
       
        public DataSet DataSetTableOrdered = null;
        decimal ivaValue, iepsValue,totalValue,subTotal = 0;
        public SalesConcentrated()
        {
            InitializeComponent();
          
        }
        public DataSet printTable(List<SalesModelConc> salesList, string date1, string date2, string siteName) //se encarga de imprimir la orden del proveedor
        {
            try
            {

                labelDate.Text = DateTime.Now.Date.ToShortDateString();
                labelDate1.Text = date1;
                labelDate2.Text = date2;
                siteLabel.Text = siteName;
                DataSetTableOrdered = new DataSet();

                DataTable DataTableOrdered = new DataTable();
                DataSetTableOrdered.Tables.Add(DataTableOrdered);

                
               
                 
                DataTableOrdered.Columns.Add(new DataColumn("fecha"));

                DataTableOrdered.Columns.Add(new DataColumn("cn_subtotal"));
                DataTableOrdered.Columns.Add(new DataColumn("cn_iva"));
                DataTableOrdered.Columns.Add(new DataColumn("cn_ieps"));
                DataTableOrdered.Columns.Add(new DataColumn("cn_total"));

                DataTableOrdered.Columns.Add(new DataColumn("bon_subtotal"));
                DataTableOrdered.Columns.Add(new DataColumn("bon_iva"));
                DataTableOrdered.Columns.Add(new DataColumn("bon_ieps"));
                DataTableOrdered.Columns.Add(new DataColumn("bon_total"));

                DataTableOrdered.Columns.Add(new DataColumn("fc_subtotal"));
                DataTableOrdered.Columns.Add(new DataColumn("fc_iva"));
                DataTableOrdered.Columns.Add(new DataColumn("fc_ieps"));
                DataTableOrdered.Columns.Add(new DataColumn("fc_total"));

                DataTableOrdered.Columns.Add(new DataColumn("dv_total"));
                DataTableOrdered.Columns.Add(new DataColumn("cu_total"))
                    ;
                DataTableOrdered.Columns.Add(new DataColumn("fac_subtotal"));
                DataTableOrdered.Columns.Add(new DataColumn("fac_iva"));
                DataTableOrdered.Columns.Add(new DataColumn("fac_ieps"));
                DataTableOrdered.Columns.Add(new DataColumn("fac_total"));

                DataTableOrdered.Columns.Add(new DataColumn("gt_subtotal"));
                DataTableOrdered.Columns.Add(new DataColumn("gt_iva"));
                DataTableOrdered.Columns.Add(new DataColumn("gt_ieps"));
                DataTableOrdered.Columns.Add(new DataColumn("gt_total"));
               





                foreach (var item in salesList)
                {
                    DataRow DataRowOrdered = DataSetTableOrdered.Tables[0].NewRow();
                    DataRowOrdered["fecha"] = item.fecha;

                    DataRowOrdered["cn_subtotal"] =  item.cn_subtotal.ToString("C"); ;
                    DataRowOrdered["cn_iva"] =  item.cn_iva.ToString("C"); ;
                    DataRowOrdered["cn_ieps"] =  item.cn_ieps.ToString("C");
                    DataRowOrdered["cn_total"] =  item.cn_total.ToString("C");

                    DataRowOrdered["bon_subtotal"] =  item.bon_subtotal.ToString("C"); ;
                    DataRowOrdered["bon_iva"] = item.bon_iva.ToString("C"); ;
                    DataRowOrdered["bon_ieps"] =  item.bon_ieps.ToString("C");
                    DataRowOrdered["bon_total"] =  item.bon_total.ToString("C");

                    DataRowOrdered["fc_subtotal"] =  item.fc_subtotal.ToString("C"); ;
                    DataRowOrdered["fc_iva"] =  item.fc_iva.ToString("C"); ;
                    DataRowOrdered["fc_ieps"] =  item.fc_ieps.ToString("C");
                    DataRowOrdered["fc_total"] =  item.fc_total.ToString("C");

                    DataRowOrdered["dv_total"] =  item.dv_total.ToString("C"); ;
                    DataRowOrdered["cu_total"] =  item.cu_total.ToString("C"); ;

                    DataRowOrdered["fac_subtotal"] =  item.fac_subtotal.ToString("C"); ;
                    DataRowOrdered["fac_iva"] =  item.fac_iva.ToString("C"); ;
                    DataRowOrdered["fac_ieps"] =  item.fac_ieps.ToString("C");
                    DataRowOrdered["fac_total"] =  item.fac_total.ToString("C");

                    DataRowOrdered["gt_subtotal"] =  item.gt_subtotal.ToString("C"); ;
                    DataRowOrdered["gt_iva"] =  item.gt_iva.ToString("C"); ;
                    DataRowOrdered["gt_ieps"] =  item.gt_ieps.ToString("C");
                    DataRowOrdered["gt_total"] =  item.gt_total.ToString("C");


                    DataSetTableOrdered.Tables[0].Rows.Add(DataRowOrdered);
                    ivaValue += item.gt_iva;
                    iepsValue += item.gt_ieps;
                    subTotal += item.gt_subtotal;
                    totalValue += item.gt_total;



                }
               
                ivaLabel.Text = ivaValue.ToString("C");
                iepsLabel.Text =  iepsValue.ToString("C");
                subtotalLabel.Text =  subTotal.ToString("C");
                totalLabel.Text =  totalValue.ToString("C");

                fecha.DataBindings.Add(new XRBinding("Text", null, "xrTableRowFormat.fecha"));

                cn_subtotal.DataBindings.Add(new XRBinding("Text", null, "xrTableRowFormat.cn_subtotal"));
                cn_iva.DataBindings.Add(new XRBinding("Text", null, "xrTableRowFormat.cn_iva"));
                cn_ieps.DataBindings.Add(new XRBinding("Text", null, "xrTableRowFormat.cn_ieps"));
                cn_total.DataBindings.Add(new XRBinding("Text", null, "xrTableRowFormat.cn_total"));

                bon_subtotal.DataBindings.Add(new XRBinding("Text", null, "xrTableRowFormat.bon_subtotal"));
                bon_iva.DataBindings.Add(new XRBinding("Text", null, "xrTableRowFormat.bon_iva"));
                bon_ieps.DataBindings.Add(new XRBinding("Text", null, "xrTableRowFormat.bon_ieps"));
                bon_total.DataBindings.Add(new XRBinding("Text", null, "xrTableRowFormat.bon_total"));

                fc_subtotal.DataBindings.Add(new XRBinding("Text", null, "xrTableRowFormat.fc_subtotal"));
                fc_iva.DataBindings.Add(new XRBinding("Text", null, "xrTableRowFormat.fc_iva"));
                fc_ieps.DataBindings.Add(new XRBinding("Text", null, "xrTableRowFormat.fc_ieps"));
                fc_total.DataBindings.Add(new XRBinding("Text", null, "xrTableRowFormat.fc_total"));

                dv_total.DataBindings.Add(new XRBinding("Text", null, "xrTableRowFormat.dv_total"));
                cu_total.DataBindings.Add(new XRBinding("Text", null, "xrTableRowFormat.cu_total"));

                fac_subtotal.DataBindings.Add(new XRBinding("Text", null, "xrTableRowFormat.fac_subtotal"));
                fac_iva.DataBindings.Add(new XRBinding("Text", null, "xrTableRowFormat.fac_iva"));
                fac_ieps.DataBindings.Add(new XRBinding("Text", null, "xrTableRowFormat.fac_ieps"));
                fac_total.DataBindings.Add(new XRBinding("Text", null, "xrTableRowFormat.fac_total"));

                gt_subtotal.DataBindings.Add(new XRBinding("Text", null, "xrTableRowFormat.gt_subtotal"));
                gt_iva.DataBindings.Add(new XRBinding("Text", null, "xrTableRowFormat.gt_iva"));
                gt_ieps.DataBindings.Add(new XRBinding("Text", null, "xrTableRowFormat.gt_ieps"));
                gt_total.DataBindings.Add(new XRBinding("Text", null, "xrTableRowFormat.gt_total"));




                return DataSetTableOrdered;
            }
            catch (Exception ex)
            {
                string d=ex.Message;
                throw;
            }
          
        }

       


    }
}
