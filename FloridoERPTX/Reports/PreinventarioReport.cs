﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
using System.Data;
using System.Collections.Generic;
using App.Entities.ViewModels.Inventory;

namespace FloridoERPTX.Reports
{
    public partial class PreinventarioReport : DevExpress.XtraReports.UI.XtraReport
    {
        public DataSet DataSetTableMovements = null;
        public PreinventarioReport()
        {
            InitializeComponent();
        }
        public DataSet printTable(List<InventoryItemLocationModel> items, string siteName, DateTime date, string location , string serial_number)
        {
            try
            {
                xrDate.Text = date.ToString();
                xrLabelSiteName.Text = siteName;
                xrLocation.Text = location;
                xrLabel.Text = serial_number;
                DataSetTableMovements = new DataSet();
                DataTable table = new DataTable();
                DataSetTableMovements.Tables.Add(table);
                table.Columns.Add(new DataColumn("cellPartNumber"));
                table.Columns.Add(new DataColumn("cellPartDescription"));
                table.Columns.Add(new DataColumn("cellCounted"));
                foreach (var item in items)
                {
                    DataRow DataRowOrdered = DataSetTableMovements.Tables[0].NewRow();
                    DataRowOrdered["cellPartNumber"] = item.part_number;
                    DataRowOrdered["cellPartDescription"] = item.product_name;
                    DataRowOrdered["cellCounted"] = item.quantity;
                    DataSetTableMovements.Tables[0].Rows.Add(DataRowOrdered);

                }
                cellPartNumber.DataBindings.Add(new XRBinding("Text", null, "xrTableRow1.cellPartNumber"));
                cellPartDescription.DataBindings.Add(new XRBinding("Text", null, "xrTableRow1.cellPartDescription"));
                cellCounted.DataBindings.Add(new XRBinding("Text", null, "xrTableRow1.cellCounted"));
                return DataSetTableMovements;
            }
            catch (Exception)
            {

                throw;
            }
        }

    }
}
