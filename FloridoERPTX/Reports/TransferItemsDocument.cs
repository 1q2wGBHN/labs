﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
using System.Data;
using App.Entities.ViewModels.Android;
using System.Collections.Generic;
using System.Linq;
using App.Entities.ViewModels.Transfer;

namespace FloridoERPTX.Reports
{
    public partial class TransferItemsDocument : DevExpress.XtraReports.UI.XtraReport
    {
        public DataSet DataSetTableOrdered = null;
        public TransferItemsDocument()
        {
            InitializeComponent();
        }
        public DataSet printTable(List<TransferDetailModel> transferItems, string folio, string transferSiteName, string destinationSiteCode)
        {
            try
            {
                DataSetTableOrdered = new DataSet();

                DataTable DataTableOrdered = new DataTable();
                DataSetTableOrdered.Tables.Add(DataTableOrdered);

                xrLabelDate.Text = transferItems.FirstOrDefault()?.TransferDate?.ToShortDateString() ?? "N/A";
                xrLabelDocumentNumber.Text = folio;
                xrLabelSite.Text = destinationSiteCode;
                xrLabel6.Text = transferSiteName;


                DataTableOrdered.Columns.Add(new DataColumn("xrTableCellPartNumber"));
                DataTableOrdered.Columns.Add(new DataColumn("xrTableCellPartDescription"));
                DataTableOrdered.Columns.Add(new DataColumn("xrTableCellQuantity"));
                DataTableOrdered.Columns.Add(new DataColumn("xrTableCellGr_quantity"));
                DataTableOrdered.Columns.Add(new DataColumn("xrTableCellImport"));
                DataTableOrdered.Columns.Add(new DataColumn("xrTableCellCost"));
                DataTableOrdered.Columns.Add(new DataColumn("xrTableCellIeps"));
                DataTableOrdered.Columns.Add(new DataColumn("xrTableCellIva"));
                DataTableOrdered.Columns.Add(new DataColumn("xrTableCellSubtotal"));

                decimal subtotal = 0;
                decimal ivatotal = 0;
                decimal iepstotal = 0;
                foreach (var item in transferItems)
                {
                    //Iva y IEPS estan calculados en base de datos
                    DataRow DataRowOrdered = DataSetTableOrdered.Tables[0].NewRow();
                    DataRowOrdered["xrTableCellPartNumber"] = item.PartNumber;
                    DataRowOrdered["xrTableCellPartDescription"] = item.Description;
                    DataRowOrdered["xrTableCellQuantity"] = Format2(item.Quantity);
                    DataRowOrdered["xrTableCellGr_quantity"] = Format2(item.GrQuantity ?? 0);
                    DataRowOrdered["xrTableCellCost"] = ReturnFormatMoney(item.UnitCost);
                    DataRowOrdered["xrTableCellSubtotal"] = ReturnFormatMoney((item.UnitCost * item.Quantity) == null ? 0 : (item.UnitCost * item.Quantity) - ((item.IEPS * item.Quantity) == null ? 0 : (item.IEPS * item.Quantity)) - ((item.Iva * item.Quantity) == null ? 0 : item.Iva * item.Quantity));
                    DataRowOrdered["xrTableCellIeps"] = ReturnFormatMoney((item.IEPS * item.Quantity) == null ? 0 : (item.IEPS * item.Quantity));
                    DataRowOrdered["xrTableCellIva"] = ReturnFormatMoney((item.Iva * item.Quantity) == null ? 0 : (item.Iva * item.Quantity));
                    DataRowOrdered["xrTableCellImport"] = ReturnFormatMoney((item.UnitCost * item.Quantity) == null ? 0 : (item.UnitCost * item.Quantity));
                    subtotal += item.SubTotal;
                    ivatotal += (item.Iva * item.Quantity) == null ? 0 : (item.Iva * item.Quantity);
                    iepstotal += (item.IEPS * item.Quantity) == null ? 0 : (item.IEPS * item.Quantity);
                    DataSetTableOrdered.Tables[0].Rows.Add(DataRowOrdered);
                }

                xrTableCellPartNumber.DataBindings.Add(new XRBinding("Text", null, "xrTableRow1.xrTableCellPartNumber"));
                xrTableCellPartDescription.DataBindings.Add(new XRBinding("Text", null, "xrTableRow1.xrTableCellPartDescription"));
                xrTableCellQuantity.DataBindings.Add(new XRBinding("Text", null, "xrTableRow1.xrTableCellQuantity"));
                xrTableCellGr_quantity.DataBindings.Add(new XRBinding("Text", null, "xrTableRow1.xrTableCellGr_quantity"));
                xrTableCellCost.DataBindings.Add(new XRBinding("Text", null, "xrTableRow1.xrTableCellCost"));
                xrTableCellIeps.DataBindings.Add(new XRBinding("Text", null, "xrTableRow1.xrTableCellIeps"));
                xrTableCellIva.DataBindings.Add(new XRBinding("Text", null, "xrTableRow1.xrTableCellIva"));
                xrTableCellSubtotal.DataBindings.Add(new XRBinding("Text", null, "xrTableRow1.xrTableCellSubtotal"));
                xrTableCellImport.DataBindings.Add(new XRBinding("Text", null, "xrTableRow1.xrTableCellImport"));
                xrLabelAmount.Text = ReturnFormatMoney((subtotal + ivatotal + iepstotal));
                xrLabelIVA.Text = ReturnFormatMoney(ivatotal);
                xrLabelIEPS.Text = ReturnFormatMoney(iepstotal);
                xrLabelSubTotal.Text = ReturnFormatMoney(subtotal);
                return DataSetTableOrdered;
            }
            catch (Exception ex)
            {
                string d = ex.Message;
                throw;
            }

        }

        public string ReturnFormatMoney(decimal value)
        {
            if (value != 0)
            {
                var c = value.ToString().Split('.');
                int decimales = Int32.Parse(c[1]);
                var total = "";
                if (decimales > 0)
                    total = value.ToString("C2");
                else
                    total = Int32.Parse(c[0]).ToString("C0");
                return total;
            }
            return "$0.00";
        }
        public string Format2(decimal value)
        {
            if (value != 0)
            {
                var c = value.ToString().Split('.');
                int decimales = Int32.Parse(c[1]);
                var total = "";
                if (decimales > 0)
                    total = value.ToString("F2");
                else
                    total = Int32.Parse(c[0]).ToString("F2");
                return total;
            }
            return "$0.00";
        }
    }
}
