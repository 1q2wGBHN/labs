﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
using System.Data;
using App.Entities.ViewModels.PurchaseOrder;
using System.Collections.Generic;
using App.Entities;

namespace FloridoERPTX.Reports
{
    public partial class PurchaseOrderReportDetail : DevExpress.XtraReports.UI.XtraReport
    {
        public DataSet DataSetTableMovements = null;
        public decimal iva, subtotal, ieps, total, ivausd, iepsusd, subtotalusd, totalusd = 0;
        public PurchaseOrderReportDetail()
        {
            InitializeComponent();
        }

        public DataSet printTable(List<PurchaseOrderItemDetailReportGeneral> POs, SITES site_info, DateTime? date1, DateTime? date2, string name)
        {
            try
            {
                DataSetTableMovements = new DataSet();
                DataTable table = new DataTable();
                DataSetTableMovements.Tables.Add(table);
                table.Columns.Add(new DataColumn("xrCellPO"));
                table.Columns.Add(new DataColumn("xrCellPartNumber"));
                table.Columns.Add(new DataColumn("xrCellDescription"));
                table.Columns.Add(new DataColumn("xrCellDepartment"));
                table.Columns.Add(new DataColumn("xrCellFamily"));
                table.Columns.Add(new DataColumn("xrCellETA"));
                table.Columns.Add(new DataColumn("xrCellProvider"));
                table.Columns.Add(new DataColumn("xrCellCurrency"));
                table.Columns.Add(new DataColumn("xrCellQuantity"));
                table.Columns.Add(new DataColumn("xrCellUnitCost"));
                table.Columns.Add(new DataColumn("xrCellImport"));
                table.Columns.Add(new DataColumn("xrCellIVA"));
                table.Columns.Add(new DataColumn("xrCellIEPS"));
                table.Columns.Add(new DataColumn("xrCellTotal"));
                table.Columns.Add(new DataColumn("xrCellStatus"));

                DateTime date = DateTime.Today;
                var firstDayOfMonth = new DateTime(date.Year, date.Month, 1);
                var lastDayOfMonth = firstDayOfMonth.AddMonths(1).AddDays(-1);
                DateTime BeginDate2 = date1 ?? firstDayOfMonth;
                DateTime EndDate2 = date2 ?? lastDayOfMonth;

                xrlblUser.Text = name;
                xrlblEndDate.Text = EndDate2.ToShortDateString();
                xrlblStartDate.Text = BeginDate2.ToShortDateString();
                xrDateShow.Text = DateTime.Now.ToShortDateString();

                //Info Tienda
                xraddress.Text = site_info.address + " C.P." + site_info.postal_code;
                xrState.Text = site_info.city + ", " + site_info.state;
                siteLabel.Text = "*" + site_info.site_name;
                xrDateShow.Text = DateTime.Now.ToShortDateString();

                foreach (var item in POs)
                {
                    DataRow DataRowOrdered = DataSetTableMovements.Tables[0].NewRow();
                    DataRowOrdered["xrCellPO"] = item.Folio;
                    DataRowOrdered["xrCellPartNumber"] = item.PartNumber;
                    DataRowOrdered["xrCellDescription"] = item.DescriptionC;
                    DataRowOrdered["xrCellDepartment"] = item.Department;
                    DataRowOrdered["xrCellFamily"] = item.Family;
                    DataRowOrdered["xrCellProvider"] = item.Supplier;
                    DataRowOrdered["xrCellETA"] = item.MerchandiseEntry != "" || item.MerchandiseEntry == null ? "Esperada para " + item.MerchandiseEntryEta : item.MerchandiseEntry;
                    DataRowOrdered["xrCellCurrency"] = item.Currency;
                    DataRowOrdered["xrCellQuantity"] = item.Quantity;
                    DataRowOrdered["xrCellUnitCost"] = item.CostUnit.ToString("C4");
                    DataRowOrdered["xrCellImport"] = item.SubTotal.ToString("C4");
                    DataRowOrdered["xrCellIVA"] = item.IVA.ToString("C4");
                    DataRowOrdered["xrCellIEPS"] = item.IEPS.ToString("C4");
                    DataRowOrdered["xrCellTotal"] = item.Amount.ToString("C4");
                    DataRowOrdered["xrCellStatus"] = item.Item_Status;
                    DataSetTableMovements.Tables[0].Rows.Add(DataRowOrdered);

                    if(item.Currency == "MXN")
                    {
                        iva += item.IVA;
                        ieps += item.IEPS;
                        subtotal += item.SubTotal;
                        total += item.Amount;
                    }
                    else
                    {
                        ivausd += item.IVA;
                        iepsusd += item.IEPS;
                        subtotalusd += item.SubTotal;
                        totalusd += item.Amount;
                    }
                }
                xrlblMxnImport.Text = subtotal.ToString("C4");
                xrlblMxnIVA.Text = iva.ToString("C4");
                xrlblMxnIEPS.Text = ieps.ToString("C4");
                xrlblMxnTotal.Text = total.ToString("C4");

                xrlblUsdImport.Text = subtotalusd.ToString("C4");
                xrlblUsdIVA.Text = ivausd.ToString("C4");
                xrlblUsdIVA.Text = iepsusd.ToString("C4");
                xrlblUsdTotal.Text = totalusd.ToString("C4");

                xrCellPO.DataBindings.Add(new XRBinding("Text", null, "xrTablePurchase.xrCellPO"));
                xrCellPartNumber.DataBindings.Add(new XRBinding("Text", null, "xrTablePurchase.xrCellPartNumber"));
                xrCellDescription.DataBindings.Add(new XRBinding("Text", null, "xrTablePurchase.xrCellDescription"));
                xrCellDepartment.DataBindings.Add(new XRBinding("Text", null, "xrTablePurchase.xrCellDepartment"));
                xrCellFamily.DataBindings.Add(new XRBinding("Text", null, "xrTablePurchase.xrCellFamily"));
                xrCellProvider.DataBindings.Add(new XRBinding("Text", null, "xrTablePurchase.xrCellProvider"));
                xrCellETA.DataBindings.Add(new XRBinding("Text", null, "xrTablePurchase.xrCellETA"));
                xrCellCurrency.DataBindings.Add(new XRBinding("Text", null, "xrTablePurchase.xrCellCurrency"));
                xrCellQuantity.DataBindings.Add(new XRBinding("Text", null, "xrTablePurchase.xrCellQuantity"));
                xrCellUnitCost.DataBindings.Add(new XRBinding("Text", null, "xrTablePurchase.xrCellUnitCost"));
                xrCellImport.DataBindings.Add(new XRBinding("Text", null, "xrTablePurchase.xrCellImport"));
                xrCellIVA.DataBindings.Add(new XRBinding("Text", null, "xrTablePurchase.xrCellIVA"));
                xrCellIEPS.DataBindings.Add(new XRBinding("Text", null, "xrTablePurchase.xrCellIEPS"));
                xrCellTotal.DataBindings.Add(new XRBinding("Text", null, "xrTablePurchase.xrCellTotal"));
                xrCellStatus.DataBindings.Add(new XRBinding("Text", null, "xrTablePurchase.xrCellStatus"));
                return DataSetTableMovements;
            }
            catch (Exception e)
            {
                var msg = e.Message;
                return null;
            }
        }
    }
}
