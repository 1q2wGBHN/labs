﻿using System;
using System.Data;
using System.Collections.Generic;
using App.DAL.PurchaseOrder;
using App.Entities;
using DevExpress.XtraReports.UI;
using App.BLL.Supplier;
using App.Entities.ViewModels.PurchaseOrder;
using App.Entities.ViewModels.Sales;
using System.Drawing;

namespace FloridoERPTX.Reports
{
    public partial class RecargasReport : DevExpress.XtraReports.UI.XtraReport
    {
       
        public DataSet DataSetTableOrdered = null;
        decimal ivaValue, comisionValue,totaComisionlValue,TotalValue,TotalRecargasValue = 0;
        public RecargasReport()
        {
            InitializeComponent();
          
        }
        public DataSet printTable(serviciosRecargas _ServiciosRecargas, string date1, string date2, string siteName) //se encarga de imprimir la orden del proveedor
        {
            try
            {

                labelDate.Text = DateTime.Now.Date.ToShortDateString();
                labelDate1.Text = date1;
                labelDate2.Text = date2;
                siteLabel.Text = siteName;
               

               
                foreach (var item in _ServiciosRecargas.Recargas)
                {
                    var rowHeight = 15f;

                    var row = new XRTableRow();
                    row.HeightF = rowHeight;

                    var cell1 = new XRTableCell();
                    cell1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
                    cell1.Font = new Font(cell1.Font.FontFamily, cell1.Font.Size, FontStyle.Bold);
                    cell1.Text = "    " + item.company.ToUpper();                   
                
                    row.Cells.Add(cell1);                 
                  
                    row.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
                    row.BackColor = Color.DarkGray;
                    _tableRecargas.Rows.Add(row);

                    //ENCABEZADO

                    row = new XRTableRow();
                    row.HeightF = rowHeight;

                    var cell1E = new XRTableCell();
                    cell1E.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
                    cell1E.Text = "FECHA";

                    var cell2E = new XRTableCell();
                    cell2E.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
                    cell2E.Text = "HORA";


                    var cell3E = new XRTableCell();
                    cell3E.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
                    cell3E.Text = "VENTA";

                    var cell4E = new XRTableCell();
                    cell4E.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
                    cell4E.Text = "TOTAL";

                    row.Cells.Add(cell1E);
                    row.Cells.Add(cell2E);
                    row.Cells.Add(cell3E);
                    row.Cells.Add(cell4E);

                    row.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
                    row.BackColor = Color.LightGray;
                    _tableRecargas.Rows.Add(row);

                    foreach (RecargasModelDetail _recDet in item.RecargasDetail)
                    {
                        

                        var rowd = new XRTableRow();
                        row.HeightF = rowHeight;

                        var cell1D = new XRTableCell();
                        cell1D.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
                        cell1D.Text = _recDet.fecha;

                        var cell2D = new XRTableCell();
                        cell2D.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
                        cell2D.Text = _recDet.hora;


                        var cell3D = new XRTableCell();
                        cell3D.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
                        cell3D.Text = _recDet.venta.ToString();

                        var cell4D = new XRTableCell();
                        cell4D.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
                        cell4D.Text = _recDet.total.ToString("C");

                        rowd.Cells.Add(cell1D);
                        rowd.Cells.Add(cell2D);
                        rowd.Cells.Add(cell3D);
                        rowd.Cells.Add(cell4D);

                        rowd.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
                        _tableRecargas.Rows.Add(rowd);

                      

                    }
                     row = new XRTableRow();
                    row.HeightF = rowHeight;

                    var cell1t = new XRTableCell();
                    cell1t.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
                    cell1t.Text = "";

                    var cell2t = new XRTableCell();
                    cell2t.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
                    cell2t.Text = "";

                    var cell3t = new XRTableCell();
                    cell3t.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
                    cell3t.Text = "TOTAL";

                    var cell4t = new XRTableCell();
                    cell4t.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
                    cell4t.Text = item.total.ToString("C");


                    row.Cells.Add(cell1t);
                    row.Cells.Add(cell2t);
                    row.Cells.Add(cell3t);
                    row.Cells.Add(cell4t);

                    row.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
                    _tableRecargas.Rows.Add(row);


                    row = new XRTableRow();
                    row.HeightF = rowHeight;
                    var cell1ES = new XRTableCell();
                    cell1ES.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
                    cell1ES.Text = "";
                    row.Cells.Add(cell1ES);                  

                    row.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
                    _tableRecargas.Rows.Add(row);

                    TotalRecargasValue += item.total;

                }

                _SumaTotalRecargas.Text = TotalRecargasValue.ToString("C");


                    return DataSetTableOrdered;
            }
            catch (Exception ex)
            {
                string d=ex.Message;
                throw;
            }
          
        }

       


    }
}
