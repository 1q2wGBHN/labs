﻿using System;
using System.Data;
using DevExpress.XtraReports.UI;
using App.Entities.ViewModels.Invoices;
using System.Globalization;
using System.Linq;
using static App.Entities.ViewModels.Common.Constants;
using System.Collections.Generic;
using System.Drawing;
using DevExpress.XtraPrinting;

namespace FloridoERPTX.Reports
{
    public partial class NewOldBalanceReport : DevExpress.XtraReports.UI.XtraReport
    {
       
        public DataSet DataSetTableOrdered = null;        
        public NewOldBalanceReport()
        {
            InitializeComponent();
          
        }
        public DataSet printTable(List<InvoiceOldBalanceReport> Model, string Currency, string Store) 
        {
            try
            {
                Date.Text = DateTime.Now.ToString(DateFormat.INT_DATE, CultureInfo.CreateSpecificCulture("es-ES"));
                CurrencyLabel.Text = Currency;
                StoreLabel.Text = Store;
                siteLabel.Text = Store;
                decimal GreatTotalIEPS = 0, GreatTotalIVA = 0, GreatTotal = 0, GreatSubtotal = 0, GreatBalance = 0;
                XRTableRow HeaderRow;
                foreach(var Client in Model)
                {
                    var rowHeightHeader = 15f;
                    HeaderRow = new XRTableRow();
                    HeaderRow.HeightF = rowHeightHeader;

                    var CustomerNameCell = new XRTableCell();
                    CustomerNameCell.TextAlignment = TextAlignment.MiddleCenter;
                    CustomerNameCell.Font = new Font(CustomerNameCell.Font.FontFamily, CustomerNameCell.Font.Size, FontStyle.Bold);
                    CustomerNameCell.Text = Client.CustomerName;
                    CustomerNameCell.WidthF = 350f;

                    var PhoneCell = new XRTableCell();
                    PhoneCell.TextAlignment = TextAlignment.MiddleCenter;
                    PhoneCell.Font = new Font(PhoneCell.Font.FontFamily, PhoneCell.Font.Size, FontStyle.Bold);
                    PhoneCell.Text = Client.Phone;
                    PhoneCell.WidthF = 200f;

                    var EmailCell = new XRTableCell();
                    EmailCell.TextAlignment = TextAlignment.MiddleCenter;
                    EmailCell.Font = new Font(EmailCell.Font.FontFamily, EmailCell.Font.Size, FontStyle.Bold);
                    EmailCell.Text = !string.IsNullOrEmpty(Client.Email) ? Client.Email.ToLower() : string.Empty;
                    EmailCell.WidthF = 200f;

                    var TotalBalanceCell = new XRTableCell();
                    TotalBalanceCell.TextAlignment = TextAlignment.MiddleCenter;
                    TotalBalanceCell.Font = new Font(TotalBalanceCell.Font.FontFamily, TotalBalanceCell.Font.Size, FontStyle.Bold);
                    TotalBalanceCell.Text = Client.TotalBalance.ToString("C");
                    TotalBalanceCell.WidthF = 150f;

                    HeaderRow.Cells.Add(CustomerNameCell);
                    HeaderRow.Cells.Add(PhoneCell);
                    HeaderRow.Cells.Add(EmailCell);
                    HeaderRow.Cells.Add(TotalBalanceCell);

                    HeaderRow.Borders = BorderSide.Bottom;
                    HeaderRow.BackColor = Color.DarkGray;
                    OldBalanceTable.Rows.Add(HeaderRow);

                    #region Details
                    //Headers of Invoice Details
                    HeaderRow = new XRTableRow();
                    HeaderRow.HeightF = rowHeightHeader;

                    var InvoiceNoHeaderCell = new XRTableCell();
                    InvoiceNoHeaderCell.TextAlignment = TextAlignment.MiddleCenter;
                    InvoiceNoHeaderCell.Text = "No. Factura";
                    InvoiceNoHeaderCell.Font = new Font(InvoiceNoHeaderCell.Font.FontFamily, InvoiceNoHeaderCell.Font.Size, FontStyle.Bold);

                    var InvoiceDateHeaderCell = new XRTableCell();
                    InvoiceDateHeaderCell.TextAlignment = TextAlignment.MiddleCenter;
                    InvoiceDateHeaderCell.Text = "Fecha Factura";
                    InvoiceDateHeaderCell.Font = new Font(InvoiceDateHeaderCell.Font.FontFamily, InvoiceDateHeaderCell.Font.Size, FontStyle.Bold);

                    var DueDateHeaderCell = new XRTableCell();
                    DueDateHeaderCell.TextAlignment = TextAlignment.MiddleCenter;
                    DueDateHeaderCell.Text = "Fecha Vencimiento";
                    DueDateHeaderCell.Font = new Font(DueDateHeaderCell.Font.FontFamily, DueDateHeaderCell.Font.Size, FontStyle.Bold);

                    var DaysHeaderCell = new XRTableCell();
                    DaysHeaderCell.TextAlignment = TextAlignment.MiddleCenter;
                    DaysHeaderCell.Text = "Días";
                    DaysHeaderCell.Font = new Font(DaysHeaderCell.Font.FontFamily, DaysHeaderCell.Font.Size, FontStyle.Bold);

                    var TotalInvoiceHeaderCell = new XRTableCell();
                    TotalInvoiceHeaderCell.TextAlignment = TextAlignment.MiddleCenter;
                    TotalInvoiceHeaderCell.Text = "Total Factura";
                    TotalInvoiceHeaderCell.Font = new Font(TotalInvoiceHeaderCell.Font.FontFamily, TotalInvoiceHeaderCell.Font.Size, FontStyle.Bold);

                    var SubtTotalInvoiceHeaderCell = new XRTableCell();
                    SubtTotalInvoiceHeaderCell.TextAlignment = TextAlignment.MiddleCenter;
                    SubtTotalInvoiceHeaderCell.Text = "Sub Total";
                    SubtTotalInvoiceHeaderCell.Font = new Font(SubtTotalInvoiceHeaderCell.Font.FontFamily, SubtTotalInvoiceHeaderCell.Font.Size, FontStyle.Bold);

                    var IEPSHeaderCell = new XRTableCell();
                    IEPSHeaderCell.TextAlignment = TextAlignment.MiddleCenter;
                    IEPSHeaderCell.Text = "IEPS";
                    IEPSHeaderCell.Font = new Font(IEPSHeaderCell.Font.FontFamily, IEPSHeaderCell.Font.Size, FontStyle.Bold);

                    var TAXHeaderCell = new XRTableCell();
                    TAXHeaderCell.TextAlignment = TextAlignment.MiddleCenter;
                    TAXHeaderCell.Text = "IVA Factura";
                    TAXHeaderCell.Font = new Font(TAXHeaderCell.Font.FontFamily, TAXHeaderCell.Font.Size, FontStyle.Bold);

                    var BalanceHeaderCell = new XRTableCell();
                    BalanceHeaderCell.TextAlignment = TextAlignment.MiddleCenter;
                    BalanceHeaderCell.Text = "Saldo";
                    BalanceHeaderCell.Font = new Font(BalanceHeaderCell.Font.FontFamily, BalanceHeaderCell.Font.Size, FontStyle.Bold);

                    HeaderRow.Cells.Add(InvoiceNoHeaderCell);
                    HeaderRow.Cells.Add(InvoiceDateHeaderCell);
                    HeaderRow.Cells.Add(DueDateHeaderCell);
                    HeaderRow.Cells.Add(DaysHeaderCell);
                    HeaderRow.Cells.Add(TotalInvoiceHeaderCell);
                    HeaderRow.Cells.Add(SubtTotalInvoiceHeaderCell);
                    HeaderRow.Cells.Add(IEPSHeaderCell);
                    HeaderRow.Cells.Add(TAXHeaderCell);
                    HeaderRow.Cells.Add(BalanceHeaderCell);

                    HeaderRow.Borders = BorderSide.Bottom;
                    HeaderRow.BackColor = Color.LightGray;
                    OldBalanceTable.Rows.Add(HeaderRow);

                    //Data of Invoice Details
                    foreach (var Detail in Client.InvoicesList)
                    {
                        var rowHeightDetail = 15f;
                        var DetailRow = new XRTableRow();
                        DetailRow.HeightF = rowHeightDetail;

                        var InvoiceNoDetailCell = new XRTableCell();
                        InvoiceNoDetailCell.TextAlignment = TextAlignment.MiddleCenter;
                        InvoiceNoDetailCell.Text = Detail.Number;

                        var InvoiceDateDetailCell = new XRTableCell();
                        InvoiceDateDetailCell.TextAlignment = TextAlignment.MiddleCenter;
                        InvoiceDateDetailCell.Text = Detail.Date;

                        var DueDateDetailCell = new XRTableCell();
                        DueDateDetailCell.TextAlignment = TextAlignment.MiddleCenter;
                        DueDateDetailCell.Text = Detail.DueDate;

                        var DaysDetailCell = new XRTableCell();
                        DaysDetailCell.TextAlignment = TextAlignment.MiddleCenter;
                        DaysDetailCell.Text = Detail.Days.ToString();

                        var TotalInvoiceDetailCell = new XRTableCell();
                        TotalInvoiceDetailCell.TextAlignment = TextAlignment.MiddleCenter;
                        TotalInvoiceDetailCell.Text = Detail.Total.ToString("C");

                        var SubTotalInvoiceDetailCell = new XRTableCell();
                        SubTotalInvoiceDetailCell.TextAlignment = TextAlignment.MiddleCenter;
                        SubTotalInvoiceDetailCell.Text = Detail.SubTotal.ToString("C");

                        var IEPSDetailCell = new XRTableCell();
                        IEPSDetailCell.TextAlignment = TextAlignment.MiddleCenter;
                        IEPSDetailCell.Text = Detail.IEPS.ToString("C");

                        var TAXDetailCell = new XRTableCell();
                        TAXDetailCell.TextAlignment = TextAlignment.MiddleCenter;
                        TAXDetailCell.Text = Detail.Tax.ToString("C");

                        var BalanceDetailCell = new XRTableCell();
                        BalanceDetailCell.TextAlignment = TextAlignment.MiddleCenter;
                        BalanceDetailCell.Text = Detail.Balance.ToString("C");

                        DetailRow.Cells.Add(InvoiceNoDetailCell);
                        DetailRow.Cells.Add(InvoiceDateDetailCell);
                        DetailRow.Cells.Add(DueDateDetailCell);
                        DetailRow.Cells.Add(DaysDetailCell);
                        DetailRow.Cells.Add(TotalInvoiceDetailCell);
                        DetailRow.Cells.Add(SubTotalInvoiceDetailCell);
                        DetailRow.Cells.Add(IEPSDetailCell);
                        DetailRow.Cells.Add(TAXDetailCell);
                        DetailRow.Cells.Add(BalanceDetailCell);

                        DetailRow.Borders = BorderSide.Bottom;
                        OldBalanceTable.Rows.Add(DetailRow);

                    }

                    //Details Totals Footer
                    HeaderRow = new XRTableRow();
                    HeaderRow.HeightF = rowHeightHeader;

                    var DetailsFooterCell1 = new XRTableCell();
                    DetailsFooterCell1.TextAlignment = TextAlignment.MiddleCenter;
                    DetailsFooterCell1.Text = string.Empty;

                    var DetailsFooterCell2 = new XRTableCell();
                    DetailsFooterCell2.TextAlignment = TextAlignment.MiddleCenter;
                    DetailsFooterCell2.Text = string.Empty;

                    var DetailsFooterCell3 = new XRTableCell();
                    DetailsFooterCell3.TextAlignment = TextAlignment.MiddleCenter;
                    DetailsFooterCell3.Text = string.Empty;

                    var DetailsFooterCell4 = new XRTableCell();
                    DetailsFooterCell4.TextAlignment = TextAlignment.MiddleCenter;
                    DetailsFooterCell4.Text = "Totales";
                    DetailsFooterCell4.Font = new Font(DetailsFooterCell4.Font.FontFamily, DetailsFooterCell4.Font.Size, FontStyle.Bold);

                    var DetailsFooterCell5 = new XRTableCell();
                    DetailsFooterCell5.TextAlignment = TextAlignment.MiddleCenter;
                    DetailsFooterCell5.Text = Client.InvoicesList.Sum(c => c.Total).ToString("C");
                    DetailsFooterCell5.Font = new Font(DetailsFooterCell5.Font.FontFamily, DetailsFooterCell5.Font.Size, FontStyle.Bold);

                    var DetailsFooterCell6 = new XRTableCell();
                    DetailsFooterCell6.TextAlignment = TextAlignment.MiddleCenter;
                    DetailsFooterCell6.Text = Client.InvoicesList.Sum(c => c.SubTotal).ToString("C");
                    DetailsFooterCell6.Font = new Font(DetailsFooterCell6.Font.FontFamily, DetailsFooterCell6.Font.Size, FontStyle.Bold);

                    var DetailsFooterCell7 = new XRTableCell();
                    DetailsFooterCell7.TextAlignment = TextAlignment.MiddleCenter;
                    DetailsFooterCell7.Text = Client.InvoicesList.Sum(c => c.IEPS).ToString("C");
                    DetailsFooterCell7.Font = new Font(DetailsFooterCell7.Font.FontFamily, DetailsFooterCell7.Font.Size, FontStyle.Bold);

                    var DetailsFooterCell8 = new XRTableCell();
                    DetailsFooterCell8.TextAlignment = TextAlignment.MiddleCenter;
                    DetailsFooterCell8.Text = Client.InvoicesList.Sum(c => c.Tax).ToString("C");
                    DetailsFooterCell8.Font = new Font(DetailsFooterCell8.Font.FontFamily, DetailsFooterCell8.Font.Size, FontStyle.Bold);

                    var DetailsFooterCell9 = new XRTableCell();
                    DetailsFooterCell9.TextAlignment = TextAlignment.MiddleCenter;
                    DetailsFooterCell9.Text = Client.InvoicesList.Sum(c => c.Balance).ToString("C");
                    DetailsFooterCell9.Font = new Font(DetailsFooterCell9.Font.FontFamily, DetailsFooterCell9.Font.Size, FontStyle.Bold);

                    HeaderRow.Cells.Add(DetailsFooterCell1);
                    HeaderRow.Cells.Add(DetailsFooterCell2);
                    HeaderRow.Cells.Add(DetailsFooterCell3);
                    HeaderRow.Cells.Add(DetailsFooterCell4);
                    HeaderRow.Cells.Add(DetailsFooterCell5);
                    HeaderRow.Cells.Add(DetailsFooterCell6);
                    HeaderRow.Cells.Add(DetailsFooterCell7);
                    HeaderRow.Cells.Add(DetailsFooterCell8);
                    HeaderRow.Cells.Add(DetailsFooterCell9);

                    HeaderRow.Borders = BorderSide.Bottom;
                    HeaderRow.BackColor = Color.LightGray;
                    OldBalanceTable.Rows.Add(HeaderRow);

                    GreatBalance += Client.InvoicesList.Sum(d => d.Balance);
                    GreatTotalIVA += Client.InvoicesList.Sum(d => d.Tax);
                    GreatTotalIEPS += Client.InvoicesList.Sum(d => d.IEPS);
                    GreatSubtotal += Client.InvoicesList.Sum(d => d.SubTotal);
                    GreatTotal += Client.InvoicesList.Sum(d => d.Total);                    
                    #endregion
                }

                //Great Totals Footer
                HeaderRow = new XRTableRow();
                HeaderRow.HeightF = 15f;

                var GreatTotalsFooterCell1 = new XRTableCell();
                GreatTotalsFooterCell1.TextAlignment = TextAlignment.MiddleCenter;
                GreatTotalsFooterCell1.Text = string.Empty;

                var GreatTotalsFooterCell2 = new XRTableCell();
                GreatTotalsFooterCell2.TextAlignment = TextAlignment.MiddleCenter;
                GreatTotalsFooterCell2.Text = string.Empty;

                var GreatTotalsFooterCell3 = new XRTableCell();
                GreatTotalsFooterCell3.TextAlignment = TextAlignment.MiddleCenter;
                GreatTotalsFooterCell3.Text = string.Empty;

                var GreatTotalsFooterCell4 = new XRTableCell();
                GreatTotalsFooterCell4.TextAlignment = TextAlignment.MiddleCenter;
                GreatTotalsFooterCell4.Text = "Gran Total";
                GreatTotalsFooterCell4.Font = new Font(GreatTotalsFooterCell4.Font.FontFamily, GreatTotalsFooterCell4.Font.Size, FontStyle.Bold);
                
                var GreatTotalsFooterCell5 = new XRTableCell();
                GreatTotalsFooterCell5.TextAlignment = TextAlignment.MiddleCenter;
                GreatTotalsFooterCell5.Text = GreatTotal.ToString("C");//Client.InvoicesList.Sum(c => c.Total).ToString("C");
                GreatTotalsFooterCell5.Font = new Font(GreatTotalsFooterCell5.Font.FontFamily, GreatTotalsFooterCell5.Font.Size, FontStyle.Bold);

                var GreatTotalsFooterCell6 = new XRTableCell();
                GreatTotalsFooterCell6.TextAlignment = TextAlignment.MiddleCenter;
                GreatTotalsFooterCell6.Text = GreatSubtotal.ToString("C");//Client.InvoicesList.Sum(c => c.SubTotal).ToString("C");
                GreatTotalsFooterCell6.Font = new Font(GreatTotalsFooterCell6.Font.FontFamily, GreatTotalsFooterCell6.Font.Size, FontStyle.Bold);

                var GreatTotalsFooterCell7 = new XRTableCell();
                GreatTotalsFooterCell7.TextAlignment = TextAlignment.MiddleCenter;
                GreatTotalsFooterCell7.Text = GreatTotalIEPS.ToString("C"); //Client.InvoicesList.Sum(c => c.IEPS).ToString("C");
                GreatTotalsFooterCell7.Font = new Font(GreatTotalsFooterCell7.Font.FontFamily, GreatTotalsFooterCell7.Font.Size, FontStyle.Bold);

                var GreatTotalsFooterCell8 = new XRTableCell();
                GreatTotalsFooterCell8.TextAlignment = TextAlignment.MiddleCenter;
                GreatTotalsFooterCell8.Text = GreatTotalIVA.ToString("C"); //Client.InvoicesList.Sum(c => c.Tax).ToString("C");
                GreatTotalsFooterCell8.Font = new Font(GreatTotalsFooterCell8.Font.FontFamily, GreatTotalsFooterCell8.Font.Size, FontStyle.Bold);

                var GreatTotalsFooterCell9 = new XRTableCell();
                GreatTotalsFooterCell9.TextAlignment = TextAlignment.MiddleCenter;
                GreatTotalsFooterCell9.Text = GreatBalance.ToString("C"); //Client.InvoicesList.Sum(c => c.Balance).ToString("C");
                GreatTotalsFooterCell9.Font = new Font(GreatTotalsFooterCell9.Font.FontFamily, GreatTotalsFooterCell9.Font.Size, FontStyle.Bold);

                HeaderRow.Cells.Add(GreatTotalsFooterCell1);
                HeaderRow.Cells.Add(GreatTotalsFooterCell2);
                HeaderRow.Cells.Add(GreatTotalsFooterCell3);
                HeaderRow.Cells.Add(GreatTotalsFooterCell4);
                HeaderRow.Cells.Add(GreatTotalsFooterCell5);
                HeaderRow.Cells.Add(GreatTotalsFooterCell6);
                HeaderRow.Cells.Add(GreatTotalsFooterCell7);
                HeaderRow.Cells.Add(GreatTotalsFooterCell8);
                HeaderRow.Cells.Add(GreatTotalsFooterCell9);

                HeaderRow.Borders = BorderSide.Bottom;
                HeaderRow.BackColor = Color.LightGray;
                OldBalanceTable.Rows.Add(HeaderRow);

                return DataSetTableOrdered;
            }
            catch (Exception ex)
            {
                string d=ex.Message;
                throw;
            }          
        }       
    }
}
