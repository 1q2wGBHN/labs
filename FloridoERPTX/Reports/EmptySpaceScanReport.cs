﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
using System.Data;
using App.Entities.ViewModels.Item;
using System.Collections.Generic;

namespace FloridoERPTX.Reports
{
    public partial class EmptySpaceScanReport : DevExpress.XtraReports.UI.XtraReport
    {
        public DataSet DataSetTableOrdered = null;
        public EmptySpaceScanReport()
        {
            InitializeComponent();
        }

        public DataSet printTable(List<ItemEmptySpaceScanModel> items, DateTime date, int incidence, string user)
        {
            try
            {
                DataSetTableOrdered = new DataSet();

                DataTable DataTableOrdered = new DataTable();
                DataSetTableOrdered.Tables.Add(DataTableOrdered);

                xrLabelNo.Text = items[0].id_empty_space.ToString();
                xrLabelUserCreate.Text = user;
                xrLabelDateCreate.Text = date.ToShortDateString();
                xrLabelIncidenceNow.Text = incidence.ToString();

                DataTableOrdered.Columns.Add(new DataColumn("xrTableCellPartNumber"));
                DataTableOrdered.Columns.Add(new DataColumn("xrTableCellPartDescription"));
                DataTableOrdered.Columns.Add(new DataColumn("xrTableCellPlanogram"));
                DataTableOrdered.Columns.Add(new DataColumn("xrTableCellStock"));
                DataTableOrdered.Columns.Add(new DataColumn("xrTableCellDateEntry"));
                DataTableOrdered.Columns.Add(new DataColumn("xrTableCellAverageSale"));
                DataTableOrdered.Columns.Add(new DataColumn("xrTableCellIncidence"));

                foreach (var item in items)
                {
                    DataRow DataRowOrdered = DataSetTableOrdered.Tables[0].NewRow();
                    DataRowOrdered["xrTableCellPartNumber"] = item.part_number;
                    DataRowOrdered["xrTableCellPartDescription"] = item.description;
                    DataRowOrdered["xrTableCellPlanogram"] = item.planogram;
                    DataRowOrdered["xrTableCellStock"] = item.stock_existing;
                    if (item.last_entry != null)
                        DataRowOrdered["xrTableCellDateEntry"] = String.Format("{0:M/d/yyyy}", item.last_entry);
                    else
                        DataRowOrdered["xrTableCellDateEntry"] = "-";
                    DataRowOrdered["xrTableCellAverageSale"] = "% " + item.average_sale;
                    DataRowOrdered["xrTableCellIncidence"] = item.incidence;
                    DataSetTableOrdered.Tables[0].Rows.Add(DataRowOrdered);
                }

                xrTableCellPartNumber.DataBindings.Add(new XRBinding("Text", null, "xrTableRow1.xrTableCellPartNumber"));
                xrTableCellPartDescription.DataBindings.Add(new XRBinding("Text", null, "xrTableRow1.xrTableCellPartDescription"));
                xrTableCellPlanogram.DataBindings.Add(new XRBinding("Text", null, "xrTableRow1.xrTableCellPlanogram"));
                xrTableCellStock.DataBindings.Add(new XRBinding("Text", null, "xrTableRow1.xrTableCellStock"));
                xrTableCellDateEntry.DataBindings.Add(new XRBinding("Text", null, "xrTableRow1.xrTableCellDateEntry"));
                xrTableCellAverageSale.DataBindings.Add(new XRBinding("Text", null, "xrTableRow1.xrTableCellAverageSale"));
                xrTableCellIncidence.DataBindings.Add(new XRBinding("Text", null, "xrTableRow1.xrTableCellIncidence"));

                return DataSetTableOrdered;
            }
            catch (Exception ex)
            {
                string d = ex.Message;
                throw;
            }
        }

    }
}
