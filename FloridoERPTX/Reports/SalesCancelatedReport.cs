﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
using System.Data;
using App.Entities.ViewModels.Expenses;
using System.Collections.Generic;
using DevExpress.XtraCharts;
using System.Linq;
using App.Entities.ViewModels.Sales;
using App.DAL.Customer;
using App.Entities;

namespace FloridoERP.Report
{
    public partial class SalesCancelatedReport : DevExpress.XtraReports.UI.XtraReport
    {
        public DataSet DataSetTable = null;
        private UserMasterRepository _UserMasterRepository;
        public decimal total = 0;
        public decimal totalIva = 0;
        public decimal totalIeps = 0;
        public SalesCancelatedReport()
        {
            InitializeComponent();
            _UserMasterRepository = new UserMasterRepository();
        }

        public DataSet printTable(SalesReportIndex Model)
        {
            try
            {
                var User = new USER_MASTER();
                DataSetTable = new DataSet();
                DataTable DataTableObject = new DataTable();
                DataSetTable.Tables.Add(DataTableObject);

                if (string.IsNullOrEmpty(Model.Cashier))
                    xrLabel21.Text = "TODOS LOS CAJEROS";
                else
                {
                    User = _UserMasterRepository.GetUserMasterByEmployeeNumber(Model.Cashier);
                    xrLabel21.Text = string.Format("{0} {1}", User.first_name, User.last_name);
                }

                xrDateTimeNow.Text = Model.Date;
                totalFooterCash.Text = "$ " + Model.CancellationsReport.Where(c => c.PaymentMethod != -1).Sum(d => d.Total).ToString("0.00");
                ivaCash.Text = "$ " + Model.CancellationsReport.Where(c => c.PaymentMethod != -1).Sum(d => d.Tax).ToString("0.00");
                subTotalCash.Text = "$ " + Model.CancellationsReport.Where(c => c.PaymentMethod != -1).Sum(d => d.SubTotal).ToString("0.00");

                totalFooterCredit.Text = "$ " + Model.CancellationsReport.Where(c => c.PaymentMethod == -1).Sum(d => d.Total).ToString("0.00");
                ivaCredit.Text = "$ " + Model.CancellationsReport.Where(c => c.PaymentMethod == -1).Sum(d => d.Tax).ToString("0.00");
                subTotalCredit.Text = "$ " + Model.CancellationsReport.Where(c => c.PaymentMethod == -1).Sum(d => d.SubTotal).ToString("0.00");

                DataTableObject.Columns.Add(new DataColumn("SaleNumber"));
                DataTableObject.Columns.Add(new DataColumn("CancellationNumber"));
                DataTableObject.Columns.Add(new DataColumn("ItemDescription"));
                DataTableObject.Columns.Add(new DataColumn("Sales"));
                DataTableObject.Columns.Add(new DataColumn("Cancelated"));
                DataTableObject.Columns.Add(new DataColumn("UnitPrice"));
                DataTableObject.Columns.Add(new DataColumn("SubTotal"));
                DataTableObject.Columns.Add(new DataColumn("Tax"));
                DataTableObject.Columns.Add(new DataColumn("TotalCash"));
                DataTableObject.Columns.Add(new DataColumn("Cashier"));
                DataTableObject.Columns.Add(new DataColumn("Motive"));
                DataTableObject.Columns.Add(new DataColumn("Supervisor"));                

                foreach (var item in Model.CancellationsReport.Where(c=>c.PaymentMethod != -1))
                {
                    DataRow DataRowOrdered = DataSetTable.Tables[0].NewRow();
                    DataRowOrdered["SaleNumber"] = item.SaleNumber;
                    DataRowOrdered["CancellationNumber"] = item.CancellationNumber;
                    DataRowOrdered["ItemDescription"] = item.ItemDescription;
                    DataRowOrdered["Sales"] = item.Sales;
                    DataRowOrdered["Cancelated"] = item.SalesCancelated;
                    DataRowOrdered["UnitPrice"] = item.UnitPrice.ToString("C");
                    DataRowOrdered["SubTotal"] = item.SubTotal.ToString("C");
                    DataRowOrdered["Tax"] = item.Tax.ToString("C");
                    DataRowOrdered["TotalCash"] = item.Total.ToString("C");
                    DataRowOrdered["Cashier"] = item.Cashier;
                    DataRowOrdered["Motive"] = item.Motive;
                    DataRowOrdered["Supervisor"] = item.Supervisor;

                    DataSetTable.Tables[0].Rows.Add(DataRowOrdered);

                }

                if(Model.CancellationsReport.Where(c=>c.PaymentMethod == -1).Count() > 0)
                {
                    DetailReport1.Visible = true;                    

                    foreach(var Item in Model.CancellationsReport.Where(c=>c.PaymentMethod == -1))
                    {
                        var row = new XRTableRow();

                        var cell = new XRTableCell();
                        cell.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
                        cell.Text = Item.SaleNumber.ToString();

                        var cell2 = new XRTableCell();
                        cell2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
                        cell2.Text = Item.CancellationNumber.ToString();

                        var cell3 = new XRTableCell();
                        cell3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
                        cell3.Text = Item.ItemDescription;

                        var cell4 = new XRTableCell();
                        cell4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
                        cell4.Text = Item.SaleNumber.ToString();

                        var cell5 = new XRTableCell();
                        cell5.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
                        cell5.Text = Item.SalesCancelated.ToString();

                        var cell6 = new XRTableCell();
                        cell6.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
                        cell6.Text = Item.UnitPrice.ToString("C");

                        var cell7 = new XRTableCell();
                        cell7.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
                        cell7.Text = Item.SubTotal.ToString("C");

                        var cell8 = new XRTableCell();
                        cell8.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
                        cell8.Text = Item.Tax.ToString("C");

                        var cell9 = new XRTableCell();
                        cell9.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
                        cell9.Text = Item.Total.ToString("C");

                        var cell10 = new XRTableCell();
                        cell10.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
                        cell10.Text = Item.Cashier;

                        var cell11 = new XRTableCell();
                        cell11.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
                        cell11.Text = Item.Motive;

                        var cell12 = new XRTableCell();
                        cell12.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
                        cell12.Text = Item.Supervisor;

                        row.Cells.Add(cell);
                        row.Cells.Add(cell2);
                        row.Cells.Add(cell3);
                        row.Cells.Add(cell4);
                        row.Cells.Add(cell5);
                        row.Cells.Add(cell6);
                        row.Cells.Add(cell7);
                        row.Cells.Add(cell8);
                        row.Cells.Add(cell9);
                        row.Cells.Add(cell10);
                        row.Cells.Add(cell11);
                        row.Cells.Add(cell12);
                        TableCredit.Rows.Add(row);
                    }
                }

                SaleNumber.DataBindings.Add(new XRBinding("Text", null, "xrTableRow1.SaleNumber"));
                CancellationNumber.DataBindings.Add(new XRBinding("Text", null, "xrTableRow1.CancellationNumber"));
                ItemDescription.DataBindings.Add(new XRBinding("Text", null, "xrTableRow1.ItemDescription"));
                Sales.DataBindings.Add(new XRBinding("Text", null, "xrTableRow1.Sales"));
                Cancelated.DataBindings.Add(new XRBinding("Text", null, "xrTableRow1.Cancelated"));
                Sales.DataBindings.Add(new XRBinding("Text", null, "xrTableRow1.Sales"));
                UnitPrice.DataBindings.Add(new XRBinding("Text", null, "xrTableRow1.UnitPrice"));
                SubTotal.DataBindings.Add(new XRBinding("Text", null, "xrTableRow1.SubTotal"));
                Tax.DataBindings.Add(new XRBinding("Text", null, "xrTableRow1.Tax"));
                TotalCash.DataBindings.Add(new XRBinding("Text", null, "xrTableRow1.TotalCash"));
                Cashier.DataBindings.Add(new XRBinding("Text", null, "xrTableRow1.Cashier"));
                Motive.DataBindings.Add(new XRBinding("Text", null, "xrTableRow1.Motive"));
                Supervisor.DataBindings.Add(new XRBinding("Text", null, "xrTableRow1.Supervisor"));

                return DataSetTable;
            }
            catch (Exception ex)
            {
                string d = ex.Message;
                throw;
            }
        }        
    }
}
