﻿namespace FloridoERPTX.Reports
{
    partial class SalesDetailsKardexReport
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(SalesDetailsKardexReport));
            this.Detail = new DevExpress.XtraReports.UI.DetailBand();
            this.TopMargin = new DevExpress.XtraReports.UI.TopMarginBand();
            this.BottomMargin = new DevExpress.XtraReports.UI.BottomMarginBand();
            this.ReportHeader = new DevExpress.XtraReports.UI.ReportHeaderBand();
            this.xrPictureBox1 = new DevExpress.XtraReports.UI.XRPictureBox();
            this.xrLabel3 = new DevExpress.XtraReports.UI.XRLabel();
            this.siteLabel = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel4 = new DevExpress.XtraReports.UI.XRLabel();
            this._labeTitle = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLine1 = new DevExpress.XtraReports.UI.XRLine();
            this._labelNoVenta = new DevExpress.XtraReports.UI.XRLabel();
            this.saNoLabel = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel9 = new DevExpress.XtraReports.UI.XRLabel();
            this.saDaLabel = new DevExpress.XtraReports.UI.XRLabel();
            this._labelFactura = new DevExpress.XtraReports.UI.XRLabel();
            this.saInLabel = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLine2 = new DevExpress.XtraReports.UI.XRLine();
            this.xrLabel6 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel2 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel1 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLine4 = new DevExpress.XtraReports.UI.XRLine();
            this.xrLine3 = new DevExpress.XtraReports.UI.XRLine();
            this.Detalle = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel17 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel18 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel19 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel23 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel24 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel25 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel26 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTable1 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow1 = new DevExpress.XtraReports.UI.XRTableRow();
            this.part_number = new DevExpress.XtraReports.UI.XRTableCell();
            this.part_name = new DevExpress.XtraReports.UI.XRTableCell();
            this.part_quantity = new DevExpress.XtraReports.UI.XRTableCell();
            this.part_price = new DevExpress.XtraReports.UI.XRTableCell();
            this.part_importe = new DevExpress.XtraReports.UI.XRTableCell();
            this.part_iva = new DevExpress.XtraReports.UI.XRTableCell();
            this.part_iva_percentage = new DevExpress.XtraReports.UI.XRTableCell();
            this.part_ieps = new DevExpress.XtraReports.UI.XRTableCell();
            this.part_ieps_percentage = new DevExpress.XtraReports.UI.XRTableCell();
            this.part_total = new DevExpress.XtraReports.UI.XRTableCell();
            this.GroupFooter1 = new DevExpress.XtraReports.UI.GroupFooterBand();
            this.ivaLabel = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel29 = new DevExpress.XtraReports.UI.XRLabel();
            this.totalLabel = new DevExpress.XtraReports.UI.XRLabel();
            this.subtotalLabel = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel33 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel32 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel31 = new DevExpress.XtraReports.UI.XRLabel();
            this.iepsLabel = new DevExpress.XtraReports.UI.XRLabel();
            this.PageFooter = new DevExpress.XtraReports.UI.PageFooterBand();
            this.xrPageInfo1 = new DevExpress.XtraReports.UI.XRPageInfo();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // Detail
            // 
            this.Detail.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable1});
            this.Detail.Dpi = 100F;
            this.Detail.HeightF = 20.47011F;
            this.Detail.Name = "Detail";
            this.Detail.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.Detail.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // TopMargin
            // 
            this.TopMargin.Dpi = 100F;
            this.TopMargin.HeightF = 10.41667F;
            this.TopMargin.Name = "TopMargin";
            this.TopMargin.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.TopMargin.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // BottomMargin
            // 
            this.BottomMargin.Dpi = 100F;
            this.BottomMargin.HeightF = 5.208333F;
            this.BottomMargin.Name = "BottomMargin";
            this.BottomMargin.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.BottomMargin.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // ReportHeader
            // 
            this.ReportHeader.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel6,
            this.xrLabel2,
            this.xrLabel1,
            this.xrLine4,
            this.xrLine3,
            this.Detalle,
            this.xrLabel17,
            this.xrLabel18,
            this.xrLabel19,
            this.xrLabel23,
            this.xrLabel24,
            this.xrLabel25,
            this.xrLabel26,
            this.xrPictureBox1,
            this.xrLine2,
            this.xrLabel3,
            this.siteLabel,
            this.xrLabel4,
            this._labeTitle,
            this.xrLine1,
            this._labelNoVenta,
            this.saNoLabel,
            this.xrLabel9,
            this.saDaLabel,
            this._labelFactura,
            this.saInLabel});
            this.ReportHeader.Dpi = 100F;
            this.ReportHeader.HeightF = 178.6566F;
            this.ReportHeader.Name = "ReportHeader";
            // 
            // xrPictureBox1
            // 
            this.xrPictureBox1.Dpi = 100F;
            this.xrPictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("xrPictureBox1.Image")));
            this.xrPictureBox1.LocationFloat = new DevExpress.Utils.PointFloat(576.9999F, 0F);
            this.xrPictureBox1.Name = "xrPictureBox1";
            this.xrPictureBox1.SizeF = new System.Drawing.SizeF(92.58575F, 61.66666F);
            this.xrPictureBox1.Sizing = DevExpress.XtraPrinting.ImageSizeMode.ZoomImage;
            // 
            // xrLabel3
            // 
            this.xrLabel3.Dpi = 100F;
            this.xrLabel3.Font = new System.Drawing.Font("Arial", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel3.LocationFloat = new DevExpress.Utils.PointFloat(368.1004F, 0F);
            this.xrLabel3.Name = "xrLabel3";
            this.xrLabel3.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel3.SizeF = new System.Drawing.SizeF(204.1053F, 15F);
            this.xrLabel3.StylePriority.UseFont = false;
            this.xrLabel3.StylePriority.UseTextAlignment = false;
            this.xrLabel3.Text = "DISTRIBUIDORA EL FLORIDO S.A. DE C.V.";
            this.xrLabel3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // siteLabel
            // 
            this.siteLabel.Dpi = 100F;
            this.siteLabel.Font = new System.Drawing.Font("Arial", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.siteLabel.LocationFloat = new DevExpress.Utils.PointFloat(371.2059F, 15.00001F);
            this.siteLabel.Name = "siteLabel";
            this.siteLabel.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.siteLabel.SizeF = new System.Drawing.SizeF(201.0001F, 15F);
            this.siteLabel.StylePriority.UseFont = false;
            this.siteLabel.StylePriority.UseTextAlignment = false;
            this.siteLabel.Text = "*FLORIDO CEDIS";
            this.siteLabel.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // xrLabel4
            // 
            this.xrLabel4.Dpi = 100F;
            this.xrLabel4.Font = new System.Drawing.Font("Arial", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel4.LocationFloat = new DevExpress.Utils.PointFloat(485.3801F, 30F);
            this.xrLabel4.Name = "xrLabel4";
            this.xrLabel4.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel4.SizeF = new System.Drawing.SizeF(86.82587F, 15F);
            this.xrLabel4.StylePriority.UseFont = false;
            this.xrLabel4.StylePriority.UseTextAlignment = false;
            this.xrLabel4.Text = "DFL-950802-5N4";
            this.xrLabel4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // _labeTitle
            // 
            this._labeTitle.Dpi = 100F;
            this._labeTitle.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._labeTitle.LocationFloat = new DevExpress.Utils.PointFloat(0F, 0F);
            this._labeTitle.Name = "_labeTitle";
            this._labeTitle.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this._labeTitle.SizeF = new System.Drawing.SizeF(155.637F, 20F);
            this._labeTitle.StylePriority.UseFont = false;
            this._labeTitle.Text = "Reporte de Ventas";
            // 
            // xrLine1
            // 
            this.xrLine1.Dpi = 100F;
            this.xrLine1.LocationFloat = new DevExpress.Utils.PointFloat(0F, 76.58339F);
            this.xrLine1.Name = "xrLine1";
            this.xrLine1.SizeF = new System.Drawing.SizeF(781.9999F, 10F);
            this.xrLine1.StylePriority.UseBorderDashStyle = false;
            // 
            // _labelNoVenta
            // 
            this._labelNoVenta.Dpi = 100F;
            this._labelNoVenta.Font = new System.Drawing.Font("Arial", 7F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._labelNoVenta.LocationFloat = new DevExpress.Utils.PointFloat(0F, 86.58339F);
            this._labelNoVenta.Name = "_labelNoVenta";
            this._labelNoVenta.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this._labelNoVenta.SizeF = new System.Drawing.SizeF(75.91912F, 15F);
            this._labelNoVenta.StylePriority.UseFont = false;
            this._labelNoVenta.Text = "No. Venta:";
            // 
            // saNoLabel
            // 
            this.saNoLabel.Dpi = 100F;
            this.saNoLabel.Font = new System.Drawing.Font("Arial", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.saNoLabel.LocationFloat = new DevExpress.Utils.PointFloat(79.43379F, 86.58339F);
            this.saNoLabel.Name = "saNoLabel";
            this.saNoLabel.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.saNoLabel.SizeF = new System.Drawing.SizeF(79.71806F, 15F);
            this.saNoLabel.StylePriority.UseFont = false;
            // 
            // xrLabel9
            // 
            this.xrLabel9.Dpi = 100F;
            this.xrLabel9.Font = new System.Drawing.Font("Arial", 7F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel9.LocationFloat = new DevExpress.Utils.PointFloat(302.3334F, 86.58339F);
            this.xrLabel9.Name = "xrLabel9";
            this.xrLabel9.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel9.SizeF = new System.Drawing.SizeF(68.87257F, 15F);
            this.xrLabel9.StylePriority.UseFont = false;
            this.xrLabel9.Text = "Fecha:";
            // 
            // saDaLabel
            // 
            this.saDaLabel.Dpi = 100F;
            this.saDaLabel.Font = new System.Drawing.Font("Arial", 7F);
            this.saDaLabel.LocationFloat = new DevExpress.Utils.PointFloat(371.2059F, 86.58339F);
            this.saDaLabel.Name = "saDaLabel";
            this.saDaLabel.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.saDaLabel.SizeF = new System.Drawing.SizeF(100F, 15F);
            this.saDaLabel.StylePriority.UseFont = false;
            // 
            // _labelFactura
            // 
            this._labelFactura.Dpi = 100F;
            this._labelFactura.Font = new System.Drawing.Font("Arial", 7F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._labelFactura.LocationFloat = new DevExpress.Utils.PointFloat(547.7736F, 86.58339F);
            this._labelFactura.Name = "_labelFactura";
            this._labelFactura.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this._labelFactura.SizeF = new System.Drawing.SizeF(89.8284F, 15F);
            this._labelFactura.StylePriority.UseFont = false;
            this._labelFactura.Text = "Factura :";
            // 
            // saInLabel
            // 
            this.saInLabel.Dpi = 100F;
            this.saInLabel.Font = new System.Drawing.Font("Arial", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.saInLabel.LocationFloat = new DevExpress.Utils.PointFloat(637.602F, 86.58339F);
            this.saInLabel.Name = "saInLabel";
            this.saInLabel.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.saInLabel.SizeF = new System.Drawing.SizeF(142.4633F, 15F);
            this.saInLabel.StylePriority.UseFont = false;
            // 
            // xrLine2
            // 
            this.xrLine2.Dpi = 100F;
            this.xrLine2.LocationFloat = new DevExpress.Utils.PointFloat(0F, 101.5834F);
            this.xrLine2.Name = "xrLine2";
            this.xrLine2.SizeF = new System.Drawing.SizeF(782.0001F, 10F);
            // 
            // xrLabel6
            // 
            this.xrLabel6.Dpi = 100F;
            this.xrLabel6.Font = new System.Drawing.Font("Arial", 7F, System.Drawing.FontStyle.Bold);
            this.xrLabel6.LocationFloat = new DevExpress.Utils.PointFloat(723.628F, 148.0385F);
            this.xrLabel6.Name = "xrLabel6";
            this.xrLabel6.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel6.SizeF = new System.Drawing.SizeF(56.43756F, 17.55346F);
            this.xrLabel6.StylePriority.UseFont = false;
            this.xrLabel6.StylePriority.UseTextAlignment = false;
            this.xrLabel6.Text = "TOTAL";
            this.xrLabel6.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrLabel2
            // 
            this.xrLabel2.Dpi = 100F;
            this.xrLabel2.Font = new System.Drawing.Font("Arial", 7F, System.Drawing.FontStyle.Bold);
            this.xrLabel2.LocationFloat = new DevExpress.Utils.PointFloat(667.1903F, 148.0385F);
            this.xrLabel2.Name = "xrLabel2";
            this.xrLabel2.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel2.SizeF = new System.Drawing.SizeF(56.43756F, 17.55346F);
            this.xrLabel2.StylePriority.UseFont = false;
            this.xrLabel2.StylePriority.UseTextAlignment = false;
            this.xrLabel2.Text = "IEPS %";
            this.xrLabel2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrLabel1
            // 
            this.xrLabel1.Dpi = 100F;
            this.xrLabel1.Font = new System.Drawing.Font("Arial", 7F, System.Drawing.FontStyle.Bold);
            this.xrLabel1.LocationFloat = new DevExpress.Utils.PointFloat(603.0045F, 148.0385F);
            this.xrLabel1.Name = "xrLabel1";
            this.xrLabel1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel1.SizeF = new System.Drawing.SizeF(64.18604F, 17.55346F);
            this.xrLabel1.StylePriority.UseFont = false;
            this.xrLabel1.StylePriority.UseTextAlignment = false;
            this.xrLabel1.Text = "IEPS";
            this.xrLabel1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrLine4
            // 
            this.xrLine4.Dpi = 100F;
            this.xrLine4.LocationFloat = new DevExpress.Utils.PointFloat(0F, 165.6463F);
            this.xrLine4.Name = "xrLine4";
            this.xrLine4.SizeF = new System.Drawing.SizeF(782.0001F, 10F);
            // 
            // xrLine3
            // 
            this.xrLine3.Dpi = 100F;
            this.xrLine3.LocationFloat = new DevExpress.Utils.PointFloat(0F, 124.5512F);
            this.xrLine3.Name = "xrLine3";
            this.xrLine3.SizeF = new System.Drawing.SizeF(782.0001F, 10F);
            // 
            // Detalle
            // 
            this.Detalle.Dpi = 100F;
            this.Detalle.Font = new System.Drawing.Font("Arial", 7F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Detalle.LocationFloat = new DevExpress.Utils.PointFloat(327.9117F, 115.5103F);
            this.Detalle.Name = "Detalle";
            this.Detalle.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.Detalle.SizeF = new System.Drawing.SizeF(119.9765F, 9.040857F);
            this.Detalle.StylePriority.UseFont = false;
            this.Detalle.StylePriority.UseTextAlignment = false;
            this.Detalle.Text = "Detalle de la venta";
            this.Detalle.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrLabel17
            // 
            this.xrLabel17.Dpi = 100F;
            this.xrLabel17.Font = new System.Drawing.Font("Arial", 7F, System.Drawing.FontStyle.Bold);
            this.xrLabel17.LocationFloat = new DevExpress.Utils.PointFloat(0F, 148.0385F);
            this.xrLabel17.Name = "xrLabel17";
            this.xrLabel17.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel17.SizeF = new System.Drawing.SizeF(96.27908F, 17.55346F);
            this.xrLabel17.StylePriority.UseFont = false;
            this.xrLabel17.StylePriority.UseTextAlignment = false;
            this.xrLabel17.Text = "No. Artículo";
            this.xrLabel17.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrLabel18
            // 
            this.xrLabel18.Dpi = 100F;
            this.xrLabel18.Font = new System.Drawing.Font("Arial", 7F, System.Drawing.FontStyle.Bold);
            this.xrLabel18.LocationFloat = new DevExpress.Utils.PointFloat(96.27908F, 148.0385F);
            this.xrLabel18.Name = "xrLabel18";
            this.xrLabel18.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel18.SizeF = new System.Drawing.SizeF(252.9027F, 17.55346F);
            this.xrLabel18.StylePriority.UseFont = false;
            this.xrLabel18.StylePriority.UseTextAlignment = false;
            this.xrLabel18.Text = "Artículo";
            this.xrLabel18.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrLabel19
            // 
            this.xrLabel19.Dpi = 100F;
            this.xrLabel19.Font = new System.Drawing.Font("Arial", 7F, System.Drawing.FontStyle.Bold);
            this.xrLabel19.LocationFloat = new DevExpress.Utils.PointFloat(353.9346F, 148.0385F);
            this.xrLabel19.Name = "xrLabel19";
            this.xrLabel19.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel19.SizeF = new System.Drawing.SizeF(48.3721F, 17.60774F);
            this.xrLabel19.StylePriority.UseFont = false;
            this.xrLabel19.StylePriority.UseTextAlignment = false;
            this.xrLabel19.Text = "Cantidad";
            this.xrLabel19.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrLabel23
            // 
            this.xrLabel23.Dpi = 100F;
            this.xrLabel23.Font = new System.Drawing.Font("Arial", 7F, System.Drawing.FontStyle.Bold);
            this.xrLabel23.LocationFloat = new DevExpress.Utils.PointFloat(402.3069F, 148.0385F);
            this.xrLabel23.Name = "xrLabel23";
            this.xrLabel23.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel23.SizeF = new System.Drawing.SizeF(45.5813F, 17.60774F);
            this.xrLabel23.StylePriority.UseFont = false;
            this.xrLabel23.StylePriority.UseTextAlignment = false;
            this.xrLabel23.Text = "Precio";
            this.xrLabel23.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrLabel24
            // 
            this.xrLabel24.Dpi = 100F;
            this.xrLabel24.Font = new System.Drawing.Font("Arial", 7F, System.Drawing.FontStyle.Bold);
            this.xrLabel24.LocationFloat = new DevExpress.Utils.PointFloat(447.8882F, 148.0385F);
            this.xrLabel24.Name = "xrLabel24";
            this.xrLabel24.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel24.SizeF = new System.Drawing.SizeF(53.48831F, 17.55346F);
            this.xrLabel24.StylePriority.UseFont = false;
            this.xrLabel24.StylePriority.UseTextAlignment = false;
            this.xrLabel24.Text = "Importe";
            this.xrLabel24.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrLabel25
            // 
            this.xrLabel25.Dpi = 100F;
            this.xrLabel25.Font = new System.Drawing.Font("Arial", 7F, System.Drawing.FontStyle.Bold);
            this.xrLabel25.LocationFloat = new DevExpress.Utils.PointFloat(501.3766F, 148.0928F);
            this.xrLabel25.Name = "xrLabel25";
            this.xrLabel25.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel25.SizeF = new System.Drawing.SizeF(64.18604F, 17.55346F);
            this.xrLabel25.StylePriority.UseFont = false;
            this.xrLabel25.StylePriority.UseTextAlignment = false;
            this.xrLabel25.Text = "IVA";
            this.xrLabel25.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrLabel26
            // 
            this.xrLabel26.Dpi = 100F;
            this.xrLabel26.Font = new System.Drawing.Font("Arial", 7F, System.Drawing.FontStyle.Bold);
            this.xrLabel26.LocationFloat = new DevExpress.Utils.PointFloat(565.5626F, 148.0928F);
            this.xrLabel26.Name = "xrLabel26";
            this.xrLabel26.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel26.SizeF = new System.Drawing.SizeF(37.44189F, 17.55346F);
            this.xrLabel26.StylePriority.UseFont = false;
            this.xrLabel26.StylePriority.UseTextAlignment = false;
            this.xrLabel26.Text = "IVA %";
            this.xrLabel26.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTable1
            // 
            this.xrTable1.BackColor = System.Drawing.Color.White;
            this.xrTable1.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrTable1.Dpi = 100F;
            this.xrTable1.Font = new System.Drawing.Font("Arial", 6F);
            this.xrTable1.LocationFloat = new DevExpress.Utils.PointFloat(0F, 0F);
            this.xrTable1.Name = "xrTable1";
            this.xrTable1.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow1});
            this.xrTable1.SizeF = new System.Drawing.SizeF(780.0653F, 20F);
            this.xrTable1.StylePriority.UseBackColor = false;
            this.xrTable1.StylePriority.UseBorders = false;
            this.xrTable1.StylePriority.UseFont = false;
            // 
            // xrTableRow1
            // 
            this.xrTableRow1.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.part_number,
            this.part_name,
            this.part_quantity,
            this.part_price,
            this.part_importe,
            this.part_iva,
            this.part_iva_percentage,
            this.part_ieps,
            this.part_ieps_percentage,
            this.part_total});
            this.xrTableRow1.Dpi = 100F;
            this.xrTableRow1.Name = "xrTableRow1";
            this.xrTableRow1.Weight = 1D;
            // 
            // part_number
            // 
            this.part_number.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.part_number.Dpi = 100F;
            this.part_number.Font = new System.Drawing.Font("Arial", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.part_number.Name = "part_number";
            this.part_number.StylePriority.UseBorders = false;
            this.part_number.StylePriority.UseFont = false;
            this.part_number.StylePriority.UseTextAlignment = false;
            this.part_number.Text = "part_number";
            this.part_number.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.part_number.Weight = 0.87266038548649782D;
            // 
            // part_name
            // 
            this.part_name.Dpi = 100F;
            this.part_name.Font = new System.Drawing.Font("Arial", 7F);
            this.part_name.Name = "part_name";
            this.part_name.StylePriority.UseFont = false;
            this.part_name.StylePriority.UseTextAlignment = false;
            this.part_name.Text = "part_name";
            this.part_name.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.part_name.Weight = 1.0419706710288981D;
            // 
            // part_quantity
            // 
            this.part_quantity.Dpi = 100F;
            this.part_quantity.Font = new System.Drawing.Font("Arial", 7F);
            this.part_quantity.Name = "part_quantity";
            this.part_quantity.StylePriority.UseFont = false;
            this.part_quantity.StylePriority.UseTextAlignment = false;
            this.part_quantity.Text = "part_quantity";
            this.part_quantity.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.part_quantity.Weight = 0.29129460621939329D;
            // 
            // part_price
            // 
            this.part_price.Dpi = 100F;
            this.part_price.Font = new System.Drawing.Font("Arial", 7F);
            this.part_price.Name = "part_price";
            this.part_price.StylePriority.UseFont = false;
            this.part_price.StylePriority.UseTextAlignment = false;
            this.part_price.Text = "part_price";
            this.part_price.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.part_price.Weight = 0.24993170750846372D;
            // 
            // part_importe
            // 
            this.part_importe.Dpi = 100F;
            this.part_importe.Font = new System.Drawing.Font("Arial", 7F);
            this.part_importe.Name = "part_importe";
            this.part_importe.StylePriority.UseFont = false;
            this.part_importe.StylePriority.UseTextAlignment = false;
            this.part_importe.Text = "part_importe";
            this.part_importe.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.part_importe.Weight = 0.29328654131028475D;
            // 
            // part_iva
            // 
            this.part_iva.Dpi = 100F;
            this.part_iva.Font = new System.Drawing.Font("Arial", 7F);
            this.part_iva.Name = "part_iva";
            this.part_iva.StylePriority.UseFont = false;
            this.part_iva.StylePriority.UseTextAlignment = false;
            this.part_iva.Text = "part_iva";
            this.part_iva.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.part_iva.Weight = 0.36825123619163258D;
            // 
            // part_iva_percentage
            // 
            this.part_iva_percentage.Dpi = 100F;
            this.part_iva_percentage.Font = new System.Drawing.Font("Arial", 7F);
            this.part_iva_percentage.Name = "part_iva_percentage";
            this.part_iva_percentage.StylePriority.UseFont = false;
            this.part_iva_percentage.StylePriority.UseTextAlignment = false;
            this.part_iva_percentage.Text = "part_iva_percentage";
            this.part_iva_percentage.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.part_iva_percentage.Weight = 0.18899377923182081D;
            // 
            // part_ieps
            // 
            this.part_ieps.Dpi = 100F;
            this.part_ieps.Font = new System.Drawing.Font("Arial", 7F);
            this.part_ieps.Name = "part_ieps";
            this.part_ieps.StylePriority.UseFont = false;
            this.part_ieps.StylePriority.UseTextAlignment = false;
            this.part_ieps.Text = "part_ieps";
            this.part_ieps.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.part_ieps.Weight = 0.35194473358325623D;
            // 
            // part_ieps_percentage
            // 
            this.part_ieps_percentage.Dpi = 100F;
            this.part_ieps_percentage.Font = new System.Drawing.Font("Arial", 7F);
            this.part_ieps_percentage.Name = "part_ieps_percentage";
            this.part_ieps_percentage.StylePriority.UseFont = false;
            this.part_ieps_percentage.StylePriority.UseTextAlignment = false;
            this.part_ieps_percentage.Text = "part_ieps_percentage";
            this.part_ieps_percentage.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.part_ieps_percentage.Weight = 0.30945735319876144D;
            // 
            // part_total
            // 
            this.part_total.Dpi = 100F;
            this.part_total.Font = new System.Drawing.Font("Arial", 7F);
            this.part_total.Name = "part_total";
            this.part_total.StylePriority.UseFont = false;
            this.part_total.StylePriority.UseTextAlignment = false;
            this.part_total.Text = "part_total";
            this.part_total.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.part_total.Weight = 0.30945735319876144D;
            // 
            // GroupFooter1
            // 
            this.GroupFooter1.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.ivaLabel,
            this.xrLabel29,
            this.totalLabel,
            this.subtotalLabel,
            this.xrLabel33,
            this.xrLabel32,
            this.xrLabel31,
            this.iepsLabel});
            this.GroupFooter1.Dpi = 100F;
            this.GroupFooter1.HeightF = 74.79172F;
            this.GroupFooter1.Name = "GroupFooter1";
            // 
            // ivaLabel
            // 
            this.ivaLabel.Dpi = 100F;
            this.ivaLabel.Font = new System.Drawing.Font("Arial", 8F);
            this.ivaLabel.LocationFloat = new DevExpress.Utils.PointFloat(667.365F, 25.00013F);
            this.ivaLabel.Name = "ivaLabel";
            this.ivaLabel.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.ivaLabel.SizeF = new System.Drawing.SizeF(114.6353F, 15F);
            this.ivaLabel.StylePriority.UseFont = false;
            this.ivaLabel.StylePriority.UseTextAlignment = false;
            this.ivaLabel.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // xrLabel29
            // 
            this.xrLabel29.Dpi = 100F;
            this.xrLabel29.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Bold);
            this.xrLabel29.LocationFloat = new DevExpress.Utils.PointFloat(614.1906F, 9.999974F);
            this.xrLabel29.Name = "xrLabel29";
            this.xrLabel29.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel29.SizeF = new System.Drawing.SizeF(53F, 15F);
            this.xrLabel29.StylePriority.UseFont = false;
            this.xrLabel29.Text = "Subtotal:";
            // 
            // totalLabel
            // 
            this.totalLabel.Dpi = 100F;
            this.totalLabel.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold);
            this.totalLabel.LocationFloat = new DevExpress.Utils.PointFloat(667.365F, 55.00005F);
            this.totalLabel.Name = "totalLabel";
            this.totalLabel.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.totalLabel.SizeF = new System.Drawing.SizeF(114.6353F, 15F);
            this.totalLabel.StylePriority.UseFont = false;
            this.totalLabel.StylePriority.UseTextAlignment = false;
            this.totalLabel.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // subtotalLabel
            // 
            this.subtotalLabel.Dpi = 100F;
            this.subtotalLabel.Font = new System.Drawing.Font("Arial", 8F);
            this.subtotalLabel.LocationFloat = new DevExpress.Utils.PointFloat(667.1904F, 9.999974F);
            this.subtotalLabel.Name = "subtotalLabel";
            this.subtotalLabel.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.subtotalLabel.SizeF = new System.Drawing.SizeF(114.8097F, 15F);
            this.subtotalLabel.StylePriority.UseFont = false;
            this.subtotalLabel.StylePriority.UseTextAlignment = false;
            this.subtotalLabel.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // xrLabel33
            // 
            this.xrLabel33.Dpi = 100F;
            this.xrLabel33.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold);
            this.xrLabel33.LocationFloat = new DevExpress.Utils.PointFloat(614.1906F, 55.00005F);
            this.xrLabel33.Name = "xrLabel33";
            this.xrLabel33.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel33.SizeF = new System.Drawing.SizeF(53.1745F, 15F);
            this.xrLabel33.StylePriority.UseFont = false;
            this.xrLabel33.StylePriority.UseTextAlignment = false;
            this.xrLabel33.Text = "Total:";
            this.xrLabel33.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
            // 
            // xrLabel32
            // 
            this.xrLabel32.Dpi = 100F;
            this.xrLabel32.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Bold);
            this.xrLabel32.LocationFloat = new DevExpress.Utils.PointFloat(633.1907F, 40.00009F);
            this.xrLabel32.Name = "xrLabel32";
            this.xrLabel32.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel32.SizeF = new System.Drawing.SizeF(34F, 15F);
            this.xrLabel32.StylePriority.UseFont = false;
            this.xrLabel32.Text = "IEPS:";
            // 
            // xrLabel31
            // 
            this.xrLabel31.Dpi = 100F;
            this.xrLabel31.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Bold);
            this.xrLabel31.LocationFloat = new DevExpress.Utils.PointFloat(640.365F, 25.00013F);
            this.xrLabel31.Name = "xrLabel31";
            this.xrLabel31.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel31.SizeF = new System.Drawing.SizeF(27F, 15F);
            this.xrLabel31.StylePriority.UseFont = false;
            this.xrLabel31.Text = "IVA:";
            // 
            // iepsLabel
            // 
            this.iepsLabel.Dpi = 100F;
            this.iepsLabel.Font = new System.Drawing.Font("Arial", 8F);
            this.iepsLabel.LocationFloat = new DevExpress.Utils.PointFloat(667.365F, 40.00009F);
            this.iepsLabel.Name = "iepsLabel";
            this.iepsLabel.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.iepsLabel.SizeF = new System.Drawing.SizeF(114.6353F, 15F);
            this.iepsLabel.StylePriority.UseFont = false;
            this.iepsLabel.StylePriority.UseTextAlignment = false;
            this.iepsLabel.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // PageFooter
            // 
            this.PageFooter.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrPageInfo1});
            this.PageFooter.Dpi = 100F;
            this.PageFooter.HeightF = 19.79167F;
            this.PageFooter.Name = "PageFooter";
            // 
            // xrPageInfo1
            // 
            this.xrPageInfo1.Dpi = 100F;
            this.xrPageInfo1.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Bold);
            this.xrPageInfo1.Format = "Página {0} de {1}";
            this.xrPageInfo1.LocationFloat = new DevExpress.Utils.PointFloat(0F, 0F);
            this.xrPageInfo1.Name = "xrPageInfo1";
            this.xrPageInfo1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrPageInfo1.SizeF = new System.Drawing.SizeF(100F, 15F);
            this.xrPageInfo1.StylePriority.UseFont = false;
            // 
            // SalesDetailsKardexReport
            // 
            this.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.Detail,
            this.TopMargin,
            this.BottomMargin,
            this.ReportHeader,
            this.GroupFooter1,
            this.PageFooter});
            this.Margins = new System.Drawing.Printing.Margins(32, 26, 10, 5);
            this.Version = "16.2";
            ((System.ComponentModel.ISupportInitialize)(this.xrTable1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

        }

        #endregion

        private DevExpress.XtraReports.UI.DetailBand Detail;
        private DevExpress.XtraReports.UI.TopMarginBand TopMargin;
        private DevExpress.XtraReports.UI.BottomMarginBand BottomMargin;
        private DevExpress.XtraReports.UI.ReportHeaderBand ReportHeader;
        private DevExpress.XtraReports.UI.XRPictureBox xrPictureBox1;
        private DevExpress.XtraReports.UI.XRLine xrLine2;
        private DevExpress.XtraReports.UI.XRLabel xrLabel3;
        private DevExpress.XtraReports.UI.XRLabel siteLabel;
        private DevExpress.XtraReports.UI.XRLabel xrLabel4;
        private DevExpress.XtraReports.UI.XRLabel _labeTitle;
        private DevExpress.XtraReports.UI.XRLine xrLine1;
        private DevExpress.XtraReports.UI.XRLabel _labelNoVenta;
        private DevExpress.XtraReports.UI.XRLabel saNoLabel;
        private DevExpress.XtraReports.UI.XRLabel xrLabel9;
        private DevExpress.XtraReports.UI.XRLabel saDaLabel;
        private DevExpress.XtraReports.UI.XRLabel _labelFactura;
        private DevExpress.XtraReports.UI.XRLabel saInLabel;
        private DevExpress.XtraReports.UI.XRLabel xrLabel6;
        private DevExpress.XtraReports.UI.XRLabel xrLabel2;
        private DevExpress.XtraReports.UI.XRLabel xrLabel1;
        private DevExpress.XtraReports.UI.XRLine xrLine4;
        private DevExpress.XtraReports.UI.XRLine xrLine3;
        private DevExpress.XtraReports.UI.XRLabel Detalle;
        private DevExpress.XtraReports.UI.XRLabel xrLabel17;
        private DevExpress.XtraReports.UI.XRLabel xrLabel18;
        private DevExpress.XtraReports.UI.XRLabel xrLabel19;
        private DevExpress.XtraReports.UI.XRLabel xrLabel23;
        private DevExpress.XtraReports.UI.XRLabel xrLabel24;
        private DevExpress.XtraReports.UI.XRLabel xrLabel25;
        private DevExpress.XtraReports.UI.XRLabel xrLabel26;
        private DevExpress.XtraReports.UI.XRTable xrTable1;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow1;
        private DevExpress.XtraReports.UI.XRTableCell part_number;
        private DevExpress.XtraReports.UI.XRTableCell part_name;
        private DevExpress.XtraReports.UI.XRTableCell part_quantity;
        private DevExpress.XtraReports.UI.XRTableCell part_price;
        private DevExpress.XtraReports.UI.XRTableCell part_importe;
        private DevExpress.XtraReports.UI.XRTableCell part_iva;
        private DevExpress.XtraReports.UI.XRTableCell part_iva_percentage;
        private DevExpress.XtraReports.UI.XRTableCell part_ieps;
        private DevExpress.XtraReports.UI.XRTableCell part_ieps_percentage;
        private DevExpress.XtraReports.UI.XRTableCell part_total;
        private DevExpress.XtraReports.UI.GroupFooterBand GroupFooter1;
        private DevExpress.XtraReports.UI.XRLabel ivaLabel;
        private DevExpress.XtraReports.UI.XRLabel xrLabel29;
        private DevExpress.XtraReports.UI.XRLabel totalLabel;
        private DevExpress.XtraReports.UI.XRLabel subtotalLabel;
        private DevExpress.XtraReports.UI.XRLabel xrLabel33;
        private DevExpress.XtraReports.UI.XRLabel xrLabel32;
        private DevExpress.XtraReports.UI.XRLabel xrLabel31;
        private DevExpress.XtraReports.UI.XRLabel iepsLabel;
        private DevExpress.XtraReports.UI.PageFooterBand PageFooter;
        private DevExpress.XtraReports.UI.XRPageInfo xrPageInfo1;
    }
}
