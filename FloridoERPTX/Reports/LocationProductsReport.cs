﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
using System.Data;
using App.Entities.ViewModels.Inventory;
using System.Collections.Generic;

namespace FloridoERPTX.Reports
{
    public partial class LocationProductsReport : DevExpress.XtraReports.UI.XtraReport
    {
        public DataSet DataSetTableMovements = null;
        public LocationProductsReport()
        {
            InitializeComponent();
        }
        public DataSet printTable(List<InventoryItemLocationModel> items , string siteName , DateTime date , string desc , string type )
        {
            try
            {
                if(type == "location")
                {
                    xrLabelTitle.Text = "Reporte Productos Por Ubicación";
                    xrLabelLocation.Text = "Locación : ";
                }else if(type == "supplier")
                {
                    xrLabelTitle.Text = "Reporte Productos Por Proveedor";
                    xrLabelLocation.Text = "Proveedor : ";
                }else if(type == "family")
                {
                    xrLabelTitle.Text = "Reporte Productos Por Familia";
                    xrLabelLocation.Text = "Familia : ";
                }
                xrDate.Text = date.ToString();
                xrLabelSiteName.Text = siteName;
                xrLocation.Text = desc;
                DataSetTableMovements = new DataSet();
                DataTable table = new DataTable();
                DataSetTableMovements.Tables.Add(table);
                table.Columns.Add(new DataColumn("cellPartNumber"));
                table.Columns.Add(new DataColumn("cellPartDescription"));
                table.Columns.Add(new DataColumn("xrBarCode1"));

                foreach (var item in items)
                {
                    DataRow DataRowOrdered = DataSetTableMovements.Tables[0].NewRow();
                    DataRowOrdered["cellPartNumber"] = item.part_number;
                    DataRowOrdered["cellPartDescription"] = item.part_description;
                    DataRowOrdered["xrBarCode1"] = item.part_number;
                    DataSetTableMovements.Tables[0].Rows.Add(DataRowOrdered);

                }
                cellPartNumber.DataBindings.Add(new XRBinding("Text", null, "xrTableRow1.cellPartNumber"));
                cellPartDescription.DataBindings.Add(new XRBinding("Text", null, "xrTableRow1.cellPartDescription"));
                xrBarCode1.DataBindings.Add(new XRBinding("Text", null, "xrTableRow1.xrBarCode1"));
                return DataSetTableMovements;
            }
            catch (Exception)
            {

                throw;
            }
        }

    }
}
