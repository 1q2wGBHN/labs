﻿using System;
using System.Data;
using System.Collections.Generic;
using DevExpress.XtraReports.UI;
using App.Entities.ViewModels.SendDeposits;

namespace FloridoERPTX.Reports
{
    public partial class SendsReport : DevExpress.XtraReports.UI.XtraReport
    {
       
        public DataSet DataSetTableOrdered = null;
        decimal subTotal = 0;
        public SendsReport()
        {
            InitializeComponent();
          
        }
        public DataSet printTable(List<SendDepositsModel> DepositsList, string date1, string siteName) 
        {
            try
            {

                labelDate.Text = DateTime.Now.Date.ToShortDateString();
                labelDate1.Text = date1;               
                siteLabel.Text = siteName;
                DataSetTableOrdered = new DataSet();

                DataTable DataTableOrdered = new DataTable();
                DataSetTableOrdered.Tables.Add(DataTableOrdered);
                
                DataTableOrdered.Columns.Add(new DataColumn("Date"));
                DataTableOrdered.Columns.Add(new DataColumn("Cuser"));
                DataTableOrdered.Columns.Add(new DataColumn("Type"));
                DataTableOrdered.Columns.Add(new DataColumn("Origin"));
                DataTableOrdered.Columns.Add(new DataColumn("Currency"));
                DataTableOrdered.Columns.Add(new DataColumn("Amount"));
                DataTableOrdered.Columns.Add(new DataColumn("Comments"));
                DataTableOrdered.Columns.Add(new DataColumn("CDate"));                

                foreach (var item in DepositsList)
                {
                    DataRow DataRowOrdered = DataSetTableOrdered.Tables[0].NewRow();
                 
                    DataRowOrdered["Date"] = item.Date;
                    DataRowOrdered["Cuser"] = item.CUser;
                    DataRowOrdered["Type"] =  item.Type;
                    DataRowOrdered["Origin"] = item.Origin;
                    DataRowOrdered["Currency"] =  item.Currency;
                    DataRowOrdered["Amount"] =  item.Amount.ToString("C");
                    DataRowOrdered["Comments"] = item.Comments;
                    DataRowOrdered["CDate"] = item.CDate;
                    
                    DataSetTableOrdered.Tables[0].Rows.Add(DataRowOrdered);                  
                }
               
                Date.DataBindings.Add(new XRBinding("Text", null, "xrTableRowFormat.Date"));
                Cuser.DataBindings.Add(new XRBinding("Text", null, "xrTableRowFormat.Cuser"));
                Type.DataBindings.Add(new XRBinding("Text", null, "xrTableRowFormat.Type"));
                Origin.DataBindings.Add(new XRBinding("Text", null, "xrTableRowFormat.Origin"));
                Currency.DataBindings.Add(new XRBinding("Text", null, "xrTableRowFormat.Currency"));
                Amount.DataBindings.Add(new XRBinding("Text", null, "xrTableRowFormat.Amount"));
                Comments.DataBindings.Add(new XRBinding("Text", null, "xrTableRowFormat.Comments"));
                CDate.DataBindings.Add(new XRBinding("Text", null, "xrTableRowFormat.CDate"));
                
                return DataSetTableOrdered;
            }
            catch (Exception ex)
            {
                string d=ex.Message;
                throw;
            }
          
        }        
    }
}
