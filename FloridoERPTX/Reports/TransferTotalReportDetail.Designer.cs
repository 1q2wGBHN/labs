﻿namespace FloridoERPTX.Reports
{
    partial class TransferTotalReportDetail
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(TransferTotalReportDetail));
            this.Detail = new DevExpress.XtraReports.UI.DetailBand();
            this.TopMargin = new DevExpress.XtraReports.UI.TopMarginBand();
            this.BottomMargin = new DevExpress.XtraReports.UI.BottomMarginBand();
            this.DetailReport = new DevExpress.XtraReports.UI.DetailReportBand();
            this.Detail1 = new DevExpress.XtraReports.UI.DetailBand();
            this.xrTablePurchase = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow1 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrCellTransfer = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrCellETA = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrCellOriginDestinationSite = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrCellQuantity = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrCellCostUnit = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrCellImport = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrCellIVA = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrCellIEPS = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrCellTotal = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrCellStatus = new DevExpress.XtraReports.UI.XRTableCell();
            this.PageFooter = new DevExpress.XtraReports.UI.PageFooterBand();
            this.xrPageInfo1 = new DevExpress.XtraReports.UI.XRPageInfo();
            this.ReportHeader = new DevExpress.XtraReports.UI.ReportHeaderBand();
            this.xrlblEndDate = new DevExpress.XtraReports.UI.XRLabel();
            this.xrlblStartDate = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel2 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel1 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrDateShow = new DevExpress.XtraReports.UI.XRLabel();
            this.xrlblDate = new DevExpress.XtraReports.UI.XRLabel();
            this.xrPictureBox2 = new DevExpress.XtraReports.UI.XRPictureBox();
            this.xrLabel21 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrState = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel6 = new DevExpress.XtraReports.UI.XRLabel();
            this.siteLabel = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel8 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrlblCreate = new DevExpress.XtraReports.UI.XRLabel();
            this.xraddress = new DevExpress.XtraReports.UI.XRLabel();
            this.xrlblUser = new DevExpress.XtraReports.UI.XRLabel();
            this.xrlblDocument = new DevExpress.XtraReports.UI.XRLabel();
            this.xrlblOriginDestionationSite = new DevExpress.XtraReports.UI.XRLabel();
            this.xrlblDateTransfer = new DevExpress.XtraReports.UI.XRLabel();
            this.xrlblEstatus = new DevExpress.XtraReports.UI.XRLabel();
            this.xrlblImport = new DevExpress.XtraReports.UI.XRLabel();
            this.xrlblIVA = new DevExpress.XtraReports.UI.XRLabel();
            this.xrlblIEPS = new DevExpress.XtraReports.UI.XRLabel();
            this.xrlblSubTotal = new DevExpress.XtraReports.UI.XRLabel();
            this.GroupFooter1 = new DevExpress.XtraReports.UI.GroupFooterBand();
            this.xrlblMxnImport = new DevExpress.XtraReports.UI.XRLabel();
            this.xrlblMxnTotal = new DevExpress.XtraReports.UI.XRLabel();
            this.xrlblMxnIEPS = new DevExpress.XtraReports.UI.XRLabel();
            this.xrlblMxnIVA = new DevExpress.XtraReports.UI.XRLabel();
            this.xrlblMXN = new DevExpress.XtraReports.UI.XRLabel();
            this.xrlblTotalMXN = new DevExpress.XtraReports.UI.XRLabel();
            this.xrlblUnitCost = new DevExpress.XtraReports.UI.XRLabel();
            this.xrlblQuantity = new DevExpress.XtraReports.UI.XRLabel();
            this.xrlblFamily = new DevExpress.XtraReports.UI.XRLabel();
            this.xrlblDepartment = new DevExpress.XtraReports.UI.XRLabel();
            this.xrlblPartNumber = new DevExpress.XtraReports.UI.XRLabel();
            this.xrlblDescription = new DevExpress.XtraReports.UI.XRLabel();
            this.xrCellFamily = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrCellDeparment = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrCellDescription = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrCellPartNumber = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrlblTransType = new DevExpress.XtraReports.UI.XRLabel();
            this.xrlblTransfTypeShow = new DevExpress.XtraReports.UI.XRLabel();
            ((System.ComponentModel.ISupportInitialize)(this.xrTablePurchase)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // Detail
            // 
            this.Detail.Dpi = 100F;
            this.Detail.HeightF = 0F;
            this.Detail.Name = "Detail";
            this.Detail.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.Detail.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // TopMargin
            // 
            this.TopMargin.Dpi = 100F;
            this.TopMargin.HeightF = 14F;
            this.TopMargin.Name = "TopMargin";
            this.TopMargin.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.TopMargin.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // BottomMargin
            // 
            this.BottomMargin.Dpi = 100F;
            this.BottomMargin.HeightF = 29F;
            this.BottomMargin.Name = "BottomMargin";
            this.BottomMargin.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.BottomMargin.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // DetailReport
            // 
            this.DetailReport.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.Detail1});
            this.DetailReport.Dpi = 100F;
            this.DetailReport.Level = 0;
            this.DetailReport.Name = "DetailReport";
            // 
            // Detail1
            // 
            this.Detail1.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTablePurchase});
            this.Detail1.Dpi = 100F;
            this.Detail1.HeightF = 50.09805F;
            this.Detail1.Name = "Detail1";
            // 
            // xrTablePurchase
            // 
            this.xrTablePurchase.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTablePurchase.Dpi = 100F;
            this.xrTablePurchase.LocationFloat = new DevExpress.Utils.PointFloat(0F, 0F);
            this.xrTablePurchase.Name = "xrTablePurchase";
            this.xrTablePurchase.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow1});
            this.xrTablePurchase.SizeF = new System.Drawing.SizeF(1046F, 50.09804F);
            this.xrTablePurchase.StylePriority.UseBorders = false;
            // 
            // xrTableRow1
            // 
            this.xrTableRow1.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrCellTransfer,
            this.xrCellPartNumber,
            this.xrCellDescription,
            this.xrCellDeparment,
            this.xrCellFamily,
            this.xrCellETA,
            this.xrCellOriginDestinationSite,
            this.xrCellQuantity,
            this.xrCellCostUnit,
            this.xrCellImport,
            this.xrCellIVA,
            this.xrCellIEPS,
            this.xrCellTotal,
            this.xrCellStatus});
            this.xrTableRow1.Dpi = 100F;
            this.xrTableRow1.Name = "xrTableRow1";
            this.xrTableRow1.Weight = 1D;
            // 
            // xrCellTransfer
            // 
            this.xrCellTransfer.Dpi = 100F;
            this.xrCellTransfer.Font = new System.Drawing.Font("Times New Roman", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrCellTransfer.Name = "xrCellTransfer";
            this.xrCellTransfer.StylePriority.UseFont = false;
            this.xrCellTransfer.StylePriority.UseTextAlignment = false;
            this.xrCellTransfer.Text = "xrCellTransfer";
            this.xrCellTransfer.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrCellTransfer.Weight = 0.58917352525241407D;
            // 
            // xrCellETA
            // 
            this.xrCellETA.Dpi = 100F;
            this.xrCellETA.Font = new System.Drawing.Font("Times New Roman", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrCellETA.Name = "xrCellETA";
            this.xrCellETA.StylePriority.UseFont = false;
            this.xrCellETA.StylePriority.UseTextAlignment = false;
            this.xrCellETA.Text = "xrCellETA";
            this.xrCellETA.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrCellETA.Weight = 0.51759771701294932D;
            // 
            // xrCellOriginDestinationSite
            // 
            this.xrCellOriginDestinationSite.Dpi = 100F;
            this.xrCellOriginDestinationSite.Font = new System.Drawing.Font("Times New Roman", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrCellOriginDestinationSite.Name = "xrCellOriginDestinationSite";
            this.xrCellOriginDestinationSite.StylePriority.UseFont = false;
            this.xrCellOriginDestinationSite.StylePriority.UseTextAlignment = false;
            this.xrCellOriginDestinationSite.Text = "xrCellOriginDestinationSite";
            this.xrCellOriginDestinationSite.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrCellOriginDestinationSite.Weight = 0.82035089205397826D;
            // 
            // xrCellQuantity
            // 
            this.xrCellQuantity.Dpi = 100F;
            this.xrCellQuantity.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrCellQuantity.Name = "xrCellQuantity";
            this.xrCellQuantity.StylePriority.UseFont = false;
            this.xrCellQuantity.StylePriority.UseTextAlignment = false;
            this.xrCellQuantity.Text = "xrCellQuantity";
            this.xrCellQuantity.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrCellQuantity.Weight = 0.53528754444864191D;
            // 
            // xrCellCostUnit
            // 
            this.xrCellCostUnit.Dpi = 100F;
            this.xrCellCostUnit.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrCellCostUnit.Name = "xrCellCostUnit";
            this.xrCellCostUnit.StylePriority.UseFont = false;
            this.xrCellCostUnit.StylePriority.UseTextAlignment = false;
            this.xrCellCostUnit.Text = "xrCellCostUnit";
            this.xrCellCostUnit.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrCellCostUnit.Weight = 0.56730310919858862D;
            // 
            // xrCellImport
            // 
            this.xrCellImport.Dpi = 100F;
            this.xrCellImport.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrCellImport.Name = "xrCellImport";
            this.xrCellImport.StylePriority.UseFont = false;
            this.xrCellImport.StylePriority.UseTextAlignment = false;
            this.xrCellImport.Text = "xrCellImport";
            this.xrCellImport.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrCellImport.Weight = 0.84380775511895956D;
            // 
            // xrCellIVA
            // 
            this.xrCellIVA.Dpi = 100F;
            this.xrCellIVA.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrCellIVA.Name = "xrCellIVA";
            this.xrCellIVA.StylePriority.UseFont = false;
            this.xrCellIVA.StylePriority.UseTextAlignment = false;
            this.xrCellIVA.Text = "xrCellIVA";
            this.xrCellIVA.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrCellIVA.Weight = 0.75659627594569978D;
            // 
            // xrCellIEPS
            // 
            this.xrCellIEPS.Dpi = 100F;
            this.xrCellIEPS.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrCellIEPS.Name = "xrCellIEPS";
            this.xrCellIEPS.StylePriority.UseFont = false;
            this.xrCellIEPS.StylePriority.UseTextAlignment = false;
            this.xrCellIEPS.Text = "xrCellIEPS";
            this.xrCellIEPS.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrCellIEPS.Weight = 0.67877885909361413D;
            // 
            // xrCellTotal
            // 
            this.xrCellTotal.Dpi = 100F;
            this.xrCellTotal.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrCellTotal.Name = "xrCellTotal";
            this.xrCellTotal.StylePriority.UseFont = false;
            this.xrCellTotal.StylePriority.UseTextAlignment = false;
            this.xrCellTotal.Text = "xrCellTotal";
            this.xrCellTotal.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrCellTotal.Weight = 0.90381888561375978D;
            // 
            // xrCellStatus
            // 
            this.xrCellStatus.Dpi = 100F;
            this.xrCellStatus.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrCellStatus.Name = "xrCellStatus";
            this.xrCellStatus.StylePriority.UseFont = false;
            this.xrCellStatus.StylePriority.UseTextAlignment = false;
            this.xrCellStatus.Text = "xrCellStatus";
            this.xrCellStatus.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrCellStatus.Weight = 0.65052423948560423D;
            // 
            // PageFooter
            // 
            this.PageFooter.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrPageInfo1});
            this.PageFooter.Dpi = 100F;
            this.PageFooter.HeightF = 33.33333F;
            this.PageFooter.Name = "PageFooter";
            // 
            // xrPageInfo1
            // 
            this.xrPageInfo1.Dpi = 100F;
            this.xrPageInfo1.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Bold);
            this.xrPageInfo1.Format = "Página {0} de {1}";
            this.xrPageInfo1.LocationFloat = new DevExpress.Utils.PointFloat(24.04175F, 9.999974F);
            this.xrPageInfo1.Name = "xrPageInfo1";
            this.xrPageInfo1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrPageInfo1.SizeF = new System.Drawing.SizeF(100F, 15F);
            this.xrPageInfo1.StylePriority.UseFont = false;
            // 
            // ReportHeader
            // 
            this.ReportHeader.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrlblTransType,
            this.xrlblTransfTypeShow,
            this.xrlblDescription,
            this.xrlblPartNumber,
            this.xrlblDepartment,
            this.xrlblFamily,
            this.xrlblQuantity,
            this.xrlblEndDate,
            this.xrlblStartDate,
            this.xrLabel2,
            this.xrLabel1,
            this.xrDateShow,
            this.xrlblDate,
            this.xrPictureBox2,
            this.xrLabel21,
            this.xrState,
            this.xrLabel6,
            this.siteLabel,
            this.xrLabel8,
            this.xrlblCreate,
            this.xraddress,
            this.xrlblUser,
            this.xrlblDocument,
            this.xrlblOriginDestionationSite,
            this.xrlblDateTransfer,
            this.xrlblEstatus,
            this.xrlblUnitCost,
            this.xrlblImport,
            this.xrlblIVA,
            this.xrlblIEPS,
            this.xrlblSubTotal});
            this.ReportHeader.Dpi = 100F;
            this.ReportHeader.HeightF = 167.9167F;
            this.ReportHeader.Name = "ReportHeader";
            // 
            // xrlblEndDate
            // 
            this.xrlblEndDate.Dpi = 100F;
            this.xrlblEndDate.Font = new System.Drawing.Font("Arial", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrlblEndDate.LocationFloat = new DevExpress.Utils.PointFloat(464.8964F, 81.45834F);
            this.xrlblEndDate.Name = "xrlblEndDate";
            this.xrlblEndDate.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrlblEndDate.SizeF = new System.Drawing.SizeF(141.1528F, 15.00001F);
            this.xrlblEndDate.StylePriority.UseFont = false;
            // 
            // xrlblStartDate
            // 
            this.xrlblStartDate.Dpi = 100F;
            this.xrlblStartDate.Font = new System.Drawing.Font("Arial", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrlblStartDate.LocationFloat = new DevExpress.Utils.PointFloat(158.5922F, 81.45831F);
            this.xrlblStartDate.Name = "xrlblStartDate";
            this.xrlblStartDate.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrlblStartDate.SizeF = new System.Drawing.SizeF(141.1528F, 15F);
            this.xrlblStartDate.StylePriority.UseFont = false;
            // 
            // xrLabel2
            // 
            this.xrLabel2.Dpi = 100F;
            this.xrLabel2.Font = new System.Drawing.Font("Arial", 7F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel2.LocationFloat = new DevExpress.Utils.PointFloat(348.1522F, 81.45831F);
            this.xrLabel2.Multiline = true;
            this.xrLabel2.Name = "xrLabel2";
            this.xrLabel2.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel2.SizeF = new System.Drawing.SizeF(97.49402F, 15F);
            this.xrLabel2.StylePriority.UseFont = false;
            this.xrLabel2.Text = "Fecha Final:\r\n";
            // 
            // xrLabel1
            // 
            this.xrLabel1.Dpi = 100F;
            this.xrLabel1.Font = new System.Drawing.Font("Arial", 7F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel1.LocationFloat = new DevExpress.Utils.PointFloat(65.52214F, 81.45831F);
            this.xrLabel1.Name = "xrLabel1";
            this.xrLabel1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel1.SizeF = new System.Drawing.SizeF(81.86905F, 15F);
            this.xrLabel1.StylePriority.UseFont = false;
            this.xrLabel1.Text = "Fecha Inicio:";
            // 
            // xrDateShow
            // 
            this.xrDateShow.Dpi = 100F;
            this.xrDateShow.Font = new System.Drawing.Font("Arial", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrDateShow.LocationFloat = new DevExpress.Utils.PointFloat(158.5922F, 107.8215F);
            this.xrDateShow.Name = "xrDateShow";
            this.xrDateShow.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrDateShow.SizeF = new System.Drawing.SizeF(141.1528F, 15F);
            this.xrDateShow.StylePriority.UseFont = false;
            // 
            // xrlblDate
            // 
            this.xrlblDate.Dpi = 100F;
            this.xrlblDate.Font = new System.Drawing.Font("Arial", 7F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrlblDate.LocationFloat = new DevExpress.Utils.PointFloat(65.52214F, 107.8215F);
            this.xrlblDate.Name = "xrlblDate";
            this.xrlblDate.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrlblDate.SizeF = new System.Drawing.SizeF(81.86905F, 15F);
            this.xrlblDate.StylePriority.UseFont = false;
            this.xrlblDate.Text = "Fecha Impresión:";
            // 
            // xrPictureBox2
            // 
            this.xrPictureBox2.Dpi = 100F;
            this.xrPictureBox2.Image = ((System.Drawing.Image)(resources.GetObject("xrPictureBox2.Image")));
            this.xrPictureBox2.LocationFloat = new DevExpress.Utils.PointFloat(887.6508F, 21.45832F);
            this.xrPictureBox2.Name = "xrPictureBox2";
            this.xrPictureBox2.SizeF = new System.Drawing.SizeF(115.963F, 75F);
            this.xrPictureBox2.Sizing = DevExpress.XtraPrinting.ImageSizeMode.ZoomImage;
            // 
            // xrLabel21
            // 
            this.xrLabel21.Dpi = 100F;
            this.xrLabel21.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel21.LocationFloat = new DevExpress.Utils.PointFloat(65.52213F, 16.45832F);
            this.xrLabel21.Name = "xrLabel21";
            this.xrLabel21.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel21.SizeF = new System.Drawing.SizeF(357.7047F, 20F);
            this.xrLabel21.StylePriority.UseBorders = false;
            this.xrLabel21.StylePriority.UseBorderWidth = false;
            this.xrLabel21.StylePriority.UseFont = false;
            this.xrLabel21.StylePriority.UseTextAlignment = false;
            this.xrLabel21.Text = "Reporte de Transferencias Detalle";
            // 
            // xrState
            // 
            this.xrState.Dpi = 100F;
            this.xrState.Font = new System.Drawing.Font("Arial", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrState.LocationFloat = new DevExpress.Utils.PointFloat(655.4581F, 81.45831F);
            this.xrState.Name = "xrState";
            this.xrState.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrState.SizeF = new System.Drawing.SizeF(200.4999F, 15F);
            this.xrState.StylePriority.UseFont = false;
            this.xrState.StylePriority.UseTextAlignment = false;
            this.xrState.Text = "- Estado";
            this.xrState.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // xrLabel6
            // 
            this.xrLabel6.Dpi = 100F;
            this.xrLabel6.Font = new System.Drawing.Font("Arial", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel6.LocationFloat = new DevExpress.Utils.PointFloat(652.3531F, 21.45832F);
            this.xrLabel6.Name = "xrLabel6";
            this.xrLabel6.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel6.SizeF = new System.Drawing.SizeF(203.605F, 15F);
            this.xrLabel6.StylePriority.UseFont = false;
            this.xrLabel6.StylePriority.UseTextAlignment = false;
            this.xrLabel6.Text = "DISTRIBUIDORA EL FLORIDO S.A. DE C.V.";
            this.xrLabel6.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // siteLabel
            // 
            this.siteLabel.Dpi = 100F;
            this.siteLabel.Font = new System.Drawing.Font("Arial", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.siteLabel.LocationFloat = new DevExpress.Utils.PointFloat(655.4586F, 36.45832F);
            this.siteLabel.Multiline = true;
            this.siteLabel.Name = "siteLabel";
            this.siteLabel.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.siteLabel.SizeF = new System.Drawing.SizeF(200.4999F, 15F);
            this.siteLabel.StylePriority.UseFont = false;
            this.siteLabel.StylePriority.UseTextAlignment = false;
            this.siteLabel.Text = "*FLORIDO\r\n";
            this.siteLabel.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // xrLabel8
            // 
            this.xrLabel8.Dpi = 100F;
            this.xrLabel8.Font = new System.Drawing.Font("Arial", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel8.LocationFloat = new DevExpress.Utils.PointFloat(769.6327F, 51.45831F);
            this.xrLabel8.Name = "xrLabel8";
            this.xrLabel8.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel8.SizeF = new System.Drawing.SizeF(86.32568F, 15F);
            this.xrLabel8.StylePriority.UseFont = false;
            this.xrLabel8.StylePriority.UseTextAlignment = false;
            this.xrLabel8.Text = "DFL-950802-5N4";
            this.xrLabel8.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // xrlblCreate
            // 
            this.xrlblCreate.Dpi = 100F;
            this.xrlblCreate.Font = new System.Drawing.Font("Arial", 7F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrlblCreate.LocationFloat = new DevExpress.Utils.PointFloat(65.52214F, 51.45831F);
            this.xrlblCreate.Name = "xrlblCreate";
            this.xrlblCreate.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrlblCreate.SizeF = new System.Drawing.SizeF(81.86905F, 15F);
            this.xrlblCreate.StylePriority.UseFont = false;
            this.xrlblCreate.Text = "Realizó:";
            // 
            // xraddress
            // 
            this.xraddress.Dpi = 100F;
            this.xraddress.Font = new System.Drawing.Font("Arial", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xraddress.LocationFloat = new DevExpress.Utils.PointFloat(655.4586F, 66.45829F);
            this.xraddress.Name = "xraddress";
            this.xraddress.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xraddress.SizeF = new System.Drawing.SizeF(200.4999F, 15F);
            this.xraddress.StylePriority.UseFont = false;
            this.xraddress.StylePriority.UseTextAlignment = false;
            this.xraddress.Text = "- Dirección";
            this.xraddress.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // xrlblUser
            // 
            this.xrlblUser.Dpi = 100F;
            this.xrlblUser.Font = new System.Drawing.Font("Arial", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrlblUser.LocationFloat = new DevExpress.Utils.PointFloat(158.5922F, 51.45831F);
            this.xrlblUser.Name = "xrlblUser";
            this.xrlblUser.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrlblUser.SizeF = new System.Drawing.SizeF(183.3846F, 15F);
            this.xrlblUser.StylePriority.UseFont = false;
            // 
            // xrlblDocument
            // 
            this.xrlblDocument.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrlblDocument.Dpi = 100F;
            this.xrlblDocument.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrlblDocument.LocationFloat = new DevExpress.Utils.PointFloat(0F, 144.9167F);
            this.xrlblDocument.Name = "xrlblDocument";
            this.xrlblDocument.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrlblDocument.SizeF = new System.Drawing.SizeF(60.02223F, 23F);
            this.xrlblDocument.StylePriority.UseBorders = false;
            this.xrlblDocument.StylePriority.UseFont = false;
            this.xrlblDocument.StylePriority.UseTextAlignment = false;
            this.xrlblDocument.Text = "Documento";
            this.xrlblDocument.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrlblOriginDestionationSite
            // 
            this.xrlblOriginDestionationSite.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrlblOriginDestionationSite.Dpi = 100F;
            this.xrlblOriginDestionationSite.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrlblOriginDestionationSite.LocationFloat = new DevExpress.Utils.PointFloat(459.5581F, 144.9167F);
            this.xrlblOriginDestionationSite.Name = "xrlblOriginDestionationSite";
            this.xrlblOriginDestionationSite.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrlblOriginDestionationSite.SizeF = new System.Drawing.SizeF(83.57333F, 23F);
            this.xrlblOriginDestionationSite.StylePriority.UseBorders = false;
            this.xrlblOriginDestionationSite.StylePriority.UseFont = false;
            this.xrlblOriginDestionationSite.StylePriority.UseTextAlignment = false;
            this.xrlblOriginDestionationSite.Text = "Sitio Origen";
            this.xrlblOriginDestionationSite.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrlblDateTransfer
            // 
            this.xrlblDateTransfer.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrlblDateTransfer.Dpi = 100F;
            this.xrlblDateTransfer.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrlblDateTransfer.LocationFloat = new DevExpress.Utils.PointFloat(406.8277F, 144.9167F);
            this.xrlblDateTransfer.Name = "xrlblDateTransfer";
            this.xrlblDateTransfer.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrlblDateTransfer.SizeF = new System.Drawing.SizeF(52.73041F, 23F);
            this.xrlblDateTransfer.StylePriority.UseBorders = false;
            this.xrlblDateTransfer.StylePriority.UseFont = false;
            this.xrlblDateTransfer.StylePriority.UseTextAlignment = false;
            this.xrlblDateTransfer.Text = "Fecha";
            this.xrlblDateTransfer.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrlblEstatus
            // 
            this.xrlblEstatus.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrlblEstatus.Dpi = 100F;
            this.xrlblEstatus.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrlblEstatus.LocationFloat = new DevExpress.Utils.PointFloat(979.7277F, 144.9167F);
            this.xrlblEstatus.Name = "xrlblEstatus";
            this.xrlblEstatus.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrlblEstatus.SizeF = new System.Drawing.SizeF(66.27222F, 23F);
            this.xrlblEstatus.StylePriority.UseBorders = false;
            this.xrlblEstatus.StylePriority.UseFont = false;
            this.xrlblEstatus.StylePriority.UseTextAlignment = false;
            this.xrlblEstatus.Text = "Estatus";
            this.xrlblEstatus.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrlblImport
            // 
            this.xrlblImport.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrlblImport.Dpi = 100F;
            this.xrlblImport.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrlblImport.LocationFloat = new DevExpress.Utils.PointFloat(655.4586F, 144.9167F);
            this.xrlblImport.Name = "xrlblImport";
            this.xrlblImport.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrlblImport.SizeF = new System.Drawing.SizeF(85.96271F, 23F);
            this.xrlblImport.StylePriority.UseBorders = false;
            this.xrlblImport.StylePriority.UseFont = false;
            this.xrlblImport.StylePriority.UseTextAlignment = false;
            this.xrlblImport.Text = "Importe";
            this.xrlblImport.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrlblIVA
            // 
            this.xrlblIVA.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrlblIVA.Dpi = 100F;
            this.xrlblIVA.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrlblIVA.LocationFloat = new DevExpress.Utils.PointFloat(741.4215F, 144.9167F);
            this.xrlblIVA.Name = "xrlblIVA";
            this.xrlblIVA.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrlblIVA.SizeF = new System.Drawing.SizeF(77.07874F, 23F);
            this.xrlblIVA.StylePriority.UseBorders = false;
            this.xrlblIVA.StylePriority.UseFont = false;
            this.xrlblIVA.StylePriority.UseTextAlignment = false;
            this.xrlblIVA.Text = "IVA";
            this.xrlblIVA.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrlblIEPS
            // 
            this.xrlblIEPS.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrlblIEPS.Dpi = 100F;
            this.xrlblIEPS.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrlblIEPS.LocationFloat = new DevExpress.Utils.PointFloat(818.5002F, 144.9167F);
            this.xrlblIEPS.Name = "xrlblIEPS";
            this.xrlblIEPS.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrlblIEPS.SizeF = new System.Drawing.SizeF(69.15057F, 23F);
            this.xrlblIEPS.StylePriority.UseBorders = false;
            this.xrlblIEPS.StylePriority.UseFont = false;
            this.xrlblIEPS.StylePriority.UseTextAlignment = false;
            this.xrlblIEPS.Text = "IEPS";
            this.xrlblIEPS.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrlblSubTotal
            // 
            this.xrlblSubTotal.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrlblSubTotal.Dpi = 100F;
            this.xrlblSubTotal.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrlblSubTotal.LocationFloat = new DevExpress.Utils.PointFloat(887.6508F, 144.9167F);
            this.xrlblSubTotal.Name = "xrlblSubTotal";
            this.xrlblSubTotal.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrlblSubTotal.SizeF = new System.Drawing.SizeF(92.07684F, 23F);
            this.xrlblSubTotal.StylePriority.UseBorders = false;
            this.xrlblSubTotal.StylePriority.UseFont = false;
            this.xrlblSubTotal.StylePriority.UseTextAlignment = false;
            this.xrlblSubTotal.Text = "Total";
            this.xrlblSubTotal.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // GroupFooter1
            // 
            this.GroupFooter1.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrlblMxnImport,
            this.xrlblMxnTotal,
            this.xrlblMxnIEPS,
            this.xrlblMxnIVA,
            this.xrlblMXN,
            this.xrlblTotalMXN});
            this.GroupFooter1.Dpi = 100F;
            this.GroupFooter1.HeightF = 55.20833F;
            this.GroupFooter1.Name = "GroupFooter1";
            // 
            // xrlblMxnImport
            // 
            this.xrlblMxnImport.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrlblMxnImport.Dpi = 100F;
            this.xrlblMxnImport.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrlblMxnImport.LocationFloat = new DevExpress.Utils.PointFloat(655.4581F, 0F);
            this.xrlblMxnImport.Name = "xrlblMxnImport";
            this.xrlblMxnImport.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrlblMxnImport.SizeF = new System.Drawing.SizeF(85.96295F, 22.99998F);
            this.xrlblMxnImport.StylePriority.UseBorders = false;
            this.xrlblMxnImport.StylePriority.UseFont = false;
            this.xrlblMxnImport.StylePriority.UseTextAlignment = false;
            this.xrlblMxnImport.Text = "$ 0";
            this.xrlblMxnImport.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrlblMxnTotal
            // 
            this.xrlblMxnTotal.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrlblMxnTotal.Dpi = 100F;
            this.xrlblMxnTotal.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrlblMxnTotal.LocationFloat = new DevExpress.Utils.PointFloat(887.6508F, 0F);
            this.xrlblMxnTotal.Name = "xrlblMxnTotal";
            this.xrlblMxnTotal.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrlblMxnTotal.SizeF = new System.Drawing.SizeF(158.3493F, 22.99998F);
            this.xrlblMxnTotal.StylePriority.UseBorders = false;
            this.xrlblMxnTotal.StylePriority.UseFont = false;
            this.xrlblMxnTotal.StylePriority.UseTextAlignment = false;
            this.xrlblMxnTotal.Text = "$ 0";
            this.xrlblMxnTotal.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrlblMxnIEPS
            // 
            this.xrlblMxnIEPS.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrlblMxnIEPS.Dpi = 100F;
            this.xrlblMxnIEPS.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrlblMxnIEPS.LocationFloat = new DevExpress.Utils.PointFloat(818.5F, 0F);
            this.xrlblMxnIEPS.Name = "xrlblMxnIEPS";
            this.xrlblMxnIEPS.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrlblMxnIEPS.SizeF = new System.Drawing.SizeF(69.15082F, 22.99998F);
            this.xrlblMxnIEPS.StylePriority.UseBorders = false;
            this.xrlblMxnIEPS.StylePriority.UseFont = false;
            this.xrlblMxnIEPS.StylePriority.UseTextAlignment = false;
            this.xrlblMxnIEPS.Text = "$ 0";
            this.xrlblMxnIEPS.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrlblMxnIVA
            // 
            this.xrlblMxnIVA.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrlblMxnIVA.Dpi = 100F;
            this.xrlblMxnIVA.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrlblMxnIVA.LocationFloat = new DevExpress.Utils.PointFloat(741.4215F, 0F);
            this.xrlblMxnIVA.Name = "xrlblMxnIVA";
            this.xrlblMxnIVA.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrlblMxnIVA.SizeF = new System.Drawing.SizeF(77.07849F, 22.99998F);
            this.xrlblMxnIVA.StylePriority.UseBorders = false;
            this.xrlblMxnIVA.StylePriority.UseFont = false;
            this.xrlblMxnIVA.StylePriority.UseTextAlignment = false;
            this.xrlblMxnIVA.Text = "$ 0";
            this.xrlblMxnIVA.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrlblMXN
            // 
            this.xrlblMXN.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrlblMXN.Dpi = 100F;
            this.xrlblMXN.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrlblMXN.LocationFloat = new DevExpress.Utils.PointFloat(60.02223F, 0F);
            this.xrlblMXN.Name = "xrlblMXN";
            this.xrlblMXN.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrlblMXN.SizeF = new System.Drawing.SizeF(595.4359F, 22.99998F);
            this.xrlblMXN.StylePriority.UseBorders = false;
            this.xrlblMXN.StylePriority.UseFont = false;
            this.xrlblMXN.StylePriority.UseTextAlignment = false;
            this.xrlblMXN.Text = "MXN";
            this.xrlblMXN.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrlblTotalMXN
            // 
            this.xrlblTotalMXN.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrlblTotalMXN.Dpi = 100F;
            this.xrlblTotalMXN.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrlblTotalMXN.LocationFloat = new DevExpress.Utils.PointFloat(0F, 0F);
            this.xrlblTotalMXN.Name = "xrlblTotalMXN";
            this.xrlblTotalMXN.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrlblTotalMXN.SizeF = new System.Drawing.SizeF(60.02223F, 22.99998F);
            this.xrlblTotalMXN.StylePriority.UseBorders = false;
            this.xrlblTotalMXN.StylePriority.UseFont = false;
            this.xrlblTotalMXN.StylePriority.UseTextAlignment = false;
            this.xrlblTotalMXN.Text = "Total";
            this.xrlblTotalMXN.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrlblUnitCost
            // 
            this.xrlblUnitCost.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrlblUnitCost.Dpi = 100F;
            this.xrlblUnitCost.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrlblUnitCost.LocationFloat = new DevExpress.Utils.PointFloat(597.6644F, 144.9167F);
            this.xrlblUnitCost.Name = "xrlblUnitCost";
            this.xrlblUnitCost.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrlblUnitCost.SizeF = new System.Drawing.SizeF(57.79419F, 23F);
            this.xrlblUnitCost.StylePriority.UseBorders = false;
            this.xrlblUnitCost.StylePriority.UseFont = false;
            this.xrlblUnitCost.StylePriority.UseTextAlignment = false;
            this.xrlblUnitCost.Text = "Costo Uni.";
            this.xrlblUnitCost.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrlblQuantity
            // 
            this.xrlblQuantity.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrlblQuantity.Dpi = 100F;
            this.xrlblQuantity.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrlblQuantity.LocationFloat = new DevExpress.Utils.PointFloat(543.1315F, 144.9167F);
            this.xrlblQuantity.Name = "xrlblQuantity";
            this.xrlblQuantity.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrlblQuantity.SizeF = new System.Drawing.SizeF(54.53278F, 23F);
            this.xrlblQuantity.StylePriority.UseBorders = false;
            this.xrlblQuantity.StylePriority.UseFont = false;
            this.xrlblQuantity.StylePriority.UseTextAlignment = false;
            this.xrlblQuantity.Text = "Cantidad";
            this.xrlblQuantity.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrlblFamily
            // 
            this.xrlblFamily.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrlblFamily.Dpi = 100F;
            this.xrlblFamily.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrlblFamily.LocationFloat = new DevExpress.Utils.PointFloat(315.0556F, 144.9167F);
            this.xrlblFamily.Name = "xrlblFamily";
            this.xrlblFamily.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrlblFamily.SizeF = new System.Drawing.SizeF(91.77203F, 23F);
            this.xrlblFamily.StylePriority.UseBorders = false;
            this.xrlblFamily.StylePriority.UseFont = false;
            this.xrlblFamily.StylePriority.UseTextAlignment = false;
            this.xrlblFamily.Text = "Familia";
            this.xrlblFamily.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrlblDepartment
            // 
            this.xrlblDepartment.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrlblDepartment.Dpi = 100F;
            this.xrlblDepartment.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrlblDepartment.LocationFloat = new DevExpress.Utils.PointFloat(209.6282F, 144.9167F);
            this.xrlblDepartment.Name = "xrlblDepartment";
            this.xrlblDepartment.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrlblDepartment.SizeF = new System.Drawing.SizeF(105.4274F, 23F);
            this.xrlblDepartment.StylePriority.UseBorders = false;
            this.xrlblDepartment.StylePriority.UseFont = false;
            this.xrlblDepartment.StylePriority.UseTextAlignment = false;
            this.xrlblDepartment.Text = "Departamento";
            this.xrlblDepartment.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrlblPartNumber
            // 
            this.xrlblPartNumber.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrlblPartNumber.Dpi = 100F;
            this.xrlblPartNumber.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrlblPartNumber.LocationFloat = new DevExpress.Utils.PointFloat(60.02223F, 144.9167F);
            this.xrlblPartNumber.Name = "xrlblPartNumber";
            this.xrlblPartNumber.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrlblPartNumber.SizeF = new System.Drawing.SizeF(60.02223F, 23F);
            this.xrlblPartNumber.StylePriority.UseBorders = false;
            this.xrlblPartNumber.StylePriority.UseFont = false;
            this.xrlblPartNumber.StylePriority.UseTextAlignment = false;
            this.xrlblPartNumber.Text = "Código";
            this.xrlblPartNumber.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrlblDescription
            // 
            this.xrlblDescription.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrlblDescription.Dpi = 100F;
            this.xrlblDescription.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrlblDescription.LocationFloat = new DevExpress.Utils.PointFloat(120.0445F, 144.9167F);
            this.xrlblDescription.Name = "xrlblDescription";
            this.xrlblDescription.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrlblDescription.SizeF = new System.Drawing.SizeF(89.58372F, 23F);
            this.xrlblDescription.StylePriority.UseBorders = false;
            this.xrlblDescription.StylePriority.UseFont = false;
            this.xrlblDescription.StylePriority.UseTextAlignment = false;
            this.xrlblDescription.Text = "Descripción";
            this.xrlblDescription.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrCellFamily
            // 
            this.xrCellFamily.Dpi = 100F;
            this.xrCellFamily.Font = new System.Drawing.Font("Times New Roman", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrCellFamily.Name = "xrCellFamily";
            this.xrCellFamily.StylePriority.UseFont = false;
            this.xrCellFamily.StylePriority.UseTextAlignment = false;
            this.xrCellFamily.Text = "xrCellFamily";
            this.xrCellFamily.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrCellFamily.Weight = 0.90082783426964874D;
            // 
            // xrCellDeparment
            // 
            this.xrCellDeparment.Dpi = 100F;
            this.xrCellDeparment.Font = new System.Drawing.Font("Times New Roman", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrCellDeparment.Name = "xrCellDeparment";
            this.xrCellDeparment.StylePriority.UseFont = false;
            this.xrCellDeparment.StylePriority.UseTextAlignment = false;
            this.xrCellDeparment.Text = "xrCellDeparment";
            this.xrCellDeparment.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrCellDeparment.Weight = 1.0348671561359917D;
            // 
            // xrCellDescription
            // 
            this.xrCellDescription.Dpi = 100F;
            this.xrCellDescription.Font = new System.Drawing.Font("Times New Roman", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrCellDescription.Name = "xrCellDescription";
            this.xrCellDescription.StylePriority.UseFont = false;
            this.xrCellDescription.StylePriority.UseTextAlignment = false;
            this.xrCellDescription.Text = "xrCellDescription";
            this.xrCellDescription.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrCellDescription.Weight = 0.87934547597833457D;
            // 
            // xrCellPartNumber
            // 
            this.xrCellPartNumber.Dpi = 100F;
            this.xrCellPartNumber.Font = new System.Drawing.Font("Times New Roman", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrCellPartNumber.Name = "xrCellPartNumber";
            this.xrCellPartNumber.StylePriority.UseFont = false;
            this.xrCellPartNumber.StylePriority.UseTextAlignment = false;
            this.xrCellPartNumber.Text = "xrCellPartNumber";
            this.xrCellPartNumber.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrCellPartNumber.Weight = 0.58917333802861471D;
            // 
            // xrlblTransType
            // 
            this.xrlblTransType.Dpi = 100F;
            this.xrlblTransType.Font = new System.Drawing.Font("Arial", 7F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrlblTransType.LocationFloat = new DevExpress.Utils.PointFloat(348.1522F, 51.45831F);
            this.xrlblTransType.Multiline = true;
            this.xrlblTransType.Name = "xrlblTransType";
            this.xrlblTransType.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrlblTransType.SizeF = new System.Drawing.SizeF(139.6609F, 15F);
            this.xrlblTransType.StylePriority.UseFont = false;
            this.xrlblTransType.Text = "Tipo de Transferencias:\r\n";
            // 
            // xrlblTransfTypeShow
            // 
            this.xrlblTransfTypeShow.Dpi = 100F;
            this.xrlblTransfTypeShow.Font = new System.Drawing.Font("Arial", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrlblTransfTypeShow.LocationFloat = new DevExpress.Utils.PointFloat(501.1317F, 51.45831F);
            this.xrlblTransfTypeShow.Name = "xrlblTransfTypeShow";
            this.xrlblTransfTypeShow.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrlblTransfTypeShow.SizeF = new System.Drawing.SizeF(105.4179F, 15F);
            this.xrlblTransfTypeShow.StylePriority.UseFont = false;
            // 
            // TransferTotalReportDetail
            // 
            this.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.Detail,
            this.TopMargin,
            this.BottomMargin,
            this.DetailReport,
            this.PageFooter,
            this.ReportHeader,
            this.GroupFooter1});
            this.Landscape = true;
            this.Margins = new System.Drawing.Printing.Margins(26, 28, 14, 29);
            this.PageHeight = 850;
            this.PageWidth = 1100;
            this.Version = "16.2";
            ((System.ComponentModel.ISupportInitialize)(this.xrTablePurchase)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

        }

        #endregion

        private DevExpress.XtraReports.UI.DetailBand Detail;
        private DevExpress.XtraReports.UI.TopMarginBand TopMargin;
        private DevExpress.XtraReports.UI.BottomMarginBand BottomMargin;
        private DevExpress.XtraReports.UI.DetailReportBand DetailReport;
        private DevExpress.XtraReports.UI.DetailBand Detail1;
        private DevExpress.XtraReports.UI.PageFooterBand PageFooter;
        private DevExpress.XtraReports.UI.ReportHeaderBand ReportHeader;
        private DevExpress.XtraReports.UI.XRLabel xrlblEndDate;
        private DevExpress.XtraReports.UI.XRLabel xrlblStartDate;
        private DevExpress.XtraReports.UI.XRLabel xrLabel2;
        private DevExpress.XtraReports.UI.XRLabel xrLabel1;
        private DevExpress.XtraReports.UI.XRLabel xrDateShow;
        private DevExpress.XtraReports.UI.XRLabel xrlblDate;
        private DevExpress.XtraReports.UI.XRPictureBox xrPictureBox2;
        private DevExpress.XtraReports.UI.XRLabel xrLabel21;
        private DevExpress.XtraReports.UI.XRLabel xrState;
        private DevExpress.XtraReports.UI.XRLabel xrLabel6;
        private DevExpress.XtraReports.UI.XRLabel siteLabel;
        private DevExpress.XtraReports.UI.XRLabel xrLabel8;
        private DevExpress.XtraReports.UI.XRLabel xrlblCreate;
        private DevExpress.XtraReports.UI.XRLabel xraddress;
        private DevExpress.XtraReports.UI.XRLabel xrlblUser;
        private DevExpress.XtraReports.UI.XRLabel xrlblDocument;
        private DevExpress.XtraReports.UI.XRLabel xrlblOriginDestionationSite;
        private DevExpress.XtraReports.UI.XRLabel xrlblDateTransfer;
        private DevExpress.XtraReports.UI.XRLabel xrlblEstatus;
        private DevExpress.XtraReports.UI.XRLabel xrlblImport;
        private DevExpress.XtraReports.UI.XRLabel xrlblIVA;
        private DevExpress.XtraReports.UI.XRLabel xrlblIEPS;
        private DevExpress.XtraReports.UI.XRLabel xrlblSubTotal;
        private DevExpress.XtraReports.UI.XRPageInfo xrPageInfo1;
        private DevExpress.XtraReports.UI.GroupFooterBand GroupFooter1;
        private DevExpress.XtraReports.UI.XRLabel xrlblMxnImport;
        private DevExpress.XtraReports.UI.XRLabel xrlblMxnTotal;
        private DevExpress.XtraReports.UI.XRLabel xrlblMxnIEPS;
        private DevExpress.XtraReports.UI.XRLabel xrlblMxnIVA;
        private DevExpress.XtraReports.UI.XRLabel xrlblMXN;
        private DevExpress.XtraReports.UI.XRLabel xrlblTotalMXN;
        private DevExpress.XtraReports.UI.XRTable xrTablePurchase;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow1;
        private DevExpress.XtraReports.UI.XRTableCell xrCellTransfer;
        private DevExpress.XtraReports.UI.XRTableCell xrCellETA;
        private DevExpress.XtraReports.UI.XRTableCell xrCellOriginDestinationSite;
        private DevExpress.XtraReports.UI.XRTableCell xrCellQuantity;
        private DevExpress.XtraReports.UI.XRTableCell xrCellCostUnit;
        private DevExpress.XtraReports.UI.XRTableCell xrCellImport;
        private DevExpress.XtraReports.UI.XRTableCell xrCellIVA;
        private DevExpress.XtraReports.UI.XRTableCell xrCellIEPS;
        private DevExpress.XtraReports.UI.XRTableCell xrCellTotal;
        private DevExpress.XtraReports.UI.XRTableCell xrCellStatus;
        private DevExpress.XtraReports.UI.XRLabel xrlblDescription;
        private DevExpress.XtraReports.UI.XRLabel xrlblPartNumber;
        private DevExpress.XtraReports.UI.XRLabel xrlblDepartment;
        private DevExpress.XtraReports.UI.XRLabel xrlblFamily;
        private DevExpress.XtraReports.UI.XRLabel xrlblQuantity;
        private DevExpress.XtraReports.UI.XRLabel xrlblUnitCost;
        private DevExpress.XtraReports.UI.XRTableCell xrCellPartNumber;
        private DevExpress.XtraReports.UI.XRTableCell xrCellDescription;
        private DevExpress.XtraReports.UI.XRTableCell xrCellDeparment;
        private DevExpress.XtraReports.UI.XRTableCell xrCellFamily;
        private DevExpress.XtraReports.UI.XRLabel xrlblTransType;
        private DevExpress.XtraReports.UI.XRLabel xrlblTransfTypeShow;
    }
}
