﻿using System;
using System.Data;
using DevExpress.XtraReports.UI;
using App.Entities.ViewModels.Withdrawal;
using System.Linq;
using App.Entities.ViewModels.TransactionsCustomers;

namespace FloridoERPTX.Reports
{
    public partial class PaymentCustomerReport : DevExpress.XtraReports.UI.XtraReport
    {
       
        public DataSet DataSetTableOrdered = null;        
        public PaymentCustomerReport()
        {
            InitializeComponent();
          
        }
        public DataSet printTable(TransactionsCustomersReport Model) 
        {
            try
            {
                siteLabel.Text = Model.SiteName;
                Date.Text = string.Format("{0} y {1}", Model.StartDate, Model.EndDate);
                Currency.Text = Model.Currency;
                DataSetTableOrdered = new DataSet();

                DataTable DataTableOrdered = new DataTable();
                DataSetTableOrdered.Tables.Add(DataTableOrdered);
                
                DataTableOrdered.Columns.Add(new DataColumn("ClientCode"));
                DataTableOrdered.Columns.Add(new DataColumn("ClientName"));
                DataTableOrdered.Columns.Add(new DataColumn("PaymentDate"));
                DataTableOrdered.Columns.Add(new DataColumn("Amount"));
                DataTableOrdered.Columns.Add(new DataColumn("InvoiceNo"));
                DataTableOrdered.Columns.Add(new DataColumn("InvoiceDate"));
                DataTableOrdered.Columns.Add(new DataColumn("InvoiceSubtotal"));
                DataTableOrdered.Columns.Add(new DataColumn("InvoiceTax"));
                DataTableOrdered.Columns.Add(new DataColumn("InvoiceTotal"));
                DataTableOrdered.Columns.Add(new DataColumn("CUser"));
                DataTableOrdered.Columns.Add(new DataColumn("PaymentNo"));

                foreach (var item in Model.PaymentsReport)
                {
                    DataRow DataRowOrdered = DataSetTableOrdered.Tables[0].NewRow();
                    DataRowOrdered["ClientCode"] = item.ClientCode;
                    DataRowOrdered["ClientName"] = item.ClientName;
                    DataRowOrdered["PaymentDate"] = item.PaymentDate;
                    DataRowOrdered["Amount"] = item.PaymentAmount.ToString("C");
                    DataRowOrdered["InvoiceNo"] = item.InvoiceNo;
                    DataRowOrdered["InvoiceDate"] = item.InvoiceDate;
                    DataRowOrdered["InvoiceSubtotal"] = item.InvoiceSubTotal.ToString("C");
                    DataRowOrdered["InvoiceTax"] = item.InvoiceTax.ToString("C");
                    DataRowOrdered["InvoiceTotal"] = item.InvoiceTotal.ToString("C");
                    DataRowOrdered["CUser"] = item.CUser;
                    DataRowOrdered["PaymentNo"] = item.PaymentNo;

                    DataSetTableOrdered.Tables[0].Rows.Add(DataRowOrdered);
                }

                GreatTotal.Text = @Model.PaymentsReport.Sum(c => c.PaymentAmount).ToString("C");

                ClientCode.DataBindings.Add(new XRBinding("Text", null, "xrTableRowFormat.ClientCode"));
                ClientName.DataBindings.Add(new XRBinding("Text", null, "xrTableRowFormat.ClientName"));
                PaymentDate.DataBindings.Add(new XRBinding("Text", null, "xrTableRowFormat.PaymentDate"));
                Amount.DataBindings.Add(new XRBinding("Text", null, "xrTableRowFormat.Amount"));
                InvoiceNo.DataBindings.Add(new XRBinding("Text", null, "xrTableRowFormat.InvoiceNo"));
                InvoiceDate.DataBindings.Add(new XRBinding("Text", null, "xrTableRowFormat.InvoiceDate"));
                InvoiceSubtotal.DataBindings.Add(new XRBinding("Text", null, "xrTableRowFormat.InvoiceSubtotal"));
                InvoiceTax.DataBindings.Add(new XRBinding("Text", null, "xrTableRowFormat.InvoiceTax"));
                InvoiceTotal.DataBindings.Add(new XRBinding("Text", null, "xrTableRowFormat.InvoiceTotal"));
                CUser.DataBindings.Add(new XRBinding("Text", null, "xrTableRowFormat.CUser"));
                PaymentNo.DataBindings.Add(new XRBinding("Text", null, "xrTableRowFormat.PaymentNo"));                

                return DataSetTableOrdered;
            }
            catch (Exception ex)
            {
                string d=ex.Message;
                throw;
            }
          
        }       
    }
}
