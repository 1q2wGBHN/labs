﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
using System.Data;
using App.Entities.ViewModels.Transfer;
using System.Collections.Generic;
using App.Entities;

namespace FloridoERPTX.Reports
{
    public partial class TransferReport : DevExpress.XtraReports.UI.XtraReport
    {
        public DataSet DataSetTableOrdered = null;
        public TransferReport()
        {
            InitializeComponent();
        }

        private void Detail_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {

        }

        public DataSet printTable(List<TransferDetailModel> TransferDetail, TransferHeaderModel TransferHeader)
        {
            try
            {
                DataSetTableOrdered = new DataSet();
                DataTable DataTableOrdered = new DataTable();
                DataSetTableOrdered.Tables.Add(DataTableOrdered);

                //xraddress.Text = site_info.address + " C.P." + site_info.postal_code;
                //xrState.Text = site_info.city + ", " + site_info.state;
                //siteLabel.Text = "*" + site_info.site_name;
                xraddress.Text = "";
                xrState.Text = "";
                siteLabel.Text = "";

                xrLabelNumberTransfer.Text = TransferHeader.TransferDocument;
                xrLabelStatus.Text = NameStatus(TransferHeader.TransferStatus);
                empFinalLabel.Text = TransferHeader.AutorizeName;
                xrLabelDateInit.Text = TransferHeader.TransferDate;
                xrLabelOriginSite.Text = TransferHeader.TransferSiteName;
                xrLabelFinalSite.Text = TransferHeader.DestinationSiteName;
                empLabel.Text = TransferHeader.AccomplishedName;

                //Footer
                xrLabelComment.Text = TransferHeader.Comment;
                xrLabelAutorize.Text = TransferHeader.AutorizeName;
                xrLabelDriver.Text = TransferHeader.DriverName;
                xrLabelFinalSiteFooter.Text = TransferHeader.DestinationSiteName;

                DataTableOrdered.Columns.Add(new DataColumn("xrTableCellArticle"));
                DataTableOrdered.Columns.Add(new DataColumn("xrTableCellDescription"));
                DataTableOrdered.Columns.Add(new DataColumn("xrTableCellUnitCost"));
                DataTableOrdered.Columns.Add(new DataColumn("xrTableCellQuantity"));
                DataTableOrdered.Columns.Add(new DataColumn("xrTableCellIEPS"));
                DataTableOrdered.Columns.Add(new DataColumn("xrTableCellSubTotal"));
                DataTableOrdered.Columns.Add(new DataColumn("xrTableCellIVA"));
                DataTableOrdered.Columns.Add(new DataColumn("xrTableCellTotal"));

                decimal subtotal = 0;
                decimal ivatotal = 0;
                decimal iepstotal = 0;
                decimal total = 0;
                foreach (var item in TransferDetail)
                {
                    
                    DataRow DataRowOrdered = DataSetTableOrdered.Tables[0].NewRow();
                    DataRowOrdered["xrTableCellArticle"] = item.PartNumber;
                    DataRowOrdered["xrTableCellDescription"] = item.Description;
                    DataRowOrdered["xrTableCellUnitCost"] = ReturnFormatMoney(item.UnitCost);//item.UnitCost.ToString("C");
                    DataRowOrdered["xrTableCellQuantity"] = Format2(item.Quantity);
                    DataRowOrdered["xrTableCellIEPS"] = ReturnFormatMoney(item.IEPS);//item.IEPS.ToString("C4");
                    DataRowOrdered["xrTableCellSubTotal"] = ReturnFormatMoney(item.SubTotal);
                    DataRowOrdered["xrTableCellIVA"] = ReturnFormatMoney(item.Iva);
                    DataRowOrdered["xrTableCellTotal"] = ReturnFormatMoney(item.Amount);
                    subtotal += item.SubTotal;
                    ivatotal += item.Iva * item.Quantity;
                    iepstotal += item.IEPS * item.Quantity;
                    total += item.Amount;
                    DataSetTableOrdered.Tables[0].Rows.Add(DataRowOrdered);
                }
                xrTableCellArticle.DataBindings.Add(new XRBinding("Text", null, "xrTableRow1.xrTableCellArticle"));
                xrTableCellDescription.DataBindings.Add(new XRBinding("Text", null, "xrTableRow1.xrTableCellDescription"));
                xrTableCellUnitCost.DataBindings.Add(new XRBinding("Text", null, "xrTableRow1.xrTableCellUnitCost"));
                xrTableCellQuantity.DataBindings.Add(new XRBinding("Text", null, "xrTableRow1.xrTableCellQuantity"));
                xrTableCellIEPS.DataBindings.Add(new XRBinding("Text", null, "xrTableRow1.xrTableCellIEPS"));
                xrTableCellSubTotal.DataBindings.Add(new XRBinding("Text", null, "xrTableRow1.xrTableCellSubTotal"));
                xrTableCellIVA.DataBindings.Add(new XRBinding("Text", null, "xrTableRow1.xrTableCellIVA"));
                xrTableCellTotal.DataBindings.Add(new XRBinding("Text", null, "xrTableRow1.xrTableCellTotal"));
                xrLabelAmount.Text = ReturnFormatMoney(total);
                xrLabelIVA.Text = ReturnFormatMoney(ivatotal); 
                xrLabelIEPS.Text = ReturnFormatMoney(iepstotal); 
                xrLabelSubTotal.Text = ReturnFormatMoney(subtotal); 
                return DataSetTableOrdered;
            }
            catch (Exception ex)
            {
                string d = ex.Message;
                throw;
            }

        }
        public string ReturnFormatMoney(decimal value)
        {
            var c = value.ToString().Split('.');
            int decimales = Int32.Parse(c[1]);
            var total = "";
            if (decimales > 0)
            {
                total = value.ToString("C2");
            }
            else
            {
                total = Int32.Parse(c[0]).ToString("C0");
            }
            return total;
        }
        public string Format2(decimal value)
        {
            var c = value.ToString().Split('.');
            int decimales = Int32.Parse(c[1]);
            var total = "";
            if (decimales > 0)
            {
                total = value.ToString("F2");
            }
            else
            {
                total = Int32.Parse(c[0]).ToString("F2");
            }
            return total;
        }
        public string NameStatus(int status)
        {
            if (status == 4)
                return "En transito";
            else if (status == 1)
                return "Aprobado";
            else if (status == 0)
                return "Pendientes";
            else if (status == 2)
                return "Rechazado";
            else if (status == 8)
                return "Cancelado";
            else if (status == 9 || status == 4 || status == 5)
                return "Enviada/Terminada";
            return "DESCONOCIDO";
        }
    }
}
