﻿using System;
using System.Data;
using DevExpress.XtraReports.UI;
using System.Linq;
using App.Entities.ViewModels.BarcodeCaptureTime;

namespace FloridoERPTX.Reports
{
    public partial class BarcodeCaptureTimeReport : DevExpress.XtraReports.UI.XtraReport
    {
       
        public DataSet DataSetTableOrdered = null;        
        public BarcodeCaptureTimeReport()
        {
            InitializeComponent();
          
        }
        public DataSet printTable(BarcodeCaptureTimeIndex Model)
        {
            try
            {
                DataSetTableOrdered = new DataSet();
                DataTable DataTableOrdered = new DataTable();
                siteLabel.Text = Model.SiteName;

                Date.Text = string.Format("{0} - {1}", Model.StartDate, Model.EndDate);
                TotalScanLabel.Text = string.Format("Total Códigos Escaneados contra No Escaneados por Cajero del {0} al {1}", Model.StartDate, Model.EndDate);
                Top10Label.Text = string.Format("Top 10 Códigos capturados No Escaneados del {0} al {1}", Model.StartDate, Model.EndDate);
                MainLabel.Text = string.Format("Relación de Códigos NO Escaneados por Cajero del {0} al {1}", Model.StartDate, Model.EndDate);

                #region Scanned Table
                foreach (var Item in Model.DataInfo.ScannedTotal)
                {                    
                    var row = new XRTableRow();

                    var cell = new XRTableCell();
                    cell.WidthF = 176.20F;
                    cell.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
                    cell.Text = Item.Cashier;

                    var cell2 = new XRTableCell();
                    cell2.WidthF = 97.20F;
                    cell2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
                    cell2.Text = Item.Scanned.ToString();

                    var cell3 = new XRTableCell();
                    cell3.WidthF = 115.20F;
                    cell3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
                    cell3.Text = Item.NotScanned.ToString();

                    var cell4 = new XRTableCell();
                    cell4.WidthF = 64.20F;
                    cell4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
                    cell4.Text = Item.Percentage.ToString("0.00");

                    row.Cells.Add(cell);
                    row.Cells.Add(cell2);
                    row.Cells.Add(cell3);
                    row.Cells.Add(cell4);
                    TotalScanTable.Rows.Add(row);
                }

                decimal TotalPercentage = (Model.DataInfo.ScannedTotal.Sum(c => c.NotScanned) * 100) / Model.DataInfo.ScannedTotal.Sum(c => c.Scanned);

                var extraRow = new XRTableRow();

                var extraCell = new XRTableCell();
                extraCell.WidthF = 176.20F;
                extraCell.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
                extraCell.Font = new System.Drawing.Font("Arial", 12f, System.Drawing.FontStyle.Bold);
                extraCell.Text = "Total";

                var extraCell2 = new XRTableCell();
                extraCell2.WidthF = 97.20F;
                extraCell2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
                extraCell2.Font = new System.Drawing.Font("Arial", 12f, System.Drawing.FontStyle.Bold);
                extraCell2.Text = Model.DataInfo.ScannedTotal.Sum(c => c.Scanned).ToString();

                var extraCell3 = new XRTableCell();
                extraCell3.WidthF = 115.20F;
                extraCell3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
                extraCell3.Font = new System.Drawing.Font("Arial", 12f, System.Drawing.FontStyle.Bold);
                extraCell3.Text = Model.DataInfo.ScannedTotal.Sum(c => c.NotScanned).ToString();

                var extraCell4 = new XRTableCell();
                extraCell4.WidthF = 64.20F;
                extraCell4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
                extraCell4.Font = new System.Drawing.Font("Arial", 12f, System.Drawing.FontStyle.Bold);
                extraCell4.Text = TotalPercentage.ToString("0.00");

                extraRow.Cells.Add(extraCell);
                extraRow.Cells.Add(extraCell2);
                extraRow.Cells.Add(extraCell3);
                extraRow.Cells.Add(extraCell4);
                TotalScanTable.Rows.Add(extraRow);
                #endregion

                #region Issues Table
                foreach (var Item in Model.DataInfo.IssuesCount)
                {                    
                    var row = new XRTableRow();

                    var cell = new XRTableCell();
                    cell.WidthF = 99.20F;
                    cell.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
                    cell.Text = Item.ItemNumber;

                    var cell2 = new XRTableCell();
                    cell2.WidthF = 133.20F;
                    cell2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
                    cell2.Text = Item.ItemDescription;

                    var cell3 = new XRTableCell();
                    cell3.WidthF = 125.20F;
                    cell3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
                    cell3.Text = Item.Department;

                    var cell4 = new XRTableCell();
                    cell4.WidthF = 91.20F;
                    cell4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
                    cell4.Text = Item.Issues.ToString();                    

                    row.Cells.Add(cell);
                    row.Cells.Add(cell2);
                    row.Cells.Add(cell3);
                    row.Cells.Add(cell4);
                    IssuesTable.Rows.Add(row);
                }
                #endregion

                #region Report Table         
                foreach(var Item in Model.DataInfo.MainData)
                {
                    var row = new XRTableRow();

                    var cell = new XRTableCell();
                    cell.WidthF = 188.20F;
                    cell.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
                    cell.Text = Item.Cashier;

                    var cell2 = new XRTableCell();
                    cell2.WidthF = 79.20F;
                    cell2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
                    cell2.Text = Item.Ticket.ToString();

                    var cell3 = new XRTableCell();
                    cell3.WidthF = 72.20F;
                    cell3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
                    cell3.Text = Item.Date;

                    var cell4 = new XRTableCell();
                    cell4.WidthF = 72.20F;
                    cell4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
                    cell4.Text = Item.Time;

                    var cell5 = new XRTableCell();
                    cell5.WidthF = 74.20F;
                    cell5.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
                    cell5.Text = Item.CheckoutNo;

                    var cell6 = new XRTableCell();
                    cell6.WidthF = 116.20F;
                    cell6.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
                    cell6.Text = Item.CaptureCode;

                    var cell7 = new XRTableCell();
                    cell7.WidthF = 181.20F;
                    cell7.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
                    cell7.Text = Item.ItemDescription;

                    var cell8 = new XRTableCell();
                    cell8.WidthF = 181.20F;
                    cell8.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
                    cell8.Text = Item.Department;

                    row.Cells.Add(cell);
                    row.Cells.Add(cell2);
                    row.Cells.Add(cell3);
                    row.Cells.Add(cell4);
                    row.Cells.Add(cell5);
                    row.Cells.Add(cell6);
                    row.Cells.Add(cell7);
                    row.Cells.Add(cell8);
                    MainTable.Rows.Add(row);
                }                
                #endregion

                return DataSetTableOrdered;
            }
            catch (Exception ex)
            {
                string d = ex.Message;
                throw;
            }

        }
    }
}
