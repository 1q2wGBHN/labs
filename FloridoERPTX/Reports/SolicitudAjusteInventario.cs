﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
using System.Data;
using System.Collections.Generic;
using App.Entities.ViewModels.DamagedsGoods;
using App.Entities.ViewModels.Item;
using System.Globalization;
using App.Entities;

namespace FloridoERPTX.Reports
{
    public partial class SolicitudAjusteInventario : DevExpress.XtraReports.UI.XtraReport
    {
        public DataSet DataSetTableOrdered = null;
        public SolicitudAjusteInventario()
        {
            InitializeComponent();
        }
        public DataSet printTable(List<ItemModel> documentItems, ItemKardexModel model, SITES site_info)
        {
            try
            {
                DataSetTableOrdered = new DataSet();
                DataTable DataTableOrdered = new DataTable();
                DataSetTableOrdered.Tables.Add(DataTableOrdered);

                //Info Site
                xraddress.Text = site_info.address + " C.P." + site_info.postal_code;
                xrState.Text = site_info.city + ", " + site_info.state;
                siteLabel.Text = "*" + site_info.site_name;

                xrLabelDate.Text = model.MovementDate;
                xrLabelDocumentNumber.Text = model.MovementDocument;
                empLabel.Text = model.CreateBy;
                statusLabel.Text = model.DocumentStatus;
                if (model.DocumentStatus == "Pendiente")
                    xrdateStatus.Text = "Fecha Creación: ";
                else
                {
                    xrdateStatus.Text = "Fecha Procesada: ";
                    empLabelFinal.Text = model.AuthorizationBy;
                    xrLabelDate.Text = model.TimeDocumentDate.Value.ToShortDateString();
                }
                if (model.DocumentStatus == "Rechazado")
                    empLabelFinalMain.Text = "Rechazado ";

                DataTableOrdered.Columns.Add(new DataColumn("xrTableCellPartNumber"));
                DataTableOrdered.Columns.Add(new DataColumn("xrTableCellPartDescription"));
                DataTableOrdered.Columns.Add(new DataColumn("xrTableCellScrapReason"));
                DataTableOrdered.Columns.Add(new DataColumn("xrTableCellPrice"));
                DataTableOrdered.Columns.Add(new DataColumn("xrTableCellQuantity"));
                DataTableOrdered.Columns.Add(new DataColumn("xrTableCellMovement"));
                DataTableOrdered.Columns.Add(new DataColumn("xrTableCellAmount"));
                DataTableOrdered.Columns.Add(new DataColumn("xrTableCellIEPS"));
                DataTableOrdered.Columns.Add(new DataColumn("xrTableCellIVA"));
                DataTableOrdered.Columns.Add(new DataColumn("xrTableCellTotal"));


                var cultureInfo = CultureInfo.GetCultureInfo("en-US");
                decimal subtotal = 0;
                decimal ivatotal = 0;
                decimal iepstotal = 0;
                decimal Total = 0;

                foreach (var item in documentItems)
                {
                    DataRow DataRowOrdered = DataSetTableOrdered.Tables[0].NewRow();
                    DataRowOrdered["xrTableCellPartNumber"] = item.PathNumber;
                    DataRowOrdered["xrTableCellPartDescription"] = item.Description;
                    DataRowOrdered["xrTableCellScrapReason"] = item.Comment;
                    DataRowOrdered["xrTableCellPrice"] = "$" + item.BaseCost;
                    DataRowOrdered["xrTableCellQuantity"] = item.Stock;
                    DataRowOrdered["xrTableCellMovement"] = item.MovementType ? "Crédito" : "Débito";
                    DataRowOrdered["xrTableCellAmount"] = "$" + Convert.ToDecimal(item.BaseCost * item.Stock).ToString("F");
                    DataRowOrdered["xrTableCellIEPS"] = "$" + Convert.ToDecimal((item.IEPS / 100) * (item.BaseCost * item.Stock)).ToString("F");
                    DataRowOrdered["xrTableCellIVA"] = "$" + Convert.ToDecimal((((item.IEPS / 100) * (item.BaseCost * item.Stock)) + item.BaseCost) * (item.PartIva / 100)).ToString("F");
                    DataRowOrdered["xrTableCellTotal"] = "$" + Convert.ToDecimal((item.BaseCost * item.Stock) + ((item.IEPS / 100) * (item.BaseCost * item.Stock)) + ((((item.IEPS / 100) * (item.BaseCost * item.Stock)) + item.BaseCost) * (item.PartIva / 100))).ToString("F");


                    subtotal += Convert.ToDecimal(item.BaseCost * item.Stock);
                    Total += Convert.ToDecimal((item.BaseCost * item.Stock) + ((item.IEPS / 100) * (item.BaseCost * item.Stock)) + ((((item.IEPS / 100) * (item.BaseCost * item.Stock)) + item.BaseCost) * (item.PartIva / 100)));
                    ivatotal += Convert.ToDecimal((((item.IEPS / 100) * (item.BaseCost * item.Stock)) + item.BaseCost) * (item.PartIva / 100));
                    iepstotal += Convert.ToDecimal((item.IEPS / 100) * (item.BaseCost * item.Stock));

                    DataSetTableOrdered.Tables[0].Rows.Add(DataRowOrdered);
                }

                xrLabelAmount.Text = String.Format(cultureInfo, "{0:C4}", Total);
                xrLabelIEPS.Text = String.Format(cultureInfo, "{0:C4}", iepstotal);
                xrLabelIVA.Text = String.Format(cultureInfo, "{0:C4}", ivatotal);
                xrLabelSub.Text = String.Format(cultureInfo, "{0:C4}", subtotal);

                xrTableCellPartNumber.DataBindings.Add(new XRBinding("Text", null, "xrTableRow1.xrTableCellPartNumber"));
                xrTableCellPartDescription.DataBindings.Add(new XRBinding("Text", null, "xrTableRow1.xrTableCellPartDescription"));
                xrTableCellScrapReason.DataBindings.Add(new XRBinding("Text", null, "xrTableRow1.xrTableCellScrapReason"));
                xrTableCellPrice.DataBindings.Add(new XRBinding("Text", null, "xrTableRow1.xrTableCellPrice"));
                xrTableCellQuantity.DataBindings.Add(new XRBinding("Text", null, "xrTableRow1.xrTableCellQuantity"));
                xrTableCellMovement.DataBindings.Add(new XRBinding("Text", null, "xrTableRow1.xrTableCellMovement"));
                xrTableCellAmount.DataBindings.Add(new XRBinding("Text", null, "xrTableRow1.xrTableCellAmount"));
                xrTableCellIEPS.DataBindings.Add(new XRBinding("Text", null, "xrTableRow1.xrTableCellIEPS"));
                xrTableCellIVA.DataBindings.Add(new XRBinding("Text", null, "xrTableRow1.xrTableCellIVA"));
                xrTableCellTotal.DataBindings.Add(new XRBinding("Text", null, "xrTableRow1.xrTableCellTotal"));


                return DataSetTableOrdered;
            }
            catch (Exception ex)
            {
                string d = ex.Message;
                throw;
            }

        }

    }
}
