﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
using App.Entities.ViewModels.Item;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Text;

namespace FloridoERPTX.Reports
{
    public partial class InventoryJustificationDoc : DevExpress.XtraReports.UI.XtraReport
    {
        public DataSet DataSetTableMovements = null;
        public InventoryJustificationDoc()
        {
            InitializeComponent();
        }
        public XtraReport loadFile(string file, string fileExt)
        {
            try
            {
                using (MemoryStream ms = new MemoryStream())
                {
                    //declare byte array to get file content from database and string to store file name
                    byte[] fileData;
                    string fileName;
                    
                    //only one record will be returned from database as expression uses condtion on primary field
                    //so get first record from returned values and retrive file content (binary) and filename

                    //fileData = (byte[])record.First().FileData.ToArray();
                    //fileName = record.First().FileName;
                    fileData = Encoding.ASCII.GetBytes(file);
                    fileName = fileExt;
                    
                    //return file and provide byte file content and file name
                    //return File(fileData, "text", fileName);

                    
                    ms.Write(fileData, 0, (int)file.Length);

                    XtraReport report = XtraReport.FromStream(ms, true);//(Server.MapPath("~/Reports/" + Name), true);

                    return report;

                }
            }
            catch (Exception ex)
            {
                string d = ex.Message;
                throw;
            }
        }
    }
}
