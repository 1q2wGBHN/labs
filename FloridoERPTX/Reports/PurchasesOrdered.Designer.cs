﻿namespace FloridoERPTX.Reports
{
    partial class PurchasesOrdered
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(PurchasesOrdered));
            this.Detail = new DevExpress.XtraReports.UI.DetailBand();
            this.xrLine4 = new DevExpress.XtraReports.UI.XRLine();
            this.xrLine3 = new DevExpress.XtraReports.UI.XRLine();
            this.Detalle = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel17 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel18 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel19 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel34 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel20 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel21 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel22 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel24 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel25 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel26 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel28 = new DevExpress.XtraReports.UI.XRLabel();
            this.TopMargin = new DevExpress.XtraReports.UI.TopMarginBand();
            this.BottomMargin = new DevExpress.XtraReports.UI.BottomMarginBand();
            this.ReportHeader = new DevExpress.XtraReports.UI.ReportHeaderBand();
            this.xrlblLastScan = new DevExpress.XtraReports.UI.XRLabel();
            this.xrlblLastScanInput = new DevExpress.XtraReports.UI.XRLabel();
            this.xrlblStartScanInput = new DevExpress.XtraReports.UI.XRLabel();
            this.xrlblStartScan = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabelCurrency = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel23 = new DevExpress.XtraReports.UI.XRLabel();
            this.dateEndLabel = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel7 = new DevExpress.XtraReports.UI.XRLabel();
            this.empFinalLabel = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel1 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrState = new DevExpress.XtraReports.UI.XRLabel();
            this.xraddress = new DevExpress.XtraReports.UI.XRLabel();
            this.supNoRealLabel = new DevExpress.XtraReports.UI.XRLabel();
            this.supDeparLabel = new DevExpress.XtraReports.UI.XRLabel();
            this.supEmailLabel = new DevExpress.XtraReports.UI.XRLabel();
            this.supTelLabel = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel16 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel15 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel14 = new DevExpress.XtraReports.UI.XRLabel();
            this.supAddrsLabel = new DevExpress.XtraReports.UI.XRLabel();
            this.commentsLabel = new DevExpress.XtraReports.UI.XRLabel();
            this.factSupLabel = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel13 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel12 = new DevExpress.XtraReports.UI.XRLabel();
            this.supNameLabel = new DevExpress.XtraReports.UI.XRLabel();
            this.dateLabel = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel11 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel9 = new DevExpress.XtraReports.UI.XRLabel();
            this.supNoLabel = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel10 = new DevExpress.XtraReports.UI.XRLabel();
            this.poNoLabel = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel8 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLine1 = new DevExpress.XtraReports.UI.XRLine();
            this.xrLabel2 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel5 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrRFC = new DevExpress.XtraReports.UI.XRLabel();
            this.siteLabel = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel3 = new DevExpress.XtraReports.UI.XRLabel();
            this.empLabel = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLine2 = new DevExpress.XtraReports.UI.XRLine();
            this.GroupFooter1 = new DevExpress.XtraReports.UI.GroupFooterBand();
            this.xrLabel4 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel6 = new DevExpress.XtraReports.UI.XRLabel();
            this.iepsLabel = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel30 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel31 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel32 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel33 = new DevExpress.XtraReports.UI.XRLabel();
            this.subtotalLabel = new DevExpress.XtraReports.UI.XRLabel();
            this.discountLabel = new DevExpress.XtraReports.UI.XRLabel();
            this.totalLabel = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel29 = new DevExpress.XtraReports.UI.XRLabel();
            this.ivaLabel = new DevExpress.XtraReports.UI.XRLabel();
            this.PageFooter = new DevExpress.XtraReports.UI.PageFooterBand();
            this.xrPageInfo1 = new DevExpress.XtraReports.UI.XRPageInfo();
            this.DetailReport = new DevExpress.XtraReports.UI.DetailReportBand();
            this.Detail1 = new DevExpress.XtraReports.UI.DetailBand();
            this.xrTable1 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow1 = new DevExpress.XtraReports.UI.XRTableRow();
            this.prod_id = new DevExpress.XtraReports.UI.XRTableCell();
            this.prod_name = new DevExpress.XtraReports.UI.XRTableCell();
            this.quantity = new DevExpress.XtraReports.UI.XRTableCell();
            this.gr_quantity = new DevExpress.XtraReports.UI.XRTableCell();
            this.um = new DevExpress.XtraReports.UI.XRTableCell();
            this.tot_units = new DevExpress.XtraReports.UI.XRTableCell();
            this.unit_price = new DevExpress.XtraReports.UI.XRTableCell();
            this.import = new DevExpress.XtraReports.UI.XRTableCell();
            this.ieps = new DevExpress.XtraReports.UI.XRTableCell();
            this.iva = new DevExpress.XtraReports.UI.XRTableCell();
            this.subtotal = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrPictureBox1 = new DevExpress.XtraReports.UI.XRPictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // Detail
            // 
            this.Detail.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLine4,
            this.xrLine3,
            this.Detalle,
            this.xrLabel17,
            this.xrLabel18,
            this.xrLabel19,
            this.xrLabel34,
            this.xrLabel20,
            this.xrLabel21,
            this.xrLabel22,
            this.xrLabel24,
            this.xrLabel25,
            this.xrLabel26,
            this.xrLabel28});
            this.Detail.Dpi = 100F;
            this.Detail.HeightF = 60.13596F;
            this.Detail.Name = "Detail";
            this.Detail.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.Detail.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLine4
            // 
            this.xrLine4.Dpi = 100F;
            this.xrLine4.LocationFloat = new DevExpress.Utils.PointFloat(0F, 50.13596F);
            this.xrLine4.Name = "xrLine4";
            this.xrLine4.SizeF = new System.Drawing.SizeF(782.0001F, 10F);
            // 
            // xrLine3
            // 
            this.xrLine3.Dpi = 100F;
            this.xrLine3.LocationFloat = new DevExpress.Utils.PointFloat(0F, 9.040855F);
            this.xrLine3.Name = "xrLine3";
            this.xrLine3.SizeF = new System.Drawing.SizeF(782.0001F, 10F);
            // 
            // Detalle
            // 
            this.Detalle.Dpi = 100F;
            this.Detalle.Font = new System.Drawing.Font("Arial", 7F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Detalle.LocationFloat = new DevExpress.Utils.PointFloat(327.9117F, 0F);
            this.Detalle.Name = "Detalle";
            this.Detalle.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.Detalle.SizeF = new System.Drawing.SizeF(62.5F, 9.040857F);
            this.Detalle.StylePriority.UseFont = false;
            this.Detalle.StylePriority.UseTextAlignment = false;
            this.Detalle.Text = "Detalle";
            this.Detalle.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrLabel17
            // 
            this.xrLabel17.Dpi = 100F;
            this.xrLabel17.Font = new System.Drawing.Font("Arial", 7F, System.Drawing.FontStyle.Bold);
            this.xrLabel17.LocationFloat = new DevExpress.Utils.PointFloat(0F, 35.08169F);
            this.xrLabel17.Name = "xrLabel17";
            this.xrLabel17.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel17.SizeF = new System.Drawing.SizeF(63.54167F, 15F);
            this.xrLabel17.StylePriority.UseFont = false;
            this.xrLabel17.StylePriority.UseTextAlignment = false;
            this.xrLabel17.Text = "No. Artículo";
            this.xrLabel17.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrLabel18
            // 
            this.xrLabel18.Dpi = 100F;
            this.xrLabel18.Font = new System.Drawing.Font("Arial", 7F, System.Drawing.FontStyle.Bold);
            this.xrLabel18.LocationFloat = new DevExpress.Utils.PointFloat(121.8254F, 35.08169F);
            this.xrLabel18.Name = "xrLabel18";
            this.xrLabel18.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel18.SizeF = new System.Drawing.SizeF(63.54167F, 15F);
            this.xrLabel18.StylePriority.UseFont = false;
            this.xrLabel18.StylePriority.UseTextAlignment = false;
            this.xrLabel18.Text = "Descripción";
            this.xrLabel18.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrLabel19
            // 
            this.xrLabel19.Dpi = 100F;
            this.xrLabel19.Font = new System.Drawing.Font("Arial", 7F, System.Drawing.FontStyle.Bold);
            this.xrLabel19.LocationFloat = new DevExpress.Utils.PointFloat(224.5096F, 20.08168F);
            this.xrLabel19.Multiline = true;
            this.xrLabel19.Name = "xrLabel19";
            this.xrLabel19.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel19.SizeF = new System.Drawing.SizeF(42F, 28.5417F);
            this.xrLabel19.StylePriority.UseFont = false;
            this.xrLabel19.StylePriority.UseTextAlignment = false;
            this.xrLabel19.Text = "Pedido\r\n(UM)";
            this.xrLabel19.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrLabel34
            // 
            this.xrLabel34.Dpi = 100F;
            this.xrLabel34.Font = new System.Drawing.Font("Arial", 7F, System.Drawing.FontStyle.Bold);
            this.xrLabel34.LocationFloat = new DevExpress.Utils.PointFloat(275.0679F, 19.04087F);
            this.xrLabel34.Name = "xrLabel34";
            this.xrLabel34.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel34.SizeF = new System.Drawing.SizeF(40.99998F, 28.54166F);
            this.xrLabel34.StylePriority.UseFont = false;
            this.xrLabel34.StylePriority.UseTextAlignment = false;
            this.xrLabel34.Text = "Entrada (UM)";
            this.xrLabel34.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrLabel20
            // 
            this.xrLabel20.Dpi = 100F;
            this.xrLabel20.Font = new System.Drawing.Font("Arial", 7F, System.Drawing.FontStyle.Bold);
            this.xrLabel20.LocationFloat = new DevExpress.Utils.PointFloat(339.3701F, 21.54004F);
            this.xrLabel20.Name = "xrLabel20";
            this.xrLabel20.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel20.SizeF = new System.Drawing.SizeF(41F, 28.54167F);
            this.xrLabel20.StylePriority.UseFont = false;
            this.xrLabel20.StylePriority.UseTextAlignment = false;
            this.xrLabel20.Text = "UM (Factor)";
            this.xrLabel20.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrLabel21
            // 
            this.xrLabel21.Dpi = 100F;
            this.xrLabel21.Font = new System.Drawing.Font("Arial", 7F, System.Drawing.FontStyle.Bold);
            this.xrLabel21.LocationFloat = new DevExpress.Utils.PointFloat(413.6143F, 36.54003F);
            this.xrLabel21.Name = "xrLabel21";
            this.xrLabel21.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel21.SizeF = new System.Drawing.SizeF(49.99997F, 13.54167F);
            this.xrLabel21.StylePriority.UseFont = false;
            this.xrLabel21.StylePriority.UseTextAlignment = false;
            this.xrLabel21.Text = "Unidades";
            this.xrLabel21.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrLabel22
            // 
            this.xrLabel22.Dpi = 100F;
            this.xrLabel22.Font = new System.Drawing.Font("Arial", 7F, System.Drawing.FontStyle.Bold);
            this.xrLabel22.LocationFloat = new DevExpress.Utils.PointFloat(503.3799F, 20.08171F);
            this.xrLabel22.Name = "xrLabel22";
            this.xrLabel22.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel22.SizeF = new System.Drawing.SizeF(48.00012F, 28.54166F);
            this.xrLabel22.StylePriority.UseFont = false;
            this.xrLabel22.StylePriority.UseTextAlignment = false;
            this.xrLabel22.Text = "Costo (Unitario)";
            this.xrLabel22.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrLabel24
            // 
            this.xrLabel24.Dpi = 100F;
            this.xrLabel24.Font = new System.Drawing.Font("Arial", 7F, System.Drawing.FontStyle.Bold);
            this.xrLabel24.LocationFloat = new DevExpress.Utils.PointFloat(574.7192F, 35.08169F);
            this.xrLabel24.Name = "xrLabel24";
            this.xrLabel24.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel24.SizeF = new System.Drawing.SizeF(43F, 15F);
            this.xrLabel24.StylePriority.UseFont = false;
            this.xrLabel24.StylePriority.UseTextAlignment = false;
            this.xrLabel24.Text = "Subtotal";
            this.xrLabel24.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrLabel25
            // 
            this.xrLabel25.Dpi = 100F;
            this.xrLabel25.Font = new System.Drawing.Font("Arial", 7F, System.Drawing.FontStyle.Bold);
            this.xrLabel25.LocationFloat = new DevExpress.Utils.PointFloat(638.9998F, 35.08169F);
            this.xrLabel25.Name = "xrLabel25";
            this.xrLabel25.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel25.SizeF = new System.Drawing.SizeF(38F, 15F);
            this.xrLabel25.StylePriority.UseFont = false;
            this.xrLabel25.StylePriority.UseTextAlignment = false;
            this.xrLabel25.Text = "IEPS %";
            this.xrLabel25.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrLabel26
            // 
            this.xrLabel26.Dpi = 100F;
            this.xrLabel26.Font = new System.Drawing.Font("Arial", 7F, System.Drawing.FontStyle.Bold);
            this.xrLabel26.LocationFloat = new DevExpress.Utils.PointFloat(684.7443F, 35.08169F);
            this.xrLabel26.Name = "xrLabel26";
            this.xrLabel26.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel26.SizeF = new System.Drawing.SizeF(32F, 15F);
            this.xrLabel26.StylePriority.UseFont = false;
            this.xrLabel26.StylePriority.UseTextAlignment = false;
            this.xrLabel26.Text = "IVA %";
            this.xrLabel26.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrLabel28
            // 
            this.xrLabel28.Dpi = 100F;
            this.xrLabel28.Font = new System.Drawing.Font("Arial", 7F, System.Drawing.FontStyle.Bold);
            this.xrLabel28.LocationFloat = new DevExpress.Utils.PointFloat(732.662F, 35.08171F);
            this.xrLabel28.Name = "xrLabel28";
            this.xrLabel28.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel28.SizeF = new System.Drawing.SizeF(49.33801F, 15F);
            this.xrLabel28.StylePriority.UseFont = false;
            this.xrLabel28.StylePriority.UseTextAlignment = false;
            this.xrLabel28.Text = "Total";
            this.xrLabel28.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // TopMargin
            // 
            this.TopMargin.Dpi = 100F;
            this.TopMargin.HeightF = 30F;
            this.TopMargin.Name = "TopMargin";
            this.TopMargin.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.TopMargin.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // BottomMargin
            // 
            this.BottomMargin.Dpi = 100F;
            this.BottomMargin.HeightF = 55F;
            this.BottomMargin.Name = "BottomMargin";
            this.BottomMargin.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.BottomMargin.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // ReportHeader
            // 
            this.ReportHeader.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrPictureBox1,
            this.xrlblLastScan,
            this.xrlblLastScanInput,
            this.xrlblStartScanInput,
            this.xrlblStartScan,
            this.xrLabelCurrency,
            this.xrLabel23,
            this.dateEndLabel,
            this.xrLabel7,
            this.empFinalLabel,
            this.xrLabel1,
            this.xrState,
            this.xraddress,
            this.supNoRealLabel,
            this.supDeparLabel,
            this.supEmailLabel,
            this.supTelLabel,
            this.xrLabel16,
            this.xrLabel15,
            this.xrLabel14,
            this.supAddrsLabel,
            this.commentsLabel,
            this.factSupLabel,
            this.xrLabel13,
            this.xrLabel12,
            this.supNameLabel,
            this.dateLabel,
            this.xrLabel11,
            this.xrLabel9,
            this.supNoLabel,
            this.xrLabel10,
            this.poNoLabel,
            this.xrLabel8,
            this.xrLine1,
            this.xrLabel2,
            this.xrLabel5,
            this.xrRFC,
            this.siteLabel,
            this.xrLabel3,
            this.empLabel,
            this.xrLine2});
            this.ReportHeader.Dpi = 100F;
            this.ReportHeader.HeightF = 273.24F;
            this.ReportHeader.Name = "ReportHeader";
            // 
            // xrlblLastScan
            // 
            this.xrlblLastScan.Dpi = 100F;
            this.xrlblLastScan.Font = new System.Drawing.Font("Arial", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrlblLastScan.LocationFloat = new DevExpress.Utils.PointFloat(641.7578F, 240.3699F);
            this.xrlblLastScan.Name = "xrlblLastScan";
            this.xrlblLastScan.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrlblLastScan.SizeF = new System.Drawing.SizeF(130.2422F, 15.00002F);
            this.xrlblLastScan.StylePriority.UseFont = false;
            this.xrlblLastScan.Visible = false;
            // 
            // xrlblLastScanInput
            // 
            this.xrlblLastScanInput.Dpi = 100F;
            this.xrlblLastScanInput.Font = new System.Drawing.Font("Arial", 7F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrlblLastScanInput.LocationFloat = new DevExpress.Utils.PointFloat(562.9254F, 240.37F);
            this.xrlblLastScanInput.Name = "xrlblLastScanInput";
            this.xrlblLastScanInput.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrlblLastScanInput.SizeF = new System.Drawing.SizeF(78.83264F, 15.00005F);
            this.xrlblLastScanInput.StylePriority.UseFont = false;
            this.xrlblLastScanInput.Text = "Último escaneo:";
            this.xrlblLastScanInput.Visible = false;
            // 
            // xrlblStartScanInput
            // 
            this.xrlblStartScanInput.Dpi = 100F;
            this.xrlblStartScanInput.Font = new System.Drawing.Font("Arial", 7F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrlblStartScanInput.LocationFloat = new DevExpress.Utils.PointFloat(397.7946F, 240.3699F);
            this.xrlblStartScanInput.Name = "xrlblStartScanInput";
            this.xrlblStartScanInput.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrlblStartScanInput.SizeF = new System.Drawing.SizeF(75.96384F, 15.00002F);
            this.xrlblStartScanInput.StylePriority.UseFont = false;
            this.xrlblStartScanInput.Text = "Inicio Escaneo:";
            this.xrlblStartScanInput.Visible = false;
            // 
            // xrlblStartScan
            // 
            this.xrlblStartScan.Dpi = 100F;
            this.xrlblStartScan.Font = new System.Drawing.Font("Arial", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrlblStartScan.LocationFloat = new DevExpress.Utils.PointFloat(482.2056F, 240.3701F);
            this.xrlblStartScan.Name = "xrlblStartScan";
            this.xrlblStartScan.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrlblStartScan.SizeF = new System.Drawing.SizeF(63.29663F, 15.00002F);
            this.xrlblStartScan.StylePriority.UseFont = false;
            this.xrlblStartScan.Visible = false;
            // 
            // xrLabelCurrency
            // 
            this.xrLabelCurrency.Dpi = 100F;
            this.xrLabelCurrency.Font = new System.Drawing.Font("Arial", 7F, System.Drawing.FontStyle.Bold);
            this.xrLabelCurrency.LocationFloat = new DevExpress.Utils.PointFloat(145.626F, 116.7917F);
            this.xrLabelCurrency.Name = "xrLabelCurrency";
            this.xrLabelCurrency.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabelCurrency.SizeF = new System.Drawing.SizeF(44F, 15F);
            this.xrLabelCurrency.StylePriority.UseFont = false;
            this.xrLabelCurrency.StylePriority.UseTextAlignment = false;
            this.xrLabelCurrency.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            // 
            // xrLabel23
            // 
            this.xrLabel23.Dpi = 100F;
            this.xrLabel23.Font = new System.Drawing.Font("Arial", 7F, System.Drawing.FontStyle.Bold);
            this.xrLabel23.LocationFloat = new DevExpress.Utils.PointFloat(96.54057F, 116.7917F);
            this.xrLabel23.Name = "xrLabel23";
            this.xrLabel23.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel23.SizeF = new System.Drawing.SizeF(44F, 15F);
            this.xrLabel23.StylePriority.UseFont = false;
            this.xrLabel23.Text = "Moneda:";
            // 
            // dateEndLabel
            // 
            this.dateEndLabel.Dpi = 100F;
            this.dateEndLabel.Font = new System.Drawing.Font("Arial", 7F);
            this.dateEndLabel.LocationFloat = new DevExpress.Utils.PointFloat(436.9502F, 116.7917F);
            this.dateEndLabel.Name = "dateEndLabel";
            this.dateEndLabel.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.dateEndLabel.SizeF = new System.Drawing.SizeF(85.7504F, 15F);
            this.dateEndLabel.StylePriority.UseFont = false;
            // 
            // xrLabel7
            // 
            this.xrLabel7.Dpi = 100F;
            this.xrLabel7.Font = new System.Drawing.Font("Arial", 7F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel7.LocationFloat = new DevExpress.Utils.PointFloat(362.2025F, 116.7917F);
            this.xrLabel7.Name = "xrLabel7";
            this.xrLabel7.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel7.SizeF = new System.Drawing.SizeF(74.74771F, 15F);
            this.xrLabel7.StylePriority.UseFont = false;
            this.xrLabel7.Text = "Fecha Entrada:";
            // 
            // empFinalLabel
            // 
            this.empFinalLabel.Dpi = 100F;
            this.empFinalLabel.Font = new System.Drawing.Font("Arial", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.empFinalLabel.LocationFloat = new DevExpress.Utils.PointFloat(81.52708F, 60.00001F);
            this.empFinalLabel.Name = "empFinalLabel";
            this.empFinalLabel.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.empFinalLabel.SizeF = new System.Drawing.SizeF(246.3848F, 15F);
            this.empFinalLabel.StylePriority.UseFont = false;
            // 
            // xrLabel1
            // 
            this.xrLabel1.Dpi = 100F;
            this.xrLabel1.Font = new System.Drawing.Font("Arial", 7F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel1.LocationFloat = new DevExpress.Utils.PointFloat(0.0001271566F, 60.00001F);
            this.xrLabel1.Name = "xrLabel1";
            this.xrLabel1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel1.SizeF = new System.Drawing.SizeF(78.12498F, 15F);
            this.xrLabel1.StylePriority.UseFont = false;
            this.xrLabel1.Text = "Recibió:";
            // 
            // xrState
            // 
            this.xrState.Dpi = 100F;
            this.xrState.Font = new System.Drawing.Font("Arial", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrState.LocationFloat = new DevExpress.Utils.PointFloat(422.9998F, 59.99997F);
            this.xrState.Name = "xrState";
            this.xrState.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrState.SizeF = new System.Drawing.SizeF(201.0001F, 15F);
            this.xrState.StylePriority.UseFont = false;
            this.xrState.StylePriority.UseTextAlignment = false;
            this.xrState.Text = "- Estado";
            this.xrState.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // xraddress
            // 
            this.xraddress.Dpi = 100F;
            this.xraddress.Font = new System.Drawing.Font("Arial", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xraddress.LocationFloat = new DevExpress.Utils.PointFloat(422.9995F, 44.99998F);
            this.xraddress.Name = "xraddress";
            this.xraddress.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xraddress.SizeF = new System.Drawing.SizeF(201.0001F, 15F);
            this.xraddress.StylePriority.UseFont = false;
            this.xraddress.StylePriority.UseTextAlignment = false;
            this.xraddress.Text = "- Dirección";
            this.xraddress.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // supNoRealLabel
            // 
            this.supNoRealLabel.Dpi = 100F;
            this.supNoRealLabel.Font = new System.Drawing.Font("Arial", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.supNoRealLabel.LocationFloat = new DevExpress.Utils.PointFloat(29.60723F, 214.37F);
            this.supNoRealLabel.Name = "supNoRealLabel";
            this.supNoRealLabel.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.supNoRealLabel.SizeF = new System.Drawing.SizeF(134.9264F, 15F);
            this.supNoRealLabel.StylePriority.UseFont = false;
            // 
            // supDeparLabel
            // 
            this.supDeparLabel.Dpi = 100F;
            this.supDeparLabel.Font = new System.Drawing.Font("Arial", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.supDeparLabel.LocationFloat = new DevExpress.Utils.PointFloat(293.7709F, 204.0001F);
            this.supDeparLabel.Name = "supDeparLabel";
            this.supDeparLabel.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.supDeparLabel.SizeF = new System.Drawing.SizeF(211.9296F, 15F);
            this.supDeparLabel.StylePriority.UseFont = false;
            // 
            // supEmailLabel
            // 
            this.supEmailLabel.Dpi = 100F;
            this.supEmailLabel.Font = new System.Drawing.Font("Arial", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.supEmailLabel.LocationFloat = new DevExpress.Utils.PointFloat(257.8429F, 188.9998F);
            this.supEmailLabel.Name = "supEmailLabel";
            this.supEmailLabel.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.supEmailLabel.SizeF = new System.Drawing.SizeF(247.8577F, 15F);
            this.supEmailLabel.StylePriority.UseFont = false;
            // 
            // supTelLabel
            // 
            this.supTelLabel.Dpi = 100F;
            this.supTelLabel.Font = new System.Drawing.Font("Arial", 7F);
            this.supTelLabel.LocationFloat = new DevExpress.Utils.PointFloat(257.8429F, 173.9998F);
            this.supTelLabel.Name = "supTelLabel";
            this.supTelLabel.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.supTelLabel.SizeF = new System.Drawing.SizeF(247.8577F, 15.00002F);
            this.supTelLabel.StylePriority.UseFont = false;
            // 
            // xrLabel16
            // 
            this.xrLabel16.Dpi = 100F;
            this.xrLabel16.Font = new System.Drawing.Font("Arial", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel16.LocationFloat = new DevExpress.Utils.PointFloat(224.5096F, 204.0001F);
            this.xrLabel16.Name = "xrLabel16";
            this.xrLabel16.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel16.SizeF = new System.Drawing.SizeF(69.26146F, 15F);
            this.xrLabel16.StylePriority.UseFont = false;
            this.xrLabel16.Text = "Departamento:";
            // 
            // xrLabel15
            // 
            this.xrLabel15.Dpi = 100F;
            this.xrLabel15.Font = new System.Drawing.Font("Arial", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel15.LocationFloat = new DevExpress.Utils.PointFloat(224.5096F, 188.9998F);
            this.xrLabel15.Name = "xrLabel15";
            this.xrLabel15.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel15.SizeF = new System.Drawing.SizeF(33.33336F, 15F);
            this.xrLabel15.StylePriority.UseFont = false;
            this.xrLabel15.Text = "Email:";
            // 
            // xrLabel14
            // 
            this.xrLabel14.Dpi = 100F;
            this.xrLabel14.Font = new System.Drawing.Font("Arial", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel14.LocationFloat = new DevExpress.Utils.PointFloat(224.5096F, 173.9998F);
            this.xrLabel14.Name = "xrLabel14";
            this.xrLabel14.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel14.SizeF = new System.Drawing.SizeF(33.33336F, 15F);
            this.xrLabel14.StylePriority.UseFont = false;
            this.xrLabel14.Text = "Tel.:";
            // 
            // supAddrsLabel
            // 
            this.supAddrsLabel.Dpi = 100F;
            this.supAddrsLabel.Font = new System.Drawing.Font("Arial", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.supAddrsLabel.LocationFloat = new DevExpress.Utils.PointFloat(224.5096F, 158.9999F);
            this.supAddrsLabel.Name = "supAddrsLabel";
            this.supAddrsLabel.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.supAddrsLabel.SizeF = new System.Drawing.SizeF(281.191F, 15F);
            this.supAddrsLabel.StylePriority.UseFont = false;
            // 
            // commentsLabel
            // 
            this.commentsLabel.Dpi = 100F;
            this.commentsLabel.Font = new System.Drawing.Font("Arial", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.commentsLabel.LocationFloat = new DevExpress.Utils.PointFloat(527.6249F, 158.9999F);
            this.commentsLabel.Name = "commentsLabel";
            this.commentsLabel.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.commentsLabel.SizeF = new System.Drawing.SizeF(238.8383F, 70.37007F);
            this.commentsLabel.StylePriority.UseFont = false;
            // 
            // factSupLabel
            // 
            this.factSupLabel.Dpi = 100F;
            this.factSupLabel.Font = new System.Drawing.Font("Arial", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.factSupLabel.LocationFloat = new DevExpress.Utils.PointFloat(638.9998F, 116.7917F);
            this.factSupLabel.Name = "factSupLabel";
            this.factSupLabel.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.factSupLabel.SizeF = new System.Drawing.SizeF(142.4633F, 15F);
            this.factSupLabel.StylePriority.UseFont = false;
            // 
            // xrLabel13
            // 
            this.xrLabel13.Dpi = 100F;
            this.xrLabel13.Font = new System.Drawing.Font("Arial", 7F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel13.LocationFloat = new DevExpress.Utils.PointFloat(526.1611F, 144.0001F);
            this.xrLabel13.Name = "xrLabel13";
            this.xrLabel13.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel13.SizeF = new System.Drawing.SizeF(78.42465F, 15F);
            this.xrLabel13.StylePriority.UseFont = false;
            this.xrLabel13.Text = "Comentarios:";
            // 
            // xrLabel12
            // 
            this.xrLabel12.Dpi = 100F;
            this.xrLabel12.Font = new System.Drawing.Font("Arial", 7F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel12.LocationFloat = new DevExpress.Utils.PointFloat(526.1611F, 116.7917F);
            this.xrLabel12.Name = "xrLabel12";
            this.xrLabel12.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel12.SizeF = new System.Drawing.SizeF(97.83884F, 15F);
            this.xrLabel12.StylePriority.UseFont = false;
            this.xrLabel12.Text = "Factura Proveedor:";
            // 
            // supNameLabel
            // 
            this.supNameLabel.Dpi = 100F;
            this.supNameLabel.Font = new System.Drawing.Font("Arial", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.supNameLabel.LocationFloat = new DevExpress.Utils.PointFloat(224.5096F, 144.0001F);
            this.supNameLabel.Name = "supNameLabel";
            this.supNameLabel.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.supNameLabel.SizeF = new System.Drawing.SizeF(281.1909F, 14.99999F);
            this.supNameLabel.StylePriority.UseFont = false;
            // 
            // dateLabel
            // 
            this.dateLabel.Dpi = 100F;
            this.dateLabel.Font = new System.Drawing.Font("Arial", 7F);
            this.dateLabel.LocationFloat = new DevExpress.Utils.PointFloat(250.5096F, 116.7917F);
            this.dateLabel.Name = "dateLabel";
            this.dateLabel.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.dateLabel.SizeF = new System.Drawing.SizeF(103.4022F, 15F);
            this.dateLabel.StylePriority.UseFont = false;
            // 
            // xrLabel11
            // 
            this.xrLabel11.Dpi = 100F;
            this.xrLabel11.Font = new System.Drawing.Font("Arial", 7F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel11.LocationFloat = new DevExpress.Utils.PointFloat(155.6369F, 164.9485F);
            this.xrLabel11.Name = "xrLabel11";
            this.xrLabel11.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel11.SizeF = new System.Drawing.SizeF(68.87257F, 15F);
            this.xrLabel11.StylePriority.UseFont = false;
            this.xrLabel11.Text = "Proveedor:";
            // 
            // xrLabel9
            // 
            this.xrLabel9.Dpi = 100F;
            this.xrLabel9.Font = new System.Drawing.Font("Arial", 7F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel9.LocationFloat = new DevExpress.Utils.PointFloat(197.5096F, 116.7917F);
            this.xrLabel9.Name = "xrLabel9";
            this.xrLabel9.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel9.SizeF = new System.Drawing.SizeF(53F, 15F);
            this.xrLabel9.StylePriority.UseFont = false;
            this.xrLabel9.Text = "Fecha OC:";
            // 
            // supNoLabel
            // 
            this.supNoLabel.Dpi = 100F;
            this.supNoLabel.Font = new System.Drawing.Font("Arial", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.supNoLabel.LocationFloat = new DevExpress.Utils.PointFloat(29.60723F, 188.9998F);
            this.supNoLabel.Name = "supNoLabel";
            this.supNoLabel.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.supNoLabel.SizeF = new System.Drawing.SizeF(134.9264F, 15F);
            this.supNoLabel.StylePriority.UseFont = false;
            // 
            // xrLabel10
            // 
            this.xrLabel10.Dpi = 100F;
            this.xrLabel10.Font = new System.Drawing.Font("Arial", 7F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel10.LocationFloat = new DevExpress.Utils.PointFloat(0F, 164.9485F);
            this.xrLabel10.Name = "xrLabel10";
            this.xrLabel10.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel10.SizeF = new System.Drawing.SizeF(121.7524F, 15F);
            this.xrLabel10.StylePriority.UseFont = false;
            this.xrLabel10.Text = "No. Proveedor /  RFC:";
            // 
            // poNoLabel
            // 
            this.poNoLabel.Dpi = 100F;
            this.poNoLabel.Font = new System.Drawing.Font("Arial", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.poNoLabel.LocationFloat = new DevExpress.Utils.PointFloat(29.60723F, 131.7917F);
            this.poNoLabel.Name = "poNoLabel";
            this.poNoLabel.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.poNoLabel.SizeF = new System.Drawing.SizeF(124.6445F, 15F);
            this.poNoLabel.StylePriority.UseFont = false;
            // 
            // xrLabel8
            // 
            this.xrLabel8.Dpi = 100F;
            this.xrLabel8.Font = new System.Drawing.Font("Arial", 7F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel8.LocationFloat = new DevExpress.Utils.PointFloat(0F, 116.7917F);
            this.xrLabel8.Name = "xrLabel8";
            this.xrLabel8.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel8.SizeF = new System.Drawing.SizeF(75.91912F, 15F);
            this.xrLabel8.StylePriority.UseFont = false;
            this.xrLabel8.Text = "No. Compra:";
            // 
            // xrLine1
            // 
            this.xrLine1.Dpi = 100F;
            this.xrLine1.LocationFloat = new DevExpress.Utils.PointFloat(0F, 106.7917F);
            this.xrLine1.Name = "xrLine1";
            this.xrLine1.SizeF = new System.Drawing.SizeF(781.9999F, 10F);
            this.xrLine1.StylePriority.UseBorderDashStyle = false;
            // 
            // xrLabel2
            // 
            this.xrLabel2.Dpi = 100F;
            this.xrLabel2.Font = new System.Drawing.Font("Arial", 7F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel2.LocationFloat = new DevExpress.Utils.PointFloat(0F, 41.45834F);
            this.xrLabel2.Name = "xrLabel2";
            this.xrLabel2.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel2.SizeF = new System.Drawing.SizeF(78.12498F, 15F);
            this.xrLabel2.StylePriority.UseFont = false;
            this.xrLabel2.Text = "Realizó:";
            // 
            // xrLabel5
            // 
            this.xrLabel5.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel5.BorderWidth = 4F;
            this.xrLabel5.Dpi = 100F;
            this.xrLabel5.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel5.LocationFloat = new DevExpress.Utils.PointFloat(65.85083F, 0F);
            this.xrLabel5.Name = "xrLabel5";
            this.xrLabel5.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel5.SizeF = new System.Drawing.SizeF(209.2172F, 30F);
            this.xrLabel5.StylePriority.UseBorders = false;
            this.xrLabel5.StylePriority.UseBorderWidth = false;
            this.xrLabel5.StylePriority.UseFont = false;
            this.xrLabel5.StylePriority.UseTextAlignment = false;
            this.xrLabel5.Text = "Entrada de Mercancía";
            this.xrLabel5.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrRFC
            // 
            this.xrRFC.Dpi = 100F;
            this.xrRFC.Font = new System.Drawing.Font("Arial", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrRFC.LocationFloat = new DevExpress.Utils.PointFloat(537.174F, 29.99996F);
            this.xrRFC.Name = "xrRFC";
            this.xrRFC.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrRFC.SizeF = new System.Drawing.SizeF(86.82587F, 15F);
            this.xrRFC.StylePriority.UseFont = false;
            this.xrRFC.StylePriority.UseTextAlignment = false;
            this.xrRFC.Text = "PRT121101JQ8";
            this.xrRFC.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // siteLabel
            // 
            this.siteLabel.Dpi = 100F;
            this.siteLabel.Font = new System.Drawing.Font("Arial", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.siteLabel.LocationFloat = new DevExpress.Utils.PointFloat(422.9998F, 14.99999F);
            this.siteLabel.Name = "siteLabel";
            this.siteLabel.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.siteLabel.SizeF = new System.Drawing.SizeF(201.0001F, 15F);
            this.siteLabel.StylePriority.UseFont = false;
            this.siteLabel.StylePriority.UseTextAlignment = false;
            this.siteLabel.Text = "*MARISCAL";
            this.siteLabel.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // xrLabel3
            // 
            this.xrLabel3.Dpi = 100F;
            this.xrLabel3.Font = new System.Drawing.Font("Arial", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel3.LocationFloat = new DevExpress.Utils.PointFloat(360.5193F, 0F);
            this.xrLabel3.Name = "xrLabel3";
            this.xrLabel3.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel3.SizeF = new System.Drawing.SizeF(263.4802F, 15F);
            this.xrLabel3.StylePriority.UseFont = false;
            this.xrLabel3.StylePriority.UseTextAlignment = false;
            this.xrLabel3.Text = "PROCESADORA DEL RANCHO DEL TIO S DE RL CV";
            this.xrLabel3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // empLabel
            // 
            this.empLabel.Dpi = 100F;
            this.empLabel.Font = new System.Drawing.Font("Arial", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.empLabel.LocationFloat = new DevExpress.Utils.PointFloat(81.52695F, 41.45834F);
            this.empLabel.Name = "empLabel";
            this.empLabel.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.empLabel.SizeF = new System.Drawing.SizeF(246.3848F, 15F);
            this.empLabel.StylePriority.UseFont = false;
            // 
            // xrLine2
            // 
            this.xrLine2.Dpi = 100F;
            this.xrLine2.LocationFloat = new DevExpress.Utils.PointFloat(8.742014E-05F, 263.24F);
            this.xrLine2.Name = "xrLine2";
            this.xrLine2.SizeF = new System.Drawing.SizeF(782.0001F, 10F);
            // 
            // GroupFooter1
            // 
            this.GroupFooter1.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel4,
            this.xrLabel6,
            this.iepsLabel,
            this.xrLabel30,
            this.xrLabel31,
            this.xrLabel32,
            this.xrLabel33,
            this.subtotalLabel,
            this.discountLabel,
            this.totalLabel,
            this.xrLabel29,
            this.ivaLabel});
            this.GroupFooter1.Dpi = 100F;
            this.GroupFooter1.HeightF = 252.8843F;
            this.GroupFooter1.Name = "GroupFooter1";
            // 
            // xrLabel4
            // 
            this.xrLabel4.Dpi = 100F;
            this.xrLabel4.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel4.LocationFloat = new DevExpress.Utils.PointFloat(0.0001271566F, 120.3843F);
            this.xrLabel4.Multiline = true;
            this.xrLabel4.Name = "xrLabel4";
            this.xrLabel4.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel4.SizeF = new System.Drawing.SizeF(781.9997F, 122.5F);
            this.xrLabel4.StylePriority.UseFont = false;
            this.xrLabel4.StylePriority.UseTextAlignment = false;
            this.xrLabel4.Text = resources.GetString("xrLabel4.Text");
            this.xrLabel4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleJustify;
            // 
            // xrLabel6
            // 
            this.xrLabel6.Dpi = 100F;
            this.xrLabel6.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel6.LocationFloat = new DevExpress.Utils.PointFloat(12.37742F, 105.3843F);
            this.xrLabel6.Name = "xrLabel6";
            this.xrLabel6.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel6.SizeF = new System.Drawing.SizeF(63.54167F, 15F);
            this.xrLabel6.StylePriority.UseFont = false;
            this.xrLabel6.StylePriority.UseTextAlignment = false;
            this.xrLabel6.Text = "Acuerdo";
            this.xrLabel6.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // iepsLabel
            // 
            this.iepsLabel.Dpi = 100F;
            this.iepsLabel.Font = new System.Drawing.Font("Arial", 8F);
            this.iepsLabel.LocationFloat = new DevExpress.Utils.PointFloat(679.4768F, 71.84258F);
            this.iepsLabel.Name = "iepsLabel";
            this.iepsLabel.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.iepsLabel.SizeF = new System.Drawing.SizeF(102.5233F, 15F);
            this.iepsLabel.StylePriority.UseFont = false;
            this.iepsLabel.StylePriority.UseTextAlignment = false;
            this.iepsLabel.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // xrLabel30
            // 
            this.xrLabel30.Dpi = 100F;
            this.xrLabel30.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Bold);
            this.xrLabel30.LocationFloat = new DevExpress.Utils.PointFloat(612F, 26.84249F);
            this.xrLabel30.Name = "xrLabel30";
            this.xrLabel30.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel30.SizeF = new System.Drawing.SizeF(65F, 15F);
            this.xrLabel30.StylePriority.UseFont = false;
            this.xrLabel30.Text = "Descuento:";
            this.xrLabel30.Visible = false;
            // 
            // xrLabel31
            // 
            this.xrLabel31.Dpi = 100F;
            this.xrLabel31.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Bold);
            this.xrLabel31.LocationFloat = new DevExpress.Utils.PointFloat(649.9999F, 56.8426F);
            this.xrLabel31.Name = "xrLabel31";
            this.xrLabel31.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel31.SizeF = new System.Drawing.SizeF(27F, 15F);
            this.xrLabel31.StylePriority.UseFont = false;
            this.xrLabel31.Text = "IVA:";
            // 
            // xrLabel32
            // 
            this.xrLabel32.Dpi = 100F;
            this.xrLabel32.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Bold);
            this.xrLabel32.LocationFloat = new DevExpress.Utils.PointFloat(643F, 71.84258F);
            this.xrLabel32.Name = "xrLabel32";
            this.xrLabel32.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel32.SizeF = new System.Drawing.SizeF(34F, 15F);
            this.xrLabel32.StylePriority.UseFont = false;
            this.xrLabel32.Text = "IEPS:";
            // 
            // xrLabel33
            // 
            this.xrLabel33.Dpi = 100F;
            this.xrLabel33.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Bold);
            this.xrLabel33.LocationFloat = new DevExpress.Utils.PointFloat(641.7577F, 86.84253F);
            this.xrLabel33.Name = "xrLabel33";
            this.xrLabel33.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel33.SizeF = new System.Drawing.SizeF(35.41666F, 15F);
            this.xrLabel33.StylePriority.UseFont = false;
            this.xrLabel33.Text = "Total:";
            // 
            // subtotalLabel
            // 
            this.subtotalLabel.Dpi = 100F;
            this.subtotalLabel.Font = new System.Drawing.Font("Arial", 8F);
            this.subtotalLabel.LocationFloat = new DevExpress.Utils.PointFloat(679.3024F, 41.84251F);
            this.subtotalLabel.Name = "subtotalLabel";
            this.subtotalLabel.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.subtotalLabel.SizeF = new System.Drawing.SizeF(102.6976F, 15F);
            this.subtotalLabel.StylePriority.UseFont = false;
            this.subtotalLabel.StylePriority.UseTextAlignment = false;
            this.subtotalLabel.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // discountLabel
            // 
            this.discountLabel.Dpi = 100F;
            this.discountLabel.Font = new System.Drawing.Font("Arial", 8F);
            this.discountLabel.LocationFloat = new DevExpress.Utils.PointFloat(679.3024F, 26.84256F);
            this.discountLabel.Name = "discountLabel";
            this.discountLabel.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.discountLabel.SizeF = new System.Drawing.SizeF(102.6976F, 15F);
            this.discountLabel.StylePriority.UseFont = false;
            this.discountLabel.StylePriority.UseTextAlignment = false;
            this.discountLabel.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.discountLabel.Visible = false;
            // 
            // totalLabel
            // 
            this.totalLabel.Dpi = 100F;
            this.totalLabel.Font = new System.Drawing.Font("Arial", 8F);
            this.totalLabel.LocationFloat = new DevExpress.Utils.PointFloat(679.4768F, 86.8426F);
            this.totalLabel.Name = "totalLabel";
            this.totalLabel.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.totalLabel.SizeF = new System.Drawing.SizeF(102.5233F, 15F);
            this.totalLabel.StylePriority.UseFont = false;
            this.totalLabel.StylePriority.UseTextAlignment = false;
            this.totalLabel.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // xrLabel29
            // 
            this.xrLabel29.Dpi = 100F;
            this.xrLabel29.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Bold);
            this.xrLabel29.LocationFloat = new DevExpress.Utils.PointFloat(623.9999F, 41.84248F);
            this.xrLabel29.Name = "xrLabel29";
            this.xrLabel29.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel29.SizeF = new System.Drawing.SizeF(53F, 15F);
            this.xrLabel29.StylePriority.UseFont = false;
            this.xrLabel29.Text = "Subtotal:";
            // 
            // ivaLabel
            // 
            this.ivaLabel.Dpi = 100F;
            this.ivaLabel.Font = new System.Drawing.Font("Arial", 8F);
            this.ivaLabel.LocationFloat = new DevExpress.Utils.PointFloat(679.4768F, 56.84267F);
            this.ivaLabel.Name = "ivaLabel";
            this.ivaLabel.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.ivaLabel.SizeF = new System.Drawing.SizeF(102.5233F, 15F);
            this.ivaLabel.StylePriority.UseFont = false;
            this.ivaLabel.StylePriority.UseTextAlignment = false;
            this.ivaLabel.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // PageFooter
            // 
            this.PageFooter.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrPageInfo1});
            this.PageFooter.Dpi = 100F;
            this.PageFooter.HeightF = 15.625F;
            this.PageFooter.Name = "PageFooter";
            // 
            // xrPageInfo1
            // 
            this.xrPageInfo1.Dpi = 100F;
            this.xrPageInfo1.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Bold);
            this.xrPageInfo1.Format = "Página {0} de {1}";
            this.xrPageInfo1.LocationFloat = new DevExpress.Utils.PointFloat(0F, 0F);
            this.xrPageInfo1.Name = "xrPageInfo1";
            this.xrPageInfo1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrPageInfo1.SizeF = new System.Drawing.SizeF(100F, 15F);
            this.xrPageInfo1.StylePriority.UseFont = false;
            // 
            // DetailReport
            // 
            this.DetailReport.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.Detail1});
            this.DetailReport.Dpi = 100F;
            this.DetailReport.Level = 0;
            this.DetailReport.Name = "DetailReport";
            // 
            // Detail1
            // 
            this.Detail1.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable1});
            this.Detail1.Dpi = 100F;
            this.Detail1.HeightF = 20F;
            this.Detail1.Name = "Detail1";
            // 
            // xrTable1
            // 
            this.xrTable1.BackColor = System.Drawing.Color.White;
            this.xrTable1.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrTable1.Dpi = 100F;
            this.xrTable1.Font = new System.Drawing.Font("Arial", 6F);
            this.xrTable1.LocationFloat = new DevExpress.Utils.PointFloat(0F, 0F);
            this.xrTable1.Name = "xrTable1";
            this.xrTable1.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow1});
            this.xrTable1.SizeF = new System.Drawing.SizeF(782.0001F, 20F);
            this.xrTable1.StylePriority.UseBackColor = false;
            this.xrTable1.StylePriority.UseBorders = false;
            this.xrTable1.StylePriority.UseFont = false;
            // 
            // xrTableRow1
            // 
            this.xrTableRow1.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.prod_id,
            this.prod_name,
            this.quantity,
            this.gr_quantity,
            this.um,
            this.tot_units,
            this.unit_price,
            this.import,
            this.ieps,
            this.iva,
            this.subtotal});
            this.xrTableRow1.Dpi = 100F;
            this.xrTableRow1.Name = "xrTableRow1";
            this.xrTableRow1.Weight = 1D;
            // 
            // prod_id
            // 
            this.prod_id.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.prod_id.Dpi = 100F;
            this.prod_id.Font = new System.Drawing.Font("Arial", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.prod_id.Name = "prod_id";
            this.prod_id.StylePriority.UseBorders = false;
            this.prod_id.StylePriority.UseFont = false;
            this.prod_id.StylePriority.UseTextAlignment = false;
            this.prod_id.Text = "prod_id";
            this.prod_id.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.prod_id.Weight = 0.70811461665213371D;
            // 
            // prod_name
            // 
            this.prod_name.Dpi = 100F;
            this.prod_name.Font = new System.Drawing.Font("Arial", 7F);
            this.prod_name.Name = "prod_name";
            this.prod_name.StylePriority.UseFont = false;
            this.prod_name.StylePriority.UseTextAlignment = false;
            this.prod_name.Text = "prod_name";
            this.prod_name.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.prod_name.Weight = 1.25220935551468D;
            // 
            // quantity
            // 
            this.quantity.Dpi = 100F;
            this.quantity.Font = new System.Drawing.Font("Arial", 7F);
            this.quantity.Name = "quantity";
            this.quantity.StylePriority.UseFont = false;
            this.quantity.StylePriority.UseTextAlignment = false;
            this.quantity.Text = "quantity";
            this.quantity.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.quantity.Weight = 0.53285420034976516D;
            // 
            // gr_quantity
            // 
            this.gr_quantity.Dpi = 100F;
            this.gr_quantity.Font = new System.Drawing.Font("Arial", 7F);
            this.gr_quantity.Name = "gr_quantity";
            this.gr_quantity.StylePriority.UseFont = false;
            this.gr_quantity.StylePriority.UseTextAlignment = false;
            this.gr_quantity.Text = "gr_quantity";
            this.gr_quantity.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.gr_quantity.Weight = 0.47896933018410492D;
            // 
            // um
            // 
            this.um.Dpi = 100F;
            this.um.Font = new System.Drawing.Font("Arial", 7F);
            this.um.Name = "um";
            this.um.StylePriority.UseFont = false;
            this.um.StylePriority.UseTextAlignment = false;
            this.um.Text = "um";
            this.um.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.um.Weight = 0.66349685346357723D;
            // 
            // tot_units
            // 
            this.tot_units.Dpi = 100F;
            this.tot_units.Font = new System.Drawing.Font("Arial", 7F);
            this.tot_units.Name = "tot_units";
            this.tot_units.StylePriority.UseFont = false;
            this.tot_units.StylePriority.UseTextAlignment = false;
            this.tot_units.Text = "tot_units";
            this.tot_units.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.tot_units.Weight = 0.73643991350123617D;
            // 
            // unit_price
            // 
            this.unit_price.Dpi = 100F;
            this.unit_price.Font = new System.Drawing.Font("Arial", 7F);
            this.unit_price.Name = "unit_price";
            this.unit_price.StylePriority.UseFont = false;
            this.unit_price.StylePriority.UseTextAlignment = false;
            this.unit_price.Text = "unit_price";
            this.unit_price.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.unit_price.Weight = 0.62554889957981652D;
            // 
            // import
            // 
            this.import.Dpi = 100F;
            this.import.Font = new System.Drawing.Font("Arial", 7F);
            this.import.Name = "import";
            this.import.StylePriority.UseFont = false;
            this.import.StylePriority.UseTextAlignment = false;
            this.import.Text = "import";
            this.import.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.import.Weight = 0.65821595940075417D;
            // 
            // ieps
            // 
            this.ieps.Dpi = 100F;
            this.ieps.Font = new System.Drawing.Font("Arial", 7F);
            this.ieps.Name = "ieps";
            this.ieps.StylePriority.UseFont = false;
            this.ieps.StylePriority.UseTextAlignment = false;
            this.ieps.Text = "ieps";
            this.ieps.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.ieps.Weight = 0.50283464900650066D;
            // 
            // iva
            // 
            this.iva.Dpi = 100F;
            this.iva.Font = new System.Drawing.Font("Arial", 7F);
            this.iva.Name = "iva";
            this.iva.StylePriority.UseFont = false;
            this.iva.StylePriority.UseTextAlignment = false;
            this.iva.Text = "iva";
            this.iva.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.iva.Weight = 0.39000882177936869D;
            // 
            // subtotal
            // 
            this.subtotal.Dpi = 100F;
            this.subtotal.Font = new System.Drawing.Font("Arial", 7F);
            this.subtotal.Name = "subtotal";
            this.subtotal.StylePriority.UseFont = false;
            this.subtotal.StylePriority.UseTextAlignment = false;
            this.subtotal.Text = "subtotal";
            this.subtotal.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.subtotal.Weight = 0.53924861858604112D;
            // 
            // xrPictureBox1
            // 
            this.xrPictureBox1.Dpi = 100F;
            this.xrPictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("xrPictureBox1.Image")));
            this.xrPictureBox1.LocationFloat = new DevExpress.Utils.PointFloat(631.7575F, 0F);
            this.xrPictureBox1.Name = "xrPictureBox1";
            this.xrPictureBox1.SizeF = new System.Drawing.SizeF(140.2425F, 91.79173F);
            // 
            // PurchasesOrdered
            // 
            this.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.Detail,
            this.TopMargin,
            this.BottomMargin,
            this.ReportHeader,
            this.GroupFooter1,
            this.PageFooter,
            this.DetailReport});
            this.Margins = new System.Drawing.Printing.Margins(40, 28, 30, 55);
            this.Version = "16.2";
            ((System.ComponentModel.ISupportInitialize)(this.xrTable1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

        }

        #endregion

        private DevExpress.XtraReports.UI.DetailBand Detail;
        private DevExpress.XtraReports.UI.TopMarginBand TopMargin;
        private DevExpress.XtraReports.UI.BottomMarginBand BottomMargin;
        private DevExpress.XtraReports.UI.ReportHeaderBand ReportHeader;
        private DevExpress.XtraReports.UI.GroupFooterBand GroupFooter1;
        private DevExpress.XtraReports.UI.PageFooterBand PageFooter;
        private DevExpress.XtraReports.UI.XRPageInfo xrPageInfo1;
        private DevExpress.XtraReports.UI.DetailReportBand DetailReport;
        private DevExpress.XtraReports.UI.DetailBand Detail1;
        private DevExpress.XtraReports.UI.XRTable xrTable1;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow1;
        private DevExpress.XtraReports.UI.XRTableCell prod_id;
        private DevExpress.XtraReports.UI.XRTableCell prod_name;
        private DevExpress.XtraReports.UI.XRTableCell quantity;
        private DevExpress.XtraReports.UI.XRTableCell gr_quantity;
        private DevExpress.XtraReports.UI.XRTableCell um;
        private DevExpress.XtraReports.UI.XRTableCell tot_units;
        private DevExpress.XtraReports.UI.XRTableCell unit_price;
        private DevExpress.XtraReports.UI.XRTableCell import;
        private DevExpress.XtraReports.UI.XRTableCell ieps;
        private DevExpress.XtraReports.UI.XRTableCell iva;
        private DevExpress.XtraReports.UI.XRTableCell subtotal;
        private DevExpress.XtraReports.UI.XRLabel supDeparLabel;
        private DevExpress.XtraReports.UI.XRLabel supEmailLabel;
        private DevExpress.XtraReports.UI.XRLabel supTelLabel;
        private DevExpress.XtraReports.UI.XRLabel xrLabel16;
        private DevExpress.XtraReports.UI.XRLabel xrLabel15;
        private DevExpress.XtraReports.UI.XRLabel xrLabel14;
        private DevExpress.XtraReports.UI.XRLabel Detalle;
        private DevExpress.XtraReports.UI.XRLabel commentsLabel;
        private DevExpress.XtraReports.UI.XRLabel factSupLabel;
        private DevExpress.XtraReports.UI.XRLabel xrLabel13;
        private DevExpress.XtraReports.UI.XRLabel xrLabel12;
        private DevExpress.XtraReports.UI.XRLabel supNameLabel;
        private DevExpress.XtraReports.UI.XRLabel dateLabel;
        private DevExpress.XtraReports.UI.XRLabel xrLabel11;
        private DevExpress.XtraReports.UI.XRLabel xrLabel9;
        private DevExpress.XtraReports.UI.XRLabel supNoLabel;
        private DevExpress.XtraReports.UI.XRLabel xrLabel10;
        private DevExpress.XtraReports.UI.XRLabel poNoLabel;
        private DevExpress.XtraReports.UI.XRLabel xrLabel8;
        private DevExpress.XtraReports.UI.XRLine xrLine1;
        private DevExpress.XtraReports.UI.XRLabel xrLabel2;
        private DevExpress.XtraReports.UI.XRLabel xrLabel5;
        private DevExpress.XtraReports.UI.XRLabel xrRFC;
        private DevExpress.XtraReports.UI.XRLabel siteLabel;
        private DevExpress.XtraReports.UI.XRLabel xrLabel3;
        private DevExpress.XtraReports.UI.XRLabel empLabel;
        private DevExpress.XtraReports.UI.XRLine xrLine2;
        private DevExpress.XtraReports.UI.XRLabel xrLabel17;
        private DevExpress.XtraReports.UI.XRLabel xrLabel18;
        private DevExpress.XtraReports.UI.XRLabel xrLabel19;
        private DevExpress.XtraReports.UI.XRLabel xrLabel34;
        private DevExpress.XtraReports.UI.XRLabel xrLabel20;
        private DevExpress.XtraReports.UI.XRLabel xrLabel21;
        private DevExpress.XtraReports.UI.XRLabel xrLabel22;
        private DevExpress.XtraReports.UI.XRLabel xrLabel24;
        private DevExpress.XtraReports.UI.XRLabel xrLabel25;
        private DevExpress.XtraReports.UI.XRLabel xrLabel26;
        private DevExpress.XtraReports.UI.XRLabel xrLabel28;
        private DevExpress.XtraReports.UI.XRLabel iepsLabel;
        private DevExpress.XtraReports.UI.XRLabel xrLabel30;
        private DevExpress.XtraReports.UI.XRLabel xrLabel31;
        private DevExpress.XtraReports.UI.XRLabel xrLabel32;
        private DevExpress.XtraReports.UI.XRLabel xrLabel33;
        private DevExpress.XtraReports.UI.XRLabel subtotalLabel;
        private DevExpress.XtraReports.UI.XRLabel discountLabel;
        private DevExpress.XtraReports.UI.XRLabel totalLabel;
        private DevExpress.XtraReports.UI.XRLabel xrLabel29;
        private DevExpress.XtraReports.UI.XRLabel ivaLabel;
        private DevExpress.XtraReports.UI.XRLine xrLine4;
        private DevExpress.XtraReports.UI.XRLine xrLine3;
        private DevExpress.XtraReports.UI.XRLabel supAddrsLabel;
        private DevExpress.XtraReports.UI.XRLabel supNoRealLabel;
        private DevExpress.XtraReports.UI.XRLabel xrState;
        private DevExpress.XtraReports.UI.XRLabel xraddress;
        private DevExpress.XtraReports.UI.XRLabel empFinalLabel;
        private DevExpress.XtraReports.UI.XRLabel xrLabel1;
        private DevExpress.XtraReports.UI.XRLabel xrLabel4;
        private DevExpress.XtraReports.UI.XRLabel xrLabel6;
        private DevExpress.XtraReports.UI.XRLabel dateEndLabel;
        private DevExpress.XtraReports.UI.XRLabel xrLabel7;
        private DevExpress.XtraReports.UI.XRLabel xrLabelCurrency;
        private DevExpress.XtraReports.UI.XRLabel xrLabel23;
        private DevExpress.XtraReports.UI.XRLabel xrlblLastScan;
        private DevExpress.XtraReports.UI.XRLabel xrlblLastScanInput;
        private DevExpress.XtraReports.UI.XRLabel xrlblStartScanInput;
        private DevExpress.XtraReports.UI.XRLabel xrlblStartScan;
        private DevExpress.XtraReports.UI.XRPictureBox xrPictureBox1;
    }
}
