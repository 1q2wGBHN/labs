﻿using System;
using System.Data;
using DevExpress.XtraReports.UI;
using App.Entities.ViewModels.Sales;

namespace FloridoERPTX.Reports
{
    public partial class AvgCustomersSalesReport : DevExpress.XtraReports.UI.XtraReport
    {
       
        public DataSet DataSetTableOrdered = null;        
        public AvgCustomersSalesReport()
        {
            InitializeComponent();
          
        }
        public DataSet printTable(AverageCustomersByHourIndex Model) 
        {
            try
            {               
                DataSetTableOrdered = new DataSet();
                siteLabel.Text = Model.SiteName;
                DataTable DataTableOrdered = new DataTable();
                DataSetTableOrdered.Tables.Add(DataTableOrdered);
                
                DataTableOrdered.Columns.Add(new DataColumn("Day"));
                DataTableOrdered.Columns.Add(new DataColumn("Hr6"));
                DataTableOrdered.Columns.Add(new DataColumn("Hr7"));
                DataTableOrdered.Columns.Add(new DataColumn("Hr8"));
                DataTableOrdered.Columns.Add(new DataColumn("Hr9"));
                DataTableOrdered.Columns.Add(new DataColumn("Hr10"));
                DataTableOrdered.Columns.Add(new DataColumn("Hr11"));
                DataTableOrdered.Columns.Add(new DataColumn("Hr12"));
                DataTableOrdered.Columns.Add(new DataColumn("Hr13"));
                DataTableOrdered.Columns.Add(new DataColumn("Hr14"));
                DataTableOrdered.Columns.Add(new DataColumn("Hr15"));
                DataTableOrdered.Columns.Add(new DataColumn("Hr16"));
                DataTableOrdered.Columns.Add(new DataColumn("Hr17"));
                DataTableOrdered.Columns.Add(new DataColumn("Hr18"));
                DataTableOrdered.Columns.Add(new DataColumn("Hr19"));
                DataTableOrdered.Columns.Add(new DataColumn("Hr20"));
                DataTableOrdered.Columns.Add(new DataColumn("Hr21"));
                DataTableOrdered.Columns.Add(new DataColumn("Hr22"));
                DataTableOrdered.Columns.Add(new DataColumn("Hr23"));

                foreach (var item in Model.DataList)
                {
                    DataRow DataRowOrdered = DataSetTableOrdered.Tables[0].NewRow();
                    DataRowOrdered["Day"] = item.Day;
                    DataRowOrdered["Hr6"] = item.Hr6;
                    DataRowOrdered["Hr7"] = item.Hr7;
                    DataRowOrdered["Hr8"] =  item.Hr8;
                    DataRowOrdered["Hr9"] = item.Hr9;
                    DataRowOrdered["Hr10"] = item.Hr10;
                    DataRowOrdered["Hr11"] = item.Hr11;
                    DataRowOrdered["Hr12"] = item.Hr12;
                    DataRowOrdered["Hr13"] = item.Hr13;
                    DataRowOrdered["Hr14"] = item.Hr14;
                    DataRowOrdered["Hr15"] = item.Hr15;
                    DataRowOrdered["Hr16"] = item.Hr16;
                    DataRowOrdered["Hr17"] = item.Hr17;
                    DataRowOrdered["Hr18"] = item.Hr18;
                    DataRowOrdered["Hr19"] = item.Hr19;
                    DataRowOrdered["Hr20"] = item.Hr20;
                    DataRowOrdered["Hr21"] = item.Hr21;
                    DataRowOrdered["Hr22"] = item.Hr22;
                    DataRowOrdered["Hr23"] = item.Hr23;

                    DataSetTableOrdered.Tables[0].Rows.Add(DataRowOrdered);                                                        
                }               

                Day.DataBindings.Add(new XRBinding("Text", null, "xrTableRowFormat.Day"));
                Hr6.DataBindings.Add(new XRBinding("Text", null, "xrTableRowFormat.Hr6"));
                Hr7.DataBindings.Add(new XRBinding("Text", null, "xrTableRowFormat.Hr7"));
                Hr8.DataBindings.Add(new XRBinding("Text", null, "xrTableRowFormat.Hr8"));
                Hr9.DataBindings.Add(new XRBinding("Text", null, "xrTableRowFormat.Hr9"));
                Hr10.DataBindings.Add(new XRBinding("Text", null, "xrTableRowFormat.Hr10"));
                Hr11.DataBindings.Add(new XRBinding("Text", null, "xrTableRowFormat.Hr11"));
                Hr12.DataBindings.Add(new XRBinding("Text", null, "xrTableRowFormat.Hr12"));
                Hr13.DataBindings.Add(new XRBinding("Text", null, "xrTableRowFormat.Hr13"));
                Hr14.DataBindings.Add(new XRBinding("Text", null, "xrTableRowFormat.Hr14"));
                Hr15.DataBindings.Add(new XRBinding("Text", null, "xrTableRowFormat.Hr15"));
                Hr16.DataBindings.Add(new XRBinding("Text", null, "xrTableRowFormat.Hr16"));
                Hr17.DataBindings.Add(new XRBinding("Text", null, "xrTableRowFormat.Hr17"));
                Hr18.DataBindings.Add(new XRBinding("Text", null, "xrTableRowFormat.Hr18"));
                Hr19.DataBindings.Add(new XRBinding("Text", null, "xrTableRowFormat.Hr19"));
                Hr20.DataBindings.Add(new XRBinding("Text", null, "xrTableRowFormat.Hr20"));
                Hr21.DataBindings.Add(new XRBinding("Text", null, "xrTableRowFormat.Hr21"));
                Hr22.DataBindings.Add(new XRBinding("Text", null, "xrTableRowFormat.Hr22"));
                Hr23.DataBindings.Add(new XRBinding("Text", null, "xrTableRowFormat.Hr23"));

                return DataSetTableOrdered;
            }
            catch (Exception ex)
            {
                string d=ex.Message;
                throw;
            }
          
        }       
    }
}
