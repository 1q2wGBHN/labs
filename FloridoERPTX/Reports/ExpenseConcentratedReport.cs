﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
using App.BLL.Supplier;
using System.Data;
using App.Entities.ViewModels.Expenses;
using App.Entities;
using System.Collections.Generic;
using App.DAL.Supplier;

namespace FloridoERPTX.Reports
{
    public partial class ExpenseConcentratedReport : DevExpress.XtraReports.UI.XtraReport
    {
        private MaSupplierRepository _maSupplierRepository;
        public DataSet DataSetTableExpense = null;
        public string supplier_name = "";
        public decimal ivaValueMxn, iepsValueMxn, ImportValueMxn, subTotalMxn, isrRMxn, ivaRMxn, discountMxn, ivaValueUsd, iepsValueUsd, ImportValueUsd, subTotalUsd, isrRUsd, ivaRUsd, discountUsd = 0;
        public ExpenseConcentratedReport()
        {
            InitializeComponent();
            _maSupplierRepository = new MaSupplierRepository();
        }
        public DataSet printTable(string SupplierId, string Category, DateTime? BeginDate, DateTime? EndDate, string Currency, string typeExpense, string status, List<ExpensesReportModel> detail, SITES site_info)
        {
            try
            {
                var firstDayOfMonth = new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1);
                var lastDayOfMonth = firstDayOfMonth.AddMonths(1).AddDays(-1);

                if (!string.IsNullOrWhiteSpace(SupplierId))
                    supplier_name = _maSupplierRepository.GetNameSupplier(Convert.ToInt32(SupplierId));
                else
                    supplier_name = "TODOS LOS PROVEEDORES";

                if (BeginDate.HasValue)
                {
                    firstDayOfMonth = BeginDate.Value;
                    lastDayOfMonth = EndDate.Value;
                }
                if (Category == "")
                    Category = "TODOS LAS AREAS";

                if (Currency == "")
                    Currency = "TODAS LAS MONEDAS";

                if (typeExpense == "")
                    typeExpense = "TODAS LOS GASTOS";

                //Gastos
                xrlblDateStartShow.Text = firstDayOfMonth.ToString("dd/MM/yyyy");
                xrlblDateEndShow.Text = lastDayOfMonth.ToString("dd/MM/yyyy");
                xrlblTypeShow.Text = typeExpense;
                xrlblAreaShow.Text = Category;
                xrlblSupplierUpShow.Text = supplier_name;
                xrlblCurrencyUpShow.Text = Currency;
                xrlblStatusShow.Text = GetStatus(status);

                //Tienda
                xraddress.Text = site_info.address + " C.P. " + site_info.postal_code;
                xrState.Text = site_info.city + ", " + site_info.state;
                siteLabel.Text = "*" + site_info.site_name;
                
                DataSetTableExpense = new DataSet();
                DataTable table = new DataTable();
                DataSetTableExpense.Tables.Add(table);
                table.Columns.Add(new DataColumn("xrCellFolio"));
                table.Columns.Add(new DataColumn("xrCellInvoice"));
                table.Columns.Add(new DataColumn("xrCellSupplier"));
                table.Columns.Add(new DataColumn("xrCellCategory"));
                table.Columns.Add(new DataColumn("xrCellType"));
                table.Columns.Add(new DataColumn("xrCellCurrecency"));
                table.Columns.Add(new DataColumn("xrCellDate"));
                table.Columns.Add(new DataColumn("xrCellSubtotal"));
                table.Columns.Add(new DataColumn("xrCellIVA"));
                table.Columns.Add(new DataColumn("xrCellIEPS"));
                table.Columns.Add(new DataColumn("xrCellDiscount"));
                table.Columns.Add(new DataColumn("xrCellISRR"));
                table.Columns.Add(new DataColumn("xrCellIVAR"));
                table.Columns.Add(new DataColumn("xrCellImport"));
                foreach (var item in detail)
                {
                    DataRow DataRowOrdered = DataSetTableExpense.Tables[0].NewRow();
                    DataRowOrdered["xrCellFolio"] = item.ReferenceFolio.ToString();
                    DataRowOrdered["xrCellInvoice"] = item.Invoice;
                    DataRowOrdered["xrCellSupplier"] = item.Supplier;
                    DataRowOrdered["xrCellCategory"] = item.Category;
                    DataRowOrdered["xrCellType"] = item.ReferenceType;
                    DataRowOrdered["xrCellCurrecency"] = item.Currency;
                    DataRowOrdered["xrCellDate"] = item.InvoiceDate.ToString("dd/MM/yyyy");
                    DataRowOrdered["xrCellDiscount"] = item.Discount.ToString("C4");
                    DataRowOrdered["xrCellSubtotal"] = item.SubTotal.ToString("C4");
                    DataRowOrdered["xrCellIVA"] = item.TotalIVA.ToString("C4");
                    DataRowOrdered["xrCellIEPS"] = item.TotalIEPS.ToString("C4");
                    DataRowOrdered["xrCellISRR"] = item.IsrRet.ToString("C4");
                    DataRowOrdered["xrCellIVAR"] = item.IvaRet.ToString("C4");
                    DataRowOrdered["xrCellImport"] = item.TotalAmount;
                    DataSetTableExpense.Tables[0].Rows.Add(DataRowOrdered);

                    if (item.Currency == "MXN")
                    {
                        ivaValueMxn += item.TotalIVA;
                        iepsValueMxn += item.TotalIEPS;
                        subTotalMxn += item.SubTotal;
                        discountMxn += item.Discount;
                        isrRMxn += item.IsrRet;
                        ivaRMxn += item.IvaRet;
                        ImportValueMxn += item.TotalAmount;
                    }
                    else
                    {
                        ivaValueUsd += item.TotalIVA;
                        iepsValueUsd += item.TotalIEPS;
                        subTotalUsd += item.SubTotal;
                        discountUsd += item.Discount;
                        isrRUsd += item.IsrRet;
                        ivaRUsd += item.IsrRet;
                        ImportValueUsd += item.TotalAmount;
                    }

                }
                xrlblMxnSubtotal.Text = subTotalMxn.ToString("C4");
                xrlblMxnIVA.Text = ivaValueMxn.ToString("C4");
                xrlblMxnIEPS.Text = iepsValueMxn.ToString("C4");
                xrlblMxnDiscount.Text = discountMxn.ToString("C4");
                xrlblMxnISRR.Text = isrRMxn.ToString("C4");
                xrlblMxnIVAR.Text = ivaRMxn.ToString("C4");
                xrlblMxnImport.Text = ImportValueMxn.ToString("C4");

                xrlblUsdSubtotal.Text = subTotalUsd.ToString("C4");
                xrlblUsdIVA.Text = ivaValueUsd.ToString("C4");
                xrlblUsdIEPS.Text = iepsValueUsd.ToString("C4");
                xrlblUsdDiscount.Text = discountUsd.ToString("C4");
                xrlblUsdISRR.Text = isrRUsd.ToString("C4");
                xrlblUsdIVAR.Text = ivaRUsd.ToString("C4");
                xrlblUsdImport.Text = ImportValueUsd.ToString("C4");

                xrCellFolio.DataBindings.Add(new XRBinding("Text", null, "xrTablePurchase.xrCellFolio"));
                xrCellInvoice.DataBindings.Add(new XRBinding("Text", null, "xrTablePurchase.xrCellInvoice"));
                xrCellSupplier.DataBindings.Add(new XRBinding("Text", null, "xrTablePurchase.xrCellSupplier"));
                xrCellCategory.DataBindings.Add(new XRBinding("Text", null, "xrTablePurchase.xrCellCategory"));
                xrCellType.DataBindings.Add(new XRBinding("Text", null, "xrTablePurchase.xrCellType"));
                xrCellCurrecency.DataBindings.Add(new XRBinding("Text", null, "xrTablePurchase.xrCellCurrecency"));
                xrCellDate.DataBindings.Add(new XRBinding("Text", null, "xrTablePurchase.xrCellDate"));
                xrCellSubtotal.DataBindings.Add(new XRBinding("Text", null, "xrTablePurchase.xrCellSubtotal"));
                xrCellIVA.DataBindings.Add(new XRBinding("Text", null, "xrTablePurchase.xrCellIVA"));
                xrCellIEPS.DataBindings.Add(new XRBinding("Text", null, "xrTablePurchase.xrCellIEPS"));
                xrCellDiscount.DataBindings.Add(new XRBinding("Text", null, "xrTablePurchase.xrCellDiscount"));
                xrCellISRR.DataBindings.Add(new XRBinding("Text", null, "xrTablePurchase.xrCellISRR"));
                xrCellIVAR.DataBindings.Add(new XRBinding("Text", null, "xrTablePurchase.xrCellIVAR"));
                xrCellImport.DataBindings.Add(new XRBinding("Text", null, "xrTablePurchase.xrCellImport"));
                return DataSetTableExpense;
            }
            catch (Exception ex)
            {
                var msg = ex.Message;
                return null;
            }
        }

        public string GetStatus(string status)
        {
            if (status == "9")
                return "TERMINADO";
            else if (status == "8")
                return "CANCELADO";
            else if (status == "1")
                return "SOLICITUD DE CANCELACIÓN";
            else if (status == "2")
                return "APROBACIÓN DE CANCELACIÓN";
            else if (status == "3")
                return "PENDIENTE DE CANCELACIÓN";
            else
                return "TODOS LOS ESTATUS";
        }
    }
}
