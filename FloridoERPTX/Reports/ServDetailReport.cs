﻿using System;
using System.Data;
using System.Collections.Generic;
using App.DAL.PurchaseOrder;
using App.Entities;
using DevExpress.XtraReports.UI;
using App.BLL.Supplier;
using App.Entities.ViewModels.PurchaseOrder;
using App.Entities.ViewModels.Sales;

namespace FloridoERPTX.Reports
{
    public partial class ServDetailReport : DevExpress.XtraReports.UI.XtraReport
    {
       
        public DataSet DataSetTableOrdered = null;
        decimal ivaValue, comisionValue,totaComisionlValue,TotalValue,TotalRecargasValue = 0;
        public ServDetailReport()
        {
            InitializeComponent();
          
        }
        public DataSet printTable(serviciosRecargas _ServiciosRecargas, string date1, string date2, string siteName, string company) //se encarga de imprimir la orden del proveedor
        {
            try
            {

                labelDate.Text = DateTime.Now.Date.ToShortDateString();
                labelDate1.Text = date1;
                labelDate2.Text = date2;
                siteLabel.Text = siteName;
                _labelServ.Text = "Servicios: " + company.ToUpper();


                foreach (var item in _ServiciosRecargas.ServicesDetail)
                {
                    var rowHeight = 15f;
                    
                    var row = new XRTableRow();
                    row.HeightF = rowHeight;

                    var cell1 = new XRTableCell(); 
                    cell1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
                    cell1.Text = item.fecha;

                    var cell2 = new XRTableCell();
                    cell2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
                    cell2.Text = item.hora;

                    var cell3 = new XRTableCell();
                    cell3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
                    cell3.Text = item.venta.ToString();

                    var cell4 = new XRTableCell();
                    cell4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
                    cell4.Text = item.comision.ToString("C");


                    var cell5 = new XRTableCell();
                    cell5.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
                    cell5.Text = item.IVAcomision.ToString("C");

                    var cell6 = new XRTableCell();
                    cell6.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
                    cell6.Text = item.TotalComision.ToString("C");

                    var cell7 = new XRTableCell();
                    cell7.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
                    cell7.Text = item.PagoServicio.ToString("C");

                    row.Cells.Add(cell1);
                    row.Cells.Add(cell2);
                    row.Cells.Add(cell3);
                    row.Cells.Add(cell4);
                    row.Cells.Add(cell5);
                    row.Cells.Add(cell6);
                    row.Cells.Add(cell7);

                    row.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
                    _tableServicios.Rows.Add(row);



                    comisionValue += item.comision;
                    ivaValue += item.IVAcomision;                   
                    totaComisionlValue += item.TotalComision;
                    TotalValue += item.PagoServicio;



                }
               
                _sumaIva.Text = ivaValue.ToString("C");
                _sumaTotalComision.Text =  totaComisionlValue.ToString("C");
                _sumaComision.Text =  comisionValue.ToString("C");
                _sumaTotalServicios.Text =  TotalValue.ToString("C");

              


                    return DataSetTableOrdered;
            }
            catch (Exception ex)
            {
                string d=ex.Message;
                throw;
            }
          
        }

       


    }
}
