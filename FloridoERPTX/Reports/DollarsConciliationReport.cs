﻿using System;
using System.Data;
using DevExpress.XtraReports.UI;
using App.Entities.ViewModels.Withdrawal;
using System.Linq;

namespace FloridoERPTX.Reports
{
    public partial class DollarsConciliationReport : DevExpress.XtraReports.UI.XtraReport
    {
       
        public DataSet DataSetTableOrdered = null;        
        public DollarsConciliationReport()
        {
            InitializeComponent();
          
        }
        public DataSet printTable(DollarsConciliationIndex Model) 
        {
            try
            {
                siteLabel.Text = Model.SiteName;
                DataSetTableOrdered = new DataSet();

                DataTable DataTableOrdered = new DataTable();
                DataSetTableOrdered.Tables.Add(DataTableOrdered);
                
                DataTableOrdered.Columns.Add(new DataColumn("Cashier"));
                DataTableOrdered.Columns.Add(new DataColumn("Received"));
                DataTableOrdered.Columns.Add(new DataColumn("Cancelled"));
                DataTableOrdered.Columns.Add(new DataColumn("Withdrawal"));
                DataTableOrdered.Columns.Add(new DataColumn("Difference"));

                foreach (var item in Model.DataReport.OrderBy(c => c.Cashier))
                {
                    DataRow DataRowOrdered = DataSetTableOrdered.Tables[0].NewRow();
                    DataRowOrdered["Cashier"] = item.Cashier;
                    DataRowOrdered["Received"] = item.Received.ToString("C");
                    DataRowOrdered["Cancelled"] = item.Cancelled.ToString("C");
                    DataRowOrdered["Withdrawal"] = item.Withdrawal.ToString("C");
                    DataRowOrdered["Difference"] = item.Difference.ToString("C");

                    DataSetTableOrdered.Tables[0].Rows.Add(DataRowOrdered);
                }
                Date.Text = Model.Date;

                totalReceived.Text = Model.DataReport.Sum(c => c.Received).ToString("C");
                totalCancelled.Text = Model.DataReport.Sum(c => c.Cancelled).ToString("C");
                totalWithdrawal.Text = Model.DataReport.Sum(c => c.Withdrawal).ToString("C");
                totalDifference.Text = Model.DataReport.Sum(c => c.Difference).ToString("C");

                Cashier.DataBindings.Add(new XRBinding("Text", null, "xrTableRowFormat.Cashier"));
                Received.DataBindings.Add(new XRBinding("Text", null, "xrTableRowFormat.Received"));
                Cancelled.DataBindings.Add(new XRBinding("Text", null, "xrTableRowFormat.Cancelled"));
                Withdrawal.DataBindings.Add(new XRBinding("Text", null, "xrTableRowFormat.Withdrawal"));
                Difference.DataBindings.Add(new XRBinding("Text", null, "xrTableRowFormat.Difference"));               
                
                return DataSetTableOrdered;
            }
            catch (Exception ex)
            {
                string d=ex.Message;
                throw;
            }
          
        }       
    }
}
