﻿using System;
using System.Data;
using System.Collections.Generic;
using App.DAL.PurchaseOrder;
using App.Entities;
using DevExpress.XtraReports.UI;
using App.BLL.Supplier;
using App.Entities.ViewModels.PurchaseOrder;
using App.Entities.ViewModels.Sales;
using System.Drawing;

namespace FloridoERPTX.Reports
{
    public partial class ServRecargas : DevExpress.XtraReports.UI.XtraReport
    {
       
        public DataSet DataSetTableOrdered = null;
        decimal ivaValue, comisionValue,totaComisionlValue,TotalValue,TotalRecargasValue = 0;
        public ServRecargas()
        {
            InitializeComponent();
          
        }
        public DataSet printTable(serviciosRecargas _ServiciosRecargas, string date1, string date2, string siteName) //se encarga de imprimir la orden del proveedor
        {
            try
            {

                labelDate.Text = DateTime.Now.Date.ToShortDateString();
                labelDate1.Text = date1;
                labelDate2.Text = date2;
                siteLabel.Text = siteName;


                XRTableRow row;
                foreach (var item in _ServiciosRecargas.Services)
                {
                    var rowHeight = 15f;

                    row = new XRTableRow();
                    row.HeightF = rowHeight;

                    var cell1 = new XRTableCell();
                    cell1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
                    //cell1.Font = new Font(null,FontStyle.Bold);
                    cell1.Font = new Font(cell1.Font.FontFamily, cell1.Font.Size, FontStyle.Bold);
                    cell1.Text = "    " + item.company.ToUpper();



                    row.Cells.Add(cell1);

                    row.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
                    row.BackColor = Color.DarkGray;
                    _tableServicios.Rows.Add(row);

                    //encabezado de detalles
                    row = new XRTableRow();
                    row.HeightF = rowHeight;

                    var cell1e = new XRTableCell();
                    cell1e.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
                    cell1e.Text = "FECHA";
                    var cell2e = new XRTableCell();
                    cell2e.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
                    cell2e.Text = "HORA";

                    var cell3e = new XRTableCell();
                    cell3e.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
                    cell3e.Text = "VENTA";

                    var cell4e = new XRTableCell();
                    cell4e.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
                    cell4e.Text = "COMISIÓN";

                    var cell5e = new XRTableCell();
                    cell5e.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
                    cell5e.Text = "IVA COMISIÓN";

                    var cell6e = new XRTableCell();
                    cell6e.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
                    cell6e.Text = "TOTAL COMISION";


                    var cell7e = new XRTableCell();
                    cell7e.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
                    cell7e.Text = "PAGO SERV";

                    row.Cells.Add(cell1e);
                    row.Cells.Add(cell2e);
                    row.Cells.Add(cell3e);
                    row.Cells.Add(cell4e);
                    row.Cells.Add(cell5e);
                    row.Cells.Add(cell6e);
                    row.Cells.Add(cell7e);
                    row.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
                    row.BackColor = Color.LightGray;
                    _tableServicios.Rows.Add(row);

                    foreach (ServicesModelDetail _serDet in item.ServicesDetail)
                    {
                        var rowHeightd = 15f;

                        var rowd = new XRTableRow();
                        row.HeightF = rowHeightd;

                        var cell1d = new XRTableCell();
                        cell1d.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
                        cell1d.Text = _serDet.fecha;

                        var cell2d = new XRTableCell();
                        cell2d.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
                        cell2d.Text = _serDet.hora;

                        var cell3d = new XRTableCell();
                        cell3d.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
                        cell3d.Text = _serDet.venta.ToString();

                        var cell4d = new XRTableCell();
                        cell4d.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
                        cell4d.Text = _serDet.comision.ToString("C");


                        var cell5d = new XRTableCell();
                        cell5d.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
                        cell5d.Text = _serDet.IVAcomision.ToString("C");

                        var cell6d = new XRTableCell();
                        cell6d.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
                        cell6d.Text = _serDet.TotalComision.ToString("C");

                        var cell7d = new XRTableCell();
                        cell7d.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
                        cell7d.Text = _serDet.PagoServicio.ToString("C");

                        rowd.Cells.Add(cell1d);
                        rowd.Cells.Add(cell2d);
                        rowd.Cells.Add(cell3d);
                        rowd.Cells.Add(cell4d);
                        rowd.Cells.Add(cell5d);
                        rowd.Cells.Add(cell6d);
                        rowd.Cells.Add(cell7d);

                        rowd.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;

                        _tableServicios.Rows.Add(rowd);

                    }
                    row = new XRTableRow();
                    row.HeightF = rowHeight;

                    var cell1t = new XRTableCell();
                    cell1t.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
                    cell1t.Text = "";
                    var cell2t = new XRTableCell();
                    cell2t.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
                    cell2t.Text = "";

                    var cell3t = new XRTableCell();
                    cell3t.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
                    cell3t.Text = "TOTALES";

                    var cell4t = new XRTableCell();
                    cell4t.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
                    cell4t.Text = item.comision.ToString("C");

                    var cell5t = new XRTableCell();
                    cell5t.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
                    cell5t.Text = item.IVAcomision.ToString("C");

                    var cell6t = new XRTableCell();
                    cell6t.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
                    cell6t.Text = item.TotalComision.ToString("C");


                    var cell7t = new XRTableCell();
                    cell7t.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
                    cell7t.Text = item.PagoServicio.ToString("C");

                    row.Cells.Add(cell1t);
                    row.Cells.Add(cell2t);
                    row.Cells.Add(cell3t);
                    row.Cells.Add(cell4t);
                    row.Cells.Add(cell5t);
                    row.Cells.Add(cell6t);
                    row.Cells.Add(cell7t);
                    row.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
                    //row.BackColor = Color.Silver;


                    _tableServicios.Rows.Add(row);

                    row = new XRTableRow();
                    row.HeightF = rowHeight;
                    var cell1ES = new XRTableCell();
                    cell1ES.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
                    cell1ES.Text = "";
                    row.Cells.Add(cell1ES);
                    row.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
                    _tableServicios.Rows.Add(row);
                    comisionValue += item.comision;
                    ivaValue += item.IVAcomision;
                    totaComisionlValue += item.TotalComision;
                    TotalValue += item.PagoServicio;



                }

                _sumaIva.Text = ivaValue.ToString("C");
                _sumaTotalComision.Text =  totaComisionlValue.ToString("C");
                _sumaComision.Text =  comisionValue.ToString("C");
                _sumaTotalServicios.Text =  TotalValue.ToString("C");

                foreach (var item in _ServiciosRecargas.Recargas)
                {
                    var rowHeight = 15f;

                     row = new XRTableRow();
                    row.HeightF = rowHeight;

                    var cell1 = new XRTableCell();
                    cell1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
                    cell1.Font = new Font(cell1.Font.FontFamily, cell1.Font.Size, FontStyle.Bold);
                    cell1.Text = "    " + item.company.ToUpper();

                    row.Cells.Add(cell1);

                    row.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
                    row.BackColor = Color.DarkGray;
                    _tableRecargas.Rows.Add(row);

                    //ENCABEZADO

                    row = new XRTableRow();
                    row.HeightF = rowHeight;

                    var cell1E = new XRTableCell();
                    cell1E.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
                    cell1E.Text = "FECHA";

                    var cell2E = new XRTableCell();
                    cell2E.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
                    cell2E.Text = "HORA";


                    var cell3E = new XRTableCell();
                    cell3E.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
                    cell3E.Text = "VENTA";

                    var cell4E = new XRTableCell();
                    cell4E.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
                    cell4E.Text = "TOTAL";

                    row.Cells.Add(cell1E);
                    row.Cells.Add(cell2E);
                    row.Cells.Add(cell3E);
                    row.Cells.Add(cell4E);

                    row.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
                    row.BackColor = Color.LightGray;
                    _tableRecargas.Rows.Add(row);

                    foreach (RecargasModelDetail _recDet in item.RecargasDetail)
                    {


                        var rowd = new XRTableRow();
                        row.HeightF = rowHeight;

                        var cell1D = new XRTableCell();
                        cell1D.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
                        cell1D.Text = _recDet.fecha;

                        var cell2D = new XRTableCell();
                        cell2D.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
                        cell2D.Text = _recDet.hora;


                        var cell3D = new XRTableCell();
                        cell3D.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
                        cell3D.Text = _recDet.venta.ToString();

                        var cell4D = new XRTableCell();
                        cell4D.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
                        cell4D.Text = _recDet.total.ToString("C");

                        rowd.Cells.Add(cell1D);
                        rowd.Cells.Add(cell2D);
                        rowd.Cells.Add(cell3D);
                        rowd.Cells.Add(cell4D);

                        rowd.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
                        _tableRecargas.Rows.Add(rowd);



                    }
                    row = new XRTableRow();
                    row.HeightF = rowHeight;

                    var cell1t = new XRTableCell();
                    cell1t.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
                    cell1t.Text = "";

                    var cell2t = new XRTableCell();
                    cell2t.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
                    cell2t.Text = "";

                    var cell3t = new XRTableCell();
                    cell3t.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
                    cell3t.Text = "TOTAL";

                    var cell4t = new XRTableCell();
                    cell4t.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
                    cell4t.Text = item.total.ToString("C");


                    row.Cells.Add(cell1t);
                    row.Cells.Add(cell2t);
                    row.Cells.Add(cell3t);
                    row.Cells.Add(cell4t);

                    row.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
                    _tableRecargas.Rows.Add(row);


                    row = new XRTableRow();
                    row.HeightF = rowHeight;
                    var cell1ES = new XRTableCell();
                    cell1ES.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
                    cell1ES.Text = "";
                    row.Cells.Add(cell1ES);

                    row.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
                    _tableRecargas.Rows.Add(row);

                    TotalRecargasValue += item.total;

                }

                _SumaTotalRecargas.Text = TotalRecargasValue.ToString("C");


                    return DataSetTableOrdered;
            }
            catch (Exception ex)
            {
                string d=ex.Message;
                throw;
            }
          
        }

       


    }
}
