﻿using System;
using System.Data;
using System.Collections.Generic;
using App.DAL.PurchaseOrder;
using App.Entities;
using DevExpress.XtraReports.UI;
using App.BLL.Supplier;
using App.Entities.ViewModels.PurchaseOrder;
using App.BLL.BlindCount;

namespace FloridoERPTX.Reports
{
    public partial class PurchasesOrder : DevExpress.XtraReports.UI.XtraReport
    {
        private SupplierBusiness _supplierBusiness;
        public DataSet DataSetTableOrdered = null;
        private BlindCountBusiness _blindCountBusiness;
        decimal ivaValue, iepsValue,totalValue,subTotal = 0;
        public PurchasesOrder()
        {
            InitializeComponent();
            _supplierBusiness = new SupplierBusiness();
            _blindCountBusiness = new BlindCountBusiness();
        }
        public DataSet printTable(List<PurchaseOrderItemListModel> orderDetail, PURCHASE_ORDER order, SITES site_info) //se encarga de imprimir la orden del proveedor
        {
            try
            {
                
                var supplierInfo = _supplierBusiness.GetSupplierInfoById(Convert.ToInt32(order.supplier_id));
                DataSetTableOrdered = new DataSet();

                DataTable DataTableOrdered = new DataTable();
                DataSetTableOrdered.Tables.Add(DataTableOrdered);

                poNoLabel.Text = order.purchase_no;
                dateLabel.Text = order.cdate.HasValue ? order.cdate.Value.ToString("dd/MM/yyyy") : "---";
                dateEndLabel.Text = order.ata != null ? order.ata.ToString() : "N/A";
                xrLabelCurrency.Text = order.currency;
                xraddress.Text = site_info.address + " C.P." + site_info.postal_code;
                xrState.Text = site_info.city + ", " + site_info.state;
                siteLabel.Text = "*" + site_info.site_name;
                var last_update = _blindCountBusiness.GetBlincCountExists(order.purchase_no);

                if (last_update != null)
                {
                    xrlblStartScan.Visible = true;
                    xrlblStartScanInput.Visible = true;
                    xrlblLastScan.Visible = true;
                    xrlblLastScanInput.Visible = true;

                    xrlblStartScan.Text = order.purchase_date.HasValue ? order.purchase_date.Value.ToString("dd/MM/yyyy") : "---";
                    xrlblLastScan.Text = last_update.Value.ToString("dd/MM/yyyy hh:mm tt");
                }
                factSupLabel.Text = order.invoice_no !=null ? order.invoice_no.ToUpper() : "";
                supNoLabel.Text = supplierInfo.Rfc.ToUpper();
                supNoRealLabel.Text = order.supplier_id.ToString();
                empLabel.Text = order.cuser.ToUpper() ?? "";
                supNameLabel.Text = supplierInfo.BusinessName;
                supAddrsLabel.Text = supplierInfo.SupplierAddress + ", CP:" + supplierInfo.ZipCode;
                commentsLabel.Text = order.purchase_remark;

                if (supplierInfo.supplier_contact_id == 0)
                {
                    supTelLabel.Text = "Sin Contacto";
                    supEmailLabel.Text = "Sin Contacto";
                    supDeparLabel.Text = "Sin Contacto";
                }
                else
                {
                    supTelLabel.Text = supplierInfo.phone.ToUpper();
                    supEmailLabel.Text = supplierInfo.email.ToUpper();
                    supDeparLabel.Text = supplierInfo.departament.ToUpper();
                }    
                DataTableOrdered.Columns.Add(new DataColumn("prod_id"));
                DataTableOrdered.Columns.Add(new DataColumn("prod_name"));
                DataTableOrdered.Columns.Add(new DataColumn("quantity"));
                //DataTableOrdered.Columns.Add(new DataColumn("grQuantity"));
                DataTableOrdered.Columns.Add(new DataColumn("um"));
                DataTableOrdered.Columns.Add(new DataColumn("totUnits"));
                //DataTableOrdered.Columns.Add(new DataColumn("discount"));
                DataTableOrdered.Columns.Add(new DataColumn("unitPrice"));
                //DataTableOrdered.Columns.Add(new DataColumn("moqPrice"));
                DataTableOrdered.Columns.Add(new DataColumn("import"));
                DataTableOrdered.Columns.Add(new DataColumn("ieps"));
                DataTableOrdered.Columns.Add(new DataColumn("iva"));
                DataTableOrdered.Columns.Add(new DataColumn("subTotal"));
               
                foreach (var item in orderDetail)
                {
                    DataRow DataRowOrdered = DataSetTableOrdered.Tables[0].NewRow();
                    DataRowOrdered["prod_id"] = item.PartNumber;
                    DataRowOrdered["prod_name"] = item.Description;
                    DataRowOrdered["quantity"] = item.Parcking != 0 ? (item.Quantity / item.Parcking) : 0 ;
                    //DataRowOrdered["grQuantity"] = item.Parcking != 0 ? (item.grQuantity/item.Parcking) : 0;
                    DataRowOrdered["um"] = item.UnitSize;
                    DataRowOrdered["totUnits"] = item.grQuantity;
                    //DataRowOrdered["discount"] = "sin Definir";
                    DataRowOrdered["unitPrice"] = "$" + item.PurchasePrice;
                    //DataRowOrdered["moqPrice"] = "$" + item.PurchasePrice * item.Parcking;
                    DataRowOrdered["import"] = "$" + item.ItemAmount;
                    DataRowOrdered["ieps"] = "$" + item.Ieps;
                    DataRowOrdered["iva"] = "$" + item.Iva;
                    DataRowOrdered["subTotal"] = "$" + (item.ItemTotalAmount >0 ? item.ItemTotalAmount.ToString(): item.ItemAmount.ToString());
                    DataSetTableOrdered.Tables[0].Rows.Add(DataRowOrdered);
                    ivaValue += item.Iva;
                    iepsValue += item.Ieps;
                    totalValue += item.ItemTotalAmount > 0 ? item.ItemTotalAmount : item.ItemAmount;
                    subTotal += item.ItemAmount;
                }
                ivaLabel.Text = "$"+ivaValue.ToString();
                iepsLabel.Text = "$" + iepsValue.ToString();
                subtotalLabel.Text = "$" + subTotal.ToString();
                totalLabel.Text = "$" + totalValue.ToString();
                prod_id.DataBindings.Add(new XRBinding("Text", null, "xrTableRowFormat.prod_id"));
                prod_name.DataBindings.Add(new XRBinding("Text", null, "xrTableRowFormat.prod_name"));
                quantity.DataBindings.Add(new XRBinding("Text", null, "xrTableRowFormat.quantity"));
                //gr_quantity.DataBindings.Add(new XRBinding("Text", null, "xrTableRowFormat.grQuantity"));
                um.DataBindings.Add(new XRBinding("Text", null, "xrTableRowFormat.um"));
                tot_units.DataBindings.Add(new XRBinding("Text", null, "xrTableRowFormat.totUnits"));
                //discount.DataBindings.Add(new XRBinding("Text", null, "xrTableRowFormat.discount"));
                unit_price.DataBindings.Add(new XRBinding("Text", null, "xrTableRowFormat.unitPrice"));
                //moq_price.DataBindings.Add(new XRBinding("Text", null, "xrTableRowFormat.moqPrice"));
                import.DataBindings.Add(new XRBinding("Text", null, "xrTableRowFormat.import"));
                ieps.DataBindings.Add(new XRBinding("Text", null, "xrTableRowFormat.ieps"));
                iva.DataBindings.Add(new XRBinding("Text", null, "xrTableRowFormat.iva"));
                subtotal.DataBindings.Add(new XRBinding("Text", null, "xrTableRowFormat.subTotal"));


                return DataSetTableOrdered;
            }
            catch (Exception ex)
            {
                string d=ex.Message;
                throw;
            }
          
        }

        internal object printTable(object p, List<PurchaseOrderItemListModel> list)
        {
            throw new NotImplementedException();
        }


    }
}
