﻿using System;
using System.Data;
using System.Collections.Generic;
using App.DAL.PurchaseOrder;
using App.Entities;
using DevExpress.XtraReports.UI;
using App.BLL.Supplier;
using App.Entities.ViewModels.PurchaseOrder;
using App.Entities.ViewModels.Sales;
using App.Entities.ViewModels.Sanctions;

namespace FloridoERPTX.Reports
{
    public partial class SanctionReport : DevExpress.XtraReports.UI.XtraReport
    {
       
        public DataSet DataSetTableOrdered = null;
        decimal subTotal = 0;
        public SanctionReport()
        {
            InitializeComponent();
          
        }
        public DataSet printTable(List<SanctionModel> sancsList, string date1, string date2, string siteName) //se encarga de imprimir la orden del proveedor
        {
            try
            {

                labelDate.Text = DateTime.Now.Date.ToShortDateString();
                labelDate1.Text = date1;
                labelDate2.Text = date2;
                siteLabel.Text = siteName;
                DataSetTableOrdered = new DataSet();

                DataTable DataTableOrdered = new DataTable();
                DataSetTableOrdered.Tables.Add(DataTableOrdered);

             


                DataTableOrdered.Columns.Add(new DataColumn("SanctionId"));
                DataTableOrdered.Columns.Add(new DataColumn("CreateDate"));
                DataTableOrdered.Columns.Add(new DataColumn("emp"));
                DataTableOrdered.Columns.Add(new DataColumn("total"));
                DataTableOrdered.Columns.Add(new DataColumn("currencyValue"));
                DataTableOrdered.Columns.Add(new DataColumn("Motive"));
                DataTableOrdered.Columns.Add(new DataColumn("statusVal"));
                DataTableOrdered.Columns.Add(new DataColumn("Comments"));





                foreach (var item in sancsList)
                {
                    DataRow DataRowOrdered = DataSetTableOrdered.Tables[0].NewRow();
                    DataRowOrdered["SanctionId"] = item.SanctionId;
                    DataRowOrdered["CreateDate"] = item.CreateDate;
                    DataRowOrdered["emp"] = item.emp;
                    DataRowOrdered["total"] =  item.total;
                    DataRowOrdered["currencyValue"] = item.currencyValue;
                    DataRowOrdered["Motive"] =  item.Motive;
                    DataRowOrdered["statusVal"] =  item.statusVal;
                    DataRowOrdered["Comments"] = item.Comments;
                    
                   

                    DataSetTableOrdered.Tables[0].Rows.Add(DataRowOrdered);
                  
                    subTotal += decimal.Parse(item.total);
                   



                }
               
               
                totalLabel.Text = "$" + subTotal.ToString("0.00");
                SanctionId.DataBindings.Add(new XRBinding("Text", null, "xrTableRowFormat.SanctionId"));
                CreateDate.DataBindings.Add(new XRBinding("Text", null, "xrTableRowFormat.CreateDate"));
                emp.DataBindings.Add(new XRBinding("Text", null, "xrTableRowFormat.emp"));
                statusVal.DataBindings.Add(new XRBinding("Text", null, "xrTableRowFormat.statusVal"));
                currencyValue.DataBindings.Add(new XRBinding("Text", null, "xrTableRowFormat.currencyValue"));
                Motive.DataBindings.Add(new XRBinding("Text", null, "xrTableRowFormat.Motive"));
                Coments.DataBindings.Add(new XRBinding("Text", null, "xrTableRowFormat.Comments"));
                total.DataBindings.Add(new XRBinding("Text", null, "xrTableRowFormat.total"));
              
               


                return DataSetTableOrdered;
            }
            catch (Exception ex)
            {
                string d=ex.Message;
                throw;
            }
          
        }

       


    }
}
