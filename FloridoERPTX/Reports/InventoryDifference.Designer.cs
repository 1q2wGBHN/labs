﻿namespace FloridoERPTX.Reports
{
    partial class InventoryDifference
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Detail = new DevExpress.XtraReports.UI.DetailBand();
            this.TopMargin = new DevExpress.XtraReports.UI.TopMarginBand();
            this.BottomMargin = new DevExpress.XtraReports.UI.BottomMarginBand();
            this.DetailReport = new DevExpress.XtraReports.UI.DetailReportBand();
            this.Detail1 = new DevExpress.XtraReports.UI.DetailBand();
            this.xrTableInventory = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow2 = new DevExpress.XtraReports.UI.XRTableRow();
            this.cellPartNumber = new DevExpress.XtraReports.UI.XRTableCell();
            this.cellPartDescription = new DevExpress.XtraReports.UI.XRTableCell();
            this.cellSupplier = new DevExpress.XtraReports.UI.XRTableCell();
            this.cellDepartment = new DevExpress.XtraReports.UI.XRTableCell();
            this.cellScrap = new DevExpress.XtraReports.UI.XRTableCell();
            this.cellStock = new DevExpress.XtraReports.UI.XRTableCell();
            this.cellCounted = new DevExpress.XtraReports.UI.XRTableCell();
            this.cellDifference = new DevExpress.XtraReports.UI.XRTableCell();
            this.cellAdjustment = new DevExpress.XtraReports.UI.XRTableCell();
            this.cellUnitPrice = new DevExpress.XtraReports.UI.XRTableCell();
            this.cellDifScrap = new DevExpress.XtraReports.UI.XRTableCell();
            this.cellAmountScrap = new DevExpress.XtraReports.UI.XRTableCell();
            this.cellAmountDifference = new DevExpress.XtraReports.UI.XRTableCell();
            this.cellDifScrapAmount = new DevExpress.XtraReports.UI.XRTableCell();
            this.formattingRule1 = new DevExpress.XtraReports.UI.FormattingRule();
            ((System.ComponentModel.ISupportInitialize)(this.xrTableInventory)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // Detail
            // 
            this.Detail.Dpi = 100F;
            this.Detail.Expanded = false;
            this.Detail.HeightF = 100F;
            this.Detail.Name = "Detail";
            this.Detail.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.Detail.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // TopMargin
            // 
            this.TopMargin.Dpi = 100F;
            this.TopMargin.HeightF = 16F;
            this.TopMargin.Name = "TopMargin";
            this.TopMargin.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.TopMargin.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // BottomMargin
            // 
            this.BottomMargin.Dpi = 100F;
            this.BottomMargin.HeightF = 21F;
            this.BottomMargin.Name = "BottomMargin";
            this.BottomMargin.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.BottomMargin.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // DetailReport
            // 
            this.DetailReport.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.Detail1});
            this.DetailReport.Dpi = 100F;
            this.DetailReport.Level = 0;
            this.DetailReport.Name = "DetailReport";
            // 
            // Detail1
            // 
            this.Detail1.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTableInventory});
            this.Detail1.Dpi = 100F;
            this.Detail1.HeightF = 35F;
            this.Detail1.Name = "Detail1";
            // 
            // xrTableInventory
            // 
            this.xrTableInventory.Dpi = 100F;
            this.xrTableInventory.LocationFloat = new DevExpress.Utils.PointFloat(0F, 10.00001F);
            this.xrTableInventory.Name = "xrTableInventory";
            this.xrTableInventory.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow2});
            this.xrTableInventory.SizeF = new System.Drawing.SizeF(1086F, 24.99999F);
            // 
            // xrTableRow2
            // 
            this.xrTableRow2.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.cellPartNumber,
            this.cellPartDescription,
            this.cellSupplier,
            this.cellDepartment,
            this.cellScrap,
            this.cellStock,
            this.cellCounted,
            this.cellDifference,
            this.cellAdjustment,
            this.cellUnitPrice,
            this.cellDifScrap,
            this.cellAmountScrap,
            this.cellAmountDifference,
            this.cellDifScrapAmount});
            this.xrTableRow2.Dpi = 100F;
            this.xrTableRow2.Name = "xrTableRow2";
            this.xrTableRow2.Weight = 1D;
            // 
            // cellPartNumber
            // 
            this.cellPartNumber.Dpi = 100F;
            this.cellPartNumber.Name = "cellPartNumber";
            this.cellPartNumber.Weight = 0.17426062476142457D;
            // 
            // cellPartDescription
            // 
            this.cellPartDescription.Dpi = 100F;
            this.cellPartDescription.Name = "cellPartDescription";
            this.cellPartDescription.Weight = 0.25559111217718522D;
            // 
            // cellSupplier
            // 
            this.cellSupplier.Dpi = 100F;
            this.cellSupplier.Name = "cellSupplier";
            this.cellSupplier.Text = "cellSupplier";
            this.cellSupplier.Weight = 0.189818083724646D;
            // 
            // cellDepartment
            // 
            this.cellDepartment.Dpi = 100F;
            this.cellDepartment.Name = "cellDepartment";
            this.cellDepartment.Text = "cellDepartment";
            this.cellDepartment.Weight = 0.1642528266848082D;
            // 
            // cellScrap
            // 
            this.cellScrap.Dpi = 100F;
            this.cellScrap.Name = "cellScrap";
            this.cellScrap.Text = "cellScrap";
            this.cellScrap.Weight = 0.16925421192268819D;
            // 
            // cellStock
            // 
            this.cellStock.Dpi = 100F;
            this.cellStock.Name = "cellStock";
            this.cellStock.Text = "cellStock";
            this.cellStock.Weight = 0.15986090352190277D;
            // 
            // cellCounted
            // 
            this.cellCounted.Dpi = 100F;
            this.cellCounted.Name = "cellCounted";
            this.cellCounted.Text = "cellCounted";
            this.cellCounted.Weight = 0.20534027112543596D;
            // 
            // cellDifference
            // 
            this.cellDifference.Dpi = 100F;
            this.cellDifference.Name = "cellDifference";
            this.cellDifference.Text = "cellDifference";
            this.cellDifference.Weight = 0.1811333874979551D;
            // 
            // cellAdjustment
            // 
            this.cellAdjustment.Dpi = 100F;
            this.cellAdjustment.Name = "cellAdjustment";
            this.cellAdjustment.Text = "cellAdjustment";
            this.cellAdjustment.Weight = 0.16371855515677702D;
            // 
            // cellUnitPrice
            // 
            this.cellUnitPrice.Dpi = 100F;
            this.cellUnitPrice.Name = "cellUnitPrice";
            this.cellUnitPrice.Text = "cellUnitPrice";
            this.cellUnitPrice.Weight = 0.13714209894790502D;
            // 
            // cellDifScrap
            // 
            this.cellDifScrap.Dpi = 100F;
            this.cellDifScrap.Name = "cellDifScrap";
            this.cellDifScrap.Text = "cellDifScrap";
            this.cellDifScrap.Weight = 0.12779518796675926D;
            // 
            // cellAmountScrap
            // 
            this.cellAmountScrap.Dpi = 100F;
            this.cellAmountScrap.Name = "cellAmountScrap";
            this.cellAmountScrap.Text = "cellAmountScrap";
            this.cellAmountScrap.Weight = 0.14441866517692267D;
            // 
            // cellAmountDifference
            // 
            this.cellAmountDifference.Dpi = 100F;
            this.cellAmountDifference.Name = "cellAmountDifference";
            this.cellAmountDifference.Text = "cellAmountDifference";
            this.cellAmountDifference.Weight = 0.12545591813020621D;
            // 
            // cellDifScrapAmount
            // 
            this.cellDifScrapAmount.Dpi = 100F;
            this.cellDifScrapAmount.Name = "cellDifScrapAmount";
            this.cellDifScrapAmount.Text = "cellDifScrapAmount";
            this.cellDifScrapAmount.Weight = 0.21011641464334063D;
            // 
            // formattingRule1
            // 
            this.formattingRule1.Name = "formattingRule1";
            // 
            // InventoryDifference
            // 
            this.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.Detail,
            this.TopMargin,
            this.BottomMargin,
            this.DetailReport});
            this.FormattingRuleSheet.AddRange(new DevExpress.XtraReports.UI.FormattingRule[] {
            this.formattingRule1});
            this.Landscape = true;
            this.Margins = new System.Drawing.Printing.Margins(10, 4, 16, 21);
            this.PageHeight = 850;
            this.PageWidth = 1100;
            this.Version = "16.2";
            ((System.ComponentModel.ISupportInitialize)(this.xrTableInventory)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

        }

        #endregion

        private DevExpress.XtraReports.UI.DetailBand Detail;
        private DevExpress.XtraReports.UI.TopMarginBand TopMargin;
        private DevExpress.XtraReports.UI.BottomMarginBand BottomMargin;
        private DevExpress.XtraReports.UI.DetailReportBand DetailReport;
        private DevExpress.XtraReports.UI.DetailBand Detail1;
        private DevExpress.XtraReports.UI.XRTable xrTableInventory;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow2;
        private DevExpress.XtraReports.UI.XRTableCell cellPartNumber;
        private DevExpress.XtraReports.UI.XRTableCell cellPartDescription;
        private DevExpress.XtraReports.UI.XRTableCell cellScrap;
        private DevExpress.XtraReports.UI.XRTableCell cellStock;
        private DevExpress.XtraReports.UI.XRTableCell cellCounted;
        private DevExpress.XtraReports.UI.XRTableCell cellDifference;
        private DevExpress.XtraReports.UI.XRTableCell cellAdjustment;
        private DevExpress.XtraReports.UI.XRTableCell cellUnitPrice;
        private DevExpress.XtraReports.UI.XRTableCell cellAmountScrap;
        private DevExpress.XtraReports.UI.FormattingRule formattingRule1;
        private DevExpress.XtraReports.UI.XRTableCell cellSupplier;
        private DevExpress.XtraReports.UI.XRTableCell cellDepartment;
        private DevExpress.XtraReports.UI.XRTableCell cellAmountDifference;
        private DevExpress.XtraReports.UI.XRTableCell cellDifScrap;
        private DevExpress.XtraReports.UI.XRTableCell cellDifScrapAmount;
    }
}