﻿using System;
using System.Data;
using System.Collections.Generic;
using App.DAL.PurchaseOrder;
using App.Entities;
using DevExpress.XtraReports.UI;
using App.BLL.Supplier;
using App.Entities.ViewModels.PurchaseOrder;
using App.Entities.ViewModels.Sales;
using App.Entities.ViewModels.Sanctions;
using System.Drawing;

namespace FloridoERPTX.Reports
{
    public partial class Portada : DevExpress.XtraReports.UI.XtraReport
    {

        public DataSet DataSetTableOrdered = null;
        decimal subTotal = 0;
        public Portada()
        {
            InitializeComponent();

        }
        public DataSet printTable(PortadaModel model, string siteName) //se encarga de imprimir la orden del proveedor
        {
            try
            {
                siteLabel.Text = siteName;

                _LprintDate.Text = DateTime.Now.Date.ToShortDateString();

                labelDate.Text = model.ReportDate;
                _Ldiferencia.Text = model.Difference.ToString("C");
                _LTipoCambio.Text = model.Currency_Rate.ToString("C");

                _MN00.Text = model.TotalesMXN[0, 0].ToString("C"); _MN01.Text = model.TotalesMXN[0, 1].ToString("C"); _MN02.Text = model.TotalesMXN[0, 2].ToString("C");
                _MN10.Text = model.TotalesMXN[1, 0].ToString("C"); _MN11.Text = model.TotalesMXN[1, 1].ToString("C"); _MN12.Text = model.TotalesMXN[1, 2].ToString("C");

                _mnEXEcre.Text = model.TotalesMXN[2, 0].ToString("C"); _mnEXEcash.Text = model.TotalesMXN[2, 1].ToString("C"); _mnEXEtotal.Text = model.TotalesMXN[2, 2].ToString("C");

                _MN20.Text = model.TotalesMXN[3, 0].ToString("C"); _MN21.Text = model.TotalesMXN[3, 1].ToString("C"); _MN22.Text = model.TotalesMXN[3, 2].ToString("C");
                _MN30.Text = model.TotalesMXN[4, 0].ToString("C"); _MN31.Text = model.TotalesMXN[4, 1].ToString("C"); _MN32.Text = model.TotalesMXN[4, 2].ToString("C");
                _MN40.Text = model.TotalesMXN[5, 0].ToString("C"); _MN41.Text = model.TotalesMXN[5, 1].ToString("C"); _MN42.Text = model.TotalesMXN[5, 2].ToString("C");
                _MN50.Text = model.TotalesMXN[6, 0].ToString("C"); _MN51.Text = model.TotalesMXN[6, 1].ToString("C"); _MN52.Text = model.TotalesMXN[6, 2].ToString("C");



                _USD00.Text = model.TotalesDLLS[0, 0].ToString("C"); _USD01.Text = model.TotalesDLLS[0, 1].ToString("C"); _USD02.Text = model.TotalesDLLS[0, 2].ToString("C");
                _USD10.Text = model.TotalesDLLS[1, 0].ToString("C"); _USD11.Text = model.TotalesDLLS[1, 1].ToString("C"); _USD12.Text = model.TotalesDLLS[1, 2].ToString("C");

                _dllsEXEcre.Text = model.TotalesDLLS[2, 0].ToString("C"); _dllsEXEcash.Text = model.TotalesDLLS[2, 1].ToString("C"); _dllsEXEtotal.Text = model.TotalesDLLS[2, 2].ToString("C");

                _USD20.Text = model.TotalesDLLS[3, 0].ToString("C"); _USD21.Text = model.TotalesDLLS[3, 1].ToString("C"); _USD22.Text = model.TotalesDLLS[3, 2].ToString("C");
                _USD30.Text = model.TotalesDLLS[4, 0].ToString("C"); _USD31.Text = model.TotalesDLLS[4, 1].ToString("C"); _USD32.Text = model.TotalesDLLS[4, 2].ToString("C");
                _USD40.Text = model.TotalesDLLS[5, 0].ToString("C"); _USD41.Text = model.TotalesDLLS[5, 1].ToString("C"); _USD42.Text = model.TotalesDLLS[5, 2].ToString("C");
                _USD50.Text = model.TotalesDLLS[6, 0].ToString("C"); _USD51.Text = model.TotalesDLLS[6, 1].ToString("C"); _USD52.Text = model.TotalesDLLS[6, 2].ToString("C");

                _LTotalTasa0.Text = model.totalTasao.ToString("C");
                _LTotalGravado.Text = model.totalGravado0.ToString("C");
                _LTotalIEPS0.Text = model.totalIEPS0.ToString("C");
                _LTotalIEPS.Text = model.totalIEPS.ToString("C");
                _LTotalIVA.Text = model.totalIVA.ToString("C");
                _LTotalTOTAL.Text = model.totalTOTAL.ToString("C");
                _EXEcreGranTotal.Text = model.totalExento.ToString("C");



                _DONMXN.Text = model.DON_MXN.ToString("C");
                _DONDLLS.Text = model.DON_DLLS.ToString("C");

                _CANelect.Text = model.CANC_ELECT.ToString("C");
                _CANefectivo.Text = model.CANC_CASH.ToString("C");

                _NCMXN.Text = model.CreditNotesMXN.ToString("C");
                _NCDLLS.Text = model.CreditNotesDLLS.ToString("C");

                _BONMXN.Text = model.BONIMXN.ToString("C");
                _BONDLLS.Text = model.BONIDLLS.ToString("C");

                _DESCMXN.Text = model.DESCMXN.ToString("C");
                _DESCDLLS.Text = model.DESCDLLS.ToString("C");

                //total de depositos
                _TDmxn.Text = model.MXNDeposits.ToString("C");
                _TDdllsmxn.Text = model.DLLSDeposits.ToString("C");
                _TDcards.Text = model.CARDSDeposits.ToString("C");
                _TDvales.Text = model.VALESDeposits.ToString("C");
                _TDvalesElec.Text = model.VALES_ELECT_Deposits.ToString("C");
                _TDtotal.Text = model.TotalDeposits.ToString("C");

                //pagos y abonos
                _PAYSmxn.Text = model.PAYSCUSTMXN.ToString("C");
                _PAYSdlls.Text = model.PAYSCUSTDLLS.ToString("C");
                _PAYSdllsMxn.Text = model.PAYSCUSTDLLSMXN.ToString("C");
                _PAYStotal.Text = model.PAYSCUSTTOTAL.ToString("C");

                //renta
                _RENTAdlls.Text = model.RENTA_DLLS.ToString("C");
                _RENTAiva.Text = model.RENTA_IVA.ToString("C");
                _RENTAtotalDlls.Text = model.RENTA_TOTAL.ToString("C");
                _RENTAtotalDllsMxn.Text = model.RENTA_MXN.ToString("C");

                //COMISIONES, PENDIENTE
                _CASHBACK.Text = model.CASHBACK_TOTAL.ToString("C");
                DataSetTableOrdered = new DataSet();

                DataTable DataTableOrdered = new DataTable();

                DataSetTableOrdered.Tables.Add(DataTableOrdered);





                DataTableOrdered.Columns.Add(new DataColumn("_DepMXNcajas"));
                DataTableOrdered.Columns.Add(new DataColumn("_DepMXNCajaTipo"));

                //DataTableOrderedCC.Columns.Add(new DataColumn("_DepMXNcc"));
                //DataTableOrderedCC.Columns.Add(new DataColumn("_DepTipoCc"));

                //DataTableOrderedCajasUSD.Columns.Add(new DataColumn("_DepDLLScajas"));
                //DataTableOrderedCajasUSD.Columns.Add(new DataColumn("_DepDLLSCajasTipo"));
                //DataTableOrderedCajasUSD.Columns.Add(new DataColumn("_DepDLLSMXNCajas"));

                //DataTableOrderedCCUSD.Columns.Add(new DataColumn("_DepDLLScc"));
                //DataTableOrderedCCUSD.Columns.Add(new DataColumn("_DepDLLSCcTipo"));
                //DataTableOrderedCCUSD.Columns.Add(new DataColumn("_DepDLLSMXNCc"));




                if (model.Deposits != null)
                {
                    foreach (var item in model.Deposits)
                    {
                        if (item.Destino == "C" && item.Currency == "MXN")
                        {
                            //DataRow DataRowOrderedPesosCajas = DataSetTableOrdered.Tables[0].NewRow();
                            //DataRowOrderedPesosCajas["_DepMXNcajas"] = item.Importe;
                            //DataRowOrderedPesosCajas["_DepMXNCajaTipo"] = item.Type;
                            //DataSetTableOrdered.Tables[0].Rows.Add(DataRowOrderedPesosCajas);


                            var rowHeight = 15f;

                            var row = new XRTableRow();
                            row.HeightF = rowHeight;

                            var cell = new XRTableCell();
                            cell.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
                            cell.Text = item.Importe.ToString("C");

                            var cell1 = new XRTableCell();
                            cell1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
                            cell1.Text = item.Type;


                            row.Cells.Add(cell);
                            row.Cells.Add(cell1);
                            row.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
                            xrTable1.Rows.Add(row);

                        }

                        if (item.Destino == "CC" && item.Currency == "MXN")
                        {



                            var rowHeight = 15f;

                            var row = new XRTableRow();
                            row.HeightF = rowHeight;

                            var cell = new XRTableCell();
                            cell.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
                            cell.Text = item.Importe.ToString("C");

                            var cell1 = new XRTableCell();
                            cell1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
                            cell1.Text = item.Type;


                            row.Cells.Add(cell);
                            row.Cells.Add(cell1);
                            row.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
                            xrTable2.Rows.Add(row);

                        }

                        if (item.Destino == "C" && item.Currency == "USD")
                        {



                            var rowHeight = 15f;

                            var row = new XRTableRow();
                            row.HeightF = rowHeight;

                            var cell = new XRTableCell();
                            cell.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
                            cell.Text = item.ImporteDLLS.ToString("C");

                            var cell1 = new XRTableCell();
                            cell1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
                            cell1.Text = item.Type;

                            var cell2 = new XRTableCell();
                            cell2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
                            cell2.Text = item.Importe_DLLS_MXN.ToString("C");

                            row.Cells.Add(cell);
                            row.Cells.Add(cell1);
                            row.Cells.Add(cell2);
                            row.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
                            xrTable3.Rows.Add(row);


                        }
                        if (item.Destino == "CC" && item.Currency == "USD")
                        {

                            var rowHeight = 15f;

                            var row = new XRTableRow();
                            row.HeightF = rowHeight;

                            var cell = new XRTableCell();
                            cell.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
                            cell.Text = item.ImporteDLLS.ToString("C");

                            var cell1 = new XRTableCell();
                            cell1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
                            cell1.Text = item.Type;
                            var cell2 = new XRTableCell();
                            cell2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
                            cell2.Text = item.Importe_DLLS_MXN.ToString("C");

                            row.Cells.Add(cell);
                            row.Cells.Add(cell1);
                            row.Cells.Add(cell2);
                            row.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
                            xrTable4.Rows.Add(row);





                        }

                    }
                }


                decimal TotalSanc = 0;
                decimal TotalDesc = 0;
                decimal TotalIEPS = 0;
                decimal TotalServ = 0;
                decimal TotalTipoPago = 0;
                if (model.Sanctions != null)
                {  var rowHeight = 15f;
                    foreach (var item in model.Sanctions)
                    {
                        TotalSanc += item.Importe;
                      
                        

                        var row = new XRTableRow();
                        row.HeightF = rowHeight;

                        var cell = new XRTableCell();
                        cell.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
                        cell.Text = item.Importe.ToString("C");
                        cell.WidthF = 59F;

                        var cell1 = new XRTableCell();
                        cell1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
                        cell1.Text = item.Empleado;
                        cell1.WidthF = 100F;
                        var cell2 = new XRTableCell();
                        cell2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
                        cell2.Text = item.Motivo;
                        cell2.WidthF = 73F;
                        row.Cells.Add(cell1);
                        row.Cells.Add(cell2);
                        row.Cells.Add(cell);
                        row.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
                        _TableSanctions.Rows.Add(row);
                    }
                 


                    var rowt = new XRTableRow();
                    rowt.HeightF = rowHeight;

                    var cellt = new XRTableCell();
                    cellt.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
                    cellt.Text = "";
                    cellt.WidthF = 100F;

                    var cell1t = new XRTableCell();
                    cell1t.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
                    cell1t.Font = new Font(cell1t.Font.FontFamily, cell1t.Font.Size, FontStyle.Bold);
                    cell1t.Text = "TOTAL";
                    cell1t.WidthF = 73F;
                    var cell2t = new XRTableCell();
                    cell2t.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
                    cell2t.Font = new Font(cell2t.Font.FontFamily, cell2t.Font.Size, FontStyle.Bold);
                    cell2t.Text = TotalSanc.ToString("C");
                    cell2t.WidthF = 59F;
                    rowt.Cells.Add(cellt);
                    rowt.Cells.Add(cell1t);
                    rowt.Cells.Add(cell2t);
                    rowt.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
                    _TableSanctions.Rows.Add(rowt);
                }
                if (model.SaleDiscountslList != null)
                {
                    var rowHeight = 15f;
                    foreach (var item in model.SaleDiscountslList)
                    {

                        

                        var row = new XRTableRow();
                        row.HeightF = rowHeight;

                        var cell = new XRTableCell();
                        cell.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
                        cell.Text = item.Importe.ToString("C");
                        TotalDesc += item.Importe;
                        var cell1 = new XRTableCell();
                        cell1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
                        cell1.Text = item.Customername;


                        row.Cells.Add(cell1);
                        row.Cells.Add(cell);
                        row.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
                        _TablesDesc.Rows.Add(row);
                    }
                    var rowt = new XRTableRow();
                    rowt.HeightF = rowHeight;

                    var cellt = new XRTableCell();
                    cellt.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
                    cellt.Font = new Font(cellt.Font.FontFamily, cellt.Font.Size, FontStyle.Bold);
                    cellt.Text = "TOTAL";
                   
                    var cell1t = new XRTableCell();
                    cell1t.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
                    cell1t.Font = new Font(cell1t.Font.FontFamily, cell1t.Font.Size, FontStyle.Bold);
                    cell1t.Text = TotalDesc.ToString("C");


                    rowt.Cells.Add(cellt);
                    rowt.Cells.Add(cell1t);
                    rowt.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
                    _TablesDesc.Rows.Add(rowt);
                }

                if (model.ServicesDetailList != null)
                { 
                    var rowHeight = 15f;
                    foreach (var item in model.ServicesDetailList)
                    {

                       

                        var row = new XRTableRow();
                        row.HeightF = rowHeight;

                        var cell = new XRTableCell();
                        cell.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
                        cell.Text = item.Importe.ToString("C");
                        TotalServ += item.Importe;
                        var cell1 = new XRTableCell();
                        cell1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
                        cell1.Text = item.ServiceName;


                        row.Cells.Add(cell1);
                        row.Cells.Add(cell);
                        row.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
                        _TableServ.Rows.Add(row);
                    }
                    var rowt = new XRTableRow();
                    rowt.HeightF = rowHeight;

                    var cellt = new XRTableCell();
                    cellt.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
                    cellt.Font = new Font(cellt.Font.FontFamily, cellt.Font.Size, FontStyle.Bold);
                    cellt.Text = "TOTAL";

                    var cell1t = new XRTableCell();
                    cell1t.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
                    cell1t.Font = new Font(cell1t.Font.FontFamily, cell1t.Font.Size, FontStyle.Bold);
                    cell1t.Text = TotalServ.ToString("C");


                    rowt.Cells.Add(cellt);
                    rowt.Cells.Add(cell1t);
                    rowt.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
                    _TableServ.Rows.Add(rowt);
                }
                if (model.IEPS_DetaillList != null)
                {
                    var rowHeight = 15f;
                    foreach (var item in model.IEPS_DetaillList)
                    {

                     

                        var row = new XRTableRow();
                        row.HeightF = rowHeight;

                        var cell = new XRTableCell();
                        cell.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
                        cell.Text = item.Importe.ToString("C");
                        TotalIEPS += item.Importe;
                        var cell1 = new XRTableCell();
                        cell1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
                        cell1.Text = item.IEPS;


                        row.Cells.Add(cell1);
                        row.Cells.Add(cell);
                        row.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
                        _TableIEPS.Rows.Add(row);
                    }

                    var rowt = new XRTableRow();
                    rowt.HeightF = rowHeight;

                    var cellt = new XRTableCell();
                    cellt.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
                    cellt.Font = new Font(cellt.Font.FontFamily, cellt.Font.Size, FontStyle.Bold);
                    cellt.Text = "TOTAL";

                    var cell1t = new XRTableCell();
                    cell1t.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
                    cell1t.Font = new Font(cell1t.Font.FontFamily, cell1t.Font.Size, FontStyle.Bold);
                    cell1t.Text = TotalIEPS.ToString("C");


                    rowt.Cells.Add(cellt);
                    rowt.Cells.Add(cell1t);
                    rowt.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
                    _TableIEPS.Rows.Add(rowt);
                }
                if (model.PaymentsMethodsList != null)
                {
                    var rowHeight = 15f;
                    foreach (var item in model.PaymentsMethodsList)
                    {

                       

                        var row = new XRTableRow();
                        row.HeightF = rowHeight;

                        var cell = new XRTableCell();
                        cell.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
                        cell.Text = item.Importe.ToString("C");
                        TotalTipoPago += item.Importe;
                        var cell1 = new XRTableCell();
                        cell1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
                        cell1.Text = item.PaymentName;

                        row.Cells.Add(cell1);
                        row.Cells.Add(cell);
                        row.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
                        _TableTipoPago.Rows.Add(row);
                    }
                    var rowt = new XRTableRow();
                    rowt.HeightF = rowHeight;

                    var cellt = new XRTableCell();
                    cellt.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
                    cellt.Font = new Font(cellt.Font.FontFamily, cellt.Font.Size, FontStyle.Bold);
                    cellt.Text = "TOTAL";

                    var cell1t = new XRTableCell();
                    cell1t.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
                    cell1t.Font = new Font(cell1t.Font.FontFamily, cell1t.Font.Size, FontStyle.Bold);
                    cell1t.Text = TotalTipoPago.ToString("C");


                    rowt.Cells.Add(cellt);
                    rowt.Cells.Add(cell1t);
                    rowt.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
                    _TableTipoPago.Rows.Add(rowt);
                }

                /*_LTotalDesc.Text = TotalDesc.ToString("C");
                _LTotalEPS.Text = TotalIEPS.ToString("C");
                _LTotalSanc.Text = TotalSanc.ToString("C");
                _LTotalServ.Text = TotalServ.ToString("C");
                _LTotalTipoPago.Text = TotalTipoPago.ToString("C");*/

                //_DepMXNcajas.DataBindings.Add(new XRBinding("Text", null, "xrTableRowFormat._DepMXNcajas"));
                //_DepMXNCajaTipo.DataBindings.Add(new XRBinding("Text", null, "xrTableRowFormat._DepMXNCajaTipo"));

                //_DepMXNcc.DataBindings.Add(new XRBinding("Text", null, "xrTableRowFormat._DepMXNcc"));
                //_DepTipoCc.DataBindings.Add(new XRBinding("Text", null, "xrTableRowFormat._DepTipoCc"));

                //_DepDLLScajas.DataBindings.Add(new XRBinding("Text", null, "xrTableRowFormat._DepDLLScajas"));
                //_DepDLLSCajasTipo.DataBindings.Add(new XRBinding("Text", null, "xrTableRowFormat._DepDLLSCajasTipo"));
                //_DepDLLSMXNCajas.DataBindings.Add(new XRBinding("Text", null, "xrTableRowFormat._DepDLLSMXNCajas"));

                //_DepDLLScc.DataBindings.Add(new XRBinding("Text", null, "xrTableRowFormat._DepDLLScc"));
                //_DepDLLSCcTipo.DataBindings.Add(new XRBinding("Text", null, "xrTableRowFormat._DepDLLSCcTipo"));
                //_DepDLLSMXNCc.DataBindings.Add(new XRBinding("Text", null, "xrTableRowFormat._DepDLLSMXNCc"));





                return DataSetTableOrdered;
            }
            catch (Exception ex)
            {
                string d = ex.Message;
                throw;
            }

        }




    }
}
