﻿using System;
using System.Data;
using System.Collections.Generic;
using App.DAL.PurchaseOrder;
using App.Entities;
using DevExpress.XtraReports.UI;
using App.BLL.Supplier;
using App.Entities.ViewModels.PurchaseOrder;
using App.Entities.ViewModels.Sales;
using App.Entities.ViewModels.Sanctions;

namespace FloridoERPTX.Reports
{
    public partial class CancellationsCashier : DevExpress.XtraReports.UI.XtraReport
    {
       
        public DataSet DataSetTableOrdered = null;
        decimal subTotal = 0;
        public CancellationsCashier()
        {
            InitializeComponent();
          
        }
        public DataSet printTable(CancellationIndex cancModel) //se encarga de imprimir la orden del proveedor
        {
            try
            {

                labelDate.Text = DateTime.Now.Date.ToShortDateString();
                labelDate1.Text = cancModel.Date;
                labelDate2.Text = cancModel.Date2;
                siteLabel.Text = cancModel.SiteName;
                DataSetTableOrdered = new DataSet();

                DataTable DataTableOrdered = new DataTable();
                DataSetTableOrdered.Tables.Add(DataTableOrdered);

             


                DataTableOrdered.Columns.Add(new DataColumn("Cashier"));
                DataTableOrdered.Columns.Add(new DataColumn("Cancellations"));
                DataTableOrdered.Columns.Add(new DataColumn("importe"));
              





                foreach (var item in cancModel.CancellationsData)
                {
                    DataRow DataRowOrdered = DataSetTableOrdered.Tables[0].NewRow();
                    DataRowOrdered["Cashier"] = item.Cashier;
                    DataRowOrdered["Cancellations"] = item.NumberCancellations;
                    DataRowOrdered["importe"] = item.Amount.ToString("C");
                    
                    
                   

                    DataSetTableOrdered.Tables[0].Rows.Add(DataRowOrdered);
                  
                    subTotal += decimal.Parse(item.Amount.ToString());
                   



                }
               
               
                totalLabel.Text = "$" + subTotal.ToString("0.00");
                Cashier.DataBindings.Add(new XRBinding("Text", null, "xrTableRowFormat.Cashier"));
                Cancellations.DataBindings.Add(new XRBinding("Text", null, "xrTableRowFormat.Cancellations"));
                Importe.DataBindings.Add(new XRBinding("Text", null, "xrTableRowFormat.Importe"));





                return DataSetTableOrdered;
            }
            catch (Exception ex)
            {
                string d=ex.Message;
                throw;
            }
          
        }

       


    }
}
