﻿using System;
using System.Data;
using DevExpress.XtraReports.UI;
using App.Entities.ViewModels.Withdrawal;

namespace FloridoERPTX.Reports
{
    public partial class WithdrawalHeaderReport : DevExpress.XtraReports.UI.XtraReport
    {
       
        public DataSet DataSetTableOrdered = null;        
        public WithdrawalHeaderReport()
        {
            InitializeComponent();
          
        }
        public DataSet printTable(WithdrawalIndex Model) 
        {
            try
            {
                Date.Text = Model.Date;
                siteLabel.Text = Model.SiteName;
                DataSetTableOrdered = new DataSet();

                DataTable DataTableOrdered = new DataTable();
                DataSetTableOrdered.Tables.Add(DataTableOrdered);
                
                DataTableOrdered.Columns.Add(new DataColumn("Cashier"));
                DataTableOrdered.Columns.Add(new DataColumn("Currency"));
                DataTableOrdered.Columns.Add(new DataColumn("Amount"));               

                foreach (var item in Model.DataReport)
                {
                    DataRow DataRowOrdered = DataSetTableOrdered.Tables[0].NewRow();
                    DataRowOrdered["Cashier"] = item.Cashier;
                    DataRowOrdered["Currency"] = item.Currency;
                    if(item.Currency != "MXN")
                    {
                        var USDToMXN = Model.ExchangeRate * item.Amount;
                        DataRowOrdered["Amount"] = string.Format("${0} (${1})", item.Amount.ToString("0.00"), USDToMXN.ToString("0.00"));
                    }
                    else
                    {
                        DataRowOrdered["Amount"] = string.Format("${0}", item.Amount.ToString("0.00"));
                    }
                    
                    
                    DataSetTableOrdered.Tables[0].Rows.Add(DataRowOrdered);                                                        
                }
                
                total.Text = string.Format("{0}{1}", "$", Model.TotalAmount.ToString("0.00"));

                Cashier.DataBindings.Add(new XRBinding("Text", null, "xrTableRowFormat.Cashier"));
                Currency.DataBindings.Add(new XRBinding("Text", null, "xrTableRowFormat.Currency"));
                Amount.DataBindings.Add(new XRBinding("Text", null, "xrTableRowFormat.Amount"));               
                
                return DataSetTableOrdered;
            }
            catch (Exception ex)
            {
                string d=ex.Message;
                throw;
            }
          
        }       
    }
}
