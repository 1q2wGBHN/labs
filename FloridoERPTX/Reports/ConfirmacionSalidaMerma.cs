﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
using System.Data;
using System.Collections.Generic;
using App.Entities.ViewModels.DamagedsGoods;
using System.Globalization;

namespace FloridoERPTX.Reports
{
    public partial class ConfirmacionSalidaMerma : DevExpress.XtraReports.UI.XtraReport
    {
        public DataSet DataSetTableOrdered = null;

        public ConfirmacionSalidaMerma()
        {
            InitializeComponent();
        }

        public DataSet printTable(List<DamagedsGoodsModel> documentItems, string folio, string siteName, string fecha)
        {
            try
            {
                var cultureInfo = CultureInfo.GetCultureInfo("en-US");
                DataSetTableOrdered = new DataSet();

                DataTable DataTableOrdered = new DataTable();
                DataSetTableOrdered.Tables.Add(DataTableOrdered);

                xrLabelDate.Text = fecha;
                xrLabelDocumentNumber.Text = folio;
                xrLabelSite.Text = siteName;

                DataTableOrdered.Columns.Add(new DataColumn("xrTableCellPartNumber"));
                DataTableOrdered.Columns.Add(new DataColumn("xrTableCellPartDescription"));
                DataTableOrdered.Columns.Add(new DataColumn("xrTableCellScrapReason"));
                DataTableOrdered.Columns.Add(new DataColumn("xrTableCellPrice"));
                DataTableOrdered.Columns.Add(new DataColumn("xrTableCellQuantity"));
                DataTableOrdered.Columns.Add(new DataColumn("xrTableCellAmount"));
                //DataTableOrdered.Columns.Add(new DataColumn("xrTableCellIEPS"));
                //DataTableOrdered.Columns.Add(new DataColumn("xrTableCellIVA"));
                //DataTableOrdered.Columns.Add(new DataColumn("xrTableCellTotal"));

                decimal subtotal = 0;
                //decimal ivatotal = 0;
                //decimal iepstotal = 0;
                //decimal Total = 0;
                foreach (var item in documentItems)
                {
                    DataRow DataRowOrdered = DataSetTableOrdered.Tables[0].NewRow();
                    DataRowOrdered["xrTableCellPartNumber"] = item.PartNumber;
                    DataRowOrdered["xrTableCellPartDescription"] = item.Description;
                    DataRowOrdered["xrTableCellScrapReason"] = item.ScrapReason;
                    DataRowOrdered["xrTableCellPrice"] = String.Format(cultureInfo, "{0:C4}", item.Price);
                    DataRowOrdered["xrTableCellQuantity"] = item.Quantity;
                    DataRowOrdered["xrTableCellAmount"] = String.Format(cultureInfo, "{0:C4}", (item.Price * item.Quantity));
                    //DataRowOrdered["xrTableCellIEPS"] = String.Format(cultureInfo, "{0:C4}", ((item.IEPS / 100) * (item.Price * item.Quantity)));
                    //DataRowOrdered["xrTableCellIVA"] = String.Format(cultureInfo, "{0:C4}", (((item.IEPS / 100) * ((item.Price * item.Quantity)) + item.Price) * (item.IVA / 100)));
                    //DataRowOrdered["xrTableCellTotal"] = String.Format(cultureInfo, "{0:C4}", (((item.IEPS / 100) * (item.Price * item.Quantity)) + item.Price) * (item.IVA / 100) + (item.IEPS / 100) * (item.Price * item.Quantity) + item.Amount);
                    subtotal += (item.Price * item.Quantity);
                    //ivatotal += (((item.IEPS / 100) * ((item.Price * item.Quantity)) + item.Price) * (item.IVA / 100));
                    //iepstotal += (item.IEPS / 100) * (item.Price * item.Quantity);
                    //Total += (((item.IEPS / 100) * (item.Price * item.Quantity)) + item.Price) * (item.IVA / 100) + (item.IEPS / 100) * (item.Price * item.Quantity) + item.Amount;
                    DataSetTableOrdered.Tables[0].Rows.Add(DataRowOrdered);
                }

                xrLabelSub.Text = String.Format(cultureInfo, "{0:C4}", subtotal);
                //xrLabelIEPS.Text = String.Format(cultureInfo, "{0:C4}", iepstotal);
                //xrLabelIVA.Text = String.Format(cultureInfo, "{0:C4}", ivatotal);
                //xrLabelAmount.Text = String.Format(cultureInfo, "{0:C4}", Total);

                xrTableCellPartNumber.DataBindings.Add(new XRBinding("Text", null, "xrTableRow1.xrTableCellPartNumber"));
                xrTableCellPartDescription.DataBindings.Add(new XRBinding("Text", null, "xrTableRow1.xrTableCellPartDescription"));
                xrTableCellScrapReason.DataBindings.Add(new XRBinding("Text", null, "xrTableRow1.xrTableCellScrapReason"));
                xrTableCellQuantity.DataBindings.Add(new XRBinding("Text", null, "xrTableRow1.xrTableCellQuantity"));
                xrTableCellPrice.DataBindings.Add(new XRBinding("Text", null, "xrTableRow1.xrTableCellPrice"));
                xrTableCellAmount.DataBindings.Add(new XRBinding("Text", null, "xrTableRow1.xrTableCellAmount"));
                //xrTableCellIEPS.DataBindings.Add(new XRBinding("Text", null, "xrTableRow1.xrTableCellIEPS"));
                //xrTableCellIVA.DataBindings.Add(new XRBinding("Text", null, "xrTableRow1.xrTableCellIVA"));
                //xrTableCellTotal.DataBindings.Add(new XRBinding("Text", null, "xrTableRow1.xrTableCellTotal"));

                return DataSetTableOrdered;
            }
            catch (Exception ex)
            {
                string d = ex.Message;
                throw;
            }
        }
    }
}