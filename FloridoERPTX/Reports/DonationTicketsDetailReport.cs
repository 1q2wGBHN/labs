﻿using System;
using System.Data;
using DevExpress.XtraReports.UI;
using System.Collections.Generic;
using System.Linq;
using App.Entities.ViewModels.Donations;

namespace FloridoERPTX.Reports
{
    public partial class DonationTicketsDetailReport : DevExpress.XtraReports.UI.XtraReport
    {
       
        public DataSet DataSetTableOrdered = null;        
        public DonationTicketsDetailReport()
        {
            InitializeComponent();
          
        }
        public DataSet printTable(List<DonationsTicketsDetail> DataList, DonationsTicketsHeader Model) 
        {
            try
            {
                Date.Text = string.Format("{0} - {1}", Model.Date, Model.Time);
                Cashier.Text = string.IsNullOrEmpty(Model.Cashier) ? "Todos los Cajeros" : Model.Cashier;

                DataSetTableOrdered = new DataSet();

                DataTable DataTableOrdered = new DataTable();
                DataSetTableOrdered.Tables.Add(DataTableOrdered);
                
                DataTableOrdered.Columns.Add(new DataColumn("Item"));
                DataTableOrdered.Columns.Add(new DataColumn("Quantity"));
                DataTableOrdered.Columns.Add(new DataColumn("UnitPrice"));
                DataTableOrdered.Columns.Add(new DataColumn("TotalPrice"));

                foreach (var item in DataList)
                {
                    DataRow DataRowOrdered = DataSetTableOrdered.Tables[0].NewRow();
                    DataRowOrdered["Item"] = item.Description;
                    DataRowOrdered["Quantity"] = item.Quantity;
                    DataRowOrdered["UnitPrice"] = item.UnitPrice.ToString("C");
                    DataRowOrdered["TotalPrice"] = item.TotalPrice.ToString("C");


                    DataSetTableOrdered.Tables[0].Rows.Add(DataRowOrdered);                                                        
                }

                GreatTotal.Text = DataList.Sum(c => c.TotalPrice).ToString("C");

                Item.DataBindings.Add(new XRBinding("Text", null, "xrTableRowFormat.Item"));
                Quantity.DataBindings.Add(new XRBinding("Text", null, "xrTableRowFormat.Quantity"));
                UnitPrice.DataBindings.Add(new XRBinding("Text", null, "xrTableRowFormat.UnitPrice"));
                Total.DataBindings.Add(new XRBinding("Text", null, "xrTableRowFormat.TotalPrice"));

                return DataSetTableOrdered;
            }
            catch (Exception ex)
            {
                string d=ex.Message;
                throw;
            }
          
        }       
    }
}
