﻿using System;
using System.Data;
using System.Collections.Generic;
using App.DAL.PurchaseOrder;
using App.Entities;
using DevExpress.XtraReports.UI;
using App.BLL.Supplier;
using App.Entities.ViewModels.PurchaseOrder;
using App.Entities.ViewModels.Sales;
using App.Entities.ViewModels.Sanctions;

namespace FloridoERPTX.Reports
{
    public partial class WeighingReport : DevExpress.XtraReports.UI.XtraReport
    {
       
        public DataSet DataSetTableOrdered = null;
        decimal subTotal = 0;
        public WeighingReport()
        {
            InitializeComponent();
          
        }
        public DataSet printTable(WeighingIndex Model) //se encarga de imprimir la orden del proveedor
        {
            try
            {

                labelDate.Text = DateTime.Now.Date.ToShortDateString();
                labelDate1.Text = Model.Date;
                labelDate2.Text = Model.Date2;

                DataSetTableOrdered = new DataSet();

                DataTable DataTableOrdered = new DataTable();
                DataSetTableOrdered.Tables.Add(DataTableOrdered);

             


                DataTableOrdered.Columns.Add(new DataColumn("codigo"));
                DataTableOrdered.Columns.Add(new DataColumn("descripcion"));
                DataTableOrdered.Columns.Add(new DataColumn("ingresado"));
                DataTableOrdered.Columns.Add(new DataColumn("bascula"));
                DataTableOrdered.Columns.Add(new DataColumn("diferencia"));
                DataTableOrdered.Columns.Add(new DataColumn("fecha"));
                DataTableOrdered.Columns.Add(new DataColumn("venta"));
                DataTableOrdered.Columns.Add(new DataColumn("cajero"));
             






                foreach (var item in Model.WeighingReportData)
                {
                    DataRow DataRowOrdered = DataSetTableOrdered.Tables[0].NewRow();
                    DataRowOrdered["codigo"] = item.Codigo;
                    DataRowOrdered["descripcion"] = item.Descripcion;
                    DataRowOrdered["ingresado"] = item.Ingresado;
                    DataRowOrdered["bascula"] = item.Bascula;
                    DataRowOrdered["diferencia"] = item.Diferencia;
                    DataRowOrdered["fecha"] = item.Fecha;
                    DataRowOrdered["venta"] = item.Venta;
                    DataRowOrdered["cajero"] = item.Cajero;
                    DataSetTableOrdered.Tables[0].Rows.Add(DataRowOrdered);
                }
               
               
               
                codigo.DataBindings.Add(new XRBinding("Text", null, "xrTableRowFormat.codigo"));
                descripcion.DataBindings.Add(new XRBinding("Text", null, "xrTableRowFormat.descripcion"));
                ingresado.DataBindings.Add(new XRBinding("Text", null, "xrTableRowFormat.ingresado"));
                bascula.DataBindings.Add(new XRBinding("Text", null, "xrTableRowFormat.bascula"));
                diferencia.DataBindings.Add(new XRBinding("Text", null, "xrTableRowFormat.diferencia"));
                venta.DataBindings.Add(new XRBinding("Text", null, "xrTableRowFormat.venta"));
                fecha.DataBindings.Add(new XRBinding("Text", null, "xrTableRowFormat.fecha"));
                cajero.DataBindings.Add(new XRBinding("Text", null, "xrTableRowFormat.cajero"));





                return DataSetTableOrdered;
            }
            catch (Exception ex)
            {
                string d=ex.Message;
                throw;
            }
          
        }

       


    }
}
