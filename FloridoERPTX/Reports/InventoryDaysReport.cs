﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
using App.Entities.ViewModels.Item;
using System.Collections.Generic;
using System.Data;

namespace FloridoERPTX.Reports
{
    public partial class InventoryDaysReport : DevExpress.XtraReports.UI.XtraReport
    {
        public DataSet DataSetTableMovements = null;
        public InventoryDaysReport()
        {
            InitializeComponent();
        }
        public DataSet printTable(List<ItemInventoryDaysModel> items, string siteName, string days)
        {
            try
            {
                if (days == "0")
                {
                    xrLabelTitle.Text = "Reporte Dias de Inventario";
                }
                else
                {
                    xrLabelTitle.Text = "Reporte Excedentes";
                    xrDays.Text = days + " Dias";
                    xrLabelDia.Text = "Dias";
                }
                xrLabelSiteName.Text = siteName;
                xrLabelDate.Text = System.DateTime.Now.ToString();
                DataSetTableMovements = new DataSet();
                DataTable table = new DataTable();
                DataSetTableMovements.Tables.Add(table);

                table.Columns.Add(new DataColumn("cellPartNumber"));
                table.Columns.Add(new DataColumn("cellPartDescription"));
                table.Columns.Add(new DataColumn("cellUnrestricted"));
                table.Columns.Add(new DataColumn("cellStorageLocation"));
                table.Columns.Add(new DataColumn("cellDays"));
                table.Columns.Add(new DataColumn("cellAvgSale"));
                table.Columns.Add(new DataColumn("cellAmount"));

                foreach (var item in items)
                {
                    DataRow DataRowOrdered = DataSetTableMovements.Tables[0].NewRow();
                    DataRowOrdered["cellPartNumber"] = item.Part_number;
                    DataRowOrdered["cellPartDescription"] = item.Part_description;
                    DataRowOrdered["cellUnrestricted"] = (int)item.Unrestricted;
                    DataRowOrdered["cellStorageLocation"] = item.Storage_location;
                    DataRowOrdered["cellDays"] = (int)item.Days_avg;
                    DataRowOrdered["cellAvgSale"] = Decimal.Round(item.Avg_Sale,2);
                    DataRowOrdered["cellAmount"] = "$" + Decimal.Round(item.SubTotal,2);
                    DataSetTableMovements.Tables[0].Rows.Add(DataRowOrdered);
                }

                cellPartNumber.DataBindings.Add(new XRBinding("Text", null, "xrTableRow1.cellPartNumber"));
                cellPartDescription.DataBindings.Add(new XRBinding("Text", null, "xrTableRow1.cellPartDescription"));
                cellUnrestricted.DataBindings.Add(new XRBinding("Text", null, "xrTableRow1.cellUnrestricted"));
                cellStorageLocation.DataBindings.Add(new XRBinding("Text", null, "xrTableRow1.cellStorageLocation"));
                cellDays.DataBindings.Add(new XRBinding("Text", null, "xrTableRow1.cellDays"));
                cellAvgSale.DataBindings.Add(new XRBinding("Text", null, "xrTableRow1.cellAvgSale"));
                cellAmount.DataBindings.Add(new XRBinding("Text", null, "xrTableRow1.cellAmount"));

                return DataSetTableMovements;
            }
            catch (Exception ex)
            {
                string d = ex.Message;
                throw;
            }
        }

    }
}
