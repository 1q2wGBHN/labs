﻿using System;
using System.Data;
using System.Collections.Generic;
using App.DAL.PurchaseOrder;
using App.Entities;
using DevExpress.XtraReports.UI;
using App.BLL.Supplier;
using App.Entities.ViewModels.PurchaseOrder;
using App.Entities.ViewModels.Sales;
using App.Entities.ViewModels.Sanctions;
using FloridoERPTX.ViewModels;

namespace FloridoERPTX.Reports
{
    public partial class ProductsWithOutSales : DevExpress.XtraReports.UI.XtraReport
    {
       
        public DataSet DataSetTableOrdered = null;
        decimal subTotal = 0;
        public ProductsWithOutSales()
        {
            InitializeComponent();
          
        }
        public DataSet printTable(ItemsWithOutSalesIndex Model, string siteName) //se encarga de imprimir la orden del proveedor
        {
            try
            {

                labelDate.Text = DateTime.Now.Date.ToShortDateString();
                labelDate1.Text = Model.Date;
                labelDate2.Text = Model.Date2;
                siteLabel.Text = siteName;
                DataSetTableOrdered = new DataSet();

                DataTable DataTableOrdered = new DataTable();
                DataSetTableOrdered.Tables.Add(DataTableOrdered);

             


                DataTableOrdered.Columns.Add(new DataColumn("codigo"));
                DataTableOrdered.Columns.Add(new DataColumn("descripcion"));
                DataTableOrdered.Columns.Add(new DataColumn("ingresado"));
                DataTableOrdered.Columns.Add(new DataColumn("bascula"));
                DataTableOrdered.Columns.Add(new DataColumn("diferencia"));
          






                foreach (var item in Model.ItemsList)
                {
                    DataRow DataRowOrdered = DataSetTableOrdered.Tables[0].NewRow();
                    DataRowOrdered["codigo"] = item.part_number;
                    DataRowOrdered["descripcion"] = item.part_description;
                    DataRowOrdered["ingresado"] = item.total_stock;
                    DataRowOrdered["bascula"] = item.fecha;
                    DataRowOrdered["diferencia"] = item.quantity;
                
                    DataSetTableOrdered.Tables[0].Rows.Add(DataRowOrdered);
                }
               
               
               
                codigo.DataBindings.Add(new XRBinding("Text", null, "xrTableRowFormat.codigo"));
                descripcion.DataBindings.Add(new XRBinding("Text", null, "xrTableRowFormat.descripcion"));
                ingresado.DataBindings.Add(new XRBinding("Text", null, "xrTableRowFormat.ingresado"));
                bascula.DataBindings.Add(new XRBinding("Text", null, "xrTableRowFormat.bascula"));
                diferencia.DataBindings.Add(new XRBinding("Text", null, "xrTableRowFormat.diferencia"));
            





                return DataSetTableOrdered;
            }
            catch (Exception ex)
            {
                string d=ex.Message;
                throw;
            }
          
        }

       


    }
}
