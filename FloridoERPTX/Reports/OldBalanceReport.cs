﻿using System;
using System.Data;
using DevExpress.XtraReports.UI;
using App.Entities.ViewModels.Invoices;
using System.Globalization;
using System.Linq;
using static App.Entities.ViewModels.Common.Constants;

namespace FloridoERPTX.Reports
{
    public partial class OldBalanceReport : DevExpress.XtraReports.UI.XtraReport
    {
       
        public DataSet DataSetTableOrdered = null;        
        public OldBalanceReport()
        {
            InitializeComponent();
          
        }
        public DataSet printTable(InvoiceOldBalanceIndex Model) 
        {
            try
            {
                Date.Text = DateTime.Now.ToString(DateFormat.INT_DATE, CultureInfo.CreateSpecificCulture("es-ES"));
                Currency.Text = Model.Currency;
                Store.Text = Model.Store;                

                DataSetTableOrdered = new DataSet();                              

                DataTable DataTableOrdered = new DataTable();
                DataSetTableOrdered.Tables.Add(DataTableOrdered);

                DataTableOrdered.Columns.Add(new DataColumn("ClientName"));
                DataTableOrdered.Columns.Add(new DataColumn("Phone"));
                DataTableOrdered.Columns.Add(new DataColumn("Email"));
                DataTableOrdered.Columns.Add(new DataColumn("Balance"));                

                foreach(var item in Model.ReportData)
                {
                    DataRow DataRowOrdered = DataSetTableOrdered.Tables[0].NewRow();
                    DataRowOrdered["ClientName"] = item.CustomerName;
                    DataRowOrdered["Phone"] = item.Phone;
                    DataRowOrdered["Email"] = item.Email;
                    DataRowOrdered["Balance"] = "$" + item.TotalBalance;                    

                    DataSetTableOrdered.Tables[0].Rows.Add(DataRowOrdered);
                }               

                ClientName.DataBindings.Add(new XRBinding("Text", null, "xrTableRowFormat.ClientName"));
                Phone.DataBindings.Add(new XRBinding("Text", null, "xrTableRowFormat.Phone"));
                Email.DataBindings.Add(new XRBinding("Text", null, "xrTableRowFormat.Email"));
                Balance.DataBindings.Add(new XRBinding("Text", null, "xrTableRowFormat.Balance"));                

                return DataSetTableOrdered;
            }
            catch (Exception ex)
            {
                string d=ex.Message;
                throw;
            }          
        }       
    }
}
