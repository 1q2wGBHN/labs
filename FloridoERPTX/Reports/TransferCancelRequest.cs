﻿using App.Entities.ViewModels.Transfer;
using DevExpress.XtraReports.UI;
using System;
using System.Collections.Generic;
using System.Data;

namespace FloridoERPTX.Reports
{
    public partial class TransferCancelRequest : XtraReport
    {
        public DataSet DataSetTableOrdered = null;

        public TransferCancelRequest()
        {
            InitializeComponent();
        }

        public DataSet PrintTable(List<TransferDetailModel> transferItems, string TransferDate, string folio, string siteName, string siteEnvio)
        {
            try
            {
                DataSetTableOrdered = new DataSet();
                DataTable DataTableOrdered = new DataTable();
                DataSetTableOrdered.Tables.Add(DataTableOrdered);

                xrLabelDate.Text = TransferDate ?? "N/A";
                xrLabelDocumentNumber.Text = folio;
                xrLabelSite.Text = siteEnvio;
                xrLabel6.Text = siteName;

                DataTableOrdered.Columns.Add(new DataColumn("xrTableCellPartNumber"));
                DataTableOrdered.Columns.Add(new DataColumn("xrTableCellPartDescription"));
                DataTableOrdered.Columns.Add(new DataColumn("xrTableCellQuantity"));
                DataTableOrdered.Columns.Add(new DataColumn("xrTableCellGr_quantity"));
                DataTableOrdered.Columns.Add(new DataColumn("xrTableCellImport"));
                DataTableOrdered.Columns.Add(new DataColumn("xrTableCellCost"));
                DataTableOrdered.Columns.Add(new DataColumn("xrTableCellIeps"));
                DataTableOrdered.Columns.Add(new DataColumn("xrTableCellIva"));
                DataTableOrdered.Columns.Add(new DataColumn("xrTableCellSubtotal"));

                decimal subtotal = 0;
                decimal iepstotal = 0;
                decimal ivatotal = 0;
                foreach (var item in transferItems)
                {
                    decimal sub = 0;
                    decimal ieps = 0;
                    decimal iva = 0;
                    decimal total = 0;

                    if (item.GrQuantity == 0)
                    {
                        sub = item.UnitCost * item.Quantity;
                        ieps = item.IEPS * item.Quantity;
                        iva = item.Iva * item.Quantity;
                        total = sub + ieps + iva;
                    }
                    else
                    {
                        sub = item.UnitCost * item.GrQuantity.Value;
                        ieps = item.IEPS * item.GrQuantity.Value;
                        iva = item.Iva * item.GrQuantity.Value;
                        total = sub + ieps + iva;
                    }

                    DataRow DataRowOrdered = DataSetTableOrdered.Tables[0].NewRow();
                    DataRowOrdered["xrTableCellPartNumber"] = item.PartNumber;
                    DataRowOrdered["xrTableCellPartDescription"] = item.Description;
                    DataRowOrdered["xrTableCellQuantity"] = item.Quantity.ToString("N4");
                    DataRowOrdered["xrTableCellGr_quantity"] = (item.GrQuantity ?? 0).ToString("N4");
                    DataRowOrdered["xrTableCellCost"] = item.UnitCost.ToString("N4");

                    DataRowOrdered["xrTableCellSubtotal"] = sub.ToString("N4");
                    DataRowOrdered["xrTableCellIeps"] = ieps.ToString("N4");
                    DataRowOrdered["xrTableCellIva"] = iva.ToString("N4");
                    DataRowOrdered["xrTableCellImport"] = total.ToString("N4");

                    subtotal += sub;
                    iepstotal += ieps;
                    ivatotal += iva;
                    DataSetTableOrdered.Tables[0].Rows.Add(DataRowOrdered);
                }

                xrTableCellPartNumber.DataBindings.Add(new XRBinding("Text", null, "xrTableRow1.xrTableCellPartNumber"));
                xrTableCellPartDescription.DataBindings.Add(new XRBinding("Text", null, "xrTableRow1.xrTableCellPartDescription"));
                xrTableCellQuantity.DataBindings.Add(new XRBinding("Text", null, "xrTableRow1.xrTableCellQuantity"));
                xrTableCellGr_quantity.DataBindings.Add(new XRBinding("Text", null, "xrTableRow1.xrTableCellGr_quantity"));
                xrTableCellCost.DataBindings.Add(new XRBinding("Text", null, "xrTableRow1.xrTableCellCost"));
                xrTableCellIeps.DataBindings.Add(new XRBinding("Text", null, "xrTableRow1.xrTableCellIeps"));
                xrTableCellIva.DataBindings.Add(new XRBinding("Text", null, "xrTableRow1.xrTableCellIva"));
                xrTableCellSubtotal.DataBindings.Add(new XRBinding("Text", null, "xrTableRow1.xrTableCellSubtotal"));
                xrTableCellImport.DataBindings.Add(new XRBinding("Text", null, "xrTableRow1.xrTableCellImport"));

                xrLabelSubTotal.Text = "$ " + subtotal.ToString("N4");
                xrLabelIEPS.Text = "$ " + iepstotal.ToString("N4");
                xrLabelIVA.Text = "$ " + ivatotal.ToString("N4");
                xrLabelAmount.Text = "$ " + (subtotal + iepstotal + ivatotal).ToString("N4");

                return DataSetTableOrdered;
            }
            catch (Exception ex)
            {
                string d = ex.Message;
                throw;
            }
        }
    }
}