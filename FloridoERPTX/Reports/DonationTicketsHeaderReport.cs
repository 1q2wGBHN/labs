﻿using System;
using System.Data;
using DevExpress.XtraReports.UI;
using System.Collections.Generic;
using System.Linq;
using App.Entities.ViewModels.Donations;

namespace FloridoERPTX.Reports
{
    public partial class DonationTicketsHeaderReport : DevExpress.XtraReports.UI.XtraReport
    {
       
        public DataSet DataSetTableOrdered = null;        
        public DonationTicketsHeaderReport()
        {
            InitializeComponent();
          
        }
        public DataSet printTable(DonationsTicketsIndex Model) 
        {
            try
            {
                RangeDate.Text = string.Format("{0} - {1}", Model.StartDate, Model.EndDate);
                Cashier.Text = string.IsNullOrEmpty(Model.Cashier) ? "Todos los Cajeros" : Model.Cashier;

                DataSetTableOrdered = new DataSet();

                DataTable DataTableOrdered = new DataTable();
                DataSetTableOrdered.Tables.Add(DataTableOrdered);
                
                DataTableOrdered.Columns.Add(new DataColumn("Date"));
                DataTableOrdered.Columns.Add(new DataColumn("Time"));
                DataTableOrdered.Columns.Add(new DataColumn("CasahierDetail"));
                DataTableOrdered.Columns.Add(new DataColumn("Checkpoint"));
                DataTableOrdered.Columns.Add(new DataColumn("Amount"));

                foreach (var item in Model.DataReport)
                {
                    DataRow DataRowOrdered = DataSetTableOrdered.Tables[0].NewRow();
                    DataRowOrdered["Date"] = item.Date;
                    DataRowOrdered["Time"] = item.Time;
                    DataRowOrdered["CasahierDetail"] = item.Cashier;
                    DataRowOrdered["Checkpoint"] = item.CheckoutNo;
                    DataRowOrdered["Amount"] = item.Total.ToString("C");


                    DataSetTableOrdered.Tables[0].Rows.Add(DataRowOrdered);
                }

                GreatTotal.Text = Model.DataReport.Sum(c => c.Total).ToString("C");

                Date.DataBindings.Add(new XRBinding("Text", null, "xrTableRowFormat.Date"));
                Time.DataBindings.Add(new XRBinding("Text", null, "xrTableRowFormat.Time"));
                CasahierDetail.DataBindings.Add(new XRBinding("Text", null, "xrTableRowFormat.CasahierDetail"));
                Checkpoint.DataBindings.Add(new XRBinding("Text", null, "xrTableRowFormat.Checkpoint"));
                Amount.DataBindings.Add(new XRBinding("Text", null, "xrTableRowFormat.Amount"));                

                return DataSetTableOrdered;
            }
            catch (Exception ex)
            {
                string d=ex.Message;
                throw;
            }
          
        }       
    }
}
