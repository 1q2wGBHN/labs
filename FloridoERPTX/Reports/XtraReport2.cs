﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
using System.Data;
using System.Globalization;
using App.Entities.ViewModels.Transfer;
using System.Collections.Generic;

namespace FloridoERPTX.Reports
{
    public partial class XtraReport2 : DevExpress.XtraReports.UI.XtraReport
    {
        public DataSet DataSetTableOrdered = null;
        public XtraReport2()
        {
            InitializeComponent();
        }
        public DataSet printTable(NewTransferReport transferHeader,List<NewTransferDetailReport> TransferDetail)
        {
            try
            {
                var cultureInfo = CultureInfo.GetCultureInfo("en-US");
                DataSetTableOrdered = new DataSet();

                DataTable DataTableOrdered = new DataTable();
                DataSetTableOrdered.Tables.Add(DataTableOrdered);

                xrLabelTransfer.Text = transferHeader.TransferDocument.ToString();
                xrLabelPallets.Text = transferHeader.PalletSn.ToString();
                xrBarCodePallet.Text = transferHeader.PalletSn.ToString();
                xrLabelSite.Text = transferHeader.SiteName;

                DataTableOrdered.Columns.Add(new DataColumn("xrTableCellPartNumber"));
                DataTableOrdered.Columns.Add(new DataColumn("xrTableCellName"));
                DataTableOrdered.Columns.Add(new DataColumn("xrTableCellQuantity"));
                DataTableOrdered.Columns.Add(new DataColumn("xrTablePrice"));
                DataTableOrdered.Columns.Add(new DataColumn("xrTableCellIeps"));
                DataTableOrdered.Columns.Add(new DataColumn("xrTableCellIva"));
                DataTableOrdered.Columns.Add(new DataColumn("xrTableCellAmount"));
                DataTableOrdered.Columns.Add(new DataColumn("xrTableCellIepsTotal"));
                DataTableOrdered.Columns.Add(new DataColumn("xrTableCellIvaTotal"));
                DataTableOrdered.Columns.Add(new DataColumn("xrTableCellTotalAmount"));
                decimal Total = 0m;
                decimal Subtotal = 0m;
                decimal Iva = 0m;
                decimal Ieps = 0m;
                foreach (var item in TransferDetail)
                {
                    DataRow DataRowOrdered = DataSetTableOrdered.Tables[0].NewRow();
                    DataRowOrdered["xrTableCellPartNumber"] = item.PartNumber;
                    DataRowOrdered["xrTableCellName"] = item.PartDescription;
                    DataRowOrdered["xrTableCellQuantity"] = String.Format("{0:0.####}", item.Quantity);
                    DataRowOrdered["xrTablePrice"] = "$" + String.Format("{0:0.####}", item.Cost);
                    DataRowOrdered["xrTableCellIeps"] = item.Ieps.ToString("N2");
                    DataRowOrdered["xrTableCellIva"] = item.Iva.ToString("N2");
                    DataRowOrdered["xrTableCellAmount"] = "$" + String.Format("{0:0.####}", item.SubTotal);
                    DataRowOrdered["xrTableCellIepsTotal"] = "$" + String.Format("{0:0.####}", item.IepsTotal);
                    DataRowOrdered["xrTableCellIvaTotal"] = "$" + String.Format("{0:0.####}", item.IvaTotal);
                    DataRowOrdered["xrTableCellTotalAmount"] = "$" + String.Format("{0:0.####}", item.Total);
                    Total += item.Total;
                    Subtotal += item.SubTotal;
                    Iva += item.IvaTotal;
                    Ieps += item.IepsTotal; 
                    DataSetTableOrdered.Tables[0].Rows.Add(DataRowOrdered);
                }

                xrLabelSub.Text = "$" + String.Format("{0:0.####}", Subtotal);
                xrLabelIEPS.Text = "$" + String.Format("{0:0.####}", Ieps);
                xrLabelIVA.Text = "$" + String.Format("{0:0.####}", Iva);
                xrLabelAmount.Text = "$" + String.Format("{0:0.####}", Total);

                xrTableCellPartNumber.DataBindings.Add(new XRBinding("Text", null, "xrTableRow1.xrTableCellPartNumber"));
                xrTableCellName.DataBindings.Add(new XRBinding("Text", null, "xrTableRow1.xrTableCellName"));
                xrTableCellQuantity.DataBindings.Add(new XRBinding("Text", null, "xrTableRow1.xrTableCellQuantity"));
                xrTablePrice.DataBindings.Add(new XRBinding("Text", null, "xrTableRow1.xrTablePrice"));
                xrTableCellIeps.DataBindings.Add(new XRBinding("Text", null, "xrTableRow1.xrTableCellIeps"));
                xrTableCellIva.DataBindings.Add(new XRBinding("Text", null, "xrTableRow1.xrTableCellIva"));
                xrTableCellAmount.DataBindings.Add(new XRBinding("Text", null, "xrTableRow1.xrTableCellAmount"));
                xrTableCellIvaTotal.DataBindings.Add(new XRBinding("Text", null, "xrTableRow1.xrTableCellIvaTotal"));
                xrTableCellIepsTotal.DataBindings.Add(new XRBinding("Text", null, "xrTableRow1.xrTableCellIepsTotal"));
                xrTableCellTotalAmount.DataBindings.Add(new XRBinding("Text", null, "xrTableRow1.xrTableCellTotalAmount"));
                return DataSetTableOrdered;
            }
            catch (Exception ex)
            {
                string d = ex.Message;
                throw;
            }
        }
    }
}
