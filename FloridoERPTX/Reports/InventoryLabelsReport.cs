﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
using System.Collections.Generic;
using App.Entities;
using App.Entities.ViewModels.Inventory;

namespace FloridoERPTX.Reports
{
    public partial class InventoryLabelsReport : DevExpress.XtraReports.UI.XtraReport
    {
        public InventoryLabelsReport()
        {
            InitializeComponent();
        }

        public void Print(string id, string inventory, string siteName)
        {
            xrInventory.Text = id;
            xrFolio.Text = inventory;
            xrBarCode.DataBindings.Add("Text", null, "serial_partnumer");
        }


    }
}
