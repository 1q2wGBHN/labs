﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
using System.Data;
using App.Entities.ViewModels.EntryFree;

namespace FloridoERPTX.Reports
{
    public partial class EntryReport : DevExpress.XtraReports.UI.XtraReport
    {
        public DataSet DataSetTableOrdered = null;

        public EntryReport()
        {
            InitializeComponent();
        }

        public DataSet printTable(EntryFreeModel EntryFree)
        {
            try
            {
                DataSetTableOrdered = new DataSet();

                DataTable DataTableOrdered = new DataTable();
                DataSetTableOrdered.Tables.Add(DataTableOrdered);

                xrLabelSupplier.Text = EntryFree.Supplier;
                xrLabelType.Text = EntryFree.Type;
                xrLabelSubType.Text = EntryFree.SubType;
                xrLabelDate.Text = EntryFree.Date;
                xrLabelFolio.Text = EntryFree.Id.ToString();
                xrLabelReference.Text = EntryFree.Reference;
                xrLabelUser.Text = EntryFree.UserName;
                xrLabelPhone.Text = EntryFree.Phone;
                xrLabelAddress.Text = EntryFree.AddressSite;
                xrLabelSite.Text = EntryFree.Site;

                DataTableOrdered.Columns.Add(new DataColumn("xrTableCellPartNumber"));
                DataTableOrdered.Columns.Add(new DataColumn("xrTableCellName"));
                DataTableOrdered.Columns.Add(new DataColumn("xrTableCellSize"));
                DataTableOrdered.Columns.Add(new DataColumn("xrTableCellQuantity"));
                DataTableOrdered.Columns.Add(new DataColumn("xrTableCellPrice"));
                DataTableOrdered.Columns.Add(new DataColumn("xrTableCellImporte"));
                DataTableOrdered.Columns.Add(new DataColumn("xrTableCellIva"));
                DataTableOrdered.Columns.Add(new DataColumn("xrTableCellIeps"));
                DataTableOrdered.Columns.Add(new DataColumn("xrTableCellTotal"));

                decimal subtotal = 0;
                decimal ivatotal = 0;
                decimal iepstotal = 0;
                foreach (var item in EntryFree.EntryFreeList)
                {
                    DataRow DataRowOrdered = DataSetTableOrdered.Tables[0].NewRow();
                    DataRowOrdered["xrTableCellPartNumber"] = item.PartNumber;
                    DataRowOrdered["xrTableCellName"] = item.Description;
                    DataRowOrdered["xrTableCellSize"] = item.Size;
                    DataRowOrdered["xrTableCellQuantity"] = item.Quantity.ToString("N4");
                    DataRowOrdered["xrTableCellPrice"] = "$" + item.Price.ToString("N4");
                    DataRowOrdered["xrTableCellImporte"] = "$" + item.Import.ToString("N4");
                    DataRowOrdered["xrTableCellIva"] = "$" + item.Iva.ToString("N4");
                    DataRowOrdered["xrTableCellIeps"] = "$" + item.Ieps.ToString("N4");
                    DataRowOrdered["xrTableCellTotal"] = "$" + (item.Import + item.Iva + item.Ieps).ToString("N4");
                    subtotal += item.Import;
                    ivatotal += item.Iva;
                    iepstotal += item.Ieps;
                    DataSetTableOrdered.Tables[0].Rows.Add(DataRowOrdered);
                }

                xrLabelTotal.Text = "$ " + (subtotal + ivatotal + iepstotal).ToString("N4");
                xrLabelAmount.Text = "$ " + subtotal.ToString("N4");
                xrLabelIvaTotal.Text = "$ " + ivatotal.ToString("N4");
                xrLabelIeps.Text = "$ " + iepstotal.ToString("N4");

                xrTableCellPartNumber.DataBindings.Add(new XRBinding("Text", null, "xrTableRow1.xrTableCellPartNumber"));
                xrTableCellName.DataBindings.Add(new XRBinding("Text", null, "xrTableRow1.xrTableCellName"));
                xrTableCellSize.DataBindings.Add(new XRBinding("Text", null, "xrTableRow1.xrTableCellSize"));
                xrTableCellQuantity.DataBindings.Add(new XRBinding("Text", null, "xrTableRow1.xrTableCellQuantity"));
                xrTableCellPrice.DataBindings.Add(new XRBinding("Text", null, "xrTableRow1.xrTableCellPrice"));
                xrTableCellImporte.DataBindings.Add(new XRBinding("Text", null, "xrTableRow1.xrTableCellImporte"));
                xrTableCellIva.DataBindings.Add(new XRBinding("Text", null, "xrTableRow1.xrTableCellIva"));
                xrTableCellIeps.DataBindings.Add(new XRBinding("Text", null, "xrTableRow1.xrTableCellIeps"));
                xrTableCellTotal.DataBindings.Add(new XRBinding("Text", null, "xrTableRow1.xrTableCellTotal"));

                return DataSetTableOrdered;
            }
            catch (Exception ex)
            {
                string d = ex.Message;
                throw;
            }
        }

        internal object printTable(object p, EntryFreeModel EntryFree)
        {
            throw new NotImplementedException();
        }
    }
}