﻿using System;
using System.Data;
using DevExpress.XtraReports.UI;
using System.Linq;
using App.Entities.ViewModels.Checkout;

namespace FloridoERPTX.Reports
{
    public partial class ZCutReport : DevExpress.XtraReports.UI.XtraReport
    {
       
        public DataSet DataSetTableOrdered = null;        
        public ZCutReport()
        {
            InitializeComponent();
          
        }
        public DataSet printTable(CheckoutIndex Model)
        {
            try
            {
                DataSetTableOrdered = new DataSet();
                DataTable DataTableOrdered = new DataTable();

                Date.Text = Model.Date;
                Cashier.Text = string.IsNullOrEmpty(Model.Cashier) ? "Todos los Cajeros" : Model.Cashier;
                totalCharge.Text = Model.DataInfo.TotalCharge.ToString("C");
                totalDonations.Text = Model.DataInfo.TotalDonations.ToString("C");
                totalReturns.Text = Model.DataInfo.TotalReturns.ToString("C");
                totalWithdrawal.Text = Model.DataInfo.TotalWithdrawal.ToString("C");
                balance.Text = Model.DataInfo.Balance.ToString("C");

                decimal exchangeRate = Model.DataInfo.ExchangeRate.Select(c => c.cur_rate1).First();

                #region Charge Table
                foreach (var Item in Model.DataInfo.Charge.Where(c => c.Monto != 0))
                {
                    decimal usdTomxn = 0;
                    var row = new XRTableRow();
                    
                    var cell = new XRTableCell();
                    cell.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
                    cell.Text = Item.pm_name;
                   
                    var cell2 = new XRTableCell();
                    cell2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
                    cell2.Text = Item.Monto.ToString();

                    var cell3 = new XRTableCell();
                    var cell4 = new XRTableCell();

                    if (Item.currency == "USD")
                    {
                        usdTomxn = Item.Monto * exchangeRate;                        
                        cell3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
                        cell3.Text = exchangeRate.ToString("C");

                        cell4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
                        cell4.Text = string.Format("{0} ({1})", usdTomxn.ToString("C"), Item.Monto.ToString("C"));
                    }
                    else
                    {
                        cell3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
                        cell3.Text = string.Empty;

                        cell4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
                        cell4.Text = Item.Monto.ToString("C");
                    }
                                        
                    row.Cells.Add(cell);
                    row.Cells.Add(cell2);
                    row.Cells.Add(cell3);
                    row.Cells.Add(cell4);                   
                    tableCharge.Rows.Add(row);
                }
                #endregion

                #region Withdrawal Table
                foreach (var Item in Model.DataInfo.Withdrawal.Where(c => c.Retiro != 0))
                {
                    decimal usdTomxn = 0;
                    var row = new XRTableRow();

                    var cell = new XRTableCell();
                    cell.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
                    cell.Text = Item.pm_name;


                    var cell2 = new XRTableCell();
                    cell2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
                    cell2.Text = Item.Retiro.ToString();

                    var cell3 = new XRTableCell();
                    var cell4 = new XRTableCell();

                    if (Item.currency == "USD")
                    {
                        usdTomxn = Item.Retiro * exchangeRate;
                        cell3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
                        cell3.Text = exchangeRate.ToString("C");

                        cell4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
                        cell4.Text = string.Format("{0} ({1})", usdTomxn.ToString("C"), Item.Retiro.ToString("C"));
                    }
                    else
                    {
                        cell3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
                        cell3.Text = string.Empty;

                        cell4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
                        cell4.Text = Item.Retiro.ToString("C");
                    }

                    row.Cells.Add(cell);
                    row.Cells.Add(cell2);
                    row.Cells.Add(cell3);
                    row.Cells.Add(cell4);
                    tableWithdrawal.Rows.Add(row);
                }
                #endregion

                #region Returns Table
                foreach (var Item in Model.DataInfo.Returns.Where(c => c.Monto != 0))
                {
                    decimal usdTomxn = 0;
                    var row = new XRTableRow();

                    var cell = new XRTableCell();
                    cell.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
                    cell.Text = Item.pm_name;


                    var cell2 = new XRTableCell();
                    cell2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
                    cell2.Text = Item.Monto.ToString();

                    var cell3 = new XRTableCell();
                    var cell4 = new XRTableCell();

                    if (Item.currency == "USD")
                    {
                        usdTomxn = Item.Monto * exchangeRate;
                        cell3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
                        cell3.Text = exchangeRate.ToString("C");

                        cell4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
                        cell4.Text = string.Format("{0} ({1})", usdTomxn.ToString("C"), Item.Monto.ToString("C"));
                    }
                    else
                    {
                        cell3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
                        cell3.Text = string.Empty;

                        cell4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
                        cell4.Text = Item.Monto.ToString("C");
                    }

                    row.Cells.Add(cell);
                    row.Cells.Add(cell2);
                    row.Cells.Add(cell3);
                    row.Cells.Add(cell4);
                    tableReturns.Rows.Add(row);
                }
                #endregion

                #region Donations Table                
                var rowd = new XRTableRow();

                var celld = new XRTableCell();
                celld.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
                celld.Text = "Donaciones";


                var celld2 = new XRTableCell();
                celld2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
                celld2.Text = Model.DataInfo.TotalDonations.ToString();

                var celld3 = new XRTableCell();
                celld3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
                celld3.Text = string.Empty;

                var celld4 = new XRTableCell();
                celld4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
                celld4.Text = Model.DataInfo.TotalDonations.ToString("C");

                rowd.Cells.Add(celld);
                rowd.Cells.Add(celld2);
                rowd.Cells.Add(celld3);
                rowd.Cells.Add(celld4);
                tableDonations.Rows.Add(rowd);
                #endregion

                return DataSetTableOrdered;
            }
            catch (Exception ex)
            {
                string d = ex.Message;
                throw;
            }

        }
    }
}
