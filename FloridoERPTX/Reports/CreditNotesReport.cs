﻿using System;
using System.Data;
using System.Collections.Generic;
using App.DAL.PurchaseOrder;
using App.Entities;
using DevExpress.XtraReports.UI;
using App.BLL.Supplier;
using App.Entities.ViewModels.PurchaseOrder;
using App.Entities.ViewModels.CreditNotes;

namespace FloridoERPTX.Reports
{
    public partial class CreditNotesReport : DevExpress.XtraReports.UI.XtraReport
    {
       
        public DataSet DataSetTableOrdered = null;
        decimal ivaValue, iepsValue,totalValue,subTotal = 0;

        private void InitializCreditNotesReporteComponent()
        {
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // CreditNotesReport
            // 
            this.Version = "16.2";
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

        }

        public CreditNotesReport()
        {
            InitializeComponent();
          
        }
        public DataSet printTable(List<CreditNotesBon> salesList, string date1, string date2, string moneda, string tipo, string status, string siteName) //se encarga de imprimir la orden del proveedor
        {
            try
            {

                labelDate.Text = DateTime.Now.Date.ToShortDateString();
                labelDate1.Text = date1;
                labelDate2.Text = date2;
                labelMoneda.Text = moneda;
                labelTipo.Text = tipo;
                labelEstado.Text = status;
                siteLabel.Text = siteName;
                DataSetTableOrdered = new DataSet();

                DataTable DataTableOrdered = new DataTable();
                DataSetTableOrdered.Tables.Add(DataTableOrdered);

                
               
                 
                DataTableOrdered.Columns.Add(new DataColumn("CfdNotaNo"));
                DataTableOrdered.Columns.Add(new DataColumn("Date"));
                DataTableOrdered.Columns.Add(new DataColumn("Cause"));
                DataTableOrdered.Columns.Add(new DataColumn("InvId"));
                DataTableOrdered.Columns.Add(new DataColumn("Customer"));
                DataTableOrdered.Columns.Add(new DataColumn("Import"));
                DataTableOrdered.Columns.Add(new DataColumn("IVA"));
                DataTableOrdered.Columns.Add(new DataColumn("IEPS"));
                DataTableOrdered.Columns.Add(new DataColumn("Total"));
                DataTableOrdered.Columns.Add(new DataColumn("User"));
                DataTableOrdered.Columns.Add(new DataColumn("Status"));
                DataTableOrdered.Columns.Add(new DataColumn("Uuid"));
                DataTableOrdered.Columns.Add(new DataColumn("Type"));





                foreach (var item in salesList)
                {
                    DataRow DataRowOrdered = DataSetTableOrdered.Tables[0].NewRow();
                    DataRowOrdered["CfdNotaNo"] = item.CfdNotaNo;
                    DataRowOrdered["Date"] = item.Date;
                    DataRowOrdered["Cause"] = item.Cause;
                    DataRowOrdered["InvId"] = item.InvId;
                    DataRowOrdered["Customer"] =  item.Customer;
                    DataRowOrdered["Import"] = "$" + item.Import.ToString("0.00");
                    DataRowOrdered["IVA"] = "$" + item.IVA.ToString("0.00");
                    DataRowOrdered["IEPS"] = "$" + item.IEPS.ToString("0.00"); 
                    DataRowOrdered["Total"] = "$" + item.Total.ToString("0.00"); ;
                    DataRowOrdered["User"] = item.User;
                    DataRowOrdered["Status"] = item.Status;
                    DataRowOrdered["Uuid"] = item.Uuid;
                    DataRowOrdered["Type"] = item.Type;



                    DataSetTableOrdered.Tables[0].Rows.Add(DataRowOrdered);
                    ivaValue += item.IVA;
                    iepsValue += item.IEPS;
                    subTotal += item.Import;
                    totalValue += item.Total;



                }
               
                ivaLabel.Text = "$"+ivaValue.ToString("0.00");
                iepsLabel.Text = "$" + iepsValue.ToString("0.00");
                subtotalLabel.Text = "$" + subTotal.ToString("0.00");
                totalLabel.Text = "$" + totalValue.ToString("0.00");
                CfdNotaNo.DataBindings.Add(new XRBinding("Text", null, "xrTableRowFormat.CfdNotaNo"));
                Date.DataBindings.Add(new XRBinding("Text", null, "xrTableRowFormat.Date"));
                Cause.DataBindings.Add(new XRBinding("Text", null, "xrTableRowFormat.Cause"));
                InvId.DataBindings.Add(new XRBinding("Text", null, "xrTableRowFormat.InvId"));
                Customer.DataBindings.Add(new XRBinding("Text", null, "xrTableRowFormat.Customer"));
                Import.DataBindings.Add(new XRBinding("Text", null, "xrTableRowFormat.Import"));
                IVA.DataBindings.Add(new XRBinding("Text", null, "xrTableRowFormat.IVA"));
                IEPS.DataBindings.Add(new XRBinding("Text", null, "xrTableRowFormat.IEPS"));   
                Total.DataBindings.Add(new XRBinding("Text", null, "xrTableRowFormat.Total"));
                User.DataBindings.Add(new XRBinding("Text", null, "xrTableRowFormat.User"));
                Status.DataBindings.Add(new XRBinding("Text", null, "xrTableRowFormat.Status"));
                Uuid.DataBindings.Add(new XRBinding("Text", null, "xrTableRowFormat.Uuid"));
                Type.DataBindings.Add(new XRBinding("Text", null, "xrTableRowFormat.Type"));
               





                return DataSetTableOrdered;
            }
            catch (Exception ex)
            {
                string d=ex.Message;
                throw;
            }
          
        }

       


    }
}
