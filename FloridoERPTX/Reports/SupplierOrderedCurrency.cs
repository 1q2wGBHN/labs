﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
using System.Data;
using App.Entities.ViewModels.Order;
using System.Collections.Generic;

namespace FloridoERPTX.Reports
{
    public partial class SupplierOrderedCurrency : DevExpress.XtraReports.UI.XtraReport
    {
        public DataSet DataSetTableOrdered = null;
        public SupplierOrderedCurrency()
        {
            InitializeComponent();
        }

        public DataSet printTable(OrderModel Order, List<OrderListModel> OrderList , string currency) //se encarga de imprimir la orden del proveedor
        {
            DataSetTableOrdered = new DataSet();

            DataTable DataTableOrdered = new DataTable();
            DataSetTableOrdered.Tables.Add(DataTableOrdered);

            xrLabelDate.Text = DateTime.Now.ToString("dd/MM/yyyy");
            xrLabelFolioFormat.Text = Order.Folio.ToString();//"38679";
            xrLabelBranchOffice.Text = Order.Site;//"*FLORIDO LAS AMARICAS 2";
            xrLabelSupplier.Text = Order.Supplier;//"BIMBO S.A. DE C.V. 2";
            xrLabelDays.Text = Order.RefillDays.ToString();
            xrLabelComment.Text = Order.Comment;
            xrLabelCurrency.Text = currency;
            //DataTableOrdered.Columns.Add(new DataColumn("xrTableCellFolioNo"));
            DataTableOrdered.Columns.Add(new DataColumn("xrTableCellCode"));
            //DataTableOrdered.Columns.Add(new DataColumn("xrTableCellBarcode"));
            DataTableOrdered.Columns.Add(new DataColumn("xrTableCellDescription"));
            DataTableOrdered.Columns.Add(new DataColumn("xrTableCellStock"));
            DataTableOrdered.Columns.Add(new DataColumn("xrTableCellPurchase"));
            DataTableOrdered.Columns.Add(new DataColumn("xrTableCellSale"));
            DataTableOrdered.Columns.Add(new DataColumn("xrTableCellSuggested"));
            DataTableOrdered.Columns.Add(new DataColumn("xrTableCellSupplier"));
            DataTableOrdered.Columns.Add(new DataColumn("xrTableCellAutorize"));
            DataTableOrdered.Columns.Add(new DataColumn("xrTableCellPacking"));

            System.Collections.Generic.List<string> Prueba = new System.Collections.Generic.List<string>();

            foreach (var item in OrderList)
            {
                DataRow DataRowOrdered = DataSetTableOrdered.Tables[0].NewRow();
                //DataRowOrdered["xrTableCellFolioNo"] = item.Folio;
                DataRowOrdered["xrTableCellCode"] = item.ParthNumber;
                //DataRowOrdered["xrTableCellBarcode"] = item.BarCode;
                DataRowOrdered["xrTableCellDescription"] = item.Description;
                DataRowOrdered["xrTableCellStock"] = item.Stock;
                DataRowOrdered["xrTableCellPurchase"] = item.PrePruchase;
                DataRowOrdered["xrTableCellSale"] = item.AverageSale;
                DataRowOrdered["xrTableCellSuggested"] = item.SuggestedSystems;
                DataRowOrdered["xrTableCellSupplier"] = "";
                DataRowOrdered["xrTableCellAutorize"] = "";
                DataRowOrdered["xrTableCellPacking"] = item.Packing;
                DataSetTableOrdered.Tables[0].Rows.Add(DataRowOrdered);
            }

            //xrTableCellFolioNo.DataBindings.Add(new XRBinding("Text", null, "xrTableRowFormat.xrTableCellFolioNo"));
            xrTableCellCode.DataBindings.Add(new XRBinding("Text", null, "xrTableRowFormat.xrTableCellCode"));
            //xrTableCellBarcode.DataBindings.Add(new XRBinding("Text", null, "xrTableRowFormat.xrTableCellBarcode"));
            xrTableCellDescription.DataBindings.Add(new XRBinding("Text", null, "xrTableRowFormat.xrTableCellDescription"));
            xrTableCellStock.DataBindings.Add(new XRBinding("Text", null, "xrTableRowFormat.xrTableCellStock"));
            xrTableCellPurchase.DataBindings.Add(new XRBinding("Text", null, "xrTableRowFormat.xrTableCellPurchase"));
            xrTableCellSale.DataBindings.Add(new XRBinding("Text", null, "xrTableRowFormat.xrTableCellSale"));
            xrTableCellSuggested.DataBindings.Add(new XRBinding("Text", null, "xrTableRowFormat.xrTableCellSuggested"));
            xrTableCellPacking.DataBindings.Add(new XRBinding("Text", null, "xrTableRowFormat.xrTableCellPacking"));
            xrTableCellSupplier.DataBindings.Add(new XRBinding("Text", null, "xrTableRowFormat.xrTableCellSupplier"));
            xrTableCellAutorize.DataBindings.Add(new XRBinding("Text", null, "xrTableRowFormat.xrTableCellAutorize"));
            return DataSetTableOrdered;
        }

        internal object printTable(object p, List<OrderListModel> list)
        {
            throw new NotImplementedException();
        }
    }
}

