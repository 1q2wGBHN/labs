﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
using System.Data;
using App.Entities.ViewModels.RawMaterial;
using System.Collections.Generic;
using System.Globalization;
using App.Entities;

namespace FloridoERPTX.Reports
{
    public partial class RawMaterialDetailReport : DevExpress.XtraReports.UI.XtraReport
    {
        public DataSet DataSetTableMovements = null;

        public RawMaterialDetailReport()
        {
            InitializeComponent();
        }

        public DataSet PrintTable(DateTime BeginDate, DateTime EndDate, List<RawMaterialDetail> raw, SITES site_info, string location, string status, string department, string family)
        {
            try
            {

                DataSetTableMovements = new DataSet();
                DataTable table = new DataTable();
                DataSetTableMovements.Tables.Add(table);
                xrLabelBeginDate.Text = BeginDate.ToString("dd/MM/yyyy");
                xrLabelEndDate.Text = EndDate.ToString("dd/MM/yyyy");
                xrlblStatus.Text = GetStatusName(status);
                xrlblLocation.Text = location != "" ? location : "Todos";
                xrlblFamily.Text = family != "" ? family : "Todos";
                xrLabelDepartment.Text = department != "" ? department : "Todos";

                //Info Site
                xraddress.Text = site_info.address + " C.P." + site_info.postal_code;
                xrState.Text = site_info.city + ", " + site_info.state;
                siteLabel.Text = "*" + site_info.site_name;

                table.Columns.Add(new DataColumn("cellFolio"));
                table.Columns.Add(new DataColumn("cellPartNumber"));
                table.Columns.Add(new DataColumn("cellDescription"));
                table.Columns.Add(new DataColumn("cellDepartment"));
                table.Columns.Add(new DataColumn("cellFamily"));
                table.Columns.Add(new DataColumn("cellLocation"));
                table.Columns.Add(new DataColumn("cellDate"));
                table.Columns.Add(new DataColumn("cellQuantity"));
                table.Columns.Add(new DataColumn("cellCost"));
                table.Columns.Add(new DataColumn("cellIva"));
                table.Columns.Add(new DataColumn("cellIeps"));
                table.Columns.Add(new DataColumn("cellTotal"));

                var cultureInfo = CultureInfo.GetCultureInfo("en-US");
                decimal subtotal = 0;
                decimal ivatotal = 0;
                decimal iepstotal = 0;
                decimal Total = 0;

                foreach (var item in raw)
                {
                    DataRow DataRowOrdered = DataSetTableMovements.Tables[0].NewRow();
                    DataRowOrdered["cellFolio"] = item.Folio;
                    DataRowOrdered["cellPartNumber"] = item.Part_Number;
                    DataRowOrdered["cellDescription"] = item.Description;
                    DataRowOrdered["cellDepartment"] = item.Department;
                    DataRowOrdered["cellFamily"] = item.Family;
                    DataRowOrdered["cellLocation"] = item.StorageLocation;
                    DataRowOrdered["cellDate"] = item.TransferDate;
                    DataRowOrdered["cellQuantity"] = (int)item.Quantity;
                    DataRowOrdered["cellCost"] = "$" + item.UnitCost.ToString("N4");
                    DataRowOrdered["cellIva"] = "$" + item.Iva.ToString("N4");
                    DataRowOrdered["cellIeps"] = "$" + item.IEPS.ToString("N4");
                    DataRowOrdered["cellTotal"] = "$" + item.Amount.ToString("N4");

                    subtotal += item.SubTotal;
                    Total += item.Amount;
                    ivatotal += item.Iva;
                    iepstotal += item.IEPS;

                    DataSetTableMovements.Tables[0].Rows.Add(DataRowOrdered);
                }
                xrLabelAmount.Text = String.Format(cultureInfo, "{0:C4}", Total);
                xrLabelIEPS.Text = String.Format(cultureInfo, "{0:C4}", iepstotal);
                xrLabelIVA.Text = String.Format(cultureInfo, "{0:C4}", ivatotal);
                xrLabelSub.Text = String.Format(cultureInfo, "{0:C4}", subtotal);

                xrTableFolio.DataBindings.Add(new XRBinding("Text", null, "xrTableRow1.cellFolio"));
                xrTableCellPartNumber.DataBindings.Add(new XRBinding("Text", null, "xrTableRow1.cellPartNumber"));
                xrTableCellDescription.DataBindings.Add(new XRBinding("Text", null, "xrTableRow1.cellDescription"));
                xrTableCellDepartment.DataBindings.Add(new XRBinding("Text", null, "xrTableRow1.cellDepartment"));
                xrTableCellFamily.DataBindings.Add(new XRBinding("Text", null, "xrTableRow1.cellFamily"));
                xrTableLocation.DataBindings.Add(new XRBinding("Text", null, "xrTableRow1.cellLocation"));
                xrTableDate.DataBindings.Add(new XRBinding("Text", null, "xrTableRow1.cellDate"));
                xrTableCellQuantity.DataBindings.Add(new XRBinding("Text", null, "xrTableRow1.cellQuantity"));
                xrTableCellCost.DataBindings.Add(new XRBinding("Text", null, "xrTableRow1.cellCost"));
                xrTableCellIva.DataBindings.Add(new XRBinding("Text", null, "xrTableRow1.cellIva"));
                xrTableCellIeps.DataBindings.Add(new XRBinding("Text", null, "xrTableRow1.cellIeps"));
                xrTableCellTotal.DataBindings.Add(new XRBinding("Text", null, "xrTableRow1.cellTotal"));
                return DataSetTableMovements;
            }
            catch (Exception ex)
            {
                string d = ex.Message;
                throw;
            }

        }
        public string GetStatusName(string status)
        {
            if (status == "0")
                return "Todos";
            else if (status == "2")
                return "Aprobado";
            else if (status == "8")
                return "Cancelado";
            else if (status == "9")
                return "Terminado";
            else
                return "Todos.";
        }
    }
}