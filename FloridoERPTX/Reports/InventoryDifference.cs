﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
using System.Data;
using App.Entities.ViewModels.Inventory;
using System.Collections.Generic;
using System.ComponentModel.Design;
using DevExpress.XtraRichEdit.API.Native;

namespace FloridoERPTX.Reports
{
    public partial class InventoryDifference : DevExpress.XtraReports.UI.XtraReport
    {
        public DataSet DataSetTableMovements = null;
        public InventoryDifference()
        {
            InitializeComponent();
        }
        public DataSet printTable(List<List<InventoryReportModel>> lista, string site, string id, string inventoryDescription, string scrapDates)
        {
            try
            {
                DataSetTableMovements = new DataSet();
                decimal amountStock = 0, amountCounted = 0, amountDifference = 0, amountScrap = 0, amountDifScrap = 0;
                List<Concentrado> concentrado = new List<Concentrado>();
                bool first = false;
                DataTable tableMovements = new DataTable();

                DataSetTableMovements.Tables.Add(tableMovements);

                tableMovements.Columns.Add(new DataColumn("cellPartNumber"));
                tableMovements.Columns.Add(new DataColumn("cellPartDescription"));
                tableMovements.Columns.Add(new DataColumn("cellStock"));
                tableMovements.Columns.Add(new DataColumn("cellCounted"));
                tableMovements.Columns.Add(new DataColumn("cellDifference"));
                tableMovements.Columns.Add(new DataColumn("cellAdjustment"));
                tableMovements.Columns.Add(new DataColumn("cellUnitPrice"));
                tableMovements.Columns.Add(new DataColumn("cellDifScrap"));
                tableMovements.Columns.Add(new DataColumn("cellDifScrapAmount"));
                tableMovements.Columns.Add(new DataColumn("cellAmountDifference"));
                tableMovements.Columns.Add(new DataColumn("cellDepartment"));
                tableMovements.Columns.Add(new DataColumn("cellScrap"));
                tableMovements.Columns.Add(new DataColumn("cellAmountScrap"));
                tableMovements.Columns.Add(new DataColumn("cellSupplier"));

                DataRow DataRowOrdered7 = DataSetTableMovements.Tables[0].NewRow();
                DataRowOrdered7["cellPartNumber"] = "INVENTARIO";
                DataRowOrdered7["cellPartDescription"] = inventoryDescription;
                DataRowOrdered7["cellScrap"] = id;
                DataRowOrdered7["cellStock"] = site;
                DataRowOrdered7["cellCounted"] = "";
                DataRowOrdered7["cellDifference"] = "";
                DataRowOrdered7["cellAdjustment"] = "";
                DataRowOrdered7["cellUnitPrice"] = "";
                DataRowOrdered7["cellDifScrap"] = "";
                DataRowOrdered7["cellDifScrapAmount"] = "";
                DataRowOrdered7["cellAmountDifference"] = "";
                DataRowOrdered7["cellDepartment"] = "";
                DataRowOrdered7["cellAmountScrap"] = "";
                DataRowOrdered7["cellSupplier"] = "";
                DataSetTableMovements.Tables[0].Rows.Add(DataRowOrdered7);

                DataRow DataRowOrdered8 = DataSetTableMovements.Tables[0].NewRow();
                DataRowOrdered8["cellPartNumber"] = "";
                DataRowOrdered8["cellPartDescription"] = "";
                DataRowOrdered8["cellScrap"] = "";
                DataRowOrdered8["cellStock"] = "";
                DataRowOrdered8["cellCounted"] = "";
                DataRowOrdered8["cellDifference"] = "";
                DataRowOrdered8["cellAdjustment"] = "";
                DataRowOrdered8["cellUnitPrice"] = "";
                DataRowOrdered8["cellDifScrap"] = "";
                DataRowOrdered8["cellDifScrapAmount"] = "";
                DataRowOrdered8["cellAmountDifference"] = "";
                DataRowOrdered8["cellDepartment"] = "";
                DataRowOrdered8["cellAmountScrap"] = "";
                DataRowOrdered8["cellSupplier"] = "";
                DataSetTableMovements.Tables[0].Rows.Add(DataRowOrdered8);

                foreach (var items in lista)
                {
                    DataRow DataRowOrdered4 = DataSetTableMovements.Tables[0].NewRow();
                    DataRowOrdered4["cellPartNumber"] = "Departamento";
                    DataRowOrdered4["cellPartDescription"] = items[0].family;
                    DataRowOrdered4["cellStock"] = "";
                    DataRowOrdered4["cellCounted"] = "";
                    DataRowOrdered4["cellDifference"] = "";
                    DataRowOrdered4["cellAdjustment"] = "";
                    DataRowOrdered4["cellUnitPrice"] = "";
                    DataRowOrdered4["cellDifScrap"] = "";
                    DataRowOrdered4["cellDifScrapAmount"] = "";
                    DataRowOrdered4["cellAmountDifference"] = "";
                    DataRowOrdered4["cellDepartment"] = "";
                    DataRowOrdered4["cellScrap"] = "";
                    DataRowOrdered4["cellAmountScrap"] = "";
                    DataRowOrdered4["cellSupplier"] = "";
                    DataSetTableMovements.Tables[0].Rows.Add(DataRowOrdered4);

                    //if(first)
                    //{
                    //    concentrado.Add(new Concentrado
                    //    {
                    //        department = items[0].family,
                    //        amountCounted = amountCounted,
                    //        amountDifference = amountDifference,
                    //        amountScrap = amountScrap,
                    //        amountStock = amountStock
                    //    });
                    //}

                    amountStock = 0;
                    amountCounted = 0;
                    amountDifference = 0;
                    amountScrap = 0;
                    amountDifScrap = 0;

                    DataRow DataRowOrdered3 = DataSetTableMovements.Tables[0].NewRow();
                    DataRowOrdered3["cellPartNumber"] = "Codigo";
                    DataRowOrdered3["cellPartDescription"] = "Descripcion";
                    DataRowOrdered3["cellStock"] = "Stock";
                    DataRowOrdered3["cellCounted"] = "Contado";
                    DataRowOrdered3["cellDifference"] = "Diferencia";
                    DataRowOrdered3["cellAdjustment"] = "Correcciones";
                    DataRowOrdered3["cellUnitPrice"] = "Costo";
                    DataRowOrdered3["cellDifScrap"] = "Dif + Merma";
                    DataRowOrdered3["cellDifScrapAmount"] = "$ Dif + Merma";
                    DataRowOrdered3["cellAmountDifference"] = "$ Diferencia";
                    DataRowOrdered3["cellDepartment"] = "Departamento";
                    DataRowOrdered3["cellScrap"] = "Merma Conocida";
                    DataRowOrdered3["cellAmountScrap"] = "$ Merma Conocida";
                    DataRowOrdered3["cellSupplier"] = "Proveedor";
                    DataSetTableMovements.Tables[0].Rows.Add(DataRowOrdered3);
                    foreach (var item in items)
                    {
                        first = true;
                        amountStock += item.currentAmount;
                        amountCounted += item.detailAmount;
                        amountDifference += item.differenceAmount;
                        amountScrap += item.amountScrap;
                        amountDifScrap += item.difScrapAmount;
                        DataRow DataRowOrdered = DataSetTableMovements.Tables[0].NewRow();
                        DataRowOrdered["cellPartNumber"] = item.part_number;
                        DataRowOrdered["cellPartDescription"] = item.part_description;
                        DataRowOrdered["cellStock"] = Math.Round(item.quantity_current, 2);
                        DataRowOrdered["cellCounted"] = Math.Round(item.quantity_detail, 2);
                        DataRowOrdered["cellDifference"] = Math.Round(item.quantity_difference, 2);
                        DataRowOrdered["cellAdjustment"] = Math.Round(item.quantity_adjusten_difference, 2);
                        DataRowOrdered["cellUnitPrice"] = Math.Round(item.map_price, 2);
                        DataRowOrdered["cellDifScrap"] = Math.Round(item.difScrapQuantity, 2);
                        DataRowOrdered["cellDifScrapAmount"] = Math.Round(item.difScrapAmount, 2).ToString("C");
                        DataRowOrdered["cellAmountDifference"] = Math.Round(item.differenceAmount, 2).ToString("C");
                        DataRowOrdered["cellScrap"] = Math.Round(item.quantityScrap, 2);
                        DataRowOrdered["cellAmountScrap"] = Math.Round(item.amountScrap, 2).ToString("C");
                        DataRowOrdered["cellDepartment"] = item.family;
                        DataRowOrdered["cellSupplier"] = item.supplier;
                        DataSetTableMovements.Tables[0].Rows.Add(DataRowOrdered);
                    }
                    DataRow DataRowOrdered2 = DataSetTableMovements.Tables[0].NewRow();
                    DataRowOrdered2["cellPartNumber"] = "Totales";
                    DataRowOrdered2["cellPartDescription"] = items[0].family;
                    DataRowOrdered2["cellStock"] = "";
                    DataRowOrdered2["cellCounted"] = "";
                    DataRowOrdered2["cellDifference"] = "";
                    DataRowOrdered2["cellAdjustment"] = "";
                    DataRowOrdered2["cellUnitPrice"] = "";
                    DataRowOrdered2["cellDifScrap"] = "";
                    DataRowOrdered2["cellDifScrapAmount"] = Math.Round(amountDifScrap, 2).ToString("C");
                    DataRowOrdered2["cellAmountDifference"] = Math.Round(amountDifference, 2).ToString("C");
                    DataRowOrdered2["cellAmountScrap"] = Math.Round(amountScrap, 2).ToString("C");
                    DataRowOrdered2["cellScrap"] = "";
                    DataRowOrdered2["cellDepartment"] = "";
                    DataRowOrdered2["cellSupplier"] = "";
                    DataSetTableMovements.Tables[0].Rows.Add(DataRowOrdered2);

                    concentrado.Add(new Concentrado
                    {
                        department = items[0].family,
                        amountCounted = amountCounted,
                        amountDifference = amountDifference,
                        amountScrap = amountScrap,
                        amountStock = amountStock,
                        amountDifScrap = amountDifScrap
                    });
                }

                DataSetTableMovements.Tables[0].Rows.Add(DataSetTableMovements.Tables[0].NewRow());

                DataRow DataRowOrdered6 = DataSetTableMovements.Tables[0].NewRow();
                DataRowOrdered6["cellPartNumber"] = "DEPARTAMENTO";
                DataRowOrdered6["cellPartDescription"] = "MERMA CONOCIDA";
                DataRowOrdered6["cellStock"] = "DIF + MERMA";
                DataRowOrdered6["cellCounted"] = "EXACTITUD";
                DataRowOrdered6["cellDifference"] = "";
                DataRowOrdered6["cellAdjustment"] = "";
                DataRowOrdered6["cellUnitPrice"] = "";
                DataRowOrdered6["cellDifScrap"] = "";
                DataRowOrdered6["cellDifScrapAmount"] = DBNull.Value;
                DataRowOrdered6["cellAmountDifference"] = DBNull.Value;
                DataRowOrdered6["cellAmountScrap"] = DBNull.Value;
                DataRowOrdered6["cellScrap"] = "DIFERENCIA";
                DataRowOrdered6["cellDepartment"] = "CONTADO";
                DataRowOrdered6["cellSupplier"] = "STOCK";
                DataSetTableMovements.Tables[0].Rows.Add(DataRowOrdered6);

                foreach (var dep in concentrado)
                {
                    DataRow DataRowOrdered5 = DataSetTableMovements.Tables[0].NewRow();
                    DataRowOrdered5["cellPartNumber"] = dep.department;
                    DataRowOrdered5["cellPartDescription"] = dep.amountScrap.ToString("C");
                    DataRowOrdered5["cellStock"] = dep.amountDifScrap.ToString("C");
                    DataRowOrdered5["cellCounted"] = dep.amountStock != 0 ? Math.Round((dep.amountCounted / dep.amountStock) * 100, 2).ToString() + " % " : "Indefinido";
                    DataRowOrdered5["cellDifference"] = "";
                    DataRowOrdered5["cellAdjustment"] = "";
                    DataRowOrdered5["cellUnitPrice"] = "";
                    DataRowOrdered5["cellDifScrap"] = "";
                    DataRowOrdered5["cellDifScrapAmount"] = DBNull.Value;
                    DataRowOrdered5["cellAmountDifference"] = DBNull.Value;
                    DataRowOrdered5["cellAmountScrap"] = DBNull.Value;
                    DataRowOrdered5["cellScrap"] = dep.amountDifference.ToString("C");
                    DataRowOrdered5["cellDepartment"] = dep.amountCounted.ToString("C");


                    DataRowOrdered5["cellSupplier"] = dep.amountStock.ToString("C");
                    DataSetTableMovements.Tables[0].Rows.Add(DataRowOrdered5);
                }


                cellPartNumber.DataBindings.Add(new XRBinding("Text", null, "xrTableRow1.cellPartNumber"));
                cellPartDescription.DataBindings.Add(new XRBinding("Text", null, "xrTableRow1.cellPartDescription"));
                cellScrap.DataBindings.Add(new XRBinding("Text", null, "xrTableRow1.cellScrap"));
                cellStock.DataBindings.Add(new XRBinding("Text", null, "xrTableRow1.cellStock"));
                cellCounted.DataBindings.Add(new XRBinding("Text", null, "xrTableRow1.cellCounted"));
                cellDifference.DataBindings.Add(new XRBinding("Text", null, "xrTableRow1.cellDifference"));
                cellAdjustment.DataBindings.Add(new XRBinding("Text", null, "xrTableRow1.cellAdjustment"));
                cellUnitPrice.DataBindings.Add(new XRBinding("Text", null, "xrTableRow1.cellUnitPrice"));
                cellDifScrap.DataBindings.Add(new XRBinding("Text", null, "xrTableRow1.cellDifScrap"));
                cellDepartment.DataBindings.Add(new XRBinding("Text", null, "xrTableRow1.cellDepartment"));
                cellAmountScrap.DataBindings.Add(new XRBinding("Text", null, "xrTableRow1.cellAmountScrap"));
                cellDifScrapAmount.DataBindings.Add(new XRBinding("Text", null, "xrTableRow1.cellDifScrapAmount"));
                cellAmountDifference.DataBindings.Add(new XRBinding("Text", null, "xrTableRow1.cellAmountDifference"));
                cellSupplier.DataBindings.Add(new XRBinding("Text", null, "xrTableRow1.cellSupplier"));
                return DataSetTableMovements;
            }
            catch (Exception ex)
            {
                string d = ex.Message;
                throw;
            }

        }

    }
}
public class Concentrado
{
    public string department { get; set; }
    public decimal amountStock { get; set; }
    public decimal amountCounted { get; set; }
    public decimal amountDifference { get; set; }
    public decimal amountScrap { get; set; }
    public decimal amountDifScrap { get; set; }
}