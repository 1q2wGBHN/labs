﻿using System;
using System.Data;
using System.Collections.Generic;
using App.DAL.PurchaseOrder;
using App.Entities;
using DevExpress.XtraReports.UI;
using App.BLL.Supplier;
using App.Entities.ViewModels.PurchaseOrder;
using App.Entities.ViewModels.Sales;

namespace FloridoERPTX.Reports
{
    public partial class RecargasDetailReport : DevExpress.XtraReports.UI.XtraReport
    {
       
        public DataSet DataSetTableOrdered = null;
        decimal ivaValue, comisionValue,totaComisionlValue,TotalValue,TotalRecargasValue = 0;
        public RecargasDetailReport()
        {
            InitializeComponent();
          
        }
        public DataSet printTable(serviciosRecargas _ServiciosRecargas, string date1, string date2, string siteName, string company) //se encarga de imprimir la orden del proveedor
        {
            try
            {

                labelDate.Text = DateTime.Now.Date.ToShortDateString();
                labelDate1.Text = date1;
                labelDate2.Text = date2;
                siteLabel.Text = siteName;
                _labelRecarga.Text = "Recargas de: " + company.ToUpper();




                foreach (var item in _ServiciosRecargas.RecargasDetail)
                {
                    var rowHeight = 15f;

                    var row = new XRTableRow();
                    row.HeightF = rowHeight;

                    var cell1 = new XRTableCell();
                    cell1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
                    cell1.Text = item.fecha;

                    var cell2 = new XRTableCell();
                    cell2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
                    cell2.Text = item.hora;


                    var cell3 = new XRTableCell();
                    cell3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
                    cell3.Text = item.venta.ToString();

                    var cell4 = new XRTableCell();
                    cell4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
                    cell4.Text = item.total.ToString("C");

                    row.Cells.Add(cell1);
                    row.Cells.Add(cell2);
                    row.Cells.Add(cell3);
                    row.Cells.Add(cell4);

                    row.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
                    _tableRecargas.Rows.Add(row);

                    TotalRecargasValue += item.total;

                }

                _SumaTotalRecargas.Text = TotalRecargasValue.ToString("C");


                    return DataSetTableOrdered;
            }
            catch (Exception ex)
            {
                string d=ex.Message;
                throw;
            }
          
        }

       


    }
}
