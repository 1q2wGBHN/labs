﻿using System;
using DevExpress.XtraReports.UI;
using System.Data;
using System.Linq;
using App.Entities.ViewModels.TransactionsCustomers;
using App.Entities.ViewModels.Sales;

namespace FloridoERP.Report
{
    public partial class GiftCardReport : DevExpress.XtraReports.UI.XtraReport
    {
        public DataSet DataSetTableServices = null;       
        public GiftCardReport()
        {
            InitializeComponent();
        }

        public DataSet printTable(GiftCardReportIndex Model)
        {
            try
            {
                DataSetTableServices = new DataSet();
                DataTable DataTableServices = new DataTable();
                DataSetTableServices.Tables.Add(DataTableServices);

                string Word = !Model.IsCharge ? "Compra" : "Cargo";
                string StatusTypeWord = !Model.IsCharge ? "Estado" : "Tipo";
                StartDate.Text = Model.StartDate;
                EndDate.Text = Model.EndDate;
                xrLabel18.Text = string.Format("Monto {0}", Word);
                xrLabel19.Text = string.Format("Fecha {0}", Word);
                xrLabel24.Text = string.Format("Sucursal {0}", Word);
                xrLabel25.Text = string.Format("Caja {0}", Word);
                xrLabel6.Text = string.Format("Venta {0}", Word);
                xrLabel2.Text = StatusTypeWord;
                //totalCharge.Text = "$" + Model.ReportList.Sum(c => c.Charge).ToString("0.00");
                //totalPayment.Text = "$" + Model.ReportList.Sum(c => c.Payment).ToString("0.00");


                DataTableServices.Columns.Add(new DataColumn("CardNumber"));
                DataTableServices.Columns.Add(new DataColumn("Amount"));
                DataTableServices.Columns.Add(new DataColumn("Date"));
                DataTableServices.Columns.Add(new DataColumn("StoreMarket"));
                DataTableServices.Columns.Add(new DataColumn("SaleCheckpoint"));
                DataTableServices.Columns.Add(new DataColumn("SaleId"));
                DataTableServices.Columns.Add(new DataColumn("StatusType"));

                foreach (var item in Model.DataReport)
                {
                    DataRow DataRowOrdered = DataSetTableServices.Tables[0].NewRow();
                    DataRowOrdered["CardNumber"] = item.CardNumber;
                    DataRowOrdered["Amount"] = item.Amount.ToString("C");
                    DataRowOrdered["Date"] = item.Date;//item.Date.ToString("dd/MM/yyyy HH:mm:ss tt");
                    DataRowOrdered["StoreMarket"] = item.StoreMarket;
                    DataRowOrdered["SaleCheckpoint"] = item.SaleCheckpoint;
                    DataRowOrdered["SaleId"] = item.SaleId;
                    DataRowOrdered["StatusType"] = item.StatusType;

                    DataSetTableServices.Tables[0].Rows.Add(DataRowOrdered);
                }

                CardNumber.DataBindings.Add(new XRBinding("Text", null, "xrTableRowFormat.CardNumber"));
                Amount.DataBindings.Add(new XRBinding("Text", null, "xrTableRowFormat.Amount"));
                Date.DataBindings.Add(new XRBinding("Text", null, "xrTableRowFormat.Date"));
                StoreMarket.DataBindings.Add(new XRBinding("Text", null, "xrTableRowFormat.StoreMarket"));
                SaleCheckpoint.DataBindings.Add(new XRBinding("Text", null, "xrTableRowFormat.SaleCheckpoint"));
                SaleId.DataBindings.Add(new XRBinding("Text", null, "xrTableRowFormat.SaleId"));
                StatusType.DataBindings.Add(new XRBinding("Text", null, "xrTableRowFormat.StatusType"));

                return DataSetTableServices;
            }
            catch (Exception ex)
            {
                string d = ex.Message;
                throw;
            }
        }        
    }
}
