﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
using System.Data;
using App.Entities.ViewModels.DamagedsGoods;
using System.Globalization;

namespace FloridoERPTX.Reports
{
    public partial class RMACancelRequest : DevExpress.XtraReports.UI.XtraReport
    {
        public DataSet DataSetTableOrdered = null;
        public RMACancelRequest()
        {
            InitializeComponent();
        }
        public DataSet printTable(DamagedGoodsHeadModel damaged)
        {
            try
            {
                var cultureInfo = CultureInfo.GetCultureInfo("en-US");
                DataSetTableOrdered = new DataSet();

                DataTable DataTableOrdered = new DataTable();
                DataSetTableOrdered.Tables.Add(DataTableOrdered);

                xrLabelSupplier.Text = damaged.Supplier;
                xrLabelDate.Text = DateTime.Now.ToString("dd/MM/yyyy");
                xrLabelFolio.Text = damaged.DamagedGoodsDoc.ToString();
                xrLabelSite.Text = damaged.SiteName;

                DataTableOrdered.Columns.Add(new DataColumn("xrTableCellPartNumber"));
                DataTableOrdered.Columns.Add(new DataColumn("xrTableCellName"));
                DataTableOrdered.Columns.Add(new DataColumn("xrTableCellPrice"));
                DataTableOrdered.Columns.Add(new DataColumn("xrTableCellStorage"));
                DataTableOrdered.Columns.Add(new DataColumn("xrTableCellQuantityRefund"));
                DataTableOrdered.Columns.Add(new DataColumn("xrTableCellAmountRefund"));

                foreach (var item in damaged.DamageGoodsItem)
                {
                    DataRow DataRowOrdered = DataSetTableOrdered.Tables[0].NewRow();
                    DataRowOrdered["xrTableCellPartNumber"] = item.PartNumber;
                    DataRowOrdered["xrTableCellName"] = item.Description;
                    DataRowOrdered["xrTableCellPrice"] = String.Format(cultureInfo, "{0:C4}", item.Price);
                    DataRowOrdered["xrTableCellStorage"] = item.Storage;
                    DataRowOrdered["xrTableCellQuantityRefund"] = item.Quantity.ToString("N2");
                    DataRowOrdered["xrTableCellAmountRefund"] = String.Format(cultureInfo, "{0:C4}", (item.Quantity * item.Price));
                    DataSetTableOrdered.Tables[0].Rows.Add(DataRowOrdered);
                }

                xrTableCellPartNumber.DataBindings.Add(new XRBinding("Text", null, "xrTableRow1.xrTableCellPartNumber"));
                xrTableCellName.DataBindings.Add(new XRBinding("Text", null, "xrTableRow1.xrTableCellName"));
                xrTableCellPrice.DataBindings.Add(new XRBinding("Text", null, "xrTableRow1.xrTableCellPrice"));
                xrTableCellStorage.DataBindings.Add(new XRBinding("Text", null, "xrTableRow1.xrTableCellStorage"));
                xrTableCellQuantityRefund.DataBindings.Add(new XRBinding("Text", null, "xrTableRow1.xrTableCellQuantityRefund"));
                xrTableCellAmountRefund.DataBindings.Add(new XRBinding("Text", null, "xrTableRow1.xrTableCellAmountRefund"));

                return DataSetTableOrdered;
            }
            catch (Exception ex)
            {
                string d = ex.Message;
                throw;
            }
        }
    }
}
