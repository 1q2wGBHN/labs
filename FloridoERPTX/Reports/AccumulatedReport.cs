﻿using System;
using System.Data;
using DevExpress.XtraReports.UI;
using App.Entities.ViewModels.Sales;

namespace FloridoERPTX.Reports
{
    public partial class AccumulatedReport : DevExpress.XtraReports.UI.XtraReport
    {
       
        public DataSet DataSetTableOrdered = null;        
        public AccumulatedReport()
        {
            InitializeComponent();
          
        }
        public DataSet printTable(SalesReportIndex Model) 
        {
            try
            {                                
                DataSetTableOrdered = new DataSet();
                DateLabel.Text = Model.Date;
                siteLabel.Text = Model.SiteName;

                DataTable DataTableOrdered = new DataTable();
                DataSetTableOrdered.Tables.Add(DataTableOrdered);
                
                DataTableOrdered.Columns.Add(new DataColumn("Cashier"));                
                DataTableOrdered.Columns.Add(new DataColumn("Total"));
                DataTableOrdered.Columns.Add(new DataColumn("Date"));

                foreach (var item in Model.AccumulatedData)
                {
                    DataRow DataRowOrdered = DataSetTableOrdered.Tables[0].NewRow();
                    DataRowOrdered["Cashier"] = item.Cashier;
                    DataRowOrdered["Total"] = string.Format("${0}", item.Total.ToString("0.00"));
                    DataRowOrdered["Date"] = item.Date;
                    
                    
                    DataSetTableOrdered.Tables[0].Rows.Add(DataRowOrdered);                                                        
                }               

                Cashier.DataBindings.Add(new XRBinding("Text", null, "xrTableRowFormat.Cashier"));
                Total.DataBindings.Add(new XRBinding("Text", null, "xrTableRowFormat.Total"));
                Date.DataBindings.Add(new XRBinding("Text", null, "xrTableRowFormat.Date"));               
                
                return DataSetTableOrdered;
            }
            catch (Exception ex)
            {
                string d = ex.Message;
                throw;
            }
          
        }       
    }
}
