﻿using System;
using System.Data;
using System.Collections.Generic;
using App.DAL.PurchaseOrder;
using App.Entities;
using DevExpress.XtraReports.UI;
using App.BLL.Supplier;
using App.Entities.ViewModels.PurchaseOrder;
using App.Entities.ViewModels.Sales;

namespace FloridoERPTX.Reports
{
    public partial class SalesReportDlls : DevExpress.XtraReports.UI.XtraReport
    {
       
        public DataSet DataSetTableOrdered = null;
        decimal ivaValue, iepsValue,totalValue,subTotal = 0;
        public SalesReportDlls()
        {
            InitializeComponent();
          
        }
        public DataSet printTable(List<DollarSalesModel> salesList, string date1, string date2, string siteName) //se encarga de imprimir la orden del proveedor
        {
            try
            {

                labelDate.Text = DateTime.Now.Date.ToShortDateString();
                labelDate1.Text = date1;
                labelDate2.Text = date2;
                siteLabel.Text = siteName;
                DataSetTableOrdered = new DataSet();

                DataTable DataTableOrdered = new DataTable();
                DataSetTableOrdered.Tables.Add(DataTableOrdered);

                
               
                 
              
            
                DataTableOrdered.Columns.Add(new DataColumn("sale_date"));
                DataTableOrdered.Columns.Add(new DataColumn("sale_time"));
                DataTableOrdered.Columns.Add(new DataColumn("usuario"));
                DataTableOrdered.Columns.Add(new DataColumn("cobrado"));
                DataTableOrdered.Columns.Add(new DataColumn("recibido"));
                DataTableOrdered.Columns.Add(new DataColumn("tipoCambio"));
                DataTableOrdered.Columns.Add(new DataColumn("sale_id"));
              





                foreach (var item in salesList)
                {
                    DataRow DataRowOrdered = DataSetTableOrdered.Tables[0].NewRow();
                    DataRowOrdered["sale_date"] = item.sale_id;
                    DataRowOrdered["sale_time"] = item.sale_date;
                    DataRowOrdered["usuario"] = item.usuario;
                    DataRowOrdered["cobrado"] = "$" + item.cobrado.ToString("0.00");
                    DataRowOrdered["recibido"] = "$" + item.recibido.ToString("0.00");
                    DataRowOrdered["tipoCambio"] = "$" + item.tipoCambio.ToString("0.00");
                    DataRowOrdered["sale_id"] = "$" + item.sale_id;
                    



                    DataSetTableOrdered.Tables[0].Rows.Add(DataRowOrdered);
                   



                }
               
               
                sale_date.DataBindings.Add(new XRBinding("Text", null, "xrTableRowFormat.sale_date"));
                sale_time.DataBindings.Add(new XRBinding("Text", null, "xrTableRowFormat.sale_time"));
                usuario.DataBindings.Add(new XRBinding("Text", null, "xrTableRowFormat.usuario"));
                cobrado.DataBindings.Add(new XRBinding("Text", null, "xrTableRowFormat.cobrado"));
                recibido.DataBindings.Add(new XRBinding("Text", null, "xrTableRowFormat.recibido"));
                tipoCambio.DataBindings.Add(new XRBinding("Text", null, "xrTableRowFormat.tipoCambio"));
                sale_id.DataBindings.Add(new XRBinding("Text", null, "xrTableRowFormat.sale_id"));
               



                return DataSetTableOrdered;
            }
            catch (Exception ex)
            {
                string d=ex.Message;
                throw;
            }
          
        }

       


    }
}
