﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
using App.Entities.ViewModels.Item;
using System.Collections.Generic;
using System.Data;

namespace FloridoERPTX.Reports
{
    public partial class MissingCodes : DevExpress.XtraReports.UI.XtraReport
    {
        public DataSet DataSetTableMovements = null;
        public MissingCodes()
        {
            InitializeComponent();
        }
        public DataSet printTable(List<ItemSalesInventory> items, string siteName)
        {
            try
            {
                xrLabelSiteName.Text = siteName;
                xrLabelDate.Text = System.DateTime.Now.ToString();
                DataSetTableMovements = new DataSet();

                DataTable table = new DataTable();
                DataSetTableMovements.Tables.Add(table);

                table.Columns.Add(new DataColumn("cellPartNumber"));
                table.Columns.Add(new DataColumn("cellPartDescription"));
                table.Columns.Add(new DataColumn("cellUnrestricted"));
                table.Columns.Add(new DataColumn("cellLastPurchase"));

                foreach (var item in items)
                {
                    DataRow DataRowOrdered = DataSetTableMovements.Tables[0].NewRow();
                    DataRowOrdered["cellPartNumber"] = item.PartNumber;
                    DataRowOrdered["cellPartDescription"] = item.PartDescription;
                    DataRowOrdered["cellUnrestricted"] = item.Unrestricted;
                    DataRowOrdered["cellLastPurchase"] = Convert.ToDateTime(item.LastPurchase).ToString("dd/MM/yyyy") == "01/01/0001" ? "" : Convert.ToDateTime(item.LastPurchase).ToString("dd/MM/yyyy");
                    DataSetTableMovements.Tables[0].Rows.Add(DataRowOrdered);
                }

                cellPartNumber.DataBindings.Add(new XRBinding("Text", null, "xrTableRow1.cellPartNumber"));
                cellPartDescription.DataBindings.Add(new XRBinding("Text", null, "xrTableRow1.cellPartDescription"));
                cellUnrestricted.DataBindings.Add(new XRBinding("Text", null, "xrTableRow1.cellUnrestricted"));
                cellLastPurchase.DataBindings.Add(new XRBinding("Text", null, "xrTableRow1.cellLastPurchase"));

                return DataSetTableMovements;
            }
            catch (Exception ex)
            {
                string d = ex.Message;
                throw;
            }
        }
    }
}
