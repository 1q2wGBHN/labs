﻿using System;
using System.Data;
using System.Collections.Generic;
using App.DAL.PurchaseOrder;
using App.Entities;
using DevExpress.XtraReports.UI;
using App.BLL.Supplier;
using App.Entities.ViewModels.PurchaseOrder;
using App.Entities.ViewModels.Sales;

namespace FloridoERPTX.Reports
{
    public partial class SalesReport : DevExpress.XtraReports.UI.XtraReport
    {
       
        public DataSet DataSetTableOrdered = null;
        decimal ivaValue, iepsValue,totalValue,subTotal = 0;
        public SalesReport()
        {
            InitializeComponent();
          
        }
        public DataSet printTable(List<SalesModel> salesList, string date1, string date2, string siteName) //se encarga de imprimir la orden del proveedor
        {
            try
            {

                labelDate.Text = DateTime.Now.Date.ToShortDateString();
                labelDate1.Text = date1;
                labelDate2.Text = date2;
                siteLabel.Text = siteName;
                DataSetTableOrdered = new DataSet();

                DataTable DataTableOrdered = new DataTable();
                DataSetTableOrdered.Tables.Add(DataTableOrdered);

                
               
                 
                DataTableOrdered.Columns.Add(new DataColumn("sale_id"));
                DataTableOrdered.Columns.Add(new DataColumn("sale_date"));
                DataTableOrdered.Columns.Add(new DataColumn("sale_time"));
                DataTableOrdered.Columns.Add(new DataColumn("sale_total"));
                DataTableOrdered.Columns.Add(new DataColumn("sale_iva"));
                DataTableOrdered.Columns.Add(new DataColumn("sale_ieps"));
                DataTableOrdered.Columns.Add(new DataColumn("sale_factura"));
                DataTableOrdered.Columns.Add(new DataColumn("sale_importe"));
                DataTableOrdered.Columns.Add(new DataColumn("tipo"));
                DataTableOrdered.Columns.Add(new DataColumn("customer"));





                foreach (var item in salesList)
                {
                    DataRow DataRowOrdered = DataSetTableOrdered.Tables[0].NewRow();
                    DataRowOrdered["sale_id"] = item.sale_id;
                    DataRowOrdered["sale_date"] = item.sale_date;
                    DataRowOrdered["sale_time"] = item.sale_time;
                    DataRowOrdered["sale_importe"] = "$" + item.sale_importe.ToString("0.00");
                    DataRowOrdered["sale_total"] = "$" + item.sale_total.ToString("0.00");
                    DataRowOrdered["sale_iva"] = "$" + item.sale_iva.ToString("0.00");
                    DataRowOrdered["sale_ieps"] = "$" + item.sale_ieps.ToString("0.00");
                    DataRowOrdered["sale_factura"] = item.sale_factura;
                    DataRowOrdered["tipo"] = item.tipo;
                    DataRowOrdered["customer"] = item.customer;



                    DataSetTableOrdered.Tables[0].Rows.Add(DataRowOrdered);
                    ivaValue += item.sale_iva;
                    iepsValue += item.sale_ieps;
                    subTotal += item.sale_importe;
                    totalValue += item.sale_total;



                }
               
                ivaLabel.Text = "$"+ivaValue.ToString("0.00");
                iepsLabel.Text = "$" + iepsValue.ToString("0.00");
                subtotalLabel.Text = "$" + subTotal.ToString("0.00");
                totalLabel.Text = "$" + totalValue.ToString("0.00");
                sale_id.DataBindings.Add(new XRBinding("Text", null, "xrTableRowFormat.sale_id"));
                sale_date.DataBindings.Add(new XRBinding("Text", null, "xrTableRowFormat.sale_date"));
                sale_time.DataBindings.Add(new XRBinding("Text", null, "xrTableRowFormat.sale_time"));
                sale_total.DataBindings.Add(new XRBinding("Text", null, "xrTableRowFormat.sale_total"));
                sale_iva.DataBindings.Add(new XRBinding("Text", null, "xrTableRowFormat.sale_iva"));
                sale_ieps.DataBindings.Add(new XRBinding("Text", null, "xrTableRowFormat.sale_ieps"));
                sale_factura.DataBindings.Add(new XRBinding("Text", null, "xrTableRowFormat.sale_factura"));
                sale_importe.DataBindings.Add(new XRBinding("Text", null, "xrTableRowFormat.sale_importe"));
                tipo.DataBindings.Add(new XRBinding("Text", null, "xrTableRowFormat.tipo"));
                customer.DataBindings.Add(new XRBinding("Text", null, "xrTableRowFormat.customer"));




                return DataSetTableOrdered;
            }
            catch (Exception ex)
            {
                string d=ex.Message;
                throw;
            }
          
        }

       


    }
}
