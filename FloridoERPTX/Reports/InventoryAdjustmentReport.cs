﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
using System.Data;
using App.Entities.ViewModels.Inventory;
using System.Collections.Generic;

namespace FloridoERPTX.Reports
{
    public partial class InventoryAdjustmentReport : DevExpress.XtraReports.UI.XtraReport
    {
        public DataSet DataSetTableMovements = null;
        public InventoryAdjustmentReport()
        {
            InitializeComponent();
        }

        private void xrPanel1_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {

        }
        public DataSet printTable(List<InventoryAdjustmentModel> items, string siteName, string id , string inventoryDesc)
        {
            try
            {
                DataSetTableMovements = new DataSet();

                DataTable table = new DataTable();
                DataSetTableMovements.Tables.Add(table);

                xrLabelSiteName.Text = siteName;
                xrLabelInventoryDesc.Text = inventoryDesc;
                xrLabelInventoryNumber.Text = id;

                table.Columns.Add(new DataColumn("xrTablePartNumber"));
                table.Columns.Add(new DataColumn("xrTablePartDescription"));
                table.Columns.Add(new DataColumn("xrTableTheory"));
                table.Columns.Add(new DataColumn("xrTableDifference"));
                table.Columns.Add(new DataColumn("xrTableQuantity"));
                table.Columns.Add(new DataColumn("xrTableNewQuantity"));
                table.Columns.Add(new DataColumn("xrTableComment"));

                foreach (var item in items)
                {
                    DataRow DataRowOrdered = DataSetTableMovements.Tables[0].NewRow();
                    DataRowOrdered["xrTablePartNumber"] = item.part_number;
                    DataRowOrdered["xrTablePartDescription"] = item.part_description;
                    DataRowOrdered["xrTableTheory"] = item.quantity_stock;
                    DataRowOrdered["xrTableDifference"] = item.quantity_difference;
                    DataRowOrdered["xrTableQuantity"] = item.previous_quantity;
                    DataRowOrdered["xrTableNewQuantity"] = item.new_quantity;
                    DataRowOrdered["xrTableComment"] = item.justification;
                    DataSetTableMovements.Tables[0].Rows.Add(DataRowOrdered);
                }

                xrTablePartNumber.DataBindings.Add(new XRBinding("Text", null, "xrTableRow1.xrTablePartNumber"));
                xrTablePartDescription.DataBindings.Add(new XRBinding("Text", null, "xrTableRow1.xrTablePartDescription"));
                xrTableTheory.DataBindings.Add(new XRBinding("Text", null, "xrTableRow1.xrTableTheory"));
                xrTableDifference.DataBindings.Add(new XRBinding("Text", null, "xrTableRow1.xrTableDifference"));
                xrTableQuantity.DataBindings.Add(new XRBinding("Text", null, "xrTableRow1.xrTableQuantity"));
                xrTableNewQuantity.DataBindings.Add(new XRBinding("Text", null, "xrTableRow1.xrTableNewQuantity"));
                xrTableComment.DataBindings.Add(new XRBinding("Text", null, "xrTableRow1.xrTableComment"));

                return DataSetTableMovements;
            }
            catch (Exception ex)
            {
                string d = ex.Message;
                throw;
            }
        }

    }
}
