﻿using System;
using System.Data;
using DevExpress.XtraReports.UI;
using App.Entities.ViewModels.Quotes;

namespace FloridoERPTX.Reports
{
    public partial class QuotesReport : DevExpress.XtraReports.UI.XtraReport
    {
       
        public DataSet DataSetTableOrdered = null;        
        public QuotesReport()
        {
            InitializeComponent();
          
        }
        public DataSet printTable(QuoteModel Model) 
        {
            try
            {
                Date.Text = DateTime.Now.Date.ToString("dd/MM/yyyy");
                QuoteNo.Text = string.Format("{0}{1}", Model.QuoteSerie, Model.QuoteNo);
                ClientName.Text = Model.ClientName;
                ClientAddress.Text = Model.Address;
                ClientPhone.Text = Model.PhoneNumber;
                ClientEmail.Text = Model.Email;
                Seller.Text = Model.Seller;

                DataSetTableOrdered = new DataSet();

                DataTable DataTableOrdered = new DataTable();
                DataSetTableOrdered.Tables.Add(DataTableOrdered);
                
                DataTableOrdered.Columns.Add(new DataColumn("Quantity"));
                DataTableOrdered.Columns.Add(new DataColumn("Stock"));
                DataTableOrdered.Columns.Add(new DataColumn("ItemNumber"));
                DataTableOrdered.Columns.Add(new DataColumn("UnitMeasure"));
                DataTableOrdered.Columns.Add(new DataColumn("ItemDescription"));
                DataTableOrdered.Columns.Add(new DataColumn("UnitPrice"));
                DataTableOrdered.Columns.Add(new DataColumn("Price"));                

                foreach (var item in Model.ItemsList)
                {
                    DataRow DataRowOrdered = DataSetTableOrdered.Tables[0].NewRow();
                    DataRowOrdered["Quantity"] = item.Quantity;
                    DataRowOrdered["Stock"] = item.Stock;
                    DataRowOrdered["ItemNumber"] = item.ItemNumber;
                    DataRowOrdered["UnitMeasure"] =  item.UnitMeasure;
                    DataRowOrdered["ItemDescription"] = item.ItemDescription;                    
                    DataRowOrdered["UnitPrice"] =  item.BasePrice;                    
                    DataRowOrdered["Price"] = item.QuotePrice;
                    
                    DataSetTableOrdered.Tables[0].Rows.Add(DataRowOrdered);                                                        
                }


                subTotal.Text = string.Format("{0}{1}", "$", Model.SubTotal.ToString("0.00"));
                tax.Text = string.Format("{0}{1}", "$", Model.Tax.ToString("0.00"));
                total.Text = string.Format("{0}{1}", "$", Model.Total.ToString("0.00"));

                Quantity.DataBindings.Add(new XRBinding("Text", null, "xrTableRowFormat.Quantity"));
                Stock.DataBindings.Add(new XRBinding("Text", null, "xrTableRowFormat.Stock"));
                ItemNumber.DataBindings.Add(new XRBinding("Text", null, "xrTableRowFormat.ItemNumber"));
                UnitMeasure.DataBindings.Add(new XRBinding("Text", null, "xrTableRowFormat.UnitMeasure"));
                ItemDescription.DataBindings.Add(new XRBinding("Text", null, "xrTableRowFormat.ItemDescription"));
                UnitPrice.DataBindings.Add(new XRBinding("Text", null, "xrTableRowFormat.UnitPrice"));                
                Price.DataBindings.Add(new XRBinding("Text", null, "xrTableRowFormat.Price"));
                
                return DataSetTableOrdered;
            }
            catch (Exception ex)
            {
                string d=ex.Message;
                throw;
            }
          
        }       
    }
}
