﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
using System.Data;
using App.Entities.ViewModels.Android;
using System.Collections.Generic;

namespace FloridoERPTX.Reports
{
    public partial class SolicitudAprobacionSalidaMerma : DevExpress.XtraReports.UI.XtraReport
    {
        public DataSet DataSetTableOrdered = null;
        public SolicitudAprobacionSalidaMerma()
        {
            InitializeComponent();
        }
        public DataSet printTable(List<ScrapControlAndroidModel> scrapItems, string folio, string siteName)
        {
            try
            {
                DataSetTableOrdered = new DataSet();

                DataTable DataTableOrdered = new DataTable();
                DataSetTableOrdered.Tables.Add(DataTableOrdered);
                
                xrLabelDate.Text = DateTime.Now.ToString("dd/MM/yyyy");
                xrLabelDocumentNumber.Text = folio;
                xrLabelSite.Text = siteName;

                DataTableOrdered.Columns.Add(new DataColumn("xrTableCellPartNumber"));
                DataTableOrdered.Columns.Add(new DataColumn("xrTableCellPartDescription"));
                DataTableOrdered.Columns.Add(new DataColumn("xrTableCellQuantity"));
                DataTableOrdered.Columns.Add(new DataColumn("xrTableCellScrapReason"));

                foreach (var item in scrapItems)
                {
                    DataRow DataRowOrdered = DataSetTableOrdered.Tables[0].NewRow();
                    //Remover despues de "-", ya que incluye la descripción mas la causa (separa por un guión "-")
                    int index = item.part_description.IndexOf("-");
                    if (index > 0)
                        item.part_description = item.part_description.Substring(0, index);
                    DataRowOrdered["xrTableCellPartNumber"] = item.part_number;
                    DataRowOrdered["xrTableCellPartDescription"] = item.part_description;
                    DataRowOrdered["xrTableCellQuantity"] = item.quantity;
                    DataRowOrdered["xrTableCellScrapReason"] = item.comment;
                    DataSetTableOrdered.Tables[0].Rows.Add(DataRowOrdered);
                }

                xrTableCellPartNumber.DataBindings.Add(new XRBinding("Text", null, "xrTableRow1.xrTableCellPartNumber"));
                xrTableCellPartDescription.DataBindings.Add(new XRBinding("Text", null, "xrTableRow1.xrTableCellPartDescription"));
                xrTableCellQuantity.DataBindings.Add(new XRBinding("Text", null, "xrTableRow1.xrTableCellQuantity"));
                xrTableCellScrapReason.DataBindings.Add(new XRBinding("Text", null, "xrTableRow1.xrTableCellScrapReason"));

                return DataSetTableOrdered;
            }
            catch (Exception ex)
            {
                string d = ex.Message;
                throw;
            }

        }
    }
}
