﻿using System;
using System.Data;
using DevExpress.XtraReports.UI;
using App.Entities.ViewModels.Invoices;

namespace FloridoERPTX.Reports
{
    public partial class InvoicesReport : DevExpress.XtraReports.UI.XtraReport
    {

        public DataSet DataSetTableOrdered = null;
        decimal ivaValue, iepsValue, totalValue, subTotal = 0;
        public InvoicesReport()
        {
            InitializeComponent();

        }

        public DataSet printTable(InvoiceIndex model, string siteName) //se encarga de imprimir la orden del proveedor
        {
            try
            {
                labelDate.Text = DateTime.Now.Date.ToShortDateString();
                labelDate1.Text = model.StartDate;
                labelDate2.Text = model.EndDate;
                siteLabel.Text = siteName;

                lbl_credit_zrate.Text = model.Totales[0, 0].ToString("C");
                lbl_credit_taxed.Text = model.Totales[1, 0].ToString("C");
                _exeCredit.Text = model.Totales[2, 0].ToString("C");
                lbl_credit_tax.Text = model.Totales[3, 0].ToString("C");
                lbl_credit_ieps.Text = model.Totales[4, 0].ToString("C");
                lbl_total_credit.Text = model.Totales[5, 0].ToString("C");

                lbl_cash_zrate.Text = model.Totales[0, 1].ToString("C");
                lbl_cash_taxed.Text = model.Totales[1, 1].ToString("C");
                _exeCash.Text = model.Totales[2, 1].ToString("C");
                lbl_cash_tax.Text = model.Totales[3, 1].ToString("C");
                lbl_cash_ieps.Text = model.Totales[4, 1].ToString("C");
                lbl_total_cash.Text = model.Totales[5, 1].ToString("C");

                lbl_total_zrate.Text = model.Totales[0, 2].ToString("C");
                lbl_total_taxed.Text = model.Totales[1, 2].ToString("C");
                _exeTotal.Text = model.Totales[2, 2].ToString("C");
                lbl_total_tax.Text = model.Totales[3, 2].ToString("C");
                lbl_total_ieps.Text = model.Totales[4, 2].ToString("C");
                lbl_total_global.Text = model.Totales[5, 2].ToString("C");

                DataSetTableOrdered = new DataSet();

                DataTable DataTableOrdered = new DataTable();
                DataSetTableOrdered.Tables.Add(DataTableOrdered);

                DataTableOrdered.Columns.Add(new DataColumn("InvoiceNumber"));
                DataTableOrdered.Columns.Add(new DataColumn("CreateDate"));
                DataTableOrdered.Columns.Add(new DataColumn("CustomerCode"));
                DataTableOrdered.Columns.Add(new DataColumn("CustomerName"));
                DataTableOrdered.Columns.Add(new DataColumn("DueDate"));
                DataTableOrdered.Columns.Add(new DataColumn("Amount"));
                DataTableOrdered.Columns.Add(new DataColumn("IepsPredicateReport"));
                DataTableOrdered.Columns.Add(new DataColumn("Ieps"));
                DataTableOrdered.Columns.Add(new DataColumn("Tax"));
                DataTableOrdered.Columns.Add(new DataColumn("TotalAmount"));
                DataTableOrdered.Columns.Add(new DataColumn("TotalCredit"));
                DataTableOrdered.Columns.Add(new DataColumn("TotalCash"));
                DataTableOrdered.Columns.Add(new DataColumn("Balance"));
                DataTableOrdered.Columns.Add(new DataColumn("CurrencyCode"));
                DataTableOrdered.Columns.Add(new DataColumn("FiscalNumberReport"));
                DataTableOrdered.Columns.Add(new DataColumn("Status"));
                DataTableOrdered.Columns.Add(new DataColumn("PaymentMethod"));

                foreach (var item in model.InvoiceList)
                {
                    DataRow DataRowOrdered = DataSetTableOrdered.Tables[0].NewRow();

                    DataRowOrdered["InvoiceNumber"] = item.InvoiceNumber;
                    DataRowOrdered["CustomerName"] = item.CustomerName;
                    DataRowOrdered["CreateDate"] = item.CreateDate;
                    DataRowOrdered["CustomerCode"] = item.CustomerCode;
                    DataRowOrdered["DueDate"] = item.DueDate;
                    DataRowOrdered["Amount"] = item.Amount.ToString("C");
                    DataRowOrdered["IepsPredicateReport"] = item.IepsPredicateReport != null ? item.IepsPredicateReport.Replace("@", "\n") : "";
                    DataRowOrdered["Ieps"] = item.Ieps.ToString("C"); ;
                    DataRowOrdered["Tax"] = item.Tax.ToString("C"); ;
                    DataRowOrdered["TotalAmount"] = item.TotalAmount.ToString("C");
                    DataRowOrdered["TotalCredit"] = item.TotalCredit.ToString("C"); ;
                    DataRowOrdered["TotalCash"] = item.TotalCash.ToString("C"); ;
                    DataRowOrdered["Balance"] = item.Balance;
                    DataRowOrdered["CurrencyCode"] = item.CurrencyCode;
                    DataRowOrdered["FiscalNumberReport"] = item.FiscalNumberReport != null ? item.FiscalNumberReport.Replace("@", Environment.NewLine) : "";
                    DataRowOrdered["Status"] = item.Status;
                    DataRowOrdered["PaymentMethod"] = item.CreditOrCash;

                    var ieps = item.IepsPredicateReport != null ? item.IepsPredicateReport.Replace("@", Environment.NewLine) : "";
                    DataSetTableOrdered.Tables[0].Rows.Add(DataRowOrdered);
                    ivaValue += item.Tax;
                    iepsValue += item.Ieps;
                    subTotal += item.Amount;
                    totalValue += item.TotalAmount;
                }

                ivaLabel.Text = ivaValue.ToString("C");
                iepsLabel.Text = iepsValue.ToString("C");
                subtotalLabel.Text = subTotal.ToString("C");
                totalLabel.Text = totalValue.ToString("C");

                InvoiceNumber.DataBindings.Add(new XRBinding("Text", null, "xrTableRowFormat.InvoiceNumber"));
                CustomerName.DataBindings.Add(new XRBinding("Text", null, "xrTableRowFormat.CustomerName"));
                CreateDate.DataBindings.Add(new XRBinding("Text", null, "xrTableRowFormat.CreateDate"));
                CustomerCode.DataBindings.Add(new XRBinding("Text", null, "xrTableRowFormat.CustomerCode"));
                DueDate.DataBindings.Add(new XRBinding("Text", null, "xrTableRowFormat.DueDate"));
                Amount.DataBindings.Add(new XRBinding("Text", null, "xrTableRowFormat.Amount"));
                IepsPredicate.DataBindings.Add(new XRBinding("Text", null, "xrTableRowFormat.IepsPredicateReport"));
                Ieps.DataBindings.Add(new XRBinding("Text", null, "xrTableRowFormat.Ieps"));
                Tax.DataBindings.Add(new XRBinding("Text", null, "xrTableRowFormat.Tax"));
                TotalAmount.DataBindings.Add(new XRBinding("Text", null, "xrTableRowFormat.TotalAmount"));
                TotalCredit.DataBindings.Add(new XRBinding("Text", null, "xrTableRowFormat.TotalCredit"));
                TotalCash.DataBindings.Add(new XRBinding("Text", null, "xrTableRowFormat.TotalCash"));
                Balance.DataBindings.Add(new XRBinding("Text", null, "xrTableRowFormat.Balance"));
                CurrencyCode.DataBindings.Add(new XRBinding("Text", null, "xrTableRowFormat.CurrencyCode"));
                FiscalNumber.DataBindings.Add(new XRBinding("Text", null, "xrTableRowFormat.FiscalNumberReport"));
                Status.DataBindings.Add(new XRBinding("Text", null, "xrTableRowFormat.Status"));
                PaymentMethod.DataBindings.Add(new XRBinding("Text", null, "xrTableRowFormat.PaymentMethod"));

                return DataSetTableOrdered;
            }
            catch (Exception ex)
            {
                string d = ex.Message;
                throw;
            }
        }
    }
}
