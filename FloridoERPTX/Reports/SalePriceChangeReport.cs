﻿using System;
using System.Data;
using DevExpress.XtraReports.UI;
using App.Entities.ViewModels.SalesPriceChange;

namespace FloridoERPTX.Reports
{
    public partial class SalePriceChangeReport : DevExpress.XtraReports.UI.XtraReport
    {
       
        public DataSet DataSetTableOrdered = null;        
        public SalePriceChangeReport()
        {
            InitializeComponent();
          
        }
        public DataSet printTable(SalePriceChangeIndex Model) 
        {
            try
            {
                StartDate.Text = Model.StartDate;
                EndDate.Text = Model.EndDate;

                DataSetTableOrdered = new DataSet();

                DataTable DataTableOrdered = new DataTable();
                DataSetTableOrdered.Tables.Add(DataTableOrdered);
                
                DataTableOrdered.Columns.Add(new DataColumn("ItemNumber"));
                DataTableOrdered.Columns.Add(new DataColumn("ItemDescription"));
                DataTableOrdered.Columns.Add(new DataColumn("Quantity"));
                DataTableOrdered.Columns.Add(new DataColumn("OriginalPrice"));
                DataTableOrdered.Columns.Add(new DataColumn("TotalOriginalPrice"));
                DataTableOrdered.Columns.Add(new DataColumn("NewPrice"));
                DataTableOrdered.Columns.Add(new DataColumn("TotalNewPrice"));
                DataTableOrdered.Columns.Add(new DataColumn("Difference"));
                DataTableOrdered.Columns.Add(new DataColumn("Percentage"));
                DataTableOrdered.Columns.Add(new DataColumn("SaleNumber"));
                DataTableOrdered.Columns.Add(new DataColumn("EmployeeName"));
                DataTableOrdered.Columns.Add(new DataColumn("Concept"));
                DataTableOrdered.Columns.Add(new DataColumn("Date"));

                foreach (var item in Model.DataReport)
                {
                    DataRow DataRowOrdered = DataSetTableOrdered.Tables[0].NewRow();
                    DataRowOrdered["ItemNumber"] = item.ItemNumber;
                    DataRowOrdered["ItemDescription"] = item.ItemDescription;
                    DataRowOrdered["Quantity"] = item.Quantity;
                    DataRowOrdered["OriginalPrice"] = "$" + item.OriginalPrice.ToString("0.00");
                    DataRowOrdered["TotalOriginalPrice"] = "$" + item.TotalOriginalPrice.ToString("0.00");
                    DataRowOrdered["NewPrice"] = "$" + item.NewPrice.ToString("0.00");
                    DataRowOrdered["TotalNewPrice"] = "$" + item.TotalNewPrice.ToString("0.00"); 
                    DataRowOrdered["Difference"] = "$" + item.Difference.ToString("0.00"); 
                    DataRowOrdered["Percentage"] = item.Percentage.ToString("0.00") + "%";
                    DataRowOrdered["SaleNumber"] = item.SaleNumber;
                    DataRowOrdered["EmployeeName"] = item.EmployeeName;
                    DataRowOrdered["Concept"] = item.Concept;
                    DataRowOrdered["Date"] = item.Date;

                    DataSetTableOrdered.Tables[0].Rows.Add(DataRowOrdered);
                }


                ItemNumber.DataBindings.Add(new XRBinding("Text", null, "xrTableRowFormat.ItemNumber"));
                ItemDescription.DataBindings.Add(new XRBinding("Text", null, "xrTableRowFormat.ItemDescription"));
                Quantity.DataBindings.Add(new XRBinding("Text", null, "xrTableRowFormat.Quantity"));
                OriginalPrice.DataBindings.Add(new XRBinding("Text", null, "xrTableRowFormat.OriginalPrice"));
                TotalOriginalPrice.DataBindings.Add(new XRBinding("Text", null, "xrTableRowFormat.TotalOriginalPrice"));
                NewPrice.DataBindings.Add(new XRBinding("Text", null, "xrTableRowFormat.NewPrice"));
                TotalNewPrice.DataBindings.Add(new XRBinding("Text", null, "xrTableRowFormat.TotalNewPrice"));
                Difference.DataBindings.Add(new XRBinding("Text", null, "xrTableRowFormat.Difference"));
                Percentage.DataBindings.Add(new XRBinding("Text", null, "xrTableRowFormat.Percentage"));
                SaleNumber.DataBindings.Add(new XRBinding("Text", null, "xrTableRowFormat.SaleNumber"));
                EmployeeName.DataBindings.Add(new XRBinding("Text", null, "xrTableRowFormat.EmployeeName"));
                Concept.DataBindings.Add(new XRBinding("Text", null, "xrTableRowFormat.Concept"));
                Date.DataBindings.Add(new XRBinding("Text", null, "xrTableRowFormat.Date"));

                return DataSetTableOrdered;
            }
            catch (Exception ex)
            {
                string d=ex.Message;
                throw;
            }
          
        }       
    }
}
