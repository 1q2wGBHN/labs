﻿namespace FloridoERPTX.Reports
{
    partial class ExpenseConcentratedReport
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ExpenseConcentratedReport));
            this.Detail = new DevExpress.XtraReports.UI.DetailBand();
            this.TopMargin = new DevExpress.XtraReports.UI.TopMarginBand();
            this.BottomMargin = new DevExpress.XtraReports.UI.BottomMarginBand();
            this.PageHeader = new DevExpress.XtraReports.UI.PageHeaderBand();
            this.xrlblAreaShow = new DevExpress.XtraReports.UI.XRLabel();
            this.xrlblCurrencyUp = new DevExpress.XtraReports.UI.XRLabel();
            this.xrlblStatusShow = new DevExpress.XtraReports.UI.XRLabel();
            this.xrlblStatus = new DevExpress.XtraReports.UI.XRLabel();
            this.xrlblSupplierUpShow = new DevExpress.XtraReports.UI.XRLabel();
            this.xrlblSupplierUp = new DevExpress.XtraReports.UI.XRLabel();
            this.xrlblDateEndShow = new DevExpress.XtraReports.UI.XRLabel();
            this.xrlblDateEnd = new DevExpress.XtraReports.UI.XRLabel();
            this.xrlblDateStartShow = new DevExpress.XtraReports.UI.XRLabel();
            this.xrlblDateStart = new DevExpress.XtraReports.UI.XRLabel();
            this.xrlblCategory = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTypeExpense = new DevExpress.XtraReports.UI.XRLabel();
            this.xrlblDate = new DevExpress.XtraReports.UI.XRLabel();
            this.xrlblCurrency = new DevExpress.XtraReports.UI.XRLabel();
            this.xraddress = new DevExpress.XtraReports.UI.XRLabel();
            this.xrlblCurrencyUpShow = new DevExpress.XtraReports.UI.XRLabel();
            this.xrPictureBox1 = new DevExpress.XtraReports.UI.XRPictureBox();
            this.xrLabel3 = new DevExpress.XtraReports.UI.XRLabel();
            this.siteLabel = new DevExpress.XtraReports.UI.XRLabel();
            this.xrRFC = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel5 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrlblArea = new DevExpress.XtraReports.UI.XRLabel();
            this.xrState = new DevExpress.XtraReports.UI.XRLabel();
            this.xrlblType = new DevExpress.XtraReports.UI.XRLabel();
            this.xrlblTypeShow = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel7 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel6 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel4 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrlblSubtotal = new DevExpress.XtraReports.UI.XRLabel();
            this.xrlblIEPs = new DevExpress.XtraReports.UI.XRLabel();
            this.xrlblIVA = new DevExpress.XtraReports.UI.XRLabel();
            this.xrlblImport = new DevExpress.XtraReports.UI.XRLabel();
            this.xrlblSupplier = new DevExpress.XtraReports.UI.XRLabel();
            this.xrlblFolio = new DevExpress.XtraReports.UI.XRLabel();
            this.DetailReport = new DevExpress.XtraReports.UI.DetailReportBand();
            this.Detail1 = new DevExpress.XtraReports.UI.DetailBand();
            this.xrTablePurchase = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow1 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrCellInvoice = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrCellSupplier = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrCellCategory = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrCellType = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrCellCurrecency = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrCellDate = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrCellSubtotal = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrCellIVA = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrCellIEPS = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrCellDiscount = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrCellISRR = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrCellIVAR = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrCellImport = new DevExpress.XtraReports.UI.XRTableCell();
            this.GroupFooter1 = new DevExpress.XtraReports.UI.GroupFooterBand();
            this.xrlblUsdImport = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel18 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrlblUsdISRR = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel25 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrlblUsdIVAR = new DevExpress.XtraReports.UI.XRLabel();
            this.xrlblUsdDiscount = new DevExpress.XtraReports.UI.XRLabel();
            this.xrlblUsdIEPS = new DevExpress.XtraReports.UI.XRLabel();
            this.xrlblUsdIVA = new DevExpress.XtraReports.UI.XRLabel();
            this.xrlblUsdSubtotal = new DevExpress.XtraReports.UI.XRLabel();
            this.xrlblMxnSubtotal = new DevExpress.XtraReports.UI.XRLabel();
            this.xrlblMxnIVA = new DevExpress.XtraReports.UI.XRLabel();
            this.xrlblMxnIEPS = new DevExpress.XtraReports.UI.XRLabel();
            this.xrlblMxnDiscount = new DevExpress.XtraReports.UI.XRLabel();
            this.xrlblMxnImport = new DevExpress.XtraReports.UI.XRLabel();
            this.xrlblMxnIVAR = new DevExpress.XtraReports.UI.XRLabel();
            this.xrlblMxnISRR = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel20 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel19 = new DevExpress.XtraReports.UI.XRLabel();
            this.PageFooter = new DevExpress.XtraReports.UI.PageFooterBand();
            this.xrPageInfo1 = new DevExpress.XtraReports.UI.XRPageInfo();
            this.xrLabel1 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrCellFolio = new DevExpress.XtraReports.UI.XRTableCell();
            ((System.ComponentModel.ISupportInitialize)(this.xrTablePurchase)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // Detail
            // 
            this.Detail.Dpi = 100F;
            this.Detail.HeightF = 0F;
            this.Detail.Name = "Detail";
            this.Detail.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.Detail.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // TopMargin
            // 
            this.TopMargin.Dpi = 100F;
            this.TopMargin.HeightF = 20.375F;
            this.TopMargin.Name = "TopMargin";
            this.TopMargin.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.TopMargin.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // BottomMargin
            // 
            this.BottomMargin.Dpi = 100F;
            this.BottomMargin.HeightF = 15.04161F;
            this.BottomMargin.Name = "BottomMargin";
            this.BottomMargin.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.BottomMargin.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // PageHeader
            // 
            this.PageHeader.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel1,
            this.xrlblAreaShow,
            this.xrlblCurrencyUp,
            this.xrlblStatusShow,
            this.xrlblStatus,
            this.xrlblSupplierUpShow,
            this.xrlblSupplierUp,
            this.xrlblDateEndShow,
            this.xrlblDateEnd,
            this.xrlblDateStartShow,
            this.xrlblDateStart,
            this.xrlblCategory,
            this.xrTypeExpense,
            this.xrlblDate,
            this.xrlblCurrency,
            this.xraddress,
            this.xrlblCurrencyUpShow,
            this.xrPictureBox1,
            this.xrLabel3,
            this.siteLabel,
            this.xrRFC,
            this.xrLabel5,
            this.xrlblArea,
            this.xrState,
            this.xrlblType,
            this.xrlblTypeShow,
            this.xrLabel7,
            this.xrLabel6,
            this.xrLabel4,
            this.xrlblSubtotal,
            this.xrlblIEPs,
            this.xrlblIVA,
            this.xrlblImport,
            this.xrlblSupplier,
            this.xrlblFolio});
            this.PageHeader.Dpi = 100F;
            this.PageHeader.HeightF = 158.9515F;
            this.PageHeader.Name = "PageHeader";
            // 
            // xrlblAreaShow
            // 
            this.xrlblAreaShow.Dpi = 100F;
            this.xrlblAreaShow.Font = new System.Drawing.Font("Arial", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrlblAreaShow.LocationFloat = new DevExpress.Utils.PointFloat(111.0637F, 72.57813F);
            this.xrlblAreaShow.Name = "xrlblAreaShow";
            this.xrlblAreaShow.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrlblAreaShow.SizeF = new System.Drawing.SizeF(358.447F, 9.999977F);
            this.xrlblAreaShow.StylePriority.UseFont = false;
            this.xrlblAreaShow.Text = "TODAS LAS AREAS";
            // 
            // xrlblCurrencyUp
            // 
            this.xrlblCurrencyUp.Dpi = 100F;
            this.xrlblCurrencyUp.Font = new System.Drawing.Font("Arial", 7F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrlblCurrencyUp.LocationFloat = new DevExpress.Utils.PointFloat(29.79F, 118.28F);
            this.xrlblCurrencyUp.Name = "xrlblCurrencyUp";
            this.xrlblCurrencyUp.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrlblCurrencyUp.SizeF = new System.Drawing.SizeF(80.20833F, 9.999992F);
            this.xrlblCurrencyUp.StylePriority.UseFont = false;
            this.xrlblCurrencyUp.Text = "Moneda:";
            // 
            // xrlblStatusShow
            // 
            this.xrlblStatusShow.Dpi = 100F;
            this.xrlblStatusShow.Font = new System.Drawing.Font("Arial", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrlblStatusShow.LocationFloat = new DevExpress.Utils.PointFloat(111.0636F, 102.28F);
            this.xrlblStatusShow.Name = "xrlblStatusShow";
            this.xrlblStatusShow.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrlblStatusShow.SizeF = new System.Drawing.SizeF(358.4468F, 9.999992F);
            this.xrlblStatusShow.StylePriority.UseFont = false;
            this.xrlblStatusShow.Text = "TODOS LOS ESTATUS";
            // 
            // xrlblStatus
            // 
            this.xrlblStatus.Dpi = 100F;
            this.xrlblStatus.Font = new System.Drawing.Font("Arial", 7F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrlblStatus.LocationFloat = new DevExpress.Utils.PointFloat(29.79F, 102.28F);
            this.xrlblStatus.Name = "xrlblStatus";
            this.xrlblStatus.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrlblStatus.SizeF = new System.Drawing.SizeF(80.20833F, 9.999992F);
            this.xrlblStatus.StylePriority.UseFont = false;
            this.xrlblStatus.Text = "Estatus:";
            // 
            // xrlblSupplierUpShow
            // 
            this.xrlblSupplierUpShow.Dpi = 100F;
            this.xrlblSupplierUpShow.Font = new System.Drawing.Font("Arial", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrlblSupplierUpShow.LocationFloat = new DevExpress.Utils.PointFloat(111.0636F, 87.57815F);
            this.xrlblSupplierUpShow.Name = "xrlblSupplierUpShow";
            this.xrlblSupplierUpShow.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrlblSupplierUpShow.SizeF = new System.Drawing.SizeF(358.447F, 9.999977F);
            this.xrlblSupplierUpShow.StylePriority.UseFont = false;
            this.xrlblSupplierUpShow.Text = "TODOS LOS PROVEEDORES";
            // 
            // xrlblSupplierUp
            // 
            this.xrlblSupplierUp.Dpi = 100F;
            this.xrlblSupplierUp.Font = new System.Drawing.Font("Arial", 7F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrlblSupplierUp.LocationFloat = new DevExpress.Utils.PointFloat(29.79141F, 87.57813F);
            this.xrlblSupplierUp.Name = "xrlblSupplierUp";
            this.xrlblSupplierUp.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrlblSupplierUp.SizeF = new System.Drawing.SizeF(80.20833F, 9.999992F);
            this.xrlblSupplierUp.StylePriority.UseFont = false;
            this.xrlblSupplierUp.Text = "Proveedor:";
            // 
            // xrlblDateEndShow
            // 
            this.xrlblDateEndShow.Dpi = 100F;
            this.xrlblDateEndShow.Font = new System.Drawing.Font("Arial", 7F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrlblDateEndShow.LocationFloat = new DevExpress.Utils.PointFloat(356.1343F, 42.57811F);
            this.xrlblDateEndShow.Name = "xrlblDateEndShow";
            this.xrlblDateEndShow.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrlblDateEndShow.SizeF = new System.Drawing.SizeF(113.3763F, 9.999992F);
            this.xrlblDateEndShow.StylePriority.UseFont = false;
            this.xrlblDateEndShow.Text = "//";
            // 
            // xrlblDateEnd
            // 
            this.xrlblDateEnd.Dpi = 100F;
            this.xrlblDateEnd.Font = new System.Drawing.Font("Arial", 7F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrlblDateEnd.LocationFloat = new DevExpress.Utils.PointFloat(265.455F, 42.57811F);
            this.xrlblDateEnd.Name = "xrlblDateEnd";
            this.xrlblDateEnd.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrlblDateEnd.SizeF = new System.Drawing.SizeF(80.20833F, 9.999992F);
            this.xrlblDateEnd.StylePriority.UseFont = false;
            this.xrlblDateEnd.Text = "Fecha Fin:";
            // 
            // xrlblDateStartShow
            // 
            this.xrlblDateStartShow.Dpi = 100F;
            this.xrlblDateStartShow.Font = new System.Drawing.Font("Arial", 7F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrlblDateStartShow.LocationFloat = new DevExpress.Utils.PointFloat(111.0636F, 42.57811F);
            this.xrlblDateStartShow.Name = "xrlblDateStartShow";
            this.xrlblDateStartShow.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrlblDateStartShow.SizeF = new System.Drawing.SizeF(137.5F, 9.999992F);
            this.xrlblDateStartShow.StylePriority.UseFont = false;
            this.xrlblDateStartShow.Text = "//";
            // 
            // xrlblDateStart
            // 
            this.xrlblDateStart.Dpi = 100F;
            this.xrlblDateStart.Font = new System.Drawing.Font("Arial", 7F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrlblDateStart.LocationFloat = new DevExpress.Utils.PointFloat(29.79166F, 42.57811F);
            this.xrlblDateStart.Name = "xrlblDateStart";
            this.xrlblDateStart.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrlblDateStart.SizeF = new System.Drawing.SizeF(80.20833F, 9.999992F);
            this.xrlblDateStart.StylePriority.UseFont = false;
            this.xrlblDateStart.Text = "Fecha Inicio:";
            // 
            // xrlblCategory
            // 
            this.xrlblCategory.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrlblCategory.Dpi = 100F;
            this.xrlblCategory.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrlblCategory.LocationFloat = new DevExpress.Utils.PointFloat(311.337F, 135.9514F);
            this.xrlblCategory.Name = "xrlblCategory";
            this.xrlblCategory.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrlblCategory.SizeF = new System.Drawing.SizeF(99.36496F, 22.99998F);
            this.xrlblCategory.StylePriority.UseBorders = false;
            this.xrlblCategory.StylePriority.UseFont = false;
            this.xrlblCategory.StylePriority.UseTextAlignment = false;
            this.xrlblCategory.Text = "Area";
            this.xrlblCategory.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTypeExpense
            // 
            this.xrTypeExpense.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTypeExpense.Dpi = 100F;
            this.xrTypeExpense.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTypeExpense.LocationFloat = new DevExpress.Utils.PointFloat(410.702F, 135.9515F);
            this.xrTypeExpense.Name = "xrTypeExpense";
            this.xrTypeExpense.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTypeExpense.SizeF = new System.Drawing.SizeF(75.24249F, 22.99998F);
            this.xrTypeExpense.StylePriority.UseBorders = false;
            this.xrTypeExpense.StylePriority.UseFont = false;
            this.xrTypeExpense.StylePriority.UseTextAlignment = false;
            this.xrTypeExpense.Text = "Tipo";
            this.xrTypeExpense.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrlblDate
            // 
            this.xrlblDate.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrlblDate.Dpi = 100F;
            this.xrlblDate.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrlblDate.LocationFloat = new DevExpress.Utils.PointFloat(533.5073F, 135.9515F);
            this.xrlblDate.Name = "xrlblDate";
            this.xrlblDate.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrlblDate.SizeF = new System.Drawing.SizeF(72.58557F, 22.99999F);
            this.xrlblDate.StylePriority.UseBorders = false;
            this.xrlblDate.StylePriority.UseFont = false;
            this.xrlblDate.StylePriority.UseTextAlignment = false;
            this.xrlblDate.Text = "Registro";
            this.xrlblDate.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrlblCurrency
            // 
            this.xrlblCurrency.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrlblCurrency.Dpi = 100F;
            this.xrlblCurrency.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrlblCurrency.LocationFloat = new DevExpress.Utils.PointFloat(485.9445F, 135.9515F);
            this.xrlblCurrency.Name = "xrlblCurrency";
            this.xrlblCurrency.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrlblCurrency.SizeF = new System.Drawing.SizeF(47.56281F, 23F);
            this.xrlblCurrency.StylePriority.UseBorders = false;
            this.xrlblCurrency.StylePriority.UseFont = false;
            this.xrlblCurrency.StylePriority.UseTextAlignment = false;
            this.xrlblCurrency.Text = "Moneda";
            this.xrlblCurrency.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xraddress
            // 
            this.xraddress.Dpi = 100F;
            this.xraddress.Font = new System.Drawing.Font("Arial", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xraddress.LocationFloat = new DevExpress.Utils.PointFloat(606.0928F, 87.57813F);
            this.xraddress.Name = "xraddress";
            this.xraddress.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xraddress.SizeF = new System.Drawing.SizeF(259.8276F, 15F);
            this.xraddress.StylePriority.UseFont = false;
            this.xraddress.StylePriority.UseTextAlignment = false;
            this.xraddress.Text = "- Dirección";
            this.xraddress.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // xrlblCurrencyUpShow
            // 
            this.xrlblCurrencyUpShow.Dpi = 100F;
            this.xrlblCurrencyUpShow.Font = new System.Drawing.Font("Arial", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrlblCurrencyUpShow.LocationFloat = new DevExpress.Utils.PointFloat(111.0636F, 118.28F);
            this.xrlblCurrencyUpShow.Name = "xrlblCurrencyUpShow";
            this.xrlblCurrencyUpShow.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrlblCurrencyUpShow.SizeF = new System.Drawing.SizeF(358.447F, 9.999977F);
            this.xrlblCurrencyUpShow.StylePriority.UseFont = false;
            this.xrlblCurrencyUpShow.Text = "TODAS LAS MONEDAS";
            // 
            // xrPictureBox1
            // 
            this.xrPictureBox1.Dpi = 100F;
            this.xrPictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("xrPictureBox1.Image")));
            this.xrPictureBox1.LocationFloat = new DevExpress.Utils.PointFloat(883.2512F, 42.57811F);
            this.xrPictureBox1.Name = "xrPictureBox1";
            this.xrPictureBox1.SizeF = new System.Drawing.SizeF(116.4632F, 75F);
            this.xrPictureBox1.Sizing = DevExpress.XtraPrinting.ImageSizeMode.ZoomImage;
            // 
            // xrLabel3
            // 
            this.xrLabel3.Dpi = 100F;
            this.xrLabel3.Font = new System.Drawing.Font("Arial", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel3.LocationFloat = new DevExpress.Utils.PointFloat(606.0928F, 42.57811F);
            this.xrLabel3.Name = "xrLabel3";
            this.xrLabel3.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel3.SizeF = new System.Drawing.SizeF(259.8278F, 15F);
            this.xrLabel3.StylePriority.UseFont = false;
            this.xrLabel3.StylePriority.UseTextAlignment = false;
            this.xrLabel3.Text = "DISTRIBUIDORA EL FLORIDO S.A. DE C.V.";
            this.xrLabel3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // siteLabel
            // 
            this.siteLabel.Dpi = 100F;
            this.siteLabel.Font = new System.Drawing.Font("Arial", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.siteLabel.LocationFloat = new DevExpress.Utils.PointFloat(606.0928F, 57.57812F);
            this.siteLabel.Name = "siteLabel";
            this.siteLabel.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.siteLabel.SizeF = new System.Drawing.SizeF(259.8278F, 15F);
            this.siteLabel.StylePriority.UseFont = false;
            this.siteLabel.StylePriority.UseTextAlignment = false;
            this.siteLabel.Text = "*FLORIDO CEDIS";
            this.siteLabel.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // xrRFC
            // 
            this.xrRFC.Dpi = 100F;
            this.xrRFC.Font = new System.Drawing.Font("Arial", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrRFC.LocationFloat = new DevExpress.Utils.PointFloat(758.6957F, 72.57807F);
            this.xrRFC.Name = "xrRFC";
            this.xrRFC.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrRFC.SizeF = new System.Drawing.SizeF(107.2252F, 15F);
            this.xrRFC.StylePriority.UseFont = false;
            this.xrRFC.StylePriority.UseTextAlignment = false;
            this.xrRFC.Text = "DFL-950802-5N4";
            this.xrRFC.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // xrLabel5
            // 
            this.xrLabel5.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel5.BorderWidth = 4F;
            this.xrLabel5.Dpi = 100F;
            this.xrLabel5.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel5.LocationFloat = new DevExpress.Utils.PointFloat(125.3368F, 10.00001F);
            this.xrLabel5.Name = "xrLabel5";
            this.xrLabel5.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel5.SizeF = new System.Drawing.SizeF(229.0089F, 30F);
            this.xrLabel5.StylePriority.UseBorders = false;
            this.xrLabel5.StylePriority.UseBorderWidth = false;
            this.xrLabel5.StylePriority.UseFont = false;
            this.xrLabel5.StylePriority.UseTextAlignment = false;
            this.xrLabel5.Text = "Reporte de Gastos Concentrado";
            this.xrLabel5.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrlblArea
            // 
            this.xrlblArea.Dpi = 100F;
            this.xrlblArea.Font = new System.Drawing.Font("Arial", 7F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrlblArea.LocationFloat = new DevExpress.Utils.PointFloat(29.79142F, 72.57813F);
            this.xrlblArea.Name = "xrlblArea";
            this.xrlblArea.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrlblArea.SizeF = new System.Drawing.SizeF(80.20833F, 9.999992F);
            this.xrlblArea.StylePriority.UseFont = false;
            this.xrlblArea.Text = "Area:";
            // 
            // xrState
            // 
            this.xrState.Dpi = 100F;
            this.xrState.Font = new System.Drawing.Font("Arial", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrState.LocationFloat = new DevExpress.Utils.PointFloat(606.0928F, 102.5781F);
            this.xrState.Name = "xrState";
            this.xrState.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrState.SizeF = new System.Drawing.SizeF(259.8278F, 15F);
            this.xrState.StylePriority.UseFont = false;
            this.xrState.StylePriority.UseTextAlignment = false;
            this.xrState.Text = "- Estado";
            this.xrState.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // xrlblType
            // 
            this.xrlblType.Dpi = 100F;
            this.xrlblType.Font = new System.Drawing.Font("Arial", 7F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrlblType.LocationFloat = new DevExpress.Utils.PointFloat(29.79166F, 57.57808F);
            this.xrlblType.Name = "xrlblType";
            this.xrlblType.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrlblType.SizeF = new System.Drawing.SizeF(80.20831F, 10F);
            this.xrlblType.StylePriority.UseFont = false;
            this.xrlblType.Text = "Tipo de Gasto:";
            // 
            // xrlblTypeShow
            // 
            this.xrlblTypeShow.Dpi = 100F;
            this.xrlblTypeShow.Font = new System.Drawing.Font("Arial", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrlblTypeShow.LocationFloat = new DevExpress.Utils.PointFloat(111.0636F, 57.57808F);
            this.xrlblTypeShow.Name = "xrlblTypeShow";
            this.xrlblTypeShow.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrlblTypeShow.SizeF = new System.Drawing.SizeF(358.4469F, 10F);
            this.xrlblTypeShow.StylePriority.UseFont = false;
            this.xrlblTypeShow.Text = "TODOS LOS GASTOS";
            // 
            // xrLabel7
            // 
            this.xrLabel7.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel7.Dpi = 100F;
            this.xrLabel7.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel7.LocationFloat = new DevExpress.Utils.PointFloat(804.5059F, 135.9515F);
            this.xrLabel7.Name = "xrLabel7";
            this.xrLabel7.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel7.SizeF = new System.Drawing.SizeF(56.44885F, 22.99998F);
            this.xrLabel7.StylePriority.UseBorders = false;
            this.xrLabel7.StylePriority.UseFont = false;
            this.xrLabel7.StylePriority.UseTextAlignment = false;
            this.xrLabel7.Text = "Descuento";
            this.xrLabel7.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrLabel6
            // 
            this.xrLabel6.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel6.Dpi = 100F;
            this.xrLabel6.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel6.LocationFloat = new DevExpress.Utils.PointFloat(860.9548F, 135.9514F);
            this.xrLabel6.Name = "xrLabel6";
            this.xrLabel6.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel6.SizeF = new System.Drawing.SizeF(58.17926F, 22.99998F);
            this.xrLabel6.StylePriority.UseBorders = false;
            this.xrLabel6.StylePriority.UseFont = false;
            this.xrLabel6.StylePriority.UseTextAlignment = false;
            this.xrLabel6.Text = "ISR Ret.";
            this.xrLabel6.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrLabel4
            // 
            this.xrLabel4.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel4.Dpi = 100F;
            this.xrLabel4.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel4.LocationFloat = new DevExpress.Utils.PointFloat(919.134F, 135.9515F);
            this.xrLabel4.Name = "xrLabel4";
            this.xrLabel4.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel4.SizeF = new System.Drawing.SizeF(56.06073F, 22.99999F);
            this.xrLabel4.StylePriority.UseBorders = false;
            this.xrLabel4.StylePriority.UseFont = false;
            this.xrLabel4.StylePriority.UseTextAlignment = false;
            this.xrLabel4.Text = "IVA Ret.";
            this.xrLabel4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrlblSubtotal
            // 
            this.xrlblSubtotal.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrlblSubtotal.Dpi = 100F;
            this.xrlblSubtotal.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrlblSubtotal.LocationFloat = new DevExpress.Utils.PointFloat(975.1948F, 135.9514F);
            this.xrlblSubtotal.Name = "xrlblSubtotal";
            this.xrlblSubtotal.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrlblSubtotal.SizeF = new System.Drawing.SizeF(74.80518F, 23.00002F);
            this.xrlblSubtotal.StylePriority.UseBorders = false;
            this.xrlblSubtotal.StylePriority.UseFont = false;
            this.xrlblSubtotal.StylePriority.UseTextAlignment = false;
            this.xrlblSubtotal.Text = "Importe";
            this.xrlblSubtotal.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrlblIEPs
            // 
            this.xrlblIEPs.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrlblIEPs.Dpi = 100F;
            this.xrlblIEPs.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrlblIEPs.LocationFloat = new DevExpress.Utils.PointFloat(741.0187F, 135.9514F);
            this.xrlblIEPs.Name = "xrlblIEPs";
            this.xrlblIEPs.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrlblIEPs.SizeF = new System.Drawing.SizeF(63.48724F, 22.99998F);
            this.xrlblIEPs.StylePriority.UseBorders = false;
            this.xrlblIEPs.StylePriority.UseFont = false;
            this.xrlblIEPs.StylePriority.UseTextAlignment = false;
            this.xrlblIEPs.Text = "IEPS";
            this.xrlblIEPs.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrlblIVA
            // 
            this.xrlblIVA.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrlblIVA.Dpi = 100F;
            this.xrlblIVA.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrlblIVA.LocationFloat = new DevExpress.Utils.PointFloat(668.4941F, 135.9514F);
            this.xrlblIVA.Name = "xrlblIVA";
            this.xrlblIVA.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrlblIVA.SizeF = new System.Drawing.SizeF(72.52429F, 23F);
            this.xrlblIVA.StylePriority.UseBorders = false;
            this.xrlblIVA.StylePriority.UseFont = false;
            this.xrlblIVA.StylePriority.UseTextAlignment = false;
            this.xrlblIVA.Text = "IVA";
            this.xrlblIVA.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrlblImport
            // 
            this.xrlblImport.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrlblImport.Dpi = 100F;
            this.xrlblImport.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrlblImport.LocationFloat = new DevExpress.Utils.PointFloat(606.0928F, 135.9515F);
            this.xrlblImport.Name = "xrlblImport";
            this.xrlblImport.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrlblImport.SizeF = new System.Drawing.SizeF(62.40118F, 22.99998F);
            this.xrlblImport.StylePriority.UseBorders = false;
            this.xrlblImport.StylePriority.UseFont = false;
            this.xrlblImport.StylePriority.UseTextAlignment = false;
            this.xrlblImport.Text = "Subtotal";
            this.xrlblImport.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrlblSupplier
            // 
            this.xrlblSupplier.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrlblSupplier.Dpi = 100F;
            this.xrlblSupplier.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrlblSupplier.LocationFloat = new DevExpress.Utils.PointFloat(144.397F, 135.9515F);
            this.xrlblSupplier.Name = "xrlblSupplier";
            this.xrlblSupplier.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrlblSupplier.SizeF = new System.Drawing.SizeF(166.9401F, 22.99998F);
            this.xrlblSupplier.StylePriority.UseBorders = false;
            this.xrlblSupplier.StylePriority.UseFont = false;
            this.xrlblSupplier.StylePriority.UseTextAlignment = false;
            this.xrlblSupplier.Text = "Proveedor";
            this.xrlblSupplier.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrlblFolio
            // 
            this.xrlblFolio.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrlblFolio.Dpi = 100F;
            this.xrlblFolio.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrlblFolio.LocationFloat = new DevExpress.Utils.PointFloat(71.87284F, 135.9514F);
            this.xrlblFolio.Name = "xrlblFolio";
            this.xrlblFolio.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrlblFolio.SizeF = new System.Drawing.SizeF(72.52425F, 23F);
            this.xrlblFolio.StylePriority.UseBorders = false;
            this.xrlblFolio.StylePriority.UseFont = false;
            this.xrlblFolio.StylePriority.UseTextAlignment = false;
            this.xrlblFolio.Text = "Factura";
            this.xrlblFolio.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // DetailReport
            // 
            this.DetailReport.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.Detail1,
            this.GroupFooter1});
            this.DetailReport.Dpi = 100F;
            this.DetailReport.Level = 0;
            this.DetailReport.Name = "DetailReport";
            // 
            // Detail1
            // 
            this.Detail1.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTablePurchase});
            this.Detail1.Dpi = 100F;
            this.Detail1.HeightF = 50.09804F;
            this.Detail1.Name = "Detail1";
            // 
            // xrTablePurchase
            // 
            this.xrTablePurchase.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTablePurchase.Dpi = 100F;
            this.xrTablePurchase.LocationFloat = new DevExpress.Utils.PointFloat(0F, 0F);
            this.xrTablePurchase.Name = "xrTablePurchase";
            this.xrTablePurchase.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow1});
            this.xrTablePurchase.SizeF = new System.Drawing.SizeF(1050F, 50.09804F);
            this.xrTablePurchase.StylePriority.UseBorders = false;
            // 
            // xrTableRow1
            // 
            this.xrTableRow1.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrCellFolio,
            this.xrCellInvoice,
            this.xrCellSupplier,
            this.xrCellCategory,
            this.xrCellType,
            this.xrCellCurrecency,
            this.xrCellDate,
            this.xrCellSubtotal,
            this.xrCellIVA,
            this.xrCellIEPS,
            this.xrCellDiscount,
            this.xrCellISRR,
            this.xrCellIVAR,
            this.xrCellImport});
            this.xrTableRow1.Dpi = 100F;
            this.xrTableRow1.Name = "xrTableRow1";
            this.xrTableRow1.Weight = 1D;
            // 
            // xrCellInvoice
            // 
            this.xrCellInvoice.Dpi = 100F;
            this.xrCellInvoice.Font = new System.Drawing.Font("Times New Roman", 6F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrCellInvoice.Name = "xrCellInvoice";
            this.xrCellInvoice.StylePriority.UseFont = false;
            this.xrCellInvoice.StylePriority.UseTextAlignment = false;
            this.xrCellInvoice.Text = "xrCellInvoice";
            this.xrCellInvoice.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrCellInvoice.Weight = 0.66713539137397027D;
            // 
            // xrCellSupplier
            // 
            this.xrCellSupplier.Dpi = 100F;
            this.xrCellSupplier.Font = new System.Drawing.Font("Times New Roman", 6F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrCellSupplier.Name = "xrCellSupplier";
            this.xrCellSupplier.StylePriority.UseFont = false;
            this.xrCellSupplier.StylePriority.UseTextAlignment = false;
            this.xrCellSupplier.Text = "xrCellSupplier";
            this.xrCellSupplier.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrCellSupplier.Weight = 1.5356480374523247D;
            // 
            // xrCellCategory
            // 
            this.xrCellCategory.Dpi = 100F;
            this.xrCellCategory.Font = new System.Drawing.Font("Times New Roman", 6F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrCellCategory.Name = "xrCellCategory";
            this.xrCellCategory.StylePriority.UseFont = false;
            this.xrCellCategory.StylePriority.UseTextAlignment = false;
            this.xrCellCategory.Text = "xrCellCategory";
            this.xrCellCategory.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrCellCategory.Weight = 0.91403889484501932D;
            // 
            // xrCellType
            // 
            this.xrCellType.Dpi = 100F;
            this.xrCellType.Font = new System.Drawing.Font("Times New Roman", 6F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrCellType.Name = "xrCellType";
            this.xrCellType.StylePriority.UseFont = false;
            this.xrCellType.StylePriority.UseTextAlignment = false;
            this.xrCellType.Text = "xrCellType";
            this.xrCellType.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrCellType.Weight = 0.69214101122394256D;
            // 
            // xrCellCurrecency
            // 
            this.xrCellCurrecency.Dpi = 100F;
            this.xrCellCurrecency.Font = new System.Drawing.Font("Times New Roman", 6F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrCellCurrecency.Name = "xrCellCurrecency";
            this.xrCellCurrecency.StylePriority.UseFont = false;
            this.xrCellCurrecency.StylePriority.UseTextAlignment = false;
            this.xrCellCurrecency.Text = "xrCellCurrecency";
            this.xrCellCurrecency.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrCellCurrecency.Weight = 0.43752070601643078D;
            // 
            // xrCellDate
            // 
            this.xrCellDate.Dpi = 100F;
            this.xrCellDate.Font = new System.Drawing.Font("Times New Roman", 6F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrCellDate.Name = "xrCellDate";
            this.xrCellDate.StylePriority.UseFont = false;
            this.xrCellDate.StylePriority.UseTextAlignment = false;
            this.xrCellDate.Text = "xrCellDate";
            this.xrCellDate.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrCellDate.Weight = 0.66770272351183746D;
            // 
            // xrCellSubtotal
            // 
            this.xrCellSubtotal.Dpi = 100F;
            this.xrCellSubtotal.Font = new System.Drawing.Font("Times New Roman", 6F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrCellSubtotal.Name = "xrCellSubtotal";
            this.xrCellSubtotal.StylePriority.UseFont = false;
            this.xrCellSubtotal.StylePriority.UseTextAlignment = false;
            this.xrCellSubtotal.Text = "xrCellSubtotal";
            this.xrCellSubtotal.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrCellSubtotal.Weight = 0.57401520304550879D;
            // 
            // xrCellIVA
            // 
            this.xrCellIVA.Dpi = 100F;
            this.xrCellIVA.Font = new System.Drawing.Font("Times New Roman", 6F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrCellIVA.Name = "xrCellIVA";
            this.xrCellIVA.StylePriority.UseFont = false;
            this.xrCellIVA.StylePriority.UseTextAlignment = false;
            this.xrCellIVA.Text = "xrCellIVA";
            this.xrCellIVA.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrCellIVA.Weight = 0.66713910060159543D;
            // 
            // xrCellIEPS
            // 
            this.xrCellIEPS.Dpi = 100F;
            this.xrCellIEPS.Font = new System.Drawing.Font("Times New Roman", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrCellIEPS.Name = "xrCellIEPS";
            this.xrCellIEPS.StylePriority.UseFont = false;
            this.xrCellIEPS.StylePriority.UseTextAlignment = false;
            this.xrCellIEPS.Text = "xrCellIEPS";
            this.xrCellIEPS.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrCellIEPS.Weight = 0.5840068272798129D;
            // 
            // xrCellDiscount
            // 
            this.xrCellDiscount.Dpi = 100F;
            this.xrCellDiscount.Font = new System.Drawing.Font("Times New Roman", 6F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrCellDiscount.Name = "xrCellDiscount";
            this.xrCellDiscount.StylePriority.UseFont = false;
            this.xrCellDiscount.StylePriority.UseTextAlignment = false;
            this.xrCellDiscount.Text = "xrCellDiscount";
            this.xrCellDiscount.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrCellDiscount.Weight = 0.51926198900111042D;
            // 
            // xrCellISRR
            // 
            this.xrCellISRR.Dpi = 100F;
            this.xrCellISRR.Font = new System.Drawing.Font("Times New Roman", 6F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrCellISRR.Name = "xrCellISRR";
            this.xrCellISRR.StylePriority.UseFont = false;
            this.xrCellISRR.StylePriority.UseTextAlignment = false;
            this.xrCellISRR.Text = "xrCellISRR";
            this.xrCellISRR.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrCellISRR.Weight = 0.53517969594560721D;
            // 
            // xrCellIVAR
            // 
            this.xrCellIVAR.Dpi = 100F;
            this.xrCellIVAR.Font = new System.Drawing.Font("Times New Roman", 6F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrCellIVAR.Name = "xrCellIVAR";
            this.xrCellIVAR.StylePriority.UseFont = false;
            this.xrCellIVAR.StylePriority.UseTextAlignment = false;
            this.xrCellIVAR.Text = "xrCellIVAR";
            this.xrCellIVAR.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrCellIVAR.Weight = 0.51569231953657568D;
            // 
            // xrCellImport
            // 
            this.xrCellImport.Dpi = 100F;
            this.xrCellImport.Font = new System.Drawing.Font("Times New Roman", 6F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrCellImport.Name = "xrCellImport";
            this.xrCellImport.StylePriority.UseFont = false;
            this.xrCellImport.StylePriority.UseTextAlignment = false;
            this.xrCellImport.Text = "xrCellImport";
            this.xrCellImport.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrCellImport.Weight = 0.68811851289945D;
            // 
            // GroupFooter1
            // 
            this.GroupFooter1.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrlblUsdImport,
            this.xrLabel18,
            this.xrlblUsdISRR,
            this.xrLabel25,
            this.xrlblUsdIVAR,
            this.xrlblUsdDiscount,
            this.xrlblUsdIEPS,
            this.xrlblUsdIVA,
            this.xrlblUsdSubtotal,
            this.xrlblMxnSubtotal,
            this.xrlblMxnIVA,
            this.xrlblMxnIEPS,
            this.xrlblMxnDiscount,
            this.xrlblMxnImport,
            this.xrlblMxnIVAR,
            this.xrlblMxnISRR,
            this.xrLabel20,
            this.xrLabel19});
            this.GroupFooter1.Dpi = 100F;
            this.GroupFooter1.HeightF = 56.25F;
            this.GroupFooter1.Name = "GroupFooter1";
            // 
            // xrlblUsdImport
            // 
            this.xrlblUsdImport.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrlblUsdImport.Dpi = 100F;
            this.xrlblUsdImport.Font = new System.Drawing.Font("Times New Roman", 6F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrlblUsdImport.LocationFloat = new DevExpress.Utils.PointFloat(975.1948F, 22.99999F);
            this.xrlblUsdImport.Name = "xrlblUsdImport";
            this.xrlblUsdImport.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrlblUsdImport.SizeF = new System.Drawing.SizeF(74.80518F, 22.99999F);
            this.xrlblUsdImport.StylePriority.UseBorders = false;
            this.xrlblUsdImport.StylePriority.UseFont = false;
            this.xrlblUsdImport.StylePriority.UseTextAlignment = false;
            this.xrlblUsdImport.Text = "$ 0";
            this.xrlblUsdImport.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrLabel18
            // 
            this.xrLabel18.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel18.Dpi = 100F;
            this.xrLabel18.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel18.LocationFloat = new DevExpress.Utils.PointFloat(71.87284F, 22.99999F);
            this.xrLabel18.Name = "xrLabel18";
            this.xrLabel18.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel18.SizeF = new System.Drawing.SizeF(534.22F, 23F);
            this.xrLabel18.StylePriority.UseBorders = false;
            this.xrLabel18.StylePriority.UseFont = false;
            this.xrLabel18.StylePriority.UseTextAlignment = false;
            this.xrLabel18.Text = "USD";
            this.xrLabel18.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrlblUsdISRR
            // 
            this.xrlblUsdISRR.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrlblUsdISRR.Dpi = 100F;
            this.xrlblUsdISRR.Font = new System.Drawing.Font("Times New Roman", 6F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrlblUsdISRR.LocationFloat = new DevExpress.Utils.PointFloat(860.9548F, 23.25001F);
            this.xrlblUsdISRR.Name = "xrlblUsdISRR";
            this.xrlblUsdISRR.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrlblUsdISRR.SizeF = new System.Drawing.SizeF(58.17926F, 22.99999F);
            this.xrlblUsdISRR.StylePriority.UseBorders = false;
            this.xrlblUsdISRR.StylePriority.UseFont = false;
            this.xrlblUsdISRR.StylePriority.UseTextAlignment = false;
            this.xrlblUsdISRR.Text = "$ 0";
            this.xrlblUsdISRR.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrLabel25
            // 
            this.xrLabel25.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel25.Dpi = 100F;
            this.xrLabel25.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel25.LocationFloat = new DevExpress.Utils.PointFloat(0F, 23F);
            this.xrLabel25.Name = "xrLabel25";
            this.xrLabel25.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel25.SizeF = new System.Drawing.SizeF(71.87288F, 23F);
            this.xrLabel25.StylePriority.UseBorders = false;
            this.xrLabel25.StylePriority.UseFont = false;
            this.xrLabel25.StylePriority.UseTextAlignment = false;
            this.xrLabel25.Text = "TOTAL";
            this.xrLabel25.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrlblUsdIVAR
            // 
            this.xrlblUsdIVAR.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrlblUsdIVAR.Dpi = 100F;
            this.xrlblUsdIVAR.Font = new System.Drawing.Font("Times New Roman", 6F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrlblUsdIVAR.LocationFloat = new DevExpress.Utils.PointFloat(919.134F, 22.99999F);
            this.xrlblUsdIVAR.Name = "xrlblUsdIVAR";
            this.xrlblUsdIVAR.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrlblUsdIVAR.SizeF = new System.Drawing.SizeF(56.06073F, 23F);
            this.xrlblUsdIVAR.StylePriority.UseBorders = false;
            this.xrlblUsdIVAR.StylePriority.UseFont = false;
            this.xrlblUsdIVAR.StylePriority.UseTextAlignment = false;
            this.xrlblUsdIVAR.Text = "$ 0";
            this.xrlblUsdIVAR.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrlblUsdDiscount
            // 
            this.xrlblUsdDiscount.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrlblUsdDiscount.Dpi = 100F;
            this.xrlblUsdDiscount.Font = new System.Drawing.Font("Times New Roman", 6F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrlblUsdDiscount.LocationFloat = new DevExpress.Utils.PointFloat(804.5059F, 23.25001F);
            this.xrlblUsdDiscount.Name = "xrlblUsdDiscount";
            this.xrlblUsdDiscount.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrlblUsdDiscount.SizeF = new System.Drawing.SizeF(56.44885F, 22.99999F);
            this.xrlblUsdDiscount.StylePriority.UseBorders = false;
            this.xrlblUsdDiscount.StylePriority.UseFont = false;
            this.xrlblUsdDiscount.StylePriority.UseTextAlignment = false;
            this.xrlblUsdDiscount.Text = "$ 0";
            this.xrlblUsdDiscount.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrlblUsdIEPS
            // 
            this.xrlblUsdIEPS.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrlblUsdIEPS.Dpi = 100F;
            this.xrlblUsdIEPS.Font = new System.Drawing.Font("Times New Roman", 6F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrlblUsdIEPS.LocationFloat = new DevExpress.Utils.PointFloat(741.0187F, 22.99999F);
            this.xrlblUsdIEPS.Name = "xrlblUsdIEPS";
            this.xrlblUsdIEPS.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrlblUsdIEPS.SizeF = new System.Drawing.SizeF(63.48724F, 22.99999F);
            this.xrlblUsdIEPS.StylePriority.UseBorders = false;
            this.xrlblUsdIEPS.StylePriority.UseFont = false;
            this.xrlblUsdIEPS.StylePriority.UseTextAlignment = false;
            this.xrlblUsdIEPS.Text = "$ 0";
            this.xrlblUsdIEPS.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrlblUsdIVA
            // 
            this.xrlblUsdIVA.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrlblUsdIVA.Dpi = 100F;
            this.xrlblUsdIVA.Font = new System.Drawing.Font("Times New Roman", 6F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrlblUsdIVA.LocationFloat = new DevExpress.Utils.PointFloat(668.494F, 22.99999F);
            this.xrlblUsdIVA.Name = "xrlblUsdIVA";
            this.xrlblUsdIVA.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrlblUsdIVA.SizeF = new System.Drawing.SizeF(72.52441F, 22.99999F);
            this.xrlblUsdIVA.StylePriority.UseBorders = false;
            this.xrlblUsdIVA.StylePriority.UseFont = false;
            this.xrlblUsdIVA.StylePriority.UseTextAlignment = false;
            this.xrlblUsdIVA.Text = "$ 0";
            this.xrlblUsdIVA.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrlblUsdSubtotal
            // 
            this.xrlblUsdSubtotal.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrlblUsdSubtotal.Dpi = 100F;
            this.xrlblUsdSubtotal.Font = new System.Drawing.Font("Times New Roman", 6F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrlblUsdSubtotal.LocationFloat = new DevExpress.Utils.PointFloat(606.0928F, 23.25001F);
            this.xrlblUsdSubtotal.Name = "xrlblUsdSubtotal";
            this.xrlblUsdSubtotal.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrlblUsdSubtotal.SizeF = new System.Drawing.SizeF(62.40118F, 22.99999F);
            this.xrlblUsdSubtotal.StylePriority.UseBorders = false;
            this.xrlblUsdSubtotal.StylePriority.UseFont = false;
            this.xrlblUsdSubtotal.StylePriority.UseTextAlignment = false;
            this.xrlblUsdSubtotal.Text = "$ 0";
            this.xrlblUsdSubtotal.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrlblMxnSubtotal
            // 
            this.xrlblMxnSubtotal.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrlblMxnSubtotal.Dpi = 100F;
            this.xrlblMxnSubtotal.Font = new System.Drawing.Font("Times New Roman", 6F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrlblMxnSubtotal.LocationFloat = new DevExpress.Utils.PointFloat(606.0928F, 0F);
            this.xrlblMxnSubtotal.Name = "xrlblMxnSubtotal";
            this.xrlblMxnSubtotal.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrlblMxnSubtotal.SizeF = new System.Drawing.SizeF(62.40118F, 23F);
            this.xrlblMxnSubtotal.StylePriority.UseBorders = false;
            this.xrlblMxnSubtotal.StylePriority.UseFont = false;
            this.xrlblMxnSubtotal.StylePriority.UseTextAlignment = false;
            this.xrlblMxnSubtotal.Text = "$ 0";
            this.xrlblMxnSubtotal.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrlblMxnIVA
            // 
            this.xrlblMxnIVA.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrlblMxnIVA.Dpi = 100F;
            this.xrlblMxnIVA.Font = new System.Drawing.Font("Times New Roman", 6F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrlblMxnIVA.LocationFloat = new DevExpress.Utils.PointFloat(668.4941F, 0F);
            this.xrlblMxnIVA.Name = "xrlblMxnIVA";
            this.xrlblMxnIVA.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrlblMxnIVA.SizeF = new System.Drawing.SizeF(72.52405F, 23F);
            this.xrlblMxnIVA.StylePriority.UseBorders = false;
            this.xrlblMxnIVA.StylePriority.UseFont = false;
            this.xrlblMxnIVA.StylePriority.UseTextAlignment = false;
            this.xrlblMxnIVA.Text = "$ 0";
            this.xrlblMxnIVA.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrlblMxnIEPS
            // 
            this.xrlblMxnIEPS.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrlblMxnIEPS.Dpi = 100F;
            this.xrlblMxnIEPS.Font = new System.Drawing.Font("Times New Roman", 6F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrlblMxnIEPS.LocationFloat = new DevExpress.Utils.PointFloat(741.0187F, 0F);
            this.xrlblMxnIEPS.Name = "xrlblMxnIEPS";
            this.xrlblMxnIEPS.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrlblMxnIEPS.SizeF = new System.Drawing.SizeF(63.48724F, 23F);
            this.xrlblMxnIEPS.StylePriority.UseBorders = false;
            this.xrlblMxnIEPS.StylePriority.UseFont = false;
            this.xrlblMxnIEPS.StylePriority.UseTextAlignment = false;
            this.xrlblMxnIEPS.Text = "$ 0";
            this.xrlblMxnIEPS.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrlblMxnDiscount
            // 
            this.xrlblMxnDiscount.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrlblMxnDiscount.Dpi = 100F;
            this.xrlblMxnDiscount.Font = new System.Drawing.Font("Times New Roman", 6F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrlblMxnDiscount.LocationFloat = new DevExpress.Utils.PointFloat(804.5059F, 0F);
            this.xrlblMxnDiscount.Name = "xrlblMxnDiscount";
            this.xrlblMxnDiscount.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrlblMxnDiscount.SizeF = new System.Drawing.SizeF(56.44885F, 23F);
            this.xrlblMxnDiscount.StylePriority.UseBorders = false;
            this.xrlblMxnDiscount.StylePriority.UseFont = false;
            this.xrlblMxnDiscount.StylePriority.UseTextAlignment = false;
            this.xrlblMxnDiscount.Text = "$ 0";
            this.xrlblMxnDiscount.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrlblMxnImport
            // 
            this.xrlblMxnImport.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrlblMxnImport.Dpi = 100F;
            this.xrlblMxnImport.Font = new System.Drawing.Font("Times New Roman", 6F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrlblMxnImport.LocationFloat = new DevExpress.Utils.PointFloat(975.1948F, 0F);
            this.xrlblMxnImport.Name = "xrlblMxnImport";
            this.xrlblMxnImport.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrlblMxnImport.SizeF = new System.Drawing.SizeF(74.80518F, 23F);
            this.xrlblMxnImport.StylePriority.UseBorders = false;
            this.xrlblMxnImport.StylePriority.UseFont = false;
            this.xrlblMxnImport.StylePriority.UseTextAlignment = false;
            this.xrlblMxnImport.Text = "$ 0";
            this.xrlblMxnImport.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrlblMxnIVAR
            // 
            this.xrlblMxnIVAR.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrlblMxnIVAR.Dpi = 100F;
            this.xrlblMxnIVAR.Font = new System.Drawing.Font("Times New Roman", 6F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrlblMxnIVAR.LocationFloat = new DevExpress.Utils.PointFloat(919.134F, 0F);
            this.xrlblMxnIVAR.Name = "xrlblMxnIVAR";
            this.xrlblMxnIVAR.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrlblMxnIVAR.SizeF = new System.Drawing.SizeF(56.06073F, 23F);
            this.xrlblMxnIVAR.StylePriority.UseBorders = false;
            this.xrlblMxnIVAR.StylePriority.UseFont = false;
            this.xrlblMxnIVAR.StylePriority.UseTextAlignment = false;
            this.xrlblMxnIVAR.Text = "$ 0";
            this.xrlblMxnIVAR.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrlblMxnISRR
            // 
            this.xrlblMxnISRR.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrlblMxnISRR.Dpi = 100F;
            this.xrlblMxnISRR.Font = new System.Drawing.Font("Times New Roman", 6F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrlblMxnISRR.LocationFloat = new DevExpress.Utils.PointFloat(860.9548F, 0F);
            this.xrlblMxnISRR.Name = "xrlblMxnISRR";
            this.xrlblMxnISRR.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrlblMxnISRR.SizeF = new System.Drawing.SizeF(58.17926F, 23F);
            this.xrlblMxnISRR.StylePriority.UseBorders = false;
            this.xrlblMxnISRR.StylePriority.UseFont = false;
            this.xrlblMxnISRR.StylePriority.UseTextAlignment = false;
            this.xrlblMxnISRR.Text = "$ 0";
            this.xrlblMxnISRR.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrLabel20
            // 
            this.xrLabel20.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel20.Dpi = 100F;
            this.xrLabel20.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel20.LocationFloat = new DevExpress.Utils.PointFloat(71.87284F, 0F);
            this.xrLabel20.Name = "xrLabel20";
            this.xrLabel20.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel20.SizeF = new System.Drawing.SizeF(534.22F, 23F);
            this.xrLabel20.StylePriority.UseBorders = false;
            this.xrLabel20.StylePriority.UseFont = false;
            this.xrLabel20.StylePriority.UseTextAlignment = false;
            this.xrLabel20.Text = "MXN";
            this.xrLabel20.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrLabel19
            // 
            this.xrLabel19.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel19.Dpi = 100F;
            this.xrLabel19.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel19.LocationFloat = new DevExpress.Utils.PointFloat(0F, 0F);
            this.xrLabel19.Name = "xrLabel19";
            this.xrLabel19.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel19.SizeF = new System.Drawing.SizeF(71.87288F, 23F);
            this.xrLabel19.StylePriority.UseBorders = false;
            this.xrLabel19.StylePriority.UseFont = false;
            this.xrLabel19.StylePriority.UseTextAlignment = false;
            this.xrLabel19.Text = "TOTAL";
            this.xrLabel19.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // PageFooter
            // 
            this.PageFooter.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrPageInfo1});
            this.PageFooter.Dpi = 100F;
            this.PageFooter.HeightF = 27.5F;
            this.PageFooter.Name = "PageFooter";
            // 
            // xrPageInfo1
            // 
            this.xrPageInfo1.Dpi = 100F;
            this.xrPageInfo1.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Bold);
            this.xrPageInfo1.Format = "Página {0} de {1}";
            this.xrPageInfo1.LocationFloat = new DevExpress.Utils.PointFloat(9.999974F, 2.500026F);
            this.xrPageInfo1.Name = "xrPageInfo1";
            this.xrPageInfo1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrPageInfo1.SizeF = new System.Drawing.SizeF(100F, 15F);
            this.xrPageInfo1.StylePriority.UseFont = false;
            // 
            // xrLabel1
            // 
            this.xrLabel1.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel1.Dpi = 100F;
            this.xrLabel1.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel1.LocationFloat = new DevExpress.Utils.PointFloat(0F, 135.9515F);
            this.xrLabel1.Name = "xrLabel1";
            this.xrLabel1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel1.SizeF = new System.Drawing.SizeF(71.87284F, 23F);
            this.xrLabel1.StylePriority.UseBorders = false;
            this.xrLabel1.StylePriority.UseFont = false;
            this.xrLabel1.StylePriority.UseTextAlignment = false;
            this.xrLabel1.Text = "Folio";
            this.xrLabel1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrCellFolio
            // 
            this.xrCellFolio.Dpi = 100F;
            this.xrCellFolio.Font = new System.Drawing.Font("Times New Roman", 6F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrCellFolio.Name = "xrCellFolio";
            this.xrCellFolio.StylePriority.UseFont = false;
            this.xrCellFolio.StylePriority.UseTextAlignment = false;
            this.xrCellFolio.Text = "xrCellFolio";
            this.xrCellFolio.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrCellFolio.Weight = 0.661145236844926D;
            // 
            // ExpenseConcentratedReport
            // 
            this.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.Detail,
            this.TopMargin,
            this.BottomMargin,
            this.PageHeader,
            this.DetailReport,
            this.PageFooter});
            this.Landscape = true;
            this.Margins = new System.Drawing.Printing.Margins(24, 26, 20, 15);
            this.PageHeight = 850;
            this.PageWidth = 1100;
            this.Version = "16.2";
            ((System.ComponentModel.ISupportInitialize)(this.xrTablePurchase)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

        }

        #endregion

        private DevExpress.XtraReports.UI.DetailBand Detail;
        private DevExpress.XtraReports.UI.TopMarginBand TopMargin;
        private DevExpress.XtraReports.UI.BottomMarginBand BottomMargin;
        private DevExpress.XtraReports.UI.PageHeaderBand PageHeader;
        private DevExpress.XtraReports.UI.DetailReportBand DetailReport;
        private DevExpress.XtraReports.UI.DetailBand Detail1;
        private DevExpress.XtraReports.UI.GroupFooterBand GroupFooter1;
        private DevExpress.XtraReports.UI.PageFooterBand PageFooter;
        private DevExpress.XtraReports.UI.XRPageInfo xrPageInfo1;
        private DevExpress.XtraReports.UI.XRLabel xrlblUsdImport;
        private DevExpress.XtraReports.UI.XRLabel xrLabel18;
        private DevExpress.XtraReports.UI.XRLabel xrlblUsdISRR;
        private DevExpress.XtraReports.UI.XRLabel xrLabel25;
        private DevExpress.XtraReports.UI.XRLabel xrlblUsdIVAR;
        private DevExpress.XtraReports.UI.XRLabel xrlblUsdDiscount;
        private DevExpress.XtraReports.UI.XRLabel xrlblUsdIEPS;
        private DevExpress.XtraReports.UI.XRLabel xrlblUsdIVA;
        private DevExpress.XtraReports.UI.XRLabel xrlblUsdSubtotal;
        private DevExpress.XtraReports.UI.XRLabel xrlblMxnSubtotal;
        private DevExpress.XtraReports.UI.XRLabel xrlblMxnIVA;
        private DevExpress.XtraReports.UI.XRLabel xrlblMxnIEPS;
        private DevExpress.XtraReports.UI.XRLabel xrlblMxnDiscount;
        private DevExpress.XtraReports.UI.XRLabel xrlblMxnImport;
        private DevExpress.XtraReports.UI.XRLabel xrlblMxnIVAR;
        private DevExpress.XtraReports.UI.XRLabel xrlblMxnISRR;
        private DevExpress.XtraReports.UI.XRLabel xrLabel20;
        private DevExpress.XtraReports.UI.XRLabel xrLabel19;
        private DevExpress.XtraReports.UI.XRTable xrTablePurchase;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow1;
        private DevExpress.XtraReports.UI.XRTableCell xrCellInvoice;
        private DevExpress.XtraReports.UI.XRTableCell xrCellSupplier;
        private DevExpress.XtraReports.UI.XRTableCell xrCellSubtotal;
        private DevExpress.XtraReports.UI.XRTableCell xrCellIVA;
        private DevExpress.XtraReports.UI.XRTableCell xrCellIEPS;
        private DevExpress.XtraReports.UI.XRTableCell xrCellDiscount;
        private DevExpress.XtraReports.UI.XRTableCell xrCellISRR;
        private DevExpress.XtraReports.UI.XRTableCell xrCellIVAR;
        private DevExpress.XtraReports.UI.XRTableCell xrCellImport;
        private DevExpress.XtraReports.UI.XRLabel xraddress;
        private DevExpress.XtraReports.UI.XRLabel xrlblCurrencyUpShow;
        private DevExpress.XtraReports.UI.XRPictureBox xrPictureBox1;
        private DevExpress.XtraReports.UI.XRLabel xrLabel3;
        private DevExpress.XtraReports.UI.XRLabel siteLabel;
        private DevExpress.XtraReports.UI.XRLabel xrRFC;
        private DevExpress.XtraReports.UI.XRLabel xrLabel5;
        private DevExpress.XtraReports.UI.XRLabel xrlblArea;
        private DevExpress.XtraReports.UI.XRLabel xrState;
        private DevExpress.XtraReports.UI.XRLabel xrlblType;
        private DevExpress.XtraReports.UI.XRLabel xrlblTypeShow;
        private DevExpress.XtraReports.UI.XRLabel xrLabel7;
        private DevExpress.XtraReports.UI.XRLabel xrLabel6;
        private DevExpress.XtraReports.UI.XRLabel xrLabel4;
        private DevExpress.XtraReports.UI.XRLabel xrlblSubtotal;
        private DevExpress.XtraReports.UI.XRLabel xrlblIEPs;
        private DevExpress.XtraReports.UI.XRLabel xrlblIVA;
        private DevExpress.XtraReports.UI.XRLabel xrlblImport;
        private DevExpress.XtraReports.UI.XRLabel xrlblSupplier;
        private DevExpress.XtraReports.UI.XRLabel xrlblFolio;
        private DevExpress.XtraReports.UI.XRTableCell xrCellCurrecency;
        private DevExpress.XtraReports.UI.XRTableCell xrCellDate;
        private DevExpress.XtraReports.UI.XRLabel xrlblDate;
        private DevExpress.XtraReports.UI.XRLabel xrlblCurrency;
        private DevExpress.XtraReports.UI.XRLabel xrlblCategory;
        private DevExpress.XtraReports.UI.XRLabel xrTypeExpense;
        private DevExpress.XtraReports.UI.XRTableCell xrCellCategory;
        private DevExpress.XtraReports.UI.XRTableCell xrCellType;
        private DevExpress.XtraReports.UI.XRLabel xrlblDateEndShow;
        private DevExpress.XtraReports.UI.XRLabel xrlblDateEnd;
        private DevExpress.XtraReports.UI.XRLabel xrlblDateStartShow;
        private DevExpress.XtraReports.UI.XRLabel xrlblDateStart;
        private DevExpress.XtraReports.UI.XRLabel xrlblStatusShow;
        private DevExpress.XtraReports.UI.XRLabel xrlblStatus;
        private DevExpress.XtraReports.UI.XRLabel xrlblSupplierUpShow;
        private DevExpress.XtraReports.UI.XRLabel xrlblSupplierUp;
        private DevExpress.XtraReports.UI.XRLabel xrlblAreaShow;
        private DevExpress.XtraReports.UI.XRLabel xrlblCurrencyUp;
        private DevExpress.XtraReports.UI.XRLabel xrLabel1;
        private DevExpress.XtraReports.UI.XRTableCell xrCellFolio;
    }
}
