﻿using System;
using System.Data;
using System.Collections.Generic;
using App.DAL.PurchaseOrder;
using App.Entities;
using DevExpress.XtraReports.UI;
using App.BLL.Supplier;
using App.Entities.ViewModels.PurchaseOrder;
using App.Entities.ViewModels.Sales;
using App.Entities.ViewModels.Invoices;

namespace FloridoERPTX.Reports
{
    public partial class TransactionsCardReport : DevExpress.XtraReports.UI.XtraReport
    {
       
        public DataSet DataSetTableOrdered = null;
        decimal? ivaValue, iepsValue,totalValue=0,subTotal = 0;
        public TransactionsCardReport()
        {
            InitializeComponent();
          
        }

        public DataSet printTable(TransactionsCardIndex model,string siteName="Florido") //se encarga de imprimir la orden del proveedor
        {
            try
            {
                siteLabel.Text = siteName;
                labelDate.Text = DateTime.Now.Date.ToString("dd/MM/yyyy");
               labelDate1.Text = model.StartDate;
                labelDate2.Text = model.EndDate;

                //lbl_credit_zrate.Text = model.Totales[0, 0].ToString("C");
                //lbl_credit_taxed.Text = model.Totales[1, 0].ToString("C");
                //lbl_credit_tax.Text = model.Totales[2, 0].ToString("C");
                //lbl_credit_ieps.Text = model.Totales[3, 0].ToString("C");
                //lbl_total_credit.Text = model.Totales[4, 0].ToString("C");

                //lbl_cash_zrate.Text = model.Totales[0, 1].ToString("C");
                //lbl_cash_taxed.Text = model.Totales[1, 1].ToString("C");
                //lbl_cash_tax.Text = model.Totales[2, 1].ToString("C");
                //lbl_cash_ieps.Text = model.Totales[3, 1].ToString("C");
                //lbl_total_cash.Text = model.Totales[4, 1].ToString("C");

                //lbl_total_zrate.Text = model.Totales[0, 2].ToString("C");
                //lbl_total_taxed.Text = model.Totales[1, 2].ToString("C");
                //lbl_total_tax.Text = model.Totales[2, 2].ToString("C");
                //lbl_total_ieps.Text = model.Totales[3, 2].ToString("C");
                //lbl_total_global.Text = model.Totales[4, 2].ToString("C");


                DataSetTableOrdered = new DataSet();

                DataTable DataTableOrdered = new DataTable();
                DataSetTableOrdered.Tables.Add(DataTableOrdered);

                
               
                 
                DataTableOrdered.Columns.Add(new DataColumn("InvoiceNumber"));               
                DataTableOrdered.Columns.Add(new DataColumn("CreateDate"));
                DataTableOrdered.Columns.Add(new DataColumn("CustomerCode"));
                DataTableOrdered.Columns.Add(new DataColumn("CustomerName"));
                DataTableOrdered.Columns.Add(new DataColumn("DueDate"));
                DataTableOrdered.Columns.Add(new DataColumn("Amount"));
                DataTableOrdered.Columns.Add(new DataColumn("IepsPredicateReport"));
                DataTableOrdered.Columns.Add(new DataColumn("Ieps"));
                DataTableOrdered.Columns.Add(new DataColumn("Tax"));
                DataTableOrdered.Columns.Add(new DataColumn("TotalAmount"));
                DataTableOrdered.Columns.Add(new DataColumn("Balance"));
                DataTableOrdered.Columns.Add(new DataColumn("CurrencyCode"));
                DataTableOrdered.Columns.Add(new DataColumn("FiscalNumberReport"));
                DataTableOrdered.Columns.Add(new DataColumn("Status"));
                DataTableOrdered.Columns.Add(new DataColumn("PaymentMethod"));





                foreach (var item in model.DataReport)
                {
                    DataRow DataRowOrdered = DataSetTableOrdered.Tables[0].NewRow();
                   

                    //DataRowOrdered["InvoiceNumber"] = item.Fecha.Value.ToString("dd/MM/yyyy");
                    //DataRowOrdered["CustomerName"] = Math.Round(item.Monto.Value,2);
                    DataRowOrdered["CreateDate"] = item.Fecha.Value.ToString("dd/MM/yyyy"); 
                    //DataRowOrdered["CustomerCode"] = Math.Round(item.Retiro.Value, 2);
                    //DataRowOrdered["DueDate"] = item.DueDate;
                    DataRowOrdered["Amount"] = "$" + Math.Round(item.Monto.Value,2);
                    //DataRowOrdered["IepsPredicateReport"] = item.IepsPredicateReport!=null? item.IepsPredicateReport.Replace("@", "\n") :"" ;
                    //DataRowOrdered["Ieps"] = "$" + item.Ieps.ToString("0.00"); ; 
                    DataRowOrdered["Tax"] = "$" + Math.Round(item.Retiro.Value, 2);
                    DataRowOrdered["TotalAmount"] = "$" + Math.Round(item.Total.Value, 2);
                    //DataRowOrdered["Balance"] = item.Balance;
                    //DataRowOrdered["CurrencyCode"] = item.CurrencyCode;
                    //DataRowOrdered["FiscalNumberReport"] = item.FiscalNumberReport!=null? item.FiscalNumberReport.Replace("@", Environment.NewLine) :"";
                    //DataRowOrdered["Status"] = item.Status;
                    //DataRowOrdered["PaymentMethod"] = item.CreditOrCash;

                    //var ieps = item.IepsPredicateReport != null ? item.IepsPredicateReport.Replace("@", Environment.NewLine) : "";
                    DataSetTableOrdered.Tables[0].Rows.Add(DataRowOrdered);
                    //ivaValue += item.Tax;
                    //iepsValue += item.Ieps;
                    //subTotal += item.Amount;
                    totalValue += item.Total;



                }
               
                //ivaLabel.Text = "$"+ivaValue.ToString("0.00");
                //iepsLabel.Text = "$" + iepsValue.ToString("0.00");
                //subtotalLabel.Text = "$" + subTotal.ToString("0.00");
                totalLabel.Text = "$" + Math.Round(totalValue.Value, 2);
               


                //InvoiceNumber.DataBindings.Add(new XRBinding("Text", null, "xrTableRowFormat.InvoiceNumber"));
                //CustomerName.DataBindings.Add(new XRBinding("Text", null, "xrTableRowFormat.CustomerName"));
                CreateDate.DataBindings.Add(new XRBinding("Text", null, "xrTableRowFormat.CreateDate"));
                //CustomerCode.DataBindings.Add(new XRBinding("Text", null, "xrTableRowFormat.CustomerCode"));
                //DueDate.DataBindings.Add(new XRBinding("Text", null, "xrTableRowFormat.DueDate"));
                Amount.DataBindings.Add(new XRBinding("Text", null, "xrTableRowFormat.Amount"));
                //IepsPredicate.DataBindings.Add(new XRBinding("Text", null, "xrTableRowFormat.IepsPredicateReport"));
                //Ieps.DataBindings.Add(new XRBinding("Text", null, "xrTableRowFormat.Ieps"));
                Tax.DataBindings.Add(new XRBinding("Text", null, "xrTableRowFormat.Tax"));
                TotalAmount.DataBindings.Add(new XRBinding("Text", null, "xrTableRowFormat.TotalAmount"));
                //Balance.DataBindings.Add(new XRBinding("Text", null, "xrTableRowFormat.Balance"));
                //CurrencyCode.DataBindings.Add(new XRBinding("Text", null, "xrTableRowFormat.CurrencyCode"));
                //FiscalNumber.DataBindings.Add(new XRBinding("Text", null, "xrTableRowFormat.FiscalNumberReport"));
                //Status.DataBindings.Add(new XRBinding("Text", null, "xrTableRowFormat.Status"));
                //PaymentMethod.DataBindings.Add(new XRBinding("Text", null, "xrTableRowFormat.PaymentMethod"));


                return DataSetTableOrdered;
            }
            catch (Exception ex)
            {
                string d=ex.Message;
                throw;
            }
          
        }

       


    }
}
