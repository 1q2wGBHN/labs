﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
using System.Data;
using App.Entities.ViewModels.RawMaterial;
using System.Globalization;

namespace FloridoERPTX.Reports
{
    public partial class RawMaterialReport : DevExpress.XtraReports.UI.XtraReport
    {
        public DataSet DataSetTableMovements = null;
        public RawMaterialReport()
        {
            InitializeComponent();
        }
        public DataSet printTable(RawMaterialHeader raw)
        {
            try
            {
                xrLabelFolio.Text = raw.Folio.ToString();
                xrLabelDate.Text = raw.TransferDate;
                xrSite.Text = raw.SiteName;
                xrLabelLocation.Text = raw.StorageLocation;
                if (raw.Status == 0)
                    xrStatus.Text = "Iniciado";
                else if (raw.Status == 2)
                    xrStatus.Text = "Aprobado por Recibo";
                else if (raw.Status == 8)
                    xrStatus.Text = "Cancelado";
                else
                    xrStatus.Text = "Completado";

                xrComment.Text = raw.Comment;

                DataSetTableMovements = new DataSet();
                DataTable table = new DataTable();
                DataSetTableMovements.Tables.Add(table);

                table.Columns.Add(new DataColumn("cellPartNumber"));
                table.Columns.Add(new DataColumn("cellDescription"));
                table.Columns.Add(new DataColumn("cellQuantity"));
                table.Columns.Add(new DataColumn("cellCost"));
                table.Columns.Add(new DataColumn("cellIva"));
                table.Columns.Add(new DataColumn("cellIeps"));
                table.Columns.Add(new DataColumn("cellTotal"));

                var cultureInfo = CultureInfo.GetCultureInfo("en-US");
                decimal subtotal = 0;
                decimal ivatotal = 0;
                decimal iepstotal = 0;
                decimal Total = 0;

                foreach (var item in raw.listRaw)
                {
                    DataRow DataRowOrdered = DataSetTableMovements.Tables[0].NewRow();
                    DataRowOrdered["cellPartNumber"] = item.Part_Number;
                    DataRowOrdered["cellDescription"] = item.Description;
                    DataRowOrdered["cellQuantity"] = (int)item.Quantity;
                    DataRowOrdered["cellCost"] = "$"+item.UnitCost.ToString("N4");
                    DataRowOrdered["cellIva"] = "$" + item.Iva.ToString("N4");
                    DataRowOrdered["cellIeps"] = "$" + item.IEPS.ToString("N4");
                    DataRowOrdered["cellTotal"] = "$" + item.Amount.ToString("N4");

                    subtotal += item.SubTotal;
                    Total += item.Amount;
                    ivatotal += item.Iva;
                    iepstotal += item.IEPS;

                    DataSetTableMovements.Tables[0].Rows.Add(DataRowOrdered);
                }

                xrLabelAmount.Text = String.Format(cultureInfo, "{0:C4}", Total);
                xrLabelIEPS.Text = String.Format(cultureInfo, "{0:C4}", iepstotal);
                xrLabelIVA.Text = String.Format(cultureInfo, "{0:C4}", ivatotal);
                xrLabelSub.Text = String.Format(cultureInfo, "{0:C4}", subtotal);

                xrTableCellPartNumber.DataBindings.Add(new XRBinding("Text", null, "xrTableRow1.cellPartNumber"));
                xrTableCellDescription.DataBindings.Add(new XRBinding("Text", null, "xrTableRow1.cellDescription"));
                xrTableCellQuantity.DataBindings.Add(new XRBinding("Text", null, "xrTableRow1.cellQuantity"));
                xrTableCellCost.DataBindings.Add(new XRBinding("Text", null, "xrTableRow1.cellCost"));
                xrTableCellIva.DataBindings.Add(new XRBinding("Text", null, "xrTableRow1.cellIva"));
                xrTableCellIeps.DataBindings.Add(new XRBinding("Text", null, "xrTableRow1.cellIeps"));
                xrTableCellTotal.DataBindings.Add(new XRBinding("Text", null, "xrTableRow1.cellTotal"));

                return DataSetTableMovements;
            }
            catch (Exception ex)
            {
                string d = ex.Message;
                throw;
            }

        }
    }
}
