﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
using System.Data;
using System.Collections.Generic;
using App.Entities.ViewModels.CostCenter;
using System.Globalization;

namespace FloridoERPTX.Reports
{
    public partial class CostCenterDocument : DevExpress.XtraReports.UI.XtraReport
    {
        public DataSet DataSetTableOrdered = null;

        public CostCenterDocument()
        {
            InitializeComponent();
        }

        public DataSet PrintTable(GICostCenter model)
        {
            try
            {
                DataSetTableOrdered = new DataSet();
                DataTable DataTableOrdered = new DataTable();
                DataSetTableOrdered.Tables.Add(DataTableOrdered);

                xrLabelDate.Text = DateTime.Now.ToString("dd/MM/yyyy");
                xrLabelDocumentNumber.Text = model.GIDocument;
                xrLabelSite.Text = model.OriginSite;
                xrLabelCostCenter.Text = model.CostCenter;
                xrLabelUserAuthorize.Text = model.UserAuthorize;

                DataTableOrdered.Columns.Add(new DataColumn("xrTableCellPartNumber"));
                DataTableOrdered.Columns.Add(new DataColumn("xrTableCellPartDescription"));
                DataTableOrdered.Columns.Add(new DataColumn("xrTableCellQuantity"));
                DataTableOrdered.Columns.Add(new DataColumn("xrTableCellCost"));
                DataTableOrdered.Columns.Add(new DataColumn("xrTableCellAmount"));
                DataTableOrdered.Columns.Add(new DataColumn("xrTableCellIEPS"));
                DataTableOrdered.Columns.Add(new DataColumn("xrTableCellIva"));
                DataTableOrdered.Columns.Add(new DataColumn("xrTableCellTotal"));

                decimal subtotal = 0;
                decimal ivatotal = 0;
                decimal iepstotal = 0;
                decimal Total = 0;
                foreach (var item in model.Items)
                {
                    DataRow DataRowOrdered = DataSetTableOrdered.Tables[0].NewRow();
                    DataRowOrdered["xrTableCellPartNumber"] = item.PartNumber;
                    DataRowOrdered["xrTableCellPartDescription"] = item.Description;
                    DataRowOrdered["xrTableCellQuantity"] = item.Quantity;
                    DataRowOrdered["xrTableCellCost"] = "$" + item.UnitCost.ToString("N4");
                    DataRowOrdered["xrTableCellAmount"] = "$" + item.Amount.ToString("N4");
                    DataRowOrdered["xrTableCellIEPS"] = "$" + item.Ieps.ToString("N4");
                    DataRowOrdered["xrTableCellIva"] = "$" + item.Iva.ToString("N4");
                    DataRowOrdered["xrTableCellTotal"] = "$" + item.Total.ToString("N4");
                    subtotal += item.Amount;
                    ivatotal += item.Iva;
                    iepstotal += item.Ieps;
                    Total += item.Total;
                    DataSetTableOrdered.Tables[0].Rows.Add(DataRowOrdered);
                }

                xrLabelAmount.Text = Total.ToString("C4", CultureInfo.CreateSpecificCulture("en-US"));
                xrLabelIEPS.Text = iepstotal.ToString("C4", CultureInfo.CreateSpecificCulture("en-US"));
                xrLabelIVA.Text = ivatotal.ToString("C4", CultureInfo.CreateSpecificCulture("en-US"));
                xrLabelSub.Text = subtotal.ToString("C4", CultureInfo.CreateSpecificCulture("en-US"));

                xrTableCellPartNumber.DataBindings.Add(new XRBinding("Text", null, "xrTableRow1.xrTableCellPartNumber"));
                xrTableCellPartDescription.DataBindings.Add(new XRBinding("Text", null, "xrTableRow1.xrTableCellPartDescription"));
                xrTableCellQuantity.DataBindings.Add(new XRBinding("Text", null, "xrTableRow1.xrTableCellQuantity"));
                xrTableCellCost.DataBindings.Add(new XRBinding("Text", null, "xrTableRow1.xrTableCellCost"));
                xrTableCellAmount.DataBindings.Add(new XRBinding("Text", null, "xrTableRow1.xrTableCellAmount"));
                xrTableCellIEPS.DataBindings.Add(new XRBinding("Text", null, "xrTableRow1.xrTableCellIEPS"));
                xrTableCellIva.DataBindings.Add(new XRBinding("Text", null, "xrTableRow1.xrTableCellIva"));
                xrTableCellTotal.DataBindings.Add(new XRBinding("Text", null, "xrTableRow1.xrTableCellTotal"));

                return DataSetTableOrdered;
            }
            catch (Exception ex)
            {
                string d = ex.Message;
                throw;
            }
        }
    }
}