﻿using System;
using System.Data;
using DevExpress.XtraReports.UI;
using App.Entities.ViewModels.Invoices;
using System.Globalization;
using System.Linq;
using App.Entities.ViewModels.Sales;

namespace FloridoERPTX.Reports
{
    public partial class TransCardReport : DevExpress.XtraReports.UI.XtraReport
    {

        public DataSet DataSetTableOrdered = null;
        public TransCardReport()
        {
            InitializeComponent();

        }
        public DataSet printTable(TransactionsCardIndex Model, string siteName = "Florido")
        {
            try
            {
                Date.Text = string.Format("{0} - {1}", Model.StartDate, Model.EndDate);
                siteLabel.Text = siteName;
                Cashier.Text = Model.CashierName;

                DataSetTableOrdered = new DataSet();

                DataTable DataTableOrdered = new DataTable();
                DataSetTableOrdered.Tables.Add(DataTableOrdered);

                DataTableOrdered.Columns.Add(new DataColumn("tableDate"));
                DataTableOrdered.Columns.Add(new DataColumn("amount"));
                DataTableOrdered.Columns.Add(new DataColumn("cashBack"));
                DataTableOrdered.Columns.Add(new DataColumn("cardNumber"));
                DataTableOrdered.Columns.Add(new DataColumn("authCode"));
                DataTableOrdered.Columns.Add(new DataColumn("sale"));
                DataTableOrdered.Columns.Add(new DataColumn("saleId"));
                DataTableOrdered.Columns.Add(new DataColumn("dataCashier"));
                DataTableOrdered.Columns.Add(new DataColumn("paymentMethod"));
                DataTableOrdered.Columns.Add(new DataColumn("responseCode"));
                DataTableOrdered.Columns.Add(new DataColumn("responseMessage"));
                DataTableOrdered.Columns.Add(new DataColumn("type"));


                foreach (var item in Model.CashierReport)
                {
                    DataRow DataRowOrdered = DataSetTableOrdered.Tables[0].NewRow();
                    DataRowOrdered["tableDate"] = item.FechaS;
                    DataRowOrdered["amount"] = "$" + item.Amount;
                    DataRowOrdered["cashBack"] = "$" + item.CashBack;
                    DataRowOrdered["cardNumber"] = item.CardNumber;
                    DataRowOrdered["authCode"] = item.AuthCode;
                    DataRowOrdered["sale"] = item.FolioVenta;
                    DataRowOrdered["saleId"] = item.SaleId;
                    DataRowOrdered["dataCashier"] = item.NombreCajero;
                    DataRowOrdered["paymentMethod"] = item.PaymentMethod;
                    DataRowOrdered["responseCode"] = item.ResponseCode;
                    DataRowOrdered["responseMessage"] = item.ResponseMessage;
                    DataRowOrdered["type"] = item.SaleType;

                    DataSetTableOrdered.Tables[0].Rows.Add(DataRowOrdered);
                }

                tableDate.DataBindings.Add(new XRBinding("Text", null, "xrTableRowFormat.tableDate"));
                amount.DataBindings.Add(new XRBinding("Text", null, "xrTableRowFormat.amount"));
                cashBack.DataBindings.Add(new XRBinding("Text", null, "xrTableRowFormat.cashBack"));
                cardNumber.DataBindings.Add(new XRBinding("Text", null, "xrTableRowFormat.cardNumber"));
                authCode.DataBindings.Add(new XRBinding("Text", null, "xrTableRowFormat.authCode"));
                sale.DataBindings.Add(new XRBinding("Text", null, "xrTableRowFormat.sale"));
                saleId.DataBindings.Add(new XRBinding("Text", null, "xrTableRowFormat.saleId"));
                dataCashier.DataBindings.Add(new XRBinding("Text", null, "xrTableRowFormat.dataCashier"));
                paymentMethod.DataBindings.Add(new XRBinding("Text", null, "xrTableRowFormat.paymentMethod"));
                responseCode.DataBindings.Add(new XRBinding("Text", null, "xrTableRowFormat.responseCode"));
                responseMessage.DataBindings.Add(new XRBinding("Text", null, "xrTableRowFormat.responseMessage"));
                type.DataBindings.Add(new XRBinding("Text", null, "xrTableRowFormat.type"));

                return DataSetTableOrdered;
            }
            catch (Exception ex)
            {
                string d = ex.Message;
                throw;
            }
        }
    }
}
