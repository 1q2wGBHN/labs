﻿using System;
using System.Data;
using System.Collections.Generic;
using App.DAL.PurchaseOrder;
using App.Entities;
using DevExpress.XtraReports.UI;
using App.BLL.Supplier;
using App.Entities.ViewModels.PurchaseOrder;
using App.Entities.ViewModels.Sales;
using App.Entities.ViewModels.Invoices;

namespace FloridoERPTX.Reports
{
    public partial class TransactionsCardByCashierReport : DevExpress.XtraReports.UI.XtraReport
    {
       
        public DataSet DataSetTableOrdered = null;
        decimal? totalValue = 0;
        public TransactionsCardByCashierReport()
        {
            InitializeComponent();
          
        }

        public DataSet printTable(TransactionsCardIndex model,string siteName="Florido") //se encarga de imprimir la orden del proveedor
        {
            try
            {
                siteLabel.Text = siteName;
                labelDate.Text = DateTime.Now.Date.ToString("dd/MM/yyyy");
                labelDate1.Text = model.StartDate;
                labelDate2.Text = model.EndDate;               

                DataSetTableOrdered = new DataSet();

                DataTable DataTableOrdered = new DataTable();
                DataSetTableOrdered.Tables.Add(DataTableOrdered);
                                                                            
                DataTableOrdered.Columns.Add(new DataColumn("CreateDate"));
                DataTableOrdered.Columns.Add(new DataColumn("hora"));
                DataTableOrdered.Columns.Add(new DataColumn("tarjeta"));
                DataTableOrdered.Columns.Add(new DataColumn("aut"));
                DataTableOrdered.Columns.Add(new DataColumn("folio"));
                DataTableOrdered.Columns.Add(new DataColumn("Amount"));
                DataTableOrdered.Columns.Add(new DataColumn("caja"));
                DataTableOrdered.Columns.Add(new DataColumn("cajero"));
                DataTableOrdered.Columns.Add(new DataColumn("cashback"));
                DataTableOrdered.Columns.Add(new DataColumn("TotalAmount"));                
                
                foreach (var item in model.CashierReport)
                {
                    DataRow DataRowOrdered = DataSetTableOrdered.Tables[0].NewRow();
                                       
                    DataRowOrdered["CreateDate"] = item.FechaS;
                    DataRowOrdered["hora"] = item.HoraS;
                    DataRowOrdered["tarjeta"] = item.CardNumber;
                    DataRowOrdered["aut"] = item.AuthCode;
                    DataRowOrdered["folio"] = item.FolioVenta;                    
                    DataRowOrdered["Amount"] = "$" + Math.Round(item.Amount.Value,2);
                    DataRowOrdered["caja"] = item.PosId;
                    DataRowOrdered["cajero"] = item.NombreCajero;
                    DataRowOrdered["cashback"] = "$" + Math.Round(item.CashBack.Value, 2);
                    DataRowOrdered["TotalAmount"] = "$" + Math.Round(item.Total.Value, 2);                    

                    DataSetTableOrdered.Tables[0].Rows.Add(DataRowOrdered);
                    totalValue += item.Total;
                    
                }
               
                totalLabel.Text = "$" + Math.Round(totalValue.Value, 2);

                CreateDate.DataBindings.Add(new XRBinding("Text", null, "xrTableRowFormat.CreateDate"));
                hora.DataBindings.Add(new XRBinding("Text", null, "xrTableRowFormat.hora"));
                tarjeta.DataBindings.Add(new XRBinding("Text", null, "xrTableRowFormat.tarjeta"));
                aut.DataBindings.Add(new XRBinding("Text", null, "xrTableRowFormat.aut"));
                folio.DataBindings.Add(new XRBinding("Text", null, "xrTableRowFormat.folio"));                
                Amount.DataBindings.Add(new XRBinding("Text", null, "xrTableRowFormat.Amount"));
                caja.DataBindings.Add(new XRBinding("Text", null, "xrTableRowFormat.caja"));
                cajero.DataBindings.Add(new XRBinding("Text", null, "xrTableRowFormat.cajero"));
                cashback.DataBindings.Add(new XRBinding("Text", null, "xrTableRowFormat.cashback"));
                TotalAmount.DataBindings.Add(new XRBinding("Text", null, "xrTableRowFormat.TotalAmount"));                

                return DataSetTableOrdered;
            }
            catch (Exception ex)
            {
                string d=ex.Message;
                throw;
            }
          
        }

       


    }
}
