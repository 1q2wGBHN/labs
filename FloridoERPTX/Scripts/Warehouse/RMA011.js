﻿window.setTimeout(function () { $("#fa").click(); }, 1000);
const fomartmoney = money => decimal => new Intl.NumberFormat("en-US", { style: "currency", currency: "USD", currencyDisplay: "symbol", maximumFractionDigits: decimal }).format(money); //Formato de Decimales
var valuesInputCost = [];
var variableTableTemp = [];

$("#ValidDate1").datepicker({
    autoclose: true,
    todayHighlight: true,
    showButtonPanel: true,
}).on("changeDate", function (e) {
    SerchRMA();
});

$("#ValidDate2").datepicker({
    autoclose: true,
    startDate: '01/01/1950',
    todayHighlight: true,
}).on("changeDate", function (e) {
    SerchRMA();
});

$(document).on("keypress", '.Numeric', function (event) {
    var keyValue = event.key;
    if (keyValue.match(/[0-9]/g))
        return true;
    else
        return false;
});

var table = $('#TablePurchases').DataTable({
    oLanguage: {
        "sProcessing": "Procesando...", "sLengthMenu": "Mostrar _MENU_ registros", "sZeroRecords": "No se encontraron resultados", "sEmptyTable": "Ningún dato disponible en esta tabla", "sInfo": "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros", "sInfoEmpty": "Mostrando registros del 0 al 0 de un total de 0 registros", "sInfoFiltered": "(filtrado de un total de _MAX_ registros)", "sInfoPostFix": "", "sSearch": "Buscar:", "sUrl": "", "sInfoThousands": ",", "sLoadingRecords": "Cargando...", "oPaginate": { "sFirst": "Primero", "sLast": "Último", "sNext": "Siguiente", "sPrevious": "Anterior" }, "oAria": { "sSortAscending": ": Activar para ordenar la columna de manera ascendente", "sSortDescending": ": Activar para ordenar la columna de manera descendente" }
    },
    responsive: true,
    autoWidth: true,
    order: [[1, "asc"]],
    dom: "<'row'<'col-sm-4'l><'col-sm-4 text-left'B><'col-sm-4'f>t<'col-sm-6'i><'col-sm-6'p>>",
    lengthMenu: [[10, 25, 50, -1], [10, 25, 50, "Todos"]],
    buttons: [
        { extend: 'csv', text: 'Excel', title: 'AprobacionRMA', className: 'btn-sm' },
        {
            extend: 'pdf', text: 'PDF', title: 'AprobacionRMA', className: 'btn-sm', exportOptions: {
                columns: [1, 2, 3]
            },
            customize: function (doc) {
                doc.content[1].table.widths =
                    Array(doc.content[1].table.body[0].length + 1).join('*').split('');
            }
        },
        { extend: 'print', text: 'Imprimir', className: 'btn-sm' },
    ],
    columns: [
        {
            "class": "details-control",
            "orderable": false,
            "data": null,
            "defaultContent": "",
            "width": "1%"
        },
        { data: 'DamagedGoodsDoc', width: '1%' },
        { data: 'Supplier', width: '1%' },
        { data: 'Comment', width: '1%' },
        { data: 'ProcessStatusDescription', width: '1%' }
    ],
    columnDefs: [{
        targets: [0, 1, 2, 3, 4],
        width: "1%"
    }]
});

var detailRows = [];

$('#TablePurchases tbody').on('click', 'tr td.details-control', function () {
    var tr = $(this).closest('tr');
    var row = table.row(tr);

    if (row.child.isShown()) {
        row.child.hide();
        tr.removeClass('details');
    }
    else {
        if (table.row('.details').length) {
            $('.details-control', table.row('.details').node()).click();
        }
        row.child(format(row.data())).show();
        tr.addClass('details');
    }
});

table.on('draw', function () {
    $.each(detailRows, function (i, id) {
        $('#' + id + ' td.details-control').trigger('click');
    });
});

function setValues(Quantity, Total) {
    if (Total <= 0)
        Total = 0;
    $("#Quantity").text(Quantity);
    $("#Total").text(fomartmoney(Total)(4));
}

var Quantity = 0;
var Total = 0;

var list = [];
var listDelete = [];
function format(d) {
    list = [];
    listDelete = [];
    var detailItems = "";
    var tabledetail = $('<div/>').addClass('loading').text('Cargando Datos...');
    StartLoading();
    $.ajax({
        url: "/DamagedsGoods/ActionGetItemSupplierByRmaLastCost",
        data: { "Folio": d.DamagedGoodsDoc, "Type": "RMA", "Status": 0 },
        type: "GET",
        success: function (data) {
            $.each(data, function (index, value) {
                
                listDelete.push({ PartNumber: value.PartNumber });
                total = value.Iva + value.Ieps + value.Amount_Doc;
                detailItems += "<tr><td class='text-center'><div class='checkbox'><label><input type='checkbox' class='i-checks' PartNumber = " + value.PartNumber + " id=" + value.PartNumber + " Quantity=" + value.Quantity + " Amount=" + value.Amount_Doc + " Price=" + value.Price + " Iterate=" + index + " /></label></div></td><td>" + value.PartNumber + "</td><td>" + value.Description + "</td><td>" + value.Size + "</td><td class='text-right'>" + value.Storage + "</td><td class='text-right'>" + value.Quantity + "</td><td><input type='number' class='form-control Numeric' value = '" + value.Quantity + "' " + `onkeyup="ValidateNumeric('${value.PartNumber}', ${index})"` + " id='inp" + value.PartNumber + "' max='" + value.Quantity + "' min='0' " + `oninput="ValidateNumeric('${value.PartNumber}', ${index})"` + `/></td><td><select id='Sel${value.PartNumber}' onchange="UpdateTableCost('${value.PartNumber}', ${index})" class='form-control'>${FillInputCost(value.listcost)}</select></td><td class='text-right'>${fomartmoney(value.Amount_Doc)(4)}</td><td class='text-right'>${fomartmoney(value.Ieps)(4)}</td><td class='text-right'>${fomartmoney(value.Iva)(4)}</td><td class='text-right'>${fomartmoney(total)(4)}</td><td  style='display:none;' class='text-right'>${value.IepsPorcentage}</td><td style='display:none;' class='text-right'>${value.IvaPorcentage}</td></tr>`
            });
            var Apro = "";
            if (d.ProcessStatusDescription == "Inicial") {
                Apro = '<div class="col-md-6 pull-right"><table id="totals" class="table table-striped table-bordered table-hover text-center" style="width:100%"><thead><tr><th class="text-center">Cantidad</th><th class="text-center">Total</th></tr></thead><tbody> <tr> <td id="Quantity">&nbsp;</td> <td id="Total">&nbsp;</td> </tr> </tbody></table></div>';
                Apro += '<div class="form-group col-md-12"> <div class="row"> <label class="col-md-12 control-label">Comentario</label> </div> <div class="row"> <div class="col-md-10"> <textarea id="comment" style="max-width:500px;max-height:150px;min-width:100px;min-height:100px;width:200px" class="form-control" maxlength="249">'+d.Comment+'</textarea> </div> <div class="pull-right"> <button id="btn-cancel" type="button" class="btn btn-sm btn-danger" DamagedGoodsDoc="' + d.DamagedGoodsDoc + '" SupplierId="' + d.SupplierId + '"><i class="fa fa-close"></i> Rechazar</button>  <button id="btn-save" type="button" class="btn btn-sm btn-success" DamagedGoodsDoc="' + d.DamagedGoodsDoc + '" SupplierId="' + d.SupplierId + '"><i class="fa fa-save"></i> Aprobar</button></div></div></div>';
            }
            tabledetail.html('<table id="tabledetail" class="table table-striped table-bordered table-hover" style="width:100%"><thead><tr><th></th><th>Codigo</th><th>Nombre</th><th>UM</th><th>Locacion</th><th>Cantidad</th><th>Nueva Cantidad</th><th>Costo</th><th>Monto</th><th>IEPS</th><th>IVA</th><th>Total</th><th  style="display:none;">IEPS%</th><th  style="display:none;">IVA%</th></tr></thead><tbody>' + detailItems + '</tbody></table>' + Apro).removeClass('loading');

            $('input[type=checkbox]').iCheck({
                checkboxClass: 'icheckbox_square-green',
            }).on('ifClicked', function () {
                $(this).trigger("change");
                var item = this;
                var check = item.checked;
                var PartNumber = item.attributes.partnumber.value;
                var index = item.attributes.iterate.value;
                var id = "#inp" + item.attributes.id.value;
                var quantity = parseInt($(id).val());
                var priceCurrent = parseFloat($("#Sel" + PartNumber).val());
                var totalCurrentComma = variableTableTemp.cell(index, 11).data().replace(/,/g, '');
                var totalCurrent = parseFloat(totalCurrentComma.substring(1));

                var newQuantity = parseInt($("#Quantity").text());
                var newTotalComma = $("#Total").text().replace(/,/g, '');
                var newTotal = parseFloat(newTotalComma.substring(1));
                if (!isNaN(newQuantity)) {
                    Quantity = newQuantity;
                    Total = newTotal;
                }
                if (!check) {
                    Quantity += quantity;
                    Total += totalCurrent;
                    setValues(Quantity, Total);
                    list.push({ PartNumber: PartNumber, Quantity: quantity, RealQuantity: quantity, Price: priceCurrent });
                }
                else {
                    $(id).iCheck('uncheck');
                    Quantity -= quantity;
                    Total -= (totalCurrent);
                    setValues(Quantity, Total);
                    list = list.filter(x => x.PartNumber != PartNumber);
                }

            });
            EndLoading();
            reloadStyleTable();
            //$.each(data, function (index, value) {
            //    $('#Sel' + value.PartNumber).select2();
            //});
        }
    });
    return tabledetail;
}

function reloadStyleTable() {
    variableTableTemp = $('#tabledetail').DataTable({
        oLanguage: {
            "sProcessing": "Procesando...", "sLengthMenu": "Mostrar _MENU_ registros", "sZeroRecords": "No se encontraron resultados", "sEmptyTable": "Ningún dato disponible en esta tabla", "sInfo": "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros", "sInfoEmpty": "Mostrando registros del 0 al 0 de un total de 0 registros", "sInfoFiltered": "(filtrado de un total de _MAX_ registros)", "sInfoPostFix": "", "sSearch": "Buscar:", "sUrl": "", "sInfoThousands": ",", "sLoadingRecords": "Cargando...", "oPaginate": { "sFirst": "Primero", "sLast": "Último", "sNext": "Siguiente", "sPrevious": "Anterior" }, "oAria": { "sSortAscending": ": Activar para ordenar la columna de manera ascendente", "sSortDescending": ": Activar para ordenar la columna de manera descendente" }
        },
        //lengthMenu: [[10, 25, 50, -1], [10, 25, 50, "Todos"]],
        //buttons: [],
        //columns: [
        //    { targets: 0, data: 'check' },
        //    { targets: 1, data: 'part_number' },
        //    { targets: 2, data: 'part_description' },
        //    { targets: 3, data: 'um' },
        //    { targets: 4, data: 'location' },
        //    { targets: 5, data: 'quantity' },
        //    { targets: 6, data: 'newQuantity' },
        //    { targets: 7, data: 'cost' },
        //    { targets: 8, data: 'amount' },
        //    { targets: 9, data: 'ieps'},
        //    { targets: 10, data: 'iva' },
        //    { targets: 10, data: 'total_amount' },
        //    { targets: 10, data: 'iepsP', visible: false },
        //    { targets: 10, data: 'ivaP', visible: false },
        //],
    });
}

function FillInputCost(ListValues) {
    var stringValue = '';
    $.each(ListValues, function (index, value) {
        if (index == 0)
            stringValue += '<option selected value=' + value.t_cost + '> $' + value.t_cost + '</option>';
        else
            stringValue += '<option value=' + value.t_cost + '> $' + value.t_cost + '</option>';
    });
    if (stringValue == "")
        stringValue = '<option selected value="0">$0</option>'
    return stringValue;
}

function UpdateTableCost(idPartNumber, index) {
    var quantityCurrent = parseFloat($("#inp" + idPartNumber).val());
    var costCurrent = parseFloat($("#Sel" + idPartNumber).val());
    var amount_doc = quantityCurrent * costCurrent;
    var iepsP = variableTableTemp.cell(index, 12).data();
    var ivaP = variableTableTemp.cell(index, 13).data();
    var iepsNewCurrent = amount_doc * (iepsP / 100);
    var ivaNewCurrent = (amount_doc + iepsNewCurrent) * (ivaP / 100);
    myIndexObj = list.findIndex(o => o.PartNumber == idPartNumber);
    if (myIndexObj >= 0) {
        list[myIndexObj].Price = costCurrent;
        list[myIndexObj].RealQuantity = $("#inp" + idPartNumber).val();
    }
    variableTableTemp.cell(index, 8).data(fomartmoney(amount_doc)(4)).draw();
    variableTableTemp.cell(index, 9).data(fomartmoney(iepsNewCurrent)(4)).draw();
    variableTableTemp.cell(index, 10).data(fomartmoney(ivaNewCurrent)(4)).draw();
    variableTableTemp.cell(index, 11).data(fomartmoney(amount_doc + iepsNewCurrent + ivaNewCurrent)(4)).draw();
    getValues();
}

function getValues() {
    var Quantity = 0;
    var Total = 0;
    $('#tabledetail').find('tr').each(function () {
        var row = $(this);
        if (row.find('input[type="checkbox"]').is(':checked')) {
            var check = row.find('input[type="checkbox"]');
            var id = "#inp" + check[0].attributes.id.value;
            var index = check[0].attributes.iterate.value;
            var amount_doc_comma = variableTableTemp.cell(index, 11).data().replace(/,/g, '');
            var amount_doc = parseFloat(amount_doc_comma.substring(1));
            var quantity = parseInt($(id).val());
            Quantity += quantity;
            Total += parseFloat(amount_doc);
        }
    });
    setValues(Quantity, Total);
}


function ValidateNumeric(id, index) {
    toastr.clear();
    var quantity = parseInt($("#inp" + id).attr("max"));
    var newquantity = parseInt($("#inp" + id).val());
    if (newquantity <= quantity && newquantity > 0) {
        UpdateTableCost(id, index);
    }
    else if (newquantity == 0) {
        $("#inp" + id).val(quantity);
        UpdateTableCost(id, index);
        toastr.warning("La cantidad a aprobar debe ser mayor a cero.");
    }
    else {
        $("#inp" + id).val(quantity);
        UpdateTableCost(id, index);
        toastr.warning("Número invalido");
    }
}

function SerchRMA() {
    if ($('#ValidDate1').val() != "" & $('#ValidDate2').val() != "") {
        if (moment($('#ValidDate1').val()) <= moment($('#ValidDate2').val())) {
            $.ajax({
                type: "GET",
                url: "/DamagedsGoods/ActionGetRMA",
                data: { "Type": "RMA", "Date1": $('#ValidDate1').val(), "Date2": $('#ValidDate2').val(), "Status": 0 },
                success: function (result) {
                    if (result.success) {
                        table.clear();
                        table.rows.add($(result.Json));
                        table.columns.adjust().draw();
                        $('#TableDiv').animatePanel();
                    }
                    else
                        toastr.warning('Alerta - Error inesperado.');
                },
                error: function (result) {
                    toastr.error('Alerta - Error inesperado contactar a sistemas.');
                }
            });
        }
        else {
            toastr.error("Fecha final no puede ser menor a la inicial.");
            document.getElementById('ValidDate1').value = '';
            document.getElementById('ValidDate2').value = '';
            $('#ValidDate1').focus();
        }
    }
}
function CancelMessage(viewModel, s, g) {
    swal({
        title: "¿Esta seguro que desea Rechazar?",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "Continuar",
        cancelButtonText: "Cancelar"
    }, function (isConfirm) {
        if (isConfirm)
            Aproval(viewModel, 3, s, g);
    });
}

function AprovalMessage(viewModel, s, g) {
    swal({
        title: "¿Esta seguro que desea Aprobar?",
        type: "info",
        showCancelButton: true,
        confirmButtonColor: "#55dd6b",
        cancelButtonText: "Cancelar",
        confirmButtonText: "Continuar"
    }, function (isConfirm) {
        if (isConfirm)
            Aproval(viewModel, 2, s, g);
    });
}

function Update() {
    if ($('#ValidDate1').val() != "" & $('#ValidDate2').val() != "") {
        if (moment($('#ValidDate1').val()) <= moment($('#ValidDate2').val())) {
            $.ajax({
                type: "GET",
                url: "/DamagedsGoods/ActionGetRMA/",
                data: { "Type": "RMA", "Date1": $('#ValidDate1').val(), "Date2": $('#ValidDate2').val(), "Status": 0 },
                success: function (result) {
                    if (result.success) {
                        $('#TableDiv').animatePanel();
                        table.clear();
                        table.rows.add($(result.Json));
                        table.columns.adjust().draw();
                    }
                    else
                        toastr.warning('Alerta - Error inesperado!!');
                },
                error: function (result) {
                    toastr.error('Alerta - Error inesperado contactar a sistemas.');
                }
            });
        } else {
            toastr.error("Fecha final no puede ser menor a la inicial.");
            document.getElementById('ValidDate1').value = '';
            document.getElementById('ValidDate2').value = '';
            $('#ValidDate1').focus();
        }
    }
    else {
        $.ajax({
            type: "GET",
            url: "/DamagedsGoods/ActionGetPendingRemissionByCost/",
            success: function (result) {
                $('#TableDiv').animatePanel();
                table.clear();
                table.rows.add($(result.Json));
                table.columns.adjust().draw();
                document.getElementById('ValidDate1').value = '';
                document.getElementById('ValidDate2').value = '';
            },
            error: function (result) {
                toastr.error('Alerta - Error inesperado contactar a sistemas.');
            }
        });
    }
}

function Aproval(viewModel, process_status, s, g) {
    var comment = $('#comment').val();
    if (comment.trim().length >= 8) {
        StartLoading();
        if (s) {
            axios.post("/DamagedsGoods/ActionUpdateRmaApprobal", { Model: viewModel, Folio: s, Status: process_status, Comment: comment, program: "RMA011.cshtml", SupplierId: g }).then(function (data) {
                EndLoading();
                console.log();
                if (data.data.Data == "SF")
                    SessionFalse("Sesion terminada");
                else if (data.data.Data == "N/A")
                    toastr.warning('Folio no existente');
                else if (!data.data.Data)
                    toastr.warning('Ocurrio un error al procesar la devolucion');
                else if (data.data.Data) {
                    Update();
                    if (process_status == 2) {
                        if (data.data.Status == 1) {
                            swal({
                                title: 'Aprobado',
                                text: "Folio aprobado con exito. Favor de dar salida a la mercancia.",
                                type: "success"
                            });
                        } else {
                            swal({
                                title: 'Aprobado',
                                text: "Folio aprobado con exito. Solicitud se mando a compras.",
                                type: "success"
                            });
                        }
                    } else {
                        swal({
                            title: 'Rechazado',
                            text: "Folio rechazado con exito.",
                            type: "warning"
                        });
                    }
                    table.clear();
                    table.rows.add($(data.data.Model));
                    table.columns.adjust().draw();
                    $('#TableDiv').animatePanel();
                }
                else if (!data.data.Data) {
                    EndLoading();
                    toastr.warning('Folio cancelado previamente o inexistente');
                }
                else {
                    EndLoading();
                    toastr.error('Alerta - Error inesperado  contactar a sistemas.');
                }
            }).catch(function (error) {
                EndLoading();
                toastr.error('Alerta - Error inesperado  contactar a sistemas.');
            })
        }
        else {
            EndLoading();
            toastr.warning('El folio no puede ser nulo');
        }
    } else {
        EndLoading();
        setTimeout(function () {
            swal('Debe de ingresar al menos un comenario de 8 caracteres.',
                'Ingrese un comentario  y vuelvalo a intentar.',
                'error');
        }, 400);
    }
}

$('#TablePurchases tbody').on('click', '.btn-success', function () {
    var damagedgoodsdoc = $(this).attr("DamagedGoodsDoc");
    var supplierid = $(this).attr("SupplierId");
    var check = false;
    list.forEach(function (element) {
        var real_quantity = $("#inp" + element.PartNumber).val();
        if (real_quantity == "" || real_quantity == "0") {
            toastr.warning("Ingrese las cantidades a aprobar.");
            return null;
        }
        if (typeof real_quantity !== 'undefined') {
            element.RealQuantity = real_quantity;
        }
        if (parseInt(real_quantity) > 0) {
            check = true;
        }
    });
    if (check)
        AprovalMessage(list, damagedgoodsdoc, supplierid);
    else
        toastr.warning("Seleccione al menos un producto.");
});

$('#TablePurchases tbody').on('click', '.btn-danger', function () {
    var damagedgoodsdoc = $(this).attr("DamagedGoodsDoc");
    var supplierid = $(this).attr("SupplierId");

    CancelMessage(listDelete, damagedgoodsdoc, supplierid);
});


var model = JSON.parse($("#model").val());

table.clear();
table.rows.add($(model));
table.columns.adjust().draw();