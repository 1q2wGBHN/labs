﻿const sesion = "Se termino su sesion.";
table = []
RmaTable = []
n = 0
formatmony = x => new Intl.NumberFormat("en-US", { style: "currency", currency: "USD", currencyDisplay: "symbol", maximumFractionDigits: 4 }).format(x)
tableTd = x => `<td>${x}</td>`
tableTdAmount = x => y => z => `<td id="${z}_${y}">${formatmony(x)}</td>`
tableTdInput = x => y => z => p => `<td><input type="number" min="1" id='inp_${x}' value="0" onkeyup="NumberValid(${x},${y},${p},'${z}')" class='form-control inputnum'> </td>`
tableTdCheck = x => `<td><div class="checkbox"><label > <input type="checkbox"  id="ckeck_${x}" onchange="IsCheck(${x})" class="i-checks s"></label></div></td>`;
tabletr = x => `<tr>${n++}${tableTdCheck(n)}${tableTd(x.PartNumber)}${tableTd(x.Description)}${tableTd(x.Size == null ? "N/A" : x.Size)}${tableTd(x.Block)}${tableTdAmount(x.Price)(n)("price")}${tableTd(x.Storage)}${tableTdInput(n)(x.Block)((x.Size == null ? "N/A" : x.Size))(x.Price)}${tableTdAmount(0)(n)("amount")}</tr>`;
listFormatTable = x => y => z => ({ Check: y, PartNumber: x.PartNumber, Description: x.Description, Size: (x.Size == null ? "N/A" : x.Size), Block: x.Block, Storage: x.Storage, Quantity: z ? x.Quantity : 0, Price: x.Price });

$(document).ready(function () {
    window.setTimeout(function () { $("#fa").click(); }, 1000);

    $("#supplier").select2({
        minimumInputLength: 2,
        formatInputTooShort: function () {
            return "Ingresa 2 Letras";
        },
        matcher: function (term, text, option) {
            return text.toUpperCase().indexOf(term.toUpperCase()) >= 0 || option.val().toUpperCase().indexOf(term.toUpperCase()) >= 0;
        }
    });

    $('#saveButton').click(function () {
        if (table.filter(x => x.Check == true && x.Quantity <= 0).length > 0) {
            toastr.warning("Tienes seleccionado un producto sin valor favor de colocarlo o quitarlo")
        }
        else if (table.filter(x => x.Check == false && x.Quantity > 0).length > 0 && table.filter(x => x.Check == true && x.Quantity > 0).length == 0) {
            toastr.warning("Tienes un producto con una cantidad pero sin seleccionar seleccionelo o borrelo")
        }
        else if (table.filter(x => x.Check == false && x.Quantity <= 0).length == table.length) {
            toastr.warning("Seleccione un producto")
        }
        else {
            swal({
                title: "Desea guardar la solicitud?",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Si",
                cancelButtonText: "No",
                closeOnConfirm: true,
                closeOnCancel: true
            }, function (isConfirm) {
                if (isConfirm)
                    Save();
            });
        }
    });

    IsOcult(false);
});

function Items() {
    $("#allCheck").iCheck('check');
    $("#tableEMSC").html("")
    IsOcult(false);
    if ($("#supplier").val() != 0) {
        StartLoading()
        axios.get("/ItemSuppliers/ActionGetItemsSupplierStock/?Supplier=" + $("#supplier").val())
            .then(function (data) {
                if (data.data.length > 0 && data.data != "SF") {
                    n = 0;
                    RmaTable = [...data.data]
                    $("#tableEMSC").html(RmaTable.map(x => tabletr(x)))
                    table = RmaTable.map(x => listFormatTable(x)(true)(false))
                    $('input[type=checkbox]').iCheck(
                        {
                            checkboxClass: 'icheckbox_square-green',
                        }).on('ifClicked', function () {
                            $(this).trigger("change");
                        });
                    $(".checkbox").iCheck('check');
                    EndLoading();
                    IsOcult(true);
                }
                else if (data.data == "SF") {
                    SessionFalse(sesion)
                }
                else {
                    toastr.warning("No se encontraron productos bloqueados")
                    ClearSolucion();
                }

            }).catch(function (error) {
            }).then(function (data) {
                EndLoading();
            })
    }
}

function IsOcult(value) {
    if (value) {
        $("#tableEMSCContent").show()
        $("#checkOcult").show()
        $("#filterDiv").show()
    }
    else {
        $("#tableEMSCContent").hide()
        $("#checkOcult").hide()
        $("#filterDiv").hide()
    }
}

function IsCheck(id) {
    table[id - 1].Check = !$("#ckeck_" + id).is(':checked')
    var quantity = table.filter(x => x.Check == false).length;
    if (quantity > 0 && quantity <= table.length) {
        $("#allCheck").iCheck('uncheck');
    }
    else {
        if (quantity == table.length) {
            $("#allCheck").iCheck('uncheck');
        }
        else {
            $("#allCheck").iCheck('check');
        }
    }
}

function IsCheckInp(id) {
    table[id - 1].Check = $("#ckeck_" + id).is(':checked')
    var quantity = table.filter(x => x.Check == false).length;
    if (quantity > 0 && quantity <= table.length) {
        $("#allCheck").iCheck('uncheck');
    }
    else {
        if (quantity == table.length) {
            $("#allCheck").iCheck('uncheck');
        }
        else {
            $("#allCheck").iCheck('check');
        }
    }
}

function AllCheck() {
    if (!$("#allCheck").is(':checked')) {
        $("#allCheck").iCheck('check');
        $(".checkbox").iCheck('check');
        table = table.map(x => listFormatTable(x)(true)(true))
    }
    else {
        $("#allCheck").iCheck('uncheck');
        $(".checkbox").iCheck('uncheck');
        table = table.map(x => listFormatTable(x)(false)(false))
        $(".inputnum").val("0")
    }
}

function CheckOption(value, id) {
    if (value) {
        $("#ckeck_" + id).iCheck('check');
        IsCheckInp(id)
    }
    else {
        $("#ckeck_" + id).iCheck('uncheck');
        IsCheckInp(id)
    }
}

function NumberValid(id, quantity, price, size) {
    isDecimal = /^\d+\.?\d{0,3}$/
    if (size == "KGS") {
        var number = parseFloat($("#inp_" + id).val())
        if (isNaN(number)) {
            toastr.warning("Ingrese un valor valido")
            $("#inp_" + id).val(0);
            table[id - 1].Quantity = 0;
            $("#amount_" + id).html(formatmony(0))
            CheckOption(false, id)
        }
        if (isDecimal.test($("#inp_" + id).val())) {
            if (number <= quantity && number > 0) {
                $("#inp_" + id).val(number);
                table[id - 1].Quantity = number
                $("#amount_" + id).html(formatmony(parseFloat((price * number).toFixed(4))))
                CheckOption(true, id)
            }
            else {
                toastr.warning("No puede ser mayor a la cantidad")
                $("#inp_" + id).val(0);
                table[id - 1].Quantity = 0
                $("#amount_" + id).html(formatmony(0))
                CheckOption(false, id)
            }
        }
        else {
            toastr.warning("No puede tener mas de 3 decimales")
            $("#inp_" + id).val(0);
            table[id - 1].Quantity = 0
            $("#amount_" + id).html(formatmony(0))
            CheckOption(false, id)
        }
    }
    else {
        var number = parseInt($("#inp_" + id).val())
        if (isNaN(number)) {
            toastr.warning("Ingrese un valor valido")
            $("#inp_" + id).val(0);
            table[id - 1].Quantity = 0
            $("#amount_" + id).html(formatmony(0))
            CheckOption(false, id)
        }
        else if (number <= quantity && number > 0) {
            $("#inp_" + id).val(number)
            table[id - 1].Quantity = number
            $("#amount_" + id).html(formatmony(parseFloat((price * number).toFixed(4))))
            CheckOption(true, id)
        }
        else {
            toastr.warning("No puede ser mayor a la cantidad")
            $("#inp_" + id).val(0);
            table[id - 1].Quantity = 0
            $("#amount_" + id).html(formatmony(0))
            CheckOption(false, id)
        }
    }
}

function Search() {
    var value = document.querySelector("#filter").value.toLowerCase()
    $("#tableEMSC tr").filter(function () {
        $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
    });
}

function Save() {
    StartLoading()
    axios.post("/DamagedsGoods/ActionCreateDamagedGoods/", { Supplier: $("#supplier").val(), DamagedsGood: table.filter(x => x.Check == true && x.Quantity > 0) })
        .then(function (data) {
            if (data.data == "SF") {
                SessionFalse(sesion)
            }
            else if (data.data.value != null) {
                EndLoading();
                if (data.data.response == 1) {
                    swal({
                        title: 'Folio: ' + data.data.doc,
                        text: "Se mando la solicitud al area de compras.",
                        type: "success"
                    });
                } else if (data.data.response == 2) {
                    $("#modalReferenceBody").html(`<iframe width='100%' height='550px' src='data:application/pdf;base64,${data.data.pdf}'></iframe>`);
                    $("#modalReference").modal('show');
                    swal({
                        title: 'Folio: ' + data.data.doc,
                        text: "Se mando la notificacion al proveedor.",
                        type: "success"
                    });
                } else if (data.data.response == 0) {
                    toastr.warning("No pudo guardarce los datos: " + data.data.doc);
                }
                ClearSolucion()
            }
            else {
                toastr.warning("No pudo guardarce los datos")
            }
        })
        .catch(function (error) {
            console.log(error.data)
        })
        .then(function (data) {
            EndLoading()
        })
}

function ClearSolucion() {
    IsOcult(false);
    $('#supplier').val($("#supplier :first").val()).trigger('change');
}