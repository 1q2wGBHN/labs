﻿window.setTimeout(function () { $("#fa").click(); }, 1000);
const fomartmoney = money => decimal => new Intl.NumberFormat("en-US", { style: "currency", currency: "USD", currencyDisplay: "symbol", maximumFractionDigits: decimal }).format(money);


$("#supplier").select2({
    minimumInputLength: 3,
    allowClear: true,
    placeholder: "Seleccione una proveedor",
    initSelection: function (element, callback) {
        callback({ id: "0", text: "Seleccione una opcion" });
    },
    ajax: {
        url: "/Suppliers/ActionSearchSupplier/",
        dataType: 'json',
        type: "GET",
        quietMillis: 50,
        data: function (Filter) {
            return {
                Filter: Filter
            };
        },
        results: function (data) {
            return {
                results: $.map(data.Json, function (item) {
                    return {
                        text: `${item.BusinessName}`,
                        id: item.SupplierId
                    }
                }),
            };
        }
    }
});

$("#department").select2();
$("#family").select2();
$("#Items").append('<tfoot><th>Total</th><th></th><th></th><th></th><th colspan = "3"></th></tfoot>');
var table = $('#Items').DataTable({
    responsive: true,
    autoWidth: true,
    oLanguage: {
        "sProcessing": "Procesando...", "sLengthMenu": "Mostrar _MENU_ registros", "sZeroRecords": "No se encontraron resultados", "sEmptyTable": "Ningún dato disponible en esta tabla", "sInfo": "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros", "sInfoEmpty": "Mostrando registros del 0 al 0 de un total de 0 registros", "sInfoFiltered": "(filtrado de un total de _MAX_ registros)", "sInfoPostFix": "", "sSearch": "Buscar:", "sUrl": "", "sInfoThousands": ",", "sLoadingRecords": "Cargando...", "oPaginate": { "sFirst": "Primero", "sLast": "Último", "sNext": "Siguiente", "sPrevious": "Anterior" }, "oAria": { "sSortAscending": ": Activar para ordenar la columna de manera ascendente", "sSortDescending": ": Activar para ordenar la columna de manera descendente" }
    },
    dom: "<'row'<'col-sm-4'l><'col-sm-4 text-left'B><'col-sm-4'f>t<'col-sm-6'i><'col-sm-6'p>>",
    lengthMenu: [[10, 25, 50, -1], [10, 25, 50, "Todos"]],
    buttons: [
        { extend: 'csv', text: 'Excel', title: 'Productos Sin Venta', className: 'btn-sm', footer: true },
        {
            extend: 'pdf', text: 'PDF', title: 'Productos Sin Venta', className: 'btn-sm', exportOptions: {
                columns: [0, 1, 2, 3, 4, 5, 6]
            },
            customize: function (doc) {
                doc.content[1].table.widths =
                    Array(doc.content[1].table.body[0].length + 1).join('*').split('');
            }, footer: true
        },
        { extend: 'print', text: 'Imprimir', className: 'btn-sm', footer: true },
    ],
    "footerCallback": function (row, data, start, end, display) {
        var api = this.api(), data;
        // converting to interger to find total
        var intVal = function (i) {
            return typeof i === 'string' ?
                i.replace(/[\$,]/g, '') * 1 :
                typeof i === 'number' ?
                    i : 0;
        };
        var monTotal = api
            .column(3)
            .data()
            .reduce(function (a, b) {
                return intVal(a) + intVal(b);
            }, 0);
        $(api.column(1).footer()).html(`<span ${monTotal == 0 ? "" : `style = "Color:${monTotal >= 0 ? "green" : "red"}"`} > ${fomartmoney(monTotal)(4)}</span >`);
    },
    columns: [
        { targets: 0, data: 'PartNumber', width: "1%" },
        { targets: 1, data: 'PartDescription', width: "1%" },
        { targets: 2, data: 'Unrestricted', width: "1%", className: "text-center" },
        { targets: 3, data: 'MapPrice', width: "1%", className: "text-center" },
        { targets: 4, data: 'StorageLocation', width: "1%", className: "text-center" },
        { targets: 5, data: 'LastPurchase', width: "1%", className: "text-center" },
        { targets: 6, data: 'Days', width: "1%", className: "text-center" }
    ],
    columnDefs: [{
        targets: [0, 1, 2, 3, 4, 5, 6],
        width: "1%"
    },
    {
        targets: [3],
        render: function (data, type, row) {
            return '$' + data;
        }
    },
    {
        targets: [5],
        render: function (data, type, row) {
            return moment(data).format('DD/MM/YYYY');
        }
    }]
});

var detailRows = [];
table.on('draw', function () {
    $.each(detailRows, function (i, id) {
        $('#' + id + ' td.details-control').trigger('click');
    });
});

$(document).on("keypress", '.Numeric', function (event) {
    var keyValue = event.key;
    if (keyValue.match(/[0-9]/g))
        return true;
    else
        return false;
});

$('#Search').on('click', function () {
    var begindate = $('#beginDate').val();
    var enddate = $('#endDate').val();
    var supplierid = parseInt($("#supplier").val());
    var departmentid = parseInt($("#department").val());
    var familyid = parseInt($("#family").val());
    var days = parseInt($(".Numeric").val());
    if (isNaN(days))
        days = 0;
    if (isNaN(supplierid))
        supplierid = 0;


    if (begindate != "" & enddate != "") {
        if (Date.parse(enddate) < Date.parse(begindate))
            toastr.error("La fecha inicial no puede ser mayor a la fecha final");
        else {
            StartLoading();
            $.ajax({
                url: "/Items/ActionGetProductsWithoutSales/",
                type: "GET",
                data: { "beginDate": begindate, "endDate": enddate, "SupplierId": supplierid, "DepartmentId": departmentid, "FamilyId": familyid, "Days": days },
                success: function (result) {
                    EndLoading();
                    if (!result.Status)
                        SessionFalse("Se terminó su sesion.");
                    else {
                        table.clear();
                        table.rows.add($(result.Model));
                        table.columns.adjust().draw();
                        $('#TableDiv').animatePanel();

                        if (!$.isEmptyObject(result.Model))
                            $("#Print").removeAttr('disabled', 'disabled');
                        else
                            $("#Print").attr('disabled', true);
                    }
                },
                error: function () {
                    EndLoading();
                    $("#Print").attr('disabled', true);
                    toastr.error('Error inesperado contactar a sistemas.');
                }
            });
        }
    }
    else {
        if (begindate == "")
            $('#beginDate').focus();
        else if (enddate == "")
            $('#endDate').focus();
        toastr.error('Capture el rango de fechas.');
    }
});

$('#Print').on('click', function () {
    var i = 0;
    listProducts = [];
    $.each(table.rows().data(), function (index, value) {
        var LastPurchase = moment(value.LastPurchase).format('MM/DD/YYYY');
        listProducts[i] = { PartNumber: value.PartNumber, PartDescription: value.PartDescription, Unrestricted: value.Unrestricted, MapPrice: value.MapPrice, StorageLocation: value.StorageLocation, LastPurchase: LastPurchase, Days: value.Days };
        i++;
    });
    StartLoading();
    $.ajax({
        url: '/Items/ActionGenerateReportInventoryWithoutSales/',
        type: "POST",
        data: { items: listProducts },
        success: function (response) {
            EndLoading();
            if (response.responseText == "Terminó tu sesión.")
                SessionFalse(response.responseText);
            if (response.success) {
                $("#modalReferenceBody").html("<iframe width='100%' height='550px' src='data:application/pdf;base64, " + encodeURI(response.responseText) + "'></iframe>");
                $("#modalReference").modal('show');
            }
            else
                toastr.error(response.responseText);
        },
        error: function () {
            EndLoading();
            toastr.error('Error inesperado contactar a sistemas.');
        }
    });
});

function FillDropdown(selector, vData) {
    if (vData != null) {
        var vItems = [];
        vItems.push('<option value="0">Seleccione una familia</option>');
        for (var i in vData) {
            if (vData[i].Selected)
                vItems.push('<option selected value="' + vData[i].class_id + '">' + vData[i].description + '</option>');
            else
                vItems.push('<option value="' + vData[i].class_id + '">' + vData[i].description + '</option>');
        }
        $('#' + selector).empty();
        $('#' + selector).append(vItems.join(''));
        $('#family').val($("#family :first").val()).trigger('change');
    }
    else {
        var vItems = [];
        vItems.push('<option selected value="0">Seleccione una familia</option>');
        $('#' + selector).empty();
        $('#' + selector).append(vItems.join(''));
        $('#family').val($("#family :first").val()).trigger('change');
    }
}

function GetFamily() {
    var depto = $("#department").val();
    if (depto == "" || depto == 0) {
        $('#family').val($("#family :first").val()).trigger('change');
        FillDropdown("family", null)
        return false;
    }
    $.ajax({
        url: '/MaClass/ActionGetFamily/',
        type: "GET",
        data: { "deptoo": depto },
        success: function (response) {
            if (response.success) {
                listFamily = response.Json;
                FillDropdown("family", listFamily);
            }
            else
                FillDropdown("family", null);
        },
        error: function () {
            FillDropdown("family", null)
            toastr.error('Error inesperado contactar a sistemas.');
        }
    });
}