﻿$(document).ready(function () {
    window.setTimeout(function () { $("#fa").click(); }, 1000);

    $('#dateInit').datepicker({
        autoclose: true,
        endDate: '01/01/2100',
        language: 'es',
        format: 'mm/dd/yyyy',
        todayHighlight: true,
    }).on("changeDate", function (e) {
        SearchDocuments();
    });

    $('#dateFin').datepicker({
        autoclose: true,
        endDate: '01/01/2100',
        language: 'es',
        format: 'mm/dd/yyyy',
        todayHighlight: true,
    }).on("changeDate", function (e) {
        SearchDocuments();
    });

    var headers = JSON.parse($("#model").val());
    table.clear();
    table.rows.add($(headers));
    table.columns.adjust().draw();
});

var table = $('#tableEMSCHead').DataTable({
    oLanguage:
    {
        "sProcessing": "Procesando...",
        "sLengthMenu": "Mostrar _MENU_ registros",
        "sZeroRecords": "No se encontraron resultados",
        "sEmptyTable": "Ningún dato disponible en esta tabla",
        "sInfo": "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
        "sInfoEmpty": "Mostrando registros del 0 al 0 de un total de 0 registros",
        "sInfoFiltered": "(filtrado de un total de _MAX_ registros)",
        "sInfoPostFix": "",
        "sSearch": "Buscar:",
        "sUrl": "",
        "sInfoThousands": ",",
        "sLoadingRecords": "Cargando...",
        "oPaginate": { "sFirst": "Primero", "sLast": "Último", "sNext": "Siguiente", "sPrevious": "Anterior" },
        "oAria": { "sSortAscending": ": Activar para ordenar la columna de manera ascendente", "sSortDescending": ": Activar para ordenar la columna de manera descendente" }
    },
    dom: "<'row'<'col-sm-4'l><'col-sm-4 text-left'B><'col-sm-4'f>t<'col-sm-6'i><'col-sm-6'p>>",
    lengthMenu: [[10, 25, 50, -1], [10, 25, 50, "Todos"]],
    buttons:
        [
            { extend: 'csv', text: 'Excel', title: 'MermaControlada', className: 'btn-sm' },
            {
                extend: 'pdf', text: 'PDF', title: 'MermaControlada', className: 'btn-sm',
                exportOptions: { columns: [1, 2, 3] },
                customize: function (doc) {
                    doc.content[1].table.widths = Array(doc.content[1].table.body[0].length + 1).join('*').split('');
                }
            },
            { extend: 'print', text: 'Imprimir', className: 'btn-sm' },
        ],
    columns:
        [
            {
                "class": "details-control",
                "orderable": false,
                "data": null,
                "defaultContent": ""
            },
            { data: 'Document' },
            { data: 'User' },
            { data: 'SiteName' },
            { data: 'Fecha' },
        ],
    columnDefs: [
        {
            targets: [0],
            width: "1%"
        },
        {
            targets: [1, 2, 3, 4],
            width: "22%"
        }]
});

var detailRows = [];
items = [];
site = "";
fecha = "";

$('#tableEMSCHead tbody').on('click', 'tr td.details-control', function () {
    var tr = $(this).closest('tr');
    var row = table.row(tr);
    if (row.child.isShown()) {
        row.child.hide();
        tr.removeClass('details');
    }
    else {
        if (table.row('.details').length) {
            $('.details-control', table.row('.details').node()).click();
        }
        row.child(format(row.data())).show();
        tr.addClass('details');
    }
});

table.on('draw', function () {
    $.each(detailRows, function (i, id) {
        $('#' + id + ' td.details-control').trigger('click');
    });
});

function format(d) {
    var detailItems = "";
    var tabledetail = $('<div/>').addClass('loading').text('Cargando Datos...');
    console.log(d);
    $.ajax({
        url: "/DamagedsGoods/ActionGetItemsDocument",
        data: { "document": d.Document },
        type: "GET",
        success: function (returndate) {
            items = returndate.Json;
            site = d.SiteName;
            fecha = d.Fecha;
            $.each(returndate.Json, function (index, value) {
                total = value.IVA + value.IEPS + value.Amount;
                detailItems += "<tr><td>" + value.PartNumber + "</td><td>" + value.Description + "</td><td>" + (value.ScrapReason == null ? 'Sin razón' : value.ScrapReason) + "</td><td class='text-right'>$" + value.Price + "</td><td class='text-right'>" + value.Quantity + "</td><td class='text-right'>" + "$" + value.Amount + "</td><td class='text-right'>" + "$ " + value.IEPS + "</td><td class='text-right'>" + "$ " + value.IVA + "</td><td class='text-right'>" + "$ " + total + "</td></tr>"
            });
            btn = '<div class="form-group col-md-12"> <div class="row"><div class="row"> <div class="col-md-2 col-md-offset-10 text-right"><button id="btn-save" type="button" class="btn btn-sm btn-success" onclick="AprovalMessage(' + d.Document + ');"><i class="fa fa-save"></i> Aplicar</button></div></div></div>'
            tabledetail.html('<table id="tabledetail" class="table table-striped table-bordered table-hover" style="width:100%">' +
                '<thead><tr><th>Codigo</th><th>Descripcion</th><th>Razon</th><th>Precio</th><th>Cantidad</th><th>Importe</th><th>IEPS</th><th>IVA</th><th>Total</th></tr></thead><tbody>' + detailItems + '</tbody></table>' + btn).removeClass('loading');
            reloadStyleTable();
        }
    });
    return tabledetail;
}

function reloadStyleTable() {
    $('#tabledetail').DataTable({
        oLanguage:
        {
            "sProcessing": "Procesando...",
            "sLengthMenu": "Mostrar _MENU_ registros",
            "sZeroRecords": "No se encontraron resultados",
            "sEmptyTable": "Ningún dato disponible en esta tabla",
            "sInfo": "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
            "sInfoEmpty": "Mostrando registros del 0 al 0 de un total de 0 registros",
            "sInfoFiltered": "(filtrado de un total de _MAX_ registros)", "sInfoPostFix": "",
            "sSearch": "Buscar:", "sUrl": "", "sInfoThousands": ",", "sLoadingRecords": "Cargando...",
            "oPaginate": { "sFirst": "Primero", "sLast": "Último", "sNext": "Siguiente", "sPrevious": "Anterior" },
            "oAria":
            {
                "sSortAscending": ": Activar para ordenar la columna de manera ascendente",
                "sSortDescending": ": Activar para ordenar la columna de manera descendente"
            }
        },
    });
}

function SearchDocuments() {
    if ($('#dateInit').val() != "" && $('#dateFin').val() != "") {
        if (moment($('#dateFin').val()) < moment($('#dateInit').val())) {
            toastr.error("Fecha final no puede ser menor a la inicial.");
            $('#dateFin').focus();
        }
        else {
            StartLoading();
            $.ajax({
                url: "/DamagedsGoods/ActionGetDocumentsInStatus2",
                type: "GET",
                data: { "dateInicial": $('#dateInit').val(), "dateFinal": $('#dateFin').val() },
                success: function (response) {
                    if (response.success) {
                        table.clear().draw();
                        table.rows.add(response.Json)
                        table.columns.adjust().draw();
                        EndLoading();
                    }
                    else {
                        EndLoading();
                        if (response.responseText === 'Termino tu sesión.') {
                            SessionFalse(response.responseText);
                        }
                        else {
                            toastr.warning(response.responseText);
                            table.clear().draw();
                        }
                    }
                },
                error: function () {
                    EndLoading();
                    toastr.error('Error inesperado contactar a sistemas.');
                }
            });
        }
    }
    else {
        table.clear().draw();
        return false;
    }
}

function AprovalMessage(document) {
    swal({
        title: "¿Esta seguro que desea Aprobar?",
        type: "info",
        showCancelButton: true,
        confirmButtonColor: "#55dd6b",
        cancelButtonText: "Cancelar",
        confirmButtonText: "Continuar"
    }, function (isConfirm) {
        if (isConfirm)
            Aprobar(document);
    });
}

function ReloadTable() {
    StartLoading();
    $.ajax({
        url: "/DamagedsGoods/ActionGetDocumentsInStatus2",
        type: "GET",
        data: { "dateInicial": $('#dateInit').val(), "dateFinal": $('#dateFin').val() },
        success: function (response) {
            if (response.success) {
                table.clear().draw();
                table.rows.add(response.Json)
                table.columns.adjust().draw();
                EndLoading();
            }
            else {
                EndLoading();
                if (response.responseText === 'Termino tu sesión.') {
                    SessionFalse(response.responseText);
                }
                else {
                    toastr.warning(response.responseText);
                    table.clear().draw();
                }
            }
        },
        error: function () {
            EndLoading();
            toastr.error(' Error inesperado contactar a sistemas.');
        }
    });
}

function Aprobar(document) {
    if (document != "") {
        StartLoading();
        $.ajax({
            url: "/DamagedsGoods/ActionAplicarDamageGoods",
            type: "POST",
            data: { "documentItems": items, "document": document, "siteName": site, "fecha": fecha },
            success: function (response) {
                if (response.success == 1) {
                    swal({
                        title: 'Aplicado',
                        text: "Folio Aplicado con exito.",
                        type: "success"
                    });
                    $('#dateInit').val("");
                    $('#dateFin').val("");
                    ReloadTable();
                }
                else {
                    EndLoading();
                    if (response.responseText === 'Termino tu sesión.') {
                        SessionFalse(response.responseText);
                    }
                    else {
                        toastr.error(ErrorListTr(response.responseText));
                    }
                }
            },
            error: function () {
                toastr.error(' Error inesperado contactar a sistemas.');
            }
        });
    }
}

function ErrorListTr(error) {
    if (error == 8001)
        return "Error: Inventario insuficiente"
    else if (error == 6001)
        return "Error: Site Code Incorrecto"
    else if (error == 6002)
        return "Error: No tiene estatus Aprovado"
    else if (error == 6003)
        return "Error: Locacion invalida"
    else
        return "Movimiento exitoso"
}