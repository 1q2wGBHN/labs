﻿window.setTimeout(function() { $("#fa").click(); }, 1000);

//$("#supplier").select2(
//    {
//        minimumInputLength: 2,
//        formatInputTooShort: function() {
//            return "Busque un proveedor";
//        },
//        matcher: function(term, text, option) {
//            return text.toUpperCase().indexOf(term.toUpperCase()) >= 0 || option.val().toUpperCase().indexOf(term.toUpperCase()) >= 0;
//        }
//    }
//);
$("#department").select2();
$("#family").select2();
$("#supplier").select2({
    minimumInputLength: 3,
    allowClear: true,
    placeholder: "Seleccione una proveedor",
    initSelection: function (element, callback) {
        callback({ id: "0", text: "Seleccione una opcion" });
    },
    ajax: {
        url: "/Suppliers/ActionSearchSupplier/",
        dataType: 'json',
        type: "GET",
        quietMillis: 50,
        data: function (Filter) {
            return {
                Filter: Filter
            };
        },
        results: function (data) {
            return {
                results: $.map(data.Json, function (item) {
                    return {
                        text: `${item.BusinessName}`,
                        id: item.SupplierId
                    }
                }),
            };
        }
    }
});
var table = $('#Items').DataTable({
    responsive: true,
    autoWidth: true,
    oLanguage: {
        "sProcessing": "Procesando...", "sLengthMenu": "Mostrar _MENU_ registros", "sZeroRecords": "No se encontraron resultados", "sEmptyTable": "Ningún dato disponible en esta tabla", "sInfo": "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros", "sInfoEmpty": "Mostrando registros del 0 al 0 de un total de 0 registros", "sInfoFiltered": "(filtrado de un total de _MAX_ registros)", "sInfoPostFix": "", "sSearch": "Buscar:", "sUrl": "", "sInfoThousands": ",", "sLoadingRecords": "Cargando...", "oPaginate": { "sFirst": "Primero", "sLast": "Último", "sNext": "Siguiente", "sPrevious": "Anterior" }, "oAria": { "sSortAscending": ": Activar para ordenar la columna de manera ascendente", "sSortDescending": ": Activar para ordenar la columna de manera descendente" }
    },
    dom: "<'row'<'col-sm-4'l><'col-sm-4 text-left'B><'col-sm-4'f>t<'col-sm-6'i><'col-sm-6'p>>",
    lengthMenu: [[10, 25, 50, -1], [10, 25, 50, "Todos"]],
    buttons: [
        { extend: 'csv', text: 'Excel', title: 'Codigos Faltantes', className: 'btn-sm' },
        {
            extend: 'pdf', text: 'PDF', title: 'Codigos Faltantes', className: 'btn-sm', exportOptions: {
                columns: [0, 1, 2, 3, 4, 5, 6]
            },
            customize: function(doc) {
                doc.content[1].table.widths =
                    Array(doc.content[1].table.body[0].length + 1).join('*').split('');
            }
        },
        { extend: 'print', text: 'Imprimir', className: 'btn-sm' },
    ],
    columns: [
        { targets: 0, data: 'PartNumber', width: "1%" },
        { targets: 1, data: 'PartDescription', width: "1%" },
        { targets: 2, data: 'Unrestricted', width: "1%", className: "text-center" },
        { targets: 3, data: 'LastPurchase', width: "1%", className: "text-center" }
    ],
    columnDefs: [{
        targets: [0, 1, 2, 3],
        width: "1%"
    },
    {
        targets: [3],
        render: function(data, type, row) {
            var date = moment(data).format('DD/MM/YYYY');
            if (date != 'Invalid date')
                return date;
            else
                return "";
        }
    }]
});

//var model = JSON.parse($("#model").val());
table.clear();
table.rows.add([])//$(model));
table.columns.adjust().draw();

if (!$.isEmptyObject(model))
    $("#Print").removeAttr('disabled', 'disabled');
else
    $("#Print").attr('disabled', true);

var detailRows = [];
table.on('draw', function() {
    $.each(detailRows, function(i, id) {
        $('#' + id + ' td.details-control').trigger('click');
    });
});

$('#Search').on('click', function() {
    var supplierid = parseInt($("#supplier").val());
    if (isNaN(supplierid))
        supplierid = 0
    var departmentid = parseInt($("#department").val());
    var familyid = parseInt($("#family").val());
    StartLoading();
    $.ajax({
        url: "/Items/ActionGetMissingProducts/",
        type: "GET",
        data: { "SupplierId": supplierid, "DepartmentId": departmentid, "FamilyId": familyid },
        success: function(result) {
            EndLoading();
            if (!result.Status)
                SessionFalse("Se terminó su sesion.");
            else {
                table.clear();
                table.rows.add($(result.Model));
                table.columns.adjust().draw();
                $('#TableDiv').animatePanel();

                if (!$.isEmptyObject(result.Model))
                    $("#Print").removeAttr('disabled', 'disabled');
                else
                    $("#Print").attr('disabled', true);
            }
        },
        error: function() {
            EndLoading();
            toastr.remove();
            $("#Print").attr('disabled', true);
            toastr.error('Error inesperado contactar a sistemas.');
        }
    });
});

$('#Print').on('click', function() {
    var i = 0;
    listProducts = [];
    $.each(table.rows().data(), function(index, value) {
        var LastPurchase = moment(value.LastPurchase).format('MM/DD/YYYY');
        listProducts[i] = { PartNumber: value.PartNumber, PartDescription: value.PartDescription, Unrestricted: value.Unrestricted, StorageLocation: value.StorageLocation, LastPurchase: LastPurchase };
        i++;
    });
    StartLoading();
    $.ajax({
        url: '/Items/ActionGenerateReportMissingCodes/',
        type: "POST",
        data: { items: listProducts },
        success: function(response) {
            EndLoading();
            if (response.responseText == "Terminó tu sesión.")
                SessionFalse(response.responseText);
            if (response.success) {
                $("#modalReferenceBody").html("<iframe width='100%' height='550px' src='data:application/pdf;base64, " + encodeURI(response.responseText) + "'></iframe>");
                $("#modalReference").modal('show');
            }
            else {
                toastr.remove();
                toastr.error(response.responseText);
            }
        },
        error: function() {
            EndLoading();
            toastr.remove();
            toastr.error('Error inesperado contactar a sistemas.');
        }
    });
});

function FillDropdown(selector, vData) {
    if (vData != null) {
        var vItems = [];
        vItems.push('<option  value="0">Seleccione una familia</option>');
        for (var i in vData) {
            if (vData[i].Selected)
                vItems.push('<option selectedselected=selected value="' + vData[i].class_id + '">' + vData[i].description + '</option>');
            else
                vItems.push('<option value="' + vData[i].class_id + '">' + vData[i].description + '</option>');
        }
        $('#' + selector).empty();
        $('#' + selector).append(vItems.join(''));
    }
    else {
        var vItems = [];
        vItems.push('<option  value="0">Seleccione una familia</option>');
        $('#' + selector).empty();
        $('#' + selector).append(vItems.join(''));
    }
}

function GetFamily() {
    var depto = $("#department").val();
    if (depto == "" || depto == 0) {
        FillDropdown("family", null)
        $('#family').val($("#family :first").val()).trigger('change');
        return false;
    }
    $.ajax({
        url: '/MaClass/ActionGetFamily/',
        type: "GET",
        data: { "deptoo": depto },
        success: function(response) {
            if (response.success) {
                listFamily = response.Json;
                FillDropdown("family", listFamily);
            }
            else
                FillDropdown("family", null);
        },
        error: function() {
            FillDropdown("family", null);
            toastr.remove();
            toastr.error('Error inesperado contactar a sistemas.');
        }
    });
    $('#family').val($("#family :first").val()).trigger('change');
}