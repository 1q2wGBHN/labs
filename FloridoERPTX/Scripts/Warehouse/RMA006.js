﻿
$(document).ready(function () {
    window.setTimeout(function () { $("#fa").click(); }, 1000);

    var Items;    
    var model = JSON.parse($("#model").val());
    table.clear();
    table.rows.add($(model));
    table.columns.adjust().draw();
});

$("#ValidDate1").datepicker({
    autoclose: true,
    todayHighlight: true,
    showButtonPanel: true,
}).on("changeDate", function (e) {
    SerchRMA();
});

$("#ValidDate2").datepicker({
    autoclose: true,
    startDate: '01/01/1950',
    todayHighlight: true,
}).on("changeDate", function (e) {
    SerchRMA();
});

var table = $('#TablePurchases').DataTable({
    oLanguage: {
        "sProcessing": "Procesando...", "sLengthMenu": "Mostrar _MENU_ registros", "sZeroRecords": "No se encontraron resultados", "sEmptyTable": "Ningún dato disponible en esta tabla", "sInfo": "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros", "sInfoEmpty": "Mostrando registros del 0 al 0 de un total de 0 registros", "sInfoFiltered": "(filtrado de un total de _MAX_ registros)", "sInfoPostFix": "", "sSearch": "Buscar:", "sUrl": "", "sInfoThousands": ",", "sLoadingRecords": "Cargando...", "oPaginate": { "sFirst": "Primero", "sLast": "Último", "sNext": "Siguiente", "sPrevious": "Anterior" }, "oAria": { "sSortAscending": ": Activar para ordenar la columna de manera ascendente", "sSortDescending": ": Activar para ordenar la columna de manera descendente" }
    },
    responsive: true,
    autoWidth: true,
    order: [[1, "asc"]],
    dom: "<'row'<'col-sm-4'l><'col-sm-4 text-left'B><'col-sm-4'f>t<'col-sm-6'i><'col-sm-6'p>>",
    lengthMenu: [[10, 25, 50, -1], [10, 25, 50, "Todos"]],
    buttons: [
        { extend: 'csv', text: 'Excel', title: 'ConfirmacionRemision', className: 'btn-sm' },
        {
            extend: 'pdf', text: 'PDF', title: 'ConfirmacionRemision', className: 'btn-sm', exportOptions: {
                columns: [1, 2, 3]
            },
            customize: function (doc) {
                doc.content[1].table.widths =
                    Array(doc.content[1].table.body[0].length + 1).join('*').split('');
            }
        },
        { extend: 'print', text: 'Imprimir', className: 'btn-sm' },
    ],
    columns: [
        {
            "class": "details-control",
            "orderable": false,
            "data": null,
            "defaultContent": "",
        },
        { data: 'DamagedGoodsDoc' },
        { data: 'Supplier' },
        { data: 'Comment' },
        { data: 'ProcessStatusDescription' }
    ],
    columnDefs: [{
        targets: [0, 1, 2, 3, 4],
        width: "1%"
    }]
});

var detailRows = [];

$('#TablePurchases tbody').on('click', 'tr td.details-control', function () {
    var tr = $(this).closest('tr');
    var row = table.row(tr);

    if (row.child.isShown()) {
        row.child.hide();
        tr.removeClass('details');
    }
    else {
        if (table.row('.details').length) {
            $('.details-control', table.row('.details').node()).click();
        }
        row.child(format(row.data())).show();
        tr.addClass('details');
    }
});

table.on('draw', function () {
    $.each(detailRows, function (i, id) {
        $('#' + id + ' td.details-control').trigger('click');
    });
});

function format(d) {
    var detailItems = "";
    var tabledetail = $('<div/>').addClass('loading').text('Cargando Datos...');
    $.ajax({
        url: "/DamagedsGoods/ActionGetItemSupplierByRma/",
        data: { "Folio": d.DamagedGoodsDoc, "type": "Remision" },
        type: "GET",
        success: function (data) {
            Items = data;
            $.each(data, function (index, value) {
                detailItems += "<tr><td>" + value.PartNumber + "</td><td>" + value.Description + "</td><td>" + value.Size + "</td><td class='text-right'>" + value.Storage + "</td><td class='text-right'>" + value.Quantity + "</td><td class='text-right'>" + "$" + value.Amount_Doc.toFixed(4) + "</td><td class='text-right'>" + "$" + value.Price.toFixed(4) + "</td>"
            });
            var Apro = "";
            if (d.ProcessStatusDescription == "Aprobada") {
                Apro = '<div class="form-group col-md-12"> <div class="row"> <label class="col-md-12 control-label">Peso total de la mercancia (Kg): </label> </div> <div class="row"> <div class="col-md-10"> <input id="weight" min="0" max="599999" type="number" class="form-control lg" style="width:25%;height:5%;"></input> </div> <div class="col-md-2 text-right"> &nbsp;&nbsp;&nbsp;<button id="btn-save" type="button" class="btn btn-sm btn-success" onclick="AprovalMessage(' + d.DamagedGoodsDoc + ',' + d.SupplierId + ');"><i class="fa fa-save"></i> Aprobar</button></div></div></div>';
            }
            tabledetail.html('<table id="tabledetail" class="table table-striped table-bordered table-hover" style="width:100%"><thead><tr><th>Codigo</th><th>Nombre</th><th>UM</th><th>Locacion</th><th>Cantidad</th><th>Monto</th><th>Precio</th></tr></thead><tbody>' + detailItems + '</tbody></table>' + Apro).removeClass('loading');
            reloadStyleTable();
        }
    });
    return tabledetail;
}

function SerchRMA() {
    if ($('#ValidDate1').val() != "" & $('#ValidDate2').val() != "") {
        if (moment($('#ValidDate1').val()) <= moment($('#ValidDate2').val())) {
            $.ajax({
                type: "GET",
                url: "/DamagedsGoods/ActionGetRMA/",
                data: { "Type": "Remision", "Date1": $('#ValidDate1').val(), "Date2": $('#ValidDate2').val(), "Status": 2 },
                success: function (result) {
                    if (result.success) {
                        $('#TableDiv').animatePanel();
                        table.clear();
                        table.rows.add($(result.Json));
                        table.columns.adjust().draw();
                    }
                    else
                        toastr.warning('Alerta - Error inesperado!!');
                },
                error: function (result) {
                    toastr.error('Alerta - Error inesperado contactar a sistemas.');
                }
            });
        } else {
            toastr.error("Fecha final no puede ser menor a la inicial.");
            document.getElementById('ValidDate1').value = '';
            document.getElementById('ValidDate2').value = '';
            $('#ValidDate1').focus();
        }
    }
}

function reloadStyleTable() {
    $('#tabledetail').DataTable({
        oLanguage: {
            "sProcessing": "Procesando...", "sLengthMenu": "Mostrar _MENU_ registros", "sZeroRecords": "No se encontraron resultados", "sEmptyTable": "Ningún dato disponible en esta tabla", "sInfo": "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros", "sInfoEmpty": "Mostrando registros del 0 al 0 de un total de 0 registros", "sInfoFiltered": "(filtrado de un total de _MAX_ registros)", "sInfoPostFix": "", "sSearch": "Buscar:", "sUrl": "", "sInfoThousands": ",", "sLoadingRecords": "Cargando...", "oPaginate": { "sFirst": "Primero", "sLast": "Último", "sNext": "Siguiente", "sPrevious": "Anterior" }, "oAria": { "sSortAscending": ": Activar para ordenar la columna de manera ascendente", "sSortDescending": ": Activar para ordenar la columna de manera descendente" }
        }
    });
}

function Update() {
    if ($('#ValidDate1').val() != "" & $('#ValidDate2').val() != "") {
        if (moment($('#ValidDate1').val()) <= moment($('#ValidDate2').val())) {
            $.ajax({
                type: "GET",
                url: "/DamagedsGoods/ActionGetRMA/",
                data: { "Type": "Remision", "Date1": $('#ValidDate1').val(), "Date2": $('#ValidDate2').val(), "Status": 2 },
                success: function (result) {
                    if (result.success) {
                        $('#TableDiv').animatePanel();
                        table.clear();
                        table.rows.add($(result.Json));
                        table.columns.adjust().draw();
                    }
                    else
                        toastr.warning('Alerta - Error inesperado!!');
                },
                error: function (result) {
                    toastr.error('Alerta - Error inesperado contactar a sistemas.');
                }
            });
        } else {
            toastr.error("Fecha final no puede ser menor a la inicial.");
            document.getElementById('ValidDate1').value = '';
            document.getElementById('ValidDate2').value = '';
            $('#ValidDate1').focus();
        }
    }
    else {
        $.ajax({
            type: "GET",
            url: "/DamagedsGoods/ActionGetApprovedRMA/",
            success: function (result) {
                $('#TableDiv').animatePanel();
                table.clear();
                table.rows.add($(result.Json));
                table.columns.adjust().draw();
                document.getElementById('ValidDate1').value = '';
                document.getElementById('ValidDate2').value = '';
            },
            error: function (result) {
                toastr.error('Alerta - Error inesperado contactar a sistemas.');
            }
        });
    }
}

function AprovalMessage(s, g) {
    swal({
        title: "¿Esta seguro que desea Aprobar?",
        type: "info",
        showCancelButton: true,
        confirmButtonColor: "#55dd6b",
        cancelButtonText: "Cancelar",
        confirmButtonText: "Continuar"
    }, function (isConfirm) {
        if (isConfirm)
            Aproval(2, s, g);
    });
}

function Aproval(process_status, s, g) {
    var weight = $('#weight').val();
    var isDecimal = /^\d+\.?\d{0,3}$/
    if (isDecimal.test(weight)) {
        if (weight > 0 && weight <= 599999) {
            StartLoading();
            if (s) {
                axios.post("/DamagedsGoods/ActionAprovalRemision/?Folio=" + s + "&Weight=" + weight + "&Provider=" + g).then(function (data) {
                    EndLoading();
                    if (data.data == "SF")
                        toastr.warning('Error inesperado');
                    else if (data.data == "N/A")
                        toastr.warning('Folio no existente');
                    else if (data.data.value) {
                        $("#modalReferenceBody").html(`<iframe width='100%' height='550px' src='data:application/pdf;base64,${data.data.response}'></iframe>`);
                        $("#modalReference").modal('show');
                        toastr.success('Folio Aplicado con exito.');
                        toastr.success('Se ha enviado a su correo este mismo documento.');
                        Update();
                    }
                    else if (!data.data.value) {
                        swal({
                            title: 'Error',
                            text: "Folio cancelado previamente o inexistente.",
                            type: "error"
                        });
                    }
                    else
                        toastr.error('Alerta - Error inesperado  contactar a sistemas.');
                }).catch(function (error) {
                    EndLoading();
                    toastr.error('Alerta - Error inesperado  contactar a sistemas.');
                })
            }
            else {
                EndLoading();
                toastr.warning('El folio no puede ser nulo');
            }
        } else {
            EndLoading();
            setTimeout(function () {
                swal(
                    'Debe de ingresar el peso correcto de la mercancia.',
                    'Ingrese el peso total de la mercancia y vuelvalo a intentar.',
                    'error')
            }, 400);
        }
    } else {
        EndLoading();
        setTimeout(function () {
            swal(
                'Debe de ingresar el peso total de la mercancia.',
                'Ingrese el peso de la mercancia y vuelvalo a intentar.',
                'error')
        }, 400);
    }
}