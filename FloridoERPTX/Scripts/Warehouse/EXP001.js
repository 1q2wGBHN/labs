﻿var detailExpense = "1";
var subtotal = 0, iva = 0, ieps = 0, total, discount, iva_r, isr_r = 0
var subtotalusd = 0, ivausd = 0, iepsusd = 0, totalusd, discountusd, iva_rusd, isr_rusd = 0
const formatmoney = money => decimal => new Intl.NumberFormat("en-US", { style: "currency", currency: "USD", currencyDisplay: "symbol", maximumFractionDigits: decimal }).format(money);

$(document).ready(function () {
    console.log("ready");
    $('#cbox_ReportType').iCheck({
        checkboxClass: 'icheckbox_square-green',
    }).on('ifClicked', function () {
        $(this).trigger("click");
    });
    window.setTimeout(function () { $("#fa").click(); }, 1000);
});


function checkDetail() {
    if ($("#cbox_ReportType").is(':checked')) {
        $("#TableExpensesDetail").show();
        $("#TableDivExpensesDetail").show();
        $("#TableExpenses").hide();
        $("#TableDivExpenses").hide();
        detailExpense = "0";
    } else {
        $("#TableExpenses").show();
        $("#TableDivExpenses").show();
        $("#TableDivExpensesDetail").hide();
        $("#TableExpensesDetail").hide();
        detailExpense = "1";
    }
    SearchExpense();
}

function FillTableTotals(Expenses, productsType) {
    subtotal = 0, iva = 0, ieps = 0, total = 0, discount = 0, iva_r = 0, isr_r = 0
    subtotalusd = 0, ivausd = 0, iepsusd = 0, totalusd = 0, discountusd = 0, iva_rusd = 0, isr_rusd = 0
    if (productsType == "1") {
        Expenses.forEach(function (element) {
            if (element.Currency == "MXN") {
                subtotal += element.SubTotal;
                iva += element.TotalIVA;
                ieps += element.TotalIEPS;
                discount += element.Discount;
                isr_r += element.IsrRet;
                iva_r += element.IvaRet;
                total += element.TotalAmount;
            }
            else {
                subtotalusd += element.SubTotal;
                ivausd += element.TotalIVA;
                iepsusd += element.TotalIEPS;
                discountusd += element.Discount;
                iva_rusd += element.IvaRet;
                isr_rusd += element.IsrRet;
                totalusd += element.TotalAmount;
            }
        });
        document.getElementById("mxsubtotal").innerHTML = formatmoney(subtotal)(4);
        document.getElementById("usdsubtotal").innerHTML = formatmoney(subtotalusd)(4);
        document.getElementById("mxiva").innerHTML = formatmoney(iva)(4);
        document.getElementById("usdiva").innerHTML = formatmoney(ivausd)(4);
        document.getElementById("usddiscount").innerHTML = formatmoney(discountusd)(4);
        document.getElementById("mxdiscount").innerHTML = formatmoney(discount)(4);
        document.getElementById("mxieps").innerHTML = formatmoney(ieps)(4);
        document.getElementById("usdieps").innerHTML = formatmoney(iepsusd)(4);
        document.getElementById("mxiva_r").innerHTML = formatmoney(iva_r)(4);
        document.getElementById("usdiva_r").innerHTML = formatmoney(iva_rusd)(4);
        document.getElementById("mxisr_r").innerHTML = formatmoney(isr_r)(4);
        document.getElementById("usdisr_r").innerHTML = formatmoney(isr_rusd)(4);
        document.getElementById("mxtotal").innerHTML = formatmoney(total)(4);
        document.getElementById("usdtotal").innerHTML = formatmoney(totalusd)(4);
    } else {
        Expenses.forEach(function (element) {
            if (element.Currency == "MXN") {
                subtotal += element.SubTotal;
                iva += element.TotalIVA;
                ieps += element.TotalIEPS;
                discount += element.Discount;
                isr_r += element.IsrRet;
                iva_r += element.IvaRet;
                total += element.TotalAmount;
            }
            else {
                subtotalusd += element.SubTotal;
                ivausd += element.TotalIVA;
                iepsusd += element.TotalIEPS;
                discountusd += element.Discount;
                iva_rusd += element.IvaRet;
                isr_rusd += element.IsrRet;
                totalusd += element.TotalAmount;
            }
        });
        document.getElementById("mxsubtotalDetail").innerHTML = formatmoney(subtotal)(4);
        document.getElementById("usdsubtotalDetail").innerHTML = formatmoney(subtotalusd)(4);
        document.getElementById("mxivaDetail").innerHTML = formatmoney(iva)(4);
        document.getElementById("usdivaDetail").innerHTML = formatmoney(ivausd)(4);
        document.getElementById("usddiscountDetail").innerHTML = formatmoney(discountusd)(4);
        document.getElementById("mxdiscountDetail").innerHTML = formatmoney(discount)(4);
        document.getElementById("mxiepsDetail").innerHTML = formatmoney(ieps)(4);
        document.getElementById("usdiepsDetail").innerHTML = formatmoney(iepsusd)(4);
        document.getElementById("mxiva_rDetail").innerHTML = formatmoney(iva_r)(4);
        document.getElementById("usdiva_rDetail").innerHTML = formatmoney(iva_rusd)(4);
        document.getElementById("mxisr_rDetail").innerHTML = formatmoney(isr_r)(4);
        document.getElementById("usdisr_rDetail").innerHTML = formatmoney(isr_rusd)(4);
        document.getElementById("mxtotalDetail").innerHTML = formatmoney(total)(4);
        document.getElementById("usdtotalDetail").innerHTML = formatmoney(totalusd)(4);
    }
}

function ClearInputs() {
    document.getElementById("mxsubtotal").innerHTML = formatmoney(0)(4);
    document.getElementById("usdsubtotal").innerHTML = formatmoney(0)(4);
    document.getElementById("mxiva").innerHTML = formatmoney(0)(4);
    document.getElementById("usdiva").innerHTML = formatmoney(0)(4);
    document.getElementById("usddiscount").innerHTML = formatmoney(0)(4);
    document.getElementById("mxdiscount").innerHTML = formatmoney(0)(4);
    document.getElementById("mxieps").innerHTML = formatmoney(0)(4);
    document.getElementById("usdieps").innerHTML = formatmoney(0)(4);
    document.getElementById("mxiva_r").innerHTML = formatmoney(0)(4);
    document.getElementById("usdiva_r").innerHTML = formatmoney(0)(4);
    document.getElementById("mxisr_r").innerHTML = formatmoney(0)(4);
    document.getElementById("usdisr_r").innerHTML = formatmoney(0)(4);
    document.getElementById("mxtotal").innerHTML = formatmoney(0)(4);
    document.getElementById("usdtotal").innerHTML = formatmoney(0)(4);
    document.getElementById("mxsubtotalDetail").innerHTML = formatmoney(0)(4);
    document.getElementById("usdsubtotalDetail").innerHTML = formatmoney(0)(4);
    document.getElementById("mxivaDetail").innerHTML = formatmoney(0)(4);
    document.getElementById("usdivaDetail").innerHTML = formatmoney(0)(4);
    document.getElementById("usddiscountDetail").innerHTML = formatmoney(0)(4);
    document.getElementById("mxdiscountDetail").innerHTML = formatmoney(0)(4);
    document.getElementById("mxiepsDetail").innerHTML = formatmoney(0)(4);
    document.getElementById("usdiepsDetail").innerHTML = formatmoney(0)(4);
    document.getElementById("mxiva_rDetail").innerHTML = formatmoney(0)(4);
    document.getElementById("usdiva_rDetail").innerHTML = formatmoney(0)(4);
    document.getElementById("mxisr_rDetail").innerHTML = formatmoney(0)(4);
    document.getElementById("usdisr_rDetail").innerHTML = formatmoney(0)(4);
    document.getElementById("mxtotalDetail").innerHTML = formatmoney(0)(4);
    document.getElementById("usdtotalDetail").innerHTML = formatmoney(0)(4);
}

$("#supplier").select2(
    {
        minimumInputLength: 2,
        formatInputTooShort: function () {
            return "Busque un proveedor";
        },
        matcher: function (term, text, option) {
            return text.toUpperCase().indexOf(term.toUpperCase()) >= 0 || option.val().toUpperCase().indexOf(term.toUpperCase()) >= 0;
        }
    }
);
$("#typeTypeExpense").select2();
$("#typeCategory").select2();
$("#typeCurrency").select2();
$("#typeStatus").select2();


var table = $('#TableExpenses').DataTable({
    autoWidth: true,
    responsive: true,
    oLanguage: {
        "sProcessing": "Procesando...", "sLengthMenu": "Mostrar _MENU_ registros", "sZeroRecords": "No se encontraron resultados", "sEmptyTable": "Ningún dato disponible en esta tabla", "sInfo": "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros", "sInfoEmpty": "Mostrando registros del 0 al 0 de un total de 0 registros", "sInfoFiltered": "(filtrado de un total de _MAX_ registros)", "sInfoPostFix": "", "sSearch": "Buscar:", "sUrl": "", "sInfoThousands": ",", "sLoadingRecords": "Cargando...", "oPaginate": { "sFirst": "Primero", "sLast": "Último", "sNext": "Siguiente", "sPrevious": "Anterior" }, "oAria": { "sSortAscending": ": Activar para ordenar la columna de manera ascendente", "sSortDescending": ": Activar para ordenar la columna de manera descendente" }
    },
    dom: "<'row'<'col-sm-4'l><'col-sm-4 text-left'B><'col-sm-4'f>t<'col-sm-6'i><'col-sm-6'p>>",
    lengthMenu: [[10, 25, 50, -1], [10, 25, 50, "Todos"]],
    buttons: [
        {
            extend: 'csv', text: 'Excel', title: 'Gastos de Tienda', className: 'btn-sm', exportOptions: {
                columns: [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14]
            }
        },
        {
            extend: 'pdf', text: 'PDF', title: 'Gastos de Tienda', className: 'btn-sm', exportOptions: {
                columns: [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14]
            },
            customize: function (doc) {
                doc.content[1].table.widths =
                    Array(doc.content[1].table.body[0].length + 1).join('*').split('');
            }
        },
        { extend: 'print', text: 'Imprimir', className: 'btn-sm' },
    ],
    columns: [
        {
            "class": "details-control",
            "orderable": false,
            "data": null,
            "defaultContent": "",
        },
        { targets: 1, data: 'ReferenceFolio' },
        { targets: 2, data: 'Category' },
        { targets: 3, data: 'ReferenceType' },
        { targets: 4, data: 'Supplier' },
        { targets: 5, data: 'Invoice' },
        { targets: 6, data: 'InvoiceDate' },
        { targets: 7, data: 'Currency' },
        { targets: 8, data: 'SubTotal' },
        { targets: 9, data: 'TotalIVA' },
        { targets: 10, data: 'TotalIEPS' },
        { targets: 11, data: 'Discount' },
        { targets: 12, data: 'IsrRet' },
        { targets: 13, data: 'IvaRet' },
        { targets: 14, data: 'TotalAmount' },
        { targets: 15, data: null },
        { targets: 16, data: 'StatusText' }
    ],
    columnDefs: [{
        targets: [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16],
        width: "1%"
    },
    {
        targets: [6],
        render: function (data, type, row) {
            var date = moment(data).format('DD/MM/YYYY');
            if (date != 'Invalid date')
                return date;
            else
                return "";
        }
    },
    {
        targets: [8, 9, 10, 11, 12, 13, 14],
        render: function (data, type, row) {
            return `<p> ${formatmoney(data)()}</p>`
        }
    },
    {
        targets: [16],
        render: function (data, type, full, meta) {
            if (full.Expense_Status == 1 || full.Expense_Status == 2 || full.Expense_Status ==  3 )
                return `<strong> ${data}</strong >`
            else if (full.Expense_Status == 8)
                return `<strong class = "text-danger" > ${data}</strong >`
            else if (full.Expense_Status == 9)
                return `<strong class = "text-info" > ${data}</strong >`
        }
    },
    {
        data: null,
        width: "1%",
        targets: 15,
        defaultContent: '<button class="btn btn-xs btn-outline btn-info" type="submit"><i class="fa fa-file-text-o"></i> Reporte</button>'
    }
    ]
});
$("#TableExpenses").append('<tfoot><tr><th></th><th colspan = "5" style="color:#008000;">Totales</th><th  colspan = "2" style="color:#008000;">MXN</th><th id="mxsubtotal" style="color:#008000;">$0</th><th id="mxiva" style="color:#008000;">$0</th><th id ="mxieps" style="color:#008000;">$0</th><th id ="mxdiscount" style="color:#008000;">$0</th><th id ="mxisr_r" style="color:#008000;">$0</th><th id ="mxiva_r" style="color:#008000;">$0</th><th id ="mxtotal" colspan = "3" style="color:#008000;">$0</th></tr><tr><th></th><th colspan = "5"  style="color:#0D0DFF;">Totales</th><th  colspan = "2"  style="color:#0D0DFF;">USD</th><th id="usdsubtotal" style="color:#0D0DFF;">$0</th><th id="usdiva"  style="color:#0D0DFF;">$0</th><th id ="usdieps"  style="color:#0D0DFF;">$0</th><th id ="usddiscount" style="color:#0D0DFF;">$0</th><th id ="usdisr_r" style="color:#0D0DFF;">$0</th><th id ="usdiva_r" style="color:#0D0DFF;">$0</th><th id ="usdtotal" colspan = "3" style="color:#0D0DFF;">$0</th></tr></tfoot>');

var tableDetail = $('#TableExpensesDetail').DataTable({
    autoWidth: true,
    responsive: true,
    oLanguage: {
        "sProcessing": "Procesando...", "sLengthMenu": "Mostrar _MENU_ registros", "sZeroRecords": "No se encontraron resultados", "sEmptyTable": "Ningún dato disponible en esta tabla", "sInfo": "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros", "sInfoEmpty": "Mostrando registros del 0 al 0 de un total de 0 registros", "sInfoFiltered": "(filtrado de un total de _MAX_ registros)", "sInfoPostFix": "", "sSearch": "Buscar:", "sUrl": "", "sInfoThousands": ",", "sLoadingRecords": "Cargando...", "oPaginate": { "sFirst": "Primero", "sLast": "Último", "sNext": "Siguiente", "sPrevious": "Anterior" }, "oAria": { "sSortAscending": ": Activar para ordenar la columna de manera ascendente", "sSortDescending": ": Activar para ordenar la columna de manera descendente" }
    },
    dom: "<'row'<'col-sm-4'l><'col-sm-4 text-left'B><'col-sm-4'f>t<'col-sm-6'i><'col-sm-6'p>>",
    lengthMenu: [[10, 25, 50, -1], [10, 25, 50, "Todos"]],
    buttons: [
        {
            extend: 'csv', text: 'Excel', title: 'Gastos de Tienda', className: 'btn-sm', exportOptions: {
                columns: [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17]
            }
        },
        {
            extend: 'pdf', text: 'PDF', title: 'Gastos de Tienda', className: 'btn-sm', exportOptions: {
                columns: [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17]
            },
            customize: function (doc) {
                doc.content[1].table.widths =
                    Array(doc.content[1].table.body[0].length + 1).join('*').split('');
            }
        },
        { extend: 'print', text: 'Imprimir', className: 'btn-sm' },
    ],
    columns: [
        { targets: 0, data: 'ReferenceFolio' },
        { targets: 1, data: 'Invoice' },
        { targets: 2, data: 'PartNumber' },
        { targets: 3, data: 'Description' },
        { targets: 4, data: 'Category' },
        { targets: 5, data: 'ReferenceType' },
        { targets: 6, data: 'Supplier' },
        { targets: 7, data: 'InvoiceDate' },
        { targets: 8, data: 'Currency' },
        { targets: 9, data: 'UnitCost' },
        { targets: 10, data: 'Quantity' },
        { targets: 11, data: 'SubTotal' },
        { targets: 12, data: 'TotalIVA' },
        { targets: 13, data: 'TotalIEPS' },
        { targets: 14, data: 'Discount' },
        { targets: 15, data: 'IsrRet' },
        { targets: 16, data: 'IvaRet' },
        { targets: 17, data: 'TotalAmount' }
    ],
    columnDefs: [{
        targets: [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17],
        width: "1%"
    },
    {
        targets: [7],
        render: function (data, type, row) {
            var date = moment(data).format('DD/MM/YYYY');
            if (date != 'Invalid date')
                return date;
            else
                return "";
        }
    },
    {
        targets: [9, 11, 12, 13, 14, 15, 16, 17],
        render: function (data, type, row) {
            return `<p> ${formatmoney(data)()}</p>`
        }
    }
    ]
});
$("#TableExpensesDetail").append('<tfoot><tr><th></th><th colspan = "8" style="color:#008000;">Totales</th><th  colspan = "2" style="color:#008000;">MXN</th><th id="mxsubtotalDetail" style="color:#008000;">$0</th><th id="mxivaDetail" style="color:#008000;">$0</th><th id ="mxiepsDetail" style="color:#008000;">$0</th><th id ="mxdiscountDetail" style="color:#008000;">$0</th><th id ="mxisr_rDetail" style="color:#008000;">$0</th><th id ="mxiva_rDetail" style="color:#008000;">$0</th><th id ="mxtotalDetail" colspan = "2" style="color:#008000;">$0</th></tr><tr><th></th><th colspan = "8"  style="color:#0D0DFF;">Totales</th><th  colspan = "2"  style="color:#0D0DFF;">USD</th><th id="usdsubtotalDetail" style="color:#0D0DFF;">$0</th><th id="usdivaDetail"  style="color:#0D0DFF;">$0</th><th id ="usdiepsDetail"  style="color:#0D0DFF;">$0</th><th id ="usddiscountDetail" style="color:#0D0DFF;">$0</th><th id ="usdisr_rDetail" style="color:#0D0DFF;">$0</th><th id ="usdiva_rDetail" style="color:#0D0DFF;">$0</th><th id ="usdtotalDetail" colspan = "2" style="color:#0D0DFF;">$0</th></tr></tfoot>');



var detailRows = [];

$('#TableExpenses tbody').on('click', 'tr td.details-control', function () {
    var tr = $(this).closest('tr');
    var row = table.row(tr);
    if (row.child.isShown()) {
        row.child.hide();
        tr.removeClass('details');
    }
    else {
        if (table.row('.details').length) {
            $('.details-control', table.row('.details').node()).click();
        }
        row.child(formatOne(row.data())).show();
        tr.addClass('details');
    }
});

table.on('draw', function () {
    $.each(detailRows, function (i, id) {
        $('#' + id + ' td.details-control').trigger('click');
    });
});

function formatOne(d) {
    var detailItems = "";
    var tabledetail = $('<div/>').addClass('loading').text('Cargando Datos...');
    $.ajax({
        url: '/Expenses/ActionExpenseGetItemById/',
        data: { "ReferenceFolio": d.ReferenceFolio },
        type: "GET",
        success: function (Data) {
            $.each(Data, function (index, value) {
                detailItems += "<tr><td>" + value.item_no + "</td><td>" + value.item_description + "</td><td>" + value.quantity + "</td><td>" + formatmoney(value.unit_cost)(4) + "</td><td>" + formatmoney(value.iva)(4) + "</td><td>" + formatmoney(value.ieps)(4) + "</td><td>" + formatmoney(value.iva_retained)(4) + "</td><td>" + formatmoney(value.ieps_retained)(4) + "</td><td>" + formatmoney(value.discount)(4) + "</td><td>" + formatmoney(value.item_amount)(4) + "</td>"

            });
            tabledetail.html('<table id="tabledetail" class="table table-striped table-bordered table-hover" style="width:100%"><thead><tr><th>Código</th><th>Descripción</th><th>Cantidad</th><th>Precio Unitario</th><th>IVA</th><th>IEPS</th><th>IVA Retenido</th><th>IEPS Retenido</th><th>Descuento</th><th>Subtotal</th></tr></thead><tbody>' + detailItems + '</tbody></table>').removeClass('loading');
            reloadStyleTable();
        }
    });
    return tabledetail;
}

function reloadStyleTable() {
    $('#tabledetail').DataTable({
        oLanguage: {
            "sProcessing": "Procesando...", "sLengthMenu": "Mostrar _MENU_ registros", "sZeroRecords": "No se encontraron resultados", "sEmptyTable": "Ningún dato disponible en esta tabla", "sInfo": "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros", "sInfoEmpty": "Mostrando registros del 0 al 0 de un total de 0 registros", "sInfoFiltered": "(filtrado de un total de _MAX_ registros)", "sInfoPostFix": "", "sSearch": "Buscar:", "sUrl": "", "sInfoThousands": ",", "sLoadingRecords": "Cargando...", "oPaginate": { "sFirst": "Primero", "sLast": "Último", "sNext": "Siguiente", "sPrevious": "Anterior" }, "oAria": { "sSortAscending": ": Activar para ordenar la columna de manera ascendente", "sSortDescending": ": Activar para ordenar la columna de manera descendente" }
        },
    });
}

var model = JSON.parse($("#model").val());
if (!$.isEmptyObject(model.ExpensesReportModelList)) {
    $("#TableDivExpensesDetail").hide();
    $("#TableExpensesDetail").hide();
    $('#TableDivExpenses').animatePanel();
    FillTableTotals(model.ExpensesReportModelList, "1");
    table.clear();
    table.rows.add($(model.ExpensesReportModelList));
    table.columns.adjust().draw();
    $("#Print").removeAttr('disabled', 'disabled');
}
else {
    $('#TableDivExpenses').animatePanel();
    table.clear();
    table.columns.adjust().draw();
    $("#Print").attr('disabled', true);
}

function SearchExpense() {
    if (ValidateDates()) {
        var supplierid = $("#supplier").val();
        var typeExpense = $("#typeTypeExpense").val();
        var category = $("#typeCategory").val();
        var begindate = $('#BeginDate').val();
        var enddate = $('#EndDate').val();
        var currency = $('#typeCurrency').val();
        var status = $('#typeStatus').val();
        StartLoading();
        ClearInputs();
        var url = "";
        var tableDiv = "";
        if (detailExpense == "1") {
            url = "/Expenses/ActionExpensesReport/";
            tableDiv = "TableDivExpenses";
        }
        else {
            url = "/Expenses/ActionExpensesReportDetail/";
            tableDiv = "TableDivExpensesDetail";
        }
        $.ajax({
            url: url,
            type: "GET",
            data: { "SupplierId": supplierid, "Category": category, "BeginDate": begindate, "EndDate": enddate, "Currency": currency, "typeExpense": typeExpense, "status": status },
            success: function (result) {
                EndLoading();
                if (result.status) {
                    if (!$.isEmptyObject(result.model)) {
                        if (detailExpense == "1") {
                            $('#' + tableDiv).animatePanel();
                            FillTableTotals(result.model, detailExpense);
                            table.clear();
                            table.rows.add($(result.model));
                            table.columns.adjust().draw();
                        } else {
                            $('#' + tableDiv).animatePanel();
                            FillTableTotals(result.model, detailExpense);
                            tableDetail.clear();
                            tableDetail.rows.add($(result.model));
                            tableDetail.columns.adjust().draw();
                        }
                    }
                    else {
                        $('#' + tableDiv).animatePanel();
                        table.clear();
                        table.columns.adjust().draw();
                    }
                }
                else
                    SessionFalse("Se terminó su sesion.");
            },
            error: function () {
                toastr.remove();
                toastr.error('Error inesperado contactar a sistemas.');
                EndLoading();
                //$("#Print").attr('disabled', true);
            }
        });
    }
}

$('#Search').on('click', function () {
    if (ValidateDates()) {
        var supplierid = $("#supplier").val();
        var typeExpense = $("#typeTypeExpense").val();
        var category = $("#typeCategory").val();
        var begindate = $('#BeginDate').val();
        var enddate = $('#EndDate').val();
        var currency = $('#typeCurrency').val();
        var status = $('#typeStatus').val();
        StartLoading();
        ClearInputs();
        var url = "";
        var tableDiv = "";
        if (detailExpense == "1") {
            url = "/Expenses/ActionExpensesReport/";
            tableDiv = "TableDivExpenses";
        }
        else {
            url = "/Expenses/ActionExpensesReportDetail/";
            tableDiv = "TableDivExpensesDetail";
        }
        $.ajax({
            url: url,
            type: "GET",
            data: { "SupplierId": supplierid, "Category": category, "BeginDate": begindate, "EndDate": enddate, "Currency": currency, "typeExpense": typeExpense, "status": status },
            success: function (result) {
                EndLoading();
                if (result.status) {
                    if (!$.isEmptyObject(result.model)) {
                        if (detailExpense == "1") {
                            $('#' + tableDiv).animatePanel();
                            FillTableTotals(result.model, detailExpense);
                            table.clear();
                            table.rows.add($(result.model));
                            table.columns.adjust().draw();
                        } else {
                            $('#' + tableDiv).animatePanel();
                            FillTableTotals(result.model, detailExpense);
                            tableDetail.clear();
                            tableDetail.rows.add($(result.model));
                            tableDetail.columns.adjust().draw();
                        }
                    }
                    else {
                        $('#' + tableDiv).animatePanel();
                        table.clear();
                        table.columns.adjust().draw();
                    }
                }
                else
                    SessionFalse("Se terminó su sesion.");
            },
            error: function () {
                toastr.remove();
                toastr.error('Error inesperado contactar a sistemas.');
                EndLoading();
                //$("#Print").attr('disabled', true);
            }
        });

    }
});

$('#Print').on('click', function () {
    if (ValidateDates()) {
        var urlDirect = "";
        var supplierid = $("#supplier").val();
        var category = $("#typeCategory").val();
        var typeExpense = $("#typeTypeExpense").val();
        var begindate = $('#BeginDate').val();
        var enddate = $('#EndDate').val();
        var currency = $('#typeCurrency').val();
        var status = $('#typeStatus').val();
        console.log(detailExpense);
        if (detailExpense === "0")
            urlDirect = "/Expenses/ActionExpensesReportDetailPrintNew";
        else
            urlDirect = "/Expenses/ActionExpensesReportPrintNew";
        StartLoading();
        $.ajax({
            url: urlDirect,
            type: "POST",
            data: { "SupplierId": supplierid, "Category": category, "BeginDate": begindate, "EndDate": enddate, "Currency": currency, "typeExpense": typeExpense, "status": status },
            success: function (response) {
                typeExpense
                EndLoading();
                if (response == "SF")
                    SessionFalse("Termino tu session");
                else {
                    document.getElementById("iframe").srcdoc = response;
                    EndLoading();
                    $("#ModalDescription").html('Reporte de gastos');
                    $('#ModalReport').modal('show');
                }
            },
            error: function () {
                EndLoading();
                toastr.remove();
                toastr.error('Error inesperado contactar a sistemas.');
            }
        });
    }

});

function ValidateDates() {
    if ($('#BeginDate').val() != "" & $('#EndDate').val() != "") {
        if (!(moment($('#BeginDate').val()) <= moment($('#EndDate').val()))) {
            toastr.remove();
            toastr.warning('La fecha inicial no puede ser mayor a la fecha final');
            return false;
        }
        else {
            toastr.remove();
            return true;
        }
    }
    else {
        toastr.remove();
        return true;
    }
}

$("#BeginDate").datepicker({
    autoclose: true,
    todayHighlight: true
}).on("changeDate", function () {
    ValidateDates();
});

$("#EndDate").datepicker({
    autoclose: true,
    todayHighlight: true
}).on("changeDate", function () {
    ValidateDates();
});


$('#TableExpenses tbody').on('click', 'button', function () {
    var values = table.row($(this).parents('tr')).data();
    values = $.map(values, function (value, index) {
        switch (index) {
            case 'ReferenceFolio':
                StartLoading();
                $.ajax({
                    type: "GET",
                    url: "/Expenses/ActionGetExpenseReportById",
                    data: { "referende_document": value },
                    success: function (returndates) {
                        if (returndates == "SF")
                            SessionFalse("Termino tu session");
                        document.getElementById("iframe").srcdoc = returndates;
                        EndLoading();
                        $("#ModalDescription").html('Reporte de gastos');
                        $('#ModalReport').modal('show');
                    },
                    error: function (returndates) {
                        toastr.error('Alerta - Error inesperado  contactar a sistemas.');
                        EndLoading();
                    }
                });
                break;
        }
    });
});
