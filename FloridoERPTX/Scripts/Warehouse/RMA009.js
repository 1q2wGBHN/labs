﻿//ESTA VISTA COMPARTE CÓDIGO DE JS PARA LA PAGINA RNA0010
//ESTA VISTA COMPARTE CÓDIGO DE JS PARA LA PAGINA RNA0010
//ESTA VISTA COMPARTE CÓDIGO DE JS PARA LA PAGINA RNA0010
//ESTA VISTA COMPARTE CÓDIGO DE JS PARA LA PAGINA RNA0010
//ESTA VISTA COMPARTE CÓDIGO DE JS PARA LA PAGINA RNA0010
//ESTA VISTA COMPARTE CÓDIGO DE JS PARA LA PAGINA RNA0010
//ESTA VISTA COMPARTE CÓDIGO DE JS PARA LA PAGINA RNA0010
//ESTA VISTA COMPARTE CÓDIGO DE JS PARA LA PAGINA RNA0010
//ESTA VISTA COMPARTE CÓDIGO DE JS PARA LA PAGINA RNA0010
//ESTA VISTA COMPARTE CÓDIGO DE JS PARA LA PAGINA RNA0010
//ESTA VISTA COMPARTE CÓDIGO DE JS PARA LA PAGINA RNA0010
//ESTA VISTA COMPARTE CÓDIGO DE JS PARA LA PAGINA RNA0010
//ESTA VISTA COMPARTE CÓDIGO DE JS PARA LA PAGINA RNA0010
//ESTA VISTA COMPARTE CÓDIGO DE JS PARA LA PAGINA RNA0010
//ESTA VISTA COMPARTE CÓDIGO DE JS PARA LA PAGINA RNA0010
//ESTA VISTA COMPARTE CÓDIGO DE JS PARA LA PAGINA RNA0010
//ESTA VISTA COMPARTE CÓDIGO DE JS PARA LA PAGINA RNA0010
//ESTA VISTA COMPARTE CÓDIGO DE JS PARA LA PAGINA RNA0010
const fomartmoney = money => decimal => new Intl.NumberFormat("en-US", { style: "currency", currency: "USD", currencyDisplay: "symbol", maximumFractionDigits: decimal }).format(money);

$(document).ready(function () {
    window.setTimeout(function () { $("#fa").click(); }, 1000);

    $("#statusSelect").select2();
});

$("#ValidDate1").datepicker({
    autoclose: true,
    todayHighlight: true,
    showButtonPanel: true,
}).on("changeDate", function (e) {

});

$("#ValidDate2").datepicker({
    autoclose: true,
    startDate: "01/01/1950",
    todayHighlight: true,
}).on("changeDate", function (e) {

});

$("#TablePurchases").append('<tfoot><th>Total</th><th colspan="7"></th><th></th><th></th></tfoot>');

var table = $("#TablePurchases").DataTable({
    oLanguage: {
        "sProcessing": "Procesando...", "sLengthMenu": "Mostrar _MENU_ registros", "sZeroRecords": "No se encontraron resultados", "sEmptyTable": "Ningún dato disponible en esta tabla", "sInfo": "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros", "sInfoEmpty": "Mostrando registros del 0 al 0 de un total de 0 registros", "sInfoFiltered": "(filtrado de un total de _MAX_ registros)", "sInfoPostFix": "", "sSearch": "Buscar:", "sUrl": "", "sInfoThousands": ",", "sLoadingRecords": "Cargando...", "oPaginate": { "sFirst": "Primero", "sLast": "Último", "sNext": "Siguiente", "sPrevious": "Anterior" }, "oAria": { "sSortAscending": ": Activar para ordenar la columna de manera ascendente", "sSortDescending": ": Activar para ordenar la columna de manera descendente" }
    },
    responsive: true,
    autoWidth: true,
    order: [[1, "desc"]],
    dom: "<'row'<'col-sm-4'l><'col-sm-4 text-left'B><'col-sm-4'f>t<'col-sm-6'i><'col-sm-6'p>>",
    lengthMenu: [[10, 25, 50, -1], [10, 25, 50, "Todos"]],
    buttons: [
        { extend: 'csv', text: 'Excel', title: 'ReporteRemision', className: 'btn-sm', footer: true },
        {
            extend: 'pdf', text: 'PDF', title: 'ReporteRemision', className: 'btn-sm', exportOptions: {
                columns: [1, 2, 3]
            }, footer: true,
            customize: function (doc) {
                doc.content[1].table.widths =
                    Array(doc.content[1].table.body[0].length + 1).join('*').split('');
            }
        },
        { extend: 'print', text: 'Imprimir', className: 'btn-sm', footer: true },
    ],
    "footerCallback": function (row, data, start, end, display) {
        var api = this.api(), data;
        // converting to interger to find total
        var intVal = function (i) {
            return typeof i === 'string' ?
                i.replace(/[\$,]/g, '') * 1 :
                typeof i === 'number' ?
                i : 0;
        };

        for (var i = 8; i < 9; i++) {
            var monTotal = api
                .column(i)
                .data()
                .reduce(function (a, b) {
                    return intVal(a) + intVal(b);
                }, 0);
            $(api.column(i).footer()).html(`<span ${monTotal == 0 ? "" : `style = "Color:${monTotal >= 0 ? "green" : "red"}"`} > ${fomartmoney(monTotal)(4)}</span >`);
        }
    },
    columns: [
        {
            class: "details-control",
            orderable: false,
            data: null,
            defaultContent: "",
            width: "1%"
        },
        { data: "DamagedGoodsDoc", width: "1%" },
        { data: "Supplier", width: "1%" },
        { data: "Comment", width: "1%" },
        { data: "ProcessStatusDescription", width: "1%" },
        { data: "ProcessStatus", width: "1%" },
        { data: "cdate", width: "1%" },
        { data: "processDate", width: "1%" },
        { data: "Total", width: "1%" },
        { data: null, width: "1%" }
    ],
    columnDefs: [{
        targets: [0, 1, 2, 3, 4, 5, 6, 7, 8, 9],
        width: "1%"
    },
    {
        targets: [3],
        render: function (data, type, full, meta) {
            if (data == "" || data == null)
                return "Sin comentarios";
            else
                return data;
        },
        width: "1%"
    },
    {
        targets: [5],
        visible: false,
        searchable: false,
        width: "1%"
    },
    {
        targets: [6],
        render: function (data, type, full, meta) {
            if (data != null)
                return moment(data).format('DD/MM/YYYY');
            else
                return "N/A";
        },
        width: "1%"
    },
    {
        targets: [7],
        render: function (data, type, full, meta) {
            if (full.ProcessStatusDescription == "Terminada") {
                if (data != null)
                    return moment(data).format('DD/MM/YYYY');
                else
                    return "N/A";
            }
            else
                return "N/A";
        },
        width: "1%"
    },
    {
        targets: [8],
        render: function (data, type, full, meta) {
            return fomartmoney(data)(4);
        },
        width: "1%"
    },
        {
            targets: [9],
            render: function (data, type, full, meta) {
                if (full.ProcessStatus == 8 || full.ProcessStatus == 9 || full.ProcessStatus == 3)
                    return '<button class="btn btn-xs btn-outline btn-info btnReport"><i class="fa fa-file-pdf-o"></i> Reporte</button>';
                else
                    return '<button class="btn btn-xs btn-info btnReport" disabled><i class="fa fa-file-pdf-o"></i> Reporte</button>';
            },
            width: "1%"
        }]
});

var detailRows = [];

$("#TablePurchases tbody").on("click", "tr td.details-control", function () {
    var tr = $(this).closest("tr");
    var row = table.row(tr);

    if (row.child.isShown()) {
        row.child.hide();
        tr.removeClass("details");
    }
    else {
        if (table.row(".details").length) {
            $(".details-control", table.row(".details").node()).click();
        }
        row.child(format(row.data())).show();
        tr.addClass("details");
    }
});

$('#TablePurchases tbody').on('click', 'button.btnReport', function () {
    var tr = $(this).closest('tr');
    var row = table.row(tr);
    model = row.data();
    StartLoading();
    $.post({
        url: '/DamagedsGoods/ActionGetPrintRMAWithControl',
        Type: 'POST',
        data: { "referenceNumber": model.DamagedGoodsDoc, "referenceType": model.ProcessType },
        success: function (data) {
            EndLoading();
            if (data.Data) {
                $("#modalReferenceBody").html(`<iframe width='100%' height='550px' src='data:application/pdf;base64,${data.response}'></iframe>`);
                $("#modalReference").modal("show");
            } else {
                SessionFalse("Termino tu session");
            }
        },
        error: function () {
            toastr.error('Alerta - Error inesperado  contactar a sistemas.');
        }
    }).then(() => { EndLoading() });
});

table.on("draw", function () {
    $.each(detailRows, function (i, id) {
        $("#" + id + " td.details-control").trigger("click");
    });
});

trTotal = element => `${fomartmoney(element.Iva + element.Ieps + element.Import)(4)}`
FullTd = element => Option => `<td>${Option == 1 ? fomartmoney(element)(4) : Option == 0 ? element : element != null && element.trim() != "" ? "Contado" : "No contado"}</td >`
FullTh = element => `<th>${element}</th>`
FullTable = element => array => array.map(title => FullTd(element[title.title])(title.option)).toString();
FullTableHeader = array => `<tr>${array.map(title => FullTh(title))}</tr>`

function format(d) {
    var detailItems = "";
    var tabledetail = $("<div/>").addClass("loading").text("Cargando Datos...");
    $.ajax({
        url: "/DamagedsGoods/ActionGetAllItemSupplierByRma/",
        data: { "Folio": d.DamagedGoodsDoc, "Type": "Remision", "Status": d.ProcessStatus },
        type: "GET",
        success: function (data) {
            if (data != "SF") {
                detailItems = data.map(value => ` <tr>${FullTable(value)([
                    { title: "PartNumber", option: 0 },
                    { title: "Description", option: 0 },
                    { title: "Size", option: 0 },
                    { title: "Storage", option: 0 },
                    { title: "Quantity", option: 0 },
                    { title: "Price", option: 1 },
                    { title: "Amount_Doc", option: 1 },
                ])}</tr>`).join().replace(/>,</g, '><').replace(/>, </g, '><')
                header = FullTableHeader(["Codigo", "Nombre", "UM", "Locacion", "Cantidad", "Precio", "Monto" /*"Iva","Ieps" ,"Total"*/]).replace(/,/g, '')
                var Html = (`<table id="tabledetail" class="table table-striped table-bordered table-hover" style="width:100%"><thead>
                   ${header}
                    </thead><tbody>${detailItems.length > 0 ? detailItems : "<td colspan='8' style='text-align: center;'>No cuenta con productos</td>"}</tbody></table>`)
                tabledetail.html(Html).removeClass('loading');
            }
            else
                SessionFalse("Termino tu session")
            reloadStyleTable();
        }
    });
    return tabledetail;
}

function SerchRMA() {
    toastr.remove();
    StartLoading();
    $.ajax({
        type: "GET",
        url: "/DamagedsGoods/ActionGetRMAFullTable/",
        data: { "Type": "Remision", "DateInit": $("#ValidDate1").val(), "DateFinal": $("#ValidDate2").val(), "Status": $("#statusSelect").val() },
        success: function (result) {
            if (result == "SF")
                SessionFalse("Terminó su sesión");
            if (result.length > 0) {
                table.clear();
                table.rows.add(result);
                table.columns.adjust().draw();
                $('#TableDiv').animatePanel();
            }
            else if (result.length == 0) {
                table.clear();
                table.columns.adjust().draw();
                $('#TableDiv').animatePanel();
                toastr.warning("No hay remisiones con esos filtros");
            }
            else
                toastr.warning("Alerta - Error inesperado!!");
        },
        error: function () {
            toastr.error("Alerta - Error inesperado contactar a sistemas.");
        }
    });
    EndLoading();
}

function reloadStyleTable() {
    $("#tabledetail").DataTable({
        oLanguage: {
            "sProcessing": "Procesando...", "sLengthMenu": "Mostrar _MENU_ registros", "sZeroRecords": "No se encontraron resultados", "sEmptyTable": "Ningún dato disponible en esta tabla", "sInfo": "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros", "sInfoEmpty": "Mostrando registros del 0 al 0 de un total de 0 registros", "sInfoFiltered": "(filtrado de un total de _MAX_ registros)", "sInfoPostFix": "", "sSearch": "Buscar:", "sUrl": "", "sInfoThousands": ",", "sLoadingRecords": "Cargando...", "oPaginate": { "sFirst": "Primero", "sLast": "Último", "sNext": "Siguiente", "sPrevious": "Anterior" }, "oAria": { "sSortAscending": ": Activar para ordenar la columna de manera ascendente", "sSortDescending": ": Activar para ordenar la columna de manera descendente" }
        },
    });
}