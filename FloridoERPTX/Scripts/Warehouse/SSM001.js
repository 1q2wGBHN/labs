﻿$(document).ready(function () {
    window.setTimeout(function () { $("#fa").click(); }, 1000);

    $('#dateInit').datepicker({
        autoclose: true,
        endDate: '01/01/2100',
        language: 'es',
        format: 'mm/dd/yyyy',
        todayHighlight: true,
    }).on("changeDate", function (e) {
        SearchDocuments();
    });

    $('#dateFin').datepicker({
        autoclose: true,
        endDate: '01/01/2100',
        language: 'es',
        format: 'mm/dd/yyyy',
        todayHighlight: true,
    }).on("changeDate", function (e) {
        SearchDocuments();
    });

    var headers = JSON.parse($("#model").val());
    table.clear();
    table.rows.add(jQuery(headers));
    table.columns.adjust().draw();
});

var table = $('#tableEMSCHead').DataTable({
    autoWidth: true,
    oLanguage:
    {
        "sProcessing": "Procesando...",
        "sLengthMenu": "Mostrar _MENU_ registros",
        "sZeroRecords": "No se encontraron resultados",
        "sEmptyTable": "Ningún dato disponible en esta tabla",
        "sInfo": "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
        "sInfoEmpty": "Mostrando registros del 0 al 0 de un total de 0 registros",
        "sInfoFiltered": "(filtrado de un total de _MAX_ registros)",
        "sInfoPostFix": "",
        "sSearch": "Buscar:",
        "sUrl": "",
        "sInfoThousands": ",",
        "sLoadingRecords": "Cargando...",
        "oPaginate": { "sFirst": "Primero", "sLast": "Último", "sNext": "Siguiente", "sPrevious": "Anterior" },
        "oAria": { "sSortAscending": ": Activar para ordenar la columna de manera ascendente", "sSortDescending": ": Activar para ordenar la columna de manera descendente" }
    },
    dom: "<'row'<'col-sm-4'l><'col-sm-4 text-left'B><'col-sm-4'f>t<'col-sm-6'i><'col-sm-6'p>>",
    lengthMenu: [[10, 25, 50, -1], [10, 25, 50, "Todos"]],
    buttons:
        [
            { extend: 'csv', text: 'Excel', title: 'Merma Contralada', className: 'btn-sm' },
            {
                extend: 'pdf', text: 'PDF', title: 'Merma Contralada', className: 'btn-sm',
                exportOptions: { columns: [1, 2, 3] },
                customize: function (doc) {
                    doc.content[1].table.widths = Array(doc.content[1].table.body[0].length + 1).join('*').split('');
                }
            },
            { extend: 'print', text: 'Imprimir', className: 'btn-sm' },
        ],
    columns:
        [
            {
                "class": "details-control",
                "orderable": false,
                "data": null,
                "defaultContent": ""
            },
            { data: 'Document' },
            { data: 'User' },
            { data: 'SiteName' },
            { data: 'Fecha' },
        ],
    columnDefs: [
        {
            targets: [0, 1, 2, 3, 4],
            width: "1%"
        }]
});

var detailRows = [];
$('#tableEMSCHead tbody').on('click', 'tr td.details-control', function () {
    var tr = $(this).closest('tr');
    var row = table.row(tr);
    if (row.child.isShown()) {
        row.child.hide();
        tr.removeClass('details');
    }
    else {
        if (table.row('.details').length) {
            $('.details-control', table.row('.details').node()).click();
        }
        row.child(format(row.data())).show();
        tr.addClass('details');
    }
});

table.on('draw', function () {
    $.each(detailRows, function (i, id) {
        $('#' + id + ' td.details-control').trigger('click');
    });
});

function setValues(Quantity, Ieps, Iva, Subtotal, Total) {
    if (Ieps <= 0)
        Ieps = 0;
    if (Iva <= 0)
        Iva = 0;
    if (Subtotal <= 0)
        Subtotal = 0;
    if (Total <= 0)
        Total = 0;

    $("#Quantity").text(Quantity);
    $("#IEPS").text("$" + (Ieps).toFixed(4));
    $("#Iva").text("$" + (Iva).toFixed(4));
    $("#Subtotal").text("$" + (Subtotal).toFixed(4));
    $("#Total").text("$" + (Total).toFixed(4));
}
var list = [];
function format(d) {
    var detailItems = "";
    var tabledetail = $('<div/>').addClass('loading').text('Cargando Datos...');
    list = [];
    $.ajax({
        url: "/DamagedsGoods/ActionGetItemsDocumentSingle",
        data: { "document": d.Document, "status": 1 },
        type: "GET",
        success: function (returndate) {
            $.each(returndate.Json, function (index, value) {
                total = value.IVA + value.IEPS + value.Amount;
                detailItems += "<tr> <td style='width:1%' class='text-center'><div class='checkbox'><label><input type='checkbox' class='i-checks' IdSingle = " + value.id_single + " PartNumber = " + value.PartNumber + "  Quantity=" + value.Quantity + " IEPS=" + value.IEPS + " Iva=" + value.IVA + " SubTotal=" + value.Amount + " Amount=" + total + " /></label></div></td> <td style='width:1%'>" + value.PartNumber + "</td><td style='width:1%'>" + value.Description + "</td><td style='width:1%'>" + (value.ScrapReason == null ? 'Sin razón' : value.ScrapReason) + "</td><td style='width:1%' class='text-right'>$ " + value.Price + "</td><td style='width:1%' class='text-right'>" + value.Quantity + "</td><td style='width:1%' class='text-right'>" + "$" + value.Amount + "</td><td style='width:1%' class='text-right'>" + "$ " + value.IEPS + "</td><td style='width:1%' class='text-right'>" + "$ " + value.IVA + "</td><td style='width:1%' class='text-right'>" + "$ " + total + "</td></tr>"
            });

            info = '<div class="row"> <div class="col-md-7 pull-right"><table id="totals" class="table table-striped table-bordered table-hover text-center" style="width:100%"><thead><tr><th class="text-center">Cantidad</th><th class="text-center">IEPS</th><th class="text-center">IVA</th><th class="text-center">Subtotal</th><th class="text-center">Total</th></tr></thead><tbody> <tr> <td id="Quantity"></td> <td id="IEPS">‬</td> <td id="Iva"></td> <td id="Subtotal"></td> <td id="Total">‬</td> </tr> </tbody></table></div> </div>';

            btn = '<div class="pull-right"><button id="btn-cancel" type="button" class="btn btn-sm btn-danger" onclick="CancelMessage(' + d.Document + ');"><i class="fa fa-close"></i> Rechazar</button>&nbsp;&nbsp;&nbsp;<button id="btn-save" type="button" class="btn btn-sm btn-success" onclick="AprovalMessage(' + d.Document + ');"><i class="fa fa-check"></i> Aprobar</button></div>'
            tabledetail.html('<table id="tabledetail" class="table table-striped table-bordered table-hover" style="width:100%">' +
                '<thead><tr><th></th><th>Codigo</th><th>Descripcion</th><th>Razon</th><th>Precio</th><th>Cantidad</th><th>Importe</th><th>IEPS</th><th>IVA</th><th>Total</th></tr></thead><tbody>' + detailItems + '</tbody></table>' + info + btn).removeClass('loading');

            var Quantity = 0;
            var Ieps = 0;
            var Iva = 0;
            var Subtotal = 0;
            var Total = 0;
            var id_single = 0;

            $('input[type=checkbox]').iCheck({
                checkboxClass: 'icheckbox_square-green',
            }).on('ifClicked', function () {
                $(this).trigger("change");

                var item = this;
                var check = item.checked;
                var id_single = item.attributes.idsingle.value;
                var PartNumber = item.attributes.partnumber.value;
                var quantity = parseFloat(item.attributes.quantity.value);
                var ieps = parseFloat(item.attributes.ieps.value);
                var iva = parseFloat(item.attributes.iva.value);
                var subtotal = parseFloat(item.attributes.subtotal.value);
                var total = parseFloat(item.attributes.amount.value);

                if (!check) {
                    Quantity += quantity;
                    Ieps += ieps;
                    Iva += iva;
                    Subtotal += subtotal;
                    Total += total;
                    setValues(Quantity, Ieps, Iva, Subtotal, Total);
                    list.push({ PartNumber: PartNumber, id_single: id_single, Quantity: quantity })
                }
                else {
                    Quantity -= quantity;
                    Ieps -= ieps;
                    Iva -= iva;
                    Subtotal -= subtotal;
                    Total -= total;
                    setValues(Quantity, Ieps, Iva, Subtotal, Total);
                    list = list.filter(x => x.id_single != id_single)
                }
            });

            reloadStyleTable();
        }
    });
    return tabledetail;
}

function reloadStyleTable() {
    $('#tabledetail').DataTable({
        oLanguage:
        {
            "sProcessing": "Procesando...",
            "sLengthMenu": "Mostrar _MENU_ registros",
            "sZeroRecords": "No se encontraron resultados",
            "sEmptyTable": "Ningún dato disponible en esta tabla",
            "sInfo": "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
            "sInfoEmpty": "Mostrando registros del 0 al 0 de un total de 0 registros",
            "sInfoFiltered": "(filtrado de un total de _MAX_ registros)", "sInfoPostFix": "",
            "sSearch": "Buscar:", "sUrl": "", "sInfoThousands": ",", "sLoadingRecords": "Cargando...",
            "oPaginate": { "sFirst": "Primero", "sLast": "Último", "sNext": "Siguiente", "sPrevious": "Anterior" },
            "oAria":
            {
                "sSortAscending": ": Activar para ordenar la columna de manera ascendente",
                "sSortDescending": ": Activar para ordenar la columna de manera descendente"
            }
        },
    });
}

function SearchDocuments() {
    if ($('#dateInit').val() != "" && $('#dateFin').val() != "") {
        if (moment($('#dateFin').val()) < moment($('#dateInit').val())) {
            toastr.error("Fecha final no puede ser menor a la inicial.");
            $('#dateFin').focus();
        }
        else {
            StartLoading();
            $.ajax({
                url: "/DamagedsGoods/ActionGetDocuments",
                type: "GET",
                data: { "dateInicial": $('#dateInit').val(), "dateFinal": $('#dateFin').val() },
                success: function (response) {
                    if (response.success) {
                        table.clear().draw();
                        table.rows.add(response.Orders)
                        table.columns.adjust().draw();
                        EndLoading();
                    }
                    else {
                        EndLoading();
                        if (response.responseText === 'Termino tu sesión.')
                            SessionFalse(response.responseText);
                        else {
                            toastr.warning(response.responseText);
                            table.clear().draw();
                        }
                    }
                },
                error: function () {
                    EndLoading();
                    toastr.error('Error inesperado contactar a sistemas.');
                }
            });
        }
    }
    else if (($('#dateInit').val() == "" && $('#dateFin').val() != "") || ($('#dateInit').val() != "" && $('#dateFin').val() == ""))
        return false;
}

function CancelMessage(document) {
    swal({
        title: "¿Esta seguro que desea Rechazar?",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "Continuar",
        cancelButtonText: "Cancelar"
    }, function (isConfirm) {
        if (isConfirm) { Rechazar(document); }
    });
}

function AprovalMessage(Document) {
    var check = false;
    if (list.length > 0)
        swal({
            title: "¿Esta seguro que desea Aprobar?",
            type: "info",
            showCancelButton: true,
            confirmButtonColor: "#55dd6b",
            cancelButtonText: "Cancelar",
            confirmButtonText: "Continuar"
        }, function (isConfirm) {
            if (isConfirm)
                Aprobar(Document);
        });
    else
        toastr.warning("Seleccione al menos un producto.");
}

function Rechazar(document) {
    if (document != "") {
        StartLoading();
        $.ajax({
            url: "/DamagedsGoods/ActionAprobacionRechazoDamageGoods",
            type: "POST",
            data: { "document": document, "msj": "RECHAZADO" },
            success: function (response) {
                if (response.success == 1) {
                    swal({
                        title: 'Rechazado',
                        text: "Folio rechazado con exito.",
                        type: "success"
                    });
                    $('#dateInit').val("");
                    $('#dateFin').val("");
                    ReloadTable();
                }
                else if (response.success == false) {
                    EndLoading();
                    if (response.responseText === 'Termino tu sesión.')
                        SessionFalse(response.responseText);
                    else
                        toastr.warning(response.responseText);
                }
            },
            error: function () {
                EndLoading();
                toastr.error(' Error inesperado contactar a sistemas.');
            }
        });
    }
}

function ReloadTable() {
    StartLoading();
    $.ajax({
        url: "/DamagedsGoods/ActionGetDocuments",
        type: "GET",
        data: {
            "dateInicial": "",
            "dateFinal": ""
        },
        success: function (response) {
            if (response.success) {
                table.clear().draw();
                table.rows.add(response.Orders)
                table.columns.adjust().draw();
                EndLoading();
            }
            else {
                EndLoading();
                if (response.responseText === 'Termino tu sesión.')
                    SessionFalse(response.responseText);
                else {
                    toastr.warning(response.responseText);
                    table.clear().draw();
                }
            }
        },
        error: function () {
            EndLoading();
            toastr.error(' Error inesperado contactar a sistemas.');
        }
    });
}

function Aprobar(document) {
    if (document != "") {
        StartLoading();
        $.ajax({
            url: "/DamagedsGoods/ActionAprobacionRechazoDamageGoods",
            type: "POST",
            data: { "Model": list, "document": document, "msj": "APROBADO" },
            success: function (response) {
                if (response.success == 1) {
                    swal({
                        title: 'Aprobado',
                        text: "Folio Aprobado con exito.",
                        type: "success"
                    });
                    $('#dateInit').val("");
                    $('#dateFin').val("");
                    ReloadTable();
                }
                else if (response.success == false) {
                    EndLoading();
                    if (response.responseText === 'Termino tu sesión.')
                        SessionFalse(response.responseText);
                    else
                        toastr.warning(response.responseText);
                }
            },
            error: function () {
                EndLoading();
                toastr.error(' Error inesperado contactar a sistemas.');
            }
        });
    }
}