﻿var DriverList = [];
const SessionText = "Se termino su sesión";
$(document).ready(function () {
    window.setTimeout(function () { $("#fa").click(); });
    DriverList = JSON.parse($("#model").val());
    FullTableDriver(DriverList);
    $("#buttonEditInventory").hide();
    $("#checkboxActiveDriver").iCheck('check');
});

$('#buttonEditInventory').click(function () {
    ValidateFormDrive("¿Deseas editar ese chofer?", "", false);
});

$('#buttonSaveInventory').click(function () {
    ValidateFormDrive("¿Deseas agregar el chofer?", "Una vez agregado no podra borrarse solo desactivarse", true);
});

function MesajeEdit(text, textOption, value, userSpecs) {
    swal({
        title: text,
        text: textOption,
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "Si",
        cancelButtonText: "No",
        closeOnConfirm: true,
        closeOnCancel: true
    }, function (isConfirm) {
        if (isConfirm) {
            if (value)
                SaveDrive(userSpecs);
            else
                EditDrive(userSpecs);
        }
    });
}

var tableDriverSpec = $('#tableDriver').DataTable({
    oLanguage: {
        "sProcessing": "Procesando...", "sLengthMenu": "Mostrar _MENU_ registros", "sZeroRecords": "No se encontraron resultados", "sEmptyTable": "Ningún dato disponible en esta tabla", "sInfo": "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros", "sInfoEmpty": "Mostrando registros del 0 al 0 de un total de 0 registros", "sInfoFiltered": "(filtrado de un total de _MAX_ registros)", "sInfoPostFix": "", "sSearch": "Buscar:", "sUrl": "", "sInfoThousands": ",", "sLoadingRecords": "Cargando...", "oPaginate": { "sFirst": "Primero", "sLast": "Último", "sNext": "Siguiente", "sPrevious": "Anterior" }, "oAria": { "sSortAscending": ": Activar para ordenar la columna de manera ascendente", "sSortDescending": ": Activar para ordenar la columna de manera descendente" }
    },
    responsive: true,
    autoWidth: true,
    order: [[1, "asc"]],
    dom: "<'row'<'col-sm-4'l><'col-sm-4 text-left'B><'col-sm-4'f>t<'col-sm-6'i><'col-sm-6'p>>",
    lengthMenu: [[10, 25, 50, -1], [10, 25, 50, "Todos"]],
    buttons: [
        { extend: 'csv', text: 'Excel', title: 'Choferes', className: 'btn-sm' },
        {
            extend: 'pdf', text: 'PDF', title: 'Choferes', className: 'btn-sm', exportOptions: {
                columns: [1, 2, 3]
            },
            customize: function (doc) {
                doc.content[1].table.widths =
                    Array(doc.content[1].table.body[0].length + 1).join('*').split('');
            }
        },
        { extend: 'print', text: 'Imprimir', className: 'btn-sm' },
    ],
    columns: [
        { data: 'EmpNo', width: "1%" },
        { data: 'FullName', width: "1%" },
        { data: 'MobileTel', width: "1%" },
        { data: 'DriverFlag', width: "1%" },
        { data: null, width: "1%" }
    ],
    columnDefs: [{
        targets: [0, 1, 2, 3],
        width: "1%"
    },
    {
        targets: [3],
        render: function (data, type, full, meta) {
            if (data) {
                return `<div class="icheckbox_square-green
                                        checked disabled" style="position: relative;">
                                        <input type="checkbox" disabled="" style="position: absolute;
                                        opacity: 0;"><ins class="iCheck-helper" style="position: absolute; top: 0%;
                                        left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px;
                                        background: rgb(255, 255, 255); border: 0px; opacity: 0;"></ins></div>`
            }
            else
                return `<div class="icheckbox_square-green
                                        disabled" style="position: relative;"><input type="checkbox" disabled=""
                                        style="position: absolute; opacity: 0;"><ins class="iCheck-helper" style="position: absolute;
                                        top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px;
                                        background: rgb(255, 255, 255); border: 0px; opacity: 0;"></ins></div>`;
        },
        width: "1%"
    },
    {
        targets: [4],
        render: function (data, type, full, meta) {
            return `<button onclick="EditDriveOption('N_${data.EmpNo}')" class="btn btn-warning"><i class="fa fa-pencil"></i> Editar</button>`;
        },
        width: "1%"
    }]
});

function FullTableDriver(List) {
    tableDriverSpec.clear().draw();
    tableDriverSpec.rows.add(List);
    tableDriverSpec.columns.adjust().draw();
}

function ValidateFormDrive(text, textOption, option) {
    var userSpecs = FormUserSpecs();
    if (userSpecs.FullName.trim() == "")
        toastr.warning("Favor de ingresar un nombre");
    else if (userSpecs.MobileTel.trim() == "")
        toastr.warning("Favor de ingresar un número");
    else if (userSpecs.EmpNo == "")
        toastr.warning("Favor de ingresar un número de empleado");
    else if (DriverList.filter(element => element.EmpNo == userSpecs.EmpNo).length > 0 && option)
        toastr.warning("Favor de ingresar otro número de empleado");
    else if (DriverList.filter(element => element.EmpNo == userSpecs.EmpNo).length == 2 && !option)
        toastr.warning("Favor de ingresar otro número de empleado");
    else
        MesajeEdit(text, textOption, option, userSpecs);
}

function SaveDrive(userSpecs) {
    axios.post("/UserSpecs/ActionSaveUserSpeck", { UserSpecs: userSpecs })
        .then(data => {
            if (data.data == "SF") {
                setTimeout(function () {
                    SessionFalse(SessionText);
                }, 500);
            }
            else if (data.data) {
                toastr.success("Se guardó con exito");
                DriverList = [userSpecs, ...DriverList];
                FullTableDriver(DriverList)
            }
            else if (!data.data)
                toastr.warning("No pudo guardarse el chofer");
            else
                toastr.error("Error desconosido");
        })
        .catch(error => {
            toastr.error("Favor de contactar a sistemas");
        })
        .then(() => { ClearForm() })
}

function EditDrive(userSpecs) {
    axios.post("/UserSpecs/ActionEditUserSpeck", { UserSpecs: userSpecs })
        .then(data => {
            if (data.data == "SF") {
                setTimeout(function () {
                    SessionFalse(SessionText);
                }, 500);
            }
            else if (data.data) {
                toastr.success("Se editó con exito");
                DriverList[DriverList.findIndex(element => element.EmpNo == userSpecs.EmpNo)] = userSpecs;
                FullTableDriver(DriverList)
            }
            else if (!data.data)
                toastr.warning("No pudo guardarse el chofer");
            else
                toastr.error("Error desconosido");
        })
        .catch(error => {
            toastr.error("Favor de contactar a sistemas");
        })
        .then(() => { ClearForm() })
}

function EditDriveOption(EmpNo) {
    var userSpecs = DriverList.filter(element => element.EmpNo == EmpNo.split("N_")[1])[0];
    $("#inputNumberEmployee").val(userSpecs.EmpNo);
    $("#inputFullName").val(userSpecs.FullName);
    $("#inputMobilNumber").val(userSpecs.MobileTel);
    $("#inputNumberEmployee").prop("disabled", true);
    $("#inputFullName").prop("disabled", true);
    $("#buttonEditInventory").show();
    $("#buttonSaveInventory").hide();
    if (userSpecs.DriverFlag)
        $("#checkboxActiveDriver").iCheck('check');
    else
        $("#checkboxActiveDriver").iCheck('uncheck');
}

function ValidatePhone() {
    var Phone = $("#inputMobilNumber").val();
    if (!/^([0-9])*$/.test(Phone)) {
        toastr.warning("Favor de poner solo numeros");
        $("#inputMobilNumber").val("");
        $('#Phone').focus();
    }
}

function MaxInputNumber() {
    var numberEmployee = $("#inputNumberEmployee").val();
    if (numberEmployee != "") {
        if (numberEmployee.length > 10) {
            $("#inputNumberEmployee").val("");
            toast.warning("Ingrese un número de empleado correcto.");
        }
    }
}

function FormUserSpecs() {
    var numberEmployee = $("#inputNumberEmployee").val();
    var fullName = $("#inputFullName").val();
    var mobilNumber = $("#inputMobilNumber").val();
    var flagDriver = $("#checkboxActiveDriver").is(":checked");
    return { EmpNo: numberEmployee, FullName: fullName, MobileTel: mobilNumber, DriverFlag: flagDriver };
}

function ClearForm() {
    $("#inputNumberEmployee").val("");
    $("#inputFullName").val("");
    $("#inputMobilNumber").val("");
    $("#buttonEditInventory").hide();
    $("#buttonSaveInventory").show();
    $("#checkboxActiveDriver").iCheck('check');
    $("#inputNumberEmployee").prop("disabled", false);
    $("#inputFullName").prop("disabled", false);
}