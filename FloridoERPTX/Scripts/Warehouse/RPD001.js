﻿$("#ItemsDrop").select2({
    minimumInputLength: 3,
    allowClear: true,
    placeholder: "Seleccione un producto",
    initSelection: function (element, callback) {
        callback({ id: "", text: "Seleccione una opcion" });
    },
    ajax: {
        url: "/Items/ActionItemBarcodeComboPresentation/",
        dataType: 'json',
        type: "GET",
        quietMillis: 50,
        data: function (Filter) {
            return {
                Filter: Filter
            };
        },
        results: function (data) {
            return {
                results: $.map(data.Json, function (item) {
                    return {
                        text: `${item.PathNumber} - ${item.Description}`,
                        id: item.PathNumber
                    }
                }),
            };
        }
    }
});

$(document).ready(function () {
    window.setTimeout(function () { $("#fa").click(); }, 1000);
});

$("#ValidDate1").datepicker({
    autoclose: true,
    todayHighlight: true,
    showButtonPanel: true,
});

$("#ValidDate2").datepicker({
    autoclose: true,
    startDate: '01/01/1950',
    todayHighlight: true,
});

list = [];
listMovements = [];
var table = $('#TableProductsMovement').DataTable({
    oLanguage:
    {
        "sProcessing": "Procesando...",
        "sLengthMenu": "Mostrar _MENU_ registros",
        "sZeroRecords": "No se encontraron resultados",
        "sEmptyTable": "Ningún dato disponible en esta tabla",
        "sInfo": "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
        "sInfoEmpty": "Mostrando registros del 0 al 0 de un total de 0 registros",
        "sInfoFiltered": "(filtrado de un total de _MAX_ registros)",
        "sInfoPostFix": "",
        "sSearch": "Buscar:",
        "sUrl": "",
        "sInfoThousands": ",",
        "sLoadingRecords": "Cargando...",
        "oPaginate": { "sFirst": "Primero", "sLast": "Último", "sNext": "Siguiente", "sPrevious": "Anterior" },
        "oAria": { "sSortAscending": ": Activar para ordenar la columna de manera ascendente", "sSortDescending": ": Activar para ordenar la columna de manera descendente" }
    },
    "ordering": false,
    dom: "<'row'<'col-sm-4'l><'col-sm-4 text-left'B><'col-sm-4'f>t<'col-sm-6'i><'col-sm-6'p>>",
    lengthMenu: [[10, 25, 50, -1], [10, 25, 50, "Todos"]],
    buttons: [
        { extend: 'csv', text: 'Excel', title: 'KardexDeProductos', className: 'btn-sm' },
        {
            extend: 'pdf', text: 'PDF', title: 'KardexDeProductos', className: 'btn-sm', exportOptions: {
                columns: [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12]
            },
            customize: function (doc) {
                doc.content[1].table.widths =
                    Array(doc.content[1].table.body[0].length + 1).join('*').split('');
            }
        },
        { extend: 'print', text: 'Imprimir', className: 'btn-sm' },
    ],
    columns:
        [
            { data: null },
            { data: 'MovementDate' },
            { data: 'MovementDocument' },
            { data: 'MovementReference' },
            { data: 'MovementReferenceType' },
            { data: 'DocumentType' },
            { data: 'MovementType' },
            { data: 'StockType' },
            { data: 'Currency' },
            { data: 'StorageLocationStock' },
            { data: 'DebitCredit' },
            { data: 'MovementQuantity' },
            { data: 'TotalStock' },
        ],
    columnDefs:
        [
            {
                targets: [0],
                orderable: false,
                data: null,
                width: "1%",
                defaultContent: '<button class="btn btn-xs btn-outline btn-info" id="btnPrint"><i class="fa fa-print"></i> Imprimir</button>'
            },
            {
                targets: [5, 6, 8],
                visible: false,
                searchable: false
            }
        ]
});

$('#TableProductsMovement tbody').on('click', '#btnPrint', function () {
    var values = table.row($(this).parents('tr')).data();
    values = $.map(values, function (value, index) {
        switch (index) {
            case 'MovementDocument':
                PrintReference(value);
                break;
        }
    });
});

function PrintReference(reference) {
    type = list.filter(x => x.movement == reference)[0];
    StartLoading();
    $.ajax({
        url: "/Items/ActionGetReportByReference",
        type: "POST",
        data: { "referenceNumber": type.reference, "type": type.type  , "document_no": type.movement},
        success: function (response) {
            EndLoading();
            if (response.responseText == "Termino tu sesión.")
                SessionFalse(response.responseText);
            else if (response.success) {
                $("#modalReferenceBody").html("<iframe width='100%' height='550px' src='data:application/pdf;base64, " + encodeURI(response.responseText) + "'></iframe>")
                $("#modalReference").modal('show');
            }
            else
                toastr.error(response.responseText);
        },
        error: function () {
            EndLoading();
            toastr.error('Error inesperado contactar a sistemas.');
        }
    });
}

$('#btnPrintMovements').on('click', function () {
    toastr.options =
        {
            "debug": false,
            "newestOnTop": false,
            "positionClass": "toast-top-center",
            "closeButton": true,
            "debug": false,
            "toastClass": "animated fadeInDown",
        };
    if (listMovements.length != 0) {
        StartLoading();
        rangoFecha = $("#ValidDate1").val() + " - " + $("#ValidDate2").val();
        var dataDrop = $("#ItemsDrop").select2('data');
        dataDropSplit = dataDrop.text.split("/");
        $.ajax({
            url: "/Items/ActionGenerateReportMovementsItem",
            type: "POST",
            data: { "items": listMovements, "part_number": $("#ItemsDrop").val(), "part_description": dataDropSplit[0].trim(), "dateRange": rangoFecha },
            success: function (response) {
                EndLoading();
                if (response.responseText == "Termino tu sesión.")
                    SessionFalse(response.responseText);
                else if (response.success) {
                    $("#modalReferenceBody").html("<iframe width='100%' height='550px' src='data:application/pdf;base64, " + encodeURI(response.responseText) + "'></iframe>")
                    $("#modalReference").modal('show');
                }
                else
                    toastr.error(response.responseText);
            },
            error: function () {
                EndLoading();
                toastr.error('Error inesperado contactar a sistemas.');
            }
        });
    }
    else
        toastr.error('Sin movimientos para imprimir.');
});

$('#btnSearchMovements').on('click', function (r, e) {
    toastr.remove();
    toastr.options =
        {
            "debug": false,
            "newestOnTop": false,
            "positionClass": "toast-top-center",
            "closeButton": true,
            "debug": false,
            "toastClass": "animated fadeInDown",
        };
    if ($("#ValidDate1").val() == "") {
        toastr.error('Seleccione una fecha');
        $("#ValidDate1").val("");
        $("#ValidDate1").focus();
    }
    else if ($("#ValidDate2").val() == "") {
        toastr.error('Seleccione una fecha');
        $("#ValidDate2").val("");
        $("#ValidDate2").focus();
    }
    else if (moment($("#ValidDate2").val()) < moment($("#ValidDate1").val())) {
        toastr.error('Fecha Final no puede ser menor a Inicial');
        $("#ValidDate2").val("");
        $("#ValidDate2").focus();
    }
    else if ($("#ItemsDrop").val() == "") {
        toastr.error('Selecciona un Producto.');
        $('#ItemsDrop').select2("open");
    }
    else {
        StartLoading();
        $.ajax({
            url: "/Items/ActionGetAllMovementsByProduct",
            type: "POST",
            data: { "Date1": $("#ValidDate1").val(), "Date2": $("#ValidDate2").val(), "part_number": $("#ItemsDrop").val() },
            success: function (returndata) {
                EndLoading();
                if (returndata.responseText === 'Termino tu sesión.')
                    SessionFalse(returndata.responseText);
                else if (returndata.success) {
                    table.clear().draw();
                    table.rows.add(returndata.ItemMovements)
                    table.columns.adjust().draw();
                    items = 0;
                    listMovements = returndata.ItemMovements;
                    $.each(returndata.ItemMovements, function (index, value) {
                        list[items] = { movement: value.MovementDocument, reference: value.MovementReference, type: value.MovementReferenceType }
                        items++;
                    });
                    document.getElementById("btnPrintMovements").removeAttribute("disabled");
                }
                else {
                    toastr.warning(returndata.responseText);
                    table.clear().draw();
                    document.getElementById("btnPrintMovements").setAttribute("disabled", "true");
                }
            },
            error: function () {
                EndLoading();
                toastr.error('Error inesperado contactar a sistemas.');
            }
        });
    }
});