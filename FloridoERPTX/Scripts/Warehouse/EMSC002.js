﻿var formatmony = x => new Intl.NumberFormat("en-US", { style: "currency", currency: "USD", currencyDisplay: "symbol", maximumFractionDigits: 4 }).format(x)
var tableTd = x => `<td>${x}</td>`
var tabletr = x => `<tr>${tableTd(x.PartNumber)} ${tableTd(x.Description)} ${tableTd(x.Size)}  ${tableTd(formatmony(x.Price))} ${tableTd(x.Quantity)}   ${tableTd(formatmony(x.Import))}   ${tableTd(formatmony(x.Iva))}  ${tableTd(formatmony(x.Ieps))} ${tableTd(formatmony(x.Import + x.Iva + x.Ieps))}</tr>`
var selectorFull = x => `<option value='${x.Id}'> ${x.Id}, ${x.Type}</option>`;
var id = 0;
var listEntryFree = [];

$("#supplier").select2({
    minimumInputLength: 3,
    allowClear: true,
    placeholder: "Seleccione una producto",
    initSelection: function (element, callback) {
        callback({ id: "", text: "Seleccione una opcion" });
    },
    ajax: {
        url: "/Suppliers/ActionSearchSupplier/",
        dataType: 'json',
        type: "GET",
        quietMillis: 50,
        data: function (Filter) {
            return {
                Filter: Filter
            };
        },
        results: function (data) {
            return {
                results: $.map(data.Json, function (item) {
                    return {
                        text: `${item.BusinessName}`,
                        id: item.SupplierId
                    }
                }),
            };
        }
    }
});

function isEnableOption21(value) {
    if (value) {
        $("#option21").show();
        $("#tableEMSCContent").show();
    }
    else {
        $("#option21").hide();
        $("#tableEMSCContent").hide();
    }
}

$(document).ready(function () {
    var listEntryFree = JSON.parse($("#model").val());
    FillInventoryTable(listEntryFree);

    $('#datetimepicker').datepicker({
        autoclose: true,
        endDate: '01/01/2100',
        language: 'es',
        format: 'mm/dd/yyyy',
        todayHighlight: true,
    });

    $('#datetimepicker2').datepicker({
        autoclose: true,
        endDate: '01/01/2100',
        language: 'es',
        format: 'mm/dd/yyyy',
        todayHighlight: true,
    });

    $("#folio").select2();

    window.setTimeout(function () { $("#fa").click(); }, 1000);

    $('#buttonCancel').click(function () {
        if ($("#folio").val() > 0) {
            swal({
                title: "Desea Cancelar la entrada?",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Si",
                cancelButtonText: "No",
                closeOnConfirm: true,
                closeOnCancel: true
            }, function (isConfirm) {
                if (isConfirm)
                    Cancel();
            });
        }
    });

    isEnableOption21(false);
});

function SearchDate() {
    isEnableOption21(false);
    StartLoading();
    var supplier = $("#supplier").val() != "" ? $("#supplier").val() : 0;
    var dateInit = $("#dateInit").val();
    var dateFin = $("#dateFin").val();
    var status = $("#status").val();
    $.ajax({
        url: "/EntryFree/ActionGetEntryFreeSearch",
        type: "GET",
        data: { "supplierId": supplier, "fechaIni": dateInit, "fechaFin": dateFin, "state": status, "deparment": "", "family": "", "currency": "", "part_number": "" },
        success: function (response) {
            EndLoading();
            debugger;
            if (response.listEntryFree.length > 0)
                FillInventoryTable(response.listEntryFree);
            else {
                toastr.warning("No se encontro ninguna entrada");
                TableInventoryDifferent.clear();                
                TableInventoryDifferent.columns.adjust().draw();
            }
        },
        error: function () {
            EndLoading();
            FillDropdown("folio", []);
            toastr.error(' Error inesperado contactar a sistemas.');
        }
    });
}

function FillInventoryTable(value) {
    TableInventoryDifferent.clear();
    TableInventoryDifferent.rows.add(value);
    TableInventoryDifferent.columns.adjust().draw();
}

var TableInventoryDifferent = $('#entryFreeTable').DataTable({
    autoWidth: false,
    responsive: true,
    oLanguage: {
        "sProcessing": "Procesando...", "sLengthMenu": "Mostrar _MENU_ registros", "sZeroRecords": "No se encontraron resultados", "sEmptyTable": "Ningún dato disponible en esta tabla", "sInfo": "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros", "sInfoEmpty": "Mostrando registros del 0 al 0 de un total de 0 registros", "sInfoFiltered": "(filtrado de un total de _MAX_ registros)", "sInfoPostFix": "", "sSearch": "Buscar:", "sUrl": "", "sInfoThousands": ",", "sLoadingRecords": "Cargando...", "oPaginate": { "sFirst": "Primero", "sLast": "Último", "sNext": "Siguiente", "sPrevious": "Anterior" }, "oAria": { "sSortAscending": ": Activar para ordenar la columna de manera ascendente", "sSortDescending": ": Activar para ordenar la columna de manera descendente" }
    },
    columnDefs: [
        {
            targets: [0, 1, 2, 3, 4, 5, 6, 7, 9],
            width: "1%"
        },
        {
            targets: [10, 11],
            visible: false,
            searchable: false,
            width: "1%"
        },
        {
            targets: [8],
            render: function (data, type, full, meta) {
                if (data == 1) {
                    return "Iniciado"
                }
                else if (data == 9) {
                    return "Finalizado"
                }
                else if (data == 8) {
                    return "Cancelado"
                }
                else {
                    return "Desconosido"
                }
            },
            width: "1%"
        }],
    dom: "<'row'<'col-sm-4'l><'col-sm-4 text-left'B><'col-sm-4'f>t<'col-sm-6'i><'col-sm-6'p>>",
    lengthMenu: [[10, 25, 50, -1], [10, 25, 50, "Todos"]],
    buttons: [],
    columns:
        [
            {
                "class": "details-control",
                "orderable": false,
                "data": null,
                "defaultContent": "",
                "width": "1%"
            },
            { data: 'Supplier', width: "1%" },
            { data: 'Type', width: "1%" },
            { data: 'SubType', width: "1%" },
            { data: 'Date', width: "1%" },
            { data: 'Commentary', width: "1%" },
            { data: 'Reference', width: "1%" },
            { data: 'User', width: "1%" },
            { data: 'Status', width: "1%" },
            { data: 'Site', width: "1%" },
            { data: 'Id', width: "1%" },
            { data: 'SupplierId', width: "1%" },
            {
                data: null,
                width: "1%",
                targets: 8,
                defaultContent: '<button class="btn btn-xs btn-outline btn-info btnReport"><i class="fa fa-file-pdf-o"></i> Reporte</button>'
            }
        ],
});

var detailRows = [];

$('#entryFreeTable tbody').on('click', 'tr td.details-control', function () {
    var tr = $(this).closest('tr');
    var row = TableInventoryDifferent.row(tr);

    if (row.child.isShown()) {
        row.child.hide();
        tr.removeClass('details');
    }
    else {
        if (TableInventoryDifferent.row('.details').length) {
            $('.details-control', TableInventoryDifferent.row('.details').node()).click();
        }
        row.child(format(row.data())).show();
        tr.addClass('details');
    }
});

TableInventoryDifferent.on('draw', function () {
    $.each(detailRows, function (i, id) {
        $('#' + id + ' td.details-control').trigger('click');
    });
});

const fomartmoney = money => decimal => new Intl.NumberFormat("en-US", { style: "currency", currency: "USD", currencyDisplay: "symbol", maximumFractionDigits: decimal }).format(money);
trTotal = element => `${fomartmoney(element.Iva + element.Ieps + element.Import)(4)}`
FullTd = element => Option => `<td>${Option == 1 ? fomartmoney(element)(4) : Option == 0 ? element : element != null && element.trim() != "" ? "Contado" : "No contado"}</td >`
FullTh = element => `<th>${element}</th>`
FullTable = element => array => array.map(title => FullTd(element[title.title])(title.option)).toString();
FullTableHeader = array => `<tr>${array.map(title => FullTh(title))}</tr>`
FullButton = (status, date, id) => `<div class="form-group col-md-12"><div class="row">${ValidationButton(status, date, id)} ${ButtonPrint(id)}</div ></div >`;
ValidationButton = (status, date, id) => {
    if (status == 9 && (moment() <= moment(date).add(30, 'days'))) {
        return `<button class="btn btn-danger" onclick = "Cancel(${id})"><i class='fa fa-close'></i> Cancelar</button>`
    }
    return ``;
}
ButtonPrint = id => {
    return `<button class="btn btn-info" onclick = "Print(${id})"><i class='fa fa-print'></i> Imprimir</button>`
}

$('#entryFreeTable tbody').on('click', 'button.btnReport', function () {
    var tr = $(this).closest('tr');
    var row = TableInventoryDifferent.row(tr);
    model = row.data();
    StartLoading();
    $.post({
        url: '/EntryFree/ActionNewFolioEntryFree',
        Type: 'POST',
        data: { "EntryFreeId": model.Id },
        success: function (data) {
            EndLoading();
            if (data == "SF")
                SessionFalse("Se termino su sesión");
            else if (data == "N/A")
                toastr.warning('Folio cancelado o inexistente');
            else if (data.success) {
                $("#myModal8").modal('show');
                id = $("#folio").val();
                $("#iframes").html(`<iframe src='data:application/pdf;base64,${data.responseText}' height='320px' width='100%'></iframe>`);
            }
            else
                toastr.error('Alerta - Error inesperado  contactar a sistemas.');
        },
        error: function () {
            toastr.error('Alerta - Error inesperado  contactar a sistemas.');
        }
    }).then(() => { EndLoading() });
});

function Print(id) {
    if (id > 0) {
        StartLoading();
        axios.get("/EntryFree/ActionNewFolioEntryFree?EntryFreeId=" + id).then(function (data) {
            if (data.data == "SF")
                SessionFalse("Se termino su sesión");
            else if (data.data == "N/A")
                toastr.warning('Folio cancelado o inexistente');
            else if (data.data.success) {
                $("#myModal8").modal('show');
                id = $("#folio").val();
                $("#iframes").html(`<iframe src='data:application/pdf;base64,${data.data.responseText}' height='320px' width='100%'></iframe>`);
            }
            else
                toastr.error('Alerta - Error inesperado  contactar a sistemas.');
        }).catch(function (error) {
            toastr.error('Alerta - Error inesperado  contactar a sistemas.');
        }).then(function (data) {
            EndLoading();
        })
    }
    else
        toastr.warning('Selecciona una opcion');
}

function format(d) {
    var detailItems = "";
    var tabledetail = $('<div/>').addClass('loading').text('Cargando Datos...');
    $.ajax({
        url: "/EntryFree/ActionGetEntryFreeReport",
        data: { "EntryFreeId": d.Id, "SupplierId": d.SupplierId },
        type: "GET",
        success: function (data) {
            if (data == "SF")
                SessionFalse("Se terminó su sesión");
            else {
                detailItems = data.map(value => ` <tr>${FullTable(value)([
                    { title: "PartNumber", option: 0 },
                    { title: "Description", option: 0 },
                    { title: "Size", option: 0 },
                    { title: "Quantity", option: 0 },
                    { title: "Price", option: 1 },
                    { title: "Import", option: 1 },
                    { title: "Iva", option: 1 },
                    { title: "Ieps", option: 1 },
                ])}<td>${trTotal(value)}</td></tr>`).join().replace(/>,</g, '><').replace(/>, </g, '><')
                header = FullTableHeader(["Codigo", "Descripcion", "Locacion", "Contado", "Precio", "Importe", "Iva", "Ieps", "Total"]).replace(/,/g, '')
                var Html = (`<table id="tabledetail" class="table table-striped table-bordered table-hover" style="width:100%"><thead>
                                   ${header}
                                    </thead><tbody>${detailItems.length > 0 ? detailItems : "<td colspan='8' style='text-align: center;'>No cuenta con productos</td>"}</tbody></table>`)
                tabledetail.html(Html).removeClass('loading');
            }
        }
    });
    return tabledetail;
}

function Cancel(id) {
    if (id > 0) {
        axios.get(`/EntryFree/ActionNewFolioCancelEntryFree/?EntryFreeId=${id}`).then(function (data) {
            if (data.data == "SF")
                SessionFalse("Se termino su sesión");
            else if (data.data == "N/A")
                toastr.warning('Folio no existente');
            else if (data.data == 801)
                toastr.warning("El usuario no cuenta con permisos para realizar esta accion");
            else if (data.data) {
                toastr.success('Folio cancelado con exito');
                SearchDate();
            }
            else if (!data.data)
                toastr.warning('Folio cancelado previamente o inexistente');
            else
                toastr.error('Alerta - Error inesperado  contactar a sistemas.');
        }).catch(function (error) {
            toastr.error('Alerta - Error inesperado  contactar a sistemas.');
        })
    }
    else
        toastr.warning('El folio no puede ser nulo');
}