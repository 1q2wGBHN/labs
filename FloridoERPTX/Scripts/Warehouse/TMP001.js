﻿
//Obtencion de datos del viewModel
$(document).ready(function () {
    window.setTimeout(function () {
        $("#fa").click();
    }, 1000);
});

$("#ValidDate1").datepicker({
    autoclose: true,
    todayHighlight: true,
    showButtonPanel: true,
}).on("changeDate", function (e) {
    SearchTransfer();
});

$("#ValidDate2").datepicker({
    autoclose: true,
    todayHighlight: true,
}).on("changeDate", function (e) {
    SearchTransfer();
});

//Tabla principal
var table = $('#TableTransfers').DataTable({
    oLanguage: {
        "sProcessing": "Procesando...", "sLengthMenu": "Mostrar _MENU_ registros", "sZeroRecords": "No se encontraron resultados", "sEmptyTable": "Ningún dato disponible en esta tabla", "sInfo": "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros", "sInfoEmpty": "Mostrando registros del 0 al 0 de un total de 0 registros", "sInfoFiltered": "(filtrado de un total de _MAX_ registros)", "sInfoPostFix": "", "sSearch": "Buscar:", "sUrl": "", "sInfoThousands": ",", "sLoadingRecords": "Cargando...", "oPaginate": { "sFirst": "Primero", "sLast": "Último", "sNext": "Siguiente", "sPrevious": "Anterior" }, "oAria": { "sSortAscending": ": Activar para ordenar la columna de manera ascendente", "sSortDescending": ": Activar para ordenar la columna de manera descendente" }
    },
    responsive: true,
    autoWidth: true,
    order: [[1, "asc"]],
    dom: "<'row'<'col-sm-4'l><'col-sm-4 text-left'B><'col-sm-4'f>t<'col-sm-6'i><'col-sm-6'p>>",
    lengthMenu: [[10, 25, 50, -1], [10, 25, 50, "Todos"]],
    buttons: [
        { extend: 'csv', text: 'Excel', title: 'Materia Prima', className: 'btn-sm' },
        {
            extend: 'pdf', text: 'PDF', title: 'Materia Prima', className: 'btn-sm', exportOptions: {
                columns: [1, 2, 3]
            },
            customize: function (doc) {
                doc.content[1].table.widths =
                    Array(doc.content[1].table.body[0].length + 1).join('*').split('');
            }
        },
        { extend: 'print', text: 'Imprimir', className: 'btn-sm' },
    ],
    columns: [
        {
            "class": "details-control",
            "orderable": false,
            "data": null,
            "defaultContent": "",
            width: "1%"
        },
        { data: 'Folio', width: "1%" },
        { data: 'SiteName', width: "1%" },
        { data: 'TransferDate', width: "1%" },
        { data: 'Cuser', width: "1%" },
        { data: 'StorageLocation', width: "1%" }
    ],
    columnDefs: [{
        targets: [0],
        width: "1%"
    },
    {
        targets: [1],
        width: "1%"
    },
    {
        targets: [2],
        width: "1%"
    },
    {
        targets: [3],
        width: "1%"
    },
    {
        targets: [4],
        width: "1%"
    }]
});

var model = JSON.parse($("#model").val());
if (!$.isEmptyObject(model)) {
    table.clear();
    table.rows.add($(model));
    table.columns.adjust().draw();
    $('#TableDiv').animatePanel();
}
else {
    table.clear();
    table.columns.adjust().draw();
    $('#TableDiv').animatePanel();
}

var detailRows = [];
$('#TableTransfers tbody').on('click', 'tr td.details-control', function () {
    var tr = $(this).closest('tr');
    var row = table.row(tr);

    if (row.child.isShown()) {
        row.child.hide();
        tr.removeClass('details');
    }
    else {
        if (table.row('.details').length) {
            $('.details-control', table.row('.details').node()).click();
        }
        row.child(format(row.data())).show();
        tr.addClass('details');
    }
});

table.on('draw', function () {
    $.each(detailRows, function (i, id) {
        $('#' + id + ' td.details-control').trigger('click');
    });
});

var list = [];
//Tabla detalles
function format(d) {
    list = [];
    var detailItems = "";
    var tabledetail = $('<div/>').addClass('loading').text('Cargando Datos...');
    $.ajax({
        url: "/RawMaterial/ActionGetAllRawMaterialDetail/",
        data: { "folio": d.Folio },
        type: "GET",
        success: function (data) {
            if (data == "SF")
                SessionFalse("Terminó tu sesión");
            else {
                $.each(data, function (index, value) {
                    detailItems += "<tr><td class='text-center'><div class='checkbox'><label><input type='checkbox'  PartNumber = " + value.Part_Number + " class='i-checks' /></label></div></td><td>" + value.Part_Number + "</td><td>" + value.Description + "</td><td>" + value.Quantity + "</td><td>$" + value.UnitCost + "</td><td>$" + value.Iva + "</td><td>$" + value.IEPS + "</td><td>$" + value.SubTotal + "</td><td>$" + value.Amount + "</td>";
                });
                var Apro = '';
                if (data.length != 0) {
                    Apro = '<div class="form-group col-md-12"> <div class="row"> <label class="col-md-12 control-label">Comentario</label> </div> <div class="row"> <div class="col-md-10"> <textarea id="Comment" maxlenght="100" class="form-control" maxlength="100"></textarea> </div> <div class="pull-right"><button id="btn-save" type="button" folio="' + d.Folio + '" class="btn btn-sm btn-success"><i class="fa fa-save"></i> Aprobar</button>  <button id="btn-cancel" type="button" folio="' + d.Folio + '" class="btn btn-sm btn-danger"><i class="fa fa-close"></i> Cancelar</button></div></div></div>';
                    tabledetail.html('<table id="tabledetail" class="table table-striped table-bordered table-hover" style="width:100%"><thead><tr><th></th><th>Codigo</th><th>Descripcion</th><th>Cantidad</th><th>Costo unitario</th><th>IVA</th><th>IEPS</th><th>SubTotal</th><th>Total</th></tr></thead><tbody>' + detailItems + '</tbody></table>' + Apro).removeClass('loading');
                    reloadStyleTable();
                }
                tabledetail.html('<table id="tabledetail" class="table table-striped table-bordered table-hover" style="width:100%"><thead><tr><th></th><th>Codigo</th><th>Descripcion</th><th>Cantidad</th><th>Costo Unitario</th><th>IVA</th><th>IEPS</th><th>SubTotal</th><th>Total</th></tr></thead><tbody>' + detailItems + '</tbody></table>' + Apro).removeClass('loading');

                $('input[type=checkbox]').iCheck({
                    checkboxClass: 'icheckbox_square-green',
                }).on('ifClicked', function () {
                    $(this).trigger("change");
                    var item = this;
                    var check = item.checked;
                    var PartNumber = item.attributes.partnumber.value;
                    if (!check) {
                        list.push({ Part_Number: PartNumber })
                    } else {
                        list = list.filter(x => x.Part_Number != PartNumber);
                    }
                });

                reloadStyleTable();
            }

            $(".btn-success").on("click", function () {
                Aprobar($(this).attr("folio"));
            });

            $(".btn-danger").on("click", function () {
                Cancelar($(this).attr("folio"));
            });
        }
    });
    return tabledetail;
}

//Buscar folio por fecha
function SearchTransfer() {
    if ($('#ValidDate1').val() != "" & $('#ValidDate2').val() != "") {
        if (moment($('#ValidDate1').val()) <= moment($('#ValidDate2').val())) {
            $.ajax({
                type: "GET",
                url: "/RawMaterial/ActionGetAllRawMaterialHeader/",
                data: { "DateInit": $('#ValidDate1').val(), "DateFin": $('#ValidDate2').val(), "Status": "0" },
                success: function (returndate) {
                    if (returndate.data == "SF")
                        SessionFalse("Terminó tu sesión");
                    else if (returndate) {
                        if (returndate.length == 0) {
                            toastr.warning('No se encontraron traspasos de materia prima.');
                            table.clear();
                            table.columns.adjust().draw();
                            $('#TableDiv').animatePanel();
                        }
                        else {
                            table.clear();
                            table.rows.add($(returndate));
                            table.columns.adjust().draw();
                            $('#TableDiv').animatePanel();
                        }
                    }
                },
                error: function (returndate) {
                    toastr.error('Alerta - Error inesperado contactar a sistemas.');
                }
            });
        }
        else {
            toastr.error("Fecha final no puede ser menor a la inicial.");
            document.getElementById('ValidDate1').value = '';
            document.getElementById('ValidDate2').value = '';
            $('#ValidDate1').focus();
        }
    }
}

//Recargar tabla
function reloadStyleTable() {
    $('#tabledetail').DataTable({
        oLanguage: {
            "sProcessing": "Procesando...", "sLengthMenu": "Mostrar _MENU_ registros", "sZeroRecords": "No se encontraron resultados", "sEmptyTable": "Ningún dato disponible en esta tabla", "sInfo": "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros", "sInfoEmpty": "Mostrando registros del 0 al 0 de un total de 0 registros", "sInfoFiltered": "(filtrado de un total de _MAX_ registros)", "sInfoPostFix": "", "sSearch": "Buscar:", "sUrl": "", "sInfoThousands": ",", "sLoadingRecords": "Cargando...", "oPaginate": { "sFirst": "Primero", "sLast": "Último", "sNext": "Siguiente", "sPrevious": "Anterior" }, "oAria": { "sSortAscending": ": Activar para ordenar la columna de manera ascendente", "sSortDescending": ": Activar para ordenar la columna de manera descendente" }
        },
        columnDefs: [{
            targets: [2],
            width: "1%"
        }]
    });
}

//Alerta para aprobar (boton)
function Aprobar(Folio) {
    if (list.length > 0)
        swal({
            title: "¿Esta seguro que desea aprobar el traspaso?",
            text: "El no. de folio : " + Folio + " será aprobado",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Continuar",
            cancelButtonText: "Cancelar"
        }, function (isConfirm) {
            if (isConfirm)
                AprovalTransfer(list, Folio);
        });
    else
        toastr.warning("Seleccione al menos un producto.");
}

//Alerta para cancelar (boton)
function Cancelar(Folio) {
    var comment = $('#Comment').val();
    if (comment.trim().length >= 8) {
        swal({
            title: "¿Esta seguro que desea Cancelar la transferencia?",
            text: "El no. de transferencia : " + Folio + " será cancelado",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Continuar",
            cancelButtonText: "Cancelar"
        }, function (isConfirm) {
            if (isConfirm)
                AprovalCancel(Folio, comment);
        });
    }
    else
        swal('Debe de ingresar al menos un comentario de 8 caracteres.',
            'Ingrese un comentario  y vuelvalo a intentar.',
            'error');
}

//Aprobacion transfer
function AprovalTransfer(viewmodel, Folio) {
    var comment = $('#Comment').val();
    StartLoading();
    if (Folio) {
        axios.post("/RawMaterial/ActionRawMaterialEditStatusRA/", { model: viewmodel, folio: Folio, comment: comment }).then(function (result) {
            EndLoading();
            if (result.data.Status == "SF")
                SessionFalse("Se terminó su sesión");
            else if (result.data.Status == "Aprobado") {
                swal({
                    title: 'Aprobado',
                    text: "Folio aprobado con éxito.",
                    type: "success"
                });

                Update(result.data.Model);
            }
            else if (result.data.Status == "Error") {
                swal({
                    title: 'Error',
                    text: "Ha ocurrido un error.",
                    type: "warning"
                });
            }
        }).catch(function (error) {
            EndLoading();
            toastr.error('Alerta - Error inesperado  contactar a sistemas.');
        })
    }
    else {
        EndLoading();
        toastr.warning('El folio no puede ser nulo');
    }
}

//Aprobacion de cancelacion
function AprovalCancel(Folio, comment) {
    if (comment.trim().length >= 8) {
        if (Folio) {
            StartLoading();
            axios.post("/RawMaterial/ActionRawMaterialEditStatusRC/", { folio: Folio, comment: comment }).then(function (result) {
                EndLoading();
                if (result.data.Status == "SF")
                    SessionFalse("Se terminó su sesión");
                else if (result.data.Status == "Cancelado") {
                    swal({
                        title: 'Cancelado',
                        text: "Folio cancelado con éxito.",
                        type: "success"
                    });

                    Update(result.data.Model);
                }
                else if (result.data.Status == "Error")
                    swal({
                        title: 'Error',
                        text: "Ha ocurrido un error.",
                        type: "warning"
                    });
            }).catch(function (error) {
                EndLoading();
                toastr.error('Alerta - Error inesperado  contactar a sistemas.');
            })
        }
        else
            toastr.warning('El folio no puede ser nulo');
    } else
        swal('Debe de ingresar al menos un comentario de 8 caracteres.',
            'Ingrese un comentario  y vuelvalo a intentar.',
            'error');
}

function Update(Model) {
    if (!$.isEmptyObject(Model)) {
        table.clear();
        table.rows.add($(Model));
        table.columns.adjust().draw();
        $('#TableDiv').animatePanel();
        $("#ValidDate1").val("");
        $("#ValidDate2").val("");
    }
    else {
        table.clear();
        table.columns.adjust().draw();
        $('#TableDiv').animatePanel();
    }
}

function errors(value) {
    if (value == 6001)
        return "site code Incorrecto"
    else if (value == 6002)
        return "Estatus no aprobado"
    else
        return "desconosido"
}