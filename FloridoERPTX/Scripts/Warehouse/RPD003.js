﻿// Tabla Principal
const fomartmoney = money => decimal => new Intl.NumberFormat("en-US", { style: "currency", currency: "USD", currencyDisplay: "symbol", maximumFractionDigits: decimal }).format(money);
$("#tableItems").append('<tfoot><th>Total</th><th></th><th colspan = "6"></th></tfoot>');

var table = $('#tableItems').DataTable({
    oLanguage:
    {
        "sProcessing": "Procesando...",
        "sLengthMenu": "Mostrar _MENU_ registros",
        "sZeroRecords": "No se encontraron resultados",
        "sEmptyTable": "Ningún dato disponible en esta tabla",
        "sInfo": "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
        "sInfoEmpty": "Mostrando registros del 0 al 0 de un total de 0 registros",
        "sInfoFiltered": "(filtrado de un total de _MAX_ registros)",
        "sInfoPostFix": "",
        "sSearch": "Buscar:",
        "sUrl": "",
        "sInfoThousands": ",",
        "sLoadingRecords": "Cargando...",
        "oPaginate": { "sFirst": "Primero", "sLast": "Último", "sNext": "Siguiente", "sPrevious": "Anterior" },
        "oAria": { "sSortAscending": ": Activar para ordenar la columna de manera ascendente", "sSortDescending": ": Activar para ordenar la columna de manera descendente" }
    },
    dom: "<'row'<'col-sm-4'l><'col-sm-4 text-left'B><'col-sm-4'f>t<'col-sm-6'i><'col-sm-6'p>>",
    lengthMenu: [[10, 25, 50, -1], [10, 25, 50, "Todos"]],
    buttons: [
        { extend: 'csv', text: 'Excel', title: 'Dias de Inventario', className: 'btn-sm', footer: true },
        {
            extend: 'pdf', text: 'PDF', title: 'Dias de Inventario', className: 'btn-sm', exportOptions: {
                columns: [0, 1, 2, 3, 4, 5, 6,7,8]
            },
            customize: function (doc) {
                doc.content[1].table.widths =
                    Array(doc.content[1].table.body[0].length + 1).join('*').split('');
            }, footer: true
        },
        { extend: 'print', text: 'Imprimir', className: 'btn-sm', footer: true },],
    order: [[2, "asc"]],
    "footerCallback": function (row, data, start, end, display) {
        var api = this.api(), data;
        // converting to interger to find total
        var intVal = function (i) {
            return typeof i === 'string' ?
                i.replace(/[\$,]/g, '') * 1 :
                typeof i === 'number' ?
                    i : 0;
        };
        var monTotal = api
            .column(8)
            .data()
            .reduce(function (a, b) {
                return intVal(a) + intVal(b);
            }, 0);
        $(api.column(1).footer()).html(`<span ${monTotal == 0 ? "" : `style = "Color:${monTotal >= 0 ? "green" : "red"}"`} > ${fomartmoney(monTotal)(2)}</span >`);
    },
    columns:
        [
            { data: 'Part_number' },
            { data: 'Part_description' },
            { data: 'Days_avg' },
            { data: 'Avg_Sale' },
            { data: 'Last_Sale' },
            { data: 'Last_Purchase' },
            { data: 'Unrestricted' },
            { data: 'Storage_location' },
            { data: 'SubTotal' },
        ],
    columnDefs:
        [{
            targets: [0, 1, 2, 3, 4, 5, 6,7,8],
            width: "1%"
        },
        {
            render: function (data, type, row) {
                return '$' + data.toFixed(2);
            },
            targets: [8]
        }, {
            render: function (data, type, row) {
                return data.toFixed(2);
            },
            targets: [6]
        }
        ]
});

$("#supplier").select2({
    minimumInputLength: 3,
    allowClear: true,
    placeholder: "Seleccione un proveedor",
    initSelection: function (element, callback) {
        callback({ id: "", text: "Seleccione una opcion" });
    },
    ajax: {
        url: "/Suppliers/ActionSearchSupplier/",
        dataType: 'json',
        type: "GET",
        quietMillis: 50,
        data: function (Filter) {
            return {
                Filter: Filter
            };
        },
        results: function (data) {
            return {
                results: $.map(data.Json, function (item) {
                    return {
                        text: `${item.BusinessName}`,
                        id: item.SupplierId
                    }
                }),
            };
        }
    }
});

// Checkbox iCheck
$(document).ready(function () {
    window.setTimeout(function () { $("#fa").click(); }, 1000);

    $('#ckbox_excess').iCheck({
        checkboxClass: 'icheckbox_square-green',
    }).on('ifClicked', function () {
        $(this).trigger("click");
    });

    $("#supplier").select2({
        minimumInputLength: 2,
        formatInputTooShort: function () {
            return "Busque un proveedor";
        },
        matcher: function (term, text, option) {
            return text.toUpperCase().indexOf(term.toUpperCase()) >= 0 || option.val().toUpperCase().indexOf(term.toUpperCase()) >= 0;
        }
    });

    $("#depto").select2();
    $("#family").select2();
    check();
    $('#TableDiv').animatePanel();
    table.clear();
    table.rows.add([]);
    table.columns.adjust().draw();
});

function check() {
    if ($("#ckbox_excess").is(':checked')) {
        $("#idLblCheck").show();
        $("#idLblDays").show();
        $("#excessDays").val('15');
    } else if (!$("#ckbox_excess").is(':checked')) {
        $("#excessDays").val('0');
        $("#idLblCheck").hide();
        $("#idLblDays").hide();
    }
}

// Generar Reporte
function print() {
    StartLoading();
    days = $("#excessDays").val();
    proveedor = $("#supplier").val();
    depto = $("#depto").val();
    familia = $("#family").val();
    if (days == "") { days = "0"; }
    if (proveedor == "") { proveedor = "0"; }
    if (depto == "") { depto = "0"; }
    if (familia == "") { familia = "0"; }
    $.ajax({
        url: "/Items/ActionGenerateReportInventoryDaysReport",
        type: "POST",
        data: { "proveedor": proveedor, "depto": depto, "familia": familia, "excessDays": days },
        success: function (response) {
            EndLoading();
            if (response.responseText == "Terminó tu sesión.")
                SessionFalse(response.responseText);
            else if (response.success) {
                $("#modalReferenceBody").html("<iframe width='100%' height='550px' src='data:application/pdf;base64, " + encodeURI(response.responseText) + "'></iframe>");
                $("#modalReference").modal('show');
            }
            else
                toastr.error(response.responseText);
        },
        error: function () {
            EndLoading();
            toastr.error('Error inesperado contactar a sistemas.');
        }
    });
}

//Obtencion de familias
function GetFamily() {
    var deptoo = $("#depto").val();
    if (deptoo == "") {
        FillDropdown("family", null)
        return false;
    }
    $.ajax({
        url: "/MaClass/ActionGetFamily",
        type: "GET",
        data: { "deptoo": deptoo },
        success: function (response) {
            if (response.success) {
                listFamily = response.Json;
                FillDropdown("family", listFamily)
            }
            else
                FillDropdown("family", null);
        },
        error: function () {
            FillDropdown("family", null);
            toastr.error('Error inesperado contactar a sistemas.');
        }
    });
}

function FillDropdown(selector, vData) {
    if (vData != null) {
        $('#' + selector).trigger("change");
        var vItems = [];
        vItems.push('<option value="">Seleccione una familia</option>');
        for (var i in vData) {
            if (vData[i].Selected)
                vItems.push('<option selectedselected=selected value="' + vData[i].class_id + '">' + vData[i].description + '</option>');
            else
                vItems.push('<option value="' + vData[i].class_id + '">' + vData[i].description + '</option>');
        }
        $('#' + selector).empty();
        $('#' + selector).append(vItems.join(''));
        $('#' + selector).val("").trigger('change');
    }
    else {
        var vItems = [];
        vItems.push('<option  value="">Seleccione una familia</option>');
        $('#' + selector).empty();
        $('#' + selector).append(vItems.join(''));
        $('#' + selector).val("").trigger('change');
    }
}

// Proveedores
function GetSupplier() {
    if (deptoo == "") {
        FillDropdown("family", null);
        return false;
    }
    $.ajax({
        url: "/MaClass/ActionGetFamily",
        type: "GET",
        data: { "deptoo": deptoo },
        success: function (response) {
            if (response.success) {
                listFamily = response.Json;
                FillDropdown("family", listFamily);
            }
            else
                FillDropdown("family", null);
        },
        error: function () {
            FillDropdown("family", null);
            toastr.error('Error inesperado contactar a sistemas.');
        }
    });
}

// Buscar e imprimir en tabla
function search() {
    days = $("#excessDays").val();
    proveedor = $("#supplier").val();
    depto = $("#depto").val();
    familia = $("#family").val();
    if (days == "") { days = "0"; }
    if (proveedor == "") { proveedor = "0"; }
    if (depto == "") { depto = "0"; }
    if (familia == "") { familia = "0"; }
    StartLoading();
    $.ajax({
        url: "/Items/ActionGetInventoryDays",
        type: "POST",
        data: { "proveedor": proveedor, "depto": depto, "familia": familia, "excessDays": days },
        success: function (returndata) {
            EndLoading();
            if (returndata.responseText == "Terminó tu sesión.")
                SessionFalse(returndata.responseText);
            else if (returndata.success) {
                $('#TableDiv').animatePanel();
                table.clear();
                table.rows.add($(returndata.ItemMovements));
                table.columns.adjust().draw();
                if (returndata.ItemMovements.length == 0)
                    toastr.warning("No se encontraron productos");
            }
            else if (returndata.responseText === 'Termino tu sesión.')
                SessionFalse(returndata.responseText);
            else {
                toastr.warning(returndata.responseText);
                table.clear().draw();
                $("#button").attr('disabled', true);
            }
        }, error: function () {
            EndLoading();
            toastr.error('Error inesperado contactar a sistemas.');
        }
    });
}