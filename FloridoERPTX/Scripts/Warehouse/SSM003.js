﻿const fomartmoney = money => decimal => new Intl.NumberFormat("en-US", { style: "currency", currency: "USD", currencyDisplay: "symbol", maximumFractionDigits: decimal }).format(money);

$(document).ready(function () {
    window.setTimeout(function () { $("#fa").click(); }, 1000);

    $('#dateInit').datepicker({
        autoclose: true,
        endDate: '01/01/2100',
        language: 'es',
        format: 'mm/dd/yyyy',
        todayHighlight: true,
    }).on("changeDate", function (e) {
        SearchDocuments();
    });

    $('#dateFin').datepicker({
        autoclose: true,
        endDate: '01/01/2100',
        language: 'es',
        format: 'mm/dd/yyyy',
        todayHighlight: true,
    }).on("changeDate", function (e) {
        SearchDocuments();
    });

    $('#ckbox_excess').iCheck({
        checkboxClass: 'icheckbox_square-green',
    }).on('ifClicked', function () {
        $(this).trigger("click");
    });

    $("#status").select2();
    $("#department").select2();
});

$("#tableEMSCHead").append('<tfoot><th>Total</th><th colspan = "5"></th><th></th></tfoot>');

var table = $('#tableEMSCHead').DataTable({
    oLanguage:
    {
        "sProcessing": "Procesando...",
        "sLengthMenu": "Mostrar _MENU_ registros",
        "sZeroRecords": "No se encontraron resultados",
        "sEmptyTable": "Ningún dato disponible en esta tabla",
        "sInfo": "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
        "sInfoEmpty": "Mostrando registros del 0 al 0 de un total de 0 registros",
        "sInfoFiltered": "(filtrado de un total de _MAX_ registros)",
        "sInfoPostFix": "",
        "sSearch": "Buscar:",
        "sUrl": "",
        "sInfoThousands": ",",
        "sLoadingRecords": "Cargando...",
        "oPaginate": { "sFirst": "Primero", "sLast": "Último", "sNext": "Siguiente", "sPrevious": "Anterior" },
        "oAria": { "sSortAscending": ": Activar para ordenar la columna de manera ascendente", "sSortDescending": ": Activar para ordenar la columna de manera descendente" }
    },
    dom: "<'row'<'col-sm-4'l><'col-sm-4 text-left'B><'col-sm-4'f>t<'col-sm-6'i><'col-sm-6'p>>",
    lengthMenu: [[10, 25, 50, -1], [10, 25, 50, "Todos"]],
    buttons:
        [
            { extend: 'csv', text: 'Excel', title: 'Merma Contralada', className: 'btn-sm', footer: true },
            {
                extend: 'pdf', text: 'PDF', title: 'Merma Contralada', className: 'btn-sm',
                exportOptions: { columns: [1, 2, 3] },
                customize: function (doc) {
                    doc.content[1].table.widths = Array(doc.content[1].table.body[0].length + 1).join('*').split('');
                }, footer: true
            },
            { extend: 'print', text: 'Imprimir', className: 'btn-sm', footer: true },
        ],
    "footerCallback": function (row, data, start, end, display) {
        var api = this.api(), data;
        // converting to interger to find total
        var intVal = function (i) {
            return typeof i === 'string' ?
                i.replace(/[\$,]/g, '') * 1 :
                typeof i === 'number' ?
                    i : 0;
        };

        for (var i = 6; i < 7; i++) {
            var monTotal = api
                .column(i)
                .data()
                .reduce(function (a, b) {
                    return intVal(a) + intVal(b);
                }, 0);
            $(api.column(i).footer()).html(`<span ${monTotal == 0 ? "" : `style = "Color:${monTotal >= 0 ? "green" : "red"}"`} > ${fomartmoney(monTotal)(4)}</span >`);
        }
    },
    columns:
        [
            {
                "class": "details-control",
                "orderable": false,
                "data": null,
                "defaultContent": ""
            },
            { data: 'Document' },
            { data: 'User' },
            { data: 'SiteName' },
            { data: 'Fecha' },
            { data: 'Status' },
            { data: 'Amount' }
        ],
    columnDefs: [
        {
            targets: [0],
            width: "1%"
        },
        {
            targets: [1, 2, 3, 4],
            width: "22%"
        },
        {
            targets: [5],
            render: function (data, type, full, meta) {
                if (data == 1) {
                    return "Inicializada"
                }
                else if (data == 2) {
                    return "Procesada"
                }
                else if (data == 9) {
                    return "Terminada"
                }
                else if (data == 6) {
                    return "Solicitud Cancelacion"
                }
                else if (data == 8 || data == 3) {
                    return "Cancelada"
                }
                else {
                    return "Desconocido"
                }
            },
            width: "1%"
        },
        {
            targets: [6],
            render: function (data, type, full, meta) {
                return `<span>${fomartmoney(data)(4)}</span>`
            },
            width: "1%"
        }
    ]
});

$("#tableScrapDep").append('<tfoot><th>Total</th><th colspan="6"></th><th></th><th colspan="5"></th></tfoot>');

var tableScrap = $('#tableScrapDep').DataTable({
    oLanguage:
    {
        "sProcessing": "Procesando...",
        "sLengthMenu": "Mostrar _MENU_ registros",
        "sZeroRecords": "No se encontraron resultados",
        "sEmptyTable": "Ningún dato disponible en esta tabla",
        "sInfo": "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
        "sInfoEmpty": "Mostrando registros del 0 al 0 de un total de 0 registros",
        "sInfoFiltered": "(filtrado de un total de _MAX_ registros)",
        "sInfoPostFix": "",
        "sSearch": "Buscar:",
        "sUrl": "",
        "sInfoThousands": ",",
        "sLoadingRecords": "Cargando...",
        "oPaginate": { "sFirst": "Primero", "sLast": "Último", "sNext": "Siguiente", "sPrevious": "Anterior" },
        "oAria": { "sSortAscending": ": Activar para ordenar la columna de manera ascendente", "sSortDescending": ": Activar para ordenar la columna de manera descendente" }
    },
    dom: "<'row'<'col-sm-4'l><'col-sm-4 text-left'B><'col-sm-4'f>t<'col-sm-6'i><'col-sm-6'p>>",
    lengthMenu: [[10, 25, 50, -1], [10, 25, 50, "Todos"]],
    buttons:
        [
            { extend: 'csv', text: 'Excel', title: 'Merma Contralada', className: 'btn-sm', footer: true },
            {
                extend: 'pdf', text: 'PDF', title: 'Merma Contralada', className: 'btn-sm',
                exportOptions: { columns: [1, 2, 3] },
                customize: function (doc) {
                    doc.content[1].table.widths = Array(doc.content[1].table.body[0].length + 1).join('*').split('');
                }, footer: true
            },
            { extend: 'print', text: 'Imprimir', className: 'btn-sm', footer: true },
        ],
    "footerCallback": function (row, data, start, end, display) {
        var api = this.api();
        api.columns('.sumP', { page: -1 }).every(function () {
            var sum = this
                .data()
                .reduce(function (a, b) {
                    var x = parseFloat(a) || 0;
                    var y = parseFloat(b) || 0;
                    return x + y;
                }, 0);
            $(this.footer()).html(fomartmoney(sum)(2));
        });
    },
    columns:
        [
            { data: 'Document' },
            { data: 'Status' },
            { data: 'PartNumber' },
            { data: 'Description' },
            { data: 'Quantity' },
            { data: 'UnitSize' },
            { data: 'Price' },
            { data: 'Amount' },
            { data: 'Date' },
            { data: 'Family' },
            { data: 'Department' },
            { data: 'ScrapReason' },
            { data: 'User' }
        ],
    columnDefs: [
        {
            targets: [0],
            width: "1%"
        },
        {
            targets: [1],
            render: function (data, type, full, meta) {
                if (data == 1) {
                    return "Inicializada"
                }
                else if (data == 2) {
                    return "Procesada"
                }
                else if (data == 9) {
                    return "Terminada"
                }
                else if (data == 8 || data == 3) {
                    return "Cancelada"
                }
                else if (data == 6) {
                    return "Solicitud Cancelacion"
                }
                else {
                    return "Desconocido"
                }
            },
            width: "1%"
        },
        {
            targets: [6, 7],
            render: function (data, type, full, meta) {
                return fomartmoney(data)(2);
            }
        }
    ]
});

$("#tableScrapTotal").append('<tfoot><th>Total</th><th></th></tfoot>');

var tableScrapTotals = $('#tableScrapTotal').DataTable({
    oLanguage:
    {
        "sProcessing": "Procesando...",
        "sLengthMenu": "Mostrar _MENU_ registros",
        "sZeroRecords": "No se encontraron resultados",
        "sEmptyTable": "Ningún dato disponible en esta tabla",
        "sInfo": "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
        "sInfoEmpty": "Mostrando registros del 0 al 0 de un total de 0 registros",
        "sInfoFiltered": "(filtrado de un total de _MAX_ registros)",
        "sInfoPostFix": "",
        "sSearch": "Buscar:",
        "sUrl": "",
        "sInfoThousands": ",",
        "sLoadingRecords": "Cargando...",
        "oPaginate": { "sFirst": "Primero", "sLast": "Último", "sNext": "Siguiente", "sPrevious": "Anterior" },
        "oAria": { "sSortAscending": ": Activar para ordenar la columna de manera ascendente", "sSortDescending": ": Activar para ordenar la columna de manera descendente" }
    },
    dom: "<'row'<'col-sm-4'l><'col-sm-4 text-left'B><'col-sm-4'f>t<'col-sm-6'i><'col-sm-6'p>>",
    lengthMenu: [[5, 10, 25, 50, -1], [5, 10, 25, 50, "Todos"]],
    pageLength: 5,
    buttons:
        [
            { extend: 'csv', text: 'Excel', title: 'Merma Contralada', className: 'btn-sm', footer: true },
            {
                extend: 'pdf', text: 'PDF', title: 'Merma Contralada', className: 'btn-sm',
                exportOptions: { columns: [1, 2, 3] },
                customize: function (doc) {
                    doc.content[1].table.widths = Array(doc.content[1].table.body[0].length + 1).join('*').split('');
                }, footer: true
            },
            { extend: 'print', text: 'Imprimir', className: 'btn-sm', footer: true },
        ],
    "footerCallback": function (row, data, start, end, display) {
        var api = this.api(), data;
        // converting to interger to find total
        var intVal = function (i) {
            return typeof i === 'string' ?
                i.replace(/[\$,]/g, '') * 1 :
                typeof i === 'number' ?
                    i : 0;
        };

        for (var i = 1; i < 2; i++) {
            var monTotal = api
                .column(i)
                .data()
                .reduce(function (a, b) {
                    return intVal(a) + intVal(b);
                }, 0);
            $(api.column(i).footer()).html(`<span ${monTotal == 0 ? "" : `style = "Color:${monTotal >= 0 ? "green" : "red"}"`} > ${fomartmoney(monTotal)(4)}</span >`);
        }
    },
    columns:
        [
            { data: 'Department' },
            { data: 'TotalAmount' }
        ],
    columnDefs: [
        {
            targets: [0, 1],
            width: "1%"
        }, {
            targets: [1],
            render: function (data, type, full, meta) {
                return fomartmoney(data)(2);
            }

        }
    ]
});

var detailRows = [];
$('#tableEMSCHead tbody').on('click', 'tr td.details-control', function () {
    var tr = $(this).closest('tr');
    var row = table.row(tr);
    if (row.child.isShown()) {
        row.child.hide();
        tr.removeClass('details');
    }
    else {
        if (table.row('.details').length) {
            $('.details-control', table.row('.details').node()).click();
        }
        row.child(format(row.data())).show();
        tr.addClass('details');
    }
});

table.on('draw', function () {
    $.each(detailRows, function (i, id) {
        $('#' + id + ' td.details-control').trigger('click');
    });
});

function format(d) {
    var detailItems = "";
    var tabledetail = $('<div/>').addClass('loading').text('Cargando Datos...');
    $.ajax({
        url: "/DamagedsGoods/ActionGetItemsByDocumentReport",
        data: { "document": d.Document, "status": d.Status },
        type: "GET",
        success: function (returndate) {
            $.each(returndate.Json, function (index, value) {
                //total = value.IVA + value.IEPS + value.Amount;
                detailItems += "<tr><td>" + value.PartNumber + "</td><td>" + value.Description + "</td><td>" + (value.ScrapReason == null ? 'Sin razón' : value.ScrapReason) + "</td><td class='text-right'>$ " + value.Price + "</td><td class='text-right'>" + value.Quantity + "</td><td class='text-right'>" + fomartmoney(value.Amount)(4) + "</td></tr>"
            });
            btn = "";
            if (d.Status == 9) {
                btn = '<div class="form-group col-md-12"> <div class="row"><div class="row"> <div class="col-md-2 col-md-offset-10 text-right"><button id="btn-save" type="button" class="btn btn-sm btn-info" onclick="SearchPDF(' + d.Document + ',' + d.Status + ');"><i class="fa fa-print"></i> PDF</button></div></div></div>'
            }
            tabledetail.html('<table id="tabledetail" class="table table-striped table-bordered table-hover" style="width:100%">' +
                '<thead><tr><th>Código</th><th>Descripción</th><th>Razón</th><th>Precio</th><th>Cantidad</th><th>Importe</th></tr></thead><tbody>' + detailItems + '</tbody></table>' + btn).removeClass('loading');
            reloadStyleTable();
        }
    });
    return tabledetail;
}

function SearchPDF(document, status) {
    StartLoading();
    axios.get(`/DamagedsGoods/ActionGetScraptPdf?document=${document}&status=${status}`)
        .then(data => {
            EndLoading();
            if (data.data.success) {
                $("#myModalpdf").modal("show")
                $("#iframes").html(`<iframe src='data:application/pdf;base64,${data.data.pdf}' height='320px' width='100%'></iframe>`)
            }
            else if (data.data.session == "SF")
                SessionFalse("Termino tu sesión.");
            else
                toastr.error("Error al generar pdf")
        }).catch(error => {
            toastr.error('Error inesperado contactar a sistemas.');
        })
}

function reloadStyleTable() {
    $('#tabledetail').DataTable({
        oLanguage:
        {
            "sProcessing": "Procesando...",
            "sLengthMenu": "Mostrar _MENU_ registros",
            "sZeroRecords": "No se encontraron resultados",
            "sEmptyTable": "Ningún dato disponible en esta tabla",
            "sInfo": "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
            "sInfoEmpty": "Mostrando registros del 0 al 0 de un total de 0 registros",
            "sInfoFiltered": "(filtrado de un total de _MAX_ registros)", "sInfoPostFix": "",
            "sSearch": "Buscar:", "sUrl": "", "sInfoThousands": ",", "sLoadingRecords": "Cargando...",
            "oPaginate": { "sFirst": "Primero", "sLast": "Último", "sNext": "Siguiente", "sPrevious": "Anterior" },
            "oAria":
            {
                "sSortAscending": ": Activar para ordenar la columna de manera ascendente",
                "sSortDescending": ": Activar para ordenar la columna de manera descendente"
            }
        },
    });
}

function SearchDocuments() {
    if ($('#dateInit').val() != "" && $('#dateFin').val() != "") {
        if (moment($('#dateFin').val()) < moment($('#dateInit').val())) {
            toastr.error("Fecha final no puede ser menor a la inicial.");
            $('#dateFin').focus();
            return;
        }
        else {
            StartLoading();
            var Detail = document.getElementById("ckbox_excess").checked;
            if (!Detail) {
                $.ajax({
                    url: "/DamagedsGoods/ActionGetDocumentsReport",
                    type: "GET",
                    data: { "dateInicial": $('#dateInit').val(), "dateFinal": $('#dateFin').val(), "status": $("#status").val() },
                    success: function (response) {
                        EndLoading();
                        if (response.success) {
                            table.clear().draw();
                            table.rows.add(response.Orders);
                            table.columns.adjust().draw();
                            showTable(true);
                        }
                        else if (response.responseText === 'Termino tu sesión.')
                            SessionFalse(response.responseText);
                        else {
                            toastr.warning(response.responseText);
                            table.clear().draw();
                        }
                    },
                    error: function () {
                        EndLoading();
                        toastr.error('Error inesperado contactar a sistemas.');
                    }
                });
            } else {
                $.ajax({
                    url: "/DamagedsGoods/ActionGetScrapItemsDepartment",
                    type: "POST",
                    data: { "dateInitial": $('#dateInit').val(), "dateFinish": $('#dateFin').val(), "department": $("#department").val(), "status": $("#status").val() },
                    success: function (response) {
                        EndLoading();
                        if (response.success) {
                            tableScrap.clear().draw();
                            tableScrap.rows.add(response.data);
                            tableScrap.columns.adjust().draw();
                            showTable(false);
                        }
                        else if (response.responseText === 'Termino tu sesión.')
                            SessionFalse(response.responseText);
                        else {
                            toastr.warning(response.responseText);
                            tableScrap.clear().draw();
                        }
                    },
                    error: function () {
                        EndLoading();
                        toastr.error('Error inesperado contactar a sistemas.');
                    }
                });
            }
        }
    }
}

function showTable(a) {
    if (!a) {
        $("#tableEMSCContent").hide();
        $("#divTableScrapDep").show();
    } else {
        $("#tableEMSCContent").show();
        $("#divTableScrapDep").hide();
    }
}

function check() {
    if ($("#ckbox_excess").is(':checked')) {
        //$("#divStatus").hide();
        $("#divDepartment").show();
        SearchDocuments();
    } else {
        //$("#divStatus").show();
        $("#divDepartment").hide();
        SearchDocuments();
    }
}

function showModalTotals() {
    if ($('#dateInit').val() == "" || $('#dateFin').val() == "") {
        toastr.warning("Seleccione rango de fechas");
        EndLoading();
        return;
    }

    var Status = parseInt(document.getElementById("status").value);
    var Depto = parseInt(document.getElementById("department").value);
    var Detail = document.getElementById("ckbox_excess").checked;

    $.ajax({
        url: "/DamagedsGoods/ActionGetScrapDepartmentTotals",
        type: "POST",
        data: { "dateInitial": $('#dateInit').val(), "dateFinish": $('#dateFin').val(), "Status": Status, "Department": Depto, "Detail": Detail },
        success: function (response) {
            EndLoading();
            if (response.success) {
                $("#modalTotals").modal("show");
                tableScrapTotals.clear().draw();
                tableScrapTotals.rows.add(response.data);
                tableScrapTotals.columns.adjust().draw();
            }
            else if (response.responseText === 'Termino tu sesión.')
                SessionFalse(response.responseText);
            else {
                toastr.warning(response.responseText);
                tableScrap.clear().draw();
            }
        },
        error: function () {
            EndLoading();
            toastr.error('Error inesperado contactar a sistemas.');
        }
    });
}