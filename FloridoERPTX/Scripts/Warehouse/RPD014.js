﻿window.setTimeout(function () { $("#fa").click(); }, 1000);

$("#ValidDate").datepicker({
    autoclose: true,
    todayHighlight: true,
    showButtonPanel: true,
});

const fomartmoney = money => decimal => new Intl.NumberFormat("en-US", { style: "currency", currency: "USD", currencyDisplay: "symbol", maximumFractionDigits: decimal }).format(money);

$("#Items").append('<tfoot><th>Total General</th><th colspan="5"></th><th></th></tfoot>');

var table = $('#Items').DataTable({
    responsive: true,
    autoWidth: true,
    oLanguage: {
        "sProcessing": "Procesando...", "sLengthMenu": "Mostrar _MENU_ registros", "sZeroRecords": "No se encontraron resultados", "sEmptyTable": "Ningún dato disponible en esta tabla", "sInfo": "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros", "sInfoEmpty": "Mostrando registros del 0 al 0 de un total de 0 registros", "sInfoFiltered": "(filtrado de un total de _MAX_ registros)", "sInfoPostFix": "", "sSearch": "Buscar:", "sUrl": "", "sInfoThousands": ",", "sLoadingRecords": "Cargando...", "oPaginate": { "sFirst": "Primero", "sLast": "Último", "sNext": "Siguiente", "sPrevious": "Anterior" }, "oAria": { "sSortAscending": ": Activar para ordenar la columna de manera ascendente", "sSortDescending": ": Activar para ordenar la columna de manera descendente" }
    },
    dom: "<'row'<'col-sm-4'l><'col-sm-4 text-left'B><'col-sm-4'f>t<'col-sm-6'i><'col-sm-6'p>>",
    lengthMenu: [[10, 25, 50, -1], [10, 25, 50, "Todos"]],
    buttons: [
        { extend: 'csv', text: 'Excel', title: 'Inventario Histórico', className: 'btn-sm', footer: true },
        {
            extend: 'pdf', text: 'PDF', title: 'Inventario Histórico', className: 'btn-sm', exportOptions: {
                columns: [0, 1, 2, 3, 4]
            },
            customize: function (doc) {
                doc.content[1].table.widths =
                    Array(doc.content[1].table.body[0].length + 1).join('*').split('');
            }, footer: true
        },
        { extend: 'print', text: 'Imprimir', className: 'btn-sm', footer: true },
    ],
    "footerCallback": function (row, data, start, end, display) {
        var api = this.api(), data;
        // converting to interger to find total
        var intVal = function (i) {
            return typeof i === 'string' ?
                i.replace(/[\$,]/g, '') * 1 :
                typeof i === 'number' ?
                    i : 0;
        };

        for (var i = 6; i < 7; i++) {
            var monTotal = api
                .column(i)
                .data()
                .reduce(function (a, b) {
                    return intVal(a) + intVal(b);
                }, 0);
            $(api.column(i).footer()).html(`<span ${monTotal == 0 ? "" : `style = "Color:${monTotal >= 0 ? "green" : "red"}"`} > ${fomartmoney(monTotal)(4)}</span >`);
        }
    },
    columns: [
        { targets: 0, data: 'part_number', width: "1%" },
        { targets: 1, data: 'part_description', width: "1%" },
        { targets: 2, data: 'family', width: "1%" },
        { targets: 3, data: 'category', width: "1%" },
        { targets: 4, data: 'Costo', width: "1%", className: "text-center" },
        { targets: 5, data: 'Quantity', width: "1%", className: "text-center" },
        { targets: 6, data: 'Monto', width: "1%", className: "text-center" }
    ],
    columnDefs: [{
        targets: [0, 1, 2, 3, 4, 5, 6],
        width: "1%"
    },
    {
        targets: [4, 6],
        render: function (data, type, row) {
            return '$' + data;
        }
    }]
});

var detailRows = [];
table.on('draw', function () {
    $.each(detailRows, function (i, id) {
        $('#' + id + ' td.details-control').trigger('click');
    });
});

$(document).on("keypress", '.Numeric', function (event) {
    var keyValue = event.key;
    if (keyValue.match(/[0-9]/g))
        return true;
    else
        return false;
});

$('#Search').on('click', function () {
    var date = $('#ValidDate').val();
    if (date != "") {
        StartLoading();
        $.ajax({
            url: "/Items/ActionGetHistoricInventory",
            type: "GET",
            data: { "Day": date },
            success: function (result) {
                EndLoading();
                if (!result.Status)
                    SessionFalse("Se terminó su sesion.");
                else {
                    table.clear();
                    table.rows.add($(result.Model));
                    table.columns.adjust().draw();
                    $('#TableDiv').animatePanel();

                    if (!$.isEmptyObject(result.Model))
                        $("#Print").removeAttr('disabled', 'disabled');
                    else
                        $("#Print").attr('disabled', true);
                }
            },
            error: function () {
                EndLoading();
                $("#Print").attr('disabled', true);
                toastr.error('Error inesperado contactar a sistemas.');
            }
        });
    }
    else {
        if (date == "") {
            $('#ValidDate').focus();
            toastr.error('Seleccione una fecha.');
        }
    }
});

$('#Print').on('click', function () {
    var i = 0;
    listProducts = [];
    $.each(table.rows().data(), function (index, value) {
        var LastPurchase = moment(value.LastPurchase).format('MM/DD/YYYY');
        listProducts[i] = { PartNumber: value.PartNumber, PartDescription: value.PartDescription, Unrestricted: value.Unrestricted, MapPrice: value.MapPrice, StorageLocation: value.StorageLocation, LastPurchase: LastPurchase, Days: value.Days };
        i++;
    });
    StartLoading();
    $.ajax({
        url: '/Items/ActionGenerateReportInventoryWithoutSales',
        type: "POST",
        data: { items: listProducts },
        success: function (response) {
            EndLoading();
            if (response.responseText == "Terminó tu sesión.")
                SessionFalse(response.responseText);
            if (response.success) {
                $("#modalReferenceBody").html("<iframe width='100%' height='550px' src='data:application/pdf;base64, " + encodeURI(response.responseText) + "'></iframe>");
                $("#modalReference").modal('show');
            }
            else
                toastr.error(response.responseText);
        },
        error: function () {
            EndLoading();
            toastr.error('Error inesperado contactar a sistemas.');
        }
    });
});