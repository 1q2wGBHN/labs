﻿window.setTimeout(function () { $("#fa").click(); }, 500);

var dateInit = $("#ValidDate1");
var dateFin = $("#ValidDate2");
var ListTable = []
const selectOption = selectId => selectName => `<option value="${selectId}">${selectId} - ${selectName}</option>`;
var listTransferDetail = [];
var count = 0;
const TransferArray = element => ({
    DestinationSiteAddress: element.DestinationSiteAddress,
    DestinationSiteCode: element.DestinationSiteCode,
    DestinationSiteName: element.DestinationSiteName,
    PrintStatus: element.PrintStatus,
    TransferDate: element.TransferDate,
    TransferDocument: element.TransferDocument,
    TransferSiteAddress: element.TransferSiteAddress,
    TransferSiteCode: element.TransferSiteCode,
    TransferSiteName: element.TransferSiteName,
    TransferStatus: element.TransferStatus,
    TransferStatusName: () => {
        var text = ""
        switch (element.TransferStatus) {
            case 0:
                text = "Pendientes"
                break
            case 3:
                text = "Transito"
                break
            default:
                text = "Desconocido"
                break
        }
        return text;
    }
});

function CreateTable(Transfer) {
    table.clear();
    table.rows.add(jQuery(Transfer.map(element => TransferArray(element))));
    table.columns.adjust().draw();
}

dateInit.datepicker({
    autoclose: true,
    todayHighlight: true,
    showButtonPanel: true,
}).on("changeDate", function (e) {
    SerchTransfer();
});

dateFin.datepicker({
    autoclose: true,
    startDate: '01/01/1950',
    todayHighlight: true,
}).on("changeDate", function (e) {
    SerchTransfer();
});

var table = $('#TablePurchases').DataTable({
    autoWidth: true,
    oLanguage: {
        "sProcessing": "Procesando...", "sLengthMenu": "Mostrar _MENU_ registros", "sZeroRecords": "No se encontraron resultados", "sEmptyTable": "Ningún dato disponible en esta tabla", "sInfo": "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros", "sInfoEmpty": "Mostrando registros del 0 al 0 de un total de 0 registros", "sInfoFiltered": "(filtrado de un total de _MAX_ registros)", "sInfoPostFix": "", "sSearch": "Buscar:", "sUrl": "", "sInfoThousands": ",", "sLoadingRecords": "Cargando...", "oPaginate": { "sFirst": "Primero", "sLast": "Último", "sNext": "Siguiente", "sPrevious": "Anterior" }, "oAria": { "sSortAscending": ": Activar para ordenar la columna de manera ascendente", "sSortDescending": ": Activar para ordenar la columna de manera descendente" }
    },
    dom: "<'row'<'col-sm-4'l><'col-sm-4 text-left'B><'col-sm-4'f>t<'col-sm-6'i><'col-sm-6'p>>",
    lengthMenu: [[10, 25, 50, -1], [10, 25, 50, "Todos"]],
    buttons: [],
    columns: [
        {
            "class": "details-control",
            "orderable": false,
            "data": null,
            "defaultContent": "",
        },
        { data: 'TransferDocument' },
        { data: 'TransferSiteName' },
        { data: 'DestinationSiteName' },
        { data: 'TransferStatusName' },
        { data: 'TransferDate' },
    ],
    columnDefs: [
        {
            targets: [0],
            width: "10%"
        },
        {
            targets: [1, 2, 3],
            width: "30%"
        }
    ]
});

var detailRows = [];

$('#TablePurchases tbody').on('click', 'tr td.details-control', function () {
    var tr = $(this).closest('tr');
    var row = table.row(tr);

    if (row.child.isShown()) {
        row.child.hide();
        tr.removeClass('details');
    }
    else {
        if (table.row('.details').length) {
            $('.details-control', table.row('.details').node()).click();
        }
        row.child(format(row.data())).show();
        tr.addClass('details');
    }
});

table.on('draw', function () {
    $.each(detailRows, function (i, id) {
        $('#' + id + ' td.details-control').trigger('click');
    });
});

function setValues(Quantity, Ieps, Iva, Subtotal, Total) {
    if (Ieps <= 0)
        Ieps = 0;
    if (Iva <= 0)
        Iva = 0;
    if (Subtotal <= 0)
        Subtotal = 0;
    if (Total <= 0)
        Total = 0;

    $("#Quantity").text(Quantity);
    $("#IEPS").text("$" + (Ieps).toFixed(4));
    $("#Iva").text("$" + (Iva).toFixed(4));
    $("#Subtotal").text("$" + (Subtotal).toFixed(4));
    $("#Total").text("$" + (Total).toFixed(4));
}

var list = [];
function format(d) {
    var detailItems = "";
    var tabledetail = $('<div/>').addClass('loading').text('Cargando Datos...');
    if (d.TransferStatus == 3) {
        $.ajax({
            url: "/TransferHeader/ActionGetAllTransferDeatilsExist",
            data: { "Document": d.TransferDocument },
            type: "GET",
            success: function (data) {
                Items = data;
                listTransferDetail = data;
                $.each(data, function (index, value) {
                    detailItems += "<tr><td class='text-center'><div class='checkbox'><label><input type='checkbox' class='i-checks' /></label></div></td><td>" + value.PartNumber + "</td><td>" + value.Description + "</td><td>$" + value.UnitCost + "</td><td class='text-right'>" + value.Quantity + "</td><td class='text-right'>$" + value.IEPS + "</td><td class='text-right'>" + "$" + value.Iva + "</td><td class='text-right'>" + "$" + value.SubTotal + "</td><td class='text-right'>" + "$" + value.Amount + "</td>"
                });
                var Apro = "";
                if (d.TransferStatus == 3 && d.PrintStatus == 0) {
                    Apro = '<div class="row"> <div class="form-group col-md-12"> <div class="pull-right"><button id = "btn-save" type = "button" class="btn btn-sm btn-success" onclick = "AprovalPrint(' + "'" + d.TransferDocument.toString() + 'R' + "'" + ');"><i class="fa fa-print"></i> Imprimir </button></div></div></div>';
                }
                if (d.TransferStatus == 3 && (d.PrintStatus == 1 || d.PrintStatus == 2)) {
                    Apro = '<div class="row">  <div class="form-group col-md-12"><div class="pull-right"><button id = "btn-save" type = "button" class="btn btn-sm btn-success" onclick = "AprovalPrint(' + "'" + d.TransferDocument.toString() + 'R' + "'" + ');"><i class="fa fa-print"></i> Reimprimir </button></div></div></div> ';
                }
                tabledetail.html('<table id="tabledetail" class="table table-striped table-bordered table-hover" style="width:100%"><thead><tr><th></th><th>Codigo</th><th>Nombre del producto</th><th>Precio unitario</th><th>Cantidad</th><th>IEPS</th><th>IVA</th><th>Subtotal</th><th>Total</th></tr></thead><tbody>' + detailItems + '</tbody></table>' + Apro).removeClass('loading');

                var Quantity = 0;
                var Ieps = 0;
                var Iva = 0;
                var Subtotal = 0;
                var Total = 0;

                $('input[type=checkbox]').iCheck({
                    checkboxClass: 'icheckbox_square-green',
                }).on('ifClicked', function () {
                    $(this).trigger("change");

                    var item = this;
                    var check = item.checked;
                    var PartNumber = item.attributes.partnumber.value;
                    var quantity = parseFloat(item.attributes.quantity.value);
                    var ieps = parseFloat(item.attributes.ieps.value);
                    var iva = parseFloat(item.attributes.iva.value);
                    var subtotal = parseFloat(item.attributes.subtotal.value);
                    var total = parseFloat(item.attributes.amount.value);

                    if (!check) {
                        Quantity += quantity;
                        Ieps += ieps;
                        Iva += iva;
                        Subtotal += subtotal;
                        Total += total;
                        setValues(Quantity, Ieps, Iva, Subtotal, Total);
                        list.push({ PartNumber: PartNumber });
                    }
                    else {
                        Quantity -= quantity;
                        Ieps -= ieps;
                        Iva -= iva;
                        Subtotal -= subtotal;
                        Total -= total;
                        setValues(Quantity, Ieps, Iva, Subtotal, Total);
                        list = list.filter(x => x.PartNumber != PartNumber);
                    }
                });
                reloadStyleTable();
            }
        });
    } else {
        list = [];
        $.ajax({
            url: "/TransferHeader/ActionGetAllTransferDetail",
            data: { "Document": d.TransferDocument },
            type: "GET",
            success: function (data) {
                Items = data.Data;
                selectIs = false;
                listTransferDetail = data.Data;
                $.each(data.Data, function (index, value) {
                    detailItems += "<tr><td class='text-center'><div class='checkbox'><label><input type='checkbox' class='i-checks' UnitCost = " + value.UnitCost + " PartNumber = " + value.PartNumber + "  Quantity=" + value.Quantity + " IEPS=" + value.IEPS + " Iva=" + value.Iva + " SubTotal=" + value.SubTotal + " Amount=" + (value.SubTotal + value.Iva + value.IEPS) + " /></label></div></td><td>" + value.PartNumber + "</td><td>" + value.Description + "</td><td>$" + value.UnitCost + "</td><td class='text-right'>" + value.Quantity + "</td><td class='text-right'>$" + value.IEPS.toFixed(4) + "</td><td class='text-right'>" + "$" + value.Iva.toFixed(4) + "</td><td class='text-right'>" + "$" + value.SubTotal.toFixed(4) + "</td><td class='text-right'>" + "$" + (value.SubTotal + value.Iva + value.IEPS).toFixed(4) + "</td>"
                });
                var Apro = "";
                if (d.TransferStatus == 0) {
                    Apro = '<div class="col-md-7 pull-right"><table id="totals" class="table table-striped table-bordered table-hover text-center" style="width:100%"><thead><tr><th class="text-center">Cantidad</th><th class="text-center">IEPS</th><th class="text-center">IVA</th><th class="text-center">Subtotal</th><th class="text-center">Total</th></tr></thead><tbody> <tr> <td id="Quantity"></td> <td id="IEPS">‬</td> <td id="Iva"></td> <td id="Subtotal"></td> <td id="Total">‬</td> </tr> </tbody></table></div>';
                    Apro += '<div class="col-lg-5"><label class="control-label"> Comentario: </label><div><textarea style="max-width:400px;max-height:200px;min-width:100px;min-height:100px;" rows="4" cols="50" maxlength="100" id="comment" class="form-control"></textarea></div></div ><div class="col-lg-5"><label class="control-label"> Chofer: </label><div><select id="driver" class="form-control select2"></select></div></div><div class="pull-right" style="margin-top:22px;"><button id="btn-save" type="button" class="btn btn-sm btn-success" onclick="AprovalMessageSave(' + " '" + d.TransferDocument.toString() + ' R' + "'" + ');" ><i class="fa fa-save"></i> Aprobar</button >&nbsp;&nbsp; <button id="btn-cancel" type="button" class="btn btn-sm btn-danger" onclick="AprovalMessageCancel(' + "'" + d.TransferDocument.toString() + 'R' + "'" + ');" ><i class="fa fa-close"></i> Rechazar</button ></div>';
                    selectIs = true;
                }
                if (d.TransferStatus == 3 && d.PrintStatus == 0) {
                    Apro = '<div class="row"> <div class="form-group col-md-12"> <div class="pull-right"><button id = "btn-save" type = "button" class="btn btn-sm btn-success" onclick = "AprovalPrint(' + "'" + d.TransferDocument.toString() + 'R' + "'" + ');"> Imprimir </button></div></div></div>';
                }
                if (d.TransferStatus == 3 && (d.PrintStatus == 1 || d.PrintStatus == 2)) {
                    Apro = '<div class="row">  <div class="form-group col-md-12"><div class="pull-right"><button id = "btn-save" type = "button" class="btn btn-sm btn-success" onclick = "AprovalPrint(' + "'" + d.TransferDocument.toString() + 'R' + "'" + ');"> Reimprimir </button></div></div></div> ';
                }
                tabledetail.html('<table id="tabledetail" class="table table-striped table-bordered table-hover" style="width:100%"><thead><tr><th></th><th>Codigo</th><th>Nombre del producto</th><th>Precio unitario</th><th>Cantidad</th><th>IEPS</th><th>IVA</th><th>Subtotal</th><th>Total</th></tr></thead><tbody>' + detailItems + '</tbody></table>' + Apro).removeClass('loading');
                FullCombobox(selectIs);

                var Quantity = 0;
                var Ieps = 0;
                var Iva = 0;
                var Subtotal = 0;
                var Total = 0;

                $('input[type=checkbox]').iCheck({
                    checkboxClass: 'icheckbox_square-green',
                }).on('ifClicked', function () {
                    $(this).trigger("change");

                    var item = this;
                    var check = item.checked;
                    var PartNumber = item.attributes.partnumber.value;
                    var unitCost = item.attributes.unitcost.value;
                    var quantity = parseFloat(item.attributes.quantity.value);
                    var ieps = parseFloat(item.attributes.ieps.value);
                    var iva = parseFloat(item.attributes.iva.value);
                    var subtotal = parseFloat(item.attributes.subtotal.value);
                    var total = subtotal + iva + ieps;//parseFloat(item.attributes.amount.value);

                    if (!check) {
                        Quantity += quantity;
                        Ieps += ieps;
                        Iva += iva;
                        Subtotal += subtotal;
                        Total += total;
                        setValues(Quantity, Ieps, Iva, Subtotal, Total);
                        list.push({ PartNumber: PartNumber, UnitCost: unitCost, Quantity: Quantity });
                    }
                    else {
                        Quantity -= quantity;
                        Ieps -= ieps;
                        Iva -= iva;
                        Subtotal -= subtotal;
                        Total -= total;
                        setValues(Quantity, Ieps, Iva, Subtotal, Total);
                        list = list.filter(x => x.PartNumber != PartNumber);
                    }
                });

                reloadStyleTable();
            }
        });
    }
    return tabledetail
}

function AprovalPrint(document) {
    StartLoading();
    axios.get("/TransferHeader/ActionPrintTransferReport/?Document=" + document.split('R')[0])
        .then(function (data) {
            if (data.data == "SF") {
                EndLoading()
                SessionFalse("Se terminó su sesion.")
            }
            else if (data.data.success) {
                $("#myModal8").modal('show')
                id = $("#folio").val();
                $("#iframes").html(`<iframe src='data:application/pdf;base64,${data.data.responseText}' height='320px' width='100%'></iframe>`)
                EndLoading()
            }
            else {
                toastr.error('Alerta - Error inesperado  contactar a sistemas.');
                EndLoading()
            }
        }).catch(function (error) {
            EndLoading();
        })
}

function FullCombobox(value) {
    if (value) {
        axios.get("/UserSpecs/ActionGetAllUserSpecs")
            .then(function (data) {
                $("#driver").html(["<option value=''>Seleccione un chofer</option>", ...data.data.map(x => selectOption(x.EmpNo)(x.FullName))])
                $("#driver").select2({
                    minimumInputLength: 2,
                    formatInputTooShort: function () {
                        return "Busca un chofer";
                    },
                    matcher: function (term, text, option) {
                        return text.toUpperCase().indexOf(term.toUpperCase()) >= 0 || option.val().toUpperCase().indexOf(term.toUpperCase()) >= 0;
                    }
                })
            }).catch(function (error) {
                console.log(error.data)
            })
    }
}

function reloadStyleTable() {
    $('#tabledetail').DataTable({
        oLanguage: {
            "sProcessing": "Procesando...", "sLengthMenu": "Mostrar _MENU_ registros", "sZeroRecords": "No se encontraron resultados", "sEmptyTable": "Ningún dato disponible en esta tabla", "sInfo": "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros", "sInfoEmpty": "Mostrando registros del 0 al 0 de un total de 0 registros", "sInfoFiltered": "(filtrado de un total de _MAX_ registros)", "sInfoPostFix": "", "sSearch": "Buscar:", "sUrl": "", "sInfoThousands": ",", "sLoadingRecords": "Cargando...", "oPaginate": { "sFirst": "Primero", "sLast": "Último", "sNext": "Siguiente", "sPrevious": "Anterior" }, "oAria": { "sSortAscending": ": Activar para ordenar la columna de manera ascendente", "sSortDescending": ": Activar para ordenar la columna de manera descendente" }
        },
    });
}

function AprovalMessageSave(s) {
    if (list.length > 0) {
        if ($("#comment").val().trim().length > 7) {
            if ($("#driver").val() != "") {
                swal({
                    title: "¿Esta seguro que desea Aprobar la Transferencia?",
                    type: "info",
                    showCancelButton: true,
                    confirmButtonColor: "#55dd6b",
                    cancelButtonText: "Cancelar",
                    confirmButtonText: "Continuar"
                }, function (isConfirm) {
                    if (isConfirm)
                        AprovalStatus(list, s, 1);
                });
            }
            else
                toastr.warning('Falta seleccionar un chofer.');
        } else
            toastr.warning('Ingresa un comentario con mínimo 8 caracteres');
    }
    else
        toastr.warning("Seleccione al menos un producto.");
}

function AprovalMessageCancel(s) {
    if ($("#comment").val().trim().length > 7) {
        swal({
            title: "¿Esta seguro que desea Rechazar la Transferencia?",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            cancelButtonText: "Cancelar",
            confirmButtonText: "Continuar"
        }, function (isConfirm) {
            if (isConfirm)
                AprovalStatus(null, s, 2); //Manda nullo, cuando se rechaza
        });
    }
    else
        toastr.warning('Pon un comentario con minimo 8 caracteres');
}

function AprovalStatus(model, document, status) {
    StartLoading()
    $("#comment").val().trim().length > 7;
    $("#comment").val().trim()
    $("#comment").val()

    const driver = $("#driver").val();
    axios.post("/TransferHeader/ActionEditStatusTransferHeader",
            {
                Model: model,
                Document: document.split('R')[0],
                Status: status,
                Comment: $("#comment").val(),
                driverCode: (status == 2 ? "" : driver)
            })
        .then(data => {
            EndLoading();
            if (data.data == "SF")
                SessionFalse("Se terminó su sesion.");
            else if (data.data) {
                swal({
                    title: status === 1 ? "Se ha aplicado con éxito." : "Se ha rechazado con éxito.",
                    type: "success",
                    closeOnConfirm: true,
                    showLoaderOnConfirm: true
                });
            } else
                toastr.warning("Se encontro un problema desconcido al " + StatusText(status))
        }).catch(error => {
            EndLoading();
            toastr.error('Alerta - Error inesperado  contactar a sistemas.');
        }).then(() => {
            ClearForm();
            SearchSelect();
        });
}

function StatusText(value) {
    if (value == 1)
        return "Aplicado"
    else if (value == 2)
        return "Cancelado"
    else if (value == 3)
        return "Transito"
    else
        return "Desconocido"
}

function ClearForm() {
    table.clear();
    table.rows.add([]);
    table.columns.adjust().draw();
    dateInit.val("")
    dateFin.val("")
}

function SearchSelect() {
    count = 0;
    $("#TableDiv").animatePanel();
    var Status = $("#SelectOptions").val();
    ListTable = []
    CreateTable([])
    if (dateInit.val() != "" & dateFin.val() != "") {
        if (moment(dateInit.val()) <= moment(dateFin.val())) {
            if (Status == "Todos") {
                ListTable = TableDate2(dateInit.val(), dateFin.val())
            }
            else {
                ListTable = TableDate(dateInit.val(), dateFin.val(), Status, ListTable)
            }
        }
        else {
            toastr.error("Fecha final no puede ser menor a la inicial.");
            dateInit.val('');
            dateFin.val('');
            dateInit.focus();
        }
    }
    else {
        if (Status == "Todos") {
            ListTable = TableDate2(moment().add(-7, 'days').format("MM/DD/YYYY"), moment().format("MM/DD/YYYY"))
        }
        else {
            ListTable = TableDate(moment().add(-7, 'days').format("MM/DD/YYYY"), moment().format("MM/DD/YYYY"), Status, ListTable)
        }
    }
    setTimeout(() => { table.columns.adjust().draw(); }, 500)
    setTimeout(() => { table.columns.adjust().draw(); }, 1500)
}

function SerchTransfer() {
    count = 0;
    var Status = $("#SelectOptions").val();
    ListTable = []
    if (dateInit.val() != "" & dateFin.val() != "") {
        $("#TableDiv").animatePanel();
        CreateTable([])
        if (moment(dateInit.val()) <= moment(dateFin.val())) {
            if (Status == "Todos") {
                ListTable = TableDate2(dateInit.val(), dateFin.val())
            }
            else {
                ListTable = TableDate(dateInit.val(), dateFin.val(), Status, ListTable)
            }
        } else {
            toastr.error("Fecha final no puede ser menor a la inicial.");
            dateInit.val('');
            dateFin.val('');
            dateInit.focus();
        }
    }
    setTimeout(() => { table.columns.adjust().draw(); }, 500)
    setTimeout(() => { table.columns.adjust().draw(); }, 1500)
}

function TableDate2(init, fin) {
    list = []
    axios.get(`/TransferHeader/ActionGetAllTransferHeader/?DateInit=${init}&DateFin=${fin}&Status=3`)
        .then((data) => {
            if (data.data == "SF") {
                SessionFalse("Se terminó su sesion.")
            }
            else if (data.data.length > 0) {
                list = [data.data, ...list]
                CreateTable(list.flat())
                count++;
            }
        }).catch((error) => {
            toastr.error('Alerta - Error inesperado  contactar a sistemas.');
        }).then(() => {
            table.columns.adjust().draw();
        })

    //quitar el setTimeOut si no funciona y descomentar el if de arriba "Calidad Florido.."
    setTimeout(() => {
        axios.get(`/TransferHeader/ActionGetAllTransferHeader/?DateInit=${init}&DateFin=${fin}&Status=0`)
            .then((data) => {
                if (data.data == "SF") {
                    SessionFalse("Se terminó su sesion.")
                }
                else if (data.data.length > 0) {
                    list = [data.data, ...list]
                    CreateTable(list.flat())
                    count++;
                }
                else {
                }
            }).catch((error) => {
                toastr.error('Alerta - Error inesperado  contactar a sistemas.');
            }).then(() => {
                if (count <= 0) {
                    toastr.warning('No hay transferencias');
                }
                table.columns.adjust().draw();
            })
    }, 500);
}

function TableDate(init, fin, status, list) {
    axios.get(`/TransferHeader/ActionGetAllTransferHeader/?DateInit=${init}&DateFin=${fin}&Status=${status}`)
        .then((data) => {
            if (data.data == "SF") {
                SessionFalse("Se terminó su sesion.")
            }
            else if (data.data.length > 0) {
                list = [data.data, ...list]
                CreateTable(list.flat())
                count++;
            }
        }).catch((error) => {
            toastr.error('Alerta - Error inesperado  contactar a sistemas.');
        }).then(() => {
            if (count <= 0) {
                toastr.warning('No hay transferencias');
            }
            table.columns.adjust().draw();
            return list;
        })
}

function errors(value) {
    if (value == 6001)
        return "site code Incorrecto"
    else if (value == 6002)
        return "Estatus no aprobado"
    else if (value == 8001)
        return "Error desconocido"
    else
        return "desconosido"
}

var Transfers = JSON.parse($("#model").val());
CreateTable(Transfers);