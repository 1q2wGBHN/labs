﻿$(document).ready(function () {
    window.setTimeout(function () { $("#fa").click(); }, 1000);
    $("#statusSelect").select2();
});

$("#ValidDate1").datepicker({
    autoclose: true,
    todayHighlight: true,
    showButtonPanel: true,
}).on("changeDate", function (e) {

});

$("#ValidDate2").datepicker({
    autoclose: true,
    startDate: "01/01/1950",
    todayHighlight: true,
}).on("changeDate", function (e) {

});

const formatmoney = money => decimal => new Intl.NumberFormat("en-US", { style: "currency", currency: "USD", currencyDisplay: "symbol", maximumFractionDigits: decimal }).format(money);

$("#TableTransferHeader").append('<tfoot><th>Total</th><th colspan = "2"></th><th></th><th></th><th></th><th></th><th></th><th></th></tfoot>');

var table = $("#TableTransferHeader").DataTable({
    oLanguage: {
        "sProcessing": "Procesando...", "sLengthMenu": "Mostrar _MENU_ registros", "sZeroRecords": "No se encontraron resultados", "sEmptyTable": "Ningún dato disponible en esta tabla", "sInfo": "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros", "sInfoEmpty": "Mostrando registros del 0 al 0 de un total de 0 registros", "sInfoFiltered": "(filtrado de un total de _MAX_ registros)", "sInfoPostFix": "", "sSearch": "Buscar:", "sUrl": "", "sInfoThousands": ",", "sLoadingRecords": "Cargando...", "oPaginate": { "sFirst": "Primero", "sLast": "Último", "sNext": "Siguiente", "sPrevious": "Anterior" }, "oAria": { "sSortAscending": ": Activar para ordenar la columna de manera ascendente", "sSortDescending": ": Activar para ordenar la columna de manera descendente" }
    },
    responsive: true,
    autoWidth: true,
    order: [[1, "desc"]],
    dom: "<'row'<'col-sm-4'l><'col-sm-4 text-left'B><'col-sm-4'f>t<'col-sm-6'i><'col-sm-6'p>>",
    lengthMenu: [[10, 25, 50, -1], [10, 25, 50, "Todos"]],
    buttons: [
        { extend: 'csv', text: 'Excel', title: 'MateriaPrimaConcentrado', className: 'btn-sm', footer: true },
        {
            extend: 'pdf', text: 'PDF', title: 'Transferencias CEDIS', className: 'btn-sm', exportOptions: {
                columns: [1, 2, 3, 4, 5, 6, 7, 8]
            }, footer: true,
            customize: function (doc) {
                doc.content[1].table.widths =
                    Array(doc.content[1].table.body[0].length + 1).join('*').split('');
            }
        },
        { extend: 'print', text: 'Imprimir', className: 'btn-sm', footer: true },
    ],
    
    columns: [
        {
            "class": "details-control",
            "orderable": false,
            "data": null,
            "defaultContent": "",
            width: "1%",
            targets: 0
        },
        { targets: 1, data: "TransferDocument", width: "1%" },
        { targets: 2, data: "PalletSn", width: "1%" },
        { targets: 3, data: "TransferStatus", width: "1%" },
        { targets: 4, data: "ScanFlag", width: "1%" },
        { targets: 5, data: "Amount", width: "1%" },
        { targets: 6, data: "Ieps", width: "1%" },
        { targets: 7, data: "Iva", width: "1%" },
        { targets: 8, data: "Total", width: "1%" },
        { targets: 9, data: "SiteName", width: "1%" }
    ],
    columnDefs: [{
        targets: [0, 1, 2, 3, 4, 5, 6, 7, 8,9],
        width: "1%"
    },
    {
        targets: [3],
        render: function (data, type, row)
        {
            var cadena = "";
            if (data === 0) {
                cadena = "Creada";
            }
            else if (data === 1) {
                cadena = "Aprobada";
            }
            else if (data === 2)
            {
                cadena = "Rechazada Gerencia";
            }
            else if (data === 3)
            {
                cadena = "Camino Transito";
            }
            else if (data === 4)
            {
                cadena = "Transito";
            }
            else if (data === 6)
            {
                cadena = "Solicitud Cancelacion";
            }
            else if (data === 7) {
                cadena = "Solicitud Cancelacion";
            }
            else if (data === 8)
            {
                color = "Red";
                cadena = "CANCELADA";
            }
            else if (data === 9)
            {
                cadena = "Terminada";
            }
            return '<div  text-align:center;"><strong>' + cadena + '</strong></div>';
        }
    },
        {
            targets: [4],
            render: function (data, type, row)
            {
                var cadena = "";
                if (data === true) {
                    cadena = "Escaneado";
                }
                else  {
                    cadena = "No escaneado";
                }
                return '<div  text-align:center;"><strong>' + cadena + '</strong></div>';
            },
            width: "1%"
        },
    {
        targets: [5],
        render: function (data, type, full, meta) {
            return "$" + data.toFixed(3);
        },
        width: "1%"
    },
    {
        targets: [6],
        render: function (data, type, full, meta) {
            return "$" + data.toFixed(2);
        },
        width: "1%"
    },
    {
        targets: [7],
        render: function (data, type, full, meta) {
            return "$" + data.toFixed(2);
        },
        width: "1%"
    },
    {
        targets: [8],
        render: function (data, type, full, meta) {
            return "$" + data.toFixed(3);
        },
        width: "1%"
    }]
});

function ClearTable(table, list) {
    table.clear();
    table.rows.add(list);
    table.columns.adjust().draw();
    $("#TableDiv").animatePanel();
}

var detailRows = [];

$("#TableTransferHeader tbody").on("click", "tr td.details-control", function () {
    var tr = $(this).closest("tr");
    var row = table.row(tr);

    if (row.child.isShown()) {
        row.child.hide();
        tr.removeClass("details");
    }
    else {
        if (table.row(".details").length) {
            $(".details-control", table.row(".details").node()).click();
        }
        row.child(format(row.data())).show();
        tr.addClass("details");
    }
});

table.on("draw", function () {
    $.each(detailRows, function (i, id) {
        $("#" + id + " td.details-control").trigger("click");
    });
});

const fomartmoney = money => decimal => new Intl.NumberFormat("en-US", { style: "currency", currency: "USD", currencyDisplay: "symbol", maximumFractionDigits: decimal }).format(money);
trTotal = element => `${fomartmoney(element.Iva + element.Ieps + element.Import)(4)}`
FullTd = element => Option => `<td>${Option == 1 ? fomartmoney(element)(4) : Option == 0 ? element : element != null && element.trim() != "" ? "Contado" : "No contado"}</td >`
FullTh = element => `<th>${element}</th>`
FullButton = element => `<div class="row pull-right">
<div class="col-lg-12"> 
<button id="btn-cancel" type="button" class="btn btn-sm btn-success" onclick="AprovalPdf('${element.PalletSn}')">
<i class="fa fa-print"></i> Imprimir</button>
</div>
</div>`
FullTable = element => array => array.map(title => FullTd(element[title.title])(title.option)).toString();
FullTableHeader = array => `<tr>${array.map(title => FullTh(title))}</tr>`

function reloadStyleTable() {
    $('#tabledetail').DataTable({
        oLanguage: {
            "sProcessing": "Procesando...", "sLengthMenu": "Mostrar _MENU_ registros", "sZeroRecords": "No se encontraron resultados", "sEmptyTable": "Ningún dato disponible en esta tabla", "sInfo": "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros", "sInfoEmpty": "Mostrando registros del 0 al 0 de un total de 0 registros", "sInfoFiltered": "(filtrado de un total de _MAX_ registros)", "sInfoPostFix": "", "sSearch": "Buscar:", "sUrl": "", "sInfoThousands": ",", "sLoadingRecords": "Cargando...", "oPaginate": { "sFirst": "Primero", "sLast": "Último", "sNext": "Siguiente", "sPrevious": "Anterior" }, "oAria": { "sSortAscending": ": Activar para ordenar la columna de manera ascendente", "sSortDescending": ": Activar para ordenar la columna de manera descendente" }
        },
        columnDefs: [{
            targets: [2],
            width: "1%"
        }]
    });
}

function format(d) {
    var detailItems = "";
    var tabledetail = $("<div/>").addClass("loading").text("Cargando Datos...");
    $.ajax({
        url: "/TransferPalletInfo/ActionDetaiPalletInfoReport/",
        data: { "PalletSn": d.PalletSn },
        type: "GET",
        success: function (data) {

            if (data != "SF") {
                detailItems = data.map(value => ` <tr>${FullTable(value)([
                    { title: "PartNumber", option: 0 },
                    { title: "PartDescription", option: 0 },
                    { title: "Quantity", option: 0 },
                    { title: "Cost", option: 1 },
                    { title: "Ieps", option: 0 },
                    { title: "Iva", option: 0 },
                    { title: "SubTotal", option: 1 },
                    { title: "IepsTotal", option: 1 },
                    { title: "IvaTotal", option: 1 },
                    { title: "Total", option: 1 },
                ])}</tr>`).join().replace(/>,</g, '><').replace(/>, </g, '><')
                header = FullTableHeader(["Codigo", "Nombre", "Cantidad", "Costo", "Ieps", "Iva", "Importe", "Ieps Total", "Iva Total", "Total"]).replace(/,/g, '')
                var Html = (`<table id="tabledetail" class="table table-striped table-bordered table-hover" style="width:100%"><thead>
                   ${header}
                    </thead><tbody>${detailItems.length > 0 ? detailItems : "<td colspan='8' style='text-align: center;'>No cuenta con productos</td>"}</tbody></table>${FullButton(d)}`)
                tabledetail.html(Html).removeClass('loading');
            }
            else
                SessionFalse("Termino tu session")
            reloadStyleTable();
        }
    });
    return tabledetail;
}

function AprovalPdf(pallet) {
    StartLoading();
    axios.post("/TransferPalletInfo/ActionPrintTransferReport", { PalletSn: pallet }).then(function (data) {
        EndLoading();
        if (data.data.Data == "SF")
            SessionFalse("Se terminó su sesión.");
        else if (data.data.success) {
            $("#modalReferenceBody").html(`<iframe width='100%' height='550px' src='data:application/pdf;base64,${data.data.responseText}'></iframe>`);
            $("#modalReference").modal("show");
        }
        else
            toastr.warning("Error inesperado  contactar a sistemas.")
    }).catch(function (error) {
        EndLoading();
        toastr.error("Alerta - Error inesperado  contactar a sistemas.");
    }).then(() => { EndLoading() })
}

function SerchRMA() {
    var date = $("#ValidDate1").val();
    var date2 = $("#ValidDate2").val();

    if (date.trim() != "" && date2.trim() != "" && moment(date) <= moment(date2)) {
        StartLoading();
        $.ajax({
            type: "GET",
            url: "/TransferPalletInfo/ActionListTransferPalletInfoReport/",
            data: { "DateInit": $("#ValidDate1").val(), "DateFinal": $("#ValidDate2").val() },
            success: function (result) {
                EndLoading();
                if (result == "SF")
                    SessionFalse("Termino tu session")
                else if (result.length > 0)
                    ClearTable(table, result)
                else if (result.length == 0)
                    toastr.warning("No hay productos con esos filtros")
                else
                    toastr.warning("Alerta - Error inesperado!!");
            },
            error: function (result) {
                EndLoading();
                toastr.error("Alerta - Error inesperado contactar a sistemas.");
            }
        });
    }
    else {
        toastr.warning("Favor de colocar una fecha valida")
        return null;
    }
}