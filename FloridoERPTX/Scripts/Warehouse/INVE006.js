﻿/// <reference path="../knockout-3.4.2.debug.js" />

window.setTimeout(function () { $("#fa").click(); }, 1000);
const fomartmoney = money => decimal => new Intl.NumberFormat("en-US", { style: "currency", currency: "USD", currencyDisplay: "symbol", maximumFractionDigits: decimal }).format(money);

var Requests = $('#Requests').DataTable({
    oLanguage: {
        "sProcessing": "Procesando...", "sLengthMenu": "Mostrar _MENU_ registros", "sZeroRecords": "No se encontraron resultados", "sEmptyTable": "Ningún dato disponible en esta tabla", "sInfo": "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros", "sInfoEmpty": "Mostrando registros del 0 al 0 de un total de 0 registros", "sInfoFiltered": "(filtrado de un total de _MAX_ registros)", "sInfoPostFix": "", "sSearch": "Buscar:", "sUrl": "", "sInfoThousands": ",", "sLoadingRecords": "Cargando...", "oPaginate": { "sFirst": "Primero", "sLast": "Último", "sNext": "Siguiente", "sPrevious": "Anterior" }, "oAria": { "sSortAscending": ": Activar para ordenar la columna de manera ascendente", "sSortDescending": ": Activar para ordenar la columna de manera descendente" }
    },
    responsive: true,
    autoWidth: true,
    order: [[1, "asc"]],
    buttons: [
        { extend: 'csv', text: 'Excel', title: 'Equipos', className: 'btn-sm' },
        {
            extend: 'pdf', text: 'PDF', title: 'Equipos', className: 'btn-sm', exportOptions: {
                columns: [1, 2, 3, 4, 5, 6, 7]
            },
            customize: function (doc) {
                doc.content[1].table.widths =
                    Array(doc.content[1].table.body[0].length + 1).join('*').split('');
            }
        },
        { extend: 'print', text: 'Imprimir', className: 'btn-sm' },
    ],
    columnDefs: [
        {
            data: 'Document',
            render: function (data, type, row) {
                if (data != '')
                    return "<button class='btn btn-xs btn-outline btn-warning btn-doc' type='button'>Documento</button>";
                else
                    return "";
            },
            targets: [0],
            className: "text-center text-nowrap",
            orderable: false,
            width: "1%"
        },
        {
            data: null,
            targets: [9],
            className: "text-center text-nowrap",
            orderable: false,
            render: function (data, type, full, meta) {
                return `<span>${(full.NewQuantity - full.PreviousQuantity)}</span>`;
            },
            width: "1%"
        },
        {
            data: null,
            targets: [10],
            className: "text-center text-nowrap",
            orderable: false,
            render: function (data, type, full, meta) {
                return `<span>${fomartmoney(data)(2)}</span>`;
            },
            width: "1%"
        },
        {
            data: null,
            targets: [11],
            className: "text-center text-nowrap",
            orderable: true,
            render: function (data, type, full, meta) {
                return `<span>${fomartmoney((full.NewQuantity - full.PreviousQuantity) * full.MapPrice)(2)}</span>`;
            },
            width: "1%"
        },
        {
            data: null,
            defaultContent: "<table><tbody><tr><td><button class='btn btn-xs btn-danger btn-cancel pull-left' type='button'><i class='fa fa-close'></i>Rechazar</button></td><td><button class='btn btn-xs btn-success btn-approve pull-right' type='button'><i class='fa fa-save'></i>Aprobar</button></td></tr></tbody></table>",
            targets: [12],
            className: "text-center text-nowrap",
            orderable: false,
            width: "1%"
        }
    ],
    columns: [
        { targets: 0, data: 'Document', width: "1%" },
        { targets: 1, data: 'InventoryHeaderId', width: "1%" },
        { targets: 2, data: 'InventoryName', width: "1%" },
        { targets: 3, data: 'PartNumber', width: "1%" },
        { targets: 4, data: 'ProductDescription', width: "1%" },
        { targets: 5, data: 'unrestricted', width: "1%" },
        { targets: 6, data: 'PreviousQuantity', width: "1%" },
        { targets: 7, data: 'NewQuantity', width: "1%" },
        { targets: 8, data: 'Justification', width: "1%" },
        { targets: 9, data: null, width: "1%" },
        { targets: 10, data: 'MapPrice', visible: true },
        { targets: 11, data: null, width: "1%", },
        { targets: 12, data: null, width: "1%", className: "text-center" }
    ],
});

var model = JSON.parse($("#model").val());
if (!$.isEmptyObject(model)) {
    $('#TableDiv').animatePanel();
    Requests.clear();
    Requests.rows.add($(model));
    Requests.columns.adjust().draw();
}
else {
    Requests.clear();
    Requests.columns.adjust().draw();
}

function viewModel() {
    var self = this;

    self.InventoryHeaderId = ko.observable();
    self.PartNumber = ko.observable();

    self.Clear = function () {
        self.InventoryHeaderId("");
        self.PartNumber("");
    }

    self.Update = function (model) {
        Requests.clear();
        Requests.rows.add($(model));
        Requests.columns.adjust().draw();
        $('#TableDiv').animatePanel();
    }

    self.Approve = function () {
        if (self.Validate()) {
            StartLoading();
            $.ajax({
                url: "/InventoryAdjustment/ActionSetAdjustment/",
                type: "POST",
                data: JSON.stringify({ "InventoryHeaderId": self.InventoryHeaderId(), "PartNumber": self.PartNumber(), "AdjustmentStatus": 9 }),
                dataType: "json",
                contentType: "application/json; charset=utf-8",
                success: function (result) {
                    if (result.status == 0)
                        SessionFalse(result.responseText);
                    else if (result.status == 1) {
                        toastr.success("Ajuste de inventario realizado con exito");
                        self.Update(result.Model);
                    }
                    else
                        toastr.error("Ocurrió un error durante el procesamiento de la solicitud.");
                    EndLoading();
                },
                error: function (result) {
                    EndLoading();
                    toastr.error("Ocurrió un error durante el procesamiento de la solicitud.");
                }
            });
        }
    }

    self.Cancel = function () {
        if (self.Validate()) {
            StartLoading();
            $.ajax({
                url: "/InventoryAdjustment/ActionSetAdjustment/",
                type: "POST",
                data: JSON.stringify({ "InventoryHeaderId": self.InventoryHeaderId(), "PartNumber": self.PartNumber(), "AdjustmentStatus": 8 }),
                dataType: "json",
                contentType: "application/json; charset=utf-8",
                success: function (result) {
                    if (result.status == 0)
                        SessionFalse(result.responseText);
                    else if (result.status == 1) {
                        toastr.success("Ajuste de inventario cancelado con exito");
                        self.Update(result.Model);
                    }
                    else
                        toastr.error("Ocurrió un error durante el procesamiento de la solicitud.");
                    EndLoading();
                },
                error: function (result) {
                    EndLoading();
                    toastr.error("Ocurrió un error durante el procesamiento de la solicitud.");
                }
            });
        }
    }

    self.Validate = function () {
        if ($.trim(self.InventoryHeaderId()) == "" && $.trim(self.PartNumber()) == "") {
            toastr.remove();
            toastr.error("Error desconocido, contacte a sistemas");
            return false;
        }
        else {
            toastr.remove();
            return true;
        }
    }
}

var vm = new viewModel();
ko.applyBindings(vm);

$('#Requests tbody').on('click', '.btn-approve', function () {
    var InventoryHeaderId = Requests.row($(this).parents('tr')).data().InventoryHeaderId;
    var PartNumber = Requests.row($(this).parents('tr')).data().PartNumber;
    var ProductDescription = Requests.row($(this).parents('tr')).data().ProductDescription;
    var PreviousQuantity = Requests.row($(this).parents('tr')).data().PreviousQuantity;
    var NewQuantity = Requests.row($(this).parents('tr')).data().NewQuantity;

    swal({
        title: "Esta seguro?",
        text: "Se modificará el conteo de : " + ProductDescription + "\nCantidad actual: " + PreviousQuantity + "\nNueva cantidad: " + NewQuantity,
        type: "info",
        closeOnConfirm: true,
        showCancelButton: true,
        confirmButtonColor: "#55dd6b",
        confirmButtonText: "Continuar",
        cancelButtonText: "Cancelar"
    }, function (isConfirm) {
        setTimeout(function () {
            if (isConfirm) {
                vm.InventoryHeaderId(InventoryHeaderId);
                vm.PartNumber(PartNumber);
                vm.Approve();
            }
        }, 400)
    });
});

$('#Requests tbody').on('click', '.btn-cancel', function () {
    var InventoryHeaderId = Requests.row($(this).parents('tr')).data().InventoryHeaderId;
    var PartNumber = Requests.row($(this).parents('tr')).data().PartNumber;
    var ProductDescription = Requests.row($(this).parents('tr')).data().ProductDescription;
    var PreviousQuantity = Requests.row($(this).parents('tr')).data().PreviousQuantity;
    var NewQuantity = Requests.row($(this).parents('tr')).data().NewQuantity;

    swal({
        title: "Esta seguro?",
        text: "Se cancelará el conteo de : " + ProductDescription + "\nCantidad actual: " + PreviousQuantity + "\nNueva cantidad: " + NewQuantity,
        type: "warning",
        closeOnConfirm: true,
        showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "Continuar",
        cancelButtonText: "Cancelar"
    }, function (isConfirm) {
        setTimeout(function () {
            if (isConfirm) {
                vm.InventoryHeaderId(InventoryHeaderId);
                vm.PartNumber(PartNumber);
                vm.Cancel();
            }
        }, 400)
    });
});

$('#Requests tbody').on('click', '.btn-doc', function () {
    var inventoryheaderid = Requests.row($(this).parents('tr')).data().InventoryHeaderId;
    var partnumber = Requests.row($(this).parents('tr')).data().PartNumber;

    var form = $("#document");
    form.attr("action", "/InventoryAdjustment/ActionGetInventoryDoc?InventoryHeaderId=" + inventoryheaderid + "&PartNumber=" + partnumber);
    form.submit();
});