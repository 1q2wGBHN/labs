﻿var xml_permission = 0;
const isDecimal = decimal => number => {
    const IsDecimal = RegExp(`^\\d+\\.?\\d{0,${decimal}}$`);
    return IsDecimal.test(number);
}
const Sum = list => Name => list.reduce((init, total) => { return init + parseFloat(total[Name]) }, 0)
const fomartmoney = money => decimal => new Intl.NumberFormat("en-US", { style: "currency", currency: "USD", currencyDisplay: "symbol", maximumFractionDigits: decimal }).format(money);

function ComboboxOreders() {
    const selectOption = selectId => selectName => `<option value="${selectId}">${selectName}</option>`;
    axios.get("/PurchaseOrder/ActionGetAllOrders").then((data) => {
        $("#folioSearch").html([`<option value="0">Seleccione una orden</option>`, ...data.data.map(x => selectOption(x.PurchaseNo)(x.PurchaseNo + " - " + x.supplier_name))])
    }).catch((error) => {

    })
}

$('#datetimepicker').datepicker({
    autoclose: true,
    format: "mm/dd/yyyy",
    endDate: new Date(),
    todayHighlight: true,
    language: 'es'
});

function SwalMessage(optionSwal, funSucces, funcError, parmas) {
    swal({
        title: optionSwal.text,
        type: optionSwal.type,
        text: optionSwal.textOptional,
        showCancelButton: true,
        confirmButtonColor: optionSwal.color,
        confirmButtonText: "Continuar",
        cancelButtonText: "Cancelar"
    },
        function (isConfirm) {
            if (isConfirm) {
                funSucces(parmas);
            }
            else {
                funcError(parmas);
            }
        });
}
function ErrorListF(value, type, text, typeS, textSucces) {
    return !value ? ToastError(type, text) : typeS != null && typeS != "" ? ToastError(typeS, textSucces, true) : true;
}
function ToastError(type, text, value) {
    if (type == "Success") {
        toastr.success(text)
        return true;
    }
    else if (type == "Error") {
        toastr.error(text)
    }
    else if (type == "Warning") {
        toastr.warning(text)
    }
    return value != undefined || value != null ? value : false;
}
function ClearComboAndForm() {
    ClearForm();
    ClearCombo();
}

function SearchAndClearForm() {
    Search();
    ClearForm()
}
var FullTd = element => Option => Option == 2 ? `<td class = "discrepancy">${element} </td>` : `<td>${Option == 1 ? fomartmoney(element)(4) : Option == 0 ? element : element != null && element.trim() != "" ? "Contado" : "No contado"}</td >`
var FullTh = element => `<th>${element}</th>`
var FullTable = element => array => array.map(title => FullTd(element[title.title])(title.option)).toString();
var FullTableHeader = array => `<tr>${array.map(title => FullTh(title))}</tr>`
Listoption = { RFC: "", Currency: "" }
listBlind = []
var base64Xml = "";
const sesion = "Se termino su sesion.";
var total = 0;
var sumTotalIva = 0;
var sumTotalIEPS = 0;
var totalAmount = 0;
var notXMl = false;
var diference = 0;
$(document).ready(function () {
    OCHideOrShow(true);
    RCHideOrShow(true)
    window.setTimeout(function () { $("#fa").click(); }, 1000);
    $('#buttonStatus').click(function () {
        SwalMessage(
            {
                text: "¿Deseas procesar la orden de compra con diferencia?", type: "warning", color: "#DD6B55"
            },
            NewModifyBlind,
            function () { },
            null
        )
    });
    $("#buttonRescaneo").click(function () {
        SwalMessage(
            {
                text: "¿Deseas rescanear la orden de compra?", type: "warning", color: "#DD6B55"
            },
            function () { StatusRC(); ClearComboAndForm(); },
            function () { },
            null
        )
    });
    $("#buttonRescaneoS4").click(function () {
        SwalMessage(
            {
                text: "¿Deseas rescanear la orden de compra?", type: "warning", color: "#DD6B55"
            },
            function () { StatusRC(); ClearComboAndForm(); },
            function () { },
            null
        )
    });
    $("#cancelOrderButton").click(function () {
        purchaseValue = $("#folioSearch").val()
        if (ErrorListF(purchaseValue != "0", "Warning", "Selecciona una orden a cancelar")) {
            SwalMessage(
                {
                    text: "¿Deseas cancelar la orden de compra?", type: "warning", color: "#DD6B55"
                },
                CancelPurchaseOrder,
                function () { },
                null
            )
        }

    });
    [".select", "#selectOption"].map(element => $(element).select2())
    $("#saveFormModel").hide();
});
function ClearCombo() {
    ComboboxOreders()
    $("#select2-chosen-1").html("Seleccione una orden")
}
function SearchPurchase() {
    diference = 0;
    StartLoading()
    notXMl = false;
    axios.get("/PurchaseOrder/ActionGetOCRC/", { params: { PurchaseNo: $("#folioSearch").val() } })
        .then(function (data) {
            if (data.data == "SF") {
                EndLoading()
                SessionFalse(sesion)
                $("#cancelOrderButtonDiv").hide();
            }
            else if (ErrorListF(data.data == "S9", null, null, "Warning", "La orden de compra ya fue procesada") ||
                ErrorListF(data.data == "S1", null, null, "Warning", "La orden de compra no esta disponible") ||
                ErrorListF(data.data == "N/A", null, null, "Warning", "La orden de compra no existe")) {
                ClearComboAndForm()
                $("#cancelOrderButtonDiv").hide();
                EndLoading()
            }
            else if (data.data == "S3" || data.data == "S4") {
                $("#cancelOrderButtonDiv").show();
                if (data.data == "S4") {
                    $("#buttonRescaneoS4Title").show();
                    $("#buttonRescaneoS4").show();
                }
                else {
                    $("#buttonRescaneoS4Title").show();
                    $("#buttonRescaneoS4").show();
                }
                SearchAndClearForm();
                notXMl = true;
            }
            else {
                toastr.warning('La orden de compra no se encuentra con el estatus adecuado para procesarlo.');
                $("#searchBody").hide();
                ClearCombo()
                EndLoading()
                $("#cancelOrderButton").hide();
            }
        }).catch(function (error) {
            toastr.error('Alerta - Error inesperado  contactar a sistemas.');
        }).then(() => { })
}

function TableORCG() {
    $("#saveFormModel").hide();
    axios.get("/PurchaseOrderItem/ActionPurchaseOrderItemList/?PurchaseNo=" + $("#folioSearch").val())
        .then(function (data) {
            data.data.map((x, index) => x.IndexP = (index + 1));
            if (data.data.length > 0) {
                $("#tableOCRG").html("")
                isDifference = data.data.filter(x => x.QuantityOC != x.QuantityRC).length
                if (isDifference > 0) {
                    OCHideOrShow(true)
                    RCHideOrShow(false)
                    $("#tableHeadORCG").html("<tr><th  class='text-center'>#</th><th  class='text-center'>Codigo</th><th  class='text-center'>Descripción</th><th class='text-center'>Cantidad de orden de compra</th><th class='text-center'>Cantidad de recibo ciego</th></tr>")

                    listBlind = data.data.map(value => ({
                        Id: value.Id, QuantityOC: value.QuantityOC,
                        QuantityRC: value.QuantityRC, ParthNumber: value.ParthNumber,
                        PurchaseNo: value.PurchaseNo
                    }));
                    data.data.map(x => x.quantity = 1);
                    $("#tableOCRG").html(data.data.map(value => (`<tr>${FullTable(value)(
                        [
                            { title: "IndexP", option: value.QuantityOC == value.QuantityRC ? 0 : 2 },
                            { title: "ParthNumber", option: value.QuantityOC == value.QuantityRC ? 0 : 2 },
                            { title: "Description", option: value.QuantityOC == value.QuantityRC ? 0 : 2 },
                            { title: "QuantityOC", option: value.QuantityOC == value.QuantityRC ? 0 : 2 },
                            { title: "QuantityRC", option: value.QuantityOC == value.QuantityRC ? 0 : 2 },
                        ])}</tr>`
                    )).join().replace(/>,</g, '><').replace(/>, </g, '><'))
                    $("#searchBody").show();
                    EndLoading();
                }
                else {
                    $("#tableHeadORCG").html("<tr><th>#</th><th class='text-center' style='width:6%;'>Articulo</th><th class='text-center' style='width:12%;'>Descripción</th><th class='text-center' style='width:7%;'>Factor</th><th class='text-center' style='width:10%;'>Cantidad</th><th class='text-center' style='width:10%;'>Unidad</th><th class='text-center' style='width:10%;'>Precio $</th><th class='text-center' style='width:10%;'>Importe</th><th class='text-center' style='width:10%;'>IVA Total $</th><th class='text-center' style='width:10%;'>IEPS Total $</th class='text-center'><th class='text-center' style='width:13%;'>Importe Total $</th></tr>")
                    $("#searchBody").show()
                    OCHideOrShow(false)
                    RCHideOrShow(true)
                    OCInfo()
                }
            }
            else {
                toastr.warning('La orden de compra se encontro vacia');
                EndLoading()
            }
        }).catch(function (error) {
            toastr.error('Alerta - Error inesperado  contactar a sistemas.');
            EndLoading()
        }).then(() => { })
}

function OCInfoFill(id, data, name) {
    $(id).text(data[name] != "" ? data[name] : "N/A");
}

function OCInfo() {
    $("#saveFormModel").hide();
    axios.get("/PurchaseOrderItem/ActionPurchaseOrderItem/?PurchaseNo=" + $("#folioSearch").val())
        .then(function (data) {
            if (data.data.Supplier.PurchaseOrderList.length > 0) {
                xml_permission = data.data.Permission;
                $("#saveFormModel").show();
                $("#tableOCRG").html("")
                ClearOrder();
                [
                    { title: "#provider", name: "CommercialName" }, { title: "#rfc", name: "RFC" }, { title: "#datee", name: "PurchaseDate" },
                    { title: "#currency", name: "Currency" }, { title: "#email", name: "Email" }, { title: "#nameContact", name: "NameContact" },
                    { title: "#phone", name: "Phone" }, { title: "#adress", name: "Adress" }
                ].map(element => OCInfoFill(element.title, data.data.Supplier, element.name))
                Listoption.RFC = data.data.Supplier.RFC
                Listoption.Currency = data.data.Supplier.Currency
                table = ""
                sumTotalIva = sumTotalIEPS = sumTotalAmount = total = 0;
                $.each(data.data.Supplier.PurchaseOrderList, function (index, value) {
                    table += "<tr><td>" + (index + 1) + "</td><td>" + value.PartNumber + "</td><td>" + value.Description + "</td>" + "<td  class='text-right'>" + value.Parcking + "</td>" + "<td  class='text-right'>" + value.Quantity + "</td>" + "<td>" + (value.UnitSize != null ? value.UnitSize : "") + "</td>" + "<td class='text-right'>" + new Intl.NumberFormat("en-US", { style: "currency", currency: "USD", currencyDisplay: "symbol", maximumFractionDigits: 4 }).format(value.PurchasePrice) + "</td>" + "<td class='text-right'>" + new Intl.NumberFormat("en-US", { style: "currency", currency: "USD", currencyDisplay: "symbol", maximumFractionDigits: 4 }).format(value.ItemAmount) + "</td>" + "<td class='text-right'>" + new Intl.NumberFormat("en-US", { style: "currency", currency: "USD", currencyDisplay: "symbol", maximumFractionDigits: 4 }).format(value.Iva) + "</td>" + "<td class='text-right'>" + new Intl.NumberFormat("en-US", { style: "currency", currency: "USD", currencyDisplay: "symbol", maximumFractionDigits: 4 }).format(value.Ieps) + "</td>" + "<td class='text-right'>" + new Intl.NumberFormat("en-US", { style: "currency", currency: "USD", currencyDisplay: "symbol", maximumFractionDigits: 4 }).format(value.ItemTotalAmount) + "</td>" + "<tr>"
                    sumTotalIva += value.Iva
                    sumTotalIEPS += value.Ieps
                    sumTotalAmount += value.ItemAmount
                })
                $("#tableOCRG").html(table);
                $("#searchBody").show();
                total = parseFloat((sumTotalIva + sumTotalIEPS + sumTotalAmount).toFixed(4));
                [{ id: "totalIva", total: sumTotalIva },
                { id: "totalIEPS", total: sumTotalIEPS },
                { id: "totalAmount", total: sumTotalAmount },
                { id: "totalFinal", total: total }]
                    .map(element => $(`#${element.id}`).html(fomartmoney(element.total)(4)))
                EndLoading()
            }
            else {
                EndLoading()
            }
        }).catch(function (error) {
            toastr.error('Alerta - Error inesperado  contactar a sistemas.');
            EndLoading()
        }).then(() => { })
}

function Search() {
    ["#folio", "#folio2"].map(id => $(id).html($("#folioSearch").val()))
    TableORCG();
}

function OCHideOrShow(value) {
    var clearInfoOC = ["#infoSupplier", "#textProvider", "#folio",
        "#provider", "#datee", "#currency", "#email", "#nameContact", "#phone",
        "#adress", "#textOrder", "#totalIvaForm", "#totalIEPSForm", "#totalAmountForm", "#totalFinalForm",
        "#totalIva", "#totalIEPS", "#totalAmount", "#totalFinal", "#saveForm"]
    value ? clearInfoOC.map(id => $(id).hide()) : clearInfoOC.map(id => $(id).show())
}
function RCHideOrShow(value) {
    var clearInfoOC = ["#folioForm", "#textOCRG", "#statusForm", "#buttonStatus"]
    value ? clearInfoOC.map(id => $(id).hide()) : clearInfoOC.map(id => $(id).show());
}

function ClearOrder() {
    var clearInfoOC = ["#provider", "#datee", "#currency", "#email", "#nameContact", "#phone", "#adress"]
    clearInfoOC.map(id => $(id).text(""))
}
function ClearForm() {
    $("#searchBody").hide();
    position = $("#folioSearch").offset()
    $('html, body').animate({ scrollTop: position.top - 480 }, "slow");
    $('#selectOption').val($("#selectOption :first").val()).trigger('change');
}
$("#buttonSave").on("click", function () {
    var idOption = $("#selectOption").val();
    if (idOption.filter(element => element == "0").length == 0) {
        if (!notXMl) {
            $('#UpdateFile').val(null);
            $('#UpdateFile').click();
        }
        else {
            if (xml_permission == 0) { //No es requerido XML (Permite entrad con remision y XML)
                SwalMessage(
                    {
                        text: "¿Desea procesar la orden de compra con xml?", type: "warning", color: "#DD6B55", textOptional: "Si selecciona 'cancelar' se abrirá la opcion de colocar factura"
                    },
                    function () {
                        $('#UpdateFile').val(null);
                        $('#UpdateFile').click();
                    },
                    function () {
                        if (Listoption.Currency == "USD")
                            $("#invoiceUSD").show();
                        else
                            $("#invoiceUSD").hide();
                        $("#dateDllsInvoice").val("");
                        $("#inputInvoice").val("");
                        $("#inputTotalInvoice").val("");
                        $("#ModalInvoice").modal("show");

                    },
                    null
                )
            } else { //xml_permission = 1 ////ES OBLIGATORIO XML
                SwalMessage(
                    {
                        text: "¿Desea procesar la orden?", type: "warning", color: "#DD6B55", textOptional: "Únicamente es permitido registro con XML."
                    },
                    function () {
                        $('#UpdateFile').val(null);
                        $('#UpdateFile').click();
                    },
                    function () { },
                    null
                )
            }
           
        }
    }
    else {
        toastr.warning("Seleccione una opcion valida")
    }
});

function StoreProcedureOptions(isXml, data, Invoice) {
    if (!isXml) {
        if (ErrorListF(data.data.StoreProcedure.ReturnValue == 1005, null, null, "Warning", "Problema xml ya registrado") ||
            ErrorListF(data.data.StoreProcedure.ReturnValue == 1001, null, null, "Warning", "Problema al guardar fecha") ||
            ErrorListF(data.data.StoreProcedure.ReturnValue == 1045, null, null, "Warning", "Problema: valores vacios (nulos) al actualizar") ||
            ErrorListF(data.data.StoreProcedure.ReturnValue == 1006, null, null, "Warning", "Problema al generar diferencia") ||
            ErrorListF(data.data.StoreProcedure.ReturnValue == 7000, null, null, "Warning", `El folio ${Invoice} ya se encuentra registrado. Revise sus ordenes con este mismo proveedor en estatus TERMINADAS.`)) {
            ClearForm();
        }
    }
    if (
        ErrorListF(data.data.StoreProcedure.ReturnValue == 8003, null, null, "Warning", "No existe registro") ||
        ErrorListF(data.data.StoreProcedure.ReturnValue == 8002, null, null, "Warning", "PO no esta confirmada") ||
        ErrorListF(data.data.StoreProcedure.ReturnValue == 8008, null, null, "Warning", "PO cancelada") ||
        ErrorListF(data.data.StoreProcedure.ReturnValue == 8009, null, null, "Warning", "Ya esta finalizada") ||
        ErrorListF(data.data.StoreProcedure.ReturnValue == 8010, null, null, "Warning", "Moneda Nulo") ||
        ErrorListF(data.data.StoreProcedure.ReturnValue == 8011, null, null, "Warning", "Valor Nulo") ||
        ErrorListF(data.data.StoreProcedure.ReturnValue == 8001, null, null, "Warning", "Error desconocido") ||
        ErrorListF(data.data.StoreProcedure.ReturnValue == 8004, null, null, "Warning", "No puede ser mayor a lo requerido") ||
        ErrorListF(data.data.StoreProcedure.ReturnValue == 8006, null, null, "Warning", "No tiene fecha de importación") ||
        ErrorListF(data.data.StoreProcedure.ReturnValue == 8007, null, null, "Warning", "No realizo movimiento") ||
        ErrorListF(data.data.StoreProcedure.ReturnValue == 8005, null, null, "Warning", "No existe locación de los productos, proporcione una locación a los productos.") ||
        ErrorListF(data.data.StoreProcedure.ReturnValue == 7000, null, null, "Warning", `El folio ${Invoice} ya se encuentra registrado. Revise sus ordenes con este mismo proveedor en estatus TERMINADAS.`) ||
        ErrorListF(data.data.StoreProcedure.ReturnValue == 1005, null, null, "Warning", "Problema xml ya registrado") ||
        ErrorListF(data.data.StoreProcedure.ReturnValue == 1001, null, null, "Warning", "Problema al guardar fecha") ||
        ErrorListF(data.data.StoreProcedure.ReturnValue == 1045, null, null, "Warning", "Problema: valores vacios (nulos) al actualizar") ||
        ErrorListF(data.data.StoreProcedure.ReturnValue == 1006, null, null, "Warning", "Problema al generar diferencia") ) {
        ClearForm();
    }
    else if (data.data.StoreProcedure.Document != null && data.data.StoreProcedure.Document != "" && data.data.StoreProcedure.ReturnValue == 0) {
        if (!isXml) {
            if (data.data.Error == 1002) { }
            else
                toastr.success('Se realizo con exito la entrada');
            ClearForm();
        }
        else {
            toastr.success("Se proceso correctamente")
            ClearForm();
            $("#ModalInvoice").modal("hide")
        }
    }
    else if (data.data == "SF") {
        SessionFalse(sesion)
    }
    else {
        toastr.warning('Hubo un problema al procesar la orden de compra13');
        ClearForm();
    }
}

function GenericStoreProcedure(isXml, url, params) {
    StartLoading()
    axios.post(url, params)
        .then(function (data) {
            StoreProcedureOptions(isXml, data, params.Invoice)
        }).catch(function (data) {
            toastr.error('Alerta - Error inesperado  contactar a sistemas.');
        }).then(() => {
            ClearCombo()
            EndLoading()
        })
}

function StoreProcedureNotXMl() {
    var purchaseNo = $("#folioSearch").val();
    var invoice = $("#inputInvoice").val();
    var idOption = $("#selectOption").val();
    var totalInvoice = $("#inputTotalInvoice").val();
    var diference = (total - parseFloat(totalInvoice))
    var dateDlls = $("#dateDllsInvoice").val();

    var currencyCurrent = Listoption.Currency;
    if (idOption.filter(element => element == "0").length > 0) {
        toastr.warning("Seleccione una opcion valida")
    }
    else if (totalInvoice < 0 || isNaN(totalInvoice) || totalInvoice.trim() == "" || totalInvoice == null) {
        toastr.warning("Favor de poner un total valido")
    }
    else if ((Listoption.Currency == "MXN" && diference > 5) || (Listoption.Currency == "USD" && diference > 0.5)) {
        toastr.warning(`El monto de su orden no concuerda con la factura del proveedor. Existe una diferencia de ${diference} favor de editar los costos de la orden de compra.`)
    }
    else if (currencyCurrent == 'USD' && dateDlls == "") {
            toastr.warning(`Ingrese la fecha que aparece en la factura del proveedor.`);
    }
    else if (ErrorListF(invoice.trim() != "", "Warning", "Favor de poner un numero de factura valido", null, null)) {
        $("#ModalInvoice").modal("hide");
        GenericStoreProcedure(notXMl, "/PurchaseOrderItem/ActionNewStoreProcedureOCRGNotXML", { PurchaseNo: purchaseNo, Invoice: invoice, Comment: idOption.reduce((reduce, element) => { return `${element} - ${reduce}` }), Diference: (diference * -1), Total: totalInvoice, dateDlls: dateDlls, currency: currencyCurrent });
    }
}

function StoreProcedure(uuid, fecha, invoice, diference, idOption) {
    var purchaseNo = $("#folioSearch").val()
    StartLoading()
    GenericStoreProcedure(notXMl, "/PurchaseOrderItem/ActionNewStoreProcedureOCRG", { PurchaseNo: purchaseNo, Base64: base64Xml, Uuid: uuid, Date: fecha, Invoice: invoice, Diference: diference, Comment: idOption.reduce((reduce, element) => { return `${element} - ${reduce}` }) });
}

function NewModifyBlind() {
    StartLoading()
    axios.post("/PurchaseOrderItem/ActionNewUpdateOCRG", { OCRC: listBlind, Comment: "" }).then(function (data) {
        if (data.data == "SF") {
            SessionFalse(sesion)
        }
        else if (data.data.Item1) {
            diference = data.data.Item2
            TableORCG()
        }
        else {
            toastr.warning('Hubo un problema al procesar la orden de compra2');
            EndLoading();
        }
    }).catch(function (error) {
        toastr.error('Alerta - Error inesperado  contactar a sistemas.');
        EndLoading();
    })
}

function StatusRC() {
    StartLoading();
    axios.post("/PurchaseOrder/ActionStatusPurchase/", { PurchaseNomber: $("#folioSearch").val(), OCRC: listBlind })
        .then(function (data) {
            EndLoading();
            if (data.data == "SF") {
                SessionFalse(sesion)
            }
            else if (data.data) {
                toastr.success('Se cambio el estatus correctamente. Es posible rescanear.');
                $("#searchBody").hide();
                $("#folioSearch").val("")
            }
            else {
                toastr.warning('Error al cambiar el estatus');
                $("#searchBody").hide();
                $("#folioSearch").val("")
            }
        })
        .catch(function (error) {
            toastr.error('Alerta - Error inesperado  contactar a sistemas.');
        })
}

function XmlCompare() {
    var fileInput = document.getElementById('UpdateFile');
    if (fileInput.files.length == 0) {
        toastr.error("Seleccione archivo XML.");
    }
    else {
        var idxDot = fileInput.files[0].name.lastIndexOf(".") + 1;
        var extFile = fileInput.files[0].name.substr(idxDot, fileInput.files[0].name.length).toLowerCase();
        if (extFile == "xml") {
            var selectedFile = ($("#UpdateFile"))[0].files[0];
            convertToBase64XML(fileInput.files);
            if (selectedFile) {
                var FileSize = 0;
                if (selectedFile.size > 1048576) {
                    FileSize = Math.round(selectedFile.size * 100 / 1048576) / 100 + " MB";
                }
                else if (selectedFile.size > 1024) {
                    FileSize = Math.round(selectedFile.size * 100 / 1024) / 100 + " KB";
                }
                else {
                    FileSize = selectedFile.size + " Bytes";
                }
                if (selectedFile.size < 5242880) {
                    swal({
                        title: "Esta seguro?",
                        text: "De Generar la Orden de Compra!",
                        type: "warning",
                        showCancelButton: true,
                        confirmButtonColor: "#DD6B55",
                        confirmButtonText: "Continuar",
                        cancelButtonText: "Cancelar"
                    },
                        function (isConfirm) {
                            if (isConfirm) {
                                var formdata = new FormData();
                                formdata.append("totalPurchases", total + diference);
                                formdata.append("IVAtotalPurchases", sumTotalIva);
                                formdata.append("IEPStotalPurchases", sumTotalIEPS);
                                formdata.append("subTotalPurchases", sumTotalAmount);
                                var supplier = Listoption.RFC
                                formdata.append("supplier", supplier);
                                var currency = Listoption.Currency
                                formdata.append("currency", currency);
                                formdata.append(fileInput.files[0].name, fileInput.files[0]);
                                var xhr = new XMLHttpRequest();
                                xhr.open('POST', '/Purchases/ActionCompareAmountXmlOCRC001');
                                xhr.send(formdata);
                                xhr.overrideMimeType("application/json");
                                xhr.onreadystatechange = function () {
                                    if (xhr.readyState == 4 && xhr.status == 200) {
                                        var j = JSON.parse(xhr.responseText);
                                        if (j.success == 1) {
                                            if (j.currency == Listoption.Currency) {
                                                var idOption = $("#selectOption").val();
                                                if (j.diferenceTotal > 0) {

                                                    setTimeout(function myfunction() {
                                                        swal({
                                                            title: `Desea procesar la orden de compra con diferencia de $${j.diferenceTotal}?`,
                                                            type: "warning",
                                                            showCancelButton: true,
                                                            confirmButtonColor: "#DD6B55",
                                                            confirmButtonText: "Si",
                                                            cancelButtonText: "No"
                                                        },
                                                            function (isConfirm) {
                                                                if (isConfirm) {
                                                                    notXMl = false;
                                                                    StoreProcedure(j.uuid, j.fecha, j.invoice, j.diferenceTotal, idOption)
                                                                }
                                                                else {
                                                                    return null;
                                                                }
                                                            });
                                                    }, 500)

                                                } else if (j.diferenceIVA > 0) {
                                                    setTimeout(function myfunction() {
                                                        swal({
                                                            title: `Desea procesar la orden de compra con diferencia de $${j.diferenceIVA} en IVA?`,
                                                            type: "warning",
                                                            showCancelButton: true,
                                                            confirmButtonColor: "#DD6B55",
                                                            confirmButtonText: "Si",
                                                            cancelButtonText: "No"
                                                        },
                                                            function (isConfirm) {
                                                                if (isConfirm) {
                                                                    notXMl = false;
                                                                    StoreProcedure(j.uuid, j.fecha, j.invoice, j.diferenceTotal, idOption)
                                                                }
                                                                else {
                                                                    return null;
                                                                }
                                                            });
                                                    }, 500)
                                                } else if (j.diferenceIEPS > 0) {
                                                    setTimeout(function myfunction() {
                                                        swal({
                                                            title: `Desea procesar la orden de compra con diferencia de $${j.diferenceIEPS} en IEPS?`,
                                                            type: "warning",
                                                            showCancelButton: true,
                                                            confirmButtonColor: "#DD6B55",
                                                            confirmButtonText: "Si",
                                                            cancelButtonText: "No"
                                                        },
                                                            function (isConfirm) {
                                                                if (isConfirm) {
                                                                    notXMl = false;
                                                                    StoreProcedure(j.uuid, j.fecha, j.invoice, j.diferenceTotal, idOption)
                                                                }
                                                                else {
                                                                    return null;
                                                                }
                                                            });
                                                    }, 500)
                                                }
                                                else
                                                    StoreProcedure(j.uuid, j.fecha, j.invoice, j.diferenceTotal, idOption);
                                            }
                                            else {
                                                toastr.warning("El xml tiene otro tipo de moneda");
                                            }
                                        }
                                        else if (j.success == 8) {
                                            toastr.error(j.responseText);
                                        }
                                    }
                                }

                            }
                        });
                }
                else {
                    toastr.error('Alerta - La imagen debe ser menor a 5MB');
                }
            }
        }
        else {
            toastr.error("Alerta -Solo se admiten archivos .xml Intentar con otro archivo");
        }
    }
}

function convertToBase64XML(file) {
    var selectedFile = file;
    if (selectedFile.length > 0) {
        var fileToLoad = selectedFile[0];
        var fileReader = new FileReader();
        fileReader.onload = function (fileLoadedEvent) {
            var base64 = window.btoa(fileLoadedEvent.target.result);
            base64Xml = base64
        };
        fileReader.readAsText(fileToLoad);
    }
}

function CancelPurchaseOrder() {
    StartLoading();
    axios.get(`/PurchaseOrderItem/ActionRemovePurchaseOrder/?Purchase=${purchaseValue}`)
        .then(data => {
            if (data.data == "SF") {
                SessionFalse(sesion)
            }
            else if (data.data == 1) {
                toastr.success("Cancelada con exito.");
                ClearForm();
            }
            else if (data.data == 2) {
                toastr.warning("Error al cancelar la orden compra, contacte a sistemas.")
            }
            else {
                toastr.error("Error desconocido contacte a sistemas.")
            }
        })
        .catch(error => {
            toastr.error('Alerta - Error inesperado  contactar a sistemas.');
        })
        .then(() => { ClearForm(); ClearCombo(); EndLoading(); })
}