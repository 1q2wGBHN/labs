﻿/// <reference path="../knockout-3.4.2.debug.js" />

var List = ko.observableArray([]);

function DamagedGoodsHeadViewModel() {
    var self = this;

    self.Document = ko.observable("");
    self.Status = ko.observable();
    self.Comments = ko.observable();
    self.Items = ko.observableArray([]);

    self.GetData = function () {
        $.ajax({
            url: "/DamagedsGoods/ActionGetBanatiDamagedGoods/",
            type: "GET",
            dataType: "json",
            contentType: "application/json; charset=utf-8",
            success: function (response) {
                if (response.result) {
                    var table = $("#Items").DataTable();
                    table.clear().draw();
                    table.rows.add($(response.model.DamagedGoodsBanatiModel));
                    table.columns.adjust().order([1, 'asc']).draw();
                    self.Items.removeAll();

                    var damagedgoodsModel = $("#DamagedGoods").DataTable();
                    damagedgoodsModel.clear().draw();
                    damagedgoodsModel.rows.add($(response.model.DamagedGoodsHeadListModel));
                    damagedgoodsModel.columns.adjust().order([1, 'asc']).draw();
                }
                else
                    SessionFalse("Terminó su sesión.");
            }
        });
    }

    self.Clear = function () {
        self.Document("");
        self.Status("");
        self.Comments("");
        self.Items.removeAll("");
        table.search("");
        DamagedGoods.search("");
        $(".checkAll").iCheck('uncheck');
        $(".i-checks").iCheck('uncheck');
    }

    self.Validate = function () {
        toastr.remove();
        var band = false;
        List = ko.observableArray([]);

        table.rows().every(function (index, element) {
            var row = $(this.node());

            var check = row.find('td').eq(0);
            var isChecked = $('input', check).prop('checked');

            var PartNumber = $(row.find('td').eq(1)).text();

            var Stock = parseInt($(row.find('td').eq(3)).text());

            var quantity = row.find('td').eq(4);
            var quantityInput = $('input', quantity);
            var Quantity = parseInt(quantityInput.val());

            var comment = row.find('td').eq(5);
            var Comment = $('input', comment).val();

            if (isChecked) {
                if (isNaN(Quantity)) {
                    band = true;
                    List.removeAll();
                    toastr.warning('Capture la cantidad de productos.');
                    quantityInput.focus();
                    return false;
                }
                else if (Quantity == 0) {
                    band = true;
                    List.removeAll();
                    toastr.warning('La cantidad de productos debe ser mayor a 0.');
                    quantityInput.focus();
                    return false;
                }
                else if ((isNaN(Quantity)) || (Quantity > Stock)) {
                    band = true;
                    List.removeAll();
                    toastr.warning('La cantidad de productos no puede ser mayor al stock.');
                    quantityInput.focus();
                    quantityInput.val("");
                    return false;
                }
                else
                    List.push({ "PartNumber": PartNumber, "Quantity": Quantity, "Comment": Comment });
            }
            else {
                if (!isNaN(Quantity) && $(quantity).prop("type") != "search") {
                    band = true;
                    List.removeAll();
                    toastr.warning('Seleccione los productos para remisión.');
                    quantity.focus();
                    return false;
                }
            }
        });

        if (!band && List().length > 0) {
            self.Items(List);
            return true;
        }
        else
            return false;
    }

    self.Save = function () {
        if (self.Validate()) {
            swal({
                title: "Esta seguro?",
                text: "Se registrará la remisión",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Continuar",
                cancelButtonText: "Cancelar"
            }, function (isConfirm) {
                if (isConfirm) {
                    StartLoading();
                    if (self.Document() != "")
                        self.Update();
                    else
                        self.Create();
                }
            });
        }
    }

    self.Create = function () {
        $.ajax({
            url: "/DamagedsGoods/ActionCreateBanati/",
            type: "POST",
            data: ko.toJSON({ model: self.Items(), Comments: self.Comments }),
            dataType: "json",
            contentType: "application/json; charset=utf-8",
            success: function (response) {
                EndLoading();
                if (!response.result)
                    SessionFalse("Terminó su sesión");
                else if (response.result & response.Folio != "") {
                    self.Clear();
                    self.GetData();
                    swal({
                        title: "Folio: " + response.Folio,
                        text: "Remisión registrada satisfactoriamente!",
                        type: "success",
                        confirmButtonText: "Continuar"
                    }, function (isConfirm) {
                        if (isConfirm) {
                            $(".document, .status, .remision").addClass("hide");
                            $('html, body').animate({ scrollTop: 0 }, 'slow');
                        }
                    });
                }
                else
                    toastr.error('Ocurrió un error durante el procesamiento de la solicitud.');
            },
            error: function (result) {
                EndLoading();
                toastr.error('Ocurrió un error durante el procesamiento de la solicitud.');
            }
        });
    }

    self.Update = function () {
        $.ajax({
            url: "/DamagedsGoods/ActionUpdateBanati/",
            type: "POST",
            data: ko.toJSON({ model: self.Items(), Comments: self.Comments(), Document: self.Document() }),
            dataType: "json",
            contentType: "application/json; charset=utf-8",
            success: function (response) {
                EndLoading();
                if (!response.result)
                    SessionFalse("Terminó su sesión");
                else if (response.result & response.Folio != "") {
                    self.Clear();
                    self.GetData();
                    swal({
                        title: "Folio: " + response.Folio,
                        text: "Remisión actualizada satisfactoriamente!",
                        type: "success",
                        confirmButtonText: "Continuar"
                    }, function (isConfirm) {
                        if (isConfirm) {
                            $(".document, .status, .remision").addClass("hide");
                            $('html, body').animate({ scrollTop: 0 }, 'slow');
                        }
                    });
                }
                else
                    toastr.error('Ocurrió un error durante el procesamiento de la solicitud.');
            },
            error: function (result) {
                EndLoading();
                toastr.error('Ocurrió un error durante el procesamiento de la solicitud.');
            }
        });
    }

    self.Process = function () {
        if (self.Validate()) {
            swal({
                title: "Esta seguro?",
                text: "Se solicitará la aprobación de la remisión",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Continuar",
                cancelButtonText: "Cancelar"
            }, function (isConfirm) {
                if (isConfirm) {
                    StartLoading();
                    $.ajax({
                        url: "/DamagedsGoods/ActionProcessBanati/",
                        type: "POST",
                        data: ko.toJSON({ model: self.Items(), Comments: self.Comments(), Document: self.Document() }),
                        dataType: "json",
                        contentType: "application/json; charset=utf-8",
                        success: function (response) {
                            EndLoading();
                            if (!response.result)
                                SessionFalse("Terminó su sesión");
                            else if (response.result & response.Folio != "") {
                                self.Clear();
                                self.GetData();
                                swal({
                                    title: "Folio: " + response.Folio,
                                    text: "Remisión creada satisfactoriamente!",
                                    type: "success",
                                    confirmButtonText: "Continuar"
                                }, function (isConfirm) {
                                    if (isConfirm) {
                                        $(".document, .status, .remision").addClass("hide");
                                        $('html, body').animate({ scrollTop: 0 }, 'slow');
                                    }
                                });
                            }
                            else
                                toastr.error('Ocurrió un error durante el procesamiento de la solicitud.');
                        },
                        error: function (result) {
                            EndLoading();
                            toastr.error('Ocurrió un error durante el procesamiento de la solicitud.');
                        }
                    });
                }
            });
        }
    }

    self.Cancel = function () {
        self.Clear();
        self.GetData();
        $(".document, .status, .remision").addClass("hide");
        $('html, body').animate({ scrollTop: 0 }, 'slow');
        $('#tab-2').animatePanel();
    }
}

vm = new DamagedGoodsHeadViewModel();
ko.applyBindings(vm);

window.setTimeout(function () { $("#fa").click(); }, 1000);

$(document).on("keypress", '.Numeric', function (event) {
    var keyValue = event.key;
    if (keyValue.match(/[0-9]/g))
        return true;
    else
        return false;
});

var table = $('#Items').DataTable({
    oLanguage: {
        "sProcessing": "Procesando...", "sLengthMenu": "Mostrar _MENU_ registros", "sZeroRecords": "No se encontraron resultados", "sEmptyTable": "Ningún dato disponible en esta tabla", "sInfo": "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros", "sInfoEmpty": "Mostrando registros del 0 al 0 de un total de 0 registros", "sInfoFiltered": "(filtrado de un total de _MAX_ registros)", "sInfoPostFix": "", "sSearch": "Buscar:", "sUrl": "", "sInfoThousands": ",", "sLoadingRecords": "Cargando...", "oPaginate": { "sFirst": "Primero", "sLast": "Último", "sNext": "Siguiente", "sPrevious": "Anterior" }, "oAria": { "sSortAscending": ": Activar para ordenar la columna de manera ascendente", "sSortDescending": ": Activar para ordenar la columna de manera descendente" }
    },
    responsive: true,
    autoWidth: true,
    order: [[1, "asc"]],
    buttons: [
        { extend: 'csv', text: 'Excel', title: 'RemisionBanati', className: 'btn-sm' },
        {
            extend: 'pdf', text: 'PDF', title: 'RemisionBanati', className: 'btn-sm', exportOptions: {
                columns: [1, 2, 3, 4, 5]
            },
            customize: function (doc) {
                doc.content[1].table.widths =
                    Array(doc.content[1].table.body[0].length + 1).join('*').split('');
            }
        },
        { extend: 'print', text: 'Imprimir', className: 'btn-sm' },
    ],
    columns: [
        {
            "orderable": false,
            "data": 'PartNumber',
            "width": "1%"
        },
        {
            "orderable": true,
            "data": 'PartNumber',
            "width": "1%"
        },
        {
            "orderable": true,
            "data": 'Description',
            "width": "1%"
        },
        {
            "orderable": true,
            "data": 'Stock',
            "width": "1%"
        },
        {
            "orderable": false,
            "data": 'Quantity',
            "width": "1%"
        },
        {
            "orderable": false,
            "data": 'Comment',
            "width": "1%"
        },
    ],
    columnDefs: [{
        targets: [0, 1, 2, 3, 4, 5],
        width: "1%"
    }],
    initComplete: function (settings, json) {
        $(document).on('ifChecked', '.child', function () {
            if (!$(this).is(':checked'))
                $(".checkAll").iCheck('uncheck');
        });

        $(document).on('ifUnchecked', '.child', function () {
            if (!$(this).is(':checked'))
                $(".checkAll").iCheck('uncheck');
        });
    },
    fnDrawCallback: function () {
        $('input[type=checkbox]').iCheck({
            checkboxClass: 'icheckbox_square-green',
        }).on('ifClicked', function () {
            $(this).trigger("change");
        });

        $(".checkAll").on('change', function () {
            if (!$(this).is(':checked'))
                $(".i-checks").iCheck('check');
            else {
                $(".i-checks").iCheck('uncheck');
                var oTable = $('#Items').dataTable();
                oTable.$('input').removeAttr('checked');
            }
        });

        if ($(".checkAll").is(":checked"))
            $(".i-checks").iCheck('check');

        $(".i-checks").on('change', function () {
            if ($(this).is(':checked'))
                $(".checkAll").iCheck('uncheck');
        });
    },
    aoColumnDefs: [
        {
            aTargets: [0],
            className: "text-center",
            mRender: function () {
                return "<div class='checkbox'><label> <input type='checkbox' class='i-checks child'></label></div>"
            }
        },
        {
            aTargets: [4],
            mRender: function () {
                return "<input placeholder='Cantidad' type='text' class='form-control Numeric'>"
            }
        },
        {
            aTargets: [5],
            mRender: function () {
                return "<input placeholder='Comentarios' type='text' class='form-control' maxlength='49'>"
            }
        }]
});

var banatiModel = JSON.parse($("#banatiModel").val());
table.clear();
table.rows.add($(banatiModel));
table.columns.adjust().draw();

function tab1() {
    $('#tab-1').animatePanel();
}

function tab2() {
    $('#tab-2').animatePanel();
}

var DamagedGoods = $("#DamagedGoods").DataTable({
    oLanguage: {
        "sProcessing": "Procesando...", "sLengthMenu": "Mostrar _MENU_ registros", "sZeroRecords": "No se encontraron resultados", "sEmptyTable": "Ningún dato disponible en esta tabla", "sInfo": "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros", "sInfoEmpty": "Mostrando registros del 0 al 0 de un total de 0 registros", "sInfoFiltered": "(filtrado de un total de _MAX_ registros)", "sInfoPostFix": "", "sSearch": "Buscar:", "sUrl": "", "sInfoThousands": ",", "sLoadingRecords": "Cargando...", "oPaginate": { "sFirst": "Primero", "sLast": "Último", "sNext": "Siguiente", "sPrevious": "Anterior" }, "oAria": { "sSortAscending": ": Activar para ordenar la columna de manera ascendente", "sSortDescending": ": Activar para ordenar la columna de manera descendente" }
    },
    responsive: true,
    autoWidth: true,
    order: [[1, "asc"]],
    dom: "<'row'<'col-sm-4'l><'col-sm-4 text-left'B><'col-sm-4'f>t<'col-sm-6'i><'col-sm-6'p>>",
    lengthMenu: [[10, 25, 50, -1], [10, 25, 50, "Todos"]],
    buttons: [
        { extend: 'csv', text: 'Excel', title: 'AprobacionRemision', className: 'btn-sm' },
        {
            extend: 'pdf', text: 'PDF', title: 'AprobacionRemision', className: 'btn-sm', exportOptions: {
                columns: [1, 2, 3, 4]
            },
            customize: function (doc) {
                doc.content[1].table.widths =
                    Array(doc.content[1].table.body[0].length + 1).join('*').split('');
            }
        },
        { extend: 'print', text: 'Imprimir', className: 'btn-sm' },
    ],
    columns: [
        {
            "class": "details-control",
            "orderable": false,
            "data": null,
            "defaultContent": "",
            "width": "1%"
        },
        { data: "DamagedGoodsDoc", width: "1%" },
        { data: "Supplier", width: "1%" },
        { data: "Comment", width: "1%" },
        { data: "ProcessStatusDescription", width: "1%" },
        {
            orderable: false,
            data: null,
            width: "1%",
            className: "text-center",
            render: function (data, type, full, meta) {
                if (full.ProcessStatusDescription == "Inicial")
                    return "<button class='btn btn-xs btn-outline btn-warning' onclick='EditRecord(" + full.DamagedGoodsDoc + ")'><i class='fa fa-info-circle'></i> Editar</button>";
                else
                    return "";
            }
        }
    ],
    columnDefs: [{
        targets: [0, 1, 2, 3, 4, 5],
        width: "1%"
    }]
});

var damagedgoodsModel = JSON.parse($("#DamagedGoodsHeadListModel").val());
DamagedGoods.clear();
DamagedGoods.rows.add($(damagedgoodsModel));
DamagedGoods.columns.adjust().draw();

var detailRows = [];

$("#DamagedGoods tbody").on("click", "tr td.details-control", function () {
    var tr = $(this).closest("tr");
    var row = DamagedGoods.row(tr);

    if (row.child.isShown()) {
        row.child.hide();
        tr.removeClass("details");
    }
    else {
        if (DamagedGoods.row(".details").length) {
            $(".details-control", DamagedGoods.row(".details").node()).click();
        }
        row.child(format(row.data())).show();
        tr.addClass("details");
    }
});

DamagedGoods.on("draw", function () {
    $.each(detailRows, function (i, id) {
        $("#" + id + " td.details-control").trigger("click");
    });
});

function reloadStyleTable() {
    $("#tabledetail").DataTable({
        oLanguage: {
            "sProcessing": "Procesando...", "sLengthMenu": "Mostrar _MENU_ registros", "sZeroRecords": "No se encontraron resultados", "sEmptyTable": "Ningún dato disponible en esta tabla", "sInfo": "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros", "sInfoEmpty": "Mostrando registros del 0 al 0 de un total de 0 registros", "sInfoFiltered": "(filtrado de un total de _MAX_ registros)", "sInfoPostFix": "", "sSearch": "Buscar:", "sUrl": "", "sInfoThousands": ",", "sLoadingRecords": "Cargando...", "oPaginate": { "sFirst": "Primero", "sLast": "Último", "sNext": "Siguiente", "sPrevious": "Anterior" }, "oAria": { "sSortAscending": ": Activar para ordenar la columna de manera ascendente", "sSortDescending": ": Activar para ordenar la columna de manera descendente" }
        }
    });
}

var list = [];
function format(d) {
    list = [];
    var detailItems = "";
    var tabledetail = $("<div/>").addClass("loading").text("Cargando Datos...");
    $.ajax({
        url: "/DamagedsGoods/ActionGetItemSupplierByRma",
        data: { "Folio": d.DamagedGoodsDoc, "Type": "Remision", "Status": 0 },
        type: "GET",
        success: function (data) {
            if (data != "SF") {
                $.each(data, function (index, value) {
                    detailItems += "<tr><td>" + value.PartNumber + "</td><td>" + value.Description + "</td><td>" + value.Size + "</td><td class='text-right'>" + value.Storage + "</td><td class='text-right'>" + value.Quantity + "</td><td class='text-right'>" + "$" + value.Amount_Doc.toFixed(4) + "</td><td class='text-right'>" + "$" + value.Price.toFixed(4) + "</td></tr>"
                });

                tabledetail.html('<table id="tabledetail" class="table table-striped table-bordered table-hover" style="width:100%"><thead><tr><th>Codigo</th><th>Nombre</th><th>UM</th><th>Locacion</th><th>Cantidad</th><th>Monto</th><th>Precio</th></tr></thead><tbody>' + detailItems + '</tbody></table>').removeClass('loading');

                reloadStyleTable();
            }
            else
                SessionFalse("Terminó su sesión");
        }
    });
    return tabledetail;
}

function EditRecord(id) {
    $.ajax({
        url: "/DamagedsGoods/ActionGetRmaItems",
        type: "POST",
        data: { "Folio": id, "Type": "Remision", "Status": 0 },
        success: function (response) {
            if (response.result == "SF")
                SessionFalse("Terminó su sesión");
            else if (response.result) {
                vm.Clear();
                vm.Document(response.model.DamagedGoodsHeadModel.DamagedGoodsDoc);
                vm.Status(response.model.DamagedGoodsHeadModel.ProcessStatusDescription);
                vm.Comments(response.model.DamagedGoodsHeadModel.Comment);

                response.model.DamagedGoodsBanatiModel.forEach(function (item) {

                    table.rows().every(function (index, element) {
                        var row = $(this.node());

                        var check = row.find('td').eq(0);
                        var checkInput = $('input', check);

                        var PartNumber = $(row.find('td').eq(1)).text();

                        var quantity = row.find('td').eq(4);
                        var quantityInput = $('input', quantity);

                        var comment = row.find('td').eq(5);
                        var commentInput = $('input', comment);

                        if (PartNumber == item.PartNumber) {
                            checkInput.prop("checked", true);
                            quantityInput.val(item.Quantity);
                            commentInput.val(item.Comment);
                        }
                    });
                });

                table.order([1, 'asc']).draw();
                $('#tab2').click();
                $('html, body').animate({ scrollTop: 0 }, 'slow');
                $(".document, .status, .remision").removeClass("hide");
            }
            else
                toastr.error('Alerta - Error inesperado contactar a sistemas.');
        },
        error: function () {
            toastr.error('Alerta - Error inesperado contactar a sistemas.');
        }
    });
}

$("#ValidDate1").datepicker({
    autoclose: true,
    todayHighlight: true,
    showButtonPanel: true,
}).on("changeDate", function (e) {
    SerchRMA();
});

$("#ValidDate2").datepicker({
    autoclose: true,
    startDate: '01/01/1950',
    todayHighlight: true,
}).on("changeDate", function (e) {
    SerchRMA();
});

function SerchRMA() {
    if ($("#ValidDate1").val() != "" & $("#ValidDate2").val() != "") {
        if (moment($("#ValidDate1").val()) <= moment($("#ValidDate2").val())) {
            $.ajax({
                type: "GET",
                url: "/DamagedsGoods/ActionGetRMA",
                data: { "Type": "REMISION", "Date1": $("#ValidDate1").val(), "Date2": $("#ValidDate2").val(), "Status": 0 },
                success: function (result) {
                    if (result.success) {
                        DamagedGoods.clear();
                        DamagedGoods.rows.add($(result.Json));
                        DamagedGoods.columns.adjust().draw();
                        $("#TableDiv").animatePanel();
                    }
                    else
                        toastr.warning("Alerta - Error inesperado.");
                },
                error: function (result) {
                    toastr.error("Alerta - Error inesperado contactar a sistemas.");
                }
            });
        }
        else {
            toastr.error("Fecha final no puede ser menor a la inicial.");
            document.getElementById("ValidDate1").value = "";
            document.getElementById("ValidDate2").value = "";
            $("#ValidDate1").focus();
        }
    }
}