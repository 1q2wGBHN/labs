﻿const formatmoney = money => decimal => new Intl.NumberFormat("en-US", { style: "currency", currency: "USD", currencyDisplay: "symbol", maximumFractionDigits: decimal }).format(money);

$(document).ready(function () {
    window.setTimeout(function () { $("#fa").click(); }, 1000);
});

$("#ValidDate1").datepicker({
    autoclose: true,
    todayHighlight: true,
    showButtonPanel: true,
}).on("changeDate", function (e) {
    if ((moment($('#ValidDate1').val()) <= moment($('#ValidDate2').val()))) {
        SearchInventoryCC();
    }
});

$("#ValidDate2").datepicker({
    autoclose: true,
    startDate: '01/01/1950',
    todayHighlight: true,
}).on("changeDate", function (e) {
    SearchInventoryCC();
});

function SerchClear() {
    ["ValidDate1", "ValidDate2", "productMovementType", "productStatus"].map(id => $(`#${id}`).val(""))
    $("#ItemsDrop").val("").trigger("change");
    $("#productMovementType").val("").trigger("change");
    $("#productStatus").val("").trigger("change");
}

$("#ItemsDrop").select2({
    minimumInputLength: 3,
    allowClear: true,
    placeholder: "Seleccione un producto",
    initSelection: function (element, callback) {
        callback({ id: "", text: "Seleccione un producto" });
    },
    ajax: {
        url: "/Items/ActionSearchItemBarcode/",
        dataType: 'json',
        type: "GET",
        quietMillis: 50,
        data: function (Filter) {
            return {
                Filter: Filter
            };
        },
        results: function (data) {
            return {
                results: $.map(data.Json, function (item) {
                    return {
                        text: `${item.PathNumber} - ${item.Description}`,
                        id: item.PathNumber
                    }
                }),
            };
        }
    }
});


function SearchInventoryCC() {
    if (moment($('#ValidDate1').val()) >= moment($('#ValidDate2').val())) {
        toastr.error("Fecha final no puede ser menor a la inicial.");
        document.getElementById('ValidDate1').value = '';
        document.getElementById('ValidDate2').value = '';
        $('#ValidDate1').focus();
        return null;
    }
    StartLoading();
    $.ajax({
        type: "POST",
        url: "/Items/ActionGetAllMovementsInventoryCC",
        data: { "date1": $('#ValidDate1').val(), "date2": $('#ValidDate2').val(), "status": $('#productStatus').val(), "part_number": $('#ItemsDrop').val(), "type_movent": $('#productMovementType').val() },
        success: function (returndate) {
            if (!returndate.success) {
                if (returndate.responseText == "SF")
                    SessionFalse("Terminó tu sesión");
                else
                    toastr.warning(returndate.responseText);
            }
            else if (returndate.success) {
                table.clear();
                table.rows.add($(returndate.ItemMovements));
                table.columns.adjust().draw();
            }
        },
        error: function (returndate) {
            toastr.error('Alerta - Error inesperado contactar a sistemas.');
        }
    });
    EndLoading();
}

var table = $('#TableInventoryCC').DataTable({
    oLanguage:
    {
        "sProcessing": "Procesando...",
        "sLengthMenu": "Mostrar _MENU_ registros",
        "sZeroRecords": "No se encontraron resultados",
        "sEmptyTable": "Ningún dato disponible en esta tabla",
        "sInfo": "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
        "sInfoEmpty": "Mostrando registros del 0 al 0 de un total de 0 registros",
        "sInfoFiltered": "(filtrado de un total de _MAX_ registros)",
        "sInfoPostFix": "",
        "sSearch": "Buscar:",
        "sUrl": "",
        "sInfoThousands": ",",
        "sLoadingRecords": "Cargando...",
        "oPaginate": { "sFirst": "Primero", "sLast": "Último", "sNext": "Siguiente", "sPrevious": "Anterior" },
        "oAria": { "sSortAscending": ": Activar para ordenar la columna de manera ascendente", "sSortDescending": ": Activar para ordenar la columna de manera descendente" }
    },
    dom: "<'row'<'col-sm-4'l><'col-sm-4 text-left'B><'col-sm-4'f>t<'col-sm-6'i><'col-sm-6'p>>",
    lengthMenu: [[10, 25, 50, -1], [10, 25, 50, "Todos"]],
    buttons: [
        { extend: 'csv', text: 'Excel', title: 'AjusteInventario', className: 'btn-sm' },
        {
            extend: 'pdf', text: 'PDF', title: 'AjusteInventario', className: 'btn-sm', exportOptions: {
                columns: [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13]
            },
            customize: function (doc) {
                doc.content[1].table.widths =
                    Array(doc.content[1].table.body[0].length + 1).join('*').split('');
            }
        },
        { extend: 'print', text: 'Imprimir', className: 'btn-sm' }
    ],
    columns:
        [
            { data: 'DocumentStatus' },
            { data: 'MovementDocument' },
            { data: 'MovementReference' },
            { data: 'MovementDate' },
            { data: 'PartNumber' },
            { data: 'PartDescription' },
            { data: 'StorageLocation' },
            { data: 'DebitCredit' },
            { data: 'StorageLocationStock' },
            { data: 'MovementQuantity' },
            { data: 'TotalStock' },
            { data: 'Reason', },
            { data: 'AuthorizationBy' },
            { data: 'CreateBy' },
            { data: null, width: "1%" }
        ],
    columnDefs:
        [
            {
                targets: [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14],
                width: "1%"
            },
            {
                render: function (data, type, row) {
                    if (data == "Pendiente")
                        return `<strong > ${data}</strong >`
                    else if (data == "Rechazado")
                        return `<strong style = "Color:red;" > ${data}</strong >`
                    else if (data == "Aprobado")
                        return `<strong style = "Color:green;" > ${data}</strong >`
                },
                targets: [0]
            },
            {
                render: function (data, type, row) {
                    if (data == "" || data == null)
                        return "Sin Moviento."
                    else
                        return data;
            },
                targets: [1, 3, 12]
            },
            {
                render: function (data, type, row) {
                    return `<button class="btn btn-xs btn-outline btn-info" onclick="PrintReport('${row.MovementReference}', '${row.PartNumber}')"><i class="fa fa-save"></i> Detalles</button>`
                },
                targets: [14]
            }
        ]
});


function PrintReport(document, part_number) {
    StartLoading();
    $.ajax({
        url: "/Items/ActionReportInventoryCC",
        type: "GET",
        data: { "document": document, "part_number": part_number },
        success: function (response) {
            EndLoading();
            if (response.success) {
                $("#modalReferenceBody").html("<iframe width='100%' height='550px' src='data:application/pdf;base64, " + encodeURI(response.responseText) + "'></iframe>");
                $("#modalReference").modal('show');
            }
            else {
                if (response.responseText == "SF")
                    SessionFalse(response.responseText);
                else
                    toastr.error(response.responseText);
            }
        },
        error: function () {
            toastr.error('Error inesperado contactar a sistemas.');
            EndLoading();
        }
    });
}