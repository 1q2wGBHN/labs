﻿const sesion = "Se termino su sesion.";
var devolution_global = '';
$(document).ready(function () {
    window.setTimeout(function () { $("#fa").click(); }, 1000);
    var RMAs = JSON.parse($("#model").val());
    table.clear();
    table.rows.add($(RMAs));
    table.columns.adjust().draw();

    $('input[type=checkbox]').iCheck({
        checkboxClass: 'icheckbox_square-green',
    }).on('ifClicked', function () {
        $(this).trigger("change");
    });

    $(".checkbox").iCheck('check');
});

$('#saveButton').click(function () {
    swal({
        title: "Desea procesar la solicitud?",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "Si",
        cancelButtonText: "No",
        closeOnConfirm: true,
        closeOnCancel: true
    }, function (isConfirm) {
        if (isConfirm)
            Aproval();
    });
});

$("#ValidDate1").datepicker({
    autoclose: true,
    todayHighlight: true,
    showButtonPanel: true,
}).on("changeDate", function (e) {
    SerchRMA();
});

$("#ValidDate2").datepicker({
    autoclose: true,
    startDate: '01/01/1950',
    todayHighlight: true,
}).on("changeDate", function (e) {
    SerchRMA();
});

var table = $('#TablePurchases').DataTable({
    oLanguage: {
        "sProcessing": "Procesando...", "sLengthMenu": "Mostrar _MENU_ registros", "sZeroRecords": "No se encontraron resultados", "sEmptyTable": "Ningún dato disponible en esta tabla", "sInfo": "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros", "sInfoEmpty": "Mostrando registros del 0 al 0 de un total de 0 registros", "sInfoFiltered": "(filtrado de un total de _MAX_ registros)", "sInfoPostFix": "", "sSearch": "Buscar:", "sUrl": "", "sInfoThousands": ",", "sLoadingRecords": "Cargando...", "oPaginate": { "sFirst": "Primero", "sLast": "Último", "sNext": "Siguiente", "sPrevious": "Anterior" }, "oAria": { "sSortAscending": ": Activar para ordenar la columna de manera ascendente", "sSortDescending": ": Activar para ordenar la columna de manera descendente" }
    },
    dom: "<'row'<'col-sm-4'l><'col-sm-4 text-left'B><'col-sm-4'f>t<'col-sm-6'i><'col-sm-6'p>>",
    lengthMenu: [[10, 25, 50, -1], [10, 25, 50, "Todos"]],
    buttons: [
        { extend: 'csv', text: 'Excel', title: 'DevolucionProveedor', className: 'btn-sm' },
        {
            extend: 'pdf', text: 'PDF', title: 'DevolucionProveedor', className: 'btn-sm', exportOptions: {
                columns: [1, 2, 3]
            },
            customize: function (doc) {
                doc.content[1].table.widths =
                    Array(doc.content[1].table.body[0].length + 1).join('*').split('');
            }
        },
        { extend: 'print', text: 'Imprimir', className: 'btn-sm' },
    ],
    columns: [
        {
            "class": "details-control",
            "orderable": false,
            "data": null,
            "defaultContent": "",
        },
        { data: 'DamagedGoodsDoc' },
        { data: 'Supplier' },
        { data: 'Comment' },
        { data: 'ProcessStatusDescription' }
    ],
    columnDefs: [{
        targets: [0, 1, 2, 3, 4],
        width: "1%"
    }]
});

var detailRows = [];

$('#TablePurchases tbody').on('click', 'tr td.details-control', function () {
    var tr = $(this).closest('tr');
    var row = table.row(tr);

    if (row.child.isShown()) {
        row.child.hide();
        tr.removeClass('details');
    }
    else {
        if (table.row('.details').length) {
            $('.details-control', table.row('.details').node()).click();
        }
        row.child(format(row.data())).show();
        tr.addClass('details');
    }
});

table.on('draw', function () {
    $.each(detailRows, function (i, id) {
        $('#' + id + ' td.details-control').trigger('click');
    });
});

function format(d) {
    StartLoading();
    var detailItems = "";
    var tabledetail = $('<div/>').addClass('loading').text('Cargando Datos...');
    $.ajax({
        url: "/DamagedsGoods/ActionGetItemSupplierByRma",
        data: { "Folio": d.DamagedGoodsDoc, "Type": "RMA", "Status": 2 },
        type: "GET",
        success: function (data) {
            $.each(data, function (index, value) {
                total = value.Iva + value.Ieps + value.Amount_Doc;
                detailItems += "<tr><td>" + value.PartNumber + "</td><td>" + value.Description + "</td><td>" + value.Size + "</td><td class='text-right'>" + value.Storage + "</td><td class='text-right'>" + value.Quantity + "</td><td class='text-right'>" + "$" + value.Price.toFixed(4) + "</td><td class='text-right'>" + "$" + value.Amount_Doc.toFixed(4) + "</td><td class='text-right'>" + "$" + value.Ieps + "</td><td class='text-right'>" + "$" + value.Iva + "</td><td class='text-right'>" + "$" + total.toFixed(4) + "</td></tr>"
            });
            var Apro = "";
            if (d.ProcessStatusDescription == "Aprobada") {
                Apro = `<div class='form-group col-md-12'> <div class='row'> <div class='col-md-12 text-left'><button id='btn-cancel' type='button' class='btn btn-sm btn-danger' onclick='CancelRMA("${d.DamagedGoodsDoc}", "${d.Supplier}");'> <i class='fa fa-close'> Cancelar </i> </button> </div>     <div class='col-md-12 text-right'><button id='btn-save' type='button' class='btn btn-sm btn-success' onclick='Aproval("${d.DamagedGoodsDoc}");'><i class='fa fa-check'></i> Aplicar</button></div></div></div>`;
            }
            tabledetail.html('<table id="tabledetail" class="table table-striped table-bordered table-hover" style="width:100%"><thead><tr><th>Codigo</th><th>Nombre</th><th>UM</th><th>Locacion</th><th>Cantidad</th><th>Precio</th><th>Monto</th><th>IEPS</th><th>IVA</th><th>Total</th></tr></thead><tbody>' + detailItems + '</tbody></table>' + Apro).removeClass('loading');
            reloadStyleTable();
            EndLoading();
        }
    });
    return tabledetail
}



function SerchRMA() {
    table.clear();
    $("#saveButton").hide()
    if ($('#ValidDate1').val() != "" & $('#ValidDate2').val() != "") {
        if (moment($('#ValidDate1').val()) <= moment($('#ValidDate2').val())) {
            $('#TableDiv').animatePanel();
            $.ajax({
                type: "GET",
                url: "/DamagedsGoods/ActionGetRMA",
                data: { "Type": "RMA", "Date1": $('#ValidDate1').val(), "Date2": $('#ValidDate2').val(), "Status": 2 },
                success: function (returndate) {
                    if (returndate.success) {
                        table.clear();
                        table.rows.add($(returndate.Json));
                        table.columns.adjust().draw();
                        if (returndate.Json.length > 0) {
                            $("#saveButton").show();
                        }
                    }
                    else
                        toastr.warning('Alerta - Error inesperado!!');
                },
                error: function (returndate) {
                    toastr.error('Alerta - Error inesperado contactar a sistemas.');
                }
            });
        }
        else
            toastr.warning('la fecha inicial no puede ser mayor a la fecha final');
    }
}

function reloadStyleTable() {
    $('#tabledetail').DataTable({
        oLanguage: {
            "sProcessing": "Procesando...", "sLengthMenu": "Mostrar _MENU_ registros", "sZeroRecords": "No se encontraron resultados", "sEmptyTable": "Ningún dato disponible en esta tabla", "sInfo": "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros", "sInfoEmpty": "Mostrando registros del 0 al 0 de un total de 0 registros", "sInfoFiltered": "(filtrado de un total de _MAX_ registros)", "sInfoPostFix": "", "sSearch": "Buscar:", "sUrl": "", "sInfoThousands": ",", "sLoadingRecords": "Cargando...", "oPaginate": { "sFirst": "Primero", "sLast": "Último", "sNext": "Siguiente", "sPrevious": "Anterior" }, "oAria": { "sSortAscending": ": Activar para ordenar la columna de manera ascendente", "sSortDescending": ": Activar para ordenar la columna de manera descendente" }
        }
    });
}

function CancelMessage(s, g) {
    swal({
        title: "¿Esta seguro que desea Rechazar?",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "Continuar",
        cancelButtonText: "Cancelar"
    }, function (isConfirm) {
        if (isConfirm)
            Aproval(false, s, g);
    });
}

function AprovalMessage(s, g) {
    swal({
        title: "¿Esta seguro que deseas confirmar?",
        type: "info",
        showCancelButton: true,
        confirmButtonColor: "#55dd6b",
        cancelButtonText: "Cancelar",
        confirmButtonText: "Continuar"
    }, function (isConfirm) {
        if (isConfirm)
            Aproval(true, s, g);
    });
}

function Aproval(RMA) {
    swal({
        title: "¿Desea procesar la solicitud?",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "Si",
        cancelButtonText: "No",
        closeOnConfirm: true,
        closeOnCancel: true
    }, function (isConfirm) {
        if (isConfirm) {
            if (RMA != null) {
                StartLoading()
                axios.post("/DamagedsGoods/ActionDamagedGoodsApprovedEmailSingle/", { RMA: [RMA] })
                    .then(function (data) {
                        if (data.data == "SF")
                            SessionFalse(sesion)
                        else if (data.data.value == "N/A") {
                            table.clear().draw();
                            $("#saveButton").hide()
                            if (data.data.error > 0) {
                                toastr.error(ErrorListTr(data.data.error))
                            }
                            else
                                toastr.warning('Correo no pudo ser mandado a ventas porque no se tiene correo registrado');
                        }
                        else if (data.data.value == "N/S") {
                            table.clear().draw();
                            $("#saveButton").hide()
                            if (data.data.error > 0) {
                                toastr.error(ErrorListTr(data.data.error))
                            }
                            else
                                toastr.warning('Correo no pudo ser mando no tiene ningun RMA sin errores');
                        }
                        else if (data.data.value) {
                            table.clear().draw();
                            $("#saveButton").hide()
                            if (data.data.error > 0) {
                                toastr.error(ErrorListTr(data.data.error))
                            }
                            else {
                                toastr.success('Correo mandado con exito.');
                                $("#modalReferenceBody").html(`<iframe width='100%' height='550px' src='data:application/pdf;base64,${data.data.response}'></iframe>`);
                                $("#modalReference").modal('show');
                            }
                        }
                        else
                            toastr.error('Alerta - Error inesperado  contactar a sistemas.');
                    })
                    .catch(function (error) {
                        console.log(error.data)
                    }).then(function () {
                        DateForAjax()
                        EndLoading();
                    })
            }
            else
                toastr.warning('Favor seleccionar un RMA');
        }
    });
}

function ErrorListTr(error) {
    if (error == 8001)
        return "Error: Inventario insuficiente"
    else if (error == 6001)
        return "Error: Site Code Incorrecto"
    else if (error == 6002)
        return "Error: No tiene estatus Aprovado"
    else if (error == 6003)
        return "Error: Locacion invalida"
    else
        return "Movimiento exitoso"
}

function DateForAjax() {
    $.ajax({
        type: "GET",
        url: "/DamagedsGoods/ActionGetRMA",
        data: { "Type": "RMA", "Date1": moment().add(-7, 'days').format("MM/DD/YYYY"), "Date2": moment().format("MM/DD/YYYY"), "Status": 2 },
        success: function (returndate) {
            if (returndate.success) {
                table.clear();
                table.rows.add($(returndate.Json));
                table.columns.adjust().draw();
                if (returndate.Json.length > 0) {
                    $("#saveButton").show()
                }
            }
            else
                toastr.warning('Alerta - Error inesperado!!');
        },
        error: function (returndate) {
            toastr.error('Alerta - Error inesperado contactar a sistemas.');
        }
    });
}

function CancelRMA(folio, supplier) {
    devolution_global = folio
    $("#inputComment").val("");
    $("#ModalTitlePurchase").html('Devolución: ' + folio);
    $("#ModalTextPurchase").html('Cancelacion de Devolución: ' + supplier);
    $("#ModalInvoice").modal("show")
}

function MethodCancelRMA() {
    var comment = $("#inputComment").val();
    if (comment.trim().length >= 8) {
        swal({
            title: "¿Desea cancelar la devolución?",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Si",
            cancelButtonText: "No",
            closeOnConfirm: true,
            closeOnCancel: true
        }, function (isConfirm) {
            if (isConfirm) {
                StartLoading()



                axios.post("/DamagedsGoods/ActionRmaAbortCancel/", { Folio: devolution_global, Comment: comment })
                    .then(function (data) {
                        if (data.data == "SF")
                            SessionFalse(sesion)
                        else if (data.data.success) {
                            table.clear().draw();
                            $("#ModalInvoice").modal("hide")
                            toastr.success('Devolución Cancelada correctamente');
                        }
                        else
                            toastr.error('Alerta - Error inesperado  contactar a sistemas.');
                    })
                    .catch(function (error) {
                        console.log(error.data)
                    }).then(function () {
                        DateForAjax()
                        EndLoading();
                    })
            }
        });
    } else
        toastr.warning("Ingrese minimo 8 caracteres para cancelar la devolución");
}