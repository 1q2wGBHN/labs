﻿$(document).ready(function () {
    window.setTimeout(function () { $("#fa").click(); }, 1000);
});

$("#SuppliersDrop").select2({
    minimumInputLength: 3,
    allowClear: true,
    placeholder: "Seleccione una producto",
    initSelection: function (element, callback) {
        callback({ id: "0", text: "Seleccione una opcion" });
    },
    ajax: {
        url: "/Suppliers/ActionSearchSupplier/",
        dataType: 'json',
        type: "GET",
        quietMillis: 50,
        data: function (Filter) {
            return {
                Filter: Filter
            };
        },
        results: function (data) {
            return {
                results: $.map(data.Json, function (item) {
                    return {
                        text: `${item.BusinessName}`,
                        id: item.SupplierId
                    }
                }),
            };
        }
    }
});

function numberWithCommas(x) {
    return x.toLocaleString(
        undefined, // leave undefined to use the browser's locale,
        // or use a string like 'en-US' to override it.
        { minimumFractionDigits: 2 }
    );
}

var table;
var rol = "Distrital";
var roles = JSON.parse($("#UserRoles").val());

const fomartmoney = money => decimal => new Intl.NumberFormat("en-US", { style: "currency", currency: "USD", currencyDisplay: "symbol", maximumFractionDigits: decimal }).format(money);

if (roles.includes(rol)) {
    $("#TablePurchases").append('<tfoot><th>Total</th><th colspan = "3"><th><th></th><th></th><th></th></tfoot>');

    table = $('#TablePurchases').DataTable({
        oLanguage: {
            "sProcessing": "Procesando...", "sLengthMenu": "Mostrar _MENU_ registros", "sZeroRecords": "No se encontraron resultados", "sEmptyTable": "Ningún dato disponible en esta tabla", "sInfo": "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros", "sInfoEmpty": "Mostrando registros del 0 al 0 de un total de 0 registros", "sInfoFiltered": "(filtrado de un total de _MAX_ registros)", "sInfoPostFix": "", "sSearch": "Buscar:", "sUrl": "", "sInfoThousands": ",", "sLoadingRecords": "Cargando...", "oPaginate": { "sFirst": "Primero", "sLast": "Último", "sNext": "Siguiente", "sPrevious": "Anterior" }, "oAria": { "sSortAscending": ": Activar para ordenar la columna de manera ascendente", "sSortDescending": ": Activar para ordenar la columna de manera descendente" }
        },
        dom: "<'row'<'col-sm-4'l><'col-sm-4 text-left'B><'col-sm-4'f>t<'col-sm-6'i><'col-sm-6'p>>",
        lengthMenu: [[10, 25, 50, -1], [10, 25, 50, "Todos"]],
        buttons: [
            { extend: 'csv', text: 'Excel', title: 'Existencias', className: 'btn-sm', footer: true },
            {
                extend: 'pdf', text: 'PDF', title: 'Existencias', className: 'btn-sm', exportOptions: {
                    columns: [0, 1, 2, 3, 4, 5, 6, 7]
                }, footer: true,
                customize: function (doc) {
                    doc.content[1].table.widths =
                        Array(doc.content[1].table.body[0].length + 1).join('*').split('');
                }
            },
            { extend: 'print', text: 'Imprimir', className: 'btn-sm' },
        ],
        "footerCallback": function (row, data, start, end, display) {
            var api = this.api(), data;
            // converting to interger to find total
            var intVal = function (i) {
                return typeof i === 'string' ?
                    i.replace(/[\$,]/g, '') * 1 :
                    typeof i === 'number' ?
                        i : 0;
            };
            for (var i = 5; i < 8; i++) {
                if (i != 6) {
                    var monTotal = api
                        .column(i)
                        .data()
                        .reduce(function (a, b) {
                            if (b > 0)
                                return intVal(a) + intVal(b);
                            else
                                return intVal(a);
                        }, 0);
                    if (i != 5)
                        $(api.column(i).footer()).html(`<span ${monTotal == 0 ? "" : `style = "Color:${monTotal >= 0 ? "green" : "red"}"`} > ${fomartmoney(monTotal)(4)}</span >`);
                    else
                        $(api.column(i).footer()).html(`<span ${monTotal == 0 ? "" : `style = "Color:${monTotal >= 0 ? "green" : "red"}"`} > <i class='fa fa-cubes'></i> ${numberWithCommas(monTotal)}</span >`);
                }
            }
        },
        columns: [
            { data: 'PathNumber' },
            { data: 'Description' },
            { data: 'department' },
            { data: 'family' },
            { data: 'Storage_name' },
            { data: 'Quantity' },
            { data: 'SalePrice' },
            { data: 'Amount' }
        ],
        columnDefs: [{
            targets: [0, 1, 2, 3, 4, 5, 6, 7],
            width: "1%"
        },
        {
            type: 'numeric-comma',
            render: function (data, type, row) {
                return fomartmoney(data)(4);
            },
            targets: [6]
        },
        {
            type: 'numeric-comma',
            render: function (data, type, row) {
                if (data > 0)
                    return fomartmoney(data)(4);
                else
                    return '$0.00';
            },
            targets: [7]
        }]
    });
}
else {
    table = $('#TablePurchases').DataTable({
        oLanguage: {
            "sProcessing": "Procesando...", "sLengthMenu": "Mostrar _MENU_ registros", "sZeroRecords": "No se encontraron resultados", "sEmptyTable": "Ningún dato disponible en esta tabla", "sInfo": "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros", "sInfoEmpty": "Mostrando registros del 0 al 0 de un total de 0 registros", "sInfoFiltered": "(filtrado de un total de _MAX_ registros)", "sInfoPostFix": "", "sSearch": "Buscar:", "sUrl": "", "sInfoThousands": ",", "sLoadingRecords": "Cargando...", "oPaginate": { "sFirst": "Primero", "sLast": "Último", "sNext": "Siguiente", "sPrevious": "Anterior" }, "oAria": { "sSortAscending": ": Activar para ordenar la columna de manera ascendente", "sSortDescending": ": Activar para ordenar la columna de manera descendente" }
        },
        dom: "<'row'<'col-sm-4'l><'col-sm-4 text-left'B><'col-sm-4'f>t<'col-sm-6'i><'col-sm-6'p>>",
        lengthMenu: [[10, 25, 50, -1], [10, 25, 50, "Todos"]],
        buttons: [
            {
                extend: 'csv', text: 'Excel', title: 'Existencias', className: 'btn-sm', exportOptions: {
                    columns: [0, 1, 2, 3, 4, 5]
                }
            },
            {
                extend: 'pdf', text: 'PDF', title: 'Existencias', className: 'btn-sm', exportOptions: {
                    columns: [0, 1, 2, 3, 4, 5]
                }
            },
            {
                extend: 'print', text: 'Imprimir', className: 'btn-sm', exportOptions: {
                    columns: [0, 1, 2, 3, 4, 5]
                }
            },
        ],
        columns: [
            { data: 'PathNumber' },
            { data: 'Description' },
            { data: 'department' },
            { data: 'family' },
            { data: 'Storage_name' },
            { data: 'Quantity' },
            { data: 'SalePrice' },
            { data: 'Amount' }
        ],
        columnDefs: [{
            targets: [0, 1, 2, 3, 4, 5, 6, 7],
            width: "1%"
        },
        {
            visible: false,
            type: 'numeric-comma',
            render: function (data, type, row) {
                return fomartmoney(data)(4);
            },
            targets: [6]
        },
        {
            visible: false,
            type: 'numeric-comma',
            render: function (data, type, row) {
                if (data > 0)
                    return fomartmoney(data)(4);
                else
                    return '$0.00';
            },
            targets: [7]
        }]
    });
}

function SerchClear() {
    ["SuppliersDrop", "departmentDrop", "productType"].map(id => $(`#${id}`).val(""))
    $("#SuppliersDrop").val("0").trigger("change");
    $("#departmentDrop").val("0").trigger("change");
    if ($("#productType").length != 0) {
        $("#productType").val("").trigger("change");
    }
}

function SerchProducts() {
    var supplier_id = $("#SuppliersDrop").val() != "" ? $("#SuppliersDrop").val() : 0;
    var class_id = $('#departmentDrop').val();
    var category = $('#productType').val();
    var family = $('#family').val();
    if ($("#productType").length == 0)
        category = "MERMA";
    else
        category = $('#productType').val();
    $('#TableDiv').animatePanel();
    StartLoading();
    $.ajax({
        type: "GET",
        url: "/ItemSuppliers/ActionGetStockBySupplier",
        data: { "supplier_id": supplier_id, "class_id": class_id, "category": category, "family_id": family },
        success: function (returndate) {
            EndLoading();
            if (returndate.success) {
                table.clear();
                table.rows.add($(returndate.Json));
                table.columns.adjust().draw();
            }
            else
                toastr.warning('Alerta - Error inesperado!!');
        },
        error: function (returndate) {
            EndLoading();
            toastr.error('Alerta - Error inesperado contactar a sistemas.');
        }
    });
}

function FillDropdown(selector, vData) {
    if (vData != null) {
        var vItems = [];
        vItems.push('<option value="0">Seleccione una familia</option>');
        for (var i in vData) {
            if (vData[i].Selected)
                vItems.push('<option selected value="' + vData[i].class_id + '">' + vData[i].description + '</option>');
            else
                vItems.push('<option value="' + vData[i].class_id + '">' + vData[i].description + '</option>');
        }
        $('#' + selector).empty();
        $('#' + selector).append(vItems.join(''));
        $('#family').val($("#family :first").val()).trigger('change');
    }
    else {
        var vItems = [];
        vItems.push('<option selected value="0">Seleccione una familia</option>');
        $('#' + selector).empty();
        $('#' + selector).append(vItems.join(''));
        $('#family').val($("#family :first").val()).trigger('change');
    }
}

function GetFamily() {
    var depto = $("#departmentDrop").val();
    if (depto == "" || depto == 0) {
        $('#family').val($("#family :first").val()).trigger('change');
        FillDropdown("family", null)
        return false;
    }
    $.ajax({
        url: '/MaClass/ActionGetFamily/',
        type: "GET",
        data: { "deptoo": depto },
        success: function (response) {
            if (response.success) {
                listFamily = response.Json;
                FillDropdown("family", listFamily);
            }
            else
                FillDropdown("family", null);
        },
        error: function () {
            FillDropdown("family", null);
            toastr.error('Error inesperado contactar a sistemas.');
        }
    });
}