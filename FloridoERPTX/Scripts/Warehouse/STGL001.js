﻿var edit = false;
var indexEdit = -1;
var tiendas = [];
var nameEdit = "";

$(document).ready(function () {
    window.setTimeout(function () { $("#fa").click(); }, 1000);
    var Locations = JSON.parse($("#model").val());
    table.clear();
    table.rows.add($(Locations));
    table.columns.adjust().draw();
});

$('#storage_type').select2();

var table = $('#table-location').DataTable({
    responsive: true,
    autoWidth: true,
    order: [[2, "asc"]],
    oLanguage: {
        "sProcessing": "Procesando...",
        "sLengthMenu": "Mostrar _MENU_ Registros",
        "sZeroRecords": "No se encontraron resultados",
        "sEmptyTable": "Ningún dato disponible en esta tabla",
        "sInfo": "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
        "sInfoEmpty": "Mostrando registros del 0 al 0 de un total de 0 registros",
        "sInfoFiltered": "(filtrado de un total de _MAX_ registros)",
        "sInfoPostFix": "",
        "sSearch": "Buscar:",
        "sUrl": "",
        "sInfoThousands": ",",
        "sLoadingRecords": "Cargando...",
        "oPaginate": { "sFirst": "Primero", "sLast": "Último", "sNext": "Siguiente", "sPrevious": "Anterior" },
        "oAria": {
            "sSortAscending": ": Activar para ordenar la columna de manera ascendente",
            "sSortDescending": ": Activar para ordenar la columna de manera descendente"
        }
    },
    dom: "<'row'<'col-sm-4'l><'col-sm-4 text-left'B><'col-sm-4'f>t<'col-sm-6'i><'col-sm-6'p>>",
    lengthMenu: [[10, 25, 50, -1], [10, 25, 50, "Todos"]],
    buttons: [],
    columns: [
        { data: null, width: "1%", className: "text-center", orderable: false },
        { data: 'site_code', width: "1%", searchable: false, visible: false },
        { data: 'storage_location', width: "1%" },
        { data: 'storage_type', width: "1%" },
        { data: 'active_flag', width: "1%" }
    ],
    columnDefs: [
        {
            data: null,
            width: "1%",
            defaultContent: '<button class="btn btn-xs btn-outline btn-warning" id="edit-button" type="submit"><i class="pe-7s-config"></i> Editar</button>',
            targets: [0]
        },
        {
            data: "active_flag",
            width: "1%",
            targets: [4],
            render: function (data, type, row) {
                if (data)
                    return "Activo";
                else
                    return "Inactivo";
            }
        }
    ]
});

$('#table-location tbody').on('click', '#edit-button', function () {
    var values = table.row($(this).parents('tr')).data();
    values = $.map(values, function (value, index) {
        switch (index) {
            case 'site_code':
                $("#site_code").val(value);
                break;
            case 'storage_location':
                $("#storage_location").val(value);
                break;
            case 'storage_type':
                $("#storage_type").val(value).trigger('change');
                break;
            case 'active_flag':
                checkBoxStyle(value);
                break;
        }
    });
    $('#storage_location').prop('disabled', true);
    $("#ModalTitle").html('Modificar Locación.');
    $("#ModalDescription").html('Formulario para Locaciones.');
    $('#modal-add-location').modal({ backdrop: 'static', keyboard: false });
    $("#modal-add-location").removeClass("hmodal-success").addClass("hmodal-info");
    $("#submit-button").removeClass("btn btn-success").addClass("btn btn-info");
    edit = true;
    nameEdit = $("#storage_location").val();
    indexEdit = table.row(this).index();
    $('#modal-add-location').modal('show');
});

function checkBoxStyle(s) {
    if (s) {
        $('#Check').children().addClass("checked").length;
        $('#active_flag').prop("checked", true);
    }
    else {
        $('#Check').children().removeClass("checked").length;
        $('#active_flag').prop("checked", false);
    }
}

function AddLocation() {
    clearModal();
    $('#modal-add-location').modal('show');
}

function clear() {
    $("#storage_location").val("");
    $("#storage_type").val("");
    $('#storage_location').prop('disabled', false);
    $("#storage_type").val("").change();
    $('#active_flag').iCheck('uncheck');
    $("#ModalTitle").html('Agregar Locación');
    $('#modal-add-location').modal({ backdrop: 'static', keyboard: false });
    $("#ModalDescription").html('Formulario para registrar una Locación.');
    $("#modal-add-location").removeClass("hmodal-warning").addClass("hmodal-success");
    $("#submit-button").removeClass("btn btn-info").addClass("btn btn-success");
    indexEdit = null;
    edit = false;
}

function clearModal() {
    clear();
    form.resetForm();
}

function ValidateLocationName() {
    toastr.options = {
        "debug": false,
        "newestOnTop": false,
        "positionClass": "toast-top-center",
        "closeButton": true,
        "debug": false,
        "toastClass": "animated fadeInDown",
    };

    var name = $('#storage_location').val();

    if (name == null || name == "") {
        return;
    }

    if (edit && name == nameEdit) {
        return;
    }

    $.ajax({
        url: "/StorageLocation/ActionNameAlreadyExists",
        type: "GET",
        data: { "storage_name": $('#storage_location').val() },
        success: function (result) {
            if (result.success) {
                toastr.warning(result.responseText);
                $("#storage_location").val(null);
                $("#storage_location").focus();
            }
            else
                toastr.success(result.responseText);
        },
        error: function () {
            toastr.error('Error inesperado contactar a sistemas');
        }
    });
}

var form = $("#form").validate({
    rules:
    {
        storage_location:
        {
            required: true,
            minlength: 4,
            maxlength: 20
        }
    },
    messages:
    {
        storage_location:
        {
            required: "El Campo no puede ser nulo",
            minlength: "Favor de ingresar mas de 4 caracteres",
            maxlength: "Máximo 20 caracteres"
        }
    },
    submitHandler: function (form) {
        if (edit) {
            if ($('#storage_type').val() == "") {
                toastr.error('Selecciona un tipo de locacion.');
                $('#storage_type').select2("open");
                return false;
            }
            StartLoading();
            $.ajax({
                type: "POST",
                url: "/StorageLocation/ActionEdit",
                data: { "storage_location": $('#storage_location').val(), "storage_type": $('#storage_type').val(), "active_flag": $('#active_flag').prop('checked') },
                success: function (result) {
                    EndLoading();
                    if (result.success) {
                        var LocationsReturn = $.parseJSON(result.Json);
                        table.clear().draw();
                        table.rows.add($(LocationsReturn));
                        table.columns.adjust().draw();
                        $('#modal-add-location').modal('hide');
                        toastr.info(result.responseText + '.');
                        $('#storage_type').val("").trigger("change");
                        $('#Check').children().removeClass("checked").length;
                        $('#active_flag').prop("checked", false);
                    }
                    else
                        toastr.warning(result.responseText);
                },
                error: function () {
                    EndLoading();
                    toastr.error('Error inesperado contactar a sistemas.');
                }
            });
            return false;
        }
        else {
            if ($('#storage_type').val() == "") {
                toastr.error('Selecciona un tipo de locacion.');
                $('#storage_type').select2("open");
                return false;
            }
            StartLoading();
            $.ajax({
                type: "POST",
                url: "/StorageLocation/ActionCreate",
                data: { "storage_location": $('#storage_location').val(), "storage_type": $('#storage_type').val(), "active_flag": $('#active_flag').prop('checked') },
                success: function (result) {
                    EndLoading();
                    if (result.success) {
                        var LocationsReturn = $.parseJSON(result.Json);
                        table.clear().draw();
                        table.rows.add($(LocationsReturn));
                        table.columns.adjust().draw();
                        $('#modal-add-location').modal('hide');
                        toastr.success(result.responseText + '.');
                        $('#storage_type').val("").trigger("change");
                        $('#Check').children().removeClass("checked").length;
                        $('#active_flag').prop("checked", false);
                    }
                    else
                        toastr.warning(result.responseText + '');
                },
                error: function (e) {
                    EndLoading();
                    toastr.error('Error inesperado contactar a sistemas.');
                }
            });
            return false;
        }
    }
});