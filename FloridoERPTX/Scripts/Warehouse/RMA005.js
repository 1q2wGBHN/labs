﻿
$(document).ready(function () {
    window.setTimeout(function () {
        $("#fa").click();
    }, 1000);

    var model = JSON.parse($("#model").val());
    table.clear();
    table.rows.add($(model));
    table.columns.adjust().draw();
});

$("#ValidDate1").datepicker({
    autoclose: true,
    todayHighlight: true,
    showButtonPanel: true,
}).on("changeDate", function (e) {
    SerchRMA();
});

$("#ValidDate2").datepicker({
    autoclose: true,
    startDate: "01/01/1950",
    todayHighlight: true,
}).on("changeDate", function (e) {
    SerchRMA();
});

var table = $("#TablePurchases").DataTable({
    oLanguage: {
        "sProcessing": "Procesando...", "sLengthMenu": "Mostrar _MENU_ registros", "sZeroRecords": "No se encontraron resultados", "sEmptyTable": "Ningún dato disponible en esta tabla", "sInfo": "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros", "sInfoEmpty": "Mostrando registros del 0 al 0 de un total de 0 registros", "sInfoFiltered": "(filtrado de un total de _MAX_ registros)", "sInfoPostFix": "", "sSearch": "Buscar:", "sUrl": "", "sInfoThousands": ",", "sLoadingRecords": "Cargando...", "oPaginate": { "sFirst": "Primero", "sLast": "Último", "sNext": "Siguiente", "sPrevious": "Anterior" }, "oAria": { "sSortAscending": ": Activar para ordenar la columna de manera ascendente", "sSortDescending": ": Activar para ordenar la columna de manera descendente" }
    },
    responsive: true,
    autoWidth: true,
    order: [[1, "asc"]],
    dom: "<'row'<'col-sm-4'l><'col-sm-4 text-left'B><'col-sm-4'f>t<'col-sm-6'i><'col-sm-6'p>>",
    lengthMenu: [[10, 25, 50, -1], [10, 25, 50, "Todos"]],
    buttons: [
        { extend: 'csv', text: 'Excel', title: 'AprobacionRemision', className: 'btn-sm' },
        {
            extend: 'pdf', text: 'PDF', title: 'AprobacionRemision', className: 'btn-sm', exportOptions: {
                columns: [1, 2, 3]
            },
            customize: function (doc) {
                doc.content[1].table.widths =
                    Array(doc.content[1].table.body[0].length + 1).join('*').split('');
            }
        },
        { extend: 'print', text: 'Imprimir', className: 'btn-sm' },
    ],
    columns: [
        {
            "class": "details-control",
            "orderable": false,
            "data": null,
            "defaultContent": "",
            "width": "1%"
        },
        { data: "DamagedGoodsDoc", width: "1%" },
        { data: "Supplier", width: "1%" },
        { data: "Comment", width: "1%" },
        { data: "ProcessStatusDescription", width: "1%" }
    ],
    columnDefs: [{
        targets: [0, 1, 2, 3, 4],
        width: "1%"
    }]
});

var detailRows = [];

$("#TablePurchases tbody").on("click", "tr td.details-control", function () {
    var tr = $(this).closest("tr");
    var row = table.row(tr);

    if (row.child.isShown()) {
        row.child.hide();
        tr.removeClass("details");
    }
    else {
        if (table.row(".details").length) {
            $(".details-control", table.row(".details").node()).click();
        }
        row.child(format(row.data())).show();
        tr.addClass("details");
    }
});

table.on("draw", function () {
    $.each(detailRows, function (i, id) {
        $("#" + id + " td.details-control").trigger("click");
    });
});

var list = [];
function format(d) {
    list = [];
    var detailItems = "";
    var tabledetail = $("<div/>").addClass("loading").text("Cargando Datos...");
    $.ajax({
        url: "/DamagedsGoods/ActionGetItemSupplierByRma/",
        data: { "Folio": d.DamagedGoodsDoc, "Type": "Remision", "Status": 1 },
        type: "GET",
        success: function (data) {
            $.each(data, function (index, value) {                
                detailItems += "<tr><td class='text-center'><div class='checkbox'><label><input type='checkbox' PartNumber = " + value.PartNumber + " class='i-checks' /></label></div></td><td>" + value.PartNumber + "</td><td>" + value.Description + "</td><td>" + value.Size + "</td><td class='text-right'>" + value.Storage + "</td><td class='text-right'>" + value.Quantity + "</td><td class='text-right'>" + "$" + value.Amount_Doc.toFixed(4) + "</td><td class='text-right'>" + "$" + value.Price.toFixed(4) + "</td></tr>"
            });
            var Apro = "";
            if (d.ProcessStatusDescription == "Pendiente") {                
                Apro = '<div class="form-group col-md-12"><div class="row"><label class="col-md-3 control-label">Comentario</label><label class="col-md-6 control-label">Peso total de la mercancia (Kg):</label></div><div class="row"><div class="col-md-3"><textarea id="comment" style="max-width:500px;max-height:150px;min-width:100px;min-height:100px;width:200px" class="form-control" maxlength="249"></textarea></div><div class="col-md-6"><input id="weight" type="number" class="form-control lg"></div><div class="pull-right"><button id="btn-cancel" type="button" class="btn btn-sm btn-danger" Folio="' + d.DamagedGoodsDoc + '" SupplierId="' + d.SupplierId + '"><i class="fa fa-close"></i> Cancelar</button>&nbsp;&nbsp;&nbsp;<button id="btn-save" type="button" class="btn btn-sm btn-success" Folio="' + d.DamagedGoodsDoc + '" SupplierId="' + d.SupplierId + '"><i class="fa fa-save"></i> Aprobar</button></div></div></div>';
            }
            tabledetail.html('<table id="tabledetail" class="table table-striped table-bordered table-hover" style="width:100%"><thead><tr><th></th><th>Codigo</th><th>Nombre</th><th>UM</th><th>Locacion</th><th>Cantidad</th><th>Monto</th><th>Precio</th></tr></thead><tbody>' + detailItems + '</tbody></table>' + Apro).removeClass('loading');

            $('input[type=checkbox]').iCheck({
                checkboxClass: 'icheckbox_square-green',
            }).on('ifClicked', function () {
                $(this).trigger("change");
                var item = this;
                var check = item.checked;
                var PartNumber = item.attributes.partnumber.value;

                if (!check) {
                    list.push({ PartNumber: PartNumber })
                } else {
                    list = list.filter(x => x.PartNumber != PartNumber);
                }
            });

            reloadStyleTable();
        }
    });
    return tabledetail;
}

function SerchRMA() {
    if ($("#ValidDate1").val() != "" & $("#ValidDate2").val() != "") {
        $("#TableDiv").animatePanel();
        $.ajax({
            type: "GET",
            url: "/DamagedsGoods/ActionGetRMA/",
            data: { "Type": "Remision", "Date1": $("#ValidDate1").val(), "Date2": $("#ValidDate2").val(), "Status": 1 },
            success: function (result) {
                if (result.success) {
                    table.clear();
                    table.rows.add($(result.Json));
                    table.columns.adjust().draw();
                }
                else
                    toastr.warning("Alerta - Error inesperado!!");
            },
            error: function (result) {
                toastr.error("Alerta - Error inesperado contactar a sistemas.");
            }
        });
    }
}

function reloadStyleTable() {
    $("#tabledetail").DataTable({
        oLanguage: {
            "sProcessing": "Procesando...", "sLengthMenu": "Mostrar _MENU_ registros", "sZeroRecords": "No se encontraron resultados", "sEmptyTable": "Ningún dato disponible en esta tabla", "sInfo": "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros", "sInfoEmpty": "Mostrando registros del 0 al 0 de un total de 0 registros", "sInfoFiltered": "(filtrado de un total de _MAX_ registros)", "sInfoPostFix": "", "sSearch": "Buscar:", "sUrl": "", "sInfoThousands": ",", "sLoadingRecords": "Cargando...", "oPaginate": { "sFirst": "Primero", "sLast": "Último", "sNext": "Siguiente", "sPrevious": "Anterior" }, "oAria": { "sSortAscending": ": Activar para ordenar la columna de manera ascendente", "sSortDescending": ": Activar para ordenar la columna de manera descendente" }
        },
    });
}

function Update() {
    if ($("#ValidDate1").val() != "" & $("#ValidDate2").val() != "") {
        if (moment($("#ValidDate1").val()) <= moment($("#ValidDate2").val())) {
            $.ajax({
                type: "GET",
                url: "/DamagedsGoods/ActionGetRMA/",
                data: { "Type": "Remision", "Date1": $("#ValidDate1").val(), "Date2": $("#ValidDate2").val(), "Status": 1 },
                success: function (result) {
                    if (result.success) {
                        $("#TableDiv").animatePanel();
                        table.clear();
                        table.rows.add($(result.Json));
                        table.columns.adjust().draw();
                    }
                    else
                        toastr.warning("Alerta - Error inesperado!!");
                },
                error: function (result) {
                    toastr.error("Alerta - Error inesperado contactar a sistemas.");
                }
            });
        } else {
            toastr.error("Fecha final no puede ser menor a la inicial.");
            document.getElementById("ValidDate1").value = "";
            document.getElementById("ValidDate2").value = "";
            $("#ValidDate1").focus();
        }
    }
    else {
        $.ajax({
            type: "GET",
            url: "/DamagedsGoods/ActionGetPendingRMA/",
            success: function (result) {
                $("#TableDiv").animatePanel();
                table.clear();
                table.rows.add($(result.Json));
                table.columns.adjust().draw();
                document.getElementById("ValidDate1").value = "";
                document.getElementById("ValidDate2").value = "";
            },
            error: function (result) {
                toastr.error("Alerta - Error inesperado contactar a sistemas.");
            }
        });
    }
}

function CancelMessage(s, g) {
    swal({
        title: "¿Esta seguro que desea Rechazar?",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "Continuar",
        cancelButtonText: "Cancelar"
    }, function (isConfirm) {
        if (isConfirm) { Aproval(false, s, g); }
    });
}

function AprovalMessage(s, g) {
    swal({
        title: "¿Esta seguro que desea Aprobar?",
        type: "info",
        showCancelButton: true,
        confirmButtonColor: "#55dd6b",
        cancelButtonText: "Cancelar",
        confirmButtonText: "Continuar"
    }, function (isConfirm) {
        if (isConfirm) { Aproval(true, s, g); }
    });
}

function validateWeight() {
    var weight = $("#weight").val();
    var isDecimal = /^\d+\.?\d{0,3}$/

    if (isDecimal.test(weight)) {
        if (weight > 0 && weight <= 599999) {
            return true;
        }
        else {
            EndLoading();
            setTimeout(function () {
                swal("Debe de ingresar el peso correcto de la mercancia.",
                    "Ingrese el peso total de la mercancia y vuelvalo a intentar.",
                    "error");
            }, 400);
            return false;
        }
    }
    else {
        EndLoading();
        setTimeout(function () {
            swal("Debe de ingresar el peso total de la mercancia.",
                "Ingrese el peso de la mercancia y vuelvalo a intentar.",
                "error");
        }, 400);
        return false;
    }
}

$(document).on("click", "#btn-cancel", function (event) {
    var tabledetail = document.getElementById("tabledetail");
    var folio = $(this).attr("folio");
    var supplier = $(this).attr("supplierid");


    swal({
        title: "¿Esta seguro que desea Rechazar?",
        type: "info",
        showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        cancelButtonText: "Cancelar",
        confirmButtonText: "Continuar"
    }, function (isConfirm) {
        if (isConfirm) { Aproval(list, false, folio, supplier); }
    });
    
});

$(document).on("click", "#btn-save", function (event) {
    var tabledetail = document.getElementById("tabledetail");
    var folio = $(this).attr("folio");
    var supplier = $(this).attr("supplierid");

    if (list.length > 0) {
        swal({
            title: "¿Esta seguro que desea Aprobar?",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#55dd6b",
            confirmButtonText: "Continuar",
            cancelButtonText: "Cancelar"
        }, function (isConfirm) {
            if (isConfirm) { Aproval(list, true, folio, supplier); }
        });
    }
    else
        toastr.warning("Seleccione al menos un producto.");
});

function Aproval(model, flag, folio, supplier) {
    var comment = $("#comment").val();
    if (comment.trim().length >= 8) {
        if (flag) {
            if (validateWeight()) {
                StartLoading();
                var weight = $("#weight").val();
                if (folio) {
                    axios.post("/DamagedsGoods/ActionUpdateRmaAprovalCancel", { Model: model, Folio: folio, Flag: flag, Comment: comment, type: "REMISION", program: "RMA005.cshtml", SupplierId: supplier, Weight: weight }).then(function (data) {
                        EndLoading();
                        if (data.data.Data == "SF")
                            SessionFalse("Se terminó su sesión.");
                        else if (data.data.Data == "N/A") {
                            toastr.warning("Folio no existente");
                        }
                        else if (data.data.Data) {
                            if (flag) {
                                $("#modalReferenceBody").html(`<iframe width='100%' height='550px' src='data:application/pdf;base64,${data.data.response}'></iframe>`);
                                $("#modalReference").modal("show");
                                toastr.success("Folio Aplicado con exito.");
                                toastr.success("Se ha enviado a su correo este mismo documento.");

                            } else {
                                swal({
                                    title: "Rechazado",
                                    text: "Folio rechazado con exito.",
                                    type: "warning"
                                });
                            }
                            Update();
                        }
                        else if (!data.data.Data)
                            toastr.warning("Folio cancelado previamente o inexistente");
                        else
                            toastr.error("Alerta - Error inesperado  contactar a sistemas.");
                    }).catch(function (error) {
                        EndLoading();
                        toastr.error("Alerta - Error inesperado  contactar a sistemas.");
                    })
                }
                else {
                    toastr.warning("El folio no puede ser nulo");
                    EndLoading();
                }
            }
        }
        else {
            StartLoading();
            if (folio) {
                axios.post("/DamagedsGoods/ActionUpdateRmaAprovalCancel", { Model: model, Folio: folio, Flag: flag, Comment: comment, type: "REMISION", program: "RMA005.cshtml", SupplierId: supplier, Weight: weight }).then(function (data) {
                    EndLoading();
                    if (data.data.Data == "SF")
                        SessionFalse("Se terminó su sesión.");
                    else if (data.data.Data == "N/A") {
                        toastr.warning("Folio no existente");
                    }
                    else if (data.data.Data) {
                        if (flag) {
                            swal({
                                title: "Aprobado",
                                text: "Folio aprobado con exito.",
                                type: "success"
                            });
                        } else {
                            swal({
                                title: "Rechazado",
                                text: "Folio rechazado con exito.",
                                type: "warning"
                            });
                        }
                        Update();
                    }
                    else if (!data.data.Data)
                        toastr.warning("Folio cancelado previamente o inexistente");
                    else
                        toastr.error("Alerta - Error inesperado  contactar a sistemas.");
                }).catch(function (error) {
                    EndLoading();
                    toastr.error("Alerta - Error inesperado  contactar a sistemas.");
                })
            }
            else {
                toastr.warning("El folio no puede ser nulo");
                EndLoading();
            }
        }
    } else {
        setTimeout(function () {
            swal("Debe de ingresar al menos un comentario de 8 caracteres.",
                "Ingrese un comentario  y vuelvalo a intentar.",
                "error");
        }, 400);
    }
}