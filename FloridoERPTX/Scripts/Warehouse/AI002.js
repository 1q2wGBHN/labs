﻿window.setTimeout(function () { $("#fa").click(); }, 1000);

var Products = $('#Items').DataTable({
    oLanguage: {
        "sProcessing": "Procesando...", "sLengthMenu": "Mostrar _MENU_ registros", "sZeroRecords": "No se encontraron resultados", "sEmptyTable": "Ningún dato disponible en esta tabla", "sInfo": "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros", "sInfoEmpty": "Mostrando registros del 0 al 0 de un total de 0 registros", "sInfoFiltered": "(filtrado de un total de _MAX_ registros)", "sInfoPostFix": "", "sSearch": "Buscar:", "sUrl": "", "sInfoThousands": ",", "sLoadingRecords": "Cargando...", "oPaginate": { "sFirst": "Primero", "sLast": "Último", "sNext": "Siguiente", "sPrevious": "Anterior" }, "oAria": { "sSortAscending": ": Activar para ordenar la columna de manera ascendente", "sSortDescending": ": Activar para ordenar la columna de manera descendente" }
    },
    responsive: true,
    autoWidth: true,
    order: [[0, "asc"]],
    buttons: [
        { extend: 'csv', text: 'Excel', title: 'Equipos', className: 'btn-sm' },
        {
            extend: 'pdf', text: 'PDF', title: 'Equipos', className: 'btn-sm', exportOptions: {
                columns: [0, 1, 2, 3, 4, 5, 6, 7, 8]
            },
            customize: function (doc) {
                doc.content[1].table.widths =
                    Array(doc.content[1].table.body[0].length + 1).join('*').split('');
            }
        },
        { extend: 'print', text: 'Imprimir', className: 'btn-sm' },
    ],
    columnDefs: [
        {
            data: 'MovementType',
            width: "1%",
            render: function (data, type, row) {
                if (data)
                    return "Crédito";
                else
                    return "Débito";
            },
            targets: [6]
        },
        {
            width: "1%",
            render: (data, type, row) => data.MovementType ? parseFloat(data.Stock) - parseFloat(data.RequestedStock) : parseFloat(data.Stock) + parseFloat(data.RequestedStock),
            targets: [8]
        },
        {
            data: null,
            width: "1%",
            defaultContent: "<input type='text' maxlength='49' placeholder='Comentarios' class='form-control' />",
            targets: [9],
            orderable: false
        },
        {
            data: null,
            width: "1%",
            defaultContent: "<table><tbody><tr><td> <button class='btn btn-xs btn-danger btn-cancel pull-left' type='button'><i class='fa fa-close'></i> Rechazar</button></td><td><button class='btn btn-xs btn-success btn-aprove pull-right' type='button'><i class='fa fa-check'></i> Aprobar</button></td></tr></tbody></table>",
            targets: [10],
            className: "text-center text-nowrap",
            orderable: false
        }
    ],
    columns: [
        { targets: 0, data: 'Document', width: "1%" },
        { targets: 1, data: 'PathNumber', width: "1%" },
        { targets: 2, data: 'Description', width: "1%" },
        { targets: 3, data: 'StorageLocation', width: "1%" },
        { targets: 4, data: 'Stock', width: "1%" },
        { targets: 5, data: 'RequestedStock', width: "1%" },
        { targets: 6, data: 'MovementType', width: "1%" },
        { targets: 7, data: 'Comment', width: "1%" },
        { targets: 8, data: null, width: "1%" },
        { targets: 9, data: null, width: "1%" },
        { targets: 10, data: null, width: "1%", className: "text-center" }
    ]
});

var model = JSON.parse($("#model").val());
if (!$.isEmptyObject(model)) {
    Products.clear();
    Products.rows.add($(model));
    Products.columns.adjust().draw();
    $('#TableDiv').animatePanel();
}
else {
    Products.clear();
    Products.columns.adjust().draw();
    $('#TableDiv').animatePanel();
}

function Cancel(itemId, document, comments) {
    StartLoading();
    $.ajax({
        type: "POST",
        url: "/Items/ActionCancelInventoryAdjustment/",
        data: { "partNumber": itemId, "Document": document, "Comments": comments },
        success: function (result) {
            if (!result) {
                SessionFalse("Se termino su sesion.")
            }
            else if (result) {
                Update();
                swal({
                    title: "Cancelado",
                    text: "Solicitud de ajuste de inventario cancelada satisfactoriamente!",
                    type: "success",
                    confirmButtonText: "Continuar"
                });
            }
            else
                toastr.warning('Alerta - Error inesperado!!');
            EndLoading();
        },
        error: function (result) {
            toastr.error('Alerta - Error inesperado  contactar a sistemas.');
            EndLoading();
        }
    });
}

function Save(itemId, document, storageLocation, quantity, type, comments) {
    StartLoading();
    $.ajax({
        type: "POST",
        url: "/Items/ActionSetInventoryAdjustment/",
        data: { "partNumber": itemId, "Document": document, "StorageLocation": storageLocation, "Quantity": quantity, "Type": type, "Comments": comments },
        success: function (result) {
            if (!result)
                SessionFalse("Se termino su sesion.")
            if (result.Document.ReturnValue == 0 && result.Document.Document != "") {
                Update();
                swal({
                    title: "Folio: " + result.Document.Document,
                    text: "Ajuste de inventario realizado satisfactoriamente!",
                    type: "success",
                    confirmButtonText: "Continuar"
                });
            }
            else {
                var texto = "";
                switch (result.Document.ReturnValue) {
                    case 6001:
                        texto = "Site code Incorrecto - (6001)"
                        break
                    case 6002:
                        texto = "No existe Locacion - (6002)"
                        break
                    case 6003:
                        texto = "Error en Precio - (6003)"
                        break
                    case 6004:
                        texto = "Stock Insuficiente - (6004)"
                        break
                    case 8001:
                        texto = "Error en un producto - (8001)"
                        break
                    default:
                        texto = "Error no pudo localizarse"
                        break
                }
                toastr.warning(texto);
            }
            EndLoading();
        },
        error: function (result) {
            toastr.error('Alerta - Error inesperado  contactar a sistemas.');
            EndLoading();
        }
    });
}

$('#Items tbody').on('click', '.btn-cancel', function () {
    var itemId = Products.row($(this).parents('tr')).data().PathNumber;
    var itemDescription = Products.row($(this).parents('tr')).data().Description;
    var document = Products.row($(this).parents('tr')).data().Document;
    var comments;
    $(this).parents('tr').find('input').each(function (a, b) {
        comments = $(this).val();
    });

    if (typeof comments == 'undefined' || comments.trim() == "")
        toastr.warning('Capture comentarios.');
    else {
        swal({
            title: "Esta seguro?",
            text: "Se cancelará el ajuste de inventario del producto: " + itemDescription,
            type: "warning",
            closeOnConfirm: true,
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Continuar",
            cancelButtonText: "Cancelar"
        }, function (isConfirm) {
            setTimeout(function () {
                if (isConfirm)
                    Cancel(itemId, document, comments);
            }, 400)
        });
    }
});

$('#Items tbody').on('click', '.btn-success', function () {
    var itemId = Products.row($(this).parents('tr')).data().PathNumber;
    var itemDescription = Products.row($(this).parents('tr')).data().Description;
    var document = Products.row($(this).parents('tr')).data().Document;
    var storageLocation = Products.row($(this).parents('tr')).data().StorageLocation;
    var quantity = Products.row($(this).parents('tr')).data().RequestedStock;
    var type = Products.row($(this).parents('tr')).data().MovementType;
    var comments;

    $(this).parents('tr').find('input').each(function (a, b) {
        comments = $(this).val();
    });

    if (typeof comments == 'undefined' || comments.trim() == "")
        toastr.warning('Capture comentarios.');
    else {
        swal({
            title: "Esta seguro?",
            text: "Se ajustará el inventario del producto: " + itemDescription,
            type: "warning",
            closeOnConfirm: true,
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Continuar",
            cancelButtonText: "Cancelar"
        }, function (isConfirm) {
            setTimeout(function () {
                if (isConfirm)
                    Save(itemId, document, storageLocation, quantity, type, comments);
            }, 400)
        });
    }
});

$(document).on("keypress", '.Numeric', function (event) {
    var keyValue = event.key;
    if (keyValue.match(/[0-9]/g))
        return true;
    else
        return false;
});

function UpdateByDate() {
    var beginDate = $('#beginDate').val();
    var endDate = $('#endDate').val();
    if (beginDate != "" && endDate != "") {
        if ((new Date(beginDate)) <= (new Date(endDate))) {
            $.ajax({
                type: "GET",
                url: "/Items/ActionGetInventoryAdjustmentRequestsByDate/",
                data: { "beginDate": beginDate, "endDate": endDate },
                success: function (result) {
                    if (result.success) {
                        if (!$.isEmptyObject(result.Json)) {
                            Products.clear();
                            Products.rows.add($(result.Json));
                            Products.columns.adjust().draw();
                            $('#TableDiv').animatePanel();
                        }
                        else {
                            Products.clear();
                            Products.columns.adjust().draw();
                            $('#TableDiv').animatePanel();
                        }
                    }
                    else
                        toastr.warning('Alerta - Error inesperado!!');
                },
                error: function (result) {
                    toastr.error('Alerta - Error inesperado contactar a sistemas.');
                }
            });
        }
        else {
            Products.clear();
            Products.columns.adjust().draw();
            toastr.error("La fecha inicial no puede ser mayor a la fecha final.");
        }
    }
}

function Update() {
    $.ajax({
        type: "GET",
        url: "/Items/ActionGetInventoryAdjustmentRequests/",
        success: function (result) {
            if (result.success) {
                if (!$.isEmptyObject(result.Json)) {
                    Products.clear();
                    Products.rows.add($(result.Json));
                    Products.columns.adjust().draw();
                    $('#TableDiv').animatePanel();
                }
                else {
                    Products.clear();
                    Products.columns.adjust().draw();
                    $('#TableDiv').animatePanel();
                }
            }
            else
                toastr.warning('Alerta - Error inesperado!!');
        },
        error: function (result) {
            toastr.error('Alerta - Error inesperado contactar a sistemas.');
        }
    });
}

$("#beginDate").datepicker({
    autoclose: true,
    todayHighlight: true,
    showButtonPanel: true,
}).on("changeDate", function (e) {
    UpdateByDate();
});

$("#endDate").datepicker({
    autoclose: true,
    startDate: '01/01/1950',
    todayHighlight: true,
}).on("changeDate", function (e) {
    UpdateByDate();
});