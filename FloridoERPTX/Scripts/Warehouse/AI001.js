﻿/// <reference path="../knockout-3.4.2.debug.js" />

$("#Item").select2({
    minimumInputLength: 3,
    allowClear: true,
    placeholder: "Seleccione una producto",
    initSelection: function (element, callback) {
        callback({ id: "", text: "Seleccione una opcion" });
    },
    ajax: {
        url: "/Items/ActionSearchItemBarcode/",
        dataType: 'json',
        type: "GET",
        quietMillis: 50,
        data: function (Filter) {
            return {
                Filter: Filter
            };
        },
        results: function (data) {
            return {
                results: $.map(data.Json, function (item) {
                    return {
                        text: `${item.PathNumber} - ${item.Description}`,
                        id: item.PathNumber
                    }
                }),
            };
        }
    }
});
var Products = $('#Items').DataTable({
    oLanguage: {
        "sProcessing": "Procesando...", "sLengthMenu": "Mostrar _MENU_ registros", "sZeroRecords": "No se encontraron resultados", "sEmptyTable": "Ningún dato disponible en esta tabla", "sInfo": "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros", "sInfoEmpty": "Mostrando registros del 0 al 0 de un total de 0 registros", "sInfoFiltered": "(filtrado de un total de _MAX_ registros)", "sInfoPostFix": "", "sSearch": "Buscar:", "sUrl": "", "sInfoThousands": ",", "sLoadingRecords": "Cargando...", "oPaginate": { "sFirst": "Primero", "sLast": "Último", "sNext": "Siguiente", "sPrevious": "Anterior" }, "oAria": { "sSortAscending": ": Activar para ordenar la columna de manera ascendente", "sSortDescending": ": Activar para ordenar la columna de manera descendente" }
    },
    responsive: true,
    autoWidth: true,
    order: [[0, "asc"]],
    columnDefs: [
        {
            data: null,
            width: "1%",
            defaultContent: "<input type='text' placeholder='Cantidad' class='form-control Numeric' />",
            orderable: false,
            targets: [4]
        },
        {
            data: 'PartNumber',
            width: "1%",
            render: function (data, type, row) {
                return "<table style='width:100%'><tr><td><input type='radio' name='movimiento_" + data + "' class='form-control' value='Credito'> Salida (-) </input></td></tr><tr><td><input type='radio' name='movimiento_" + data + "' class='form-control' value='Debito'> Entrada (+) </input></td></tr></table>";
            },
            orderable: false,
            targets: [5]
        },
        {
            data: null,
            width: "1%",
            defaultContent: "<input type='text' maxlength='49' placeholder='Comentarios' class='form-control' />",
            orderable: false,
            targets: [6]
        },
        {
            data: null,
            width: "1%",
            defaultContent: "<button class='btn btn-xs btn-outline btn-danger btn-cancel' type='button'><i class='fa fa-trash'></i> Remover</button>",
            targets: [7],
            orderable: false
        }
    ],
    columns: [
        { targets: 0, data: 'PartNumber', width: "1%" },
        { targets: 1, data: 'Description', width: "1%" },
        { targets: 2, data: 'StorageLocation', width: "1%" },
        { targets: 3, data: 'Stock', width: "1%" },
        { targets: 4, data: 'NuevoStock', width: "1%" },
        { targets: 5, data: 'PartNumber', width: "1%" },
        { targets: 6, data: null, width: "1%" },
        { targets: 7, data: null, width: "1%", className: "text-center" }
    ],
});

$('#Items tbody').on('click', '.btn-cancel', function () {
    vm.Remove(Products.row($(this).parents('tr')).data().PartNumber);
    Products.row($(this).parents('tr')).remove().draw();
});

function ItemViewModel() {
    var self = this;

    self.PartNumber = ko.observable();
    self.Description = ko.observable();
    self.Stock = ko.observable();
    self.StorageLocation = ko.observable();
    self.MovementType = ko.observable(null);
    self.SelectedItems = ko.observableArray([]);

    /// <summary>
    /// Elimina un producto del arreglo de productos 
    /// Si la vista esta vacia desabilita los botones Cancelar y Guardar
    /// </summary>
    self.Remove = function (itemId) {
        self.SelectedItems.remove(function (item) {
            return item.PathNumber === itemId;
        });

        if (self.SelectedItems().length === 0) {
            $(".btn-danger").prop("disabled", true);
            $(".btn-success").prop("disabled", true);
        }
    }

    /// <summary>
    /// Consulta en la base de datos si el producto seleccionado 
    /// por el usuario se encuentra en existencia
    /// </summary>
    self.GetItem = function () {
        var itemId = $("#Item").val();
        $.ajax({
            url: "/Items/ActionGetItemStock",
            type: "GET",
            data: { "PartNumber": itemId },
            success: function (data) {
                if (data.Status) {
                    self.PartNumber(data.Item.PathNumber);
                    self.Description(data.Item.Description);
                    self.Stock(data.Item.Stock);
                    self.StorageLocation(data.Item.StorageLocation);
                    self.SelectItem();
                }
                else {
                    $("#Item").select2("val", "");
                    $("#Item").select2("open");
                    toastr.warning("Este producto no tiene locación.");
                }
            },
            error: function () {
                toastr.error("Ocurrió un error durante el procesamiento de la solicitud.");
            }
        });
    }

    /// <summary>
    /// Agrega el producto seleccionado por el usuario a la vista(datatable) y al arreglo de productos
    /// Valida que el producto seleccionado por el usuario no sea repetido
    /// </summary>
    self.SelectItem = function () {
        var product = [];
        var band = false;
        var PartNumber;
        var Description;
        var Stock;
        var StorageLocation;

        if (self.SelectedItems().length > 0) {
            self.SelectedItems().forEach(function (item) {
                if (item.PathNumber === self.PartNumber()) {
                    band = true;
                    $("#Item").select2("val", "");
                    $("#Item").select2("open");
                    toastr.warning("Producto ya seleccionado: " + item.Description);
                    return;
                }
            });

            if (!band) {
                PartNumber = self.PartNumber();
                Description = self.Description();
                Stock = self.Stock();
                StorageLocation = self.StorageLocation();

                product.push({ "PartNumber": PartNumber, "Description": Description, "Stock": Stock, "StorageLocation": StorageLocation });
                self.SelectedItems().push({ "PathNumber": PartNumber, "Description": Description, "Stock": Stock, "StorageLocation": StorageLocation, "MovementType": null, "Comment": null });
                Products.rows.add($(product));
                Products.columns.adjust().draw();
                $("#Item").select2("val", "");
                $("#Item").select2("open");
                $(".btn-danger").prop("disabled", false);
                $(".btn-success").prop("disabled", false);
            }
        }
        else {
            PartNumber = self.PartNumber();
            Description = self.Description();
            Stock = self.Stock();
            StorageLocation = self.StorageLocation();

            product.push({ "PartNumber": PartNumber, "Description": Description, "Stock": Stock, "StorageLocation": StorageLocation });
            self.SelectedItems().push({ "PathNumber": PartNumber, "Description": Description, "Stock": Stock, "StorageLocation": StorageLocation, "MovementType": null, "Comment": null });
            Products.rows.add($(product));
            Products.columns.adjust().draw();
            $("#Item").select2("val", "");
            $("#Item").select2("open");
            $(".btn-danger").prop("disabled", false);
            $(".btn-success").prop("disabled", false);
        }
    }

    /// <summary>
    /// Limpia las variables y desabilita los botones Cancelar y Guardar    
    /// </summary>
    self.Clear = function () {
        self.PartNumber(null);
        self.Description(null);
        self.Stock(null);
        self.SelectedItems.removeAll();
        Products.clear();
        Products.columns.adjust().draw();
        $(".btn-danger").prop("disabled", true);
        $(".btn-success").prop("disabled", true);
    }

    /// <summary>
    /// Confirmación antes de grabar la solicitud de ajuste de inventario
    /// </summary>
    self.Confirm = function () {
        if (self.SelectedItems().length > 0) {
            swal({
                title: "Esta seguro?",
                text: "Se solicitará el ajuste de inventario",
                type: "warning",
                closeOnConfirm: true,
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Continuar",
                cancelButtonText: "Cancelar"
            }, function (isConfirm) {
                setTimeout(function () {
                    if (isConfirm) {
                        StartLoading();
                        self.Save();
                    }
                }, 400)
            });
        }
    }

    /// <summary>
    /// Valida que la captura este completa
    /// Graba en base de datos la solicitud de ajuste de inventario
    /// </summary>
    self.Save = function () {
        var band = false;

        /// Recorre el datatable para validar que la captura este completa
        Products.rows().every(function (index, element) {
            var row = $(this.node());

            var part_number = $(row.find('td').eq(0)).text();

            var stock = parseFloat($(row.find('td').eq(3)).text());

            var quantity = row.find('td').eq(4);
            var quantityInput = $('input', quantity);
            var newstock = parseFloat(quantityInput.val());

            var creditIsCheck = row.find('td').eq(6);
            var credit = $('input', creditIsCheck).prop('checked');

            var debitIsCheck = row.find('td').eq(7);
            var debit = $('input', debitIsCheck).prop('checked');

            var Comment = row.find('td').eq(8);
            var comment = $('input', Comment).val();

            if (!isNaN(newstock)) {
                self.SelectedItems().forEach(function (item) {
                    if (item.PathNumber === part_number) {
                        //if (newstock === stock) {
                        //    toastr.warning("El nuevo stock capturado del producto: " + item.Description + " no puede ser el mismo del stock actual.");
                        //    band = true;
                        //    return false;
                        //}
                        //else
                        if (!credit && !debit) {
                            toastr.warning("Especifique el tipo de movimiento: Entrada ó Salida del producto: " + item.Description + ".");
                            band = true;
                            return false;
                        }
                        else if (comment.trim() == "") {
                            toastr.warning("Capture comentarios del producto: " + item.Description + ".");
                            band = true;
                            return false;
                        }
                        else {
                            item.Stock = newstock;
                            if (credit)
                                item.MovementType = true;
                            else
                                item.MovementType = false;
                            item.Comment = comment;
                        }
                    }
                });
            }
            else {
                toastr.warning('Capture el nuevo stock.');
                band = true;
                return false;
            }
        });

        /// Si la captura esta completa envia al controlador 
        /// la lista de productos para grabar en base de datos 
        if (!band) {
            $.ajax({
                url: "/Items/ActionSetItemStock/",
                type: "POST",
                data: ko.toJSON(self.SelectedItems()),
                dataType: "json",
                contentType: "application/json; charset=utf-8",
                success: function (result) {
                    if (!result)
                        SessionFalse("Se termino su sesion.");
                    else if (result) {
                        self.Clear();
                        swal({
                            title: "Folio: " + result.Document,
                            text: "Solicitud de ajuste de inventario creada satisfactoriamente!",
                            type: "success",
                            confirmButtonText: "Continuar"
                        });
                    }
                    else
                        toastr.error("Ocurrió un error durante el procesamiento de la solicitud.");
                    EndLoading();
                },
                error: function (result) {
                    EndLoading();
                    toastr.error("Ocurrió un error durante el procesamiento de la solicitud.");
                }
            });
        }
        else
            EndLoading();
    }
}

vm = new ItemViewModel();
ko.applyBindings(vm);

window.setTimeout(function () {
    $(".normalheader").addClass("small-header");
    $("#fa").removeClass("fa-arrow-up").addClass("fa-arrow-down");
    $("#hbreadcrumb").removeClass("m-t-lg");
}, 1000);

$(document).on("keypress", ".Numeric", function (event) {
    var keyValue = event.key;
    if (keyValue.match(/[{0-9},{.}]/g))
        return true;
    else
        return false;
});

$(document).on("blur", ".Numeric", function (event) {
    var keyValue = Number(event.target.value);
    if (isNaN(keyValue)) {
        toastr.error("La cantidad debe ser numérica.");
        $(this).val("");
        $(this).focus();
    } else {
        if (parseFloat(keyValue) > 999999) {
            toastr.error("La cantidad debe ser un número razonable.");
            $(this).val("");
            $(this).focus();
        }
    }
});

//$("#Item").select2({
//    minimumInputLength: 2,
//    formatInputTooShort: function () {
//        return "Busca un producto";
//    },
//    matcher: function (term, text, option) {
//        return text.toUpperCase().indexOf(term.toUpperCase()) >= 0 || option.val().toUpperCase().indexOf(term.toUpperCase()) >= 0;
//    }
//});