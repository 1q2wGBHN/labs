﻿formatmony = x => new Intl.NumberFormat("en-US", { style: "currency", currency: "USD", currencyDisplay: "symbol", maximumFractionDigits: 4 }).format(x)
tableTd = x => `<td>${x}</td>`
tabletr = x => `<tr>${tableTd(x.PartNumber)} ${tableTd(x.Description)} ${tableTd(x.Size)}  ${tableTd(formatmony(x.Price))} ${tableTd(x.Quantity)}   ${tableTd(formatmony(x.Import))}   ${tableTd(formatmony(x.Iva))}  ${tableTd(formatmony(x.Ieps))} ${tableTd(formatmony(x.Import + x.Iva + x.Ieps))}</tr>`
selectorFull = x => `<option value='${x.Id}'> ${x.Id}, ${x.Type}</option>`;
id = 0;

$(document).ready(function () {
    $('#datetimepicker').datepicker({
        autoclose: true,
        endDate: '01/01/2100',
        language: 'es',
        format: 'mm/dd/yyyy',
        todayHighlight: true,
    });

    $('#datetimepicker2').datepicker({
        autoclose: true,
        endDate: '01/01/2100',
        language: 'es',
        format: 'mm/dd/yyyy',
        todayHighlight: true,
    });

    $("#supplier").select2({
        minimumInputLength: 2,
        formatInputTooShort: function () {
            return "Busque un proveedor";
        },
        matcher: function (term, text, option) {
            return text.toUpperCase().indexOf(term.toUpperCase()) >= 0 || option.val().toUpperCase().indexOf(term.toUpperCase()) >= 0;
        }
    });

    $("#folio").select2();

    window.setTimeout(function () { $("#fa").click(); }, 1000);

    $('#buttonCancel').click(function () {
        if ($("#folio").val() > 0) {
            swal({
                title: "Desea Cancelar la entrada?",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Si",
                cancelButtonText: "No",
                closeOnConfirm: true,
                closeOnCancel: true
            }, function (isConfirm) {
                if (isConfirm)
                    Cancel();
            });
        }
    });

    isEnableOption21(false)
});

function isEnableOption21(value) {
    if (value) {
        $("#option21").show();
        $("#tableEMSCContent").show();
    }
    else {
        $("#option21").hide();
        $("#tableEMSCContent").hide();
    }
}

function SearchDate() {
    $("#option3").hide()
    isEnableOption21(false)
    var supplier = $("#supplier").val();
    var dateInit = $("#dateInit").val();
    var dateFin = $("#dateFin").val();
    if (dateInit == "") {
        toastr.error('Ingresa Fecha Inicial.');
        $("#dateInit").focus();
    }
    else if (dateFin == "") {
        toastr.error('Ingresa Fecha Final.');
        $("#dateFin").focus();
    }
    else {
        if (moment(dateFin) < moment(dateInit)) {
            toastr.error('Fecha Final No puede ser Menor a la Inicial.');
            $("#dateFin").focus();
        }
        else {
            $.ajax({
                url: "/EntryFree/ActionGetFolios",
                type: "GET",
                data: { "supplierId": supplier, "fechaIni": dateInit, "fechaFin": dateFin },
                success: function (response) {
                    if (response.success) {
                        listFolios = response.Json;
                        FillDropdown("folio", listFolios);
                        $("#option3").show()
                    }
                    else {
                        FillDropdown("folio", []);
                        toastr.error('Sin Folios para Ese proveedor en el rango de fechas');
                    }
                },
                error: function () {
                    FillDropdown("folio", []);
                    toastr.error(' Error inesperado contactar a sistemas.');
                }
            });
        }
    }
}

function FillDropdown(selector, vData) {
    $('#' + selector).html([`<option selected hidden value="0">Seleccione un Folio</option>`, ...vData.map(x => selectorFull(x))])
    $('#' + selector).val("")
    $("#folio option:first").prop('disabled', true);
}

function Search() {
    isEnableOption21(false)
    if ($("#folio").val() == "") { return false; }
    if ($("#folio").val() > 0) {
        axios.get("/EntryFree/ActionGetEntryFree/?EntryFreeId=" + $("#folio").val()).then(function (data) {
            if (data.data == "SF") {
                SessionFalse("Se termino su sesión")
            }
            else if (data.data == "N/A") {
                toastr.warning('Folio no existente');
            }
            else if (data.data.EntryFreeList.length > 0) {
                /*datos*/
                $("#supplierText").html(data.data.Supplier);
                $("#typeText").html(data.data.Type);
                $("#subTypeText").html(data.data.SubType);
                $("#dateText").html(data.data.Date);
                $("#commentary").html(data.data.Commentary)
                $("#reference").html(data.data.Reference)
                $("#phone").html(data.data.Phone)
                $("#userName").html(data.data.UserName)
                /*Relleno de tabla*/
                $("#tableEMSC").html(data.data.EntryFreeList.map(x => tabletr(x)))
                /*reducir*/

                importetotal = data.data.EntryFreeList.map(x => x.Import).reduce((total, a) => total + a, 0);
                ivatotal = data.data.EntryFreeList.map(x => x.Iva).reduce((total, a) => total + a, 0);
                iepstotal = data.data.EntryFreeList.map(x => x.Ieps).reduce((total, a) => total + a, 0)
                /**/
                $("#totalAmount").html(formatmony(importetotal));
                $("#totalIva").html(formatmony(ivatotal));
                $("#totalIEPS").html(formatmony(iepstotal));
                $("#totalFinal").html(formatmony((iepstotal + ivatotal + importetotal)));
                isEnableOption21(true)
            }
            else if (data.data.EntryFreeList.length == 0) {
                toastr.warning('Folio cancelado  o inexistente');
            }
            else {
                toastr.error('Alerta - Error inesperado  contactar a sistemas.');
            }
        }).catch(function (error) {
            toastr.error('Alerta - Error inesperado  contactar a sistemas.');
        }).then(function (data) {
        })
    }
    else
        toastr.warning('Selecciona una opcion');
}

function Print() {
    if ($("#folio").val() > 0) {
        StartLoading()
        axios.get("/EntryFree/ActionFolioEntryFree/?EntryFreeId=" + $("#folio").val()).then(function (data) {
            if (data.data == "SF") {
                SessionFalse("Se termino su sesión")
            }
            else if (data.data == "N/A") {
                toastr.warning('Folio cancelado o inexistente');
            }
            else if (data.data.success) {
                $("#myModal8").modal('show')
                id = $("#folio").val();
                $("#iframes").html(`<iframe src='data:application/pdf;base64,${data.data.responseText}' height='320px' width='100%'></iframe>`)
            }
            else {
                toastr.error('Alerta - Error inesperado  contactar a sistemas.');
            }
        }).catch(function (error) {
            toastr.error('Alerta - Error inesperado  contactar a sistemas.');
        }).then(function (data) {
            EndLoading()
        })
    }
    else
        toastr.warning('Selecciona una opcion');
}

function Cancel() {
    if ($("#folio").val() > 0) {
        axios.get("/EntryFree/ActionFolioCancelEntryFree/?EntryFreeId=" + $("#folio").val()).then(function (data) {
            if (data.data == "SF") {
                SessionFalse("Se termino su sesión")
            }
            else if (data.data == "N/A") {
                toastr.warning('Folio no existente');
            }
            else if (data.data) {
                toastr.success('Folio cancelado con exito');
            }
            else if (data.data == false) {
                toastr.warning('Folio cancelado previamente o inexistente');
            }
            else {
                toastr.error('Alerta - Error inesperado  contactar a sistemas.');
            }
        }).catch(function (error) {
            toastr.error('Alerta - Error inesperado  contactar a sistemas.');
        })
    }
    else
        toastr.warning('El folio no puede ser nulo');

    $('#supplier').val($("#supplier :first").val()).trigger('change');
    $('#folio').val($("#folio :first").val()).trigger('change');
    $("#dateInit").val("")
    $("#dateFin").val("");
    $("#option3").hide()
    isEnableOption21(false)
}

function Email() {
    StartLoading()
    axios.get("/EntryFree/ActionEmailEntryFree/?EntryFreeId=" + $("#folio").val()).then(function (data) {
        if (data.data == "SF") {
            SessionFalse("Se termino su sesión")
        }
        else if (data.data) {
            toastr.success('Mandado con exito el pdf');
        }
        else if (data.data == "N/A") {
            toastr.warning('No pudo mandarce el pdf');
        }
    }).catch(function (error) {
        toastr.error('Alerta - Error inesperado  contactar a sistemas.');
    }).then(function (data) {
        EndLoading()
    })

    EndLoading();
}

function Aprovate() {
    if ($("#folio").val() > 0) {
        StartLoading();
        axios.post("/EntryFree/ActionEditStatus/?EntryFreeId=" + $("#folio").val()).then(function (data) {
            if (data.data == "SF") {
                SessionFalse("Se termino su sesión")
            }
            else if (data.data == 0) {
                toastr.success('Se a aprovado con exito');
                $('#supplier').val($("#supplier :first").val()).trigger('change');
                $('#folio').val($("#folio :first").val()).trigger('change');
                $("#dateInit").val("")
                $("#dateFin").val("");
                $("#option3").hide()
                isEnableOption21(false)
            }
            else if (data.data == 80010) {
                toastr.error('Problema al guardar');
            }
            else if (data.data == 1002) {
                toastr.error('No se encontro la transferencia');
            }
            else if (data.data == 1001) {
                toastr.warning('Ya fue actualizado el estatus recarge la pagina');
            }
            else if (data.data == 8003) {
                toastr.warning('No existe registro');
            }
            else if (data.data == 8002) {
                toastr.warning('No esta confirmada');
            }
            else if (data.data == 8004) {
                toastr.warning('No puede ser mayor a lo requerido');
            }
            else if (data.data == 8005) {
                toastr.warning('No existe locacion');
            }
            else if (data.data == 8001) {
                toastr.warning('Error desconosido');
            }
            else {
                toastr.error('Error desconosido');
            }
        }).catch(function (error) {
            toastr.error('Alerta - Error inesperado  contactar a sistemas.');
        }).then(() => { EndLoading() })
    }
    else
        toastr.warning("Seleccione una opcion")
}