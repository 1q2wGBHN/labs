﻿const fomartmoney = money => decimal => new Intl.NumberFormat("en-US", { style: "currency", currency: "USD", currencyDisplay: "symbol", maximumFractionDigits: decimal }).format(money);
var part_number_local = '';
$(document).ready(function () {
    var model = JSON.parse($("#model").val());
    table.rows.add(model);
    table.columns.adjust().draw();
    $('#tableItems').animatePanel();
    $('.active').iCheck({ checkboxClass: 'icheckbox_square-green', });
});

$("#ItemsDrop").select2({
    multiple: true,
    minimumInputLength: 3,
    allowClear: true,
    placeholder: "Seleccione un producto",
    initSelection: function (element, callback) {
        callback({ id: "", text: "Seleccione una opcion" });
    },
    ajax: {
        url: "/Items/ActionSearchItemBarcode/",
        dataType: 'json',
        type: "GET",
        quietMillis: 50,
        data: function (Filter) {
            return {
                Filter: Filter
            };
        },
        results: function (data) {
            return {
                results: $.map(data.Json, function (item) {
                    return {
                        text: `${item.PathNumber} - ${item.Description}`,
                        id: item.PathNumber
                    }
                }),
            };
        }
    }
});

function FillDropdown(selector, vData) {
    if (vData != null) {
        var vItems = [];
        vItems.push('<option value="0">Seleccione una familia</option>');
        for (var i in vData) {
            if (vData[i].Selected)
                vItems.push('<option selected value="' + vData[i].class_id + '">' + vData[i].description + '</option>');
            else
                vItems.push('<option value="' + vData[i].class_id + '">' + vData[i].description + '</option>');
        }
        $('#' + selector).empty();
        $('#' + selector).append(vItems.join(''));
        $('#family').val($("#family :first").val()).trigger('change');
    }
    else {
        var vItems = [];
        vItems.push('<option selected value="0">Seleccione una familia</option>');
        $('#' + selector).empty();
        $('#' + selector).append(vItems.join(''));
        $('#family').val($("#family :first").val()).trigger('change');
    }
}

function GetFamily() {
    var depto = $("#department").val();
    if (depto == "" || depto == 0) {
        $('#family').val($("#family :first").val()).trigger('change');
        FillDropdown("family", null)
        return false;
    }
    $.ajax({
        url: '/MaClass/ActionGetFamily/',
        type: "GET",
        data: { "deptoo": depto },
        success: function (response) {
            if (response.success) {
                listFamily = response.Json;
                FillDropdown("family", listFamily);
            }
            else
                FillDropdown("family", null);
        },
        error: function () {
            FillDropdown("family", null)
            toastr.error('Error inesperado contactar a sistemas.');
        }
    });
}


function DetailProduct(part_number, buyer_division, descripcion, price_total, base_cost, total_stock, unit_size) {
    $('#PartNumberDescritionModal').html("<i class='fa fa-cube fa-2x'></i> #" + part_number + " - " + descripcion);
    $('#PriceSaleModal').html("<i class='fa fa-shopping-cart'></i> Precio Venta: $" + price_total);
    $('#BaseCostModal').html("<i class='fa fa-shopping-basket'></i> Ultimo Costo: $" + base_cost);
    $('#TotalStockModal').html("<i class='fa fa-cubes'></i> Existencia: " + total_stock + " " + unit_size);
    $('#BuyerModal').html("<i class='fa fa-legal'></i> Comprador: " + buyer_division);
    StartLoading();
    axios.get(`/ItemSuppliers/ActionGetListItemBaseCost/?PartNumber=${part_number}`)
        .then(data => {
            part_number_local = part_number;
            console.log("data.data");
            console.log(data.data);
            if (data.data == "SF")
                SessionFalse(sesion);
            else if (data.data.Pre.length > 0) {
                //Presentaciones
                tablePresentation.clear();
                tablePresentation.rows.add(data.data.Pre);
                tablePresentation.columns.adjust().draw();
                //Proveedores
                tableSup.clear();
                tableSup.rows.add(data.data.Sup);
                tableSup.columns.adjust().draw();
                //Precio Base
                tableBaseCost.clear();
                tableBaseCost.rows.add(data.data.Base);
                tableBaseCost.columns.adjust().draw();
                //Precio de Venta
                tableSalePrice.clear();
                tableSalePrice.rows.add(data.data.Sale);
                tableSalePrice.columns.adjust().draw();
                //Promocion
                tablePromo.clear();
                tablePromo.rows.add(data.data.Promo);
                tablePromo.columns.adjust().draw();
                //Precios Forzados
                tableSalePriceForced.clear();
                tableSalePriceForced.rows.add(data.data.Forced);
                tableSalePriceForced.columns.adjust().draw();
                $("#showProductCommon").modal("show");
            }
            else
                toastr.warning("No tiene informacion");
        })
        .catch(error => {
            toastr.error('Error inesperado contactar a sistemas.');
        }).then(() => { EndLoading() })
}


$('#Search').on('click', function () {
    var partNumbers = $("#ItemsDrop").val();
    var departmentid = parseInt($("#department").val());
    var familyid = parseInt($("#family").val());
    StartLoading();
    $.ajax({
        url: "/Items/ActionGetInfoProducts/",
        type: "GET",
        data: { "part_numbers": partNumbers, "deparment": departmentid, "family": familyid },
        success: function (result) {
            EndLoading();
            if (!result.success)
                SessionFalse("Se terminó su sesion.");
            else {
                table.clear();
                table.rows.add($(result.Json));
                table.columns.adjust().draw();
                $('#TableDiv').animatePanel();

                if (!$.isEmptyObject(result.Model))
                    $("#Print").removeAttr('disabled', 'disabled');
                else
                    $("#Print").attr('disabled', true);
            }
        },
        error: function () {
            EndLoading();
            $("#Print").attr('disabled', true);
            toastr.error('Error inesperado contactar a sistemas.');
        }
    });

});

var table = $('#tableItems').DataTable({
    oLanguage:
    {
        "sProcessing": "Procesando...",
        "sLengthMenu": "Mostrar _MENU_ registros",
        "sZeroRecords": "No se encontraron resultados",
        "sEmptyTable": "Ningún dato disponible en esta tabla",
        "sInfo": "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
        "sInfoEmpty": "Mostrando registros del 0 al 0 de un total de 0 registros",
        "sInfoFiltered": "(filtrado de un total de _MAX_ registros)",
        "sInfoPostFix": "",
        "sSearch": "Buscar:",
        "sUrl": "",
        "sInfoThousands": ",",
        "sLoadingRecords": "Cargando...",
        "oPaginate": { "sFirst": "Primero", "sLast": "Último", "sNext": "Siguiente", "sPrevious": "Anterior" },
        "oAria": { "sSortAscending": ": Activar para ordenar la columna de manera ascendente", "sSortDescending": ": Activar para ordenar la columna de manera descendente" }
    },
    dom: "<'row'<'col-sm-4'l><'col-sm-4 text-left'B><'col-sm-4'f>t<'col-sm-6'i><'col-sm-6'p>>",
    lengthMenu: [[10, 25, 50, -1], [10, 25, 50, "Todos"]],
    buttons: [
        { extend: 'csv', text: 'Excel', title: 'InformacionProductos', className: 'btn-sm' },
        {
            extend: 'pdf', text: 'PDF', title: 'InformacionProductos', className: 'btn-sm', exportOptions: {
                columns: [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19]
            },
            customize: function (doc) {
                doc.content[1].table.widths =
                    Array(doc.content[1].table.body[0].length + 1).join('*').split('');
            }
        },
        { extend: 'print', text: 'Imprimir', className: 'btn-sm' }
    ],
    columns:
        [
            { data: 'part_number' },
            { data: 'part_description' },
            { data: 'buyer_division' },
            { data: 'division' },
            { data: 'department' },
            { data: 'expirations_days' },
            { data: 'active_flag' },
            { data: 'weight_flag' },
            { data: 'price' },
            { data: 'part_iva_sale' },
            { data: 'iva_sale' },
            { data: 'part_iva' },
            { data: 'iva', },
            { data: 'part_ieps' },
            { data: 'ieps' },
            { data: 'total' },
            { data: null, width: "1%" },
            { data: 'unrestricted' },
            { data: 'map_price' },
            { data: 'last_purchase_price' },
            { data: null, width: "1%" }
        ],
    columnDefs:
        [
            {
                targets: [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19,20],
                width: "1%"
            },
            {
                targets: [8, 10, 12, 14, 15, 18, 19],
                orderable: false,
                data: null,
                width: "1%",
                render: function (data, type, row) {
                    return `${fomartmoney(data)(4)}`
                }
            },
            {
                render: function (data, type, row) {
                    if (data) {
                        return `<div class="icheckbox_square-green
                                checked disabled" style="position: relative;">
                                <input type="checkbox" disabled="" style="position: absolute;
                                opacity: 0;"><ins class="iCheck-helper" style="position: absolute; top: 0%;
                                left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px;
                                background: rgb(255, 255, 255); border: 0px; opacity: 0;"></ins></div>`
                    }
                    else
                        return `<div class="icheckbox_square-green
                                disabled" style="position: relative;"><input type="checkbox" disabled=""
                                style="position: absolute; opacity: 0;"><ins class="iCheck-helper" style="position: absolute;
                                top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px;
                                background: rgb(255, 255, 255); border: 0px; opacity: 0;"></ins></div>`;
                },
                targets: [6]
            },
            {
                render: function (data, type, row) {
                    if (data) {
                        return `<div class="icheckbox_square-green
                                checked disabled" style="position: relative;">
                                <input type="checkbox" disabled="" style="position: absolute;
                                opacity: 0;"><ins class="iCheck-helper" style="position: absolute; top: 0%;
                                left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px;
                                background: rgb(255, 255, 255); border: 0px; opacity: 0;"></ins></div>`
                    }
                    else
                        return `<div class="icheckbox_square-green
                                disabled" style="position: relative;"><input type="checkbox" disabled=""
                                style="position: absolute; opacity: 0;"><ins class="iCheck-helper" style="position: absolute;
                                top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px;
                                background: rgb(255, 255, 255); border: 0px; opacity: 0;"></ins></div>`;
                },
                targets: [7]
            },
            {
                render: function (data, type, row) {
                    var supplierCost = row.last_purchase_price;
                    var ieps = parseInt(row.ieps);
                    var ieps2 = 1 + (ieps / 100);
                    var costoConIEPS = ieps2 * supplierCost;
                    var x = costoConIEPS / parseFloat(row.total);
                    var margen = (1 - x) * 100;
                    return `${fomartmoney(margen)(4)}`
                },
                targets: [16]
            },
            {
                render: function (data, type, full, meta) {
                    console.log(data);
                    return `<button class="btn btn-xs btn-outline btn-info" onclick="DetailProduct('${data.part_number}', '${data.buyer_division}', '${data.part_description}', '${data.price}','${data.last_purchase_price}','${data.unrestricted}', '${data.unit}')"><i class="fa fa-cogs"></i> Detalles</button>`
                },
                targets: [20]
            }
        ]
});

$('#tableItems tbody').on('click', '#bt2', function (e) {
    values = table.row($(this).parents('tr')).data();
    $('#txtItemName').html(values.part_description);
    $('#txtItemPartNumber').html(values.part_number);
    $('#txtItemType').html(values.part_type);
    $('#Dunit').html(values.unit);
    $('#division').html(values.buyer_division);
    $('#description').html(values.part_description);
    if (values.active_flag) {
        $('#active').prop("checked", true);
        $('#activeCheck').html('Activo');
    }
    else {
        $('#active').prop("checked", false);
        $('#activeCheck').html('No Activo');
    }
    if (!values.weight_flag) {
        $('#weight').prop("checked", false);
        $('#weightCheck').html('No Pesado');
    }
    else {
        $('#weight').prop("checked", true);
        $('#weightCheck').html('Pesado');
    }
    if (values.extra_items_flag == false) {
        $('#cbExtraItem').prop("checked", false);
    }
    else {
        $('#cbExtraItem').prop("checked", true);
    }

    $('#active').iCheck({ checkboxClass: 'icheckbox_square-green', });
    $('#weight').iCheck({ checkboxClass: 'icheckbox_square-green', });
    $('#active').iCheck('update');
    $('#weight').iCheck('update');
    $('#active').iCheck('disable');
    $('#weight').iCheck('disable');
    $('#SATUnit').html(values.unit_sat);
    $('#SATPartNumber').html(values.part_number_sat);
    $('#IVA').html(values.part_iva);
    $('#IVASale').html(values.part_iva_sale);
    $('#IEPS').html(values.part_ieps);
    $('#showItem').modal('show');
});
var tableSup = $('#tableSuppliers').DataTable({
    oLanguage:
    {
        "sProcessing": "Procesando...",
        "sLengthMenu": "Mostrar _MENU_ registros",
        "sZeroRecords": "No se encontraron resultados",
        "sEmptyTable": "Ningún dato disponible en esta tabla",
        "sInfo": "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
        "sInfoEmpty": "Mostrando registros del 0 al 0 de un total de 0 registros",
        "sInfoFiltered": "(filtrado de un total de _MAX_ registros)",
        "sInfoPostFix": "",
        "sSearch": "Buscar:",
        "sUrl": "",
        "sInfoThousands": ",",
        "sLoadingRecords": "Cargando...",
        "oPaginate": { "sFirst": "Primero", "sLast": "Último", "sNext": "Siguiente", "sPrevious": "Anterior" },
        "oAria": { "sSortAscending": ": Activar para ordenar la columna de manera ascendente", "sSortDescending": ": Activar para ordenar la columna de manera descendente" }
    },
    dom: "<'row'<'col-sm-4'l><'col-sm-4 text-left'B><'col-sm-4'f>t<'col-sm-6'i><'col-sm-6'p>>",
    lengthMenu: [[10, 25, 50, -1], [10, 25, 50, "Todos"]],
    buttons: [],
    columns:
        [
            { data: 'Supplier' },
            { data: 'PartNumber' },
            { data: 'Cost' },
        ],
    columnDefs:
        [
            {
                targets: [0, 1],
                width: "1%"
            },
            {
                targets: [2],
                orderable: false,
                data: null,
                width: "1%",
                render: function (data, type, row) {
                    return `${fomartmoney(data)(4)}`
                }
            }
        ]
});

var tablePresentation = $('#tablePresentation').DataTable({
    oLanguage:
    {
        "sProcessing": "Procesando...",
        "sLengthMenu": "Mostrar _MENU_ registros",
        "sZeroRecords": "No se encontraron resultados",
        "sEmptyTable": "Ningún dato disponible en esta tabla",
        "sInfo": "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
        "sInfoEmpty": "Mostrando registros del 0 al 0 de un total de 0 registros",
        "sInfoFiltered": "(filtrado de un total de _MAX_ registros)",
        "sInfoPostFix": "",
        "sSearch": "Buscar:",
        "sUrl": "",
        "sInfoThousands": ",",
        "sLoadingRecords": "Cargando...",
        "oPaginate": { "sFirst": "Primero", "sLast": "Último", "sNext": "Siguiente", "sPrevious": "Anterior" },
        "oAria": { "sSortAscending": ": Activar para ordenar la columna de manera ascendente", "sSortDescending": ": Activar para ordenar la columna de manera descendente" }
    },
    dom: "<'row'<'col-sm-4'l><'col-sm-4 text-left'B><'col-sm-4'f>t<'col-sm-6'i><'col-sm-6'p>>",
    lengthMenu: [[10, 25, 50, -1], [10, 25, 50, "Todos"]],
    buttons: [],
    columns:
        [
            { data: 'Barcode' },
            { data: 'Description' },
            { data: 'SalePriceUnit' },
            { data: 'SalePrice' },
            { data: 'UnitSize' },
            { data: 'Count' },
            { data: 'Active_Flag' },
        ],
    columnDefs:
        [
            {
                targets: [0, 1],
                width: "1%"
            },
            {
                targets: [2, 3],
                data: null,
                width: "1%",
                render: function (data, type, row) {
                    return `${fomartmoney(data)(4)}`
                }
            },
            {
                targets: [6],
                data: null,
                width: "1%",
                render: function (data, type, row) {
                    if (data == "Activo")
                        return `<b style="color:#198c19;">${data}</b>`;
                    else if (data == "Presentación Activa")
                        return `<b style="color:#198c19;">${data}</b>`;
                    else if (data == "Desactivada")
                        return `<b style="color:#aaaaaa;">${data}</b>`;
                    return data;
                }
            }
        ]
});

var tableBaseCost = $('#tableBase').DataTable({
    oLanguage:
    {
        "sProcessing": "Procesando...",
        "sLengthMenu": "Mostrar _MENU_ registros",
        "sZeroRecords": "No se encontraron resultados",
        "sEmptyTable": "Ningún dato disponible en esta tabla",
        "sInfo": "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
        "sInfoEmpty": "Mostrando registros del 0 al 0 de un total de 0 registros",
        "sInfoFiltered": "(filtrado de un total de _MAX_ registros)",
        "sInfoPostFix": "",
        "sSearch": "Buscar:",
        "sUrl": "",
        "sInfoThousands": ",",
        "sLoadingRecords": "Cargando...",
        "oPaginate": { "sFirst": "Primero", "sLast": "Último", "sNext": "Siguiente", "sPrevious": "Anterior" },
        "oAria": { "sSortAscending": ": Activar para ordenar la columna de manera ascendente", "sSortDescending": ": Activar para ordenar la columna de manera descendente" }
    },
    dom: "<'row'<'col-sm-4'l><'col-sm-4 text-left'B><'col-sm-4'f>t<'col-sm-6'i><'col-sm-6'p>>",
    lengthMenu: [[10, 25, 50, -1], [10, 25, 50, "Todos"]],
    buttons: [],
    columns:
        [
            { data: 'item_number' },
            { data: 'proveedor' },
            { data: 'base_cost' },
            { data: 'fechainicio' },
            { data: 'user' },
            { data: 'active_flag' },
        ],
    columnDefs:
        [
            {
                targets: [0, 1],
                width: "1%"
            },
            {
                targets: [2],
                data: null,
                width: "1%",
                render: function (data, type, row) {
                    return `${fomartmoney(data)(4)}`
                }
            },
            {
                targets: [3],
                data: null,
                width: "1%",
                render: function (data, type, row) {
                    return moment(data).format('DD/MM/YYYY');
                }
            },
            {
                targets: [5],
                data: null,
                width: "1%",
                render: function (data, type, row) {
                    if (data == "Activo")
                        return `<b style="color:#198c19;">${data}</b>`;
                    return data;
                }
            }
        ]
});

var tableSalePrice = $('#tableSale').DataTable({
    oLanguage:
    {
        "sProcessing": "Procesando...",
        "sLengthMenu": "Mostrar _MENU_ registros",
        "sZeroRecords": "No se encontraron resultados",
        "sEmptyTable": "Ningún dato disponible en esta tabla",
        "sInfo": "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
        "sInfoEmpty": "Mostrando registros del 0 al 0 de un total de 0 registros",
        "sInfoFiltered": "(filtrado de un total de _MAX_ registros)",
        "sInfoPostFix": "",
        "sSearch": "Buscar:",
        "sUrl": "",
        "sInfoThousands": ",",
        "sLoadingRecords": "Cargando...",
        "oPaginate": { "sFirst": "Primero", "sLast": "Último", "sNext": "Siguiente", "sPrevious": "Anterior" },
        "oAria": { "sSortAscending": ": Activar para ordenar la columna de manera ascendente", "sSortDescending": ": Activar para ordenar la columna de manera descendente" }
    },
    dom: "<'row'<'col-sm-4'l><'col-sm-4 text-left'B><'col-sm-4'f>t<'col-sm-6'i><'col-sm-6'p>>",
    lengthMenu: [[10, 25, 50, -1], [10, 25, 50, "Todos"]],
    buttons: [],
    columns:
        [
            { data: 'item_number' },
            { data: 'margin' },
            { data: 'item_price' },
            { data: 'item_price_iva' },
            { data: 'fechainicio' },
            { data: 'user' },
            { data: 'active_flag' },
        ],
    columnDefs:
        [
            {
                targets: [0, 1],
                width: "1%"
            },
            {
                targets: [2, 3],
                data: null,
                width: "1%",
                render: function (data, type, row) {
                    return `${fomartmoney(data)(4)}`
                }
            },
            {
                targets: [4],
                data: null,
                width: "1%",
                render: function (data, type, row) {
                    return moment(data).format('DD/MM/YYYY');
                }
            },
            {
                targets: [6],
                data: null,
                width: "1%",
                render: function (data, type, row) {
                    if (data == "Activo")
                        return `<b style="color:#198c19;">${data}</b>`;
                    return data;
                }
            }
        ]
});


var tablePromo = $('#tablePromotion').DataTable({
    oLanguage:
    {
        "sProcessing": "Procesando...",
        "sLengthMenu": "Mostrar _MENU_ registros",
        "sZeroRecords": "No se encontraron resultados",
        "sEmptyTable": "Ningún dato disponible en esta tabla",
        "sInfo": "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
        "sInfoEmpty": "Mostrando registros del 0 al 0 de un total de 0 registros",
        "sInfoFiltered": "(filtrado de un total de _MAX_ registros)",
        "sInfoPostFix": "",
        "sSearch": "Buscar:",
        "sUrl": "",
        "sInfoThousands": ",",
        "sLoadingRecords": "Cargando...",
        "oPaginate": { "sFirst": "Primero", "sLast": "Último", "sNext": "Siguiente", "sPrevious": "Anterior" },
        "oAria": { "sSortAscending": ": Activar para ordenar la columna de manera ascendente", "sSortDescending": ": Activar para ordenar la columna de manera descendente" }
    },
    dom: "<'row'<'col-sm-4'l><'col-sm-4 text-left'B><'col-sm-4'f>t<'col-sm-6'i><'col-sm-6'p>>",
    lengthMenu: [[10, 25, 50, -1], [10, 25, 50, "Todos"]],
    buttons: [],
    columns:
        [
            {
                "class": "details-control",
                "orderable": false,
                "data": null,
                "defaultContent": "",
            },
            { data: 'promotion_code' },
            { data: 'promotion_descrip' },
            { data: 'promotion_date_start' },
            { data: 'promotion_date_end' },
            { data: 'promotion_create_user' },
            { data: 'promotion_type' },
        ],
    columnDefs:
        [
            {
                targets: [3,4],
                data: null,
                width: "1%",
                render: function (data, type, row) {
                    return moment(data).format('MMM/DD/YYYY');
                }
            }
        ]
});

var detailRows = [];
$('#tablePromotion tbody').on('click', 'tr td.details-control', function () {
    var tr = $(this).closest('tr');
    var row = tablePromo.row(tr);
    if (row.child.isShown()) {
        row.child.hide();
        tr.removeClass('details');
    }
    else {
        if (tablePromo.row('.details').length) {
            $('.details-control', tablePromo.row('.details').node()).click();
        }
        row.child(formatOne(row.data())).show();
        tr.addClass('details');
    }
});

tablePromo.on('draw', function () {
    $.each(detailRows, function (i, id) {
        $('#' + id + ' td.details-control').trigger('click');
    });
});

function formatOne(d) {
    var detailItems = "";
    var tabledetail = $('<div/>').addClass('loading').text('Cargando Datos...');
    var styleColor = '';
    $.ajax({
        url: "/Promotion/ActionGetPromotionById",
        data: { "FolioPromotion": d.promotion_code, "type": d.promotion_type },
        type: "GET",
        success: function (Data) {
            $.each(Data, function (index, value) {
                if (value.part_number == part_number_local)
                    styleColor = "class='success';";
                else
                    styleColor = "";
                if (d.promotion_type == "Avanzada") {
                    if (value.common == 0) {
                        detailItems += `<tr ${styleColor}><td> ${value.part_number}</td><td>${value.description}</td><td>${value.quantity}</td><td>${fomartmoney(value.price_original)(4)}</td><td>${fomartmoney(value.price)(4)}</td><td><button class='btn btn-xs btn-primary' onclick='MessageNoProducts()'><i class='fa fa-cubes'></i> Productos</button></td>`;
                    } else {
                        detailItems += `<tr ${styleColor}><td> ${value.part_number}</td><td>${value.description}</td><td>${value.quantity}</td><td>${fomartmoney(value.price_original)(4)}</td><td>${fomartmoney(value.price)(4)}</td><td><button class="btn btn-xs btn-success" onclick="ShowPromotionCommonItem('${value.promotion_code}', '${value.part_number}', '${value.price}', '${value.quantity}', '${value.description}')"><i class="fa fa-cubes"></i> Productos</button></td>`;
                    }
                }
                else
                    detailItems += `<tr ${styleColor}><td>${value.part_number}</td><td>${value.description}</td><td>${fomartmoney(value.price_original)(4)}</td><td>${fomartmoney(value.price)(4)}</td>`
            });
            if (d.promotion_type == "Avanzada")
                tabledetail.html('<table id="tabledetail' + d.promotion_code + '" class="table table-striped table-bordered table-hover" style="width:100%"><thead><tr><th>Codigo</th><th>Descripcion</th><th>Cantidad</th><th>Monto Original</th><th>Precio Promocion</th><th>Comun</th></tr></thead><tbody>' + detailItems + '</tbody></table>').removeClass('loading');
            else
                tabledetail.html('<table id="tabledetail' + d.promotion_code + '" class="table table-striped table-bordered table-hover" style="width:100%"><thead><tr><th>Codigo</th><th>Descripcion</th><th>Precio Original</th><th>Precio Promocion</th></tr></thead><tbody>' + detailItems + '</tbody></table>').removeClass('loading');
            reloadStyleTable(d);
        }
    });
    return tabledetail;
}

function reloadStyleTable(d) {
    $('#tabledetail' + d.promotion_code).DataTable({
        oLanguage: {
            "sProcessing": "Procesando...", "sLengthMenu": "Mostrar _MENU_ registros", "sZeroRecords": "No se encontraron resultados", "sEmptyTable": "Ningun dato disponible en esta tabla", "sInfo": "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros", "sInfoEmpty": "Mostrando registros del 0 al 0 de un total de 0 registros", "sInfoFiltered": "(filtrado de un total de _MAX_ registros)", "sInfoPostFix": "", "sSearch": "Buscar:", "sUrl": "", "sInfoThousands": ",", "sLoadingRecords": "Cargando...", "oPaginate": { "sFirst": "Primero", "sLast": "Ãltimo", "sNext": "Siguiente", "sPrevious": "Anterior" }, "oAria": { "sSortAscending": ": Activar para ordenar la columna de manera ascendente", "sSortDescending": ": Activar para ordenar la columna de manera descendente" }
        }
    });
}

function MessageNoProducts() {
    toastr.warning("Este producto no tiene productos en común.");
}


var tableCommons = $('#TableCommons').DataTable({
    "autoWidth": true,
    oLanguage: {
        "sProcessing": "Procesando...", "sLengthMenu": "Mostrar _MENU_ registros", "sZeroRecords": "No se encontraron resultados", "sEmptyTable": "NingÃºn dato disponible en esta tabla", "sInfo": "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros", "sInfoEmpty": "Mostrando registros del 0 al 0 de un total de 0 registros", "sInfoFiltered": "(filtrado de un total de _MAX_ registros)", "sInfoPostFix": "", "sSearch": "Buscar:", "sUrl": "", "sInfoThousands": ",", "sLoadingRecords": "Cargando...", "oPaginate": { "sFirst": "Primero", "sLast": "Ãltimo", "sNext": "Siguiente", "sPrevious": "Anterior" }, "oAria": { "sSortAscending": ": Activar para ordenar la columna de manera ascendente", "sSortDescending": ": Activar para ordenar la columna de manera descendente" }
    },
    dom: "<'row'<'col-sm-4'l><'col-sm-4 text-left'B><'col-sm-4'f>t<'col-sm-6'i><'col-sm-6'p>>",
    lengthMenu: [[10, 25, 50, -1], [10, 25, 50, "Todos"]],
    buttons: [
        { extend: 'csv', text: 'Excel', title: 'Gastos de Tienda', className: 'btn-sm' },
        {
            extend: 'pdf', text: 'PDF', title: 'Gastos de Tienda', className: 'btn-sm', exportOptions: {
                columns: [0, 1, 2, 3, 4, 5, 6]
            },
            customize: function (doc) {
                doc.content[1].table.widths =
                    Array(doc.content[1].table.body[0].length + 1).join('*').split('');
            }
        },
        { extend: 'print', text: 'Imprimir', className: 'btn-sm' },
    ],
    columns: [
        { targets: 1, data: 'part_number' },
        { targets: 2, data: 'description' },
        { targets: 3, data: 'price' },

    ],
    columnDefs: [{
        targets: [0, 1, 2],
        width: "1%"
    }]
});

function ShowPromotionCommonItem(promotion_code, common, price_promo, quantity_promo, descrip_part_number) {
    $.ajax({
        type: "GET",
        url: "/Promotion/ActionGetPromotionCommon",
        data: { "promotion_code": promotion_code, "common": common, },
        success: function (response) {
            if (response.status) {
                $('#folioOferta').html(promotion_code);
                $('#descriptionOferta').html(common);
                $('#tipoOferta').html(descrip_part_number);
                $('#precioOferta').html("$ " + price_promo);
                $('#cantidadOFerta').html(quantity_promo);
                tableCommons.clear();
                tableCommons.rows.add(response.model);
                tableCommons.columns.adjust().draw();
                $('#showProductCommon').modal('toggle');
                setTimeout(function () { $('#showStores').modal({ backdrop: 'static', keyboard: false }); }, 400);
                //$('#showStores').modal('show');
            } else {
                SessionFalse("Termino su sesión");
            }
        },
        error: function (response) {
            toastr.error('Error inesperado contactar a sistemas.' + response + '');
        }
    });
}

function CloseModalTwo() {
    $('#showStores').modal('hide');//ModalAddPages
    setTimeout(function () { $('#showProductCommon').modal({ backdrop: 'static', keyboard: false }); }, 400);
}

var tableSalePriceForced = $('#tableSaleForced').DataTable({
    oLanguage:
    {
        "sProcessing": "Procesando...",
        "sLengthMenu": "Mostrar _MENU_ registros",
        "sZeroRecords": "No se encontraron resultados",
        "sEmptyTable": "Ningún dato disponible en esta tabla",
        "sInfo": "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
        "sInfoEmpty": "Mostrando registros del 0 al 0 de un total de 0 registros",
        "sInfoFiltered": "(filtrado de un total de _MAX_ registros)",
        "sInfoPostFix": "",
        "sSearch": "Buscar:",
        "sUrl": "",
        "sInfoThousands": ",",
        "sLoadingRecords": "Cargando...",
        "oPaginate": { "sFirst": "Primero", "sLast": "Último", "sNext": "Siguiente", "sPrevious": "Anterior" },
        "oAria": { "sSortAscending": ": Activar para ordenar la columna de manera ascendente", "sSortDescending": ": Activar para ordenar la columna de manera descendente" }
    },
    dom: "<'row'<'col-sm-4'l><'col-sm-4 text-left'B><'col-sm-4'f>t<'col-sm-6'i><'col-sm-6'p>>",
    lengthMenu: [[10, 25, 50, -1], [10, 25, 50, "Todos"]],
    buttons: [],
    columns:
        [
            { data: 'item_number' },
            { data: 'margin' },
            { data: 'item_price' }, //Viejo
            { data: 'item_price_iva' }, //Nuevo
            { data: 'fechainicio' },
            { data: 'user' },
            { data: 'active_flag' },
        ],
    columnDefs:
        [
            {
                targets: [0, 1],
                width: "1%"
            },
            {
                targets: [2, 3],
                data: null,
                width: "1%",
                render: function (data, type, row) {
                    return `${fomartmoney(data)(4)}`
                }
            },
            {
                targets: [4],
                data: null,
                width: "1%",
                render: function (data, type, row) {
                    return moment(data).format('DD/MM/YYYY');
                }
            },
            {
                targets: [6],
                data: null,
                width: "1%",
                render: function (data, type, row) {
                    if (data == "Activo")
                        return `<b style="color:#198c19;">${data}</b>`;
                    return data;
                }
            }
        ]
});