﻿const formatmoney = money => decimal => new Intl.NumberFormat("en-US", { style: "currency", currency: "USD", currencyDisplay: "symbol", maximumFractionDigits: decimal }).format(money);

$(document).ready(function () {
    window.setTimeout(function () { $("#fa").click(); }, 1000);

    $('#cbox_ReportType').iCheck({
        checkboxClass: 'icheckbox_square-green',
    }).on('ifClicked', function () {
        $(this).trigger("click");
    });
});
var detailTransfer = false;


$("#ValidDate1").datepicker({
    autoclose: true,
    todayHighlight: true,
    showButtonPanel: true,
}).on("changeDate", function (e) {
    if (moment($('#ValidDate1').val()) <= moment($('#ValidDate2').val())) {
        SearchTransfer();
    }
    
});

$("#ValidDate2").datepicker({
    autoclose: true,
    todayHighlight: true,
}).on("changeDate", function (e) {
    SearchTransfer();
});


$("#ItemSelect").select2({
    minimumInputLength: 3,
    allowClear: true,
    placeholder: "Seleccione una producto",
    initSelection: function (element, callback) {
        callback({ id: "", text: "Seleccione una opcion" });
    },
    ajax: {
        url: "/Items/ActionSearchItem/",
        dataType: 'json',
        type: "GET",
        quietMillis: 50,
        data: function (Filter) {
            return {
                Filter: Filter
            };
        },
        results: function (data) {
            return {
                results: $.map(data.Json, function (item) {
                    return {
                        text: item.Description,
                        id: item.PathNumber
                    }
                }),
            };
        }
    }
});

function SelectorTransfer() {
    if ($("#SelectOptions").val() == "salida") {
        $("#statusTransferOut").show();
        $("#statusTransferIn").hide();
        console.log("salidas");
    } else {
        $("#statusTransferIn").show();
        $("#statusTransferOut").hide();
        console.log("entradas");
    }
}

function SearchClear() {
    ["statusIn", "statusOut", "depto", "family", "ItemSelect", "sites"].map(id => $(`#${id}`).val(""))
    $("#statusIn").val("").trigger("change");
    $("#statusOut").val("").trigger("change");
    $("#depto").val("0").trigger("change");
    $("#family").val("").trigger("change");
    $("#ItemSelect").val("").trigger("change");
    $("#sites").val("").trigger("change");
    $("#ValidDate1").val("");
    $("#ValidDate2").val("");
}

function checkDetail() {
    if ($("#cbox_ReportType").is(':checked')) {
        $("#TableDivDetail").show();
        $("#TableDiv").hide();
        detailTransfer = true;
    } else {
        $("#TableDiv").show();
        $("#TableDivDetail").hide();
        detailTransfer = false;
    }
    SearchTransfer();
}

function GetFamily() {
    var deptoo = $("#depto").val();
    if (deptoo == "") {
        FillDropdown("family", null)
        return false;
    }
    $.ajax({
        url: "/MaClass/ActionGetFamily",
        type: "GET",
        data: { "deptoo": deptoo },
        success: function (response) {
            if (response.success) {
                listFamily = response.Json;
                FillDropdown("family", listFamily)
            }
            else
                FillDropdown("family", null)
        },
        error: function () {
            FillDropdown("family", null)
            toastr.error('Error inesperado contactar a sistemas.');
        }
    });
}

function FillDropdown(selector, vData) {
    if (vData != null) {
        $('#' + selector).trigger("change");
        var vItems = [];
        vItems.push('<option value="">Seleccione una familia</option>');
        for (var i in vData) {
            if (vData[i].Selected)
                vItems.push('<option selectedselected=selected value="' + vData[i].class_id + '">' + vData[i].description + '</option>');
            else
                vItems.push('<option value="' + vData[i].class_id + '">' + vData[i].description + '</option>');
        }
        $('#' + selector).empty();
        $('#' + selector).append(vItems.join(''));
        $('#' + selector).val("").trigger('change');
    }
    else {
        var vItems = [];
        vItems.push('<option value="">Seleccione una familia</option>');
        $('#' + selector).empty();
        $('#' + selector).append(vItems.join(''));
        $('#' + selector).val("").trigger('change');
    }
}

const fomartmoney = money => decimal => new Intl.NumberFormat("en-US", { style: "currency", currency: "USD", currencyDisplay: "symbol", maximumFractionDigits: decimal }).format(money);


var table = $('#tableTransfers').DataTable({
    oLanguage:
    {
        "sProcessing": "Procesando...",
        "sLengthMenu": "Mostrar _MENU_ registros",
        "sZeroRecords": "No se encontraron resultados",
        "sEmptyTable": "Ningún dato disponible en esta tabla",
        "sInfo": "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
        "sInfoEmpty": "Mostrando registros del 0 al 0 de un total de 0 registros",
        "sInfoFiltered": "(filtrado de un total de _MAX_ registros)",
        "sInfoPostFix": "",
        "sSearch": "Buscar:",
        "sUrl": "",
        "sInfoThousands": ",",
        "sLoadingRecords": "Cargando...",
        "oPaginate": { "sFirst": "Primero", "sLast": "Último", "sNext": "Siguiente", "sPrevious": "Anterior" },
        "oAria": { "sSortAscending": ": Activar para ordenar la columna de manera ascendente", "sSortDescending": ": Activar para ordenar la columna de manera descendente" }
    },
    dom: "<'row'<'col-sm-4'l><'col-sm-4 text-left'B><'col-sm-4'f>t<'col-sm-6'i><'col-sm-6'p>>",
    lengthMenu: [[10, 25, 50, -1], [10, 25, 50, "Todos"]],
    buttons: [ ],
    
    columns:
        [
            {
                "class": "details-control",
                "orderable": false,
                "data": null,
                "defaultContent": "",
                width: "1%"
            },
            { data: 'TransferDocument' },
            { data: 'TransferSiteName' },
            { data: 'DestinationSiteName' },
            { data: 'TransferDate' },
            { data: 'Status' },
            { data: 'Comment' },
            { data: 'TotalAmount' },
            { data: 'TotalIva' },
            { data: 'TotalIEPS' },
            { data: null },
            { data: null }
        ],
    columnDefs:
        [
        {
            targets: [6],
            width: "12rem"
        },
        {
            targets: [7, 8, 9],
            render: function (data, type, row) {
                return `${fomartmoney(data)(2)}`
            }
        },
        {
            targets: [10],
            render: function (data, type, row) {
                return `${fomartmoney(row.TotalIva + row.TotalIEPS + row.TotalAmount)(2)}`
            }
        },
        {
            targets: [11],
            render: function (data, type, full, meta) {
                if ($("#SelectOptions").val() === "salida") {
                    return `<button class="btn btn-xs btn-outline btn-info" id="${data.TransferDocument}" onClick = "AprovalPrint('${data.TransferDocument}','${data.TransferSiteCode}','${data.DestinationSiteCode}')"><i class="fa fa-info-circle"></i> Detalle</button>`;
                } else {
                    return `<button class="btn btn-xs btn-outline btn-info" id="${data.TransferDocument}" onClick = "PrintReport('${data.TransferDocument}','${data.TransferSiteCode}','${data.DestinationSiteCode}')"><i class="fa fa-info-circle"></i> Detalle</button>`;
                }

            }
        }]
});

var detailRows = [];
$('#tableTransfers tbody').on('click', 'tr td.details-control', function () {
    var tr = $(this).closest('tr');
    var row = table.row(tr);

    if (row.child.isShown()) {
        row.child.hide();
        tr.removeClass('details');
    }
    else {
        if (table.row('.details').length) {
            $('.details-control', table.row('.details').node()).click();
        }
        row.child(format(row.data())).show();
        tr.addClass('details');
    }
});

table.on('draw', function () {
    $.each(detailRows, function (i, id) {
        $('#' + id + ' td.details-control').trigger('click');
    });
});

//Tabla detalles
function format(d) {
    var url;
    if ($("#SelectOptions").val() == "salida")
        url = "/TransferHeader/ActionGetAllTransferDetailsReport/";
    else
        url = "/TransferHeader/ActionGetTransferDetailIn/";
    var detailItems = "";
    var tabledetail = $('<div/>').addClass('loading').text('Cargando Datos...');
    $.ajax({
        url: url,
        data: { "Document": d.TransferDocument, "TransferSiteCode": d.TransferSiteCode },
        type: "GET",
        success: function (data) {
            if (data == "SF") {
                SessionFalse("Terminó tu sesión");
            }
            else {
                var time = new Date().valueOf();
                if ($("#SelectOptions").val() == "salida") {
                    $.each(data, function (index, value) {
                        detailItems += "<tr><td>" + value.transfer_status_desc + "</td><td>" + value.PartNumber + "</td><td>" + value.Description + "</td><td>" + value.Quantity + "</td><td>" + value.UnitCost + "</td><td>" + value.Iva + "</td><td>" + value.IEPS + "</td><td>" + value.SubTotal + "</td><td>" + value.Amount + "</td>";
                    });
                    if (data.length != 0) {
                        tabledetail.html('<table id="tabledetail' + time + '" class="table table-striped table-bordered table-hover" style="width:100%"><thead><tr><th>Estatus</th><th>Codigo</th><th>Descripcion</th><th>Cantidad</th><th>Costo unitario</th><th>IVA</th><th>IEPS</th><th>SubTotal</th><th>Total</th></tr></thead><tbody>' + detailItems + '</tbody></table>').removeClass('loading');
                        reloadStyleTable(time);
                    }
                    tabledetail.html('<table id="tabledetail' + time + '" class="table table-striped table-bordered table-hover" style="width:100%"><thead><tr><th>Estatus</th><th>Codigo</th><th>Descripcion</th><th>Cantidad</th><th>Costo Unitario</th><th>IVA</th><th>IEPS</th><th>SubTotal</th><th>Total</th></tr></thead><tbody>' + detailItems + '</tbody></table>').removeClass('loading');
                    reloadStyleTable(time);
                } else {
                    $.each(data, function (index, value) {
                        detailItems += "<tr><td>" + value.transfer_status_desc  + "</td><td>" + value.PartNumber + "</td><td>" + value.Description + "</td><td>" + value.Quantity + "</td><td>" + value.UnitCost + "</td><td>" + value.Iva + "</td><td>" + value.IEPS + "</td><td>" + value.SubTotal + "</td><td>" + value.Amount + "</td>";
                    });
                    if (data.length != 0) {
                        tabledetail.html('<table id="tabledetail' + time + '" class="table table-striped table-bordered table-hover" style="width:100%"><thead><tr><th >Estatus</th><th>Codigo</th><th>Descripcion</th><th>Cantidad</th><th>Costo unitario</th><th>IVA</th><th>IEPS</th><th>SubTotal</th><th>Total</th></tr></thead><tbody>' + detailItems + '</tbody></table>').removeClass('loading');
                        reloadStyleTable(time);
                    }
                    tabledetail.html('<table id="tabledetail' + time + '" class="table table-striped table-bordered table-hover" style="width:100%"><thead><tr><th >Estatus</th><th>Codigo</th><th>Descripcion</th><th>Cantidad</th><th>Costo Unitario</th><th>IVA</th><th>IEPS</th><th>SubTotal</th><th>Total</th></tr></thead><tbody>' + detailItems + '</tbody></table>').removeClass('loading');
                    reloadStyleTable(time);
                }
            }

            $(".btn-success").on("click", function () {
                Aprobar($(this).attr("folio"));
            });

            $(".btn-danger").on("click", function () {
                Cancelar($(this).attr("folio"));
            });
        }
    });
    return tabledetail;
}

//Recargar tabla
function reloadStyleTable(id) {
    $('#tabledetail' + id).DataTable({
        oLanguage: {
            "sProcessing": "Procesando...", "sLengthMenu": "Mostrar _MENU_ registros", "sZeroRecords": "No se encontraron resultados", "sEmptyTable": "Ningún dato disponible en esta tabla", "sInfo": "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros", "sInfoEmpty": "Mostrando registros del 0 al 0 de un total de 0 registros", "sInfoFiltered": "(filtrado de un total de _MAX_ registros)", "sInfoPostFix": "", "sSearch": "Buscar:", "sUrl": "", "sInfoThousands": ",", "sLoadingRecords": "Cargando...", "oPaginate": { "sFirst": "Primero", "sLast": "Último", "sNext": "Siguiente", "sPrevious": "Anterior" }, "oAria": { "sSortAscending": ": Activar para ordenar la columna de manera ascendente", "sSortDescending": ": Activar para ordenar la columna de manera descendente" }
        },
        columnDefs: [
            {
                targets: [0, 1],
                width: "1%",
                
            },{
                targets: [2],
                width: "1%"
            },
            {
                targets: [3],
                width: "1%",
                render: function (data, type, row) {

                    return `${new Intl.NumberFormat("en-US", { maximumFractionDigits: 2}).format(data)}`;
                }
            },
            
            {
                targets: [4,5, 6, 7, 8],
                width: "1%",
                render: function (data, type, row) {

                    return `${fomartmoney(data)(2)}`;
                }
            },
            ]
    });
}

function SearchTransfer() {
    var status;
    var transfertype = $("#SelectOptions").val();
    var product = $("#ItemSelect").val();
    var department = $("#depto").val();
    var family = $("#family").val();
    var originSite = $("#sites").val();
    if (department == 0)
        department = "";
    if (transfertype == "salida") {
        status = $("#statusOut").val();
    } else {
        status = $("#statusIn").val();
    }
    if ($('#ValidDate1').val() != "" & $('#ValidDate2').val() != "") {
        if (moment($('#ValidDate1').val()) <= moment($('#ValidDate2').val())) {
            StartLoading();
            if (detailTransfer) {
                $('#tableTransfersDetailDiv').show();
                $('#TableProduct').hide();
                $.ajax({
                    type: "POST",
                    url: "/TransferHeader/ActionGetTransferHeaderInDetail/",
                    data: { "date1": $('#ValidDate1').val(), "date2": $('#ValidDate2').val(), "TransferType": transfertype, "status": status, "originSite": originSite, "partNumber": product, "department": department, "family": family },
                    success: function (returndate) {
                        EndLoading();

                        if (returndate == "SF")
                            SessionFalse("Terminó tu sesión");
                        else if (returndate) {
                            if (returndate.length == 0) {
                                console.log("1");
                                toastr.warning('No se encontraron transferencias.');
                                tableDetail.clear();
                                tableDetail.columns.adjust().draw();
                                $('#tableTransfersDetailDiv').animatePanel();
                            }
                            else {
                                tableDetail.clear();
                                tableDetail.rows.add($(returndate));
                                tableDetail.columns.adjust().draw();
                                $('#tableTransfersDetailDiv').animatePanel();
                            }
                        }
                        console.log("3");
                    },
                    error: function (returndate) {
                        EndLoading();
                        toastr.error('Alerta - Error inesperado contactar a sistemas.');
                    }
                });
            } else {
                $('#tableTransfersDetailDiv').hide();
                $('#TableProduct').show();
                $.ajax({
                    type: "POST",
                    url: "/TransferHeader/ActionGetTransferHeaderInInputs/",
                    data: { "BeginDate": $('#ValidDate1').val(), "EndDate": $('#ValidDate2').val(), "TransferType": transfertype, "status": status, "originSite": originSite, "partNumber": product, "department": department, "family": family },
                    success: function (returndate) {
                        EndLoading();
                        if (returndate == "SF")
                            SessionFalse("Terminó tu sesión");
                        else if (returndate.Data) {
                            if (returndate.Data.length == 0) {
                                toastr.warning('No se encontraron transferencias.');
                                table.clear();
                                table.columns.adjust().draw();
                                $('#TableProduct').animatePanel();
                            }
                            else {
                                table.clear();
                                table.rows.add($(returndate.Data));
                                table.columns.adjust().draw();
                                $('#TableProduct').animatePanel();
                            }
                        }
                    },
                    error: function (returndate) {
                        EndLoading();
                        toastr.error('Alerta - Error inesperado contactar a sistemas.');
                    }
                });
            }
        }
        else {
            toastr.error("Fecha final no puede ser menor a la inicial.");
            document.getElementById('ValidDate1').value = '';
            document.getElementById('ValidDate2').value = '';
            $('#ValidDate1').focus();
        }
    } else {
        toastr.error("Seleccione un rango de fechas.");
    }
}

function AprovalPrint(document, TransferSiteCode, DestinationSiteCode) {
    StartLoading();
    axios.get("/TransferHeader/ActionPrintTransferReport?Document=" + document + "&TransferSiteCode=" + TransferSiteCode + "&DestinationSiteCode=" + DestinationSiteCode)
        .then(function (data) {
            EndLoading()
            if (data.data == "SF")
                SessionFalse("Se terminó su sesion.")
            else if (data.data.success) {
                $("#myModal8").modal('show')
                id = $("#folio").val();
                $("#iframes").html(`<iframe src='data:application/pdf;base64,${data.data.responseText}' height='520px' width='100%'></iframe>`)
            }
            else
                toastr.error('Alerta - Error inesperado  contactar a sistemas.7');
        }).catch(function (error) {
            EndLoading()
        })
}

function PrintReport(reference, TransferSiteCode, DestinationSiteCode) {
    StartLoading();
    $.ajax({
        url: "/TransferHeader/ActionPrintTransferReport",
        type: "POST",
        data: { "document": reference, "TransferSiteCode": TransferSiteCode, "DestinationSiteCode": DestinationSiteCode },
        success: function (response) {
            EndLoading();
            if (response.Data == "Termino tu sesión.")
                SessionFalse(response.Data);
            else if (response.success) {
                $("#modalReferenceBody").html(`<iframe src='data:application/pdf;base64,${response.responseText}' height='520px' width='100%'></iframe>`)
                $("#modalReference").modal('show');
            }
            else
                toastr.error(response.Data);
        },
        error: function () {
            EndLoading();
            toastr.error('Error inesperado contactar a sistemas.');            
        }
    });
}


$("#tableTransfersDetail").append(`<tfoot><th style="color:#7fbf7f"></th><th style="color:#66b266"></th><th id="mxsubtotalDetail" style="color:#4ca64c;"></th><th></th><th id="mxivaDetail" style="color:#329932;"></th><th id ="mxiepsDetail" style="color:#198c19;"></th><th></th><th id ="mxtotalDetail" style="color:#008000;"></th><th></th><th></th><th style="color:green;">Totales MXN</th><th style="color:#6d6dff;">USD</th><th id="usdsubtotalDetail" style="color:#5555ff;"></th><th id="usdivaDetail"  style="color:#3d3dff;"></th><th id ="usdiepsDetail"  style="color:#2525ff;"></th><th id ="usdtotalDetail"  style="color:#0D0DFF;"></th> </tfoot>`);

var tableDetail = $('#tableTransfersDetail').DataTable({
    oLanguage: {
        "sProcessing": "Procesando...", "sLengthMenu": "Mostrar _MENU_ registros", "sZeroRecords": "No se encontraron resultados", "sEmptyTable": "Ningún dato disponible en esta tabla", "sInfo": "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros", "sInfoEmpty": "Mostrando registros del 0 al 0 de un total de 0 registros", "sInfoFiltered": "(filtrado de un total de _MAX_ registros)", "sInfoPostFix": "", "sSearch": "Buscar:", "sUrl": "", "sInfoThousands": ",", "sLoadingRecords": "Cargando...", "oPaginate": { "sFirst": "Primero", "sLast": "Último", "sNext": "Siguiente", "sPrevious": "Anterior" }, "oAria": { "sSortAscending": ": Activar para ordenar la columna de manera ascendente", "sSortDescending": ": Activar para ordenar la columna de manera descendente" }
    },
    dom: "<'row'<'col-sm-4'l><'col-sm-4 text-left'B><'col-sm-4'f>t<'col-sm-6'i><'col-sm-6'p>>",
    lengthMenu: [[10, 25, 50, -1], [10, 25, 50, "Todos"]],
    buttons: [ ],
    "footerCallback": function (row, data, start, end, display) {
        var api = this.api(), data;
        // converting to interger to find total
        var intVal = function (i) {
            return typeof i === 'string' ?
                i.replace(/[\$,]/g, '') * 1 :
                typeof i === 'number' ?
                i : 0;
        };
        for (var i = 11; i < 15; i++) {
                var monTotal = api
                    .column(i)
                    .data()
                    .reduce(function (a, b) {
                        if (b > 0)
                            return intVal(a) + intVal(b);
                        else
                            return intVal(a);
                    }, 0);
                $(api.column(i).footer()).html(`<span ${monTotal == 0 ? "" : `style = "Color:${monTotal >= 0 ? "green" : "red"}"`} > ${fomartmoney(monTotal)(4)}</span >`);
        }
    },
    columns: [
        { data: 'Folio' },
        { data: 'PartNumber' },
        { data: 'DescriptionC' },
        { data: 'Department' },
        { data: 'Family' },
        { data: 'MerchandiseEntry' },
        { data: 'MerchandiseEntryEta', visible: false },
        { data: 'Supplier' },
        { data: 'Currency', visible: false },
        { data: 'Quantity' },
        { data: 'CostUnit' },
        { data: 'SubTotal' },
        { data: 'IVA' },
        { data: 'IEPS' },
        { data: 'Amount' },
        { data: 'Item_Status' }
    ],
    columnDefs: [{
        targets: [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15],
        width: "1%"
    },
    {
        render: function (data, type, full, meta) {
            if (data == null || data == "")
                return "Llego en " + full.MerchandiseEntryEta;
            else
                return data;
        },
        targets: [5],
    },
    {
        type: 'numeric-comma',
        render: function (data, type, row) {
            return formatmoney(data)(4);
        },
        targets: [10, 11, 12, 13, 14],
    }],
});

function SearchTransferReport() {
    var status;
    var urlConfig;
    var transfertype = $("#SelectOptions").val();
    var product = $("#ItemSelect").val();
    var department = $("#depto").val();
    var family = $("#family").val();
    var originSite = $("#sites").val();
    if (department == 0)
        department = "";
    if (transfertype == "salida") {
        status = $("#statusOut").val();
    } else {
        status = $("#statusIn").val();
    }

    if (detailTransfer) {
        urlConfig = '/TransferHeader/ActionGetReportPurchaseDetail';
    } else {
        urlConfig = '/TransferHeader/ActionGetReportTransfer';
    }

    if ($('#ValidDate1').val() != "" & $('#ValidDate2').val() != "") {
        if (moment($('#ValidDate1').val()) <= moment($('#ValidDate2').val())) {
            StartLoading();
            $.ajax({
                type: "POST",
                url: urlConfig,
                data: { "BeginDate": $('#ValidDate1').val(), "EndDate": $('#ValidDate2').val(), "TransferType": transfertype, "status": status, "originSite": originSite, "partNumber": product, "department": department, "family": family },
                success: function (report) {
                    EndLoading();
                    if (report == "SF")
                        SessionFalse("Terminó tu sesión");
                    else if (report) {
                        document.getElementById("iframe").srcdoc = report;
                        EndLoading();
                        $('#ModalReport').modal('show');
                    } else {
                        toastr.error('Alerta - Error inesperado contactar a sistemas.');
                    }
                },
                error: function (returndate) {
                    EndLoading();
                    toastr.error('Alerta - Error inesperado contactar a sistemas.');
                }
            });
        } else {
            toastr.error("Fecha final no puede ser menor a la inicial.");
            document.getElementById('ValidDate1').value = '';
            document.getElementById('ValidDate2').value = '';
            $('#ValidDate1').focus();
        }
    } else {
        toastr.error("Seleccione un rango de fechas.");
    }
}