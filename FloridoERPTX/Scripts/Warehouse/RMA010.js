﻿        function format(d) {
            var detailItems = "";
            var tabledetail = $("<div/>").addClass("loading").text("Cargando Datos...");
            $.ajax({
                url: "/DamagedsGoods/ActionGetAllItemSupplierByRma/",
                data: { "Folio": d.DamagedGoodsDoc, "Type": "RMA", "Status": d.ProcessStatus },
                type: "GET",
                success: function (data) {
                    if (data != "SF") {
                        detailItems = data.map(value => ` <tr>${FullTable(value)([
                            { title: "PartNumber", option: 0 },
                            { title: "Description", option: 0 },
                            { title: "Size", option: 0 },
                            { title: "Storage", option: 0 },
                            { title: "Quantity", option: 0 },
                            { title: "Price", option: 1 },
                            { title: "Amount_Doc", option: 1 },
                        ])}</tr>`).join().replace(/>,</g, '><').replace(/>, </g, '><')
                        header = FullTableHeader(["Codigo", "Nombre", "UM", "Locacion", "Cantidad", "Precio", "Monto" /*"Iva","Ieps" ,"Total"*/]).replace(/,/g, '')
                        var Html = (`<table id="tabledetail" class="table table-striped table-bordered table-hover" style="width:100%"><thead>
                       ${header}
                        </thead><tbody>${detailItems.length > 0 ? detailItems : "<td colspan='8' style='text-align: center;'>No cuenta con productos</td>"}</tbody></table>`)
                        tabledetail.html(Html).removeClass('loading');
                    }
                    else
                        SessionFalse("Termino tu session")
                    reloadStyleTable();
                }
            });
            return tabledetail;
        }
        function SerchRMA() {

            table.clear();
            table.rows.add([]);
            table.columns.adjust().draw();

            StartLoading();
            $("#TableDiv").animatePanel();
            $.ajax({
                type: "GET",
                url: "/DamagedsGoods/ActionGetRMAFullTable/",
                data: { "Type": "RMA", "DateInit": $("#ValidDate1").val(), "DateFinal": $("#ValidDate2").val(), "Status": $("#statusSelect").val() },
                success: function (result) {
                    if (result == "SF")
                        SessionFalse("Termino tu session")

                    if (result.length > 0) {
                        table.clear();
                        table.rows.add(result);
                        table.columns.adjust().draw();
                    }

                    else if (result.length == 0)
                        toastr.warning("No hay productos con esos filtros")

                    else
                        toastr.warning("Alerta - Error inesperado!!");
                },
                error: function (result) {
                    toastr.error("Alerta - Error inesperado contactar a sistemas.");
                }
            });
            EndLoading();

        }