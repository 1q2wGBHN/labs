﻿$(document).ready(function ()
{
    window.setTimeout(function () { $("#fa").click(); }, 1000);
    $("#driver").select2();

    var model = JSON.parse($("#ListStatus4").val());
    table.clear();
    table.rows.add(model);
    table.columns.adjust().draw();

    tableEntradas.clear();
    tableEntradas.rows.add(JSON.parse($("#ListStatus9").val()));
    tableEntradas.columns.adjust().draw();
});

var table = $('#Transfers').DataTable({
    oLanguage:
    {
        "sProcessing": "Procesando...",
        "sLengthMenu": "Mostrar _MENU_ registros", "sZeroRecords": "No se encontraron resultados", "sEmptyTable": "Ningún dato disponible en esta tabla",
        "sInfo": "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros", "sInfoEmpty": "Mostrando registros del 0 al 0 de un total de 0 registros",
        "sInfoFiltered": "(filtrado de un total de _MAX_ registros)", "sInfoPostFix": "", "sSearch": "Buscar:", "sUrl": "", "sInfoThousands": ",",
        "sLoadingRecords": "Cargando...", "oPaginate": { "sFirst": "Primero", "sLast": "Último", "sNext": "Siguiente", "sPrevious": "Anterior" },
        "oAria":
        {
            "sSortAscending": ": Activar para ordenar la columna de manera ascendente",
            "sSortDescending": ": Activar para ordenar la columna de manera descendente"
        }
    },
    responsive: true,
    autoWidth: true,
    order: [[1, "asc"]],
    dom: "<'row'<'col-sm-4'l><'col-sm-4 text-left'B><'col-sm-4'f>t<'col-sm-6'i><'col-sm-6'p>>",
    lengthMenu: [[10, 25, 50, -1], [10, 25, 50, "Todos"]],
    buttons: [
        { extend: "csv", text: "Excel", title: "CancelacionTransferencia", className: "btn-sm" },
        {
            extend: "pdf", text: "PDF", title: "CancelacionTransferencia", className: "btn-sm", exportOptions:
            {
                columns: [1, 2, 3, 4]
            }
        },
        { extend: "print", text: "Imprimir", className: "btn-sm" }
    ],
    columns:
        [
            {
                class: "details-control",
                orderable: false,
                data: null,
                defaultContent: "",
                width: "5%"
            },
            { data: "TransferDocument" },
            { data: "DestinationSiteCode" },
            { data: "DestinationSiteName" },
            { data: "TransferSiteCode" },
            { data: "TransferSiteName" },
            { data: "TransferDate" },
            { data: "TransferStatus" },
            { data: "Comment" },
            { data: null }
        ],

    columnDefs:
        [
            {
                visible: false,
                searchable: false,
                targets: [2, 4, 5]
            },
            {
                targets: [0, 9],
                width: "5%"
            },
            {
                targets: [1, 2, 4, 6, 7],
                width: "10%"
            },
            {
                targets: [3, 5],
                width: "20%"
            },
            {
                defaultContent: '<button class="btn btn-xs btn-outline btn-danger" id="cancel-button" type="submit">Cancelar</button>',
                targets: [9]
            },
            {
                targets: [7],
                render: function (data, type, row)
                {
                    var cadena = "Transito";
                    return '<div  text-align:center;"><strong>' + cadena + '</strong></div>';
                }
            }
        ]
});

var detailRows = [];
$("#Transfers tbody").on("click", "tr td.details-control", function ()
{
    var tr = $(this).closest("tr");
    var row = table.row(tr);
    if (row.child.isShown())
    {
        row.child.hide();
        tr.removeClass("details");
    }
    else
    {
        if (table.row(".details").length)
        {
            $(".details-control", table.row(".details").node()).click();
        }
        row.child(format(row.data())).show();
        tr.addClass("details");
    }
});

table.on("draw", function ()
{
    $.each(detailRows, function (i, id)
    {
        $("#" + id + " td.details-control").trigger("click");
    });
});

function format(d)
{
    var detailItems = "";
    var tabledetail = $("<div/>").addClass("loading").text("Cargando Datos...");
    $.ajax({
        url: "/TransferHeader/ActionGetAllTransferDetailNewV2",
        data: { "Document": d.TransferDocument, "TransferSiteCode": d.TransferSiteCode, "DestinationSiteCode": d.DestinationSiteCode },
        type: "GET",
        success: function (data)
        {
            if (data.data === "SF")
            {
                SessionFalse("Terminó su sesión");
            }
            if (data.length !== 0)
            {
                $.each(data.Data, function (index, value)
                {
                    detailItems += "<tr><td>" + value.DestinationSiteName + "</td><td>" + value.PartNumber + "</td><td>" + value.Quantity + "</td><td>" + value.Description + "</td></tr>";
                });
            }
            tabledetail.html('<table id="tabledetail" class="table table-striped table-bordered table-hover" style="width:100%"><thead><tr><th>Sitio Destino</th><th>Codigo</th><th>Cantidad</th><th>Nombre del producto</th></tr></thead><tbody>' + detailItems + '</tbody></table>').removeClass('loading');
            reloadStyleTable();
        }
    });
    return tabledetail;
}

function reloadStyleTable()
{
    $('#tabledetail').DataTable({
        oLanguage:
        {
            "sProcessing": "Procesando...",
            "sLengthMenu": "Mostrar _MENU_ registros",
            "sZeroRecords": "No se encontraron resultados",
            "sEmptyTable": "Ningún dato disponible en esta tabla",
            "sInfo": "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
            "sInfoEmpty": "Mostrando registros del 0 al 0 de un total de 0 registros",
            "sInfoFiltered": "(filtrado de un total de _MAX_ registros)", "sInfoPostFix": "",
            "sSearch": "Buscar:", "sUrl": "", "sInfoThousands": ",", "sLoadingRecords": "Cargando...",
            "oPaginate": { "sFirst": "Primero", "sLast": "Último", "sNext": "Siguiente", "sPrevious": "Anterior" },
            "oAria":
            {
                "sSortAscending": ": Activar para ordenar la columna de manera ascendente",
                "sSortDescending": ": Activar para ordenar la columna de manera descendente"
            }
        }
    });
}
var destinationSite = "";
var transferSite = "";
$('#Transfers tbody').on('click', '#cancel-button', function ()
{
    var values = table.row($(this).parents('tr')).data();
    values = $.map(values, function (value, index)
    {
        switch (index)
        {
            case 'TransferDocument':
                AprovalCancel(value);
                break;
            case 'DestinationSiteCode':
                destinationSite = value;
                break;
            case 'TransferSiteCode':
                transferSite = value;
                break;
        }
    });
});

function AprovalCancel(valor)
{
    //console.log(valor);
    swal({
        title: "Se Cancelara la Transferencia, ¿desea continuar?",
        text: "Transferencia # " + valor,
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "Continuar",
        cancelButtonText: "Cancelar"
    }, function (isConfirm)
    {
        if (isConfirm) 
        {
            ExecuteAproval(valor);
        }
    });
}

function ExecuteAproval(transferDoc)
{
    StartLoading();
    $.ajax({
        url: '/TransferHeader/ActionCancelTransferOriginSite',
        type: "POST",
        data:
        {
            "transferDoc": transferDoc,
            "destination_site": destinationSite,
            "transferSiteCode": transferSite
        },
        success: function (response)
        {
            if (response.success)
            {
                table.clear();
                table.rows.add(response.Json1);
                table.columns.adjust().draw();

                tableEntradas.clear();
                tableEntradas.rows.add(response.Json2);
                tableEntradas.columns.adjust().draw();

                toastr.success(response.responseText);
                EndLoading();
            }
            else
            {
                EndLoading();
                if (response.responseText === "SF")
                {
                    SessionFalse(response.responseText);
                }
                else
                {
                    toastr.warning(response.responseText);
                }
            }
        },
        error: function ()
        {
            EndLoading();
            toastr.error('Error inesperado contactar a sistemas.');
        }
    });
}

/////////////---------------------------------ENTRADAS---------------------------//////////////

var tableEntradas = $('#TransfersEntradas').DataTable({
    oLanguage: {
        "sProcessing": "Procesando...", "sLengthMenu": "Mostrar _MENU_ registros", "sZeroRecords": "No se encontraron resultados", "sEmptyTable": "Ningún dato disponible en esta tabla", "sInfo": "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros", "sInfoEmpty": "Mostrando registros del 0 al 0 de un total de 0 registros", "sInfoFiltered": "(filtrado de un total de _MAX_ registros)", "sInfoPostFix": "", "sSearch": "Buscar:", "sUrl": "", "sInfoThousands": ",", "sLoadingRecords": "Cargando...", "oPaginate": { "sFirst": "Primero", "sLast": "Último", "sNext": "Siguiente", "sPrevious": "Anterior" }, "oAria": { "sSortAscending": ": Activar para ordenar la columna de manera ascendente", "sSortDescending": ": Activar para ordenar la columna de manera descendente" }
    },
    responsive: true,
    autoWidth: true,
    order: [[1, "asc"]],
    dom: "<'row'<'col-sm-4'l><'col-sm-4 text-left'B><'col-sm-4'f>t<'col-sm-6'i><'col-sm-6'p>>",
    lengthMenu: [[10, 25, 50, -1], [10, 25, 50, "Todos"]],
    buttons: [
        { extend: "csv", text: "Excel", title: "CancelacionTransferencia", className: "btn-sm" },
        {
            extend: "pdf", text: "PDF", title: "CancelacionTransferencia", className: "btn-sm", exportOptions: {
                columns: [1, 2, 3, 4]
            },
        },
        { extend: "print", text: "Imprimir", className: "btn-sm" },
    ],
    columns:
        [
            {
                class: "details-control",
                orderable: false,
                data: null,
                defaultContent: "",
                width: "5%"
            },
            { data: "TransferDocument" },
            { data: "DestinationSiteCode" },
            { data: "DestinationSiteName" },
            { data: "TransferSiteCode" },
            { data: "TransferSiteName" },
            { data: "TransferDate" },
            { data: "TransferStatus" },
            { data: "Comment" },
            { data: null }
        ],

    columnDefs:
        [
            {
                visible: false,
                searchable: false,
                targets: [2, 4, 5]
            },
            {
                targets: [0, 9],
                width: "5%"
            },
            {
                targets: [1, 2, 4, 6, 7],
                width: "10%"
            },
            {
                targets: [3, 5],
                width: "20%"
            },
            {
                defaultContent: '<button class="btn btn-xs btn-outline btn-danger" id="cancel-button-entradas" type="submit">Cancelar</button>',
                targets: [9]
            },
            {
                targets: [7],
                render: function (data, type, row)
                {
                    var cadena = "Terminada";
                    return '<div  text-align:center;"><strong>' + cadena + '</strong></div>';
                }
            }
        ]
});

var detailRows2 = [];
$("#TransfersEntradas tbody").on("click", "tr td.details-control", function ()
{
    var tr = $(this).closest("tr");
    var row = tableEntradas.row(tr);
    if (row.child.isShown())
    {
        row.child.hide();
        tr.removeClass("details");
    }
    else
    {
        if (tableEntradas.row(".details").length)
        {
            $(".details-control", tableEntradas.row(".details").node()).click();
        }
        row.child(formatEntradas(row.data())).show();
        tr.addClass("details");
    }
});

tableEntradas.on("draw", function ()
{
    $.each(detailRows2, function (i, id)
    {
        $("#" + id + " td.details-control").trigger("click");
    });
});

function formatEntradas(d)
{
    var detailItems = "";
    var tabledetail = $("<div/>").addClass("loading").text("Cargando Datos...");
    $.ajax({
        url: "/TransferHeader/ActionGetAllTransferDeatilsExist",
        data: { "Document": d.TransferDocument, "TransferSiteCode": d.TransferSiteCode },
        type: "GET",
        success: function (data)
        {
            if (data.data === "SF")
            {
                SessionFalse("Terminó su sesión");
            }
            if (data.length !== 0)
            {
                $.each(data.Data, function (index, value)
                {
                    detailItems += "<tr><td>" + d.TransferSiteName + "</td><td>" + value.DestinationSiteName + "</td><td>" + value.PartNumber + "</td><td>" + value.Quantity + "</td><td>" + value.Description + "</td></tr>";
                });
            }
            tabledetail.html('<table id="tabledetailEntradas" class="table table-striped table-bordered table-hover" style="width:100%"><thead><tr><th>Sitio Envio</th><th>Sitio Destino</th><th>Codigo</th><th>Cantidad</th><th>Nombre del producto</th></tr></thead><tbody>' + detailItems + '</tbody></table>').removeClass('loading');
            reloadStyleTableEntradas();
        }
    });
    return tabledetail;
}

function reloadStyleTableEntradas()
{
    $('#tabledetailEntradas').DataTable({
        oLanguage:
        {
            "sProcessing": "Procesando...",
            "sLengthMenu": "Mostrar _MENU_ registros",
            "sZeroRecords": "No se encontraron resultados",
            "sEmptyTable": "Ningún dato disponible en esta tabla",
            "sInfo": "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
            "sInfoEmpty": "Mostrando registros del 0 al 0 de un total de 0 registros",
            "sInfoFiltered": "(filtrado de un total de _MAX_ registros)", "sInfoPostFix": "",
            "sSearch": "Buscar:", "sUrl": "", "sInfoThousands": ",", "sLoadingRecords": "Cargando...",
            "oPaginate": { "sFirst": "Primero", "sLast": "Último", "sNext": "Siguiente", "sPrevious": "Anterior" },
            "oAria":
            {
                "sSortAscending": ": Activar para ordenar la columna de manera ascendente",
                "sSortDescending": ": Activar para ordenar la columna de manera descendente"
            }
        }
    });
}

$('#TransfersEntradas tbody').on('click', '#cancel-button-entradas', function ()
{
    var values = tableEntradas.row($(this).parents('tr')).data();
    values = $.map(values, function (value, index)
    {
        switch (index)
        {
            case 'TransferDocument':
                AprovalCancelEntrada(value);
                break;
            case 'DestinationSiteCode':
                destinationSite = value;
                break;
            case 'TransferSiteCode':
                transferSite = value;
                break;
        }
    });
});

function AprovalCancelEntrada(valor)
{
    swal({
        title: "Se Cancelara la Transferencia, ¿desea continuar?",
        text: "Transferencia # " + valor,
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "Continuar",
        cancelButtonText: "Cancelar"
    }, function (isConfirm) {
        if (isConfirm) 
        {
            ExecuteAprovalEntrada(valor);
        }
    });
}

function ExecuteAprovalEntrada(transferDoc)
{
    if ($("#driver").val() === "")
    {
        toastr.warning("Selecciona un Chofer");
        $("#driver").select2('open');
        return false;
    }
    StartLoading();
    $.ajax({
        url: '/TransferHeader/ActionCancelTransferDestinationSite',
        type: "POST",
        data:
        {
            "transferDoc": transferDoc,
            "destination_site": destinationSite,
            "transferSiteCode": transferSite,
            "driver": $("#driver").val()
        },
        success: function (response)
        {
            if (response.success)
            {
                table.clear();
                table.rows.add(response.Json1);
                table.columns.adjust().draw();

                tableEntradas.clear();
                tableEntradas.rows.add(response.Json2);
                tableEntradas.columns.adjust().draw();

                toastr.success(response.responseText);

                EndLoading();
            }
            else
            {
                EndLoading();
                if (response.responseText === "SF")
                {
                    SessionFalse(response.responseText);
                }
                else
                {
                    toastr.warning(response.responseText);
                }
            }
        },
        error: function ()
        {
            EndLoading();
            toastr.error('Error inesperado contactar a sistemas.');
        }
    });
}