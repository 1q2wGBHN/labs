﻿//DatePicker y validacion solo el mes
var date = new Date();
var primerDia = new Date(date.getFullYear(), date.getMonth(), 1);
var ultimoDia = new Date(date.getFullYear(), date.getMonth() + 1, 0);
var mes = (primerDia.getMonth() + 1).toString();
var mesAnterior = (primerDia.getMonth()).toString();
var ano = primerDia.getFullYear().toString();
var diaFin = ultimoDia.getDate().toString();
var fechaInicio = mesAnterior + "/1/" + ano;
var fechaFin = mes + "/" + diaFin + "/" + ano;

$("#BeginDate").datepicker({
    autoclose: true,
    todayHighlight: true,
    showButtonPanel: true,
    startDate: fechaInicio,
    endDate: fechaFin,
}).on("changeDate", function (e) {
    SearchTransfer();
});

$("#EndDate").datepicker({
    autoclose: true,
    startDate: fechaInicio,
    endDate: fechaFin,
    todayHighlight: true,
}).on("changeDate", function (e) {
    SearchTransfer();
});

var table = $('#Transfers').DataTable({
    oLanguage: {
        "sProcessing": "Procesando...", "sLengthMenu": "Mostrar _MENU_ registros", "sZeroRecords": "No se encontraron resultados", "sEmptyTable": "Ningún dato disponible en esta tabla", "sInfo": "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros", "sInfoEmpty": "Mostrando registros del 0 al 0 de un total de 0 registros", "sInfoFiltered": "(filtrado de un total de _MAX_ registros)", "sInfoPostFix": "", "sSearch": "Buscar:", "sUrl": "", "sInfoThousands": ",", "sLoadingRecords": "Cargando...", "oPaginate": { "sFirst": "Primero", "sLast": "Último", "sNext": "Siguiente", "sPrevious": "Anterior" }, "oAria": { "sSortAscending": ": Activar para ordenar la columna de manera ascendente", "sSortDescending": ": Activar para ordenar la columna de manera descendente" }
    },
    responsive: true,
    autoWidth: true,
    order: [[1, "asc"]],
    dom: "<'row'<'col-sm-4'l><'col-sm-4 text-left'B><'col-sm-4'f>t<'col-sm-6'i><'col-sm-6'p>>",
    lengthMenu: [[10, 25, 50, -1], [10, 25, 50, "Todos"]],
    buttons: [
        { extend: "csv", text: "Excel", title: "CancelacionTransferencia", className: "btn-sm" },
        {
            extend: "pdf", text: "PDF", title: "CancelacionTransferencia", className: "btn-sm", exportOptions: {
                columns: [1, 2, 3, 4, 5, 6]
            },
        },
        { extend: "print", text: "Imprimir", className: "btn-sm" },
    ],
    columns:
        [{
            class: "details-control",
            orderable: false,
            data: null,
            defaultContent: "",
            width: "1%"
        },
        { data: "TransferDocument", width: "1%" },
        { data: "TransferSiteName", width: "1%" },
        { data: "DestinationSiteName", width: "1%" },
        { data: "Status", width: "1%" },
        { data: "TransferDate", width: "1%" },
        { data: "Comment", width: "1%" },
        { data: "TransferSiteCode", width: "1%", visible: false }],
    columnDefs:
        [{
            targets: [0, 1, 2, 3, 4, 5],
            width: "1%"
        }]
});

var detailRows = [];
$("#Transfers tbody").on("click", "tr td.details-control", function () {
    var tr = $(this).closest("tr");
    var row = table.row(tr);

    if (row.child.isShown()) {
        row.child.hide();
        tr.removeClass("details");
    }
    else {
        if (table.row(".details").length) {
            $(".details-control", table.row(".details").node()).click();
        }
        row.child(format(row.data())).show();
        tr.addClass("details");
    }
});

table.on("draw", function () {
    $.each(detailRows, function (i, id) {
        $("#" + id + " td.details-control").trigger("click");
    });
});
var destination = "";
function format(d) {
    var detailItems = "";
    var tabledetail = $("<div/>").addClass("loading").text("Cargando Datos...");
    destination = d.DestinationSiteName;
    console.log(destination);
    $.ajax({
        url: "/TransferHeader/ActionGetAllTransferDetailNew",
        data: { "Document": d.TransferDocument, "TransferSiteCode": d.TransferSiteCode, "DestinationSite": destination },
        type: "GET",
        success: function (response) {
            if (response.Data == "SF")
                SessionFalse("Terminó su sesión");
            $.each(response.Data, function (index, value) {
                detailItems += "<tr><td>" + value.PartNumber + "</td><td>" + value.Description + "</td><td class='text-center'>" + value.Quantity + "</td><td class='text-center'>$" + value.UnitCost + "</td><td class='text-center'>$" + value.IEPS + "</td><td class='text-center'>" + "$" + value.Iva + "</td><td class='text-center'>" + "$" + value.SubTotal + "</td><td class='text-center'>" + "$" + value.Amount + "</td>";
            });
            var Apro = "";
            if (response.Data.length != 0) {
                Apro = '<div class="form-group col-md-12"> <div class="row"> <label class="col-md-12 control-label">Motivo de Cancelación</label> </div> <div class="row"> <div class="col-md-10"> <textarea style="max-height: 150px;max-width:500px;" id="Reason" class="form-control" maxlength="249"></textarea> </div> <div class="col-md-2 text-right"> <button id="btn-cancel" type="button" class="btn btn-sm btn-danger" onclick=Cancelar("' + d.TransferDocument + '","' + d.TransferSiteCode + '");><i class="fa fa-close"></i>Solicitar Cancelacion</button>&nbsp;&nbsp;&nbsp;</div></div></div>';
                tabledetail.html('<table id="tabledetail" class="table table-striped table-bordered table-hover" style="width:100%"><thead><tr><th>Codigo</th><th>Nombre del producto</th><th class="text-center">Cantidad</th><th class="text-center">Precio unitario</th><th class="text-center">IEPS</th><th class="text-center">IVA</th><th class="text-center">Subtotal</th><th class="text-center">Total</th></tr></thead><tbody>' + detailItems + '</tbody></table>' + Apro).removeClass('loading');
                reloadStyleTable();
            }
            tabledetail.html('<table id="tabledetail" class="table table-striped table-bordered table-hover" style="width:100%"><thead><tr><th>Codigo</th><th>Nombre del producto</th><th class="text-center">Cantidad</th><th class="text-center">Precio unitario</th><th class="text-center">IEPS</th><th class="text-center">IVA</th><th class="text-center">Subtotal</th><th class="text-center">Total</th></tr></thead><tbody>' + detailItems + '</tbody></table>' + Apro).removeClass('loading');
            reloadStyleTable();
        }
    });
    return tabledetail
}

function SearchTransfer() {
    var beginDate = document.getElementById("BeginDate").value;
    var endDate = document.getElementById("EndDate").value;

    if (beginDate != "" & endDate != "") {
        if (moment(beginDate) <= moment(endDate)) {
            $.ajax({
                type: "GET",
                url: "/TransferHeader/ActionGetTransfersInTransitByDates",
                data: { "BeginDate": beginDate, "EndDate": endDate },
                success: function (response) {
                    if (response.Data == "SF")
                        SessionFalse("Terminó su sesión.");
                    else if (response) {
                        table.clear();
                        table.rows.add(response.Data);
                        table.columns.adjust().draw();
                        $("#TableDiv").animatePanel();
                        if (response.Data.length == 0)
                            toastr.warning("No se encontraron transferencias en tránsito.");
                    }
                },
                error: function () {
                    toastr.error("Alerta - Error inesperado contactar a sistemas.");
                }
            });
        }
        else {
            toastr.error("Fecha final no puede ser menor a la inicial.");
            document.getElementById("BeginDate").value = "";
            document.getElementById("EndDate").value = "";
            document.getElementById("BeginDate").focus();
        }
    }
}

function GetTransfer() {
    document.getElementById("BeginDate").value = "";
    document.getElementById("EndDate").value = "";
    $.ajax({
        type: "GET",
        url: "/TransferHeader/ActionGetTransfersInTransit",
        success: function (response) {
            if (response.Data == "SF")
                SessionFalse("Terminó su sesión.");
            else {
                table.clear();
                table.rows.add(response.Data);
                table.columns.adjust().draw();
                $("#TableDiv").animatePanel();
                if (response.Data.length == 0)
                    toastr.warning("No se encontraron transferencias en tránsito.");
            }
        },
        error: function () {
            toastr.error("Alerta - Error inesperado contactar a sistemas.");
        }
    });
}

function reloadStyleTable() {
    $("#tabledetail").DataTable({
        oLanguage: {
            "sProcessing": "Procesando...", "sLengthMenu": "Mostrar _MENU_ registros", "sZeroRecords": "No se encontraron resultados", "sEmptyTable": "Ningún dato disponible en esta tabla", "sInfo": "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros", "sInfoEmpty": "Mostrando registros del 0 al 0 de un total de 0 registros", "sInfoFiltered": "(filtrado de un total de _MAX_ registros)", "sInfoPostFix": "", "sSearch": "Buscar:", "sUrl": "", "sInfoThousands": ",", "sLoadingRecords": "Cargando...", "oPaginate": { "sFirst": "Primero", "sLast": "Último", "sNext": "Siguiente", "sPrevious": "Anterior" }, "oAria": { "sSortAscending": ": Activar para ordenar la columna de manera ascendente", "sSortDescending": ": Activar para ordenar la columna de manera descendente" }
        },
        columnDefs: [{
            targets: [2],
            width: "5%"
        }]
    });
}

function Cancelar(TransferDocument, TransferSiteCode) {
    var reason = $("#Reason").val();
    if (reason.trim().length >= 8) {
        swal({
            title: "¿Esta seguro de solicitar la cancelación?",
            text: "Distrital será notificado que desea cancelar la transferencia no. " + TransferDocument,
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Continuar",
            cancelButtonText: "Cancelar"
        }, function (isConfirm) {
            if (isConfirm)
                AprovalCancel(TransferDocument, TransferSiteCode, reason);
        });
    } else {
        EndLoading();
        setTimeout(function () {
            swal("Debe de ingresar al menos un comentario de 8 caracteres.",
                "Ingrese un comentario  y vuelvalo a intentar.",
                "error");
        }, 400);
    }
}

function AprovalCancel(TransferDocument, TransferSiteCode, Reason) {
    var reason = $("#Reason").val();
    if (reason.trim().length >= 8) {
        if (TransferDocument) {
            StartLoading();
            $.ajax({
                type: "POST",
                url: "/TransferHeader/ActionCancelRequestTransferHeader",
                data: { "TransferDocument": TransferDocument, "TransferSiteCode": TransferSiteCode, "Reason": Reason, "DestinationName": destination },
                success: function (response) {
                    EndLoading();
                    if (response.Data == "SF")
                        SessionFalse("Terminó su sesión.");
                    else if (response.Data) {
                        $("html, body").animate({ scrollTop: 0 }, "slow");
                        GetTransfer();
                        swal({
                            title: "Solicitud Enviada",
                            text: "Se notificó a Distrital la solicitud de cancelación.",
                            type: "success",
                            confirmButtonColor: "#55dd6b",
                            confirmButtonText: "Continuar"
                        });
                    }
                    else
                        swal({
                            title: "Error",
                            text: "Ha ocurrido un error.",
                            type: "warning",
                            confirmButtonColor: "#DD6B55",
                            confirmButtonText: "Continuar",
                        });
                },
                error: function () {
                    EndLoading();
                    toastr.error("Alerta - Error inesperado contactar a sistemas.");
                }
            });
        }
        else
            toastr.warning("El folio no puede ser nulo");
    } else {
        setTimeout(function () {
            swal("Debe de ingresar al menos un comenario de 8 caracteres.",
                "Ingrese un comentario  y vuelvalo a intentar.",
                "error");
        }, 400);
    }
}
 
function errors(value) {
    if (value == 6001)
        return "Site Code Incorrecto";
    else if (value == 6002)
        return "Estatus No Aprobado";
    else
        return "Desconocido";
}

$(document).ready(function () {
    window.setTimeout(function () { $("#fa").click(); }, 1000);
    var model = JSON.parse($("#model").val());
    table.clear();
    table.rows.add(model);
    table.columns.adjust().draw();
});