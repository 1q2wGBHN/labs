﻿window.setTimeout(function () { $("#fa").click(); }, 1000);

var table = $('#TableRMA').DataTable({
    oLanguage: {
        "sProcessing": "Procesando...", "sLengthMenu": "Mostrar _MENU_ registros", "sZeroRecords": "No se encontraron resultados", "sEmptyTable": "Ningún dato disponible en esta tabla", "sInfo": "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros", "sInfoEmpty": "Mostrando registros del 0 al 0 de un total de 0 registros", "sInfoFiltered": "(filtrado de un total de _MAX_ registros)", "sInfoPostFix": "", "sSearch": "Buscar:", "sUrl": "", "sInfoThousands": ",", "sLoadingRecords": "Cargando...", "oPaginate": { "sFirst": "Primero", "sLast": "Último", "sNext": "Siguiente", "sPrevious": "Anterior" }, "oAria": { "sSortAscending": ": Activar para ordenar la columna de manera ascendente", "sSortDescending": ": Activar para ordenar la columna de manera descendente" }
    },
    dom: "<'row'<'col-sm-4'l><'col-sm-4 text-left'B><'col-sm-4'f>t<'col-sm-6'i><'col-sm-6'p>>",
    lengthMenu: [[10, 25, 50, -1], [10, 25, 50, "Todos"]],
    buttons: [
        { extend: 'csv', text: 'Excel', title: 'CancelacionDevolucion', className: 'btn-sm' },
        {
            extend: 'pdf', text: 'PDF', title: 'CancelacionDevolucion', className: 'btn-sm', exportOptions: {
                columns: [1, 2, 3]
            },
            customize: function (doc) {
                doc.content[1].table.widths =
                    Array(doc.content[1].table.body[0].length + 1).join('*').split('');
            }
        },
        { extend: 'print', text: 'Imprimir', className: 'btn-sm' },
    ],
    columns: [
        {
            "class": "details-control",
            "orderable": false,
            "data": null,
            "defaultContent": "",
        },
        { data: 'DamagedGoodsDoc' },
        { data: 'Supplier' },
        { data: 'Comment' },
        { data: 'ProcessStatusDescription' }
    ],
    columnDefs: [{
        targets: [0, 1, 2, 3, 4],
        width: "1%"
    }]
});

var detailRows = [];

$('#TableRMA tbody').on('click', 'tr td.details-control', function () {
    var tr = $(this).closest('tr');
    var row = table.row(tr);

    if (row.child.isShown()) {
        row.child.hide();
        tr.removeClass('details');
    }
    else {
        if (table.row('.details').length) {
            $('.details-control', table.row('.details').node()).click();
        }
        row.child(format(row.data())).show();
        tr.addClass('details');
    }
});

table.on('draw', function () {
    $.each(detailRows, function (i, id) {
        $('#' + id + ' td.details-control').trigger('click');
    });
});

function reloadStyleTable() {
    $('#tabledetail').DataTable({
        oLanguage: {
            "sProcessing": "Procesando...", "sLengthMenu": "Mostrar _MENU_ registros", "sZeroRecords": "No se encontraron resultados", "sEmptyTable": "Ningún dato disponible en esta tabla", "sInfo": "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros", "sInfoEmpty": "Mostrando registros del 0 al 0 de un total de 0 registros", "sInfoFiltered": "(filtrado de un total de _MAX_ registros)", "sInfoPostFix": "", "sSearch": "Buscar:", "sUrl": "", "sInfoThousands": ",", "sLoadingRecords": "Cargando...", "oPaginate": { "sFirst": "Primero", "sLast": "Último", "sNext": "Siguiente", "sPrevious": "Anterior" }, "oAria": { "sSortAscending": ": Activar para ordenar la columna de manera ascendente", "sSortDescending": ": Activar para ordenar la columna de manera descendente" }
        }
    });
}

function format(d) {
    var detailItems = "";
    var tabledetail = $('<div/>').addClass('loading').text('Cargando Datos...');
    $.ajax({
        url: "/DamagedsGoods/ActionGetItemSupplierByRma/",
        data: { "Folio": d.DamagedGoodsDoc, "Type": "RMA", "Status": 9 },
        type: "GET",
        success: function (data) {
            $.each(data, function (index, value) {
                total = value.Iva + value.Ieps + value.Amount_Doc;
                detailItems += "<tr><td>" + value.PartNumber + "</td><td>" + value.Description + "</td><td>" + value.Size + "</td><td class='text-right'>" + value.Storage + "</td><td class='text-right'>" + value.Quantity + "</td><td class='text-right'>" + "$" + value.Price.toFixed(4) + "</td><td class='text-right'>" + "$" + value.Amount_Doc.toFixed(4) + "</td><td class='text-right'>" + "$" + value.Ieps + "</td><td class='text-right'>" + "$" + value.Iva + "</td><td class='text-right'>" + "$" + total.toFixed(4) + "</td></tr>"
            });
            var Apro = "";
            if (d.ProcessStatusDescription == "Terminada")
                Apro = '<div class="form-group col-md-12"> <div class="row"> <label class="col-md-12 control-label">Comentario</label> </div> <div class="row"> <div class="col-md-10"> <textarea id="comment" style="max-width:500px;max-height:150px;min-width:100px;min-height:100px;width:200px" class="form-control" maxlength="249"></textarea> </div> <div class="col-md-2 text-right"> <button id="btn-save" type="button" class="btn btn-sm btn-success" DamagedGoodsDoc="' + d.DamagedGoodsDoc + '" SupplierId="' + d.SupplierId + '"><i class="fa fa-check"></i> Cancelar</button></div></div></div>';
            tabledetail.html('<table id="tabledetail" class="table table-striped table-bordered table-hover" style="width:100%"><thead><tr><th>Codigo</th><th>Nombre</th><th>UM</th><th>Locacion</th><th>Cantidad</th><th>Precio</th><th>Monto</th><th>IEPS</th><th>IVA</th><th>Total</th></tr></thead><tbody>' + detailItems + '</tbody></table>' + Apro).removeClass('loading');
            reloadStyleTable();
        }
    });
    return tabledetail
}

function SerchRMA() {
    if ($('#BeginDate').val() != "" & $('#EndDate').val() != "") {
        if (moment($('#BeginDate').val()) <= moment($('#EndDate').val())) {
            var date = new Date();
            var Restrictedbegindate = new Date($('#BeginDate').val() );
            if (date.getMonth() != Restrictedbegindate.getMonth() || date.getFullYear() != Restrictedbegindate.getFullYear()) {
                $('#BeginDate').val('');
                $('#EndDate').val('');
                toastr.error("Solo es permitido fechas dentro del mismo mes.");
                return false;
            }
            $.ajax({
                type: "GET",
                url: "/DamagedsGoods/ActionGetFinishedRMA/",
                data: { "Type": "RMA", "BeginDate": $('#BeginDate').val(), "EndDate": $('#EndDate').val(), "Status": 9 },
                success: function (result) {
                    if (result.success) {
                        $('#TableDiv').animatePanel();
                        table.clear();
                        table.rows.add($(result.Json));
                        table.columns.adjust().draw();
                    }
                    else
                        toastr.warning('Alerta - Error inesperado.');
                },
                error: function (result) {
                    toastr.error('Alerta - Error inesperado contactar a sistemas.');
                }
            });
        }
        else
            toastr.warning('La fecha inicial no puede ser mayor a la fecha final');
    }
}

function Update() {
    $.ajax({
        type: "GET",
        url: "/DamagedsGoods/ActionGetInventoryAdjustmentRequests/",
        success: function (result) {
            if (!$.isEmptyObject(result.Json)) {
                $('#TableDiv').animatePanel();
                table.clear();
                table.rows.add($(result.Json));
                table.columns.adjust().draw();
            }
            else {
                table.clear();
                table.columns.adjust().draw();
            }
        },
        error: function (returndate) {
            toastr.error('Alerta - Error inesperado contactar a sistemas.');
        }
    });
}

function Request(damagedgoodsdoc, supplierid) {
    var comment = $('#comment').val();
    if ($.trim(comment).length >= 8) {
        StartLoading();
        $.ajax({
            type: "GET",
            url: "/DamagedsGoods/ActionRmaCancelRequest/",
            data: { "Folio": damagedgoodsdoc, "Comment": comment, "SupplierId": supplierid },
            success: function (result) {
                EndLoading();
                if (result) {
                    Update();
                    swal({
                        title: "Solicitado",
                        text: "Cancelación de Devolución solicitado satisfactoriamente!",
                        type: "success",
                        confirmButtonText: "Continuar"
                    });
                }
                else
                    toastr.error('Alerta - Error inesperado.');
            },
            error: function (returndate) {
                EndLoading();
                toastr.error('Alerta - Error inesperado contactar a sistemas.');
            }
        });
    } else {
        setTimeout(function () {
            swal(
                'Debe de ingresar al menos un comenario de 8 caracteres.',
                'Ingrese un comentario  y vuelvalo a intentar.',
                'error'
            )
        }, 400);
    }
}

function ConfirmMessage(damagedgoodsdoc, supplierid) {
    swal({
        title: "¿Esta seguro que desea solicitar la cancelación",
        type: "info",
        showCancelButton: true,
        confirmButtonColor: "#55dd6b",
        cancelButtonText: "Cancelar",
        confirmButtonText: "Continuar"
    }, function (isConfirm) {
        if (isConfirm)
            Request(damagedgoodsdoc, supplierid);
    });
}

$('#TableRMA tbody').on('click', '.btn-success', function () {
    var damagedgoodsdoc = $(this).attr("DamagedGoodsDoc");
    var supplierid = $(this).attr("supplierid");
    ConfirmMessage(damagedgoodsdoc, supplierid);
});

$('#TableDiv').animatePanel();
var model = JSON.parse($("#model").val());
table.clear();
table.rows.add($(model));
table.columns.adjust().draw();

var today = new Date();
var startDate = new Date(today.getFullYear(), today.getMonth(), 1);
var endDate = new Date(today.getFullYear(), today.getMonth() + 1, 0);

$("#BeginDate").datepicker({
    autoclose: true,
    todayHighlight: true,
    startDate: startDate,
    endDate: endDate
}).on("changeDate", function () {
    SerchRMA();
});

$("#EndDate").datepicker({
    autoclose: true,
    todayHighlight: true,
    startDate: startDate,
    endDate: endDate
}).on("changeDate", function () {
    SerchRMA();
});