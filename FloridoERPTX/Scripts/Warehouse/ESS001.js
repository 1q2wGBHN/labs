﻿$(document).ready(function () {
    window.setTimeout(function () { $("#fa").click(); }, 1000);
    var model = JSON.parse($("#model").val());
    table.clear();
    table.rows.add($(model));
    table.columns.adjust().draw();
});

$("#ValidDate1").datepicker({
    autoclose: true,
    todayHighlight: true,
    showButtonPanel: true,
}).on("changeDate", function (e) {
    SerchESS();
});

$("#ValidDate2").datepicker({
    autoclose: true,
    startDate: '01/01/1950',
    todayHighlight: true,
}).on("changeDate", function (e) {
    SerchESS();
});

var table = $('#TablePurchases').DataTable({
    oLanguage: {
        "sProcessing": "Procesando...", "sLengthMenu": "Mostrar _MENU_ registros", "sZeroRecords": "No se encontraron resultados", "sEmptyTable": "Ningún dato disponible en esta tabla", "sInfo": "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros", "sInfoEmpty": "Mostrando registros del 0 al 0 de un total de 0 registros", "sInfoFiltered": "(filtrado de un total de _MAX_ registros)", "sInfoPostFix": "", "sSearch": "Buscar:", "sUrl": "", "sInfoThousands": ",", "sLoadingRecords": "Cargando...", "oPaginate": { "sFirst": "Primero", "sLast": "Último", "sNext": "Siguiente", "sPrevious": "Anterior" }, "oAria": { "sSortAscending": ": Activar para ordenar la columna de manera ascendente", "sSortDescending": ": Activar para ordenar la columna de manera descendente" }
    },
    dom: "<'row'<'col-sm-4'l><'col-sm-4 text-left'B><'col-sm-4'f>t<'col-sm-6'i><'col-sm-6'p>>",
    lengthMenu: [[10, 25, 50, -1], [10, 25, 50, "Todos"]],
    buttons: [],
    columns: [
        {
            "class": "details-control",
            "orderable": false,
            "data": null,
            "defaultContent": "",
        },
        { data: 'id_empty_space' },
        { data: 'cuser' },
        { data: 'cdate' },
        { data: 'incidence' }
    ],
    columnDefs: [{
        targets: [0, 2, 3, 4],
        width: "1%"
    }]
});
var detailRows = [];

//Ocultar el Id
table.column(1).visible(false);

$('#TablePurchases tbody').on('click', 'tr td.details-control', function () {
    var tr = $(this).closest('tr');
    var row = table.row(tr);

    if (row.child.isShown()) {
        row.child.hide();
        tr.removeClass('details');
    }
    else {
        if (table.row('.details').length) {
            $('.details-control', table.row('.details').node()).click();
        }
        row.child(format(row.data())).show();
        tr.addClass('details');
    }
});

table.on('draw', function () {
    $.each(detailRows, function (i, id) {
        $('#' + id + ' td.details-control').trigger('click');
    });
});

function format(d) {
    var detailItems = "";
    var tabledetail = $('<div/>').addClass('loading').text('Cargando Datos...');
    $.ajax({
        url: "/EmptySpaceScan/ActionGetDetailsById",
        data: { "id_empty_space": d.id_empty_space },
        type: "GET",
        success: function (Data) {
            $.each(Data, function (index, value) {
                detailItems += "<tr><td>" + value.part_number + "</td><td>" + value.description + "</td><td>" + value.planogram + "</td><td class='text-center'>" + value.stock_existing + "</td><td class='text-center'>" + (value.last_entry != null ? moment(value.last_entry).format("DD/MM/YYYY") : "N/A") + "</td><td class='text-center'>" + (value.average_sale == 0 ? "N/A" : "%" + value.average_sale) + "</td><td class='text-center'>" + value.incidence + "</td>"
            });
            var htmlPrint = '<div class="col-md-2 text-left"> <button id="btn-cancel" type="button" class="btn btn-sm btn-success" onclick="print(' + d.id_empty_space + ');"><i class="fa fa-print"></i> Imprimir</button></div>';
            tabledetail.html('<table id="tabledetail" class="table table-striped table-bordered table-hover" style="width:100%"><thead><tr><th>Código</th><th>Descripción</th><th>Planograma</th><th>Stock Actual</th><th>Última Entrada</th><th>Promedio Venta</th><th>Incidencias</th></tr></thead><tbody>' + detailItems + '</tbody></table>' + htmlPrint).removeClass('loading');
            reloadStyleTable();
        }
    });
    return tabledetail
}

function SerchESS() {
    if ($('#ValidDate1').val() != "" & $('#ValidDate2').val() != "") {
        if (moment($('#ValidDate1').val()) <= moment($('#ValidDate2').val())) {
            $.ajax({
                type: "GET",
                url: "/EmptySpaceScan/ActionGetESS",
                data: { "DateIni": $('#ValidDate1').val(), "DateFin": $('#ValidDate2').val() },
                success: function (returndate) {
                    if (returndate.success) {
                        table.clear();
                        table.rows.add($(returndate.Json));
                        table.columns.adjust().draw();
                        $('#TableDiv').animatePanel();
                    }
                    else
                        toastr.warning('Alerta - Error inesperado.');
                },
                error: function (returndate) {
                    toastr.error('Alerta - Error inesperado contactar a sistemas.');
                }
            });
        }
        else {
            toastr.error("Fecha final no puede ser menor a la inicial.");
            document.getElementById('ValidDate1').value = '';
            document.getElementById('ValidDate2').value = '';
            $('#ValidDate1').focus();
        }
    }
}

function reloadStyleTable() {
    $('#tabledetail').DataTable({
        oLanguage: {
            "sProcessing": "Procesando...", "sLengthMenu": "Mostrar _MENU_ registros", "sZeroRecords": "No se encontraron resultados", "sEmptyTable": "Ningún dato disponible en esta tabla", "sInfo": "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros", "sInfoEmpty": "Mostrando registros del 0 al 0 de un total de 0 registros", "sInfoFiltered": "(filtrado de un total de _MAX_ registros)", "sInfoPostFix": "", "sSearch": "Buscar:", "sUrl": "", "sInfoThousands": ",", "sLoadingRecords": "Cargando...", "oPaginate": { "sFirst": "Primero", "sLast": "Último", "sNext": "Siguiente", "sPrevious": "Anterior" }, "oAria": { "sSortAscending": ": Activar para ordenar la columna de manera ascendente", "sSortDescending": ": Activar para ordenar la columna de manera descendente" }
        },
    });
}

function print(id) {
    StartLoading();
    $.ajax({
        url: "/EmptySpaceScan/ActionPrintEmptySpace",
        type: "POST",
        data: { "id_empty_scan": id },
        success: function (response) {
            EndLoading();
            if (response.responseText == "Terminó tu sesión.")
                SessionFalse(response.responseText);
            else if (response.success) {
                $("#modalReferenceBody").html("<iframe width='100%' height='550px' src='data:application/pdf;base64, " + encodeURI(response.responseText) + "'></iframe>")
                $("#modalReference").modal('show');
            }
            else
                toastr.error(response.responseText + " ddd");
        },
        error: function () {
            toastr.error('Error inesperado contactar a sistemas.');
            EndLoading();
        }
    });
}