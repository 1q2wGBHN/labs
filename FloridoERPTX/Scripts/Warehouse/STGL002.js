﻿$(document).ready(function () {
    window.setTimeout(function () { $("#fa").click(); }, 1000);
    var Locations = JSON.parse($("#model").val());
    table.clear();
    table.rows.add($(Locations));
    table.columns.adjust().draw();
});

$("#itemProduct").select2({
    minimumInputLength: 2,
    formatInputTooShort: function () {
        return "Busca un producto";
    },
    matcher: function (term, text, option) {
        return text.toUpperCase().indexOf(term.toUpperCase()) >= 0 || option.val().toUpperCase().indexOf(term.toUpperCase()) >= 0;
    }
});

function PanelInfoShow() {
    $('#PanelGid').animatePanel();
    $('#PanelGid').addClass("hide");
    $('#PanelInf').removeClass("hide");
    $('#PanelInf').animatePanel();
}

function PanelGridShow() {
    $('#form')[0].reset();
    $('#itemProduct').val('').trigger('change');
    $("#btExec").removeClass("btn btn-warning").addClass("btn btn-success");
    $("#PanelExec").removeClass("hyellow").addClass("hgreen");
    $("#btExec").text('Registrar');
    $('#PanelGid').animatePanel();
    $('#PanelInf').addClass("hide");
    $('#PanelGid').removeClass("hide");
    $('#PanelInf').animatePanel();
}

var table = $('#tablePlanogramLocation').DataTable({
    oLanguage: {
        "sProcessing": "Procesando...", "sLengthMenu": "Mostrar _MENU_ registros", "sZeroRecords": "No se encontraron resultados", "sEmptyTable": "Ningún dato disponible en esta tabla", "sInfo": "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros", "sInfoEmpty": "Mostrando registros del 0 al 0 de un total de 0 registros", "sInfoFiltered": "(filtrado de un total de _MAX_ registros)", "sInfoPostFix": "", "sSearch": "Buscar:", "sUrl": "", "sInfoThousands": ",", "sLoadingRecords": "Cargando...", "oPaginate": { "sFirst": "Primero", "sLast": "Último", "sNext": "Siguiente", "sPrevious": "Anterior" }, "oAria": { "sSortAscending": ": Activar para ordenar la columna de manera ascendente", "sSortDescending": ": Activar para ordenar la columna de manera descendente" }
    },
    dom: "<'row'<'col-sm-4'l><'col-sm-4 text-left'B><'col-sm-4'f>t<'col-sm-6'i><'col-sm-6'p>>",
    lengthMenu: [[10, 25, 50, -1], [10, 25, 50, "Todos"]],
    buttons: [
        { extend: 'csv', text: 'Excel', title: 'Planograma', className: 'btn-sm' },
        {
            extend: 'pdf', text: 'PDF', title: 'Planograma', className: 'btn-sm', exportOptions: {
                columns: [1, 2, 3, 4]
            },
            customize: function (doc) {
                doc.content[1].table.widths =
                    Array(doc.content[1].table.body[0].length + 1).join('*').split('');
            }
        },
        { extend: 'print', text: 'Imprimir', className: 'btn-sm' },
    ],
    columns: [
        { data: null },
        { data: 'planogram_location' },
        { data: 'part_number' },
        { data: 'part_description' },
        { data: 'quantity' },
        { data: 'planogram_description' },
        { data: 'planogram_type' },
        { data: 'planogram_status' },
        { data: 'bin1' },
        { data: 'bin2' },
        { data: 'bin3' }
    ],
    columnDefs: [
        {
            data: null,
            width: "1%",
            defaultContent: '<button class="btn btn-xs btn-outline btn-warning" id="edit-button" type="submit"><i class="pe-7s-config"></i> Editar</button>',
            targets: [0]
        },
        {
            width: "20%",
            targets: [4]
        },
        {
            visible: false,
            targets: [7, 8, 9, 10]
        }
    ]
});

$('#tablePlanogramLocation tbody').on('click', '#edit-button', function () {
    var values = table.row($(this).parents('tr')).data();
    values = $.map(values, function (value, index) {
        switch (index) {
            case 'planogram_location':
                $("#planogram_location").val(value);
                $("#planogram_locationEdit").val(value);
                break;
            case 'part_number':
                $("#itemProduct").val(value).trigger('change');
                $("#itemProductEdit").val(value);
                break;
            case 'planogram_description':
                $("#planogram_description").val(value);
                break;
            case 'quantity':
                $("#quantity").val(value);
                break;
            case 'planogram_type':
                $("#planogram_type").val(value);
                break;
            case 'bin1':
                $("#bin1").val(value);
                break;
            case 'bin2':
                $("#bin2").val(value);
                break;
            case 'bin3':
                $("#bin3").val(value);
                break;
        }
    });
    $("#btExec").removeClass("btn btn-success").addClass("btn btn-warning");
    $("#PanelExec").removeClass("hgreen").addClass("hyellow");
    $("#btExec").text('Editar');
    PanelInfoShow();
});

function binsChange() {
    var locationString = $("#planogram_location").val().toUpperCase();
    if (locationString.length == 6) {
        $("#bin1").val(locationString.slice(0, 2));
        $("#bin2").val(locationString.slice(2, 4));
        $("#bin3").val(locationString.slice(4, 6));
    }
}

function ValidLocacionExist() {

}

var ValidateF = $("#form").validate({
    ignore: [],
    rules: {
        planogram_location: {
            required: true,
            minlength: 6,
            maxlength: 6
        },
        itemProduct: {
            required: true
        },
        planogram_description: {
            required: true
        },
        bin1: {
            required: true
        },
        bin2: {
            required: true
        },
        bin3: {
            required: true
        },
        quantity: {
            required: true,
            number: true
        },
        planogram_type: {
            required: true
        }
    },
    messages: {
        planogram_location: {
            required: "El Campo no puede ser nulo",
            minlength: "Por favor ingrese al menos 6 caracteres.",
            maxlength: "Por favor, introduzca no más de 6 caracteres."
        },
        itemProduct: {
            required: "El Campo no puede ser nulo"
        },
        planogram_description: {
            required: "El Campo no puede ser nulo"
        },
        bin1: {
            required: "El Campo no puede ser nulo"
        },
        bin2: {
            required: "El Campo no puede ser nulo"
        },
        bin3: {
            required: "El Campo no puede ser nulo"
        },
        quantity: {
            required: "El Campo no puede ser nulo"

        },
        planogram_type: {
            required: "El Campo no puede ser nulo"
        }
    }
});

function Register() {
    if (ValidateF.element("#planogram_location") && ValidateF.element("#itemProduct") && ValidateF.element("#bin1") && ValidateF.element("#bin2") && ValidateF.element("#bin3") && ValidateF.element("#quantity") && ValidateF.element("#planogram_description") && ValidateF.element("#planogram_type")) {
        swal({
            title: "Esta seguro?",
            text: "Desea guardar los cambios?",
            type: "warning",
            closeOnConfirm: true,
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Continuar",
            cancelButtonText: "Cancelar"
        }, function (isConfirm) {
            setTimeout(function () {
                if (isConfirm) {
                    var type = true; $('#btExec').text();
                    if ($('#btExec').text() == "Editar")
                        type = false;
                    StartLoading();
                    $.ajax({
                        url: "/PlanogramLocation/ActionExecute",
                        type: "POST",
                        data: { "planogram_location": $('#planogram_location').val(), "part_number": $('#itemProduct').val(), "planogram_description": $('#planogram_description').val(), "bin1": $('#bin1').val(), "bin2": $('#bin2').val(), "bin3": $('#bin3').val(), "quantity": $('#quantity').val(), "planogram_type": $('#planogram_type').val(), typeExecute: type, planogram_locationEdit: $('#planogram_locationEdit').val(), itemProductEdit: $('#itemProductEdit').val() },
                        success: function (result) {
                            EndLoading();
                            if (result.success) {
                                table.clear();
                                table.rows.add($(result.JsonList));
                                table.columns.adjust().draw();
                                toastr.success(result.responseText);
                                PanelGridShow();
                            }
                            else
                                toastr.error(result.responseText);
                        },
                        error: function (result) {
                            EndLoading();
                            toastr.error("Ocurrió un error al procesar.");
                        }
                    });
                }
            }, 400)
        });
    }
}

var tour = new Tour({
    backdrop: true,//Bloquea contenido Atras
    keyboard: true,//Cambia de paso con flechas del teclado
    //Lenguaje Español
    template: "<div class='popover tour' style='max-width: 400px;width: 400px;'> <div class='arrow'></div> <h3 class='popover-title'></h3> <div class='popover-content'></div> <nav class='popover-navigation'> <div class='btn-group'> <button class='btn btn-default' data-role='prev' type='button'>Atras</button> <button class='btn btn-default' data-role='next' type='button'>Siguiente</button> <button class='btn btn-primary' data-role='end' type='button'>Fin</button> </div> </nav> </div>",
    onShown: function (tour) {//Animacion
        $('.animated').removeClass('fadeIn');
        $('.animated-panel').removeClass('zoomIn');
    },
    steps: [//Pasos del tour
        {
            delay: 300,
            element: "#PanelGid",
            title: "Reporte de Planograma",
            content: "Reporte donde se muestran todas las locaciones del Planograma",
            placement: "top",
        },
        {
            delay: 300,
            backdropPadding: 8,
            element: "#Btadd",
            title: "Agregar",
            content: "Boton para agregar nueva locacion.",
            placement: "right",
            onNext: function () {
                PanelInfoShow();
            }
        },
        {
            delay: 300,
            element: "#PanelInf",
            title: "Registro de Planograma",
            content: "Formulario para registrar o editar alguna locación, Favor de llenar todos los campos. La locación se compone por 6 caracteres",
            placement: "top",
            onPrev: function () {
                PanelGridShow();
            }
        },
        {
            delay: 300,
            backdropPadding: 8,
            element: "#btExec",
            title: "Registro de Planograma",
            content: "Boton para registrar los cambios",
            placement: "top",
        }
    ]
});
tour.init();
function HelpTour() {
    setTimeout(function () {
        PanelGridShow();
    }, 400);
    tour.restart();
}