﻿$(document).ready(function () {
    window.setTimeout(function () { $("#fa").click(); }, 1000);
});

var Transfers = $('#Transfers').DataTable({
    oLanguage: {
        "sProcessing": "Procesando...", "sLengthMenu": "Mostrar _MENU_ registros", "sZeroRecords": "No se encontraron resultados", "sEmptyTable": "Ningún dato disponible en esta tabla", "sInfo": "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros", "sInfoEmpty": "Mostrando registros del 0 al 0 de un total de 0 registros", "sInfoFiltered": "(filtrado de un total de _MAX_ registros)", "sInfoPostFix": "", "sSearch": "Buscar:", "sUrl": "", "sInfoThousands": ",", "sLoadingRecords": "Cargando...", "oPaginate": { "sFirst": "Primero", "sLast": "Último", "sNext": "Siguiente", "sPrevious": "Anterior" }, "oAria": { "sSortAscending": ": Activar para ordenar la columna de manera ascendente", "sSortDescending": ": Activar para ordenar la columna de manera descendente" }
    },
    responsive: true,
    autoWidth: true,
    order: [[1, "desc"]],
    buttons: [
        { extend: 'csv', text: 'Excel', title: 'Transferencias', className: 'btn-sm' },
        {
            extend: 'pdf', text: 'PDF', title: 'Transferencias', className: 'btn-sm', exportOptions: {
                columns: [0, 1, 2, 3, 4, 5, 6, 7]
            },
            customize: function (doc) {
                doc.content[1].table.widths =
                    Array(doc.content[1].table.body[0].length + 1).join('*').split('');
            }
        },
        { extend: 'print', text: 'Imprimir', className: 'btn-sm' },
    ],
    columnDefs: [
        { targets: 5, type: 'numeric-comma' },
        {
            type: "numeric-comma",
            render: function (data, type, row) {
                return '$' + data.toFixed(2);
            },
            targets: [6]
        }],
    columns: [
        {
            "class": "details-control",
            "orderable": false,
            "data": null,
            "defaultContent": "",
            width: "1%",
            targets: 0
        },
        { targets: 1, data: 'TransferDocument', width: "1%" },
        { targets: 2, data: 'TransferSiteName', width: "1%" },
        { targets: 3, data: 'DestinationSiteName', width: "1%" },
        { targets: 4, data: 'Status', width: "1%" },
        { targets: 5, data: 'TransferDate', width: "1%" },
        { targets: 6, data: 'TotalAmount', width: "1%" },
        {
            targets: [7],
            orderable: false,
            data: 'Diferences',
            width: "1%",
            className: "text-center",
            render: function (data, type, full, meta) {
                if ($("#TransferType").val() == "entrada" && (full.Status == "Recibida" || full.Status == "Recibido") && full.TransferSiteName == "FLORIDO CEDIS") {

                    return `<button class="btn btn-xs btn-outline btn-info" onClick = "ProcessTransfer('${full.TransferDocument}','${full.TransferSiteCode}')"><i class="fa fa-info-circle"></i> Procesar</button>`;

                }
                if ($("#TransferType").val() == "entrada" &&
                    full.Status == "En transito" &&
                    full.TransferSiteName == "FLORIDO CEDIS") {
                    return "Con diferencia";
                } else {
                    return "";
                }


            }
        }]
});

function reloadStyleTable() {
    $('#tabledetail').DataTable({
        oLanguage: {
            "sProcessing": "Procesando...", "sLengthMenu": "Mostrar _MENU_ registros", "sZeroRecords": "No se encontraron resultados", "sEmptyTable": "Ningún dato disponible en esta tabla", "sInfo": "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros", "sInfoEmpty": "Mostrando registros del 0 al 0 de un total de 0 registros", "sInfoFiltered": "(filtrado de un total de _MAX_ registros)", "sInfoPostFix": "", "sSearch": "Buscar:", "sUrl": "", "sInfoThousands": ",", "sLoadingRecords": "Cargando...", "oPaginate": { "sFirst": "Primero", "sLast": "Último", "sNext": "Siguiente", "sPrevious": "Anterior" }, "oAria": { "sSortAscending": ": Activar para ordenar la columna de manera ascendente", "sSortDescending": ": Activar para ordenar la columna de manera descendente" }
        },
        columnDefs: [{
            targets: [2],
            width: "1%"
        }]
    });
}

function format(d) {
    var transfertype = $("#TransferType").val();
    var url;
    if (transfertype == "salida")
        url = "/TransferHeader/ActionGetAllTransferDetailsReport";
    else
        url = "/TransferHeader/ActionGetTransferDetailsIn";

    var detailItems = "";
    var tabledetail = $('<div/>').addClass('loading').text('Cargando Datos...');
    $.ajax({
        url: url,
        data: { "Document": d.TransferDocument, "TransferSiteCode": d.TransferSiteCode},
        type: "GET",
        success: function (data) {
            if (data == "SF")
                SessionFalse("Terminó tu sesión");
            else {
                if (transfertype == "salida") {
                    $.each(data, function (index, value) {
                        detailItems += "<tr><td>" + value.PartNumber + "</td><td>" + value.Description + "</td><td>" + value.Quantity + "</td><td>" + value.transfer_status_desc + "</td><td>$" + value.UnitCost + "</td><td>$" + value.Iva + "</td><td>$" + value.IEPS + "</td><td>$" + value.SubTotal + "</td><td>$" + value.Amount + "</td>";
                    });
                    var Apro = '';
                    if (data.length != 0) {
                        tabledetail.html('<table id="tabledetail" class="table table-striped table-bordered table-hover" style="width:100%"><thead><tr><th>Codigo</th><th>Descripcion</th><th>Cantidad</th><th>Status</th><th>Costo unitario</th><th>IVA</th><th>IEPS</th><th>SubTotal</th><th>Total</th></tr></thead><tbody>' + detailItems + '</tbody></table>').removeClass('loading');
                        reloadStyleTable();
                    }
                    tabledetail.html('<table id="tabledetail" class="table table-striped table-bordered table-hover" style="width:100%"><thead><tr><th>Codigo</th><th>Descripcion</th><th>Cantidad</th><th>Status</th><th>Costo Unitario</th><th>IVA</th><th>IEPS</th><th>SubTotal</th><th>Total</th></tr></thead><tbody>' + detailItems + '</tbody></table>').removeClass('loading');
                    reloadStyleTable();
                } else { // Entradas
                    $.each(data, function (index, value) {
                        if (value.GrQuantity == null)
                            value.GrQuantity = "";

                        if (value.TransferStatus == 2 & value.DestinationSiteCode == "FLORIDO CEDIS") {
                            if (value.GrQuantity != value.Quantity)
                                detailItems += "<tr><td>" + value.PartNumber + "</td><td>" + value.Description + "</td><td class='text-center' style='background-color:#ffcf40; font-weight:bold;'>" + value.Quantity + "</td><td class='text-center' style='background-color:#ffcf40; font-weight:bold;'>" + value.GrQuantity + "</td><td>$" + value.UnitCost + "</td><td>$" + value.Iva + "</td><td>$" + value.IEPS + "</td><td>$" + value.SubTotal + "</td><td>$" + value.Amount + "</td>";
                            else
                                detailItems += "<tr><td>" + value.PartNumber + "</td><td>" + value.Description + "</td><td class='text-center'>" + value.Quantity + "</td><td class='text-center'>" + value.GrQuantity + "</td><td>$" + value.UnitCost + "</td><td>$" + value.Iva + "</td><td>$" + value.IEPS + "</td><td>$" + value.SubTotal + "</td><td>$" + value.Amount + "</td>";
                        }
                        else
                            detailItems += "<tr><td>" + value.PartNumber + "</td><td>" + value.Description + "</td><td class='text-center'>" + value.Quantity + "</td><td class='text-center'>" + value.GrQuantity + "</td><td>$" + value.UnitCost + "</td><td>$" + value.Iva + "</td><td>$" + value.IEPS + "</td><td>$" + value.SubTotal + "</td><td>$" + value.Amount + "</td>";
                    });
                    var Apro = '';
                    if (data.length != 0) {
                        tabledetail.html('<table id="tabledetail" class="table table-striped table-bordered table-hover" style="width:100%"><thead><tr><th>Codigo</th><th>Descripcion</th><th class="text-center">Cantidad</th><th class="text-center">Cantidad Escaneada</th><th>Costo unitario</th><th>IVA</th><th>IEPS</th><th>SubTotal</th><th>Total</th></tr></thead><tbody>' + detailItems + '</tbody></table>').removeClass('loading');
                        reloadStyleTable();
                    }
                    tabledetail.html('<table id="tabledetail" class="table table-striped table-bordered table-hover" style="width:100%"><thead><tr><th>Codigo</th><th>Descripcion</th><th class="text-center">Cantidad</th><th class="text-center">Cantidad Escaneada</th><th>Costo Unitario</th><th>IVA</th><th>IEPS</th><th>SubTotal</th><th>Total</th></tr></thead><tbody>' + detailItems + '</tbody></table>').removeClass('loading');
                    reloadStyleTable();
                }
            }
        },
        error: function (json) {
            EndLoading();
            toastr.error('Alerta - Error inesperado contactar a sistemas.');
        }
    });
    return tabledetail;
}

var detailRows = [];
$('#Transfers tbody').on('click', 'tr td.details-control', function () {
    var tr = $(this).closest('tr');
    var row = Transfers.row(tr);

    if (row.child.isShown()) {
        row.child.hide();
        tr.removeClass('details');
    }
    else {
        if (Transfers.row('.details').length) {
            $('.details-control', Transfers.row('.details').node()).click();
        }
        row.child(format(row.data())).show();
        tr.addClass('details');
    }
});

Transfers.on('draw', function () {
    $.each(detailRows, function (i, id) {
        $('#' + id + ' td.details-control').trigger('click');
    });
});

function Update() {
    $.ajax({
        type: "POST",
        url: "/TransferHeader/ActionUpdateTransferHeaderInInputs/",
        success: function (response) {
            EndLoading();
            if (response.result == "SF")
                SessionFalse("Terminó tu sesión");
            else if (response.result) {
                Transfers.clear();
                Transfers.rows.add($(response.Data));
                Transfers.columns.adjust().draw();
                $('#Transfers').animatePanel();
            }
        },
        error: function (response) {
            EndLoading();
            toastr.error('Alerta - Error inesperado contactar a sistemas.');
        }
    });
}

function SearchTransfer() {
    toastr.remove();
    var transfertype = $("#TransferType").val();
    var url;

    if (transfertype == "salida")
        url = "/TransferHeader/ActionGetAllTransferHeaderWithoutStatus/";
    else
        url = "/TransferHeader/ActionGetTransferHeaderIn/";

    if ($('#beginDate').val() != "" & $('#endDate').val() != "") {
        if (moment($('#beginDate').val()) <= moment($('#endDate').val())) {
            if (transfertype != "0") {
                StartLoading();
                $.ajax({
                    type: "POST",
                    url: "/TransferHeader/ActionGetTransferHeaderInInputsView/",
                    data: { "BeginDate": $('#beginDate').val(), "EndDate": $('#endDate').val(), "TransferType": transfertype, "status": "", "originSite": "", "partNumber": "", "department": "", "family": "" },
                    success: function (response) {
                        EndLoading();
                        if (response.result == "SF")
                            SessionFalse("Terminó tu sesión");
                        else if (response.result) {
                            Transfers.clear();
                            Transfers.rows.add($(response.Data));
                            Transfers.columns.adjust().draw();
                            $('#Transfers').animatePanel();
                        }
                    },
                    error: function (response) {
                        EndLoading();
                        toastr.error('Alerta - Error inesperado contactar a sistemas.');
                    }
                });
            }
            else
                toastr.error("Seleccione el tipo de transferencia.");
        }
        else {
            toastr.error("Fecha final no puede ser menor a la inicial.");
            document.getElementById('beginDate').value = '';
            document.getElementById('endDate').value = '';
            $('#beginDate').focus();
        }
    }
}

$("#beginDate").datepicker({
    autoclose: true,
    todayHighlight: true,
    showButtonPanel: true,
}).on("changeDate", function (e) {
    SearchTransfer();
});

$("#endDate").datepicker({
    autoclose: true,
    startDate: '01/01/1950',
    todayHighlight: true,
}).on("changeDate", function (e) {
    SearchTransfer();
});

function ProcessTransfer(document,transferSiteCode) {
    swal({
        title: "¿Esta seguro que desea procesar la transferencia?",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "Continuar",
        cancelButtonText: "Cancelar"
    }, function (isConfirm) {
        if (isConfirm) {
            StartLoading();
            $.ajax({
                url: "/TransferHeader/ActionProcessTransferHeaderIn",
                type: "POST",
                data: { "TransferDocument": document,TransferSiteCode:transferSiteCode },
                success: function (response) {
                    EndLoading();
                    if (response.Data == "SF")
                        SessionFalse("Terminó tu sesión");
                    else if (response.Data.ReturnValue == 0) {
                        toastr.success("Transferido Correctamente");
                        Update();
                    }
                    else
                        toastr.error('Error inesperado contactar a sistemas.');
                },
                error: function () {
                    EndLoading();
                    toastr.error('Error inesperado contactar a sistemas.');
                }
            });
        }
    });
}