﻿const formatmoney = money => decimal => new Intl.NumberFormat("en-US", { style: "currency", currency: "USD", currencyDisplay: "symbol", maximumFractionDigits: decimal }).format(money);

$(document).ready(function () {
    window.setTimeout(function () { $("#fa").click(); }, 1000);
    $('#cbox_ReportType').iCheck({
        checkboxClass: 'icheckbox_square-green',
    }).on('ifClicked', function () {
        $(this).trigger("click");
    });
});

$("#ValidDate1").datepicker({
    autoclose: true,
    todayHighlight: true,
    showButtonPanel: true,
}).on("changeDate", function (e) {
    if ((moment($('#ValidDate1').val()) <= moment($('#ValidDate2').val()))) {
        SearchTransfer();
    }
});

$("#ValidDate2").datepicker({
    autoclose: true,
    todayHighlight: true,
}).on("changeDate", function (e) {
    SearchTransfer();
});

function ClearSelect() {
    $('#ReportTypeStatus').val($("#ReportTypeStatus :first").val()).trigger('change');
    $('#LocationsDrop').val($("#LocationsDrop :first").val()).trigger('change');
    $('#departmentDrop').val($("#departmentDrop :first").val()).trigger('change');
    $('#family').val($("#family :first").val()).trigger('change');
    $("#cbox_ReportType").iCheck('uncheck');
    $("#ValidDate1").val("");
    $("#ValidDate2").val("");
    checkDetailRaw();
}

function checkDetailRaw() {
    if ($("#cbox_ReportType").is(':checked')) {
        $("#departmentDropDiv").show();
        $("#familyDiv").show();
        $("#btnPrintDiv").show();

    } else {
        $("#departmentDropDiv").hide();
        $("#familyDiv").hide();
        $("#btnPrintDiv").hide();
    }
    SearchTransfer();
}

function FillDropdown(selector, vData) {
    if (vData != null) {
        var vItems = [];
        vItems.push('<option value="">Seleccione una familia</option>');
        for (var i in vData) {
            if (vData[i].Selected)
                vItems.push('<option selected value="' + vData[i].class_id + '">' + vData[i].description + '</option>');
            else
                vItems.push('<option value="' + vData[i].class_id + '">' + vData[i].description + '</option>');
        }
        $('#' + selector).empty();
        $('#' + selector).append(vItems.join(''));
        $('#family').val($("#family :first").val()).trigger('change');
    }
    else {
        var vItems = [];
        vItems.push('<option selected value="">Seleccione una familia</option>');
        $('#' + selector).empty();
        $('#' + selector).append(vItems.join(''));
        $('#family').val($("#family :first").val()).trigger('change');
    }
}


function GetFamily() {
    var depto = $("#departmentDrop").val();
    if (depto == "" || depto == 0) {
        $('#family').val($("#family :first").val()).trigger('change');
        FillDropdown("family", null)
        return false;
    }
    $.ajax({
        url: '/MaClass/ActionGetFamily/',
        type: "GET",
        data: { "deptoo": depto },
        success: function (response) {
            if (response.success) {
                listFamily = response.Json;
                FillDropdown("family", listFamily);
            }
            else
                FillDropdown("family", null);
        },
        error: function () {
            FillDropdown("family", null);
            toastr.error('Error inesperado contactar a sistemas.');
        }
    });
}

$("#tableRaw").append('<tfoot><th>Total</th><th colspan = "7"></th><th ></th><th ></th><th ></th><th ></th><th ></th></tfoot>');
/////////////////////////////////////p
var table = $('#tableRaw').DataTable({
    responsive: true,
    autoWidth: true,
    oLanguage:
    {
        "sProcessing": "Procesando...",
        "sLengthMenu": "Mostrar _MENU_ registros",
        "sZeroRecords": "No se encontraron resultados",
        "sEmptyTable": "Ningún dato disponible en esta tabla",
        "sInfo": "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
        "sInfoEmpty": "Mostrando registros del 0 al 0 de un total de 0 registros",
        "sInfoFiltered": "(filtrado de un total de _MAX_ registros)",
        "sInfoPostFix": "",
        "sSearch": "Buscar:",
        "sUrl": "",
        "sInfoThousands": ",",
        "sLoadingRecords": "Cargando...",
        "oPaginate": { "sFirst": "Primero", "sLast": "Último", "sNext": "Siguiente", "sPrevious": "Anterior" },
        "oAria": { "sSortAscending": ": Activar para ordenar la columna de manera ascendente", "sSortDescending": ": Activar para ordenar la columna de manera descendente" }
    },
    dom: "<'row'<'col-sm-4'l><'col-sm-4 text-left'B><'col-sm-4'f>t<'col-sm-6'i><'col-sm-6'p>>",
    lengthMenu: [[10, 25, 50, -1], [10, 25, 50, "Todos"]],
    buttons: [
        { extend: 'csv', text: 'Excel', title: 'MateriaPrimaConcentrado', className: 'btn-sm', footer: true },
        {
            extend: 'pdf', text: 'PDF', title: 'MateriaPrimaConcentrado', className: 'btn-sm', exportOptions: {
                columns: [1, 2, 3, 4, 5, 6, 7]
            },
            customize: function (doc) {
                doc.content[1].table.widths =
                    Array(doc.content[1].table.body[0].length + 1).join('*').split('');
            }, footer: true
        },
        { extend: 'print', text: 'Imprimir', className: 'btn-sm', footer: true },
    ],
    "footerCallback": function (row, data, start, end, display) {
        var api = this.api(), data;
        // converting to interger to find total
        var intVal = function (i) {
            return typeof i === 'string' ?
                i.replace(/[\$,]/g, '') * 1 :
                typeof i === 'number' ?
                    i : 0;
        };
        for (var i = 8; i < 12; i++) {
            var monTotal = api
                .column(i)
                .data()
                .reduce(function (a, b) {
                    return intVal(a) + intVal(b);
                }, 0);
            $(api.column(i).footer()).html(`<span ${monTotal == 0 ? "" : `style = "Color:${monTotal >= 0 ? "green" : "red"}"`} > ${formatmoney(monTotal)(4)}</span >`);
        }
    },
    columns: [
        {
            "class": "details-control",
            "orderable": false,
            "data": null,
            "defaultContent": "",
            width: "1%"
        },
        { data: 'Folio', width: "1%" },
        { data: 'Status', width: "1%" },
        { data: 'TransferDate', width: "1%" },
        { data: 'Received_by', width: "1%" },
        { data: 'Autothized_by', width: "1%" },
        { data: 'StorageLocation', width: "1%" },
        { data: 'Comment', width: "1%" },
        { data: 'Iva', width: "1%" },
        { data: 'Ieps', width: "1%" },
        { data: 'SubTotal', width: "1%" },
        { data: 'Total', width: "1%" },
        { data: null, width: "1%" }
    ],
    columnDefs: [
        {
            targets: [0, 1, 2, 3, 4, 5, 6, 7, 8],
            width: "1%"
        },
        {
            targets: [12],
            render: function (data, type, full, meta) {
                return `<button class="btn btn-xs btn-outline btn-info" id="${data.Folio}" onClick = "PrintReport(${data.Folio})"><i class="fa fa-info-circle"></i> Detalle</button>`;
            }
        },
        {
            targets: [8, 9, 10, 11],
            width: "1%",
            render: function (data, type, full, meta) {
                return `${formatmoney(data)(4)}`
            },
        },
        {
            targets: [2],
            orderable: false,
            data: null,
            width: "1%",
            render: function (data, type, full, meta) {
                if (data == 0)
                    return "Inicial";
                else if (data == 2)
                    return "Aprobado por Recibo";
                else if (data == 9)
                    return "Completado";
                else if (data == 8)
                    return "Cancelado";
            },
        }]
});

$("#detailReport").append('<tfoot><th>Total</th><th colspan = "10"></th><th id="12"></th><th id="13"></th><th id=14"></th><th id="15"></th><th id="16"></th></tfoot>');

var tableDetail = $('#detailReport').DataTable({
    responsive: true,
    autoWidth: true,
    oLanguage:
    {
        "sProcessing": "Procesando...",
        "sLengthMenu": "Mostrar _MENU_ registros",
        "sZeroRecords": "No se encontraron resultados",
        "sEmptyTable": "Ningún dato disponible en esta tabla",
        "sInfo": "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
        "sInfoEmpty": "Mostrando registros del 0 al 0 de un total de 0 registros",
        "sInfoFiltered": "(filtrado de un total de _MAX_ registros)",
        "sInfoPostFix": "",
        "sSearch": "Buscar:",
        "sUrl": "",
        "sInfoThousands": ",",
        "sLoadingRecords": "Cargando...",
        "oPaginate": { "sFirst": "Primero", "sLast": "Último", "sNext": "Siguiente", "sPrevious": "Anterior" },
        "oAria": { "sSortAscending": ": Activar para ordenar la columna de manera ascendente", "sSortDescending": ": Activar para ordenar la columna de manera descendente" }
    },
    dom: "<'row'<'col-sm-4'l><'col-sm-4 text-left'B><'col-sm-4'f>t<'col-sm-6'i><'col-sm-6'p>>",
    lengthMenu: [[10, 25, 50, -1], [10, 25, 50, "Todos"]],
    "footerCallback": function (row, data, start, end, display) {
        var api = this.api(), data;
        // converting to interger to find total
        var intVal = function (i) {
            return typeof i === 'string' ?
                i.replace(/[\$,]/g, '') * 1 :
                typeof i === 'number' ?
                    i : 0;
        };
        for (var i = 12; i < 16; i++) {
            var monTotal = api
                .column(i)
                .data()
                .reduce(function (a, b) {
                    return intVal(a) + intVal(b);
                }, 0);
            $(api.column(i).footer()).html(`<span ${monTotal == 0 ? "" : `style = "Color:${monTotal >= 0 ? "green" : "red"}"`} > ${formatmoney(monTotal)(4)}</span >`);
        }
    },
    buttons: [
        { extend: 'csv', text: 'Excel', title: 'MateriaPrimaDetalle', className: 'btn-sm', footer: true },
        {
            extend: 'pdf', text: 'PDF', title: 'MateriaPrimaDetalle', className: 'btn-sm', exportOptions: {
                columns: [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14]
            },
            customize: function (doc) {
                doc.content[1].table.widths =
                    Array(doc.content[1].table.body[0].length + 1).join('*').split('');
            }, footer: true
        },
        { extend: 'print', text: 'Imprimir', className: 'btn-sm', footer: true },
    ],
    columns: [
        { data: 'Folio', width: "1%" }, //**0
        { data: 'Part_Number', width: "1%" }, //0 //**1
        { data: 'Description', width: "1%" }, //0 //**2
        { data: 'Department', width: "1%" }, //1 //**3
        { data: 'Family', width: "1%" }, //2 //**4
        { data: 'TransferDate', width: "1%" }, //**5
        { data: 'Received_by', width: "1%" }, //**6
        { data: 'Autothized_by', width: "1%" }, //**7
        { data: 'StorageLocation', width: "1%" }, //**8
        { data: 'Item_Status', width: "1%" }, //**9
        { data: 'Quantity', width: "1%" }, //3 //**10
        { data: 'UnitCost', width: "1%" }, //4 //**11
        { data: 'Iva', width: "1%" }, //5 //**12
        { data: 'IEPS', width: "1%" }, //6 //**13
        { data: 'SubTotal', width: "1%" }, //7 //**14
        { data: 'Amount', width: "1%" } //8 -- //14 //**15
    ]
});

var detailRows = [];
$('#tableRaw tbody').on('click', 'tr td.details-control', function () {
    var tr = $(this).closest('tr');
    var row = table.row(tr);

    if (row.child.isShown()) {
        row.child.hide();
        tr.removeClass('details');
    }
    else {
        if (table.row('.details').length) {
            $('.details-control', table.row('.details').node()).click();
        }
        row.child(format(row.data())).show();
        tr.addClass('details');
    }
});

table.on('draw', function () {
    $.each(detailRows, function (i, id) {
        $('#' + id + ' td.details-control').trigger('click');
    });
});

//Tabla detalles
function format(d) {
    var detailItems = "";
    var tabledetail = $('<div/>').addClass('loading').text('Cargando Datos...');
    $.ajax({
        url: "/RawMaterial/ActionGetAllRawMaterialDetailByStatus",
        data: { "folio": d.Folio, "status": d.Status },
        type: "GET",
        success: function (data) {
            if (data == "SF")
                SessionFalse("Terminó tu sesión");
            else {
                $.each(data, function (index, value) {
                    detailItems += "<tr><td>" + value.Part_Number + "</td><td>" + value.Description + "</td><td>" + value.Quantity + "</td><td>$" + value.UnitCost + "</td><td>$" + value.Iva + "</td><td>$" + value.IEPS + "</td><td>$" + value.SubTotal + "</td><td>$" + value.Amount + "</td>";
                });
                var Apro = '';
                if (data.length != 0) {
                    tabledetail.html('<table id="tabledetail" class="table table-striped table-bordered table-hover" style="width:100%"><thead><tr><th>Codigo</th><th>Descripcion</th><th>Cantidad</th><th>Costo unitario</th><th>IVA</th><th>IEPS</th><th>SubTotal</th><th>Total</th></tr></thead><tbody>' + detailItems + '</tbody></table>').removeClass('loading');
                    reloadStyleTable();
                }
                tabledetail.html('<table id="tabledetail" class="table table-striped table-bordered table-hover" style="width:100%"><thead><tr><th>Codigo</th><th>Descripcion</th><th>Cantidad</th><th>Costo Unitario</th><th>IVA</th><th>IEPS</th><th>SubTotal</th><th>Total</th></tr></thead><tbody>' + detailItems + '</tbody></table>').removeClass('loading');
                reloadStyleTable();
            }

            $(".btn-success").on("click", function () {
                Aprobar($(this).attr("folio"));
            });

            $(".btn-danger").on("click", function () {
                Cancelar($(this).attr("folio"));
            });
        }
    });
    return tabledetail;
}

//Recargar tabla
function reloadStyleTable() {
    $('#tabledetail').DataTable({
        oLanguage: {
            "sProcessing": "Procesando...", "sLengthMenu": "Mostrar _MENU_ registros", "sZeroRecords": "No se encontraron resultados", "sEmptyTable": "Ningún dato disponible en esta tabla", "sInfo": "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros", "sInfoEmpty": "Mostrando registros del 0 al 0 de un total de 0 registros", "sInfoFiltered": "(filtrado de un total de _MAX_ registros)", "sInfoPostFix": "", "sSearch": "Buscar:", "sUrl": "", "sInfoThousands": ",", "sLoadingRecords": "Cargando...", "oPaginate": { "sFirst": "Primero", "sLast": "Último", "sNext": "Siguiente", "sPrevious": "Anterior" }, "oAria": { "sSortAscending": ": Activar para ordenar la columna de manera ascendente", "sSortDescending": ": Activar para ordenar la columna de manera descendente" }
        },
        columnDefs: [{
            targets: [2],
            width: "1%"
        }]
    });
}

function SearchTransfer() {
    toastr.remove();
    if ($('#ValidDate1').val() != "" & $('#ValidDate2').val() != "") {
        if (moment($('#ValidDate1').val()) <= moment($('#ValidDate2').val())) {
            StartLoading();
            if (!$("#cbox_ReportType").is(':checked')) {
                $.ajax({
                    type: "POST",
                    url: "/RawMaterial/ActionGetAllTransfersToRaw",
                    data: { "initial": $('#ValidDate1').val(), "finish": $('#ValidDate2').val(), "status": $('#ReportTypeStatus').val(), "location": $('#LocationsDrop').val() },
                    success: function (returndate) {
                        if (returndate.data == "SF")
                            SessionFalse("Terminó tu sesión");
                        else if (returndate) {
                            $("#tableDetail").addClass("hidden");
                            $('#TableProduct').removeClass("hidden");
                            if (returndate.length == 0) {
                                toastr.warning('No se encontraron traspasos de materia prima.');
                                table.clear();
                                table.columns.adjust().draw();
                                $('#TableProduct').animatePanel();
                            }
                            else {
                                table.clear();
                                table.rows.add($(returndate.Model));
                                table.columns.adjust().draw();
                                $('#TableProduct').animatePanel();
                            }
                        EndLoading();
                        }
                    },
                    error: function (returndate) {
                        toastr.error('Alerta - Error inesperado contactar a sistemas.');
                        EndLoading();
                    }
                });
            }
            else {
                $.ajax({
                    type: "POST",
                    url: "/RawMaterial/ActionGetAllRawMaterialDetailByDates",
                    data: { "BeginDate": $('#ValidDate1').val(), "EndDate": $('#ValidDate2').val(), "status": $('#ReportTypeStatus').val(), "department": $('#departmentDrop').val(), "family": $('#family').val(), "location": $('#LocationsDrop').val() },
                    success: function (response) {
                        console.log("response");
                        
                        if (response == "SF")
                            SessionFalse("Terminó tu sesión");
                        else if (response.result) {
                            $("#TableProduct").addClass("hidden");
                            $('#tableDetail').removeClass("hidden");
                            tableDetail.clear();
                            tableDetail.rows.add($(response.Model));
                            tableDetail.columns.adjust().draw();
                            $('#tableDetail').animatePanel();
                        }
                        EndLoading();
                    },
                    error: function (response) {
                        EndLoading();
                        toastr.error('Alerta - Error inesperado contactar a sistemas.');
                    }
                });
            }
        }
        else {
            toastr.error("Fecha final no puede ser menor a la inicial.");
            document.getElementById('ValidDate1').value = '';
            document.getElementById('ValidDate2').value = '';
            $('#ValidDate1').focus();
        }
    }
    else {
        table.clear();
        table.columns.adjust().draw();
        tableDetail.clear();
        tableDetail.columns.adjust().draw();
        toastr.error("Selecciona un rango de fechas.");
        $('#ValidDate1').focus();
    }
}

function PrintReport(reference) {
    StartLoading();
    $.ajax({
        url: "/RawMaterial/ActionGetRawDetails",
        type: "POST",
        data: { "folio": reference },
        success: function (response) {
            EndLoading();
            if (response.success) {
                $("#modalReferenceBody").html("<iframe width='100%' height='550px' src='data:application/pdf;base64, " + encodeURI(response.responseText) + "'></iframe>");
                $("#modalReference").modal('show');
            }
            else {
                if (response.responseText == "Termino tu sesión.")
                    SessionFalse(response.responseText);
                else
                    toastr.error(response.responseText);
            }
        },
        error: function () {
            toastr.error('Error inesperado contactar a sistemas.');
            EndLoading();
        }
    });
}

$("#btnPrint").click(function () {
    StartLoading();
    $.ajax({
        url: "/RawMaterial/ActionGetRawDetailsReport",
        type: "POST",
        data: { "BeginDate": $('#ValidDate1').val(), "EndDate": $('#ValidDate2').val(), "status": $('#ReportTypeStatus').val(), "department": $('#departmentDrop').val(), "family": $('#family').val(), "location": $('#LocationsDrop').val() },
        success: function (response) {
            EndLoading();
            if (response.result) {
                $("#modalReferenceBody").html("<iframe width='100%' height='550px' src='data:application/pdf;base64, " + encodeURI(response.responseText) + "'></iframe>");
                $("#modalReference").modal('show');
            }
            else if (!response.result)
                toastr.error(response.responseText);
            else
                SessionFalse("Termino tu sesión.");
        },
        error: function () {
            EndLoading();
            toastr.error('Error inesperado contactar a sistemas.');
        }
    });
});