﻿var listAddProducts = [];
var listFinal = [];
var enable = true;
var count = 1;

$(document).ready(function () {
    window.setTimeout(function () { $("#fa").click(); }, 1000);
    $("#siteDestino").select2();
});

const $ItemsDrop = $("#ItemsDrop");
const $siteDestino = $("#siteDestino");
const $qty = $("#qty");
const $quantity =  $("#quantity");
const $product_button = $("product_button");
$ItemsDrop.select2({
    minimumInputLength: 3,
    allowClear: true,
    placeholder: "Seleccione una producto",
    initSelection: function (element, callback) {
        callback({ id: "", text: "Seleccione una opcion" });
    },
    ajax: {
        url: "/Items/ActionSearchItem/",
        dataType: 'json',
        type: "GET",
        quietMillis: 50,
        data: function (Filter) {
            return {
                Filter: Filter
            };
        },
        results: function (data) {
            return {
                results: $.map(data.Json, function (item) {
                    return {
                        text: item.Description,
                        id: item.PathNumber
                    }
                }),
            };
        }
    }
});

var tableProducts = $('#table_products').DataTable({
    "paging": false,
    "ordering": false,
    "info": false,
    "searching": false,
    responsive: true,
    oLanguage: {
        "sZeroRecords": "",
        "sEmptyTable": "",
    },
    columnDefs: [
        {
            data: null,
            width: "1%",
            defaultContent: '<button class="btn btn-xs btn-danger btn-circle" id="remove-button" ><i class="fa fa-times"></i></button> ',
            targets: [3]
        }
    ],
    columns: [
        { data: 'part_number' },
        { data: 'part_description' },
        { data: 'quantity' },
        { data: null, width: "1%" }
    ],
});

tableProducts.clear();
tableProducts.columns.adjust().draw();



function addProduct() {
    toastr.remove();
    var qtyAvailable = parseInt($qty.val());
    var qtyRequired = parseInt($quantity.val());
    if ($siteDestino.val() == null || $siteDestino.val() == "" || $siteDestino.select2('data').text == "" || $siteDestino.select2('data').text == null) {
        toastr.warning("Seleccione un tienda a transferir.");
        $siteDestino.select2('open');
    } else {
        if ($ItemsDrop.val() == "" || $ItemsDrop.val() == 0 || $ItemsDrop.val() == null) {
            toastr.warning('Selecciona un producto.');
            $ItemsDrop.select2('open');
        } else {
            if (qtyRequired == "" || qtyRequired == null) {
                toastr.warning('Ingrese una cantidad.');
                $quantity.focus();
            } else {
                if (isNaN(qtyRequired)) {
                    toastr.warning('La cantidad requerida debe ser un numero entero, no puede ser decimal o de otro tipo.');
                } else {
                    if ($quantity.val().indexOf(".") != -1 || $quantity.val().indexOf("+") != -1 || $quantity.val().indexOf("-") != -1 || $quantity.val().indexOf("e") != -1) { /*$quantity.val().includes(".") || $quantity.val().includes("e") || $quantity.val().includes("-")){*/
                        toastr.warning('Ingrese solo numeros enteros.');
                        $quantity.focus();
                    } else {
                        if (qtyRequired > 500000) {
                            toastr.warning('No es posible ingresar cantidades mayores a 500000.');
                        } else {
                            if (qtyRequired <= 0) {
                                toastr.warning('La cantidad requerida debe ser mayor a 0.');
                                $quantity.focus();
                            } else {
                                toastr.success('Producto agregado: ' + $ItemsDrop.select2('data').text + ".");
                                $("#btnSave").prop("disabled", false);
                                $("#btnCancel").prop("disabled", false);
                                var Product =
                                {
                                    part_number: $ItemsDrop.val(),
                                    part_description: $ItemsDrop.select2('data').text,
                                    quantity: $quantity.val(),
                                };
                                var Final =
                                {
                                    part_number: $ItemsDrop.val(),
                                    quantity: $quantity.val(),
                                }
                                listFinal.push(Final);
                                listAddProducts.push(Product);
                                clearProduct();
                                $ItemsDrop.select2("val", "");
                            }
                        }
                    }
                }

            }
        }
    }
}

var form2 = $("#form_product").validate({
    rules: {},
    messages: {},
    submitHandler: function (form_product) {
        addProduct();
        tableProducts.clear();
        tableProducts.rows.add($(listAddProducts));
        tableProducts.columns.adjust().draw();
        return false;
    }
});

function ValidateSite() {
    if ($siteDestino.val() !== "") {
        $siteDestino.prop("disabled", true);
        $ItemsDrop.prop("disabled", false);
        $ItemsDrop.select2('open');
        $quantity.prop("disabled", false);
    }else {
        $ItemsDrop.prop("disabled", true);
        $qty.prop("disabled", true);
        $siteDestino.prop("disabled", false);
    }
    
    $("#btnCancel").prop("disabled", false);
}

function ValidateItem() {
    var valor = $ItemsDrop.val();
    if (listAddProducts == null) {
        return false;
    }
    for (var i = 0; i < listFinal.length; i++) {
        if (listFinal[i].part_number == valor) {
            for (var j = 0; j < listFinal.length; j++) {
                if (listFinal != null) {
                    if (listFinal[j].part_number == valor) {
                        toastr.remove();
                        toastr.error('Ya agregaste ' + listAddProducts[j].part_description);
                        $ItemsDrop.val('').trigger("change");
                        $ItemsDrop.select2('open');
                        clearProduct();
                        return;
                    }
                }
            }
        }
    }
    $quantity.prop("disabled", false);
    validateDestinationSite(valor);
}

function validateDestinationSite(productId) {
    const siteDestino = $("#siteDestino").val();
    if(!siteDestino){
        toastr.waiting('Selecciona una tienda antes del producto');
        clearProduct();
        siteDestino.select2('open');
        return
    }
    $product_button.prop("disabled", true);
    axios.get(`/TransferHeader/ActionValidateDestinationSite?PartNumber=${productId}&DestinationSite=${siteDestino}`)
        .then(r =>{
            if(!r.data.Status){
                toastr.error("Ocurrio un error inesperado.");
                clearProduct();
                $ItemsDrop.select2('open');
            }else{
                if (r.data.value == 1001) {
                    toastr.error("Este producto no se puede enviar a esta locación");
                    clearProduct();
                    $ItemsDrop.select2('open');
                } else if (r.data.value == 1002) {
                    toastr.warning("Producto NO DISPONIBLE para transferir fuera de esta sucursal. Contacte a su comprador.");
                    clearProduct();
                    $ItemsDrop.select2('open');
                } else if (r.data.value == 1003) {
                    toastr.error("Producto NO DISPONIBLE para transferir a la sucursal destino. Contacte a su comprador.");
                    clearProduct();
                    $ItemsDrop.select2('open');
                }
            }
        }).catch(e=>{
        console.log(e);
        toastr.error('Error inesperado contactar a sistemas.');
    }).then(()=>$product_button.prop("disabled", false));
    
}

function getStock() {
    if ($ItemsDrop.val() == "" || $ItemsDrop.val() == 0 || $ItemsDrop.val() == null) {
        return false;
    } else {
        $.ajax({
            url: "/Items/ActionGetItemStockSelectStock",
            type: "GET",
            data: { "PartNumber": $ItemsDrop.val() },
            success: function (returndata) {
                if (returndata.Status) {
                    $("#qty").val(returndata.value);
                    $quantity.focus();
                }
                else {
                    if (returndata.value == "SF") {
                        SessionFalse("Terminó su sesión");
                    } else {
                        $("#qty").val(0);
                        $quantity.focus();
                    }
                }
            },
            error: function () {
                toastr.remove();
                toastr.error('Error inesperado contactar a sistemas.');
            }
        });
    }
}

function clearProduct() {
    
    $quantity.val("");
    $qty.val("");
    $quantity.prop("disabled", true);
    $ItemsDrop.prop("disabled", false);
}

$('#table_products tbody').on('click', '#remove-button', function () {
    if (!enable) {
        return 0
    }
    var index = tableProducts.row($(this).parents('tr')).index();
    listAddProducts.splice(index, 1);
    listFinal.splice(index, 1);
    tableProducts.clear();
    tableProducts.rows.add($(listAddProducts));
    tableProducts.columns.adjust().draw();
    if (listAddProducts.length == 0 || listFinal.length == 0) {
        $("#btnSave").prop("disabled", true);
        $siteDestino.val("").trigger("change");
        $("#btnCancel").prop("disabled", true);
        $siteDestino.prop("disabled", false);
    }
});

function clear() {
    toastr.remove();
    $("#btnSave").prop("disabled", true);
    $("#btnCancel").prop("disabled", true);
    $siteDestino.prop("disabled", false);
    
    clearProduct();
    $ItemsDrop.select2("val", "");
    $ItemsDrop.prop("disabled", true);
    listAddProducts = [];
    listFinal = [];
    tableProducts.clear();
    tableProducts.columns.adjust().draw();
}

function RefreshItems() {
    swal({
        title: "¿Esta seguro que desea Cancelar la transferencia?",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "Continuar",
        cancelButtonText: "Cancelar"
    }, function (isConfirm) {
        if (isConfirm)
            clear();
    });
}

function TransfeMessage() {
    if ($siteDestino.val() == null || $siteDestino.val() == "" || $siteDestino.select2('data').text == "" || $siteDestino.select2('data').text == null) {
        toastr.warning('Seleccione una Tienda de destino.');
        $siteDestino.select2('open');
    } else {
        swal({
            title: "¿Esta seguro que desea transferir la mercancia a '" + toTitleCase($siteDestino.select2('data').text) + "'?",
            type: "info",
            showCancelButton: true,
            confirmButtonColor: "#55dd6b",
            confirmButtonText: "Continuar",
            cancelButtonText: "Cancelar"
        }, function (isConfirm) {
            if (isConfirm) { SaveTransfer(); }
        });
    }
}

function toTitleCase(str) {
    return str.replace(
        /\w\S*/g,
        function (txt) {
            return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();
        }
    );
}

function SaveTransfer() {
    StartLoading();
    $.ajax({
        type: "POST",
        url: "/TransferHeader/ActionSendItemsToDamageWeb",
        data: { "Items": listAddProducts, "SiteDestination": $siteDestino.val() },
        success: function (returndata) {
            EndLoading();
            clear();
            if (returndata.Data) {
                if (returndata.Data == "1") {
                    toastr.warning("Error inesperado contactar a sistemas. Error al momento de insertar el folio.");
                } else if (returndata.Data == "2") {
                    toastr.warning("Error inesperado contactar a sistemas. Error al insertar el Header.");
                } else if (returndata.Data == "SF") {
                    SessionFalse("Terminó su sesión.");
                } else if (returndata.Data == "0") {
                    enable = false;
                    swal({
                        title: "Transferencia capturada correctamente.",
                        type: "success",
                        closeOnConfirm: true,
                        showLoaderOnConfirm: true
                    });
                }
            }
            else {
                EndLoading();
                toastr.warning("Error inesperado contactar a sistemas");
            }
        },
        error: function () {
            EndLoading();
            toastr.error('Error inesperado contactar a sistemas');
        }
    });
}