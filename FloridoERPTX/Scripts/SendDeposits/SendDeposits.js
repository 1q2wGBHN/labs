﻿//Save Edit
function SaveDeposit() {
    if (ValidateType() && ValidateOrigin() && ValidateCurrency() && ValidateAmount()) {
        $('#ModalEditDeposit').modal('hide');        
        StartLoading();
        $.post({
            url: urlSaveEdit,
            Type: 'POST',
            dataType: 'json',
            data: $("#formEditDeposits").serialize(),
            success: function (data) {
                EndLoading();
                if (data.success) {                    
                    swal({
                        title: 'Editado',
                        text: "Envío Editado Correctamente",
                        type: "success"
                    }, function () { window.location.reload(true); });
                }
                else {
                    swal("Error", "Ocurrió un error, contacte a sistemas", "error");
                }
            }
        });
    }
}

//Add error class 
function AddErrorClass(element) {
    $(element).closest('.form-group').removeClass('has-success has-error').addClass('has-error');
    $(element).closest('.help-block').remove();
}

//Add Error message
function AddMessageError(element, message) {
    $(element).closest("form")
              .find("span[data-valmsg-for='" + element.attr("id") + "']")
              .html(message);
}

//Remove Error class and enable button
function RemoveError(element) {
    $("#SaveEdit").removeAttr("disabled");
    $(element).closest('.form-group').removeClass('has-success has-error');
    $(element).closest('.help-block').remove();
    $(element).closest("form")
              .find("span[data-valmsg-for='" + element.attr("id") + "']")
              .html('');
}

//Validate Type Field
function ValidateType() {
    var typeField = $("#Type");
    if (typeField[0].value == '') {
        AddErrorClass(typeField);
        AddMessageError(typeField, 'Este campo es requerido.');
        $("#SaveEdit").attr("disabled", "disabled");
    } else
        RemoveError(typeField);

    return true;
}

//Validate Origin Field
function ValidateOrigin() {
    var originField = $("#Origin");
    if (originField[0].value == '') {
        AddErrorClass(originField);
        AddMessageError(originField, 'Este campo es requerido.');
        $("#SaveEdit").attr("disabled", "disabled");
    } else
        RemoveError(originField);

    return true;
}

//Validate Currency Field
function ValidateCurrency() {
    var currencyField = $("#Currency");
    if (currencyField[0].value == '') {
        AddErrorClass(currencyField);
        AddMessageError(currencyField, 'Este campo es requerido.');
        $("#SaveEdit").attr("disabled", "disabled");
    } else
        RemoveError(currencyField);

    return true;
}

//Validate Amount Field
function ValidateAmount() {    
    var regex = /^(?:-?\d+|-?\d{1,3}(?:,\d{3})+)?(?:\.\d+)?$/;
    var amountField = $("#Amount");

    if (parseFloat(amountField[0].value) <= 0) {
        AddErrorClass(amountField);
        AddMessageError(amountField, 'Monto debe ser mayor a 0.');
        $("#SaveEdit").attr("disabled", "disabled");
        return false;
    }

    if (amountField[0].value == '') {
        AddErrorClass(amountField);
        AddMessageError(amountField, 'Este campo es requerido.');
        $("#SaveEdit").attr("disabled", "disabled");
        return false;
    }

    if (!regex.test(amountField[0].value)) {
        AddErrorClass(amountField);
        AddMessageError(amountField, 'Debe contener solo números');
        $("#SaveEdit").attr("disabled", "disabled");
        return false;
    }

    RemoveError(amountField);
    return true;
}

//Initialize table
var table = $('#sendDeposits-table').DataTable({
    oLanguage: {
        "sProcessing": "Procesando...", "sLengthMenu": "Mostrar _MENU_ registros", "sZeroRecords": "No se encontraron resultados", "sEmptyTable": "Ningún dato disponible en esta tabla", "sInfo": "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros", "sInfoEmpty": "Mostrando registros del 0 al 0 de un total de 0 registros", "sInfoFiltered": "(filtrado de un total de _MAX_ registros)", "sInfoPostFix": "", "sSearch": "Buscar:", "sUrl": "", "sInfoThousands": ",", "sLoadingRecords": "Cargando...", "oPaginate": { "sFirst": "Primero", "sLast": "Último", "sNext": "Siguiente", "sPrevious": "Anterior" }, "oAria": { "sSortAscending": ": Activar para ordenar la columna de manera ascendente", "sSortDescending": ": Activar para ordenar la columna de manera descendente" }
    },
    dom: "<'row'<'col-sm-4'l><'col-sm-4 text-left'B><'col-sm-4'f>t<'col-sm-6'i><'col-sm-6'p>>",
    lengthMenu: [[10, 25, 50, -1], [10, 25, 50, "Todos"]],
    buttons: [],
    columns: [
        { data: 'Id' },
        { data: 'Date' },
        { data: 'CUser' },
        { data: 'Type' },
        { data: 'Origin' },
        { data: 'Currency' },
        { data: 'Amount' },
        { data: 'Comments' },
        { data: 'CDate' }
    ],

    columnDefs: [
        {
            className: 'text-center'
        },
        {
            targets: [0],
            visible: false
        },
          {
              targets: [8],
              render: function (data, type, row) {
                  return moment(data).format("DD-MM-YYYY HH:mm:ss");;
              },
          },
          {
              targets: [6],
              type: 'numeric-comma',
              render: function (data, type, row) {
                  return '$' + data.toFixed(2);
              }              
          },
        {
            data: null,
            width: "20%",
            targets: 9,
            defaultContent: '<button class="btn btn-xs btn-outline btn-info EditDeposit">Editar</button> <button class="btn btn-xs btn-outline btn-danger DeleteDeposit">Borrar</button>'
        }
    ]
});

//Modal to edit deposit
$('#sendDeposits-table tbody').on('click', 'tr button.EditDeposit', function () {
    var tr = table.row($(this).parents('tr')).data();
    var DepositId = tr.Id;    
    
    $.ajax({
        url: urlModal,
        Type: 'GET',
        data: { "DepositId": DepositId },
        success: function (data) {
            $("#EditDeposit").html(data);
            $("#ModalEditDeposit").modal('show');
        }
    });
});


//Display swal for delete campaign
$('#sendDeposits-table tbody').on('click', 'tr button.DeleteDeposit', function () {
    var tr = table.row($(this).parents('tr')).data();
    var DepositId = tr.Id;

    swal({
        title: "¿Estas seguro?",
        text: "Una vez que se borre no podrás revertir el proceso",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "Sí",
        cancelButtonText: "No",
        closeOnConfirm: true,
        closeOnCancel: false
    },
    function (isConfirm) {
        if (isConfirm) {
            StartLoading();
            $.post({
                url: urlDeleteDeposit,
                Type: 'POST',
                dataType: 'json',
                data: { "DepositId": DepositId },
                success: function (data) {
                    EndLoading();
                    if (data.success) {
                        swal({
                            title: 'Borrado',
                            text: "Envío Borrado",
                            type: "success"
                        }, function () { $("#SearchDeposits").click(); });
                    }
                    else {
                        swal("Error", "Ocurrió un error, contacte a sistemas", "error");
                    }
                }
            });
        } else {
            swal("Error", "Envío no borrado", "error");
        }
    })
});

//Search Deposits by Date
$("#SearchDeposits").click(function () {
    var date = $("#Date").val();
    StartLoading();
    $.ajax({
        url: urlDeposits,
        Type: 'GET',
        data: { "Date": date },
        success: function (data) {
            EndLoading();
            if (data.success) {
                var Deposits = data.Json;               
                table.clear();
                table.rows.add(jQuery(Deposits));
                table.columns.adjust().draw();                
            }
        }
    });
});