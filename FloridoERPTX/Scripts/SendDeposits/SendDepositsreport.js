﻿//Initialize table
var table = $('#sendDeposits-table').DataTable({
    oLanguage: {
        "sProcessing": "Procesando...", "sLengthMenu": "Mostrar _MENU_ registros", "sZeroRecords": "No se encontraron resultados", "sEmptyTable": "Ningún dato disponible en esta tabla", "sInfo": "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros", "sInfoEmpty": "Mostrando registros del 0 al 0 de un total de 0 registros", "sInfoFiltered": "(filtrado de un total de _MAX_ registros)", "sInfoPostFix": "", "sSearch": "Buscar:", "sUrl": "", "sInfoThousands": ",", "sLoadingRecords": "Cargando...", "oPaginate": { "sFirst": "Primero", "sLast": "Último", "sNext": "Siguiente", "sPrevious": "Anterior" }, "oAria": { "sSortAscending": ": Activar para ordenar la columna de manera ascendente", "sSortDescending": ": Activar para ordenar la columna de manera descendente" }
    },
    dom: "<'row'<'col-sm-4'l><'col-sm-4 text-left'B><'col-sm-4'f>t<'col-sm-6'i><'col-sm-6'p>>",
    lengthMenu: [[10, 25, 50, -1], [10, 25, 50, "Todos"]],
    buttons: [],
    columns: [
       
        { data: 'Date' },
        { data: 'CUser' },
        { data: 'Type' },
        { data: 'Origin' },
        { data: 'Currency' },
        { data: 'Amount' },
        { data: 'Comments' },
        { data: 'CDate' }
    ],

    columnDefs: [
        {
            className: 'text-center'
        },       
        {
            targets: [7],
            render: function (data, type, row) {
                return moment(data).format("DD-MM-YYYY HH:mm:ss");;
            },
        },
        {
            targets: [5],
            type: 'numeric-comma',
            render: function (data, type, row) {
                return '$' + data.toFixed(2);
            }
        },
    ]
});

//Search Deposits by Date
$("#SearchDeposits").click(function () {
    var date = $("#Date").val();
    StartLoading();
    $.ajax({
        url: urlDeposits,
        Type: 'GET',
        data: { "Date": date },
        success: function (data) {
            EndLoading();
            if (data.success) {
                var Deposits = data.Json;
                if (Deposits.length > 0)
                    ReportButton.disabled = false;
                else
                    ReportButton.disabled = true;
                table.clear();
                table.rows.add(jQuery(Deposits));
                table.columns.adjust().draw();                
            }
        }
    });
});

$("#ReportButton").click(function () {
   
    var date = $("#Date").val();
            StartLoading();
            $.ajax({

                type: 'POST',
                url: urlReport,
                dataType: 'html',
                data: { 'Date': date },//{ "Model":  $("#formDeposits").serialize()},
                success: function (returndates) {
                    document.getElementById('iframe').srcdoc = returndates;
                    EndLoading();
                    $("#ModalDescription").html('Relación de Sanciones');
                    $('#ModalReport').modal('show');
                },
                error: function (returndates) {
                    EndLoading();
                    toastr.error('Alerta - Error inesperado  contactar a sistemas.');

                }

            });
});
