﻿var subTotal = 0;
var tax = 0;
var total = 0;
var ModelController; //This model includes pdf data;
var ItemList = [];

//Get Client Info from Combobox
$("#ProcessClient").click(function () {
    var clientValue = $("#ClientCode").val();
    if (clientValue) {
        $("#formQuoteClient").submit();
    }
});

//Update Client Information
$("#ClientNumber").keypress(function () {
    var numberCode = $("#ClientNumber").val();
    var keycode = (event.keyCode ? event.keyCode : event.which);
    if (keycode == '13') {
        $.ajax({
            url: urlClientInfo,
            data: { "ClientNumber": numberCode },
            dataType: 'json',
            success: function (data) {
                if (data.success) {
                    $("#ClientNumber").val(data.Json.Code);
                    $("#ClientRFC").val(data.Json.RFC);
                    $("#ClientName").val(data.Json.Name);
                    $("#Address").val(data.Json.Address);
                    $("#PhoneNumber").val(data.Json.PhoneNumber);
                    $("#Email").val(data.Json.Email);
                    $(".ClientName").html(data.Json.Name);
                    $(".Address").html(data.Json.Address);
                    $(".PhoneNumber").html(data.Json.PhoneNumber);
                    $(".FaxNumber").html(data.Json.FaxNumber);
                    $(".Email").html(data.Json.Email);
                }
                else {
                    toastr.error('Cliente ' + numberCode + ' no encontrado', 'Error');
                    $("#ClientNumber").val(numberCode);
                    $("#ClientRFC").val('');
                    $("#ClientName").val('');
                    $("#Address").val('');
                    $("#PhoneNumber").val('');
                    $("#Email").val('');
                    $(".ClientName").html('');
                    $(".Address").html('');
                    $(".PhoneNumber").html('');
                    $(".FaxNumber").html('');
                    $(".Email").html('');
                }
            }
        });
    }
});

//Avoid user to use up and down keys to increase or decrease number on inputs.
$("input[type=number]").on("focus", function () {
    $(this).on("keydown", function (event) {
        if (event.keyCode === 38 || event.keyCode === 40) {
            event.preventDefault();
        }
    });
});

//Display Item Information based on Item Number or Description
$("#SearchItem").click(function () {
    var item = $("#Item").val();
    if (item != '') {
        $.ajax({
            url: urlItems,
            Type: 'GET',
            data: { "Item": item },
            success: function (data) {
                if (data.success) {
                    $("#TableSection").show();
                    var Articles = data.Json;
                    arcticlesTable.clear();
                    arcticlesTable.rows.add(jQuery(Articles));
                    arcticlesTable.columns.adjust().draw();
                }
                else
                    toastr.error('No se encontraron artículos', 'Error');
            }
        });

        $("#Item").val('');
    }
    else {
        $("#TableSection").hide();
        toastr.error('Escribe el nombre o número de articulo a buscar', 'Error');
    }
});

//Enable 'Enter' key on search Item to make a search
$("#Item").keypress(function () {
    var keycode = (event.keyCode ? event.keyCode : event.which);
    if (keycode == '13') {
        $("#SearchItem").click();
    }
});

//Initialize Table where it´s going to display Items information
var arcticlesTable = $('#articlesTable').DataTable({
    createdRow: function (row, data, dataIndex) {
        if (data['IsPackage'] == true) {
            $(row).css('background-color', 'rgb(227, 241, 116)');
        }
    },
    oLanguage: {
        "sProcessing": "Procesando...", "sLengthMenu": "Mostrar _MENU_ registros", "sZeroRecords": "No se encontraron resultados", "sEmptyTable": "Ningún dato disponible en esta tabla", "sInfo": "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros", "sInfoEmpty": "Mostrando registros del 0 al 0 de un total de 0 registros", "sInfoFiltered": "(filtrado de un total de _MAX_ registros)", "sInfoPostFix": "", "sSearch": "Buscar:", "sUrl": "", "sInfoThousands": ",", "sLoadingRecords": "Cargando...", "oPaginate": { "sFirst": "Primero", "sLast": "Último", "sNext": "Siguiente", "sPrevious": "Anterior" }, "oAria": { "sSortAscending": ": Activar para ordenar la columna de manera ascendente", "sSortDescending": ": Activar para ordenar la columna de manera descendente" }
    },
    dom: "<'row'<'col-sm-4'l><'col-sm-4 text-left'B><'col-sm-4'f>t<'col-sm-6'i><'col-sm-6'p>>",
    lengthMenu: [[10, 25, 50, -1], [10, 25, 50, "Todos"]],
    buttons: [],
    columns: [
        { data: 'UnitMeasure' },
        { data: 'ItemTaxPercentage' },
        { data: 'ItemTax' },
        { data: 'ItemNumber' },
        { data: 'ItemDescription' },
        { data: 'Stock' },
        { data: 'BasePrice' },
        { data: 'QuotePrice' },
        { data: 'Quantity' },
        { data: 'IsPackage' },
        { data: 'PresentationCode' },
        { data: 'PresentationCodeText' }

    ],

    columnDefs: [
        {
            className: 'text-center'
        },
        {
            targets: [0, 1, 2, 9, 10, 11],
            visible: false
        },
        {
            targets: [3],
            render: function (data, type, full, row) {
                if (!full.IsPackage)
                    return data;
                else
                    return full.PresentationCodeText;
            }
        },
        {
            targets: [7],
            width: "5%",
            render: function (data, type, full, row) {
                if (full.IsPackage)
                    return '<input class="QuotePrice" type="number"  value = ' + data.toFixed(2) + '  readonly>';
                else
                    return '<input class="QuotePrice" type="number"  value = ' + data.toFixed(2) + '  >';
            }
        },
        {
            targets: [8],
            width: "5%",
            render: function (data, type, full, row) {
                if (full.IsPackage)
                    return '<input class="Quantity" type="number"  value = ' + data.toFixed(2) + '  readonly>';
                else
                    return '<input class="Quantity" type="number"  value = ' + data.toFixed(2) + '  >';
            },

        },
        {
            data: null,
            width: "5%",
            targets: 12,
            defaultContent: '<button class="btn btn-xs btn-outline btn-success AddItem">Agregar</button>'
        }
    ]
});

//Initialize Quote Table
var quoteTable = $('#quoteTable').DataTable({
    oLanguage: {
        "sProcessing": "Procesando...", "sLengthMenu": "Mostrar _MENU_ registros", "sZeroRecords": "No se encontraron resultados", "sEmptyTable": "Ningún dato disponible en esta tabla", "sInfo": "Mostrando registros", "sInfoEmpty": "Mostrando registros del 0 al 0 de un total de 0 registros", "sInfoFiltered": "(filtrado de un total de _MAX_ registros)", "sInfoPostFix": "", "sSearch": "Buscar:", "sUrl": "", "sInfoThousands": ",", "sLoadingRecords": "Cargando...", "oPaginate": { "sFirst": "Primero", "sLast": "Último", "sNext": "Siguiente", "sPrevious": "Anterior" }, "oAria": { "sSortAscending": ": Activar para ordenar la columna de manera ascendente", "sSortDescending": ": Activar para ordenar la columna de manera descendente" }
    },
    dom: "<'row'<'col-sm-4'l><'col-sm-4 text-left'B><'col-sm-4'f>t<'col-sm-6'i><'col-sm-6'p>>",
    lengthMenu: [[10, 25, 50, -1], [10, 25, 50, "Todos"]],
    buttons: [],
    columns: [
        { data: 'ItemTaxPercentage' },
        { data: 'ItemTax' },
        { data: 'Quantity' },
        { data: 'Stock' },
        { data: 'ItemNumber' },
        { data: 'UnitMeasure' },
        { data: 'ItemDescription' },
        { data: 'BasePrice' },
        { data: 'QuotePrice' },
    ],

    columnDefs: [
        {
            className: 'text-center'
        },
        {
            targets: [0, 1],
            visible: false
        },
        {
            data: null,
            width: "5%",
            targets: 9,
            defaultContent: '<span class="input-group-addon" style="border-style:none;"><a class="deleteItemQuote"><i class="fa fa-trash"></i></a></span>'
        }
    ]
});

//Add Item to Quote Table
$('#articlesTable tbody').on('click', 'tr button.AddItem', function () {
    var decimalRegex = /^(?=.?\d)\d*(\.\d{0,4})?$/;
    var tr = arcticlesTable.row($(this).parents('tr')).data();
    var quantity;
    var quotePrice;
    var data = quoteTable.rows().data();

    if (data.length > 0) {
        for (i = 0; i < data.length; i++) {
            if (data[i].ItemNumber == tr.ItemNumber && !data[i].IsPackage) {
                toastr.error('Artículo ya agregado a la cotización', 'Error');
                return false;
            }
        }
    }

    $(this).closest('tr').find('input').each(function () {
        if ($(this).attr("class") == "Quantity") quantity = $(this).val();
        if ($(this).attr("class") == "QuotePrice") quotePrice = $(this).val();
    });

    if (quantity == '') {
        toastr.error('Campo Cantidad no puede estar vacío', 'Error');
        return false;
    }

    if (!decimalRegex.test(quantity)) {
        toastr.error('Campo Cantidad tiene valor erróneo', 'Error');
        return false;
    }

    if (parseFloat(quantity) == 0) {
        toastr.error('Campo Cantidad debe ser mayor a 0', 'Error');
        return false;
    }

    if (quotePrice == '') {
        toastr.error('Campo Precio no puede estar vacío', 'Error');
        return false;
    }

    if (!decimalRegex.test(quotePrice)) {
        toastr.error('Campo Precio tiene valor erróneo', 'Error');
        return false;
    }

    if (parseFloat(quotePrice) == 0) {
        toastr.error('Campo Precio debe ser mayor a 0', 'Error');
        return false;
    }

    //Calculate new tax if base price and quot price are not the same.    
    if (quotePrice != parseFloat(tr.BasePrice)) {
        tr.ItemTax = quotePrice * (tr.ItemTaxPercentage / 100);
    }

    tr.Quantity = quantity;
    tr.BasePrice = quotePrice;
    tr.QuotePrice = quotePrice * quantity;
    tr.QuotePrice = tr.QuotePrice.toFixed(2);

    $("#TableQuoteSection").show();
    $("#ProcessQuote").show();

    var list = GetItemsQuote();
    if (tr.IsPackage && list.length > 0)
        UpdateRows(tr);
    else {
        quoteTable.rows.add(jQuery(tr));
        quoteTable.columns.adjust().draw();
    }

    subTotal += parseFloat(tr.QuotePrice);
    tax += (parseFloat(tr.ItemTax) * parseFloat(tr.Quantity));
    total += parseFloat(tr.QuotePrice) + (parseFloat(tr.ItemTax) * parseFloat(tr.Quantity));

    $('.Subtotal').html('$' + parseFloat(subTotal).toFixed(2));
    $('.Iva').html('$' + parseFloat(tax).toFixed(2));
    $('.Total').html('$' + parseFloat(total).toFixed(2));
});

//Delete Row from Quote Table
$("#quoteTable").on('click', '.deleteItemQuote', function () {
    var tr = quoteTable.row($(this).parents('tr')).data();
    var zero = 0;
    subTotal -= parseFloat(tr.QuotePrice);
    tax -= (parseFloat(tr.ItemTax) * parseFloat(tr.Quantity));
    total -= ((parseFloat(tr.ItemTax) * parseFloat(tr.Quantity)) + parseFloat(tr.QuotePrice));

    $('.Subtotal').html('$' + subTotal.toFixed(2));
    $('.Iva').html('$' + tax.toFixed(2));
    $('.Total').html('$' + total.toFixed(2));

    $(this).closest('tr').remove();

    var rowCount = document.getElementById('quoteTable').rows.length;
    if (rowCount == 4) { //4 is the number of rows (1 from header, 3 from footer) when table body has no data
        quoteTable.clear();       
        $('.Subtotal').html('$' + zero);
        $('.Iva').html('$' + zero);
        $('.Total').html('$' + zero);
        subTotal = zero;
        tax = zero;
        total = zero;
        $("#TableQuoteSection").hide();
        $("#ProcessQuote").hide();
    }
});

//Call controller to save information quote
$("#ProcessQuote").click(function () {
    var clientInfo = $("#ClientRFC").val();
    var model = MappingModel();
    if (clientInfo != '') {
        StartLoading();
        $.post({
            url: urlProcessQuote,
            Type: 'POST',
            data: { "Model": model },
            success: function (data) {
                EndLoading();
                if (data.success) {
                    var pdf = 'data:application/pdf;base64,' + data.Json.PdfBase64;
                    $("#Pdf-QuoteiFrame").attr("data", pdf);
                    $('#ModalPdf').modal('show');
                    ModelController = data.Json;
                }
                else {
                    swal({
                        title: 'Error',
                        text: "Ocurrio un error, contacte a sistemas para solucionarlo.",
                        type: "error"
                    });
                }
            }
        });
    }
    else
        toastr.error('Selecciona un cliente', 'Error');
});

//Mapping Model to send to the controller
function MappingModel() {
    var Model = {};
    Model.ClientNumber = document.getElementById('ClientNumber').value;
    Model.ClientCode = document.getElementById('ClientCode').value;
    Model.QuoteSerie = document.getElementById('QuoteSerie').value;
    Model.QuoteNo = document.getElementById('QuoteNo').value;
    Model.SiteCode = document.getElementById('SiteCode').value;
    Model.ClientName = document.getElementById('ClientName').value;
    Model.Address = document.getElementById('Address').value;
    Model.PhoneNumber = document.getElementById('PhoneNumber').value;
    Model.Email = document.getElementById('Email').value;
    Model.Seller = document.getElementById('Seller').value;
    Model.SellerEmail = document.getElementById('SellerEmail').value;
    Model.SubTotal = subTotal;
    Model.Total = total;
    Model.Tax = tax;
    Model.ItemsList = GetItemsQuote();

    return Model;
}

//Get data from quote table
function GetItemsQuote() {
    var ItemList = [];
    var data = quoteTable.rows().data();

    for (i = 0; i < data.length; i++) {
        ItemList.push(data[i]);
    }

    return ItemList;
}

//Reload Page when close modal
$("#ModalPdf").on("hidden.bs.modal", function () {
    window.location.href = urlLoadingPage;
});

$("#SendPdf").click(function () {
    var model = MappingModel();
    ModelController.PdfBase64 = '';
    ModelController = model;
    if (model.Email === '' || 0 === model.Email.length || model.Email.trim() || /^\s*$/.test(model.Email)) {
        swal({
            title: "Cliente sin correo",
            text: "¿El cliente no tiene correo asignado, ¿quieres qué se envíe la cotización a tu correo: " + model.SellerEmail + "?",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Sí",
            cancelButtonText: "No",
            closeOnConfirm: true,
            closeOnCancel: true
        },
            function (isConfirm) {
                if (isConfirm) {
                    SendPdf(true, ModelController);
                    return;
                }
                else {
                    swal("Correo no enviado.", "Si gusta puede guardar la cotización en su computadora local", "warning");
                }
            })
    }
});

function SendPdf(sendToSeller, ModelController) {
    StartLoading();
    $.post({
        url: urlSendMail,
        Type: 'POST',
        data: { "Model": ModelController, "SendToSeller": sendToSeller },
        success: function (data) {
            EndLoading();
            if (data.success) {
                swal({
                    title: 'Correo',
                    text: "Correo enviado exitosamente.",
                    type: "success"
                },
                function () {
                    window.location.href = urlLoadingPage;
                });
            }
            else {
                swal({
                    title: 'Error',
                    text: "Ocurrio un error, contacte a sistemas para solucionarlo.",
                    type: "error"
                });
            }
        }
    });

    return;
}

//Populate Combobox
function PopulateCombobox(CustomersList) {
    $("#ClientCode").empty();
    $("#ClientCode").append("<option value=''>--Selecciona Cliente--</option>");
    $.each(CustomersList, function (index, optiondata) {
        $("#ClientCode").append("<option value='" + optiondata.Code + "'>" + optiondata.RFC + ", " + optiondata.Name + ", " + optiondata.Code + "</option>");
    });
}

function UpdateRows(tr) {    
    var listFirst = quoteTable.rows().data(); 
    var list = quoteTable.rows().data(); 
    console.log(listFirst);
    var qty = 0;
    for (i = 0; i < list.length; i++) {
        if (list[i].ItemNumber == tr.ItemNumber) {
            
            qty = parseFloat(list[i].Quantity);
            var newQty = qty + parseFloat(tr.Quantity);
            list[i].Quantity = newQty;

            quoteTable.rows(i).data = list[i];
            quoteTable.cell(i, 2).data(newQty.toFixed(2));
            quoteTable.cell(i, 2).data(newQty.toFixed(2)).draw(false);
            //quoteTable.dataTable().fnUpdate(newQty, i, undefined, false);                        
        }
    }
    //quoteTable.clear();
    //quoteTable.rows.add(jQuery(list));
    //quoteTable.columns.adjust().draw();
    var list2 = quoteTable.rows().data();   
    console.log(list);
    console.log(list2);
}