import {fomartmoney} from './generic_equations.js'
import * as tablesOption from './generic_elements_table.js'

/* cuando quieran quitar jquery  */
function replaceAll( text, busca, reemplaza ){
    while (text.toString().indexOf(busca) != -1)
        text = text.toString().replace(busca,reemplaza);
    return text;
}

/*agrega el formato de moneda validado*/
function ListFormatMoney(list,type,array,decimal) {
    //const fomartmoney = money => new Intl.NumberFormat("en-US", { style: "currency", currency: "USD", currencyDisplay: "symbol", maximumFractionDigits: 4 }).format(money)
    const typemony = modification => components => element => dataArray => decimal=> dataArray[components] = modification.filter(elementsModificaction => elementsModificaction == components).length >0 ? fomartmoney(element)(decimal): element;
    const listFormat = arrayComponents => modification => arrayElements => newElementsArray => decimal => { arrayComponents.map(Components =>  newElementsArray[Components]=typemony(modification)(Components)(arrayElements[Components])({})(decimal)); return newElementsArray};
    return list.map(elements => listFormat(array)(type)(elements)({})(decimal))
}

/* genera un select basico con un solo nombre */
function selectGeneric(id,list,array,firstOption) {
    const selectOption = selectId => selectName => `<option value="${selectId}">${selectName}</option>`;
    const selectList = arrayOption => elements => selectOption(elements[arrayOption[0]])(elements[arrayOption[1]]);
    $(id).html([`${selectOption(firstOption.id)(firstOption.text)}`, ...list.map(elements => selectList(array)(elements))])
    //return list.map(elements => selectList(array)(elements))
}

/* genera una tabla basica sin inputs o ids */
function tableGeneric(id,list,array){
    // const textTable = text => array => elements => {array.map(component => text += tablesOption.tableTd(elements[component])); return text}
    // const tableTr = array => elements => text => `<tr>${text =textTable(text)(array)(elements) }</tr>`
    const tableTr = array => elements => `<tr>${array.map(component => tablesOption.tableTd(elements[component]))}</tr>`
    $(id).html(list.map(elements => tableTr(array)(elements)));
    //return list.map(elements => tableTr(array)(elements));
}

/* genera un los th para los titulos*/
function tableThGeneric(id, list) {
    $(id).html(`<tr>${list.map(text => tablesOption.tableTh(text))}</tr>`)
}

/* genera una tabla con 2 datos */
function tableGenericError(id, list, array, compare) {
    const isDiferent = isTrue => elements => array => {
        if (isTrue)
           return `${array.map(component => tablesOption.tableTd(elements[component]))}`
        else
            return `${array.map(component => tablesOption.tableTdError(elements[component]))}`
    };
    const tableTr = array => elements => compare => `<tr>${isDiferent((elements[compare[0]] == elements[compare[1]] ? true : false))(elements)(array)}</tr >`
    const html = list.map(elements => tableTr(array)(elements)(compare));
    $(id).html(html.length > 0 ? html : `<tr><td colspan="${array.length}" style="text-align: center;">No hay productos</td><tr>`);
}

/* Puedes mandar multiples parametros para que aparescan en el texto */
function multiSelect(id, list, value, array, valueRemplase, firstOption) {
    const selectOption = selectId => selectName => `<option value="${selectId}">${selectName}</option>`;
    const selectList = arrayOption => elements => idSelect => valueRemplase => selectOption(elements[idSelect[0]])( replaceAll(arrayOption.map(text =>  elements[text]),",",valueRemplase)); 
    $(id).html([`${selectOption(firstOption.id)(firstOption.text)}`,... list.map(elements => selectList(array)(elements)(value)(valueRemplase))])
    //return list.map(elements => selectList(array)(elements)(value)(valueRemplase));
}

/* Esta funcion genera multi datos para las tablas */
function multiOptionTable(id,list,type,array,arrayLengt){
    //inicia la seleccion de las opciones de la tabla
    const tableOptioon = array => elements => position => type => arrayLengt  =>{ 
        var positionTd = 0;  
        return `<tr> ${array.map(nameElement => validation(elements[nameElement])(type)(positionTd++)(position)(arrayLengt))}</tr>`
    };
    //se realizan las validaciones
    const validation = arrayElement => type => position => posiciontr=>arrayLengt =>{  
        const isExist = type.filter(element => element.position == position).length>0;
        if(arrayLengt-1 == position){
            if(isExist)
            {
                var nose = type.filter(element => element.position == position).map(element =>  typemodelOption(element)(posiciontr))+tablesOption.tableTd(arrayElement) ;
                return  nose +  type.filter(element => element.type == "total").map(element => tablesOption.tableTdTotal(element.id+posiciontr)(0))
            }
            else{
                return tablesOption.tableTd(arrayElement) +type.filter(element => element.position == null).map(element=>  tablesOption.tableTdTotal(element.id+posiciontr)(0))
            }
        }
        if(isExist)
        {
            return type.filter(element => element.position == position).map(element => typemodelOption(element)(posiciontr)) +  tablesOption.tableTd(arrayElement)
        }       
        return  tablesOption.tableTd(arrayElement);
    };
    //se seleccione el tipo de td que se utilizara
    const typemodelOption = element => position => {
            if( element.type == "check"){
                return tablesOption.tableTdCheck(element.id+position)
            }
            if( element.type == "inputN"){
                return tablesOption.tableTdInputNumber(element.id+position)
            }
            if( element.type == "inputNF"){
                return tablesOption.tableTdInputNumberFuction(element.id+position)(element.id)
            }
            if( element.type == "checkF"){
                return tablesOption.tableTdCheckFuction(element.id+position)(element.id)
            }
    }
    var position = 0;
    $(id).html(list.map(elements => tableOptioon(array)(elements)(position++)(type)(arrayLengt)))
    return list.map(elements => tableOptioon(array)(elements)(position++)(type)(arrayLengt))
}

/* es un buscador generico que busca los datos de toda la tabla */
function Search(id,table) {
    var value = document.querySelector(id).value.toLowerCase()
    $(table+ " tr").filter(function () {
        $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
    });
}
/* table double pendiente */
//function doubleTable(id, list,type,array,options,rows,css){
//    const countRow = 0;
//    const optionsRow = optionsTable=>{
//        if(optionsTable.type == "Button"){

//        }
//    }
//    const tableTrSecundari =  array => elements => `<tr>${array.map(component => tablesOption.tableTd(elements[component]))}</tr>`;
//    const tableTr = array => elements =>css => `<tr>${tablesOption.tableTdClass(css)("")}${array.map(component => tablesOption.tableTd(elements[component]))}</tr>`;
//    const tableGenerate = css => element => oterList => array => type=> optionsTable => options => rows => countRow=>`
//    <tr>${tableTr(array)(element)(css)}</tr>
//    <tr hidden class="${css}" id="${countRow}"><td colspan="${rows}">
//    <div> 
//        <table>
//            ${tableTrSecundari(oterList,options)}
//        </table>
//        ${optionsTable.map(components => optionsRow(components))}
//    <div>
//    </td></tr>`
//}

export { ListFormatMoney, selectGeneric, tableGeneric, tableGenericError, multiSelect, multiOptionTable, Search, tableThGeneric}