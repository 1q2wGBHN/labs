﻿function ValidateLetters(fieldValue, fieldName) {
    const letterRegex = /^[a-zA-ZáÁéÉíÍóÓúÚñÑüÜ\s]+$/;

    if (!letterRegex.test(fieldValue)) {

        toastr.error("Debe contener solo letras.");
        document.getElementById(fieldName).value = '';
        document.getElementById(fieldName).focus();
    }
    return true;
}

function ValidateNumbers(fieldValue, fieldName) {
    const numberRegex = /^([0-9])*$/;

    if (!numberRegex.test(fieldValue)) {

        toastr.error("Debe contener solo números.");
        document.getElementById(fieldName).value = '';
        document.getElementById(fieldName).focus();
    }
    return true;
}

function ValidateDates(StartDate, EndDate) {
    var dateEnd = stringToDate(EndDate);
    var dateStart = stringToDate(StartDate);

    if (dateEnd < dateStart) {        
        toastr.error('Fecha Final no puede ser menor a Inicial.');
        return false;
    }

    return true;
}