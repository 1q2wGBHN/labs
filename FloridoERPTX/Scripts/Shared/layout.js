﻿$(document).ready(function () {
    window.setTimeout(function () {
        $(".normalheader").addClass("small-header");
        $("#fa").removeClass("fa-arrow-up").addClass("fa-arrow-down");
        $("#hbreadcrumb").removeClass("m-t-lg");
    }, 5000);

    $('.select2').select2();

    $("input").attr("autocomplete", "off");
});

function SessionFalse(data) {
    swal({
        title: data + " No se realizó ninguna acción.",
        type: "warning",
        showCancelButton: false,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "Iniciar Sesión"
    }, function (isConfirm) {
        if (isConfirm)
            location.reload();
    });
}

//Convert strings formats dd/mm/yyyy to date
function stringToDate(dateStr) {
    var parts = dateStr.split("/")
    return new Date(parts[2], parts[1] - 1, parts[0])
}

//Function to Get the Current Date in format dd/mm/yyyy
function GetCurrentDate() {
    var today = new Date();
    var dd = today.getDate();
    var mm = today.getMonth() + 1; //January is 0!
    var yyyy = today.getFullYear();

    if (dd < 10) {
        dd = '0' + dd;
    }

    if (mm < 10) {
        mm = '0' + mm;
    }

    today = dd + '/' + mm + '/' + yyyy;

    return today;
}

var pages = [];
$.ajax({
    url: '/Home/ActionGetPagesByUser/',
    type: "GET",
    data: {},
    success: function (returndata) {
        if (returndata == "SF")
            SessionFalse("Su sesión a terminado.")
        else
            pages = returndata.pages;
    },
    error: function () {
        toastr.error('Error inesperado contactar a sistemas.');
    }
});

$("#searchingMainNoRepeat").on('keyup', function () {
    inputValueSearching = $("#searchingMainNoRepeat").val();
    if (inputValueSearching.length > 2) {
        $('#search_containerNoRepeat').html('');
        $.each(pages, function (index, value) {
            var name = RemoveAccentsSearching(value.page_name.toLowerCase());
            var description = RemoveAccentsSearching(value.description.toLowerCase());
            if (name.indexOf(RemoveAccentsSearching(inputValueSearching.toLowerCase())) >= 0 || description.indexOf(RemoveAccentsSearching(inputValueSearching.toLowerCase())) >= 0) {
                $('#search_containerNoRepeat').append(`
                            <a href="${value.url}" onclick="RedirectPageSearching();" class="list-group-item mx-0" style="background-color: white;">
                                <strong>${value.page_name}</strong> <span>${value.description}</span>
                            </a>`);
            }
        });
    } else {
        $('#search_containerNoRepeat').html('');
    }
});

$('#searchingMainNoRepeat').blur(function () {
    setTimeout(function () { $('#search_containerNoRepeat').html(''); }, 200);
});

//Remover acentos
function RemoveAccentsSearching(strAccents) {
    var strAccents = strAccents.split('');
    var strAccentsOut = new Array();
    var strAccentsLen = strAccents.length;
    var accents = 'ÀÁÂÃÄÅàáâãäåÒÓÔÕÕÖØòóôõöøÈÉÊËèéêëðÇçÐÌÍÎÏìíîïÙÚÛÜùúûüÑñŠšŸÿýŽž';
    var accentsOut = "AAAAAAaaaaaaOOOOOOOooooooEEEEeeeeeCcDIIIIiiiiUUUUuuuuNnSsYyyZz";
    for (var y = 0; y < strAccentsLen; y++) {
        if (accents.indexOf(strAccents[y]) != -1) {
            strAccentsOut[y] = accentsOut.substr(accents.indexOf(strAccents[y]), 1);
        } else
            strAccentsOut[y] = strAccents[y];
    }
    strAccentsOut = strAccentsOut.join('');
    return strAccentsOut;
}

function RedirectPageSearching() {
    StartLoading();
}