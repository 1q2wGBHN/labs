﻿
$(document).ready(function () {
    $("#EmployeeNumber").focus();
    $("#FormInsert").validate({
        rules: {
            EmpNumber: {
                required: true,
                number: true,
                digits: true,
                minlength: 2,
                maxlength: 10
            }
        },
        messages: {
            EmpNumber: {
                required: "El Campo no puede ser nulo",
                number: "Solo se permiten numeros.",
                minlength: "Favor de ingresar mas de 2 caracteres"

            }
        }
    });
});

function SendEmailToGetCode() {
    if ($('#EmailORuser').val()) {
        StartLoading();
        $.ajax({
            type: "POST",
            dataType: "json",
            url: "/Home/GetCodeRecoveryPassword/",
            data: { "employeeUsername": $('#EmailORuser').val(), "type": false },
            success: function (returndate) {

                if (returndate.success == true) {
                    toastr.success('' + returndate.responseText + '');
                    $('#EmailORuser').val(null);
                }
                else {
                    toastr.warning('' + returndate.responseText + '');
                }
            },
            error: function (returndate) {
                toastr.error('Error Inesperado');
            }
        });
        EndLoading();
        return false;
    }
}
