﻿document.addEventListener("DOMContentLoaded", function (event) {
    window.setTimeout(function () { document.getElementById("fa").click(); }, 1000);

    //var model = JSON.parse(document.getElementById("model").value);
    //table.clear();
    //table.rows.add(model);
    //table.columns.adjust().draw();
});

var amount = 0;
var iva = 0;
var ieps = 0;
var total = 0;
const formatmoney = money => decimal => new Intl.NumberFormat("en-US", { style: "currency", currency: "USD", currencyDisplay: "symbol", maximumFractionDigits: decimal }).format(money);

$("#BeginDate").datepicker({
    autoclose: true,
    todayHighlight: true,
    showButtonPanel: true,
});

$("#EndDate").datepicker({
    autoclose: true,
    startDate: '01/01/1950',
    todayHighlight: true,
});

$("#ItemSelect").select2({
    minimumInputLength: 3,
    allowClear: true,
    placeholder: "Seleccione una producto",
    initSelection: function (element, callback) {
        callback({ id: "", text: "Seleccione una opcion" });
    },
    ajax: {
        url: "/Items/ActionSearchItem/",
        dataType: 'json',
        type: "GET",
        quietMillis: 50,
        data: function (Filter) {
            return {
                Filter: Filter
            };
        },
        results: function (data) {
            return {
                results: $.map(data.Json, function (item) {
                    return {
                        text: item.Description,
                        id: item.PathNumber
                    }
                }),
            };
        }
    }
});

var table = $('#Codes').DataTable({
    oLanguage: {
        "sProcessing": "Procesando...", "sLengthMenu": "Mostrar _MENU_ registros", "sZeroRecords": "No se encontraron resultados", "sEmptyTable": "Ningún dato disponible en esta tabla", "sInfo": "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros", "sInfoEmpty": "Mostrando registros del 0 al 0 de un total de 0 registros", "sInfoFiltered": "(filtrado de un total de _MAX_ registros)", "sInfoPostFix": "", "sSearch": "Buscar:", "sUrl": "", "sInfoThousands": ",", "sLoadingRecords": "Cargando...", "oPaginate": { "sFirst": "Primero", "sLast": "Último", "sNext": "Siguiente", "sPrevious": "Anterior" }, "oAria": { "sSortAscending": ": Activar para ordenar la columna de manera ascendente", "sSortDescending": ": Activar para ordenar la columna de manera descendente" }
    },
    dom: "<'row'<'col-sm-4'l><'col-sm-4 text-left'B><'col-sm-4'f>t<'col-sm-6'i><'col-sm-6'p>>",
    responsive: true,
    autoWidth: true,
    order: [[0, "desc"]],
    buttons: [
        { extend: 'copy', text: 'Copiar', className: 'btn-sm' },
        { extend: 'csv', text: 'Excel', title: 'Articulos de Venta Cajero', className: 'btn-sm' },
        {
            extend: 'pdf', text: 'PDF', title: 'Articulos de Venta Cajero', className: 'btn-sm', exportOptions: {
                columns: [0, 1, 2, 3, 4, 5, 6, 7]
            },
            customize: function (doc) {
                doc.content[1].table.widths =
                    Array(doc.content[1].table.body[0].length + 1).join('*').split('');
            }
        },
        { extend: 'print', text: 'Imprimir', className: 'btn-sm' }
    ],
    columns:
        [
            { targets: 0, data: 'PartNumber', width: "1%" },
            { targets: 1, data: 'PartDescription', width: "1%" },
            { targets: 2, data: 'Cashier', width: "1%" },
            { targets: 3, data: 'Quantity', width: "1%" },
            { targets: 4, data: 'Amount', width: "1%" },
            { targets: 5, data: 'IVA', width: "1%" },
            { targets: 6, data: 'IEPS', width: "1%" },
            { targets: 7, data: 'Total', width: "1%" }
        ],
    columnDefs: [
        {
            targets: [4, 7],
            type: "numeric-comma",
            render: function (data, type, row) {
                return '$' + data.toFixed(4);
            }
        },
        {
            targets: [5, 6],
            type: "numeric-comma",
            render: function (data, type, row) {
                return '$' + data.toFixed(2);
            }
        }]
});

var detailRows = [];
$("#Codes").append('<tfoot><tr><th></th><th colspan="3">Totales</th><th id="amount">$0</th><th id="iva">$0</th><th id="ieps">$0</th><th id="total">$0</th></tr></tfoot>');

$('#Codes tbody').on('click', 'tr td.details-control', function () {
    var tr = $(this).closest('tr');
    var row = table.row(tr);

    if (row.child.isShown()) {
        row.child.hide();
        tr.removeClass('details');
    }
    else {
        if (table.row('.details').length) {
            $('.details-control', table.row('.details').node()).click();
        }
        row.child(format(row.data())).show();
        tr.addClass('details');
    }
});

table.on('draw', function () {
    $.each(detailRows, function (i, id) {
        $('#' + id + ' td.details-control').trigger('click');
    });
});

var list = [];
function format(d) {
    list = [];
    var detailItems = "";
    var tabledetail = $('<div/>').addClass('loading').text('Cargando Datos...');
    $.ajax({
        url: "/Purchases/ActionGetCodePackages",
        data: { "OriginPartNumber": d.OriginPartNumber },
        type: "POST",
        success: function (response) {
            if (response.result == "SF")
                SessionFalse("Terminó tu sesión");
            else if (response.result) {
                response.model.forEach(function (item) {
                    detailItems += "<tr><td>" + item.PartNumber + "</td><td>" + item.PartDescription + "</td><td>" + item.Quantity + "</td><td>$" + item.Cost + "</td><td>$" + (item.Quantity * item.Cost).toFixed(2) + "</td>";
                });

                if (response.model.length > 0) {
                    tabledetail.html('<table id="tabledetail" class="table table-striped table-bordered table-hover" style="width:100%"><thead><tr><th>Código</th><th>Descripción</th><th>Cantidad</th><th>Costo Unitario</th><th>Total</th></tr></thead><tbody>' + detailItems + '</tbody></table>').removeClass('loading');
                    reloadStyleTable();
                }

                tabledetail.html('<table id="tabledetail" class="table table-striped table-bordered table-hover" style="width:100%"><thead><tr><th>Código</th><th>Descripción</th><th>Cantidad</th><th>Costo Unitario</th><th>Total</th></tr></thead><tbody>' + detailItems + '</tbody></table>').removeClass('loading');
                reloadStyleTable();
            }
        }
    });
    return tabledetail;
}

function reloadStyleTable() {
    $('#tabledetail').DataTable({
        order: [[0, "desc"]],
        oLanguage: {
            "sProcessing": "Procesando...", "sLengthMenu": "Mostrar _MENU_ registros", "sZeroRecords": "No se encontraron resultados", "sEmptyTable": "Ningún dato disponible en esta tabla", "sInfo": "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros", "sInfoEmpty": "Mostrando registros del 0 al 0 de un total de 0 registros", "sInfoFiltered": "(filtrado de un total de _MAX_ registros)", "sInfoPostFix": "", "sSearch": "Buscar:", "sUrl": "", "sInfoThousands": ",", "sLoadingRecords": "Cargando...", "oPaginate": { "sFirst": "Primero", "sLast": "Último", "sNext": "Siguiente", "sPrevious": "Anterior" }, "oAria": { "sSortAscending": ": Activar para ordenar la columna de manera ascendente", "sSortDescending": ": Activar para ordenar la columna de manera descendente" }
        },
        columnDefs:
            [
                {
                    targets: [0],
                    width: "1%"
                },
                {
                    targets: [2],
                    width: "1%"
                }
            ]
    });
}

function FillTableTotals(Products) {
    amount = 0;
    iva = 0;
    ieps = 0;
    total = 0;
    Products.forEach(function (element) {
        amount += element.Amount;
        iva += element.IVA;
        ieps += element.IEPS;
        total += element.Total;
    });
    document.getElementById("amount").innerHTML = formatmoney(amount)(4);
    document.getElementById("iva").innerHTML = formatmoney(iva)(4);
    document.getElementById("ieps").innerHTML = formatmoney(ieps)(4);
    document.getElementById("total").innerHTML = formatmoney(total)(4);
}

function Search() {
    var product = $("#ItemSelect").val();
    if (product != "") {
        var begindate = $('#BeginDate').val().trim();
        var enddate = $('#EndDate').val().trim();
        if (begindate != "" && enddate != "") {
            if (enddate >= begindate) {
                StartLoading();
                $.ajax({
                    type: "GET",
                    url: "/Sales/ActionGetItemSalesByCashier",
                    data: { "BeginDate": begindate, "EndDate": enddate, "PartNumber": product },
                    success: function (response) {
                        EndLoading();
                        if (response.success) {
                            FillTableTotals(response.Json);
                            table.clear();
                            table.rows.add(response.Json);
                            table.columns.adjust().draw();
                        }
                    },
                    error: function () {
                        EndLoading();
                        toastr.error('Alerta - Error inesperado  contactar a sistemas.');
                    }
                });
            }
            else {
                toastr.warning("Favor de colocar la fecha correctamente")
                return null;
            }
        }
    }
    else {
        toastr.warning("Seleccione un producto")
        return null;
    }
}