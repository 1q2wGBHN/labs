﻿var pendingBool = false;
var statusUserReal = 0;

$(document).ready(function () {
    window.setTimeout(function () { $("#fa").click(); }, 1000);
    $("form").submit(function () { return false; });
    $('input').iCheck({ checkboxClass: 'icheckbox_square-green', });
    $("#user_status").select2();
    $('#UsersList').find('input[name="Users"]').iCheck({
        radioClass: 'iradio_square-green'
    }).on('ifChecked', function (e) {
        var EmployeeNumber = $(this).val();
        $.ajax({
            type: "GET",
            url: "/Configuration/ActionGetAllRolesAvailable/",
            data: { "EmployeeNumber": EmployeeNumber },
            success: function (returndate) {
                if (returndate.success) {
                    pendingBool = false;
                    var arr_json = JSON.parse(returndate.Json);
                    var status_user = returndate.Status;
                    statusUserReal = 1;
                    if (status_user != 'P') {
                        $('#user_status').val(status_user).trigger('change');
                        $('#pending').addClass('hide');
                    } else {
                        $('#user_status').val("").trigger('change');
                        pendingBool = true;
                        $('#pending').removeClass('hide');
                    }
                    $("#availableList").empty();
                    $.each(arr_json, function (i, v) {
                        $("#availableList").append(" <li class='list-group-item'><p class='checkbox'><input type='checkbox' name='available' value='" + v.role_id + "'>&nbsp;(" + v.role_name + ') ' + v.role_description + " </p></li>");
                    });
                    $('#availableList').find('input[name="available"]').iCheck({
                        checkboxClass: 'icheckbox_square-green'
                    });
                    arr_json = null;
                    returndate = null;
                    $.ajax({
                        type: "GET",
                        url: "/Configuration/ActionGetAllRolesOfUser/",
                        data: { "EmployeeNumber": EmployeeNumber },
                        success: function (returndate) {
                            if (returndate.success) {
                                var arr_json = JSON.parse(returndate.Json);
                                $("#assignedList").empty();
                                $.each(arr_json, function (i, v) {
                                    $("#assignedList").append(" <li class='list-group-item'><p class='checkbox'><input type='checkbox' name='notAvailable' value='" + v.role_id + "'>&nbsp;(" + v.role_name + ') ' + v.role_description + " </p></li>");
                                });
                                $('#assignedList').find('input[name="notAvailable"]').iCheck({
                                    checkboxClass: 'icheckbox_square-green'
                                });
                            }
                            else
                                toastr.warning('' + returndate.responseText + '');
                        },
                        error: function (returndate) {
                            toastr.error('Error inesperado contactar a sistemas.');
                        }
                    });
                }
                else
                    toastr.warning('' + returndate.responseText + '');
            },
            error: function (returndate) {
                toastr.error('Error inesperado contactar a sistemas.');
            }
        });
    }).end();
});

function OnChangeSelect2() {
    if (pendingBool) {
        if (status != "") {
            $('#pending').addClass('hide');
            pendingBool = false;
        } else {
            $('#pending').removeClass('hide');
            pendingBool = false;
        }
    }

    var status = $('#user_status').val();
    if (statusUserReal != 1) {
        if (status != "" && status != null) {
            var EmployeeNumber = '';
            $.each($("input[name='Users']:checked"), function () {
                EmployeeNumber += $(this).val() + ",";
            });
            var userNumber = EmployeeNumber.slice(0, -1);
            if (EmployeeNumber.length != 0) {
                $("user_status").prop("disabled", true);
                var status = $('#user_status').val();

                $.ajax({
                    type: "POST",
                    url: "/Configuration/ActionUpdateStatus/",
                    data: { "user": userNumber, "statusUser": status },
                    success: function (returndate) {
                        if (returndate.success) {
                            var resp = returndate.responseText;
                            if (resp == 0)
                                toastr.error('Alerta - Error al actualizar contacte a sistemas.');
                            else {
                                toastr.success('Se ha actualizado correctamente.');
                                $("user_status").prop("disabled", false);
                            }
                        }
                        else
                            toastr.warning('Alerta - Error al actualizar contacte a sistemas.');
                    },
                    error: function (returndate) {
                        toastr.error('Alerta - Error inesperado contactar a sistemas.');
                    }
                });
            }
        }
    }
    statusUserReal++;
}

$('#UsersList').slimScroll({
    height: '500px'
});

$('#availableList').slimScroll({
    height: '500px'
});

$('#assignedList').slimScroll({
    height: '500px'
});

function AddToUser() {
    var EmployeeNumber = '';
    var Roles = '';

    $.each($("input[name='Users']:checked"), function () {
        EmployeeNumber += $(this).val() + ',';
    });

    $.each($("input[name='available']:checked"), function () {
        Roles += $(this).val() + ',';
    });

    if (EmployeeNumber.length != 0 && Roles.length != 0) {
        $.ajax({
            type: "POST",
            url: "/Configuration/ActionAddRolesToUser/",
            data: { "EmployeeNumber": EmployeeNumber.slice(0, -1), "Roles": Roles.slice(0, -1) },
            success: function (returndate) {
                if (returndate.success) {
                    var arr_json = JSON.parse(returndate.Json);
                    $("#availableList").empty();
                    $.each(arr_json, function (i, v) {
                        $("#availableList").append("<li class='list-group-item'><p class='checkbox'><input type='checkbox' name='available' value='" + v.role_id + "'>&nbsp;(" + v.role_name + ') ' + v.role_description + " </p></li>");
                    });
                    $('#availableList').find('input[name="available"]').iCheck({
                        checkboxClass: 'icheckbox_square-green'
                    });
                    arr_json = null;
                    returndate = null;
                    $.ajax({
                        type: "GET",
                        url: "/Configuration/ActionGetAllRolesOfUser/",
                        data: { "EmployeeNumber": EmployeeNumber.slice(0, -1) },
                        success: function (returndate) {
                            if (returndate.success) {
                                var arr_json = JSON.parse(returndate.Json);
                                $("#assignedList").empty();
                                $.each(arr_json, function (i, v) {
                                    $("#assignedList").append(" <li class='list-group-item'><p class='checkbox'><input type='checkbox' name='notAvailable' value='" + v.role_id + "'>&nbsp;(" + v.role_name + ') ' + v.role_description + " </p></li>");
                                });
                                $('#assignedList').find('input[name="notAvailable"]').iCheck({
                                    checkboxClass: 'icheckbox_square-green'
                                });
                            }
                            else
                                toastr.warning('' + returndate.responseText + '');
                        },
                        error: function (returndate) {
                            toastr.error('Error inesperado contactar a sistemas.');
                        }
                    });
                }
                else
                    toastr.warning('' + returndate.responseText + '');
            },
            error: function (returndate) {
                toastr.error('Error inesperado contactar a sistemas.');
            }
        });
    }
    else
        toastr.warning('Debe seleccionar algun rol.');
}

function RemoveFromUser() {
    var EmployeeNumber = '';
    var Roles = '';

    $.each($("input[name='Users']:checked"), function () {
        EmployeeNumber += $(this).val() + ',';
    });

    $.each($("input[name='notAvailable']:checked"), function () {
        Roles += $(this).val() + ',';
    });

    if (Roles.length != 0 && EmployeeNumber.length != 0) {
        $.ajax({
            type: "POST",
            url: "/Configuration/ActionRemoveRolesFromUser/",
            data: { "EmployeeNumber": EmployeeNumber.slice(0, -1), "Roles": Roles.slice(0, -1) },
            success: function (returndate) {
                if (returndate.success) {
                    var arr_json = JSON.parse(returndate.Json);
                    $("#availableList").empty();
                    $.each(arr_json, function (i, v) {
                        $("#availableList").append(" <li class='list-group-item'><p class='checkbox'><input type='checkbox' name='available' value='" + v.role_id + "'>&nbsp;(" + v.role_name + ') ' + v.role_description + " </p></li>");
                    });
                    $('#availableList').find('input[name="available"]').iCheck({
                        checkboxClass: 'icheckbox_square-green'
                    });
                    arr_json = null;
                    returndate = null;
                    $.ajax({
                        type: "GET",
                        url: "/Configuration/ActionGetAllRolesOfUser/",
                        data: { "EmployeeNumber": EmployeeNumber.slice(0, -1) },
                        success: function (returndate) {
                            if (returndate.success) {
                                var arr_json = JSON.parse(returndate.Json);
                                $("#assignedList").empty();
                                $.each(arr_json, function (i, v) {
                                    $("#assignedList").append(" <li class='list-group-item'><p class='checkbox'><input type='checkbox' name='notAvailable' value='" + v.role_id + "'>&nbsp;(" + v.role_name + ') ' + v.role_description + " </p></li>");
                                });
                                $('#assignedList').find('input[name="notAvailable"]').iCheck({
                                    checkboxClass: 'icheckbox_square-green'
                                });
                            }
                            else
                                toastr.warning('' + returndate.responseText + '');
                        },
                        error: function (returndate) {
                            toastr.error('Error inesperado contactar a sistemas.');
                        }
                    });
                }
                else
                    toastr.warning('' + returndate.responseText + '');
            },
            error: function (returndate) {
                toastr.error('Error inesperado contactar a sistemas.');
            }
        });
    }
    else
        toastr.warning('Debe seleccionar algun rol.');
}

function AddToRoleAll() {
    $('#availableList').find('input[name="available"]').iCheck('check');

    if ($('#availableList').find('input[name="available"]').iCheck('check').length === 0)
        toastr.warning('No existen roles disponibles.');
    else {
        swal({
            title: "Esta seguro?",
            text: "Se asignaran todos los rol al usuario seleccionado!",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Continuar",
            cancelButtonText: "Cancelar"
        }, function (isConfirm) {
            if (isConfirm)
                AddToUser();
            else
                $('#availableList').find('input[name="available"]').iCheck('uncheck');
        });
    }
}

function RemoveFromRoleAll() {
    $('#assignedList').find('input[name="notAvailable"]').iCheck('check');

    if ($('#assignedList').find('input[name="notAvailable"]').iCheck('check').length === 0)
        toastr.warning('No existen roles disponibles.');
    else {
        swal({
            title: "Esta seguro?",
            text: "Se eliminaran todos los roles relacionados al usuario!",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Continuar",
            cancelButtonText: "Cancelar"
        }, function (isConfirm) {
            if (isConfirm)
                RemoveFromUser();
            else
                $('#assignedList').find('input[name="notAvailable"]').iCheck('uncheck');
        });
    }
}