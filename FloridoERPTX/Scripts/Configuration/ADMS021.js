﻿const fomartmoney = money => decimal => new Intl.NumberFormat("en-US", { style: "currency", currency: "USD", currencyDisplay: "symbol", maximumFractionDigits: decimal }).format(money); //Formato de Decimales
const DateToday = new Date();
const Today = new Date(DateToday.getFullYear(), DateToday.getMonth(), DateToday.getDate());
jQuery(document).ready(function () {
    if ($("#Bascula1").val() == "False")
        $('#b1').addClass("hide");
    if ($("#Bascula2").val() == "False")
        $('#b2').addClass("hide");
    if ($("#Bascula3").val() == "False")
        $('#b3').addClass("hide");
    $('#cbox_ReportType').iCheck({
        checkboxClass: 'icheckbox_square-green',
    }).on('ifClicked', function () {
        $(this).trigger("click");
    });
    ClearTable(JSON.parse($("#ProductsForTable").val()));
});
function checkDetail() {
    StartLoading();
    if ($("#cbox_ReportType").is(':checked'))
        ClearTable(tableFromBascule.data().toArray().filter(f => moment(f.last_update).format('YYYY-MM-DD') == moment(Today).format('YYYY-MM-DD')));
    else
        ClearTable(JSON.parse($("#ProductsForTable").val()));
    EndLoading();
}
function ClearTable(information) {
    tableFromBascule.clear().draw();
    tableFromBascule.rows.add(information);
    tableFromBascule.columns.adjust().draw();
}
var tableFromBascule = $('#TablaBascula').DataTable({
    "ordering": true, "searching": true, "paging": true, "info": true, responsive: true, order: [[2, "asc"]], dom: "<'row'<'col-sm-4'l><'col-sm-4 text-left'B><'col-sm-4'f>t<'col-sm-6'i><'col-sm-6'p>>", lengthMenu: [[10, 25, 50, -1], [10, 25, 50, "Todos"]],
    buttons: [
        { extend: 'csv', text: 'Excel', title: 'ProductosBascula', className: 'btn-sm', exportOptions: { columns: [0, 1, 2, 3, 4, 5] } },
        { extend: 'pdf', text: 'PDF', title: 'ProductosBascula', className: 'btn-sm', exportOptions: { columns: [0, 1, 2, 3, 4, 5] },
            customize: function (doc) {
                doc.content[1].table.widths =
                    Array(doc.content[1].table.body[0].length + 1).join('*').split('');
            }
        },
        { extend: 'print', text: 'Imprimir', className: 'btn-sm', exportOptions: { columns: [0, 1, 2, 3, 4, 5] } }
    ],
    oLanguage: {
        "sProcessing": "Procesando...", "sLengthMenu": "Mostrar _MENU_ registros", "sZeroRecords": "No se encontraron resultados", "sEmptyTable": "Ningún dato disponible en esta tabla", "sInfo": "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros", "sInfoEmpty": "Mostrando registros del 0 al 0 de un total de 0 registros", "sInfoFiltered": "(filtrado de un total de _MAX_ registros)", "sInfoPostFix": "", "sSearch": "Buscar:", "sUrl": "", "sInfoThousands": ",", "sLoadingRecords": "Cargando...", "oPaginate": { "sFirst": "Primero", "sLast": "Último", "sNext": "Siguiente", "sPrevious": "Anterior" }, "oAria": { "sSortAscending": ": Activar para ordenar la columna de manera ascendente", "sSortDescending": ": Activar para ordenar la columna de manera descendente" }
    },
    columnDefs: [
        {
            targets: [1],
            render: function (data, type, full) {
                return `<p><b>${data}</b></p>`
            }
        },
        {
            targets: [3],
            render: function (data, type, full) {
                return `<p>${fomartmoney(data)(4)}</p>`
            }
        },
        {
            targets: [4],
            render: function (data, type, full) {
                if (data.charAt(0) == "0") //Remove 0
                    return data.slice(1);
                return data;
            }
        },
        {
            width: "1%",
            targets: [5],
            render: function (data, type, full, meta) {
                if (data)
                    return moment(data).lang('es').format('DD MMMM [a las] h:mm a');
                return "N/A";
            }
        }
    ],
    columns:
        [
            { data: 'part_number_origin' }, //0
            { data: 'part_number' },//1
            { data: 'part_description' },//2
            { data: 'Price' },//3
            { data: 'expiration_days' },//4
            { data: 'last_update' },//5
            { data: 'part_caducidad', visible: false },//6
            { data: 'department', visible: false },//7
            { data: 'level_header', visible: false },//8
            { data: 'level1_code', visible: false },//9
            { data: 'level2_code', visible: false },//10
            { data: 'Weight_flag', visible: false },//11
            { data: 'flag_visible', visible: false }//12
        ]
});
