﻿$(document).ready(function () {
    window.setTimeout(function () { $("#fa").click(); }, 1000);
    $("form").submit(function () { return false; });
    $('input').iCheck({ checkboxClass: 'icheckbox_square-green', });
    $('#rolList').find('input[name="Rols"]').iCheck({
        radioClass: 'iradio_square-green'
    }).on('ifChecked', function (e) {
        var rolID = $(this).val();
        $.ajax({
            type: "GET",
            url: "/Configuration/ActionGetAllPagesOfRole",
            data: { "role_id": rolID },
            success: function (returndate) {
                if (returndate.success) {
                    var arr_json = JSON.parse(returndate.Json);
                    $("#availableList").empty();
                    $.each(arr_json, function (i, v) {
                        $("#availableList").append(" <li class='list-group-item'><p class='checkbox'><input type='checkbox' name='available' value='" + v.page_id + "'>&nbsp;(" + v.page_id + ') ' + v.description + " </p></li>");
                    });
                    $('#availableList').find('input[name="available"]').iCheck({
                        checkboxClass: 'icheckbox_square-green'
                    });
                    arr_json = null;
                    returndate = null;
                    $.ajax({
                        type: "GET",
                        url: "/Configuration/ActionGetAllPagesAvailable",
                        data: { "role_id": rolID },
                        success: function (returndate) {
                            if (returndate.success) {
                                var arr_json = JSON.parse(returndate.Json);
                                $("#assignedList").empty();
                                $.each(arr_json, function (i, v) {
                                    $("#assignedList").append(" <li class='list-group-item'><p class='checkbox'><input type='checkbox' name='notAvailable' value='" + v.page_id + "'>&nbsp;(" + v.page_id + ') ' + v.description + " </p></li>");
                                });
                                $('#assignedList').find('input[name="notAvailable"]').iCheck({
                                    checkboxClass: 'icheckbox_square-green'
                                });
                            }
                            else
                                toastr.warning('Alerta - ' + returndate.responseText + '');
                        },
                        error: function (returndate) {
                            toastr.error('Alerta - Error inesperado contactar a sistemas.');
                        }
                    });
                }
                else
                    toastr.warning('Alerta - ' + returndate.responseText + '');
            },
            error: function (returndate) {
                toastr.error('Alerta - Error inesperado contactar a sistemas.');
            }
        });
    }).end();
});

function searchLists(s) {
    var input, filter, ul, li, a, i, element;
    element = s.split(",");
    filter = $(element[1]).val().toUpperCase();
    ul = document.getElementById(element[0]);
    li = ul.getElementsByTagName("li");
    for (i = 0; i < li.length; i++) {
        a = li[i].getElementsByTagName("P")[0];
        if (a.innerHTML.toUpperCase().indexOf(filter) > -1) {
            li[i].style.display = "";
        } else {
            li[i].style.display = "none";
        }
    }
    input = null, filter = null, ul = null, li = null, a = null, i = null, element = null;
}

$('#rolList').slimScroll({
    height: '500px'
});

$('#availableList').slimScroll({
    height: '500px'
});

$('#assignedList').slimScroll({
    height: '500px'
});

function AddToRole() {
    var Role = '';
    var Pages = '';
    $.each($("input[name='Rols']:checked"), function () {
        Role += $(this).val() + ',';
    });
    $.each($("input[name='available']:checked"), function () {
        Pages += $(this).val() + ',';
    });
    if (Pages.length != 0 && Role.length != 0) {
        $.ajax({
            type: "POST",
            url: "/Configuration/ActionAddPagesToRoles",
            data: { "roles": Role.slice(0, -1), "pages": Pages.slice(0, -1) },
            success: function (returndate) {
                if (returndate.success) {
                    var arr_json = JSON.parse(returndate.Json);
                    $("#availableList").empty();
                    $.each(arr_json, function (i, v) {
                        $("#availableList").append(" <li class='list-group-item'><p class='checkbox'><input type='checkbox' name='available' value='" + v.page_id + "'>&nbsp;(" + v.page_id + ') ' + v.description + " </p></li>");
                    });
                    $('#availableList').find('input[name="available"]').iCheck({
                        checkboxClass: 'icheckbox_square-green'
                    });
                    arr_json = null;
                    returndate = null;
                    $.ajax({
                        type: "GET",
                        url: "/Configuration/ActionGetAllPagesAvailable",
                        data: { "role_id": Role.slice(0, -1) },
                        success: function (returndate) {
                            if (returndate.success) {
                                var arr_json = JSON.parse(returndate.Json);
                                $("#assignedList").empty();
                                $.each(arr_json, function (i, v) {
                                    $("#assignedList").append(" <li class='list-group-item'><p class='checkbox'><input type='checkbox' name='notAvailable' value='" + v.page_id + "'>&nbsp;(" + v.page_id + ') ' + v.description + " </p></li>");
                                });
                                $('#assignedList').find('input[name="notAvailable"]').iCheck({
                                    checkboxClass: 'icheckbox_square-green'
                                });
                            }
                            else
                                toastr.warning('Alerta - ' + returndate.responseText + '');
                        },
                        error: function (returndate) {
                            toastr.error('Alerta - Error inesperado contactar a sistemas.');
                        }
                    });
                }
                else
                    toastr.warning('Alerta - ' + returndate.responseText + '');
            },
            error: function (returndate) {
                toastr.error('Alerta - Error inesperado contactar a sistemas.');
            }
        });
    }
    else
        toastr.warning('Alerta - Debe seleccionar alguna pagina.');
}

function RemoveFromRole() {
    var Role = '';
    var Pages = '';
    $.each($("input[name='Rols']:checked"), function () {
        Role += $(this).val() + ',';
    });
    $.each($("input[name='notAvailable']:checked"), function () {
        Pages += $(this).val() + ',';
    });
    if (Pages.length != 0 && Role.length != 0) {
        $.ajax({
            type: "POST",
            url: "/Configuration/ActionRemovePagesFromRoles",
            data: { "roles": Role.slice(0, -1), "pages": Pages.slice(0, -1) },
            success: function (returndate) {
                if (returndate.success) {
                    var arr_json = JSON.parse(returndate.Json);
                    $("#availableList").empty();
                    $.each(arr_json, function (i, v) {
                        $("#availableList").append(" <li class='list-group-item'><p class='checkbox'><input type='checkbox' name='available' value='" + v.page_id + "'>&nbsp;(" + v.page_id + ') ' + v.description + " </p></li>");
                    });
                    $('#availableList').find('input[name="available"]').iCheck({
                        checkboxClass: 'icheckbox_square-green'
                    });
                    arr_json = null;
                    returndate = null;
                    $.ajax({
                        type: "GET",
                        url: "/Configuration/ActionGetAllPagesAvailable",
                        data: { "role_id": Role.slice(0, -1) },
                        success: function (returndate) {
                            if (returndate.success) {
                                var arr_json = JSON.parse(returndate.Json);
                                $("#assignedList").empty();
                                $.each(arr_json, function (i, v) {
                                    $("#assignedList").append(" <li class='list-group-item'><p class='checkbox'><input type='checkbox' name='notAvailable' value='" + v.page_id + "'>&nbsp;(" + v.page_id + ') ' + v.description + " </p></li>");
                                });
                                $('#assignedList').find('input[name="notAvailable"]').iCheck({
                                    checkboxClass: 'icheckbox_square-green'
                                });
                            }
                            else
                                toastr.warning('Alerta - ' + returndate.responseText + '');
                        },
                        error: function (returndate) {
                            toastr.error('Alerta - Error inesperado contactar a sistemas.');
                        }
                    });
                }
                else
                    toastr.warning('Alerta - ' + returndate.responseText + '');
            },
            error: function (returndate) {
                toastr.error('Alerta - Error inesperado contactar a sistemas.');
            }
        });
    }
    else
        toastr.warning('Alerta - Debe seleccionar alguna pagina.');
}

function AddToRoleAll() {
    $('#availableList').find('input[name="available"]').iCheck('check');

    if ($('#availableList').find('input[name="available"]').iCheck('check').length === 0)
        toastr.warning('Alerta - No existen paginas disponibles.');
    else {
        swal({
            title: "Esta seguro?",
            text: "Se asignaran todas las páginas al rol seleccionado!",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Continuar",
            cancelButtonText: "Cancelar"
        }, function (isConfirm) {
            if (isConfirm)
                AddToRole();
            else
                $('#availableList').find('input[name="available"]').iCheck('uncheck');
        });
    }
}

function RemoveFromRoleAll() {
    $('#assignedList').find('input[name="notAvailable"]').iCheck('check');

    if ($('#assignedList').find('input[name="notAvailable"]').iCheck('check').length === 0)
        toastr.warning('Alerta - No existen paginas disponibles.');
    else {
        swal({
            title: "Esta seguro?",
            text: "Se eliminaran todas las paginas relacionadas al rol!",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Continuar",
            cancelButtonText: "Cancelar"
        }, function (isConfirm) {
            if (isConfirm)
                RemoveFromRole();
            else
                $('#assignedList').find('input[name="notAvailable"]').iCheck('uncheck');
        });
    }
}