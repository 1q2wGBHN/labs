﻿var subTotal = 0;
var quantityReturn;
var regex = /^(?:-?\d+|-?\d{1,3}(?:,\d{3})+)?(?:\.\d+)?$/;
var formValid;
var isBoniValue = document.getElementById('IsBonification').value;
var isBonification = isBoniValue.toLowerCase() == 'true' ? true : false;
var canSelectItemValue = document.getElementById('CanSelectItems').value;
var canSelectItem = canSelectItemValue.toLowerCase() == 'true' ? true : false;
var totalInvoice = document.getElementsByName('InvoiceDetails.Total')[0].value;
var totalTaxInvoice = document.getElementsByName('InvoiceDetails.Tax')[0].value;
var subTotalInvoice = document.getElementsByName('InvoiceDetails.SubTotal')[0].value;
var creditNote = $("#Serie").val() + $("#Number").val();
var hasIeps = false;

$('#invoice-details tfoot').on('change keyup', 'td', 'input', function () {        
    ValidateTaxes();
});

$('#invoice-details tbody').on('change keyup', 'td', 'input', function () {

    //var validInput = Validations($(this).closest('td'));

    //if (isBonification && canSelectItem && validInput) {        
    //    var td = $(this).closest('td');                
    //    var rowInfo = invoDetails.row(td.parents('tr')).data();
        
    //    SingleCalculateAmount(rowInfo, td);
    var validInput = Validations($(this).closest('td'));

    if (isBonification && canSelectItem && validInput)
        CalculateAmounts($(this).closest('td'));   
});

//Validate Date.
$("#IssuedDate").on('change', function () {
    var isValid = ValidateDocumentDate();    
    if (isValid) {
        $("#GenerateCreditNote").removeAttr("disabled");
        $("#IssuedDate").css('border-color', '');
    }
});

//Form was not serializing
function MappingModel() {
    var Model = {};
    Model.IssuedDate = document.getElementById('IssuedDate').value;
    Model.Cause = document.getElementById('Cause').value;
    Model.Comments = document.getElementById('Comments').value;
    Model.CreditNoteReference = document.getElementById('CreditNoteReference').value;
    Model.Serie = document.getElementById('Serie').value;
    Model.Number = document.getElementById('Number').value;
    Model.IsBonification = document.getElementById('IsBonification').value;
    Model.InvoiceBalance = document.getElementById('InvoiceBalance').value;
    Model.IsSaleFromGlobal = document.getElementById('IsSaleFromGlobal').value;
    Model.SaleNumber = document.getElementById('SaleNumber').value;
    Model.IsReadOnly = document.getElementById('IsReadOnly').value;
    Model.IsSaleInvoiced = document.getElementById('IsSaleInvoiced').value;
    Model.IsEmployeeBon = document.getElementById('IsEmployeeBon').value;
    Model.SelectAll = IsSelectAllChecked();
    Model.InvoiceNumber = document.getElementById('InvoiceNumber').value;
    Model.InvoiceDetails = {
        InvoiceNumber: document.getElementsByName('InvoiceDetails.InvoiceNumber')[0].value,
        InvoiceSerie: document.getElementsByName('InvoiceDetails.InvoiceSerie')[0].value,
        Date: document.getElementsByName('InvoiceDetails.Date')[0].value,
        ClientNumber: document.getElementsByName('InvoiceDetails.ClientNumber')[0].value,
        ClientName: document.getElementsByName('InvoiceDetails.ClientName')[0].value,
        PaymentMethod: document.getElementsByName('InvoiceDetails.PaymentMethod')[0].value,
        Currency: document.getElementsByName('InvoiceDetails.Currency')[0].value,
        SubTotal: document.getElementsByName('InvoiceDetails.SubTotal')[0].value,
        Tax: document.getElementsByName('InvoiceDetails.Tax')[0].value,
        Total: document.getElementsByName('InvoiceDetails.Total')[0].value,
        CurrencyRate: document.getElementsByName('InvoiceDetails.CurrencyRate')[0].value,
        SatPMCode: document.getElementsByName('InvoiceDetails.SatPMCode')[0].value,
        Products: GetDataFromTable()
    };

    Model.BonificationTaxes = GetFooterData();

    return Model;
}

//Get value data from inputs in table body
function GetDataFromTable() {
    var data = invoDetails.rows().data();
    var infoReturnList = [];
    var infoInputList = [];
    
    //check each input textbox in table body
    invoDetails.rows().every(function (index, element) {        
        var infoReturn = {};
        var row = $(this.node());
        var dataRow = $(this.data());
        
        var inputValue = !isBonification ? row.context.children[3].firstChild.value : 0;

        //validate if the input value is > 0
        if (inputValue > 0) {
            infoReturn.QuantityReturn = inputValue;
            infoReturn.ItemNumber = row.context.children[0].firstChild.data;
            infoReturn.DetailId = dataRow[0].DetailId;
            infoReturn.ItemPriceNoTax = dataRow[0].ItemPriceNoTax;
            infoReturn.SalePartNumber = dataRow[0].SalePartNumber;
            infoInputList.push(infoReturn);
        }
    });
    
    //update original array (data) with the quantity to return
    for (i = 0; i < infoInputList.length; i++) {
        for (j = 0; j < data.length; j++) {
            if (infoInputList[i].ItemNumber == data[j].ItemNumber && infoInputList[i].DetailId == data[j].DetailId)
                data[j].QuantityReturn = infoInputList[i].QuantityReturn;
        }
    }

    //validate each input in the array
    for (i = 0; i < data.length; i++) {
        if (regex.test(data[i].QuantityReturn))
            infoReturnList.push(data[i]);
    }

    return infoReturnList; //Return only rows that its input value has changed.
}

//Disable button and formValid flag = false
function DisableButton(inputData) {
    formValid = false;
    $("#GenerateCreditNote").attr("disabled", "disabled");
    //inputData.find('input').css("border-color", "red");
    //inputData.find('input').val(0.00);
}

//Enable button and formValid flag = true
function EnableButton(inputData) {
    formValid = true;
    $("#GenerateCreditNote").removeAttr("disabled");
    inputData.find('input').css("border-color", "");
}

//Get Data from footer inputs
function GetFooterData() {
    var list = [];
    var footer = $('#invoice-details tfoot');
    for (i = 0; i < footer[0].children.length; i++) {
        var elementId = footer[0].children[i].id;
        var inputValue = footer[0].children[i].lastChild.firstChild.value;
        if ((elementId !== null || elementId !== "") && inputValue > 0) {
            var BonificationTax = {};
            BonificationTax.TaxCode = footer[0].children[i].id;
            BonificationTax.Bonification = footer[0].children[i].lastChild.firstChild.value;
            BonificationTax.TaxName = footer[0].children[i].firstChild.firstChild.data.replace("Bonificación ", "");

            list.push(BonificationTax);
        }
    }
    return list;
}

//Validate FooterTaxes
function ValidateTaxes() {
    var footer = $('#invoice-details tfoot');
    var totalInput = 0;
    var totalInputTax = 0;
    var totalInputIEPS = 0;

    for (i = 0; i < footer[0].children.length; i++) {
        var elementId = footer[0].children[i].id;
        var inputValue = Number(footer[0].children[i].lastChild.firstChild.value);
        if ((elementId !== null || elementId !== "") && inputValue > 0) {

            if (elementId.includes('SubTotal') && (inputValue > totalInvoice || inputValue > subTotalInvoice)) {
                $("#GenerateCreditNote").attr("disabled", "disabled");
                toastr.error('Error - Subtotal no debe ser mayor al de la factura.');
                break;
            }

            if (elementId.includes('TAX')) {
                if (inputValue > totalTaxInvoice) {
                    $("#GenerateCreditNote").attr("disabled", "disabled");
                    toastr.error('Error - IVA no debe ser mayor al de la factura.');
                    break;
                }
                else
                    totalInputTax = totalInputTax + inputValue;
            }

            if (elementId.includes('IEPS')) {
                if (inputValue > totalInvoice) {
                    $("#GenerateCreditNote").attr("disabled", "disabled");
                    toastr.error('Error - IEPS no debe ser mayor al total de la factura.');
                    break;
                }
                else
                    totalInputIEPS = totalInputIEPS + inputValue;
            }

            totalInput = totalInput + inputValue;
            
            if (totalInput.toFixed(2) > parseFloat(totalInvoice)) {                
                $("#GenerateCreditNote").attr("disabled", "disabled");
                toastr.error('Suma de bonificaciones no debe ser mayor al total de la factura.');
                break;
            }

            if (totalInputTax.toFixed(2) > parseFloat(totalTaxInvoice)) {
                $("#GenerateCreditNote").attr("disabled", "disabled");
                toastr.error('Suma de IVA no debe ser mayor al IVA de la factura.');
                break;
            }

            if (totalInputIEPS.toFixed(2) > parseFloat(totalInvoice)) {
                $("#GenerateCreditNote").attr("disabled", "disabled");
                toastr.error('Suma de IEPS no debe ser mayor al total de la factura o mayor al IVA.');
                break;
            }

            $("#GenerateCreditNote").removeAttr("disabled");
        }
    }

    return totalInput;
}

//Function to Create CreditNote
function CreateCreditNote(url, model, urlLoading) {
    StartLoading();
    $.post({
        url: url,
        Type: 'POST',
        dataType: 'json',
        data: { "Model": model },
        success: function (data) {
            EndLoading();            
            if (data.success && (data.Json.IsStamped || data.Json.IsStamped == 'true')) {                
                var pdf = 'data:application/pdf;base64,' + data.Json.PdfEncoded;
                $("#Pdf-iframe").attr("data", pdf);
                $('#ModalPdf').modal('show');
            }
            else if (data.success) {
                if (isBonification) {
                    swal({
                        title: 'Bonificación',
                        text: "Bonificación creada pero no timbrada. Bonificación " + creditNote,
                        type: "warning"
                    }, function () {
                        window.location.href = urlLoading;
                    });
                }
                else {
                    swal({
                        title: 'Nota de Crédito',
                        text: "Nota creada pero no timbrada. Nota de Crédito " + creditNote,
                        type: "warning"
                    }, function () {
                        window.location.href = urlLoading;
                    });
                }
            }
            else {
                swal({
                    title: 'Error',
                    text: "Ocurrio un error, contacte a sistemas para solucionarlo.",
                    type: "error"
                });
            }
        }
    });
}

//Function to validate inputs from table body and foot
function Validations(tdInput) {
    var obj = tdInput.find('input').attr('value', this.value);
    quantityReturn = parseFloat(obj.context.firstChild.value);
    var rowInfo = invoDetails.row(tdInput.parents('tr')).data();
    quantityAvailable = parseFloat(rowInfo.QuantityAvailable);

    //validations
    //if (!regex.test(quantityReturn)) {
    //    DisableButton(tdInput);
    //    toastr.error('Escribe solo números');
    //    return;
    //}

    if (quantityReturn < 0) {
        DisableButton(tdInput);
        toastr.error('Escribe solo números positivos');
        return false;
    }

    //if (quantityReturn.match("..")) {
    //    DisableButton(tdInput);
    //    toastr.error('Escribe solo números');
    //    return;
    //}

    //This validations only apply when type is Credit Notes or can return items on Bonifications 
    if (rowInfo.IsWeightAvailable === "0" && (quantityReturn - Math.floor(quantityReturn) !== 0)) {
        DisableButton(tdInput);
        toastr.error('Artículo no permite regresar en partes');
        return false;
    }

    if (quantityReturn > quantityAvailable) {
        DisableButton(tdInput);
        toastr.error('La cantidad ingresada excede a la cantidad disponible');
        return false;
    }

    if (regex.test(quantityReturn))
        EnableButton(tdInput);

    return true;
}

//Function to Calucalate SubTotal and Taxs (IVA and IEPS) when Bonifications can select items (SelectAll Checkbox)
function CalculateAmounts() {    
    var subTotal = 0;    
    invoDetails.rows().every(function (index, element) {    
        var subtTotalItem = 0; var ivaItem = 0; var iepsItem = 0;
        var row = $(this.node());
        var inputValue = row.context.children[3].firstChild.value; //Get the value of the input on table.
        var dataRow = $("#invoice-details").DataTable().row(row).data();            
        
        input = parseFloat(inputValue);
        itemPrice = parseFloat(dataRow['ItemPriceNoTax']);

        subtTotalItem = input.toFixed(3) * itemPrice.toFixed(3);
        subTotal += parseFloat(subtTotalItem.toFixed(3));

        if (parseFloat(dataRow['ItemTax']) > 0) {
            ivaItem += input * parseFloat(dataRow['ItemTax']);
            var nameTax = 'Tax_' + dataRow['IVACode'];
            var inputHtmlTax = document.getElementById(nameTax);
            inputHtmlTax.value = ivaItem.toFixed(2);
        }
        else
            ivaItem = 0;


        if (parseFloat(dataRow['ItemIEPS']) > 0) {
            hasIeps = true;
            iepsItem += input * parseFloat(dataRow['ItemIEPS']);
            var nameIEPS = 'Tax_' + dataRow['IEPSCode'];
            var inputHtmlIEPS = document.getElementById(nameIEPS);
            inputHtmlIEPS.value = iepsItem.toFixed(2);
        }
        else             
            iepsItem = 0;        
               
        subTotal -= parseFloat(iepsItem);
    });
    
    //Update Subtotal Field
    $("#Tax_Sub_Total").val(subTotal.toFixed(2));
}

//
function SingleCalculateAmount(rowInfo, tdInput) {
    var obj = tdInput.find('input').attr('value', this.value);
    var itemPrice = parseFloat(rowInfo['ItemPriceNoTax']);
    var inputValue = parseFloat(obj.context.firstChild.value);

    var subTotal = 0;    
    var subtTotalItem = 0; var ivaItem = 0; var iepsItem = 0;    
    var subTotalValue = document.getElementById('Tax_Sub_Total');

    subtTotalItem = inputValue.toFixed(3) * itemPrice.toFixed(3);

    if (parseFloat(rowInfo['ItemTax']) > 0) {
        var nameTax = 'Tax_' + dataRow['IVACode'];
        var inputHtmlTax = document.getElementById(nameTax);
        var itemPreviousValue = parseFloat(inputHtmlTax.value);

        if (inputValue > 0) 
            itemPreviousValue += (input * parseFloat(dataRow['ItemTax']));
        else
            ivaItem -=
        inputHtmlTax.value = ivaItem.toFixed(2);
    }

    subTotal = parseFloat(subTotalValue.value);    
}

//Function to Get the Current Date in format dd/mm/yyyy
function GetCurrentDate() {
    var today = new Date();
    var dd = today.getDate();
    var mm = today.getMonth() + 1; //January is 0!
    var yyyy = today.getFullYear();

    if (dd < 10) {
        dd = '0' + dd;
    }

    if (mm < 10) {
        mm = '0' + mm;
    }

    today = dd + '/' + mm + '/' + yyyy;

    return today;
}

function ValidateDocumentDate()
{
    var date = $("#IssuedDate").val();
    var invoiceDate = $("#InvoiceDetails_Date").val();
    var currentDate = GetCurrentDate();
    var motive = $("#Cause").val();
    var comments = $("#Comments").val();


    //if (stringToDate(invoiceDate) < stringToDate(date)) {
    //    toastr.error('La fecha de aplicación no puede ser mayor a la fecha de la factura.');
    //    $("#GenerateCreditNote").attr("disabled", "disabled");
    //    $("#IssuedDate").attr('style', 'border-color: red !important');
    //    return false;
    //}

    if (currentDate > stringToDate(date)) {
        toastr.error('La fecha de aplicación no puede ser mayor al día de hoy.');
        $("#GenerateCreditNote").attr("disabled", "disabled");
        $("#IssuedDate").attr('style', 'border-color: red !important');
        return false;
    }

    if (motive == null || motive == '') {
        toastr.error('Favor de introducir un motivo.');
        $("#GenerateCreditNote").attr("disabled", "disabled");
        $("#Cause").attr('style', 'border-color: red !important');
        return false;
    }

    if (comments == null || comments == '') {
        toastr.error('Favor de introducir comentarios.');
        $("#GenerateCreditNote").attr("disabled", "disabled");
        $("#Comments").attr('style', 'border-color: red !important');
        return false;
    }

    return true;
}

$("#SelectAll").click(function () {
    var IsChecked = $(this).is(':checked');
    if (IsChecked) 
        SelectAllItems();    
    else
        UnSelectAllItems();

    if (isBonification) {
        CalculateAmounts();
        if (IsChecked) {
            if (!hasIeps) {
                var realSubTotal = $("#InvoiceDetails_SubTotal").val();
                $("#Tax_Sub_Total").val(realSubTotal);
            }

            var realTax = $("#InvoiceDetails_Tax").val();            
            if (parseFloat(realTax) > 0)
                $("#Tax_TAX_8").val(realTax);           
        }
    }        
});

function IsSelectAllChecked() {
    var IsChecked = $("#SelectAll").is(':checked');
    if (IsChecked)
        return true;
    else
        return false;
}

//Function to Select All Items When Checkbox is selected it.
function SelectAllItems() {    
    var tdInput;
    invoDetails.rows().every(function (index, element) {        
        var row = $(this.node());
        var dataRow = $(this.data());
        tdInput = $(this).closest('td');        
        row.context.children[3].firstChild.value = parseFloat(dataRow[0].QuantityAvailableBon);
    });
    EnableButton(tdInput);
}

//Function to UnSelect All Items When Checkbox is not selected it.
function UnSelectAllItems() {
    invoDetails.rows().every(function (index, element) {
        var row = $(this.node());        

        row.context.children[3].firstChild.value = "0.00";
    });
}

$("#Comments").on('change keyup', function () {
    $("#GenerateCreditNote").removeAttr("disabled");
    $("#Comments").css('border-color', '');
});

$("#Cause").on('change keyup', function () {
    $("#GenerateCreditNote").removeAttr("disabled");
    $("#Cause").css('border-color', '');
});