﻿"use strict";


const baseUrl = "/Approval";
class ApprovalViewModel {
    constructor() {

	    this.process_search = ko.observable('');
	    this.process_list = ko.observableArray();
        this.process_list_filtered = ko.pureComputed(() => {
	        if (!this.process_search()) return this.process_list();
            return ko.utils.arrayFilter(this.process_list(), (e) => e.process_name.toLowerCase().includes(this.process_search().toLowerCase()));
        }).extend({
	        rateLimit: 100
        });
        ;
        this.process_selected = ko.observable({ });
        this.process_name_add = ko.observable('');

        this.step_search = ko.observable('');
        this.step_list = ko.observableArray();
        this.step_list_filtered = ko.pureComputed(() => {
            if (!this.step_search()) return this.step_list();
            return ko.utils.arrayFilter(this.step_list(), (e) => e.step_level.toString().toLowerCase().includes(this.step_search().toLowerCase()));
        }).extend({
	        rateLimit: 100
        });
        ;
        this.step_selected = ko.observable({});
        this.approval_opt = ko.observable("");


        this.user_search = ko.observable('');
        this.user_list = ko.observableArray();
        this.user_list_filtered = ko.pureComputed(() => {
            if (!this.user_search()) return this.user_list();
            return ko.utils.arrayFilter(this.user_list(), (e) => e.emp_name.toLowerCase().includes(this.user_search().toLowerCase()));
        }).extend({
	        rateLimit: 100
        });
        ;
        this.user_selected = ko.observable({});
        this.emp_no_add = ko.observable('');
        this.emp_process_group_add = ko.observable('');

        this.search_user_results = ko.observableArray([]);
        this.emp_no_add.extend({ rateLimit: { timeout: 300, method: "notifyWhenChangesStop" } });
        this.emp_no_add.subscribe(this.search_on_text_input.bind(this));

        this.load_process_list = this.load_process_list.bind(this);
        this.process_add = this.process_add.bind(this);
        this.load_step_list = this.load_step_list.bind(this);
        this.step_add = this.step_add.bind(this);
        this.load_user_list = this.load_user_list.bind(this);
        this.user_add = this.user_add.bind(this);
        this.select_process = this.select_process.bind(this);
        this.select_step = this.select_step.bind(this);
        this.edit_step_type = this.edit_step_type.bind(this);
        this.load_process_list();
        


    }

    select_process(p) {
        console.log(p);
        this.process_selected(p);
        this.step_selected({});
        this.load_step_list(p);
        this.step_list.removeAll();
        this.user_list.removeAll();
    }
    select_step(s) {
        console.log(s);
        this.step_selected(s);
        this.load_user_list(s);
        

    }
    select_user(u) {
	    this.user_selected(p);
	    console.log(u);
    }
    





    async load_process_list() {
	    console.log("aaa");
        const r = await Requests.get(`${baseUrl}/ActionProcessList`, {}).send();
        if (r.Ok) {
	        this.process_list.removeAll();
            ko.utils.arrayPushAll(this.process_list,r.Data);
            console.log(r.Data);
        }
        this.step_selected({});
    }
    async process_add() {
	    const process_name = this.process_name_add().trim();
        if (!process_name) {
		    toastr.warning("Se ocupa poner un nombre de proceso");
		    return;
	    }
        if (this.process_list().find((e) => e.process_name === process_name)) {
            toastr.error("Este nombre de proceso ya se encuentra en la lista");
            return;
        }

        this.process_list.push({ process_name: process_name });
    }
    async load_step_list(p) {
        const r = await Requests.get(`${baseUrl}/ActionStepList`, { process_name:p.process_name}).send();
	    if (r.Ok) {
		    this.step_list.removeAll();
            ko.utils.arrayPushAll(this.step_list,r.Data);
		    console.log(r.Data);
	    }
    }
     async disableOnRun(view, lamda) {
	    try {
		    view.disabled = true;
		    await lamda();
	    } finally {
		    view.disabled = false;
	    } 
    }

    async step_add(v) {
	    this.disableOnRun(v,async () => {
		    const data = {
			    process_name: this.process_selected().process_name,
			    step_level: Math.max(...this.step_list().map(e => e.step_level).concat([0])) + 1,
			    approval_type: this.approval_opt(),
			    finish_flag: true,

		    };
		    if (!data.process_name) {
			    toastr.warning("No se ha seleccionado un proceso");
			    return;
		    }
		    const r = await Requests.post(`${baseUrl}/ActionStepAdd`,
			    data
		    ).send();
		    if (r.Ok) {
			    this.step_list.push(data);
			    toastr.success("Paso agregado correctamente");
		    } 
		});

    }
    async edit_step_type(data, event) {
        const nval = event.target.value;
        data.approval_type = nval;
	    const r = await Requests.post(`${baseUrl}/ActionStepEdit`,
		    data
	    ).send();
        if (!r.Ok) {
	        this.load_step_list(this.process_selected());
        }
    }

    async load_user_list(s) {
        const r = await Requests.get(`${baseUrl}/ActionUserList`, { process_name: s.process_name, step_level: s.step_level  }).send();
	    if (r.Ok) {
            this.user_list.removeAll();
            ko.utils.arrayPushAll(this.user_list,r.Data);
            console.log(r.Data);
            console.log(this.user_list());
	    }
    }
    async user_add_searched(data) {
        this.emp_no_add(data.emp_no);
        this.user_add();
    }

    async user_add(v) {
	    this.disableOnRun(v,
		    async () => {
			    const data = {
				    process_name: this.process_selected().process_name,
				    step_level: this.step_selected().step_level,
				    emp_no: this.emp_no_add().trim(),
				    process_group: this.emp_process_group_add().trim()
			    }
			    if (!data.process_name || !data.step_level) {
				    toastr.warning("No se ha seleccionado un proceso y paso");
				    return;
			    }
			    if (!data.emp_no || !data.process_group) {
				    toastr.warning("No se ha llenado el grupo y numero de empleado");
				    return;
			    }
			    const r = await Requests.post(`${baseUrl}/ActionUserAdd`,
				    data
			    ).send();
			    if (r.Ok) {
				    this.emp_no_add('');
				    this.emp_process_group_add('');

				    toastr.success('usuario agregado correctamente');
			    }
			    this.load_user_list(this.step_selected());
		    });
    }

    async user_del(user) {
	    swal({
			    title: "Editar permisos",
			    text: `¿Desea retirar los permisos de este usuario?\n${user.emp_name}`,
			    type: "warning",
			    showCancelButton: true,
			    cancelButtonText: "Cancelar",
			    confirmButtonColor: "#DD6B55",
			    confirmButtonText: "Aceptar"

		    },
            async (va) => {
                if (!va) return;
			    await Requests.post(`${baseUrl}/ActionUserDel`,
				    user
			    ).send();
			    this.load_user_list(this.step_selected());
		    });
    }

    async search_on_text_input() {
	    console.log(this.emp_no_add());
    }

}