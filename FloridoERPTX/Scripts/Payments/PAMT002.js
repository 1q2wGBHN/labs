﻿var InvoicesCombobox = [];
var PaymentsCombobox = [];
var amountPayment = 0; //This variable is to check if the user asign the total payment to 1 or more invoices.
var originalPaymentAmount = 0; //This variable is to display it in invoices table.
var addNewPayment = true; //Flag to check when new payment is add it to the payments table.
var paymentId = ""; //Keep the payment id in invoices table.
var payments = document.getElementById("PaymentMethod");

//Add Payments to Table
$("#AddPaymentAmount").click(function () {
    var validAmount = $("#formPayments").validate().element("#AmountPayment");
    var methodId = $("#PaymentMethod").val();
    var operationNumber = $("#OperationNumber").val();
    var bankId = $("#Bank").val();
    var bankReference = $("#BankReference").val();
    var methodText = $("#PaymentMethod :selected").text();
    var datePaymentString = $("#AmountPaymentDate").val();
    var bankText = $("#Bank :selected").text();
    var row = $("<tr>");

    if (methodId == "") {
        toastr.error('Alerta - Elige un método de pago.');
        return;
    }

    if (!ValidateDate())
        return;

    if (totalInvoiceAmount != totalAmount) {
        toastr.error('Alerta - Asigne todo el monto del pago previo a 1 o mas facturas antes de agregar otro.');
        return;
    }

    if (bankId == null || bankId == '')
        bankText = '';

    //Read payment amount and create paymentId after pass all the validations
    amountPayment = parseFloat($("#AmountPayment").val());
    paymentId = CreateRandomString(4);
    originalPaymentAmount = parseFloat($("#AmountPayment").val());
    addNewPayment = true;

    if (validAmount) {
        if (amountPayment > 0) {
            //Display Invoices Section
            $("#InvoicesSection").show();
            $("#NestedTables").show();

            //Empty combobox and leave only the selected payment method
            $("#PaymentMethod").empty();
            var option = document.createElement("OPTION");
            option.innerHTML = methodText;
            option.value = methodId;
            payments.options.add(option);

            //Update total payment amount on view            
            totalAmount += amountPayment;
            $('.TotalPayment').html('$' + totalAmount.toFixed(2));

            //add new row to the table with info user wrote and selected
            row.append($("<td id=" + methodId + ">" + methodText + "</td>"))
               .append($("<td id=" + paymentId + "> $" + amountPayment + "</td>"))
               .append($("<td id=" + operationNumber + ">" + operationNumber + "</td>"))
               .append($("<td id=date>" + datePaymentString + "</td>"))
                .append($("<td id=" + bankId + ">" + bankText + "</td>"))
               .append($("<td id=" + bankReference + ">" + bankReference + "</td>"))
               .append($("<td id=payment>" + '<span class="input-group-addon" style="border-style:none;"><a class="deletePayment" id="a-' + paymentId + '"><i class="fa fa-trash"></i></a></span>' + "</td>")); //id to Identify the row as paymentRow

            //row.appendTo('#PaymentMethodTable');
            row.clone().appendTo('#NestedTables');
            $("#PaymentMethodTable").hide();

            //reset textbox and set selected payment method in combobox from payments
            $("#AmountPayment").val(0.00);
            $("#PaymentMethod").val(methodId);
            $("#Bank").val("");
            $("#BankReference").val("");
        }
        else {
            toastr.error('Alerta - El monto a pagar debe ser mayor a 0.');
            return;
        }
    }
});

//Add Invoices to Table
$("#AddInvoiceAmount").click(function () {
    var validAmount = $("#formPayments").validate().element("#AmountInvoice");
    var amountInvoice = parseFloat($("#AmountInvoice").val());
    var invoice = $("#Invoice").val();
    var row = $('<tr style="background-color:gainsboro;">');
    var rowCount = document.getElementById('InvoiceTable').rows.length;

    //get the amount of the selected invoice
    var invoiceText = $("#Invoice :selected").text();
    var invoiceBalance = parseFloat(invoiceText.substring(invoiceText.indexOf("$") + 1));
    var newInvoiceText = invoiceText.split('$')[0];
    var newinvoiceBalance = 0;

    if (totalAmount == 0) {
        toastr.error('Alerta - Agrega una forma de pago.');
        return;
    }

    if (invoice == "") {
        toastr.error('Alerta - Elige una factura a pagar.');
        return;
    }

    if (validAmount) {
        if (parseFloat(amountInvoice.toFixed(2)) > 0) {
            if (parseFloat(totalAmount.toFixed(2)) >= parseFloat(amountInvoice.toFixed(2))) {
                if (parseFloat(amountInvoice.toFixed(2)) <= parseFloat(invoiceBalance.toFixed(2))) {
                    totalInvoiceAmount += amountInvoice;
                    if (parseFloat(totalInvoiceAmount.toFixed(2)) <= parseFloat(totalAmount.toFixed(2))) {
                        //Update total invoice amount on view.
                        $('.TotalInvoices').html('$' + totalInvoiceAmount.toFixed(2));

                        //update invoice balance in combobox
                        newinvoiceBalance = invoiceBalance - amountInvoice;
                        $("#Invoice :selected").text(newInvoiceText + ' $' + newinvoiceBalance.toFixed(2));

                        //Remove element from combobox if balance remaining equals to 0
                        if (parseFloat(newinvoiceBalance.toFixed(2)) == 0) {
                            InvoicesComboboxList();
                            InvoicesCombobox = InvoicesCombobox.filter(x => x.text != invoice);
                            $("#Invoice").empty();
                            PopulateInvoiceCombobox();
                        }

                        //add new row to the table with info user wrote and selected                                                
                        row.append($('<td style="text-align: right !important;" id=' + invoice + ">" + invoiceText + "</td>"))
                            .append($("<td id=" + paymentId + "> $" + amountInvoice + "</td>"))
                            .append($('<td id="invoice" colspan="4"></td>')) //id to identify the row as invoiceRow
                            .append($("<td>" + '<span class="input-group-addon" style="border-style:none;background-color: gainsboro;"><a class="deleteInvoice" id="delete-' + paymentId + '"><i class="fa fa-trash"></i></a></span>' + "</td>"));

                        //row.appendTo('#InvoiceTable');
                        row.clone().appendTo('#NestedTables');
                        $("#InvoiceTable").hide();

                        //reset values of combobox and textbox from Invoices
                        $("#AmountInvoice").val(0.00);
                        $("#Invoice").val("");

                        //update value of the Payment Amount
                        amountPayment -= amountInvoice;
                        var newAmountPayment = parseFloat(amountPayment.toFixed(2));
                        amountPayment = newAmountPayment;
                        addNewPayment = false;
                    }
                    else {
                        totalInvoiceAmount -= amountInvoice;
                        toastr.error('Alerta - El monto total de facturas no puede ser mayor al monto total de pago.');
                        return;
                    }
                }
                else {
                    toastr.error('Alerta - El monto a pagar no debe ser mayor al monto de la factura.');
                    return;
                }
            }
            else {
                toastr.error('Alerta - El monto a pagar de la factura no debe ser mayor al monto total de pago.');
                return;
            }
        }
        else {
            toastr.error('Alerta - El monto a pagar de la factura debe ser mayor a 0.');
            return;
        }
    }
});

//Remove payments from table
$("#NestedTables").on('click', '.deletePayment', function () {
    var row = $(this).closest('tr');
    var paymentAmountText = row.find("td:eq(1)").text();
    var paymentAmount = parseFloat(paymentAmountText.substring(paymentAmountText.indexOf("$") + 1));
    var rowPaymentNestedTableId = row.find("td:eq(1)").attr('id');
    $(this).closest('tr').remove();

    //Update total payment amount on view
    totalAmount -= paymentAmount;
    if (parseFloat(amountPayment.toFixed(2)) != 0)
        amountPayment = parseFloat(totalAmount.toFixed(2));

    $('.TotalPayment').html('$' + totalAmount.toFixed(2));

    //Remove payment from hidden table
    $("#PaymentMethodTable tr").each(function () {
        var rowCheckPaymentTable = $(this).closest('tr');
        var rowCheckPaymentTableId = rowCheckPaymentTable.find("td:eq(1)").attr('id');

        if (rowPaymentNestedTableId === rowCheckPaymentTableId) {
            rowCheckPaymentTable.remove();
        }
    })

    //Remove all the invoices related to the payment that will be delete it from hidden table.  
    $("#InvoiceTable tr").each(function () {
        var rowCheck = $(this).closest('tr');
        var rowCheckPaymentId = rowCheck.find("td:eq(1)").attr('id');

        if (rowPaymentNestedTableId === rowCheckPaymentId)
            rowCheck.remove();
    })

    //Remove all the invoices related to the payment that will be delete it from the visible table.  
    $("#NestedTables tr").each(function () {
        var rowNestedPayment = $(this).closest('tr');
        var rowNestedPaymentId = rowNestedPayment.find("td:eq(1)").attr('id');
        var rowDeleteNested = document.getElementById('delete-' + rowNestedPaymentId);

        if (rowPaymentNestedTableId === rowNestedPaymentId) {
            rowDeleteNested.click();
        }
    })

    //If table is empty, populate combobox with previous options
    var rowCount = document.getElementById('NestedTables').rows.length;
    if (rowCount == 1) {
        $("#PaymentMethod").empty();
        PopulatePaymentsCombobox();
        $("#InvoicesSection").hide();
        $("#NestedTables").hide();
    }
});

//Remove Invoices related to payment from table
$("#NestedTables").on('click', '.deleteInvoice', function () {
    var row = $(this).closest('tr');
    var invoiceAmountText = row.find("td:eq(0)").text();
    var invoiceAmount = parseFloat(row.find("td:eq(1)").text().split('$')[1]);
    var invoice = row.find("td:eq(0)").attr('id');
    var rowPaymentId = row.find("td:eq(1)").attr('id');
    $(this).closest('tr').remove();

    //Update total invoice amount on view
    totalInvoiceAmount -= invoiceAmount
    $('.TotalInvoices').html('$' + totalInvoiceAmount.toFixed(2));

    //Update amount invoice in combobox            
    var newInvoiceText = row.find("td:eq(0)").text().split('$')[0];
    InvoicesComboboxList();
    var amountInvoiceText = InvoicesCombobox.find(x => x.text === invoice);
    if (amountInvoiceText != null) {
        amountInvoiceText = InvoicesCombobox.find(x => x.text === invoice).innerHTML;
        var updateInvoiceBalance = invoiceAmount + parseFloat(amountInvoiceText.substring(amountInvoiceText.indexOf("$") + 1));
        InvoicesCombobox.find(x => x.text === invoice).innerHTML = newInvoiceText + "$" + updateInvoiceBalance.toFixed(2);
    }
    else { //If invoice deleted is not in the combobox, add it
        var obj = {};
        obj.text = invoice;
        obj.innerHTML = newInvoiceText + "$" + invoiceAmount.toFixed(2);
        InvoicesCombobox.push(obj);
    }
    $("#Invoice").empty();
    PopulateInvoiceCombobox();

    $("#InvoiceTable tr").each(function () {
        var rowCheck = $(this).closest('tr');
        var rowCheckPaymentId = rowCheck.find("td:eq(1)").attr('id');
        var rowDelete = document.getElementById('a-' + rowPaymentId);

        if (rowPaymentId === rowCheckPaymentId) {
            rowCheck.remove();
        }
    })

    $("#PaymentMethodTable tr").each(function () {
        var rowCheck = $(this).closest('tr');
        var rowCheckPaymentId = rowCheck.find("td:eq(1)").attr('id');
        var rowDelete = document.getElementById('a-' + rowPaymentId);

        if (rowPaymentId === rowCheckPaymentId) {
            rowCheck.remove();
        }
    })

    //$("#NestedTables tr").each(function () {
    //    var rowCheck = $(this).closest('tr');
    //    var rowCheckPaymentId = rowCheck.find("td:eq(1)").attr('id');
    //    var rowDelete = document.getElementById('a-' + rowPaymentId);

    //    //if (rowPaymentId === rowCheckPaymentId) {
    //    //    rowDelete.click();
    //    //    return;
    //    //}
    //})
});

//Keep PaymentMethod list to manipulate later.
function PaymentMethodList() {
    var ddl = document.getElementById('PaymentMethod');
    for (i = 0; i < ddl.options.length; i++) {
        var paymentObj = {};
        paymentObj.text = ddl.options[i].value;
        paymentObj.innerHTML = ddl.options[i].text;

        PaymentsCombobox.push(paymentObj);
    }
}

//Keep Invoice list to manipulate later.
function InvoicesComboboxList() {
    InvoicesCombobox = [];
    var ddl = document.getElementById('Invoice');
    for (i = 0; i < ddl.options.length; i++) {
        var invoiceObj = {};
        invoiceObj.text = ddl.options[i].value;
        invoiceObj.innerHTML = ddl.options[i].text;

        InvoicesCombobox.push(invoiceObj);
    }
}

//Get Invoices from table
function GetInvoiceList() {
    var InvoiceList = []; //This list will be send to the controller
    $("#NestedTables tr").each(function () {
        var rowInvoice = $(this).closest('tr');
        var IsInvoice = rowInvoice.find("td:eq(2)").attr('id');
        if (IsInvoice != '' && IsInvoice === 'invoice') {
            var invoice = isPayment ? rowInvoice.find("td:eq(0)").attr('id') : rowInvoice.find("td:eq(1)").attr('id');

            //Mapping object with info of the invoice paid.
            var number = invoice;
            var serie = invoice;
            var obj = {};

            //obj.Balance = newinvoiceBalance;
            obj.PaymentInvoice = isPayment ? rowInvoice.find("td:eq(1)").text().split('$')[1] : rowInvoice.find("td:eq(2)").text().split('$')[1];
            obj.Invoice = invoice;
            obj.Number = number.replace(/\D/g, '');
            obj.Serie = serie.replace(/[0-9]/g, '');
            obj.PaymentId = isPayment ? rowInvoice.find("td:eq(1)").attr('id') : rowInvoice.find("td:eq(0)").attr('id');

            //Add to InvoiceList the info of the invoice paid.
            InvoiceList.push(obj);
        }
    });
    return InvoiceList;
}

//Get Payments from table
function GetPaymentsList() {
    var PaymentList = []; //This list will be send to the controller
    $("#NestedTables tr").each(function () {
        var rowPayment = $(this).closest('tr');
        var IsRowPayment = rowPayment.find("td:eq(6)").attr('id');
        
        if (IsRowPayment != '' && IsRowPayment === 'payment') {
            var amountText = rowPayment.find("td:eq(1)").text();            

            //Mapping object with info of the payment's table.
            var obj = {};
            obj.Amount = parseFloat(amountText.substring(amountText.indexOf("$") + 1));
            obj.PaymentDate = rowPayment.find("td:eq(3)").text();
            obj.ViewPaymentPMId = rowPayment.find("td:eq(1)").attr('id');
            obj.PaymentMethodId = rowPayment.find("td:eq(0)").attr('id');
            obj.OperationNumber = rowPayment.find("td:eq(2)").attr('id');
            obj.Bank = rowPayment.find("td:eq(4)").attr('id');
            obj.BankReference = rowPayment.find("td:eq(5)").text();

            //Add to PaymentList the info of the payment.
            PaymentList.push(obj);
        }
    });
    
    return PaymentList;
}

//Function to create random Id's for payments and delete all the invoices related to it.
function CreateRandomString(length) {
    var str = "";
    for (; str.length < length; str += Math.random().toString(36).substr(2));
    return str.substr(0, length);
}

function PopulatePaymentsCombobox() {
    for (var i = 0; i < PaymentsCombobox.length; i++) {
        var opt = PaymentsCombobox[i];
        var element = document.createElement("OPTION");
        element.innerHTML = opt.innerHTML;
        element.value = opt.text;
        payments.options.add(element);
    }
}

function PopulateInvoiceCombobox() {
    var invoices = document.getElementById("Invoice");
    for (var i = 0; i < InvoicesCombobox.length; i++) {
        var opt = InvoicesCombobox[i];
        var element = document.createElement("OPTION");
        element.innerHTML = opt.innerHTML;
        element.value = opt.text;
        invoices.options.add(element);
    }
}

//Only mapping necessary fields to create payments.
function MappingModel() {
    var Model = {};
    Model.Reference = document.getElementById('Reference').value;
    Model.Balance = document.getElementById('Balance').value;
    Model.Serie = document.getElementById('Serie').value;
    Model.Number = document.getElementById('Number').value;
    Model.AmountPayment = document.getElementById('AmountPayment').value;
    Model.Currency = document.getElementById('Currency').value;
    Model.TotalPayment = totalAmount;
    Model.IsPayment = document.getElementById('IsPayment').value;
    Model.PaymentPMId = document.getElementById('PaymentPMId').value;
    Model.PreviousPaymentNumber = document.getElementById('PreviousPaymentNumber').value;
    Model.Invoices = GetInvoiceList();
    Model.Payments = GetPaymentsList();
    Model.Customer = {
        RFC: document.getElementsByName('Customer.RFC')[0].value,
        Code: document.getElementsByName('Customer.Code')[0].value
    };
    
    return Model;
}

//Populate Combobox Invoices with the Invoices of the selected payment currency
function InvoicesByCurrency(url, paymentMethodId, customerCode) {
    $.ajax({
        url: url,
        Type: 'GET',
        data: { PaymentMethodId: paymentMethodId, CustomerCode: customerCode },
        dataType: 'json',
        success: function (data) {
            var balance = data.Json.Balance;
            var minDate = data.Json.MinDate;
            var currency = data.Json.Currency;
            $(".Currency").html(currency);
            $(".BalanceDisplay").html(data.Json.BalanceDisplay);
            $("#Invoice").empty();
            var invoices = document.getElementById("Invoice");
            var element = document.createElement("OPTION");
            element.innerHTML = "-- Seleccionar Factura --";
            element.value = "";
            invoices.options.add(element);
            if (data.Json.Invoices.length > 0) {
                for (var i = 0; i < data.Json.Invoices.length; i++) {
                    var opt = data.Json.Invoices[i];
                    element = document.createElement("OPTION");
                    element.innerHTML = opt.Text;
                    element.value = opt.Value;
                    invoices.options.add(element);
                }
            }
            else
                toastr.warning('Aviso - El cliente no tiene facturas por pagar con este tipo de moneda: ' + currency);

            $("#Currency").val(currency);
            $("#Balance").val(balance);
            $("#MinInvoiceDate").val(minDate);
        }
    });
}

//Function to process payments
function ProcessPayment(url, urlLoading) {
    var model = MappingModel();
    $.post({
        url: url,
        Type: 'POST',
        data: { "Model": model },
        dataType: 'json',
        success: function (data) {
            EndLoading();
            if (data.success && (data.Json.IsStamped || data.Json.IsStamped == 'true')) {
                var pdf = 'data:application/pdf;base64,' + data.Json.PdfEncoded;
                $("#Pdf-iframe").attr("data", pdf);
                $('#ModalPdf').modal('show');
            }
            else if (data.success) {
                swal({
                    title: 'Pago',
                    text: "Pago procesado pero no timbrado. Pago " + model.Serie + model.Number,
                    type: "warning"
                }, function () {
                    window.location.href = urlLoading;
                });
            }
            else {
                swal({
                    title: 'Error',
                    text: "Ocurrio un error, contacte a sistemas para solucionarlo.",
                    type: "error"
                });
            }
        }
    });
}

//Only used when substitute payment
function ValidateDate() {
    var currentDate = stringToDate(GetCurrentDate());
    var minDate = stringToDate($("#MinInvoiceDate").val());
    var datePayment = stringToDate($("#AmountPaymentDate").val());

    if (datePayment < minDate) {
        toastr.error('Alerta - Fecha de pago debe ser mayor o igual a la fecha de la primer factura. (' + $("#MinInvoiceDate").val() + ')');
        return false;
    }

    if (datePayment > currentDate) {
        toastr.error('Alerta - Fecha de pago no debe ser mayor al día actual.');
        return false;
    }

    return true
}