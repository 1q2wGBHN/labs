﻿
//-------------------------------SELECT CASCADING-------------------------//
var currentCities = [];
//var BATTUTA_KEY = "a559cbba42872e578504e2383032528c"
var BATTUTA_KEY = "f33c6ea68e4eb8ec9968fda4c2763202"

// Populate country select box from battuta API
url = "https://battuta.medunes.net/api/country/all/?key=" + BATTUTA_KEY + "&callback=?";
$.getJSON(url, function (countries) {
    
    $('#Countries').select();
    //loop through countries..
    $.each(countries, function (key, country) {
        $("<option></option>")
                        .attr("value", country.code)
                        .append(country.name)
                        .appendTo($("#Countries"));

    });
    // trigger "change" to fire the #States section update process
    $("#Countries").select('update');
    $("#Countries").trigger("change");


});

$("#Countries").on("change", function () {

    countryCode = $("#Countries").val();

    // Populate country select box from battuta API
    url = "https://battuta.medunes.net/api/region/"
    + countryCode
    + "/all/?key=" + BATTUTA_KEY + "&callback=?";

    $.getJSON(url, function (regions) {
        $("#States option").remove();
        //loop through regions..
        $.each(regions, function (key, region) {
            $("<option></option>")
                            .attr("value", region.region)
                            .append(region.region)
                            .appendTo($("#States"));
        });
        // trigger "change" to fire the #States section update process
        $("#States").select('update');
        $("#States").trigger("change");

    });

});
$("#States").on("change", function () {

    // Populate country select box from battuta API
    countryCode = $("#Countries").val();
    region = $("#States").val();
    url = "https://battuta.medunes.net/api/city/"
    + countryCode
    + "/search/?region="
    + region
    + "&key="
    + BATTUTA_KEY
    + "&callback=?";

    $.getJSON(url, function (cities) {
        currentCities = cities;
        var i = 0;
        $("#Cities option").remove();

        //loop through regions..
        $.each(cities, function (key, city) {
            $("<option></option>")
                            .attr("value", i++)
                            .append(city.city)
                    .appendTo($("#Cities"));
        });
        // trigger "change" to fire the #States section update process
        $("#Cities").select('update');
        $("#Cities").trigger("change");

    });

});
$("#Cities").on("change", function () {
    currentIndex = $("#Cities").val();
    currentCity = currentCities[currentIndex];
    city = currentCity.city;
    region = currentCity.region;
    country = currentCity.country;
    lat = currentCity.latitude;
    lng = currentCity.longitude;
    $("#location").html('<i class="fa fa-map-marker"></i> <strong> ' + city + "/" + region + "</strong>(" + lat + "," + lng + ")");
});
//-------------------------------END OF SELECT CASCADING-------------------------//