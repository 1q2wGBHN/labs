﻿"use strict";
class CreateExpensesViewModel {
    constructor() {

        this.name = ko.observable("create");
        this.search_results = ko.observableArray();
        this.expense_list = ko.observableArray();
        this.search_text_input = ko.observable();
        this.to_new_projects = ko.observable(false);
        this.to_new_projects.subscribe(c => console.log(c));
        this.to_new_projects(false);
        this.search_text_input.extend({ rateLimit: { timeout: 300, method: "notifyWhenChangesStop" } });
        this.search_text_input.subscribe(this.search_on_text_input.bind(this));
        this.total_quantity = ko.observable(0);
        this.total_estimate_price = ko.observable(0);

        this.search_item_onclick = this.search_item_onclick.bind(this);
        this.registry_expense = this.registry_expense.bind(this);
        this.on_change_quantity = this.on_change_quantity.bind(this);
        this.on_change_estimate_price = this.on_change_estimate_price.bind(this);
        this.on_upload_ok = () => { };
        this.txt_msg = ko.observable('');
    }
    search_item_onclick(item) {
        console.log(item);
        this.search_text_input("");
        if (this.expense_list().find(e =>
            e.part_number === item.part_number
            && e.supplier_name === item.supplier_name)) {
            this.search_text_input("");
            toastr.warning("este elemento ya esta en la lista");
            return;
        }
        this.expense_list.push(item);

    }

    async search_on_text_input(value) {
        console.log(value);
        if (!value || value.length <3) {
            this.search_results.removeAll();
            this.txt_msg('');
            return;
        }
        const res = await Requests
            .get("/ExpenseRequisitions/ActionSearchItems", { search: value })
            .send_result();
        if (res.Ok) {
            this.search_results.removeAll();
            ko.utils.arrayPushAll(this.search_results, res.Data);
            if (res.Data.length === 0) this.txt_msg('No se encontraron productos');
            else this.txt_msg('');
        }   
    }
    async registry_expense() {
        if (this.expense_list().length < 1) return;
        const list = this.expense_list().filter(e => e.quantity > 0);
        let msg = "¿Esta seguro de registrar esta requisición?";
        if (this.total_estimate_price() >= 10000 || this.to_new_projects()) {
            msg += "\nSera necesaria autorizacion de nuevos proyectos";
        } else if (this.total_estimate_price() >= 5000) {
            msg += "\nSera necesaria autorizacion de distrital";
        }

        swal({
            title: "Registro de Requisicion",
            text: msg,
            type: "warning",
            showCancelButton: true,
            cancelButtonText: "Cancelar",
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Aceptar"

        },
            async (va) => {
                if (!va) return;
                StartLoading();
                const r = await Requests.post("/ExpenseRequisitions/ActionExpenseStart")
                    .data({
                        Username: null,
                        ProgramId: window.location.pathname.split('/').pop(),
                        Products: this.expense_list(),
                        ToNewProjects: this.to_new_projects(),
                    }).send();
                if (r.Ok) {
                    swal({
                            title: "Registro de Requisicion",
                            text: "Requisición registrada exitosamente",
                            type: "success",
                            showCancelButton: false,
                            cancelButtonText: "Cancelar",
                            confirmButtonColor: "#DD6B55",
                            confirmButtonText: "Aceptar"

                        },
                        async (va) => {});
                    
                    this.search_results.removeAll();
                    this.expense_list.removeAll();
                    this.search_text_input("");
                    this.to_new_projects(false);
                    this.on_upload_ok();
                    this.total_quantity(0);
                    this.total_estimate_price(0);
                }
                EndLoading();

            });
    }

    on_change_quantity() {
        this.on_change_estimate_price();
        
    }
    on_change_estimate_price(e) {
        console.log(e); 
        this.total_quantity(this.expense_list()
            .map(e => parseFloat(e.quantity || 0))
            .reduce((a, b) => a + b).toFixed(2));
        this.total_estimate_price(this.expense_list()
            .map(e => parseFloat(e.quantity || 0) * parseFloat(e.estimate_price || 0))
            .reduce((a, b) => a + b).toFixed(2));
    }

}


