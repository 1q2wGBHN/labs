﻿
class ListExpensesViewModel {
    constructor() {
        this.name = ko.observable("nombre classzxczxc");
        this.requests_list = ko.observableArray();
        this.history_list = ko.observableArray();
        this.show_ended = ko.observable(false);
        this.date0 = ko.observable("");
        this.date1 = ko.observable("");
        
        this.send_approval = this.send_approval.bind(this);
        this.upload_xmls = this.upload_xmls.bind(this);
        this.change_display = this.change_display.bind(this);
        this.on_details_click = this.on_details_click.bind(this);
        this.confirm_cancel = this.confirm_cancel.bind(this);
        this.clear_filters = this.clear_filters.bind(this);
        
        this.s1=this.show_ended.subscribe(()=>this.load_list());
        this.s2=this.date0.subscribe(()=>this.load_list());
        this.s3=this.date1.subscribe(()=>this.load_list());

        this.load_list();
    }
    async load_list() {
        const r2 = await Requests
            .get("/ExpenseRequisitions/ActionRequestsHistoryList",{
                    show_ended: this.show_ended(),
                    date0: this.date0(),
                    date1: this.date1(),
                })
            .send();
        if (r2.Ok) {
            this.requests_list.removeAll();
            r2.Data.forEach(e => {
                e.is_open = ko.observable(false);
                e.expense_list = ko.observableArray();
                e.total_quantity = ko.observable(e.total_quantity.toFixed(2));
                e.total_estimate_price = ko.observable(e.total_estimate_price.toFixed(2));
            });
            ko.utils.arrayPushAll(this.requests_list, r2.Data);
        }
        const r = await Requests
            .get("/ExpenseRequisitions/ActionHistoryList",{
                    show_ended: this.show_ended(),
                    date0: this.date0(),
                    date1: this.date1(),
                })
            .send();
        if (r.Ok) {
            this.history_list.removeAll();
            r.Data.forEach(e => {
                e.is_open = ko.observable(false);
                e.expense_list = ko.observableArray();
                e.total_quantity = ko.observable(e.total_quantity.toFixed(2));
                e.total_estimate_price = ko.observable(e.total_estimate_price.toFixed(2));
            });
            ko.utils.arrayPushAll(this.history_list, r.Data);
        }
        


    }
    change(data) {

    }

    send_approval(b, data) {
        if (!data.remark || data.remark.length < 8) {
            toastr.error("Debe escribir un comentario de por lo menos 8 caracteres");
            return;
        }
        swal({
                title: "Confirmar acción",
                text: b ? "Aprobar petición" : "Rechazar petición",
                type: "warning",
                showCancelButton: true,
                cancelButtonText: "Cancelar",
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Aceptar"

            },
            async (va) => {
                if (!va) return;
                StartLoading();
                data.new_approval_status = b ? "aprobado" : "rechazado";
                data.new_remark = data.remark;
                data.program_id = window.location.pathname.split("/").pop();
                const r = await Requests.post("/ExpenseRequisitions/ActionExpenseUpdate")
                    .data(data).send();
                if (r.Ok) {
                    toastr.success("Requisición actualizada exitosamente");
                }
                this.load_list();
                EndLoading();
            });
        
    }

    async upload_xmls(data,i) {
        try {
            console.log("asdfasdfasdfa");
            console.log(data);
            const node = document.getElementById('input_' + i());
            const files = Array.from(node.files);

            if (!files || files.length === 0) {
                toastr.error('No se han seleccionado archivos');
                return;
            }
            const strs = [];
            for (let j = 0; j < files.length; j++) {
                strs.push({
                    xml: await this.get_base64(files[j]),
                    xml_name: files[j].name,
                } );
            }
            const f = {
                history:data,
                files: strs,
                program_id: window.location.pathname.split("/").pop(),
                    
            }

            swal({
                    title: "Confirmar acción",
                    text:  "Agregar XML",
                    type: "warning",
                    showCancelButton: true,
                    cancelButtonText: "Cancelar",
                    confirmButtonColor: "#DD6B55",
                    confirmButtonText: "Aceptar"

                },
                async (va) => {
                    
                    if (!va) return;
                    const r = await Requests.post("/ExpenseRequisitions/ActionAddXml")
                        .data(f).send();
                    if (r.Ok) {
                        toastr.success("Requisición actualizada exitosamente");
                    }
                    this.load_list();
                });
        } catch (e) {
            console.log(e);
            toastr.error('Error al leer archivos');
        }
            
    }

    get_base64(file) {
        return new Promise((resolve, reject) => {
            const reader = new FileReader();
            reader.readAsText(file);
            reader.onload = () => resolve(window.btoa(reader.result));
            reader.onerror = error => reject(error);
        });
    }
    async on_details_click(prefix_btn, prefix,data, index) {
        
        if (this.change_display(prefix_btn+index, prefix + index)) {
            
            const r = await Requests
                .get("/ExpenseRequisitions/ActionGetDetails", {folio:data.folio})
                .send();
            if (r.Ok) {
                
                data.total_estimate_price(
                    r.Data.map(e => parseFloat(e.quantity) * parseFloat(e.estimate_price))
                        .reduce((a, b) => a + b).toFixed(2));
                data.total_quantity(
                    r.Data.map(e => parseFloat(e.quantity))
                        .reduce((a, b) => a + b).toFixed(2));
                r.Data.forEach(e => {
                    e.quantity = e.quantity.toFixed(2);
                    e.estimate_price = e.estimate_price.toFixed(2);
                });
               
                data.expense_list.removeAll();
                
                ko.utils.arrayPushAll(data.expense_list, r.Data);
            }
        }
    }

    change_display(btn_id, id) {
        const btn_node = document.getElementById(btn_id);
        btn_node.classList.remove("arrow_down");
        btn_node.classList.remove("arrow");
        const node = document.getElementById(id);
        if (!node) return false;
        if (node.style.display === "") {
            node.style.display = "none";
            btn_node.classList.add("arrow");
            return false;
        } else {
            node.style.display = "";
            btn_node.classList.add("arrow_down");
            return true;
        }
        
    }

    async confirm_cancel(data) {
        console.log(data);
        const r = await Requests
            .post("/ExpenseRequisitions/ActionConfirmCancel")
            .data({ "folio": data.folio })
            .send();
        if (r.Ok) {
            toastr.success("Requisición actualizada exitosamente");
        }
        this.load_list();
    }
    clear_filters(){
        this.s1.dispose();
        this.s2.dispose();
        this.s3.dispose();
        this.date0("");
        this.date1("");
        this.show_ended("");
        this.s1=this.show_ended.subscribe(()=>this.load_list());
        this.s2=this.date0.subscribe(()=>this.load_list());
        this.s3=this.date1.subscribe(()=>this.load_list());
    }
}


