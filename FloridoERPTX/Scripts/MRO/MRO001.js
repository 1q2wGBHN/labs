﻿
var listAddServices = [];
var XMLtoPost = [];
var DocumentToPost = [];

$(document).ready(function () {
    window.setTimeout(function () { $("#fa").click(); }, 1000);
    var servicios = JSON.parse($("#model").val());
    table.clear();
    table.rows.add($(servicios));
    table.columns.adjust().draw();
    $('#TableDiv').animatePanel();
});

$("#ValidDate1").datepicker({
    autoclose: true,
    todayHighlight: true,
    showButtonPanel: true,
}).on("changeDate", function (e) {
    searchService();
});

$("#ValidDate2").datepicker({
    autoclose: true,
    todayHighlight: true,

}).on("changeDate", function (e) {
    searchService();
});

function dispFile(contents) {
    document.getElementById('contents').innerHTML = contents
}

function clickElem(elem) {
    var eventMouse = document.createEvent("MouseEvents")
    eventMouse.initMouseEvent("click", true, false, window, 0, 0, 0, 0, 0, false, false, false, false, 0, null)
    elem.dispatchEvent(eventMouse)
}

function openFile(func) {
    readFile = function (e) {
        var file = e.target.files;
        convertToBase64XML(file);
    }
    fileInput = document.createElement("input")
    fileInput.id = 'fileXML'
    fileInput.type = 'file'
    fileInput.style.display = 'none'
    fileInput.accept = '.xml'
    fileInput.onchange = readFile
    fileInput.multiple = true
    fileInput.func = func
    document.body.appendChild(fileInput)
    clickElem(fileInput)
}

//Funcion para convertir XML => base 64
var b = 0;
function convertToBase64XML(selectedFile) {
    if (selectedFile != null) {
        if (selectedFile.length > 0) {
            for (var i = 0; i < selectedFile.length; i++) {
                var xmlName = selectedFile[i].name
                var services = XMLtoPost.filter(e => e.xml_name == xmlName).length
                if (services == 0) {
                    var fileToLoad = selectedFile[i];
                    var xml_name = fileToLoad.name
                    var fileReader = new FileReader();
                    XMLtoPost = [...XMLtoPost, { xml: "", xml_name: xml_name }]
                    fileReader.onload = function (fileLoadedEvent) {
                        var base64 = window.btoa(unescape(encodeURIComponent(fileLoadedEvent.target.result)));
                        var xml = base64;
                        XMLtoPost[b].xml = xml;
                        b++;
                    };
                    fileReader.readAsText(fileToLoad);
                    var Service = { xml: fileToLoad.name };
                    listAddServices.push(Service);
                    tableXML.clear().draw();
                    tableXML.rows.add($(listAddServices));
                    tableXML.columns.adjust().draw();
                }
                else
                    toastr.warning("No se pudo leer el archivo.");
            }
            $("#tableXML").show();
        }
        $("#fileXML").val("");
    }
}

//Funcion para convertir PDF => base 64
function convertToBase64Document(id, service_id) {
    var selectedFile = document.getElementById("file" + id + "").files;
    $("#lbl" + id + "").text("Archivo");
    $("#divPDF" + id).show();
    var pdf;
    if (selectedFile.length > 0) {
        for (var i = 0; i < selectedFile.length; i++) {
            var fileToLoad = selectedFile[i];
            var fileReader = new FileReader();
            var base64;
            fileReader.onload = function (fileLoadedEvent) {
                base64 = fileLoadedEvent.target.result;
                pdf = base64;
                var DocumentPost =
                {
                    pdf: pdf
                };
                DocumentToPost.push(DocumentPost);
                $("#divPDF" + id).show();
                $("#div" + id).hide();
                $('#checkBox' + id).iCheck('check');
                $('#checkBox' + id).iCheck({
                    checkboxClass: 'icheckbox_square-green',
                }).on('ifClicked', function () {
                    $(this).trigger("change");
                });
                var date = $('#date' + id + '').val();
                var commentary = $('#o' + id + '').val();
                var index = list.findIndex(elements => elements.equipment_id == id);
                if (index >= 0) {
                    list[index] = { equipment_id: id, service_date_finished: date, service_document: pdf, service_commentary: commentary, service_id: service_id };
                }
                else {
                    list.push({ equipment_id: id, service_date_finished: date, service_document: pdf, service_commentary: commentary, service_id: service_id });
                }
            };
            fileReader.readAsDataURL(fileToLoad);
        }
    }
    return pdf;
}

//Funcion para guardar los datos modificados o cancelar
function save(status) {
    if (list.length != 0 || status == 8) {
        if (XMLtoPost.length != 0 || status == 8) {
            StartLoading();
            console.table(ServicesToPost)
            $.ajax({
                type: "POST",
                url: "/ServicesCalendarHeader/ActionAddXML/",
                data: { "listDetails": list, "list": XMLtoPost, "status": status, "service_id": service_id },
                success: function (response) {
                    if (response == "SF") {
                        EndLoading();
                        SessionFalse("Terminó tu sesión")
                    }
                    if (response) {
                        EndLoading();
                        if (status == 8) {
                            swal({
                                title: 'Cancelado Correctamente',
                                text: "Servicio Cancelado Correctamente.",
                                type: "success"
                            }, function (isConfirm) {
                                if (isConfirm)
                                    clearPage();
                            });
                        } else {
                            swal({
                                title: 'Gurdado Correctamente',
                                text: "Servicio Guardado correctamente.",
                                type: "success"
                            }, function (isConfirm) {
                                if (isConfirm)
                                    clearPage();
                            });
                        }
                        $('#table_products').DataTable().clear().draw();
                    }
                    else {
                        EndLoading();
                        if (response != null || response != "")
                            toastr.error(response);
                        else
                            toastr.error("Error al actualizar la información. Verifique no tenga la misma maquinaria el mismo día. Contacte a sistemas.");
                    }
                },
                error: function () {
                    EndLoading();
                    toastr.error("DESCONOCIDO, CONTACTA A SISTEMAS.")
                }
            });

        } else
            toastr.warning("Agregue al menos un archivo xml");
    } else
        toastr.warning("Registre al menos una fecha de servicio");
}

//Tabla de XML en modal
var tableXML = $('#tableXML').DataTable({
    "paging": false,
    "ordering": false,
    "info": false,
    "searching": false,
    oLanguage:
    {
        "sZeroRecords": "",
        "sEmptyTable": "",
    },
    columns:
        [
            { data: 'xml' },
            { data: null }
        ],
    columnDefs:
        [{
            targets: [1],
            render: function (data, type, full, meta) {
                return `<button class="btn btn-xs btn-danger btn-circle" style="text-align:center;border-right-width: 0px;padding-right: 0px;padding-left: 0px;padding-top: 0px;padding-bottom: 0px;margin-top: 5px;" onclick="deleteXML('${data.xml}')"><i class="fa fa-times"></i></button>`;
            }
        }],
});

function deleteXML(xml) {
    if (b != 0) {
        b--;
    }
    listAddServices = listAddServices.filter(x => x.xml != xml);
    XMLtoPost = XMLtoPost.filter(x => x.xml_name != xml);
    tableXML.clear()
    tableXML.rows.add(listAddServices);
    tableXML.columns.adjust().draw();
    if (XMLtoPost.length == 0)
        $("#tableXML").hide();
    else
        $("#tableXML").show();
}

var tableServices = $('#tableXML2').DataTable({
    "paging": false,
    "ordering": false,
    "info": false,
    "searching": false,
    oLanguage:
    {
        "sZeroRecords": "",
        "sEmptyTable": "",
    },
    columns:
        [
            { data: 'service_id' },
            { data: 'equipment_description' },
            { data: null },
            { data: null },
            { data: null }
        ],
    columnDefs:
        [{
            targets: [2],
            render: function (data, type, full, meta) {
                return `<div class="input-group date"><span class="input-group-addon"><span class="fa fa-calendar"></span></span><input class="datepicker form-control" autocomplete="off" id="date${data.equipment_id}" onchange="set(${data.equipment_id},${data.service_id})" value = "${data.service_date_finished2}" /></div>`;

            }
        }, {
            targets: [3],
            render: function (data, type, full, meta) {
                return `<div class="col-lg-3"><textarea class="form-control" autocomplete="off" maxlength="249" style=resize:vertical;min-height:34px;max-height:150px;height:34px id="o${data.equipment_id}" onchange="set(${data.equipment_id},${data.service_id})" value = "${data.service_commentary}"></textarea></div>`;
            }
        }, {
            targets: [4],
            'createdCell': function (td, data, rowData, row, col) {
                $(td).attr('id', 'td' + data.equipment_id);
            },
            render: function (data, type, full, meta) {
                return `<div id="divPDF${data.equipment_id}" style="display:none;"><label><input class="i-checks" type = "checkbox" id="checkBox${data.equipment_id}" onchange = "deletePDF('${data.equipment_id}')"  checked></input></label></div><div class="col-lg-5" id = "div${data.equipment_id}"><label><input id="file${data.equipment_id}" type="file" style="display: none;" onchange='convertToBase64Document(${data.equipment_id},${data.service_id})' accept=".pdf"><i class="fa fa-clipboard btn btn-default btn-xl"></i></label></div>`;
            }
        }, {
            targets: [1, 2, 3, 4],
            width: "1%"
        }],
});

function deletePDF(id) {
    if ($('#checkBox' + id).is(':checked')) {
        $("#divPDF" + id + "").hide();
        $("#div" + id).show();
        var index = list.findIndex(elements => elements.equipment_id == id);
        list[index].service_document = ""
        $('#checkBox' + id).is(':unchecked')
    }
}

function swalPDF(id) {
    $("#file" + id).click();
}

//Ocultar service_id
tableServices.column(0).visible(false);
list = [];
//funcion para obtener los datos de cada registro de datatable modal
function set(id, service_id) {
    var date = $('#date' + id + '').val();
    var commentary = $('#o' + id + '').val();
    var index = list.findIndex(elements => elements.equipment_id == id);
    if (index >= 0) {
        if (list[index].service_document != null) {
            list[index] = { equipment_id: id, service_date_finished: date, service_document: list[index].service_document, service_commentary: commentary, service_id: service_id };
        } else {
            list[index] = { equipment_id: id, service_date_finished: date, service_commentary: commentary, service_id: service_id };
        }
    }
    else {
        list.push({ equipment_id: id, service_date_finished: date, service_commentary: commentary, service_id: service_id });
    }
}

function clearModal() {
    $(".footer").show();
}

var service_id = 0;
//Funcion para mostrar modal del servicio
function modal(id, status) {
    b = 0;
    XMLtoPost = [];
    $(".footer").hide();
    service_id = id;
    $("#no_status").text("Datos de Servicio No : " + id);
    $(".datepicker").datepicker({
        autoclose: true,
        todayHighlight: true,
        showButtonPanel: true,
    }).on("changeDate", function (e) {
    });
    $('#modalService').modal('show');

    $("#status select").val(status);
}

//Tabla principal de servicios
var table = $('#TableTransfers').DataTable({
    oLanguage: {
        "sProcessing": "Procesando...", "sLengthMenu": "Mostrar _MENU_ registros", "sZeroRecords": "No se encontraron resultados", "sEmptyTable": "Ningún dato disponible en esta tabla", "sInfo": "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros", "sInfoEmpty": "Mostrando registros del 0 al 0 de un total de 0 registros", "sInfoFiltered": "(filtrado de un total de _MAX_ registros)", "sInfoPostFix": "", "sSearch": "Buscar:", "sUrl": "", "sInfoThousands": ",", "sLoadingRecords": "Cargando...", "oPaginate": { "sFirst": "Primero", "sLast": "Último", "sNext": "Siguiente", "sPrevious": "Anterior" }, "oAria": { "sSortAscending": ": Activar para ordenar la columna de manera ascendente", "sSortDescending": ": Activar para ordenar la columna de manera descendente" }
    },
    responsive: true,
    autoWidth: true,
    order: [[1, "asc"]],
    dom: "<'row'<'col-sm-4'l><'col-sm-4 text-left'B><'col-sm-4'f>t<'col-sm-6'i><'col-sm-6'p>>",
    lengthMenu: [[10, 25, 50, -1], [10, 25, 50, "Todos"]],
    buttons: [],
    columns: [
        {
            "class": "details-control",
            "orderable": false,
            "data": null,
            "defaultContent": "",
        },
        { data: 'service_id' },
        { data: 'service_date' },
        { data: 'service_status_' },
    ],
    columnDefs: [{
        targets: [0, 1, 2, 3],
        width: "1%"
    }]
});

var detailRows = [];
$('#TableTransfers tbody').on('click', 'tr td.details-control', function () {
    var tr = $(this).closest('tr');
    var row = table.row(tr);

    if (row.child.isShown()) {
        row.child.hide();
        tr.removeClass('details');
    }
    else {
        if (table.row('.details').length) {
            $('.details-control', table.row('.details').node()).click();
        }
        row.child(format(row.data())).show();
        tr.addClass('details');
    }
});

table.on('draw', function () {
    $.each(detailRows, function (i, id) {
        $('#' + id + ' td.details-control').trigger('click');
    });
});

//Tabla detalles
function format(d) {
    service_id = d.service_id;
    var detailItems = "";
    var Apro = '';
    var tabledetail = $('<div/>').addClass('loading').text('Cargando Datos...');
    $.ajax({
        url: "/ServicesCalendarHeader/ActionGetServicesDetail/",
        data: { "id": d.service_id },
        type: "POST",
        success: function (data) {
            if (data == "SF")
                SessionFalse("Terminó tu sesión");
            else {
                listAddServices = [];
                ServicesToPost = [];
                var DocumentToPost = [];
                $("#tableXML").hide();
                tableServices.clear();
                tableServices.rows.add(data);
                tableServices.columns.adjust().draw();
                $.each(data, function (index, value) {

                    if (value.quotation_document != null) {
                        detailItems += "<tr><td>" + value.equipment_description + "</td><td>" + value.supplier_name + "</td><td>$" + value.service_cost + "</td>" + "<td>" + value.currency + "</td>" + "<td>" + value.service_department + "</td><td><button class='btn btn-xs btn-outline btn-warning btn-edit text-center' type='button' onclick= viewDocument('" + value.quotation_document + "')><i class='fa fa-file-o'></i> Cotización</button></td>";
                    }
                    else {
                        detailItems += "<tr><td>" + value.equipment_description + "</td><td>" + value.supplier_name + "</td><td>$" + value.service_cost + "</td>" + "<td>" + value.currency + "</td>" + "<td>" + value.service_department + "</td><td>No tiene cotizacón</td>" + "</td>";
                    }
                    if (d.service_status_ == "Programado") {
                        //Apro = '<div class="row pull-right"><div class="col-lg-12"> <button id="btn-cancel" type="button" class="btn btn-sm btn-success" onclick="modal(' + value.service_id + ',' + value.service_status + ')"><i class="fa fa-check"></i> Terminar</button>&nbsp;&nbsp;&nbsp;<button id="btn-cancel" type="button" class="btn btn-sm btn-danger" onclick="cancelar()"><i class="fa fa-close"></i> Cancelar</button>&nbsp;&nbsp;&nbsp;</div></div>';
                        Apro = '<div class="row pull-right"><div class="col-lg-12"> <button id="btn-cancel" type="button" class="btn btn-sm btn-success" onclick="modal(' + value.service_id + ',' + value.service_status + ')"><i class="fa fa-check"></i> Terminar</button></div></div>';
                    }

                });

                if (data.length != 0) {
                    tabledetail.html('<table id="tabledetail" class="table table-striped table-bordered table-hover" style="width:100%"><thead><tr><th>Equipo</th><th>Proveedor</th><th>Costo aproximado</th><th>Moneda</th><th>Departamento</th><th>Ver Cotización</th></tr></thead><tbody>' + detailItems + '</tbody></table>' + Apro).removeClass('loading');
                    reloadStyleTable();
                }
                tabledetail.html('<table id="tabledetail" class="table table-striped table-bordered table-hover" style="width:100%"><thead><tr><th>Equipo</th><th>Proveedor</th><th>Costo aproximado</th><th>Moneda</th><th>Departamento</th><th>Ver Cotización</th></tr></thead><tbody>' + detailItems + '</tbody></table>' + Apro).removeClass('loading');
                reloadStyleTable();
            }
        }
    });
    return tabledetail
}

//Funcion para ver el documento PDF de cotizacion
function viewDocument(id) {
    $("#modalReferenceBody").html("<iframe width='100%' height='550px' src='data:application/pdf;base64, " + id + "'></iframe>")
    $("#modalReference").modal('show');
}

//Funcion para cancelar servicio
function cancelar() {
    swal({
        title: "¿Esta seguro que desea Cancelar el servicio?",
        text: "El no. de servicio : " + service_id + " será cancelado",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "Continuar",
        cancelButtonText: "Cancelar"
    }, function (isConfirm) {
        if (isConfirm) {
            setTimeout(function () { save(8); }, 400);
        }
    });
}

//Recargar tabla
function reloadStyleTable() {
    $('#tabledetail').DataTable({
        oLanguage: {
            "sProcessing": "Procesando...", "sLengthMenu": "Mostrar _MENU_ registros", "sZeroRecords": "No se encontraron resultados", "sEmptyTable": "Ningún dato disponible en esta tabla", "sInfo": "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros", "sInfoEmpty": "Mostrando registros del 0 al 0 de un total de 0 registros", "sInfoFiltered": "(filtrado de un total de _MAX_ registros)", "sInfoPostFix": "", "sSearch": "Buscar:", "sUrl": "", "sInfoThousands": ",", "sLoadingRecords": "Cargando...", "oPaginate": { "sFirst": "Primero", "sLast": "Último", "sNext": "Siguiente", "sPrevious": "Anterior" }, "oAria": { "sSortAscending": ": Activar para ordenar la columna de manera ascendente", "sSortDescending": ": Activar para ordenar la columna de manera descendente" }
        },
        columnDefs: []
    });
}

function clearPage() {
    clearVariables();
    $("#closeModal").click();
    $("#ValidDate1").val('');
    $("#ValidDate2").val('');
    $('#SelectOptions').val($("#SelectOptions :first").val());
    $.ajax({
        type: "POST",
        url: "/ServicesCalendarHeader/ActionGetServices/",
        data: {},
        success: function (response) {
            if (response == "SF") {
                EndLoading();
                SessionFalse("Terminó tu sesión")
            } else {
                EndLoading();
                table.clear();
                table.rows.add(response);
                table.columns.adjust().draw();
                $('#TableDiv').animatePanel();
            }
        },
        error: function () {
            EndLoading();
            toastr.error("DESCONOCIDO, CONTACTA A SISTEMAS.")
        }
    });
}

function clearVariables() {
    listAddServices = [];
    XMLtoPost = [];
    DocumentToPost = [];
    list = [];
}

function searchService() {
    let status = $("#SelectOptions").val()
    var url;
    var data;
    if ($('#ValidDate1').val() != "" & $('#ValidDate2').val() != "" || ($('#ValidDate1').val() == "" & $('#ValidDate2').val() == "")) {
        if (moment($('#ValidDate1').val()) <= moment($('#ValidDate2').val()) || ($('#ValidDate1').val() == "" & $('#ValidDate2').val() == "")) {
            if (status == "Todos") {
                url = "/ServicesCalendarHeader/ActionGetServicesHeaderByDate/";
                if ($('#ValidDate1').val() == "" & $('#ValidDate2').val() == "") {
                    data = { "start": moment().add(-7, 'days').format("MM/DD/YYYY"), "finish": moment().add(1, 'days').format("MM/DD/YYYY") }
                } else {
                    data = { "start": $('#ValidDate1').val(), "finish": $('#ValidDate2').val() }
                }
            }
            else {
                url = "/ServicesCalendarHeader/ActionGetServicesHeaderByDateAndStatus/";
                data = { "start": $('#ValidDate1').val(), "finish": $('#ValidDate2').val(), "status": $("#SelectOptions").val() }
                if ($('#ValidDate1').val() == "" & $('#ValidDate2').val() == "") {
                    data = { "start": moment().add(-7, 'days').format("MM/DD/YYYY"), "finish": moment().add(1, 'days').format("MM/DD/YYYY"), "status": $("#SelectOptions").val() }
                }
            }

            $.ajax({
                type: "POST",
                url: url,
                data: data,
                success: function (data) {
                    if (data == "SF")
                        SessionFalse("Terminó tu sesión");
                    else if (data == "error")
                        toastr.error("Error inesperado contactar a sistemas.");
                    else if (data.length == 0) {
                        table.clear();
                        table.rows.add($(data));
                        table.columns.adjust().draw();
                        $('#TableDiv').animatePanel();
                        toastr.warning("No existen servicios programados en el rango de fecha");
                    }
                    else {
                        table.clear();
                        table.rows.add($(data));
                        table.columns.adjust().draw();
                        $('#TableDiv').animatePanel();
                    }
                },
                error: function (returndate) {
                    toastr.error('Alerta - Error inesperado contactar a sistemas.');
                }
            });
        }
        else {
            toastr.error("Fecha final no puede ser menor a la inicial.");
            document.getElementById('ValidDate1').value = '';
            document.getElementById('ValidDate2').value = '';
            $('#ValidDate1').focus();
        }
    }
}