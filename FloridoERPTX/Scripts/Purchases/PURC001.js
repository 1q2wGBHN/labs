﻿
var itemList = [];
var itemPurchase;
const sesion = "Se termino su sesion.";
const fomartmoney = money => decimal => new Intl.NumberFormat("en-US", { style: "currency", currency: "USD", currencyDisplay: "symbol", maximumFractionDigits: decimal }).format(money);

$("#selectSupplier").select2({
    minimumInputLength: 3,
    allowClear: true,
    placeholder: "Seleccione un proveedor",
    initSelection: function (element, callback) {
        callback({ id: "", text: "Seleccione una opcion" });
    },
    ajax: {
        url: "/Suppliers/ActionSearchSupplier/",
        dataType: 'json',
        type: "GET",
        quietMillis: 50,
        data: function (Filter) {
            return {
                Filter: Filter
            };
        },
        results: function (data) {
            return {
                results: $.map(data.Json, function (item) {
                    return {
                        text: `${item.BusinessName}`,
                        id: item.SupplierId
                    }
                }),
            };
        }
    }
});

$(document).ready(function () {
    window.setTimeout(function () { $("#fa").click(); }, 1000);

    $("#selectItem").select2({
        minimumInputLength: 2,
        formatInputTooShort: function () {
            return "Busca un producto";
        },
        matcher: function (term, text, option) {
            return text.toUpperCase().indexOf(term.toUpperCase()) >= 0 || option.val().toUpperCase().indexOf(term.toUpperCase()) >= 0;
        }
    });
    $("#selectCurrency").select2();
    $("#selectPackage").select2();
    $('#datePurchase').datepicker({
        autoclose: true,
        startDate: new Date(),
        todayHighlight: true,
    });

    $("#radioEmpaque").on('ifChanged', function (event) { OptionTypeRadio() });
    $("#radioPiezas").on('ifChanged', function (event) { OptionTypeRadio() });
    ComboboxCurrency();
    OptionTypeRadio();
})

function OptionTypeRadio() {
    if ($("#radioEmpaque").is(":checked")) {
        OptionAmount();
        $('#iconPasillas').removeClass('col-lg-4');
        $('#iconPasillas').addClass('col-lg-2');
        $("#textAmount").text("Caja");
        $("#divEmpaque").show();
        $('#selectPackage').val($("#selectPackage :first").val()).trigger('change');
        if ($('#selectPackage').val() == 0) {
            toastr.warning("No hay empaques");
        }
    }
    if ($("#radioPiezas").is(":checked")) {
        $('#iconPasillas').removeClass('col-lg-2');
        $('#iconPasillas').addClass('col-lg-4');
        $("#textAmount").text("Piezas");
        $("#divEmpaque").hide();
        OptionAmount();
    }
}

//no se quien hizo esta funcion pero la use si truena borrarla
function ComboboxCurrency() {
    axios.get('/MaCode/ActionGetListCurrency')
        .then(data => {
            //esto fue agregado por mi para disminuir codigo
            var currencyCombo = value => `<option val="${value.VKey}">${value.VKey}</option>`;
            $("#selectCurrency").html(data.data.map(currency => currencyCombo(currency)));
        }).catch(error => {
            console.log(error.data);
        }).then(() => {
            $('#selectCurrency').val($("#selectCurrency :first").val()).trigger('change');
        })
}

function ReloadItemList() {
    var idSupplier = $("#selectSupplier").val();
    if (idSupplier != 0) {
        AddItemList();
    }
    ClearProduct();
}

function AddItemList() {
    ClearProduct();
    var idSupplier = $("#selectSupplier").val();
    var currency = $("#selectCurrency").val();
    if (idSupplier != 0) {
        axios.get(`/ItemSuppliers/ActionGetListItemCurrency/?Supplier=${idSupplier}&Currency=${currency}`)
            .then(data => {
                if (data.data.length == 0) {
                    $("#selectItem").html(`<option value="0">No se encuentra ningun producto</option >`);
                    toastr.warning("El proveedor no tiene productos")
                }
                else if (data.data.length > 0) {
                    var itemCombo = value => `<option value="${value.PathNumber}">${value.Description}</option>`;
                    $("#selectItem").html([`<option value="0">Seleccione un producto</option >`, ...data.data.map(item => itemCombo(item))]);
                }
                else {
                    $("#selectItem").html(`<option value="0">No se encuentra ningun producto</option >`);
                    toastr.warning("Error desconocido");
                }
            })
            .catch(error => {
                console.log(error.data)
            })
            .then(() => {
                $('#selectItem').val($("#selectItem :first").val()).trigger('change');
            })
    }
    else {
        $("#selectItem").html(`<option value="0">No se encuentra ningun producto</option >`);
        $('#selectItem').val($("#selectItem :first").val()).trigger('change');
    }
}

function InsertDescription() {
    var partnumber = $("#selectItem").val();
    if (partnumber != 0) {
        $("#inputUPC").val(partnumber);
        AllPackage(partnumber);
        InfoItem(partnumber);
    }
    else {
        $("#inputUPC").val("");
        AllPackage(partnumber);
    }
}

function InfoItem(partnumber) {
    var Supplier = $("#selectSupplier").val();
    axios.post("/Items/ActionCostInfo", { PartNumber: partnumber, Supplier: Supplier })
        .then(data => {
            if (data.data == "SF") {
                SessionFalse(sesion);
            }
            else if (data.data != null) {
                $("#inputPrice").val(data.data.BaseCost);
                $("#inputIVA").val(data.data.Iva);
                $("#inputIEPS").val(data.data.Ieps);
                itemPurchase = data.data;
                $("#inputUniteSize").val(data.data.UnitSize != "" ? data.data.UnitSize : "N/A");
            }
            else {
                toastr.warning("No contiene datos el producto");
                itemPurchase = {};
                ClearProduct()
            }
        })
        .catch(error => {
            console.log(`%c${error.data}`, "color: red; font-size: 20px;");
        })
}

function AllPackage(partNumber) {
    if (partNumber != 0) {
        axios.get(`/ItemSpeck/ActionPartSpeck/?PartNumber=${partNumber}`)
            .then(data => {
                if (data.data == "SF") {
                    SessionFalse(sesion);
                }
                else if (data.data.length == 0) {
                    $("#selectPackage").html("<option value='0'>N/A</option>");
                    $('#selectPackage').val($("#selectPackage :first").val()).trigger('change');
                    $("#radioPiezas").iCheck('check');
                }
                else if (data.data.length > 0) {
                    if (data.data.filter(x => x.PackingOfSize > 0).length > 0) {
                        var packingCombo = packing => `<option value="${packing.PackingOfSize}">${packing.PackingOfSize}</option>`;
                        $("#selectPackage").html(data.data.map(element => packingCombo(element)));
                        $('#selectPackage').val($("#selectPackage :first").val()).trigger('change');
                    }
                    else {
                        $("#selectPackage").html("<option value='0'>N/A</option>");
                        $('#selectPackage').val($("#selectPackage :first").val()).trigger('change');
                        $("#radioPiezas").iCheck('check');
                    }
                }
                else {
                    toastr.warning("Error desconocido");
                }
            })
            .catch(error => {
                console.log(`%c${error.data}`, "color: red; font-size: 20px;");
            })
    }
    else {
        $("#selectPackage").html("<option value='0'>N/A</option>")
        $('#selectPackage').val($("#selectPackage :first").val()).trigger('change');
    }
}

function BoxForPackage() {
    OptionAmount();
}

function OptionAmount() {
    var itemAmount = $("#inputAmount").val();
    var package = $("#selectPackage").val();
    if ($("#radioEmpaque").is(":checked") && package > 0 && itemAmount > 0 && itemAmount <= 2000000) {
        if (parseFloat(itemAmount) * parseFloat(package) > 21474800) {
            $("#inputQuantity").val("");
        }
        else
            $("#inputQuantity").val(parseFloat(itemAmount) * parseFloat(package));
    }
    else if ($("#radioEmpaque").is(":checked") && package < 0) {
        $("#inputQuantity").val("");
        toastr.warning("Favor de seleccionar la opcion de piezas")
    }
    else if ($("#radioPiezas").is(":checked") && itemAmount > 0 && itemAmount <= 2000000) {
        $("#inputQuantity").val(parseFloat(itemAmount) * 1);
    }
    else {
        $("#inputQuantity").val("");
    }
}

$("#addProduct").click(() => {
    OptionAmount();
    var date = $("#datePurchase").val();
    var supplier = $("#selectSupplier").val();
    var item = $("#selectItem").val();
    var itemAmount = $("#inputAmount").val();
    var Price = $("#inputPrice").val();
    if (Price != "0") {
        if (item == "0") {
            toastr.warning("Selecciona un producto")
        }
        else if (itemList.filter(element => element.PartNumber == item).length == 0 && item != "0") {
            if (date != "") {
                if (supplier != 0 && item != 0) {
                    if ($("#radioEmpaque").is(":checked") && $("#selectPackage").val() > 0 && itemAmount > 0) {
                        BlockOptionProduct(true);
                        ReadyToSave(date, item);
                    }
                    else if ($("#radioPiezas").is(":checked") && itemAmount > 0) {
                        BlockOptionProduct(true);
                        ReadyToSave(date, item);
                    }
                    else {
                        if (itemAmount > 0)
                            toastr.warning("Favor de seleccionar la opcion de piezas");
                        else
                            toastr.warning("Favor de agregar una cantidad mayor a 0");
                    }
                }
                else
                    toastr.warning("Favor de revisar que tiene el proveedor y el producto seleccionado");
            }
            else {
                setTimeout(function () {
                    position = $("#datePurchase").offset()
                    $('html, body').animate({ scrollTop: position.top - 380 }, "slow");
                    $("#errorPurchase").show();
                    $("#datePurchase").addClass("error");
                    if ($("#datePurchase").val().trim() != null && $("#datePurchase").val().trim() != "") {
                        $("#errorPurchase").html("Poner una fecha valida")
                    }
                    else
                        $("#errorPurchase").html("El Campo no puede ser nulo")
                    $("#errorPurchase").fadeOut(5000, function () {
                        $("#datePurchase").removeClass("error")
                    })
                }, 100);
                $(".datepicker").hide();

                toastr.warning("Ingresa una fecha");
            }
        }
        else
            toastr.warning("Ya ingresaste ese producto")
    } else {
        toastr.warning("Solo puede ingresar productos con precio establecido, intente con otro producto.")
    }
})

function BlockOptionProduct(value) {
    if (value) {
        $("#selectSupplier").prop("disabled", true);
        $("#selectCurrency").prop("disabled", true);
        $("#datePurchase").prop("disabled", true);
    }
    else {
        $("#selectSupplier").prop("disabled", false);
        $("#selectCurrency").prop("disabled", false);
        $("#datePurchase").prop("disabled", false);
    }
}

function ReadyToSave(date, item) {
    date = moment(date)
    var product = $("#selectItem option:selected").text();
    var box = $("#inputAmount").val();
    var quantity = $("#inputQuantity").val();
    var unitSize = $("#inputUniteSize").val();
    var purchasePrice = $("#inputPrice").val();
    var iva = $("#inputIVA").val();
    var ieps = $("#inputIEPS").val();
    var amount = parseFloat((parseFloat(quantity) * parseFloat(purchasePrice)).toFixed(4));
    var iepstotal = parseFloat((parseFloat(quantity) * parseFloat(itemPurchase.IEPS)).toFixed(4));
    var ivatotal = parseFloat((parseFloat(quantity) * parseFloat(itemPurchase.IVA)).toFixed(4));
    var itemAmount = parseFloat((iepstotal + ivatotal + parseFloat(amount)).toFixed(4));
    var Currency = $("#selectCurrency").val();
    if ($("#radioPiezas").is(":checked")) {
        itemList = [({
            PartNumber: item,
            PartDescription: product,
            PackingOfSize: 1,
            Boxes: box,
            Quantity: quantity,
            UnitSize: unitSize,
            PurchasePrice: purchasePrice,
            Iva: iva,
            Ieps: ieps,
            ItemAmount: amount,
            IvaTotal: ivatotal,
            IepsTotal: iepstotal,
            ItemTotalAmount: itemAmount,
            Currency: Currency
        }), ...itemList]
    }
    if ($("#radioEmpaque").is(":checked")) {
        var Factor = $("#selectPackage").val();
        itemList = [({
            PartNumber: item,
            PartDescription: product,
            PackingOfSize: Factor,
            Boxes: box,
            Quantity: quantity,
            UnitSize: unitSize,
            PurchasePrice: purchasePrice,
            Iva: iva,
            Ieps: ieps,
            ItemAmount: amount,
            IvaTotal: ivatotal,
            IepsTotal: iepstotal,
            ItemTotalAmount: itemAmount,
            Currency: Currency
        }), ...itemList]
    }
    TotalPurchase();
    AddListOrder(itemList);
    $("#buttonSavePurchase").prop("disabled", false);
    ClearProduct();
}

function TotalPurchase() {
    if (itemList.length > 0) {
        var subtotaltable = itemList.map(item => item.ItemAmount).reduce((a, b) => a + b);
        var ivatotaltable = itemList.map(item => item.IvaTotal).reduce((a, b) => a + b);
        var iepstotaltable = itemList.map(item => item.IepsTotal).reduce((a, b) => a + b);
        $("#subTotalTable").html(fomartmoney(parseFloat(subtotaltable.toFixed(4)))(4));
        $("#ivaTotalTable").html(fomartmoney(parseFloat(ivatotaltable.toFixed(4)))(4));
        $("#iepsTotalTable").html(fomartmoney(parseFloat(iepstotaltable.toFixed(4)))(4));
        $("#importeTotalTable").html(fomartmoney(parseFloat((parseFloat(subtotaltable.toFixed(4)) + parseFloat(ivatotaltable.toFixed(4)) + parseFloat(iepstotaltable.toFixed(4))).toFixed(4)))(4));
    }
    else {
        $("#subTotalTable").html(fomartmoney(0)(4));
        $("#ivaTotalTable").html(fomartmoney(0)(4));
        $("#iepsTotalTable").html(fomartmoney(0)(4));
        $("#importeTotalTable").html(fomartmoney(0)(4));
    }
}

function ClearProduct() {
    $("#selectItem").val($("#selectItem :first").val()).trigger('change');
    $("#inputPrice").val("")
    $("#inputIVA").val("")
    $("#inputIEPS").val("")
    $("#inputQuantity").val("")
    $("#inputUniteSize").val("")
    $("#inputAmount").val("")
    $("#radioPiezas").iCheck('check');
}

function ClearSupplier() {
    $("#datePurchase").val("");
    $("#selectSupplier").val($("#selectSupplier :first").val()).trigger('change');
    $("#selectCurrency").val($("#selectCurrency :first").val()).trigger('change');
    $("#textAreaComment").val("");
}

$("#buttonCancelPurchase").click(function () {
    swal({
        title: "¿Deseas cancelar la orden de compra?",
        //text: "Si imprime se guardara y no podrá ser cancelado el formato de pedido",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "Si",
        cancelButtonText: "No",
        closeOnConfirm: true,
        closeOnCancel: true
    }, function (isConfirm) {
        if (isConfirm) {
            FullClear();
        } else {
        }
    });
})

$("#buttonSavePurchase").click(function () {
    swal({
        title: "¿Deseas generar la orden de compra?",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "Si",
        cancelButtonText: "No",
        closeOnConfirm: true,
        closeOnCancel: true
    }, function (isConfirm) {
        if (isConfirm) { SavePurchaseOrder(); }
        else { }
    });
})

function SavePurchaseOrder() {
    StartLoading();
    var supplier = $("#selectSupplier").val();
    var currency = $("#selectCurrency").val();
    var purchasedate = $("#datePurchase").val();
    var purchaseremark = $("#textAreaComment").val();
    var purchaseOrder = { SupplierId: supplier, Currency: currency, PurchaseDate: purchasedate, PurchaseRemark: purchaseremark }
    axios.post("/purchases/ActionCreateNewPurchasesOrder/", { arrayHead: purchaseOrder, detailList: itemList })
        .then(data => {
            if (data.data == "SF") {
                SessionFalse(sesion);
            }
            else if (data.data.Error == 1000) {
                swal({
                    title: 'Número de Orden: ' + data.data.Document,
                    text: "¡Su orden de compra se generó correctamente!",
                    type: "success"
                });
            }
            else if (data.data.Error == 1003) {
                toastr.warning("No se mando el correo a proveedor porque no tiene aria de ventas");
                swal({
                    title: 'Número de Orden: ' + data.data.Document,
                    text: "¡Su orden de compra se generó correctamente!",
                    type: "success"
                });
            }
            else if (data.data.Error == 1001) {
                toastr.error("Problema al generar orden de compra");
            }
            else if (data.data.Error == 1002) {
                toastr.error("Problema al generar orden de compra por los productos");
            }
            else {
                toastr.error("Error desconocido");
            }
        })
        .catch(error => {
            console.log(`%c${error.data}`, "color:red");
        })
        .then(() => {
            FullClear();
            EndLoading();
        })
}

function AddListOrder(data) {
    table.clear().draw();
    table.rows.add(data);
    table.columns.adjust().draw();
}

function DeleteOption(partnumber) {
    itemList = itemList.filter(item => item.PartNumber != partnumber.split("N")[1]);
    TotalPurchase();
    if (itemList.length <= 0) {
        $("#buttonSavePurchase").prop("disabled", true);
        BlockOptionProduct(false);
        ClearProduct();
        ClearSupplier();
    }
    AddListOrder(itemList);
}

var table = $("#TablePurchases").DataTable({
    "autoWidth": true,
    "paging": false,
    "searching": false,
    "info": false,
    responsive: true,
    oLanguage:
    {
        "sProcessing": "Procesando...",
        "sLengthMenu": "Mostrar _MENU_ registros",
        "sZeroRecords": "No se encontraron resultados",
        "sEmptyTable": "Ningun dato disponible en esta tabla",
        "sInfo": "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
        "sInfoEmpty": "Mostrando registros del 0 al 0 de un total de 0 registros",
        "sInfoFiltered": "(filtrado de un total de _MAX_ registros)",
        "sInfoPostFix": "",
        "sSearch": "Buscar:",
        "sUrl": "",
        "sInfoThousands": ",",
        "sLoadingRecords": "Cargando...",
        "oPaginate": { "sFirst": "Primero", "sLast": "�ltimo", "sNext": "Siguiente", "sPrevious": "Anterior" },
        "oAria":
        {
            "sSortAscending": ": Activar para ordenar la columna de manera ascendente",
            "sSortDescending": ": Activar para ordenar la columna de manera descendente"
        }
    },
    columns:
        [
            { data: 'PartNumber' },
            { data: 'PartDescription' },
            { data: 'PackingOfSize' },
            { data: 'Boxes' },
            { data: 'Quantity' },
            { data: 'UnitSize' },
            { data: 'PurchasePrice' },
            { data: 'Iva' },
            { data: 'Ieps' },
            { data: 'ItemAmount' },
            { data: 'IvaTotal' },
            { data: 'IepsTotal' },
            { data: 'ItemTotalAmount' },
            { data: null }
        ],
    columnDefs:
        [
            {
                targets: [13],
                render: function (data, type, full, meta) {
                    return `<button data-toggle="tooltip" onclick="DeleteOption('N${full.PartNumber}')" title="Eleminar" class="btn btn-sm btn-danger btn-circle" id="remove-button" ><i class="fa fa-times"></i></button>`;
                },
                orderable: false,
                width: "1%"
            },
            {
                data: 'purchasePrice',
                type: 'numeric-comma',
                render: function (data, type, row) { return `${fomartmoney(parseFloat(data))(4)}`; },
                targets: [6, 9, 10, 11, 12]
            },
            {
                data: 'iva',
                render: function (data, type, row) { return data + ' %'; },
                targets: [7, 8]
            }
        ]
});

function FullClear() {
    ClearProduct();
    ClearSupplier();
    BlockOptionProduct(false);
    itemList = [];
    AddListOrder(itemList);
    $("#buttonSavePurchase").prop("disabled", true);
    TotalPurchase();
}