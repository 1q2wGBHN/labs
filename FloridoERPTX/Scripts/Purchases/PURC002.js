﻿var enable = true;
var listProducts = [];
$(document).ready(function () {
    window.setTimeout(function () { $("#fa").click(); }, 1000);
});

$('#fechaAppPrecio').datepicker({ autoclose: true, todayHighlight: true, language: 'en' });

var table = $('#tableSalesPrice').DataTable({
    oLanguage:
    {
        "sProcessing": "Procesando...",
        "sLengthMenu": "Mostrar _MENU_ Registros",
        "sZeroRecords": "No se encontraron resultados",
        "sEmptyTable": "Ningún dato disponible en esta tabla",
        "sInfo": "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
        "sInfoEmpty": "Mostrando registros del 0 al 0 de un total de 0 registros",
        "sInfoFiltered": "(filtrado de un total de _MAX_ registros)",
        "sInfoPostFix": "",
        "sSearch": "Buscar:",
        "sUrl": "",
        "sInfoThousands": ",",
        "sLoadingRecords": "Cargando...",
        "oPaginate":
        {
            "sFirst": "Primero",
            "sLast": "Último",
            "sNext": "Siguiente",
            "sPrevious": "Anterior"
        },
        "oAria":
        {
            "sSortAscending": ": Activar para ordenar la columna de manera ascendente",
            "sSortDescending": ": Activar para ordenar la columna de manera descendente"
        }
    },
    dom: "<'row'<'col-sm-4'><'col-sm-4 text-left'B><'col-sm-4'>t<'col-sm-6'i><'col-sm-6'p>>",
    lengthMenu: [],
    buttons: [],
    columns:
        [
            { data: 'sales_price_id' },
            { data: 'part_number' },
            { data: 'part_description' },
            { data: 'sale_price' },
            { data: 'applied_date' },
            { data: 'existencias' },
            { data: 'Department' },
            { data: 'Family' }
        ],
    columnDefs:
        [
            {
                visible: false,
                searchable: false,
                targets: [0]
            }
        ]
});

$('#button').click(function () {
    i = 0;
    listProducts = [];
    $.each(table.rows('.active').data(), function (index, value) {
        listProducts[i] = { sales_price_id: value.sales_price_id, part_number: value.part_number, part_description: value.part_description, applied_date: value.applied_date, sale_price: value.sale_price, existencias: value.existencias, Department: value.Department, Family: value.Family };
        i++;
    });
    if (listProducts.length == 0) {
        toastr.error('Selecciona almenos un Producto.');
    }
    else {
        StartLoading();
        $.ajax({
            url: "/SalesPriceChange/ActionReportOrder",
            type: "POST",
            data: { listProducts: listProducts },
            success: function (response) {
                EndLoading();
                if (response.success) {
                    $("#qwerty").html("<iframe width='100%' height='550px' src='data:application/pdf;base64, " + encodeURI(response.responseText) + "'></iframe>");
                    $("#modal-cenefa").modal('show');
                }
                else
                    toastr.error(response.responseText);
            },
            error: function () {
                EndLoading();
                toastr.error('Error inesperado contactar a sistemas.');
            }
        });
    }
});

function GetFamily() {
    var deptoo = $("#depto").val();
    if (deptoo == "") {
        FillDropdown("family", null)
        return false;
    }
    $.ajax({
        url: "/MaClass/ActionGetFamily",
        type: "GET",
        data: { "deptoo": deptoo },
        success: function (response) {
            if (response.success) {
                listFamily = response.Json;
                FillDropdown("family", listFamily);
            }
            else
                FillDropdown("family", null);
        },
        error: function () {
            FillDropdown("family", null);
            toastr.error('Error inesperado contactar a sistemas.');
        }
    });
}

function FillDropdown(selector, vData) {
    if (vData != null) {
        $('#' + selector).trigger("change");
        var vItems = [];
        vItems.push('<option  value="">Selecciona una Familia</option>');
        for (var i in vData) {
            if (vData[i].Selected)
                vItems.push('<option selectedselected=selected value="' + vData[i].class_id + '">' + vData[i].description + '</option>');
            else
                vItems.push('<option value="' + vData[i].class_id + '">' + vData[i].description + '</option>');
        }
        $('#' + selector).empty();
        $('#' + selector).append(vItems.join(''));
        $('#' + selector).val("").trigger('change');
    }
    else {
        var vItems = [];
        vItems.push('<option  value="">Selecciona una Familia</option>');
        $('#' + selector).empty();
        $('#' + selector).append(vItems.join(''));
        $('#' + selector).val("").trigger('change');
    }
}

$('#tableSalesPrice tbody').on('click', 'tr', function () {
    $(this).toggleClass('active success');
});

$('#buscar').on('click', function (r, e) {
    var existencias = "";
    toastr.options =
        {
            "debug": false,
            "newestOnTop": false,
            "positionClass": "toast-top-center",
            "closeButton": true,
            "debug": false,
            "toastClass": "animated fadeInDown",
        };
    if ($("#removeExist").is(':checked')) {
        existencias = "SI";
    }
    else {
        existencias = "";
    }
    if ($("#fechaAppPrecio").val() == "") {
        toastr.error('Selecciona una fecha');
        $("#fechaAppPrecio").val("");
        $("#fechaAppPrecio").focus();
    }
    else {
        StartLoading();
        $.ajax({
            url: "/SalesPriceChange/ActionGetSalesPriceChange",
            type: "POST",
            data: { "fecha": $("#fechaAppPrecio").val(), "depto": $("#depto").val(), "familia": $("#family").val(), "existencias": existencias },
            success: function (returndata) {
                EndLoading();
                if (returndata.success) {
                    table.clear().draw();
                    table.rows.add(returndata.Json)
                    table.columns.adjust().draw();
                    $("#button").removeAttr('disabled', 'disabled');
                }
                else {
                    if (returndata.responseText === 'Termino tu sesión.') {
                        SessionFalse(returndata.responseText);
                    }
                    else {
                        toastr.warning(returndata.responseText);
                        table.clear().draw();
                        $("#button").attr('disabled', true);
                    }
                }
            },
            error: function () {
                EndLoading();
                toastr.error('Error inesperado contactar a sistemas.');
            }
        });
    }
});
//------------------------------------------------------------BASCULAS

function BasculaGenerateFile(s) {
    StartLoading();
    $.ajax({
        url: "/SalesPriceBasculeChange/ActionReportOrder",
        type: "POST",
        data: { "TypeOfBascule": s }, //2 = TOLEDO ,1 = CAS, ToledoBPlus = 3
        success: function (returndata) {
            EndLoading();
            if (returndata.success) {
                toastr.success(returndata.responseText);
            }
            else {
                if (returndata.responseText === 'SF') {
                    SessionFalse(returndata.responseText);
                }
            }
        },
        error: function () {
            EndLoading();
            toastr.error('Error inesperado contactar a sistemas.');
        }
    });
}

function Base64ToArrayBuffer(base64) {
    var binaryString = window.atob(base64);
    var binaryLen = binaryString.length;
    var bytes = new Uint8Array(binaryLen);
    for (var i = 0; i < binaryLen; i++) {
        var ascii = binaryString.charCodeAt(i);
        bytes[i] = ascii;
    }
    return bytes;
}

function SaveByteArray(reportName, byte) {
    var blob = new Blob([byte]);
    var link = document.createElement('a');
    link.href = window.URL.createObjectURL(blob);
    link.download = reportName;
    link.click();
}


function FillDropdown(selector, vData) {
    if (vData != null) {
        var vItems = [];
        vItems.push('<option value="0">Seleccione una familia</option>');
        for (var i in vData) {
            if (vData[i].Selected)
                vItems.push('<option selected value="' + vData[i].class_id + '">' + vData[i].description + '</option>');
            else
                vItems.push('<option value="' + vData[i].class_id + '">' + vData[i].description + '</option>');
        }
        $('#' + selector).empty();
        $('#' + selector).append(vItems.join(''));
        $('#family2').val($("#family2 :first").val()).trigger('change');
    }
    else {
        var vItems = [];
        vItems.push('<option selected value="0">Seleccione una familia</option>');
        $('#' + selector).empty();
        $('#' + selector).append(vItems.join(''));
        $('#family2').val($("#family2 :first").val()).trigger('change');
    }
}

function GetFamily() {
    var depto = $("#departament").val();
    if (depto == "" || depto == 0) {
        $('#family').val($("#family :first").val()).trigger('change');
        FillDropdown("family", null)
        return false;
    }
    $.ajax({
        url: '/MaClass/ActionGetFamily/',
        type: "GET",
        data: { "deptoo": depto },
        success: function (response) {
            if (response.success) {
                listFamily = response.Json;
                FillDropdown("family", listFamily);
            }
            else
                FillDropdown("family", null);
        },
        error: function () {
            FillDropdown("family", null)
            toastr.error('Error inesperado contactar a sistemas.');
        }
    });
}


//Función Modal
function ShowReportPrice() {
    var dep = $("#departament").val();
    var fam = $("#family2").val();

    if (dep == "")
        dep = "0";
    if (fam == "")
        fam = "0";

    if (dep == "0") {
        toastr.warning('Seleccione un departamento para continuar.');
    } else {
        StartLoading();
        $.ajax({
            type: "GET",
            url: "/SalesPriceChange/ActionGetReportSalePriceCurrent",
            data: { "deparment": dep, "family": fam },
            success: function (returndates) {
                $("#modalReferenceBody").html(`<iframe width='100%' height='550px' src='data:application/pdf;base64,${returndates}'></iframe>`);
                EndLoading();
                $('#ModalReport').modal('show');
            },
            error: function (returndates) {
                toastr.error('Alerta - Error inesperado  contactar a sistemas.');
                EndLoading();
            }
        });
    }
}