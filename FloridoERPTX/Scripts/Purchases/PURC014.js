﻿var xml_permission = 0;
var totalSum = 0;
var active_flag_expense_varibles = false;
var strTitle = "";
var strText = "";
//Funciones genericas
function HidenOrCheckFull(id, value) {
    value ? $(id).show() : $(id).hide()
}

function ErrorList(value, type, text) {
    return !value ? ToastError(type, text) : type == "Succes" ? ToastError(type, text) : true;
}

function ToastError(type, text) {
    if (type == "Succes") {
        toastr.success(text)
        return true;
    }
    else if (type == "Error") {
        toastr.error(text)
    }
    else if (type == "Warning") {
        toastr.warning(text)
    }
    return false;
}


function validateNumber(value, nameInput, optionValue) {
    if (active_flag_expense_varibles) {
        if (!isNaN(value) &&
            (value >= optionValue && value < 99999999)) {
            $("#" + nameInput).val(value);
            var inputSubtotal = $('#inputSubtotal2').val() ? parseFloat($('#inputSubtotal2').val()) : 0;
            var inputIVA = $('#inputIVA2').val() ? parseFloat($('#inputIVA2').val()) : 0;
            var inputIEPS = $('#inputIEPS2').val() ? parseFloat($('#inputIEPS2').val()) : 0;
            var inputDiscount = $('#inputDiscount2').val() ? parseFloat($('#inputDiscount2').val()) : 0;
            var inputIVARetenido = $('#inputIVARetenido2').val() ? parseFloat($('#inputIVARetenido2').val()) : 0;
            var inputISRRetenido = $('#inputISRRetenido2').val() ? parseFloat($('#inputISRRetenido2').val()) : 0;
            var sum = (inputSubtotal + inputIVA + inputIEPS - inputDiscount - (inputIVARetenido + inputISRRetenido));

            totalSum = sum;
            $('#total_sum2').html(sum);
            return true;
        } else {
            $("#" + nameInput).focus();
            $("#" + nameInput).val("");
            return false;
        }
    } else {
        if (!isNaN(value) &&
            (value >= optionValue && value < 99999999)) {
            $("#" + nameInput).val(value);
            var inputSubtotal = $('#inputSubtotal').val() ? parseFloat($('#inputSubtotal').val()) : 0;
            var inputIVA = $('#inputIVA').val() ? parseFloat($('#inputIVA').val()) : 0;
            var inputIEPS = $('#inputIEPS').val() ? parseFloat($('#inputIEPS').val()) : 0;
            var inputDiscount = $('#inputDiscount').val() ? parseFloat($('#inputDiscount').val()) : 0;
            var inputIVARetenido = $('#inputIVARetenido').val() ? parseFloat($('#inputIVARetenido').val()) : 0;
            var inputISRRetenido = $('#inputISRRetenido').val() ? parseFloat($('#inputISRRetenido').val()) : 0;

            var sum = (inputSubtotal + inputIVA + inputIEPS - inputDiscount - (inputIVARetenido + inputISRRetenido));

            totalSum = sum;
            $('#total_sum').html(sum);
            return true;
        } else {
            $("#" + nameInput).focus();
            $("#" + nameInput).val("");
            return false;
        }
    }
}

function SwalFunc(typeOption) {
    if (xml_permission == 0) { //No es requerido XML (Permite entrad con remision y XML)
        strTitle = "¿Esta seguro que desea generar el gasto?";
        strText = "Si selecciona 'no' se abrirá la opcion de colocar factura";
    } else { //xml_permission = 1 ////ES OBLIGATORIO XML
        strTitle = "¿Esta seguro que desea generar el gasto?";
        strText = "Únicamente es permitido registro con XML.";
    }
    if (typeOption == 1) {
        swal({
            title: strTitle,
            text: strText,
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Si",
            cancelButtonText: "No"
        }, function (isConfirm) {
            if (isConfirm) {
                readFile = function (e) {
                    var file = e.target.files;
                    convertToBase64XML(file);
                }
                fileInput = document.createElement("input")
                fileInput.type = 'file'
                fileInput.style.display = 'none'
                fileInput.accept = '.xml'
                fileInput.onchange = readFile
                fileInput.multiple = true
                //fileInput.func = func
                document.body.appendChild(fileInput)
                clickElem(fileInput)
            } else {
                if (xml_permission == 0) {
                    if (active_flag_expense_varibles) {
                        ["inputInvoice2", "paymentList2", "inputSubtotal2", "inputIVA2", "inputIEPS2", "inputDiscount2", "inputQuantity2", 'inputIVARetenido2', 'inputISRRetenido2'].map(id => $(`#${id}`).val(""))
                        $("#paymentList2").val("").trigger("change");
                        $("#ModalInvoice2").modal("show");
                    } else {
                        ["inputInvoice", "paymentList", "inputSubtotal", "inputIVA", "inputIEPS", "inputDiscount", "inputQuantity", 'inputIVARetenido', 'inputISRRetenido', 'inputComment'].map(id => $(`#${id}`).val(""))
                        $("#paymentList").val("").trigger("change");
                        $("#ModalInvoice").modal("show");
                    }
                }
            }
        });
    } else {
        listXML = [];
        toastr.success("El gasto ha sido guardado correctamente.");
    }
}

//Jquery Ready
$(document).ready(function () {
    $(".select").select2();
    window.setTimeout(function () { $("#fa").click(); }, 1000);
    $("#selectOption").select2();
    $("#optionsExpense").select2();
    $("#optionsExpenseAreas").select2();
    $("#optionsExpenseAreas2").select2();
    $("#paymentList").select2();
    $("#selectOption2").select2();
    $("#paymentList2").select2();
    SearchListTemp();
});

optionsExpenseAreas2div
function showElements() {
    ["searchBody", "buttonSavediv", "optionsExpensediv", "searchBody2", "buttonSavediv2", "optionsExpensediv2", "optionsExpenseAreas2div", "optionsExpenseAreasdiv"].map(x => HidenOrCheckFull(`#${x}`, true));
}

function hideElements() {
    ["searchBody", "buttonSavediv", "optionsExpensediv", "searchBody2", "buttonSavediv2", "optionsExpensediv2", "optionsExpenseAreas2div", , "optionsExpenseAreasdiv"].map(x => HidenOrCheckFull(`#${x}`, false));
}

function SearchListTemp() {
    $.ajax({
        type: "GET",
        url: "/Expenses/ActionGetListGeneric",
        success: function (data) {
            if (data) {
                $.each(data, function (i, p) {
                    $('#optionsExpense').append($('<option></option>').val(p.vkey).html(p.vkey));
                });
            }
            else {
                if (returndata.responseText == "SF") {
                    SessionFalse("Su sesión a terminado.")
                }
                hideElements();
            }
        },
        error: function () {
            toastr.error('Error inesperado contactar a sistemas');
            EndLoading();
        }
    });
}

function SearchCreditors() {
    if (active_flag_expense_varibles) {
        var value = $("#supplierList2").val();
        value = value == null ? 0 : value;
        if (value != 0) {
            StartLoading();
            $.ajax({
                type: "GET",
                url: "/Expenses/ActionGetInformationSupplier",
                data: { "supplier_id": value, },
                success: function (returndata) {
                    EndLoading();
                    if (returndata.Data) {
                        $("#provider2").text(returndata.Supplier.CommercialName != "" ? returndata.Supplier.CommercialName : "N/A");
                        $("#rfc2").text(returndata.Supplier.Rfc != "" ? returndata.Supplier.Rfc : "N/A");
                        $("#email2").text(returndata.Supplier.email != "" ? returndata.Supplier.email : "N/A");
                        $("#nameContact2").text(returndata.Supplier.name != "" ? returndata.Supplier.name : "N/A");
                        $("#phone2").text(returndata.Supplier.phone != "" ? returndata.Supplier.phone : "N/A");
                        $("#adress2").text(returndata.Supplier.SupplierAddress != "" ? returndata.Supplier.SupplierAddress : "N/A");
                        xml_permission = returndata.Permission;
                        showElements();
                    }
                    else {
                        if (returndata.responseText == "SF") {
                            SessionFalse("Su sesión a terminado.")
                        }
                        $("#searchBody2").hide();
                        hideElements();
                    }
                },
                error: function () {
                    EndLoading();
                    toastr.error('Error inesperado contactar a sistemas');
                }
            });
        } else
            hideElements();
    } else {
        var value = $("#supplierList").val();
        value = value == null ? 0 : value;
        if (value != 0) {
            StartLoading();
            $.ajax({
                type: "GET",
                url: "/Expenses/ActionGetInformationSupplier",
                data: { "supplier_id": value, },
                success: function (returndata) {
                    EndLoading();
                    if (returndata.Data) {
                        $("#provider").text(returndata.Supplier.CommercialName != "" ? returndata.Supplier.CommercialName : "N/A");
                        $("#rfc").text(returndata.Supplier.Rfc != "" ? returndata.Supplier.Rfc : "N/A");
                        $("#email").text(returndata.Supplier.email != "" ? returndata.Supplier.email : "N/A");
                        $("#nameContact").text(returndata.Supplier.name != "" ? returndata.Supplier.name : "N/A");
                        $("#phone").text(returndata.Supplier.phone != "" ? returndata.Supplier.phone : "N/A");
                        $("#adress").text(returndata.Supplier.SupplierAddress != "" ? returndata.Supplier.SupplierAddress : "N/A");
                        xml_permission = returndata.Permission;
                        showElements();
                    }
                    else {
                        if (returndata.responseText == "SF") {
                            SessionFalse("Su sesión a terminado.")
                        }
                        $("#searchBody").hide();
                        hideElements();
                    }
                },
                error: function () {
                    EndLoading();
                    toastr.error('Error inesperado contactar a sistemas');
                }
            });
        } else
            hideElements();
    }
}

function saveInvoice() {
    if (active_flag_expense_varibles) {
        var option = $("#commentVariable").val();
        var optionArea = $("#optionsExpenseAreas2").val();
        if (ErrorList(optionArea != "", 'Warning', 'Ingrese el área donde se realizo el gasto.') &&
            ErrorList(option != "", 'Warning', 'Ingrese un comentario del gasto variable.')) {
            SwalFunc(1);
        }
    } else {
        var option = $("#optionsExpense").val();
        var optionArea = $("#optionsExpenseAreas").val();
        if (ErrorList(option != "", 'Warning', 'Seleccione una tipo de gasto')) {
            SwalFunc(1);
        }
    }
}

function SaveWithFolio() {
    if (active_flag_expense_varibles) {
        if (ErrorList($("#inputInvoice2").val() != '', 'Warning', 'Ingrese una factura correcta') &&
            ErrorList($("#paymentList2").val() != '', 'Warning', 'Seleccione una moneda.') &&
            ErrorList($("#inputSubtotal2").val() != '', 'Warning', 'Ingrese un subtotal.') &&
            ErrorList($("#inputIVA2").val() != '', 'Warning', 'Ingrese un IVA.') &&
            ErrorList($("#inputIEPS2").val() != '', 'Warning', 'Ingrese un IEPS.') &&
            ErrorList($("#inputDiscount2").val() != '', 'Warning', 'Ingrese un Descuento.') &&
            ErrorList($("#inputQuantity2").val() != '', 'Warning', 'Ingrese una cantidad.') &&
            ErrorList($("#inputIVARetenido2").val() != '', 'Warning', 'Ingrese el IVA Retenido.') &&
            ErrorList($("#inputISRRetenido2").val() != '', 'Warning', 'Ingrese el ISR Retenido.') &&
            ErrorList(parseFloat($("#inputSubtotal2").val()) > 0, 'Warning', 'Ingrese un subtotal correcto.') &&
            ErrorList(totalSum > 0, 'Warning', 'El costo final debe ser mayor a 0.')) {
            var model = {
                invoice: $("#inputInvoice2").val(), currency: $("#paymentList2").val(), subtotal: $("#inputSubtotal2").val(), iva: $("#inputIVA2").val(),
                ieps: $("#inputIEPS2").val(),
                total: totalSum,
                discount: $("#inputDiscount2").val(),
                quantity: $("#inputQuantity2").val(),
                ivaRetenido: $("#inputIVARetenido2").val(),
                isrRetenidos: $("#inputISRRetenido2").val(),
                comment: $("#commentVariable").val(),
                area: $('#optionsExpenseAreas2').val()
            };
            SwalFunction1(model);
        }
    } else {

        if (ErrorList($("#inputInvoice").val() != '', 'Warning', 'Ingrese una factura correcta') &&
            ErrorList($("#paymentList").val() != '', 'Warning', 'Seleccione una moneda.') &&
            ErrorList($("#inputSubtotal").val() != '', 'Warning', 'Ingrese un subtotal.') &&
            ErrorList($("#inputIVA").val() != '', 'Warning', 'Ingrese un IVA.') &&
            ErrorList($("#inputIEPS").val() != '', 'Warning', 'Ingrese un IEPS.') &&
            ErrorList($("#inputDiscount").val() != '', 'Warning', 'Ingrese un Descuento.') &&
            ErrorList($("#inputQuantity").val() != '', 'Warning', 'Ingrese una cantidad.') &&
            ErrorList($("#inputIVARetenido").val() != '', 'Warning', 'Ingrese el IVA Retenido.') &&
            ErrorList($("#inputISRRetenido").val() != '', 'Warning', 'Ingrese el ISR Retenido.') &&
            ErrorList(parseFloat($("#inputSubtotal").val()) > 0, 'Warning', 'Ingrese un subtotal correcto.') &&
            ErrorList(totalSum > 0, 'Warning', 'El costo final debe ser mayor a 0.')) {

            var model = {
                invoice: $("#inputInvoice").val(), currency: $("#paymentList").val(), subtotal: $("#inputSubtotal").val(), iva: $("#inputIVA").val(),
                ieps: $("#inputIEPS").val(),
                total: totalSum,
                discount: $("#inputDiscount").val(),
                quantity: $("#inputQuantity").val(),
                ivaRetenido: $("#inputIVARetenido").val(),
                isrRetenidos: $("#inputISRRetenido").val(),
                comment: $("#inputComment").val(),
                area: $("#optionsExpense").val()
            };
            SwalFunction2(model);
        }
    }
}

function SwalFunction1(model) {
    swal({
        title: "¿Esta seguro que desea generar el gasto?",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "Continuar",
        cancelButtonText: "Cancelar"
    }, function (isConfirm) {
        if (isConfirm) {
            StartLoading();
            $.ajax({
                type: "POST",
                url: "/Expenses/ActionSaveFixedExpenseFolio",
                data: { "model": model, "supplier_id": $("#supplierList2").val(), "type_expense": 'VARIADO', "comment": $("#commentVariable").val() },
                success: function (response) {
                    EndLoading();
                    $("#ModalInvoice2").modal("hide");
                    if (response == "SF") {
                        SessionFalse("Terminó tu sesión")
                    }
                    if (response != true) {
                        listXML = [];
                        toastr.error(response);
                    }
                    else {
                        ClearTabs("1");
                        $('#supplierList').val($("#supplierList :first").val()).trigger('change');
                        $("#optionsExpense").val("").trigger("change");
                        hideElements();
                        SwalFunc(0);
                    }
                },
                error: function () {
                    EndLoading();
                    toastr.error("DESCONOCIDO, CONTACTA A SISTEMAS.")
                }
            });
        }
    });
}

function SwalFunction2(model) {
    swal({
        title: "¿Esta seguro que desea generar el gasto?",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "Continuar",
        cancelButtonText: "Cancelar"
    }, function (isConfirm) {
        StartLoading();
        $.ajax({
            type: "POST",
            url: "/Expenses/ActionSaveFixedExpenseFolio",
            data: { "model": model, "supplier_id": $("#supplierList").val(), "type_expense": 'FIJO', "comment": '' },
            success: function (response) {
                console.log("SwalFunction2 response");
                console.log(response);
                EndLoading();
                $("#ModalInvoice").modal("hide");
                if (response == "SF") {
                    SessionFalse("Terminó tu sesión")
                }
                if (response != true) {
                    listXML = [];
                    toastr.error(response);
                }
                else {
                    ClearTabs("1");
                    $('#supplierList').val($("#supplierList :first").val()).trigger('change');
                    $("#optionsExpense").val("").trigger("change");
                    hideElements();
                    SwalFunc(0);
                }
            },
            error: function () {
                EndLoading();
                toastr.error("DESCONOCIDO, CONTACTA A SISTEMAS.")
            }
        });
    });
}

function SaveXML() {
    if (active_flag_expense_varibles) {
        var value = $("#supplierList2").val();
        if (ErrorList(listXML.length != 0, 'Warning', 'Adjunte por lo menos un XML')) {
            StartLoading();
            $.ajax({
                type: "POST",
                url: "/Expenses/ActionSaveFixedExpense",
                data: { "list": listXML, "supplier_id": value, "type_expense": 'VARIADO' },
                success: function (response) {
                    console.log("supplierList2 response");
                    console.log(response);
                    EndLoading();
                    $("#ModalInvoice2").modal("hide");
                    if (response == "SF") {
                        SessionFalse("Terminó tu sesión")
                    }
                    if (response != true) {
                        listXML = [];
                        toastr.error(response);
                    } else if (response) {
                        $('#supplierList2').val($("#supplierList2 :first").val()).trigger('change');
                        $("#commentVariable").val("");
                        hideElements();
                        SwalFunc(0);
                    } else {
                        listXML = [];
                        toastr.error(response);
                    }
                },
                error: function () {
                    EndLoading();
                    toastr.error("DESCONOCIDO, CONTACTA A SISTEMAS.")
                }
            });
        }
    } else {
        var value = $("#supplierList").val();
        var option = $("#optionsExpense").val();
        if (ErrorList(listXML.length != 0, 'Warning', 'Adjunte por lo menos un XML')) {
            StartLoading();
            $.ajax({
                type: "POST",
                url: "/Expenses/ActionSaveFixedExpense",
                data: { "list": listXML, "supplier_id": value, "type_expense": option },
                success: function (response) {
                    console.log("optionsExpense response");
                    console.log(response);
                    EndLoading();
                    $("#ModalInvoice").modal("hide");
                    if (response == "SF") {
                        SessionFalse("Terminó tu sesión")
                    }
                    if (response != true) {
                        listXML = [];
                        toastr.error(response);
                    } else if (response) {
                        $('#supplierList').val($("#supplierList :first").val()).trigger('change');
                        $("#optionsExpense").val("").trigger("change");
                        hideElements();
                        SwalFunc(0);
                    }
                    else {
                        listXML = [];
                        toastr.error(response);
                    }
                },
                error: function () {
                    EndLoading();
                    toastr.error("DESCONOCIDO, CONTACTA A SISTEMAS.")
                }
            });
        }
    }
}

function clickElem(elem) {
    var eventMouse = document.createEvent("MouseEvents")
    eventMouse.initMouseEvent("click", true, false, window, 0, 0, 0, 0, 0, false, false, false, false, 0, null)
    elem.dispatchEvent(eventMouse)
}

listXML = [];
//Funcion para convertir XML => base 64
var b = 0
function convertToBase64XML(file) {
    var selectedFile = file;
    if (selectedFile.length >= 2)
        toastr.warning("Solo puede seleccionar un XML.");
    else if (selectedFile.length > 0) {
        for (var i = 0; i < selectedFile.length; i++) {
            var idxDot = file[0].name.lastIndexOf(".") + 1;
            var extFile = file[0].name.substr(idxDot, file[0].name.length).toLowerCase();//Obtenemos extension de archivo
            if (extFile == "xml") {
                var xmlName = selectedFile[i].name
                var services = listXML.filter(e => e.xml_name == xmlName).length
                if (services == 0) {
                    var fileToLoad = selectedFile[i];
                    var fileReader = new FileReader();
                    listXML = [...listXML, { xml: "", xml_name: fileToLoad.name }]
                    fileReader.onload = function (fileLoadedEvent) {
                        var base64 = window.btoa(unescape(encodeURIComponent(fileLoadedEvent.target.result)));
                        var xml = base64;
                        listXML[b].xml = xml;
                        //b++;

                        //Contar XMl (si es más de uno)
                        SaveXML();

                    };
                    fileReader.readAsText(fileToLoad);
                }
                else {
                    listXML = [];
                    toastr.warning("Ya se agregó el mismo XML");
                }
            }
            else {
                toastr.warning('Debe ser un xml');
                listXML = [];
                b = 0;
            }
        }
        if (listXML.length > 0) {
            $("#tableXML").show();
        }
    }
}

function ClearTabs(value) {
    if (value == "1") {
        active_flag_expense_varibles = true;
    } else {
        active_flag_expense_varibles = false;
    }
    ["inputInvoice2", "paymentList2", "inputSubtotal2", "inputIVA2", "inputDiscount2", "inputQuantity2", 'inputIVARetenido2', 'inputISRRetenido2'].map(id => $(`#${id}`).val(""))
    $('#supplierList2').val($("#supplierList2 :first").val()).trigger('change');
    $('#supplierList').val($("#supplierList :first").val()).trigger('change');
    $('#optionsExpenseAreas2').val($("#optionsExpenseAreas2 :first").val()).trigger('change');
    $("#optionsExpense").val("").trigger("change");
    $("#commentVariable").val("");
    hideElements();
}