﻿$(document).ready(function () {
    OCHideOrShow(true);
    RCHideOrShow(true)
    window.setTimeout(function () { $("#fa").click(); }, 1000);
    number = 10005.55
    alert(new Intl.NumberFormat("en-US", { style: "currency", currency: "USD", currencyDisplay: "symbol" }).format(number))
});

function SearchPurchase() {
    axios.get("/PurchaseOrder/ActionGetOCRC/?PurchaseNo=" + $("#folioSearch").val())
        .then(function (data) {
            if (data.data == "SF") {
                window.location.href = 'http://' + window.location.host + '/Home/Login';
            }
            else if (data.data) {
                Search();
            }
            else {
                toastr.warning('La orden de compra no se encuentra lista para comparar o no existe');
                $("#searchBody").hide();
            }
        }).catch(function (error) {
            console.log(error.data)
            toastr.error('Alerta - Error inesperado  contactar a sistemas.');
        })
}

function TableORCG() {
    axios.get("/PurchaseOrderItem/ActionPurchaseOrderItemList/?PurchaseNo=" + $("#folioSearch").val())
        .then(function (data) {
            if (data.data.length > 0) {
                toastr.success('Se encontro la orden de compra');
                $("#tableOCRG").html("")
                isDifference = data.data.filter(x => x.QuantityOC != x.QuantityRC).length
                if (isDifference > 0) {
                    StartLoading();
                    OCHideOrShow(true)
                    RCHideOrShow(false)
                    $("#tableHeadORCG").html("<tr><th  class='text-center'>Codigo</th><th class='text-center'>Orden de compra cantidad</th><th class='text-center'>Recibo ciego cantidad</th></tr>")
                    table = ""
                    $.each(data.data, function (index, value) {
                        if (value.QuantityOC == value.QuantityRC)
                            table += "<tr><td>" + value.ParthNumber + "</td><td>" + value.QuantityOC + "</td><td>" + value.QuantityRC + "</td><tr>"
                        else
                            table += "<tr><td class='discrepancy'>" + value.ParthNumber + "</td><td class='discrepancy'>" + value.QuantityOC + "</td><td class='discrepancy'>" + value.QuantityRC + "</td><tr>"
                    })
                    $("#tableOCRG").html(table)
                    $("#searchBody").show();
                    EndLoading();
                }
                else {
                    $("#tableHeadORCG").html("<tr><th class='text-center' style='width:8%;'>Articulo</th><th class='text-center' style='width:22%;'>Descripción</th><th class='text-center' style='width:7%;'>Empaque</th><th class='text-center' style='width:10%;'>Cantidad</th><th class='text-center' style='width:10%;'>Unidad</th><th class='text-center' style='width:10%;'>Precio $</th><th class='text-center' style='width:10%;'>IVA Total $</th><th class='text-center' style='width:10%;'>IEPS Total $</th class='text-center'><th class='text-center' style='width:13%;'>Importe Total $</th></tr>")
                    $("#searchBody").show()
                    OCHideOrShow(false)
                    RCHideOrShow(true)
                    OCInfo()
                }
            }
            else {
                toastr.warning('La orden de compra se encontro vacia');
            }
        }).catch(function (error) {
            console.log(error.data)
            toastr.error('Alerta - Error inesperado  contactar a sistemas.');
        })
}

function OCInfo() {
    axios.get("/PurchaseOrderItem/ActionPurchaseOrderItem/?PurchaseNo=" + $("#folio").html())
        .then(function (data) {
            if (data.data.PurchaseOrderList.length > 0) {
                StartLoading();
                $("#tableOCRG").html("")
                ClearOrder();
                $("#provider").text(data.data.CommercialName);
                $("#datee").text(data.data.PurchaseDate);
                $("#currency").text(data.data.Currency);
                $("#email").text(data.data.Email)
                $("#nameContact").text(data.data.NameContact)
                $("#phone").text(data.data.Phone)
                $("#adress").text(data.data.Adress)
                table = ""
                sumTotalIva = 0
                sumTotalIEPS = 0
                sumTotalAmount = 0
                $.each(data.data.PurchaseOrderList, function (index, value) {
                    table += "<tr><td>" + value.PartNumber + "</td><td>" + value.Description + "</td>" + "<td  class='text-right'>" + value.Parcking + "</td>" + "<td  class='text-right'>" + value.Quantity + "</td>" + "<td>" + value.UnitSize + "</td>" + "<td class='text-right'>$" + value.PurchasePrice + "</td>" + "<td class='text-right'>$" + value.Iva + "</td>" + "<td class='text-right'>$" + value.Ieps + "</td>" + "<td class='text-right'>$" + value.ItemAmount + "</td>" + "<tr>"
                    sumTotalIva += value.Iva
                    sumTotalIEPS += value.Ieps
                    sumTotalAmount += value.ItemAmount
                })
                $("#tableOCRG").html(table)
                $("#totalIva").text("$" + sumTotalIva.toFixed(2));
                $("#totalIEPS").text("$" + sumTotalIEPS.toFixed(2));
                $("#totalAmount").text("$" + sumTotalAmount.toFixed(2));
                $("#totalFinal").text("$" + (sumTotalIva + sumTotalIEPS + sumTotalAmount).toFixed(2))
                $("#searchBody").show();
                EndLoading();
            }
        }).catch(function (error) {
            console.log(error.data)
            toastr.error('Alerta - Error inesperado  contactar a sistemas.');
        })
}

function Search() {
    TableORCG();
    $("#folio").html($("#folioSearch").val())
    $("#folio2").html($("#folioSearch").val())
}

function OCHideOrShow(value) {
    if (value) {
        $("#infoSupplier").hide();
        $("#textProvider").hide();
        $("#folio").hide();
        $("#provider").hide();
        $("#datee").hide();
        $("#currency").hide();
        $("#email").hide()
        $("#nameContact").hide()
        $("#phone").hide()
        $("#adress").hide()
        $("#textOrder").hide();
        $("#totalIvaForm").hide();
        $("#totalIEPSForm").hide();
        $("#totalAmountForm").hide();
        $("#totalFinalForm").hide();
        $("#totalIva").hide();
        $("#totalIEPS").hide();
        $("#totalAmount").hide()
        $("#totalFinal").hide();
        $("#saveForm").hide()
    }
    else {
        $("#infoSupplier").show();
        $("#textProvider").show();
        $("#folio").show();
        $("#provider").show();
        $("#datee").show();
        $("#currency").show();
        $("#email").show()
        $("#nameContact").show()
        $("#phone").show()
        $("#adress").show()
        $("#textOrder").show();
        $("#totalIvaForm").show();
        $("#totalIEPSForm").show();
        $("#totalAmountForm").show();
        $("#totalFinalForm").show();
        $("#totalIva").show();
        $("#totalIEPS").show();
        $("#totalAmount").show()
        $("#totalFinal").show();
        $("#saveForm").show()
    }
}

function RCHideOrShow(value) {
    if (value) {
        $("#folioForm").hide();
        $("#textOCRG").hide();
    }
    else {
        $("#folioForm").show();
        $("#textOCRG").show();
    }
}

function ClearOrder() {
    $("#provider").text("");
    $("#datee").text("");
    $("#currency").text("");
    $("#email").text("")
    $("#nameContact").text("")
    $("#phone").text("")
    $("#adress").text("")
}

$("#buttonSave").on("click", function () {
    axios.get("/PurchaseOrderItem/ActionStoreProcedureOCRG/?PurchaseNo=" + $("#folio").html())
        .then(function (data) {
            if (data.data != "" && data.data != null && data.data != "SF") {
                $("#searchBody").hide();
                $("#folioSearch").val("")
                toastr.success('Se realizo con exito el proceso de la orden de compra');
            }
            else if (data.data == "SF") {
                window.location.href = 'http://' + window.location.host + '/Home/Login';
            }
            else {
                toastr.warning('Hubo un problema al procesar la orden de compra');
            }
        }).catch(function (data) {
            console.log(data.data)
            toastr.error('Alerta - Error inesperado  contactar a sistemas.');
        })
});