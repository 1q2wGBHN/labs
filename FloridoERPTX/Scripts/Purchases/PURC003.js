﻿const fomartmoney = money => decimal => new Intl.NumberFormat("en-US", { style: "currency", currency: "USD", currencyDisplay: "symbol", maximumFractionDigits: decimal }).format(money);

$(document).ready(function () {
    window.setTimeout(function () { $("#fa").click(); }, 1000);
    $("#SuppliersDrop").select2({
        minimumInputLength: 2,
        formatInputTooShort: function () { return "Ingresa 2 Caracteres"; },
        matcher: function (term, text, option) { return text.toUpperCase().indexOf(term.toUpperCase()) >= 0 || option.val().toUpperCase().indexOf(term.toUpperCase()) >= 0; }
    });
});

$("#ValidDate1").datepicker({
    autoclose: true,
    todayHighlight: true,
    showButtonPanel: true,
});

$("#ValidDate2").datepicker({
    autoclose: true,
    startDate: '01/01/1950',
    todayHighlight: true,
});

$("#TablePurchases").append('<tfoot><th>Total</th><th colspan="6"></th><th></th><th></th></tfoot>');

var total = 0;
var table = $('#TablePurchases').DataTable({
    oLanguage:
    {
        "sProcessing": "Procesando...",
        "sLengthMenu": "Mostrar _MENU_ registros",
        "sZeroRecords": "No se encontraron resultados",
        "sEmptyTable": "Ningún dato disponible en esta tabla",
        "sInfo": "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
        "sInfoEmpty": "Mostrando registros del 0 al 0 de un total de 0 registros",
        "sInfoFiltered": "(filtrado de un total de _MAX_ registros)",
        "sInfoPostFix": "",
        "sSearch": "Buscar:",
        "sUrl": "",
        "sInfoThousands": ",",
        "sLoadingRecords": "Cargando...",
        "oPaginate": { "sFirst": "Primero", "sLast": "Último", "sNext": "Siguiente", "sPrevious": "Anterior" },
        "oAria": { "sSortAscending": ": Activar para ordenar la columna de manera ascendente", "sSortDescending": ": Activar para ordenar la columna de manera descendente" }
    },
    dom: "<'row'<'col-sm-4'l><'col-sm-4 text-left'B><'col-sm-4'f>t<'col-sm-6'i><'col-sm-6'p>>",
    lengthMenu: [[10, 25, 50, -1], [10, 25, 50, "Todos"]],
    buttons:
        [
            { extend: 'csv', text: 'Excel', title: 'EficienciaProveedores', className: 'btn-sm' },
            {
                extend: 'pdf', text: 'PDF', title: 'EficienciaProveedores', className: 'btn-sm',
                exportOptions: { columns: [1, 2, 3] },
                customize: function (doc) {
                    doc.content[1].table.widths = Array(doc.content[1].table.body[0].length + 1).join('*').split('');
                }
            },
            { extend: 'print', text: 'Imprimir', className: 'btn-sm' },
        ],
    "footerCallback": function (row, data, start, end, display) {
        var api = this.api(), data;
        // converting to interger to find total
        var intVal = function (i) {
            return typeof i === 'string' ?
                i.replace(/[\$,]/g, '') * 1 :
                typeof i === 'number' ?
                    i : 0;
        };

        for (var i = 7; i < 9; i++) {
            var monTotal = api
                .column(i)
                .data()
                .reduce(function (a, b) {
                    return intVal(a) + intVal(b);
                }, 0);
            var count = monTotal;

            if (total > 0)
                monTotal = monTotal / total;

            //$(api.column(i).footer()).html(`<span ${monTotal == 0 ? "" : `style = "Color:${monTotal >= 0 ? "green" : "red"}"`} > ${fomartmoney(monTotal)(4)}</span >`);
            $(api.column(i).footer()).html(`<span ${monTotal == 0 ? "" : `style = "Color:${monTotal >= 0 ? "green" : "red"}"`} > ${monTotal.toFixed(4)} %</span >`);
        }
    },
    columns:
        [
            {
                "class": "details-control",
                "orderable": false,
                "data": null,
                "defaultContent": "",
            },
            { data: 'PurchaseNo' },
            { data: 'PurchaseDate' },
            { data: 'requestedQuantity' },
            { data: 'assortedQuantity' },
            { data: 'requestedAmount' },
            { data: 'assortedAmount' },
            { data: 'efficiencyInAmount' },
            { data: 'efficiencyInQuantity' }
        ],
    columnDefs:
        [
            {
                targets: [2],
                render: function (data) {
                    return moment(data).format('l');
                }
            },
            {
                targets: [5, 6],
                render: function (data) {
                    return "$ " + data;
                }
            },
            {
                targets: [7, 8],
                render: function (data) {
                    return data + " %";
                }
            }
        ]
});

var x = "";
var detailRows = [];
$('#TablePurchases tbody').on('click', 'tr td.details-control', function () {
    var tr = $(this).closest('tr');
    var row = table.row(tr);
    if (row.child.isShown()) {
        row.child.hide();
        tr.removeClass('details');
    }
    else {
        if (table.row('.details').length) {
            $('.details-control', table.row('.details').node()).click();
        }
        row.child(format(row.data())).show();
        tr.addClass('details');
    }
});

table.on('draw', function () {
    $.each(detailRows, function (i, id) {
        $('#' + id + ' td.details-control').trigger('click');
    });
});

function format(d) {
    var detailItems = "";
    var tabledetail = $('<div/>').addClass('loading').text('Cargando Datos...');
    $.ajax({
        url: "/PurchaseOrder/ActionGetAllPurchasesDetailByPurchaseNo",
        data: { "poNumber": d.PurchaseNo },
        type: "GET",
        success: function (Data) {
            $.each(Data, function (index, value) {
                detailItems += "<tr><td>" + value.PartNumber + "</td><td>" + value.Description + "</td><td>" + value.requestedQuantity + "</td><td class='text-right'>" + value.assortedQuantity + "</td><td class='text-right'>" + "$ " + value.requestedAmount + "</td><td class='text-right'>" + "$ " + value.assortedAmount + "</td><td class='text-right'>" + value.efficiencyInAmount + " %" + "</td><td class='text-right'>" + value.efficiencyInQuantity + " %" + "</td></tr>"
            });
            tabledetail.html('<table id="tabledetail" class="table table-striped table-bordered table-hover" style="width:100%">' +
                '<thead><tr><th>Codigo</th><th>Nombre</th><th>Cantidad Solicitada</th><th>Cantidad Surtida</th><th>Monto Solicitado</th><th>Monto Surtido</th><th>Eficiencia Monto</th><th>Eficiencia Cantidad</th></tr></thead><tbody>' + detailItems + '</tbody></table>').removeClass('loading');
            reloadStyleTable();
        }
    });
    return tabledetail
}

function reloadStyleTable() {
    $('#tabledetail').DataTable({
        oLanguage:
        {
            "sProcessing": "Procesando...",
            "sLengthMenu": "Mostrar _MENU_ registros",
            "sZeroRecords": "No se encontraron resultados",
            "sEmptyTable": "Ningún dato disponible en esta tabla",
            "sInfo": "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
            "sInfoEmpty": "Mostrando registros del 0 al 0 de un total de 0 registros",
            "sInfoFiltered": "(filtrado de un total de _MAX_ registros)", "sInfoPostFix": "",
            "sSearch": "Buscar:", "sUrl": "", "sInfoThousands": ",", "sLoadingRecords": "Cargando...",
            "oPaginate": { "sFirst": "Primero", "sLast": "Último", "sNext": "Siguiente", "sPrevious": "Anterior" },
            "oAria":
            {
                "sSortAscending": ": Activar para ordenar la columna de manera ascendente",
                "sSortDescending": ": Activar para ordenar la columna de manera descendente"
            }
        }
    });
}

$('#buscar').on('click', function (r, e) {
    toastr.options =
    {
        "debug": false,
        "newestOnTop": false,
        "positionClass": "toast-top-center",
        "closeButton": true,
        "debug": false,
        "toastClass": "animated fadeInDown",
    };
    if ($("#ValidDate1").val() == "") {
        toastr.error('Selecciona una fecha');
        $("#ValidDate1").val("");
        $("#ValidDate1").focus();
    }
    else if ($("#ValidDate2").val() == "") {
        toastr.error('Selecciona una fecha');
        $("#ValidDate2").val("");
        $("#ValidDate2").focus();
    }
    else if (moment($("#ValidDate2").val()) < moment($("#ValidDate1").val())) {
        toastr.error('Fecha Final no puede ser menor a Inicial');
        $("#ValidDate2").val("");
        $("#ValidDate2").focus();
    }
    else if ($("#SuppliersDrop").val() == "") {
        toastr.error('Selecciona un Proveedor.');
        $('#SuppliersDrop').select2("open");
    }
    else {
        StartLoading();
        $.ajax({
            url: "/PurchaseOrder/ActionGetAllPurchasesOrdersBySupplier",
            type: "POST",
            data: { "Date1": $("#ValidDate1").val(), "Date2": $("#ValidDate2").val(), "supplier": $("#SuppliersDrop").val() },
            success: function (returndata) {
                EndLoading();
                if (returndata.success) {
                    total = returndata.Orders.length;
                    table.clear().draw();
                    table.rows.add(returndata.Orders)
                    table.columns.adjust().draw();
                    x = returndata.ItemsOrders;
                    $("#button").removeAttr('disabled', 'disabled');
                }
                else {
                    if (returndata.responseText === 'Termino tu sesión.') {
                        SessionFalse(returndata.responseText);
                    }
                    else {
                        toastr.warning(returndata.responseText);
                        table.clear().draw();
                        $("#button").attr('disabled', true);
                    }
                }
            },
            error: function () {
                EndLoading();
                toastr.error('Error inesperado contactar a sistemas.');
            }
        });
    }
});