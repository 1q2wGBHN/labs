﻿document.addEventListener("DOMContentLoaded", function (event) {
    window.setTimeout(function () { document.getElementById("fa").click(); }, 1000);
});

function tab1() {
    $('#tab-1').animatePanel();
}

function tab2() {
    $('#tab-2').animatePanel();
}

$("#Items").select2({
    minimumInputLength: 3,
    allowClear: true,
    placeholder: "Seleccione un producto",
    initSelection: function (element, callback) {
        callback({ id: "", text: "Seleccione una opcion" });
    },
    ajax: {
        url: "/Items/ActionSearchItemBarcode/",
        dataType: 'json',
        type: "GET",
        quietMillis: 50,
        data: function (Filter) {
            return {
                Filter: Filter
            };
        },
        results: function (data) {
            return {
                results: $.map(data.Json, function (item) {
                    return {
                        text: item.Description,
                        id: item.PathNumber + '*/*/*/' + item.Description
                    }
                }),
            };
        }
    }
});

var Records = $('#Records').DataTable({
    oLanguage: {
        "sProcessing": "Procesando...", "sLengthMenu": "Mostrar _MENU_ registros", "sZeroRecords": "No se encontraron resultados", "sEmptyTable": "Ningún dato disponible en esta tabla", "sInfo": "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros", "sInfoEmpty": "Mostrando registros del 0 al 0 de un total de 0 registros", "sInfoFiltered": "(filtrado de un total de _MAX_ registros)", "sInfoPostFix": "", "sSearch": "Buscar:", "sUrl": "", "sInfoThousands": ",", "sLoadingRecords": "Cargando...", "oPaginate": { "sFirst": "Primero", "sLast": "Último", "sNext": "Siguiente", "sPrevious": "Anterior" }, "oAria": { "sSortAscending": ": Activar para ordenar la columna de manera ascendente", "sSortDescending": ": Activar para ordenar la columna de manera descendente" }
    },
    dom: "<'row'<'col-sm-4'l><'col-sm-4 text-left'B><'col-sm-4'f>t<'col-sm-6'i><'col-sm-6'p>>",
    responsive: true,
    autoWidth: true,
    order: [[1, "desc"]],
    buttons: [
        { extend: 'copy', text: 'Copiar', className: 'btn-sm' },
        { extend: 'csv', text: 'Excel', title: 'Consumo Interno', className: 'btn-sm' },
        {
            extend: 'pdf', text: 'PDF', title: 'Consumo Interno', className: 'btn-sm', exportOptions: {
                columns: [1, 2, 3, 4, 5]
            },
            customize: function (doc) {
                doc.content[1].table.widths =
                    Array(doc.content[1].table.body[0].length + 1).join('*').split('');
            }
        },
        { extend: 'print', text: 'Imprimir', className: 'btn-sm' }
    ],
    columns:
        [
            {
                "class": "details-control",
                "orderable": false,
                "data": null,
                "defaultContent": "",
                width: "1%",
                targets: 0
            },
            { targets: 1, data: 'GiCostCenterId', width: "1%" },
            { targets: 2, data: 'CostCenter', width: "1%" },
            { targets: 3, data: 'Status', width: "1%" },
            { targets: 4, data: 'Amount', width: "1%" },
            { targets: 5, data: 'CreateDate', width: "1%" },
            {
                targets: [6],
                orderable: false,
                data: null,
                width: "1%",
                className: "text-center",
                render: function (data, type, full, meta) {
                    if (full.Status == "Inicial") {
                        return "<button class='btn btn-xs btn-outline btn-warning' onclick=EditRecord(" + full.GiCostCenterId + ")><i class='fa fa-info-circle'></i> Editar</button>";
                    }
                    else if (full.Status == "Terminado") {
                        return "<button class='btn btn-xs btn-outline btn-info' onclick=GetReport(" + full.GIDocument + ")><i class='fa fa-print'></i> Reporte</button>";
                    }
                    else
                        return "";
                }
            }
        ],
    columnDefs: [
        { targets: 4, type: 'numeric-comma' },
        {
            targets: [4],
            type: "numeric-comma",
            render: function (data, type, row) {
                return '$' + data.toFixed(2);
            }
        },
        {
            targets: [5],
            render: function (data, type, row) {
                return moment(data).format('DD/MM/YYYY');
            }
        }]
});

document.getElementById("Records").insertAdjacentHTML("beforeend", "<tfoot><tr><th></th><th colspan='2'>Total</th><th id='totalMovement' colspan='5'>$0</th></tr></tfoot>");

var RecordsDetail = $('#RecordsDetail').DataTable({
    oLanguage: {
        "sProcessing": "Procesando...", "sLengthMenu": "Mostrar _MENU_ registros", "sZeroRecords": "No se encontraron resultados", "sEmptyTable": "Ningún dato disponible en esta tabla", "sInfo": "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros", "sInfoEmpty": "Mostrando registros del 0 al 0 de un total de 0 registros", "sInfoFiltered": "(filtrado de un total de _MAX_ registros)", "sInfoPostFix": "", "sSearch": "Buscar:", "sUrl": "", "sInfoThousands": ",", "sLoadingRecords": "Cargando...", "oPaginate": { "sFirst": "Primero", "sLast": "Último", "sNext": "Siguiente", "sPrevious": "Anterior" }, "oAria": { "sSortAscending": ": Activar para ordenar la columna de manera ascendente", "sSortDescending": ": Activar para ordenar la columna de manera descendente" }
    },
    dom: "<'row'<'col-sm-4'l><'col-sm-4 text-left'B><'col-sm-4'f>t<'col-sm-6'i><'col-sm-6'p>>",
    responsive: true,
    autoWidth: true,
    order: [[0, "desc"]],
    buttons: [
        { extend: 'copy', text: 'Copiar', className: 'btn-sm' },
        { extend: 'csv', text: 'Excel', title: 'Consumo Interno Detalle', className: 'btn-sm' },
        {
            extend: 'pdf', text: 'PDF', title: 'Consumo Interno Detalle', className: 'btn-sm', orientation: 'landscape', pageSize: 'LEGAL', exportOptions: {
                columns: [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11]
            },
            customize: function (doc) {
                doc.content[1].table.widths =
                    Array(doc.content[1].table.body[0].length + 1).join('*').split('');
            }
        },
        { extend: 'print', text: 'Imprimir', className: 'btn-sm' }
    ],
    columns:
        [
            { targets: 0, data: 'GiDocument', width: "1%" },
            { targets: 1, data: 'PartNumber', width: "1%" },
            { targets: 2, data: 'Description', width: "1%" },
            { targets: 3, data: 'CostCenter', width: "1%" },
            { targets: 4, data: 'Department', width: "1%" },
            { targets: 5, data: 'Family', width: "1%" },
            { targets: 6, data: 'Quantity', width: "1%", className: "text-center" },
            { targets: 7, data: 'UnitCost', width: "1%" },
            { targets: 8, data: 'Amount', width: "1%" },
            { targets: 9, data: 'Ieps', width: "1%" },
            { targets: 10, data: 'Iva', width: "1%" },
            { targets: 11, data: 'Total', width: "1%" }
        ],
    columnDefs: [
        {
            targets: [7],
            type: "numeric-comma",
            render: function (data, type, row) {
                return '$' + data.toFixed(2);
            }
        },
        {
            targets: [8],
            type: "numeric-comma",
            render: function (data, type, row) {
                return '$' + data.toFixed(2);
            }
        },
        {
            targets: [9],
            type: "numeric-comma",
            render: function (data, type, row) {
                return '$' + data.toFixed(2);
            }
        },
        {
            targets: [10],
            type: "numeric-comma",
            render: function (data, type, row) {
                return '$' + data.toFixed(2);
            }
        },
        {
            targets: [11],
            type: "numeric-comma",
            render: function (data, type, row) {
                return '$' + data.toFixed(4);
            }
        }]
});

document.getElementById("RecordsDetail").insertAdjacentHTML("beforeend", "<tfoot><tr><th></th><th colspan='2'>Total</th><th id='totalDetail' colspan='9'>$0</th></tr></tfoot>");

function LoadRecords(data, reportType) {
    if (reportType == "movimiento") {
        document.getElementById("detail").classList.add("hide");
        RecordsDetail.clear();
        RecordsDetail.columns.adjust().draw();
        document.getElementById("movement").classList.remove("hide");
        FillTableTotalMovement(data);
        Records.clear();
        Records.rows.add(data);
        Records.columns.adjust().draw();
    }
    else {
        document.getElementById("movement").classList.add("hide");
        Records.clear();
        Records.columns.adjust().draw();
        document.getElementById("detail").classList.remove("hide");

        if (data.length > 0)
            document.getElementById("btnReport").disabled = false;
        else
            document.getElementById("btnReport").disabled = true;

        FillTableTotalDetail(data);
        RecordsDetail.clear();
        RecordsDetail.rows.add(data);
        RecordsDetail.columns.adjust().draw();
    }

    $('#TableDivRecords').animatePanel();
    $('html, body').animate({ scrollTop: 0 }, "slow");
}

var model = JSON.parse(document.getElementById("model").value);
LoadRecords(model, "movimiento");

function reloadStyleTable() {
    $('#tabledetail').DataTable({
        oLanguage: {
            "sProcessing": "Procesando...", "sLengthMenu": "Mostrar _MENU_ registros", "sZeroRecords": "No se encontraron resultados", "sEmptyTable": "Ningún dato disponible en esta tabla", "sInfo": "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros", "sInfoEmpty": "Mostrando registros del 0 al 0 de un total de 0 registros", "sInfoFiltered": "(filtrado de un total de _MAX_ registros)", "sInfoPostFix": "", "sSearch": "Buscar:", "sUrl": "", "sInfoThousands": ",", "sLoadingRecords": "Cargando...", "oPaginate": { "sFirst": "Primero", "sLast": "Último", "sNext": "Siguiente", "sPrevious": "Anterior" }, "oAria": { "sSortAscending": ": Activar para ordenar la columna de manera ascendente", "sSortDescending": ": Activar para ordenar la columna de manera descendente" }
        },
        columnDefs: [{
            targets: [2],
            width: "1%"
        }]
    });
}

function format(d) {
    var detailItems = "";
    var tabledetail = $('<div/>').addClass('loading').text('Cargando Datos...');
    $.ajax({
        url: "/CostCenter/ActionGetCostCenterItems",
        data: { "GiCostCenterId": d.GiCostCenterId },
        type: "POST",
        success: function (response) {
            if (response.result == "SF")
                SessionFalse("Terminó tu sesión");
            else if (response.result) {
                response.Data.forEach(function (item) {
                    detailItems += "<tr><td>" + item.PartNumber + "</td><td>" + item.Description + "</td><td>" + item.Department + "</td><td>" + item.Family + "</td><td>" + item.Quantity + "</td><td>$" + item.UnitCost + "</td><td>$" + item.Amount + "</td><td>$" + item.Ieps + "</td><td>$" + item.Iva + "</td><td>$" + item.Total + "</td>";
                });
                if (response.Data.length > 0) {
                    tabledetail.html('<table id="tabledetail" class="table table-striped table-bordered table-hover" style="width:100%"><thead><tr><th>Código</th><th>Descripción</th><th>Departamento</th><th>Familia</th><th>Cantidad</th><th>Precio</th><th>Importe</th><th>IEPS</th><th>IVA</th><th>Total</th></tr></thead><tbody>' + detailItems + '</tbody></table>').removeClass('loading');
                    reloadStyleTable();
                }
                tabledetail.html('<table id="tabledetail" class="table table-striped table-bordered table-hover" style="width:100%"><thead><tr><th>Código</th><th>Descripción</th><th>Departamento</th><th>Familia</th><th>Cantidad</th><th>Precio</th><th>Importe</th><th>IEPS</th><th>IVA</th><th>Total</th></tr></thead><tbody>' + detailItems + '</tbody></table>').removeClass('loading');
                reloadStyleTable();
            }
        }
    });
    return tabledetail;
}

var detailRows = [];
$('#Records tbody').on('click', 'tr td.details-control', function () {
    var tr = $(this).closest('tr');
    var row = Records.row(tr);

    if (row.child.isShown()) {
        row.child.hide();
        tr.removeClass('details');
    }
    else {
        if (Records.row('.details').length) {
            $('.details-control', Records.row('.details').node()).click();
        }
        row.child(format(row.data())).show();
        tr.addClass('details');
    }
});

Records.on('draw', function () {
    $.each(detailRows, function (i, id) {
        $('#' + id + ' td.details-control').trigger('click');
    });
});

function clearDates() {
    document.getElementById("beginDate").value = "";
    document.getElementById("endDate").value = "";
}

function SearchRecords() {
    var beginDate = document.getElementById("beginDate").value;
    var endDate = document.getElementById("endDate").value;
    var reportType = document.getElementById("ReportType").value;

    if (beginDate != "" & endDate != "") {
        if (new Date(beginDate) <= new Date(endDate)) {
            StartLoading();
            var xhttp = new XMLHttpRequest();
            xhttp.onreadystatechange = function () {
                if (this.readyState == 4 && this.status == 200) {
                    EndLoading();
                    var response = JSON.parse(this.responseText);
                    if (response.result == "SF")
                        SessionFalse("Terminó tu sesión");
                    else if (response.result)
                        LoadRecords(response.Data, reportType);
                    else
                        toastr.error('Alerta - Error inesperado contactar a sistemas.');
                }
                else if (this.readyState == 4 && this.status != 200) {
                    EndLoading();
                    toastr.error("Alerta - Error inesperado  contactar a sistemas.");
                }
            }
            xhttp.open("GET", "/CostCenter/ActionGetCostCenterRecords?BeginDate=" + beginDate + "&EndDate=" + endDate + "&ReportType=" + reportType, true);
            xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
            xhttp.send(null);
        }
        else {
            toastr.error("Fecha final no puede ser menor a la inicial.");
            clearDates();
            document.getElementById("beginDate").focus();
        }
    }
}

$("#beginDate").datepicker({
    autoclose: true,
    todayHighlight: true,
    showButtonPanel: true,
}).on("changeDate", function (e) {
    SearchRecords();
});

$("#endDate").datepicker({
    autoclose: true,
    startDate: '01/01/1950',
    todayHighlight: true,
}).on("changeDate", function (e) {
    SearchRecords();
});

var Items = $('#Products').DataTable({
    oLanguage: {
        "sProcessing": "Procesando...", "sLengthMenu": "Mostrar _MENU_ registros", "sZeroRecords": "No se encontraron resultados", "sEmptyTable": "Ningún dato disponible en esta tabla", "sInfo": "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros", "sInfoEmpty": "Mostrando registros del 0 al 0 de un total de 0 registros", "sInfoFiltered": "(filtrado de un total de _MAX_ registros)", "sInfoPostFix": "", "sSearch": "Buscar:", "sUrl": "", "sInfoThousands": ",", "sLoadingRecords": "Cargando...", "oPaginate": { "sFirst": "Primero", "sLast": "Último", "sNext": "Siguiente", "sPrevious": "Anterior" }, "oAria": { "sSortAscending": ": Activar para ordenar la columna de manera ascendente", "sSortDescending": ": Activar para ordenar la columna de manera descendente" }
    },
    responsive: true,
    autoWidth: true,
    columns:
        [
            { targets: 0, data: 'PartNumber', width: "1%" },
            { targets: 1, data: 'Description', width: "1%" },
            { targets: 2, data: 'Quantity', width: "1%", className: "text-center" },
            { targets: 3, data: null, width: "1%", className: "text-center", orderable: false },
        ],
    columnDefs:
        [
            {
                targets: [3],
                width: "1%",
                render: function (data, type, full, meta) {
                    return '<button class="btn btn-xs btn-outline btn-danger" onclick="DeleteItem(' + meta.row + ')"><i class="fa fa-close"></i> Eliminar</button>';
                }
            }
        ]
});

function FillTableTotalMovement(data) {
    total = 0;
    data.forEach(function (element) {
        total += element.Amount;
    });
    document.getElementById("totalMovement").innerHTML = "$" + total.toFixed(4);
}

function FillTableTotalDetail(data) {
    total = 0;
    data.forEach(function (element) {
        total += element.Total;
    });
    document.getElementById("totalDetail").innerHTML = "$" + total.toFixed(4);
}

function LoadItems(data) {
    Items.clear().draw();
    Items.rows.add(data);
    Items.columns.adjust().draw();
}

var ItemsList = [];
function EditRecord(id) {
    var xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function () {
        if (this.readyState == 4 && this.status == 200) {
            EndLoading();
            var response = JSON.parse(this.responseText);
            if (response.result == "SF")
                SessionFalse("Terminó tu sesión");
            else if (response.result) {
                ItemsList = [];

                document.getElementById("CenterCostId").value = response.Data[0].GiCostCenterId;
                document.getElementById("CostCenter").value = response.Data[0].CostCenter;
                $("#CostCenter").trigger("change");

                if (response.Data.length > 0) {
                    response.Data.forEach(function (item) {
                        var newItem = { PartNumber: item.PartNumber, Description: item.Description, Quantity: item.Quantity }
                        ItemsList.push(newItem);
                    });

                    LoadItems(ItemsList);
                    document.getElementById("btnSave").removeAttribute("disabled");
                    document.getElementById("btnProcess").removeAttribute("disabled");
                }

                document.getElementById("tab2").click();
                $('html, body').animate({ scrollTop: 0 }, "slow");
            }
            else
                toastr.error("Alerta - Error inesperado  contactar a sistemas.");
        }
        else if (this.readyState == 4 && this.status != 200) {
            EndLoading();
            toastr.error("Alerta - Error inesperado  contactar a sistemas.");
        }
    }
    xhttp.open("GET", "/CostCenter/ActionGetCostCenter?CenterCostId=" + id, true);
    xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    xhttp.send(null);
}

function GetReport(doc) {
    StartLoading();
    var xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function () {
        if (this.readyState == 4 && this.status == 200) {
            EndLoading();
            var response = JSON.parse(this.responseText);
            if (response.result == "SF")
                SessionFalse("Terminó su sesión");
            else if (response.result) {
                document.getElementById("modalReportBody").innerHTML = "<iframe width='100%' height='550px' src='data:application/pdf;base64, " + encodeURI(response.Report) + "'></iframe>";
                $("#modalReport").modal('show');
            }
            else
                toastr.error('Alerta - Error inesperado contactar a sistemas.');
        }
        else if (this.readyState == 4 && this.status != 200) {
            EndLoading();
            toastr.error("Alerta - Error inesperado  contactar a sistemas.");
        }
    }
    xhttp.open("POST", "/CostCenter/ActionGetCostCenterReport", true);
    xhttp.setRequestHeader("Content-Type", "application/json;charset=UTF-8");
    xhttp.send(JSON.stringify({ "GiDocument": doc }));
}

function ValidateDuplicates(ItemsList) {
    var newItemsList = [];

    ItemsList.forEach(function (item) {
        if (newItemsList.length > 0) {
            var find = newItemsList.find(element => element.PartNumber == item.PartNumber);

            if (find) {
                for (var i in newItemsList) {
                    if (newItemsList[i].PartNumber == item.PartNumber) {
                        newItemsList[i].Quantity = item.Quantity + find.Quantity;
                        break;
                    }
                }
            }
            else {
                var newItem = { PartNumber: item.PartNumber, Description: item.Description, Quantity: item.Quantity }
                newItemsList.push(newItem);
            }
        }
        else {
            var newItem = { PartNumber: item.PartNumber, Description: item.Description, Quantity: item.Quantity }
            newItemsList.push(newItem);
        }
    });

    return newItemsList;
}

$(document).on("keypress", '.Numeric', function (event) {
    var keyValue = event.key;
    if (keyValue.match(/[0-9]/g))
        return true;
    else
        return false;
});

function AddItem() {
    toastr.remove();
    var costcenter = document.getElementById("CostCenter").value;
    var item = document.getElementById("Items").value;
    var quantity = document.getElementById("Quantity").value;

    if (costcenter == "0") {
        toastr.warning("Seleccione un centro de costos");
        $("#CostCenter").select2('open');
        return;
    }
    else if (item == "") {
        toastr.warning("Ingrese un producto");
        $("#Items").select2('open');
        return;
    }
    else if (quantity == "") {
        toastr.warning("Ingrese una cantidad");
        document.getElementById("Quantity").focus();
        return;
    }

    var part_number = item.split("*/*/*/")[0];
    var part_description = item.split("*/*/*/")[1];
    var newItem = { PartNumber: part_number, Description: part_description, Quantity: parseInt(quantity) }
    ItemsList.push(newItem);

    ItemsList = ValidateDuplicates(ItemsList);
    LoadItems(ItemsList);

    document.getElementById("Items").value = "";
    $("#Items").trigger("change");
    document.getElementById("Quantity").value = "";
    document.getElementById("btnSave").removeAttribute("disabled");
    document.getElementById("btnProcess").removeAttribute("disabled");
}

function DeleteItem(num) {
    ItemsList.splice(num, 1);
    LoadItems(ItemsList);

    if (ItemsList.length == 0) {
        document.getElementById("btnSave").setAttribute("disabled", "disabled");
        document.getElementById("btnProcess").setAttribute("disabled", "disabled");
    }
}

function Clear() {
    ItemsList = [];
    Items.clear();
    Items.columns.adjust().draw();
    $('#TableDiv').animatePanel();
    document.getElementById("btnSave").setAttribute("disabled", "disabled");
    document.getElementById("btnProcess").setAttribute("disabled", "disabled");
}

function Save() {
    var costcenter = document.getElementById("CostCenter").value;
    var Id = document.getElementById("CenterCostId").value;
    var reportType = document.getElementById("ReportType").value;

    if (ItemsList.length > 0 & costcenter != "") {
        swal({
            title: "¿Esta seguro que desea registrar el consumo?",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Continuar",
            cancelButtonText: "Cancelar"
        }, function (isConfirm) {
            if (isConfirm) {
                StartLoading();
                var xhttp = new XMLHttpRequest();
                xhttp.onreadystatechange = function () {
                    if (this.readyState == 4 && this.status == 200) {
                        EndLoading();
                        var response = JSON.parse(this.responseText);
                        if (response.result == "SF")
                            SessionFalse("Terminó tu sesión");
                        else if (response.result) {
                            if (Id == 0) {
                                document.getElementById("CenterCostId").value = response.Id;
                                clearDates();
                                LoadRecords(response.Data, reportType);
                            }
                            else
                                $('html, body').animate({ scrollTop: 0 }, "slow");
                            toastr.success("Registrado correctamente");
                        }
                        else
                            toastr.error('Alerta - Error inesperado contactar a sistemas.');
                    }
                    else if (this.readyState == 4 && this.status != 200) {
                        EndLoading();
                        toastr.error("Alerta - Error inesperado  contactar a sistemas.");
                    }
                }
                xhttp.open("POST", "/CostCenter/ActionSaveCostCenter", true);
                xhttp.setRequestHeader("Content-Type", "application/json;charset=UTF-8");
                xhttp.send(JSON.stringify({ "CostCenter": costcenter, "Status": 0, "Items": ItemsList, "CenterCostId": Id, "ReportType": reportType }));
            }
        });
    }
}

function Process() {
    var costcenter = document.getElementById("CostCenter").value;
    var Id = document.getElementById("CenterCostId").value;
    var reportType = document.getElementById("ReportType").value;

    if (ItemsList.length > 0 & costcenter != "") {
        swal({
            title: "Esta seguro?",
            text: "Se solicitará la aprobación del consumo",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Continuar",
            cancelButtonText: "Cancelar"
        }, function (isConfirm) {
            if (isConfirm) {
                StartLoading();
                var xhttp = new XMLHttpRequest();
                xhttp.onreadystatechange = function () {
                    if (this.readyState == 4 && this.status == 200) {
                        EndLoading();
                        var response = JSON.parse(this.responseText);
                        if (response.result == "SF")
                            SessionFalse("Terminó tu sesión");
                        else if (response.result) {
                            document.getElementById("CenterCostId").value = "0";
                            document.getElementById("CostCenter").value = "0";
                            $("#CostCenter").trigger("change");
                            Clear();
                            toastr.success("Procesado correctamente");
                            clearDates();
                            LoadRecords(response.Data, reportType);
                        }
                        else
                            toastr.error('Alerta - Error inesperado contactar a sistemas.');
                    }
                    else if (this.readyState == 4 && this.status != 200) {
                        EndLoading();
                        toastr.error("Alerta - Error inesperado  contactar a sistemas.");
                    }
                }
                xhttp.open("POST", "/CostCenter/ActionProcessCostCenter", true);
                xhttp.setRequestHeader("Content-Type", "application/json;charset=UTF-8");
                xhttp.send(JSON.stringify({ "CostCenter": costcenter, "Status": 1, "Items": ItemsList, "CenterCostId": Id, "ReportType": reportType }));
            }
        });
    }
}

function ReportDetail() {
    var beginDate = document.getElementById("beginDate").value;
    var endDate = document.getElementById("endDate").value;

    if (beginDate != "" & endDate != "") {
        if (new Date(beginDate) <= new Date(endDate)) {
            StartLoading();
            var xhttp = new XMLHttpRequest();
            xhttp.onreadystatechange = function () {
                if (this.readyState == 4 && this.status == 200) {
                    EndLoading();
                    var response = JSON.parse(this.responseText);
                    if (response.result == "SF")
                        SessionFalse("Terminó tu sesión");
                    else if (response.result) {
                        document.getElementById("modalReportBody").innerHTML = "<iframe width='100%' height='550px' src='data:application/pdf;base64, " + encodeURI(response.Report) + "'></iframe>";
                        $("#modalReport").modal('show');
                    }
                    else
                        toastr.error('Alerta - Error inesperado contactar a sistemas.');
                }
                else if (this.readyState == 4 && this.status != 200) {
                    EndLoading();
                    toastr.error("Alerta - Error inesperado  contactar a sistemas.");
                }
            }
            xhttp.open("POST", "/CostCenter/ActionGetCostCenterDetailReport", true);
            xhttp.setRequestHeader("Content-Type", "application/json;charset=UTF-8");
            xhttp.send(JSON.stringify({ "BeginDate": beginDate, "EndDate": endDate }));
        }
        else {
            toastr.error("Fecha final no puede ser menor a la inicial.");
            clearDates();
            document.getElementById("beginDate").focus();
        }
    }
}