﻿var folioSearch = 0;
var listAutorize = []
var listItem = []
const sesion = "Se termino su sesion.";

$(document).ready(function () {
    $("#orderSearch").hide()
    window.setTimeout(function () { $("#fa").click(); }, 1000);
    $('#buttonSave').click(function () {
        if ($("#dateOrder").val().trim() != "" && IsDate($("#dateOrder").val())) {
            if (listAutorize.filter(x => x.authorized > 0).length > 0) {
                swal({
                    title: "¿Desea generar la orden de compra con la moneda " + $('#currency').val() + " ?",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: "#DD6B55",
                    confirmButtonText: "Si",
                    cancelButtonText: "No",
                    closeOnConfirm: true,
                    closeOnCancel: true
                }, function (isConfirm) {
                    if (isConfirm)
                        Save()
                });
            }
            else
                toastr.warning("La orden no puede ir en cero ")
        }
        else {
            setTimeout(function () {
                position = $("#dateOrder").offset()
                $('html, body').animate({ scrollTop: position.top - 380 }, "slow");
                $("#OrderError").show();
                $("#dateOrder").addClass("error");
                if ($("#dateOrder").val().trim() != null && $("#dateOrder").val().trim() != "") {
                    $("#OrderError").html("Poner una fecha valida")
                }
                else
                    $("#OrderError").html("El Campo no puede ser nulo")
                $("#OrderError").fadeOut(5000, function () {
                    $("#dateOrder").removeClass("error")
                })
            }, 100);
            $(".datepicker").hide();
            toastr.error('necesita tener una fecha');
        }
    });

    $('#removeOrderList').click(
        function () {
            if ($("#folioOrderSearch").val() != null && $("#folioOrderSearch").val() != 0) {
                swal({
                    title: "¿Desea cancelar el formato de pedido?",
                    text: "Si cancela no podrá recuperarse.",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: "#DD6B55",
                    confirmButtonText: "Si",
                    cancelButtonText: "No",
                    closeOnConfirm: true,
                    closeOnCancel: true
                }, function (isConfirm) {
                    if (isConfirm)
                        RemoveOrder()
                });
            }
            else
                toastr.warning("No hay folios")
        });
    $(".js-source-states-2").select2();
    ComboboxCurrency();
    $('#datetimepicker').datepicker({
        autoclose: true,
        startDate: new Date(),
        todayHighlight: true,
    });
});

function ComboboxCurrency() {
    axios.get('/MaCode/ActionGetListCurrency')
        .then(function (returndates) {
            var currencyComboBox = ""
            $.each(returndates.data, function (Purchase002, value) {
                currencyComboBox += '<option val="' + value.VKey + '">' + value.VKey + '</option>'
            });
            $("#currency").html(currencyComboBox);
            $('#currency').val($("#currency :first").val()).trigger('change');
        })
}

function UpdateOrder() {
    if ($("#folioOrderSearch").val() != null && $("#folioOrderSearch").val() != 0) {
        axios.get("/Orders/ActionOrderStatus/?Folio=" + $("#folioOrderSearch").val())
            .then(function (data) {
                if (data.data == 1) {
                    StartLoading();
                    listAutorize = []
                    listItem = []
                    folioSearch = $("#folioOrderSearch").val()
                    $("#folio").html(folioSearch)
                    $("#orderSearch").show()
                    $("#orderSearch").animatePanel()
                    Order()
                    OrderList()
                }
                else if (data.data == "SF") {
                    SessionFalse(sesion)
                }
                else {
                    toastr.warning("No se encontro el Folio")
                    $("#orderSearch").hide()
                }
            })
            .catch(function (error) {
                toastr.error('Alerta - Error inesperado contactar a sistemas.');
            });
    }
    else
        toastr.warning("No hay folios")
}

function RemoveOrder() {
    if ($("#folioOrderSearch").val() != null && $("#folioOrderSearch").val() != 0) {
        axios.get("/Orders/ActionCancelStatusOrder/?Folio=" + $("#folioOrderSearch").val())
            .then(function (data) {
                if (data.data == "SF") {
                    SessionFalse(sesion)
                }
                else if (data.data) {
                    toastr.success("El formato de pedido ha sido cancelado");
                    Clear();
                }
                else {
                    toastr.warning("No se encontro el Folio")
                }
            }).catch(function (error) {
                toastr.error('Alerta - Error inesperado contactar a sistemas.');
            })
    }
    else {
        toastr.warning("No hay folios")
    }
}

function Order() {
    axios.get("/Orders/ActionOrder?Folio=" + folioSearch).
        then(function (returndates) {
            GetCurrency();
            $("#supplier").html('<option value="' + returndates.data.SupplierId + '">' + returndates.data.Supplier + '</option>')
            $("#supplier").prop('disabled', 'disabled');
            $("#site").html(' <option value="' + returndates.data.SiteId + '">' + returndates.data.Site + '</option>')
            $("#site").prop('disabled', 'disabled');
            $("#days").val(returndates.data.RefillDays);
            $("#days").prop('disabled', 'disabled');
            $("#dateOrder").val(returndates.data.Date);
            $("#comment").val(returndates.data.Comment)
        }).catch(function (error) {
            toastr.error('Alerta - Error inesperado contactar a sistemas.');
        })
}

function OrderList() {
    axios.get("/OrderLists/ActionOrderList?Folio=" + folioSearch)
        .then(function (returndates) {
            i = 0
            x = 0;
            listItem = returndates.data
            table = "";
            $.each(returndates.data, function (index, value) {
                var partn = value.ParthNumber.toString();
                table += "<tr><td class='FolioHidden'>" + value.Folio + "</td><td>" + value.ParthNumber + "</td><td>" + value.Description + "</td><td>" + value.Stock + "</td><td>" + value.PrePruchase + "</td><td>" + value.AverageSale + "</td>" + "</td><td>" + value.SuggestedSystems + "</td>"

                table += "<td class='textHidde'><input class='form-control' id='" + ("L" + folioSearch + i) + "' type='text' onkeyup='Autorize(event," + x + "," + folioSearch + i + "," + folioSearch + ")' name='name' value='" + (value.Authorize != null ? value.Authorize : 0) + "'/></td>"
                table += "<td>" + (value.Packing == null ? "" : value.Packing) + "</td></tr>";
                listAutorize[x] = { id: folioSearch, ParthNumber: partn, authorized: (value.Authorize != null ? value.Authorize : 0) };
                i++;
                x++;
            });
            $("#tableOrdered").html(table)
            $(".FolioHidden").hide();
            EndLoading();
        }).catch(function (error) {
            toastr.error('Alerta - Error inesperado contactar a sistemas.');
        })
}

function Autorize(e, id, position, value) {
    if ($("#L" + position).val() >= 0 && $("#L" + position).val() <= 9000000 && $("#L" + position).val() != null && $("#L" + position).val().trim != "") {
        if (isNaN(parseInt($("#L" + position).val())))
            $("#L" + position).val(0);
        else
            $("#L" + position).val(parseInt($("#L" + position).val()));
        listAutorize[id] = { id: value, ParthNumber: listAutorize[id].ParthNumber, authorized: $("#L" + position).val() };
    }
    else {
        $("#L" + position).val("0")
        listAutorize[id] = { id: value, ParthNumber: listAutorize[id].ParthNumber, authorized: $("#L" + position).val() };
    }
}

function SaveOrderAndGenerateStoreProcedure(option, OrderListVi) {
    StartLoading()
    $.ajax({
        type: "POST",
        url: "/Orders/ActionNewPurchaseOrder",
        data: {
            OrderList: OrderListVi.filter(x => x.Authorized > 0),
            Folio: folioSearch,
            Date: $("#dateOrder").val(),
            Comment: $("#comment").val(),
            Currency: $("#currency").val(),
            Supplier: $("#supplier").val(),
            Site: $("#site").val(),
            Option: option
        },
        success: function (returndates) {
            EndLoading();
            if (returndates == "SF") {
                SessionFalse(sesion)
            }
            else if (returndates.Error == 1000) {
                toastr.success("Se guardaron los datos de manera correcta")
            }
            else if (returndates.Error == 1001) {
                toastr.warning("Problema al guardar todos los datos")
            }
            else if (returndates.Error == 1002) {
                toastr.warning("Problema al guardar todos los productos")
            }
            else if (returndates.Error == 1008) {
                toastr.warning("Problema interno del programa favor de contactar a sistemas")
            }
            else if (returndates.Error == 2000) {
                swal({
                    title: 'Numero de Orden: ' + returndates.Document,
                    text: "Su orden de compra se genero correctamente!!",
                    type: "success"
                });
                Clear();
            }
            else if (returndates.Error == 2001) {
                swal({
                    title: 'Numero de Orden: ' + returndates.Document,
                    text: "Su orden de compra se genero correctamente!!",
                    type: "success"
                });
                Clear();
            }
            else if (returndates.Error == 2002) {
                if (returndates.Store == 8001) {
                    toastr.warning("Estatus Incorrecto")
                    setTimeout(function () {
                        location.reload();
                    }, 1500)
                }
                else if (returndates.Store == 8002) {
                    toastr.warning("Error al generar orden de compra")
                    setTimeout(function () {
                        location.reload();
                    }, 1500)
                }
                else if (returndates.Store == 8003) {
                    toastr.warning("Algun producto no contiene precio")
                    setTimeout(function () {
                        location.reload();
                    }, 1500)
                }
            }
            else if (returndates.Error == 2003) {
                toastr.warning("Problema al guardar todos los productos")
            }
            else {
                toastr.error("Error desconocido")
            }
        },
        error: function (returndates) {
            EndLoading();
            toastr.error('Alerta - Error inesperado contactar a sistemas.');
        }
    });
}

function Save() {
    if ($("#dateOrder").val().trim() != "" && IsDate($("#dateOrder").val())) {
        if (moment($("#dateOrder").val().trim()).format("DD/MM/YYYY") != "Invalid date") {
            if (listAutorize.length == listItem.length) {
                OrderListVi = []
                OrderListVi = listAutorize
                    .filter(x => x.id > 0 && x.authorized > 0)
                    .map(x => ({
                        Id: x.id,
                        ParthNumber: x.ParthNumber,
                        Supplier: 0,
                        Authorized: parseInt(x.authorized)
                    }))
                if (OrderListVi.length > 0) {
                    SaveOrderAndGenerateStoreProcedure(2, OrderListVi)
                    Clear();
                }
                else {
                    toastr.error("Rellena todos los datos y verifica que sean mayor o igual a 0")
                }
            }
            else
                toastr.error("Rellena todos los datos y verifica que sean mayor o iguala 0")
        }
        else
            toastr.error("Favor de poner una Fecha valida")
    }
    else {
        setTimeout(function () {
            position = $("#dateOrder").offset()
            $('html, body').animate({ scrollTop: position.top - 380 }, "slow");
            $("#OrderError").show();
            $("#dateOrder").addClass("error");
            if ($("#dateOrder").val().trim() != null && $("#dateOrder").val().trim() != "") {
                $("#OrderError").html("Poner una fecha valida")
            }
            else
                $("#OrderError").html("El Campo no puede ser nulo")
            $("#OrderError").fadeOut(5000, function () {
                $("#dateOrder").removeClass("error")
            })
        }, 100);
        $(".datepicker").hide();
        toastr.error('necesita tener una fecha');
    }
}

function TemporaryChanges() {
    OrderListVi = []
    if (listAutorize.length == listItem.length) {

        OrderListVi = listAutorize
            .filter(x => x.id > 0 && x.authorized > 0)
            .map(x => ({
                Id: x.id,
                ParthNumber: x.ParthNumber,
                Supplier: 0,
                Authorized: parseInt(x.authorized)
            }))
    }
    SaveOrderAndGenerateStoreProcedure(1, OrderListVi)
}

function IsDate(date) {
    var reDate = new RegExp("^((0?[1-9]|1[012])[- /.](0?[1-9]|[12][0-9]|3[01])[- /.](19|20)?[0-9]{2})*$")
    return reDate.test(date);
}

function Clear() {
    $("#search").show()
    $("#orderSearch").hide()
    $("select#folioOrderSearch option[value='" + $("#folioOrderSearch").val() + "']").remove();
    $("#select2-chosen-2").html("")
    $('#folioOrderSearch').val($("#folioOrderSearch :first").val()).trigger('change');
}

function Search(id, table) {
    var value = document.querySelector(id).value.toLowerCase()
    $(table + " tr").filter(function () {
        $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
    });
}

$("#SearchInput").keyup(() => { Search("#SearchInput", "#tableOrdered") })

function GetCurrency() {
    $.ajax({
        type: "POST",
        url: "/Orders/ActionGetCurrencyPO",
        data: { folio: folioSearch },
        success: function (returndates) {
            console.log(returndates)
            if (returndates == "SF") {
                SessionFalse(sesion)
            }
            else {
                $('#currency').val(returndates).trigger('change');
                //$("#currency").val(returndates);
                $("#currency").prop("disabled", true)
            }
        }
    });
}