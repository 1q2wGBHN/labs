﻿var listAddServices = [];
var ServicesToPost = [];
var DocumentToPost = [];
var id_supplier = 0;

//Obtencion de datos del viewModel
$(document).ready(function () {
    window.setTimeout(function () { $("#fa").click(); }, 1000);
    var servicios = JSON.parse($("#model").val());
    tableOrder.clear();
    tableOrder.rows.add($(servicios));
    tableOrder.columns.adjust().draw();
    $(".footer").hide();
});

$("#ValidDate1").datepicker({
    autoclose: true,
    todayHighlight: true,
    showButtonPanel: true,
}).on("changeDate", function (e) {
    searchOrder();
});

$("#ValidDate2").datepicker({
    autoclose: true,
    todayHighlight: true,
}).on("changeDate", function (e) {
    searchOrder();
});

function searchOrder() {
    let status = $("#SelectOptions").val()
    var url;
    var data;
    if ($('#ValidDate1').val() != "" & $('#ValidDate2').val() != "" || ($('#ValidDate1').val() == "" & $('#ValidDate2').val() == "")) {
        if (moment($('#ValidDate1').val()) <= moment($('#ValidDate2').val()) || ($('#ValidDate1').val() == "" & $('#ValidDate2').val() == "")) {
            if (status == "Todos") {
                url = "/PurchaseOrderGlobal/ActionGetOrderByDate";
                if ($('#ValidDate1').val() == "" & $('#ValidDate2').val() == "") {
                    data = { "startDate": moment().add(-7, 'days').format("MM/DD/YYYY"), "finishDate": moment().add(1, 'days').format("MM/DD/YYYY") }
                } else {
                    data = { "startDate": $('#ValidDate1').val(), "finishDate": $('#ValidDate2').val() }
                }
            }
            else {
                url = "/PurchaseOrderGlobal/ActionGetOrderByDateAndStatus";
                data = { "startDate": $('#ValidDate1').val(), "finishDate": $('#ValidDate2').val(), "status": $("#SelectOptions").val() }
                if ($('#ValidDate1').val() == "" & $('#ValidDate2').val() == "") {
                    data = { "startDate": moment().add(-7, 'days').format("MM/DD/YYYY"), "finishDate": moment().add(1, 'days').format("MM/DD/YYYY"), "status": $("#SelectOptions").val() }
                }
            }
            StartLoading();
            $.ajax({
                type: "POST",
                url: url,
                data: data,
                success: function (data) {
                    EndLoading();
                    if (data == "SF")
                        SessionFalse("Terminó tu sesión");
                    else if (data == "error")
                        toastr.error("Error inesperado contactar a sistemas.");
                    else if (data.length == 0) {
                        $('#tableOrder').animatePanel();
                        tableOrder.clear();
                        tableOrder.rows.add(data);
                        tableOrder.columns.adjust().draw();
                        toastr.warning("No existen ordenes en el rango de fecha");
                    }
                    else {
                        $('#tableOrder').animatePanel();
                        tableOrder.clear();
                        tableOrder.rows.add(data);
                        tableOrder.columns.adjust().draw();
                    }
                },
                error: function (returndate) {
                    EndLoading();
                    toastr.error('Alerta - Error inesperado contactar a sistemas.');
                }
            });
        }
        else {
            toastr.error("Fecha final no puede ser menor a la inicial.");
            document.getElementById('ValidDate1').value = '';
            document.getElementById('ValidDate2').value = '';
            $('#ValidDate1').focus();
        }
    }
}

function dispFile(contents) {
    document.getElementById('contents').innerHTML = contents
}

function clickElem(elem) {
    var eventMouse = document.createEvent("MouseEvents")
    eventMouse.initMouseEvent("click", true, false, window, 0, 0, 0, 0, 0, false, false, false, false, 0, null)
    elem.dispatchEvent(eventMouse)
}

function openFile(func) {
    readFile = function (e) {
        var file = e.target.files;
        convertToBase64XML(file);
    }
    fileInput = document.createElement("input")
    fileInput.type = 'file'
    fileInput.style.display = 'none'
    fileInput.accept = '.xml'
    fileInput.onchange = readFile
    fileInput.multiple = true
    fileInput.func = func
    document.body.appendChild(fileInput)
    clickElem(fileInput)
}

//Tabla principal
var tableOrder = $('#tableOrder').DataTable({
    oLanguage: {
        "sProcessing": "Procesando...", "sLengthMenu": "Mostrar _MENU_ registros", "sZeroRecords": "No se encontraron resultados", "sEmptyTable": "Ningún dato disponible en esta tabla", "sInfo": "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros", "sInfoEmpty": "Mostrando registros del 0 al 0 de un total de 0 registros", "sInfoFiltered": "(filtrado de un total de _MAX_ registros)", "sInfoPostFix": "", "sSearch": "Buscar:", "sUrl": "", "sInfoThousands": ",", "sLoadingRecords": "Cargando...", "oPaginate": { "sFirst": "Primero", "sLast": "Último", "sNext": "Siguiente", "sPrevious": "Anterior" }, "oAria": { "sSortAscending": ": Activar para ordenar la columna de manera ascendente", "sSortDescending": ": Activar para ordenar la columna de manera descendente" }
    },
    dom: "<'row'<'col-sm-4'l><'col-sm-4 text-left'B><'col-sm-4'f>t<'col-sm-6'i><'col-sm-6'p>>",
    lengthMenu: [[10, 25, 50, -1], [10, 25, 50, "Todos"]],
    buttons: [],
    order: [[1, "desc"]],
    columns: [
        {
            "class": "details-control",
            "orderable": false,
            "data": null,
            "defaultContent": "",
        },
        { data: 'purchase_no' },
        { data: 'supplier' },
        { data: 'purchase_date' },
        { data: 'purchase_status' }
    ],
    columnDefs: [{
        targets: [0, 1, 2, 3, 4],
        width: "1%"
    },
    {
        targets: [4],
        render: function (data, type, row) {
            if (data == 0)
                return "Enviado a proveedores";
            else if (data == 9)
                return "Recibido";
        }
    }]
});

var detailRows = [];
$('#tableOrder tbody').on('click', 'tr td.details-control', function () {
    var tr = $(this).closest('tr');
    var row = tableOrder.row(tr);

    if (row.child.isShown()) {
        row.child.hide();
        tr.removeClass('details');
    }
    else {
        if (tableOrder.row('.details').length) {
            $('.details-control', tableOrder.row('.details').node()).click();
        }
        row.child(format(row.data())).show();
        tr.addClass('details');
    }
});

tableOrder.on('draw', function () {
    $.each(detailRows, function (i, id) {
        $('#' + id + ' td.details-control').trigger('click');
    });
});

//Tabla detalles
function format(d) {
    var detailItems = "";
    var Apro = '';
    var tabledetail = $('<div>').addClass('loading').text('Cargando Datos...');
    $.ajax({
        url: "/PurchaseOrderGlobal/ActionGetDetailOrder",
        data: { "purchase_no": d.purchase_no },
        type: "POST",
        success: function (data) {
            id_supplier = data[0].supplier_id;
            if (data.data == "SF")
                SessionFalse("Terminó tu sesión");
            $.each(data, function (index, value) {
                detailItems += "<tr><td>" + value.part_number + "</td><td>" + value.part_description + "</td><td>" + value.quantity + "</td><td>$" + value.item_total_amount.toFixed(2) + "</td>";
                if (d.purchase_status == 0) {
                    Apro = `<div class="row pull-right"><div class="col-lg-12"> <button id="btn-cancel" type="button" class="btn btn-sm btn-primary" onclick="modal(` + d.purchase_no + `,'` + d.supplier + `')"><i class="fa fa-arrow-circle-o-up"></i> Agregar XML</button>&nbsp;&nbsp;&nbsp;</div></div>`;
                }
            });
            var TheOne = new Date().getTime();
            if (data.length != 0) {
                tabledetail.html('<table id="tabledetail' + TheOne + '" class="table table-striped table-bordered table-hover" style="width:100%"><thead><tr><th>Código</th><th>Descripción</th><th>Cantidad</th><th>Precio Aproximado</th></thead><tbody>' + detailItems + '</tbody></table>' + Apro).removeClass('loading');
                reloadStyleTable(TheOne);
            }
        }
    });
    return tabledetail
}

//Recargar tabla
function reloadStyleTable(TheOne) {
    $('#tabledetail' + TheOne).DataTable({
        oLanguage: {
            "sProcessing": "Procesando...", "sLengthMenu": "Mostrar _MENU_ registros", "sZeroRecords": "No se encontraron resultados", "sEmptyTable": "Ningún dato disponible en esta tabla", "sInfo": "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros", "sInfoEmpty": "Mostrando registros del 0 al 0 de un total de 0 registros", "sInfoFiltered": "(filtrado de un total de _MAX_ registros)", "sInfoPostFix": "", "sSearch": "Buscar:", "sUrl": "", "sInfoThousands": ",", "sLoadingRecords": "Cargando...", "oPaginate": { "sFirst": "Primero", "sLast": "Último", "sNext": "Siguiente", "sPrevious": "Anterior" }, "oAria": { "sSortAscending": ": Activar para ordenar la columna de manera ascendente", "sSortDescending": ": Activar para ordenar la columna de manera descendente" }
        }
    });
}

var id_purchase = 0;
//Funcion para mostrar modal del servicio
function modal(id, supplier) {
    id_purchase = id;
    listXML = [];
    b = 0;
    listAddServices = [];
    tableXML.clear().draw();
    tableXML.rows.add($(listAddServices));
    tableXML.columns.adjust().draw();
    $("#tableXML").hide();
    $("#purchase_no").text("Número de Compra : " + id);
    $("#supplier").text("Proveedor : " + supplier);
    $(".datepicker").datepicker({
        autoclose: true,
        todayHighlight: true,
        showButtonPanel: true,
    }).on("changeDate", function (e) { });
    $('#modalService').modal('show');
}

listXML = [];
//Funcion para convertir XML => base 64
var b = 0;
function convertToBase64XML(file) {
    var selectedFile = file;
    if (selectedFile.length > 0) {
        for (var i = 0; i < selectedFile.length; i++) {
            var idxDot = file[0].name.lastIndexOf(".") + 1;
            var extFile = file[0].name.substr(idxDot, file[0].name.length).toLowerCase();//Obtenemos extension de archivo
            if (extFile == "xml") {
                var xmlName = selectedFile[i].name
                var services = listXML.filter(e => e.xml_name == xmlName).length
                if (services == 0) {
                    var fileToLoad = selectedFile[i];
                    var fileReader = new FileReader();
                    listXML = [...listXML, { xml: "", xml_name: fileToLoad.name }]
                    fileReader.onload = function (fileLoadedEvent) {
                        var base64 = window.btoa(unescape(encodeURIComponent(fileLoadedEvent.target.result)));
                        var xml = base64;
                        listXML[b].xml = xml;
                        b++;
                    };
                    fileReader.readAsText(fileToLoad);
                    tableXML.clear().draw();
                    tableXML.rows.add($(listXML));
                    tableXML.columns.adjust().draw();
                }
                else
                    toastr.warning("Error al leer el XML.");
            }
            else {
                toastr.warning('Debe ser un xml');
                listXML = [];
                b = 0;
                $("tableXML").hide();
            }
        }
        if (listXML.length > 0)
            $("#tableXML").show();
    }
}

//Tabla de XML en modal
var tableXML = $('#tableXML').DataTable({
    "paging": false,
    "ordering": false,
    "info": false,
    "searching": false,
    oLanguage:
    {
        "sZeroRecords": "",
        "sEmptyTable": "",
    },
    columns:
        [
            { data: 'xml_name' },
            { data: null }
        ],
    columnDefs:
        [{
            targets: [1],
            render: function (data, type, full, meta) {
                return `<button class="btn btn-xs btn-danger btn-circle" style="text-align:center;border-right-width: 0px;padding-right: 0px;padding-left: 0px;padding-top: 0px;padding-bottom: 0px;margin-top: 5px;" onclick="deleteXML('${data.xml_name}')"><i class="fa fa-times"></i></button>`;
            }
        }]
});

function deleteXML(xml) {
    b--;
    listXML = listXML.filter(x => x.xml_name != xml);
    tableXML.clear();
    tableXML.rows.add(listXML);
    tableXML.columns.adjust().draw();
    if (listXML.length == 0) {
        $("#tableXML").hide();
    }
}

//Funcion para guardar los XML
function save() {
    if (listXML.length != 0) {
        StartLoading();
        $.ajax({
            type: "POST",
            url: "/PurchaseOrderGlobal/ActionAddXML",
            data: { "list": listXML, "id": id_purchase },
            success: function (response) {
                EndLoading();
                if (response.Status == "SF")
                    SessionFalse("Terminó tu sesión");
                else if (response.Status) {
                    clearPage();
                    swal({
                        title: 'Guardado Correctamente',
                        text: "Archivos guardados correctamente",
                        type: "success"
                    });
                    $('#table_products').DataTable().clear().draw();
                }
                else
                    toastr.error(response.Data);
            },
            error: function () {
                EndLoading();
                toastr.error("DESCONOCIDO, CONTACTA A SISTEMAS.")
            }
        });

    } else
        toastr.warning("Adjunte por lo menos un XML");
}

//Borrar Form
function clearPage() {
    listXML = [];
    supplier_id = 0;
    b = 0;
    $("#closeModal").click();
    searchOrder();
}