﻿var xml_id_global = 0;
var expense_invoice_global = "";
var expense_month_global = false;
var detailExpense = "1";
var subtotal = 0, iva = 0, ieps = 0, total, discount, iva_r, isr_r = 0
var subtotalusd = 0, ivausd = 0, iepsusd = 0, totalusd, discountusd, iva_rusd, isr_rusd = 0
const formatmoney = money => decimal => new Intl.NumberFormat("en-US", { style: "currency", currency: "USD", currencyDisplay: "symbol", maximumFractionDigits: decimal }).format(money);
function MessageNo(text) { toastr.warning(text); }

$(document).ready(function () {
    $('#cbox_ReportType').iCheck({
        checkboxClass: 'icheckbox_square-green',
    }).on('ifClicked', function () {
        $(this).trigger("click");
    });
    window.setTimeout(function () { $("#fa").click(); }, 1000);
});


function checkDetail() {
    if ($("#cbox_ReportType").is(':checked')) {
        $("#TableExpensesDetail").show();
        $("#TableDivExpensesDetail").show();
        $("#TableExpenses").hide();
        $("#TableDivExpenses").hide();
        detailExpense = "0";
    }
    SearchExpense();
}

function FillTableTotals(Expenses, productsType) {
    subtotal = 0, iva = 0, ieps = 0, total = 0, discount = 0, iva_r = 0, isr_r = 0
    subtotalusd = 0, ivausd = 0, iepsusd = 0, totalusd = 0, discountusd = 0, iva_rusd = 0, isr_rusd = 0
    if (productsType == "1") {
        Expenses.forEach(function (element) {
            if (element.Currency == "MXN") {
                subtotal += element.SubTotal;
                iva += element.TotalIVA;
                ieps += element.TotalIEPS;
                discount += element.Discount;
                isr_r += element.IsrRet;
                iva_r += element.IvaRet;
                total += element.TotalAmount;
            }
            else {
                subtotalusd += element.SubTotal;
                ivausd += element.TotalIVA;
                iepsusd += element.TotalIEPS;
                discountusd += element.Discount;
                iva_rusd += element.IvaRet;
                isr_rusd += element.IsrRet;
                totalusd += element.TotalAmount;
            }
        });
        document.getElementById("mxsubtotal").innerHTML = formatmoney(subtotal)(4);
        document.getElementById("usdsubtotal").innerHTML = formatmoney(subtotalusd)(4);
        document.getElementById("mxiva").innerHTML = formatmoney(iva)(4);
        document.getElementById("usdiva").innerHTML = formatmoney(ivausd)(4);
        document.getElementById("usddiscount").innerHTML = formatmoney(discountusd)(4);
        document.getElementById("mxdiscount").innerHTML = formatmoney(discount)(4);
        document.getElementById("mxieps").innerHTML = formatmoney(ieps)(4);
        document.getElementById("usdieps").innerHTML = formatmoney(iepsusd)(4);
        document.getElementById("mxiva_r").innerHTML = formatmoney(iva_r)(4);
        document.getElementById("usdiva_r").innerHTML = formatmoney(iva_rusd)(4);
        document.getElementById("mxisr_r").innerHTML = formatmoney(isr_r)(4);
        document.getElementById("usdisr_r").innerHTML = formatmoney(isr_rusd)(4);
        document.getElementById("mxtotal").innerHTML = formatmoney(total)(4);
        document.getElementById("usdtotal").innerHTML = formatmoney(totalusd)(4);
    }
}

function ClearInputs() {
    document.getElementById("mxsubtotal").innerHTML = formatmoney(0)(4);
    document.getElementById("usdsubtotal").innerHTML = formatmoney(0)(4);
    document.getElementById("mxiva").innerHTML = formatmoney(0)(4);
    document.getElementById("usdiva").innerHTML = formatmoney(0)(4);
    document.getElementById("usddiscount").innerHTML = formatmoney(0)(4);
    document.getElementById("mxdiscount").innerHTML = formatmoney(0)(4);
    document.getElementById("mxieps").innerHTML = formatmoney(0)(4);
    document.getElementById("usdieps").innerHTML = formatmoney(0)(4);
    document.getElementById("mxiva_r").innerHTML = formatmoney(0)(4);
    document.getElementById("usdiva_r").innerHTML = formatmoney(0)(4);
    document.getElementById("mxisr_r").innerHTML = formatmoney(0)(4);
    document.getElementById("usdisr_r").innerHTML = formatmoney(0)(4);
    document.getElementById("mxtotal").innerHTML = formatmoney(0)(4);
    document.getElementById("usdtotal").innerHTML = formatmoney(0)(4);

}

$("#supplier").select2(
    {
        minimumInputLength: 2,
        formatInputTooShort: function () {
            return "Busque un proveedor";
        },
        matcher: function (term, text, option) {
            return text.toUpperCase().indexOf(term.toUpperCase()) >= 0 || option.val().toUpperCase().indexOf(term.toUpperCase()) >= 0;
        }
    }
);
$("#typeTypeExpense").select2();
$("#typeCategory").select2();
$("#typeCurrency").select2();
$("#typeStatus").select2();


var table = $('#TableExpenses').DataTable({
    autoWidth: true,
    responsive: true,
    oLanguage: {
        "sProcessing": "Procesando...", "sLengthMenu": "Mostrar _MENU_ registros", "sZeroRecords": "No se encontraron resultados", "sEmptyTable": "Ningún dato disponible en esta tabla", "sInfo": "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros", "sInfoEmpty": "Mostrando registros del 0 al 0 de un total de 0 registros", "sInfoFiltered": "(filtrado de un total de _MAX_ registros)", "sInfoPostFix": "", "sSearch": "Buscar:", "sUrl": "", "sInfoThousands": ",", "sLoadingRecords": "Cargando...", "oPaginate": { "sFirst": "Primero", "sLast": "Último", "sNext": "Siguiente", "sPrevious": "Anterior" }, "oAria": { "sSortAscending": ": Activar para ordenar la columna de manera ascendente", "sSortDescending": ": Activar para ordenar la columna de manera descendente" }
    },
    dom: "<'row'<'col-sm-4'l><'col-sm-4 text-left'B><'col-sm-4'f>t<'col-sm-6'i><'col-sm-6'p>>",
    lengthMenu: [[10, 25, 50, -1], [10, 25, 50, "Todos"]],
    buttons: [
        {
            extend: 'csv', text: 'Excel', title: 'Gastos de Tienda', className: 'btn-sm', exportOptions: {
                columns: [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14]
            }
        },
        {
            extend: 'pdf', text: 'PDF', title: 'Gastos de Tienda', className: 'btn-sm', exportOptions: {
                columns: [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14]
            },
            customize: function (doc) {
                doc.content[1].table.widths =
                    Array(doc.content[1].table.body[0].length + 1).join('*').split('');
            }
        },
        { extend: 'print', text: 'Imprimir', className: 'btn-sm' },
    ],
    columns: [
        {
            "class": "details-control",
            "orderable": false,
            "data": null,
            "defaultContent": "",
        },
        { targets: 1, data: 'ReferenceFolio' },
        { targets: 2, data: 'Category' },
        { targets: 3, data: 'ReferenceType' },
        { targets: 4, data: 'Supplier' },
        { targets: 5, data: 'Invoice' },
        { targets: 6, data: 'InvoiceDate' },
        { targets: 7, data: 'Currency' },
        { targets: 8, data: 'SubTotal' },
        { targets: 9, data: 'TotalIVA' },
        { targets: 10, data: 'TotalIEPS' },
        { targets: 11, data: 'Discount' },
        { targets: 12, data: 'IsrRet' },
        { targets: 13, data: 'IvaRet' },
        { targets: 14, data: 'TotalAmount' },
        { targets: 15, data: null },
        { targets: 16, data: 'StatusText', visible: false },
        { targets: 17, data: 'TypeXML', visible: false },
        { targets: 18, data: 'ExpenseToday', visible: false },
        { targets: 19, data: 'ExpenseMonth', visible: false },
        { targets: 20, data: 'Expense_Status', visible: false },
        { targets: 21, data: 'RemarkXML', visible: false },
        { targets: 22, data: null }
    ],
    columnDefs: [{
        targets: [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22],
        width: "1%"
    },
    {
        targets: [6],
        render: function (data, type, row) {
            var date = moment(data).format('DD/MM/YYYY');
            if (date != 'Invalid date')
                return date;
            else
                return "";
        }
    },
    {
        targets: [8, 9, 10, 11, 12, 13, 14],
        render: function (data, type, row) {
            return `<p> ${formatmoney(data)(4)}</p>`
        }
    },
    {
        targets: [22],
        render: function (data, type, full, meta) {
            if (full.Expense_Status == 1)
                return `<button class="btn btn-xs btn-outline btn-danger" onclick="CancelInvoiceShowModal('${full.ReferenceFolio}', '${full.Invoice}', '${full.Supplier}', '${full.RemarkXML}', ${full.ExpenseMonth} )"><i class="fa fa-close"></i> Eliminar</button>`;
            else if (full.Expense_Status == 9)
                return `<button class='btn btn-xs btn-danger' onclick='MessageNo("Este gasto se encuentra Terminado.")'><i class='fa fa-close'></i> Eliminar</button>`;
            else if (full.Expense_Status == 8)
                return `<button class='btn btn-xs btn-danger' onclick='MessageNo("Este gasto se encuentra eliminado.")'><i class='fa fa-close'></i> Eliminar</button>`;
            else if (full.Expense_Status == 2)
                return `<button class='btn btn-xs btn-warning' onclick='MessageNo("Este gasto se encuentra en proceso de Cancelar Distrital.")'><i class='fa fa-close'></i> Eliminar</button>`;
            else if (full.Expense_Status == 3)
                return `<button class='btn btn-xs btn-warning' onclick='MessageNo("Este gasto se encuentra pendiente por cancelar.")'><i class='fa fa-close'></i> Eliminar</button>`;
        }
    },
        {
            targets: [15],
            render: function (data, type, full, meta) {
                return "<button class='btn btn-xs btn-outline btn-info' onclick='ShowReport(" + full.ReferenceFolio + ")'> <i class='fa fa-file-pdf-o'></i> Reporte </button>";
            }
        }
    ]
});
$("#TableExpenses").append('<tfoot><tr><th></th><th colspan = "5" style="color:#008000;">Totales</th><th  colspan = "2" style="color:#008000;">MXN</th><th id="mxsubtotal" style="color:#008000;">$0</th><th id="mxiva" style="color:#008000;">$0</th><th id ="mxieps" style="color:#008000;">$0</th><th id ="mxdiscount" style="color:#008000;">$0</th><th id ="mxisr_r" style="color:#008000;">$0</th><th id ="mxiva_r" style="color:#008000;">$0</th><th id ="mxtotal" colspan = "3" style="color:#008000;">$0</th></tr><tr><th></th><th colspan = "5"  style="color:#0D0DFF;">Totales</th><th  colspan = "2"  style="color:#0D0DFF;">USD</th><th id="usdsubtotal" style="color:#0D0DFF;">$0</th><th id="usdiva"  style="color:#0D0DFF;">$0</th><th id ="usdieps"  style="color:#0D0DFF;">$0</th><th id ="usddiscount" style="color:#0D0DFF;">$0</th><th id ="usdisr_r" style="color:#0D0DFF;">$0</th><th id ="usdiva_r" style="color:#0D0DFF;">$0</th><th id ="usdtotal" colspan = "3" style="color:#0D0DFF;">$0</th></tr></tfoot>');

var detailRows = [];

$('#TableExpenses tbody').on('click', 'tr td.details-control', function () {
    var tr = $(this).closest('tr');
    var row = table.row(tr);
    if (row.child.isShown()) {
        row.child.hide();
        tr.removeClass('details');
    }
    else {
        if (table.row('.details').length) {
            $('.details-control', table.row('.details').node()).click();
        }
        row.child(formatOne(row.data())).show();
        tr.addClass('details');
    }
});

table.on('draw', function () {
    $.each(detailRows, function (i, id) {
        $('#' + id + ' td.details-control').trigger('click');
    });
});

function formatOne(d) {
    var detailItems = "";
    var tabledetail = $('<div/>').addClass('loading').text('Cargando Datos...');
    $.ajax({
        url: '/Expenses/ActionExpenseGetItemById/',
        data: { "ReferenceFolio": d.ReferenceFolio },
        type: "GET",
        success: function (Data) {
            $.each(Data, function (index, value) {
                detailItems += "<tr><td>" + value.item_no + "</td><td>" + value.item_description + "</td><td>" + value.quantity + "</td><td>" + formatmoney(value.unit_cost)(4) + "</td><td>" + formatmoney(value.iva)(4) + "</td><td>" + formatmoney(value.ieps)(4) + "</td><td>" + formatmoney(value.iva_retained)(4) + "</td><td>" + formatmoney(value.ieps_retained)(4) + "</td><td>" + formatmoney(value.discount)(4) + "</td><td>" + formatmoney(value.item_amount)(4) + "</td>"

            });
            tabledetail.html('<table id="tabledetail" class="table table-striped table-bordered table-hover" style="width:100%"><thead><tr><th>Código</th><th>Descripción</th><th>Cantidad</th><th>Precio Unitario</th><th>IVA</th><th>IEPS</th><th>IVA Retenido</th><th>IEPS Retenido</th><th>Descuento</th><th>Subtotal</th></tr></thead><tbody>' + detailItems + '</tbody></table>').removeClass('loading');
            reloadStyleTable();
        }
    });
    return tabledetail;
}

function reloadStyleTable() {
    $('#tabledetail').DataTable({
        oLanguage: {
            "sProcessing": "Procesando...", "sLengthMenu": "Mostrar _MENU_ registros", "sZeroRecords": "No se encontraron resultados", "sEmptyTable": "Ningún dato disponible en esta tabla", "sInfo": "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros", "sInfoEmpty": "Mostrando registros del 0 al 0 de un total de 0 registros", "sInfoFiltered": "(filtrado de un total de _MAX_ registros)", "sInfoPostFix": "", "sSearch": "Buscar:", "sUrl": "", "sInfoThousands": ",", "sLoadingRecords": "Cargando...", "oPaginate": { "sFirst": "Primero", "sLast": "Último", "sNext": "Siguiente", "sPrevious": "Anterior" }, "oAria": { "sSortAscending": ": Activar para ordenar la columna de manera ascendente", "sSortDescending": ": Activar para ordenar la columna de manera descendente" }
        },
    });
}

var model = JSON.parse($("#model").val());
if (!$.isEmptyObject(model.ExpensesReportModelList)) {
    $("#TableDivExpensesDetail").hide();
    $("#TableExpensesDetail").hide();
    $('#TableDivExpenses').animatePanel();
    FillTableTotals(model.ExpensesReportModelList, "1");
    table.clear();
    table.rows.add($(model.ExpensesReportModelList));
    table.columns.adjust().draw();
    $("#Print").removeAttr('disabled', 'disabled');
}
else {
    $('#TableDivExpenses').animatePanel();
    table.clear();
    table.columns.adjust().draw();
    $("#Print").attr('disabled', true);
}

function SearchExpense() {
    if (ValidateDates()) {
        var supplierid = $("#supplier").val();
        var typeExpense = $("#typeTypeExpense").val();
        var category = $("#typeCategory").val();
        var begindate = $('#BeginDate').val();
        var enddate = $('#EndDate').val();
        var currency = $('#typeCurrency').val();
        var status = $('#typeStatus').val();
        StartLoading();
        ClearInputs();
        var url = "";
        var tableDiv = "";
        if (detailExpense == "1") {
            url = "/Expenses/ActionExpensesReport/";
            tableDiv = "TableDivExpenses";
        }
        else {
            url = "/Expenses/ActionExpensesReportDetail/";
            tableDiv = "TableDivExpensesDetail";
        }
        $.ajax({
            url: url,
            type: "GET",
            data: { "SupplierId": supplierid, "Category": category, "BeginDate": begindate, "EndDate": enddate, "Currency": currency, "typeExpense": typeExpense, "status": status },
            success: function (result) {
                EndLoading();
                if (result.status) {
                    if (!$.isEmptyObject(result.model)) {
                        if (detailExpense == "1") {
                            $('#' + tableDiv).animatePanel();
                            FillTableTotals(result.model, detailExpense);
                            table.clear();
                            table.rows.add($(result.model));
                            table.columns.adjust().draw();
                        } else {
                            $('#' + tableDiv).animatePanel();
                            FillTableTotals(result.model, detailExpense);
                            tableDetail.clear();
                            tableDetail.rows.add($(result.model));
                            tableDetail.columns.adjust().draw();
                        }
                    }
                    else {
                        $('#' + tableDiv).animatePanel();
                        table.clear();
                        table.columns.adjust().draw();
                    }
                }
                else
                    SessionFalse("Se terminó su sesion.");
            },
            error: function () {
                toastr.remove();
                toastr.error('Error inesperado contactar a sistemas.');
                EndLoading();
            }
        });
    }
}

$('#Search').on('click', function () {
    if (ValidateDates()) {
        var begindate = $('#BeginDate').val();
        var enddate = $('#EndDate').val();
        if (begindate == "") {
            var date = new Date();
            begindate = moment(new Date(date.getFullYear(), date.getMonth() - 3, 1)).format('MM/DD/YYYY');
            enddate = moment(new Date(date.getFullYear(), date.getMonth() + 1, 0)).format('MM/DD/YYYY');
        }
        var supplierid = $("#supplier").val();
        var typeExpense = $("#typeTypeExpense").val();
        var category = $("#typeCategory").val();
        var currency = $('#typeCurrency').val();
        var status = $('#typeStatus').val();
        StartLoading();
        ClearInputs();
        var url = "";
        var tableDiv = "";
        if (detailExpense == "1") {
            url = "/Expenses/ActionExpensesReport/";
            tableDiv = "TableDivExpenses";
        }
        else {
            url = "/Expenses/ActionExpensesReportDetail/";
            tableDiv = "TableDivExpensesDetail";
        }
        $.ajax({
            url: url,
            type: "GET",
            data: { "SupplierId": supplierid, "Category": category, "BeginDate": begindate, "EndDate": enddate, "Currency": currency, "typeExpense": typeExpense, "status": status },
            success: function (result) {
                EndLoading();
                if (result.status) {
                    if (!$.isEmptyObject(result.model)) {
                        if (detailExpense == "1") {
                            $('#' + tableDiv).animatePanel();
                            FillTableTotals(result.model, detailExpense);
                            table.clear();
                            table.rows.add($(result.model));
                            table.columns.adjust().draw();
                        } else {
                            $('#' + tableDiv).animatePanel();
                            FillTableTotals(result.model, detailExpense);
                            tableDetail.clear();
                            tableDetail.rows.add($(result.model));
                            tableDetail.columns.adjust().draw();
                        }
                    }
                    else {
                        $('#' + tableDiv).animatePanel();
                        table.clear();
                        table.columns.adjust().draw();
                    }
                }
                else
                    SessionFalse("Se terminó su sesion.");
            },
            error: function () {
                toastr.remove();
                toastr.error('Error inesperado contactar a sistemas.');
                EndLoading();
            }
        });

    }
});

function ValidateDates() {
    if ($('#BeginDate').val() != "" & $('#EndDate').val() != "") {
        if (!(moment($('#BeginDate').val()) <= moment($('#EndDate').val()))) {
            toastr.remove();
            toastr.warning('La fecha inicial no puede ser mayor a la fecha final');
            return false;
        }
        else {
            toastr.remove();
            return true;
        }
    }
    else {
        toastr.remove();
        return true;
    }
}

$("#BeginDate").datepicker({
    autoclose: true,
    todayHighlight: true
}).on("changeDate", function () {
    ValidateDates();
});

$("#EndDate").datepicker({
    autoclose: true,
    todayHighlight: true
}).on("changeDate", function () {
    ValidateDates();
});


function ShowReport(value) {
    StartLoading();
    $.ajax({
        type: "GET",
        url: "/Expenses/ActionGetExpenseReportById",
        data: { "referende_document": value },
        success: function (returndates) {
            if (returndates == "SF")
                SessionFalse("Termino tu session");
            document.getElementById("iframe").srcdoc = returndates;
            EndLoading();
            $("#ModalDescription").html('Reporte de gastos');
            $('#ModalReport').modal('show');
        },
        error: function (returndates) {
            toastr.error('Alerta - Error inesperado  contactar a sistemas.');
            EndLoading();
        }
    });
}

function CancelInvoiceShowModal(expense_id, invoice, supplier, remark, month) {
    var remarkPart = remark.split("S:");
    $('#commentStore').html(remarkPart[1]);
    xml_id_global = expense_id;
    expense_invoice_global = invoice;
    expense_month_global = month;
    $("#inputComment").val("");
    $("#ModalTitleInvoice").html('Factura: ' + invoice + '');
    $("#ModalTextInvoice").html('Proveedor: ' + supplier + '');
    $("#ModalInvoice").modal("show");
}


function MessageSaveCancelInvoiceAuthorization() {
    var comment = $("#inputComment").val();
    if (comment.trim().length >= 8) {
        var strSubject = "";
        if (expense_month_global)
            strSubject = "Se avisará a la sucursal ";
        else
            strSubject = "Se mandará solicitud a Distrital ";
        swal({
            title: "¿Esta seguro?",
            text: strSubject + "para cancelar la factura del folio: " + xml_id_global + " de la factura " + expense_invoice_global,
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Continuar",
            cancelButtonText: "Cancelar"
        }, function (isConfirm) {
            if (isConfirm) {
                StartLoading();
                $("#ModalInvoice").modal("hide");
                var texto = "";
                var success = "";
                axios.post("/Expenses/ActionCancelInvoiceAprobal/", { "xml_id": xml_id_global, "comment": comment.trim(), "expense_month": expense_month_global })
                    .then(function (data) {
                        EndLoading();
                        if (data.data == "SF")
                            SessionFalse(data.data);
                        else if (data.data == 2 || data.data == 3)
                            success = "Se ha autorizado la cancelación del gasto.";
                        else {
                            switch (data.data) {
                                case 1000:
                                    texto = "No se encontre el gasto - (1000)"
                                    break;
                                case 1001:
                                    texto = "No se encontre el gasto - (1001)"
                                    break;
                                case 1002:
                                    texto = "El gasto no corresponde a esta surcusal. - (1002)"
                                    break;
                                case 8001:
                                    texto = "Error desconocido - (8001)"
                                    break;
                                default:
                                    texto = "Error inesperado contactar a sistemas."
                                    break;
                            }
                        }
                    })
                    .catch(function (error) {
                        console.log(error.data)
                    }).then(function () {
                        EndLoading();
                        $("#Search").click();
                        if (texto != "")
                            toastr.error(texto);
                        if (success != "")
                            toastr.success(success);
                    })
            }
        });
    } else {
        toastr.warning("Ingrese minimo 8 caracteres para cancelar la factura.");
    }
}


function MessageCancelReverse() {
    var comment = $("#inputComment").val();
    if (comment.trim().length >= 8) {
        swal({
            title: "¿Esta seguro?",
            text: "Rechazará la cancelación del folio: " + xml_id_global + " de la factura " + expense_invoice_global,
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Continuar",
            cancelButtonText: "Cancelar"
        }, function (isConfirm) {
            if (isConfirm) {
                StartLoading();
                $("#ModalInvoice").modal("hide");
                var texto = "";
                var success = "";
                axios.post("/Expenses/ActionCancelInvoiceReverse/", { "xml_id": xml_id_global, "comment": comment.trim(), 'old_status': 1, "program_id": "PURCH024" })
                    .then(function (data) {
                        EndLoading();
                        if (data.data == "SF")
                            SessionFalse(data.data);
                        else if (data.data == 9)
                            success = "Se ha rechazado la solicitud del gastos correctamente.";
                        else {
                            switch (data.data) {
                                case 1000:
                                    texto = "No se encontro el gasto - (1000)"
                                    break;
                                case 1001:
                                    texto = "No se encontro el gasto - (1001)"
                                    break;
                                case 1002:
                                    texto = "El gasto no corresponde a esta surcusal. - (1002)"
                                    break;
                                case 8001:
                                    texto = "Error desconocido - (8001)"
                                    break;
                                default:
                                    texto = "Error inesperado contactar a sistemas."
                                    break;
                            }
                        }
                    })
                    .catch(function (error) {
                        console.log(error.data)
                    }).then(function () {
                        EndLoading();
                        $("#Search").click();
                        if (texto != "")
                            toastr.error(texto);
                        if (success != "")
                            toastr.success(success);
                    })
            }
        });
    } else {
        toastr.warning("Ingrese minimo 8 caracteres para cancelar la factura.");
    }
}