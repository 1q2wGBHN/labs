﻿window.setTimeout(function () { $("#fa").click(); }, 500);

var table = $('#tableItems').DataTable({
    oLanguage:
    {
        "sProcessing": "Procesando...",
        "sLengthMenu": "Mostrar _MENU_ registros",
        "sZeroRecords": "No se encontraron resultados",
        "sEmptyTable": "Ningún dato disponible en esta tabla",
        "sInfo": "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
        "sInfoEmpty": "Mostrando registros del 0 al 0 de un total de 0 registros",
        "sInfoFiltered": "(filtrado de un total de _MAX_ registros)",
        "sInfoPostFix": "",
        "sSearch": "Buscar:",
        "sUrl": "",
        "sInfoThousands": ",",
        "sLoadingRecords": "Cargando...",
        "oPaginate": { "sFirst": "Primero", "sLast": "Último", "sNext": "Siguiente", "sPrevious": "Anterior" },
        "oAria": { "sSortAscending": ": Activar para ordenar la columna de manera ascendente", "sSortDescending": ": Activar para ordenar la columna de manera descendente" }
    },
    dom: "<'row'<'col-sm-4'l><'col-sm-4 text-left'B><'col-sm-4'f>t<'col-sm-6'i><'col-sm-6'p>>",
    lengthMenu: [[10, 25, 50, -1], [10, 25, 50, "Todos"]],
    buttons: [],
    order: [[0, "asc"]],
    columns:
        [
            { data: 'Department' },
            { data: 'Family' },
            { data: 'PartNumber' },
            { data: 'Description' },
            { data: 'Quantity' },
            { data: 'Amount' },
            { data: 'Ieps' },
            { data: 'TotalAmount' },
        ],
    columnDefs:
        [{
            targets: [0, 1, 2, 3, 4, 5, 6, 7],
            width: "1%"
        },
        {
            render: function (data, type, row) {
                return '$' + data.toFixed(2);
            },
            targets: [5]
        },
        {
            render: function (data, type, row) {
                return '$' + data.toFixed(2);
            },
            targets: [6]
        },
        {
            render: function (data, type, row) {
                return '$' + data.toFixed(2);
            },
            targets: [7]
        }]
});

$(document).ready(function () {
    $("#supplier").select2({
        minimumInputLength: 2,
        formatInputTooShort: function () {
            return "Busque un proveedor";
        },
        matcher: function (term, text, option) {
            return text.toUpperCase().indexOf(term.toUpperCase()) >= 0 || option.val().toUpperCase().indexOf(term.toUpperCase()) >= 0;
        }
    });

    $("#depto").select2();
    $("#family").select2();
    $('#TableDiv').animatePanel();

    var model = JSON.parse($("#model").val());
    table.clear();
    table.rows.add($(model));
    table.columns.adjust().draw();
});

// Generar Reporte
function print() {
    StartLoading();
    days = $("#excessDays").val();
    proveedor = $("#supplier").val();
    depto = $("#depto").val();
    familia = $("#family").val();
    if (days == "") { days = "0"; }
    if (proveedor == "") { proveedor = "0"; }
    if (depto == "") { depto = "0"; }
    if (familia == "") { familia = "0"; }
    $.ajax({
        url: "/Items/ActionGenerateReportInventoryDaysReport",
        type: "POST",
        data: { "proveedor": proveedor, "depto": depto, "familia": familia, "excessDays": days },
        success: function (response) {
            EndLoading();
            if (response.responseText == "Terminó tu sesión.") {
                SessionFalse(response.responseText);
            } else if (response.success) {
                $("#modalReferenceBody").html("<iframe width='100%' height='550px' src='data:application/pdf;base64, " + encodeURI(response.responseText) + "'></iframe>")
                $("#modalReference").modal('show');
            }
            else
                toastr.error(response.responseText);
        },
        error: function () {
            EndLoading();
            toastr.error('Error inesperado contactar a sistemas.');
        }
    });
}

//Obtencion de familias
function GetFamily() {
    var deptoo = $("#depto").val();
    if (deptoo == "") {
        FillDropdown("family", null)
        return false;
    }
    $.ajax({
        url: "/MaClass/ActionGetFamily",
        type: "GET",
        data: { "deptoo": deptoo },
        success: function (response) {
            if (response.success) {
                listFamily = response.Json;
                FillDropdown("family", listFamily)
            }
            else
                FillDropdown("family", null)
        },
        error: function () {
            FillDropdown("family", null)
            toastr.error('Error inesperado contactar a sistemas.');
        }
    });
}

function FillDropdown(selector, vData) {
    if (vData != null) {
        $('#' + selector).trigger("change");
        var vItems = [];
        vItems.push('<option value="">Selecciona una Familia</option>');
        for (var i in vData) {
            if (vData[i].Selected)
                vItems.push('<option selectedselected=selected value="' + vData[i].class_id + '">' + vData[i].description + '</option>');
            else
                vItems.push('<option value="' + vData[i].class_id + '">' + vData[i].description + '</option>');
        }
        $('#' + selector).empty();
        $('#' + selector).append(vItems.join(''));
        $('#' + selector).val("").trigger('change');
    }
    else {
        var vItems = [];
        vItems.push('<option  value="">Selecciona una Familia</option>');
        $('#' + selector).empty();
        $('#' + selector).append(vItems.join(''));
        $('#' + selector).val("").trigger('change');
    }
}

// Proveedores
function GetSupplier() {
    if (deptoo == "") {
        FillDropdown("family", null)
        return false;
    }
    $.ajax({
        url: "/MaClass/ActionGetFamily",
        type: "GET",
        data: { "deptoo": deptoo },
        success: function (response) {
            if (response.success) {
                listFamily = response.Json;
                FillDropdown("family", listFamily)
            }
            else
                FillDropdown("family", null)
        },
        error: function () {
            FillDropdown("family", null)
            toastr.error('Error inesperado contactar a sistemas.');
        }
    });
}

// Buscar e imprimir en tabla
function search() {
    days = $("#excessDays").val();
    proveedor = $("#supplier").val();
    depto = $("#depto").val();
    familia = $("#family").val();
    if (days == "") { days = "0"; }
    if (proveedor == "") { proveedor = "0"; }
    if (depto == "") { depto = "0"; }
    if (familia == "") { familia = "0"; }
    StartLoading();

    $.ajax({
        url: "/Items/ActionGetInventoryDays",
        type: "POST",
        data: { "proveedor": proveedor, "depto": depto, "familia": familia, "excessDays": days },
        success: function (returndata) {
            EndLoading();
            if (returndata.responseText == "Terminó tu sesión.") {
                SessionFalse(returndata.responseText);
            } else if (returndata.success) {
                $('#TableDiv').animatePanel();
                table.clear();
                table.rows.add($(returndata.ItemMovements));
                table.columns.adjust().draw();
                if (returndata.ItemMovements.length == 0)
                    toastr.warning("No se encontraron productos");
            }
            else {
                if (returndata.responseText === 'Termino tu sesión.')
                    SessionFalse(returndata.responseText);
                else {
                    toastr.warning(returndata.responseText);
                    table.clear().draw();
                    $("#button").attr('disabled', true);
                }
            }
        },
        error: function () {
            EndLoading();
            toastr.error('Error inesperado contactar a sistemas.');
        }
    });
}