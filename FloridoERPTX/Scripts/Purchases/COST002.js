﻿document.addEventListener("DOMContentLoaded", function (event) {
    window.setTimeout(function () { document.getElementById("fa").click(); }, 1000);

    var model = JSON.parse(document.getElementById("model").value);
    table.clear();
    table.rows.add(model);
    table.columns.adjust().draw();
});

$("#beginDate").datepicker({
    autoclose: true,
    todayHighlight: true,
    showButtonPanel: true,
}).on("changeDate", function (e) {
    Search();
});

$("#endDate").datepicker({
    autoclose: true,
    startDate: "01/01/1950",
    todayHighlight: true,
}).on("changeDate", function (e) {
    Search();
});

var table = $('#Records').DataTable({
    oLanguage: {
        "sProcessing": "Procesando...", "sLengthMenu": "Mostrar _MENU_ registros", "sZeroRecords": "No se encontraron resultados", "sEmptyTable": "Ningún dato disponible en esta tabla", "sInfo": "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros", "sInfoEmpty": "Mostrando registros del 0 al 0 de un total de 0 registros", "sInfoFiltered": "(filtrado de un total de _MAX_ registros)", "sInfoPostFix": "", "sSearch": "Buscar:", "sUrl": "", "sInfoThousands": ",", "sLoadingRecords": "Cargando...", "oPaginate": { "sFirst": "Primero", "sLast": "Último", "sNext": "Siguiente", "sPrevious": "Anterior" }, "oAria": { "sSortAscending": ": Activar para ordenar la columna de manera ascendente", "sSortDescending": ": Activar para ordenar la columna de manera descendente" }
    },
    dom: "<'row'<'col-sm-4'l><'col-sm-4 text-left'B><'col-sm-4'f>t<'col-sm-6'i><'col-sm-6'p>>",
    responsive: true,
    autoWidth: true,
    order: [[1, "desc"]],
    buttons: [
        { extend: 'copy', text: 'Copiar', className: 'btn-sm' },
        { extend: 'csv', text: 'Excel', title: 'Consumo Interno', className: 'btn-sm' },
        {
            extend: 'pdf', text: 'PDF', title: 'Consumo Interno', className: 'btn-sm', exportOptions: {
                columns: [1, 2, 3, 4, 5]
            },
            customize: function (doc) {
                doc.content[1].table.widths =
                    Array(doc.content[1].table.body[0].length + 1).join('*').split('');
            }
        },
        { extend: 'print', text: 'Imprimir', className: 'btn-sm' }
    ],
    columns:
        [
            {
                "class": "details-control",
                "orderable": false,
                "data": null,
                "defaultContent": "",
                width: "1%",
                targets: 0
            },
            { targets: 1, data: 'GiCostCenterId', width: "1%" },
            { targets: 2, data: 'CostCenter', width: "1%" },
            { targets: 3, data: 'Status', width: "1%" },
            { targets: 4, data: 'Amount', width: "1%" },
            { targets: 5, data: 'CreateDate', width: "1%" }
        ],
    columnDefs: [
        { targets: 4, type: 'numeric-comma' },
        {
            targets: [4],
            type: "numeric-comma",
            render: function (data, type, row) {
                return '$' + data.toFixed(2);
            }
        },
        {
            targets: [5],
            render: function (data, type, row) {
                return moment(data).format('DD/MM/YYYY');
            }
        }]
});

var detailRows = [];
$('#Records tbody').on('click', 'tr td.details-control', function () {
    var tr = $(this).closest('tr');
    var row = table.row(tr);

    if (row.child.isShown()) {
        row.child.hide();
        tr.removeClass('details');
    }
    else {
        if (table.row('.details').length) {
            $('.details-control', table.row('.details').node()).click();
        }
        row.child(format(row.data())).show();
        tr.addClass('details');
    }
});

table.on('draw', function () {
    $.each(detailRows, function (i, id) {
        $('#' + id + ' td.details-control').trigger('click');
    });
});

var list = [];
function format(d) {
    list = [];
    var detailItems = "";
    var tabledetail = $('<div/>').addClass('loading').text('Cargando Datos...');
    $.ajax({
        url: "/CostCenter/ActionGetCostCenterItems",
        data: { "GiCostCenterId": d.GiCostCenterId },
        type: "POST",
        success: function (response) {
            if (response.result == "SF")
                SessionFalse("Terminó tu sesión");
            else if (response.result) {
                response.Data.forEach(function (item) {
                    detailItems += "<tr><td class='text-center'><div class='checkbox'><label><input type='checkbox' PartNumber=" + item.PartNumber + " Quantity=" + item.Quantity + " class='i-checks' /></label></div></td><td>" + item.PartNumber + "</td><td>" + item.Description + "</td><td>" + item.Department + "</td><td>" + item.Family + "</td><td>" + item.Quantity + "</td><td>$" + item.UnitCost + "</td><td>$" + item.Amount + "</td><td>$" + item.Ieps + "</td><td>$" + item.Iva + "</td><td>$" + item.Total + "</td>";
                });

                if (response.Data.length > 0) {
                    tabledetail.html('<table id="tabledetail" class="table table-striped table-bordered table-hover" style="width:100%"><thead><tr><th></th><th>Código</th><th>Descripción</th><th>Departamento</th><th>Familia</th><th>Cantidad</th><th>Precio</th><th>Importe</th><th>IEPS</th><th>IVA</th><th>Total</th></tr></thead><tbody>' + detailItems + '</tbody></table>').removeClass('loading');
                    reloadStyleTable();
                }

                var Apro = "";
                if (d.Status == "Pendiente") {
                    Apro = "<div class='pull-right'><button id='btn-cancel' type='button' class='btn btn-sm btn-danger' GiCostCenterId=" + d.GiCostCenterId + "><i class='fa fa-close'></i> Cancelar</button>&nbsp;&nbsp;&nbsp;<button id='btn-save' type='button' class='btn btn-sm btn-success' GiCostCenterId=" + d.GiCostCenterId + "><i class='fa fa-save'></i> Aprobar</button></div></div></div>";
                }

                tabledetail.html('<table id="tabledetail" class="table table-striped table-bordered table-hover" style="width:100%"><thead><tr><th></th><th>Código</th><th>Descripción</th><th>Departamento</th><th>Familia</th><th>Cantidad</th><th>Precio</th><th>Importe</th><th>IEPS</th><th>IVA</th><th>Total</th></tr></thead><tbody>' + detailItems + '</tbody></table>' + Apro).removeClass('loading');
                reloadStyleTable();

                $('input[type=checkbox]').iCheck({
                    checkboxClass: 'icheckbox_square-green',
                }).on('ifClicked', function () {
                    $(this).trigger("change");
                    var item = this;
                    var check = item.checked;
                    var PartNumber = item.attributes.partnumber.value;
                    var Quantity = item.attributes.quantity.value;

                    if (!check) {
                        list.push({ "PartNumber": PartNumber, "Quantity": Quantity });
                    } else {
                        list = list.filter(x => x.PartNumber != PartNumber);
                    }
                });

                document.getElementById("btn-cancel").addEventListener("click", function () {
                    swal({
                        title: "¿Esta seguro que desea Rechazar?",
                        type: "warning",
                        showCancelButton: true,
                        confirmButtonColor: "#DD6B55",
                        cancelButtonText: "Cancelar",
                        confirmButtonText: "Continuar"
                    }, function (isConfirm) {
                        if (isConfirm)
                            Reject();
                    });
                });

                document.getElementById("btn-save").addEventListener("click", function () {
                    swal({
                        title: "¿Esta seguro que desea Aprobar?",
                        type: "info",
                        showCancelButton: true,
                        confirmButtonColor: "#55dd6b",
                        cancelButtonText: "Cancelar",
                        confirmButtonText: "Continuar"
                    }, function (isConfirm) {
                        if (isConfirm)
                            Process();
                    });
                });
            }
        }
    });
    return tabledetail;
}

function clearDates() {
    document.getElementById("beginDate").value = "";
    document.getElementById("endDate").value = "";
}

function Search() {
    var beginDate = document.getElementById("beginDate").value;
    var endDate = document.getElementById("endDate").value;

    if (beginDate != "" & endDate != "") {
        if (new Date(beginDate) <= new Date(endDate)) {
            StartLoading();
            var xhttp = new XMLHttpRequest();
            xhttp.onreadystatechange = function () {
                if (this.readyState == 4 && this.status == 200) {
                    EndLoading();
                    var response = JSON.parse(this.responseText);
                    if (response.result == "SF")
                        SessionFalse("Terminó tu sesión");
                    else if (response.result) {
                        table.clear();
                        table.rows.add(response.model);
                        table.columns.adjust().draw();
                        $("#TableDiv").animatePanel();
                    }
                    else
                        toastr.error("Alerta - Error inesperado  contactar a sistemas.");
                }
                else if (this.readyState == 4 && this.status != 200) {
                    EndLoading();
                    toastr.error("Alerta - Error inesperado  contactar a sistemas.");
                }
            }
            xhttp.open("GET", "/CostCenter/ActionGetPendingCostCenterRecords?BeginDate=" + beginDate + "&EndDate=" + endDate, true);
            xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
            xhttp.send(null);
        }
        else {
            toastr.error("Fecha final no puede ser menor a la inicial.");
            clearDates();
            document.getElementById("beginDate").focus();
        }
    }
}

function reloadStyleTable() {
    $('#tabledetail').DataTable({
        order: [[1, "desc"]],
        oLanguage: {
            "sProcessing": "Procesando...", "sLengthMenu": "Mostrar _MENU_ registros", "sZeroRecords": "No se encontraron resultados", "sEmptyTable": "Ningún dato disponible en esta tabla", "sInfo": "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros", "sInfoEmpty": "Mostrando registros del 0 al 0 de un total de 0 registros", "sInfoFiltered": "(filtrado de un total de _MAX_ registros)", "sInfoPostFix": "", "sSearch": "Buscar:", "sUrl": "", "sInfoThousands": ",", "sLoadingRecords": "Cargando...", "oPaginate": { "sFirst": "Primero", "sLast": "Último", "sNext": "Siguiente", "sPrevious": "Anterior" }, "oAria": { "sSortAscending": ": Activar para ordenar la columna de manera ascendente", "sSortDescending": ": Activar para ordenar la columna de manera descendente" }
        },
        columnDefs:
            [
                {
                    targets: [0],
                    width: "1%",
                    orderable: false
                },
                {
                    targets: [2],
                    width: "1%"
                }
            ],
        "drawCallback": function (settings) {
            $('input[type=checkbox]').iCheck({
                checkboxClass: 'icheckbox_square-green',
            }).on('ifClicked', function () {
                $(this).trigger("change");
                var item = this;
                var check = item.checked;
                var PartNumber = item.attributes.partnumber.value;
                var Quantity = item.attributes.quantity.value;

                if (!check) {
                    list.push({ "PartNumber": PartNumber, "Quantity": Quantity });
                } else {
                    list = list.filter(x => x.PartNumber != PartNumber);
                }
            });
        }
    });
}

function Reject() {
    StartLoading();
    var id = parseInt(document.getElementById("btn-cancel").getAttribute("GiCostCenterId"));

    var xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function () {
        if (this.readyState == 4 && this.status == 200) {
            EndLoading();
            var response = JSON.parse(this.responseText);
            if (response.result) {
                table.clear();
                table.rows.add(response.model);
                table.columns.adjust().draw();
                $('#TableDiv').animatePanel();
                $('html, body').animate({ scrollTop: 0 }, "slow");
                toastr.success("Consumo interno rechazado con éxito.");
            }
            else
                toastr.error("Alerta - Error inesperado  contactar a sistemas.");
        }
        else if (this.readyState == 4 && this.status != 200) {
            EndLoading();
            toastr.error("Alerta - Error inesperado  contactar a sistemas.");
        }
    }
    xhttp.open("POST", "/CostCenter/ActionCancelCostCenter", true);
    xhttp.setRequestHeader("Content-Type", "application/json;charset=UTF-8");
    xhttp.send(JSON.stringify({ "CostCenterId": id }));
}

function Process() {
    StartLoading();
    var id = parseInt(document.getElementById("btn-save").getAttribute("GiCostCenterId"));

    var xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function () {
        if (this.readyState == 4 && this.status == 200) {
            EndLoading();
            var response = JSON.parse(this.responseText);
            if (response.result) {
                table.clear();
                table.rows.add(response.Data);
                table.columns.adjust().draw();
                $('#TableDiv').animatePanel();
                $('html, body').animate({ scrollTop: 0 }, "slow");
                toastr.success("Consumo interno aprobado con éxito.");
                document.getElementById("modalReportBody").innerHTML = "<iframe width='100%' height='550px' src='data:application/pdf;base64, " + encodeURI(response.Report) + "'></iframe>";
                $("#modalReport").modal('show');
            }
            else
                toastr.error("Alerta - Error inesperado  contactar a sistemas.");
        }
        else if (this.readyState == 4 && this.status != 200) {
            EndLoading();
            toastr.error("Alerta - Error inesperado  contactar a sistemas.");
        }
    }
    xhttp.open("POST", "/CostCenter/ActionAuthorizeCostCenter", true);
    xhttp.setRequestHeader("Content-Type", "application/json;charset=UTF-8");
    xhttp.send(JSON.stringify({ "CostCenterId": id, "Items": list }));
}