﻿var xml_id_global = 0;
var type_expense_global = 0;
var detailExpense = "1";
var subtotal = 0, iva = 0, ieps = 0, total, discount, iva_r, isr_r = 0
var subtotalusd = 0, ivausd = 0, iepsusd = 0, totalusd, discountusd, iva_rusd, isr_rusd = 0
const formatmoney = money => decimal => new Intl.NumberFormat("en-US", { style: "currency", currency: "USD", currencyDisplay: "symbol", maximumFractionDigits: decimal }).format(money);
function MessageNo(text) { toastr.warning(text); }

function ErrorList(value, type, text) {
    return !value ? ToastError(type, text) : type == "Succes" ? ToastError(type, text) : true;
}
function ToastError(type, text) {
    if (type == "Succes") {
        toastr.success(text)
        return true;
    }
    else if (type == "Error") {
        toastr.error(text)
    }
    else if (type == "Warning") {
        toastr.warning(text)
    }
    return false;
}

function SearchListTemp() {
    $.ajax({
        type: "GET",
        url: "/Expenses/ActionGetListGeneric",
        success: function (data) {
            if (data) {
                $.each(data, function (i, p) {
                    $('#optionsExpenseEdit').append($('<option></option>').val(p.vkey).html(p.vkey));
                });
            }
            else {
                if (returndata.responseText == "SF") {
                    SessionFalse("Su sesión a terminado.")
                }
                hideElements();
            }
        },
        error: function () {
            toastr.error('Error inesperado contactar a sistemas');
            EndLoading();
        }
    });
}
$(document).ready(function () {
    $('#cbox_ReportType').iCheck({
        checkboxClass: 'icheckbox_square-green',
    }).on('ifClicked', function () {
        $(this).trigger("click");
    });
    window.setTimeout(function () { $("#fa").click(); }, 1000);
    SearchListTemp();
});


function FillTableTotals(Expenses) {
    subtotal = 0, iva = 0, ieps = 0, total = 0, discount = 0, iva_r = 0, isr_r = 0
    subtotalusd = 0, ivausd = 0, iepsusd = 0, totalusd = 0, discountusd = 0, iva_rusd = 0, isr_rusd = 0
    Expenses.forEach(function (element) {
        if (element.Currency == "MXN") {
            subtotal += element.SubTotal;
            iva += element.TotalIVA;
            ieps += element.TotalIEPS;
            discount += element.Discount;
            isr_r += element.IsrRet;
            iva_r += element.IvaRet;
            total += element.TotalAmount;
        }
        else {
            subtotalusd += element.SubTotal;
            ivausd += element.TotalIVA;
            iepsusd += element.TotalIEPS;
            discountusd += element.Discount;
            iva_rusd += element.IvaRet;
            isr_rusd += element.IsrRet;
            totalusd += element.TotalAmount;
        }
    });
    document.getElementById("mxsubtotal").innerHTML = formatmoney(subtotal)(4);
    document.getElementById("usdsubtotal").innerHTML = formatmoney(subtotalusd)(4);
    document.getElementById("mxiva").innerHTML = formatmoney(iva)(4);
    document.getElementById("usdiva").innerHTML = formatmoney(ivausd)(4);
    document.getElementById("usddiscount").innerHTML = formatmoney(discountusd)(4);
    document.getElementById("mxdiscount").innerHTML = formatmoney(discount)(4);
    document.getElementById("mxieps").innerHTML = formatmoney(ieps)(4);
    document.getElementById("usdieps").innerHTML = formatmoney(iepsusd)(4);
    document.getElementById("mxiva_r").innerHTML = formatmoney(iva_r)(4);
    document.getElementById("usdiva_r").innerHTML = formatmoney(iva_rusd)(4);
    document.getElementById("mxisr_r").innerHTML = formatmoney(isr_r)(4);
    document.getElementById("usdisr_r").innerHTML = formatmoney(isr_rusd)(4);
    document.getElementById("mxtotal").innerHTML = formatmoney(total)(4);
    document.getElementById("usdtotal").innerHTML = formatmoney(totalusd)(4);

}

function ClearInputs() {
    document.getElementById("mxsubtotal").innerHTML = formatmoney(0)(4);
    document.getElementById("usdsubtotal").innerHTML = formatmoney(0)(4);
    document.getElementById("mxiva").innerHTML = formatmoney(0)(4);
    document.getElementById("usdiva").innerHTML = formatmoney(0)(4);
    document.getElementById("usddiscount").innerHTML = formatmoney(0)(4);
    document.getElementById("mxdiscount").innerHTML = formatmoney(0)(4);
    document.getElementById("mxieps").innerHTML = formatmoney(0)(4);
    document.getElementById("usdieps").innerHTML = formatmoney(0)(4);
    document.getElementById("mxiva_r").innerHTML = formatmoney(0)(4);
    document.getElementById("usdiva_r").innerHTML = formatmoney(0)(4);
    document.getElementById("mxisr_r").innerHTML = formatmoney(0)(4);
    document.getElementById("usdisr_r").innerHTML = formatmoney(0)(4);
    document.getElementById("mxtotal").innerHTML = formatmoney(0)(4);
    document.getElementById("usdtotal").innerHTML = formatmoney(0)(4);
}

$("#supplier").select2(
    {
        minimumInputLength: 2,
        formatInputTooShort: function () {
            return "Busque un proveedor";
        },
        matcher: function (term, text, option) {
            return text.toUpperCase().indexOf(term.toUpperCase()) >= 0 || option.val().toUpperCase().indexOf(term.toUpperCase()) >= 0;
        }
    }
);
$("#typeTypeExpense").select2();
$("#typeCategory").select2();
$("#typeCategoryEdit").select2();
$("#typeCurrency").select2();

var table = $('#TableExpenses').DataTable({
    autoWidth: true,
    responsive: true,
    oLanguage: {
        "sProcessing": "Procesando...", "sLengthMenu": "Mostrar _MENU_ registros", "sZeroRecords": "No se encontraron resultados", "sEmptyTable": "Ningún dato disponible en esta tabla", "sInfo": "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros", "sInfoEmpty": "Mostrando registros del 0 al 0 de un total de 0 registros", "sInfoFiltered": "(filtrado de un total de _MAX_ registros)", "sInfoPostFix": "", "sSearch": "Buscar:", "sUrl": "", "sInfoThousands": ",", "sLoadingRecords": "Cargando...", "oPaginate": { "sFirst": "Primero", "sLast": "Último", "sNext": "Siguiente", "sPrevious": "Anterior" }, "oAria": { "sSortAscending": ": Activar para ordenar la columna de manera ascendente", "sSortDescending": ": Activar para ordenar la columna de manera descendente" }
    },
    dom: "<'row'<'col-sm-4'l><'col-sm-4 text-left'B><'col-sm-4'f>t<'col-sm-6'i><'col-sm-6'p>>",
    lengthMenu: [[10, 25, 50, -1], [10, 25, 50, "Todos"]],
    buttons: [
        {
            extend: 'csv', text: 'Excel', title: 'Gastos de Tienda', className: 'btn-sm', exportOptions: {
                columns: [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14]
            }
        },
        {
            extend: 'pdf', text: 'PDF', title: 'Gastos de Tienda', className: 'btn-sm', exportOptions: {
                columns: [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14]
            },
            customize: function (doc) {
                doc.content[1].table.widths =
                    Array(doc.content[1].table.body[0].length + 1).join('*').split('');
            }
        },
        { extend: 'print', text: 'Imprimir', className: 'btn-sm' },
    ],
    columns: [
        {
            "class": "details-control",
            "orderable": false,
            "data": null,
            "defaultContent": "",
        },
        { targets: 1, data: 'ReferenceFolio' },
        { targets: 2, data: 'Category' },
        { targets: 3, data: 'ReferenceType' },
        { targets: 4, data: 'Supplier' },
        { targets: 5, data: 'Invoice' },
        { targets: 6, data: 'InvoiceDate' },
        { targets: 7, data: 'Currency' },
        { targets: 8, data: 'SubTotal' },
        { targets: 9, data: 'TotalIVA' },
        { targets: 10, data: 'TotalIEPS' },
        { targets: 11, data: 'Discount' },
        { targets: 12, data: 'IsrRet' },
        { targets: 13, data: 'IvaRet' },
        { targets: 14, data: 'TotalAmount' },
        { targets: 15, data: null },
        { targets: 16, data: 'StatusText' },
        { targets: 17, data: 'ExpenseToday', visible: false },
        { targets: 18, data: 'TypeXML', visible: false },
        { targets: 19, data: null }
    ],
    columnDefs: [{
        targets: [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19],
        width: "1%"
    },
    {
        targets: [6],
        render: function (data, type, row) {
            var date = moment(data).format('DD/MM/YYYY');
            if (date != 'Invalid date')
                return date;
            else
                return "";
        }
    },
    {
        targets: [8, 9, 10, 11, 12, 13, 14],
        render: function (data, type, row) {
            return `<p> ${formatmoney(data)(4)}</p>`
        }
    },
    {
        targets: [19],
        render: function (data, type, full, meta) {
            if (full.Expense_Status == 9) {
                if (full.ExpenseToday && !full.TypeXML) {
                    return `<button class="btn btn-xs btn-outline btn-warning" onclick="ShowModalEdit('${full.ReferenceFolio}', '${full.Supplier}', '${full.ReferenceType}', '${full.Category}')"><i class="fa fa-pencil"></i> Editar</button>`;
                }
                else {
                    return `<button class='btn btn-xs btn-warning' onclick='MessageNo("Este gasto fue subido con XML y no es posible modificarlo.")'><i class='fa fa-close'></i> Editar</button>`;
                }
            }
            else if (full.Expense_Status == 8)
                return `<button class='btn btn-xs btn-danger' onclick='MessageNo("Este gasto se encuentra eliminado.")'><i class='fa fa-close'></i> Editar</button>`;
        }
    },
    {
        targets: [15],
        render: function (data, type, full, meta) {
            return "<button class='btn btn-xs btn-outline btn-info' onclick='ShowReport(" + full.ReferenceFolio + ")'> <i class='fa fa-file-pdf-o'></i> Reporte </button>";
        }
    },
    {
        targets: [16],
        render: function (data, type, full, meta) {
            if (full.Expense_Status == 1 || full.Expense_Status == 2 || full.Expense_Status == 3)
                return `<strong> ${data}</strong >`
            else if (full.Expense_Status == 8)
                return `<strong style = "Color:red;" > ${data}</strong >`
            else if (full.Expense_Status == 9)
                return `<strong style = "Color:green;" > ${data}</strong >`
        }
    },
    ]
});
$("#TableExpenses").append('<tfoot><tr><th></th><th colspan = "5" style="color:#008000;">Totales</th><th  colspan = "2" style="color:#008000;">MXN</th><th id="mxsubtotal" style="color:#008000;">$0</th><th id="mxiva" style="color:#008000;">$0</th><th id ="mxieps" style="color:#008000;">$0</th><th id ="mxdiscount" style="color:#008000;">$0</th><th id ="mxisr_r" style="color:#008000;">$0</th><th id ="mxiva_r" style="color:#008000;">$0</th><th id ="mxtotal" colspan = "4" style="color:#008000;">$0</th></tr><tr><th></th><th colspan = "5"  style="color:#0D0DFF;">Totales</th><th  colspan = "2"  style="color:#0D0DFF;">USD</th><th id="usdsubtotal" style="color:#0D0DFF;">$0</th><th id="usdiva"  style="color:#0D0DFF;">$0</th><th id ="usdieps"  style="color:#0D0DFF;">$0</th><th id ="usddiscount" style="color:#0D0DFF;">$0</th><th id ="usdisr_r" style="color:#0D0DFF;">$0</th><th id ="usdiva_r" style="color:#0D0DFF;">$0</th><th id ="usdtotal" colspan = "4" style="color:#0D0DFF;">$0</th></tr></tfoot>');
var detailRows = [];
$('#TableExpenses tbody').on('click', 'tr td.details-control', function () {
    var tr = $(this).closest('tr');
    var row = table.row(tr);
    if (row.child.isShown()) {
        row.child.hide();
        tr.removeClass('details');
    }
    else {
        if (table.row('.details').length) {
            $('.details-control', table.row('.details').node()).click();
        }
        row.child(formatOne(row.data())).show();
        tr.addClass('details');
    }
});
table.on('draw', function () {
    $.each(detailRows, function (i, id) {
        $('#' + id + ' td.details-control').trigger('click');
    });
});
function formatOne(d) {
    var detailItems = "";
    var tabledetail = $('<div/>').addClass('loading').text('Cargando Datos...');
    $.ajax({
        url: '/Expenses/ActionExpenseGetItemById/',
        data: { "ReferenceFolio": d.ReferenceFolio },
        type: "GET",
        success: function (Data) {
            $.each(Data, function (index, value) {
                detailItems += "<tr><td>" + value.item_no + "</td><td>" + value.item_description + "</td><td>" + value.quantity + "</td><td>" + formatmoney(value.unit_cost)(4) + "</td><td>" + formatmoney(value.iva)(4) + "</td><td>" + formatmoney(value.ieps)(4) + "</td><td>" + formatmoney(value.iva_retained)(4) + "</td><td>" + formatmoney(value.ieps_retained)(4) + "</td><td>" + formatmoney(value.discount)(4) + "</td><td>" + formatmoney(value.item_amount)(4) + "</td>"

            });
            tabledetail.html('<table id="tabledetail" class="table table-striped table-bordered table-hover" style="width:100%"><thead><tr><th>Código</th><th>Descripción</th><th>Cantidad</th><th>Precio Unitario</th><th>IVA</th><th>IEPS</th><th>IVA Retenido</th><th>IEPS Retenido</th><th>Descuento</th><th>Subtotal</th></tr></thead><tbody>' + detailItems + '</tbody></table>').removeClass('loading');
            reloadStyleTable();
        }
    });
    return tabledetail;
}
function reloadStyleTable() {
    $('#tabledetail').DataTable({
        oLanguage: {
            "sProcessing": "Procesando...", "sLengthMenu": "Mostrar _MENU_ registros", "sZeroRecords": "No se encontraron resultados", "sEmptyTable": "Ningún dato disponible en esta tabla", "sInfo": "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros", "sInfoEmpty": "Mostrando registros del 0 al 0 de un total de 0 registros", "sInfoFiltered": "(filtrado de un total de _MAX_ registros)", "sInfoPostFix": "", "sSearch": "Buscar:", "sUrl": "", "sInfoThousands": ",", "sLoadingRecords": "Cargando...", "oPaginate": { "sFirst": "Primero", "sLast": "Último", "sNext": "Siguiente", "sPrevious": "Anterior" }, "oAria": { "sSortAscending": ": Activar para ordenar la columna de manera ascendente", "sSortDescending": ": Activar para ordenar la columna de manera descendente" }
        },
    });
}

var model = JSON.parse($("#model").val());
if (!$.isEmptyObject(model.ExpensesReportModelList)) {
    $("#TableDivExpensesDetail").hide();
    $("#TableExpensesDetail").hide();
    $('#TableDivExpenses').animatePanel();

    var NewModel = model.ExpensesReportModelList.filter(f => f.ExpenseToday == true);
    FillTableTotals(NewModel);
    table.clear();
    table.rows.add($(NewModel));
    table.columns.adjust().draw();
    $("#Print").removeAttr('disabled', 'disabled');
}
else {
    $('#TableDivExpenses').animatePanel();
    table.clear();
    table.columns.adjust().draw();
    $("#Print").attr('disabled', true);
}

function ShowReport(value) {
    StartLoading();
    $.ajax({
        type: "GET",
        url: "/Expenses/ActionGetExpenseReportById",
        data: { "referende_document": value },
        success: function (returndates) {
            if (returndates == "SF")
                SessionFalse("Termino tu session");
            document.getElementById("iframe").srcdoc = returndates;
            EndLoading();
            $("#ModalDescription").html('Reporte de gastos');
            $('#ModalReport').modal('show');
        },
        error: function (returndates) {
            toastr.error('Alerta - Error inesperado  contactar a sistemas.');
            EndLoading();
        }
    });
}

function ShowModalEdit(xml_id, supplier_name, type_expense, category_expense) {
    StartLoading();
    $.ajax({
        type: "GET",
        url: "/Expenses/ActionGetExpenseInfoById",
        data: { "referende_document": xml_id },
        success: function (returndates) {
            if (returndates == "SF")
                SessionFalse("Termino tu session");
            xml_id_global = xml_id;
            $('#inputInvoice').val(returndates.DataHeader.invoice);
            $('#inputSubtotal').val(returndates.DataHeader.subtotal);
            $('#inputIVA').val(returndates.DataHeader.iva);
            $('#inputIEPS').val(returndates.DataHeader.ieps);
            $('#inputDiscount').val(returndates.DataHeader.discount);
            $('#inputQuantity').val(returndates.DataDetail[0].quantity);
            $('#inputIVARetenido').val(returndates.DataHeader.iva_retained);
            $('#inputISRRetenido').val(returndates.DataHeader.isr_retained);
            $('#inputComment').val(returndates.DataHeader.remark);
            $('#paymentList').val(returndates.DataHeader.currency).trigger('change');
            validateNumber(returndates.DataHeader.subtotal, 'inputSubtotal', 0);
            $("#ModalEditTitle").html("<i class='fa fa-newspaper-o'></i> Gasto: " + supplier_name);
            if (type_expense.toUpperCase() == "FIJO") {
                type_expense_global = 1;
                $('#variableExpense').addClass("hide");
                $('#fixedExpense').removeClass("hide");
                $('#optionsExpenseEdit').val(category_expense).trigger('change');
            } else {
                type_expense_global = 0;
                $('#fixedExpense').addClass("hide");
                $('#variableExpense').removeClass("hide");
                $('#typeCategoryEdit').val(category_expense).trigger('change');
            }
            $("#ModalEditDescrip").val("show");
            $("#ModalInvoiceEdit").modal("show");
            EndLoading();
        },
        error: function (returndates) {
            toastr.error('Alerta - Error inesperado  contactar a sistemas.');
            EndLoading();
        }
    });
}

function validateNumber(value, nameInput, optionValue) {
    if (!isNaN(value) &&
        (value >= optionValue && value < 99999999)) {
        $("#" + nameInput).val(value);
        var inputSubtotal = $('#inputSubtotal').val() ? parseFloat($('#inputSubtotal').val()) : 0;
        var inputIVA = $('#inputIVA').val() ? parseFloat($('#inputIVA').val()) : 0;
        var inputIEPS = $('#inputIEPS').val() ? parseFloat($('#inputIEPS').val()) : 0;
        var inputDiscount = $('#inputDiscount').val() ? parseFloat($('#inputDiscount').val()) : 0;
        var inputIVARetenido = $('#inputIVARetenido').val() ? parseFloat($('#inputIVARetenido').val()) : 0;
        var inputISRRetenido = $('#inputISRRetenido').val() ? parseFloat($('#inputISRRetenido').val()) : 0;
        var sum = (inputSubtotal + inputIVA + inputIEPS - inputDiscount - (inputIVARetenido + inputISRRetenido));
        totalSum = sum;
        $('#total_sum').html(formatmoney(sum)(4));
        return true;
    } else {
        $("#" + nameInput).focus();
        $("#" + nameInput).val("");
        return false;
    }
}


function SaveEditWithFolio() {
    if (type_expense_global == 1 && $('#optionsExpenseEdit').val() == "") {
        toastr.warning("Ingrese un tipo de gasto.");
        return false;
    } else if (type_expense_global == 0 && $('#typeCategoryEdit').val() == "") {
        toastr.warning("Ingrese un Area.");
        return false;
    }
    if (ErrorList($("#inputInvoice").val() != '', 'Warning', 'Ingrese una factura correcta') &&
        ErrorList($("#paymentList").val() != '', 'Warning', 'Seleccione una moneda.') &&
        ErrorList($("#inputSubtotal").val() != '', 'Warning', 'Ingrese un subtotal.') &&
        ErrorList($("#inputIVA").val() != '', 'Warning', 'Ingrese un IVA.') &&
        ErrorList($("#inputIEPS").val() != '', 'Warning', 'Ingrese un IEPS.') &&
        ErrorList($("#inputDiscount").val() != '', 'Warning', 'Ingrese un Descuento.') &&
        ErrorList($("#inputQuantity").val() != '', 'Warning', 'Ingrese una cantidad.') &&
        ErrorList($("#inputIVARetenido").val() != '', 'Warning', 'Ingrese el IVA Retenido.') &&
        ErrorList($("#inputComment").val() != '', 'Warning', 'Ingrese un comentario') &&
        ErrorList(parseFloat($("#inputSubtotal").val()) > 0, 'Warning', 'Ingrese un subtotal correcto.') &&
        ErrorList(totalSum > 0, 'Warning', 'El costo final debe ser mayor a 0.')) {
        var model = { invoice: $("#inputInvoice").val(), currency: $("#paymentList").val(), subtotal: $("#inputSubtotal").val(), iva: $("#inputIVA").val(), ieps: $("#inputIEPS").val(), total: totalSum, discount: $("#inputDiscount").val(), quantity: $("#inputQuantity").val(), ivaRetenido: $("#inputIVARetenido").val(), isrRetenidos: $("#inputISRRetenido").val(), comment: $("#inputComment").val(), area: type_expense_global == 1 ? $('#optionsExpenseEdit').val() : $('#typeCategoryEdit').val(), type_expense: type_expense_global, xml_id: xml_id_global };
        swal({
            title: "¿Esta seguro?",
            text: "Se editara el gasto",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Continuar",
            cancelButtonText: "Cancelar"
        }, function (isConfirm) {
            if (isConfirm) {
                StartLoading();
                $("#ModalInvoiceEdit").modal("hide");
                var texto = "";
                var success = "";
                axios.post("/Expenses/ActionEditExpense/", { "modelExpense": model })
                    .then(function (data) {
                        EndLoading();
                        if (data.data == "SF")
                            SessionFalse(data.data);
                        else if (data.data == 0)
                            success = "Se ha modificado el gasto correctamente.";
                        else {
                            switch (data.data) {
                                case 1001:
                                    texto = "No se encontro el gasto - (1000)"
                                    break;
                                case 1002:
                                    texto = "No se encontro el gasto - (1002)"
                                    break;
                                case 1003:
                                    texto = "No se encontro el gasto - (1003)"
                                    break;
                                case 1004:
                                    texto = "El folio ya se encuentra registrado con este mismo proveedor. - (1004)"
                                    break;
                                case 8001:
                                    texto = "Error desconocido al procesar los datos - (8001)"
                                    break;
                                default:
                                    texto = "Error inesperado contactar a sistemas."
                                    break;
                            }
                        }
                    })
                    .catch(function (error) {
                        console.log(error.data)
                    }).then(function () {
                        EndLoading();
                        UpdateExpense();
                        if (texto != "")
                            toastr.error(texto);
                        if (success != "")
                            toastr.success(success);
                    })
            }
        });
    }
}

function UpdateExpense() {
    var date = new Date();
    $.ajax({
        url: "/Expenses/ActionExpensesReport/",
        type: "GET",
        data: { "SupplierId": "", "Category": "", "BeginDate": moment(date).format('MM/DD/YYYY'), "EndDate": moment(date).format('MM/DD/YYYY'), "Currency": "", "typeExpense": "", "status": "9" },
        success: function (result) {
            EndLoading();
            if (result.status) {
                if (!$.isEmptyObject(result.model)) {
                    $('#TableDivExpenses').animatePanel();
                    FillTableTotals(result.model);
                    table.clear();
                    table.rows.add($(result.model));
                    table.columns.adjust().draw();
                }
                else {
                    $('#TableDivExpenses').animatePanel();
                    table.clear();
                    table.columns.adjust().draw();
                }
            }
            else
                SessionFalse("Se terminó su sesion.");
        },
        error: function () {
            toastr.remove();
            toastr.error('Error inesperado contactar a sistemas.');
            EndLoading();
        }
    });
}