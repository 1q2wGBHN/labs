﻿listCheck = [];
listItem = [];
list = [];
$(document).ready(function () {
    $(document).on("keypress", '.Numeric', function (event) {
        var keyValue = event.key;
        if (keyValue.match(/^(\d*\.?\d*)$/))
            return true;
        else
            return false;
    });

    $("#suppliersDrop").select2({
        minimumInputLength: 2,
        formatInputTooShort: function () {
            return "Busque un proveedor";
        },
        matcher: function (term, text, option) {
            return text.toUpperCase().indexOf(term.toUpperCase()) >= 0 || option.val().toUpperCase().indexOf(term.toUpperCase()) >= 0;
        }
    });

    window.setTimeout(function () {
        $(".normalheader").addClass("small-header");
        $("#fa").removeClass("fa-arrow-up").addClass("fa-arrow-down");
        $("#hbreadcrumb").removeClass("m-t-lg");
    }, 500);
});

function GetProductsCostBySupplier() {
    if ($('#suppliersDrop').val() != "" && $('#suppliersDrop').val() != null) {
        listCheck = [];
        listFinals = [];
        $.ajax({
            url: "/ItemSuppliers/ActionGetItemsBySupplierCodeInSalePrice",
            type: "GET",
            data: { "supplier_code": $('#suppliersDrop').val() },
            success: function (returndates) {
                EndLoading();
                if (returndates.success) {
                    var table = "";
                    i = 0;
                    listItem = returndates.Json
                    $.each(returndates.Json, function (Purch012, value) {
                        var iva = parseInt(value.iva);
                        var ieps = parseInt(value.ieps);
                        var costo = parseFloat(value.item_cost)
                        var costoIEPS = (ieps / 100) * (costo);
                        var costoTotal = ((costo + costoIEPS) * (iva / 100)) + costo;
                        if (list.length > 0) {
                            const result = list.filter(x => x.partNumber == value.item_number);
                            if (result.length > 0) {
                                table += "<tr class='active success'><td style='text-align:center;font-family: Verdana; font-size:10pt'><a id='UPC_" + Purch012 + "'>" + value.item_number + "</a></td>"
                                table += "<td style='text-align:center;font-family: Verdana; font-size:10pt'><a id='description'>" + value.item_name + " </a></td>"
                                table += "<td style='text-align:center;font-family: Verdana; font-size:10pt'><a id='costSupplier_" + Purch012 + "'>$" + value.item_cost + "</a></td>"
                                table += "<td style='text-align:center;font-family: Verdana; font-size:10pt'><a id='iva_" + Purch012 + "'>" + value.iva + "</a></td>"
                                table += "<td style='text-align:center;font-family: Verdana; font-size:10pt'><a id='ieps_" + Purch012 + "'>" + value.ieps + "</a></td>"
                                table += "<td style='text-align:center;font-family: Verdana; font-size:10pt'><a id='costTotalSupplier_" + Purch012 + "'>$" + costoTotal.toFixed(4) + "</a></td>"
                                table += "<td style='text-align:center;font-family: Verdana; font-size:10pt'><a id='margen_" + Purch012 + "'>" + value.margen + "</a></td>"
                                table += "<td style='text-align:center;font-family: Verdana; font-size:10pt'><div class='input-group m-b'><span class='input-group-addon'>$</span><input type='number' min='0' max='9999999' value='" + value.price + "' id='priceUser" + Purch012 + "' class='form-control Numeric' style='text-align:center;font-family: Verdana; font-size:10pt' onfocusout='CheckQuantity(" + Purch012 + ")' /></div></td></tr>"
                                listCheck[i] = { part_number: value.item_number, margin: 0, priceUser: 0, part_iva: value.iva, part_ieps: value.ieps, check: false };
                            }
                            else {
                                table += "<tr class=''><td style='text-align:center;font-family: Verdana; font-size:10pt'><a id='UPC_" + Purch012 + "'>" + value.item_number + "</a></td>"
                                table += "<td style='text-align:center;font-family: Verdana; font-size:10pt'><a id='description'>" + value.item_name + " </a></td>"
                                table += "<td style='text-align:center;font-family: Verdana; font-size:10pt'><a id='costSupplier_" + Purch012 + "'>$" + value.item_cost + "</a></td>"
                                table += "<td style='text-align:center;font-family: Verdana; font-size:10pt'><a id='iva_" + Purch012 + "'>" + value.iva + "</a></td>"
                                table += "<td style='text-align:center;font-family: Verdana; font-size:10pt'><a id='ieps_" + Purch012 + "'>" + value.ieps + "</a></td>"
                                table += "<td style='text-align:center;font-family: Verdana; font-size:10pt'><a id='costTotalSupplier_" + Purch012 + "'>$" + costoTotal.toFixed(4) + "</a></td>"
                                table += "<td style='text-align:center;font-family: Verdana; font-size:10pt'><a id='margen_" + Purch012 + "'></a></td>"
                                table += "<td style='text-align:center;font-family: Verdana; font-size:10pt'><div class='input-group m-b'><span class='input-group-addon'>$</span><input type='number' min='0' max='9999999' id='priceUser" + Purch012 + "' class='form-control Numeric' style='text-align:center;font-family: Verdana; font-size:10pt' onfocusout='CheckQuantity(" + Purch012 + ")' /></div></td></tr>"
                                listCheck[i] = { part_number: value.item_number, margin: 0, priceUser: 0, part_iva: value.iva, part_ieps: value.ieps, check: false };
                            }
                        }
                        else {
                            table += "<tr class=''><td style='text-align:center;font-family: Verdana; font-size:10pt'><a id='UPC_" + Purch012 + "'>" + value.item_number + "</a></td>"
                            table += "<td style='text-align:center;font-family: Verdana; font-size:10pt'><a id='description'>" + value.item_name + " </a></td>"
                            table += "<td style='text-align:center;font-family: Verdana; font-size:10pt'><a id='costSupplier_" + Purch012 + "'>$" + value.item_cost + "</a></td>"
                            table += "<td style='text-align:center;font-family: Verdana; font-size:10pt'><a id='iva_" + Purch012 + "'>" + value.iva + "</a></td>"
                            table += "<td style='text-align:center;font-family: Verdana; font-size:10pt'><a id='ieps_" + Purch012 + "'>" + value.ieps + "</a></td>"
                            table += "<td style='text-align:center;font-family: Verdana; font-size:10pt'><a id='costTotalSupplier_" + Purch012 + "'>$" + costoTotal.toFixed(4) + "</a></td>"
                            table += "<td style='text-align:center;font-family: Verdana; font-size:10pt'><a id='margen_" + Purch012 + "'></a></td>"
                            table += "<td style='text-align:center;font-family: Verdana; font-size:10pt'><div class='input-group m-b'><span class='input-group-addon'>$</span><input type='number' min='0' max='9999999' id='priceUser" + Purch012 + "' class='form-control Numeric' style='text-align:center;font-family: Verdana; font-size:10pt' onfocusout='CheckQuantity(" + Purch012 + ")' /></div></td></tr>"
                            listCheck[i] = { part_number: value.item_number, margin: 0, priceUser: 0, part_iva: value.iva, part_ieps: value.ieps, check: false };
                        }
                        i++;
                    });
                    $("#tableChangePriceBody").html(table);
                    $(".textHidde").hide();
                }
                else {
                    if (returndates.responseText == "Termino tu sesion.") {
                        SessionFalse(returndates.responseText);
                    }
                    else {
                        toastr.warning('' + returndates.responseText + '');
                        $("#suppliersDrop").select2('open');
                        $("#suppliersDrop").val("").trigger("change");
                        $("#tableChangePriceBody").html("");
                    }
                }
            },
            error: function () {
                EndLoading();
                console.log("Desconocido, contacte a sistemas.")
            }
        });
    }
    else {
        $("#tableChangePriceBody").html("");
    }
}

function Search() {
    var value = document.querySelector("#filter").value.toLowerCase()
    $("#tableChangePriceBody tr").filter(function () {
        $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
    });
}

function CheckQuantity(index) {
    var price = $('#priceUser' + index).val();
    isDecimal = /^\d+\.9$/;
    var cost = $('#costTotalSupplier_' + index).text();
    var pIEPS = $('#ieps_' + index).text();
    if (!isNaN(price)) {
        if (price != "") {
            if (parseFloat(price) > 0 && parseFloat(price) < 99999999) {
                if (price != "" && isDecimal.test(parseFloat(price))) {
                    listCheck[index].priceUser = price;
                    listCheck[index].check = true;
                    OperationMargin(index, price, cost, pIEPS);
                }
                else if (price != "" && !isDecimal.test(parseFloat(price))) {
                    $('#priceUser' + index).focus();
                    toastr.warning("Precio debe terminar en .9");
                }
                else {
                    listCheck[index].priceUser = price;
                    listCheck[index].check = false;
                    return false;
                }
            } else {
                $('#priceUser' + index).val('');
                toastr.warning("Ingrese una cantidad correcta.");
                listCheck[index].priceUser = price;
                listCheck[index].check = false;
            }
        } else {
            $('#priceUser' + index).val('');
            toastr.warning("Ingrese una cantidad correcta.");
            listCheck[index].priceUser = price;
            listCheck[index].check = false;
        }
    } else {
        $('#priceUser' + index).val('');
        toastr.warning("Ingrese una cantidad correcta.");
        listCheck[index].priceUser = price;
        listCheck[index].check = false;
        return false;
    }
}

function OperationMargin(index, PriceUser, costSupplier, pIEPS) {
    var supplierCost = costSupplier.split("$");
    var ieps = parseInt(pIEPS);
    var ieps2 = 1 + (ieps / 100);
    var costoConIEPS = ieps2 * supplierCost[1];
    var x = costoConIEPS / parseFloat(PriceUser);
    var margen = (1 - x) * 100;
    $('#margen_' + index).text('$' + margen.toFixed(4));
    listCheck[index].margin = margen.toFixed(4);
}

function SavePrices() {
    listFinals = [];
    for (var x = 0; x < listCheck.length; x++) {
        if (listCheck[x].check) {
            listFinals[x] = { partNumber: listCheck[x].part_number, margin: listCheck[x].margin, priceUser: listCheck[x].priceUser };
        }
    }
    if ($("#suppliersDrop").val() == '') {
        toastr.warning('Seleccione Un Proveedor');
    }
    else if (listFinals.length < 1) {
        toastr.warning("Ingresa precio de al menos 1 producto.");
    }
    else {
        swal({
            title: "¿Esta seguro que desea guardar el cambio de precio?",
            text: "Los cambios se veran reflejados en seguida solo el dia de hoy.",
            type: "info",
            showCancelButton: true,
            confirmButtonColor: "#55dd6b",
            confirmButtonText: "Continuar",
            cancelButtonText: "Cancelar"
        }, function (isConfirm) {
            if (isConfirm)
                SaveNewPrices(listFinals);
        });
    }
}

function SaveNewPrices(listFinals) {
    listFinals = listFinals.filter(Boolean);
    StartLoading();
    $.ajax({
        type: "POST",
        url: "/ItemSuppliers/ActionSaveChangePriceStores/",
        data: { itemChangePrice: listFinals, ProgramId: "PURCH012.cshtml" },
        success: function (returndate) {
            EndLoading();
            if (returndate.success) {
                toastr.success(returndate.responseText);
                DeleteAll();
                DeleteAll();
            }
            else {
                if (returndate.responseText == 'Tu sesion termino.') {
                    setTimeout(function () {
                        SessionFalse(returndate.responseText);
                    }, 700)
                }
                else {
                    setTimeout(function () {
                        toastr.warning(returndate.responseText + '.');
                    }, 700)
                }
            }
        },
        error: function () {
            EndLoading();
            toastr.error("Error Inesperado, Contacte a sistemas");
        }
    });
}

function Clear() {
    $('#fechaGeneral').val("");
    $("#filter").val("");
}

function DeleteAll() {
    $('#tableChangePriceBody').html("");
    listCheck = [];
    listFinals = [];
    $('#fechaGeneral').val("");
    $('#suppliersDrop').prop("selectedIndex", 0).trigger('change');
    $("#filter").val("");
}