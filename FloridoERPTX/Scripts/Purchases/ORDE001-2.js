﻿listSupplier = []
listAutorize = []
listItem = []
listItemSave = []
const sesion = "Se termino su sesion.";
$(document).ready(function () {
    $("#currency").select2();
    $("#allCheck").iCheck('uncheck');
    $(".content").hide();
    $("#commentForm").hide();
    $("#disableCheck").hide();
    $("#folioDiv").hide()
    $("#buttonPrint").hide()
    $("#buttonList").attr('disabled', 'disabled')
    $('#buttonPrint').click(function () {
        swal({
            title: "Desea imprimir?",
            text: "Si imprime se guardara y no podrá ser cancelado el formato de pedido",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Si",
            cancelButtonText: "No",
            closeOnConfirm: true,
            closeOnCancel: true
        }, function (isConfirm) {
            if (isConfirm)
                SaveOrder()
        });
    });
    window.setTimeout(function () { $("#fa").click(); }, 1000);
    $(".content").show()
    $("#searchDiv").hide()
    $("#searchDivFilter").hide()
});

function RemoveList() {
    $("#buttonPrint").hide()
    $("#tableOrdered").html("");
    $("#disableCheck").hide();
    $("#tableOrderHead").hide()
    $("#commentForm").hide();
    $("#searchDiv").hide()
    $("#searchDivFilter").hide()
    $("#comment").val("");
    $("#allCheck").iCheck('uncheck');
    listItem = []
}
function RemoveFilter() {
    $('#depto').val($("#depto :first").val()).trigger('change');
    $('#family').val($("#family :first").val()).trigger('change');
    console.log("Department*******");
    console.log($('#depto').val());
    console.log("Family*******");
    console.log($('#family').val());
    SearchFunction();
}

function RemoveListCombo() {
    RemoveList()
    $("#days").val(0)
    $("#buttonList").attr('disabled', 'disabled')
}

function IsPositive() {
    if (parseInt($("#days").val()) > 0) {
        $("#days").val(parseInt($("#days").val()));
        $("#buttonList").removeAttr('disabled');
        RemoveList()
    }
    else {
        $("#buttonList").attr('disabled', 'disabled')
        RemoveList()
        $("#days").val(0);
    }
}

function SaveOrder() {
    OrderHead = { supplier_id: $("#supplier").val(), site_id: $("#site").val(), refill_days: $("#days").val(), status: 1, comment: $("#comment").val() }
    OrderSaveList = [];
    i = 0;
    $.each(DeleteDuplicates(listItem, "Part_number"), function (index, value) {
        if (listItemSave[index].Check) {
            OrderSaveList.push(value);
        }
    });
    StartLoading();
    $.ajax({
        type: "POST",
        url: "/Orders/ActionNewOrderHeaderAndOrderList",
        data: { orderHead: OrderHead, OrderList: OrderSaveList , Currency : $("#currency").val() },
        success: function (returndates) {
            EndLoading()
            if (returndates == "SF") {
                SessionFalse(sesion)
            }
            else if (returndates.Number == 1000) {
                $("#buttonPrint").hide()
                $("#buttonList").hide()
                $("#site").attr('disabled', 'disabled')
                $("#supplier").attr('disabled', 'disabled')
                $("#days").attr('disabled', 'disabled')
                toastr.success('Se realizo con exito el pdf');
                var sampleArr = Base64ToArrayBuffer(returndates.responseText);
                SaveByteArray("reporte", sampleArr);
                Clear()
            }
            else if (returndates.Number == 1001) {
                toastr.warning("Hubo un problema al guardar el formato pedido")
            }
            else if (returndates.Number == 1002) {
                toastr.warning("Hubo un problema al guardar los productos de el formato pedido")
            }
            else if (returndates.Number == 1003) {
                toastr.warning("No se pudo borrarse una formato pedido erroneo")
            }
            else if (returndates.Number == 1004) {
                toastr.warning("No se pudo generar el pdf")
            }
            else {
                toastr.warning("Error desconocido")
            }
        },
        error: function (returndates) {
            EndLoading()
            toastr.error('Alerta - Error inesperado  contactar a sistemas.');
        }
    })
    Clear();
}

function AddOrderList() {
    $("#tableOrdered").html("");
    listItem = []
    if ($("#select2-chosen-2").html() != "Selecciona un proveedor") {
        StartLoading()
        axios.get("/ItemSuppliers/ActionGetOrder?Supplier=" + $("#supplier").val() + "&Site=" + $("#site").val() + "&Currency=" + $("#currency").val()).then(function (returndates) {
            if (returndates.data == "SF") {
                SessionFalse(sesion)
            }
            else if (returndates.data.length > 0) {
                $("#allCheck").iCheck('uncheck');
                $("#tableOrdered").html("");
                $.each(returndates.data, function (index, value) {
                    var OrderList = {
                        Folio: value.Folio, Part_number: value.ParthNumber, Stock: value.Stock, Description: value.Description,
                        Pre_purchase: value.PrePruchase, Average_sale: value.AverageSale,
                        Department: value.Department, Family: value.Family, Fam: value.Fam, Dep: value.Dep,
                        Barcode: value.BarCode, Packing: value.Packing, Suggested_systems: ((value.AverageSale * $("#days").val() + 2) - value.Stock - value.PrePruchase) > 0 ? ((value.AverageSale * $("#days").val() + 2) - value.Stock - value.PrePruchase) : 0, Id: 0
                    }
                    listItem.push(OrderList)
                });
                AddTable(listItem)
                $("#disableCheck").show();
                $("#tableOrderHead").show();
                $("#searchDiv").show()
                $("#searchDivFilter").show()
                $("#comment").val("")
                $("#commentForm").show();
            }
            else {
                $("#buttonPrint").hide()
                $("#searchDiv").hide()
                $("#searchDivFilter").hide()
                $("#disableCheck").hide();
                $("#days").val(0)
                $("#buttonList").attr('disabled', 'disabled')
                toastr.warning('No se encontraron productos');
            }
        }).catch(function (error) {

        }).then(function () {
            EndLoading()
        })
    }
    else {
        toastr.warning('Selecciona un proveedor');
    }
}

function AddTable(list) {
    if ($("#allCheck").is(':checked') && $("#days").val() > 0 && listItem.length > 0) {
        $("#buttonPrint").show()
    }
    else {
        $("#buttonPrint").hide()
    }
    $("#tableOrdered").html("");
    var table = "";
    listItemSave = []
    i = 0;
    $.each(DeleteDuplicates(list, "Part_number"), function (index, value) {
        table += "<tr>"
        if ($("#allCheck").is(':checked'))
            table += "<td>" + '<div class="checkbox checked "><label> <input type="checkbox" id="' + i + '" onchange="IsCheck(' + i + ')" class="i-checks" checked="checked"></label></div>' + "</td>";
        else
            table += "<td>" + '<div class="checkbox "><label> <input type="checkbox" id="' + i + '" onchange="IsCheck(' + i + ')" class="i-checks" ></label></div>' + "</td>";
        table += "<td>" + value.Part_number + "</td>"
        table += "<td>" + value.Description + "</td><td>" + value.Stock + "</td><td>" + value.Pre_purchase + "</td><td>" + value.Average_sale + "</td>" + "</td>";
        table += "<td>" + (value.Suggested_systems.toFixed(2) > 0 ? value.Suggested_systems.toFixed(2) : 0) + "<td >" + value.Packing + "</td><td >" + value.Department + "</td><td>" + value.Family + "</td><td style='display: none;'>" + value.Dep + "</td><td style='display: none;'>" + value.Fam + "</td></tr>"; //<td style='display:none;'>
        i++;
        listItemSave.push({
            Folio: 0, Part_number: value.Part_number, Stock: value.Stock, Description: value.Description,
            Pre_purchase: value.Pre_purchase, Average_sale: value.Average_sale,
            Barcode: 0, Packing: value.Packing, Suggested_systems: value.Suggested_systems, Id: 0, Check: $("#allCheck").is(':checked')
        })
    });
    $("#tableOrdered").html(table)
    $('input').iCheck({
        checkboxClass: 'icheckbox_square-green',
    }).on('ifClicked', function () {
        $(this).trigger("change");
    });

    $(".textHidde").hide()
}

function AllCheck() {
    if (!$("#allCheck").is(':checked')) {
        $("#allCheck").iCheck('check');
        if ($("#days").val() > 0 && listItem.length > 0) {
            $("#buttonPrint").show()
        }
    }
    else {
        $("#allCheck").iCheck('uncheck');
        $("#buttonPrint").hide()
    }
    AddTable(listItem)
}

function IsCheck(id) {
    listItemSave[id].Check = !$("#" + id).is(':checked')
    if (listItemSave.filter(x => x.Check == false).length > 0 && listItemSave.filter(x => x.Check == false).length < listItemSave.length) {
        $("#allCheck").iCheck('uncheck');
        $("#buttonPrint").show()
    }
    else {
        if (listItemSave.filter(x => x.Check == false).length == listItemSave.length) {
            $("#allCheck").iCheck('uncheck');
            $("#buttonPrint").hide()
        }
        else {
            $("#allCheck").iCheck('check');
            $("#buttonPrint").show()
        }
    }
}

function DeleteDuplicates(arr, prop) {
    var newArray = [];
    var lookup = {};

    for (var i in arr) {
        lookup[arr[i][prop]] = arr[i];
    }

    for (i in lookup) {
        newArray.push(lookup[i]);
    }

    return newArray;
}

$("#supplier").select2({
    minimumInputLength: 3,
    allowClear: true,
    placeholder: "Seleccione un proveedor",
    initSelection: function (element, callback) {
        callback({ id: "", text: "Seleccione una opcion" });
    },
    ajax: {
        url: "/Suppliers/ActionSearchSupplier/",
        dataType: 'json',
        type: "GET",
        quietMillis: 50,
        data: function (Filter) {
            return {
                Filter: Filter
            };
        },
        results: function (data) {
            return {
                results: $.map(data.Json, function (item) {
                    return {
                        text: item.BusinessName,
                        id: item.SupplierId
                    }
                }),
            };
        }
    }
});

function Search(id, table) {
    var value = document.querySelector(id).value.toLowerCase();
    $(table + " tr").filter(function () {
        $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
    });
}

function SearchFunction() {
    var input, filter, table, tr, td, tdDepartment, tdFamiliy, i, txtValue, Department, Family, bool;
    Department = $('#depto').val();
    Family = $('#family').val();
    if (Department == "")
        Department = 0;
    if (Family == "")
        Family = 0;
    input = document.getElementById("SearchInput");
    filter = input.value.trim().toUpperCase();
    table = document.getElementById("tableOrdered");
    tr = table.getElementsByTagName("tr");
    bool = true;
    for (i = 0; i < tr.length; i++) {
        td = tr[i].getElementsByTagName("td")[1];
        td2 = tr[i].getElementsByTagName("td")[2];
        tdDepartment = tr[i].getElementsByTagName("td")[10];
        tdFamiliy = tr[i].getElementsByTagName("td")[11];


        if (Department == 0 && Family == 0) {
            tr[i].style.display = "";
            if (filter != "") {
                if (td) {
                    txtValue = td.textContent || td.innerText;
                    txtValue2 = td2.textContent || td2.innerText;
                    if ((txtValue.toUpperCase().indexOf(filter) > -1)) {
                        tr[i].style.display = "";
                    } else if ((txtValue2.toUpperCase().indexOf(filter) > -1)) {
                        tr[i].style.display = "";
                    } else if ((txtValue2.toUpperCase().indexOf(filter) <= -1)) {
                        tr[i].style.display = "none";
                    } else {
                        tr[i].style.display = "none";
                    }
                }
            }
        } else {
            if (Department != 0) {
                if (tdDepartment) {
                    txtValue = tdDepartment.textContent || tdDepartment.innerText;
                    if (txtValue.toUpperCase() == Department) {
                        tr[i].style.display = "";
                        bool = true;
                    } else {
                        tr[i].style.display = "none";
                        bool = false;
                    }
                }
                if (Family != 0) {
                    if (tdFamiliy) {
                        txtValue = tdFamiliy.textContent || tdFamiliy.innerText;
                        if (txtValue.toUpperCase() == Family) {
                            if (bool) {
                                tr[i].style.display = "";
                                bool = true;
                            }
                        } else {
                            tr[i].style.display = "none";
                            bool = false;
                        }
                    }
                }
            }
            if (filter != "") {
                if (td) {
                    txtValue = td.textContent || td.innerText;
                    txtValue2 = td2.textContent || td2.innerText;
                    if (txtValue.toUpperCase().indexOf(filter) > -1) {
                        if (bool) {
                            tr[i].style.display = "";
                        }
                    } else if ((txtValue2.toUpperCase().indexOf(filter) > -1)) {
                        if (bool) {
                            tr[i].style.display = "";
                        }
                    } else if ((txtValue2.toUpperCase().indexOf(filter) <= -1)) {
                        if (bool) {
                            tr[i].style.display = "none";
                        } else {
                            tr[i].style.display = "none";
                        }
                    }
                }
            }
        }
    }
}

function FillDropdown(selector, vData) {
    if (vData != null) {
        var vItems = [];
        vItems.push('<option value="0">Seleccione una familia</option>');
        for (var i in vData) {
            if (vData[i].Selected)
                vItems.push('<option selected value="' + vData[i].class_id + '">' + vData[i].description + '</option>');
            else
                vItems.push('<option value="' + vData[i].class_id + '">' + vData[i].description + '</option>');
        }
        $('#' + selector).empty();
        $('#' + selector).append(vItems.join(''));
        $('#family').val($("#family :first").val()).trigger('change');
    }
    else {
        var vItems = [];
        vItems.push('<option selected value="0">Seleccione una familia</option>');
        $('#' + selector).empty();
        $('#' + selector).append(vItems.join(''));
        $('#family').val($("#family :first").val()).trigger('change');
    }
}

function GetFamily() {
    var depto = $("#depto").val();
    if (depto == "" || depto == 0) {
        $('#family').val($("#family :first").val()).trigger('change');
        FillDropdown("family", null)
        return false;
    }
    $.ajax({
        url: '/MaClass/ActionGetFamily/',
        type: "GET",
        data: { "deptoo": depto },
        success: function (response) {
            if (response.success) {
                listFamily = response.Json;
                FillDropdown("family", listFamily);
            }
            else
                FillDropdown("family", null);
        },
        error: function () {
            FillDropdown("family", null);
            toastr.error('Error inesperado contactar a sistemas.');
        }
    });
}

function Base64ToArrayBuffer(base64) {
    var binaryString = window.atob(base64);
    var binaryLen = binaryString.length;
    var bytes = new Uint8Array(binaryLen);
    for (var i = 0; i < binaryLen; i++) {
        var ascii = binaryString.charCodeAt(i);
        bytes[i] = ascii;
    }
    return bytes;
}

function SaveByteArray(reportName, byte) {
    var blob = new Blob([byte]);
    var link = document.createElement('a');
    link.href = window.URL.createObjectURL(blob);
    var fileName = reportName;
    link.download = fileName + ".pdf";
    link.click();
}


function Clear() {
    $("#buttonList").show()
    $("#buttonList").attr('disabled', 'disabled')
    $("#supplier").removeAttr('disabled', 'disabled')
    $("#days").removeAttr('disabled', 'disabled')
    $("#days").val(0)
    $("#comment").val("")
    $("#allCheck").iCheck('check');
    $("#disableCheck").hide();
    $("#commentForm").hide()
    $('#supplier').val('').trigger('change');
    $("#currency").prop("disabled", false);
    listItemSave = [];
    listItem = [];
    RemoveList()
}

function blockCurrency() {
    $("#currency").prop("disabled", true);
}