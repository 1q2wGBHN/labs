﻿var ListTable = [];
const sesion = "Se termino su sesion.";
var User;
var product = { partNumber: '', baseCost: 0 }
var supplier = 0;
var product = {};
const isDecimal = number => decimal => {
    const IsDecimal = RegExp(`^\\d+\\.?\\d{0,${decimal}}$`);
    return IsDecimal.test(number);
}
function ComboboxOreders() {
    const selectOption = selectId => selectName => `<option value="${selectId}">${selectName}</option>`;
    axios.get("/PurchaseOrder/ActionGetAllOrdersEdit").then((data) => {
        $("#orderSelect").html([`<option value="0">Seleccione una orden</option>`, ...data.data.map(x => selectOption(x.PurchaseNo) (x.PurchaseNo + " - " +x.supplier_name))])
    }).catch((error) => {

    })
}
const Sum = list => Name => list.reduce((init, total) => { return init + parseFloat(total[Name]) }, 0)
const fomartmoney = money => decimal => new Intl.NumberFormat("en-US", { style: "currency", currency: "USD", currencyDisplay: "symbol", maximumFractionDigits: decimal }).format(money);

function UpdateProducts() {
    if (supplier != null && supplier != 0) {
        swal({
            title: `Desea Actualizar todos los productos`,
            text: "Una vez actualizados no habra forma de recuperar los precios anteriores.",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Continuar",
            cancelButtonText: "Cancelar"
        }, function (isConfirm) {
            if (isConfirm) {
                StartLoading();
                axios
                    .post("/purchaseOrder/ActionUpdatePurchaseOrder", { Supplier: supplier, purchaseNo: $("#orderSelect").val() })
                    .then(data => {
                        if (data.data == "SF") {
                            SessionFalse(sesion);
                        }
                        else if (data.data == 1) {
                            PurchasesList()
                            toastr.success("Actualizado con exito")
                        }
                        else if (data.data == 2) {
                            toastr.warning("Hubo un problema al actualizar todos los productos")
                        }
                        else if (data.data == 3) {
                            toastr.warning(`Su usuario ${User.Name} no cuenta con las caracteristicas para hacer esa accion`);
                        }
                        else {
                            toastr.error("Error desconosido contactar a sistemas")
                        }
                    })
                    .catch(error => { /*console.error(`%c${error}`, "color:blue");*/ toastr.error("Contacte a sistemas") })
                    .then(() => { EndLoading(); });
            }
        });
    }
    else {
        toastr.warning("Seleccione una opcion")
    }
}

function SaveAddProduct() {
    var quantity = !isNaN(parseFloat($("#inputQuantity").val())) ? parseFloat($("#inputQuantity").val()) : 0;
    var part_number = $("#ItemsDrop").val();
    if (quantity <= 0 || quantity >= 999999) {
        toastr.warning("Favor de colocar una cantidad valida")
    }
    else if (ListTable.filter(x => x.PartNumber == part_number).length > 0) {
        toastr.warning("El producto ya existe favor de colocar otro")
    }
    else if (part_number == "" || part_number == null) {
        toastr.warning("Seleccione una opcion valida")
    }
    else {
        StartLoading();
        axios
            .post("/PurchaseOrder/ActionPurchaseOrderAddPartNumber", { purchaseNo: $("#orderSelect").val(), PartNumber: $("#ItemsDrop").val(), Quantity: quantity, Supplier: supplier })
            .then(data => {
                if (data.data == "SF") {
                    SessionFalse(sesion);
                }
                else if (data.data == 1) {
                    PurchasesList();
                    $("#ItemsDrop").select2("val", "");
                    $("#inputQuantity").val(null);
                    toastr.success("Agregado correctamente")
                }
                else if (data.data == 2) {
                    toastr.warning("Ya fue agregada y esta activa")
                }
                else if (data.data == 3) {
                    toastr.warning(`Su usuario ${User.Name} no cuenta con las caracteristicas para hacer esa accion`);
                }
                else if (data.data == 4) {
                    toastr.error("Problema al registrar el producto")
                }
                else if (data.data == 5) {
                    PurchasesList();
                    $("#ItemsDrop").select2("val", "");
                    $("#inputQuantity").val(null);
                    toastr.success("Reactivado correctamente")
                }
                else if (data.data == 6) {
                    toastr.warning("Solo es posible agregar nuevos productos desde la pocket en el modo Recibo a Pie de Camión.")
                }
                else {
                    toastr.warning("Error con al agregar producto");
                }
            })
            .catch(error => {
                console.log(`%c${error.data}`, "color: red; font-size: 20px;");
            })
            .then(() => { EndLoading(); $("#modalAdd").modal("hide") })
    }
}

function AddInfoProduct() {
    axios
        .post("/Items/ActionCostInfo", { PartNumber: $("#ItemsDrop").val(), Supplier: supplier })
        .then(data => {
            if (data.data == "SF") {
                SessionFalse(sesion);
            }
            else if (data.data != null) {
                product = data.data
                $("#pPriceAdd").html(`${fomartmoney(data.data.BaseCost)(4)}`);
                $("#pIva").html(`${data.data.Iva}%`);
                $("#pIeps").html(`${data.data.Ieps}%`);
            }
            else {
                toastr.warning("No contiene datos el producto");
            }
        })
        .catch(error => {
            console.log(`%c${error.data}`, "color: red; font-size: 20px;");
        })
}

function WhatIsSupplier() {
    if ($("#orderSelect").val() != "0") {
        axios
            .post("/purchaseOrder/ActionSupplierPurchaseOrder", { purchaseNo: $("#orderSelect").val() })
            .then(data => {
                console.log(data)
                if (data.data == "SF") {
                    SessionFalse(sesion);
                }
                else if (data.data != 0 && data.data != 3) {
                    supplier = data.data;
                }
                else if (data.data == 3) {
                    //toastr.warning(`Sr/Srta ${User.Name} su usuario ${User.UserName} no cuenta con las caracteristicas para realizar las siguientes acciones.`);
                }
                else if (data.data == 0) {
                    toastr.warning(`No se encontro el proveedor`);
                }
                else {
                    toastr.error("Error desconosido contactar a sistemas")
                }
            })
            .catch(error => { /*console.error(`%c${error}`, "color:blue");*/ toastr.error("Contacte a sistemas") })
            .then(() => { $("#modalEdit").modal("hide"); EndLoading(); });
    }
    else {
        //toastr.warning("Seleccione una opcion")
    }
}

function AddProduct() {
    if (supplier != null && supplier != 0) {
        $("#modalAdd").modal("show")
    }
    else {
        toastr.warning("Seleccione una opcion")
    }
}

$("#cancelOrderButton").click(function () {
    purchaseValue = $("#orderSelect").val()
    if (ErrorListF(purchaseValue != "0", "Warning", "Selecciona una orden a cancelar")) {
        SwalMessage(
            {
                text: "¿Deseas cancelar la orden de compra?", type: "warning", color: "#DD6B55"
            },
            CancelPurchaseOrder,
            function () { },
            null
        )
    }

});

function CancelPurchaseOrder() {
    StartLoading();
    axios.get(`/PurchaseOrderItem/ActionRemovePurchaseOrder/?Purchase=${purchaseValue}`)
        .then(data => {
            if (data.data == "SF") {
                SessionFalse(sesion)
            }
            else if (data.data == 1) {
                ClearForm();
                toastr.success("Cancelada con exito.");
            }
            else if (data.data == 2) {
                toastr.warning("Error al cancelar la orden compra, contacte a sistemas.")
            }
            else {
                toastr.error("Error desconocido contacte a sistemas.")
            }
        })
        .catch(error => {
            toastr.error('Alerta - Error inesperado  contactar a sistemas.');
        })
        .then(() => { EndLoading(); })
}

$("#ItemsDrop").select2({
    minimumInputLength: 3,
    allowClear: true,
    placeholder: "Seleccione una producto",
    initSelection: function (element, callback) {
        callback({ id: "", text: "Seleccione una opcion" });
    },
    ajax: {
        url: "/Items/ActionSearchItemBarcodeSupplierCurrency/",
        dataType: 'json',
        type: "GET",
        quietMillis: 50,
        data: function (Filter) {
            return {
                Filter: Filter,
                Supplier: supplier,
                Currency: currencyGlobal
            };
        },
        results: function (data) {
            return {
                results: $.map(data.Json, function (item) {
                    return {
                        text: item.Description,
                        id: item.PathNumber
                    }
                }),
            };
        }
    }
});

var table = $("#purchasesTable").DataTable({
    "autoWidth": true,
    "paging": true,
    "searching": true,
    "info": true,
    responsive: true,
    dom: "<'row'<'col-sm-4'l><'col-sm-4 text-left'B><'col-sm-4'f>t<'col-sm-6'i><'col-sm-6'p>>",
    lengthMenu: [[10, 25, 50, -1], [10, 25, 50, "Todos"]],
    buttons: [],
    oLanguage:
    {
        "sProcessing": "Procesando...",
        "sLengthMenu": "Mostrar _MENU_ registros",
        "sZeroRecords": "No se encontraron resultados",
        "sEmptyTable": "Ningun dato disponible en esta tabla",
        "sInfo": "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
        "sInfoEmpty": "Mostrando registros del 0 al 0 de un total de 0 registros",
        "sInfoFiltered": "(filtrado de un total de _MAX_ registros)",
        "sInfoPostFix": "",
        "sSearch": "Buscar:",
        "sUrl": "",
        "sInfoThousands": ",",
        "sLoadingRecords": "Cargando...",
        "oPaginate": { "sFirst": "Primero", "sLast": "�ltimo", "sNext": "Siguiente", "sPrevious": "Anterior" },
        "oAria":
        {
            "sSortAscending": ": Activar para ordenar la columna de manera ascendente",
            "sSortDescending": ": Activar para ordenar la columna de manera descendente"
        }
    },
    columns:
        [
            { data: null },
            { data: 'PartNumber' },
            { data: 'PartDescription' },
            { data: 'Quantity' },
            { data: 'UnitSize' },
            { data: 'PurchasePrice' },
            { data: 'Iva' },
            { data: 'Ieps' },
            { data: 'ItemAmount' },
            { data: 'IvaTotal' },
            { data: 'IepsTotal' },
            { data: 'ItemTotalAmount' },
            { data: 'BaseCost' },
            { data: null },
        ],
    columnDefs:
        [
            {
                targets: [13],
                render: function (data, type, full, meta) {
                    return `<button data-toggle="tooltip" onclick="EditModal('${full.PartNumber}',${full.PurchasePrice},${full.BaseCost},${full.Quantity} )" title="Editar" class="btn btn-sm btn-warning" id="remove-button" > <i class="fa fa-pencil"></i> Editar</button>`;
                },
                orderable: false,
                width: "1%"
            },
            {
                targets: [0],
                render: function (data, type, full, meta) {
                    return `<button data-toggle="tooltip" onclick="DeleteProduct('${full.PartNumber}')" title="Borrar" class="btn btn-sm btn-danger" id="remove-button" > <i class="fa fa-close"></i> Borrar</button>`;
                },
                orderable: false,
                width: "1%"
            },
            {
                data: 'purchasePrice',
                type: 'numeric-comma',
                render: function (data, type, row) { return `${fomartmoney(parseFloat(data))(4)}`; },
                targets: [5, 8, 9, 10, 11, 12]
            },
            {
                targets: [12],
                visible: false
            },
            {
                data: 'iva',
                render: function (data, type, row) { return data + ' %'; },
                targets: [6, 7]
            },
            {
                targets: [4],
                render: function (data, type, full, meta) {
                    return `${data != "" && data != null ? data : "N/A"}`;
                },
                orderable: false,
                width: "1%"
            },
        ]
});

function AddPurchaseTable(data) {
    table.clear().draw();
    table.rows.add(data);
    table.columns.adjust().draw();
}

function EditModal(partNumber, price, baseCost , quantity) {
    $("#modalEdit").modal("show");
    $("#pPartNumber").html(partNumber);
    product.partNumber = partNumber;
    product.baseCost = baseCost;
    $("#inputPrice").val(price);
    $("#inputEditQuantity").val(quantity);
    $("#pPrice").html("$" + baseCost);
}

function DeleteProduct(partNumber) {
    var purchaseNo = $("#orderSelect").val();
    if (ListTable.length > 1) {
        swal({
            title: `¿Desea borrar el producto ${partNumber}?`,
            text: "Se borrará el producto.",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Continuar",
            cancelButtonText: "Cancelar"
        }, function (isConfirm) {
            if (isConfirm) {
                ReloadTotalPurchase(false);
                StartLoading();
                axios
                    .post("/purchaseOrder/ActionDeleteProductPurchaseOrder", { partNumber: partNumber, purchaseNo: purchaseNo })
                    .then(data => {
                        if (data.data == "SF") {
                            SessionFalse(sesion);
                        }
                        else if (data.data == 1) {
                            ListTable = ListTable.filter(x => x.PartNumber != partNumber)
                            AddPurchaseTable(ListTable);
                            ReloadTotalPurchase(true);
                            toastr.success("Guardado con exito")

                        }
                        else if (data.data == 2) {
                            toastr.warning("Hubo un problema al borrar el producto")
                        }
                        else if (data.data == 3) {
                            toastr.warning(`Su usuario ${User.Name} no cuenta con las caracteristicas para hacer esa accion`);
                        } else if (data.data == 6) {
                            toastr.warning("Solo es posible ELIMINAR los productos desde la pocket cuando la orden es Pie de Camión.")
                        }
                        else {
                            toastr.error("Error desconosido contactar a sistemas")
                        }
                    })
                    .catch(error => { /*console.error(`%c${error}`, "color:blue");*/ toastr.error("Contacte a sistemas") })
                    .then(() => { $("#modalEdit").modal("hide"); EndLoading(); });
            }
        });
    }
    else
        toastr.warning("No puede borrar todos los productos")
}

function EditOrderPartNumber() {
    var partnumber = product.partNumber
    var price = $("#inputPrice").val();
    var editQuantity = $("#inputEditQuantity").val();
    var purchaseNo = $("#orderSelect").val();
    if (PriceValidation(price <= 0, "Ingrese un valor positivo") || PriceValidation(!isDecimal(parseFloat(price))(4), "Ingrese un numero con maximo 4 decimales")) { }
    else if (User.BuyerDivicion == "N/A" && PriceValidation(price > product.baseCost, `Ingrese un valor menor al precio base`)) { }
    else {
        ReloadTotalPurchase(false);
        StartLoading();
        axios
            .post("/purchaseOrder/ActionEditPurchaseOrder", { partNumber: partnumber, price: price, purchaseNo: purchaseNo  })
            .then(data => {
                if (data.data == "SF") {
                    SessionFalse(sesion);
                }
                else if (data.data.Item2 == 0) {
                    ListTable[ListTable.findIndex(x => x.PartNumber == partnumber)] = data.data.Item1;
                    AddPurchaseTable(ListTable);
                    ReloadTotalPurchase(true);
                    $("#ItemsDrop").select2("val", "");
                    $("#inputQuantity").val(null);
                    toastr.success("Guardado con exito")
                }
                else if (data.data.Item2 == 1) {
                    toastr.error("Hubo un problema al editar el precio del producto")
                }
                else if (data.data.Item2 == 2) {
                    toastr.warning("Hubo un problema al editar el producto")
                }
                else if (data.data.Item2 == 4) {
                    toastr.warning(`Su usuario ${User.Name} no cuenta con las caracteristicas para hacer esa accion`);
                }
                else {
                    toastr.error("Error desconosido contactar a sistemas")
                }
            })
            .catch(error => { console.error(`%c${error}`, "color:blue"); toastr.error("Contacte a sistemas") })
            .then(() => { $("#modalEdit").modal("hide"); EndLoading(); });
    }
}
$("#orderSelect").change(function () {
    AddPurchaseTable([])
    var purchaseOrder = $("#orderSelect").val();
    ReloadTotalPurchase(false)
    if (purchaseOrder != "0") {
        StartLoading();
        PurchasesList();
    }
});

var currencyGlobal = "";
function PurchasesList() {
    axios.get("/Purchases/ActionGetPurchaseDetailMod/?poNumber=" + $("#orderSelect").val())
        .then(function (data) {
            if (data.data == "SF") {
                SessionFalse(sesion);
            }
            else if (data.data.length > 0) {
                ListTable = data.data;
                ReloadTotalPurchase(true);
                AddPurchaseTable(data.data);
                $("#currency").html(data.data[0].Currency);
                currencyGlobal = data.data[0].Currency;
                $("#cancelOrderButtonDiv").show();
            }
            else {
                toastr.warning('La orden de compra se encontro vacia');
            }
        })
        .catch(error => { console.error(`%c${error}`, "color:blue"); toastr.error("Contacte a sistemas"); })
        .then(() => { EndLoading(); })
}

function ReloadTotalPurchase(value) {
    if (value) {
        $("#importeTotalTable").html(fomartmoney(Sum(ListTable)("ItemTotalAmount"))(4));
        $("#iepsTotalTable").html(fomartmoney(Sum(ListTable)("IepsTotal"))(4));
        $("#ivaTotalTable").html(fomartmoney(Sum(ListTable)("IvaTotal"))(4));
        $("#subTotalTable").html(fomartmoney(Sum(ListTable)("ItemAmount"))(4));
    }
    else {
        $("#importeTotalTable").html("$0")
        $("#iepsTotalTable").html("$0");
        $("#ivaTotalTable").html("$0");
        $("#subTotalTable").html("$0");
    }
}

$("#inputPrice").keyup(() => {
    const price = parseFloat($("#inputPrice").val());
    PriceValidation(price <= 0 && !isNaN(price), "Ingrese un valor positivo")
    PriceValidation(!isDecimal(price)(4) && !isNaN(price), "Ingrese un numero con maximo 4 decimales")
})

function PriceValidation(value, text) {
    if (value) {
        toastr.warning(text);
        $("#inputPrice").val("")
        return true;
    }
    return false;
}

$(document).ready(function () {
    $("#orderSelect").select2();
    AddPurchaseTable([]);
    User = JSON.parse($("#model").val());
});

function ClearForm() {
    ComboboxOreders();
    $('#orderSelect').val($("#orderSelect :first").val()).trigger('change');
    $("#cancelOrderButtonDiv").hide();
    AddPurchaseTable([]);
    supplier = 0;
}


function SwalMessage(optionSwal, funSucces, funcError, parmas) {
    swal({
        title: optionSwal.text,
        type: optionSwal.type,
        text: optionSwal.textOptional,
        showCancelButton: true,
        confirmButtonColor: optionSwal.color,
        confirmButtonText: "Continuar",
        cancelButtonText: "Cancelar"
    },
        function (isConfirm) {
            if (isConfirm) {
                funSucces(parmas);
            }
            else {
                funcError(parmas);
            }
        });
}

function ErrorListF(value, type, text, typeS, textSucces) {
    return !value ? ToastError(type, text) : typeS != null && typeS != "" ? ToastError(typeS, textSucces, true) : true;
}
function ToastError(type, text, value) {
    if (type == "Success") {
        toastr.success(text)
        return true;
    }
    else if (type == "Error") {
        toastr.error(text)
    }
    else if (type == "Warning") {
        toastr.warning(text)
    }
    return value != undefined || value != null ? value : false;
}