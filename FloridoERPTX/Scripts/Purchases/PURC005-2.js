﻿function tab1() {
    $('#tab-1').animatePanel();
};

function tab2() {
    $('#tab-2').animatePanel();
};

$("#ValidDate1").datepicker({
    autoclose: true,
    todayHighlight: true,
    showButtonPanel: true,
}).on("changeDate", function (e) {
    SearchTransfer();
});

$("#ValidDate2").datepicker({
    autoclose: true,
    todayHighlight: true,
}).on("changeDate", function (e) {
    SearchTransfer();
});

var items;
//Obtencion de datos del viewModel
$(document).ready(function () {
    window.setTimeout(function () { $("#fa").click(); }, 1000);
    getItemsCreditors();
    getListRequirements();
});

listDefault = [];
function getListRequirements() {
    $.ajax({
        url: "/InternalRequirement/ActionGetAllRequirementsHeaderBySite",
        data: {},
        type: "POST",
        success: function (data) {
            if (data == "SF") {
                SessionFalse("Terminó tu sesión");
            } else if (data == "error") {
                toastr.error("Error inesperado contactar a sistemas.");
            } else if (data.length == 0) {
                toastr.warning("No existen requisiciones en el rango de fecha");
            } else {
                listDefault = data;
                tableReq.clear();
                tableReq.rows.add(data);
                tableReq.columns.adjust().draw();
            }
        },
        error: function () {
            toastr.error('Error inesperado contactar a sistemas.');
            EndLoading();
        }
    });
}

dataList = []
function getItemsCreditors() {
    $.ajax({
        url: "/InternalRequirement/ActionGetAllItemsCreditors",
        data: {},
        type: "POST",
        success: function (data) {
            if (data == "SF") {
                SessionFalse("Terminó tu sesión");
            } else if (data == "error") {
                toastr.error("Error inesperado contactar a sistemas.");
            } else {
                dataList = data;
                table.clear();
                table.rows.add(data);
                table.columns.adjust().draw();
            }
        },
        error: function () {
            toastr.error('Error inesperado contactar a sistemas.');
            EndLoading();
        }
    });
}

//Buscar folio por fecha
function SearchTransfer() {
    if ($('#ValidDate1').val() != "" & $('#ValidDate2').val() != "") {
        if (moment($('#ValidDate1').val()) <= moment($('#ValidDate2').val())) {
            StartLoading();
            $.ajax({
                type: "POST",
                url: "/InternalRequirement/ActionGetAllRequirementsHeaderBySiteByDate",
                data: { "DateInit": $('#ValidDate1').val(), "DateFin": $('#ValidDate2').val() },
                success: function (data) {
                    if (data == "SF") {
                        EndLoading();
                        SessionFalse("Terminó tu sesión");
                    } else if (data == "error") {
                        EndLoading();
                        toastr.error("Error inesperado contactar a sistemas.");
                    } else if (data.length == 0) {
                        EndLoading();
                        $('#TableReqDiv').animatePanel();
                        tableReq.clear();
                        tableReq.rows.add(data);
                        tableReq.columns.adjust().draw();
                        toastr.warning("No existen requisiciones en el rango de fecha");
                    }
                    else {
                        EndLoading();
                        $('#TableReqDiv').animatePanel();
                        tableReq.clear();
                        tableReq.rows.add(data);
                        tableReq.columns.adjust().draw();
                    }
                },
                error: function (returndate) {
                    toastr.error('Alerta - Error inesperado contactar a sistemas.');
                }
            });
        }
        else {
            toastr.error("Fecha final no puede ser menor a la inicial.");
            document.getElementById('ValidDate1').value = '';
            document.getElementById('ValidDate2').value = '';
            $('#ValidDate1').focus();
        }
    }
}

// Tabla Principal
var table = $('#tableItems').DataTable({
    oLanguage:
    {
        "sProcessing": "Procesando...",
        "sLengthMenu": "Mostrar _MENU_ registros",
        "sZeroRecords": "No se encontraron resultados",
        "sEmptyTable": "Ningún dato disponible en esta tabla",
        "sInfo": "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
        "sInfoEmpty": "Mostrando registros del 0 al 0 de un total de 0 registros",
        "sInfoFiltered": "(filtrado de un total de _MAX_ registros)",
        "sInfoPostFix": "",
        "sSearch": "Buscar:",
        "sUrl": "",
        "sInfoThousands": ",",
        "sLoadingRecords": "Cargando...",
        "oPaginate": { "sFirst": "Primero", "sLast": "Último", "sNext": "Siguiente", "sPrevious": "Anterior" },
        "oAria": { "sSortAscending": ": Activar para ordenar la columna de manera ascendente", "sSortDescending": ": Activar para ordenar la columna de manera descendente" }
    },
    lengthMenu: [[10, 25, 50, -1], [10, 25, 50, "Todos"]],
    columns:
        [
            { data: 'id_item_supplier' },
            { data: 'part_number' },
            { data: 'description' },
            { data: 'category' },
            { data: 'supplier_name' },
            { data: 'price' },
            { data: 'buyer_division', visible: false },
            { data: null }
        ],
    columnDefs:
        [{
            targets: [1, 2, 3, 4, 5, 6, 7],
            width: "1%"
        },
        {
            render: function (data, type, row) {
                return '$' + data;
            },
            targets: [5]
        },
        {
            targets: [7],
            render: function (data, type, full, meta) {
                return `<input class="form-control" id="${data.id_item_supplier}" onkeypress="return event.charCode >= 48 && event.charCode <= 57 || event.charCode == 13  " min="0" onchange= "get('${data.id_item_supplier}')" value = "${data.value}" style="width:80px"></input>`;
            }
        }]

});

//Ocultar el Ids
table.column(0).visible(false);

// Tabla Principal
var table2 = $('#tableItems2').DataTable({
    oLanguage:
    {
        "sProcessing": "Procesando...",
        "sLengthMenu": "Mostrar _MENU_ registros",
        "sZeroRecords": "No se encontraron resultados",
        "sEmptyTable": "Ningún dato disponible en esta tabla",
        "sInfo": "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
        "sInfoEmpty": "Mostrando registros del 0 al 0 de un total de 0 registros",
        "sInfoFiltered": "(filtrado de un total de _MAX_ registros)",
        "sInfoPostFix": "",
        "sSearch": "Buscar:",
        "sUrl": "",
        "sInfoThousands": ",",
        "sLoadingRecords": "Cargando...",
        "oPaginate": { "sFirst": "Primero", "sLast": "Último", "sNext": "Siguiente", "sPrevious": "Anterior" },
        "oAria": { "sSortAscending": ": Activar para ordenar la columna de manera ascendente", "sSortDescending": ": Activar para ordenar la columna de manera descendente" }
    },
    lengthMenu: [[10, 25, 50, -1], [10, 25, 50, "Todos"]],
    columns:
        [
            { data: 'id_item_supplier' },
            { data: 'part_number' },
            { data: 'description' },
            { data: 'category' },
            { data: 'supplier_name' },
            { data: 'price' },
            { data: null },
            { data: null },
            { data: 'buyer_division', visible: false }

        ],
    columnDefs:
        [{
            targets: [1, 2, 3, 4, 5, 6, 7, 8],
            width: "1%"
        },
        {
            render: function (data, type, row) {
                return '$' + data;
            },
            targets: [5]
        },
        {
            targets: [6],
            render: function (data, type, full, meta) {
                return `<input class="form-control" id="selected${data.id_item_supplier}" onkeypress="return event.charCode >= 48 && event.charCode <= 57 || event.charCode == 13  " min="0" onchange= "set('${data.id_item_supplier}')" value = "${data.quantity}" style="width:80px"></input>`;
            }
        },
        {
            targets: [7],
            render: function (data, type, full, meta) {
                return `<button class="btn btn-xs btn-danger btn-circle" style="text-align:center;border-right-width: 0px;padding-right: 0px;padding-left: 0px;padding-top: 0px;padding-bottom: 0px;margin-top: 5px;" onclick="deleteItem(${data.id_item_supplier})"><i class="fa fa-times"></i></button>`;
            }
        }
        ]
});

//Ocultar el Ids
table2.column(0).visible(false);

list = [];
items = 0;
function get(id_item_supplier) {
    var quantity = $("#" + id_item_supplier + "").val();
    var listToSend = dataList.filter(elements => elements.id_item_supplier == id_item_supplier)[0];
    if ($("#" + id_item_supplier + "").val() >= 999999999) {
        $("#" + id_item_supplier + "").val(0);
        toastr.warning("Favor de ingresar un numero valido")
        quantity = 0
    }
    if ($("#" + id_item_supplier + "").val() > 0) {

        var index = list.findIndex(elements => elements.id_item_supplier == id_item_supplier);
        if (index >= 0) {
            dataList.filter(x => x.id_item_supplier == id_item_supplier)[0].value = quantity;
            list[index] = { part_number: listToSend.part_number, description: listToSend.description, category: listToSend.category, supplier_name: listToSend.supplier_name, price: listToSend.price, id_item_supplier: id_item_supplier, quantity: quantity, buyer_division: listToSend.buyer_division };
        }
        else {
            dataList.filter(x => x.id_item_supplier == id_item_supplier)[0].value = quantity;
            list.push({ part_number: listToSend.part_number, description: listToSend.description, category: listToSend.category, supplier_name: listToSend.supplier_name, price: listToSend.price, id_item_supplier: id_item_supplier, quantity: quantity, buyer_division: listToSend.buyer_division });
        }

    } else {
        list = list.filter(x => x.id_item_supplier != id_item_supplier);
        dataList.filter(x => x.id_item_supplier == id_item_supplier)[0].value = 0;
    }
    table2.clear();
    table2.rows.add(list);
    table2.columns.adjust().draw();
    table.clear();
    table.rows.add(dataList);
    table.columns.adjust().draw();
}

function set(id_item_supplier) {
    var listToSend = dataList.filter(elements => elements.id_item_supplier == id_item_supplier)[0];
    var quantity = $("#selected" + id_item_supplier + "").val();

    if ($("#selected" + id_item_supplier + "").val() > 0) {
        var index = list.findIndex(elements => elements.id_item_supplier == id_item_supplier);
        if (index >= 0) {
            dataList.filter(x => x.id_item_supplier == id_item_supplier)[0].value = quantity;
            list[index] = { part_number: listToSend.part_number, description: listToSend.description, category: listToSend.category, supplier_name: listToSend.supplier_name, price: listToSend.price, id_item_supplier: id_item_supplier, quantity: quantity, buyer_division: listToSend.buyer_division };
        }
        else {
            dataList.filter(x => x.id_item_supplier == id_item_supplier)[0].value = quantity;
            list.push({ part_number: listToSend.part_number, description: listToSend.description, category: listToSend.category, supplier_name: listToSend.supplier_name, price: listToSend.price, id_item_supplier: id_item_supplier, quantity: quantity, buyer_division: listToSend.buyer_division });
        }

    } else {
        list = list.filter(x => x.id_item_supplier != id_item_supplier);
        dataList.filter(x => x.id_item_supplier == id_item_supplier)[0].value = 0;
    }
    dataList.filter(x => x.id_item_supplier == id_item_supplier)[0].value = quantity;
    table.clear();
    table.rows.add(dataList);
    table.columns.adjust().draw();
    table2.clear();
    table2.rows.add(list);
    table2.columns.adjust().draw();

    $("#" + id_item_supplier + "").val(quantity);
    if ($("#selected" + id_item_supplier + "").val() >= 999999999) {
        $("#selected" + id_item_supplier + "").val(0)
        $("#" + id_item_supplier + "").val(0);
        toastr.warning("Favor de ingresar un numero valido")
    }
}

function deleteItem(id_item_supplier) {
    list = list.filter(x => x.id_item_supplier != id_item_supplier);
    dataList.filter(x => x.id_item_supplier == id_item_supplier)[0].value = 0;
    table2.clear();
    table2.rows.add(list);
    table2.columns.adjust().draw();
    table.clear();
    table.rows.add(dataList);
    table.columns.adjust().draw();
}

function save() {
    $('#items').hide();
    $('#selected').show();
    StartLoading();
}

function aprovalSave() {
    StartLoading();
    $.ajax({
        url: "/InternalRequirement/ActionAddInternalRequirement",
        type: "POST",
        data: { "items": list },
        success: function (returndata) {
            EndLoading();
            if (returndata == "SF") {
                SessionFalse("Terminó tu sesión");
            } else if (returndata != 0) {
                $("#tabs").tabs({
                    active: 0
                });
                $("#tab1").tab('show');
                tab1();
                list = [];
                getItemsCreditors();
                table2.clear();
                table2.rows.add(list);
                table2.columns.adjust().draw();
                getListRequirements();
                swal({
                    title: 'Requisición Guardada',
                    text: "Requisición guardada con éxito con el folio : " + returndata + ".",
                    type: "success"
                }, function (isConfirm) {
                    if (isConfirm) {
                    }
                });
            }
            else
                toastr.error('Error inesperado contactar a sistemas.');
        },
        error: function () {
            EndLoading();
            toastr.error('Error inesperado contactar a sistemas.');
        }
    });
}

//Tabla principal
var tableReq = $('#TableRequirements').DataTable({
    oLanguage: {
        "sProcessing": "Procesando...", "sLengthMenu": "Mostrar _MENU_ registros", "sZeroRecords": "No se encontraron resultados", "sEmptyTable": "Ningún dato disponible en esta tabla", "sInfo": "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros", "sInfoEmpty": "Mostrando registros del 0 al 0 de un total de 0 registros", "sInfoFiltered": "(filtrado de un total de _MAX_ registros)", "sInfoPostFix": "", "sSearch": "Buscar:", "sUrl": "", "sInfoThousands": ",", "sLoadingRecords": "Cargando...", "oPaginate": { "sFirst": "Primero", "sLast": "Último", "sNext": "Siguiente", "sPrevious": "Anterior" }, "oAria": { "sSortAscending": ": Activar para ordenar la columna de manera ascendente", "sSortDescending": ": Activar para ordenar la columna de manera descendente" }
    },
    order: [[1, "asc"]],
    dom: "<'row'<'col-sm-4'l><'col-sm-4 text-left'B><'col-sm-4'f>t<'col-sm-6'i><'col-sm-6'p>>",
    lengthMenu: [[10, 25, 50, -1], [10, 25, 50, "Todos"]],
    buttons: [],
    columns: [
        {
            "class": "details-control",
            "orderable": false,
            "data": null,
            "defaultContent": "",
        },
        { data: 'folio' },
        { data: 'cdate' },
        { data: 'requirement_status' },
        { data: 'cuser' }
        ,
    ],
    columnDefs: [
        {
            targets: [0, 1, 2, 3, 4],
            width: "1%"
        }
    ]
});
var detailRows = [];

$('#TableRequirements tbody').on('click', 'tr td.details-control', function () {
    var tr = $(this).closest('tr');
    var row = tableReq.row(tr);

    if (row.child.isShown()) {
        row.child.hide();
        tr.removeClass('details');
    }
    else {
        if (tableReq.row('.details').length) {
            $('.details-control', tableReq.row('.details').node()).click();
        }
        row.child(format(row.data())).show();
        tr.addClass('details');
    }
});

table.on('draw', function () {
    $.each(detailRows, function (i, id) {
        $('#' + id + ' td.details-control').trigger('click');
    });
});

//Tabla detalles
function format(d) {
    var detailItems = "";
    var tabledetail = $('<div>').addClass('loading').text('Cargando Datos...');
    $.ajax({
        url: "/InternalRequirement/ActionGetDetailsRequirement",
        data: { "folio": d.folio },
        type: "POST",
        success: function (data) {
            if (data == "SF") {
                SessionFalse("Terminó tu sesión");
            } else if (data == "error") {
                toastr.error("Error inesperado contactar a sistemas");
            }
            else {
                $.each(data, function (index, value) {
                    detailItems += "<tr><td>" + value.part_number + "</td><td>" + value.part_description + "</td><td>" + value.quantity + "</td><td>$" + value.price + "</td><td>" + value.supplier_name + "</td>";
                });
                var TheOne = new Date().getTime();
                if (data.length != 0) {
                    tabledetail.html('<table id="tabledetail' + TheOne + '" class="table table-striped table-bordered table-hover" style="width:100%"><thead><tr><th>Codigo</th><th>Descripcion</th><th>Cantidad</th><th>Precio Estimado</th><th>Proveedor</th></tr></thead><tbody>' + detailItems + '</tbody></table>').removeClass('loading');
                    reloadStyleTable(TheOne);
                }
                tabledetail.html('<table id="tabledetail' + TheOne + '" class="table table-striped table-bordered table-hover" style="width:100%"><thead><tr><th>Codigo</th><th>Descripcion</th><th>Cantidad</th><th>Precio Estimado</th><th>Proveedor</th></thead><tbody>' + detailItems + '</tbody></table>').removeClass('loading');
                reloadStyleTable(TheOne);
            }
        }
    });
    return tabledetail
}

function viewRequirements() {
    var site = $("#site").val();
    $.ajax({
        url: "/InternalRequirementHeader/ActionGetAllRequirementsHeaderBySite",
        data: { "site": site },
        type: "POST",
        success: function (data) {
            if (data == "SF") {
                SessionFalse("Terminó tu sesión");
            } else {
                if (data.length > 0) {
                    $("#TableReqDiv").show();
                    tableReq.clear();
                    tableReq.rows.add(data);
                    tableReq.columns.adjust().draw();
                } else {
                    toastr.error("No existen requisiciones en la tienda");
                    $("#TableReqDiv").hide();
                }
            }
        }
    });
}

function reloadStyleTable(TheOne) {
    $('#tabledetail' + TheOne).DataTable({
        oLanguage: {
            "sProcessing": "Procesando...", "sLengthMenu": "Mostrar _MENU_ registros", "sZeroRecords": "No se encontraron resultados", "sEmptyTable": "Ningún dato disponible en esta tabla", "sInfo": "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros", "sInfoEmpty": "Mostrando registros del 0 al 0 de un total de 0 registros", "sInfoFiltered": "(filtrado de un total de _MAX_ registros)", "sInfoPostFix": "", "sSearch": "Buscar:", "sUrl": "", "sInfoThousands": ",", "sLoadingRecords": "Cargando...", "oPaginate": { "sFirst": "Primero", "sLast": "Último", "sNext": "Siguiente", "sPrevious": "Anterior" }, "oAria": { "sSortAscending": ": Activar para ordenar la columna de manera ascendente", "sSortDescending": ": Activar para ordenar la columna de manera descendente" }
        }
    });
}

function save() {
    if (list.length > 0) {
        swal({
            title: "¿Desea guardar la requisición?",
            text: "La requisición será guardada",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Continuar",
            cancelButtonText: "Cancelar"
        }, function (isConfirm) {
            if (isConfirm) { setTimeout(function () { aprovalSave(); }, 500); }
        });
    } else {
        swal({
            title: 'No ha seleccionado ningun producto',
            text: "Seleccione por lo menos 1 producto para requisición",
            type: "error"
        });
    }
}