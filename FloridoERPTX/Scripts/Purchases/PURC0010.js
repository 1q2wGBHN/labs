﻿$(document).ready(function () {
    window.setTimeout(function () { $("#fa").click(); }, 1000);
    $("#supplier").select2();
    $("#ItemsDrop").select2();
    $("#typeSelect").select2();
    var itemsModel = JSON.parse($("#ItemsList").val());
    var Promotions = JSON.parse($("#Promotions").val());

    $("#ItemsDrop").select2({
        minimumInputLength: 2,
        formatInputTooShort: function () {
            return "Escriba un producto.";
        },
        matcher: function (term, text, option) {
            return text.toUpperCase().indexOf(term.toUpperCase()) >= 0 || option.val().toUpperCase().indexOf(term.toUpperCase()) >= 0;
        }
    });

    if (!$.isEmptyObject(Promotions)) {
        $('#TableDiv').animatePanel();
        table.clear();
        table.rows.add($(Promotions));
        table.columns.adjust().draw();
        $("#Print").removeAttr('disabled', 'disabled');
    }
    else {
        $('#TableDiv').animatePanel();
        table.clear();
        table.columns.adjust().draw();
        $("#Print").attr('disabled', true);
    }
});

// Create our number formatter.
var formatter = new Intl.NumberFormat('en-US', {
    style: 'currency',
    currency: 'USD',
});

var table = $('#TablePromotions').DataTable({
    "autoWidth": true,
    oLanguage: {
        "sProcessing": "Procesando...", "sLengthMenu": "Mostrar _MENU_ registros", "sZeroRecords": "No se encontraron resultados", "sEmptyTable": "NingÃºn dato disponible en esta tabla", "sInfo": "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros", "sInfoEmpty": "Mostrando registros del 0 al 0 de un total de 0 registros", "sInfoFiltered": "(filtrado de un total de _MAX_ registros)", "sInfoPostFix": "", "sSearch": "Buscar:", "sUrl": "", "sInfoThousands": ",", "sLoadingRecords": "Cargando...", "oPaginate": { "sFirst": "Primero", "sLast": "Ãltimo", "sNext": "Siguiente", "sPrevious": "Anterior" }, "oAria": { "sSortAscending": ": Activar para ordenar la columna de manera ascendente", "sSortDescending": ": Activar para ordenar la columna de manera descendente" }
    },
    dom: "<'row'<'col-sm-4'l><'col-sm-4 text-left'B><'col-sm-4'f>t<'col-sm-6'i><'col-sm-6'p>>",
    lengthMenu: [[10, 25, 50, -1], [10, 25, 50, "Todos"]],
    buttons: [
        { extend: 'csv', text: 'Excel', title: 'Gastos de Tienda', className: 'btn-sm' },
        {
            extend: 'pdf', text: 'PDF', title: 'Gastos de Tienda', className: 'btn-sm', exportOptions: {
                columns: [0, 1, 2, 3, 4, 5, 6]
            },
            customize: function (doc) {
                doc.content[1].table.widths =
                    Array(doc.content[1].table.body[0].length + 1).join('*').split('');
            }
        },
        { extend: 'print', text: 'Imprimir', className: 'btn-sm' },
    ],
    columns: [
        {
            "class": "details-control",
            "orderable": false,
            "data": null,
            "defaultContent": "",
        },
        { targets: 1, data: 'promotion_code' },
        { targets: 2, data: 'promotion_description' },
        { targets: 3, data: 'promotion_type' },
        { targets: 4, data: 'promotion_start_date' },
        { targets: 5, data: 'promotion_end_date' }
    ],
    columnDefs: [{
        targets: [0, 1, 2, 3, 4, 5],
        width: "1%"
    },
    {
        targets: [2],
        render: function (data, type, row) {
            if (data != '')
                return data;
            else
                return 'Sin descripcion.';
        }
    },
    {
        targets: [4],
        render: function (data, type, row) {
            var date = moment(data).format('DD/MM/YYYY');
            if (date != 'Invalid date')
                return date;
            else
                return "";
        }
    },
    {
        targets: [5],
        render: function (data, type, row) {
            var date = moment(data).format('DD/MM/YYYY');
            if (date != 'Invalid date')
                return date;
            else
                return "";
        }
    }]
});

var tableCommons = $('#TableCommons').DataTable({
    "autoWidth": true,
    oLanguage: {
        "sProcessing": "Procesando...", "sLengthMenu": "Mostrar _MENU_ registros", "sZeroRecords": "No se encontraron resultados", "sEmptyTable": "NingÃºn dato disponible en esta tabla", "sInfo": "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros", "sInfoEmpty": "Mostrando registros del 0 al 0 de un total de 0 registros", "sInfoFiltered": "(filtrado de un total de _MAX_ registros)", "sInfoPostFix": "", "sSearch": "Buscar:", "sUrl": "", "sInfoThousands": ",", "sLoadingRecords": "Cargando...", "oPaginate": { "sFirst": "Primero", "sLast": "Ãltimo", "sNext": "Siguiente", "sPrevious": "Anterior" }, "oAria": { "sSortAscending": ": Activar para ordenar la columna de manera ascendente", "sSortDescending": ": Activar para ordenar la columna de manera descendente" }
    },
    dom: "<'row'<'col-sm-4'l><'col-sm-4 text-left'B><'col-sm-4'f>t<'col-sm-6'i><'col-sm-6'p>>",
    lengthMenu: [[10, 25, 50, -1], [10, 25, 50, "Todos"]],
    buttons: [
        { extend: 'csv', text: 'Excel', title: 'Gastos de Tienda', className: 'btn-sm' },
        {
            extend: 'pdf', text: 'PDF', title: 'Gastos de Tienda', className: 'btn-sm', exportOptions: {
                columns: [0, 1, 2, 3, 4, 5, 6]
            },
            customize: function (doc) {
                doc.content[1].table.widths =
                    Array(doc.content[1].table.body[0].length + 1).join('*').split('');
            }
        },
        { extend: 'print', text: 'Imprimir', className: 'btn-sm' },
    ],
    columns: [
        { targets: 1, data: 'part_number' },
        { targets: 2, data: 'description' },
        { targets: 3, data: 'price' },

    ],
    columnDefs: [{
        targets: [0, 1, 2],
        width: "1%"
    }]
});

function ShowPromotionCommonItem(promotion_code, common, price_promo, quantity_promo, descrip_part_number) {
    $.ajax({
        type: "GET",
        url: "/Promotion/ActionGetPromotionCommon",
        data: { "promotion_code": promotion_code, "common": common, },
        success: function (response) {
            if (response.status) {
                $('#folioOferta').html(promotion_code);
                $('#descriptionOferta').html(common);
                $('#tipoOferta').html(descrip_part_number);
                $('#precioOferta').html("$ " + price_promo);
                $('#cantidadOFerta').html(quantity_promo);
                tableCommons.clear();
                tableCommons.rows.add(response.model);
                tableCommons.columns.adjust().draw();
                $('#showStores').modal('show');
            } else {
                SessionFalse("Termino su sesión");
            }
        },
        error: function (response) {
            toastr.error('Error inesperado contactar a sistemas.' + response + '');
        }
    });
}

var detailRows = [];

$('#TablePromotions tbody').on('click', 'tr td.details-control', function () {
    var tr = $(this).closest('tr');
    var row = table.row(tr);
    if (row.child.isShown()) {
        row.child.hide();
        tr.removeClass('details');
    }
    else {
        if (table.row('.details').length) {
            $('.details-control', table.row('.details').node()).click();
        }
        row.child(formatOne(row.data())).show();
        tr.addClass('details');
    }
});

table.on('draw', function () {
    $.each(detailRows, function (i, id) {
        $('#' + id + ' td.details-control').trigger('click');
    });
});

function formatOne(d) {
    var detailItems = "";
    var tabledetail = $('<div/>').addClass('loading').text('Cargando Datos...');
    $.ajax({
        url: "/Promotion/ActionGetPromotionById",
        data: { "FolioPromotion": d.promotion_code, "type": d.promotion_type },
        type: "GET",
        success: function (Data) {
            $.each(Data, function (index, value) {
                if (d.promotion_type == "AVANZADA") {
                    if (value.common == 0) {
                        detailItems += "<tr><td>" + value.part_number + "</td><td>" + value.description + "</td><td>" + value.quantity + "</td><td>" + formatter.format(value.price_original) + "</td><td>" + formatter.format(value.price) + "</td><td><button class='btn btn-xs btn-primary' onclick='MessageNoProducts()'><i class='fa fa-cubes'></i> Productos</button></td>";
                    } else {
                        detailItems += `<tr><td> ${value.part_number}</td><td>${value.description}</td><td>${value.quantity}</td><td>${formatter.format(value.price_original)}</td><td>${formatter.format(value.price)}</td><td><button class="btn btn-xs btn-success" onclick="ShowPromotionCommonItem('${value.promotion_code}', '${value.part_number}', '${value.price}', '${value.quantity}', '${value.description}')"><i class="fa fa-cubes"></i> Productos</button></td>`;
                    }
                }
                else
                    detailItems += "<tr><td>" + value.part_number + "</td><td>" + value.description + "</td><td>" + formatter.format(value.price_original) + "</td><td>" + formatter.format(value.price) + "</td>"
            });
            if (d.promotion_type == "AVANZADA")
                tabledetail.html('<table id="tabledetail' + d.promotion_code + '" class="table table-striped table-bordered table-hover" style="width:100%"><thead><tr><th>Codigo</th><th>Descripcion</th><th>Cantidad</th><th>Monto Original</th><th>Precio Promocion</th><th>Comun</th></tr></thead><tbody>' + detailItems + '</tbody></table>').removeClass('loading');
            else
                tabledetail.html('<table id="tabledetail' + d.promotion_code + '" class="table table-striped table-bordered table-hover" style="width:100%"><thead><tr><th>Codigo</th><th>Descripcion</th><th>Precio Original</th><th>Precio Promocion</th></tr></thead><tbody>' + detailItems + '</tbody></table>').removeClass('loading');
            reloadStyleTable(d);
        }
    });
    return tabledetail;
}

function reloadStyleTable(d) {
    $('#tabledetail' + d.promotion_code).DataTable({
        oLanguage: {
            "sProcessing": "Procesando...", "sLengthMenu": "Mostrar _MENU_ registros", "sZeroRecords": "No se encontraron resultados", "sEmptyTable": "Ningun dato disponible en esta tabla", "sInfo": "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros", "sInfoEmpty": "Mostrando registros del 0 al 0 de un total de 0 registros", "sInfoFiltered": "(filtrado de un total de _MAX_ registros)", "sInfoPostFix": "", "sSearch": "Buscar:", "sUrl": "", "sInfoThousands": ",", "sLoadingRecords": "Cargando...", "oPaginate": { "sFirst": "Primero", "sLast": "Ãltimo", "sNext": "Siguiente", "sPrevious": "Anterior" }, "oAria": { "sSortAscending": ": Activar para ordenar la columna de manera ascendente", "sSortDescending": ": Activar para ordenar la columna de manera descendente" }
        }
    });
}

function MessageNoProducts() {
    toastr.warning("Este producto no tiene productos en común.");
}

$('#Search').on('click', function () {
    if (ValidateDates()) {
        var item = $("#ItemsDrop").val();
        var type = $("#typeSelect").val();
        var begindate = $('#BeginDate').val();
        var enddate = $('#EndDate').val();
        StartLoading();
        $.ajax({
            url: "/Promotion/ActionGetPromotion",
            type: "GET",
            data: { "BeginDate": begindate, "EndDate": enddate, "item": item, "type": type },
            success: function (result) {
                if (result.status) {
                    if (!$.isEmptyObject(result.model)) {
                        $('#TableDiv').animatePanel();
                        table.clear();
                        table.rows.add($(result.model));
                        table.columns.adjust().draw();
                        $("#Print").removeAttr('disabled', 'disabled');
                    }
                    else {
                        $('#TableDiv').animatePanel();
                        table.clear();
                        table.columns.adjust().draw();
                        $("#Print").attr('disabled', true);
                    }
                }
                else {
                    SessionFalse("Se terminÃ³ su sesion.");
                    $('#TableDiv').animatePanel();
                    table.clear();
                    table.columns.adjust().draw();
                    $("#Print").attr('disabled', true);
                }
                EndLoading();
            },
            error: function () {
                toastr.remove();
                toastr.error('Error inesperado contactar a sistemas.');
                EndLoading();
                $("#Print").attr('disabled', true);
            }
        });
    }
});

function ValidateDates() {
    if ($('#BeginDate').val() != "" & $('#EndDate').val() != "") {
        if (!(moment($('#BeginDate').val()) <= moment($('#EndDate').val()))) {
            toastr.remove();
            toastr.warning('La fecha inicial no puede ser mayor a la fecha final');
            return false;
        }
        else {
            toastr.remove();
            return true;
        }
    }
    else {
        toastr.remove();
        return true;
    }
}

$("#BeginDate").datepicker({
    autoclose: true,
    todayHighlight: true
}).on("changeDate", function () {
    ValidateDates();
});

$("#EndDate").datepicker({
    autoclose: true,
    todayHighlight: true
}).on("changeDate", function () {
    ValidateDates();
});