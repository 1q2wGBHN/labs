﻿/// <reference path="../knockout-3.4.2.debug.js" />

var model = $("#model").val();

window.setTimeout(function () {
    $(".normalheader").addClass("small-header");
    $("#fa").removeClass("fa-arrow-up").addClass("fa-arrow-down");
    $("#hbreadcrumb").removeClass("m-t-lg");
}, 500);

$(document).on("keypress", '.Numeric', function (event) {
    var keyValue = event.key;
    if (keyValue.match(/[0-9]/g))
        return true;
    else
        return false;
});

var table = $('#Items').DataTable({
    oLanguage: {
        "sProcessing": "Procesando...", "sLengthMenu": "Mostrar _MENU_ registros", "sZeroRecords": "No se encontraron resultados", "sEmptyTable": "Ningún dato disponible en esta tabla", "sInfo": "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros", "sInfoEmpty": "Mostrando registros del 0 al 0 de un total de 0 registros", "sInfoFiltered": "(filtrado de un total de _MAX_ registros)", "sInfoPostFix": "", "sSearch": "Buscar:", "sUrl": "", "sInfoThousands": ",", "sLoadingRecords": "Cargando...", "oPaginate": { "sFirst": "Primero", "sLast": "Último", "sNext": "Siguiente", "sPrevious": "Anterior" }, "oAria": { "sSortAscending": ": Activar para ordenar la columna de manera ascendente", "sSortDescending": ": Activar para ordenar la columna de manera descendente" }
    },
    dom: "<'row'<'col-sm-4'l><'col-sm-4 text-left'B><'col-sm-4'f>t<'col-sm-6'i><'col-sm-6'p>>",
    lengthMenu: [[10, 25, 50, -1], [10, 25, 50, "Todos"]],
    responsive: true,
    autoWidth: true,
    order: [[1, "asc"]],
    buttons: [
        { extend: 'csv', text: 'Excel', title: 'Requisiciones', className: 'btn-sm' },
        {
            extend: 'pdf', text: 'PDF', title: 'Requisiciones', className: 'btn-sm', exportOptions: {
                columns: [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11],
            },
            customize: function (doc) {
                doc.content[1].table.widths =
                    Array(doc.content[1].table.body[0].length + 1).join('*').split('');
            }
        },
        { extend: 'print', text: 'Imprimir', className: 'btn-sm' },
    ],
    columns: [
                {
                    "class": "details-control",
                    "orderable": false,
                    "data": null,
                    "defaultContent": "",
                },
                { data: 'PurchaseNo' },
                { data: 'supplier_name' },
                { data: 'SiteName' },
                { data: 'PurchaseDate' },
                { data: 'Currency' },
                { data: 'ItemAmount' },
                { data: 'Iva' },
                { data: 'Ieps' },
                { data: 'ItemTotalAmount' },
                { data: 'purchase_status_description' }
    ],
    columnDefs: [{
        targets: [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11],
        width: "1%"
    }],
    aoColumnDefs: [
        {
            aTargets: [0],
            className: "text-center",
            mRender: function (data, type, full) {
                return "<input type='hidden' value='" + data + "'><div class='checkbox'><label> <input type='checkbox' class='i-checks child'></label></div>"
            }
        }]
});

table.clear();

if (model !== "[]") {
    table.rows.add($(model));
    table.columns.adjust().draw();
}