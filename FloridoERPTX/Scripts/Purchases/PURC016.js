﻿const fomartmoney = money => decimal => new Intl.NumberFormat("en-US", { style: "currency", currency: "USD", currencyDisplay: "symbol", maximumFractionDigits: decimal }).format(money);
const session = "se termino tu sesión";
var detailRows = [];

$(document).ready(function () {
    window.setTimeout(function () { $("#fa").click(); }, 1000);

    $("#ValidDate1").datepicker({
        autoclose: true,
        todayHighlight: true,
        showButtonPanel: true,
    }).on("changeDate", function (e) {

    });

    $("#ValidDate2").datepicker({
        autoclose: true,
        startDate: "01/01/1950",
        todayHighlight: true,
    }).on("changeDate", function (e) {

    });
});

$("#ItemSelect").select2({
    minimumInputLength: 3,
    allowClear: true,
    placeholder: "Seleccione una producto",
    initSelection: function (element, callback) {
        callback({ id: "", text: "Seleccione una opcion" });
    },
    ajax: {
        url: "/Items/ActionSearchItem/",
        dataType: 'json',
        type: "GET",
        quietMillis: 50,
        data: function (Filter) {
            return {
                Filter: Filter
            };
        },
        results: function (data) {
            return {
                results: $.map(data.Json, function (item) {
                    return {
                        text: item.Description,
                        id: item.PathNumber
                    }
                }),
            };
        }
    }
});

var tableProducts = $('#TablePurchase').DataTable({
    "paging": false,
    "ordering": false,
    "info": false,
    "searching": false,
    responsive: true,
    oLanguage: {
        "sZeroRecords": "",
        "sEmptyTable": "",
    },
    columnDefs: [
        {
            targets: [1],
            render: function (data, type, full, meta) {
                return moment(data).format("DD/MM/YYYY");
            }
        },
        {
            targets: [8, 9, 10],
            render: function (data, type, full, meta) {
                return fomartmoney(data)(4)
            }
        },
        {
            targets: [11],
            render: function (data, type, full, meta) {
                return `<button class="btn btn-info" onclick="Print('${full.PurchaseNo}')"><i class="fa fa-print"></i> </button>`
            }
        },
    ],
    columns: [
        {
            "class": "details-control",
            "orderable": false,
            "data": null,
            "defaultContent": ""
        },
        { data: 'Date' },
        { data: 'PurchaseNo' },
        { data: 'SiteCode', visible: false },
        { data: 'SiteName' },
        { data: 'PartNumber' },
        { data: 'Description' },
        { data: 'Quantity' },
        { data: 'Price' },
        { data: 'Iva' },
        { data: 'Ieps' },
        { data: null, width: "1%" }
    ]
});

$('#TablePurchase tbody').on('click', 'tr td.details-control', function () {
    var tr = $(this).closest('tr');
    var row = tableProducts.row(tr);

    if (row.child.isShown()) {
        row.child.hide();
        tr.removeClass('details');
    }
    else {
        if (tableProducts.row('.details').length) {
            $('.details-control', tableProducts.row('.details').node()).click();
        }
        row.child(format(row.data())).show();
        tr.addClass('details');
    }
});

tableProducts.on('draw', function () {
    $.each(detailRows, function (i, id) {
        $('#' + id + ' td.details-control').trigger('click');
    });
});

function format(d) {
    var detailItems = "";
    var tabledetail = $('<div/>').addClass('loading').text('Cargando Datos...');
    $.ajax({
        url: "/Purchases/ActionGetPurchaseDetail",
        data: { "poNumber": d.PurchaseNo },
        type: "GET",
        success: function (returndate) {
            $.each(returndate.Json, function (index, value) {
                detailItems += "<tr><td>" + value.PartNumber + "</td><td>" + value.Description + "</td><td>" + value.UnitSize + "(" + value.Parcking + ")" + "</td><td class='text-right'>" + fomartmoney(value.PurchasePrice)(4) + "</td><td class='text-right'>" + value.Quantity + "</td><td class='text-right'>" + fomartmoney(value.ItemAmount)(4) + "</td><td class='text-right'>" + fomartmoney(value.Iva)(4) + "</td><td class='text-right'>" + fomartmoney(value.Ieps)(4) + "</td><td class='text-right'>" + fomartmoney(value.ItemTotalAmount)(4) + "</td></tr>"
            });
            tabledetail.html('<table id="tabledetail" class="table table-striped table-bordered table-hover" style="width:100%">' +
                '<thead><tr><th>Codigo</th><th>Nombre</th><th>UM</th><th>Precio</th><th>Cantidad</th><th>Sub Total</th><th>Iva</th><th>Ieps</th><th>Total</th></tr></thead><tbody>' + detailItems + '</tbody></table>').removeClass('loading');
        }
    });
    return tabledetail
}

function Search() {
    var part_number = $("#ItemSelect").val();
    var date1 = $("#ValidDate1").val();
    var date2 = $("#ValidDate2").val();
    if (moment(date1) >= moment(date2)) {
        toastr.warning("la primera fecha no puede ser mayor a la segunda");
        $("#ValidDate1").val("");
    }
    else if (part_number.trim() != "" && moment(date1) <= moment(date2)) {
        StartLoading();
        axios.get(`/PurchaseOrderItem/ActionGetAllProducts/?part_number=${part_number}&date=${date1}&date2=${date2}`).
            then(data => {
                if (data.data == "SF") {
                    SessionFalse(session)
                }
                else {
                    if (data.data.length > 0) {
                        ClearTable(tableProducts, data.data)
                    }
                    else {
                        ClearTable(tableProducts, [])
                        toastr.warning("No hay productos con esa fecha")
                    }
                }
            })
            .catch(error => { console.error(error); toastr.error('Error inesperado contactar a sistemas.'); })
            .then(() => { EndLoading(); })
    }
    else {
        toastr.warning("Verifique los valores colocados")
    }
}

function ClearTable(table, list) {
    table.clear();
    table.rows.add(list);
    table.columns.adjust().draw();
}

function Print(purchase) {
    StartLoading();
    $.ajax({
        type: "GET",
        url: "/OrderLists/ActionGetPurchasesOrderReport",
        data: { "poNumber": purchase },
        success: function (returndates) {
            document.getElementById("iframe").srcdoc = returndates;
            EndLoading();
            $("#ModalDescription").html('Reporte de orden de compra Numero: ' + purchase + '');
            $('#ModalReport').modal('show');
        },
        error: function (returndates) {
            toastr.error('Alerta - Error inesperado  contactar a sistemas.');
            EndLoading();
        }
    });
}