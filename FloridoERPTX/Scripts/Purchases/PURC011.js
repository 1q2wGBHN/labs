﻿var ComboHeaderList = [];
$(document).ready(function () {
    window.setTimeout(function () { $("#fa").click(); }, 1000);
    ComboHeaderList = JSON.parse($("#model").val());
    FullTable(ComboHeaderList);
});

function FullTable(List) {
    table.clear().draw();
    table.rows.add(List);
    table.columns.adjust().draw();
}

table = $("#tableCombo").DataTable({
    responsive: true,
    autoWidth: true,
    order: [[1, "desc"]],
    oLanguage: {
        "sProcessing": "Procesando...",
        "sLengthMenu": "Mostrar _MENU_ registros",
        "sZeroRecords": "No se encontraron resultados",
        "sEmptyTable": "Ningún dato disponible en esta tabla",
        "sInfo": "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
        "sInfoEmpty": "Mostrando registros del 0 al 0 de un total de 0 registros",
        "sInfoFiltered": "(filtrado de un total de _MAX_ registros)",
        "sInfoPostFix": "",
        "sSearch": "Buscar:",
        "sUrl": "",
        "sInfoThousands": ",",
        "sLoadingRecords": "Cargando...",
        "oPaginate": { "sFirst": "Primero", "sLast": "Último", "sNext": "Siguiente", "sPrevious": "Anterior" },
        "oAria": {
            "sSortAscending": ": Activar para ordenar la columna de manera ascendente",
            "sSortDescending": ": Activar para ordenar la columna de manera descendente"
        }
    },
    dom: "<'row'<'col-sm-4'l><'col-sm-4 text-left'B><'col-sm-4'f>t<'col-sm-6'i><'col-sm-6'p>>",
    lengthMenu: [[10, 25, 50, -1], [10, 25, 50, "Todos"]],
    buttons: [
        { extend: 'copy', className: 'btn-sm' },
        { extend: 'csv', title: 'ReporteDeCombos', className: 'btn-sm' },
    ],
    columns: [
        {
            "class": "details-control",
            "orderable": false,
            "data": null,
            "defaultContent": "",
            width: "1%"
        },
        { data: 'PartNumberCombo', width: "1%" },
        { data: 'Description', width: "1%" },
        { data: 'Iva', width: "1%" },
        { data: 'Margin', width: "1%" },
        { data: 'ComboCost', width: "1%" },
        { data: 'ComboPrice', width: "1%" },
        { data: 'CountCombo', width: "1%" }
    ],
    columnDefs: [
        {
            targets: [0],
            width: "1%"
        },
        {
            targets: [2],
            width: "1%"
        }
    ]
});

$('#tableCombo tbody').on('click', 'tr td.details-control', function () {
    var tr = $(this).closest('tr');
    var row = table.row(tr);
    if (row.child.isShown()) {
        row.child.hide();
        tr.removeClass('details');
    }
    else {
        if (table.row('.details').length) {
            $('.details-control', table.row('.details').node()).click();
        }
        row.child(formatOne(row.data())).show();
        tr.addClass('details');
    }
});

var detailRows = [];
table.on('draw', function () {
    $.each(detailRows, function (i, id) {
        $('#' + id + ' td.details-control').trigger('click');
    });
});

function formatOne(d) {
    var detailItems = "";
    var tabledetail = $('<div/>').addClass('loading').text('Cargando Datos...');
    $.ajax({
        url: "/ComboDetail/ActionGetComboDetail",
        data: { "PartNumber": d.PartNumberCombo },
        type: "GET",
        success: function (Data) {
            $.each(Data, function (index, value) {
                detailItems += `<tr><td>${value.PartNumber}</td><td>${value.Description}</td><td>${value.Quantity}</td><td>${value.Iva}</td><td>${value.TotalPriceSale}</td>`;
            });
            tabledetail.html(`<table id="tabledetail" class="table table-striped table-bordered table-hover" style="width:100%">
                                                <thead><tr><th>Código</th>
                                                <th>Descripción</th>
                                                <th>Cantidad</th>
                                                <th>Iva</th>
                                                <th>Precio Total</th></tr>
                                                </thead>
                                                <tbody>`
                + detailItems + '</tbody></table>').removeClass('loading');
            reloadStyleTable();
        }
    });
    return tabledetail;
}

function reloadStyleTable() {
    $('#tabledetail').DataTable({
        oLanguage: {
            "sProcessing": "Procesando...", "sLengthMenu": "Mostrar _MENU_ registros", "sZeroRecords": "No se encontraron resultados", "sEmptyTable": "Ningún dato disponible en esta tabla", "sInfo": "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros", "sInfoEmpty": "Mostrando registros del 0 al 0 de un total de 0 registros", "sInfoFiltered": "(filtrado de un total de _MAX_ registros)", "sInfoPostFix": "", "sSearch": "Buscar:", "sUrl": "", "sInfoThousands": ",", "sLoadingRecords": "Cargando...", "oPaginate": { "sFirst": "Primero", "sLast": "Último", "sNext": "Siguiente", "sPrevious": "Anterior" }, "oAria": { "sSortAscending": ": Activar para ordenar la columna de manera ascendente", "sSortDescending": ": Activar para ordenar la columna de manera descendente" }
        }
    });
}