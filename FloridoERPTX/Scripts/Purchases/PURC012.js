﻿const http = {
    get: function (url, options) {
        return this.request(axios.get(url, options));
    },
    post: function (url, data, options) {
        return this.request(axios.post(url, data, options));
    },
    request: function (axiosProm) {
        return new Promise((resolve, reject) => {
            axiosProm.then(res => {
                if (res.data.Ok) {
                    return resolve(res.data);
                } else if (res.data.Type === "sf") {
                    swal({
                        title: "Sesión terminada",
                        text: "Se redigira a la pagina de inicio de sesión",
                        type: "warning",
                        showCancelButton: false,
                        confirmButtonColor: "#DD6B55",
                        confirmButtonText: "Aceptar"
                    }, (va) => {
                        const loc = window.location;
                        window.location.href = `${loc.protocol}//${loc.host}/Home/Login`;
                    });
                    return Promise.resolve();
                } else {
                    return res.data.Ok ? resolve(res.data) : reject(res.data);
                }

            }).catch(e => {
                const code = e.response ? e.response.status : -1;
                let msg;
                if (code === 404) {
                    msg = "Url no encontrada";
                } else {
                    msg = `Error desconocido[${code}]`;
                }
                const ne = { Code: e.code, Msg: msg };

                if (reject) {
                    return reject(ne);
                }
                //default
                toastr.error(ne.Msg);
                return Promise.reject(ne);
            });
        });
    }
};

var pr_form;

const modal = $("#pr_modal");
pr_form = {
    presentationId: -1,
    form: document.getElementById("form"),
    txt_name: document.getElementById("txt_name"),
    txt_presentation_code: document.getElementById("txt_presentation_code"),
    txt_part_number: document.getElementById("txt_part_number"),
    label_part_name: document.getElementById("label_part_name"),
    txt_quantity: document.getElementById("txt_quantity"),
    select_unit_size: document.getElementById("select_unit_size"),
    ckbox_active: document.getElementById("ckbox_active"),
    txt_price_total: document.getElementById("txt_price_total"),
    modal_title: document.getElementById("modal_title"),
    modal_description: document.getElementById("modal_description"),
    btn_form_cancel: document.getElementById("btn_form_cancel"),
    btn_form_accept: document.getElementById("btn_form_accept"),
    label_price_extra: document.getElementById("label_price_extra"),
};

const table = $('#table').DataTable({
    destroy: true,
    oLanguage: {
        "sProcessing": "Procesando...",
        "sLengthMenu": "Mostrar _MENU_ registros",
        "sZeroRecords": "No se encontraron resultados",
        "sEmptyTable": "Ningún dato disponible en esta tabla",
        "sInfo": "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
        "sInfoEmpty": "Mostrando registros del 0 al 0 de un total de 0 registros",
        "sInfoFiltered": "(filtrado de un total de _MAX_ registros)",
        "sInfoPostFix": "",
        "sSearch": "Buscar:",
        "sUrl": "",
        "sInfoThousands": ",",
        "sLoadingRecords": "Cargando...",
        "oPaginate": {
            "sFirst": "Primero",
            "sLast": "Último",
            "sNext": "Siguiente",
            "sPrevious": "Anterior"
        },
        "oAria": {
            "sSortAscending": ": Activar para ordenar la columna de manera ascendente",
            "sSortDescending": ": Activar para ordenar la columna de manera descendente"
        }
    },
    dom: "<'row'<'col-sm-4'l><'col-sm-4 text-left'B><'col-sm-4'f>t<'col-sm-6'i><'col-sm-6'p>>",
    lengthMenu: [[10, 25, 50, -1], [10, 25, 50, "Todos"]],
    buttons: [
        { extend: 'csv', text: 'Excel', title: 'ReportePresentaciones', className: 'btn-sm' },
        {
            extend: 'pdf',
            text: 'PDF',
            title: 'ReportePresentaciones',
            className: 'btn-sm',
            exportOptions: {
                columns: [1, 2, 3]
            },
            customize: function (doc) {
                doc.content[1].table.widths =
                    Array(doc.content[1].table.body[0].length + 1).join('*').split('');
            }
        },
        { extend: 'print', text: 'Imprimir', className: 'btn-sm' },
    ],
    columns: [
        {
            class: "",
            data: null,
            defaultContent: "<button class='btn btn-xs btn-outline btn-warning'> Detalles</button>",
            ordering: false,
            orderable: false
        },
        { data: 'PresentationCode' },
        { data: 'Name' },
        { data: 'Quantity' },
        { data: 'FactorUnitSize' },
        { data: data => round(data.Price) },
        { data: data => round(data.IVA) },
        { data: data => round(data.IEPS) },
        { data: data => round(data.TotalPrice) }
    ]
});

const tbody = $("#tbody");
tbody.off('click');
tbody.on('click',
    'button',
    function () {
        const data = table.row($(this).parents('tr')).data();
        openForm(data);
    });

function updateTable(filter) {
    StartLoading();
    let url = "/ItemPresentation/ActionGetAllPresentation";
    http.get(url).then(res => {
        var data = res.Data;
        table.clear();
        table.rows.add($(data));
        table.columns.adjust().draw();
        const tr = document.getElementById("pr_th_btn");
        tr.classList.remove("sorting_asc");
        tr.classList.add("sorting_disabled");
        EndLoading();
    });
}

function openForm(model) {
    pr_form.presentationId = model.PresentationId || "";
    pr_form.txt_name.value = model.Name || "";
    pr_form.txt_presentation_code.value = model.PresentationCode || "";
    pr_form.txt_quantity.value = model.Quantity || 0;
    pr_form.select_unit_size.value = model.FactorUnitSize || 0;
    pr_form.txt_price_total.value = model.TotalPrice || 0;
    pr_form.modal_title.innerHTML = "<i class='fa fa-object-group'></i> Detalles Presentación";
    pr_form.modal_description.innerHTML = "";
    pr_form.txt_part_number.value = model.PartNumber || "";
    updateProductName(model.PartNumber);
    updatePriceExtras(model.PartNumber);
    var txt_nameVar = $('#txt_name').val();
    var txt_quantityVar = $('#txt_quantity').val();
    var txt_presentation_codeVar = $('#txt_presentation_code').val();
    var txt_price_totalVar = $('#txt_price_total').val();

    $('#txt_name').val(txt_nameVar.trim());
    $('#txt_quantity').val(txt_quantityVar.trim());
    $('#txt_presentation_code').val(txt_presentation_codeVar.trim());
    $('#txt_price_total').val(txt_price_totalVar.trim());
    updateProductName(model.PartNumber);
    updatePriceExtras(model.PartNumber);
    modal.modal('show');
}

function updateProductName(part_number) {
    pr_form.label_part_name.innerHTML = "";
    axios.get(`/Items/ActionGetItemDescription?part_number=${part_number}`).then(res => {
        if (!res.data.success) {
            pr_form.label_part_name.innerHTML = res.data.error;
        } else {
            pr_form.label_part_name.innerHTML = res.data.Description;
        }
    }).catch(e => {
        pr_form.label_part_name.innerHTML = ("[Error al obtener nombre de producto]");
    });
}

function updatePriceExtras(part_number) {
    pr_form.label_price_extra.innerHTML = "";
    axios.get(`/Items/ActionGetItemIvaIepsValue?part_number=${part_number}`)
        .then(res => {
            if (res.data.success) {
                pr_form.txt_price_total.oninput = () => {
                    const total = Number(pr_form.txt_price_total.value);
                    if (isNaN(total)) {
                        pr_form.label_price_extra.innerHTML = "";
                        return;
                    }
                    const price_iva = total / (1 + res.data.Data.Iva);
                    const iva = round(total - price_iva);
                    let price = price_iva / (1 + res.data.Data.Ieps);
                    const ieps = round(price_iva - price);
                    price = round(price);
                    if (isNaN(price) || isNaN(iva) || isNaN(ieps)) pr_form.label_price_extra.innerHTML = "";
                    else
                        pr_form.label_price_extra.innerHTML =
                            `<b>Precio base:</b> ${price} <b> IEPS:</b> ${ieps} <b> IVA:</b> ${iva}`;
                }

                pr_form.txt_price_total.oninput();
            }
        }).catch(e => {

        });
}

function round(value, decimals) {
    decimals = decimals || 4;
    return Number(Math.round(value + 'e' + decimals) + 'e-' + decimals);
}

updateTable();