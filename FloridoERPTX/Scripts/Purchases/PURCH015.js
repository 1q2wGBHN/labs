﻿$(document).ready(function () {
    window.setTimeout(function () { $("#fa").click(); }, 1000);
    $("#SuppliersDrop").select2({
        minimumInputLength: 2,
        formatInputTooShort: function () { return "Ingresa 2 Caracteres"; },
        matcher: function (term, text, option) { return text.toUpperCase().indexOf(term.toUpperCase()) >= 0 || option.val().toUpperCase().indexOf(term.toUpperCase()) >= 0; }
    });
    var Products = [];
    table.clear();
    table.rows.add($(Products));
    table.columns.adjust().draw();
});

var table = $('#TablePurchases').DataTable({
    oLanguage: {
        "sProcessing": "Procesando...", "sLengthMenu": "Mostrar _MENU_ registros", "sZeroRecords": "No se encontraron resultados", "sEmptyTable": "Ningún dato disponible en esta tabla", "sInfo": "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros", "sInfoEmpty": "Mostrando registros del 0 al 0 de un total de 0 registros", "sInfoFiltered": "(filtrado de un total de _MAX_ registros)", "sInfoPostFix": "", "sSearch": "Buscar:", "sUrl": "", "sInfoThousands": ",", "sLoadingRecords": "Cargando...", "oPaginate": { "sFirst": "Primero", "sLast": "Último", "sNext": "Siguiente", "sPrevious": "Anterior" }, "oAria": { "sSortAscending": ": Activar para ordenar la columna de manera ascendente", "sSortDescending": ": Activar para ordenar la columna de manera descendente" }
    },
    dom: "<'row'<'col-sm-4'l><'col-sm-4 text-left'B><'col-sm-4'f>t<'col-sm-6'i><'col-sm-6'p>>",
    lengthMenu: [[10, 25, 50, -1], [10, 25, 50, "Todos"]],
    buttons: [
        { extend: 'csv', text: 'Excel', title: 'ReporteExistencias', className: 'btn-sm' },
        {
            extend: 'pdf', text: 'PDF', title: 'ReporteExistencias', className: 'btn-sm', exportOptions: {
                columns: [1, 2, 3]
            },
            customize: function (doc) {
                doc.content[1].table.widths =
                    Array(doc.content[1].table.body[0].length + 1).join('*').split('');
            }
        },
        { extend: 'print', text: 'Imprimir', className: 'btn-sm' },
    ],
    columns: [
        { data: 'PathNumber' },
        { data: 'Description' },
        { data: 'Quantity' },
        { data: 'SalePrice' },
        { data: 'Amount' }
    ],
    columnDefs: [{
        targets: [0, 1, 2, 3, 4],
        width: "1%"
    },
    {
        type: 'numeric-comma',
        render: function (data, type, row) {
            return '$' + data.toFixed(4);
        },
        targets: [4]
    }]
});

function SerchPO() {
    var supplier_id = $('#SuppliersDrop').val();
    var class_id = $('#departmentDrop').val();
    if (supplier == "") {
        supplier = 0;
    }
    $('#TableDiv').animatePanel();
    StartLoading();
    $.ajax({
        type: "GET",
        url: "/ItemSuppliers/ActionGetStockBySupplier",
        data: { "supplier_id": supplier_id, "class_id": class_id },
        success: function (returndate) {
            EndLoadging();
            if (returndate.success) {
                $.each(returndate.Json, function (index, value) {
                    //if (value.Quantity <= 0) {

                    //}
                });

                table.clear();
                table.rows.add($(returndate.Json));
                table.columns.adjust().draw();
            }
            else {
                toastr.warning('Alerta - Error inesperado!!');
            }
        },
        error: function (returndate) {
            EndLoadging();
            toastr.error('Alerta - Error inesperado contactar a sistemas.');
        }
    });
}