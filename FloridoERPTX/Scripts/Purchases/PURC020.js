﻿const isDecimal = decimal => number => {
    const IsDecimal = RegExp(`^\\d+\\.?\\d{0,${decimal}}$`);
    return IsDecimal.test(number);
}
const Sum = list => Name => list.reduce((init, total) => { return init + parseFloat(total[Name]) }, 0)
const fomartmoney = money => decimal => new Intl.NumberFormat("en-US", { style: "currency", currency: "USD", currencyDisplay: "symbol", maximumFractionDigits: decimal }).format(money);

function ComboboxOreders() {
    const selectOption = selectId => selectName => `<option value="${selectId}">${selectName}</option>`;
    axios.get("/PurchaseOrder/GetAllOrdersWithoutXML").then((data) => {
        $("#folioSearch").html([`<option value="0">Seleccione una orden</option>`, ...data.data.map(x => selectOption(x.PurchaseNo)(x.PurchaseNo + " - " + x.supplier_name))])
    }).catch((error) => {

    })
}

$('#datetimepicker').datepicker({
    autoclose: true,
    format: "mm/dd/yyyy",
    endDate: new Date(),
    todayHighlight: true,
    language: 'es'
});

function SwalMessage(optionSwal, funSucces, funcError, parmas) {
    swal({
        title: optionSwal.text,
        type: optionSwal.type,
        text: optionSwal.textOptional,
        showCancelButton: true,
        confirmButtonColor: optionSwal.color,
        confirmButtonText: "Continuar",
        cancelButtonText: "Cancelar"
    },
        function (isConfirm) {
            if (isConfirm) {
                funSucces(parmas);
            }
            else {
                funcError(parmas);
            }
        });
}

function ErrorListF(value, type, text, typeS, textSucces) {
    return !value ? ToastError(type, text) : typeS != null && typeS != "" ? ToastError(typeS, textSucces, true) : true;
}

function ToastError(type, text, value) {
    if (type == "Success") {
        toastr.success(text)
        return true;
    }
    else if (type == "Error") {
        toastr.error(text)
    }
    else if (type == "Warning") {
        toastr.warning(text)
    }
    return value != undefined || value != null ? value : false;
}

function ClearComboAndForm() {
    ClearForm();
    ClearCombo();
}

function SearchAndClearForm() {
    Search();
    ClearForm()
}

var FullTd = element => Option => Option == 2 ? `<td class = "discrepancy">${element} </td>` : `<td>${Option == 1 ? fomartmoney(element)(4) : Option == 0 ? element : element != null && element.trim() != "" ? "Contado" : "No contado"}</td >`
var FullTh = element => `<th>${element}</th>`
var FullTable = element => array => array.map(title => FullTd(element[title.title])(title.option)).toString();
var FullTableHeader = array => `<tr>${array.map(title => FullTh(title))}</tr>`
Listoption = { RFC: "", Currency: "" }
listBlind = []
var base64Xml = "";
const sesion = "Se termino su sesion.";
var total = 0;
var notXMl = false;
var diference = 0;

$(document).ready(function () {
    OCHideOrShow(true);
    RCHideOrShow(true)
    window.setTimeout(function () { $("#fa").click(); }, 1000);
    $('#buttonStatus').click(function () {
        SwalMessage(
            {
                text: "¿Deseas procesar la orden de compra con diferencia?", type: "warning", color: "#DD6B55"
            },
            NewModifyBlind,
            function () { },
            null
        )
    });
    $("#buttonRescaneo").click(function () {
        SwalMessage(
            {
                text: "¿Deseas rescanear la orden de compra?", type: "warning", color: "#DD6B55"
            },
            function () { StatusRC(); ClearComboAndForm(); },
            function () { },
            null
        )
    });
    $("#buttonRescaneoS4").click(function () {
        SwalMessage(
            {
                text: "¿Deseas rescanear la orden de compra?", type: "warning", color: "#DD6B55"
            },
            function () { StatusRC(); ClearComboAndForm(); },
            function () { },
            null
        )
    });
    $("#cancelOrderButton").click(function () {
        purchaseValue = $("#folioSearch").val()
        if (ErrorListF(purchaseValue != "0", "Warning", "Selecciona una orden a cancelar")) {
            SwalMessage(
                {
                    text: "¿Deseas cancelar la orden de compra?", type: "warning", color: "#DD6B55"
                },
                CancelPurchaseOrder,
                function () { },
                null
            )
        }

    });
    [".select", "#selectOption"].map(element => $(element).select2())
    $("#saveFormModel").hide();
});

function ClearCombo() {
    ComboboxOreders()
    $("#select2-chosen-1").html("Seleccione una orden")
}

function SearchPurchase() {
    diference = 0;
    notXMl = false;
    var folio = $("#folioSearch").val();
    if (folio != "0") {
        StartLoading();
        axios.get("/PurchaseOrder/ActionGetOCRC", { params: { PurchaseNo: $("#folioSearch").val() } })
            .then(function (data) {
                if (data.data == "SF") {
                    EndLoading()
                    SessionFalse(sesion)
                    $("#cancelOrderButtonDiv").hide();
                }
                else if (data.data == "S9") {
                    $("#cancelOrderButtonDiv").show();
                    SearchAndClearForm();
                    notXMl = true;
                }
                else if (ErrorListF(data.data == "S1", null, null, "Warning", "La orden de compra no esta disponible") ||
                    ErrorListF(data.data == "N/A", null, null, "Warning", "La orden de compra no existe")) {
                    ClearComboAndForm()
                    $("#cancelOrderButtonDiv").hide();
                    EndLoading()
                }
                else if (data.data == "S3" || data.data == "S4") {
                    ClearComboAndForm()
                    $("#cancelOrderButtonDiv").hide();
                    EndLoading()
                }
                else {
                    toastr.warning('La orden de compra no se encuentra con el estatus adecuado para procesarlo.');
                    $("#searchBody").hide();
                    ClearCombo()
                    EndLoading()
                    $("#cancelOrderButton").hide();
                }
            }).catch(function (error) {
                toastr.error('Alerta - Error inesperado  contactar a sistemas.');
            }).then(() => { })
    }
    else
        ClearForm();
}

function TableORCG() {
    $("#saveFormModel").hide();
    axios.get("/PurchaseOrderItem/ActionPurchaseOrderItemList/?PurchaseNo=" + $("#folioSearch").val())
        .then(function (data) {
            data.data.map((x, index) => x.IndexP = (index + 1));
            if (data.data.length > 0) {
                $("#tableOCRG").html("")
                isDifference = data.data.filter(x => x.QuantityOC != x.QuantityRC).length
                if (isDifference > 0) {
                    OCHideOrShow(true)
                    RCHideOrShow(false)
                    $("#tableHeadORCG").html("<tr><th  class='text-center'>#</th><th  class='text-center'>Codigo</th><th  class='text-center'>Descripción</th><th class='text-center'>Cantidad de orden de compra</th><th class='text-center'>Cantidad de recibo ciego</th></tr>")

                    listBlind = data.data.map(value => ({
                        Id: value.Id, QuantityOC: value.QuantityOC,
                        QuantityRC: value.QuantityRC, ParthNumber: value.ParthNumber,
                        PurchaseNo: value.PurchaseNo
                    }));
                    data.data.map(x => x.quantity = 1);
                    $("#tableOCRG").html(data.data.map(value => (`<tr>${FullTable(value)(
                        [
                            { title: "IndexP", option: value.QuantityOC == value.QuantityRC ? 0 : 2 },
                            { title: "ParthNumber", option: value.QuantityOC == value.QuantityRC ? 0 : 2 },
                            { title: "Description", option: value.QuantityOC == value.QuantityRC ? 0 : 2 },
                            { title: "QuantityOC", option: value.QuantityOC == value.QuantityRC ? 0 : 2 },
                            { title: "QuantityRC", option: value.QuantityOC == value.QuantityRC ? 0 : 2 },
                        ])}</tr>`
                    )).join().replace(/>,</g, '><').replace(/>, </g, '><'))
                    $("#searchBody").show();
                    EndLoading();
                }
                else {
                    $("#tableHeadORCG").html("<tr><th>#</th><th class='text-center' style='width:6%;'>Articulo</th><th class='text-center' style='width:12%;'>Descripción</th><th class='text-center' style='width:7%;'>Factor</th><th class='text-center' style='width:10%;'>Cantidad</th><th class='text-center' style='width:10%;'>Unidad</th><th class='text-center' style='width:10%;'>Precio $</th><th class='text-center' style='width:10%;'>Importe</th><th class='text-center' style='width:10%;'>IVA Total $</th><th class='text-center' style='width:10%;'>IEPS Total $</th class='text-center'><th class='text-center' style='width:13%;'>Importe Total $</th></tr>")
                    $("#searchBody").show()
                    OCHideOrShow(false)
                    RCHideOrShow(true)
                    OCInfo()
                }
            }
            else {
                toastr.warning('La orden de compra se encontro vacia');
                EndLoading()
            }
        }).catch(function (error) {
            toastr.error('Alerta - Error inesperado  contactar a sistemas.');
            EndLoading()
        }).then(() => { })
}

function OCInfoFill(id, data, name) {
    $(id).text(data[name] != "" ? data[name] : "N/A");
}
function OCInfo() {
    $("#saveFormModel").hide();
    $("#Invoice").val("");
    $("#PurchaseOrder").val("");
    axios.get("/PurchaseOrderItem/ActionPurchaseOrderItem/?PurchaseNo=" + $("#folioSearch").val())
        .then(function (data) {
            if (data.data.Supplier.PurchaseOrderList.length > 0) {
                $("#saveFormModel").show();
                $("#tableOCRG").html("")
                ClearOrder();
                [
                    { title: "#provider", name: "CommercialName" }, { title: "#rfc", name: "RFC" }, { title: "#datee", name: "PurchaseDate" },
                    { title: "#currency", name: "Currency" }, { title: "#email", name: "Email" }, { title: "#nameContact", name: "NameContact" },
                    { title: "#phone", name: "Phone" }, { title: "#adress", name: "Adress" }
                ].map(element => OCInfoFill(element.title, data.data.Supplier, element.name))
                $("#PurchaseOrder").val($("#folioSearch").val());
                $("#Invoice").val(data.data.InvoiceNo);
                Listoption.RFC = data.data.Supplier.RFC
                Listoption.Currency = data.data.Supplier.Currency
                table = ""
                sumTotalIva = sumTotalIEPS = sumTotalAmount = total = 0;
                $.each(data.data.Supplier.PurchaseOrderList, function (index, value) {
                    table += "<tr><td>" + (index + 1) + "</td><td>" + value.PartNumber + "</td><td>" + value.Description + "</td>" + "<td  class='text-right'>" + value.Parcking + "</td>" + "<td  class='text-right'>" + value.Quantity + "</td>" + "<td>" + (value.UnitSize != null ? value.UnitSize : "") + "</td>" + "<td class='text-right'>" + new Intl.NumberFormat("en-US", { style: "currency", currency: "USD", currencyDisplay: "symbol", maximumFractionDigits: 4 }).format(value.PurchasePrice) + "</td>" + "<td class='text-right'>" + new Intl.NumberFormat("en-US", { style: "currency", currency: "USD", currencyDisplay: "symbol", maximumFractionDigits: 4 }).format(value.ItemAmount) + "</td>" + "<td class='text-right'>" + new Intl.NumberFormat("en-US", { style: "currency", currency: "USD", currencyDisplay: "symbol", maximumFractionDigits: 4 }).format(value.Iva) + "</td>" + "<td class='text-right'>" + new Intl.NumberFormat("en-US", { style: "currency", currency: "USD", currencyDisplay: "symbol", maximumFractionDigits: 4 }).format(value.Ieps) + "</td>" + "<td class='text-right'>" + new Intl.NumberFormat("en-US", { style: "currency", currency: "USD", currencyDisplay: "symbol", maximumFractionDigits: 4 }).format(value.ItemTotalAmount) + "</td>" + "<tr>"
                    sumTotalIva += value.Iva
                    sumTotalIEPS += value.Ieps
                    sumTotalAmount += value.ItemAmount
                })
                $("#tableOCRG").html(table);
                $("#searchBody").show();
                total = parseFloat((sumTotalIva + sumTotalIEPS + sumTotalAmount).toFixed(4));
                [{ id: "totalIva", total: sumTotalIva },
                { id: "totalIEPS", total: sumTotalIEPS },
                { id: "totalAmount", total: sumTotalAmount },
                { id: "totalFinal", total: total }]
                    .map(element => $(`#${element.id}`).html(fomartmoney(element.total)(4)))
                EndLoading()
            }
            else {
                EndLoading()
            }
        }).catch(function (error) {
            toastr.error('Alerta - Error inesperado  contactar a sistemas.');
            EndLoading()
        }).then(() => { })
}


function Search() {
    ["#folio", "#folio2"].map(id => $(id).html($("#folioSearch").val()))
    TableORCG();
}

function OCHideOrShow(value) {
    var clearInfoOC = ["#infoSupplier", "#textProvider", "#folio",
        "#provider", "#datee", "#currency", "#email", "#nameContact", "#phone",
        "#adress", "#textOrder", "#totalIvaForm", "#totalIEPSForm", "#totalAmountForm", "#totalFinalForm",
        "#totalIva", "#totalIEPS", "#totalAmount", "#totalFinal", "#saveForm", "#Invoice"]
    value ? clearInfoOC.map(id => $(id).hide()) : clearInfoOC.map(id => $(id).show())
}

function RCHideOrShow(value) {
    var clearInfoOC = ["#folioForm", "#textOCRG", "#statusForm", "#buttonStatus"]
    value ? clearInfoOC.map(id => $(id).hide()) : clearInfoOC.map(id => $(id).show());
}

function ClearOrder() {
    var clearInfoOC = ["#provider", "#datee", "#currency", "#email", "#nameContact", "#phone", "#adress", "#Invoice"]
    clearInfoOC.map(id => $(id).text(""))
}

function ClearForm() {
    $("#searchBody").hide();
    position = $("#folioSearch").offset()
    $('html, body').animate({ scrollTop: position.top - 480 }, "slow");
    $('#selectOption').val($("#selectOption :first").val()).trigger('change');
    $("#PurchaseOrder").val("");
    $("#Invoice").val("");
    $("#Comments").val("");
}

$("#buttonSave").on("click", function () {
    var purchaseorder = $("#PurchaseOrder").val();
    var invoiceno = $("#Invoice").val();
    var comments = $("#Comments").val();

    if (invoiceno == "") {
        toastr.warning("Capture Número de Folio");
        $("#InvoiceNo").focus();
    }
    else if (comments == "") {
        toastr.warning("Capture Comentarios");
        $("#Comments").focus();
    }
    else {
        swal({
            title: "¿Esta seguro que desea actualizar la orden de compra?",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Continuar",
            cancelButtonText: "Cancelar"
        }, function (isConfirm) {
            if (isConfirm) {
                StartLoading();
                var xhttp = new XMLHttpRequest();
                xhttp.onreadystatechange = function () {
                    if (this.readyState == 4 && this.status == 200) {
                        EndLoading();
                        var response = JSON.parse(this.responseText);
                        if (response.result == "SF")
                            SessionFalse("Terminó tu sesión");
                        else if (response.result) {
                            ClearOrder();
                            ClearForm();
                            $("#folioSearch").val("0").change();
                            $('html, body').animate({ scrollTop: 0 }, "slow");
                            toastr.success("Actualizado correctamente");
                        }
                        else
                            toastr.error('Alerta - Error inesperado contactar a sistemas.');
                    }
                    else if (this.readyState == 4 && this.status != 200) {
                        EndLoading();
                        toastr.error("Alerta - Error inesperado  contactar a sistemas.");
                    }
                }
                xhttp.open("POST", "/PurchaseOrder/ActionUpdatePurchaseOrderInvoice", true);
                xhttp.setRequestHeader("Content-Type", "application/json;charset=UTF-8");
                xhttp.send(JSON.stringify({ "PurchaseOrder": purchaseorder, "InvoiceNo": invoiceno, "Comments": comments }));
            }
        });
    }
});