﻿var purchase_no_global = "";

window.setTimeout(function () {
    $(".normalheader").addClass("small-header");
    $("#fa").removeClass("fa-arrow-up").addClass("fa-arrow-down");
    $("#hbreadcrumb").removeClass("m-t-lg");
}, 1000);

var table = $('#Purchases').DataTable({
    oLanguage: {
        "sProcessing": "Procesando...", "sLengthMenu": "Mostrar _MENU_ registros", "sZeroRecords": "No se encontraron resultados", "sEmptyTable": "Ningún dato disponible en esta tabla", "sInfo": "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros", "sInfoEmpty": "Mostrando registros del 0 al 0 de un total de 0 registros", "sInfoFiltered": "(filtrado de un total de _MAX_ registros)", "sInfoPostFix": "", "sSearch": "Buscar:", "sUrl": "", "sInfoThousands": ",", "sLoadingRecords": "Cargando...", "oPaginate": { "sFirst": "Primero", "sLast": "Último", "sNext": "Siguiente", "sPrevious": "Anterior" }, "oAria": { "sSortAscending": ": Activar para ordenar la columna de manera ascendente", "sSortDescending": ": Activar para ordenar la columna de manera descendente" }
    },
    responsive: true,
    autoWidth: true,
    order: [[1, "asc"]],
    dom: "<'row'<'col-sm-4'l><'col-sm-4 text-left'B><'col-sm-4'f>t<'col-sm-6'i><'col-sm-6'p>>",
    lengthMenu: [[10, 25, 50, -1], [10, 25, 50, "Todos"]],
    buttons: [
        {
            extend: 'csv', text: 'Excel', title: 'PurchasesOrder', className: 'btn-sm', exportOptions: {
                columns: [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
            },
        },
        {
            extend: 'pdf', text: 'PDF', title: 'PurchasesOrder', className: 'btn-sm', exportOptions: {
                columns: [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
            },
            customize: function (doc) {
                doc.content[1].table.widths =
                    Array(doc.content[1].table.body[0].length + 1).join('*').split('');
            }
        },
        {
            extend: 'print', text: 'Imprimir', className: 'btn-sm', exportOptions: {
                columns: [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
            },
        },
    ],
    columns: [
        {
            "class": "details-control",
            "orderable": false,
            "data": null,
            "defaultContent": "",
        },
        { data: 'PurchaseNo' },
        { data: 'supplier_name' },
        { data: 'SiteName' },
        { data: 'PurchaseDate' },
        { data: 'Currency' },
        { data: 'ItemAmount' },
        { data: 'Iva' },
        { data: 'Ieps' },
        { data: 'ItemTotalAmount' },
        { data: 'purchase_status_description' }
    ],
    columnDefs: [{
        targets: [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10],
        width: "1%"
    },
    {
        targets: [5, 6, 7, 8, 9],
        className: 'text-right'
    },
    {
        targets: [4],
        render: function (data, type, row) {
            return moment(data).format('DD/MM/YYYY');
        }
    },
    {
        type: 'numeric-comma',
        render: function (data, type, row) {
            return '$' + data.toFixed(4);
        },
        targets: [6, 7, 8, 9]
    },
    {
        data: null,
        width: "1%",
        targets: 11,
        defaultContent: '<button class="btn btn-xs btn-outline btn-info btn-report" type="button"><i class="fa fa-file-text-o"></i> Reporte</button>'
    },
    {
        data: null,
        width: "1%",
        targets: 12,
        defaultContent: '<button class="btn btn-xs btn-outline btn-danger btn-cancel" type="button"><i class="fa fa-close"></i> Cancelar</button>'
    }]
});

var model = JSON.parse($("#model").val());
table.clear();
table.rows.add($(model));
table.columns.adjust().draw();

function UpdateByDate() {
    var beginDate = $('#beginDate').val();
    var endDate = $('#endDate').val();
    if (beginDate != "" && endDate != "") {

        var date = new Date();
        var Restrictedbegindate = new Date(beginDate);
        if (date.getMonth() != Restrictedbegindate.getMonth() || date.getFullYear() != Restrictedbegindate.getFullYear() ) {
            $('#beginDate').val('');
            $('#endDate').val('');
            toastr.error("Solo es permitido fechas dentro del mismo mes.");
            return false;
        }
        if ((new Date(beginDate)) <= (new Date(endDate))) {
            toastr.clear();
            $.ajax({
                type: "GET",
                url: "/Purchases/ActionGetFinishedPurchasesOrdersByDate/",
                data: { "beginDate": beginDate, "endDate": endDate },
                success: function (result) {
                    if (result.success) {
                        $('#TableDiv').animatePanel();
                        table.clear();
                        table.rows.add($(result.Json));
                        table.columns.adjust().draw();
                    }
                    else {
                        if (result.Json == "D")
                            toastr.warning('Alerta - Error inesperado!!');
                        else if (result.Json == "SF")
                            SessionFalse("");
                        else
                            toastr.warning('Alerta - Error inesperado!!');
                    }
                },
                error: function (result) {
                    toastr.error('Alerta - Error inesperado contactar a sistemas.');
                }
            });
        }
        else
            toastr.error("La fecha inicial no puede ser mayor a la fecha final.");
    }
}

function Update() {
    $.ajax({
        type: "GET",
        url: "/Purchases/ActionGetAllFinishedPurchasesOrders/",
        success: function (result) {
            EndLoading();
            if (result.success) {
                $('#TableDiv').animatePanel();
                table.clear();
                table.rows.add($(result.Json));
                table.columns.adjust().draw();
                swal({
                    title: "Cancelado",
                    text: "Orden de compra cancelada satisfactoriamente!",
                    type: "success",
                    confirmButtonText: "Continuar"
                });
            }
            else
                toastr.warning('Alerta - Error inesperado!!');
        },
        error: function (result) {
            EndLoading();
            toastr.error('Alerta - Error inesperado contactar a sistemas.');
        }
    });
}



var today = new Date();
var startDate = new Date(today.getFullYear(), today.getMonth(), 1);
var endDate = new Date(today.getFullYear(), today.getMonth() + 1, 0);

$("#beginDate").datepicker({
    autoclose: true,
    todayHighlight: true,
    startDate: startDate,
    endDate: endDate
}).on("changeDate", function () {
    UpdateByDate();
});

$("#endDate").datepicker({
    autoclose: true,
    todayHighlight: true,
    startDate: startDate,
    endDate: endDate
}).on("changeDate", function () {
    UpdateByDate();
});


function reloadStyleTable() {
    $('#tableDetail').DataTable({
        oLanguage: {
            "sProcessing": "Procesando...", "sLengthMenu": "Mostrar _MENU_ registros", "sZeroRecords": "No se encontraron resultados", "sEmptyTable": "Ningún dato disponible en esta tabla", "sInfo": "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros", "sInfoEmpty": "Mostrando registros del 0 al 0 de un total de 0 registros", "sInfoFiltered": "(filtrado de un total de _MAX_ registros)", "sInfoPostFix": "", "sSearch": "Buscar:", "sUrl": "", "sInfoThousands": ",", "sLoadingRecords": "Cargando...", "oPaginate": { "sFirst": "Primero", "sLast": "Último", "sNext": "Siguiente", "sPrevious": "Anterior" }, "oAria": { "sSortAscending": ": Activar para ordenar la columna de manera ascendente", "sSortDescending": ": Activar para ordenar la columna de manera descendente" }
        }
    });
}

function Format(d) {
    var detailItems = "";
    var tableDetail = $('<div/>').addClass('loading').text('Cargando Datos...');
    $.ajax({
        url: "/Purchases/ActionGetPurchaseDetail/",
        data: { "poNumber": d.PurchaseNo },
        type: "GET",
        success: function (result) {
            $.each(result.Json, function (index, value) {
                detailItems += "<tr><td>" + value.PartNumber + "</td><td>" + value.Description + "</td><td>" + value.UnitSize + "(" + value.Parcking + ")" + "</td><td class='text-right'>" + "$" + value.PurchasePrice.toFixed(4) + "</td><td class='text-right'>" + value.Quantity + "</td><td class='text-right'>" + "$" + value.ItemAmount.toFixed(4) + "</td><td class='text-right'>" + "$" + value.Iva.toFixed(4) + "</td><td class='text-right'>" + "$" + value.Ieps.toFixed(4) + "</td><td class='text-right'>" + "$" + value.ItemTotalAmount.toFixed(4) + "</td></tr>"
            });
            tableDetail.html('<table id="tableDetail" class="table table-striped table-bordered table-hover" style="width:100%">' +
                '<thead><tr><th>Codigo</th><th>Nombre</th><th>UM</th><th>Precio</th><th>Cantidad</th><th>Sub Total</th><th>Iva</th><th>Ieps</th><th>Total</th></tr></thead><tbody>' + detailItems + '</tbody></table>').removeClass('loading');
            reloadStyleTable();
        }
    });
    return tableDetail;
}

$('#Purchases tbody').on('click', 'tr td.details-control', function () {
    var tr = $(this).closest('tr');
    var row = table.row(tr);

    if (row.child.isShown()) {
        row.child.hide();
        tr.removeClass('details');
    }
    else {
        if (table.row('.details').length)
            $('.details-control', table.row('.details').node()).click();

        row.child(Format(row.data())).show();
        tr.addClass('details');
    }
});

$('#Purchases tbody').on('click', '.btn-report', function () {
    StartLoading();
    var purchase_no = table.row($(this).parents('tr')).data().PurchaseNo;
    $.ajax({
        type: "GET",
        url: "/OrderLists/ActionGetPurchasesOrderReport/",
        data: { "poNumber": purchase_no },
        success: function (result) {
            EndLoading();
            document.getElementById("iframe").srcdoc = result;
            $("#ModalDescription").html('Reporte de orden de compra Numero: ' + purchase_no + '');
            $('#ModalReport').modal('show');
        },
        error: function (result) {
            EndLoading();
            toastr.error('Alerta - Error inesperado  contactar a sistemas.');
        }
    });
});

function Save(purchase_no, comment) {
    $("#ModalInvoice").modal("hide")
    $.ajax({
        type: "POST",
        url: "/Purchases/ActionCancelPurchaseOrder/",
        data: { "PurchaseOrder": purchase_no, "comment": comment },
        success: function (result) {
            EndLoading();

            if (!result.Status && result.Response == "Terminó tu sesión.")
                SessionFalse(result.Response);

            else if (result.Status && result.Response == 0 && result.Document != "")
                Update();

            else if (!result.Status && result.Document == "") {
                var texto = "";
                switch (result.Response) {
                    case 6001:
                        texto = "Site code incorrecto - (6001)"
                        break
                    case 6002:
                        texto = "No se encontro orden de compra - (6002)"
                        break
                    case 6003:
                        texto = "No tiene estatus terminado - (6003)"
                        break
                    case 8001:
                        texto = "Error desconocido - (8001)"
                        break
                    case 9999:
                        texto = "Solo los Gerentes pueden cancelar las ordenes de compra."
                        break
                    default:
                        texto = "Error inesperado contactar a sistemas."
                        break
                }
                toastr.error(texto);
            }
        },
        error: function (result) {
            EndLoading();
            toastr.error('Error inesperado contactar a sistemas.');
        }
    });
}

$('#Purchases tbody').on('click', '.btn-cancel', function () {
    purchase_no_global = table.row($(this).parents('tr')).data().PurchaseNo;
    var proveedor = table.row($(this).parents('tr')).data().supplier_name;
    $("#inputComment").val("");
    $("#ModalTitlePurchase").html('Compra: ' + purchase_no_global + '');
    $("#ModalTextPurchase").html('Proveedor: ' + proveedor + '');
    $("#ModalInvoice").modal("show")
});


function MessageSavePurchaseCancel() {
    var comment = $("#inputComment").val();
    if (comment.trim().length >= 8) {
        swal({
            title: "¿Esta seguro?",
            text: "Se cancelará la orden de compra: " + purchase_no_global,
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Continuar",
            cancelButtonText: "Cancelar"
        }, function (isConfirm) {
            if (isConfirm) {
                StartLoading();
                Save(purchase_no_global, comment.trim());
            }
        });
    } else {
        toastr.warning("Ingrese minimo 8 caracteres para cancelar la orden de compra.");
    }
}