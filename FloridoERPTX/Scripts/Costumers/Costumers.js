﻿function CallCities(url) {    
    var stateId = $('#StateId').val();
    $.ajax({
        url: url,
        dataType: 'json',
        data: { StateId: stateId },
        success: function (data) {
            $("#CityId option").remove();
            $.each(data, function (index, optiondata) {
                $("#CityId").append("<option value='" + optiondata.city_id + "'>" + optiondata.city_nombre + "</option>");
            });
        }
    });
}
function ValidateForm() {
    $("#formEditClient").validate({
        errorClass: 'help-block animation-slideDown', // You can change the animation class for a different entrance animation - check animations page
        errorElement: "span",
        errorPlacement: function (error, element) {
            $(element)
                    .closest("form")
                    .find("span[data-valmsg-for='" + element.attr("id") + "']")
                    .append(error);
        },
        highlight: function (e) {
            $("#SaveEditCustomer").attr("disabled", "disabled");
            $(e).closest('.form-group').removeClass('has-success has-error').addClass('has-error');
            $(e).closest('.help-block').remove();
        },
        success: function (e) {
            $("#SaveEditCustomer").removeAttr("disabled");
            e.closest('.form-group').removeClass('has-success has-error');
            e.closest('.help-block').remove();
        },
        rules: {
            RFC: {
                required: true,
                minlength: 12,
                maxlength: 13,
                rfc: true,
                      
               },
            Code: {
                digits: true,
                requried: true
            },            

            ZipCode: {
                digits: true
            },            

            Email: {
                email: true
            },

            Phone: {
                number: true
            },

            Credit_Limit: {
                number: true
            },

            Credit_Limit_Alt: {
                number: true
            }
        },
        messages:
        {
            RFC: {
                required: function () { return "RFC requerido"; },
                minlength: function () { return "RFC debe ser de 12 o 13 caracteres"; },
                maxlength: function () { return "RFC debe ser de 12 o 13 caracteres"; },
                rfc: function () { return "Formato RFC inválido"; }//,
                // remote: jQuery.validator.format("RFC {0} ya esta registrado en el sistema.")
            },
           
        },
        submitHandler: function (form) {
            form.submit();
        }
    });
}