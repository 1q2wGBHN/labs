﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.WebPages;
using App.BLL.Item;
using App.BLL.PriceTagPrints;
using App.Common;
using App.DAL.PriceTagPrints;
using App.Entities.ViewModels.PriceTag;


namespace FloridoERPTX.Controllers.PriceTagPrints
{
    public class PriceTagPrintsController : Controller
    {
        
        private readonly PriceTagPrintsBusiness _priceTagPrintsBusiness;
        public PriceTagPrintsController()
        {
            _priceTagPrintsBusiness = new PriceTagPrintsBusiness();
        }

        public JsonResult ActionItemExist(string part_number)
        {
           //if (Session["User"] == null) return  Json(Result.SF());
            return Json(_priceTagPrintsBusiness.ItemExist(part_number),JsonRequestBehavior.AllowGet);
        }
        public JsonResult ActionItemById(string part_number)
        {
            //if (Session["User"] == null) return  Json(Result.SF());
            return Json(_priceTagPrintsBusiness.ItemById(part_number), JsonRequestBehavior.AllowGet);
        }

        public JsonResult ActionGetPriceTagList(PrintOrderFilter filter)
        {
            return Json(_priceTagPrintsBusiness.GetPriceTagList(filter),JsonRequestBehavior.AllowGet);
        }

        public JsonResult ActionGetTagList(int printId)
        {
            return Json(_priceTagPrintsBusiness.GetTagList(printId), JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult ActionCreatePriceTagPrint(List<PrintTagReq> products,string username,string program_id, string name, bool fav)
        {
            //if (Session["User"] == null) return Json(Result.SF());
                
            return Json(_priceTagPrintsBusiness.CreatePriceTagPrint(products,username,program_id,name,fav));
        }
        [HttpPost]
        public JsonResult ActionSetAsPrinted(int id)
        {
            //if (Session["User"] == null) return Json(Result.SF());

            return Json(_priceTagPrintsBusiness.SetAsPrinted(id));
           
        }

        [HttpPost]
        public JsonResult ActionSetAsFav(int id,bool fav)
        {
            //if (Session["User"] == null) return Json(Result.SF());

            return Json(_priceTagPrintsBusiness.SetAsFav(id,fav));

        }
        [HttpPost]
        public JsonResult ActionAddToList(int id, List<PrintTagReq> list)
        {
            //if (Session["User"] == null) return Json(Result.SF());

            return Json(_priceTagPrintsBusiness.AddToList(id, list));

        }
        [HttpPost]
        public JsonResult ActionRemoveFromList(int id, int sequence_num)
        {
            //if (Session["User"] == null) return Json(Result.SF());

            return Json(_priceTagPrintsBusiness.RemoveFromList(id, sequence_num));

        }
        
        [HttpPost]
        public JsonResult ActionEditTagList(int id, PriceTagPrintModel model, string userName)
        {
            //if (Session["User"] == null) return Json(Result.SF());

            return Json(_priceTagPrintsBusiness.EditList(id, model,userName));

        }

        public JsonResult ActionGetHistoryList(HistoryFilter filter)
        {
            var r = _priceTagPrintsBusiness.HistoryList(filter);
            
            return Json(r, JsonRequestBehavior.AllowGet);
            
            
        }
        [HttpPost]
        public JsonResult ActionCreateHistory(HistoryModel model)
        {
            //if (Session["User"] == null) return Json(Result.SF());

            return Json(_priceTagPrintsBusiness.HistoryCreate(model));
        }
    }
}