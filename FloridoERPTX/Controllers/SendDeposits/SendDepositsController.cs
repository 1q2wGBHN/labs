﻿using App.BLL;
using App.BLL.Currency;
using App.BLL.SendDeposits;
using App.BLL.Site;
using App.DAL;
using App.Entities.ViewModels.SendDeposits;
using DevExpress.XtraReports.UI;
using FloridoERPTX.Reports;
using System;
using System.Collections.Generic;
using System.Web.Mvc;
using static App.Entities.ViewModels.Common.Constants;

namespace FloridoERPTX.Controllers.SendDeposits
{
    public class SendDepositsController : Controller
    {
        private SendDepositsTypeBusiness _SendDepositsTypeBusiness;
        private SendDepositsOriginBusiness _SendDepositsOriginBusiness;
        private CurrencyBusiness _CurrencyBusiness;
        private SendDepositsBusiness _SendDepositsBusiness;
        private SiteConfigBusiness _SiteConfigBussiness;
        private SysRoleUserRepository _SysRoleUserRepository;
        private UserMasterBusiness _UserMasterBussines;

        public SendDepositsController()
        {
            _SendDepositsOriginBusiness = new SendDepositsOriginBusiness();
            _SendDepositsTypeBusiness = new SendDepositsTypeBusiness();
            _SendDepositsBusiness = new SendDepositsBusiness();
            _CurrencyBusiness = new CurrencyBusiness();
            _SiteConfigBussiness = new SiteConfigBusiness();
            _SysRoleUserRepository = new SysRoleUserRepository();
            _UserMasterBussines = new UserMasterBusiness();
        }

        public ActionResult SEDE001()
        {
            string Uuser = Session["User"].ToString();
            var Model = new SendDepositsModel();
            Model.CanEdit = _UserMasterBussines.IsInRole(Uuser, Roles.SC) || _UserMasterBussines.IsInRole(Uuser, Roles.GC) || _UserMasterBussines.IsInRole(Uuser, Roles.IT_Department);
            FillLookups();
            return View(Model);
        }

        [HttpPost]
        public ActionResult SEDE001(SendDepositsModel Model)
        {
            Model.CUser = Session["User"].ToString();            
            if (_SendDepositsBusiness.Create(Model))
                return Json(new { success = true, Json = Model, responseText = "Envío Registrado Correctamente" }, JsonRequestBehavior.AllowGet);

            return View(Model);
        }

        public ActionResult SEDE002()
        {
            var Model = new EditDeposits();
            Model.DepositsList = _SendDepositsBusiness.GetDeposits(Model.Date);
            return View(Model);
        }

        [HttpPost]
        public ActionResult ActionDeleteDeposit(int DepositId)
        {
            string Uuser = Session["User"].ToString();
            if (_SendDepositsBusiness.DeleteDeposits(DepositId, Uuser))
                return Json(new { success = true, Json = DepositId, responseText = "Envío Borrado Correctamente" }, JsonRequestBehavior.AllowGet);

            return Json(new { success = false, Json = DepositId, responseText = "Envío No Borrado" }, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public ActionResult ActionGetDepositDetails(int DepositId)
        {
            string Uuser = Session["User"].ToString();
            var Model = new SendDepositsModel();                        
            Model = _SendDepositsBusiness.GetDepositById(DepositId);
            var fecha = Model.Date.ToString();
            var fecha2 = DateTime.Now.Date.ToString(DateFormat.INT_DATE);
            var rol = _UserMasterBussines.IsInRole(Uuser, Roles.SC) || _UserMasterBussines.IsInRole(Uuser, Roles.GC) || _UserMasterBussines.IsInRole(Uuser, Roles.IT_Department);
            if (rol && fecha != fecha2)
                Model.CanEdit = true;
            FillLookups();
            return PartialView("_SEDE002", Model);
        }

        [HttpPost]
        public ActionResult ActionSaveEditDeposit(SendDepositsModel Model)
        {
            Model.CUser = Session["User"].ToString();
            if(_SendDepositsBusiness.UpdateDeposit(Model))
                return Json(new { success = true, Json = Model, responseText = "Envío Editado Correctamente" }, JsonRequestBehavior.AllowGet);

            return Json(new { success = false, Json = Model, responseText = "Envío No Editado" }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult ActionGetDeposits(string Date)
        {
            var Model = new List<SendDepositsModel>();
            Model = _SendDepositsBusiness.GetDeposits(Date);
            return Json(new { success = true, Json = Model, responseText = "Envío Editado Correctamente" }, JsonRequestBehavior.AllowGet);
        }

        void FillLookups()
        {
            ViewBag.Type = new SelectList(_SendDepositsTypeBusiness.TypeList(), "code", "description");
            ViewBag.Origin = new SelectList(_SendDepositsOriginBusiness.OriginList(), "code", "description");
            ViewBag.Currency = new SelectList(_CurrencyBusiness.CurrencyList(), "cur_code", "cur_name");
        }

        //report
        public ActionResult SEDE003()
        {
            var Model = new EditDeposits();
            Model.DepositsList = _SendDepositsBusiness.GetDeposits(Model.Date);
            return View(Model);
        }
        [HttpPost]
        public ActionResult ActionReportSends(string Date)
        {            
            var report = new SendsReport();
            var Model = new EditDeposits();
            Model.DepositsList = _SendDepositsBusiness.GetDeposits(Date);
            string siteName = _SiteConfigBussiness.GetSiteCodeName();
            (report.Bands["DetailReport"] as DetailReportBand).DataSource = report.printTable(Model.DepositsList, Date, siteName);
            (report.Bands["DetailReport"] as DetailReportBand).DataMember = "xrTableRowFormat";            
            return PartialView("~/Views/ReportsViews/SanctionsReport.cshtml", report);
        }
    }
}
