﻿using App.BLL.CreditNotes;
using App.BLL.Invoices;
using App.BLL.Lookups;
using App.BLL.Ma_Tax;
using App.BLL.Sales;
using App.Entities.ViewModels.Common;
using App.Entities.ViewModels.CreditNotes;
using App.Entities.ViewModels.Invoices;
using System.Web.Mvc;
using FloridoERPTX.Reports;
using DevExpress.XtraReports.UI;
using System;
using FloridoERPTX.CommonMethods;
using App.Entities;
using FloridoERPTX.Timbrado;
using App.BLL.SysLog;
using App.BLL.Site;
using System.Globalization;
using static App.Entities.ViewModels.Common.Constants;
using App.BLL.TransactionsCustomers;
using Ionic.Zip;
using System.Text;
using System.IO;
using System.Linq;
using System.Xml;
using App.BLL;
using App.BLL.DigitalTaxStamp;

namespace FloridoERPTX.Controllers.CreditNotes
{
    public class CreditNotesController : Controller
    {
        private SalesBusiness _SaleBusiness;
        private InvoiceBusiness _InvoiceBusiness;
        private ReferenceNumberBusiness _ReferenceNumberBusiness;
        private CreditNotesBusiness _CreditNotesBusiness;
        private CreditNotesDetailBusiness _CreditNotesDetailBusiness;
        private CreditNotesDetailTaxBusiness _CreditNotesDetailTaxBusiness;
        private MaTaxBusiness _MaTaxBusiness;
        private InvoiceDetailBusiness _InvoiceDetailBusiness;
        private CreditNotesBonBusiness _CreditNotesBonBusiness;
        private SalesDetailBusiness _SalesDetailBusiness;
        private StampDocument _StampDocument;
        private SysLogBusiness _SysLogBusiness;
        private SiteConfigBusiness _SiteConfigBusiness;
        private TransactionsCustomersBusiness _TransactionsCustomersBusiness;
        private CreditNotesSalesBusiness _CreditNotesSalesBusiness;
        private UserMasterBusiness _UserMasterBussines;
        private DigitalTaxStampBusiness _DigitalTaxStampBusiness;

        private string tipo = "";
        private string status = "";
        private string moneda = "";
        private string timbre = "";
        public CreditNotesController()
        {
            _SaleBusiness = new SalesBusiness();
            _InvoiceBusiness = new InvoiceBusiness();
            _ReferenceNumberBusiness = new ReferenceNumberBusiness();
            _CreditNotesBusiness = new CreditNotesBusiness();
            _CreditNotesDetailBusiness = new CreditNotesDetailBusiness();
            _CreditNotesDetailTaxBusiness = new CreditNotesDetailTaxBusiness();
            _MaTaxBusiness = new MaTaxBusiness();
            _InvoiceDetailBusiness = new InvoiceDetailBusiness();
            _CreditNotesBonBusiness = new CreditNotesBonBusiness();
            _SalesDetailBusiness = new SalesDetailBusiness();
            _StampDocument = new StampDocument();
            _SysLogBusiness = new SysLogBusiness();
            _SiteConfigBusiness = new SiteConfigBusiness();
            _TransactionsCustomersBusiness = new TransactionsCustomersBusiness();
            _CreditNotesSalesBusiness = new CreditNotesSalesBusiness();
            _UserMasterBussines = new UserMasterBusiness();
            _DigitalTaxStampBusiness = new DigitalTaxStampBusiness();
        }

        #region Reports
        ///REPORTES
        public ActionResult CRNO003()
        {
            CreditNotesBonIndex Model = new CreditNotesBonIndex();
            Model.StartDate = DateTime.Now.Date.ToString(DateFormat.INT_DATE);
            Model.EndDate = DateTime.Now.Date.ToString(DateFormat.INT_DATE);
            Model.CreditNotesBonList = _CreditNotesBonBusiness.GetCreditNotesBonLis(Model);
            Model.CanEditDate = CanEditDate();
            return View(Model);
        }

        [HttpPost]
        public ActionResult CRNO003(CreditNotesBonIndex model)
        {
            model.CreditNotesBonList = _CreditNotesBonBusiness.GetCreditNotesBonLis(model);
            model.CreditNotesBonList.ForEach(x => x.xml = string.Empty);
            //return Json(new { success = true, Json = model }, JsonRequestBehavior.AllowGet);
            model.CanEditDate = CanEditDate();
            return View(model);
        }
        private void getEstados(CreditNotesBonIndex model)
        {
            if (model.Currency == Currencys.MXN)
            {
                moneda = "Pesos";

            }
            if (model.Currency == Currencys.USD)
            {
                moneda = "Dolares";
            }
            if (model.Status == CreditNoteStatus.All)
            {
                status = "Todas";
            }
            if (model.Status == CreditNoteStatus.Active)
            {
                status = "Ativas";

            }
            if (model.Status == CreditNoteStatus.Cancelled)
            {
                status = "Canceladas";
            }
            if (model.Type == CreditNoteTypesReports.All)
            {
                tipo = "Todas";
            }
            if (model.Type == CreditNoteTypesReports.Bonification)
            {
                tipo = "Bonificación";

            }
            if (model.Type == CreditNoteTypesReports.CreditNote)
            {
                tipo = "Nota de Crédito";
            }
            if (model.Uuid == CreditNoteStatusFiscal.All)
            {
                timbre = "Todas";
            }
            if (model.Uuid == CreditNoteStatusFiscal.SinTimbre)
            {
                timbre = "Sin Timbrar";

            }
            if (model.Uuid == CreditNoteStatusFiscal.Timbrada)
            {
                timbre = "Timbradas";
            }

        }
        
        [HttpPost]
        public ActionResult ActionReportGral(CreditNotesBonIndex model)
        {
            getEstados(model);
            CreditNotesReport report = new CreditNotesReport();
            string siteName = _SiteConfigBusiness.GetSiteCodeName();
            (report.Bands["DetailReport"] as DetailReportBand).DataSource = report.printTable(model.CreditNotesBonList, model.StartDate, model.EndDate, moneda, tipo, status, siteName);
            (report.Bands["DetailReport"] as DetailReportBand).DataMember = "xrTableRowFormat";
            return PartialView("~/Views/ReportsViews/CreditNotesReport.cshtml", report);
        }
       
        public ActionResult ActionGetCreditNotesBonDetail(long cnId, decimal importe)
        {
            return Json(new { success = true, Json = _CreditNotesBonBusiness.GetCreditNotesBondeDetailList(cnId, importe) }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult ActionReportCreditNotesDetails(long cnId, string folio, decimal importe, string date, string invoice, string customer, string type)
        {
            SalesReportDetail report = new SalesReportDetail();
            string siteName = _SiteConfigBusiness.GetSiteCodeName();
            (report.Bands["DetailReport"] as DetailReportBand).DataSource = report.printTable(_CreditNotesBonBusiness.GetCreditNotesBondeDetailList(cnId, importe), folio, date, invoice, true, siteName, customer, type);
            (report.Bands["DetailReport"] as DetailReportBand).DataMember = "xrTableRowFormat";
            return PartialView("~/Views/ReportsViews/SaleReportView.cshtml", report);
        }

        public ActionResult ActionGetXMLCreditNotes(CreditNotesBonIndex Model)
        {
            var List = _CreditNotesBonBusiness.GetCreditNotesBonLis(Model);
            using (ZipFile zip = new ZipFile())
            {
                foreach (var item in List.Where(c => !string.IsNullOrEmpty(c.xml)))
                {
                    byte[] xmlFile = null;
                    string fileName = string.Format("{0}.xml", item.CfdNotaNo);
                    xmlFile = Encoding.UTF8.GetBytes(item.xml);
                    zip.AddEntry(fileName, xmlFile);
                }

                if (zip.Count > 0)
                {
                    DateTime StartDate = DateTime.ParseExact(Model.StartDate, DateFormat.INT_DATE, CultureInfo.InvariantCulture);
                    DateTime EndDate = DateTime.ParseExact(Model.EndDate, DateFormat.INT_DATE, CultureInfo.InvariantCulture);
                    string zipName = string.Format("Notas de Crédito/Bonificaciones del {0} al {1}.zip", StartDate.ToString("d", CultureInfo.CreateSpecificCulture("de-DE")), EndDate.ToString("d", CultureInfo.CreateSpecificCulture("de-DE")));
                    using (MemoryStream output = new MemoryStream())
                    {
                        zip.Save(output);
                        return File(output.ToArray(), "application/zip", zipName);
                    }
                }
            }
            return null;
        }

        public ActionResult ActionGetXMLFile(long DocumentNo)
        {
            XmlDocument doc = new XmlDocument();
            var DocumentItem = _CreditNotesBusiness.GetCreditNoteByNumber(DocumentNo);
            string fileName = string.Format("{0}{1}.xml", DocumentItem.cn_serie, DocumentItem.cn_id);

            using (MemoryStream output = new MemoryStream())
            {
                doc.LoadXml(DocumentItem.xml);
                XmlTextWriter writer = new XmlTextWriter(output, Encoding.UTF8);
                doc.WriteTo(writer);
                writer.Flush();
                Response.Clear();
                byte[] byteArray = output.ToArray();
                Response.AppendHeader("Content-Disposition", "filename=" + fileName);
                Response.AppendHeader("Content-Length", byteArray.Length.ToString());
                Response.ContentType = "application/octet-stream";
                Response.BinaryWrite(byteArray);
                writer.Close();
            }
            return null;
        }
        #endregion

        #region Create Credit Notes and Bonifications
        public ActionResult CRNO001()
        {
            CreditNoteIndex Model = new CreditNoteIndex();
            return View(Model);
        }

        [HttpPost]
        public ActionResult CRNO001(CreditNoteIndex Model)
        {
            CreditNote Item = new CreditNote();
            InvoiceDetails Details;
            var Reference = _ReferenceNumberBusiness.GetReferenceNumber(VoucherType.EXPENSES);

            while(_CreditNotesBusiness.DocumentExist(Reference.number))
            {
                _CreditNotesBusiness.SprocUpdateCreditNoteNumber();
                var n = _ReferenceNumberBusiness.GetReferenceNumber(VoucherType.EXPENSES);
                Reference = n;
            }
            var Invoice = new INVOICES();
            Item.Number = Reference.number;
            Item.Serie = Reference.serie;
            Item.CreditNoteReference = string.Format("{0}{1}", Reference.serie, Reference.number);
            Item.CanSelectItems = Model.SelectItems;
            long InvoiceNumber = !string.IsNullOrEmpty(Model.InvoiceNumber) ? long.Parse(Model.InvoiceNumber) : 0;
            long SaleNumber = !string.IsNullOrEmpty(Model.SaleNumber) ? long.Parse(Model.SaleNumber) : 0;            
            if (Model.Type != CreditNoteType.EmployeeBonification)
            {
                if (!string.IsNullOrEmpty(Model.SaleNumber))
                {
                    Item.IsSaleFromGlobal = _SaleBusiness.IsSaleFromGlobalInvoice(SaleNumber);
                    var Sale = _SaleBusiness.GetSaleByNumber(SaleNumber);
                    if (Sale.invoice_no.HasValue)
                    {
                        InvoiceNumber = Sale.invoice_no.Value;
                        Details = _InvoiceBusiness.InvoiceDetails(Sale.invoice_no.Value);
                        Details.Products = Item.IsSaleFromGlobal ? _SalesDetailBusiness.InvoiceProductsList(SaleNumber) : _InvoiceDetailBusiness.InvoiceProductsList(null, SaleNumber);
                        if (Item.IsSaleFromGlobal)
                        {
                            Details.Tax = Sale.sale_tax;
                            Details.Total = Sale.sale_total;
                            Details.SubTotal = Sale.sale_total - Sale.sale_tax;
                        }
                    }
                    else
                    {                        
                        Item.IsSaleInvoiced = false;
                        Details = _SaleBusiness.GetSaleDetailForBonificationCreditNotes(SaleNumber);
                        Details.Date = Sale.sale_date.ToString(DateFormat.INT_DATE);
                        Details.Products = _SalesDetailBusiness.InvoiceProductsList(SaleNumber);
                    }
                }
                else
                {
                    Details = _InvoiceBusiness.InvoiceDetails(InvoiceNumber);
                    Details.Products = _InvoiceDetailBusiness.InvoiceProductsList(InvoiceNumber, null);
                }
            }
            else
            {
                Invoice = _InvoiceBusiness.GetGlobalInvoiceByDate(Model.Date);
                Details = _InvoiceBusiness.InvoiceDetailsByDate(Model.Date);
                Details.Products = _InvoiceDetailBusiness.InvoiceProductsList(Invoice.invoice_no, null);
            }

            Item.InvoiceDetails = Details;
            Item.IsBonification = Model.Type == CreditNoteType.Bonification || Model.Type == CreditNoteType.EmployeeBonification;
            Item.InvoiceNumber = InvoiceNumber;

            if (Model.Type == CreditNoteType.Bonification)
            {
                Item.BonificationTaxes = (InvoiceNumber != 0 && !_InvoiceBusiness.IsInvoiceDaily(InvoiceNumber) && Item.IsSaleInvoiced) ? _MaTaxBusiness.GetTaxForGlobalInvoiceBonification(InvoiceNumber) : _MaTaxBusiness.GetTaxesForBonifications();
                BonificationTax SubTotal = new BonificationTax();
                SubTotal.TaxCode = TaxCodes.SUB_TOTAL;
                SubTotal.TaxName = TaxTypes.SUBTOTAL;
                SubTotal.Bonification = 0;
                SubTotal.TaxValue = 0;
                Item.BonificationTaxes.Add(SubTotal);
                Item.InvoiceBalance = 0;
                Item.IsGlobalInvoice = Item.IsSaleInvoiced ? !_InvoiceBusiness.IsInvoiceDaily(InvoiceNumber) : false;
            }
            else if (Model.Type == CreditNoteType.EmployeeBonification)
            {
                Item.IsEmployeeBon = true;
                Item.IssuedDate = Model.Date;
                Item.IsReadOnly = true;
                BonificationTax SubTotal = new BonificationTax();
                SubTotal.TaxCode = TaxCodes.SUB_TOTAL;
                SubTotal.TaxName = TaxTypes.SUBTOTAL;
                SubTotal.Bonification = _SaleBusiness.GetBonificationEmployeeTotalByInvoiceDate(Invoice.invoice_date);//_SaleBusiness.GetBonificationEmployeeTotalByInvoice(Invoice.invoice_no, Invoice.invoice_serie);
                SubTotal.TaxValue = 0;
                Item.BonificationTaxes.Add(SubTotal);
                Item.Cause = string.Format("Descuento sobre venta correspondiente al {0}", Invoice.invoice_date.ToString("D", new CultureInfo("es-ES")));
                Item.Comments = string.Format("Descuento sobre venta correspondiente al {0}", Invoice.invoice_date.ToString("D", new CultureInfo("es-ES")));
            }

            return View("CRNO002", Item);
        }

        //Create document selected. 
        [HttpPost]
        public ActionResult ActionCreateCreditNote(CreditNote Model)
        {
            Model.User = Session["User"].ToString();
            var ResponseStamp = new Response();
            if (Session["User"] != null)
            {
                if (_CreditNotesBusiness.CreateCreditNote(Model)) //Create data with total and tax = 0
                    if (_CreditNotesDetailBusiness.CreateCreditNoteDetail(Model))
                        if (_CreditNotesDetailTaxBusiness.CreateCreditNoteTax(Model))
                            if (_CreditNotesBusiness.UpdateTotals(Model)) //Update total and tax
                            {
                                _CreditNotesBusiness.UpdateStock(Model, false);
                                ResponseStamp = _StampDocument.DocumentStamp(Model, false);
                                if (ResponseStamp.Status)
                                {
                                    Model.IsStamped = true;
                                    var newInstance = new CreditNotesBusiness();
                                    var EntityStamped = newInstance.GetCreditNoteByNumber(Model.Number);
                                    Model.PdfEncoded = EntityStamped.pdf;
                                    return Json(new { success = true, Json = Model, responseText = "Nota de Crédito creada correctamente" }, JsonRequestBehavior.AllowGet);
                                }
                                else if (_SysLogBusiness.CreateLogStampDocuments(Model, ResponseStamp.Message, ResponseStamp.ErrorNumber, false, false))
                                {
                                    Model.IsStamped = false;
                                    return Json(new { success = true, Json = Model, responseText = "Documento creado pero no timbrado." }, JsonRequestBehavior.AllowGet);
                                }
                                else
                                    goto ErrorLabel;
                            }
                            else
                                goto ErrorLabel;
            }
            else
                return Json(new { success = false, responseText = "Terminó tu sesión" }, JsonRequestBehavior.AllowGet);

            ErrorLabel:
            Rollback(Model);
            return Json(new { success = false, responseText = "Ocurrió un error, contacte a sistemas!" }, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Cancel Credit Notes and Bonifications
        //Display main view to cancel selected document on menu.
        public ActionResult ActionCRNO005(bool IsBonification)
        {
            CancelModel Model = new CancelModel();
            Model.IsBonification = IsBonification;
            return View("CRNO005", Model);
        }

        //Displaye details of selected document
        [HttpGet]
        public ActionResult ActionDocumentCancelDetails(CancelModel Item)
        {
            CreditNoteDetails Model = new CreditNoteDetails();
            Model = _CreditNotesBusiness.GetCreditNoteDetailsByNumber(long.Parse(Item.Number));
            Model.Products = _CreditNotesDetailBusiness.GetCreditNoteProductsDetailsByNumber(long.Parse(Item.Number));
            Model.IsBonification = Item.IsBonification;
            return PartialView("_CRNO005", Model);
        }

        //Method to cancel selected document
        [HttpPost]
        public ActionResult ActionCancelDocument(CancelModel Model)
        {
            var ResponseStamp = new Response();           

            if (!string.IsNullOrEmpty(Model.Uuid))
                ResponseStamp = _StampDocument.DocumentStamp(Model, true); //IsCancel = true
            else
                ResponseStamp.Status = true;

            var stampInfo = _DigitalTaxStampBusiness.GetStampInfo(long.Parse(Model.Number), Model.Serie, VoucherType.EXPENSES);

            Model.User = Session["User"].ToString();

            if (ResponseStamp.Status || (stampInfo.cancelled_request.HasValue && stampInfo.cancelled_request.Value))
            {
                var Number = long.Parse(Model.Number);
                var Document = _CreditNotesBusiness.GetCreditNoteByNumber(Number);

                Document.uuser = Session["User"].ToString();
                Document.udate = DateTime.Now;

                var SysLog = new SYS_LOG();
                DateTime dt = DateTime.Now;


                SysLog.Action = "CANCELÓ";
                SysLog.Date = DateTime.Now.Date;
                SysLog.Description = "CANCELACION";
                SysLog.idAfect = Number;
                SysLog.TableName = "NOTAS DE CREDITO";
                SysLog.Menu = "VENTAS";
                SysLog.time = dt.ToString("HH:mm");
                SysLog.uuser = Session["User"].ToString();

                //Update local DB.
                if (_CreditNotesBusiness.UpdateCreditNote(Document))
                {
                    if (Document.invoice_no.HasValue)
                    {
                        var Invoice = _InvoiceBusiness.GetInvoiceByNumber(Document.invoice_no.Value);
                        if (Invoice.is_credit)
                        {
                            Invoice.invoice_balance += Document.cn_total;
                            if (!_InvoiceBusiness.UpdateInvoice(Invoice, SysLog))
                                goto ErrorUpdate;
                        }

                        if (!Model.IsBonification && !Invoice.is_daily) //This is to check if the Credit Notes is from a non global invoice
                        {
                            var Products = _CreditNotesDetailBusiness.GetCreditNoteProductsDetailsByNumber(Number);
                            foreach (var Item in Products)
                            {
                                var InvoiceDetailProduct = _InvoiceDetailBusiness.GetInvoiceDetailsById(Item.InoviceDetailId.Value);
                                InvoiceDetailProduct.detail_qty_available += Item.Quantity;
                                if (!_InvoiceDetailBusiness.Update(InvoiceDetailProduct))
                                    goto ErrorUpdate;
                            }
                        }
                    }

                    if (!Model.IsBonification)
                    {
                        var CreditModel = new CreditNote();
                        CreditModel.User = Model.User;
                        CreditModel.Number = long.Parse(Model.Number);
                        CreditModel.Serie = Model.Serie;
                        CreditModel.IsBonification = Model.IsBonification;

                        _CreditNotesBusiness.UpdateStock(CreditModel, true);
                    }
                    return Json(new { success = true, Json = Number, responseText = "Documento cancelado correctamente" }, JsonRequestBehavior.AllowGet);
                }
            }
            else //Create error log.
                _SysLogBusiness.CreateLogStampDocuments(Model, ResponseStamp.Message, ResponseStamp.ErrorNumber, true, false);


            ErrorUpdate:
            return Json(new { success = false, Json = Model.Number, responseText = "Ocurrió un error, contacte a sistemas!" }, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Validations
        [HttpPost]
        public ActionResult ActionValidateSale(long SaleNumber, CreditNoteType Type)
        {
            var Reference = _ReferenceNumberBusiness.GetReferenceNumber(VoucherType.EXPENSES);
            if (Reference == null)
                return Json(new { success = false, Json = SaleNumber, responseText = string.Format("Por el momento no hay número ni serie disponibles para Nota de Crédito o Bonificación.", SaleNumber) }, JsonRequestBehavior.AllowGet);

            var Sale = _SaleBusiness.GetSaleByNumber(SaleNumber);

            if (Sale == null)
                return Json(new { success = false, Json = SaleNumber, responseText = string.Format("Venta {0} no existe.", SaleNumber) }, JsonRequestBehavior.AllowGet);

            if ((!Sale.invoice_no.HasValue || string.IsNullOrEmpty(Sale.invoice_serie)) && Type == CreditNoteType.CreditNote)
                return Json(new { success = false, Json = SaleNumber, responseText = string.Format("Venta {0} no esta facturada en el sistema.", SaleNumber) }, JsonRequestBehavior.AllowGet);


            if (Sale.invoice_no.HasValue) //Validations for Invoiced Sales
            {
                bool IsFromGlobal = _SaleBusiness.IsSaleFromGlobalInvoice(SaleNumber);
                long InvoiceNumber = _SaleBusiness.GetInvoiceBySaleNumber(SaleNumber);
                if (Type == CreditNoteType.CreditNote)
                {                    
                    if (!IsFromGlobal)
                    {
                        if (!_InvoiceDetailBusiness.HasInvoiceItemsAvailable(InvoiceNumber))
                            return Json(new { success = false, Json = InvoiceNumber, responseText = string.Format("Venta {0} con factura {1}, sin artículos disponibles para Nota de Crédito.", SaleNumber, InvoiceNumber) }, JsonRequestBehavior.AllowGet);
                    }
                    else
                    {
                        if (!_SalesDetailBusiness.HasSaleItemsAvailable(SaleNumber))
                            return Json(new { success = false, Json = InvoiceNumber, responseText = string.Format("Venta {0} con factura {1}, sin artículos disponibles para Nota de Crédito.", SaleNumber, InvoiceNumber) }, JsonRequestBehavior.AllowGet);
                    }
                }

                if (Type == CreditNoteType.Bonification)
                {
                    decimal InvoiceTotal = _InvoiceBusiness.InvoiceTotal(InvoiceNumber);
                    decimal InvoiceBonificationsTotal = _CreditNotesBusiness.TotalBonificationInvoice(InvoiceNumber);

                    if (InvoiceBonificationsTotal == InvoiceTotal)
                        return Json(new { success = false, Json = InvoiceNumber, responseText = string.Format("Venta {0} con factura {1}, tiene bonificiaciones sumando el total de la factura. Ya no se pueden agregar mas bonificaciones.", SaleNumber, InvoiceNumber) }, JsonRequestBehavior.AllowGet);
                }
            }
            else //Validations for UnInvoiced Sales
            {
                if (Type == CreditNoteType.CreditNote && !_SalesDetailBusiness.HasSaleItemsAvailable(SaleNumber))
                    return Json(new { success = false, Json = SaleNumber, responseText = string.Format("Venta {0} sin artículos disponibles para Nota de Crédito.", SaleNumber) }, JsonRequestBehavior.AllowGet);
                
                if(Type == CreditNoteType.Bonification)
                {
                    decimal SalesBonificationTotal = _CreditNotesBusiness.TotalBonificationSales(SaleNumber);

                    if(SalesBonificationTotal == Sale.sale_total)
                        return Json(new { success = false, Json = SaleNumber, responseText = string.Format("Venta {0} tiene bonificiaciones sumando el total de la venta. Ya no se pueden agregar mas bonificaciones.", SaleNumber) }, JsonRequestBehavior.AllowGet);
                }
            }
            return Json(new { success = true, Json = SaleNumber, responseText = string.Format("Venta {0} disponible para nota de crédito.", SaleNumber) }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult ActionValidateInvoice(long InvoiceNumber, CreditNoteType Type)
        {
            var Invoice = _InvoiceBusiness.GetInvoiceByNumber(InvoiceNumber);
            if (Invoice == null)
                return Json(new { success = false, Json = InvoiceNumber, responseText = string.Format("Factura {0} no existe", InvoiceNumber) }, JsonRequestBehavior.AllowGet);
            if (!Invoice.invoice_status)
                return Json(new { success = false, Json = InvoiceNumber, responseText = string.Format("Factura {0} cancelada", InvoiceNumber) }, JsonRequestBehavior.AllowGet);

            if (Invoice.is_credit && Invoice.invoice_balance == 0)
                return Json(new { success = false, Json = InvoiceNumber, responseText = string.Format("Factura {0} a credito sin saldo pendiente", InvoiceNumber) }, JsonRequestBehavior.AllowGet);

            decimal InvoiceTotal = Invoice.invoice_total;

            if (Type == CreditNoteType.CreditNote)
            {
                if (Invoice.is_daily)
                    return Json(new { success = false, Json = InvoiceNumber, responseText = string.Format("Factura {0} es global diaria, para ver detalles introduce el No. de Venta", InvoiceNumber) }, JsonRequestBehavior.AllowGet);

                if (!_InvoiceDetailBusiness.HasInvoiceItemsAvailable(InvoiceNumber))
                    return Json(new { success = false, Json = InvoiceNumber, responseText = string.Format("Factura {0} sin artículos disponibles para Nota de Crédito.", InvoiceNumber) }, JsonRequestBehavior.AllowGet);

                decimal InoviceCreditNotesTotal = _CreditNotesBusiness.TotalCreditNoteInvoice(InvoiceNumber);
                if (InoviceCreditNotesTotal == InvoiceTotal)
                    return Json(new { success = false, Json = InvoiceNumber, responseText = string.Format("Las notas de crédito existentes suman el total de la factura {0}. Ya no se pueden agregar mas notas de crédito.", InvoiceNumber) }, JsonRequestBehavior.AllowGet);
            }

            if (Type == CreditNoteType.Bonification)
            {                
                decimal InvoiceBonificationsTotal = _CreditNotesBusiness.TotalBonificationInvoice(InvoiceNumber);

                if (InvoiceBonificationsTotal == InvoiceTotal)
                    return Json(new { success = false, Json = InvoiceNumber, responseText = string.Format("Las bonificaciones existentes suman el total de la factura {0}. Ya no se pueden agregar mas bonificaciones.", InvoiceNumber) }, JsonRequestBehavior.AllowGet);
            }

            return Json(new { success = true, Json = InvoiceNumber, responseText = string.Format("Factura {0} disponible para nota de crédito.", InvoiceNumber) }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult ActionCheckTotalBonificationsInvoice(CreditNote Model, string BonificationValue)
        {
            if (decimal.Parse(BonificationValue) <= 0 && !Model.IsReadOnly)
                return Json(new { success = false, Json = BonificationValue, responseText = "Específica una cantidad mayor a 0 para la bonificación." }, JsonRequestBehavior.AllowGet);

            decimal TotalBonificationValue = Model.SaleNumber != 0 ? _CreditNotesBusiness.TotalBonificationSales(Model.SaleNumber) : _CreditNotesBusiness.TotalBonificationInvoice(Model.InvoiceDetails.InvoiceNumber);

            if (Model.InvoiceDetails.Total < (Math.Round(decimal.Parse(BonificationValue), 2) + TotalBonificationValue))
                return Json(new { success = false, Json = BonificationValue, responseText = "La cantidad específicada, sumado a las bonificaciones existentes, excede el total de la factura." }, JsonRequestBehavior.AllowGet);


            return Json(new { success = true, Json = BonificationValue, responseText = "Total de bonificación válido" }, JsonRequestBehavior.AllowGet);

        }

        [HttpPost]
        public ActionResult ActionValidateBonification(long DocumentNumber, bool IsBonification)
        {
            if (IsBonification)
            {
                if (!_CreditNotesDetailBusiness.BonificationExist(DocumentNumber))
                    return Json(new { success = false, Json = DocumentNumber, responseText = string.Format("Bonificación {0} no existe", DocumentNumber) }, JsonRequestBehavior.AllowGet);

                if (!_CreditNotesBusiness.IsDocumentActive(DocumentNumber))
                    return Json(new { success = false, Json = DocumentNumber, responseText = string.Format("Bonificación {0} cancelada", DocumentNumber) }, JsonRequestBehavior.AllowGet);

                var BonificationItem = _CreditNotesDetailBusiness.GetBonificationByNumber(DocumentNumber);
                if (BonificationItem != null && !_InvoiceBusiness.IsInvoiceActive(BonificationItem.CREDIT_NOTES.invoice_no.Value))
                    return Json(new { success = false, Json = DocumentNumber, responseText = string.Format("Factura a la que pertence la bonificación {0} está cancelada", DocumentNumber) }, JsonRequestBehavior.AllowGet);

                //if (BonificationItem.CREDIT_NOTES.xml == null)
                //    return Json(new { success = false, Json = DocumentNumber, responseText = string.Format("Bonificación {0} sin timbrar", DocumentNumber) }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                if (!_CreditNotesBusiness.CreditNoteExist(DocumentNumber))
                    return Json(new { success = false, Json = DocumentNumber, responseText = string.Format("Nota de Crédito {0} no existe", DocumentNumber) }, JsonRequestBehavior.AllowGet);

                if (!_CreditNotesBusiness.IsDocumentActive(DocumentNumber))
                    return Json(new { success = false, Json = DocumentNumber, responseText = string.Format("Nota de Crédito {0} cancelada", DocumentNumber) }, JsonRequestBehavior.AllowGet);

                var CreditNotesItem = _CreditNotesDetailBusiness.GetBonificationByNumber(DocumentNumber);
                if (CreditNotesItem != null && !_InvoiceBusiness.IsInvoiceActive(CreditNotesItem.CREDIT_NOTES.invoice_no.Value))
                    return Json(new { success = false, Json = DocumentNumber, responseText = string.Format("Factura a la que pertence la nota de crédito {0} está cancelada", DocumentNumber) }, JsonRequestBehavior.AllowGet);
            }

            return Json(new { success = true, Json = DocumentNumber, responseText = "Documento válido para cancelar" }, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public ActionResult ActionValidateDate(string Date)
        {
            var ExistGlobalInvoice = _InvoiceBusiness.GetGlobalInvoiceByDate(Date);
            bool ExistEmployeeBonification = _CreditNotesBusiness.CheckEmployeeBonificationByDate(Date);
            bool ExistSalesWithEmployeeBoni = _SaleBusiness.CheckSalesWithPMEpmloyeeBon(Date);

            if (ExistGlobalInvoice == null)
                return Json(new { success = false, Json = Date, responseText = "Fecha no tiene factura global." }, JsonRequestBehavior.AllowGet);

            if (ExistEmployeeBonification)
                return Json(new { success = false, Json = Date, responseText = "Fecha ya tiene bonificación de empleado." }, JsonRequestBehavior.AllowGet);

            if (!ExistSalesWithEmployeeBoni)
                return Json(new { success = false, Json = Date, responseText = "Fecha no tiene ventas con descuento empleado." }, JsonRequestBehavior.AllowGet);

            return Json(new { success = true, Json = Date, responseText = "Fecha válida para bonificación de empleado" }, JsonRequestBehavior.AllowGet);

        }
        #endregion        

        private bool Rollback(CreditNote CreditNote)
        {
            var Model = new DeleteModel();
            Model.DocumentType = VoucherType.EXPENSES;
            Model.DocumentNumber = CreditNote.Number;
            Model.InvoiceNumber = CreditNote.InvoiceDetails.InvoiceNumber;
            Model.InvoiceSerie = CreditNote.Serie;

            if (_TransactionsCustomersBusiness.DeleteTransactions(Model))
                if (_CreditNotesDetailTaxBusiness.DeleteCreditNoteDetailTax(CreditNote.Number, CreditNote.Serie))
                    if (_CreditNotesDetailBusiness.DeleteCreditNoteDetails(CreditNote.Number, CreditNote.Serie))
                        if (_CreditNotesSalesBusiness.DeleteCreditNoteSalesDetails(CreditNote.SaleNumber))
                            if (_CreditNotesBusiness.DeleteCreditNoteByNumber(CreditNote.Number, CreditNote.Serie))
                                if (_ReferenceNumberBusiness.UpdateReferenceNumber(CreditNote.Number, VoucherType.EXPENSES))
                                    return true;

            return false;
        }

        private bool CanEditDate()
        {
            string Uuser = Session["User"].ToString();
            return _UserMasterBussines.IsInRole(Uuser, Roles.SC) || _UserMasterBussines.IsInRole(Uuser, Roles.IT_Department);
        }

        [HttpGet]
        public ActionResult ActionEditDocumentDate(long DocumentId)
        {
            CreditNote Item = new CreditNote();
            var Document = _CreditNotesBusiness.GetCreditNoteByNumber(DocumentId);
            Item.IssuedDate = Document.cn_date.ToString(DateFormat.INT_DATE);
            Item.Serie = Document.cn_serie;
            Item.Number = Document.cfdi_id;
            Item.Total = Document.cn_total;
            return PartialView("_CRNO003", Item);
        }

        [HttpPost]
        public ActionResult ActionEditDocumentDate(CreditNote Item)
        {
            string SessionUser = Session["User"].ToString();
            if (_CreditNotesBusiness.UpdateDocumentDate(Item, SessionUser))
            {
                var SysLog = new SYS_LOG();
                DateTime dt = DateTime.Now;
                SysLog.Action = "EDITAR";
                SysLog.Date = DateTime.Now.Date;
                SysLog.Description = string.Format("Usuario {0} editó fecha de documento {1}", SessionUser, Item.Number);
                SysLog.idAfect = Item.Number;
                SysLog.TableName = "CREDIT_NOTES";
                SysLog.Menu = "VENTAS";
                SysLog.time = dt.ToLongTimeString();
                SysLog.uuser = SessionUser;
                _SysLogBusiness.CreateLogRecord(SysLog);
                return Json(new { success = true, Json = Item, responseText = "Fecha Actualizada Correctamente" }, JsonRequestBehavior.AllowGet);
            }
            return Json(new { success = false, Json = Item, responseText = "Ocurrió un error, contacte a sistemas!" }, JsonRequestBehavior.AllowGet);
        }
    }
}
