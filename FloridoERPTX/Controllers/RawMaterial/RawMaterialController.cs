﻿using App.BLL.RawMaterial;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using App.Common;
using System.Threading.Tasks;
using System.Configuration;
using App.BLL.Configuration;
using FloridoERPTX.Reports;
using System.IO;
using App.Entities.ViewModels.RawMaterial;
using App.BLL.Site;
using System.Drawing;
using App.BLL.MaClass;
using App.DAL.Site;

namespace FloridoERPTX.Controllers.RawMaterial
{
    public class RawMaterialController : Controller
    {
        private readonly RawMaterialBusiness _RawMaterialBusiness;
        private readonly SendMail information;
        private readonly Common common;
        private readonly SysRoleMasterBusiness _sysRoleMasterBusiness;
        private readonly SiteConfigBusiness _SiteConfigBusiness;
        private readonly SiteBusiness _siteBusiness;
        private readonly MaClassBusiness _maClassBusiness;
        private readonly SiteConfigRepository _siteConfigRepository;

        public RawMaterialController()
        {
            _RawMaterialBusiness = new RawMaterialBusiness();
            information = new SendMail();
            common = new Common();
            _sysRoleMasterBusiness = new SysRoleMasterBusiness();
            _SiteConfigBusiness = new SiteConfigBusiness();
            _siteBusiness = new SiteBusiness();
            _maClassBusiness = new MaClassBusiness();
            _siteConfigRepository = new SiteConfigRepository();
        }

        public ActionResult ActionGetAllRawMaterialDetail(int folio)
        {
            if (Session["User"] != null)
            {
                return new JsonResult() { Data = _RawMaterialBusiness.GetAllRawMaterialDetail(folio), JsonRequestBehavior = JsonRequestBehavior.AllowGet, MaxJsonLength = Int32.MaxValue };
            }
            return new JsonResult() { Data = "SF", JsonRequestBehavior = JsonRequestBehavior.AllowGet, MaxJsonLength = Int32.MaxValue };
        }

        public ActionResult ActionGetAllRawMaterialDetailByStatus(int folio, int status)
        {
            if (Session["User"] != null)
            {
                return new JsonResult() { Data = _RawMaterialBusiness.GetAllRawMaterialDetail(folio, status), JsonRequestBehavior = JsonRequestBehavior.AllowGet, MaxJsonLength = Int32.MaxValue };
            }
            return new JsonResult() { Data = "SF", JsonRequestBehavior = JsonRequestBehavior.AllowGet, MaxJsonLength = Int32.MaxValue };
        }

        [HttpPost]
        public ActionResult ActionGetAllRawMaterialDetailByDates(DateTime BeginDate, DateTime EndDate, string status, string department, string family, string location)
        {
            if (Session["User"] != null)
            {
                var response = new { result = true, Model = _RawMaterialBusiness.GetAllRawMaterialDetailByDates(BeginDate, EndDate, status, department, family, location) };
                return new JsonResult() { Data = response, JsonRequestBehavior = JsonRequestBehavior.AllowGet, MaxJsonLength = Int32.MaxValue };
            }

            return new JsonResult() { Data = "SF", JsonRequestBehavior = JsonRequestBehavior.AllowGet, MaxJsonLength = Int32.MaxValue };
        }

        public ActionResult ActionGetAllRawMaterialHeader(int Status, DateTime DateInit, DateTime DateFin)
        {
            if (Session["User"] != null)
            {
                return new JsonResult() { Data = _RawMaterialBusiness.GetAllRawMaterialHeader(Status, DateInit, DateFin), JsonRequestBehavior = JsonRequestBehavior.AllowGet, MaxJsonLength = Int32.MaxValue };
            }
            return new JsonResult() { Data = "SF", JsonRequestBehavior = JsonRequestBehavior.AllowGet, MaxJsonLength = Int32.MaxValue };
        }
        [HttpPost]
        //RA = Receipt Aproval / Aprobacion recibo
        public ActionResult ActionRawMaterialEditStatusRA(List<RawMaterialDetail> model, int folio, string comment)
        {
            if (Session["User"] != null)
            {
                if (_RawMaterialBusiness.RawMaterialEditStatusRA(model, folio, Session["User"].ToString(), comment))
                {
                    var Gerencia = _sysRoleMasterBusiness.GetEmailsByRol(ConfigurationManager.AppSettings["Gerente"].ToString());
                    var data = _RawMaterialBusiness.GetRawByFolioByStatus(folio, 2);
                    var SiteInfo = _siteConfigRepository.GetSiteCodeAddress();
                    Task task = new Task(() =>
                    {
                        var Email = Gerencia;
                        information.folio = folio.ToString();
                        information.store = comment;
                        information.subject = SiteInfo.SiteName + " - Traspaso Materia Prima - " + folio.ToString();
                        information.from = SiteInfo.SiteName;
                        information.email = Email;
                        information.title = SiteInfo.SiteName + " - Traspaso Materia Prima";
                        information.body = "Tiene un folio para aprobación de traspaso de Materia prima con el folio : " + folio.ToString() + ".";
                        var stream = new MemoryStream();
                        RawMaterialReport report = new RawMaterialReport();
                        report.DataSource = report.printTable(data);
                        report.ExportToPdf(stream);

                        string returnEmail = common.MailMessageRawMaterial(information, stream);
                        System.Diagnostics.Debug.WriteLine(returnEmail);
                    });
                    task.Start();

                    return Json(new { Status = "Aprobado", Model = _RawMaterialBusiness.GetAllRawMaterialHeader(0, DateTime.Now.AddDays(-7), DateTime.Now), JsonRequestBehavior = JsonRequestBehavior.AllowGet });
                }
                else
                    Json(new { Status = "Error", JsonRequestBehavior = JsonRequestBehavior.AllowGet });
            }
            return Json(new { Status = "SF", JsonRequestBehavior = JsonRequestBehavior.AllowGet, MaxJsonLength = Int32.MaxValue });
        }

        [HttpPost]
        //RC = Receipt Cancel / Cancelacion recibo
        public ActionResult ActionRawMaterialEditStatusRC(int folio, string comment)
        {
            if (Session["User"] != null)
            {
                if (_RawMaterialBusiness.RawMaterialEditStatusRC(folio, Session["User"].ToString(), comment))
                    return Json(new { Status = "Cancelado", Model = _RawMaterialBusiness.GetAllRawMaterialHeader(0, DateTime.Now.AddDays(-7), DateTime.Now), JsonRequestBehavior = JsonRequestBehavior.AllowGet });
                else
                    Json(new { Status = "Error", JsonRequestBehavior = JsonRequestBehavior.AllowGet });
            }
            return Json(new { Data = "SF", JsonRequestBehavior = JsonRequestBehavior.AllowGet });
        }

        [HttpPost]
        //MA = Management Aproval / Aprobacion gerencia
        public ActionResult ActionRawMaterialEditStatusMA(List<RawMaterialDetail> model, int folio, string comment)
        {
            if (Session["User"] != null)
            {
                var returnValue = _RawMaterialBusiness.RawMaterialEditStatusMA(model, folio, Session["User"].ToString(), comment);
                if (returnValue == 0)
                    return Json(new { Status = returnValue, Model = _RawMaterialBusiness.GetAllRawMaterialHeader(2, DateTime.Now.AddDays(-7), DateTime.Now), JsonRequestBehavior = JsonRequestBehavior.AllowGet });
                else
                    return Json(new { Status = returnValue, JsonRequestBehavior = JsonRequestBehavior.AllowGet });
            }
            else
                return Json(new { Status = "SF", JsonRequestBehavior = JsonRequestBehavior.AllowGet, MaxJsonLength = Int32.MaxValue });
        }
        [HttpPost]
        //MC = Management Cancel / Cancelacion gerencia
        public ActionResult ActionRawMaterialEditStatusMC(int folio, string comment)
        {
            if (Session["User"] != null)
            {
                if (_RawMaterialBusiness.RawMaterialEditStatusMC(folio, Session["User"].ToString(), comment))
                    return Json(new { Status = true, Model = _RawMaterialBusiness.GetAllRawMaterialHeader(2, DateTime.Now.AddDays(-7), DateTime.Now), JsonRequestBehavior = JsonRequestBehavior.AllowGet });
                else
                    return Json(new { Status = false, JsonRequestBehavior = JsonRequestBehavior.AllowGet });
            }
            return Json(new { Status = "SF", JsonRequestBehavior = JsonRequestBehavior.AllowGet });
        }
        [HttpPost]
        public ActionResult ActionGetAllTransfersToRaw(DateTime initial, DateTime finish, string status, string location)
        {
            if (Session["User"] != null)
            {
                return Json(new { Status = true, Model = _RawMaterialBusiness.GetAllTransferToRaw(initial, finish, status, location), JsonRequestBehavior = JsonRequestBehavior.AllowGet });
            }
            return Json(new { Status = "SF", JsonRequestBehavior = JsonRequestBehavior.AllowGet });
        }
        [HttpPost]
        public ActionResult ActionGetRawDetails(int folio)
        {
            if (Session["User"] != null)
            {
                var data = _RawMaterialBusiness.NewGetRawByFolio(folio);
                var stream = new MemoryStream();
                RawMaterialReport reportRaw = new RawMaterialReport();
                if (data.Print_Status != 0 && (data.Status == 9 || data.Status == 8))
                {
                    reportRaw.Watermark.Text = "RE-IMPRESION";
                    reportRaw.Watermark.TextDirection = DevExpress.XtraPrinting.Drawing.DirectionMode.ForwardDiagonal;
                    reportRaw.Watermark.Font = new Font(reportRaw.Watermark.Font.FontFamily, 40);
                    reportRaw.Watermark.ForeColor = Color.DodgerBlue;
                    reportRaw.Watermark.TextTransparency = 150;
                    reportRaw.Watermark.ShowBehind = false;
                    reportRaw.Watermark.PageRange = "1-99";
                    var print = _RawMaterialBusiness.DamagedsGoodsEditStatusPrint(folio, data.Print_Status, "RPD007.CSHTML");
                }
                else
                {
                    var print = _RawMaterialBusiness.DamagedsGoodsEditStatusPrint(folio, data.Print_Status, "RPD007.CSHTML");
                }
                reportRaw.DataSource = reportRaw.printTable(data);
                reportRaw.ExportToPdf(stream);
                if (stream != null)
                {
                    return Json(new { success = true, responseText = Convert.ToBase64String(stream.ToArray()) }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(new { success = false, responseText = "No podemos mostrar archivo, contacta a sistemas." }, JsonRequestBehavior.AllowGet);
                }
            }
            else
                return Json(new { Status = "SF", JsonRequestBehavior = JsonRequestBehavior.AllowGet });
        }

        [HttpPost]
        public ActionResult ActionGetRawDetailsReport(DateTime BeginDate, DateTime EndDate, string status, string department, string family, string location)
        {
            if (Session["User"] != null)
            {
                var model = _RawMaterialBusiness.GetAllRawMaterialDetailByDates(BeginDate, EndDate, status, department, family, location);
                var stream = new MemoryStream();
                RawMaterialDetailReport report = new RawMaterialDetailReport();
                department = department != "0" ? _maClassBusiness.GetDepartmentById(Convert.ToInt32(department)) : "";
                family = family != "" ? _maClassBusiness.GetFamilyById(Convert.ToInt32(family)) : "";
                report.DataSource = report.PrintTable(BeginDate, EndDate, model, _siteBusiness.GetInfoSite(), location, status, department, family);
                report.ExportToPdf(stream);

                if (stream != null)
                    return Json(new { result = true, responseText = Convert.ToBase64String(stream.ToArray()) }, JsonRequestBehavior.AllowGet);
                else
                    return Json(new { result = false, responseText = "No podemos mostrar archivo, contacta a sistemas." }, JsonRequestBehavior.AllowGet);
            }
            else
                return Json(new { Status = "SF", JsonRequestBehavior = JsonRequestBehavior.AllowGet });
        }

    }
}