﻿using App.BLL.PurchaseOrder;
using App.BLL.Site;
using App.Entities.ViewModels.PurchaseOrder;
using System;
using System.Collections.Generic;
using System.Web.Mvc;

namespace FloridoERPTX.Controllers.Purchases
{
    public class PurchaseOrderGlobalController : Controller
    {
        private readonly PurchaseOrderGlobalBusiness _purchaseOrderGlobalBusiness;
        private readonly SiteConfigBusiness _siteConfigBusiness;

        public PurchaseOrderGlobalController()
        {
            _purchaseOrderGlobalBusiness = new PurchaseOrderGlobalBusiness();
            _siteConfigBusiness = new SiteConfigBusiness();
        }

        // GET: PurchaseOrderGlobal
        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public ActionResult ActionGetDetailOrder(int purchase_no)
        {
            if (Session["User"] != null)
                return Json(_purchaseOrderGlobalBusiness.GetDetailOrder(purchase_no), JsonRequestBehavior.AllowGet);

            return Json("SF", JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult ActionGetOrderByDate(DateTime startDate, DateTime finishDate)
        {
            if (Session["User"] != null)
            {
                var site = _siteConfigBusiness.GetSiteCode();
                return Json(_purchaseOrderGlobalBusiness.GetPurchaseOrder(startDate, finishDate, site), JsonRequestBehavior.AllowGet);
            }

            return Json("SF", JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult ActionGetOrderByDateAndStatus(DateTime startDate, DateTime finishDate, int status)
        {
            if (Session["User"] != null)
            {
                var site = _siteConfigBusiness.GetSiteCode();
                return Json(_purchaseOrderGlobalBusiness.GetPurchaseOrderByStatus(startDate, finishDate, status, site), JsonRequestBehavior.AllowGet);
            }

            return Json("SF", JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult ActionAddXML(List<PurchaseOrderGlobalModel> list, int id)
        {
            if (Session["User"] != null)
            {
                var site = _siteConfigBusiness.GetSiteCode();
                string user = Session["User"].ToString();
                var x = new errors();

                if (list != null)
                    x = _purchaseOrderGlobalBusiness.ActionAddXml(list, user, id);

                if (x.pass)
                    return Json(new { Status = true, Data = _purchaseOrderGlobalBusiness.UpdatePurchaseOrder(user, x.expense_id, id), JsonRequestBehavior.AllowGet });
                else
                    return Json(new { Status = false, Data = x.error, JsonRequestBehavior.AllowGet });
            }
            else
                return Json(new { Status = "SF", JsonRequestBehavior.AllowGet });
        }
    }
}