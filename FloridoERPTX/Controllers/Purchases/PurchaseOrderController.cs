﻿using App.BLL.Ma_Tax;
using App.BLL.Order;
using App.BLL.PurchaseOrder;
using App.Entities.ViewModels.PurchaseOrder;
using DevExpress.XtraReports.UI;
using FloridoERPTX.Reports;
using System;
using System.Collections.Generic;
using System.IO;
using System.Web.Mvc;

namespace FloridoERPTX.Controllers.Purchases
{
    public class PurchaseOrderController : Controller
    {
        // GET: PurchaseOrder
        private readonly OrderListBusiness _OrderListBusiness;
        private readonly OrderBusiness _OrderBusiness;
        private readonly MaTaxBusiness _maTaxBusiness;
        private readonly PurchaseOrderBusiness _PurchaseOrderBusiness;

        public PurchaseOrderController()
        {
            _OrderListBusiness = new OrderListBusiness();
            _PurchaseOrderBusiness = new PurchaseOrderBusiness();
            _OrderBusiness = new OrderBusiness();
            _maTaxBusiness = new MaTaxBusiness();
        }

        public ActionResult ActionGetAllOC()
        {
            return Json(_PurchaseOrderBusiness.GetAllOC(), JsonRequestBehavior.AllowGet);
        }

        public ActionResult ActionGetOCRC(string PurchaseNo)
        {
            if (Session["User"] != null)
                return Json(_PurchaseOrderBusiness.GetOCRC(PurchaseNo), JsonRequestBehavior.AllowGet);

            return Json("SF", JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult ActionStatusPurchase(string PurchaseNomber, List<PurchaseOrderItemModel> OCRC)
        {
            if (Session["User"] != null)
                return Json(_PurchaseOrderBusiness.StatusPurchase(PurchaseNomber, OCRC, Session["User"].ToString()), JsonRequestBehavior.AllowGet);

            return Json("SF", JsonRequestBehavior.AllowGet);
        }

        public ActionResult ActionReportOrder(int Folio) //genera el reporte supplierOrdered
        {
            SupplierOrdered report = new SupplierOrdered();
            (report.Bands["DetailReport1"] as DetailReportBand).DataSource = report.printTable(_OrderBusiness.Get(Folio), _OrderListBusiness.GetAll(Folio));
            (report.Bands["DetailReport1"] as DetailReportBand).DataMember = "xrTableRowFormat";

            var stream = new MemoryStream();
            report.ExportToPdf(stream);

            return Json(new { success = true, responseText = Convert.ToBase64String(stream.ToArray()) }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult ActionGetAllPurchasesOrdersBySupplier(DateTime Date1, DateTime Date2, string supplier)
        {
            if (Session["User"] != null)
            {
                var tupla = _PurchaseOrderBusiness.GetAllListFillRate(Date1, Date2, supplier);
                var listOrders = tupla.Item1;
                var listItems = tupla.Item2;
                if (listOrders.Count > 0 && listItems.Count > 0)
                    return Json(new { success = true, Orders = listOrders, ItemsOrders = listItems }, JsonRequestBehavior.AllowGet);
                else
                    return Json(new { success = false, responseText = "Sin registros en ese filtro." }, JsonRequestBehavior.AllowGet);
            }
            else
                return Json(new { success = false, responseText = "Termino tu sesión." }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult ActionGetAllOrders()
        {
            if (Session["User"] != null)
                return new JsonResult() { Data = _PurchaseOrderBusiness.GetAllOrders(), JsonRequestBehavior = JsonRequestBehavior.AllowGet, MaxJsonLength = Int32.MaxValue };

            return Json("SF", JsonRequestBehavior.AllowGet);
        }

        public ActionResult ActionGetAllOrdersWithoutXML()
        {
            if (Session["User"] != null)
                return new JsonResult() { Data = _PurchaseOrderBusiness.GetAllOrdersWithoutXML(), JsonRequestBehavior = JsonRequestBehavior.AllowGet, MaxJsonLength = Int32.MaxValue };

            return Json("SF", JsonRequestBehavior.AllowGet);
        }

        public ActionResult ActionGetAllOrdersEdit()
        {
            if (Session["User"] != null)
                return new JsonResult() { Data = _PurchaseOrderBusiness.GetAllOrdersEdits(), JsonRequestBehavior = JsonRequestBehavior.AllowGet, MaxJsonLength = Int32.MaxValue };

            return Json("SF", JsonRequestBehavior.AllowGet);
        }

        public ActionResult ActionGetAllProducts(int supplier, DateTime date, DateTime date2)
        {
            return new JsonResult() { Data = _PurchaseOrderBusiness.GetAllProductsSupplier(supplier, date, date2), JsonRequestBehavior = JsonRequestBehavior.AllowGet, MaxJsonLength = Int32.MaxValue };
        }

        [HttpPost]
        public ActionResult ActionEditPurchaseOrder(string purchaseNo, decimal price, string partNumber)
        {
            if (User.Identity.IsAuthenticated)
                return new JsonResult() { Data = _PurchaseOrderBusiness.EditPurchaseOrder(purchaseNo, price, partNumber, User.Identity.Name), JsonRequestBehavior = JsonRequestBehavior.AllowGet, MaxJsonLength = Int32.MaxValue };

            return Json("SF", JsonRequestBehavior.AllowGet);
        }

        public ActionResult ActionDeleteProductPurchaseOrder(string purchaseNo, string partNumber)
        {
            if (User.Identity.IsAuthenticated)
                return new JsonResult() { Data = _PurchaseOrderBusiness.DeleteProductPurchaseOrder(purchaseNo, partNumber, User.Identity.Name), JsonRequestBehavior = JsonRequestBehavior.AllowGet, MaxJsonLength = Int32.MaxValue };

            return Json("SF", JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public ActionResult ActionSupplierPurchaseOrder(string purchaseNo)
        {
            if (User.Identity.IsAuthenticated)
                return new JsonResult() { Data = _PurchaseOrderBusiness.SupplierPurchaseOrder(purchaseNo, User.Identity.Name), JsonRequestBehavior = JsonRequestBehavior.AllowGet, MaxJsonLength = Int32.MaxValue };

            return Json("SF", JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult ActionPurchaseOrderAddPartNumber(string purchaseNo, string partNumber, decimal Quantity, int Supplier)
        {
            if (User.Identity.IsAuthenticated)
                return new JsonResult() { Data = _PurchaseOrderBusiness.PurchaseOrderAddPartNumber(purchaseNo, partNumber, Quantity, Supplier, User.Identity.Name), JsonRequestBehavior = JsonRequestBehavior.AllowGet, MaxJsonLength = Int32.MaxValue };

            return Json("SF", JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult ActionUpdatePurchaseOrder(string purchaseNo, int Supplier)
        {
            if (User.Identity.IsAuthenticated)
                return new JsonResult() { Data = _PurchaseOrderBusiness.UpdatePurchaseOrder(purchaseNo, Supplier, User.Identity.Name), JsonRequestBehavior = JsonRequestBehavior.AllowGet, MaxJsonLength = Int32.MaxValue };

            return Json("SF", JsonRequestBehavior.AllowGet);
        }

        public ActionResult ActionGetAllPurchasesDetailByPurchaseNo(string poNumber)
        {
            if (Session["User"] != null)
            {
                var listItems = _PurchaseOrderBusiness.GetAllPurchasesDetailByPurchaseNo(poNumber);
                for (int e = 0; e < listItems.Count; e++)
                {
                    var ivaValue = _maTaxBusiness.GetTaxValueByCode(listItems[e].iva_text);
                    var iepsValue = _maTaxBusiness.GetTaxValueByCode(listItems[e].ieps_text);

                    var amount = listItems[e].requestedQuantity * listItems[e].PurchasePrice;
                    var TotalIEPS = amount * (iepsValue.tax_value / 100);
                    var TotalIVA = (amount + TotalIEPS) * (ivaValue.tax_value / 100);
                    var Total = amount + TotalIEPS + TotalIVA;

                    if (listItems[e].assortedQuantity <= 0)
                        listItems[e].assortedAmount = 0;

                    listItems[e].requestedAmount = Decimal.Parse(Total.ToString(string.Format("0.{0}", new string('0', 4))));
                    listItems[e].efficiencyInQuantity = Decimal.Parse(((listItems[e].assortedQuantity * 100) / listItems[e].requestedQuantity).ToString(string.Format("0.{0}", new string('0', 4))));
                    if (listItems[e].requestedAmount != 0)
                        listItems[e].efficiencyInAmount = Decimal.Parse(((listItems[e].assortedAmount * 100) / listItems[e].requestedAmount).ToString(string.Format("0.{0}", new string('0', 4))));
                    else
                        listItems[e].efficiencyInAmount = 0; //Este problema llevaba a pasar cuando el precio llegaba en 0
                }
                return new JsonResult() { Data = listItems, JsonRequestBehavior = JsonRequestBehavior.AllowGet, MaxJsonLength = Int32.MaxValue };
            }
            return Json("SF", JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult ActionUpdatePurchaseOrderInvoice(string PurchaseOrder, string InvoiceNo, string Comments)
        {
            if (Session["User"] != null)
                return Json(new { result = _PurchaseOrderBusiness.UpdatePurchaseOrderInvoice(PurchaseOrder, InvoiceNo, Comments, Session["User"].ToString()) }, JsonRequestBehavior.AllowGet);

            return Json(new { result = "SF" }, JsonRequestBehavior.AllowGet);
        }
    }
}