﻿using App.BLL;
using App.BLL.Configuration;
using App.BLL.MaCode;
using App.BLL.PurchaseOrder;
using App.BLL.Site;
using App.BLL.Supplier;
using App.Common;
using App.DAL;
using App.DAL.Supplier;
using App.Entities.ViewModels.DamagedsGoods;
using App.Entities.ViewModels.PurchaseOrder;
using DevExpress.XtraReports.UI;
using FloridoERPTX.Reports;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Drawing;
using System.IO;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace FloridoERPTX.Controllers.Purchases
{
    public class PurchaseOrderItemController : Controller
    {
        private readonly PurchaseOrderItemBusiness _PurchaseOrderBusiness;
        private readonly PurchaseOrderBusiness __PurchaseOrderBusiness;
        private readonly MaSupplierContactRepository _MaSupplierContactRepo;
        private readonly Common common;
        private readonly SendMail inforamtion;
        private readonly SiteBusiness _siteBusiness;
        private readonly UserMasterBusiness _userMasterBusiness;
        private readonly SysRoleMasterBusiness _sysRoleMasterBusiness;
        private readonly SupplierBusiness _supplierBusiness;
        private readonly MaCodeBusiness _maCodeBusiness;

        public PurchaseOrderItemController()
        {
            _PurchaseOrderBusiness = new PurchaseOrderItemBusiness();
            common = new Common();
            inforamtion = new SendMail();
            __PurchaseOrderBusiness = new PurchaseOrderBusiness();
            _siteBusiness = new SiteBusiness();
            _MaSupplierContactRepo = new MaSupplierContactRepository();
            _userMasterBusiness = new UserMasterBusiness();
            _sysRoleMasterBusiness = new SysRoleMasterBusiness();
            _supplierBusiness = new SupplierBusiness();
            _maCodeBusiness = new MaCodeBusiness();
        }

        public ActionResult ActionPurchaseOrderItemList(string PurchaseNo)
        {
            return Json(_PurchaseOrderBusiness.GetPurchaseOrderItem(PurchaseNo), JsonRequestBehavior.AllowGet);
        }

        public ActionResult ActionGetAllProducts(string part_number, DateTime date, DateTime date2)
        {
            return new JsonResult() { Data = __PurchaseOrderBusiness.GetAllProducts(part_number, date, date2), JsonRequestBehavior = JsonRequestBehavior.AllowGet, MaxJsonLength = Int32.MaxValue };
        }

        public ActionResult ActionPurchaseOrderItem(string PurchaseNo)
        {
            var supplier = _PurchaseOrderBusiness.GetPurchaseOrderItemList(PurchaseNo);
            var persmission = _maCodeBusiness.GetPermisionXML(supplier.RFC);
            return Json(new { Supplier = supplier, Permission = persmission }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult ActionNewStoreProcedureOCRG(string PurchaseNo, string Base64, string Uuid, string Date, string Invoice, decimal Diference, string Comment)
        {
            if (Session["User"] != null)
            {
                var Fecha = Convert.ToDateTime(Date);
                if (Invoice != null && Comment != null)
                {
                    if (!_PurchaseOrderBusiness.SearchFolioBySupplier(PurchaseNo, Invoice))
                    {
                        if (_PurchaseOrderBusiness.ExistUuid(Uuid) == 0)
                        {
                            var IsCreateCreditNote = true;
                            if (Diference > 0)
                                IsCreateCreditNote = _PurchaseOrderBusiness.CreateCreditNote(PurchaseNo, Session["User"].ToString(), Diference);
                            if (IsCreateCreditNote)
                            {
                                if (_PurchaseOrderBusiness.SaveExchangeRateDate(PurchaseNo, Fecha, Session["User"].ToString()))
                                {
                                    var document = _PurchaseOrderBusiness.StoreProcedureOCRG(PurchaseNo, Session["User"].ToString());
                                    var XML = false;
                                    if (document.ReturnValue == 0)
                                    {
                                        XML = _PurchaseOrderBusiness.AddXMLPurchaseOrderItem(PurchaseNo, Base64, Uuid, Session["User"].ToString(), Invoice, Comment);
                                        if (XML)
                                        {
                                            var infoSupplier = __PurchaseOrderBusiness.GetPurchaseOrderByPo(PurchaseNo);
                                            var purchase = _PurchaseOrderBusiness.SupplierInfo(PurchaseNo);
                                            //por aqui paso pasillas y lo modifico
                                            var emailSupplier = _MaSupplierContactRepo.EmailsGeneric(purchase.Item1, "Contabilidad");
                                            if (emailSupplier == "")
                                                emailSupplier = _MaSupplierContactRepo.EmailsGeneric(purchase.Item1, "Ventas");

                                            var ListPurchase = __PurchaseOrderBusiness.NewGetAllPurchasesDetailStatus(PurchaseNo, 1);
                                            var site = _siteBusiness.GetInfoSite();
                                            var userEmail = _userMasterBusiness.GetEmailByUserName(Session["User"].ToString());
                                            Task task = new Task(() =>
                                            {
                                                PurchasesOrdered report = new PurchasesOrdered();
                                                (report.Bands["DetailReport"] as DetailReportBand).DataSource = report.printTable(ListPurchase, infoSupplier, site);
                                                (report.Bands["DetailReport"] as DetailReportBand).DataMember = "xrTableRowFormat";
                                                Stream stream = new MemoryStream();
                                                report.ExportToPdf(stream);

                                                inforamtion.folio = PurchaseNo;
                                                inforamtion.store = purchase.Item2;
                                                inforamtion.subject = site.site_name + " - Entrada de Mercancia de " + purchase.Item2 + ".";
                                                inforamtion.from = site.site_name;
                                                inforamtion.email = (!string.IsNullOrWhiteSpace(emailSupplier) ? emailSupplier + "," : "") + (!string.IsNullOrWhiteSpace(userEmail) ? userEmail : "");

                                                common.PurchaseEmail(inforamtion, stream, site.site_name);
                                            });
                                            task.Start();
                                        }
                                        else
                                        {
                                            StoreProcedureResult s5 = new StoreProcedureResult { Document = "", ReturnValue = 1045 };
                                            return Json(new { Error = 1045, StoreProcedure = s5 }, JsonRequestBehavior.AllowGet);
                                        }
                                    }
                                    return Json(new { StoreProcedure = document, Error = XML }, JsonRequestBehavior.AllowGet);
                                }
                                //problema al guardar la fecha
                                else
                                {
                                    StoreProcedureResult s6 = new StoreProcedureResult { Document = "", ReturnValue = 1045 };
                                    return Json(new { Error = 1045, StoreProcedure = s6 }, JsonRequestBehavior.AllowGet);
                                }
                            }
                            else
                            {
                                StoreProcedureResult s7 = new StoreProcedureResult { Document = "", ReturnValue = 1006 };
                                return Json(new { Error = 1006, StoreProcedure = s7 }, JsonRequestBehavior.AllowGet);
                            }
                        }
                    }
                    //problema ya existe el xml
                    StoreProcedureResult s8 = new StoreProcedureResult { Document = "", ReturnValue = 1005 };
                    return Json(new { Error = 1005, StoreProcedure = s8 }, JsonRequestBehavior.AllowGet);
                }
                StoreProcedureResult s9 = new StoreProcedureResult { Document = "", ReturnValue = 1045 };
                return Json(new { Error = 1045, StoreProcedure = s9 }, JsonRequestBehavior.AllowGet);

            }
            return Json("SF", JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult ActionNewStoreProcedureOCRGNotXML(string PurchaseNo, string Invoice, string Comment, decimal Diference, decimal Total, DateTime? dateDlls, string currency)
        {
            if (Session["User"] != null)
            {
                var XML = false;
                if (Invoice != null && Comment != null)
                {
                    if (Diference > 0)
                        _PurchaseOrderBusiness.CreateCreditNote(PurchaseNo, Session["User"].ToString(), Diference);

                    if (currency == "USD")
                    {
                        if (dateDlls == null)
                        {
                            StoreProcedureResult StoreProcedure = new StoreProcedureResult() { ReturnValue = 1001, Document = PurchaseNo };
                            return Json(new { StoreProcedure = StoreProcedure, Error = 1001 }, JsonRequestBehavior.AllowGet);
                        }

                        DateTime UpdatedTime = dateDlls ?? DateTime.Now;
                        if (!_PurchaseOrderBusiness.SaveExchangeRateDate(PurchaseNo, UpdatedTime, Session["User"].ToString()))
                        {
                            {
                                StoreProcedureResult StoreProcedure = new StoreProcedureResult() { ReturnValue = 1001, Document = PurchaseNo };
                                return Json(new { StoreProcedure = StoreProcedure, Error = 1001 }, JsonRequestBehavior.AllowGet);
                            }
                        }

                    }

                    if (_PurchaseOrderBusiness.SearchFolioBySupplier(PurchaseNo, Invoice))
                    {
                        StoreProcedureResult s1 = new StoreProcedureResult { Document = "", ReturnValue = 7000 };
                        return Json(new { Error = 7000, StoreProcedure = s1 }, JsonRequestBehavior.AllowGet);
                    }

                    var document = _PurchaseOrderBusiness.StoreProcedureOCRG(PurchaseNo, Session["User"].ToString());
                    if (document.ReturnValue == 0)
                    {
                        XML = _PurchaseOrderBusiness.UpdatePurchaseOrderItemAndTotal(PurchaseNo, Invoice, Session["User"].ToString(), Comment, Total);
                        var infoSupplier = __PurchaseOrderBusiness.GetPurchaseOrderByPo(PurchaseNo);
                        var purchase = _PurchaseOrderBusiness.SupplierInfo(PurchaseNo);

                        var emailSupplier = _MaSupplierContactRepo.EmailsGeneric(purchase.Item1, "Contabilidad");
                        if (emailSupplier == "")
                            emailSupplier = _MaSupplierContactRepo.EmailsGeneric(purchase.Item1, "Ventas");
                        var ListPurchase = __PurchaseOrderBusiness.NewGetAllPurchasesDetail(PurchaseNo);
                        var site = _siteBusiness.GetInfoSite();
                        var userEmail = _userMasterBusiness.GetEmailByUserName(Session["User"].ToString());
                        Task task = new Task(() =>
                        {
                            PurchasesOrdered report = new PurchasesOrdered();
                            (report.Bands["DetailReport"] as DetailReportBand).DataSource = report.printTable(ListPurchase, infoSupplier, site);
                            (report.Bands["DetailReport"] as DetailReportBand).DataMember = "xrTableRowFormat";
                            Stream stream = new MemoryStream();
                            report.ExportToPdf(stream);

                            inforamtion.folio = PurchaseNo;
                            inforamtion.store = purchase.Item2;
                            inforamtion.subject = site.site_name + " - Entrada de mercancia de " + purchase.Item2;
                            inforamtion.from = site.site_name;
                            inforamtion.email = (!string.IsNullOrWhiteSpace(emailSupplier) ? emailSupplier + "," : "") + (!string.IsNullOrWhiteSpace(userEmail) ? userEmail : "");

                            common.PurchaseEmail(inforamtion, stream, site.site_name);
                        });
                        task.Start();
                    }
                    return Json(new { StoreProcedure = document, Error = XML }, JsonRequestBehavior.AllowGet);
                }
                StoreProcedureResult s2 = new StoreProcedureResult { Document = "", ReturnValue = 1045 };
                return Json(new { Error = 1045, StoreProcedure = s2 }, JsonRequestBehavior.AllowGet);
            }
            return Json("SF", JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult ActionStoreProcedureOCRG(List<PurchaseOrderItemModel> OCRC)
        {
            if (Session["User"] != null)
                return Json(_PurchaseOrderBusiness.UpdateOC(OCRC, Session["User"].ToString()), JsonRequestBehavior.AllowGet);

            return Json("SF", JsonRequestBehavior.AllowGet);
        }

        public ActionResult ActionRemovePurchaseOrder(string Purchase)
        {
            if (Session["User"] != null)
                return Json(_PurchaseOrderBusiness.RemovePurchaseOrder(Purchase, Session["User"].ToString()), JsonRequestBehavior.AllowGet);

            return Json("SF", JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult ActionNewUpdateOCRG(List<PurchaseOrderItemModel> OCRC, string Comment)
        {
            if (Session["User"] != null)
                return Json(_PurchaseOrderBusiness.NewUpdateOC(OCRC, Session["User"].ToString(), Comment), JsonRequestBehavior.AllowGet);

            return Json("SF", JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult ActionDiferenceOCRG(string PurchNo)
        {
            if (Session["User"] != null)
                return Json(_PurchaseOrderBusiness.DiferenceOCRG(PurchNo), JsonRequestBehavior.AllowGet);

            return Json("SF", JsonRequestBehavior.AllowGet);
        }

        public ActionResult ActionGetEmailsPurchase(string purchase)
        {
            if (Session["User"] != null)
            {
                int supplier = __PurchaseOrderBusiness.GetSupplierIdByPurchase(purchase);
                var status = __PurchaseOrderBusiness.GetPurchaseOrderByNoAllStatus(purchase);
                if (supplier == 0)
                    return Json(new { success = true, emailSuplier = "" }, JsonRequestBehavior.AllowGet);
                var Recibo = _sysRoleMasterBusiness.GetEmailsByRol(ConfigurationManager.AppSettings["Recibo"].ToString());
                var MC = _sysRoleMasterBusiness.GetEmailsByRol(ConfigurationManager.AppSettings["MesaControl"].ToString());
                var userEmail = _userMasterBusiness.GetEmailByUserName(Session["User"].ToString());

                string emailSupplier = "";
                if (status.purchase_status != 8 || status.purchase_status != 9)
                {
                    emailSupplier = _MaSupplierContactRepo.EmailsGeneric(supplier, "Ventas");
                    if (emailSupplier == "")
                        emailSupplier = _MaSupplierContactRepo.EmailsGeneric(supplier, "Contabilidad");
                }
                else
                {
                    emailSupplier = _MaSupplierContactRepo.EmailsGeneric(supplier, "Contabilidad");
                    if (emailSupplier == "")
                        emailSupplier = _MaSupplierContactRepo.EmailsGeneric(supplier, "Ventas");
                }

                var email = Recibo + "," + MC + "," + userEmail + "," + emailSupplier;
                return Json(new { success = true, emailSuplier = email.ToLower() }, JsonRequestBehavior.AllowGet);
            }
            return Json(new { success = false, emailSuplier = "SF" }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult ActionSendEmailByPurchases(string purchase)
        {
            {
                if (Session["User"] != null)
                {
                    int supplier = __PurchaseOrderBusiness.GetSupplierIdByPurchase(purchase);
                    var status = __PurchaseOrderBusiness.GetPurchaseOrderByNoAllStatus(purchase);
                    if (supplier == 0)
                        return Json(new { success = false, emailSuplier = "" }, JsonRequestBehavior.AllowGet);
                    var Recibo = _sysRoleMasterBusiness.GetEmailsByRol(ConfigurationManager.AppSettings["Recibo"].ToString());
                    var MC = _sysRoleMasterBusiness.GetEmailsByRol(ConfigurationManager.AppSettings["MesaControl"].ToString());
                    var userEmail = _userMasterBusiness.GetEmailByUserName(Session["User"].ToString());
                    var site = _siteBusiness.GetInfoSite();

                    string emailSupplier = "";
                    if (status.purchase_status != 8 && status.purchase_status != 9)
                    {
                        emailSupplier = _MaSupplierContactRepo.EmailsGeneric(supplier, "Ventas");
                        if (emailSupplier == "")
                            emailSupplier = _MaSupplierContactRepo.EmailsGeneric(supplier, "Contabilidad");
                    }
                    else
                    {
                        emailSupplier = _MaSupplierContactRepo.EmailsGeneric(supplier, "Contabilidad");
                        if (emailSupplier == "")
                            emailSupplier = _MaSupplierContactRepo.EmailsGeneric(supplier, "Ventas");
                    }
                    var email = Recibo + "," + MC + "," + userEmail + "," + emailSupplier;

                    var purchase_order = __PurchaseOrderBusiness.GetPurchaseOrderByPo(purchase);

                    Task task = new Task(() =>
                    {
                        if (status.purchase_status != 8 && status.purchase_status != 9)
                        {
                            PurchasesOrder report = new PurchasesOrder();
                            report.Watermark.Text = "COPIA";
                            report.Watermark.TextDirection = DevExpress.XtraPrinting.Drawing.DirectionMode.ForwardDiagonal;
                            report.Watermark.Font = new Font(report.Watermark.Font.FontFamily, 40);
                            report.Watermark.ForeColor = Color.DodgerBlue;
                            report.Watermark.TextTransparency = 150;
                            report.Watermark.ShowBehind = false;
                            report.Watermark.PageRange = "1-99";
                            (report.Bands["DetailReport"] as DetailReportBand).DataSource = report.printTable(__PurchaseOrderBusiness.NewGetAllPurchasesDetail(purchase), purchase_order, _siteBusiness.GetInfoSite());
                            (report.Bands["DetailReport"] as DetailReportBand).DataMember = "xrTableRowFormat";

                            Stream stream = new MemoryStream();
                            report.ExportToPdf(stream);

                            inforamtion.folio = purchase;
                            inforamtion.currecy = purchase_order.currency;
                            inforamtion.store = site.site_name;
                            inforamtion.subject = site.site_name + " - FWD: Orden de Compra de " + _supplierBusiness.GetNameSupplier(status.supplier_id ?? 0);
                            inforamtion.from = site.site_name;
                            inforamtion.date = purchase_order.eta.Value.ToString("dd/MM/yyyy");
                            inforamtion.email = email;

                            EmailDamagedGoodsModel Pdf = new EmailDamagedGoodsModel() { RMA = purchase, Rerpote = stream };
                            common.MailMessageOrderSingle(inforamtion, Pdf, site.site_name);
                        }
                        else
                        {

                            PurchasesOrdered report = new PurchasesOrdered();
                            report.Watermark.Text = "COPIA";
                            report.Watermark.TextDirection = DevExpress.XtraPrinting.Drawing.DirectionMode.ForwardDiagonal;
                            if (status.purchase_status == 8)
                            {
                                report.Watermark.Font = new Font(report.Watermark.Font.FontFamily, 60);
                                report.Watermark.ForeColor = Color.Firebrick;
                            }
                            else
                            {
                                report.Watermark.Font = new Font(report.Watermark.Font.FontFamily, 40);
                                report.Watermark.ForeColor = Color.DodgerBlue;
                            }
                            report.Watermark.TextTransparency = 150;
                            report.Watermark.ShowBehind = false;
                            report.Watermark.PageRange = "1-99";
                            (report.Bands["DetailReport"] as DetailReportBand).DataSource = report.printTable(__PurchaseOrderBusiness.NewGetAllPurchasesDetail(purchase), purchase_order, _siteBusiness.GetInfoSite());
                            (report.Bands["DetailReport"] as DetailReportBand).DataMember = "xrTableRowFormat";
                            Stream stream = new MemoryStream();
                            report.ExportToPdf(stream);

                            inforamtion.folio = purchase;
                            inforamtion.currecy = purchase_order.currency;
                            inforamtion.store = site.site_name;
                            inforamtion.subject = site.site_name + " - FWD: Orden de Compra para " + _supplierBusiness.GetNameSupplier(status.supplier_id ?? 0);
                            inforamtion.from = site.site_name;
                            inforamtion.date = purchase_order.ata.Value.ToString("dd/MM/yyyy");
                            inforamtion.email = email;

                            EmailDamagedGoodsModel Pdf = new EmailDamagedGoodsModel() { RMA = purchase, Rerpote = stream };
                            common.MailMessageOrderSingleOrdered(inforamtion, Pdf, site.site_name);
                        }
                    });
                    task.Start();
                    return Json(new { success = true }, JsonRequestBehavior.AllowGet);

                }
                return Json(new { success = false, emailSuplier = "SF" }, JsonRequestBehavior.AllowGet);
            }
        }
    }
}