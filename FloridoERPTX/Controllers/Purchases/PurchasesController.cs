﻿using App.BLL;
using App.BLL.CodesPackages;
using App.BLL.Combo;
using App.BLL.Configuration;
using App.BLL.CostCenter;
using App.BLL.Expenses;
using App.BLL.Item;
using App.BLL.Ma_Tax;
using App.BLL.MaClass;
using App.BLL.MaCode;
using App.BLL.MovementNumber;
using App.BLL.Order;
using App.BLL.Promotions;
using App.BLL.PurchaseOrder;
using App.BLL.Site;
using App.BLL.Supplier;
using App.Common;
using App.Entities;
using App.Entities.ViewModels.CostCenter;
using App.Entities.ViewModels.DamagedsGoods;
using App.Entities.ViewModels.Expenses;
using App.Entities.ViewModels.InternalRequirement;
using App.Entities.ViewModels.Item;
using App.Entities.ViewModels.PurchaseOrder;
using DevExpress.XtraReports.UI;
using FloridoERPTX.Reports;
using FloridoERPTX.ViewModels.Purchases;
using FloridoERPTX.ViewModels.Warehouse;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using System.Xml;
using System.Xml.Linq;

namespace FloridoERPTX.Controllers.Purchases
{
    public class PurchasesController : Controller
    {
        private readonly ExpensesBusiness _ExpensesBusiness;
        private readonly MovementNumberBusiness _MovementNumberBusiness;
        private readonly PurchaseOrderBusiness __PurchaseOrderBusiness;
        private readonly SysSiteBusiness _SysSiteBusiness;
        private readonly OrderBusiness _OrderBusiness;
        private readonly MaClassBusiness _maClassBusiness;
        private readonly SupplierBusiness _SupplierBussines;
        private readonly MaSupplierContactBusiness _MaSupplierContactBusiness;
        private readonly SendMail information;
        private readonly Common common;
        private readonly SiteConfigBusiness _siteConfigBusiness;
        private readonly PurchaseOrderGlobalBusiness _purchaseOrderGlobalBusiness;
        private readonly SysRoleMasterBusiness _sysRoleMasterBusiness;
        private readonly ItemBusiness _itemBusiness;
        private readonly MaCodeBusiness _maCodeBusiness;
        private readonly PromotionBusiness _promotionBusiness;
        private readonly ComboHeaderBusiness _ComboHeaderBusiness;
        private readonly UserMasterBusiness _UserMasterBusiness;
        private readonly SysSiteConfigBusiness _SiteConfig;
        private readonly SiteBusiness _siteBusiness;
        private readonly CostCenterBusiness _CostCenterBusiness;
        private readonly CodesPackagesBusiness _CodesPackagesBusiness;
        private readonly MaTaxBusiness _sysMaTaxBusiness;
        public PurchasesController()
        {
            _OrderBusiness = new OrderBusiness();
            _MovementNumberBusiness = new MovementNumberBusiness();
            __PurchaseOrderBusiness = new PurchaseOrderBusiness();
            _SysSiteBusiness = new SysSiteBusiness();
            _maClassBusiness = new MaClassBusiness();
            _SupplierBussines = new SupplierBusiness();
            _siteConfigBusiness = new SiteConfigBusiness();
            _ExpensesBusiness = new ExpensesBusiness();
            _purchaseOrderGlobalBusiness = new PurchaseOrderGlobalBusiness();
            _MaSupplierContactBusiness = new MaSupplierContactBusiness();
            _sysRoleMasterBusiness = new SysRoleMasterBusiness();
            information = new SendMail();
            common = new Common();
            _siteBusiness = new SiteBusiness();
            _itemBusiness = new ItemBusiness();
            _maCodeBusiness = new MaCodeBusiness();
            _promotionBusiness = new PromotionBusiness();
            _ComboHeaderBusiness = new ComboHeaderBusiness();
            _UserMasterBusiness = new UserMasterBusiness();
            _SiteConfig = new SysSiteConfigBusiness();
            _CostCenterBusiness = new CostCenterBusiness();
            _CodesPackagesBusiness = new CodesPackagesBusiness();
            _sysMaTaxBusiness = new MaTaxBusiness();
        }


        public ActionResult PURC001()
        {
            if (Session["User"] != null)
                return View();

            return RedirectToAction("Login", "Home");
        }

        public ActionResult PURC0014()
        {
            if (User.Identity.IsAuthenticated)
            {
                var user = _UserMasterBusiness.GetUserMasterByUsername(User.Identity.Name);
                PurchesOrderAndUserModel UserAndPurchase = new PurchesOrderAndUserModel();
                UserAndPurchase.User.UserName = user.cuser;
                UserAndPurchase.User.Name = $"{user.first_name} {user.last_name}";
                UserAndPurchase.User.BuyerDivicion = user.buyer_division ?? "N/A";
                UserAndPurchase.ListPurchase = __PurchaseOrderBusiness.GetAllOrdersEdits();
                return View(UserAndPurchase);
            }
            return RedirectToAction("Login", "Home");
        }

        public ActionResult ORDE001()
        {

            if (Session["User"] != null)
            {
                PurchaseOrderDepartmentModel model = new PurchaseOrderDepartmentModel
                {
                    site = _SiteConfig.GetSiteModel(),
                    Departaments = _maClassBusiness.GetAllDepartments(1)
                };
                return View(model);
            }
            return RedirectToAction("Login", "Home");
        }

        public ActionResult ORDE002()
        {
            if (Session["User"] != null)
                return View(_OrderBusiness.Filter());

            return RedirectToAction("Login", "Home");
        }

        [HttpPost]
        public ActionResult ORDE003(int Folio)
        {
            if (_OrderBusiness.Status(Folio) == 1)
            {
                ViewBag.Id = Folio;
                return View();
            }

            return RedirectToAction("ORDE001", "Purchases");
        }

        public ActionResult OCRG001()
        {
            if (Session["User"] != null)
                return View();

            return RedirectToAction("Login", "Home");
        }

        public ActionResult MENU000()
        {
            if (Session["User"] != null)
                return View();

            return RedirectToAction("Login", "Home");
        }

        public ActionResult MENU001()
        {
            if (Session["User"] != null)
                return View();

            return RedirectToAction("Login", "Home");
        }

        public ActionResult MENU002()
        {
            if (Session["User"] != null)
                return View();

            return RedirectToAction("Login", "Home");
        }

        public ActionResult MENU003()
        {
            if (Session["User"] != null)
                return View();

            return RedirectToAction("Login", "Home");
        }

        public ActionResult MENU004()
        {
            if (Session["User"] != null)
                return View();

            return RedirectToAction("Login", "Home");
        }

        public ActionResult PURC000()
        {
            PurchasesOrderViewModel model = new PurchasesOrderViewModel
            {
                purchasesOrder = __PurchaseOrderBusiness.NewGetAllPurchasesOrders(DateTime.Now.AddDays(-7), DateTime.Now),
                Suppliers = _SupplierBussines.GetAll(),
                Departaments = _maClassBusiness.GetAllDepartments(1),
                CurrencyList = _maCodeBusiness.GetCurrency()
            };
            return View(model);
        }

        public ActionResult ActionGetAllPurchasesOrders(DateTime date1, DateTime date2)
        {
            return Json(new { success = true, Json = __PurchaseOrderBusiness.GetAllPurchasesOrders(date1, date2) }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult ActionGetAllPurchasesOrdersStatusSupplier(DateTime? date1, DateTime? date2, int status, int supplier, string partNumber, string department, string family, string currency)
        {
            return Json(new { success = true, Json = __PurchaseOrderBusiness.GetAllPurchasesOrdersStatusSupplier(date1, date2, status, supplier, partNumber, department, family, currency) }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult ActionGetAllPurchasesOrdersXML(DateTime? date1, DateTime? date2, int supplier, string partNumber, string department, string family, string currency)
        {
            return Json(new { success = true, Json = __PurchaseOrderBusiness.GetAllPurchasesOrdersXML(date1, date2, supplier, partNumber, department, family, currency) }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult ActionGetAllPurchasesOrdersStatusSupplierDetail(DateTime? date1, DateTime? date2, int status, int supplier, string partNumber, string department, string family, string currency)
        {
            return new JsonResult() { Data = __PurchaseOrderBusiness.GetAllPurchasesOrdersStatusSupplierDetail(date1, date2, status, supplier, partNumber, department, family, currency), JsonRequestBehavior = JsonRequestBehavior.AllowGet, MaxJsonLength = int.MaxValue };
        }

        public ActionResult ActionGetAllPurchasesOrdersStatusSupplierDetailXML(DateTime date1, DateTime date2, int supplier, string partNumber, string department, string family, string currency)
        {
            return new JsonResult() { Data = __PurchaseOrderBusiness.GetAllPurchasesOrdersStatusSupplierDetailXML(date1, date2, supplier, partNumber, department, family, currency), JsonRequestBehavior = JsonRequestBehavior.AllowGet, MaxJsonLength = int.MaxValue };
        }

        [HttpGet]
        public ActionResult ActionGetPurchaseDetail(string poNumber, string status)
        {
            return Json(new { success = true, Json = __PurchaseOrderBusiness.NewGetAllPurchasesDetailStatusFormatView(poNumber, status) }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult ActionGetPurchaseDetailMod(string poNumber)
        {
            if (User.Identity.IsAuthenticated)
                return new JsonResult() { Data = __PurchaseOrderBusiness.GetAllPurchasesDetailMod(poNumber), JsonRequestBehavior = JsonRequestBehavior.AllowGet, MaxJsonLength = Int32.MaxValue };

            return Json("SF", JsonRequestBehavior.AllowGet);
        }

        public ActionResult PURC002()
        {
            if (Session["User"] != null)
            {
                SalesPriceChangeViewModel Model = new SalesPriceChangeViewModel
                {
                    Departamentos = _maClassBusiness.GetAllDepartments(1)
                };
                return View(Model);
            }

            return RedirectToAction("Login", "Home");
        }

        public ActionResult PURC003(PurchasesOrderViewModel model)
        {
            if (Session["User"] != null)
            {
                model.Suppliers = _SupplierBussines.GetAll();
                return View(model);
            }
            return RedirectToAction("Login", "Home");
        }

        public ActionResult PURC016()
        {
            if (Session["User"] != null)
                return View();

            return RedirectToAction("Login", "Home");
        }

        [HttpPost]
        public ActionResult ActioncreatePurchasesOrder(PurchasesOrderViewModel arrayHead, List<PurchasesOrderItemViewModel> detailList)
        {
            var sesion = Session["User"].ToString();
            if (sesion != null)
            {
                arrayHead.comment = string.Empty;

                var SiteCode = _SysSiteBusiness.GetSite();
                var numberForlio = _MovementNumberBusiness.ExecuteSp_Get_Next_Document();
                int Folio = 0;
                var program_id = "PURC001.cshtml";

                var addOrderPurchase = __PurchaseOrderBusiness.AddOrderPurchase(numberForlio, Folio, SiteCode.site_code, arrayHead.Supplier, arrayHead.orderDate, arrayHead.currency, arrayHead.comment, sesion, program_id);
                var addOrderItemPurchase = 0;
                foreach (var item in detailList)
                {
                    addOrderItemPurchase = __PurchaseOrderBusiness.AddOrderItemPurchase(numberForlio, item.partNumber, item.quantity, item.unitSize, item.purchasePrice, item.factor, item.itemAmount, item.resultIva, item.resultTax, item.resultTotal, sesion);
                }

                if (addOrderPurchase == 1 || addOrderItemPurchase == 1)
                {
                    __PurchaseOrderBusiness.SendEmailSupplier(arrayHead.Supplier, numberForlio);

                    return Json(new { success = true, responseText = numberForlio }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(new { responseText = "No se guardo con exito" }, JsonRequestBehavior.AllowGet);
                }
            }
            return Json("Sesion", JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult ActionCreateNewPurchasesOrder(PurchaseHeader arrayHead, List<ItemPurchase> detailList)
        {
            var sesion = Session["User"].ToString();
            if (sesion != "")
            {
                var SiteCode = _SysSiteBusiness.GetSite();
                var numberForlio = _MovementNumberBusiness.ExecuteSp_Get_Next_Document();
                var program_id = "PURC001.cshtml";

                var addOrderPurchase = __PurchaseOrderBusiness.AddNewOrderPurchase(arrayHead, SiteCode.site_code, numberForlio, sesion, program_id);
                if (addOrderPurchase)
                {
                    if (__PurchaseOrderBusiness.AddNewOrderItemPurchase(detailList, numberForlio, sesion, program_id))
                    {
                        var site = _siteBusiness.GetInfoSite();
                        var purchases = __PurchaseOrderBusiness.OrderListEmail(numberForlio);
                        var Supplier = _SupplierBussines.GetNameSupplier(arrayHead.SupplierId);
                        var email = _MaSupplierContactBusiness.EmailsGeneric(arrayHead.SupplierId, "Ventas");
                        if (email == "")
                            email = _MaSupplierContactBusiness.EmailsGeneric(arrayHead.SupplierId, "Contabilidad");
                        var Recibo = _sysRoleMasterBusiness.GetEmailsByRol(ConfigurationManager.AppSettings["Recibo"].ToString());
                        var Mesa = _sysRoleMasterBusiness.GetEmailsByRol(ConfigurationManager.AppSettings["MesaControl"].ToString());
                        var UuserEmail = _UserMasterBusiness.GetEmailByUserName(sesion);
                        Task task = new Task(() =>
                        {
                            if (purchases.Count > 0 && !string.IsNullOrWhiteSpace(email.Trim()) && email.Trim() != "N/A")
                            {
                                information.folio = numberForlio;
                                information.store = SiteCode.site_name;
                                information.date = DateTime.Now.Date.ToShortDateString();
                                information.currecy = arrayHead.Currency;
                                information.subject = site.site_name + " - Orden de Compra para " + Supplier + ".";
                                information.from = site.site_name;
                                information.email = (!string.IsNullOrWhiteSpace(email) ? email + "," : "") +
                                        (!string.IsNullOrWhiteSpace(Recibo) ? Recibo + "," : "") +
                                        (!string.IsNullOrWhiteSpace(Mesa) ? Mesa + "," : "") +
                                        (!string.IsNullOrWhiteSpace(UuserEmail) ? UuserEmail + "," : "");

                                ///Antiguo reporte
                                //OrderReportEmail report = new OrderReportEmail();
                                //report.DataSource = report.printTable(purchases, SiteCode.site_name, arrayHead.Currency, arrayHead.PurchaseDate);
                                PurchasesOrder report = new PurchasesOrder();
                                (report.Bands["DetailReport"] as DetailReportBand).DataSource = report.printTable(__PurchaseOrderBusiness.GetAllPurchasesDetail(numberForlio), __PurchaseOrderBusiness.GetPurchaseOrderByPo(numberForlio), _siteBusiness.GetInfoSite());
                                (report.Bands["DetailReport"] as DetailReportBand).DataMember = "xrTableRowFormat";

                                Stream stream = new MemoryStream();
                                Stream streamExcel = new MemoryStream();
                                report.ExportToPdf(stream);
                                report.ExportToXlsx(streamExcel);

                                EmailDamagedGoodsModel Pdf = new EmailDamagedGoodsModel() { RMA = numberForlio, Rerpote = stream, ReporteExcel = streamExcel };
                                common.MailMessageOrderSingle(information, Pdf, site.site_name);
                            }
                        });
                        task.Start();

                        return Json(new { Error = 1000, Document = numberForlio }, JsonRequestBehavior.AllowGet);
                    }
                    else
                    {
                        // Error al guardar los datos
                        __PurchaseOrderBusiness.RemovePurchaseOrder(numberForlio);
                        return Json(new { Error = 1002 });
                    }
                }
                else
                {
                    // Error al guardar
                    return Json(new { Error = 1001 }, JsonRequestBehavior.AllowGet);
                }
            }
            return Json("SF", JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult ActionCompareAmountXml()
        {
            if (Session["User"] != null)
            {
                try
                {
                    var arrayHead = Request["totalPurchases"];
                    string subtotalXML = "";
                    for (int i = 0; i < Request.Files.Count; i++)
                    {
                        //Carga XML
                        HttpPostedFileBase file = Request.Files[i];
                        var xmlDocument = new XmlDocument();
                        xmlDocument.Load(file.InputStream);
                        var xmlN = xmlDocument.GetElementsByTagName("cfdi:Comprobante");
                        var insideXML = xmlN[0];
                        var atributes = insideXML.Attributes;
                        subtotalXML = atributes["Total"].Value;
                    }
                    //valida subtotal de bd contra subtotal de XML
                    if (Decimal.Parse(arrayHead) == Decimal.Parse(subtotalXML))
                    {
                        return Json(new { success = 1, responseText = "Monto Correcto" });
                    }
                    else
                    {
                        var diference = Decimal.Parse(arrayHead) - Decimal.Parse(subtotalXML);
                        return Json(new { success = 8, responseText = "El monto de su orden no concuerda con su XML. <br> Existe una diferencia de " + diference });
                    }
                }
                catch (Exception)
                {
                    return Json(new { success = 8, responseText = "Error al leer archivo XML." }, JsonRequestBehavior.AllowGet);
                }
            }
            else
            {
                return Json(new { success = 8, responseText = "Termino tu sesión." }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public JsonResult ActionCompareAmountXmlOCRC001()
        {
            if (Session["User"] != null)
            {
                try
                {
                    var arrayTotalHead = Request["totalPurchases"];
                    var arraySubHead = Request["subTotalPurchases"];
                    var arrayIVAHead = Request["IVAtotalPurchases"];
                    var arrayIEPSHead = Request["IEPStotalPurchases"];
                    var arrayHeadSupplier = Request["supplier"];
                    var arrayHeadCurrency = Request["currency"];
                    var Uuid = "";
                    string totalXML = "";
                    string supploerNameXML = "";
                    string suppierNameFlorido = "";
                    string currencyXML = "";
                    string Folio = "";
                    string Serie = "";
                    string UuidStr = "";
                    var iepsTotal = 0M;
                    var ivaTotal = 0M;
                    DateTime Fecha = DateTime.Now;
                    for (int i = 0; i < Request.Files.Count; i++)
                    {
                        //Carga XML
                        HttpPostedFileBase file = Request.Files[i];
                        var fileXML = file.FileName;
                        string theFileName = Path.GetFileName(file.FileName);
                        byte[] thePictureAsBytes = new byte[file.ContentLength];
                        using (BinaryReader theReader = new BinaryReader(file.InputStream))
                        {
                            thePictureAsBytes = theReader.ReadBytes(file.ContentLength);
                        }
                        string thePictureDataAsString = Convert.ToBase64String(thePictureAsBytes);
                        var xml = Encoding.UTF8.GetString(Convert.FromBase64String(thePictureDataAsString));
                        string _byteOrderMarkUtf8 = Encoding.UTF8.GetString(Encoding.UTF8.GetPreamble());
                        if (xml.StartsWith(_byteOrderMarkUtf8))
                            xml = xml.Remove(0, _byteOrderMarkUtf8.Length);
                        if (xml[0] != '<')
                            xml = "<" + xml;

                        var doc = XDocument.Parse(xml);
                        var root = doc.Root;
                        var space = root?.Name.NamespaceName;

                        totalXML = (string)root?.Attribute("Total") ?? "0";
                        currencyXML = (string)root?.Attribute("Moneda") ?? "";
                        Fecha = (DateTime)root?.Attribute("Fecha");

                        var rfcSpace = XNamespace.Get("http://www.sat.gob.mx/TimbreFiscalDigital").NamespaceName;
                        var FolioSpace = XNamespace.Get("http://www.sat.gob.mx/registrofiscal").NamespaceName;
                        Uuid = (string)root?.Descendants(XName.Get("TimbreFiscalDigital", rfcSpace)).FirstOrDefault()?.Attribute("UUID").Value.ToString().ToUpper();

                        if ((string)root?.Attribute("Serie") != null)
                            Serie = (string)root?.Attribute("Serie");
                        else
                            Serie = (string)root?.Descendants(XName.Get("CFDIRegistroFiscal", FolioSpace)).FirstOrDefault()?.Attribute("Serie") ?? "";
                        if ((string)root?.Attribute("Folio") != null)
                            Folio = (string)root?.Attribute("Folio");
                        else if ((string)root?.Descendants(XName.Get("CFDIRegistroFiscal", FolioSpace)).FirstOrDefault()?.Attribute("Folio") != null)
                            Folio = (string)root?.Descendants(XName.Get("CFDIRegistroFiscal", FolioSpace)).FirstOrDefault()?.Attribute("Folio");
                        else
                            Folio = Uuid.Substring(Uuid.Length - 6); //Si contiene folio tomar los ultimos 5 digitos

                        supploerNameXML = (string)root?.Element(XName.Get("Emisor", space))?.Attribute("Rfc");
                        suppierNameFlorido = (string)root?.Element(XName.Get("Receptor", space))?.Attribute("Rfc");

                        try
                        {
                            iepsTotal = root.Element(XName.Get("Impuestos", space)).Element(XName.Get("Traslados", space)).Descendants(XName.Get("Traslado", space)).Where(a => (int)a.Attribute("Impuesto") == 3).Select(e => (decimal?)e.Attribute("Importe") ?? 0).ToList().Sum();
                        }
                        catch (Exception ex)
                        {
                            string msg = ex.Message;
                            iepsTotal = 0;
                        }
                        try
                        {
                            ivaTotal = root.Element(XName.Get("Impuestos", space)).Element(XName.Get("Traslados", space)).Descendants(XName.Get("Traslado", space)).Where(a => (int)a.Attribute("Impuesto") == 2).Select(e => (decimal?)e.Attribute("Importe") ?? 0).ToList().Sum();
                        }
                        catch (Exception ex)
                        {
                            string msg = ex.Message;
                            ivaTotal = 0;
                        }
                    }
                    var newInvoice = "";
                    if (Folio != null)
                        newInvoice = Serie + Folio;
                    else
                        newInvoice = UuidStr.Substring(UuidStr.Length - 6);

                    if (suppierNameFlorido.Trim().ToUpper() == ConfigurationManager.AppSettings["RfcFlorido"].ToString())
                    {
                        //valida subtotal de bd contra subtotal de XML
                        if (currencyXML.ToUpper() == arrayHeadCurrency.ToUpper())
                        {
                            if (supploerNameXML.Trim().ToUpper() == arrayHeadSupplier.Trim().ToUpper())
                            {
                                var DiferenceTotal = Decimal.Parse(arrayTotalHead) - Decimal.Parse(totalXML);
                                var DiferenceIVA = Decimal.Parse(arrayIVAHead) - ivaTotal;
                                var DiferenceIEPS = Decimal.Parse(arrayIEPSHead) - iepsTotal;

                                if (currencyXML.Trim().ToUpper() == "MXN")
                                {
                                    if (DiferenceIVA <= 5 && DiferenceIVA >= -5)
                                        DiferenceIVA = 0;
                                    else if (DiferenceIVA < -5)
                                        DiferenceIVA = (DiferenceIVA * -1);

                                    if (DiferenceIEPS <= 5 && DiferenceIEPS >= -5)
                                        DiferenceIEPS = 0;
                                    else if (DiferenceIEPS < -5)
                                        DiferenceIEPS = (DiferenceIEPS * -1);

                                    if (DiferenceTotal <= 5 && DiferenceTotal >= -5)
                                    {
                                        return Json(new { success = 1, uuid = Uuid, fecha = Fecha.ToShortDateString(), invoice = newInvoice, currency = currencyXML, diferenceTotal = 0, diferenceIEPS = DiferenceIEPS, diferenceIVA = DiferenceIVA, responseText = "Monto Correcto" });
                                    }
                                    else if (DiferenceTotal < -5)
                                    {
                                        return Json(new { success = 1, uuid = Uuid, fecha = Fecha.ToShortDateString(), invoice = newInvoice, currency = currencyXML, diferenceTotal = (DiferenceTotal * -1), diferenceIEPS = DiferenceIEPS, diferenceIVA = DiferenceIVA, responseText = "Monto Correcto" });
                                    }
                                    //var diference = Decimal.Parse(arrayHead) - Decimal.Parse(subtotalXML);
                                }
                                else if (currencyXML.Trim().ToUpper() == "USD")
                                {

                                    if (DiferenceIVA <= 5m && DiferenceIVA >= -0.5m)
                                        DiferenceIVA = 0;
                                    else if (DiferenceIVA < -0.5m)
                                        DiferenceIVA = (DiferenceIVA * -1);

                                    if (DiferenceIEPS <= 0.5m && DiferenceIEPS >= -0.5m)
                                        DiferenceIEPS = 0;
                                    else if (DiferenceIEPS < -0.5m)
                                        DiferenceIEPS = (DiferenceIEPS * -1);

                                    if (DiferenceTotal <= 0.5m && DiferenceTotal >= -0.5m)
                                    {
                                        return Json(new { success = 1, uuid = Uuid, fecha = Fecha.ToShortDateString(), invoice = newInvoice, currency = currencyXML, diferenceTotal = 0, diferenceIEPS = DiferenceIEPS, diferenceIVA = DiferenceIVA, responseText = "Monto Correcto" });
                                    }
                                    else if (DiferenceTotal < -0.5m)
                                    {
                                        return Json(new { success = 1, uuid = Uuid, fecha = Fecha.ToShortDateString(), invoice = newInvoice, currency = currencyXML, diferenceTotal = (DiferenceTotal * -1), diferenceIEPS = DiferenceIEPS, diferenceIVA = DiferenceIVA, responseText = "Monto Correcto" });
                                    }
                                    //var diference = Decimal.Parse(arrayHead) - Decimal.Parse(subtotalXML);
                                }
                                return Json(new { success = 8, responseText = $"El monto de su orden no concuerda con su XML. <br> Existe una diferencia de  ${DiferenceTotal } favor de editar los costos hacia abajo de la orden de compra." });
                            }
                            return Json(new { success = 8, responseText = "RFC de proveedor incorrecto" });
                        }
                        else
                            return Json(new { success = 8, responseText = "Moneda incorrecta" });
                    }
                    else
                        return Json(new { success = 8, responseText = "Contacte al proveedor, debido a que este archivo XML no va dirigo a DISTRIBUIDORA EL FLORIDO, S.A. DE C.V." });

                }
                catch (Exception e)
                {
                    var msg = e.Message;
                    return Json(new { success = 8, responseText = "Error al leer archivo XML." }, JsonRequestBehavior.AllowGet);
                }
            }
            else
            {
                return Json(new { success = 8, responseText = "Termino tu sesión." }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public JsonResult ActionCancelPurchaseOrder(string PurchaseOrder, string comment)
        {
            if (Session["User"] != null)
            {
                string document = "";
                int response = 1;
                if (__PurchaseOrderBusiness.UpdateCommentInCancelPO(PurchaseOrder, comment, Session["User"].ToString()))
                {
                    var result = __PurchaseOrderBusiness.CancelFinishedPurchaseOrder(PurchaseOrder, Session["User"].ToString(), out document, out response);
                    if (result)
                    {
                        var AreaContable = _sysRoleMasterBusiness.GetEmailsByRol(ConfigurationManager.AppSettings["AreaContable"].ToString());
                        var Recibo = _sysRoleMasterBusiness.GetEmailsByRol(ConfigurationManager.AppSettings["Recibo"].ToString());
                        var MC = _sysRoleMasterBusiness.GetEmailsByRol(ConfigurationManager.AppSettings["MesaControl"].ToString());
                        var Gerente = _sysRoleMasterBusiness.GetEmailsByRol(ConfigurationManager.AppSettings["Gerente"].ToString());
                        var site = _siteBusiness.GetInfoSite();

                        var orderDetail = __PurchaseOrderBusiness.NewGetAllPurchasesDetailStatus(PurchaseOrder, 8);
                        var order = __PurchaseOrderBusiness.GetPurchaseOrderByPo(PurchaseOrder);

                        Task task = new Task(() =>
                        {
                            CanceledPurchasesOrder report = new CanceledPurchasesOrder();
                            report.Watermark.Text = "CANCELADO";
                            report.Watermark.TextDirection = DevExpress.XtraPrinting.Drawing.DirectionMode.ForwardDiagonal;
                            report.Watermark.Font = new Font(report.Watermark.Font.FontFamily, 60);
                            report.Watermark.ForeColor = Color.Firebrick;
                            report.Watermark.TextTransparency = 150;
                            report.Watermark.ShowBehind = false;
                            report.Watermark.PageRange = "1-99";
                            report.DataSource = report.printTable(orderDetail, order, site);

                            Stream stream = new MemoryStream();
                            report.ExportToPdf(stream);
                            information.subject = site.site_name + " - Cancelacion de orden de compra";
                            information.from = site.site_name;
                            information.email = Recibo + "," + MC + "," + Gerente + "," + AreaContable;

                            string status = common.MailMessageCanceledPurchase(information, stream);
                        });
                        task.Start();

                        return Json(new { Status = true, Response = response, Document = document }, JsonRequestBehavior.AllowGet);
                    }
                    else
                        return Json(new { Status = false, Response = response, Document = document }, JsonRequestBehavior.AllowGet);
                }
                else
                    return Json(new { Status = false, Response = response, Document = document }, JsonRequestBehavior.AllowGet);
            }

            return Json(new { Status = false, Response = "Terminó tu sesión." }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult ActionGetFinishedPurchasesOrders()
        {
            return Json(new { success = true, Json = __PurchaseOrderBusiness.GetFinishedPurchasesOrders(DateTime.Now.AddDays(-7), DateTime.Now) }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult ActionGetFinishedPurchasesOrdersByDate(DateTime beginDate, DateTime endDate)
        {
            if (Session["User"] != null)
            {
                DateTime now = DateTime.Now;
                if (beginDate.Month != now.Month || beginDate.Year != now.Year)
                    return Json(new { success = false, Json = "D" }, JsonRequestBehavior.AllowGet);
                var orders = __PurchaseOrderBusiness.GetFinishedPurchasesOrders(beginDate, endDate);
                return Json(new { success = true, Json = __PurchaseOrderBusiness.GetFinishedPurchasesOrders(beginDate, endDate) }, JsonRequestBehavior.AllowGet);
            }
            return Json(new { success = false, Json = "SF" }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult ActionGetAllFinishedPurchasesOrders()
        {
            return Json(new { success = true, Json = __PurchaseOrderBusiness.GetAllFinishedPurchasesOrders() }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult PURC004()
        {
            if (Session["User"] != null)
                return View(__PurchaseOrderBusiness.GetAllFinishedPurchasesOrders());

            return RedirectToAction("Login", "Home");
        }

        public async Task<Result<List<InternalRequirementModel>>> GetItemsCreditors()
        {
            return await Requests.Post("http://localhost:60532/InternalRequirementHeader/ActionGetAllItemsCreditors").Send<List<InternalRequirementModel>>();
        }

        public async Task<Result<List<InternalRequirementPOModel>>> GetAllItemsCreditorsBySite(string site)
        {
            return await Requests.Post("http://localhost:60532/InternalRequirementHeader/ActionGetAllItemsCreditorsBySite", site).Send<List<InternalRequirementPOModel>>();
        }

        public ActionResult PURC005()
        {
            if (Session["User"] != null)
                return View();

            return RedirectToAction("Login", "Home");
        }

        public ActionResult PURC006()
        {
            if (Session["User"] != null)
                return View();

            return RedirectToAction("Login", "Home");
        }

        public ActionResult PURCH022()
        {
            if (Session["User"] != null)
            {
                DateTime date = DateTime.Today;
                var firstDayOfMonth = DateTime.Now;
                ExpensesReportListModel model = new ExpensesReportListModel
                {
                    Ma_SupplierList = _SupplierBussines.GetAllByType("Acreedor"),
                    ExpensesReportModelList = _ExpensesBusiness.ExpensesReport("", "", firstDayOfMonth, firstDayOfMonth, "", "", "9"),
                    ItemCreditorsCategoryList = _ExpensesBusiness.AllCreditorsCategories(),
                    ListCategory = _maCodeBusiness.GetListGeneric("MAQUINERY_TYPE")
                };

                return View(model);
            }
            return RedirectToAction("Login", "Home");
        }

        public ActionResult PURCH023()
        {
            if (Session["User"] != null)
            {
                DateTime date = DateTime.Today;
                var firstDayOfMonth = new DateTime(date.Year, date.Month, 1);
                var lastDayOfMonth = firstDayOfMonth.AddMonths(1).AddDays(-1);
                ExpensesReportListModel model = new ExpensesReportListModel
                {
                    Ma_SupplierList = _SupplierBussines.GetAllByType("Acreedor"),
                    ExpensesReportModelList = _ExpensesBusiness.ExpensesReport("", "", firstDayOfMonth, lastDayOfMonth, "", "", "9"),
                    ItemCreditorsCategoryList = _ExpensesBusiness.AllCreditorsCategories(),
                    ListCategory = _maCodeBusiness.GetListGeneric("MAQUINERY_TYPE")
                };

                return View(model);
            }
            return RedirectToAction("Login", "Home");
        }

        public ActionResult PURCH024()
        {
            if (Session["User"] != null)
            {
                DateTime date = DateTime.Today;
                var firstDayOfMonth = new DateTime(date.Year, date.Month - 3, 1);
                var lastDayOfMonth = firstDayOfMonth.AddMonths(4).AddDays(-1);
                ExpensesReportListModel model = new ExpensesReportListModel
                {
                    Ma_SupplierList = _SupplierBussines.GetAllByType("Acreedor"),
                    ExpensesReportModelList = _ExpensesBusiness.ExpensesReport("", "", firstDayOfMonth, lastDayOfMonth, "", "", "1"),
                    ItemCreditorsCategoryList = _ExpensesBusiness.AllCreditorsCategories(),
                    ListCategory = _maCodeBusiness.GetListGeneric("MAQUINERY_TYPE")
                };

                return View(model);
            }
            return RedirectToAction("Login", "Home");
        }

        public ActionResult PURCH025()
        {
            if (Session["User"] != null)
            {
                DateTime date = DateTime.Today;
                var firstDayOfMonth = new DateTime(date.Year, date.Month - 3, 1);
                var lastDayOfMonth = firstDayOfMonth.AddMonths(4).AddDays(-1);
                ExpensesReportListModel model = new ExpensesReportListModel
                {
                    Ma_SupplierList = _SupplierBussines.GetAllByType("Acreedor"),
                    ExpensesReportModelList = _ExpensesBusiness.ExpensesReport("", "", firstDayOfMonth, lastDayOfMonth, "", "", "2"),
                    ItemCreditorsCategoryList = _ExpensesBusiness.AllCreditorsCategories(),
                    ListCategory = _maCodeBusiness.GetListGeneric("MAQUINERY_TYPE")
                };

                return View(model);
            }
            return RedirectToAction("Login", "Home");
        }

        public ActionResult PURCH026()
        {
            if (Session["User"] != null)
            {
                DateTime date = DateTime.Today;
                var firstDayOfMonth = new DateTime(date.Year, date.Month - 3, 1);
                var lastDayOfMonth = firstDayOfMonth.AddMonths(4).AddDays(-1);
                ExpensesReportListModel model = new ExpensesReportListModel
                {
                    Ma_SupplierList = _SupplierBussines.GetAllByType("Acreedor"),
                    ExpensesReportModelList = _ExpensesBusiness.ExpensesReport("", "", firstDayOfMonth, lastDayOfMonth, "", "", "3"),
                    ItemCreditorsCategoryList = _ExpensesBusiness.AllCreditorsCategories(),
                    ListCategory = _maCodeBusiness.GetListGeneric("MAQUINERY_TYPE")
                };

                return View(model);
            }
            return RedirectToAction("Login", "Home");
        }

        public ActionResult PURC007()
        {
            if (Session["User"] != null)
            {
                DateTime date = DateTime.Today;
                var firstDayOfMonth = new DateTime(date.Year, date.Month, 1);
                var lastDayOfMonth = firstDayOfMonth.AddMonths(1).AddDays(-1);
                ExpensesReportListModel model = new ExpensesReportListModel
                {
                    Ma_SupplierList = _SupplierBussines.GetAllByType("Acreedor"),
                    ExpensesReportModelList = _ExpensesBusiness.ExpensesReport("", "", firstDayOfMonth, lastDayOfMonth, "", "", ""),
                    ItemCreditorsCategoryList = _ExpensesBusiness.AllCreditorsCategories(),
                    ListCategory = _maCodeBusiness.GetListGeneric("MAQUINERY_TYPE")
                };

                return View(model);
            }
            return RedirectToAction("Login", "Home");
        }

        public ActionResult PURC008()
        {
            if (Session["User"] != null)
            {
                var site = _siteConfigBusiness.GetSiteCode();
                return View(_purchaseOrderGlobalBusiness.GetPurchaseOrderByStatus(DateTime.Now.AddDays(-7), DateTime.Now, 0, site));
            }
            return RedirectToAction("Login", "Home");
        }

        // configuracion de permisos para ApprovalRoute
        public ActionResult PURC009()
        {
            if (Session["User"] != null)
                return View();

            return RedirectToAction("Login", "Home");
        }

        public ActionResult PURC0010(PURCH0010ViewModel Model)
        {
            if (Session["User"] != null)
            {
                Model.Items = _itemBusiness.GetAllProductsToDropDown().Select(e => new ItemDropDownModel
                {
                    part_description = e.Description,
                    part_number = e.PartNumber
                }).ToList();
                Model.Site = new List<SITES>();
                Model.CurrentSite = _siteConfigBusiness.GetSiteCode();
                Model.Ofertas = _maCodeBusiness.GetListTypeOffer();
                DateTime date = DateTime.Today;
                var firstDayOfMonth = new DateTime(date.Year, date.Month, 1);
                var lastDayOfMonth = firstDayOfMonth.AddMonths(1).AddDays(-1);
                Model.Promotions = _promotionBusiness.GetAllPromotions("", "", null, null);
                return View(Model);
            }
            return RedirectToAction("Login", "Home");
        }

        public ActionResult PURC011()
        {
            if (Session["User"] != null)
                return View(_ComboHeaderBusiness.GetComboHeder());

            return RedirectToAction("Login", "Home");
        }

        public ActionResult PURC012()
        {
            if (Session["User"] != null)
                return View();

            return RedirectToAction("Login", "Home");
        }

        public ActionResult PURC013()
        {
            if (Session["User"] != null)
            {
                PUCH013ViewModel Model = new PUCH013ViewModel
                {
                    Suplier = _SupplierBussines.GetAllOnlySuppliers()
                };
                return View(Model);
            }
            return RedirectToAction("Login", "Home");
        }

        public ActionResult PURC014()
        {
            if (Session["User"] != null)
            {
                ViewModelPurch014 model = new ViewModelPurch014
                {
                    Suplier = _SupplierBussines.GetAllOnlyCreditors(),
                    Code = _maCodeBusiness.GetListGeneric("MAQUINERY_TYPE").Where(w => w.vkey_seq != 0).ToList(), //Remover Servicios
                };
                return View(model);
            }

            return RedirectToAction("Login", "Home");
        }

        public ActionResult PURCH015()
        {
            if (Session["User"] != null)
            {
                PURCH015ViewModel Model = new PURCH015ViewModel
                {
                    Suplier = _SupplierBussines.GetAllOnlySuppliers(),
                    Departamentos = _maClassBusiness.GetAllDepartments(1)
                };
                return View();
            }
            return RedirectToAction("Login", "Home");
        }

        public ActionResult PURC017()
        {
            if (Session["User"] != null)
            {
                PurchaseOrderDepartmentModel model = new PurchaseOrderDepartmentModel
                {
                    Departaments = _maClassBusiness.GetAllDepartments(1),
                    Suppliers = _SupplierBussines.GetAll()
                };
                return View(model);
            }

            return RedirectToAction("Login", "Home");
        }

        public ActionResult PURC018()
        {
            if (Session["User"] != null)
                return View();

            return RedirectToAction("Login", "Home");
        }

        public ActionResult COST001()
        {
            if (Session["User"] != null)
            {
                var model = new GICostCenterViewModel
                {
                    GICostCenterRecords = _CostCenterBusiness.GetCostCenterRecords(DateTime.Now.AddDays(-7), DateTime.Now),
                    CostCenters = _maCodeBusiness.GetListGeneric("COST_CENTER")
                };
                return View(model);
            }

            return RedirectToAction("Login", "Home");
        }


        public ActionResult COST002()
        {
            if (Session["User"] != null)
            {
                var model = new GICostCenterViewModel
                {
                    GICostCenterRecords = _CostCenterBusiness.GetCostCenterRecords(DateTime.Now.AddDays(-7), DateTime.Now, 1),
                    CostCenters = _maCodeBusiness.GetListGeneric("COST_CENTER")
                };
                return View(model);
            }

            return RedirectToAction("Login", "Home");
        }

        public ActionResult ActionGetReportPurchase(DateTime? date1, DateTime? date2, int status, int supplier, string partNumber, string department, string family, string currency)
        {
            if (Session["User"] != null)
            {
                var purchases = __PurchaseOrderBusiness.GetAllPurchasesOrdersStatusSupplier(date1, date2, status, supplier, partNumber, department, family, currency);
                string nameUser = _UserMasterBusiness.getCompleteName(Session["User"].ToString());
                PurchaseOrderReport reportPurchaseOrders = new PurchaseOrderReport();
                (reportPurchaseOrders.Bands["DetailReport"] as DetailReportBand).DataSource = reportPurchaseOrders.printTable(purchases, _siteBusiness.GetInfoSite(), date1, date2, nameUser);
                (reportPurchaseOrders.Bands["DetailReport"] as DetailReportBand).DataMember = "xrTableRowFormat";
                return PartialView("~/Views/ReportsViews/PurchaseOrderReport.cshtml", reportPurchaseOrders);
            }
            return Json("SF", JsonRequestBehavior.AllowGet);
        }

        public ActionResult ActionGetReportPurchaseXML(DateTime? date1, DateTime? date2, int supplier, string partNumber, string department, string family, string currency)
        {
            if (Session["User"] != null)
            {
                var purchases = __PurchaseOrderBusiness.GetAllPurchasesOrdersXML(date1, date2, supplier, partNumber, department, family, currency);
                string nameUser = _UserMasterBusiness.getCompleteName(Session["User"].ToString());
                PurchaseOrderReport reportPurchaseOrders = new PurchaseOrderReport();
                (reportPurchaseOrders.Bands["DetailReport"] as DetailReportBand).DataSource = reportPurchaseOrders.printTable(purchases, _siteBusiness.GetInfoSite(), date1, date2, nameUser);
                (reportPurchaseOrders.Bands["DetailReport"] as DetailReportBand).DataMember = "xrTableRowFormat";
                return PartialView("~/Views/ReportsViews/PurchaseOrderReport.cshtml", reportPurchaseOrders);
            }
            return Json("SF", JsonRequestBehavior.AllowGet);
        }

        public ActionResult ActionGetReportPurchaseDetail(DateTime? date1, DateTime? date2, int status, int supplier, string partNumber, string department, string family, string currency)
        {
            if (Session["User"] != null)
            {
                var purchases = __PurchaseOrderBusiness.GetAllPurchasesOrdersStatusSupplierDetail(date1, date2, status, supplier, partNumber, department, family, currency);
                string nameUser = _UserMasterBusiness.getCompleteName(Session["User"].ToString());
                PurchaseOrderReportDetail reportPurchaseOrders = new PurchaseOrderReportDetail();
                (reportPurchaseOrders.Bands["DetailReport"] as DetailReportBand).DataSource = reportPurchaseOrders.printTable(purchases, _siteBusiness.GetInfoSite(), date1, date2, nameUser);
                (reportPurchaseOrders.Bands["DetailReport"] as DetailReportBand).DataMember = "xrTableRowFormat";
                return PartialView("~/Views/ReportsViews/PurchaseOrderReport.cshtml", reportPurchaseOrders);
            }
            return Json("SF", JsonRequestBehavior.AllowGet);
        }

        public ActionResult ActionGetReportPurchaseDetailXML(DateTime date1, DateTime date2, int supplier, string partNumber, string department, string family, string currency)
        {
            if (Session["User"] != null)
            {
                var purchases = __PurchaseOrderBusiness.GetAllPurchasesOrdersStatusSupplierDetailXML(date1, date2, supplier, partNumber, department, family, currency);
                string nameUser = _UserMasterBusiness.getCompleteName(Session["User"].ToString());
                PurchaseOrderReportDetail reportPurchaseOrders = new PurchaseOrderReportDetail();
                (reportPurchaseOrders.Bands["DetailReport"] as DetailReportBand).DataSource = reportPurchaseOrders.printTable(purchases, _siteBusiness.GetInfoSite(), date1, date2, nameUser);
                (reportPurchaseOrders.Bands["DetailReport"] as DetailReportBand).DataMember = "xrTableRowFormat";
                return PartialView("~/Views/ReportsViews/PurchaseOrderReport.cshtml", reportPurchaseOrders);
            }
            return Json("SF", JsonRequestBehavior.AllowGet);
        }

        public ActionResult PURC019()
        {
            if (Session["User"] != null)
                return View(_CodesPackagesBusiness.GetCodesPackages());

            return RedirectToAction("Login", "Home");
        }

        public ActionResult ActionGetCodePackages(string OriginPartNumber)
        {
            if (Session["User"] != null)
                return Json(new { result = true, model = _CodesPackagesBusiness.GetCodePackagesByOriginPartNumber(OriginPartNumber) }, JsonRequestBehavior.AllowGet);

            return Json(new { result = "SF" }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult PURC020()
        {
            var Purchase = new PurchaseAndMaCode()
            {
                PurchaseList = __PurchaseOrderBusiness.GetAllOrdersWithoutXML()
            };
            return View(Purchase);
        }

        public ActionResult PURC021()
        {
            PurchasesOrderViewModel model = new PurchasesOrderViewModel
            {
                purchasesOrder = __PurchaseOrderBusiness.NewGetAllPurchasesOrdersXML(DateTime.Now.AddDays(-7), DateTime.Now),
                Suppliers = _SupplierBussines.GetAll(),
                Departaments = _maClassBusiness.GetAllDepartments(1),
                CurrencyList = _maCodeBusiness.GetCurrency()
            };
            return View(model);
        }
        [HttpGet]
        public ActionResult PURCH002()
        {
            if (Session["User"] != null)
            {
                AddSupplierViewModel Model = new AddSupplierViewModel
                {
                    TipoProveedor = _maCodeBusiness.GetAllCodeByCode("SUPPLIER_TYPE"),
                    TipoProveedorIVA = _sysMaTaxBusiness.GetAllIVANormal()
                };
                return View(Model);
            }
            return RedirectToAction("Login", "Home");
        }
        //Listado Proveedores
        [HttpGet]
        public ActionResult PURCH003()
        {
            if (Session["User"] != null)
            {
                ListSuppliersViewModel model = new ListSuppliersViewModel
                {
                    TipoProveedor = _maCodeBusiness.GetAllCodeByCode("SUPPLIER_TYPE"),
                    TipoProveedorIVA = _sysMaTaxBusiness.GetAllIVANormal()
                };
                return View(model);
            }

            return RedirectToAction("Login", "Home");
        }
    }
}