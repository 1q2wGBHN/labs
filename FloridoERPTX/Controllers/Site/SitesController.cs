﻿using App.BLL.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace FloridoERPTX.Controllers.Site
{
    public class SitesController : Controller
    {
        // GET: Sites
        //public ActionResult Index()
        //{
        //    return View();
        //}
        private readonly SysSiteBusiness _SiteBusiness;
        public SitesController()
        {
            _SiteBusiness = new SysSiteBusiness();
        }
        public ActionResult ActionGetSite()
        {
            return Json(_SiteBusiness.GetSite(), JsonRequestBehavior.AllowGet);
        }
    }
}