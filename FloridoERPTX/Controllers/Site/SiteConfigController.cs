﻿using App.BLL.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using App.BLL.Site;

namespace FloridoERPTX.Controllers.Site
{
    public class SiteConfigController : Controller
    {
        // GET: SiteConfig
        private readonly SysSiteConfigBusiness _SiteConfig;
        private readonly SiteConfigBusiness _siteConfigBusiness;
        public SiteConfigController()
        {
            _SiteConfig = new SysSiteConfigBusiness();
            _siteConfigBusiness = new SiteConfigBusiness();
        }
        public ActionResult ActionGetSite()
        {
            return Json(_SiteConfig.GetSite(), JsonRequestBehavior.AllowGet);
        }
        public ActionResult ActionGetSiteSuccess()
        {
            var json = _SiteConfig.GetSite();
            return Json(new { success = true, Json = json }, JsonRequestBehavior.AllowGet);
        }
        public ActionResult ActionGetSiteConfig()
        {
            var json = _siteConfigBusiness.GetSiteConfig();
            return Json(json , JsonRequestBehavior.AllowGet);
        }
    }
}