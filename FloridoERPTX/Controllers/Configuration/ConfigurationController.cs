﻿using App.BLL;
using App.BLL.Configuration;
using App.BLL.MaCode;
using App.BLL.Sales;
using App.Common;
using FloridoERPTX.ViewModels;
using FloridoERPTX.ViewModels.Configuration;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace FloridoERPTX.Controllers.Configuration
{
    [Authorize]
    public class ConfigurationController : Controller
    {
        private readonly SysPageMasterBusiness _PageMasterRepo;
        private readonly SysRoleMasterBusiness _RoleMasterRepo;
        private readonly SysRolePageBusiness _RolePageRepo;
        private readonly UserMasterBusiness _userMasterRepo;
        private readonly SysRoleUserBusiness _RoleUserRepo;
        private readonly UserPwdBusiness _userPwdBusiness;
        private readonly SalesPriceChangeBusiness _salesPriceChangeBusiness;
        private readonly MaCodeBusiness _maCodeBusiness;


        public ConfigurationController()
        {
            _PageMasterRepo = new SysPageMasterBusiness();
            _RoleMasterRepo = new SysRoleMasterBusiness();
            _RolePageRepo = new SysRolePageBusiness();
            _userMasterRepo = new UserMasterBusiness();
            _RoleUserRepo = new SysRoleUserBusiness();
            _userPwdBusiness = new UserPwdBusiness();
            _salesPriceChangeBusiness = new SalesPriceChangeBusiness();
            _maCodeBusiness = new MaCodeBusiness();
        }


        public bool TimeUpdateWeighingMachine(int basculeType)
        {
            try
            {
                var culture = new System.Globalization.CultureInfo("es-ES");
                string WeighingMachine = "";
                if (basculeType == 1)
                    WeighingMachine = "TOLEDO";
                else if (basculeType == 2)
                    WeighingMachine = "MT";
                else if (basculeType == 3)
                    return _maCodeBusiness.GetWeighingMachineCAS();
                else
                    return false;
                string path = "C:\\" + WeighingMachine + "\\";

                if (System.IO.Directory.Exists(path))
                    return true;
                else
                    return false;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public string GetProcessWeightMachineToledo()
        {
            System.Diagnostics.Process si = new System.Diagnostics.Process();
            //si.StartInfo.WorkingDirectory = "C:\\Windows\\system32";
            si.StartInfo.UseShellExecute = false;
            si.StartInfo.FileName = "cmd.exe";
            si.StartInfo.WindowStyle = System.Diagnostics.ProcessWindowStyle.Hidden;
            //si.StartInfo.FileName = "wmic";
            //si.StartInfo.Arguments = " process where \"commandline like '%serversab2%'\" get ExecutablePath";// wmic
            //si.StartInfo.Arguments = " path win32_process get commandline | find \"serverSAB\" "; //wmic
            si.StartInfo.Arguments = " /C netstat -a -n |find \"1100\" | find \"LISTENING\" "; //Revisar puerto
            si.StartInfo.CreateNoWindow = true;
            si.StartInfo.RedirectStandardInput = true;
            si.StartInfo.RedirectStandardOutput = true;
            si.StartInfo.RedirectStandardError = true;
            si.Start();
            string output = si.StandardOutput.ReadToEnd();
            si.Close();

            if (!output.ToLower().Contains("1100"))
            {
                //System.Diagnostics.Process siJava = new System.Diagnostics.Process(); //Ejecutar el servidor de basculas
                //siJava.StartInfo.WorkingDirectory = "C:\\TOLEDO";
                //siJava.StartInfo.UseShellExecute = false;
                //siJava.StartInfo.FileName = "java";
                //siJava.StartInfo.Arguments = "-jar serverSAB2.9.jar";
                //siJava.StartInfo.CreateNoWindow = true;
                //siJava.StartInfo.RedirectStandardInput = false;
                //siJava.StartInfo.RedirectStandardOutput = true;
                //siJava.StartInfo.RedirectStandardError = true;
                //siJava.Start();
                ////string outputJava = siJava.StandardOutput.ReadToEnd(); //Evitar la output de la conola (no es necesario)
                //siJava.Close();
                return "black";
                //wmic process where "commandline like '%serversab%'" call terminate
            }
            return "blue";
        }


        public ActionResult MENADMS000()
        {
            return View();
        }

        public ActionResult ADMS001()
        {
            ConfigurationAddPages model = new ConfigurationAddPages
            {
                Page = _PageMasterRepo.GetAllPagesAdmin()
            };
            return View(model);
        }

        public ActionResult ADMS002()
        {
            ConfigurationAddRoles model = new ConfigurationAddRoles
            {
                RolesCollection = _RoleMasterRepo.GetAllRolesAdmin(),
                active_flag = true
            };
            return View(model);
        }

        public ActionResult ADMS003()
        {
            ConfigurationAddRoles model = new ConfigurationAddRoles
            {
                RolesCollection = _RoleMasterRepo.GetAllRolesAdmin(),
                active_flag = true
            };
            return View(model);
        }

        public ActionResult ADMS004()
        {
            //ConfigurationAddPagesUser model = new ConfigurationAddPagesUser();
            //model.Users = _userMasterRepo.GetAllUsersByDrop();
            return View(_userMasterRepo.GetAllUsersByDrop());
        }

        public ActionResult ADMS005()
        {
            return View();
        }

        public ActionResult ADMS007()
        {
            return View();
        }

        //Sub-Menu Basculas
        public ActionResult ADMS020()
        {
            return View();
        }

        //Actualizar Basculas
        public ActionResult ADMS021()
        {
            string toledoServer = "black";
            if (TimeUpdateWeighingMachine(1))
                toledoServer = GetProcessWeightMachineToledo();
            ViewBag.BasculaToledoServer = toledoServer;
            ViewBag.WeighingMachineToledo = TimeUpdateWeighingMachine(1);
            ViewBag.WeighingMachineToledoBPLus = TimeUpdateWeighingMachine(2);
            ViewBag.WeighingMachineToledoCAS = TimeUpdateWeighingMachine(3);
            ConfigurationWeighingMachine Model = new ConfigurationWeighingMachine
            {
                Products = _salesPriceChangeBusiness.GetAllSalesPricesForBasculeToledoBPlus()
            };
            return View(Model);
        }

        //Obetenemos informacion de usuario y retornamos modelo a la vista ADMS006
        public ActionResult ADMS006()
        {
            RegisterUserMaster model = new RegisterUserMaster();
            //Busca informacion por nombre de usuario HttpContext.User.Identity.Name
            var item = _userMasterRepo.GetUserMasterByUsername(HttpContext.User.Identity.Name);
            if (item != null)
            {
                //Asignamos Valores al ViewModel RegisterUserMaster
                model.EmployeeNumber = item.emp_no;
                model.Username = item.user_name;
                model.FirstName = item.first_name;
                model.LastName = item.last_name;
                model.Phone = item.office_tel;
                model.Cellphone = item.mobile_tel;
                model.DateOfBirth = item.birth_date.Value;
                model.Email = item.email;
                model.photo = item.photo;
            }
            model.ListRol = _RoleUserRepo.GetRolByEmpNo(item.emp_no);
            return View(model);
        }

        [HttpPost]
        public ActionResult ActionUpdateUser(RegisterUserMaster model)
        {
            if (ModelState.IsValid)
            {
                if (model.Email != null)  //Puse esta validacion porque cuando cambiabas contraseña no manda correo al modelo
                    if (!_userMasterRepo.IsValidEmailAddress(model.Email))
                    {
                        return Json(new { success = false, responseText = "Dominio de correo incorrecto. Ejemplo: correo@elflorido.com.mx" }, JsonRequestBehavior.AllowGet);
                    }
                var employee = _userMasterRepo.GetUserMasterByEmployeeNumber(model.EmployeeNumber);
                if (_userMasterRepo.EmployeeUsernameExists(model.Username) & employee.user_name != model.Username)
                    return Json(new { success = false, responseText = "Este nombre de usuario ya se encuentra registrado" }, JsonRequestBehavior.AllowGet);

                if (model.Password != null)
                {
                    employee.USER_PWD.password = Common.SetPassword(model.Password);
                    employee.USER_PWD.status = "A";
                    employee.USER_PWD.failed_attempts = 0;
                    employee.USER_PWD.pass_code = "";
                }
                else
                {
                    employee.user_name = Session["User"].ToString();
                    employee.first_name = model.FirstName;
                    employee.last_name = model.LastName;
                    employee.office_tel = model.Phone;
                    employee.mobile_tel = model.Cellphone;
                    employee.birth_date = model.DateOfBirth;
                    employee.email = model.Email;
                }

                if (_userMasterRepo.UpdateUserMaster(employee))
                {
                    //Fue comentado el código de abajo por que se quedaba trabado la página
                    //var json = JsonConvert.SerializeObject(employee, Formatting.Indented, new JsonSerializerSettings()
                    //{
                    //    ReferenceLoopHandling = Newtonsoft.Json.ReferenceLoopHandling.Ignore
                    //});
                    return Json(new { success = true, responseText = "Registro Exitoso!" }, JsonRequestBehavior.AllowGet);
                }
                else
                    return Json(new { success = false, responseText = "Error al editar usuario" }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                IEnumerable<ModelError> allErrors = ModelState.Values.SelectMany(v => v.Errors);
                return Json(new { success = false, responseText = allErrors.ToString() }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public JsonResult ActionUploadUserPhoto()
        {
            try
            {
                var EmployeeNumber = Request["EmployeeNumber"];
                var employee = _userMasterRepo.GetUserMasterByEmployeeNumber(EmployeeNumber);
                HttpPostedFileBase fs = Request.Files[0];
                BinaryReader br = new BinaryReader(Request.Files[0].InputStream);
                byte[] bytes = br.ReadBytes((Int32)fs.ContentLength);
                employee.photo = bytes;

                if (_userMasterRepo.UpdateUserMaster(employee))
                    return Json(new { success = true, responseText = "Registro Exitoso!" }, JsonRequestBehavior.AllowGet);
                else
                    return Json(new { success = false, responseText = "Error al editar usuario" }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception df)
            {
                var k = df.Message;
                throw;
            }
        }

        public ActionResult ActionDeletePicture(string EmployeeNumber)
        {
            var employee = _userMasterRepo.GetUserMasterByEmployeeNumber(EmployeeNumber);

            if (_userMasterRepo.DeletePicture(employee))
                return Json(new { success = true, responseText = "Registro Exitoso!" }, JsonRequestBehavior.AllowGet);
            else
                return Json(new { success = false, responseText = "Error al editar usuario." }, JsonRequestBehavior.AllowGet);
        }

        //inserta paginas en el sistema.
        [HttpPost]
        public ActionResult ActionInsertPages(ConfigurationAddPages AddPages)
        {
            if (Session["User"] != null)
            {
                AddPages.cuser = Session["User"].ToString();
                AddPages.cdate = DateTime.Now;
                AddPages.active_flag = true;
                if (ModelState.IsValid)
                {
                    var page = _PageMasterRepo.SearchPage(AddPages.page_id);
                    if (page == null)
                    {
                        if (_PageMasterRepo.AddPagesMaster(AddPages.PAGES))
                        {
                            List<ConfigurationAddPages> CostumModel = new List<ConfigurationAddPages>();
                            var list = _PageMasterRepo.GetAllPagesAdmin();
                            foreach (var item in list)
                            {
                                ConfigurationAddPages ite = new ConfigurationAddPages();
                                ite.page_id = item.page_id;
                                ite.page_name = item.page_name;
                                ite.description = item.description;
                                ite.url = item.url;
                                ite.active_flag = item.active_flag;
                                CostumModel.Add(ite);
                            }
                            var json = JsonConvert.SerializeObject(CostumModel, Formatting.Indented, new JsonSerializerSettings()
                            {
                                ReferenceLoopHandling = Newtonsoft.Json.ReferenceLoopHandling.Ignore
                            });

                            return Json(new { success = true, Json = json, responseText = "Registro Exitoso!" }, JsonRequestBehavior.AllowGet);
                        }
                        else
                            return Json(new { success = false, responseText = "Ocurrió un error al registrar los datos.!" }, JsonRequestBehavior.AllowGet);
                    }
                    else
                        return Json(new { success = false, responseText = "El Page ID le pertenece a la pagina '" + page.page_name + "', ingresa otro Page ID." }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    var errors = ModelState.Select(x => x.Value.Errors).Where(y => y.Count > 0).ToList();

                    return Json(new { success = false, responseText = "Error Desconocido, contacte a sistemas " + errors + "" }, JsonRequestBehavior.AllowGet);
                }
            }
            else
                return Json(new { success = false, responseText = "Termino tu sesión.!" }, JsonRequestBehavior.AllowGet);
        }

        //Edita las paginas del sistema
        [HttpPost]
        public ActionResult ActionEditPages(ConfigurationAddPages upPages)
        {
            if (Session["User"] != null)
            {
                upPages.uuser = Session["User"].ToString();
                if (ModelState.IsValid)
                {
                    var Page = _PageMasterRepo.GetPageByID(upPages.page_id);
                    Page.page_name = upPages.page_name;
                    Page.description = upPages.description;
                    Page.url = upPages.url;
                    Page.active_flag = upPages.active_flag;
                    Page.uuser = Session["User"].ToString();
                    Page.udate = DateTime.Now;
                    if (_PageMasterRepo.UpdatePagesMaster(Page))
                    {
                        List<ConfigurationAddPages> CostumModel = new List<ConfigurationAddPages>();
                        var list = _PageMasterRepo.GetAllPagesAdmin();
                        foreach (var item in list)
                        {
                            ConfigurationAddPages ite = new ConfigurationAddPages();
                            ite.page_id = item.page_id;
                            ite.page_name = item.page_name;
                            ite.description = item.description;
                            ite.url = item.url;
                            ite.active_flag = item.active_flag;
                            CostumModel.Add(ite);
                        }
                        var json = JsonConvert.SerializeObject(CostumModel, Formatting.Indented, new JsonSerializerSettings()
                        {
                            ReferenceLoopHandling = Newtonsoft.Json.ReferenceLoopHandling.Ignore
                        });

                        return Json(new { success = true, Json = json, responseText = "Registro Exitoso!" }, JsonRequestBehavior.AllowGet);
                    }
                    else
                        return Json(new { success = false, responseText = "Ocurrió un error al registrar los datos.!" }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    var errors = ModelState.Select(x => x.Value.Errors).Where(y => y.Count > 0).ToList();

                    return Json(new { success = false, responseText = "Error Desconocido, contacte a sistemas " + errors + "" }, JsonRequestBehavior.AllowGet);
                }
            }
            else
                return Json(new { success = false, responseText = "Termino tu sesión.!" }, JsonRequestBehavior.AllowGet);
        }

        //Inserta roles en el sistema
        [HttpPost]
        public ActionResult ActionInsertRoles(ConfigurationAddRoles AddRoles)
        {
            if (Session["User"] != null)
            {
                AddRoles.cuser = Session["User"].ToString();
                AddRoles.cdate = DateTime.Now;
                if (ModelState.IsValid)
                {
                    var role = _RoleMasterRepo.SearchRole(AddRoles.role_id);
                    if (role == null)
                    {
                        if (_RoleMasterRepo.AddRolesMaster(AddRoles.ROLES))
                        {
                            List<ConfigurationAddRoles> CostumModel = new List<ConfigurationAddRoles>();
                            var list = _RoleMasterRepo.GetAllRolesAdmin();
                            foreach (var item in list)
                            {
                                ConfigurationAddRoles ite = new ConfigurationAddRoles();
                                ite.role_id = item.role_id;
                                ite.role_name = item.role_name;
                                ite.role_description = item.role_description;
                                ite.active_flag = item.active_flag;
                                CostumModel.Add(ite);
                            }
                            var json = JsonConvert.SerializeObject(CostumModel, Formatting.Indented, new JsonSerializerSettings()
                            {
                                ReferenceLoopHandling = Newtonsoft.Json.ReferenceLoopHandling.Ignore
                            });
                            return Json(new { success = true, Json = json, responseText = "Registro Exitoso!" }, JsonRequestBehavior.AllowGet);
                        }
                        else
                            return Json(new { success = false, responseText = "Ocurrió un error al registrar los datos.!" }, JsonRequestBehavior.AllowGet);
                    }
                    else
                        return Json(new { success = false, responseText = "El Role ID que ingreso le pertenece al role de '" + role.role_name + "', intenta con otro Role ID." }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    var errors = ModelState.Select(x => x.Value.Errors).Where(y => y.Count > 0).ToList();

                    return Json(new { success = false, responseText = "Error Desconocido, contacte a sistemas " + errors + "" }, JsonRequestBehavior.AllowGet);
                }
            }
            else
                return Json(new { success = false, responseText = "Termino tu sesión.!" }, JsonRequestBehavior.AllowGet);
        }

        //Edita roles del sistema
        [HttpPost]
        public ActionResult ActionEditRol(ConfigurationAddRoles updateRol)
        {
            if (Session["User"] != null)
            {
                if (ModelState.IsValid)
                {
                    var Rol = _RoleMasterRepo.GetRoleByID(updateRol.role_id);
                    Rol.role_name = updateRol.role_name;
                    Rol.role_description = updateRol.role_description;
                    Rol.active_flag = updateRol.active_flag;
                    Rol.uuser = Session["User"].ToString();
                    Rol.udate = DateTime.Now;
                    if (_RoleMasterRepo.UpdateRoleMaster(Rol))
                    {
                        List<ConfigurationAddRoles> CostumModel = new List<ConfigurationAddRoles>();
                        var list = _RoleMasterRepo.GetAllRolesAdmin();
                        foreach (var item in list)
                        {
                            ConfigurationAddRoles ite = new ConfigurationAddRoles();
                            ite.role_id = item.role_id;
                            ite.role_name = item.role_name;
                            ite.role_description = item.role_description;
                            ite.active_flag = item.active_flag;
                            CostumModel.Add(ite);
                        }
                        var json = JsonConvert.SerializeObject(CostumModel, Formatting.Indented, new JsonSerializerSettings()
                        {
                            ReferenceLoopHandling = Newtonsoft.Json.ReferenceLoopHandling.Ignore
                        });
                        return Json(new { success = true, Json = json, responseText = "Registro Exitoso!" }, JsonRequestBehavior.AllowGet);
                    }
                    else
                        return Json(new { success = false, responseText = "Ocurrió un error al registrar los datos.!" }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    var errors = ModelState.Select(x => x.Value.Errors).Where(y => y.Count > 0).ToList();

                    return Json(new { success = false, responseText = "Error Desconocido, contacte a sistemas " + errors + "" }, JsonRequestBehavior.AllowGet);
                }
            }
            else
                return Json(new { success = false, responseText = "Termino tu sesión.!" }, JsonRequestBehavior.AllowGet);
        }

        //Obtenemos todas las paginas del rol 
        [HttpGet]
        public JsonResult ActionGetAllPagesOfRole(ConfigurationAddRoles role)
        {
            List<ConfigurationAddPages> CostumModel = new List<ConfigurationAddPages>();
            var list = _PageMasterRepo.GetAllPagesOfRole(role.role_id);
            foreach (var item in list)
            {
                ConfigurationAddPages ite = new ConfigurationAddPages();
                ite.page_id = item.page_id;
                ite.page_name = item.page_name;
                ite.description = item.description;
                CostumModel.Add(ite);
            }
            var json = JsonConvert.SerializeObject(CostumModel, Formatting.Indented, new JsonSerializerSettings()
            {
                ReferenceLoopHandling = Newtonsoft.Json.ReferenceLoopHandling.Ignore
            });
            return Json(new { success = true, Json = json }, JsonRequestBehavior.AllowGet);
        }

        //Obtenemos todas las paginas disponibles que no tenga ese rol
        [HttpGet]
        public JsonResult ActionGetAllPagesAvailable(ConfigurationAddRoles role)
        {
            List<ConfigurationAddPages> CostumModel = new List<ConfigurationAddPages>();
            var list = _PageMasterRepo.GetAllPagesAvailable(role.role_id);
            foreach (var item in list)
            {
                ConfigurationAddPages ite = new ConfigurationAddPages();
                ite.page_id = item.page_id;
                ite.page_name = item.page_name;
                ite.description = item.description;
                CostumModel.Add(ite);
            }
            var json = JsonConvert.SerializeObject(CostumModel, Formatting.Indented, new JsonSerializerSettings()
            {
                ReferenceLoopHandling = Newtonsoft.Json.ReferenceLoopHandling.Ignore
            });
            return Json(new { success = true, Json = json }, JsonRequestBehavior.AllowGet);
        }

        //Asignamos paginas a el rol 
        [HttpPost]
        public JsonResult ActionAddPagesToRoles(string pages, string roles)
        {
            if (Session["User"] != null)
            {
                if (ModelState.IsValid)
                {
                    if (_RolePageRepo.AddPagesToRoles(pages, roles, Session["User"].ToString()))
                    {
                        List<ConfigurationAddPages> CostumModel = new List<ConfigurationAddPages>();
                        var list = _PageMasterRepo.GetAllPagesOfRole(roles);
                        foreach (var item in list)
                        {
                            ConfigurationAddPages ite = new ConfigurationAddPages();
                            ite.page_id = item.page_id;
                            ite.page_name = item.page_name;
                            ite.description = item.description;
                            CostumModel.Add(ite);
                        }
                        var json = JsonConvert.SerializeObject(CostumModel, Formatting.Indented, new JsonSerializerSettings()
                        {
                            ReferenceLoopHandling = Newtonsoft.Json.ReferenceLoopHandling.Ignore
                        });
                        return Json(new { success = true, Json = json, responseText = "Registro Exitoso!" }, JsonRequestBehavior.AllowGet);
                    }
                    else
                        return Json(new { success = false, responseText = "Ocurrió un error al registrar los datos.!" }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    var errors = ModelState.Select(x => x.Value.Errors).Where(y => y.Count > 0).ToList();

                    return Json(new { success = false, responseText = "Error Desconocido, contacte a sistemas" + errors + "" }, JsonRequestBehavior.AllowGet);
                }
            }
            else
                return Json(new { success = false, responseText = "Termino tu sesión.!" }, JsonRequestBehavior.AllowGet);
        }

        //Quitamos paginas a el rol 
        [HttpPost]
        public JsonResult ActionRemovePagesFromRoles(string pages, string roles)
        {
            if (Session["User"] != null)
            {
                if (ModelState.IsValid)
                {
                    if (_RolePageRepo.RemovePagesFromRoles(pages, roles))
                    {
                        List<ConfigurationAddPages> CostumModel = new List<ConfigurationAddPages>();
                        var list = _PageMasterRepo.GetAllPagesOfRole(roles);
                        foreach (var item in list)
                        {
                            ConfigurationAddPages ite = new ConfigurationAddPages();
                            ite.page_id = item.page_id;
                            ite.page_name = item.page_name;
                            ite.description = item.description;
                            CostumModel.Add(ite);
                        }
                        var json = JsonConvert.SerializeObject(CostumModel, Formatting.Indented, new JsonSerializerSettings()
                        {
                            ReferenceLoopHandling = Newtonsoft.Json.ReferenceLoopHandling.Ignore
                        });
                        return Json(new { success = true, Json = json, responseText = "Registro Exitoso!" }, JsonRequestBehavior.AllowGet);
                    }
                    else
                        return Json(new { success = false, responseText = "Ocurrió un error al registrar los datos.!" }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    var errors = ModelState.Select(x => x.Value.Errors).Where(y => y.Count > 0).ToList();

                    return Json(new { success = false, responseText = "Error Desconocido, contacte a sistemas " + errors + "" }, JsonRequestBehavior.AllowGet);
                }
            }
            else
                return Json(new { success = false, responseText = "Termino tu sesión.!" }, JsonRequestBehavior.AllowGet);
        }

        //Obtenemos todos los roles del usuario
        [HttpGet]
        public JsonResult ActionGetAllRolesOfUser(ConfigurationAddPagesUser User)
        {
            List<ConfigurationAddRoles> CostumModel = new List<ConfigurationAddRoles>();
            var list = _RoleUserRepo.GetAllRolesOfUser(User.EmployeeNumber);
            foreach (var item in list)
            {
                ConfigurationAddRoles ite = new ConfigurationAddRoles();
                ite.role_id = item.role_id;
                ite.role_name = item.role_name;
                ite.role_description = item.role_description;
                CostumModel.Add(ite);
            }
            var json = JsonConvert.SerializeObject(CostumModel, Formatting.Indented, new JsonSerializerSettings()
            {
                ReferenceLoopHandling = Newtonsoft.Json.ReferenceLoopHandling.Ignore
            });
            return Json(new { success = true, Json = json }, JsonRequestBehavior.AllowGet);
        }

        //Obtenemos todos los roles disponibles 
        [HttpGet]
        //[OverrideAuthorization]
        public JsonResult ActionGetAllRolesAvailable(ConfigurationAddPagesUser User)
        {
            List<ConfigurationAddRoles> CostumModel = new List<ConfigurationAddRoles>();
            var list = _RoleUserRepo.GetAllRolesAvailable(User.EmployeeNumber);
            var status_user = _userPwdBusiness.GetUserPwdByEmployeeNumber(User.EmployeeNumber);
            foreach (var item in list)
            {
                ConfigurationAddRoles ite = new ConfigurationAddRoles();
                ite.role_id = item.role_id;
                ite.role_name = item.role_name;
                ite.role_description = item.role_description;
                CostumModel.Add(ite);
            }
            var json = JsonConvert.SerializeObject(CostumModel, Formatting.Indented, new JsonSerializerSettings()
            {
                ReferenceLoopHandling = Newtonsoft.Json.ReferenceLoopHandling.Ignore
            });
            return Json(new { success = true, Json = json, Status = status_user.status }, JsonRequestBehavior.AllowGet);
        }

        //Asignamos paginas a el rol 
        [HttpPost]
        public JsonResult ActionAddRolesToUser(string EmployeeNumber, string roles)
        {
            if (Session["User"] != null)
            {
                if (ModelState.IsValid)
                {

                    if (_RoleUserRepo.AddRolesToUser(EmployeeNumber, roles, Session["User"].ToString()))
                    {
                        List<ConfigurationAddRoles> CostumModel = new List<ConfigurationAddRoles>();
                        var list = _RoleUserRepo.GetAllRolesAvailable(EmployeeNumber);
                        foreach (var item in list)
                        {
                            ConfigurationAddRoles ite = new ConfigurationAddRoles();
                            ite.role_id = item.role_id;
                            ite.role_name = item.role_name;
                            ite.role_description = item.role_description;
                            CostumModel.Add(ite);
                        }
                        var json = JsonConvert.SerializeObject(CostumModel, Formatting.Indented, new JsonSerializerSettings()
                        {
                            ReferenceLoopHandling = Newtonsoft.Json.ReferenceLoopHandling.Ignore
                        });
                        return Json(new { success = true, Json = json, responseText = "Registro Exitoso!" }, JsonRequestBehavior.AllowGet);
                    }
                    else
                        return Json(new { success = false, responseText = "Ocurrió un error al registrar los datos.!" }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    var errors = ModelState.Select(x => x.Value.Errors).Where(y => y.Count > 0).ToList();

                    return Json(new { success = false, responseText = "Error Desconocido, contacte a sistemas " + errors + "" }, JsonRequestBehavior.AllowGet);
                }
            }
            else
                return Json(new { success = false, responseText = "Termino tu sesión.!" }, JsonRequestBehavior.AllowGet);
        }

        public JsonResult ActionUpdateStatus(string user, string statusUser)
        {
            if (Session["User"] != null)
                return Json(new { success = true, responseText = _userPwdBusiness.UpdateStatusUser(user, statusUser) }, JsonRequestBehavior.AllowGet);

            return Json(new { success = false, responseText = "Termino tu sesión.!" }, JsonRequestBehavior.AllowGet);
        }

        //Quitamos paginas a el rol 
        [HttpPost]
        public JsonResult ActionRemoveRolesFromUser(string EmployeeNumber, string roles)
        {
            if (Session["User"] != null)
            {
                if (ModelState.IsValid)
                {
                    if (_RoleUserRepo.RemoveRolesFromUser(EmployeeNumber, roles))
                    {
                        List<ConfigurationAddRoles> CostumModel = new List<ConfigurationAddRoles>();
                        var list = _RoleUserRepo.GetAllRolesAvailable(EmployeeNumber);
                        foreach (var item in list)
                        {
                            ConfigurationAddRoles ite = new ConfigurationAddRoles();
                            ite.role_id = item.role_id;
                            ite.role_name = item.role_name;
                            ite.role_description = item.role_description;
                            CostumModel.Add(ite);
                        }
                        var json = JsonConvert.SerializeObject(CostumModel, Formatting.Indented, new JsonSerializerSettings()
                        {
                            ReferenceLoopHandling = Newtonsoft.Json.ReferenceLoopHandling.Ignore
                        });
                        return Json(new { success = true, Json = json, responseText = "Registro Exitoso!" }, JsonRequestBehavior.AllowGet);
                    }
                    else
                        return Json(new { success = false, responseText = "Ocurrió un error al registrar los datos.!" }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    var errors = ModelState.Select(x => x.Value.Errors).Where(y => y.Count > 0).ToList();

                    return Json(new { success = false, responseText = "Error Desconocido, contacte a sistemas " + errors + "" }, JsonRequestBehavior.AllowGet);
                }
            }
            else
                return Json(new { success = false, responseText = "Termino tu sesión.!" }, JsonRequestBehavior.AllowGet);
        }
    }
}