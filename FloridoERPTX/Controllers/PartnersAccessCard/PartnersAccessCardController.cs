﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web.Mvc;
using App.BLL;
using App.Entities;
using App.BLL.PartnersAccessCard;
using static App.Entities.ViewModels.Common.Constants;

namespace FloridoERPTX.Controllers.PartnersAccessCard
{
    public class PartnersAccessCardController : Controller
    {
        private PartnersAccessCardBusiness _PartnersCardAccessBusiness;
        private UserMasterBusiness _UserMasterBusiness;
        public PartnersAccessCardController()
        {
            _PartnersCardAccessBusiness=new PartnersAccessCardBusiness();
            _UserMasterBusiness=new UserMasterBusiness();
        }

        // GET: DFLPOS_USER_CARD_ACCESS
        //Index
        public ActionResult Part001()
        {
            var pArtnersCardAccess = _PartnersCardAccessBusiness.GetAll();
            return View(pArtnersCardAccess.ToList());
        }

        // GET: DFLPOS_USER_CARD_ACCESS/Details/5
        //Details
        public ActionResult Part008(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            DFLPOS_USER_CARD_ACCESS pArtnersCardAccess = _PartnersCardAccessBusiness.Details(id);
            if (pArtnersCardAccess == null)
            {
                return HttpNotFound();
            }
            return View(pArtnersCardAccess);
        }

        // GET: DFLPOS_USER_CARD_ACCESS/Create
        public ActionResult ActionPart002()
        {
            List<string> list= new List<string>() { Roles.SC, Roles.SCGRAL, Roles.MANAGER, Roles.IT_Department, Roles.ACGRAL, Roles.Cobranza, Roles.SUBMANAGER, "rol08" };

            ViewBag.emp_id = new SelectList(_UserMasterBusiness.GetUsersMasterByRol(list).OrderBy(o => o.full_name), "emp_no", "full_name");
            return View("Part002");
        }

        // POST: DFLPOS_USER_CARD_ACCESS/Create
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult ActionPart002([Bind(Include = "emp_id,card_number,Id")] DFLPOS_USER_CARD_ACCESS pArtnersCardAccess)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    _PartnersCardAccessBusiness.Create(pArtnersCardAccess);
                    return RedirectToAction("Part001");
                }
                catch (Exception e)
                {
                    ViewBag.emp_id = new SelectList(_UserMasterBusiness.GetAllUsers().OrderBy(o=>o.full_name), "emp_no", "full_name", pArtnersCardAccess.emp_id);
                    ViewBag.Error = e.Message;
                    return View("Part002", pArtnersCardAccess);
                }
                
            }

            ViewBag.emp_id = new SelectList(_UserMasterBusiness.GetAllUsers().OrderBy(o=>o.full_name), "emp_no", "full_name", pArtnersCardAccess.emp_id);
            return View("Part002", pArtnersCardAccess);
        }

        // GET: DFLPOS_USER_CARD_ACCESS/Edit/5
        public ActionResult ActionPart004(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            DFLPOS_USER_CARD_ACCESS pArtnersCardAccess = _PartnersCardAccessBusiness.Details(id);
            if (pArtnersCardAccess == null)
            {
                return HttpNotFound();
            }
            ViewBag.emp_id = new SelectList(_UserMasterBusiness.GetAllUsers().OrderBy(o=>o.full_name), "emp_no", "full_name", pArtnersCardAccess.emp_id);
            return View("Part004", pArtnersCardAccess);
        }
        // POST: DFLPOS_USER_CARD_ACCESS/Edit/5
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult ActionPart004([Bind(Include = "emp_id,card_number,Id")] DFLPOS_USER_CARD_ACCESS pArtnersCardAccess)
        {
            var emp = _PartnersCardAccessBusiness.Details(pArtnersCardAccess.id);
            pArtnersCardAccess.emp_id = emp.emp_id;
            //pArtnersCardAccess.USER_MASTER = emp.USER_MASTER;
            //pArtnersCardAccess.DFLPOS_USER_CARD_ACCESS_LOGS = emp.DFLPOS_USER_CARD_ACCESS_LOGS;
            //if (ModelState.IsValid)
            //{
                try
                {
                    _PartnersCardAccessBusiness.Edit(pArtnersCardAccess);
                    return RedirectToAction("Part001");
                }
                catch (Exception e)
                {
                    ViewBag.emp_id = new SelectList(_UserMasterBusiness.GetAllUsers().OrderBy(o=>o.full_name), "emp_no", "full_name", pArtnersCardAccess.emp_id);
                    ViewBag.Error = e.Message;
                    return View("Part004", pArtnersCardAccess);
                }
                
            //}
            //ViewBag.emp_id = new SelectList(_UserMasterBusiness.GetAllUsers().OrderBy(o=>o.full_name), "emp_no", "full_name", pArtnersCardAccess.emp_id);
            //return View("Part004",pArtnersCardAccess);
        }

        // GET: DFLPOS_USER_CARD_ACCESS/Delete/5
        public ActionResult ActionPart006(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            DFLPOS_USER_CARD_ACCESS pArtnersCardAccess = _PartnersCardAccessBusiness.Details(id);
            if (pArtnersCardAccess == null)
            {
                return HttpNotFound();
            }
            return View("Part006",pArtnersCardAccess);
        }

        // POST: DFLPOS_USER_CARD_ACCESS/Delete/5
        [HttpPost]
        public JsonResult ActionPart006(int id)
        {
            try
            {
                ViewBag.Reload = false;
                _PartnersCardAccessBusiness.Delete(id);
                ViewBag.Reload = true;
                //return RedirectToAction("Part001");
                return Json(new { success = true, Json = id }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                return Json(new { success = false, Json = id }, JsonRequestBehavior.AllowGet);
            }
           
           
        }

       
    }
}
