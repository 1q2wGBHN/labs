﻿using App.BLL;
using App.BLL.Customer;
using App.BLL.Item;
using App.BLL.Lookups;
using App.BLL.Quotes;
using App.Entities.ViewModels.Customers;
using App.Entities.ViewModels.Item;
using App.Entities.ViewModels.Quotes;
using DevExpress.XtraReports.UI;
using FloridoERPTX.Reports;
using System.Collections.Generic;
using System.Web.Mvc;
using System.IO;
using System;
using App.Common;
using static App.Entities.ViewModels.Common.Constants;
using FloridoERPTX.CommonMethods;
using App.Entities.ViewModels.Common;

namespace FloridoERPTX.Controllers.Quotes
{
    public class QuotesController : Controller
    {
        private CustomerBusiness _CustomerBusiness;
        private ReferenceNumberBusiness _ReferenceNumberBusiness;
        private ItemBusiness _ItemBusiness;
        private UserMasterBusiness _UserMasterBusiness;
        private QuoteBusiness _QuoteBusiness;
        private QuotesDetailsBusiness _QuotesDetailsBusiness;
        private CustomerLookup _CustomerLookup;
        private readonly Common _Common;

        public QuotesController()
        {
            _CustomerBusiness = new CustomerBusiness();
            _ReferenceNumberBusiness = new ReferenceNumberBusiness();
            _ItemBusiness = new ItemBusiness();
            _UserMasterBusiness = new UserMasterBusiness();
            _QuoteBusiness = new QuoteBusiness();
            _QuotesDetailsBusiness = new QuotesDetailsBusiness();
            _CustomerLookup = new CustomerLookup();
            _Common = new Common();
        }

        public ActionResult QUOT001()
        {
            var Model = new QuoteModel();
            ViewBag.ClientCode = _CustomerLookup.FillLookupCustomer(CustomersLookup.Filter);
            return View(Model);
        }

        [HttpPost]
        public ActionResult QUOT001(QuoteModel Model)
        {
            var UserName = Session["User"].ToString();
            var UserInfo = _UserMasterBusiness.GetUserMasterByUsername(UserName);
            var ReferenceNumber = _ReferenceNumberBusiness.GetReferenceNumber(VoucherType.QUOTES);
            var Customer = _CustomerBusiness.GetCustomer(Model.ClientCode);
            var Item = new QuoteModel();
            Item.IsFromMenu = false;            
            Item.ClientName = Customer.Name;
            Item.ClientNumber = Customer.Code.Replace("T", string.Empty);
            Item.Email = Customer.Email;
            Item.ClientCode = Item.ClientCode;
            Item.FaxNumber = Customer.Phone_2;
            Item.PhoneNumber = Customer.Phone;
            Item.Address = Customer.Address;
            Item.Seller = string.Format("{0} {1}", UserInfo.first_name, UserInfo.last_name);
            Item.SellerEmail = UserInfo.email;

            if (ReferenceNumber == null)
                Item.HasSerieAndNumber = false;
            else
            {
                Item.Quote = ReferenceNumber.serie + ReferenceNumber.number;
                Item.QuoteNo = ReferenceNumber.number;
                Item.QuoteSerie = ReferenceNumber.serie;
            }

            return View(Item);
        }

        [HttpGet]
        public ActionResult ActionGetCustomerInfo(string ClientNumber)
        {            
            CustomerViewModel Item = _CustomerBusiness.GetCustomerByCode(ClientNumber);

            if (string.IsNullOrEmpty(Item.Code))
                return Json(new { success = false, Json = Item, responseText = "Datos cliente no encontrados" }, JsonRequestBehavior.AllowGet);

            Item.Code = Item.Code.Replace("T", string.Empty);
            return Json(new { success = true, Json = Item, responseText = "Datos cliente actualizados" }, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public ActionResult ActionGetItemInformation(string Item)
        {
            List<ItemQuotation> ItemList = _ItemBusiness.GetItemQuotations(Item);
            if (ItemList.Count == 0 || ItemList == null)
            {
                return Json(new { success = false, Json = ItemList, responseText = "No hay articulos" }, JsonRequestBehavior.AllowGet);
            }
            var returnList = Json(new { success = true, Json = ItemList, responseText = "Articulos encontrados" }, JsonRequestBehavior.AllowGet);
            returnList.MaxJsonLength = Int32.MaxValue;
            return returnList;
        }

        [HttpPost]
        public ActionResult ActionProcessQuotation(QuoteModel Model)
        {
            Model.UserName = Session["User"].ToString();
            Model.PdfBase64 = MakeReportPdf(Model);
            if (_QuoteBusiness.CreateQuote(Model))
                if (_QuotesDetailsBusiness.CreateQuoteDetail(Model))
                    return Json(new { success = true, Json = Model, responseText = "Cotización Creada" }, JsonRequestBehavior.AllowGet);

            return Json(new { success = false, Json = Model, responseText = "Ocurrió un error, contacte a sistemas" }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult ActionSendEmail(QuoteModel Model, bool SendToSeller)
        {
            //Model.UserName = Session["User"].ToString();
            Model.PdfBase64 = MakeReportPdf(Model);
            if (_Common.SendMailQuotes(Model, SendToSeller))
                return Json(new { success = true, Json = Model, responseText = "Pdf Enviado" }, JsonRequestBehavior.AllowGet);

            return Json(new { success = false, Json = Model, responseText = "Ocurrió un error, contacte a sistemas" }, JsonRequestBehavior.AllowGet);
        }        

        private string MakeReportPdf(QuoteModel Model)
        {
            var Report = new QuotesReport();
            (Report.Bands["DetailReport"] as DetailReportBand).DataSource = Report.printTable(Model);
            (Report.Bands["DetailReport"] as DetailReportBand).DataMember = "xrTableRowFormat";            
            
            MemoryStream ms = new MemoryStream();
            Report.ExportToPdf(ms);
            byte[] pdfBytes = ms.ToArray();
            string pdfBase64 = Convert.ToBase64String(pdfBytes);

            return pdfBase64;
        }

        public ActionResult QUOT002()
        {
            var Model = new QuoteIndex();            
            ViewBag.ClientCode = _CustomerLookup.FillLookupCustomer(CustomersLookup.ByQuotation);
            return View(Model);
        }

        [HttpPost]
        public ActionResult QUOT002(QuoteIndex Model)
        {
            Model.IsFromMenu = false;
            Model.DataReport = _QuoteBusiness.QuoteReport(Model.StartDate, Model.EndDate, Model.ClientCode);
            ViewBag.ClientCode = _CustomerLookup.FillLookupCustomer(CustomersLookup.ByQuotation);
            return View(Model);
        }

        public ActionResult ActionGetQuotePDF(long Number, string Serie)
        {
            string PdfEncoded = string.Empty;
            var Quote = _QuoteBusiness.GetQuotation(Number, Serie);
            PdfEncoded = Quote.pdf;
            return Json(new { success = true, Json = PdfEncoded, responseText = "PDF Obtenido" }, JsonRequestBehavior.AllowGet);
        }
    }
}
