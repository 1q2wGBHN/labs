﻿using App.BLL;
using App.BLL.Configuration;
using App.BLL.EntryFree;
using App.BLL.Site;
using App.Common;
using FloridoERPTX.Reports;
using System;
using System.Configuration;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace FloridoERPTX.Controllers.EntryFree
{
    public class EntryFreeController : Controller
    {
        private readonly EntryFreeHeadBusiness _EntryFreeBusiness;
        private readonly Common common;
        private readonly SysRoleMasterBusiness _sysRoleMasterBusiness;
        private readonly SendMail inforamtion;
        private readonly UserMasterBusiness _UserMasterBusiness;
        private readonly SiteBusiness _siteBusiness;

        public EntryFreeController()
        {
            _sysRoleMasterBusiness = new SysRoleMasterBusiness();
            _EntryFreeBusiness = new EntryFreeHeadBusiness();
            common = new Common();
            inforamtion = new SendMail();
            _UserMasterBusiness = new UserMasterBusiness();
            _siteBusiness = new SiteBusiness();
        }

        public ActionResult ActionGetEntryFree(int EntryFreeId)
        {
            if (Session["User"] != null)
            {
                if (EntryFreeId > 0)
                    return new JsonResult() { Data = _EntryFreeBusiness.GetEntryFree(EntryFreeId), JsonRequestBehavior = JsonRequestBehavior.AllowGet, MaxJsonLength = Int32.MaxValue };

                return new JsonResult() { Data = "N/A", JsonRequestBehavior = JsonRequestBehavior.AllowGet, MaxJsonLength = Int32.MaxValue };
            }
            return new JsonResult() { Data = "SF", JsonRequestBehavior = JsonRequestBehavior.AllowGet, MaxJsonLength = Int32.MaxValue };
        }

        public ActionResult ActionGetEntryFreeReport(int EntryFreeId, int SupplierId)
        {
            if (Session["User"] != null)
            {
                if (EntryFreeId > 0 && SupplierId > 0)
                    return new JsonResult() { Data = _EntryFreeBusiness.GetEntryFreeReport(EntryFreeId, SupplierId), JsonRequestBehavior = JsonRequestBehavior.AllowGet, MaxJsonLength = Int32.MaxValue };

                return new JsonResult() { Data = "N/A", JsonRequestBehavior = JsonRequestBehavior.AllowGet, MaxJsonLength = Int32.MaxValue };
            }
            return new JsonResult() { Data = "SF", JsonRequestBehavior = JsonRequestBehavior.AllowGet, MaxJsonLength = Int32.MaxValue };
        }

        public ActionResult ActionFolioCancelEntryFree(int EntryFreeId)
        {
            if (Session["User"] != null)
            {
                if (EntryFreeId > 0)
                    return new JsonResult() { Data = _EntryFreeBusiness.PutEntryFree(EntryFreeId, Session["User"].ToString()), JsonRequestBehavior = JsonRequestBehavior.AllowGet, MaxJsonLength = Int32.MaxValue };

                return new JsonResult() { Data = "N/A", JsonRequestBehavior = JsonRequestBehavior.AllowGet, MaxJsonLength = Int32.MaxValue };
            }
            return new JsonResult() { Data = "SF", JsonRequestBehavior = JsonRequestBehavior.AllowGet, MaxJsonLength = Int32.MaxValue };
        }

        public ActionResult ActionNewFolioCancelEntryFree(int EntryFreeId)
        {
            if (Session["User"] != null)
            {
                if (_UserMasterBusiness.IsRolonUser(Session["User"].ToString(), ConfigurationManager.AppSettings["Gerente"].ToString()))
                {
                    if (EntryFreeId > 0)
                        return new JsonResult() { Data = _EntryFreeBusiness.NewPutEntryFree(EntryFreeId, Session["User"].ToString()), JsonRequestBehavior = JsonRequestBehavior.AllowGet, MaxJsonLength = Int32.MaxValue };

                    return new JsonResult() { Data = "N/A", JsonRequestBehavior = JsonRequestBehavior.AllowGet, MaxJsonLength = Int32.MaxValue };
                }
                return new JsonResult() { Data = 801, JsonRequestBehavior = JsonRequestBehavior.AllowGet, MaxJsonLength = Int32.MaxValue };
            }
            return new JsonResult() { Data = "SF", JsonRequestBehavior = JsonRequestBehavior.AllowGet, MaxJsonLength = Int32.MaxValue };
        }

        public ActionResult ActionFolioEntryFree(int EntryFreeId)
        {
            if (Session["User"] != null)
            {
                if (EntryFreeId > 0)
                {
                    var EntryFree = _EntryFreeBusiness.GetEntryFree(EntryFreeId);

                    EntryReport report = new EntryReport();
                    report.DataSource = report.printTable(EntryFree);

                    var stream = new MemoryStream();
                    report.ExportToPdf(stream);

                    if (EntryFree.EntryFreeList.Count() > 0)
                        return Json(new { success = true, responseText = Convert.ToBase64String(stream.ToArray()) }, JsonRequestBehavior.AllowGet);
                }
                return new JsonResult() { Data = "N/A", JsonRequestBehavior = JsonRequestBehavior.AllowGet, MaxJsonLength = Int32.MaxValue };
            }
            return new JsonResult() { Data = "SF", JsonRequestBehavior = JsonRequestBehavior.AllowGet, MaxJsonLength = Int32.MaxValue };
        }

        public ActionResult ActionNewFolioEntryFree(int EntryFreeId)
        {
            if (Session["User"] != null)
            {
                if (EntryFreeId > 0)
                {
                    var stream = new MemoryStream();
                    var entryFree = _EntryFreeBusiness.GetEntryFreeReport(EntryFreeId);
                    EntryReport reportEntryFree = new EntryReport();
                    if (entryFree.Print_status != 0)
                    {
                        reportEntryFree.Watermark.Text = "RE-IMPRESION";
                        reportEntryFree.Watermark.TextDirection = DevExpress.XtraPrinting.Drawing.DirectionMode.ForwardDiagonal;
                        reportEntryFree.Watermark.Font = new Font(reportEntryFree.Watermark.Font.FontFamily, 40);
                        reportEntryFree.Watermark.ForeColor = Color.DodgerBlue;
                        reportEntryFree.Watermark.TextTransparency = 150;
                        reportEntryFree.Watermark.ShowBehind = false;
                        reportEntryFree.Watermark.PageRange = "1-99";
                        var print = _EntryFreeBusiness.EntryFreeEditStatusPrint(EntryFreeId, entryFree.Print_status, "RPD001.CSHTML");
                    }
                    else
                    {
                        var print = _EntryFreeBusiness.EntryFreeEditStatusPrint(EntryFreeId, entryFree.Print_status, "RPD001.CSHTML");
                    }
                    reportEntryFree.DataSource = reportEntryFree.printTable(entryFree);
                    reportEntryFree.ExportToPdf(stream);
                    if (entryFree.EntryFreeList.Count() > 0)
                        return Json(new { success = true, responseText = Convert.ToBase64String(stream.ToArray()) }, JsonRequestBehavior.AllowGet);
                }
                return new JsonResult() { Data = "N/A", JsonRequestBehavior = JsonRequestBehavior.AllowGet, MaxJsonLength = Int32.MaxValue };
            }
            return new JsonResult() { Data = "SF", JsonRequestBehavior = JsonRequestBehavior.AllowGet, MaxJsonLength = Int32.MaxValue };
        }

        public ActionResult ActionEmailEntryFree(int EntryFreeId)
        {
            if (Session["User"] != null)
            {
                if (EntryFreeId > 0)
                {
                    var EntryFree = _EntryFreeBusiness.GetEntryFree(EntryFreeId);

                    var Recibo = _sysRoleMasterBusiness.GetEmailsByRol(ConfigurationManager.AppSettings["Recibo"].ToString());
                    var MesaControl = _sysRoleMasterBusiness.GetEmailsByRol(ConfigurationManager.AppSettings["MesaControl"].ToString());
                    var site = _siteBusiness.GetInfoSite();

                    Task task = new Task(() =>
                    {
                        EntryReport report = new EntryReport();
                        report.DataSource = report.printTable(EntryFree);

                        Stream stream = new MemoryStream();
                        report.ExportToPdf(stream);
                        var Email = (!string.IsNullOrWhiteSpace(Recibo) ? Recibo + "," : "") + (!string.IsNullOrWhiteSpace(MesaControl) ? MesaControl : "");
                        inforamtion.folio = EntryFreeId.ToString();
                        inforamtion.store = EntryFree.Supplier;
                        inforamtion.subject = site.site_name + " - Entradas de mercancia de " + EntryFree.Supplier;
                        inforamtion.from = site.site_name;
                        inforamtion.email = Email;

                        common.MailMessageEntryFree(inforamtion, stream, site.site_name);
                    });
                    task.Start();

                    return new JsonResult() { Data = true, JsonRequestBehavior = JsonRequestBehavior.AllowGet, MaxJsonLength = Int32.MaxValue };
                }
                return new JsonResult() { Data = "N/A", JsonRequestBehavior = JsonRequestBehavior.AllowGet, MaxJsonLength = Int32.MaxValue };
            }
            return new JsonResult() { Data = "SF", JsonRequestBehavior = JsonRequestBehavior.AllowGet, MaxJsonLength = Int32.MaxValue };
        }

        //public ActionResult ActionEmailEntryFreeSupplierP(int EntryFreeId)
        //{
        //    if (Session["User"] != null)
        //    {
        //        if (EntryFreeId > 0)
        //        {
        //            var EntryFree = _EntryFreeBusiness.getEntryFree(EntryFreeId);

        //            EntryReport report = new EntryReport();
        //            report.DataSource = report.printTable(EntryFree);

        //            if (EntryFree.Emailpurchases != null && EntryFree.Emailpurchases != "N/A")
        //            {
        //                Task task = new Task(() =>
        //                {
        //                    Stream stream = new MemoryStream();
        //                    report.ExportToPdf(stream);

        //                    var Email = EntryFree.Emailpurchases;

        //                    inforamtion.folio = EntryFreeId.ToString();
        //                    inforamtion.store = EntryFree.Supplier;
        //                    inforamtion.subject = "Entradas de mercancia";
        //                    inforamtion.from = "El florido";
        //                    inforamtion.email = Email + ",mpasillas@elflorido.com.mx";

        //                    common.MailMessageEntryFreeSupplier(inforamtion, stream);
        //                });
        //                task.Start();

        //                return new JsonResult() { Data = true, JsonRequestBehavior = JsonRequestBehavior.AllowGet, MaxJsonLength = Int32.MaxValue };
        //            }
        //            return new JsonResult() { Data = false, JsonRequestBehavior = JsonRequestBehavior.AllowGet, MaxJsonLength = Int32.MaxValue };
        //        }
        //        return new JsonResult() { Data = "N/A", JsonRequestBehavior = JsonRequestBehavior.AllowGet, MaxJsonLength = Int32.MaxValue };
        //    }
        //    return new JsonResult() { Data = "SF", JsonRequestBehavior = JsonRequestBehavior.AllowGet, MaxJsonLength = Int32.MaxValue };
        //}

        //public ActionResult ActionEmailEntryFreeSupplierS(int EntryFreeId)
        //{
        //    if (Session["User"] != null)
        //    {
        //        if (EntryFreeId > 0)
        //        {
        //            var EntryFree = _EntryFreeBusiness.getEntryFree(EntryFreeId);

        //            EntryReport report = new EntryReport();
        //            report.DataSource = report.printTable(EntryFree);

        //            if (EntryFree.EmailSale != null && EntryFree.EmailSale != "N/A")
        //            {
        //                Task task = new Task(() =>
        //                {
        //                    Stream stream = new MemoryStream();
        //                    report.ExportToPdf(stream);

        //                    var Email = EntryFree.EmailSale;

        //                    inforamtion.folio = EntryFreeId.ToString();
        //                    inforamtion.store = EntryFree.Supplier;
        //                    inforamtion.subject = "Entradas de mercancia";
        //                    inforamtion.from = "El florido";
        //                    inforamtion.email = Email + ",mpasillas@elflorido.com.mx";

        //                    common.MailMessageEntryFreeSupplier(inforamtion, stream);
        //                });
        //                task.Start();
        //                return new JsonResult() { Data = true, JsonRequestBehavior = JsonRequestBehavior.AllowGet, MaxJsonLength = Int32.MaxValue };
        //            }
        //            return new JsonResult() { Data = false, JsonRequestBehavior = JsonRequestBehavior.AllowGet, MaxJsonLength = Int32.MaxValue };
        //        }
        //        return new JsonResult() { Data = "N/A", JsonRequestBehavior = JsonRequestBehavior.AllowGet, MaxJsonLength = Int32.MaxValue };
        //    }
        //    return new JsonResult() { Data = "SF", JsonRequestBehavior = JsonRequestBehavior.AllowGet, MaxJsonLength = Int32.MaxValue };
        //}

        public ActionResult ActionGetFolios(string supplierId, string fechaIni, string fechaFin)
        {
            var foliosList = _EntryFreeBusiness.GetFoliosBySupplierAndFechaRange(supplierId, fechaIni, fechaFin);
            if (foliosList.Count != 0)
                return Json(new { success = true, Json = foliosList }, JsonRequestBehavior.AllowGet);
            else
                return Json(new { success = false }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult ActionGetEntryFreeSearch(int supplierId, DateTime? fechaIni, DateTime? fechaFin, int state, string deparment, string family, string currency, string part_number)
        {
            if (Session["User"] != null)
                return new JsonResult() { Data = new { success = true, listEntryFree = _EntryFreeBusiness.GetAllEntryFreeOptions(fechaIni, fechaFin, supplierId, state, deparment, family, currency, part_number) }, JsonRequestBehavior = JsonRequestBehavior.AllowGet, MaxJsonLength = Int32.MaxValue };
            else
                return new JsonResult() { Data = new { success = false }, JsonRequestBehavior = JsonRequestBehavior.AllowGet, MaxJsonLength = Int32.MaxValue };
        }

        public ActionResult ActionGetEntryFreeSearchDetail(int supplierId, DateTime? fechaIni, DateTime? fechaFin, int state, string deparment, string family, string currency, string part_number)
        {
            if (Session["User"] != null)
                return new JsonResult() { Data = new { success = true, listEntryFree = _EntryFreeBusiness.EntryFreeListDetail(fechaIni, fechaFin, supplierId, state, deparment, family, currency, part_number) }, JsonRequestBehavior = JsonRequestBehavior.AllowGet, MaxJsonLength = Int32.MaxValue };
            else
                return new JsonResult() { Data = new { success = false }, JsonRequestBehavior = JsonRequestBehavior.AllowGet, MaxJsonLength = Int32.MaxValue };
        }

        [HttpPost]
        public ActionResult ActionEditStatus(int EntryFreeId)
        {
            if (Session["User"] != null)
            {
                if (EntryFreeId > 0)
                {
                    var EntryFree = _EntryFreeBusiness.UpdateEntryFree(EntryFreeId, Session["User"].ToString());
                    return new JsonResult() { Data = EntryFree, JsonRequestBehavior = JsonRequestBehavior.AllowGet, MaxJsonLength = Int32.MaxValue };
                }
                return new JsonResult() { Data = "N/A", JsonRequestBehavior = JsonRequestBehavior.AllowGet, MaxJsonLength = Int32.MaxValue };
            }
            return new JsonResult() { Data = "SF", JsonRequestBehavior = JsonRequestBehavior.AllowGet, MaxJsonLength = Int32.MaxValue };
        }

        [HttpPost]
        public ActionResult ActionNewEditStatus(int EntryFreeId)
        {
            if (Session["User"] != null)
            {
                if (EntryFreeId > 0)
                {
                    var EntryFree = _EntryFreeBusiness.NewUpdateEntryFree(EntryFreeId, Session["User"].ToString());
                    return new JsonResult() { Data = EntryFree, JsonRequestBehavior = JsonRequestBehavior.AllowGet, MaxJsonLength = Int32.MaxValue };
                }
                return new JsonResult() { Data = "N/A", JsonRequestBehavior = JsonRequestBehavior.AllowGet, MaxJsonLength = Int32.MaxValue };
            }
            return new JsonResult() { Data = "SF", JsonRequestBehavior = JsonRequestBehavior.AllowGet, MaxJsonLength = Int32.MaxValue };
        }
    }
}