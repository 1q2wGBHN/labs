﻿using App.BLL.Combo;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace FloridoERPTX.Controllers.Combo
{
    public class ComboDetailController : Controller
    {
        private readonly ComboDetailBusiness _ComboDetailBusiness;
        public ComboDetailController()
        {
            _ComboDetailBusiness = new ComboDetailBusiness();
        }

        public ActionResult ActionGetComboDetail(string PartNumber)
        {
            return new JsonResult() { Data = _ComboDetailBusiness.GetComboDetail(PartNumber), JsonRequestBehavior = JsonRequestBehavior.AllowGet, MaxJsonLength = Int32.MaxValue }; 
        }
    }
}