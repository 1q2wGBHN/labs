﻿using App.BLL;
using App.BLL.Site;
using App.BLL.WithdrawalCash;
using App.Entities.ViewModels.Common;
using App.Entities.ViewModels.Withdrawal;
using DevExpress.XtraReports.UI;
using FloridoERPTX.CommonMethods;
using FloridoERPTX.Reports;
using System.Linq;
using System.Web.Mvc;
using static App.Entities.ViewModels.Common.Constants;

namespace FloridoERPTX.Controllers.Withdrawal
{
    public class WithdrawalController : Controller
    {
        private UserMasterBusiness _UserMasterBusiness;
        private WithdrawalCashBusiness _WithdrawalCashBusiness;
        private WithdrawalCashDetailsBusiness _WithdrawalCashDetailsBusiness;
        private SiteConfigBusiness _SiteConfigBusiness;
        private CashierLookup _CashierLookup;

        public WithdrawalController()
        {
            _UserMasterBusiness = new UserMasterBusiness();
            _WithdrawalCashBusiness = new WithdrawalCashBusiness();
            _WithdrawalCashDetailsBusiness = new WithdrawalCashDetailsBusiness();
            _SiteConfigBusiness = new SiteConfigBusiness();
            _CashierLookup = new CashierLookup();
        }
        
        public ActionResult WIDR001()
        {
            var Model = new WithdrawalIndex();            
            ViewBag.Cashier = _CashierLookup.FillLookupCashiers(CashiersLookup.AllCashier);
            return View(Model);
        }

        [HttpPost]
        public ActionResult WIDR001(WithdrawalIndex Model)
        {
            decimal Difference = 0;
            Model.DataReport = _WithdrawalCashBusiness.GetReportData(Model.Date, Model.Cashier);
            Model.TotalAmount = _WithdrawalCashBusiness.GetTotalAmount(Model.Date, Model.Cashier);
            Model.AcumulateData = _WithdrawalCashBusiness.GetAcumulateData(Model.Date, Model.Cashier);
            Model.IsFromMenu = false;
            ViewBag.Cashier = _CashierLookup.FillLookupCashiers(CashiersLookup.AllCashier);
            Difference = Model.TotalAmount - Model.DataReport.Where(c => c.Currency == Currencies.MXN).Sum(c=>c.Amount);
            if (Difference != 0 && Model.DataReport.Count > 0)
                Model.ExchangeRate = Difference / Model.DataReport.Where(c => c.Currency == Currencies.USD).Sum(c => c.Amount);

            return View(Model);
        }

        public ActionResult WIDR002()
        {
            var Model = new DollarsConciliationIndex();
            return View(Model);
        }

        [HttpPost]
        public ActionResult WIDR002(DollarsConciliationIndex Model)
        {
            Model.IsFromMenu = false;
            Model.DataReport = _WithdrawalCashDetailsBusiness.DollarsConciliation(Model.Date);
            return View(Model);
        }

        public ActionResult ActionGetWithdrawalDetails(string Date, string Currency, string EmployeeNo)
        {
            return Json(new { success = true, Json = _WithdrawalCashDetailsBusiness.WithDrawalDetails(Date, Currency, EmployeeNo) }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult ActionGetWithdrawalHeaderReport(WithdrawalIndex Model)
        {
            var report = new WithdrawalHeaderReport();
            Model.SiteName = _SiteConfigBusiness.GetSiteCodeName();
            (report.Bands["DetailReport"] as DetailReportBand).DataSource = report.printTable(Model);
            (report.Bands["DetailReport"] as DetailReportBand).DataMember = "xrTableRowFormat";
            return PartialView("~/Views/ReportsViews/ReportView.cshtml", report);
        }

        [HttpPost]
        public ActionResult ActionGetWithdrawalDetailsReport(string Date, string Currency, string EmployeeNo, string EmployeeName)
        {
            var DataReport = _WithdrawalCashDetailsBusiness.WithDrawalDetails(Date, Currency, EmployeeNo);
            var SiteName = _SiteConfigBusiness.GetSiteCodeName();
            var report = new WithdrawalDetailsReport();
            (report.Bands["DetailReport"] as DetailReportBand).DataSource = report.printTable(DataReport, Date, Currency, EmployeeName, SiteName);
            (report.Bands["DetailReport"] as DetailReportBand).DataMember = "xrTableRowFormat";
            return PartialView("~/Views/ReportsViews/ReportView.cshtml", report);
        }

        [HttpPost]
        public ActionResult ActionGetWithdrawalHeaderTotalsReport(WithdrawalIndex Model)
        {
            var report = new WithdrawalHeaderTotalReport();
            Model.SiteName = _SiteConfigBusiness.GetSiteCodeName();
            (report.Bands["DetailReport"] as DetailReportBand).DataSource = report.printTable(Model);
            (report.Bands["DetailReport"] as DetailReportBand).DataMember = "xrTableRowFormat";
            return PartialView("~/Views/ReportsViews/ReportView.cshtml", report);
        }

        [HttpPost]
        public ActionResult ActionGetDollarsConcilliationReport(DollarsConciliationIndex Model)
        {
            var report = new DollarsConciliationReport();
            Model.SiteName = _SiteConfigBusiness.GetSiteCodeName();
            (report.Bands["DetailReport"] as DetailReportBand).DataSource = report.printTable(Model);
            (report.Bands["DetailReport"] as DetailReportBand).DataMember = "xrTableRowFormat";
            return PartialView("~/Views/ReportsViews/ReportView.cshtml", report);
        }
    }
}
