﻿using App.BLL;
using App.BLL.Configuration;
using App.BLL.Expenses;
using App.BLL.MaCode;
using App.BLL.MaEmail;
using App.BLL.PurchaseOrder;
using App.BLL.Site;
using App.BLL.Supplier;
using App.Common;
using App.Entities.ViewModels.Expenses;
using App.Entities.ViewModels.PurchaseOrder;
using DevExpress.XtraReports.UI;
using FloridoERP.Report;
using FloridoERPTX.Reports;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace FloridoERPTX.Controllers.Expenses
{
    public class ExpensesController : Controller
    {
        private readonly ExpensesBusiness _ExpensesBusiness;
        private readonly SiteConfigBusiness _siteConfigBusiness;
        private readonly UserMasterBusiness _UserMasterBusiness;
        private readonly SupplierBusiness _supplierBusiness;
        private readonly MaSupplierContactBusiness _maSupplierContactBusiness;
        private readonly MaCodeBusiness _maCodeBusiness;
        private readonly PurchaseOrderGlobalBusiness _purchaseOrderGlobalBusiness;
        private readonly SiteBusiness _siteBusiness;
        private readonly SendMail _sendMail;
        private readonly Common common;
        private readonly MaEmailBusiness _maEmailBusiness;
        private readonly SysRoleMasterBusiness _sysRoleMasterBusiness;


        public ExpensesController()
        {
            _ExpensesBusiness = new ExpensesBusiness();
            _siteConfigBusiness = new SiteConfigBusiness();
            _UserMasterBusiness = new UserMasterBusiness();
            _supplierBusiness = new SupplierBusiness();
            _maSupplierContactBusiness = new MaSupplierContactBusiness();
            _maCodeBusiness = new MaCodeBusiness();
            _purchaseOrderGlobalBusiness = new PurchaseOrderGlobalBusiness();
            _siteBusiness = new SiteBusiness();
            _sendMail = new SendMail();
            common = new Common();
            _maEmailBusiness = new MaEmailBusiness();
            _sysRoleMasterBusiness = new SysRoleMasterBusiness();
        }

        public ActionResult ActionExpensesReport(string SupplierId, string Category, DateTime? BeginDate, DateTime? EndDate, string Currency, string typeExpense, string status)
        {
            if (Session["User"] != null)
                return Json(new { status = true, model = _ExpensesBusiness.ExpensesReport(SupplierId, Category, BeginDate, EndDate, Currency, typeExpense, status) }, JsonRequestBehavior.AllowGet);

            return Json(new { status = false }, JsonRequestBehavior.AllowGet);
        }
        public ActionResult ActionExpensesReportDetail(string SupplierId, string Category, DateTime? BeginDate, DateTime? EndDate, string Currency, string typeExpense, string status)
        {
            if (Session["User"] != null)
                return Json(new { status = true, model = _ExpensesBusiness.ExpensesReportDetail(SupplierId, Category, BeginDate, EndDate, Currency, typeExpense, status) }, JsonRequestBehavior.AllowGet);

            return Json(new { status = false }, JsonRequestBehavior.AllowGet);
        }
        public ActionResult ActionExpensesReportPrintNew(string SupplierId, string Category, DateTime? BeginDate, DateTime? EndDate, string Currency, string typeExpense, string status)
        {
            if (Session["User"] != null)
            {
                var detail = _ExpensesBusiness.ExpensesReport(SupplierId, Category, BeginDate, EndDate, Currency, typeExpense, status);
                ExpenseConcentratedReport report = new ExpenseConcentratedReport();
                (report.Bands["DetailReport"] as DetailReportBand).DataSource = report.printTable(SupplierId, Category, BeginDate, EndDate, Currency, typeExpense, status, detail, _siteBusiness.GetInfoSite());
                (report.Bands["DetailReport"] as DetailReportBand).DataMember = "xrTableRowFormat";
                return PartialView("~/Views/ReportsViews/ExpensesReport.cshtml", report);
            }

            return Json(new { success = false, }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult ActionExpensesReportDetailPrintNew(string SupplierId, string Category, DateTime? BeginDate, DateTime? EndDate, string Currency, string typeExpense, string status)
        {
            if (Session["User"] != null)
            {
                var detail = _ExpensesBusiness.ExpensesReportDetail(SupplierId, Category, BeginDate, EndDate, Currency, typeExpense, status);
                ExpenseConcentratedDetailReport report = new ExpenseConcentratedDetailReport();
                (report.Bands["DetailReport"] as DetailReportBand).DataSource = report.printTable(SupplierId, Category, BeginDate, EndDate, Currency, typeExpense, status, detail, _siteBusiness.GetInfoSite());
                (report.Bands["DetailReport"] as DetailReportBand).DataMember = "xrTableRowFormat";
                return PartialView("~/Views/ReportsViews/ExpensesReport.cshtml", report);
            }

            return Json(new { success = false, }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult ActionExpensesReportPrint(string SupplierId, string Category, DateTime? BeginDate, DateTime? EndDate, string Currency, string typeExpense, string status)
        {
            if (Session["User"] != null)
            {
                List<ItemXMLDetail> items = new List<ItemXMLDetail>();
                string site_code = _siteConfigBusiness.GetSiteCode();
                var model = _ExpensesBusiness.ExpensesReport(SupplierId, Category, BeginDate, EndDate, Currency, typeExpense, status);
                string siteName = _siteConfigBusiness.GetSiteCodeName();
                string nameComplete = _UserMasterBusiness.getCompleteName(Session["User"].ToString());

                //crear procedimiento almacenado
                var itemsModel = model.Select(x => x.ReferenceFolio).Distinct().ToList();

                foreach (var item in itemsModel)
                {
                    var i = _ExpensesBusiness.getAllItemsById(item);
                    if (i != null || i.Count() > 0)
                    {
                        foreach (var e in i)
                        {
                            ItemXMLDetail XMLDetail = new ItemXMLDetail
                            {
                                id = e.id,
                                xml_id = e.xml_id,
                                item_no = e.item_no,
                                item_description = e.item_description,
                                item_amount = e.item_amount,
                                unit_cost = e.unit_cost,
                                quantity = e.quantity,
                                discount = e.discount,
                                iva = e.iva,
                                ieps = e.ieps,
                                ieps_retained = e.ieps_retained,
                                iva_retained = e.iva_retained
                            };
                            items.Add(XMLDetail);
                        }
                    }
                }

                ModelChartExpenses modelChart = new ModelChartExpenses
                {
                    CreditorsExpensive = _ExpensesBusiness.CreditorsExpensive(),
                    CreditorsExpensiveQuantity = _ExpensesBusiness.CreditorsExpensiveQuantity(),
                    CreditorsExpensiveReferenceType = _ExpensesBusiness.CreditorsExpensiveReferenceType()
                };

                ExpenseReport reporte = new ExpenseReport();
                (reporte.Bands["DetailReport"] as DetailReportBand).DataSource = reporte.printTable(siteName, nameComplete, _supplierBusiness.GetNameSupplier(Convert.ToInt32(SupplierId)), Category, BeginDate, EndDate, model, modelChart);
                (reporte.Bands["DetailReport"] as DetailReportBand).DataMember = "xrTable2";
                (reporte.Bands["DetailReport1"] as DetailReportBand).DataSource = reporte.printTable2(items);
                (reporte.Bands["DetailReport1"] as DetailReportBand).DataMember = "xrTable3";

                var stream = new MemoryStream();
                reporte.ExportToPdf(stream);
                reporte.Dispose();
                return Json(new { success = true, responseText = Convert.ToBase64String(stream.ToArray()) }, JsonRequestBehavior.AllowGet);
            }

            return Json(new { success = false, }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult ActionExpenseGetItemById(int ReferenceFolio)
        {
            if (Session["User"] != null)
                return new JsonResult() { Data = _ExpensesBusiness.getAllItemsById(ReferenceFolio), JsonRequestBehavior = JsonRequestBehavior.AllowGet, MaxJsonLength = Int32.MaxValue };
            else
                return Json(new { Data = false }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult ActionGetInformationSupplier(int supplier_id)
        {
            if (Session["User"] != null)
            {
                var supplier = _maSupplierContactBusiness.InfoBasicContactContabilidad(supplier_id);
                var persmission = _maCodeBusiness.GetPermisionXML(supplier.Rfc);
                return Json(new { Data = true, Supplier = supplier, Permission = persmission }, JsonRequestBehavior.AllowGet);
            }
            else
                return Json(new { Data = false }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult ActionGetListGeneric()
        {
            if (Session["User"] != null)
                return new JsonResult() { Data = _maCodeBusiness.GetListGeneric("FIXED_EXPENSE"), JsonRequestBehavior = JsonRequestBehavior.AllowGet, MaxJsonLength = Int32.MaxValue };
            else
                return Json(new { Data = false }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult ActionSaveFixedExpenseFolio(ExpenseModelFolio model, int supplier_id, string type_expense)
        {
            if (Session["User"] != null)
            {
                string User = Session["User"].ToString();
                var data = new errors();

                data = _purchaseOrderGlobalBusiness.ActionFixedExpenseXmlOnlyInvoice(model, User, supplier_id, type_expense);
                //return new JsonResult() { Data = _purchaseOrderGlobalBusiness.ActionFixedExpenseXmlOnlyInvoice(model, User, supplier_id, type_expense), JsonRequestBehavior = JsonRequestBehavior.AllowGet, MaxJsonLength = Int32.MaxValue };
                if (data.pass)
                    return new JsonResult() { Data = true, JsonRequestBehavior = JsonRequestBehavior.AllowGet, MaxJsonLength = Int32.MaxValue };
                else
                    return new JsonResult() { Data = data.error, JsonRequestBehavior = JsonRequestBehavior.AllowGet, MaxJsonLength = Int32.MaxValue };

            }
            else
                return new JsonResult() { Data = "SF", JsonRequestBehavior = JsonRequestBehavior.AllowGet };
        }


        public ActionResult ActionEditExpense(ExpenseModelFolio modelExpense)
        {
            if (Session["User"] != null)
                return new JsonResult() { Data = _purchaseOrderGlobalBusiness.EditExpenseOneDetail(modelExpense, Session["User"].ToString()), JsonRequestBehavior = JsonRequestBehavior.AllowGet };
            else
                return new JsonResult() { Data = "SF", JsonRequestBehavior = JsonRequestBehavior.AllowGet };
        }

        [HttpPost]
        public ActionResult ActionSaveFixedExpense(List<PurchaseOrderGlobalModel> list, int supplier_id, string type_expense, string comment)
        {
            if (Session["User"] != null)
            {
                var site = _siteConfigBusiness.GetSiteCode();
                var x = new errors();
                string User = Session["User"].ToString();

                if (list != null)
                    x = _purchaseOrderGlobalBusiness.ActionFixedExpenseXml(list, User, supplier_id, type_expense, comment);

                if (x.pass)
                    return new JsonResult() { Data = true, JsonRequestBehavior = JsonRequestBehavior.AllowGet, MaxJsonLength = Int32.MaxValue };
                else
                    return new JsonResult() { Data = x.error, JsonRequestBehavior = JsonRequestBehavior.AllowGet, MaxJsonLength = Int32.MaxValue };
            }
            else
                return new JsonResult() { Data = "SF", JsonRequestBehavior = JsonRequestBehavior.AllowGet };
        }

        public ActionResult ActionGetExpenseReportById(int referende_document)
        {
            if (Session["User"] != null)
            {
                var xml_header = _ExpensesBusiness.GetAllItemHeaderById(referende_document);
                var xml_detail = _ExpensesBusiness.getAllItemsById(referende_document);
                FixedExpensesReport report = new FixedExpensesReport();
                if(xml_header.status == 8)
                {
                    report.Watermark.Text = "CANCELADO";
                    report.Watermark.TextDirection = DevExpress.XtraPrinting.Drawing.DirectionMode.ForwardDiagonal;
                    report.Watermark.Font = new Font(report.Watermark.Font.FontFamily, 60);
                    report.Watermark.ForeColor = Color.Firebrick;
                    report.Watermark.TextTransparency = 150;
                    report.Watermark.ShowBehind = false;
                    report.Watermark.PageRange = "1-99";
                }
                else
                {
                    if (xml_header.print_status >= 1)
                    {
                        report.Watermark.Text = "RE-IMPRESION";
                        report.Watermark.TextDirection = DevExpress.XtraPrinting.Drawing.DirectionMode.ForwardDiagonal;
                        report.Watermark.Font = new Font(report.Watermark.Font.FontFamily, 60);
                        report.Watermark.ForeColor = Color.DodgerBlue;
                        report.Watermark.TextTransparency = 150;
                        report.Watermark.ShowBehind = false;
                        report.Watermark.PageRange = "1-99";
                    }
                }
                var updatePrint = _ExpensesBusiness.UpdatePrintXMLHeader(xml_header.expense_id/*referende_document*/);
                (report.Bands["DetailReport"] as DetailReportBand).DataSource = report.printTable(xml_header, xml_detail, _siteBusiness.GetInfoSite());
                (report.Bands["DetailReport"] as DetailReportBand).DataMember = "xrTableRowFormat";
                return PartialView("~/Views/ReportsViews/ExpensesReport.cshtml", report);
            }
            else
                return new JsonResult() { Data = "SF", JsonRequestBehavior = JsonRequestBehavior.AllowGet };
        }

        public ActionResult ActionGetExpenseInfoById(int referende_document)
        {
            if (Session["User"] != null)
            {
                var xml_header = _ExpensesBusiness.GetAllItemHeaderById(referende_document);
                var xml_detail = _ExpensesBusiness.getAllItemsById(referende_document);
                return Json(new { Data = true, DataHeader = xml_header, DataDetail = xml_detail }, JsonRequestBehavior.AllowGet);
            }
            else
                return new JsonResult() { Data = "SF", JsonRequestBehavior = JsonRequestBehavior.AllowGet };
        }

        public ActionResult ActionCancelInvoiceAuthorization(int xml_id, bool flag_month, bool flag_today, string comment)
        {
            if (Session["User"] != null)
            {
                var data = _ExpensesBusiness.ExpenseCancelAuthorization(xml_id, flag_month, flag_today, comment, Session["User"].ToString());
                if (data == 1) //Mandar correo a contabilidad
                {

                    var info_site = _siteBusiness.GetInfoSite();
                    var UuserEmail = _UserMasterBusiness.GetEmailByUserName(Session["User"].ToString());
                    var emailSite = _maEmailBusiness.GetEmailsSiteCode(info_site.site_code);
                    var xml_header = _ExpensesBusiness.GetAllItemHeaderById(xml_id);
                    var xml_detail = _ExpensesBusiness.getAllItemsById(xml_id);
                    var info_supplier = _supplierBusiness.GetSupplierInfoById(xml_header.supplier_id);
                    string ip = _siteConfigBusiness.GetSiteCodeAddress().IP;
                    var AreaContable = _sysRoleMasterBusiness.GetEmailsByRol(ConfigurationManager.AppSettings["AreaContable"].ToString());
                    var Email = emailSite.gerencia_email + "," + UuserEmail + "," + AreaContable;
                    var stream = new MemoryStream();
                    Task task = new Task(() =>
                    {
                        FixedExpensesReport report = new FixedExpensesReport();
                        report.DataSource = report.printTable(xml_header, xml_detail, info_site);
                        report.ExportToPdf(stream);
                        _sendMail.buttonLink = "http://" + ip.Trim() + ":35/Purchases/PURCH024";
                        _sendMail.folio = xml_header.xml_id.ToString();
                        _sendMail.subject = info_site.site_name + " - Solicitud de Cancelación del gasto de la factura " + xml_header.invoice.ToString();
                        _sendMail.from = info_site.site_name;
                        _sendMail.body = "Se le notifica por parte del Sistema del Florido que la sucursal " + info_site.site_name + " ha enviado una solicitud de cancelación de gasto del proveedor <b>" + info_supplier.BusinessName + "</b> con el folio <b>" + xml_header.invoice.ToString() + "</b> para su revisión.";
                        _sendMail.email = Email;
                        common.MailMessageExpensiveGeneric(_sendMail, stream);
                    });
                    task.Start();
                }

                return new JsonResult() { Data = data, JsonRequestBehavior = JsonRequestBehavior.AllowGet };
            }
            else
                return new JsonResult() { Data = "SF", JsonRequestBehavior = JsonRequestBehavior.AllowGet };
        }

        public ActionResult ActionCancelInvoiceAprobal(int xml_id, string comment, bool expense_month)
        {
            if (Session["User"] != null)
            {
                var data = _ExpensesBusiness.ExpenseCancelAproval(xml_id, comment, expense_month, Session["User"].ToString());
                if (data == 2 || data == 3)
                {
                    string subjectEdit = "";
                    string bodyEdit = "";
                    string buttonLinkStr = "";
                    var AreaDistrital = _maCodeBusiness.GetEmailDistrict();

                    var Email = "";
                    var UuserEmail = _UserMasterBusiness.GetEmailByUserName(Session["User"].ToString());
                    var info_site = _siteBusiness.GetInfoSite();
                    var emailSite = _maEmailBusiness.GetEmailsSiteCode(info_site.site_code);
                    var xml_header = _ExpensesBusiness.GetAllItemHeaderById(xml_id);
                    var xml_detail = _ExpensesBusiness.getAllItemsById(xml_id);
                    var info_supplier = _supplierBusiness.GetSupplierInfoById(xml_header.supplier_id);
                    string ip = _siteConfigBusiness.GetSiteCodeAddress().IP;

                    if (data == 2)//Distrital
                    {
                        Email = UuserEmail + "," + AreaDistrital;
                        buttonLinkStr = "http://" + ip.Trim() + ":35/Purchases/PURCH025";
                        subjectEdit = " - Solicitud de Cancelación del gasto de la factura " + xml_header.invoice.ToString();
                        bodyEdit = "Se le notifica por parte del Sistema del Florido que la sucursal " + info_site.site_name + " ha enviado una solicitud de cancelación de gasto del proveedor <b>" + info_supplier.BusinessName + "</b> con el folio <b>" + xml_header.invoice.ToString() + "</b> para su revisión.";
                    }
                    else //Gerente
                    {
                        Email = UuserEmail + "," + emailSite.gerencia_email;
                        buttonLinkStr = "http://" + ip.Trim() + ":35/Purchases/PURCH026"; 
                        subjectEdit = " - Gasto pendiente para aplicar: factura " + xml_header.invoice.ToString();
                        bodyEdit = "Se le notifica por parte del Sistema del Florido de la sucursal " + info_site.site_name + " ha aprobado la cancelación del gasto del proveedor <b>" + info_supplier.BusinessName + "</b> con el folio <b>" + xml_header.invoice.ToString() + "</b>, el unico paso que falta es el Aplicar la cancelacion de gasto.";
                    }
                    var stream = new MemoryStream();
                    Task task = new Task(() =>
                    {
                        FixedExpensesReport report = new FixedExpensesReport();
                        report.DataSource = report.printTable(xml_header, xml_detail, info_site);
                        report.ExportToPdf(stream);
                        _sendMail.buttonLink = buttonLinkStr;
                        _sendMail.folio = xml_header.xml_id.ToString();
                        _sendMail.subject = info_site.site_name + subjectEdit;
                        _sendMail.from = info_site.site_name;
                        _sendMail.body = bodyEdit;
                        _sendMail.email = Email;
                        common.MailMessageExpensiveGeneric(_sendMail, stream);
                    });
                    task.Start();
                }
                return new JsonResult() { Data = data, JsonRequestBehavior = JsonRequestBehavior.AllowGet };
            }
            else
                return new JsonResult() { Data = "SF", JsonRequestBehavior = JsonRequestBehavior.AllowGet };
        }

        public ActionResult ActionCancelInvoiceFinal(int xml_id, string comment)
        {
            if (Session["User"] != null)
            {
                var data = _ExpensesBusiness.ExpenseCancelFinal(xml_id, comment, Session["User"].ToString());
                if (data == 8)
                {
                    var UuserEmail = _UserMasterBusiness.GetEmailByUserName(Session["User"].ToString());
                    var info_site = _siteBusiness.GetInfoSite();
                    var emailSite = _maEmailBusiness.GetEmailsSiteCode(info_site.site_code);
                    var xml_header = _ExpensesBusiness.GetAllItemHeaderById(xml_id);
                    var xml_detail = _ExpensesBusiness.getAllItemsById(xml_id);
                    var info_supplier = _supplierBusiness.GetSupplierInfoById(xml_header.supplier_id);
                    string ip = _siteConfigBusiness.GetSiteCodeAddress().IP;
                    var Email = emailSite.gerencia_email + "," + UuserEmail;
                    var stream = new MemoryStream();
                    Task task = new Task(() =>
                    {
                        FixedExpensesReport report = new FixedExpensesReport();
                        report.Watermark.Text = "CANCELADO";
                        report.Watermark.TextDirection = DevExpress.XtraPrinting.Drawing.DirectionMode.ForwardDiagonal;
                        report.Watermark.Font = new Font(report.Watermark.Font.FontFamily, 60);
                        report.Watermark.ForeColor = Color.Firebrick;
                        report.Watermark.TextTransparency = 150;
                        report.Watermark.ShowBehind = false;
                        report.Watermark.PageRange = "1-99";
                        report.DataSource = report.printTable(xml_header, xml_detail, info_site);
                        report.ExportToPdf(stream);
                        _sendMail.buttonLink = "http://" + ip.Trim() + ":35/Purchases/PURC007";
                        _sendMail.folio = xml_header.xml_id.ToString();
                        _sendMail.subject = info_site.site_name + " - Cancelación Realizada de la factura " + xml_header.invoice.ToString();
                        _sendMail.from = info_site.site_name;
                        _sendMail.body = "Se le notifica por parte del Sistema del Florido que la sucursal " + info_site.site_name + " ha realizado la cancelacion del gasto del proveedor <b>" + info_supplier.BusinessName + "</b> con el folio <b>" + xml_header.invoice.ToString() + "</b>.";
                        _sendMail.email = Email;
                        common.MailMessageExpensiveGeneric(_sendMail, stream);
                    });
                    task.Start();
                }

                return new JsonResult() { Data = data, JsonRequestBehavior = JsonRequestBehavior.AllowGet };
            }
            else
                return new JsonResult() { Data = "SF", JsonRequestBehavior = JsonRequestBehavior.AllowGet };
        }
        public ActionResult ActionCancelPendingStore(int xml_id)
        {
            if (Session["User"] != null)
            {
                var data = _ExpensesBusiness.ExpenseCancelPendingStore(xml_id, Session["User"].ToString());
                if (data == 8)
                {
                    var info_site = _siteBusiness.GetInfoSite();
                    var emailSite = _maEmailBusiness.GetEmailsSiteCode(info_site.site_code);
                    var xml_header = _ExpensesBusiness.GetAllItemHeaderById(xml_id);
                    var xml_detail = _ExpensesBusiness.getAllItemsById(xml_id);
                    var info_supplier = _supplierBusiness.GetSupplierInfoById(xml_header.supplier_id);
                    string ip = _siteConfigBusiness.GetSiteCodeAddress().IP;
                    var Email = emailSite.gerencia_email;
                    var stream = new MemoryStream();
                    Task task = new Task(() =>
                    {
                        FixedExpensesReport report = new FixedExpensesReport();
                        report.Watermark.Text = "CANCELADO";
                        report.Watermark.TextDirection = DevExpress.XtraPrinting.Drawing.DirectionMode.ForwardDiagonal;
                        report.Watermark.Font = new Font(report.Watermark.Font.FontFamily, 60);
                        report.Watermark.ForeColor = Color.Firebrick;
                        report.Watermark.TextTransparency = 150;
                        report.Watermark.ShowBehind = false;
                        report.Watermark.PageRange = "1-99";
                        report.DataSource = report.printTable(xml_header, xml_detail, info_site);
                        report.ExportToPdf(stream);
                        _sendMail.buttonLink = "http://" + ip.Trim() + ":35/Purchases/PURC007";
                        _sendMail.folio = xml_header.xml_id.ToString();
                        _sendMail.subject = info_site.site_name + " - Cancelación Realizada de la factura " + xml_header.invoice.ToString();
                        _sendMail.from = info_site.site_name;
                        _sendMail.body = "Se le notifica por parte del Sistema del Florido que la sucursal " + info_site.site_name + " ha realizado la cancelacion del gasto del proveedor <b>" + info_supplier.BusinessName + "</b> con el folio <b>" + xml_header.invoice.ToString() + "</b>.";
                        _sendMail.email = Email;
                        common.MailMessageExpensiveGeneric(_sendMail, stream);
                    });
                    task.Start();
                }

                return new JsonResult() { Data = data, JsonRequestBehavior = JsonRequestBehavior.AllowGet };
            }
            else
                return new JsonResult() { Data = "SF", JsonRequestBehavior = JsonRequestBehavior.AllowGet };
        }


        public ActionResult ActionCancelInvoiceReverse(int xml_id, string comment, int old_status, string program_id)
        {
            if (Session["User"] != null)
            {
                var data = _ExpensesBusiness.ExpenseReverseStatus(xml_id, comment, old_status, program_id, Session["User"].ToString());
                if (data == 9)
                {
                    var extra = "";
                    if (old_status == 1)
                        extra = " del Area Contable ";
                    else if (old_status == 2)
                        extra = " del Distrital ";
                    else if (old_status == 3)
                        extra = " del Area de Surcursal ";
                    var UuserEmail = _UserMasterBusiness.GetEmailByUserName(Session["User"].ToString());
                    var info_site = _siteBusiness.GetInfoSite();
                    var emailSite = _maEmailBusiness.GetEmailsSiteCode(info_site.site_code);
                    var xml_header = _ExpensesBusiness.GetAllItemHeaderById(xml_id);
                    var xml_detail = _ExpensesBusiness.getAllItemsById(xml_id);
                    var info_supplier = _supplierBusiness.GetSupplierInfoById(xml_header.supplier_id);
                    string ip = _siteConfigBusiness.GetSiteCodeAddress().IP;
                    var Email = emailSite.gerencia_email + "," + UuserEmail;
                    var stream = new MemoryStream();
                    Task task = new Task(() =>
                    {
                        FixedExpensesReport report = new FixedExpensesReport();
                        report.DataSource = report.printTable(xml_header, xml_detail, info_site);
                        report.ExportToPdf(stream);
                        _sendMail.buttonLink = "http://" + ip.Trim() + ":35/Purchases/PURC007";
                        _sendMail.folio = xml_header.xml_id.ToString();
                        _sendMail.subject = info_site.site_name + " - Cancelación Rechazada de la factura " + xml_header.invoice.ToString();
                        _sendMail.from = info_site.site_name;
                        _sendMail.body = "Se le notifica por parte del Sistema del Florido que la sucursal " + info_site.site_name + " ha rechazado de parte " + extra + "del gasto del proveedor <b>" + info_supplier.BusinessName + "</b> con el folio <b>" + xml_header.invoice.ToString() + "</b>.";
                        _sendMail.email = Email;
                        common.MailMessageExpensiveGeneric(_sendMail, stream);
                    });
                    task.Start();
                }
                return new JsonResult() { Data = data, JsonRequestBehavior = JsonRequestBehavior.AllowGet };
            }
            else
                return new JsonResult() { Data = "SF", JsonRequestBehavior = JsonRequestBehavior.AllowGet };
        }
        //public ActionResult ActionGetExpenseReportConcentrated(int referende_document)
        //{
        //    if (Session["User"] != null)
        //    {
        //        var xml_header = _ExpensesBusiness.getAllItemHeaderById(referende_document);
        //        var xml_detail = _ExpensesBusiness.getAllItemsById(referende_document);
        //        ExpenseConcentratedReport report = new ExpenseConcentratedReport();
        //        (report.Bands["DetailReport"] as DetailReportBand).DataSource = report.printTable(xml_header, xml_detail, _siteBusiness.GetInfoSite());
        //        (report.Bands["DetailReport"] as DetailReportBand).DataMember = "xrTableRowFormat";
        //        return PartialView("~/Views/ReportsViews/ExpensesReport.cshtml", report);
        //    }
        //    else
        //        return new JsonResult() { Data = "SF", JsonRequestBehavior = JsonRequestBehavior.AllowGet };
        //}
    }


}