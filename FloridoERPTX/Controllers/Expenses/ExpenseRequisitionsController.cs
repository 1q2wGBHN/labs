﻿using System.Web.Mvc;
using App.BLL.Expenses;
using App.BLL.InternalRequirement;
using App.Common;
using App.DAL.Expenses;

namespace FloridoERPTX.Controllers.Expenses
{
    public class ExpenseRequisitionsController : Controller
    {
        // GET: ExpenseRequisition
        private readonly InternalRequirementBusiness _internalRequirementBusiness = new InternalRequirementBusiness();
        private readonly ExpenseRequisitionsBussines _expenseBusiness = new ExpenseRequisitionsBussines();
         
        
        public JsonResult ActionHistoryList(ExpenseRequisitionFilter filter)
        {
            if (Session["User"] == null) return Json(Result.SF(), JsonRequestBehavior.AllowGet);
            if(filter==null) filter = new ExpenseRequisitionFilter();
            filter.user_name = Session["User"].ToString();
            var r = _expenseBusiness.ApprovalHistoryList(filter);
            return Json(r, JsonRequestBehavior.AllowGet);
        }
        
        public JsonResult ActionRequestsHistoryList(ExpenseRequisitionFilter filter)
        {
            if (Session["User"] == null) return Json(Result.SF(), JsonRequestBehavior.AllowGet);
            if(filter==null) filter = new ExpenseRequisitionFilter();
            filter.user_name = Session["User"].ToString();
            var r = _expenseBusiness.RequestsHistoryList(filter);
            return Json(r, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public JsonResult ActionExpenseStart(ExpenseStartReq data)
        {
            if (Session["User"] == null) return Json(Result.SF(), JsonRequestBehavior.AllowGet);
            data.Username = Session["User"].ToString();
            return Json(_expenseBusiness.ExpenseStart(data), JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult ActionExpenseUpdate(ExpenseUpdateReq data)
        {
            if (Session["User"] == null) return Json(Result.SF(), JsonRequestBehavior.AllowGet);
            data.user_name = Session["User"].ToString();
            return Json(_expenseBusiness.ExpenseUpdate(data), JsonRequestBehavior.AllowGet);
        }

        public JsonResult ActionSearchItems(string search)
        {
            if (Session["User"] == null) return Json(Result.SF(), JsonRequestBehavior.AllowGet);
           return Json(Result.OK(_internalRequirementBusiness.SearchItemsCreditors(search)), JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public JsonResult ActionAddXml(XmlReq data)
        {
            if (Session["User"] == null) return Json(Result.SF(), JsonRequestBehavior.AllowGet);
            data.user_name = Session["User"].ToString();
            return Json(_expenseBusiness.ActionAddXml(data), JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public JsonResult ActionConfirmCancel(int folio)
        {
            if (Session["User"] == null) return Json(Result.SF(), JsonRequestBehavior.AllowGet);
            
            return Json(_expenseBusiness.ConfirmCancel(folio), JsonRequestBehavior.AllowGet);
        }
        public JsonResult ActionGetDetails(int folio)
        {
            if (Session["User"] == null) return Json(Result.SF(), JsonRequestBehavior.AllowGet);
            return Json(Result.OK(_expenseBusiness.GetDetailsByFolio(folio)), JsonRequestBehavior.AllowGet);
        }
        
    }
}