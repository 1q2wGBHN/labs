﻿using App.BLL.StorageLocation;
using App.Entities.ViewModels.StorageLocation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using App.Common;
using App.DAL.StorageLocation;

namespace FloridoERPTX.Controllers.Location
{
    public class PlanogramLocationController : Controller
    {
        private readonly PlanogramLocationBusiness _PlanogramLocationBusiness;

        public PlanogramLocationController()
        {
            _PlanogramLocationBusiness = new PlanogramLocationBusiness();
        }

        [HttpPost]
        public ActionResult ActionExecute(PlanogramLocationModel model , bool typeExecute,string planogram_locationEdit,string itemProductEdit)
        {
            if (HttpContext.User.Identity.Name != "")
            {
                if (ModelState.IsValid)
                {
                    DateTime cdate = DateTime.Now;
                    string uuser = HttpContext.User.Identity.Name;
                    model.cuser = uuser;
                    model.uuser = uuser;
                    model.program_id = "STGL002.cshtml";
                    if (typeExecute)
                    {
                        var addReturn = _PlanogramLocationBusiness.AddPlanogramLocation(model);
                        if (addReturn)
                        {
                            model.PlanogramList = _PlanogramLocationBusiness.GetAllPlanogramLocations();
                            return new JsonResult() { Data = new { success = true, JsonList = model.PlanogramList, responseText = "Registro Exitoso!" }, JsonRequestBehavior = JsonRequestBehavior.AllowGet, MaxJsonLength = Int32.MaxValue };
                        }
                        else
                            return Json(new { success = false, responseText = "Error al registrar!" }, JsonRequestBehavior.AllowGet);
                    }
                    else
                    {

                        var updateReturn = _PlanogramLocationBusiness.UpdatePlanogramLocation(model, planogram_locationEdit, itemProductEdit);
                        if (updateReturn)
                        {
                            model.PlanogramList = _PlanogramLocationBusiness.GetAllPlanogramLocations();
                            return new JsonResult() { Data = new { success = true, JsonList = model.PlanogramList, responseText = "Registro Exitoso!" }, JsonRequestBehavior = JsonRequestBehavior.AllowGet, MaxJsonLength = Int32.MaxValue };
                        }//return Json(new { success = true, responseText = "Registro Exitoso!" }, JsonRequestBehavior.AllowGet);
                        else
                            return Json(new { success = false, responseText = "Error al registrar!" }, JsonRequestBehavior.AllowGet);
                    }
                }
                else
                {
                    var errors = ModelState.Select(x => x.Value.Errors).Where(y => y.Count > 0).ToList();
                    return Json(new { success = false, responseText = "Error Desconocido "+errors+", contacte a sistemas" }, JsonRequestBehavior.AllowGet);
                }
            }
            else
                return Json(new { success = false, responseText = "Termino tu sesión.!" }, JsonRequestBehavior.AllowGet);
        }


        [HttpPost]
        public ActionResult ActionCreateAndroid(PlanogramLocationModel model)
        {
            var storageCreate = _PlanogramLocationBusiness.AddPlanogramLocationAndroid(model);
            return Json(storageCreate);
        }

        [HttpPost]
        public ActionResult ActionEditAndroid(PlanogramLocationModel model, string planogram_location_old, string item_product_old)
        {
            var storageCreate = _PlanogramLocationBusiness.UpdatePlanogramLocationAndroid(model,planogram_location_old,item_product_old);
            return Json(storageCreate);
        }
        public ActionResult ActionGetLocation(string loc1, string loc2, string loc3,string part_number)
        {
            var location = _PlanogramLocationBusiness.GetLocation(loc1,loc2,loc3, part_number);
            
            return Json(location != null ? Result.OK(location) : Result.Error("No se encontro registro"), JsonRequestBehavior.AllowGet);
        }

        public ActionResult ActionLocations1()
        {
            var locations = _PlanogramLocationBusiness.PlanogramLocation1();
            return Json( Result.OK(locations),JsonRequestBehavior.AllowGet );
        }
        public ActionResult ActionLocations2(string loc1)
        {
            var locations = _PlanogramLocationBusiness.PlanogramLocation2(loc1);
            return Json(Result.OK(locations), JsonRequestBehavior.AllowGet);
        }
        public ActionResult ActionLocations3(string loc1,string loc2)
        {
            var locations = _PlanogramLocationBusiness.PlanogramLocation3(loc1,loc2);
            return Json(Result.OK(locations), JsonRequestBehavior.AllowGet);
        }
        public ActionResult ActionLocations(string loc1, string loc2)
        {
            var locations = _PlanogramLocationBusiness.PlanogramLocations(loc1, loc2);
            return Json(Result.OK(locations), JsonRequestBehavior.AllowGet);
        }

        public ActionResult ActionTagsWithFilter(PlanogramLocationRepository.PlanogramFilter filter)
        {
            var tags = _PlanogramLocationBusiness.TagsWithFilter(filter);
            return Json(Result.OK(tags), JsonRequestBehavior.AllowGet);
        }
    }
}