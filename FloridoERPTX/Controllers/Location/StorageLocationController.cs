﻿using App.BLL.StorageLocation;
using App.DAL.Site;
using App.Entities.ViewModels.StorageLocation;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using App.Common;

namespace FloridoERPTX.Controllers.Location
{
    public class StorageLocationController : Controller
    {
        private readonly StorageLocationBusiness _storageLocationBusiness;
        private readonly SiteConfigRepository _siteConfigRepository;

        public StorageLocationController()
        {
            _storageLocationBusiness = new StorageLocationBusiness();
            _siteConfigRepository = new SiteConfigRepository();
        }

        public ActionResult ActionNameAlreadyExists(string storage_name)
        {
            var location = _storageLocationBusiness.GetLocationById(storage_name);
            if (location != null) { return Json(new { success = true, responseText = "¡Esta Nombre ya existe!" }, JsonRequestBehavior.AllowGet); }
            else { return Json(new { success = false, responseText = "¡Este Nombre esta disponible!" }, JsonRequestBehavior.AllowGet); }
        }

        [HttpPost]
        public ActionResult ActionCreate(StorageLocationModel model)
        {
            if (Session["User"] != null)
            {
                if (ModelState.IsValid)
                {
                    model.cuser = Session["User"].ToString();                    
                    if (_storageLocationBusiness.AddLocation(model))
                    {
                        var locations = _storageLocationBusiness.GetAllLocationsInStatusActive();
                        var json = JsonConvert.SerializeObject(locations, Formatting.Indented, new JsonSerializerSettings()
                        {
                            ReferenceLoopHandling = ReferenceLoopHandling.Ignore
                        });
                        return Json(new { success = true, Json = json, responseText = "Registro Exitoso!" }, JsonRequestBehavior.AllowGet);
                    }
                    else
                        return Json(new { success = false, responseText = "Ocurrió un error al registrar los datos.!" }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    var errors = ModelState.Select(x => x.Value.Errors).Where(y => y.Count > 0).ToList();
                    return Json(new { success = false, responseText = "Error Desconocido, contacte a sistemas" }, JsonRequestBehavior.AllowGet);
                }
            }
            else
                return Json(new { success = false, responseText = "Termino tu sesión.!" }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult ActionEdit(StorageLocationModel model)
        {
            if (Session["User"] != null)
            {
                if (ModelState.IsValid)
                {
                    var location = _storageLocationBusiness.GetLocationByNameAndSite(model.storage_location, _siteConfigRepository.GetSiteCode());
                    location.uuser = Session["User"].ToString();                    
                    if (_storageLocationBusiness.UpdateLocation(location, model.storage_type, model.active_flag))
                    {
                        var json = JsonConvert.SerializeObject(_storageLocationBusiness.GetAllLocations(), Formatting.Indented, new JsonSerializerSettings()
                        {
                            ReferenceLoopHandling = ReferenceLoopHandling.Ignore
                        });

                        return Json(new { success = true, Json = json, responseText = "Registro Actualizado!" }, JsonRequestBehavior.AllowGet);
                    }
                    else
                        return Json(new { success = false, responseText = "Ocurrió un error al actualizar los datos.!" }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    var errors = ModelState.Select(x => x.Value.Errors).Where(y => y.Count > 0).ToList();

                    return Json(new { success = false, responseText = "Error Desconocido, contacte a sistemas" }, JsonRequestBehavior.AllowGet);
                }
            }
            else
                return Json(new { success = false, responseText = "Termino tu sesión.!" }, JsonRequestBehavior.AllowGet);
        }
    }
}