﻿using App.BLL.CostCenter;
using App.BLL.Site;
using App.Entities.ViewModels.CostCenter;
using FloridoERPTX.Reports;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace FloridoERPTX.Controllers.CostCenter
{
    public class CostCenterController : Controller
    {
        private readonly CostCenterBusiness _CostCenterBusiness;
        private readonly SiteBusiness _SiteBusiness;

        public CostCenterController()
        {
            _CostCenterBusiness = new CostCenterBusiness();
            _SiteBusiness = new SiteBusiness();
        }

        public ActionResult ActionGetCostCenterRecords(DateTime BeginDate, DateTime EndDate, string ReportType)
        {
            if (Session["User"] != null)
            {
                if (ReportType == "movimiento")
                    return Json(new { result = true, Data = _CostCenterBusiness.GetCostCenterRecords(BeginDate, EndDate) }, JsonRequestBehavior.AllowGet);
                else
                    return Json(new { result = true, Data = _CostCenterBusiness.GetCostCenterRecordsDetail(BeginDate, EndDate) }, JsonRequestBehavior.AllowGet);
            }

            return Json(new { result = "SF" }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult ActionGetPendingCostCenterRecords(DateTime BeginDate, DateTime EndDate)
        {
            if (Session["User"] != null)
                return Json(new { result = true, model = _CostCenterBusiness.GetCostCenterRecords(BeginDate, EndDate, 1) }, JsonRequestBehavior.AllowGet);

            return Json(new { result = "SF" }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult ActionGetCostCenterItems(int GiCostCenterId)
        {
            if (Session["User"] != null)
                return Json(new { result = true, Data = _CostCenterBusiness.GetCostCenterDetailsById(GiCostCenterId) }, JsonRequestBehavior.AllowGet);

            return Json(new { result = "SF" }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult ActionGetCostCenter(int CenterCostId)
        {
            if (Session["User"] != null)
                return Json(new { result = true, Data = _CostCenterBusiness.GetCostCenterById(CenterCostId) }, JsonRequestBehavior.AllowGet);

            return Json(new { result = "SF" }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult ActionGetCostCenterReport(string GiDocument)
        {
            if (Session["User"] != null)
                return Report(GiDocument);

            return Json(new { result = "SF" }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult ActionGetCostCenterDetailReport(DateTime BeginDate, DateTime EndDate)
        {
            if (Session["User"] != null)
                return ReportDetail(BeginDate, EndDate);

            return Json(new { result = "SF" }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult ActionSaveCostCenter(string CostCenter, int Status, List<GICostCenterItem> Items, int CenterCostId, string ReportType)
        {
            if (Session["User"] != null)
            {
                if (CenterCostId > 0)
                {
                    if (_CostCenterBusiness.UpdateCostCenter(CenterCostId, CostCenter, Status, Session["User"].ToString(), Items, "COST001.cshtml"))
                        return Json(new { result = true }, JsonRequestBehavior.AllowGet);
                    else
                        return Json(new { result = false }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    CenterCostId = _CostCenterBusiness.CreateCostCenter(CostCenter, Status, Session["User"].ToString(), Items, "COST001.cshtml");
                    if (CenterCostId > 0)
                    {
                        if (ReportType == "movimiento")
                            return Json(new { result = true, Id = CenterCostId, Data = _CostCenterBusiness.GetCostCenterRecords(DateTime.Now.AddDays(-7), DateTime.Now) }, JsonRequestBehavior.AllowGet);
                        else
                            return Json(new { result = true, Id = CenterCostId, Data = _CostCenterBusiness.GetCostCenterRecordsDetail(DateTime.Now.AddDays(-7), DateTime.Now) }, JsonRequestBehavior.AllowGet);
                    }
                    else
                        return Json(new { result = false }, JsonRequestBehavior.AllowGet);
                }
            }

            return Json(new { result = "SF" }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult ActionProcessCostCenter(string CostCenter, int Status, List<GICostCenterItem> Items, int CenterCostId)
        {
            if (Session["User"] != null)
            {
                if (CenterCostId > 0)
                {
                    if (_CostCenterBusiness.UpdateProcessCostCenter(CenterCostId, CostCenter, Status, Session["User"].ToString(), Items, "COST001.cshtml"))
                        return Json(new { result = true, Data = _CostCenterBusiness.GetCostCenterRecords(DateTime.Now.AddDays(-7), DateTime.Now) }, JsonRequestBehavior.AllowGet);
                    else
                        return Json(new { result = false }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    if (_CostCenterBusiness.ProcessCostCenter(CostCenter, Status, Session["User"].ToString(), Items, "COST001.cshtml"))
                        return Json(new { result = true, Data = _CostCenterBusiness.GetCostCenterRecords(DateTime.Now.AddDays(-7), DateTime.Now) }, JsonRequestBehavior.AllowGet);
                    else
                        return Json(new { result = false }, JsonRequestBehavior.AllowGet);
                }
            }

            return Json(new { result = "SF" }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult ActionAuthorizeCostCenter(int CostCenterId, List<GICostCenterItem> Items)
        {
            if (Session["User"] != null)
            {
                string GiDocument;
                if (_CostCenterBusiness.AproveProcessCostCenter(CostCenterId, Items, Session["User"].ToString(), out GiDocument))
                    return Report(GiDocument, "movimiento");
                else
                    return Json(new { result = false }, JsonRequestBehavior.AllowGet);
            }

            return Json(new { result = "SF" }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult ActionCancelCostCenter(int CostCenterId)
        {
            if (Session["User"] != null)
            {
                if (_CostCenterBusiness.CancelCostCenter(CostCenterId, Session["User"].ToString()))
                    return Json(new { result = true, model = _CostCenterBusiness.GetCostCenterRecords(DateTime.Now.AddDays(-7), DateTime.Now, 1) }, JsonRequestBehavior.AllowGet);
                else
                    return Json(new { result = false }, JsonRequestBehavior.AllowGet);
            }

            return Json(new { result = "SF" }, JsonRequestBehavior.AllowGet);
        }

        private ActionResult Report(string GiDocument, string ReportType = "")
        {
            var model = _CostCenterBusiness.GetGICostCenterReport(GiDocument);
            if (model != null)
            {
                int print_status = _CostCenterBusiness.GetCostCenterPrintStatus(GiDocument);

                var stream = new MemoryStream();
                CostCenterDocument report = new CostCenterDocument();
                report.DataSource = report.PrintTable(model);
                if (print_status != 0)
                {
                    report.Watermark.Text = "RE-IMPRESION";
                    report.Watermark.TextDirection = DevExpress.XtraPrinting.Drawing.DirectionMode.ForwardDiagonal;
                    report.Watermark.Font = new Font(report.Watermark.Font.FontFamily, 40);
                    report.Watermark.ForeColor = Color.DodgerBlue;
                    report.Watermark.TextTransparency = 150;
                    report.Watermark.ShowBehind = false;
                    report.Watermark.PageRange = "1-99";
                }

                _CostCenterBusiness.GetCostCenterUpdatePrintStatus(GiDocument, print_status, Session["User"].ToString());
                report.ExportToPdf(stream);
                report.Dispose();

                if (ReportType == "movimiento")
                    return Json(new { result = true, Report = Convert.ToBase64String(stream.ToArray()), Data = _CostCenterBusiness.GetCostCenterRecords(DateTime.Now.AddDays(-7), DateTime.Now) }, JsonRequestBehavior.AllowGet);
                else if (ReportType == "detalle")
                    return Json(new { result = true, Report = Convert.ToBase64String(stream.ToArray()), Data = _CostCenterBusiness.GetCostCenterRecordsDetail(DateTime.Now.AddDays(-7), DateTime.Now) }, JsonRequestBehavior.AllowGet);
                else
                    return Json(new { result = true, Report = Convert.ToBase64String(stream.ToArray()) }, JsonRequestBehavior.AllowGet);
            }
            else
                return Json(new { result = false }, JsonRequestBehavior.AllowGet);
        }

        private ActionResult ReportDetail(DateTime BeginDate, DateTime EndDate)
        {
            var model = _CostCenterBusiness.GetCostCenterDetailsByDates(BeginDate, EndDate);
            if (model != null)
            {
                var stream = new MemoryStream();
                CostCenterDocumentDetail report = new CostCenterDocumentDetail();
                report.DataSource = report.PrintTable(model, _SiteBusiness.GetInfoSite().site_name, BeginDate, EndDate);
                report.ExportToPdf(stream);
                report.Dispose();

                return Json(new { result = true, Report = Convert.ToBase64String(stream.ToArray()) }, JsonRequestBehavior.AllowGet);
            }
            else
                return Json(new { result = false }, JsonRequestBehavior.AllowGet);
        }
    }
}