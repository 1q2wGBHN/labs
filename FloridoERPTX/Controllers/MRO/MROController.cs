﻿using App.BLL.ServicesCalendar;
using App.BLL.Site;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace FloridoERPTX.Controllers.MRO
{
    public class MROController : Controller
    {
        private readonly ServicesCalendarHeaderBusiness _servicesCalendarHeaderBusiness;
        private readonly SiteConfigBusiness _siteConfigBusiness;

        public MROController()
        {
            _servicesCalendarHeaderBusiness = new ServicesCalendarHeaderBusiness();
            _siteConfigBusiness = new SiteConfigBusiness();
        }
        //Menu MRO
        public ActionResult MMRO000()
        {
            return View();
        }
        //Menu Maquinaria y Equipo
        public ActionResult MRO000()
        {
            return View();
        }
        //Vista Servicios Programados
        public ActionResult MRO001()
        {
            return View(_servicesCalendarHeaderBusiness.GetServicesHeaderByDateAndStatus(_siteConfigBusiness.GetSiteCode(), DateTime.Now, DateTime.Now.AddDays(7), 1));
        }
    }
}