using System;
using System.Web.Mvc;
using App.BLL.Promotions;

namespace FloridoERPTX.Controllers.Promotions
{
    public class PromotionController: Controller
    {
        private readonly PromotionBusiness _promotionBusinees;
        public PromotionController()
        {
            _promotionBusinees = new PromotionBusiness();
        }
        public ActionResult ActionGetPromotionById(string FolioPromotion, string type)
        {
            if (Session["User"] != null)
                return new JsonResult() { Data = _promotionBusinees.GetPromotionId(FolioPromotion, type), JsonRequestBehavior = JsonRequestBehavior.AllowGet, MaxJsonLength = Int32.MaxValue };
            else
                return Json(new { Data = false }, JsonRequestBehavior.AllowGet);
        }
        public ActionResult ActionGetPromotion(DateTime? BeginDate, DateTime? EndDate, string type, string item)
        {
            if (Session["User"] != null)
                return Json(new { status = true, model = _promotionBusinees.GetAllPromotions(type, item, BeginDate, EndDate) }, JsonRequestBehavior.AllowGet);
            return Json(new { status = false }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult ActionGetPromotionCommon(string promotion_code, string common)
        {
            if (Session["User"] != null)
                return Json(new { status = true, model = _promotionBusinees.GetPromotionAdvancedCommonByCode(promotion_code, common) }, JsonRequestBehavior.AllowGet);
            return Json(new { status = false }, JsonRequestBehavior.AllowGet);
        }
    }
}