﻿using App.BLL.UserSpecs;
using App.Entities.ViewModels.UserSpecs;
using System;
using System.Web.Mvc;

namespace FloridoERPTX.Controllers.UserSpecs
{
    public class UserSpecsController : Controller
    {
        private readonly UserSpecsBusiness _UserSpecsBusinnes;

        public UserSpecsController()
        {
            _UserSpecsBusinnes = new UserSpecsBusiness();
        }

        public ActionResult ActionGetAllUserSpecs()
        {
            return new JsonResult() { Data = _UserSpecsBusinnes.GetAllUserSpecs(), JsonRequestBehavior = JsonRequestBehavior.AllowGet, MaxJsonLength = Int32.MaxValue };
        }

        [HttpPost]
        public ActionResult ActionSaveUserSpeck(UserSpecsModel UserSpecs)
        {
            if (Session["User"] != null)
                return new JsonResult() { Data = _UserSpecsBusinnes.SaveUserSpeck(UserSpecs, Session["User"].ToString()), JsonRequestBehavior = JsonRequestBehavior.AllowGet, MaxJsonLength = Int32.MaxValue };
            else
                return new JsonResult() { Data = "SF", JsonRequestBehavior = JsonRequestBehavior.AllowGet, MaxJsonLength = Int32.MaxValue };
        }

        [HttpPost]
        public ActionResult ActionEditUserSpeck(UserSpecsModel UserSpecs)
        {
            if (Session["User"] != null)
                return new JsonResult() { Data = _UserSpecsBusinnes.EditUserSpeck(UserSpecs, Session["User"].ToString()), JsonRequestBehavior = JsonRequestBehavior.AllowGet, MaxJsonLength = Int32.MaxValue };
            else
                return new JsonResult() { Data = "SF", JsonRequestBehavior = JsonRequestBehavior.AllowGet, MaxJsonLength = Int32.MaxValue };
        }
    }
}