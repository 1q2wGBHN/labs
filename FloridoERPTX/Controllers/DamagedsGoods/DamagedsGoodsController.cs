﻿using App.BLL;
using App.BLL.Configuration;
using App.BLL.Rma;
using App.BLL.Site;
using App.BLL.Supplier;
using App.Common;
using App.DAL;
using App.DAL.Item;
using App.DAL.Site;
using App.Entities.ViewModels.DamagedsGoods;
using FloridoERPTX.Reports;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace FloridoERPTX.Controllers.Rma
{
    public class DamagedsGoodsController : Controller
    {
        private readonly SiteConfigRepository _siteConfigRepository;
        private readonly DamagedsGoodsBusiness _RmaHeadBusiness;
        private readonly DamagedsGoodsItemBusiness _damagedsGoodsItemBusiness;
        private readonly MaSupplierContactBusiness _MaSupplierContact;
        private readonly DamagedsGoodsItemBusiness _DamagedsGoodsItemBusiness;
        private readonly UserMasterBusiness _userMasterBusiness;
        private readonly SupplierBusiness _supplierBusiness;
        private readonly SysRoleMasterBusiness _sysRoleMasterBusiness;
        private readonly Common common;
        private readonly SendMail information;
        private readonly SiteConfigBusiness _siteConfigBusiness;

        public DamagedsGoodsController()
        {
            _RmaHeadBusiness = new DamagedsGoodsBusiness();
            _MaSupplierContact = new MaSupplierContactBusiness();
            _DamagedsGoodsItemBusiness = new DamagedsGoodsItemBusiness();
            _damagedsGoodsItemBusiness = new DamagedsGoodsItemBusiness();
            _userMasterBusiness = new UserMasterBusiness();
            _supplierBusiness = new SupplierBusiness();
            _sysRoleMasterBusiness = new SysRoleMasterBusiness();
            _siteConfigRepository = new SiteConfigRepository();
            common = new Common();
            information = new SendMail();
            _siteConfigBusiness = new SiteConfigBusiness();
        }

        [HttpGet]
        public ActionResult ActionGetBanatiItem(string partNumber)
        {
            return Json(_RmaHeadBusiness.GetItemStockByPartNumber(partNumber), JsonRequestBehavior.AllowGet);
        }

        public ActionResult ActionGetDamagedGoods()
        {
            return Json(_RmaHeadBusiness.GetItemsBloqued(), JsonRequestBehavior.AllowGet);
        }

        public ActionResult ActionGetBanatiDamagedGoods()
        {
            if (Session["User"] != null)
                return Json(new { result = true, model = _RmaHeadBusiness.GetBanatiItemsBloqued() }, JsonRequestBehavior.AllowGet);

            return Json(new { result = false }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult ActionCreateBanati(List<DamagedGoodsBanatiModel> model, string Comments)
        {
            if (Session["User"] != null)
            {
                string document = _RmaHeadBusiness.AddBanati(model, Comments, Session["User"].ToString(), 0);

                if (document != "")
                    return Json(new { result = true, Folio = document }, JsonRequestBehavior.AllowGet);
                else
                    return Json(new { result = true, Folio = "" }, JsonRequestBehavior.AllowGet);
            }
            return Json(new { result = false }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult ActionUpdateBanati(List<DamagedGoodsBanatiModel> model, string Comments, string Document)
        {
            if (Session["User"] != null)
            {
                if (_RmaHeadBusiness.UpdateBanati(model, Document, Comments, Session["User"].ToString(), 0, "RMA003.cshtml"))
                    return Json(new { result = true, Folio = Document }, JsonRequestBehavior.AllowGet);
                else
                    return Json(new { result = true, Folio = "" }, JsonRequestBehavior.AllowGet);
            }
            return Json(new { result = false }, JsonRequestBehavior.AllowGet);
        }

        private void RmaBanatiEmail(string Document)
        {
            string gerente = ConfigurationManager.AppSettings["Gerente"].ToString();
            string emails = _sysRoleMasterBusiness.GetEmailsByRol(gerente);
            var SiteInfo = _siteConfigBusiness.GetSiteCodeAddress();
            var RmaBanati = _RmaHeadBusiness.GetRmaHeadByDocument(Document);
            Task task = new Task(() =>
            {

                RMABanati report = new RMABanati();
                report.DataSource = report.printTable(RmaBanati);
                Stream stream = new MemoryStream();
                report.ExportToPdf(stream);
                information.folio = RmaBanati.DamagedGoodsDoc.ToString();
                information.store = RmaBanati.Supplier;
                information.subject = SiteInfo.SiteName + " - Remision de mercancias - " + Document;
                information.from = SiteInfo.SiteName;
                information.email = emails;
                string status = common.MailMessageRMA(information, stream);
            });
            task.Start();
        }

        [HttpPost]
        public ActionResult ActionProcessBanati(List<DamagedGoodsBanatiModel> model, string Comments, string Document)
        {
            if (Session["User"] != null)
            {
                if (Document != "")
                {
                    if (_RmaHeadBusiness.UpdateBanati(model, Document, Comments, Session["User"].ToString(), 1, "RMA003.cshtml"))
                    {
                        RmaBanatiEmail(Document);
                        return Json(new { result = true, Folio = Document }, JsonRequestBehavior.AllowGet);
                    }
                    else
                        return Json(new { result = true, Folio = "" }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    Document = _RmaHeadBusiness.AddBanati(model, Comments, Session["User"].ToString(), 1);

                    if (Document != "")
                    {
                        RmaBanatiEmail(Document);
                        return Json(new { result = true, Folio = Document }, JsonRequestBehavior.AllowGet);
                    }
                    else
                        return Json(new { result = true, Folio = "" }, JsonRequestBehavior.AllowGet);
                }
            }
            return Json(new { result = false }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult ActionCreateDamagedGoods(int Supplier, List<DamagedsGoodsModel> DamagedsGood)
        {
            if (Session["User"] != null)
            {
                var infoSupplier = _supplierBusiness.GetSupplierDamagedsGood(Supplier);
                var SiteInfo = _siteConfigBusiness.GetSiteCodeAddress();
                var UuserEmail = _userMasterBusiness.GetEmailByUserName(Session["User"].ToString());
                //Se creará petición a Compras (Aprobación)

                if (infoSupplier != null)
                {
                    var DamagedsGoods = _RmaHeadBusiness.AddDamagedGoodsRequest(DamagedsGood, Supplier, Session["User"].ToString());
                    if (DamagedsGoods != null)
                    {
                        var session = Session["User"].ToString();
                        Task task = new Task(() =>
                        {
                            RMAReport report = new RMAReport();
                            report.DataSource = report.printTable(DamagedsGoods);
                            Stream stream = new MemoryStream();
                            report.ExportToPdf(stream);

                            information.folio = DamagedsGoods.DamagedGoodsDoc.ToString();
                            information.store = DamagedsGoods.Supplier;
                            information.subject = SiteInfo.SiteName + " - Aprobacion de Devolucion de Material";
                            information.from = SiteInfo.SiteName;
                            information.email = UuserEmail; //_sysRoleMasterBusiness.GetEmailsByRol(ConfigurationManager.AppSettings["Compras"].ToString());

                            string status = common.MailMessageRMA(information, stream);
                        });
                        task.Start();
                    }
                    return Json(new { value = true, doc = DamagedsGoods.DamagedGoodsDoc, response = "1" }, JsonRequestBehavior.AllowGet);
                }
                else
                {   //Mandar directamente a proveedor
                    var DamagedsGoods = _RmaHeadBusiness.AddDamagedGoods(DamagedsGood, Supplier, Session["User"].ToString());
                    if (DamagedsGoods != null)
                    {
                        // Realiza el paso de la operacion                        
                        //Task task = new Task(() =>
                        //{
                        //    EmailDamagedGoodsModel listaRMA = new EmailDamagedGoodsModel();
                        //    RMAReportSuppliercs report = new RMAReportSuppliercs();
                        //    report.DataSource = report.printTable("", DamagedsGoods);
                        //    var SupplierEmail = _MaSupplierContact.Email(DamagedsGoods.SupplierId, "Ventas");
                        //    var stream = new MemoryStream();
                        //    report.ExportToPdf(stream);
                        //    listaRMA = new EmailDamagedGoodsModel() { Rerpote = stream, RMA = DamagedsGoods.DamagedGoodsDoc };
                        //    information.folio = DamagedsGoods.DamagedGoodsDoc.ToString();
                        //    information.store = SiteInfo.SiteName;
                        //    information.Supplier = DamagedsGoods.Supplier;
                        //    information.subject = SiteInfo.SiteName + " - Aprobación de Devolucion de mercancias - Folio - " + DamagedsGoods.DamagedGoodsDoc;
                        //    information.from = SiteInfo.SiteName;
                        //    information.email = SupplierEmail + "," + UuserEmail;
                        //    string returnEmail = common.MailMessageRemisionConfirmacionSingleRM002(information, listaRMA);
                        //});
                        //task.Start();

                        var number = _RmaHeadBusiness.StoreProcedureDamagedGoods(DamagedsGoods.DamagedGoodsDoc, Session["User"].ToString(), "RMA001.cshtml");
                        if (number.ReturnValue == 0)
                        {
                            var stream = new MemoryStream();

                            RMAReportSuppliercs report = new RMAReportSuppliercs();
                            List<DamagedsGoodsModel> documentItems = _DamagedsGoodsItemBusiness.GetItemsByDocument(DamagedsGoods.DamagedGoodsDoc);
                            DamagedsGoods.DamageGoodsItem = documentItems;
                            report.DataSource = report.printTable(number.Document, DamagedsGoods);
                            EmailDamagedGoodsModel listaRMA = new EmailDamagedGoodsModel();
                            report.ExportToPdf(stream);
                            listaRMA = new EmailDamagedGoodsModel() { Rerpote = stream, RMA = DamagedsGoods.DamagedGoodsDoc };
                            var SupplierEmail = _MaSupplierContact.Email(DamagedsGoods.SupplierId, "Ventas");
                            information.folio = DamagedsGoods.DamagedGoodsDoc;
                            information.store = _siteConfigRepository.GetSiteCodeName();
                            information.Supplier = DamagedsGoods.Supplier;
                            information.subject = SiteInfo.SiteName + " - Devolucion de mercancias - Folio - " + DamagedsGoods.DamagedGoodsDoc;
                            information.from = SiteInfo.SiteName;
                            information.email = SupplierEmail + "," + UuserEmail;
                            common.MailMessageRemisionConfirmacionSingle(information, listaRMA);
                            return Json(new { value = true, doc = DamagedsGoods.DamagedGoodsDoc, response = "2", pdf = Convert.ToBase64String(stream.ToArray()) }, JsonRequestBehavior.AllowGet);
                        }
                        return Json(new { value = false, doc = "Error en el SP, contacte a sistemas.", response = "0" }, JsonRequestBehavior.AllowGet);
                    }
                    return Json(new { value = false, doc = "Error al actualizar, contacte a sistemas.", response = "0" }, JsonRequestBehavior.AllowGet);
                }
            }
            return Json("SF", JsonRequestBehavior.AllowGet);
        }

        public ActionResult ActionDamagedGoodsApproved(int Supplier)
        {
            if (Session["User"] != null)
            {
                var DamagedsGoods = _RmaHeadBusiness.DamagedGoodsApproved(Supplier, "RMA");
                return Json(DamagedsGoods, JsonRequestBehavior.AllowGet);
            }
            return Json("SF", JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult ActionDamagedGoodsApprovedEmailSingle(List<string> RMA)
        {
            if (Session["User"] != null)
            {
                int status = 2;
                int problem = 0;

                var SiteInfo = _siteConfigBusiness.GetSiteCodeAddress();
                var User = Session["User"].ToString();
                var UuserEmail = _userMasterBusiness.GetEmailByUserName(Session["User"].ToString());
                List<StoreProcedureResult> RMABool = new List<StoreProcedureResult>();
                List<DamagedGoodsHeadModel> Damaged = new List<DamagedGoodsHeadModel>();
                foreach (var item in RMA)
                {
                    var DamagedsGoods = _RmaHeadBusiness.DamagedGoodsApprovedPrint(item, status);
                    DamagedsGoods.ProcessStatus = 9;
                    Damaged.Add(DamagedsGoods);
                    if (DamagedsGoods != null)
                    {
                        var number = _RmaHeadBusiness.StoreProcedureDamagedGoods(item, User, "RMA004.cshtml");
                        RMABool.Add(number);
                    }
                }
                var stream = new MemoryStream();
                int count = 0;
                int countDamaged = 0;
                foreach (var item in RMA)
                {
                    var DamagedsGoods = Damaged[countDamaged];
                    countDamaged++;
                    if (DamagedsGoods != null)
                    {
                        var number = RMABool[count];
                        RMAReportSuppliercs report = new RMAReportSuppliercs();
                        DamagedsGoods.processDate = DateTime.Now;
                        report.DataSource = report.printTable(number.Document, DamagedsGoods);
                        problem = number.ReturnValue;
                        if (number.ReturnValue == 0)
                        {
                            report.ExportToPdf(stream);
                            var listaRMA = new EmailDamagedGoodsModel() { Rerpote = stream, RMA = item };
                            var SupplierEmail = _MaSupplierContact.Email(DamagedsGoods.SupplierId, "Ventas");
                            string SupplierN = DamagedsGoods.Supplier;
                            information.folio = item; // folio
                            information.store = _siteConfigRepository.GetSiteCodeName();
                            information.Supplier = SupplierN;
                            information.subject = SiteInfo.SiteName + " - Devolucion de mercancias - Folio - " + DamagedsGoods.DamagedGoodsDoc;
                            information.from = SiteInfo.SiteName;
                            information.email = SupplierEmail + "," + UuserEmail;
                            common.MailMessageRemisionConfirmacionSingle(information, listaRMA);
                        }
                        count++;
                    }
                }
                return Json(new { value = true, error = problem, response = Convert.ToBase64String(stream.ToArray()) }, JsonRequestBehavior.AllowGet);
            }
            return Json("SF", JsonRequestBehavior.AllowGet);
        }

        public ActionResult ActionGetFolios(string supplierId, string SiteCode)
        {
            if (Session["User"] != null)
            {
                var foliosList = _RmaHeadBusiness.GetFoliosBySupplierAndFechaRange(supplierId, SiteCode);
                if (foliosList.Count != 0)
                    return Json(new { success = true, Json = foliosList }, JsonRequestBehavior.AllowGet);
                else
                    return Json(new { success = false }, JsonRequestBehavior.AllowGet);
            }
            return Json("SF", JsonRequestBehavior.AllowGet);
        }

        public ActionResult ActionGetRMAFullTable(string Type, DateTime? DateInit, DateTime? DateFinal, int Status)
        {
            if (Session["User"] != null)
                return new JsonResult() { Data = _RmaHeadBusiness.GetAllRma(Type, DateInit, DateFinal, Status), JsonRequestBehavior = JsonRequestBehavior.AllowGet, MaxJsonLength = Int32.MaxValue };

            return Json("SF", JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult ActionUpdateRmaAprovalCancel(List<DamagedsGoodsModel> Model, string Folio, bool Flag, string Comment, string type, string program, int SupplierId, Decimal? Weight)
        {
            if (Session["User"] != null)
            {
                var SiteInfo = _siteConfigBusiness.GetSiteCodeAddress();
                var Data = _RmaHeadBusiness.UpdateRmaAprovalCancel(Model, Folio, Flag, Comment, Session["User"].ToString(), type, program, Weight);
                if (Data)
                {
                    MemoryStream stream = new MemoryStream();
                    if (Flag)
                    {
                        var number = _RmaHeadBusiness.StoreProcedureDamagedGoods(Folio, Session["User"].ToString(), "RMA005.cshtml");

                        var DamagedsGoods = _RmaHeadBusiness.DamagedGoodsApprovedPrint(SupplierId, Folio, 9);
                        List<EmailDamagedGoodsModel> listaRemision = new List<EmailDamagedGoodsModel>();
                        List<DamagedsGoodsModel> documentItems = _DamagedsGoodsItemBusiness.GetItemsByDocument(DamagedsGoods.DamagedGoodsDoc);
                        DamagedsGoods.DamageGoodsItem = documentItems;
                        DamagedsGoods.Weight = Weight;
                        ApprovalBanati report = new ApprovalBanati();
                        report.DataSource = report.printTable(number.Document, DamagedsGoods);

                        report.ExportToPdf(stream);
                        listaRemision.Add(new EmailDamagedGoodsModel() { Rerpote = stream, RMA = number.Document });
                    }

                    Task task = new Task(() =>
                    {
                        int status = 0;
                        if (Flag)
                            status = 9;
                        else
                            status = 3;

                        if (status == 9)
                        {
                            List<EmailDamagedGoodsModel> listaRemision = new List<EmailDamagedGoodsModel>();
                            var DamagedsGoods = _RmaHeadBusiness.DamagedGoodsApprovedPrint(SupplierId, Folio, status);
                            RMAReportSuppliercs report = new RMAReportSuppliercs();
                            report.DataSource = report.printTable(null, DamagedsGoods);
                            report.ExportToPdf(stream);
                            listaRemision.Add(new EmailDamagedGoodsModel() { Rerpote = stream, RMA = Folio });
                            var Usuario = _userMasterBusiness.GetEmailByUserName(DamagedsGoods.cuser);
                            var Proveedor = _MaSupplierContact.Email(SupplierId, "Ventas");
                            var Email = Usuario
                            + (!string.IsNullOrWhiteSpace(Usuario) && !string.IsNullOrWhiteSpace(Proveedor) ? ", " + Proveedor : Proveedor);
                            information.folio = DamagedsGoods.DamagedGoodsDoc.ToString();
                            information.Supplier = DamagedsGoods.Supplier;
                            information.store = _siteConfigRepository.GetSiteCodeName();
                            information.subject = SiteInfo.SiteName + " - Remisión de Mercancia - " + DamagedsGoods.DamagedGoodsDoc;
                            information.from = SiteInfo.SiteName;
                            information.email = Email;
                            string returnEmail = common.MailMessageRMA005(information, listaRemision);
                        }
                        else
                        {
                            List<EmailDamagedGoodsModel> listaRemision = new List<EmailDamagedGoodsModel>();
                            var DamagedsGoods = _RmaHeadBusiness.DamagedGoodsApprovedPrint(SupplierId, Folio, status);
                            RMAReportSuppliercs report = new RMAReportSuppliercs();
                            report.DataSource = report.printTable(null, DamagedsGoods);
                            report.ExportToPdf(stream);
                            listaRemision.Add(new EmailDamagedGoodsModel() { Rerpote = stream, RMA = Folio });
                            var Usuario = _userMasterBusiness.GetEmailByUserName(DamagedsGoods.cuser);
                            information.folio = DamagedsGoods.DamagedGoodsDoc.ToString();
                            information.store = _siteConfigRepository.GetSiteCodeName();
                            information.subject = SiteInfo.SiteName + " - Remisión de Mercancia - " + DamagedsGoods.DamagedGoodsDoc;
                            information.from = SiteInfo.SiteName;
                            information.email = Usuario;
                            string returnEmail = common.MailMessageRMA005Cancel(information, listaRemision);
                        }
                    });
                    task.Start();
                    return Json(new { Data = true, response = Convert.ToBase64String(stream.ToArray()) }, JsonRequestBehavior.AllowGet);
                }
                else
                    return Json(new { Data = false }, JsonRequestBehavior.AllowGet);
            }
            return Json(new { Data = "SF" }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult ActionGetItemSupplierByRma(string Folio, string Type, int Status)
        {
            if (Session["User"] != null)
                return new JsonResult() { Data = _DamagedsGoodsItemBusiness.GetRMAItemsByFolio(Folio, Type, Status), JsonRequestBehavior = JsonRequestBehavior.AllowGet, MaxJsonLength = Int32.MaxValue };

            return Json("SF", JsonRequestBehavior.AllowGet);
        }


        public ActionResult ActionGetItemSupplierByRmaLastCost(string Folio, string Type, int Status)
        {
            if (Session["User"] != null)
                return new JsonResult() { Data = _DamagedsGoodsItemBusiness.GetRMAItemsByFolioLastCost(Folio, Type, Status), JsonRequestBehavior = JsonRequestBehavior.AllowGet, MaxJsonLength = Int32.MaxValue };

            return Json("SF", JsonRequestBehavior.AllowGet);
        }

        public ActionResult ActionGetRmaItems(string Folio, string Type, int Status)
        {
            if (Session["User"] != null)
                return Json(new { result = true, model = _DamagedsGoodsItemBusiness.GetRMAItemsByDocument(Folio, Type, Status), JsonRequestBehavior.AllowGet });

            return Json(new { result = "SF", JsonRequestBehavior.AllowGet });
        }

        public ActionResult ActionGetPrintRMAWithControl(string referenceNumber, string referenceType)
        {
            if (Session["User"] != null)
            {
                MemoryStream stream = new MemoryStream();
                if (referenceType == "REMISION")
                {
                    var DamagedsGoods = _RmaHeadBusiness.DamagedGoodsPrint(referenceNumber);
                    List<DamagedsGoodsModel> documentItems = _DamagedsGoodsItemBusiness.GetItemsByDocumentReport(DamagedsGoods.DamagedGoodsDoc, DamagedsGoods.ProcessStatus);
                    ApprovalBanati report = new ApprovalBanati();
                    DamagedsGoods.DamageGoodsItem = documentItems;
                    if (DamagedsGoods.Print_Document != 0)
                    {
                        report.Watermark.Text = "RE-IMPRESION";
                        report.Watermark.TextDirection = DevExpress.XtraPrinting.Drawing.DirectionMode.ForwardDiagonal;
                        report.Watermark.Font = new Font(report.Watermark.Font.FontFamily, 40);
                        report.Watermark.ForeColor = Color.DodgerBlue;
                        report.Watermark.TextTransparency = 150;
                        report.Watermark.ShowBehind = false;
                        report.Watermark.PageRange = "1-99";
                        report.DataSource = report.printTable(DamagedsGoods.document_no, DamagedsGoods);
                        var print = _RmaHeadBusiness.DamagedsGoodsEditStatusPrint(referenceNumber, DamagedsGoods.Print_Document, "RMA009.CSHTML");
                    }
                    else
                    {
                        report.DataSource = report.printTable(DamagedsGoods.document_no, DamagedsGoods);
                        var print = _RmaHeadBusiness.DamagedsGoodsEditStatusPrint(referenceNumber, DamagedsGoods.Print_Document, "RMA009.CSHTML");
                    }

                    report.ExportToPdf(stream);
                }
                else if (referenceType == "RMA")
                {
                    var DamagedsGoods = _RmaHeadBusiness.GetRMAHeaderDetail(referenceNumber);
                    RMAReportSuppliercs RMAReport = new RMAReportSuppliercs();
                    if(DamagedsGoods[0].ProcessStatus == 3)
                    {
                        DamagedsGoods[0].processDate = null;
                        RMAReport.Watermark.Text = "RECHAZADO";
                        RMAReport.Watermark.TextDirection = DevExpress.XtraPrinting.Drawing.DirectionMode.ForwardDiagonal;
                        RMAReport.Watermark.Font = new Font(RMAReport.Watermark.Font.FontFamily, 60);
                        RMAReport.Watermark.ForeColor = Color.Firebrick;
                        RMAReport.Watermark.TextTransparency = 150;
                        RMAReport.Watermark.ShowBehind = false;
                        RMAReport.Watermark.PageRange = "1-99";
                        RMAReport.DataSource = RMAReport.printTable(DamagedsGoods[0].document_no, DamagedsGoods[0]);
                        var print = _RmaHeadBusiness.DamagedsGoodsEditStatusPrint(referenceNumber, DamagedsGoods[0].Print_Document, "RMA0010.CSHTML");
                    }
                    else if(DamagedsGoods[0].ProcessStatus == 8)
                    {
                        DamagedsGoods[0].processDate = null;
                        RMAReport.Watermark.Text = "CANCELADO";
                        RMAReport.Watermark.TextDirection = DevExpress.XtraPrinting.Drawing.DirectionMode.ForwardDiagonal;
                        RMAReport.Watermark.Font = new Font(RMAReport.Watermark.Font.FontFamily, 60);
                        RMAReport.Watermark.ForeColor = Color.Firebrick;
                        RMAReport.Watermark.TextTransparency = 150;
                        RMAReport.Watermark.ShowBehind = false;
                        RMAReport.Watermark.PageRange = "1-99";
                        RMAReport.DataSource = RMAReport.printTable(DamagedsGoods[0].document_no, DamagedsGoods[0]);
                        var print = _RmaHeadBusiness.DamagedsGoodsEditStatusPrint(referenceNumber, DamagedsGoods[0].Print_Document, "RMA0010.CSHTML");
                    }
                    else if (DamagedsGoods[0].ProcessStatus == 9)
                    {
                        if (DamagedsGoods[0].Print_Document != 0)
                        {
                            RMAReport.Watermark.Text = "RE-IMPRESION";
                            RMAReport.Watermark.TextDirection = DevExpress.XtraPrinting.Drawing.DirectionMode.ForwardDiagonal;
                            RMAReport.Watermark.Font = new Font(RMAReport.Watermark.Font.FontFamily, 40);
                            RMAReport.Watermark.ForeColor = Color.DodgerBlue;
                            RMAReport.Watermark.TextTransparency = 150;
                            RMAReport.Watermark.ShowBehind = false;
                            RMAReport.Watermark.PageRange = "1-99";
                        }
                        RMAReport.DataSource = RMAReport.printTable(DamagedsGoods[0].document_no, DamagedsGoods[0]);
                        var print = _RmaHeadBusiness.DamagedsGoodsEditStatusPrint(referenceNumber, DamagedsGoods[0].Print_Document, "RMA0010.CSHTML");
                    }
                    else
                    {
                        DamagedsGoods[0].processDate = null;
                        RMAReport.DataSource = RMAReport.printTable(DamagedsGoods[0].document_no, DamagedsGoods[0]);
                        var print = _RmaHeadBusiness.DamagedsGoodsEditStatusPrint(referenceNumber, DamagedsGoods[0].Print_Document, "RMA0010.CSHTML");
                    }
                    RMAReport.ExportToPdf(stream);
                }
                return Json(new { Data = true, response = Convert.ToBase64String(stream.ToArray()) }, JsonRequestBehavior.AllowGet);
            }
            return Json("SF", JsonRequestBehavior.AllowGet);
        }

        public ActionResult ActionGetAllItemSupplierByRma(string Folio, string Type, int status)
        {
            if (Session["User"] != null)
                return new JsonResult() { Data = _DamagedsGoodsItemBusiness.GetAllRMAItemsByFolio(Folio, Type, status), JsonRequestBehavior = JsonRequestBehavior.AllowGet, MaxJsonLength = Int32.MaxValue };

            return Json("SF", JsonRequestBehavior.AllowGet);
        }

        public ActionResult ActionGetFoliosByRemision(string supplierId)
        {
            var foliosList = _RmaHeadBusiness.GetFoliosByRemisionAprovalByDates(supplierId);
            if (foliosList.Count != 0)
                return Json(new { success = true, Json = foliosList }, JsonRequestBehavior.AllowGet);
            else
                return Json(new { success = false }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult ActionGetDocuments(string dateInicial, string dateFinal)
        {
            if (Session["User"] != null)
            {
                var headers = _RmaHeadBusiness.GetDocumentsByDates(dateInicial, dateFinal);
                if (headers.Count > 0)
                    return Json(new { success = true, Orders = headers }, JsonRequestBehavior.AllowGet);
                else
                    return Json(new { success = false, responseText = "Sin Documentos en ese rango de fecha." }, JsonRequestBehavior.AllowGet);
            }
            else
                return Json(new { success = false, responseText = "Termino tu sesión." }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult ActionGetDocumentsReport(DateTime? dateInicial, DateTime? dateFinal, int status)
        {
            if (Session["User"] != null)
            {
                var headers = _RmaHeadBusiness.GetItemsByDocumentReport(dateInicial, dateFinal, status);
                if (headers.Count > 0)
                    return Json(new { success = true, Orders = headers }, JsonRequestBehavior.AllowGet);
                else
                    return Json(new { success = false, responseText = "Sin Documentos en ese rango de fecha o estatus." }, JsonRequestBehavior.AllowGet);
            }
            else
                return Json(new { success = false, responseText = "Termino tu sesión." }, JsonRequestBehavior.AllowGet);
        }
        [HttpGet]
        public ActionResult ActionGetDocumentsScrapReport(DateTime? dateInicial, DateTime? dateFinal, int status)
        {
            if (Session["User"] != null)
            {
                var headers = _RmaHeadBusiness.GetItemsByDocumentApproval(dateInicial, dateFinal, status);
                if (headers.Count > 0)
                    return Json(new { success = true, Orders = headers }, JsonRequestBehavior.AllowGet);
                else
                    return Json(new { success = false, responseText = "Sin Documentos en ese rango de fecha o estatus." }, JsonRequestBehavior.AllowGet);
            }
            else
                return Json(new { success = false, responseText = "Termino tu sesión." }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult ActionGetDocumentsInStatus2(string dateInicial, string dateFinal)
        {
            if (Session["User"] != null)
            {
                var foliosList = _RmaHeadBusiness.GetDocumentsByDatesInStatus2(dateInicial, dateFinal);
                if (foliosList.Count != 0)
                    return Json(new { success = true, Json = foliosList }, JsonRequestBehavior.AllowGet);
                else
                    return Json(new { success = false, responseText = "Sin Documentos en ese rango de fecha." }, JsonRequestBehavior.AllowGet);
            }
            else
                return Json(new { success = false, responseText = "Termino tu sesión." }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult ActionGetItemsDocument(string document, int status)
        {
            var foliosList = _damagedsGoodsItemBusiness.GetItemsByDocument(document, status);
            if (foliosList.Count != 0)
                return Json(new { success = true, Json = foliosList }, JsonRequestBehavior.AllowGet);
            else
                return Json(new { success = false }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult ActionGetItemsDocumentSingle(string document, int status)
        {
            var foliosList = _damagedsGoodsItemBusiness.GetItemsByDocumentSingle(document, status);
            if (foliosList.Count != 0)
                return Json(new { success = true, Json = foliosList }, JsonRequestBehavior.AllowGet);
            else
                return Json(new { success = false }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult ActionGetItemsByDocumentReport(string document, int Status)
        {
            var foliosList = _damagedsGoodsItemBusiness.GetItemsByDocumentReport(document, Status);
            if (foliosList.Count != 0)
                return Json(new { success = true, Json = foliosList }, JsonRequestBehavior.AllowGet);
            else
                return Json(new { success = false }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult ActionAprovalRemision(string Folio, Decimal? Weight, int Provider)
        {
            if (Session["User"] != null)
            {
                var SiteInfo = _siteConfigBusiness.GetSiteCodeAddress();
                var DamagedsGoods = _RmaHeadBusiness.DamagedGoodsApprovedPrint(Provider, Folio, 2);
                var number = _RmaHeadBusiness.StoreProcedureDamagedGoods(Folio, Session["User"].ToString(), "RMA006.cshtml");
                var Data = _RmaHeadBusiness.UpdateDamegeGoodsConfirmacionRemision(Folio, Weight, Session["User"].ToString(), "RMA006.cshtml");

                if (Data)
                {
                    List<EmailDamagedGoodsModel> listaRemision = new List<EmailDamagedGoodsModel>();
                    List<DamagedsGoodsModel> documentItems = _DamagedsGoodsItemBusiness.GetItemsByDocument(DamagedsGoods.DamagedGoodsDoc);
                    DamagedsGoods.DamageGoodsItem = documentItems;
                    DamagedsGoods.Weight = Weight;
                    RMAReportSuppliercs report = new RMAReportSuppliercs();
                    report.DataSource = report.printTable(number.Document, DamagedsGoods);

                    var stream = new MemoryStream();
                    report.ExportToPdf(stream);
                    listaRemision.Add(new EmailDamagedGoodsModel() { Rerpote = stream, RMA = number.Document });
                    var UuserEmail = _userMasterBusiness.GetEmailByUserName(Session["User"].ToString());
                    var Proveedor = _MaSupplierContact.Email(Provider, "Ventas");
                    var Gerencia = _sysRoleMasterBusiness.GetEmailsByRol(ConfigurationManager.AppSettings["Gerente"].ToString());
                    string Email = Proveedor + (!string.IsNullOrWhiteSpace(Gerencia) && !string.IsNullOrWhiteSpace(Proveedor) ? ", " + Gerencia : Gerencia);
                    information.Supplier = _supplierBusiness.GetNameSupplier(Provider);
                    information.store = _siteConfigRepository.GetSiteCodeName();
                    information.subject = SiteInfo.SiteName + " - Aplicación de Remisión - Folio - " + DamagedsGoods.DamagedGoodsDoc;
                    information.from = SiteInfo.SiteName;
                    information.folio = number.Document;
                    information.email = Email + "," + UuserEmail;
                    common.MailMessageRMASupplier(information, listaRemision);
                    return Json(new { value = true, response = Convert.ToBase64String(stream.ToArray()) }, JsonRequestBehavior.AllowGet);
                }
                return Json(new { value = false }, JsonRequestBehavior.AllowGet);
            }
            return Json("SF", JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult ActionAprobacionRechazoDamageGoods(List<DamagedsGoodsModel> Model, string msj, string document)
        {
            if (Session["User"] != null)
            {
                var SiteInfo = _siteConfigRepository.GetSiteCodeAddress();
                string uuser = Session["User"].ToString();
                List<DamagedsGoodsModel> documentItems = _damagedsGoodsItemBusiness.NewGetItemsByDocument(document);
                var respuesta = _RmaHeadBusiness.UpdateDamageGoodsStatus(msj, document, uuser);
                if (respuesta == 1)
                {
                    _RmaHeadBusiness.UpdateDamageGoodsItemStatusSingle(Model, document, msj, uuser);
                    var Items = _damagedsGoodsItemBusiness.GetDocumentGoods(document);

                    if (msj == "RECHAZADO")
                    {
                        var returnBlocked = _RmaHeadBusiness.MoveBlockToUnrestrictedModelDG(documentItems, "SSM001BtoU", uuser);
                    }

                    if (msj == "APROBADO")
                    {
                        var respuesta2 = _RmaHeadBusiness.StoreProcedureDamagedGoods(document, uuser, "SSM001.cshtml");
                        //Luis dijo que los mismos gerentes usan la misma cuenta asi que no hay problema porque ponga esto :C
                        var Recibo = _sysRoleMasterBusiness.GetEmailsByRol(ConfigurationManager.AppSettings["Recibo"].ToString());
                        var Gerencia = _sysRoleMasterBusiness.GetEmailsByRol(ConfigurationManager.AppSettings["Gerente"].ToString());
                        if (respuesta2.ReturnValue == 0)
                        {
                            Task task = new Task(() =>
                            {
                                if (msj == "APROBADO")
                                {
                                    ConfirmacionSalidaMerma reporte = new ConfirmacionSalidaMerma();
                                    reporte.DataSource = reporte.printTable(documentItems, document, Items.SiteName, Items.Fecha);
                                    Stream stream = new MemoryStream();
                                    reporte.ExportToPdf(stream);
                                    var Email = Gerencia + "," + Recibo;
                                    information.folio = document;
                                    information.subject = SiteInfo.SiteName + " - Aplicacion de Salida por Merma";
                                    information.from = SiteInfo.SiteName;
                                    information.email = Email;
                                    common.MailMessageSalidaMermaAplicada(information, stream);
                                }
                                else
                                {
                                    information.folio = document;
                                    information.subject = SiteInfo.SiteName + " - Rechazo de Salida por Merma";
                                    information.from = SiteInfo.SiteName;
                                    information.email = Recibo;
                                    common.MailMessageSalidaMermaRechazada(information);
                                }
                            });
                            task.Start();
                            return Json(new { success = true, responseText = "" }, JsonRequestBehavior.AllowGet);
                        }
                    }
                    return Json(new { success = true, responseText = "" }, JsonRequestBehavior.AllowGet);
                }
                else if (respuesta == 0)
                    return Json(new { success = false, responseText = "No fue posible Rechazar la solicitud." }, JsonRequestBehavior.AllowGet);
                else
                    return Json(new { success = false, responseText = "Documento No Encontrado." }, JsonRequestBehavior.AllowGet);
            }
            else
                return Json(new { success = false, responseText = "Termino tu sesión." }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult ActionUpdateRmaApprobal(List<DamagedsGoodsModel> Model, string Folio, int Status, string Comment, string program, int SupplierId)
        {
            if (Session["User"] != null)
            {
                var session = Session["User"].ToString();
                var SiteInfo = _siteConfigRepository.GetSiteCodeAddress();
                var UuserEmail = _userMasterBusiness.GetEmailByUserName(session);
                var infoSupplier = _supplierBusiness.GetSupplierDamagedsGood(SupplierId);
                if (infoSupplier != null)
                    Status = 2;
                var Data = _RmaHeadBusiness.UpdateRmaRemisionInitial(Model, Folio, Status, Comment, session, program);

                if (Data)
                {
                    var DamagedsGoods = _RmaHeadBusiness.DamagedGoodsPrint(Folio);
                    List<DamagedsGoodsModel> documentItems = _DamagedsGoodsItemBusiness.GetItemsByDocumentReport(DamagedsGoods.DamagedGoodsDoc, DamagedsGoods.ProcessStatus);

                    //Se creará petición a Compras (Aprobación)
                    if (infoSupplier != null)
                    {
                        Task task = new Task(() =>
                        {
                            ApprovalBanati report = new ApprovalBanati();
                            if (DamagedsGoods != null)
                            {
                                RMAReport reportRMA = new RMAReport();
                                reportRMA.DataSource = reportRMA.printTable(DamagedsGoods);
                                Stream stream = new MemoryStream();
                                report.ExportToPdf(stream);
                                information.folio = DamagedsGoods.DamagedGoodsDoc.ToString();
                                information.store = DamagedsGoods.Supplier;
                                information.subject = SiteInfo.SiteName + " - Aprobacion de Devolucion de Material";
                                information.from = SiteInfo.SiteName;
                                information.email = UuserEmail;

                                string status = common.MailMessageRMA(information, stream);
                            }
                        });
                        task.Start();
                        return Json(new { Data = true, Model = _RmaHeadBusiness.GetAllRmaHeadStatus("RMA", DateTime.Now.AddDays(-7), DateTime.Now, 0), Status = 0 }, JsonRequestBehavior.AllowGet);
                    }
                    else
                    {   
                        return Json(new { Data = true, Model = _RmaHeadBusiness.GetAllRmaHeadStatus("RMA", DateTime.Now.AddDays(-7), DateTime.Now, 0), Status = 1 }, JsonRequestBehavior.AllowGet);
                    }

                }
                return Json(new { Data = false }, JsonRequestBehavior.AllowGet);
            }
            return Json(new { Data = "SF" }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult ActionUpdateRmaRemisionAprovalReject(List<DamagedsGoodsModel> Model, string Folio, int Status, string Comment, string type, string program, int SupplierId)
        {
            if (Session["User"] != null)
            {
                var Email = "";
                int problem = 0;
                int count = 0;
                var SupplierN = "";
                var Data = _RmaHeadBusiness.UpdateRmaRemisionAprovalReject(Model, Folio, Status, Comment, Session["User"].ToString(), type, program);
                var SiteInfo = _siteConfigBusiness.GetSiteCodeAddress();
                if (Data)
                {
                    var MesaControl = _sysRoleMasterBusiness.GetEmailsByRol(ConfigurationManager.AppSettings["MesaControl"].ToString());
                    Task task = new Task(() =>
                    {
                        EmailDamagedGoodsModel listaRMA = new EmailDamagedGoodsModel();
                        var DamagedsGoods = _RmaHeadBusiness.DamagedGoodsApprovedPrint(SupplierId, Folio, Status);
                        if (DamagedsGoods != null)
                        {
                            if (Status == 2)
                            {
                                if (type == "RMA")
                                {
                                    RMAReportSuppliercs report = new RMAReportSuppliercs();
                                    report.DataSource = report.printTable("", DamagedsGoods);
                                    
                                    var SupplierEmail = _MaSupplierContact.Email(DamagedsGoods.SupplierId, "Ventas");

                                    var stream = new MemoryStream();
                                    report.ExportToPdf(stream);
                                    listaRMA = new EmailDamagedGoodsModel() { Rerpote = stream, RMA = DamagedsGoods.DamagedGoodsDoc };

                                    information.folio = DamagedsGoods.DamagedGoodsDoc.ToString();
                                    information.store = _siteConfigRepository.GetSiteCodeName();
                                    information.Supplier = DamagedsGoods.Supplier;
                                    information.subject = SiteInfo.SiteName + " - Devolucion de mercancias - Folio - " + DamagedsGoods.DamagedGoodsDoc;
                                    information.from = SiteInfo.SiteName;
                                    information.email = SupplierEmail + "," + Email + "," + MesaControl;

                                    string returnEmail = common.MailMessageRemisionConfirmacionSingleRM002(information, listaRMA);
                                }
                                else
                                {
                                    var number = _RmaHeadBusiness.StoreProcedureDamagedGoods(DamagedsGoods.DamagedGoodsDoc, Session["User"].ToString(), "RMA004.cshtml");
                                    RMAReportSuppliercs report = new RMAReportSuppliercs();
                                    report.DataSource = report.printTable(number.Document, DamagedsGoods);
                                    problem = number.ReturnValue;
                                    if (number.ReturnValue == 0)
                                    {
                                        var stream = new MemoryStream();
                                        report.ExportToPdf(stream);
                                        listaRMA = new EmailDamagedGoodsModel() { Rerpote = stream, RMA = DamagedsGoods.DamagedGoodsDoc };
                                        var SupplierEmail = _MaSupplierContact.Email(DamagedsGoods.SupplierId, "Ventas");
                                        Email = SupplierEmail + "," + MesaControl;
                                        SupplierN = DamagedsGoods.Supplier;
                                        information.folio = Folio;
                                        information.Supplier = SupplierN;
                                        information.store = _siteConfigRepository.GetSiteCodeName();
                                        information.subject = SiteInfo.SiteName + " - Devolucion de mercancias";
                                        information.from = SiteInfo.SiteName;
                                        information.email = Email;
                                        common.MailMessageRemisionConfirmacionSingle(information, listaRMA);
                                        if (SupplierEmail != null && SupplierEmail != "N/A")
                                            count++;
                                    }
                                }
                            }
                            else if (Status == 3)
                            {
                                if (type == "RMA")
                                {
                                    RMAReport report = new RMAReport();
                                    report.DataSource = report.printTable(DamagedsGoods);
                                    Email = _userMasterBusiness.GetEmailByUserName(DamagedsGoods.cuser);

                                    var stream = new MemoryStream();
                                    report.ExportToPdf(stream);
                                    listaRMA = new EmailDamagedGoodsModel() { Rerpote = stream, RMA = DamagedsGoods.DamagedGoodsDoc };

                                    information.folio = DamagedsGoods.DamagedGoodsDoc.ToString();
                                    information.subject = SiteInfo.SiteName + " - Devolucion de mercancias cancelada";
                                    information.from = SiteInfo.SiteName;
                                    information.email = Email + "," + MesaControl;

                                    string returnEmail = common.MailMessageRemisionCancelacionSingleRM002(information, listaRMA);
                                }
                            }
                        }
                    });
                    task.Start();
                    return Json(new { Data = true, Model = _RmaHeadBusiness.GetAllRmaHeadStatus("RMA", DateTime.Now.AddDays(-7), DateTime.Now, 1) }, JsonRequestBehavior.AllowGet);
                }
                return Json(new { Data = false }, JsonRequestBehavior.AllowGet);
            }
            return Json(new { Data = "SF" }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult ActionRmaAbortCancel(string Folio, string Comment)
        {
            if (Session["User"] != null)
            {
                if(_RmaHeadBusiness.UpdateRmaRemisionCancel(Folio, Comment, Session["User"].ToString(), "RMA004.CSTHML"))
                    return Json(new { success = true, session = "" }, JsonRequestBehavior.AllowGet);
                return Json(new { success = false, session = "" }, JsonRequestBehavior.AllowGet);
            }
            return Json(new { success = false, session = "SF" }, JsonRequestBehavior.AllowGet);
        }
    

        public ActionResult ActionRmaCancelRequest(string Folio, string Comment, int SupplierId)
        {
            if (Session["User"] != null)
            {
                if (_RmaHeadBusiness.RmaCancelRequest(Folio, Comment, Session["User"].ToString()))
                {
                    var SiteInfo = _siteConfigBusiness.GetSiteCodeAddress();
                    var Compras = _sysRoleMasterBusiness.GetEmailsByRol(ConfigurationManager.AppSettings["Compras"].ToString());
                    var AuxCompras = _sysRoleMasterBusiness.GetEmailsByRol(ConfigurationManager.AppSettings["AuxCompras"].ToString());
                    var Gerencia = _sysRoleMasterBusiness.GetEmailsByRol(ConfigurationManager.AppSettings["Gerente"].ToString());
                    var Proveedor = _MaSupplierContact.Email(SupplierId, "Ventas");
                    var DamagedsGoods = _RmaHeadBusiness.DamagedGoodsApprovedPrint(SupplierId, Folio, 4);

                    //Colocar el correo correspondiente a compras dependiendo la division
                    if (DamagedsGoods != null)
                    {
                        Task task = new Task(() =>
                        {
                            RMACancelRequest report = new RMACancelRequest();
                            report.DataSource = report.printTable(DamagedsGoods);

                            Stream stream = new MemoryStream();
                            report.ExportToPdf(stream);
                            var Email = Compras
                            + (!string.IsNullOrWhiteSpace(Compras) && !string.IsNullOrWhiteSpace(Gerencia) ? ", " + Gerencia : Gerencia)
                            + (!string.IsNullOrWhiteSpace(Gerencia) && !string.IsNullOrWhiteSpace(Proveedor) ? ", " + Proveedor : Proveedor)
                            + (!string.IsNullOrWhiteSpace(Proveedor) && !string.IsNullOrWhiteSpace(AuxCompras) ? ", " + AuxCompras : AuxCompras);

                            information.folio = DamagedsGoods.DamagedGoodsDoc.ToString();
                            information.Supplier = DamagedsGoods.Supplier;
                            information.store = _siteConfigRepository.GetSiteCodeName();
                            information.subject = SiteInfo.SiteName + " - Solicitud de Cancelación de Devolucion";
                            information.from = SiteInfo.SiteName;
                            information.email = Email;

                            common.MailMessageRMACancelRequestRMA007(information, stream);
                        });
                        task.Start();
                    }
                    return Json(true, JsonRequestBehavior.AllowGet);
                }
            }
            return Json(false, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult ActionRmaRejectCancelRequest(string Folio, string Comment)
        {
            if (Session["User"] != null)
            {
                var Damageds = _RmaHeadBusiness.RmaRejectCancelRequest(Folio, Comment, Session["User"].ToString());
                if (Damageds > 0)
                {
                    var SiteInfo = _siteConfigBusiness.GetSiteCodeAddress();
                    var DamagedsGoods = _RmaHeadBusiness.DamagedGoodsApprovedPrint(Damageds, Folio, 5);
                    var Email = _userMasterBusiness.GetEmailByUserName(DamagedsGoods.cuser);
                    Task task = new Task(() =>
                    {
                        RMAApproveCancelRequest report = new RMAApproveCancelRequest();
                        report.DataSource = report.printTable(DamagedsGoods);

                        Stream stream = new MemoryStream();
                        report.ExportToPdf(stream);

                        information.folio = DamagedsGoods.DamagedGoodsDoc;
                        information.Supplier = DamagedsGoods.Supplier;
                        information.store = _siteConfigRepository.GetSiteCodeName();
                        information.subject = SiteInfo.SiteName + " - Rechazo de la Cancelación de Devolución";
                        information.from = SiteInfo.SiteName;
                        information.email = Email;

                        common.MailMessageRMARejectionRequestRMA008(information, stream);
                    });
                    task.Start();
                    return Json(true, JsonRequestBehavior.AllowGet);
                }
                else
                    return Json(true, JsonRequestBehavior.AllowGet);
            }
            return Json(false, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult ActionRmaApproveCancelRequest(string Folio, string Comment)
        {
            if (Session["User"] != null)
            {
                if (_RmaHeadBusiness.RmaApproveCancelRequest(Folio, Comment, Session["User"].ToString()))
                {
                    var SiteInfo = _siteConfigBusiness.GetSiteCodeAddress();
                    int? SupplierId = _RmaHeadBusiness.GetAll().Where(x => x.damaged_goods_doc == Folio).FirstOrDefault().supplier_id;
                    var DamagedsGoods = _RmaHeadBusiness.DamagedGoodsApprovedPrint(Convert.ToInt32(SupplierId), Folio, 8);
                    if (DamagedsGoods != null)
                    {
                        Task task = new Task(() =>
                        {
                            RMAApproveCancelRequest report = new RMAApproveCancelRequest();
                            report.DataSource = report.printTable(DamagedsGoods);

                            Stream stream = new MemoryStream();
                            report.ExportToPdf(stream);
                            var EmailsBuyer = _userMasterBusiness.GetEmailByBuyerDivision(DamagedsGoods.BuyerDivision);
                            //var Compras = _sysRoleMasterBusiness.GetEmailsByRol(ConfigurationManager.AppSettings["Compras"].ToString());
                            var UsuarioGerencia = _userMasterBusiness.GetEmailByUserName(DamagedsGoods.cuser);
                            var Proveedor = _MaSupplierContact.Email(Convert.ToInt32(SupplierId), "Ventas");
                            var Email = EmailsBuyer
                            + (!string.IsNullOrWhiteSpace(EmailsBuyer) && !string.IsNullOrWhiteSpace(UsuarioGerencia) ? ", " + UsuarioGerencia : UsuarioGerencia)
                            + (!string.IsNullOrWhiteSpace(UsuarioGerencia) && !string.IsNullOrWhiteSpace(Proveedor) ? ", " + Proveedor : Proveedor);

                            information.folio = DamagedsGoods.DamagedGoodsDoc.ToString();
                            information.Supplier = DamagedsGoods.Supplier;
                            information.store = _siteConfigRepository.GetSiteCodeName();
                            information.subject = SiteInfo.SiteName + " - Aprobación de Cancelación de Devolución";
                            information.from = SiteInfo.SiteName;
                            information.email = Email;

                            common.MailMessageRMACancelRequestRMA008(information, stream);
                        });
                        task.Start();
                    }
                    return Json(true, JsonRequestBehavior.AllowGet);
                }
            }
            return Json(false, JsonRequestBehavior.AllowGet);
        }

        public ActionResult ActionGetInventoryAdjustmentRequests()
        {
            return Json(new { Json = _RmaHeadBusiness.GetAllRmaHeadStatusToCancelRequest("RMA", 9) }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult ActionGetRMA(string Type, DateTime Date1, DateTime Date2, int Status)
        {
            return Json(new { success = true, Json = _RmaHeadBusiness.GetAllRmaHeadStatus(Type, Date1, Date2, Status) }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult ActionGetApprovedRMA()
        {
            return Json(new { success = true, Json = _RmaHeadBusiness.GetAllRmaHeadStatus("Remision", DateTime.Now.AddDays(-7), DateTime.Now, 2) }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult ActionGetPendingRMA()
        {
            return Json(new { success = true, Json = _RmaHeadBusiness.GetAllRmaHeadStatus("Remision", DateTime.Now.AddDays(-7), DateTime.Now, 1) }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult ActionGetPendingRemission()
        {
            return Json(new { success = true, Json = _RmaHeadBusiness.GetAllRmaHeadStatus("RMA", DateTime.Now.AddDays(-7), DateTime.Now, 1) }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult ActionGetPendingRemissionByCost()
        {
            return Json(new { success = true, Json = _RmaHeadBusiness.GetAllRmaHeadStatus("RMA", DateTime.Now.AddDays(-7), DateTime.Now, 0) }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult ActionGetFinishedRMA(string Type, DateTime BeginDate, DateTime EndDate, int Status)
        {
            if (BeginDate >= DateTime.Now.AddDays(-30))
                return Json(new { success = true, Json = _RmaHeadBusiness.GetAllRmaHeadStatus(Type, BeginDate, EndDate, Status) }, JsonRequestBehavior.AllowGet);
            else
                return Json(new { success = true, Json = _RmaHeadBusiness.GetAllRmaHeadStatus(Type, DateTime.Now.AddDays(-30), EndDate, Status) }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult ActionGetScraptPdf(string document, int status)
        {
            if (User.Identity.IsAuthenticated)
            {
                var stream = new MemoryStream();
                ConfirmacionSalidaMerma reporteScrap = new ConfirmacionSalidaMerma();
                List<DamagedsGoodsModel> documentItems = _damagedsGoodsItemBusiness.NewGetItemsByDocumentReport(document, status);
                var ScrapHeader = _damagedsGoodsItemBusiness.GetDocumentGoods(document);
                if (ScrapHeader.Print_Status != 0 && status == 9)
                {
                    reporteScrap.Watermark.Text = "RE-IMPRESION";
                    reporteScrap.Watermark.TextDirection = DevExpress.XtraPrinting.Drawing.DirectionMode.ForwardDiagonal;
                    reporteScrap.Watermark.Font = new Font(reporteScrap.Watermark.Font.FontFamily, 40);
                    reporteScrap.Watermark.ForeColor = Color.DodgerBlue;
                    reporteScrap.Watermark.TextTransparency = 150;
                    reporteScrap.Watermark.ShowBehind = false;
                    reporteScrap.Watermark.PageRange = "1-99";
                    reporteScrap.DataSource = reporteScrap.printTable(documentItems, document, ScrapHeader.SiteName, ScrapHeader.Fecha);
                    var print = _RmaHeadBusiness.DamagedsGoodsEditStatusPrint(document, ScrapHeader.Print_Status, "SSM003.CSHTML");
                }
                else
                {
                    reporteScrap.DataSource = reporteScrap.printTable(documentItems, document, ScrapHeader.SiteName, ScrapHeader.Fecha);
                    var print = _RmaHeadBusiness.DamagedsGoodsEditStatusPrint(document, ScrapHeader.Print_Status, "SSM003.CSHTML");
                }

                reporteScrap.ExportToPdf(stream);
                string Pdf = Convert.ToBase64String(stream.ToArray());
                return Json(new { success = true, pdf = Pdf }, JsonRequestBehavior.AllowGet);
            }
            return Json(new { success = false, session = "SF" }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult ActionPdfBamati(string Folio, int SupplierId, int typeAproval)
        {
            if (Session["User"] != null)
            {
                var DamagedsGoods = _RmaHeadBusiness.DamagedGoodsApprovedPrintRMA(SupplierId, Folio);
                List<EmailDamagedGoodsModel> listaRemision = new List<EmailDamagedGoodsModel>();
                List<DamagedsGoodsModel> documentItems = _DamagedsGoodsItemBusiness.GetItemsByDocumentReport(DamagedsGoods.DamagedGoodsDoc, DamagedsGoods.ProcessStatus);

                if (typeAproval == 1)
                {

                    DamagedsGoods.DamageGoodsItem = documentItems;
                    ApprovalBanati report = new ApprovalBanati();
                    report.DataSource = report.printTable(DamagedsGoods.document_no, DamagedsGoods);

                    var stream = new MemoryStream();
                    report.ExportToPdf(stream);
                    listaRemision.Add(new EmailDamagedGoodsModel() { Rerpote = stream, RMA = DamagedsGoods.document_no });

                    return Json(new { Data = true, response = Convert.ToBase64String(stream.ToArray()) }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    DamagedsGoods.DamageGoodsItem = documentItems;
                    DevolucionMercancia report = new DevolucionMercancia();
                    report.DataSource = report.printTable(DamagedsGoods.document_no, DamagedsGoods);

                    var stream = new MemoryStream();
                    report.ExportToPdf(stream);
                    listaRemision.Add(new EmailDamagedGoodsModel() { Rerpote = stream, RMA = DamagedsGoods.document_no });

                    return Json(new { Data = true, response = Convert.ToBase64String(stream.ToArray()) }, JsonRequestBehavior.AllowGet);
                }
            }

            return Json(new { Data = "SF" }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult ActionGetScrapItemsDepartment(DateTime dateInitial, DateTime dateFinish, string department, string status)
        {
            if (Session["User"] != null)
                return Json(new { success = true, data = _RmaHeadBusiness.GetScrapItemsDepartment(dateInitial, dateFinish, department, status) }, JsonRequestBehavior.AllowGet);

            return Json("SF", JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult ActionGetScrapDepartmentTotals(DateTime dateInitial, DateTime dateFinish, int Status, int Department, bool Detail)
        {
            if (Session["User"] != null)
                return Json(new { success = true, data = _RmaHeadBusiness.GetScrapDepartmentTotal(dateInitial, dateFinish, Status, Department, Detail) }, JsonRequestBehavior.AllowGet);
            else
                return Json(new { success = false, data = "Termino tu sesión." }, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public ActionResult ActionChangeStatusScrap(string doc , int old_status , int new_status)
        {
            if (Session["User"] != null)
            {
                 var ret = _RmaHeadBusiness.ChangeStatusScrapCancelation(doc, new_status, old_status, _siteConfigBusiness.GetSiteCode(), Session["User"].ToString());
                //var ret = new Tuple<bool,string>(true,"1000");
                if (ret.Item1)
                {
                    if(new_status == 6)
                    {
                        Task task = new Task(() =>
                        {
                            var Inventarios = _sysRoleMasterBusiness.GetEmailsByRol(ConfigurationManager.AppSettings["DepartamentoInventarios"].ToString());
                            List<DamagedsGoodsModel> documentItems = _damagedsGoodsItemBusiness.NewGetItemsByDocumentReport(doc, new_status);
                            var siteName = _siteConfigRepository.GetSiteCodeName();
                            var siteIp = _siteConfigBusiness.GetSiteCodeAddress();
                            var ScrapHeader = _damagedsGoodsItemBusiness.GetDocumentGoods(doc);
                            ConfirmacionSalidaMerma reporte = new ConfirmacionSalidaMerma();
                            reporte.DataSource = reporte.printTable(documentItems, doc, siteName, ScrapHeader.Fecha);
                            Stream stream = new MemoryStream();
                            reporte.ExportToPdf(stream);
                            information.folio = doc;
                            information.buttonLink = siteIp.IP;
                            information.subject = siteName + " - Cancelacion Merma Conocida";
                            information.from = siteName;
                            information.email = Inventarios;
                            common.MailMessageCancellationScrap(information, stream);

                        });
                        task.Start();
                    }

                    return Json(new { success = true, data = ret.Item2 }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(new { success = true, data = ret.Item2 }, JsonRequestBehavior.AllowGet);
                }
                
               
            }
            else
                return Json(new { success = false, data = "SF" }, JsonRequestBehavior.AllowGet);
        }
    }
}