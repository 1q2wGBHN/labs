﻿using App.BLL.MaCode;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace FloridoERPTX.Controllers.MaCode
{
    public class MaCodeController : Controller
    {
        private readonly MaCodeBusiness _MaCodeBusiness;

        public MaCodeController()
        {
            _MaCodeBusiness = new MaCodeBusiness();
        }
        // GET: MaCode
        public ActionResult ActionGetListCurrency ()
        {
           return Json(_MaCodeBusiness.GetCurrency(), JsonRequestBehavior.AllowGet);
           
        }
    }
}