﻿using App.BLL.Sales;
using System.Web.Mvc;
using App.Entities;
using App.BLL.Invoices;
using App.BLL.Lookups;
using App.BLL.Customer;
using System.Collections.Generic;
using System;
using System.Linq;
using App.Entities.ViewModels;
using App.Entities.ViewModels.Invoices;
using App.Entities.ViewModels.Common;
using static App.Common.Common;
using FloridoERPTX.Reports;
using DevExpress.XtraReports.UI;
using App.BLL.Site;
using FloridoERPTX.CommonMethods;
using App.BLL.DigitalTaxStamp;
using static App.Entities.ViewModels.Common.Constants;
using App.BLL.TransactionsCustomers;
using App.BLL.Currency;
using System.Globalization;
using FloridoERPTX.Timbrado;
using App.BLL.SysLog;
using App.BLL.CreditNotes;
using App.Entities.ViewModels.CreditNotes;
using App.BLL.Ma_Tax;
using App.Entities.ViewModels.Customers;
using System.Text;
using Ionic.Zip;
using App.BLL;
using System.IO;
using System.Configuration;
using System.Xml;
using FloridoERPTX.Filters;
using App.BLL.CancelInvoicesSales;

namespace FloridoERPTX.Controllers.Invoices
{
    public class InvoicesController : Controller
    {
        #region BLL Variables
        private SalesBusiness _SaleBusiness;
        private InvoiceBusiness _InvoiceBusiness;
        private SatPaymentMethodCodeBusiness _SatPaymentMethodCodeBusiness;
        private SatUseCodeBusiness _SatUseCodeBusiness;
        private CustomerBusiness _CustomerBusinnes;
        private PaymentMethodBusiness _PaymentMethodBusiness;
        private SalesPaymentMethodBusiness _SalesPaymentMethodBusiness;
        private ReferenceNumberBusiness _ReferenceNumberBusiness;
        private InvoiceDetailBusiness _InvoiceDetailBusiness;
        private InvoiceDetailTaxBusiness _InvoiceDetailTaxBusiness;
        private SalesDetailBusiness _SalesDetailBusiness;
        private SiteConfigBusiness _SiteConfigBusiness;
        private StampDocument _StampDocument;
        private DigitalTaxStampBusiness _DigitalTaxStampBusiness;
        private TransactionsCustomersBusiness _TransactionsCustomersBusiness;
        private CurrencyBusiness _CurrencyBusiness;
        private SysLogBusiness _SysLogBusiness;
        private CreditNotesBusiness _CreditNotesBusiness;
        private CreditNotesDetailBusiness _CreditNotesDetailBusiness;
        private CreditNotesDetailTaxBusiness _CreditNotesDetailTaxBusiness;
        private SatUnitCodeBusiness _SatUnitCodeBusiness;
        private SatProductCodeBusiness _SatProductCodeBusiness;
        private MaTaxBusiness _MaTaxBusiness;
        private UserMasterBusiness _UserMasterBussines;
        private CustomerLookup _CustomerLookup;
        private CancelInovicesSalesBusiness _CancelInovicesSalesBusiness;
        #endregion

        public InvoicesController()
        {
            _SaleBusiness = new SalesBusiness();
            _InvoiceBusiness = new InvoiceBusiness();
            _SatPaymentMethodCodeBusiness = new SatPaymentMethodCodeBusiness();
            _SatUseCodeBusiness = new SatUseCodeBusiness();
            _CustomerBusinnes = new CustomerBusiness();
            _PaymentMethodBusiness = new PaymentMethodBusiness();
            _SalesPaymentMethodBusiness = new SalesPaymentMethodBusiness();
            _ReferenceNumberBusiness = new ReferenceNumberBusiness();
            _InvoiceDetailBusiness = new InvoiceDetailBusiness();
            _InvoiceDetailTaxBusiness = new InvoiceDetailTaxBusiness();
            _SalesDetailBusiness = new SalesDetailBusiness();
            _SiteConfigBusiness = new SiteConfigBusiness();
            _StampDocument = new StampDocument();
            _DigitalTaxStampBusiness = new DigitalTaxStampBusiness();
            _TransactionsCustomersBusiness = new TransactionsCustomersBusiness();
            _CurrencyBusiness = new CurrencyBusiness();
            _SysLogBusiness = new SysLogBusiness();
            _CreditNotesBusiness = new CreditNotesBusiness();
            _CreditNotesDetailBusiness = new CreditNotesDetailBusiness();
            _CreditNotesDetailTaxBusiness = new CreditNotesDetailTaxBusiness();
            _SatUnitCodeBusiness = new SatUnitCodeBusiness();
            _SatProductCodeBusiness = new SatProductCodeBusiness();
            _MaTaxBusiness = new MaTaxBusiness();
            _UserMasterBussines = new UserMasterBusiness();
            _CustomerLookup = new CustomerLookup();
            _CancelInovicesSalesBusiness = new CancelInovicesSalesBusiness();
        }

        #region Create Invoice
        // GET: Invoices
        public ActionResult INVO001()
        {
            InvoicesSalesIndex Model = new InvoicesSalesIndex();
            return View(Model);
        }

        [HttpPost]
        public ActionResult INVO001(InvoicesSalesIndex Model)
        {
            var Reference = _ReferenceNumberBusiness.GetReferenceNumber(VoucherType.INCOMES);            
            InvoicesSales Item = new InvoicesSales();
            var customer = new CustomerViewModel();
            if (!Model.IsGlobal)
            {
                var Sale = _SaleBusiness.GetSaleByNumber(long.Parse(Model.SalesNumber));
                customer = Sale.customer_code == GenericCustomer.CODE ? new CustomerViewModel() : _CustomerBusinnes.GetCustomerByCode(Sale.customer_code);
                Item.SaleNumber = Sale.sale_id;
                Item.Subtotal = Sale.sale_total - Sale.sale_tax;
                Item.IVA = Sale.sale_tax;
                Item.Total = Sale.sale_total;
                Item.PaymentTypeCode = _SalesPaymentMethodBusiness.GetPaymentMethodBySaleId(Sale.sale_id);
                Item.IsPMCredit = Item.PaymentTypeCode == PaymentMethods.CREDIT;                
            }
            else
            {
                var SalesTotal = _SaleBusiness.GetGlobalInvoiceTotal(Model.SaleDate);
                Item.SaleNumber = 0;
                Item.Total = SalesTotal.sale_total;
                Item.IVA = SalesTotal.sale_iva;
                Item.Subtotal = SalesTotal.sale_total - SalesTotal.sale_iva;
                Item.PaymentTypeCode = PaymentMethods.MXN_CASH;
            }

            Item.Date = string.Format("{0}", Model.SaleDate);
            Item.IsGlobal = Model.IsGlobal;
            Item.Number = Reference.number;
            Item.Serie = Reference.serie;
            Item.NextInvoice = string.Format("{0}{1}", Reference.serie, Reference.number);
            Item.SaleDate = Model.SaleDate;
            if (customer != null && Item.IsPMCredit)
            {
                Item.ClientName = customer.Name;
                Item.ClientNumber = customer.Code;
                Item.Client = customer.Code;
                Item.ClientCode = customer.Code;
            }
            else
            {
                Item.ClientNumber = "GENERAL";
                Item.ClientName = "VENTAL AL PUBLICO EN GENERAL";
                Item.Client = "GENERAL";

            }
            FillLookups(false, Item.IsPMCredit);
            return View("INVO002", Item);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [PreventDuplicateRequest]
        public ActionResult ActionCreateInvoice(InvoicesSales Model)
        {
            if (Session["User"] != null)
            {
                Model.User = Session["User"].ToString();
                try
                {
                    if (ModelState.IsValid)
                        return CreateInvoice(Model);
                    else
                        return Json(new { success = false, Json = Model, responseText = "Ya hay un proceso realizando la factura, no sea desesperado." }, JsonRequestBehavior.AllowGet);

                }
                catch (Exception ex)
                {
                    Rollback(Model.INVOICE, false);
                    string msg = ex.Message;
                    return Json(new { success = false, Json = Model, responseText = "Ocurrió un error, contacte a sistemas" }, JsonRequestBehavior.AllowGet);
                }
            }

            return Json(new { success = false, Json = Model, responseText = "Sesión expiró" }, JsonRequestBehavior.AllowGet);
        }

        private ActionResult CreateInvoice(InvoicesSales Model)
        {
            var n = _ReferenceNumberBusiness.GetReferenceNumber(VoucherType.INCOMES);
            while (_InvoiceBusiness.InvoiceExist(n.number))
            {
                _InvoiceBusiness.SprocUpdateInvoiceNumber();
                var referenceNumber = _ReferenceNumberBusiness.GetReferenceNumber(VoucherType.INCOMES);
                n.number = referenceNumber.number;
            }

            var ResponseStamp = new Response();
            bool isCurrencyUSD = false;
            bool Stamp16 = ConfigurationManager.AppSettings["Stamp16"].ToString() == "1";
            bool HasTax16 = false;            
            Model.Number = n.number;
            Model.Serie = n.serie;
            bool IsCredit = Model.PaymentTypeCode == PaymentMethods.CREDIT || Model.PaymentTypeCode == PaymentMethods.COMBO_CARD;// || Model.PaymentTypeCode == PaymentMethods.COMPENSATION;
            #region Create Invoice Item            

            long? GlobalDetailId = null; //This is used for global invoices
            var Invoice = Model.INVOICE;
            var PM = _PaymentMethodBusiness.GetPaymentMethodById(Model.PaymentTypeCode);
            var Customer = Model.IsGlobal
                ? _CustomerBusinnes.GetCustomer(GenericCustomer.CODE)
                : _CustomerBusinnes.GetCustomer(Model.ClientNumber);
            var Sale = _SaleBusiness.GetSaleByNumber(Model.SaleNumber);
            Invoice.cur_code = Model.PaymentTypeCode == PaymentMethods.CREDIT
                ? Model.Currency
                : (Sale != null && !Model.IsGlobal
                    ? Sale.DFLPOS_SALES_DETAIL.First().cur_code
                    : (PM != null ? PM.currency : CURRENCY_DEFAULT)); //Default currency code for sales
            Invoice.customer_code = Customer.Code;
            Invoice.invoice_date = Model.IsGlobal
                ? DateTime.ParseExact(Model.SaleDate, DateFormat.INT_DATE, CultureInfo.InvariantCulture)
                : DateTime.Now.Date;
            Invoice.invoice_status = true;            
            Invoice.is_daily = Model.IsGlobal;            
            Invoice.site_code = _SiteConfigBusiness.GetSiteCode();
            Invoice.sat_pm_code = PM != null ? PM.sat_pm_code : SatPaymentMethods.UNDEFINED;
            Invoice.cdate = DateTime.Now;
            Invoice.program_id = "INVO002.cshtml";
            if (Model.Currency == Currencies.USD && Model.PaymentTypeCode == PaymentMethods.CREDIT)
            {
                isCurrencyUSD = true;
                Invoice.invoice_total = Math.Round(Model.Total / Sale.DFLPOS_SALES_DETAIL.First().currency_rate, 2);
                Invoice.invoice_tax = Invoice.invoice_tax > 0
                    ? Math.Round(Invoice.invoice_tax / Sale.DFLPOS_SALES_DETAIL.First().currency_rate, 2)
                    : Invoice.invoice_tax;
                Invoice.invoice_cur_rate = Sale.DFLPOS_SALES_DETAIL.First().currency_rate;
            }

            //Invoice.invoice_balance = Invoice.is_credit && Model.PaymentTypeCode != PaymentMethods.COMPENSATION ? Invoice.invoice_total : 0;                              
            if(Model.PaymentTypeCode == PaymentMethods.COMPENSATION)
            {
                if (Model.PM_Type == PaymentType.PPD)
                    Invoice.payment_type = PaymentTypes.PPD;
                else
                    Invoice.payment_type = PaymentTypes.PUE;
            }

            Invoice.is_credit = Model.PaymentTypeCode == PaymentMethods.COMPENSATION && Model.PM_Type == PaymentType.PPD ? true : IsCredit;
            if (Invoice.is_credit && Model.PaymentTypeCode != PaymentMethods.COMPENSATION)
                Invoice.invoice_balance = Invoice.invoice_total;
            else
                Invoice.invoice_balance = 0;

            Invoice.invoice_duedate = Invoice.is_credit && Customer.Credit_Days.HasValue ? (DateTime.Now.Date.AddDays(Customer.Credit_Days.Value)) : Invoice.invoice_date;
            #endregion

            //Create Invoice
            if (_InvoiceBusiness.CreateInvoice(Invoice))
            {
                var InvoiceDetailList = Model.IsGlobal
                    ? _InvoiceDetailBusiness.InvoiceGlobal(Model.Serie, Model.Number, Model.Total)
                    : _InvoiceDetailBusiness.GetInvoiceDetailsFromSalesDetails(Model.Serie, Model.Number, Model.SaleNumber,
                        isCurrencyUSD);
                var DetailsTaxList = new List<DetailTax>();

                //Create Invoice Detail foreach Item on Invoiced Sale
                if (!_InvoiceDetailBusiness.CreateInvoiceDetail(InvoiceDetailList))
                    goto ErrorInvoice;

                if (!Model.IsGlobal)
                {
                    if (!string.IsNullOrEmpty(Model.NewCardNumber))
                    {
                        Customer.Code = "T" + Model.NewCardNumber;
                        _CustomerBusinnes.Update(Customer.CUSTOMER, null);
                    }

                    //Update Sale with Invoice Serie and Number (single sale)
                    Sale.invoice_no = Model.Number;
                    Sale.invoice_serie = Model.Serie;
                    Sale.customer_code = Customer.Code;
                    if (!_SaleBusiness.UpdateSale(Sale))
                        goto ErrorInvoice;
                }
                else
                {                    
                    //Update Sales with Invoice Serie and Number (multiple sales)                        
                    if (!_SaleBusiness.UpdateSalesWithInvoiceData(Customer.Code, Model.Serie, Model.Number, Model.SaleDate))
                        goto ErrorInvoice;

                    GlobalDetailId = _InvoiceDetailBusiness.GetDetailByInvoice(Model.Number, Model.Serie).detail_id;                    
                }

                //Update PaymentMethodSale with Invoice Serie and Number                    
                if (!_SalesPaymentMethodBusiness.UpdatePaymentMethodsWithInvoiceData(Model.Serie, Model.Number))
                    goto ErrorInvoice;

                //Create Invoice Detail Tax foreach Item that has tax
                DetailsTaxList = Model.IsGlobal
                    ? _SalesDetailBusiness.SalesDetailsWithTax(Model.Serie, Model.Number, GlobalDetailId.Value)
                    : _InvoiceDetailBusiness.InvoiceDetailsWithTax(Model.Serie, Model.Number, Model.SaleNumber, isCurrencyUSD);
                var InsertTaxList = CreateTaxList(DetailsTaxList, Model.IsGlobal);
                if (!_InvoiceDetailTaxBusiness.CreateInvoiceDetailTax(InsertTaxList))
                    goto ErrorInvoice;

                HasTax16 = InsertTaxList.Any(c => c.tax_code == TaxCodes.TAX_16);                

                //Create record in Transaction Customer Table
                if (Invoice.is_credit)
                    if (!_TransactionsCustomersBusiness.CreateTransactions(Invoice, null, null, false))
                        goto ErrorInvoice;


                //Check if Header Tax and Detail Tax Match
                if (Model.IsGlobal && !_InvoiceBusiness.CheckInvoiceTax(Invoice))
                {
                    if (!_InvoiceBusiness.MatchHeaderTaxDetailTax(Invoice, Model))
                        _SysLogBusiness.CreateLogStampDocuments(Model, "Error al actualizar cabecera de factura para igualar IVA", "Error 418", false, false);
                }

                //Check if Total And Subtotals Match
                if (!Model.IsGlobal && !_InvoiceBusiness.CheckTotalAndSubtotalInvoice(Invoice))
                {
                    if (!_InvoiceBusiness.MatchTotalAndSubtotal(Invoice))
                        _SysLogBusiness.CreateLogStampDocuments(Model, "Error al actualizar detalles de factura para igualar totales", "Error 418", false, false);
                }
                                
                if (!HasTax16 || (HasTax16 && Stamp16)) //Validation to Stamps Invoices with TAX at 16%
                    //Create Stamp Document
                    ResponseStamp = _StampDocument.DocumentStamp(Model, false);
                else
                {
                    ResponseStamp.Status = false;
                    ResponseStamp.ErrorNumber = "IVA 16";
                    ResponseStamp.Message = "Factura tiene IVA al 16% y no se puede timbrar";                    
                }

                if (ResponseStamp.Status)
                {
                    if(Model.IsGlobal)
                    {
                        var SumPMEmployeeDiscount = _SaleBusiness.GetBonificationEmployeeTotalByInvoiceDate(Invoice.invoice_date);

                        //Create EmployeeBonification                            
                        if (SumPMEmployeeDiscount > 0)
                            CreateEmployeeBonification(Model.Number, Model.Serie, Model.User, SumPMEmployeeDiscount, Model.SaleDate);
                    }

                    Model.IsStamped = true;
                    var newInstance = new InvoiceBusiness();
                    var EntityInvoice = newInstance.GetInvoiceByNumber(Model.Number);
                    Model.PdfEncoded = EntityInvoice.pdf;
                    return Json(new { success = ResponseStamp.Status, Json = Model, responseText = "Venta Facturada Correctamente" }, JsonRequestBehavior.AllowGet);
                }
                else if (_SysLogBusiness.CreateLogStampDocuments(Model, ResponseStamp.Message, ResponseStamp.ErrorNumber, false, false))
                {
                    Model.IsStamped = false;

                    if (!string.IsNullOrEmpty(ResponseStamp.ErrorNumber) && ResponseStamp.ErrorNumber.Contains("CFDI33132"))
                    {                        
                        Invoice.invoice_status = false;
                        Invoice.udate = DateTime.Now;
                        Invoice.uuser = Model.User;

                        if (!Model.IsGlobal)
                            _CancelInovicesSalesBusiness.CreateRecord(Invoice, Model.SaleNumber, Customer.RFC);

                        if (_InvoiceBusiness.UpdateInvoice(Invoice, null))
                            if (_SaleBusiness.UpdateSalesFromCanceledlInvoice(Invoice.invoice_no, Invoice.invoice_serie, Invoice.is_credit))
                                return Json(new { success = false, Json = Model, responseText = "RFC debe estar en la lista de RFC inscritos no cancelados en el SAT." }, JsonRequestBehavior.AllowGet);
                    }
                    else
                        return Json(new { success = true, Json = Model, responseText = "Documento creado pero no timbrado." }, JsonRequestBehavior.AllowGet);
                }
                else
                    goto ErrorInvoice;
            }

        ErrorInvoice:
            Rollback(Invoice, false);
            return Json(new { success = false, Json = Invoice, responseText = "Ocurrió un error, contacte a sistemas" },
                JsonRequestBehavior.AllowGet);
        }

        List<INVOICE_DETAIL_TAX> CreateTaxList(List<DetailTax> DetailsTaxList, bool IsGlobal)
        {
            var InvDetailTaxList = new List<INVOICE_DETAIL_TAX>();
            var InvDetailTaxListGlobal = new List<INVOICE_DETAIL_TAX>();

            foreach (var Element in DetailsTaxList)
            {                
                if (Element.TaxAmount > 0)
                {
                    Element.TaxCode = Element.TaxAmount > 0 && Element.IVACode == TaxCodes.NOT_AVAILABLE ? TaxCodes.TAX_8 : Element.IVACode;                    
                    InvDetailTaxList.Add(Element.INVOICEDETAILTAX);
                }

                if (!string.IsNullOrEmpty(Element.IEPSCode) && Element.IEPSCode != TaxCodes.NOT_AVAILABLE)
                {
                    Element.TaxCode = Element.IEPSCode;
                    decimal ItemPriceNOIeps = Math.Round(Element.ItemPrice / (1 + (Element.IEPSValue.Value / 100)), 2);
                    var Amount = Math.Round((Element.IEPSValue.Value / 100) * ItemPriceNOIeps, 3); //If a number = 1.435 on DB saves it has 1.43, for SAT purpose we need it has 1.44
                    Element.TaxAmount = Math.Round(Amount, 2);

                    InvDetailTaxList.Add(Element.INVOICEDETAILTAX);
                }
            }

            if (IsGlobal)
            {
                InvDetailTaxListGlobal = (from idt in InvDetailTaxList
                                          group idt by new { idt.tax_code, idt.detail_id } into g
                                          select new INVOICE_DETAIL_TAX
                                          {
                                              tax_code = g.Key.tax_code,
                                              detail_id = g.Key.detail_id,
                                              tax_amount = g.Sum(d => d.tax_amount)
                                          }).ToList();

                return InvDetailTaxListGlobal;
            }

            return InvDetailTaxList;
        }

        [HttpGet] //Update Invoice view with data client.
        public ActionResult ActionUpdateInvoiceDetails(InvoicesSales Item)
        {
            var customer = _CustomerBusinnes.GetCustomer(Item.ClientCode);
            Item.City = customer.CityName;
            Item.State = customer.StateName;
            Item.Address = customer.Address;
            Item.PhoneNumber = customer.Phone;
            Item.ZipCode = customer.ZipCode;
            Item.FaxNumber = customer.Phone_2;
            Item.ClientName = customer.Name;
            Item.ClientNumber = customer.Code;
            return Json(new { success = true, Json = Item, responseText = "Datos cliente actualizados" }, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public ActionResult ActionGetInvoicesTotalOnUSD(InvoicesSales Item)
        {
            var sale = _SaleBusiness.GetSaleByNumber(Item.SaleNumber);
            var saleRate = sale != null ? sale.DFLPOS_SALES_DETAIL.First().currency_rate : 1;

            Item.IVA = Math.Round(Item.IVA / saleRate, 2);
            Item.Subtotal = Math.Round(Item.Subtotal / saleRate, 2);
            Item.Total = Math.Round(Item.Total / saleRate, 2);
            Item.CurrencyExchange = saleRate;
            return Json(new { success = true, Json = Item, responseText = "Datos factura actualizados" }, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public ActionResult ActionGetInvoicesTotalOnMXN(InvoicesSales Item)
        {
            var sale = _SaleBusiness.GetSaleByNumber(Item.SaleNumber);

            Item.IVA = Math.Round(sale.sale_tax, 2);
            Item.Subtotal = Math.Round(sale.sale_total - sale.sale_tax, 2);
            Item.Total = Math.Round(sale.sale_total, 2);
            Item.CurrencyExchange = 0;
            return Json(new { success = true, Json = Item, responseText = "Datos factura actualizados" }, JsonRequestBehavior.AllowGet);
        }
        #endregion        

        #region Cancel Invoice

        public ActionResult INVO003()
        {
            CancelModel Model = new CancelModel();
            return View(Model);
        }

        [HttpPost]
        public ActionResult INVO003(CancelModel Model)
        {
            var Number = long.Parse(Model.Number);
            Model.IsFromMenu = false;
            Model.InvoDetails = _InvoiceBusiness.InvoiceDetails(Number);
            Model.InvoDetails.Products = _InvoiceDetailBusiness.InvoiceProductsList(Number, null);
            return View(Model);
        }        

        [HttpPost]
        public ActionResult ActionCancelInvoice(CancelModel Model)
        {
            if (Session["User"] != null)
            {
                var Number = long.Parse(Model.Number);
                Model = PopulateCancelModel(Number);  
                
                var Invoice = _InvoiceBusiness.GetInvoiceByNumber(Number);
                var ResponseStamp = new Response();
                if (!string.IsNullOrEmpty(Model.Uuid))
                    ResponseStamp = _StampDocument.DocumentStamp(Model, true); //IsCancel = true            
                else
                    ResponseStamp.Status = true;

                var stampInfo = _DigitalTaxStampBusiness.GetStampInfo(Invoice.invoice_no, Invoice.invoice_serie, VoucherType.INCOMES);

                //Call web service to cancel stamp            
                if (ResponseStamp.Status || (stampInfo.cancelled_request.HasValue && stampInfo.cancelled_request.Value) || (stampInfo == null && Invoice.invoice_status))
                {
                    var SysLog = new SYS_LOG();
                    DateTime dt = DateTime.Now;
                    SysLog.Action = "CANCELÓ";
                    SysLog.Date = DateTime.Now.Date;
                    SysLog.Description = "CANCELACIÓN DE FACTURA";
                    SysLog.idAfect = Number;
                    SysLog.TableName = "INVOICES";
                    SysLog.Menu = "VENTAS";
                    SysLog.time = dt.ToString("HH:mm");
                    SysLog.uuser = Session["User"].ToString();

                    Invoice.invoice_status = false;
                    Invoice.uuser = Session["User"].ToString();
                    Invoice.udate = DateTime.Now;

                    if (_InvoiceBusiness.UpdateInvoice(Invoice, SysLog))
                    {
                        if (_SaleBusiness.UpdateSalesFromCanceledlInvoice(Invoice.invoice_no, Invoice.invoice_serie, Invoice.is_credit))
                            return Json(new { success = true, Json = Number, responseText = string.Format("Factura {0} cancelada correctamente.", Number) }, JsonRequestBehavior.AllowGet);
                        else
                        {
                            Invoice.invoice_status = true;
                            _InvoiceBusiness.UpdateInvoice(Invoice, SysLog);
                        }
                    }
                }
                else //Create error log.
                    _SysLogBusiness.CreateLogStampDocuments(Model, ResponseStamp.Message, ResponseStamp.ErrorNumber, true, false);

                return Json(new { success = false, Json = Number, responseText = "Ocurrió un error, contacte a sistemas" }, JsonRequestBehavior.AllowGet);
            }

            return Json(new { success = false, Json = Model, responseText = "Terminó tu sesión" }, JsonRequestBehavior.AllowGet);
        }

        private CancelModel PopulateCancelModel(long InvoiceNumber)
        {
            var CancelModel = new CancelModel();            
            var Invoice = _InvoiceBusiness.GetInvoiceByNumber(InvoiceNumber);
            var StampInfo = _DigitalTaxStampBusiness.GetStampInfo(Invoice.invoice_no, Invoice.invoice_serie, VoucherType.INCOMES);
            CancelModel.Number = InvoiceNumber.ToString();
            CancelModel.Uuid = StampInfo != null ? StampInfo.UUID : string.Empty;
            CancelModel.Serie = Invoice.invoice_serie;
            CancelModel.DocumentType = VoucherType.INCOMES;
            CancelModel.User = Session["User"].ToString();
            return CancelModel;
        }
        #endregion       

        #region Validations
        [HttpPost]
        public ActionResult ActionValidateSale(InvoicesSalesIndex Model)
        {
            var Reference = _ReferenceNumberBusiness.GetReferenceNumber(VoucherType.INCOMES);
            if (Reference == null)
                return Json(new { success = false, Json = Model.SalesNumber, responseText = string.Format("Por el momento no hay número ni serie disponibles para factura.", Model.SalesNumber) }, JsonRequestBehavior.AllowGet);

            if (!Model.IsGlobal)
            {
                long SaleNumber = long.Parse(Model.SalesNumber);

                if (_SaleBusiness.IsSaleInvoiced(SaleNumber))
                    return Json(new { success = false, Json = SaleNumber, responseText = string.Format("Venta {0} ya esta facturada en el sistema.", SaleNumber) }, JsonRequestBehavior.AllowGet);
                else if (!_SaleBusiness.ExistingSale(SaleNumber))
                    return Json(new { success = false, Json = SaleNumber, responseText = string.Format("Venta {0} no existe.", SaleNumber) }, JsonRequestBehavior.AllowGet);
                else if (_SaleBusiness.SalePMForbidden(SaleNumber))
                    return Json(new { success = false, Json = SaleNumber, responseText = string.Format("Venta {0} pagada con un método que no permite facturas.", SaleNumber) }, JsonRequestBehavior.AllowGet);
                else if (_SaleBusiness.IsSaleCancelled(SaleNumber))
                    return Json(new { success = false, Json = SaleNumber, responseText = string.Format("Venta {0} esta cancelada.", SaleNumber) }, JsonRequestBehavior.AllowGet);
                else if(_CancelInovicesSalesBusiness.HasSaleCancelInovice(SaleNumber))
                    return Json(new { success = false, Json = SaleNumber, responseText = string.Format("Venta {0} ya se intentó facturar con RFC no aprobado por el SAT.", SaleNumber) }, JsonRequestBehavior.AllowGet);
                //else if (!_SaleBusiness.IsSaleFromToday(SaleNumber))
                //    return Json(new { success = false, Json = SaleNumber, responseText = string.Format("Venta {0} no corresponde al día de hoy.", SaleNumber) }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                var SalesList = _SaleBusiness.GetUnInvoicedSalesByDate(Model.SaleDate);

                if (SalesList.Count <= 0)
                    return Json(new { success = false, Json = Model, responseText = string.Format("Fecha {0} no tiene ventas por facturar.", Model.SaleDate) }, JsonRequestBehavior.AllowGet);
            }

            return Json(new { success = true, Json = Model, responseText = "Válido para facturar." }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult ActionValidateInvoice(long InvoiceNumber)
        {
            var Invoice = _InvoiceBusiness.GetInvoiceByNumber(InvoiceNumber);
            //var Sales = _SaleBusiness.GetSalesByInvoiceNumber(InvoiceNumber);
            string User = Session["User"].ToString();
            if (Invoice == null)
                return Json(new { success = false, Json = InvoiceNumber, responseText = string.Format("Factura {0} no existe.", InvoiceNumber) }, JsonRequestBehavior.AllowGet);
            if (!Invoice.invoice_status)
                return Json(new { success = false, Json = InvoiceNumber, responseText = string.Format("Factura {0} cancelada.", InvoiceNumber) }, JsonRequestBehavior.AllowGet);            
            if (_InvoiceBusiness.InvoiceHasCreditNotesOrBonifications(InvoiceNumber))
                return Json(new { success = false, Json = InvoiceNumber, responseText = string.Format("Factura {0} tiene notas de crédito o bonificaciones activas.", InvoiceNumber) }, JsonRequestBehavior.AllowGet);
            if (_InvoiceBusiness.InvoiceHasPayments(InvoiceNumber))
                return Json(new { success = false, Json = InvoiceNumber, responseText = string.Format("Factura {0} a crédito y tiene pagos vigentes.", InvoiceNumber) }, JsonRequestBehavior.AllowGet);
            if (Invoice.invoice_date != DateTime.Now.Date && !_UserMasterBussines.IsInRole(User, Roles.IT_Department))
                return Json(new { success = false, Json = InvoiceNumber, responseText = string.Format("Factura {0} no corresponde al día de hoy.", InvoiceNumber) }, JsonRequestBehavior.AllowGet);
            //if (Sales.Count <= 0)
            //    return Json(new { success = false, Json = InvoiceNumber, responseText = string.Format("Factura {0} no esta asociada a alguna venta.", InvoiceNumber) }, JsonRequestBehavior.AllowGet);

            return Json(new { success = true, Json = InvoiceNumber, responseText = string.Format("Factura {0} disponible para cancelar.", InvoiceNumber) }, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Substitute Invoice
        public ActionResult INVO007()
        {
            var Model = new InvoicesSales();
            return View(Model);
        }

        [HttpPost]
        public ActionResult INVO007(InvoicesSales Model)
        {            
            Model = _InvoiceBusiness.InvoiceSubstituteInfo(long.Parse(Model.InvoiceNumber));
            Model.IsFromMenu = false;
            FillLookups();
            return View(Model);
        }

        public ActionResult ActionSubstitutionInvoice(InvoicesSales Model)
        {
            var ResponseStamp = new Response();

            //Cancel Previous Invoice            
            var StampInfo = _DigitalTaxStampBusiness.GetStampInfo(long.Parse(Model.InvoiceNumber), Model.Serie, VoucherType.INCOMES);
            if (StampInfo != null && StampInfo.cancelled_request.Value && StampInfo.cancelled_accepted.Value)
                ResponseStamp.Status = true;
            else
            {
                var cancelModel = PopulateCancelModel(long.Parse(Model.InvoiceNumber));
                ResponseStamp = _StampDocument.DocumentStamp(cancelModel, true); //IsCancel = true
            }

            //Create New Invoice
            var Reference = _ReferenceNumberBusiness.GetReferenceNumber(VoucherType.INCOMES);
            long InvoiceSubst = long.Parse(Model.InvoiceSubst.Replace(Model.Serie, string.Empty));
            Model.Serie = Reference.serie;
            Model.Number = Reference.number;
            Model.DocumentType = VoucherType.INCOMES;
            Model.User = Session["User"].ToString();
            var Invoice = Model.INVOICE;

            if (ResponseStamp.Status)
            {
                if (!_InvoiceBusiness.CreateInvoiceSubtitution(Model))
                    goto ErrorReturn;
                else
                    Invoice = _InvoiceBusiness.SubstitutionInvoice(Model, InvoiceSubst);

                if (!_SaleBusiness.UpdateSalesWithSubstitutedInvoice(Model.Serie, Model.Number, InvoiceSubst))
                    goto ErrorReturn;

                if (!_SalesPaymentMethodBusiness.UpdatePaymentMethodsWithInvoiceData(Model.Serie, Model.Number))
                    goto ErrorReturn;

                ResponseStamp = _StampDocument.DocumentStamp(Model, false);
                if (ResponseStamp.Status)
                {
                    Model.IsStamped = true;
                    var newInstance = new InvoiceBusiness();
                    var EntityInvoice = newInstance.GetInvoiceByNumber(Model.Number);
                    Model.PdfEncoded = EntityInvoice.pdf;
                    return Json(new { success = ResponseStamp.Status, Json = Model, responseText = "Venta Facturada Correctamente" }, JsonRequestBehavior.AllowGet);
                }
                else if (_SysLogBusiness.CreateLogStampDocuments(Model, ResponseStamp.Message, ResponseStamp.ErrorNumber, false, false))
                {
                    Model.IsStamped = false;

                    return Json(new { success = true, Json = Model, responseText = "Documento creado pero no timbrado." }, JsonRequestBehavior.AllowGet);
                }
                else
                    goto ErrorReturn;
            }
            else
                return Json(new { success = false, Json = Model, responseText = "Documento a sustituir no se pudo cancelar." }, JsonRequestBehavior.AllowGet);

            ErrorReturn:
            Rollback(Invoice, true);
            return Json(new { success = false, Json = Model, responseText = "Ocurrió un error, contacte a sistemas" }, JsonRequestBehavior.AllowGet);
        }
        #endregion

        void FillLookups(bool isCfdi = false, bool isCredit = false)
        {
            if (!isCfdi)
                ViewBag.PaymentTypeCode = new SelectList(_PaymentMethodBusiness.PaymentMethodList(isCredit, true), "pm_id", "pm_name");
            else
                ViewBag.PaymentTypeCode = new SelectList(_PaymentMethodBusiness.PaymentMethodListSpecialInvoices(), "pm_id", "pm_name");
            ViewBag.CFDICode = new SelectList(_SatUseCodeBusiness.SatUseCodeList(), "sat_us_code", "sat_use_description");
            ViewBag.Currency = new SelectList(_CurrencyBusiness.CurrencyList(), "cur_code", "cur_name");            

            var list = new List<SelectListItem>();
            ViewBag.ClientCode = list;

            if (isCfdi)
            {
                var cfdiType = new List<SelectListItem>();
                var item = new SelectListItem();
                item.Text = "Ingresos";
                item.Value = "I";
                cfdiType.Add(item);
                ViewBag.CFDIType = cfdiType;
                ViewBag.UnitSatCode = list;
                ViewBag.ItemSatCode = list;
                ViewBag.TAXCode = new SelectList(_MaTaxBusiness.GetTaxList(), "tax_code", "name");
                ViewBag.IEPSCode = new SelectList(_MaTaxBusiness.GetIEPSList(), "tax_code", "name");
            }
        }

        [HttpGet]
        public JsonResult ActionGetCustomers()
        {            
            var customers = _CustomerLookup.FillLookupCustomer(CustomersLookup.ForInvoices);
            return Json(new { success = true, Json = customers }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult ActionValidateFilters(string CustomerCode)
        {
            string message;
            var Customer = _CustomerBusinnes.GetCustomerByCode(CustomerCode);
            if (Customer == null)
            {
                message = string.Format("<p>La entrada <strong>{0}</strong> no es un código de cliente existente.</p>", CustomerCode);
                return Json(new
                {
                    success = false,
                    Json = CustomerCode,
                    responseText = message
                }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                message = "<p>" + Customer.Name + "</p>";
                return Json(new
                {
                    success = true,
                    Json = CustomerCode,
                    responseText = message
                }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public ActionResult INVO004(InvoiceIndex model)
        {
            model = _InvoiceBusiness.GetListInvoiceModel(model);

            return View(model);
        }

        public ActionResult INVO004()
        {
            var modelIndex = new InvoiceIndex();
            modelIndex.StartDate = DateTime.Now.Date.ToString(DateFormat.INT_DATE);
            modelIndex.EndDate = DateTime.Now.Date.ToString(DateFormat.INT_DATE);
            modelIndex = _InvoiceBusiness.GetListInvoiceModel(modelIndex);
            return View(modelIndex);
        }

        [HttpGet]
        public ActionResult ActionGetInvoiceDetail(long InvoiceNumber)
        {
            return Json(new { success = true, Json = _InvoiceBusiness.GetInvoiceReportDetail(InvoiceNumber) }, JsonRequestBehavior.AllowGet);
        }        

        [HttpPost]
        public ActionResult ActionReportGralInvoices(InvoiceIndex model)
        {
            model.IsDataForReport = true;
            model = _InvoiceBusiness.GetListInvoiceModel(model);
            
            NewInvoicesReport report = new NewInvoicesReport();
            string siteName = _SiteConfigBusiness.GetSiteCodeName();
            report.DataSource = report.printTable(model, siteName);

            return PartialView("~/Views/ReportsViews/InvoicesReport.cshtml", report);
        }

        public ActionResult ActionReportCreditNotesDetails(string date, long InvoiceNumber, string customer, string serie)
        {
            SalesReportDetail report = new SalesReportDetail();
            string siteName = _SiteConfigBusiness.GetSiteCodeName();
            string Invoice = string.Format("{0}{1}", serie, InvoiceNumber);
            (report.Bands["DetailReport"] as DetailReportBand).DataSource = report.printTable(_InvoiceBusiness.GetInvoiceReportDetail(InvoiceNumber), "", date, Invoice, false, siteName,customer,"");
            (report.Bands["DetailReport"] as DetailReportBand).DataMember = "xrTableRowFormat";
            return PartialView("~/Views/ReportsViews/SaleReportView.cshtml", report);
        }

        public ActionResult INVO005()
        {
            var Model = new InvoiceOldBalanceIndex();
            ViewBag.Currency = new SelectList(_CurrencyBusiness.CurrencyList(), "cur_code", "cur_name");
            return View(Model);
        }

        [HttpPost]
        public ActionResult INVO005(InvoiceOldBalanceIndex Model)
        {
            ViewBag.Currency = new SelectList(_CurrencyBusiness.CurrencyList(), "cur_code", "cur_name");
            Model.IsFromMenu = false;
            Model.ReportData = _InvoiceBusiness.CustomerBalanceByCurrency(Model.Currency);
            Model.Date = DateTime.Now.ToString(DateFormat.INT_DATE, CultureInfo.CreateSpecificCulture("es-ES"));
            Model.Store = _SiteConfigBusiness.GetSiteCodeName();
            return View(Model);
        }

        public ActionResult ActionGetInvoiceDetails(string Currency, string CustomerCode)
        {
            return Json(new { success = true, Json = _InvoiceBusiness.OldBalanceReportData(Currency, CustomerCode) }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult ActionGetOldBalanceReportByCustomer(string Currency, InvoiceOldBalanceReport Model)
        {
            var report = new OldBalanceReportByCustomer();
            Model.InvoicesList = _InvoiceBusiness.OldBalanceReportData(Currency, Model.CustomerCode);
            (report.Bands["DetailReport"] as DetailReportBand).DataSource = report.printTable(Model, Currency, _SiteConfigBusiness.GetSiteCodeName());
            (report.Bands["DetailReport"] as DetailReportBand).DataMember = "xrTableRowFormat";

            return PartialView("~/Views/ReportsViews/ReportView.cshtml", report);
        }
        
        [HttpPost]
        public ActionResult ActionGetOldBalanceReport(InvoiceOldBalanceIndex Model)
        {
            var report = new NewOldBalanceReport();
            var ReportList = new List<InvoiceOldBalanceReport>();
            ReportList = _InvoiceBusiness.CustomerBalanceByCurrency(Model.Currency);
            (report.Bands["DetailReport"] as DetailReportBand).DataSource = report.printTable(ReportList, Model.Currency, Model.Store);
            (report.Bands["DetailReport"] as DetailReportBand).DataMember = "xrTableRowFormat";
            return PartialView("~/Views/ReportsViews/ReportView.cshtml", report);
        }
        
        [HttpPost]
        public ActionResult ActionGetPDFInvoice(long CFDI)
        {
            var Entity = new INVOICES();
            string PdfEncoded = string.Empty;

            Entity = _InvoiceBusiness.GetInvoiceByNumber(CFDI);

            if (!string.IsNullOrEmpty(Entity.pdf))
                return Json(new { success = true, Json = Entity.pdf, responseText = "Documento PDF" }, JsonRequestBehavior.AllowGet);

            return Json(new { success = false, Json = PdfEncoded, responseText = "Factura sin timbrar, no tiene documento PDF" }, JsonRequestBehavior.AllowGet);
        }

        public bool CreateEmployeeBonification(long InvoiceNo, string InvoiceSerie, string User, decimal EmployeeDiscountTotal, string DateValue)
        {
            var Reference = _ReferenceNumberBusiness.GetReferenceNumber(VoucherType.EXPENSES);
            while (_CreditNotesBusiness.DocumentExist(Reference.number))
            {
                _CreditNotesBusiness.SprocUpdateCreditNoteNumber();
                var n = _ReferenceNumberBusiness.GetReferenceNumber(VoucherType.EXPENSES);
                Reference = n;
            }
            var EmployeeBoni = new CreditNote();
            var ResponseStamp = new Response();
            DateTime Date = DateTime.ParseExact(DateValue, DateFormat.INT_DATE, CultureInfo.InvariantCulture);

            EmployeeBoni.User = User;
            EmployeeBoni.Number = Reference.number;
            EmployeeBoni.Serie = Reference.serie;
            EmployeeBoni.IssuedDate = DateValue;
            EmployeeBoni.IsBonification = true;
            EmployeeBoni.Cause = string.Format("Descuento sobre venta correspondiente al {0}", Date.ToString("D", new CultureInfo("es-ES")));
            EmployeeBoni.Comments = string.Format("Descuento sobre venta correspondiente al {0}", Date.ToString("D", new CultureInfo("es-ES")));
            BonificationTax SubTotal = new BonificationTax();
            SubTotal.TaxCode = TaxCodes.SUB_TOTAL;
            SubTotal.TaxName = TaxTypes.SUBTOTAL;
            SubTotal.Bonification = EmployeeDiscountTotal; //Here goes the sum of payment method == 18
            SubTotal.TaxValue = 0;
            EmployeeBoni.BonificationTaxes.Add(SubTotal);
            EmployeeBoni.IsEmployeeBon = true;
            EmployeeBoni.InvoiceDetails = _InvoiceBusiness.InvoiceDetails(InvoiceNo);

            if (!_CreditNotesBusiness.CheckEmployeeBonificationByDate(DateValue))
            {
                if (_CreditNotesBusiness.CreateCreditNote(EmployeeBoni))
                    if (_CreditNotesDetailBusiness.CreateCreditNoteDetail(EmployeeBoni))
                    {
                        ResponseStamp = _StampDocument.DocumentStamp(EmployeeBoni, false);
                        if (!ResponseStamp.Status)
                            _SysLogBusiness.CreateLogStampDocuments(EmployeeBoni, ResponseStamp.Message, ResponseStamp.ErrorNumber, false, false);

                        return true;
                    }
            }
            else
                return true;

            return false;
        }

        private bool Rollback(INVOICES Invoice, bool IsSubstitution)
        {
            long Number = Invoice.invoice_no;
            string Serie = Invoice.invoice_serie;

            bool ContinueDeleting = true;
            if (Invoice.is_credit)
            {
                var Model = new DeleteModel();
                Model.DocumentType = VoucherType.INCOMES;
                Model.InvoiceNumber = Number;
                Model.InvoiceSerie = Serie;

                ContinueDeleting = _TransactionsCustomersBusiness.DeleteTransactions(Model);
            }

            if (ContinueDeleting)
                if (_InvoiceDetailTaxBusiness.DeleteDetailTax(Number, Serie))
                    if (_SalesPaymentMethodBusiness.RollbackUpdatePaymentMethods(Serie, Number))
                        if (_CreditNotesDetailTaxBusiness.DeleteDetailTaxByInvoice(Number, Serie))
                            if (_CreditNotesDetailBusiness.DeleteCreditNoteDetailsByInvoice(Number, Serie))
                                if (_CreditNotesBusiness.DeleteCreditNoteByInvoice(Number, Serie))
                                    if (_InvoiceDetailBusiness.DeleteInvoiceDetails(Number, Serie))
                                        if (_SaleBusiness.RollbackUpdateSales(Serie, Number, Invoice.invoice_no_subst, IsSubstitution))
                                            if (_InvoiceBusiness.DeleteInvoice(Number, Serie))
                                                if (_ReferenceNumberBusiness.UpdateReferenceNumber(Number, VoucherType.INCOMES))
                                                    return true;

            return false;
        }

        public ActionResult INVO006()
        {
            var Reference = _ReferenceNumberBusiness.GetReferenceNumber(VoucherType.INCOMES);
            var Model = new CreateCFDI();
            Model.Serie = Reference.serie;
            Model.Number = Reference.number;
            Model.NextInvoice = string.Format("{0}{1}", Reference.serie, Reference.number);
            Model.CurrencyExchange = _CurrencyBusiness.GetCurrencyRateByDate(DateTime.Now.Date.ToString(DateFormat.INT_DATE));
            FillLookups(true, false);
            return View(Model);
        }

        //Special Invoice
        [HttpPost]
        public ActionResult INVO006(CreateCFDI Model)
        {
            Model.User = Session["User"].ToString();            
            var PaymentMethod = _PaymentMethodBusiness.GetPaymentMethodById(Model.PaymentTypeCode);
            var Customer = _CustomerBusinnes.GetCustomer(Model.ClientCode);
            var InvoiceDetailList = new List<InvoiceDetailsInsert>();
            var TaxList = new List<DetailTax>();
            var ResponseStamp = new Response();
            bool IsCredit = Model.PaymentTypeCode == PaymentMethods.CREDIT;// || Model.PaymentTypeCode == PaymentMethods.COMPENSATION;
            Model.Number = CheckReferenceNumber();
            var Invoice = Model.INVOICE;
            Invoice.invoice_balance = IsCredit && Model.PaymentTypeCode != PaymentMethods.COMPENSATION ? Invoice.invoice_total : 0;
            Invoice.invoice_duedate = IsCredit && Model.PaymentTypeCode != PaymentMethods.COMPENSATION && Customer.Credit_Days.HasValue ? (DateTime.Now.Date.AddDays(Customer.Credit_Days.Value)) : DateTime.Now.Date;            
            Invoice.invoice_cur_rate = Model.Currency == Currencies.USD ? Model.CurrencyExchange : 0;
            Invoice.site_code = _SiteConfigBusiness.GetSiteCode();
            Invoice.sat_pm_code = PaymentMethod != null ? PaymentMethod.sat_pm_code : SatPaymentMethods.UNDEFINED;            
            Invoice.program_id = "INVO006.cshtml";

            if (Model.PM_Type == PaymentType.PPD)
                Invoice.payment_type = PaymentTypes.PPD;
            else
                Invoice.payment_type = PaymentTypes.PUE;

            Invoice.is_credit = Model.PaymentTypeCode == PaymentMethods.COMPENSATION && Model.PM_Type == PaymentType.PPD ? true : IsCredit;

            foreach (var item in Model.Concepts)
            {                
                var tax = _MaTaxBusiness.GetTaxValueByCode(item.TaxCode);
                var detailPrice = (item.UnitPrice * item.Quantity) - ((tax.tax_value * (item.UnitPrice * item.Quantity)) / 100);
                var InvoDetail = new InvoiceDetailsInsert();                

                InvoDetail.InvoiceNumber = Model.Number;
                InvoDetail.InvoiceSerie = Model.Serie;
                InvoDetail.ItemPartNumber = item.ItemSatCode;
                InvoDetail.ItemDescription = item.Description;
                InvoDetail.ItemPartPercentage = tax.tax_value;
                InvoDetail.DetailQty = item.Quantity;
                InvoDetail.QtyAvailable = item.Quantity;
                InvoDetail.DetailPrice = detailPrice;
                InvoDetail.ProductCode = item.ItemSatCode;
                InvoDetail.UnitCode = item.UnitSatCode;

                InvoiceDetailList.Add(InvoDetail);
            }

            //Create Header Invoice
            if (_InvoiceBusiness.CreateInvoice(Invoice))
            {
                //Create Details Invoice
                if (!_InvoiceDetailBusiness.CreateInvoiceDetail(InvoiceDetailList))
                    goto ErrorSpecialInvoice;


                //Create Tax Details Invoice
                TaxList = _InvoiceDetailBusiness.SpecialInvoiceDetails(Model.Serie, Model.Number, Model.Concepts);
                var InsertTaxList = CreateTaxList(TaxList, false);                
                if (!_InvoiceDetailTaxBusiness.CreateInvoiceDetailTax(InsertTaxList))
                    goto ErrorSpecialInvoice;

                //Update Header with total and tax
                var sumTotal = InvoiceDetailList.Sum(c => c.DetailPrice) + InsertTaxList.Where(d => d.tax_code.Contains(TaxTypes.TAX)).Sum(c => c.tax_amount);
                var sumTaxList = InsertTaxList.Where(d => d.tax_code.Contains(TaxTypes.TAX)).Sum(c => c.tax_amount);
               
                Invoice.invoice_total = sumTotal;
                Invoice.invoice_tax = sumTaxList;
                Invoice.invoice_balance = IsCredit && Model.PaymentTypeCode != PaymentMethods.COMPENSATION ? Invoice.invoice_total : 0;
                if (!_InvoiceBusiness.UpdateInvoice(Invoice, null))
                    goto ErrorSpecialInvoice;

                //Create record in Transaction Customer Table
                if (Invoice.is_credit)
                    if (!_TransactionsCustomersBusiness.CreateTransactions(Invoice, null, null, false))
                        goto ErrorSpecialInvoice;

                //Create Stamp Document
                Model.DocumentType = Model.CFDIType;
                ResponseStamp = _StampDocument.DocumentStamp(Model, false);
                if (ResponseStamp.Status)
                {
                    Model.IsStamped = true;
                    return Json(new { success = Response.Status, Json = Model, responseText = "Venta Facturada Correctamente" }, JsonRequestBehavior.AllowGet);
                }
                else if (_SysLogBusiness.CreateLogStampDocuments(Model, ResponseStamp.Message, ResponseStamp.ErrorNumber, false, false))
                {
                    Model.IsStamped = false;
                    return Json(new { success = true, Json = Model, responseText = "Documento creado pero no timbrado." }, JsonRequestBehavior.AllowGet);
                }
                else
                    goto ErrorSpecialInvoice;
            }

        ErrorSpecialInvoice:
            Rollback(Invoice, false);
            return Json(new { success = false, Json = Invoice, responseText = "Ocurrió un error, contacte a sistemas" }, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public ActionResult ActionGetUnitSat()
        {
            var unitSat = _SatUnitCodeBusiness.SatUseCodeListSPROC();
            return Json(new { success = true, Json = unitSat }, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public ActionResult ActionGetProductSat()
        {
            var productSat = Json(_SatProductCodeBusiness.SatProductCodeListSPROC(), JsonRequestBehavior.AllowGet);
            productSat.MaxJsonLength = int.MaxValue;
            return productSat;
        }

        [HttpPost]
        public ActionResult ActionGetXMLInvoices(InvoiceIndex Model)
        {            
            var List = _InvoiceBusiness.GetInvoicesListByFilterReport(Model);
            using (ZipFile zip = new ZipFile())
            {
                foreach (var item in List.Where(c => !string.IsNullOrEmpty(c.xml)))
                {
                    byte[] xmlFile = null;
                    string fileName = string.Format("{0}{1}.xml", item.invoice_serie, item.invoice_no);
                    xmlFile = Encoding.UTF8.GetBytes(item.xml);
                    zip.AddEntry(fileName, xmlFile);
                }

                if (zip.Count > 0)
                {
                    DateTime StartDate = DateTime.ParseExact(Model.StartDate, DateFormat.INT_DATE, CultureInfo.InvariantCulture);
                    DateTime EndDate = DateTime.ParseExact(Model.EndDate, DateFormat.INT_DATE, CultureInfo.InvariantCulture);
                    string zipName = string.Format("Facturas del {0} al {1}.zip", StartDate.ToString("d", CultureInfo.CreateSpecificCulture("de-DE")), EndDate.ToString("d", CultureInfo.CreateSpecificCulture("de-DE")));
                    using (MemoryStream output = new MemoryStream())
                    {
                        zip.Save(output);
                        return File(output.ToArray(), "application/zip", zipName);
                    }
                }
            }
            return null;
        }
        
        public ActionResult ActionGetXMLFile(long InvoiceNo)
        {
            XmlDocument doc = new XmlDocument();
            var InvoiceItem = _InvoiceBusiness.GetInvoiceByNumber(InvoiceNo);            
            string fileName = string.Format("{0}{1}.xml", InvoiceItem.invoice_serie, InvoiceItem.invoice_no);            
           
            using (MemoryStream output = new MemoryStream())
            {
                doc.LoadXml(InvoiceItem.xml);
                XmlTextWriter writer = new XmlTextWriter(output, Encoding.UTF8);
                doc.WriteTo(writer);
                writer.Flush();
                Response.Clear();
                byte[] byteArray = output.ToArray();
                Response.AppendHeader("Content-Disposition", "filename=" + fileName);
                Response.AppendHeader("Content-Length", byteArray.Length.ToString());
                Response.ContentType = "application/octet-stream";
                Response.BinaryWrite(byteArray);
                writer.Close();
            }
            return null;
        }

        [HttpPost]
        public ActionResult ActionCreateInvoiceK(InvoicesSales Model)
        {            
            Model.User = "Kiosco";            
            try
            {
                var Sale = _SaleBusiness.GetSaleByNumber(Model.SaleNumber);
                Model.Total = Sale.sale_total;
                Model.IVA = Sale.sale_tax;
                return CreateInvoice(Model);
            }
            catch (Exception ex)
            {
                Rollback(Model.INVOICE, false);
                string msg = ex.Message;
                return Json(new { success = false, Json = Model, responseText = "Ocurrió un error, contacte a sistemas" }, JsonRequestBehavior.AllowGet);
            }
        }

        private long CheckReferenceNumber()
        {
            var n = _ReferenceNumberBusiness.GetReferenceNumber(VoucherType.INCOMES);
            while (_InvoiceBusiness.InvoiceExist(n.number))
            {
                _InvoiceBusiness.SprocUpdateInvoiceNumber();
                var referenceNumber = _ReferenceNumberBusiness.GetReferenceNumber(VoucherType.INCOMES);
                n.number = referenceNumber.number;
            }
            return n.number;
        }
    }
}
