﻿using App.BLL;
using App.BLL.Configuration;
using App.BLL.MaCode;
using App.BLL.MaEmail;
using App.BLL.Site;
using App.BLL.Transfer;
using App.Common;
using App.Entities.ViewModels.Android;
using App.Entities.ViewModels.Transfer;
using DevExpress.XtraReports.UI;
using FloridoERPTX.Reports;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Mvc;
using Formatting = Newtonsoft.Json.Formatting;

namespace FloridoERPTX.Controllers.Transfer
{
    public class TransferHeaderController : Controller
    {
        private readonly TransferHeaderBusiness _TransferHeaderBusiness;
        private readonly TransferHeaderInBusiness _transferHeaderInBusiness;
        private readonly SiteConfigBusiness _siteConfigBusiness;
        private readonly SiteBusiness _siteBusiness;
        private readonly Common _common;
        private readonly SysRoleMasterBusiness _sysRoleMasterBusiness;
        private readonly UserMasterBusiness _UserMasterBusiness;
        private readonly MaEmailBusiness _maEmailBusiness;
        private readonly SendMail information;
        private readonly TransferDetailBusiness _TransferDetailBusiness;
        private readonly MaCodeBusiness _maCodeBusiness;

        public TransferHeaderController()
        {
            _siteConfigBusiness = new SiteConfigBusiness();
            _sysRoleMasterBusiness = new SysRoleMasterBusiness();
            _common = new Common();
            _siteBusiness = new SiteBusiness();
            _TransferHeaderBusiness = new TransferHeaderBusiness();
            _UserMasterBusiness = new UserMasterBusiness();
            _transferHeaderInBusiness = new TransferHeaderInBusiness();
            _maEmailBusiness = new MaEmailBusiness();
            information = new SendMail();
            _TransferDetailBusiness = new TransferDetailBusiness();
            _maCodeBusiness = new MaCodeBusiness();
        }
        [HttpGet]
        public ActionResult ActionValidateDestinationSite(string PartNumber,string  DestinationSite)
        {
            var b = _TransferHeaderBusiness.ValidateDestinationCode(PartNumber,DestinationSite);

            return Json(new { Status = true, value = b }, JsonRequestBehavior.AllowGet);

        }
        public ActionResult ActionGetAllTransferHeader(DateTime DateInit, DateTime DateFin)//
        {
            if (Session["User"] != null)
                return new JsonResult() { Data = _TransferHeaderBusiness.GetAllTransferHeaderInit(DateInit, DateFin), JsonRequestBehavior = JsonRequestBehavior.AllowGet, MaxJsonLength = Int32.MaxValue };
            //return Json(new { Data = _TransferHeaderBusiness.GetTransferHeaders(Status, DateInit, DateFin) }, JsonRequestBehavior.AllowGet);
            return Json(new { Data = "SF" }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult ActionGetTransfersInTransit()
        {
            if (Session["User"] != null)
                return Json(new { Data = _TransferHeaderBusiness.GetAllTransferByMonth() }, JsonRequestBehavior.AllowGet);

            return Json(new { Data = "SF" }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult ActionGetTransfersInTransitByDates(DateTime BeginDate, DateTime EndDate)
        {
            if (Session["User"] != null)
                return Json(new { Data = _TransferHeaderBusiness.GetAllTransferByDates(BeginDate, EndDate) }, JsonRequestBehavior.AllowGet);

            return Json(new { Data = "SF" }, JsonRequestBehavior.AllowGet);
        }

        //public ActionResult ActionEditStatusTransferHeaderReal(string Document)
        //{
        //    if (Session["User"] != null)
        //        return new JsonResult() { Data = _TransferHeaderBusiness.TransferHeaderEditStatusReal(Document, Session["User"].ToString()), JsonRequestBehavior = JsonRequestBehavior.AllowGet, MaxJsonLength = Int32.MaxValue };

        //    return new JsonResult() { Data = "SF", JsonRequestBehavior = JsonRequestBehavior.AllowGet, MaxJsonLength = Int32.MaxValue };
        //}

        [HttpPost]
        public ActionResult ActionEditStatusTransferHeader(List<TransferDetailModel> Model, string Document, int Status, string Comment, string driverCode)
        {
            if (Session["User"] != null)
                return new JsonResult() { Data = _TransferHeaderBusiness.TransferHeaderEditStatus(Model, Document, Status, Session["User"].ToString(), Comment, driverCode), JsonRequestBehavior = JsonRequestBehavior.AllowGet, MaxJsonLength = Int32.MaxValue };


            return new JsonResult() { Data = "SF", JsonRequestBehavior = JsonRequestBehavior.AllowGet, MaxJsonLength = Int32.MaxValue };
        }

        public ActionResult ActionGetAllTransferDetail(string Document)
        {
            if (Session["User"] != null)
                return Json(new { Data = _TransferHeaderBusiness.GetAllTransferDeatil(Document), JsonRequestBehavior = JsonRequestBehavior.AllowGet }, JsonRequestBehavior.AllowGet);

            return Json(new { Data = "SF" }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult ActionGetAllTransferDeatilsExist(string Document, string TransferSiteCode)
        {
            if (Session["User"] != null)
                return Json(new { Data = _TransferHeaderBusiness.GetAllTransferDeatilsExist(Document, TransferSiteCode) }, JsonRequestBehavior.AllowGet);

            return Json(new { Data = "SF" }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult ActionGetAllTransferDetailNew(string Document, string TransferSiteCode, string DestinationSite)
        {
            if (Session["User"] != null)
                return Json(new { Data = _TransferHeaderBusiness.GetAllTransferDetail(Document, TransferSiteCode, DestinationSite) }, JsonRequestBehavior.AllowGet);

            return Json(new { Data = "SF" }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult ActionGetAllTransferDetailNewV2(string Document, string TransferSiteCode, string DestinationSiteCode)
        {
            if (Session["User"] != null)
                return Json(new { Data = _TransferHeaderBusiness.GetAllTransferDetailNew(Document, TransferSiteCode, DestinationSiteCode) }, JsonRequestBehavior.AllowGet);

            return Json(new { Data = "SF" }, JsonRequestBehavior.AllowGet);
        }





        [HttpPost]
        public ActionResult ActionSendItemsToDamageWeb(List<TransferHeaderAndroidModel> Items, string SiteDestination)
        {
            if (Session["User"] != null)
            {
                List<TransferHeaderAndroidModel> ListItems = new List<TransferHeaderAndroidModel>();
                for (int i = 0; i < Items.Count; i++)
                {
                    TransferHeaderAndroidModel model = new TransferHeaderAndroidModel
                    {
                        part_number = Items[i].part_number,
                        quantity = Items[i].quantity,
                        part_description = Items[i].part_description
                    };
                    ListItems.Add(model);
                }

                string result = _TransferHeaderBusiness.SendItemsToTransferWeb(ListItems, Session["User"].ToString(), _siteConfigBusiness.GetSiteCode(), SiteDestination);
                if (result == "")
                {
                    return new JsonResult() { Data = "1", JsonRequestBehavior = JsonRequestBehavior.AllowGet, MaxJsonLength = Int32.MaxValue };
                }
                else if (result == "ERROR HEADER")
                {
                    return new JsonResult() { Data = "2", JsonRequestBehavior = JsonRequestBehavior.AllowGet, MaxJsonLength = Int32.MaxValue };
                }
                else
                {
                    var Gerencia = _sysRoleMasterBusiness.GetEmailsByRol(ConfigurationManager.AppSettings["Gerente"].ToString());
                    string siteDestinationName = _siteBusiness.GetSiteCodeName(SiteDestination);
                    string siteEnvio = _siteConfigBusiness.GetSiteCodeName();


                    var header = _TransferHeaderBusiness.TranferByDoc(result, _siteConfigBusiness.GetSiteCode());
                    var items = _TransferHeaderBusiness.GetAllTransferDetailWithoutStatus(result);

                    Task task = new Task(() =>
                    {
                        SendMail info = new SendMail();
                        TransferItemsDocument reporte = new TransferItemsDocument();
                        Stream stream = new MemoryStream();

                        reporte.DataSource = reporte.printTable(items, result, siteDestinationName, siteEnvio);
                        reporte.ExportToPdf(stream);
                        var Email = Gerencia;

                        info.folio = result;
                        info.subject = "Transferencia de Mercancia";
                        info.from = "El Florido, Area de Recibo";
                        info.email = Email;

                        var json = JsonConvert.SerializeObject(header, Formatting.None);
                        var json64 = System.Convert.ToBase64String(System.Text.Encoding.UTF8.GetBytes(json));
                        var stream2 = new MemoryStream();
                        var writer = new StreamWriter(stream2);
                        writer.Write(json64);
                        writer.Flush();
                        stream2.Position = 0;
                        string status = _common.MailTransferSite(info, stream, null);
                    });
                    task.Start();
                    return Json(new { Data = "0" }, JsonRequestBehavior.AllowGet);
                }
            }
            return new JsonResult() { Data = "SF", JsonRequestBehavior = JsonRequestBehavior.AllowGet, MaxJsonLength = Int32.MaxValue };
        }

        public ActionResult ActionGetAllTransferHeaderWithoutStatus(DateTime DateInit, DateTime DateFin)
        {
            if (Session["User"] != null)
                return new JsonResult() { Data = _TransferHeaderBusiness.GetAllTransferHeaderWithoutStatus(DateInit, DateFin), JsonRequestBehavior = JsonRequestBehavior.AllowGet, MaxJsonLength = Int32.MaxValue };

            return new JsonResult() { Data = "SF", JsonRequestBehavior = JsonRequestBehavior.AllowGet, MaxJsonLength = Int32.MaxValue };
        }

        //public ActionResult ActionPrintTransfer(string Document)
        //{
        //    if (Session["User"] != null)
        //    {
        //        var siteT = _siteConfigBusiness.GetSiteCode();
        //        var ListTransfer = _TransferHeaderBusiness.NewGetAllTransfeDetailsExistFree(Document);
        //        var transfer = _TransferHeaderBusiness.GetTransferHeaderDocument(Document, siteT);
        //        var siteName = _siteBusiness.GetSiteCodeName(siteT);
        //        var siteAdress = _siteBusiness.GetSiteCodeAddress(siteT);
        //        TransferReport report = new TransferReport();
        //        if (transfer.TransferStatus == 8)
        //        {
        //            report.Watermark.Text = "CANCELADO";
        //            report.Watermark.TextDirection = DevExpress.XtraPrinting.Drawing.DirectionMode.ForwardDiagonal;
        //            report.Watermark.Font = new Font(report.Watermark.Font.FontFamily, 60);
        //            report.Watermark.ForeColor = Color.Firebrick;
        //            report.Watermark.TextTransparency = 150;
        //            report.Watermark.ShowBehind = false;
        //            report.Watermark.PageRange = "1-99";
        //        }
        //        else
        //        {
        //            if (transfer.PrintStatus == 1 || transfer.PrintStatus == 2)
        //            {
        //                report.Watermark.Text = "RE-IMPRESION";
        //                report.Watermark.TextDirection = DevExpress.XtraPrinting.Drawing.DirectionMode.ForwardDiagonal;
        //                report.Watermark.Font = new Font(report.Watermark.Font.FontFamily, 40);
        //                report.Watermark.ForeColor = Color.DodgerBlue;
        //                report.Watermark.TextTransparency = 150;
        //                report.Watermark.ShowBehind = false;
        //                report.Watermark.PageRange = "1-99";
        //                _TransferHeaderBusiness.TransferHeaderEditStatusPrint(transfer.TransferDocument, 2, Session["User"].ToString());
        //            }
        //            if (transfer.PrintStatus == 0)
        //            {
        //                _TransferHeaderBusiness.TransferHeaderEditStatusPrint(transfer.TransferDocument, 1, Session["User"].ToString());
        //            }
        //        }

        //        report.DataSource = report.printTable(ListTransfer, transfer, _siteBusiness.GetInfoSite());
        //        var stream = new MemoryStream();
        //        report.ExportToPdf(stream);
        //        return Json(new { success = true, responseText = Convert.ToBase64String(stream.ToArray()) }, JsonRequestBehavior.AllowGet);
        //    }
        //    return new JsonResult() { Data = "SF", JsonRequestBehavior = JsonRequestBehavior.AllowGet, MaxJsonLength = Int32.MaxValue };
        //}

        public ActionResult ActionPrintTransferReport(string Document, string TransferSiteCode, string DestinationSiteCode)
        {
            if (Session["User"] != null)
            {
                
                var transfer = _TransferHeaderBusiness.GetTransferHeaderDocumentReport(Document, TransferSiteCode, DestinationSiteCode);
                var ListTransfer = _TransferHeaderBusiness.GetAllTransferDetailWithoutCancelled(transfer.TransferDocument,transfer.TransferSiteCode,transfer.DestinationSiteCode);
                TransferReport report = TransferHeaderDocumentReport(transfer, ListTransfer);
                var stream = new MemoryStream();
                report.ExportToPdf(stream);
                return Json(new { success = true, responseText = Convert.ToBase64String(stream.ToArray()) }, JsonRequestBehavior.AllowGet);
            }
            return new JsonResult() { Data = "SF", JsonRequestBehavior = JsonRequestBehavior.AllowGet, MaxJsonLength = Int32.MaxValue };
        }

        public  TransferReport TransferHeaderDocumentReport(TransferHeaderModel transfer, List<TransferDetailModel> list)
        {
            TransferReport report = new TransferReport();
            if (transfer.TransferStatus == 8 || transfer.TransferStatus == 2)
            {
                report.Watermark.Text = "CANCELADO";
                report.Watermark.TextDirection = DevExpress.XtraPrinting.Drawing.DirectionMode.ForwardDiagonal;
                report.Watermark.Font = new Font(report.Watermark.Font.FontFamily, 60);
                report.Watermark.ForeColor = Color.Firebrick;
                report.Watermark.TextTransparency = 150;
                report.Watermark.ShowBehind = false;
                report.Watermark.PageRange = "1-99";
            }
            else
            {
                if (transfer.PrintStatus == 1 || transfer.PrintStatus == 2)
                {
                    report.Watermark.Text = "RE-IMPRESION";
                    report.Watermark.TextDirection = DevExpress.XtraPrinting.Drawing.DirectionMode.ForwardDiagonal;
                    report.Watermark.Font = new Font(report.Watermark.Font.FontFamily, 40);
                    report.Watermark.ForeColor = Color.DodgerBlue;
                    report.Watermark.TextTransparency = 150;
                    report.Watermark.ShowBehind = false;
                    report.Watermark.PageRange = "1-99";
                    _TransferHeaderBusiness.TransferHeaderEditStatusPrint(transfer.TransferDocument, 2, Session["User"].ToString());
                }
                if (transfer.PrintStatus == 0)
                {
                    _TransferHeaderBusiness.TransferHeaderEditStatusPrint(transfer.TransferDocument, 1, Session["User"].ToString());
                }
            }

            report.DataSource = report.printTable(list, transfer);
            return report;
        }

        public ActionResult ActionGetTransferHeaderIn(DateTime DateInit, DateTime DateFin)
        {
            if (Session["User"] != null)
                return new JsonResult() { Data = _TransferHeaderBusiness.GetAllTransferHeaderWithoutStatus(DateInit, DateFin), JsonRequestBehavior = JsonRequestBehavior.AllowGet, MaxJsonLength = Int32.MaxValue };

            return new JsonResult() { Data = "SF", JsonRequestBehavior = JsonRequestBehavior.AllowGet, MaxJsonLength = Int32.MaxValue };
        }

        public ActionResult ActionGetTransferDetailIn(string document, string transferSiteCode)
        {
            if (Session["User"] != null)
                return new JsonResult() { Data = _transferHeaderInBusiness.GetTransferDetailsInReport(document, transferSiteCode), JsonRequestBehavior = JsonRequestBehavior.AllowGet, MaxJsonLength = Int32.MaxValue };

            return new JsonResult() { Data = "SF", JsonRequestBehavior = JsonRequestBehavior.AllowGet, MaxJsonLength = Int32.MaxValue };
        }

        public ActionResult ActionGetTransferDetailsIn(string document, string transferSiteCode)
        {
            if (Session["User"] != null)
                return new JsonResult() { Data = _transferHeaderInBusiness.GetTransferDetailsInReport(document, transferSiteCode), JsonRequestBehavior = JsonRequestBehavior.AllowGet, MaxJsonLength = Int32.MaxValue };

            return new JsonResult() { Data = "SF", JsonRequestBehavior = JsonRequestBehavior.AllowGet, MaxJsonLength = Int32.MaxValue };
        }

        public ActionResult ActionGetAllTransferDetailsReport(string Document)
        {
            if (Session["User"] != null)
                return new JsonResult() { Data = _TransferHeaderBusiness.GetAllTransferDetailWithoutStatus(Document), JsonRequestBehavior = JsonRequestBehavior.AllowGet, MaxJsonLength = Int32.MaxValue };

            return new JsonResult() { Data = "SF", JsonRequestBehavior = JsonRequestBehavior.AllowGet, MaxJsonLength = Int32.MaxValue };
        }



        [HttpPost]
        public ActionResult ActionGetTransferHeaderInInputs(DateTime BeginDate, DateTime EndDate, string TransferType, string status, string originSite, string partNumber, string department, string family)
        {
            if (Session["User"] != null)
            {
                var list = _transferHeaderInBusiness.GetTransfersHeaderIn(BeginDate, EndDate, TransferType, status, originSite, partNumber, department, family);
                return Json(new { result = true, Data = list, JsonRequestBehavior.AllowGet });
            }
            else
            {
                return Json(new { result = "SF", JsonRequestBehavior.AllowGet });
            }
        }

        [HttpPost]
        public ActionResult ActionGetTransferHeaderInInputsView(DateTime BeginDate, DateTime EndDate, string TransferType, string status, string originSite, string partNumber, string department, string family)
        {
            if (Session["User"] != null)
            {
                var list = _transferHeaderInBusiness.GetTransfersHeaderInView(BeginDate, EndDate, TransferType, status, originSite, partNumber, department, family);
                return Json(new { result = true, Data = list, JsonRequestBehavior.AllowGet });
            }
            else
            {
                return Json(new { result = "SF", JsonRequestBehavior.AllowGet });
            }
        }

        [HttpPost]
        public ActionResult ActionProcessTransferHeaderIn(string TransferDocument, string TransferSiteCode)
        {
            if (Session["User"] != null)
                return Json(new { Data = _transferHeaderInBusiness.ActionProcessTransferHeaderIn(TransferDocument, Session["User"].ToString(), TransferSiteCode), JsonRequestBehavior.AllowGet });

            return Json(new { Data = "SF", JsonRequestBehavior.AllowGet });
        }

        [HttpPost]
        public ActionResult ActionUpdateTransferHeaderInInputs()
        {
            if (Session["User"] != null)
                return Json(new { result = true, Data = _transferHeaderInBusiness.GetTransfersHeaderIn(DateTime.Now.AddDays(-7), DateTime.Now, "entrada", "", "", "", "", ""), JsonRequestBehavior.AllowGet });

            return Json(new { result = "SF", JsonRequestBehavior.AllowGet });
        }

        public ActionResult ActionGetTransferHeaderInDetail(DateTime date1, DateTime date2, string TransferType, string status, string originSite, string partNumber, string department, string family)
        {
            if (Session["User"] != null)
                return new JsonResult() { Data = _transferHeaderInBusiness.GetAllTransferDetail(date1, date2, TransferType, status, originSite, partNumber, department, family), JsonRequestBehavior = JsonRequestBehavior.AllowGet, MaxJsonLength = Int32.MaxValue };

            return new JsonResult() { Data = "SF", JsonRequestBehavior = JsonRequestBehavior.AllowGet, MaxJsonLength = Int32.MaxValue };
        }

        public ActionResult ActionGetReportTransfer(DateTime BeginDate, DateTime EndDate, string TransferType, string status, string originSite, string partNumber, string department, string family)
        {
            if (Session["User"] != null)
            {
                var transfer = _transferHeaderInBusiness.GetTransfersHeaderIn(BeginDate, EndDate, TransferType, status, originSite, partNumber, department, family);
                string nameUser = _UserMasterBusiness.getCompleteName(Session["User"].ToString());
                TransferTotalReport reportTransfer = new TransferTotalReport();
                (reportTransfer.Bands["DetailReport"] as DetailReportBand).DataSource = reportTransfer.printTable(transfer, _siteBusiness.GetInfoSite(), BeginDate, EndDate, nameUser, TransferType);
                (reportTransfer.Bands["DetailReport"] as DetailReportBand).DataMember = "xrTableRowFormat";
                return PartialView("~/Views/ReportsViews/TransferTotalReport.cshtml", reportTransfer);
            }
            return Json("SF", JsonRequestBehavior.AllowGet);
        }

        public ActionResult ActionGetReportPurchaseDetail(DateTime BeginDate, DateTime EndDate, string TransferType, string status, string originSite, string partNumber, string department, string family)
        {
            if (Session["User"] != null)
            {
                var transfer = _transferHeaderInBusiness.GetAllTransferDetail(BeginDate, EndDate, TransferType, status, originSite, partNumber, department, family);
                string nameUser = _UserMasterBusiness.getCompleteName(Session["User"].ToString());
                TransferTotalReportDetail reportTransfer = new TransferTotalReportDetail();
                (reportTransfer.Bands["DetailReport"] as DetailReportBand).DataSource = reportTransfer.printTable(transfer, _siteBusiness.GetInfoSite(), BeginDate, EndDate, nameUser, TransferType);
                (reportTransfer.Bands["DetailReport"] as DetailReportBand).DataMember = "xrTableRowFormat";
                return PartialView("~/Views/ReportsViews/TransferTotalReport.cshtml", reportTransfer);
            }
            return Json("SF", JsonRequestBehavior.AllowGet);
        }

        //Cancela transferencia en STATUS 6, regresa mercancia a Disponible
        [HttpPost]
        public ActionResult ActionCancelTransferOriginSite(string transferDoc, string destination_site, string transferSiteCode)
        {
            if (Session["User"] != null)
            {
                if (_UserMasterBusiness.IsRolonUser(Session["User"].ToString(), ConfigurationManager.AppSettings["Distrital"].ToString()))
                {
                    //EJECUTA SP
                    var result = _TransferHeaderBusiness.sproc_ST_Transit_Reverse_G(transferDoc, Session["User"].ToString(), transferSiteCode, destination_site);
                    var jsonResult = new JsonResult();
                    switch (result.ReturnValue)
                    {
                        case 0:
                            _TransferHeaderBusiness.CancelTransfer(transferDoc, transferSiteCode, destination_site, Session["User"].ToString());
                            _TransferDetailBusiness.CancelDetails(transferDoc, transferSiteCode, destination_site, Session["User"].ToString());
                            jsonResult = Json(new { success = true, responseText = "Transferencia Cancelada Correctamente.", Json1 = _TransferHeaderBusiness.GetAllTransferHeaderStatus6(), Json2 = _TransferHeaderBusiness.GetAllTransferHeaderStatus7() }, JsonRequestBehavior.AllowGet);
                            break;
                        case 6001:
                            jsonResult = Json(new { success = false, responseText = "Site Incorrecto." }, JsonRequestBehavior.AllowGet);
                            break;
                        case 6002:
                            jsonResult = Json(new { success = false, responseText = "Estatus invalido." }, JsonRequestBehavior.AllowGet);
                            break;
                        case 8001:
                            jsonResult = Json(new { success = false, responseText = "Rollback, Contacta a Sistemas." }, JsonRequestBehavior.AllowGet);
                            break;
                        default:
                            jsonResult = Json(new { success = false, responseText = "DESCONOCIDO, Contacta a Sistemas." }, JsonRequestBehavior.AllowGet);
                            break;
                    }
                    jsonResult.MaxJsonLength = Int32.MaxValue;
                    return jsonResult;
                }
                else
                    return Json(new { success = false, responseText = "Solo puede realizar la Cancelación Distrital." }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(new { success = false, responseText = "SF" }, JsonRequestBehavior.AllowGet);
            }
        }

        //Cancela en Status 7, pondra en Status 8 y generara una transferencia para regresar la mercancia
        [HttpPost]
        public ActionResult ActionCancelTransferDestinationSite(string transferDoc, string destination_site, string transferSiteCode, string driver)
        {
            if (Session["User"] != null)
            {
                if (_UserMasterBusiness.IsRolonUser(Session["User"].ToString(), ConfigurationManager.AppSettings["Distrital"].ToString()))
                {
                    //Actualiza a Status 8 
                    if (_TransferHeaderBusiness.CancelTransfer(transferDoc, transferSiteCode, destination_site, Session["User"].ToString()) == 1 && _TransferDetailBusiness.CancelDetails(transferDoc, transferSiteCode, destination_site, Session["User"].ToString()) == 0)
                    {
                        //GENERA NUEVA TRANSFERENCIA PARA REGRESAR MERCANCIA

                        //trae items de transferencia cancelada
                        List<TransferHeaderAndroidModel> ListItems = _TransferHeaderBusiness.GetItems(transferDoc, destination_site, transferSiteCode);
                        //se invierten los sitios
                        string result = _TransferHeaderBusiness.SendItemsToTransferWeb(ListItems, Session["User"].ToString(), destination_site, transferSiteCode);
                        if (result == "")
                        {
                            return Json(new { success = false, responseText = "Error al momento de insertar el folio." }, JsonRequestBehavior.AllowGet);
                        }
                        else if (result == "ERROR HEADER")
                        {
                            return Json(new { success = false, responseText = "Error al insertar el Header." }, JsonRequestBehavior.AllowGet);
                        }
                        else
                        {
                            var Gerencia = _sysRoleMasterBusiness.GetEmailsByRol(ConfigurationManager.AppSettings["Gerente"].ToString());
                            string siteDestinationName = _siteBusiness.GetSiteCodeName(transferSiteCode);
                            string siteEnvio = _siteConfigBusiness.GetSiteCodeName();

                            var header = _TransferHeaderBusiness.TranferByDoc(result, _siteConfigBusiness.GetSiteCode());
                            var items = _TransferHeaderBusiness.GetAllTransferDetailWithoutStatus(result);

                            Task task = new Task(() =>
                            {
                                SendMail info = new SendMail();
                                TransferItemsDocument reporte = new TransferItemsDocument();
                                Stream stream = new MemoryStream();

                                reporte.DataSource = reporte.printTable(items, result, siteDestinationName, siteEnvio);
                                reporte.ExportToPdf(stream);
                                var Email = Gerencia;

                                info.folio = result;
                                info.subject = "Transferencia de Mercancia";
                                info.from = "El Florido, Area de Recibo";
                                info.email = Email;

                                var json = JsonConvert.SerializeObject(header, Formatting.None);
                                var json64 = System.Convert.ToBase64String(System.Text.Encoding.UTF8.GetBytes(json));
                                var stream2 = new MemoryStream();
                                var writer = new StreamWriter(stream2);
                                writer.Write(json64);
                                writer.Flush();
                                stream2.Position = 0;
                                string status = _common.MailTransferSite(info, stream, null);
                            });
                            task.Start();
                            if (_TransferHeaderBusiness.TransferHeaderEditStatus(items, result, 1, Session["User"].ToString(), header.TransferHeader.comment, driver))
                            {
                                var jsonResult = new JsonResult();
                                jsonResult = Json(new { success = true, responseText = "Transferencia Cancelada y Regresada Correctamente.", Json1 = _TransferHeaderBusiness.GetAllTransferHeaderStatus6(), Json2 = _TransferHeaderBusiness.GetAllTransferHeaderStatus7() }, JsonRequestBehavior.AllowGet);
                                jsonResult.MaxJsonLength = Int32.MaxValue;
                                return jsonResult;
                            }
                            else
                            {
                                return Json(new { success = false, responseText = "Error al Regresar Transferencia en automatico." }, JsonRequestBehavior.AllowGet);
                            }
                        }
                    }
                    else
                    {
                        return Json(new { success = false, responseText = "Error al cancelar transferencia." }, JsonRequestBehavior.AllowGet);
                    }
                }
                else
                    return Json(new { success = false, responseText = "Solo puede realizar la Cancelación Distrital." }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(new { success = false, responseText = "SF" }, JsonRequestBehavior.AllowGet);
            }
        }

        //Solicitud de Cancelacion
        [HttpPost]
        public ActionResult ActionCancelRequestTransferHeader(string TransferDocument, string TransferSiteCode, string Reason, string DestinationName)
        {
            if (Session["User"] != null)
            {
                var emailDistrital = _maCodeBusiness.GetEmailDistrict();
                //var emailDistrital = _sysRoleMasterBusiness.GetEmailsByRol(ConfigurationManager.AppSettings["Distrital"].ToString());
                if (_TransferHeaderBusiness.CancelRequestTransferHeaderNew(TransferDocument, TransferSiteCode, Session["User"].ToString(), Reason, DestinationName))
                {
                Task task = new Task(() =>
                {
                    var TransferHeader = _TransferHeaderBusiness.GetTransferHeaderByDocumentAndSites(TransferDocument, TransferSiteCode, DestinationName);
                    SendMail info = new SendMail();
                    TransferCancelRequest reporte = new TransferCancelRequest();
                    Stream stream = new MemoryStream();

                    string siteDestinationName = DestinationName;//_siteBusiness.GetSiteCodeName(TransferHeader.DestinationSiteCode);
                        string siteEnvio = _siteBusiness.GetSiteCodeName(TransferHeader.TransferSiteCode);

                    reporte.DataSource = reporte.PrintTable(TransferHeader.Items, TransferHeader.TransferDate, TransferDocument, siteDestinationName, siteEnvio);
                    reporte.ExportToPdf(stream);
                    var Email = emailDistrital;//"nbadajos@elflorido.com.mx"; 

                        info.folio = TransferDocument;
                    info.subject = "Cancelación Transferencia de Mercancia";
                    info.from = "El Florido, Gerencia";
                    info.email = Email;
                    string status = _common.MailTransferCancelRequest(info, stream);
                });
                task.Start();

                return Json(new { Data = true }, JsonRequestBehavior.AllowGet);
            }
            }

            return Json(new { Data = "SF" }, JsonRequestBehavior.AllowGet);
        }
    }
}