﻿using App.BLL.Transfer;
using FloridoERPTX.Reports;
using System;
using System.IO;
using System.Web.Mvc;
//
namespace FloridoERPTX.Controllers.Transfer
{
    public class TransferPalletInfoController : Controller
    {
        private readonly TransferPalletInfoBusiness _TransferPalletBusinnes;

        public TransferPalletInfoController()
        {
            _TransferPalletBusinnes = new TransferPalletInfoBusiness();
        }


        public ActionResult ActionListTransferPalletInfoReport(DateTime DateInit, DateTime DateFinal)//
        {
            if (Session["User"] != null)
                return Json(_TransferPalletBusinnes.TransferPalletReport(DateInit, DateFinal, 4), JsonRequestBehavior.AllowGet);

            return new JsonResult() { Data = "SF", JsonRequestBehavior = JsonRequestBehavior.AllowGet, MaxJsonLength = Int32.MaxValue };
        }

        public ActionResult ActionDetaiPalletInfoReport(string PalletSn)//
        {
            if (Session["User"] != null)
                return Json(_TransferPalletBusinnes.TransferPalletDetailReport(PalletSn), JsonRequestBehavior.AllowGet);

            return new JsonResult() { Data = "SF", JsonRequestBehavior = JsonRequestBehavior.AllowGet, MaxJsonLength = Int32.MaxValue };
        }

        [HttpPost]
        public ActionResult ActionPrintTransferReport(string PalletSn)//
        {
            if (Session["User"] != null)
            {
                XtraReport2 report = new XtraReport2();
                report.DataSource = report.printTable(_TransferPalletBusinnes.GetTransferPalletReport(PalletSn), _TransferPalletBusinnes.TransferPalletDetailReport(PalletSn));

                var stream = new MemoryStream();
                report.ExportToPdf(stream);
                return Json(new { success = true, responseText = Convert.ToBase64String(stream.ToArray()) }, JsonRequestBehavior.AllowGet);
            }

            return new JsonResult() { Data = "SF", JsonRequestBehavior = JsonRequestBehavior.AllowGet, MaxJsonLength = Int32.MaxValue };
        }

        public JsonResult ActionTransfersToReceive()
        {
            return Json(_TransferPalletBusinnes.TransfersToReceive(), JsonRequestBehavior.AllowGet);
        }

        public JsonResult ActionPalletsInTransfer(string transferDocument)
        {
            return Json(_TransferPalletBusinnes.PalletsInTransfer(transferDocument), JsonRequestBehavior.AllowGet);
        }

    }
}