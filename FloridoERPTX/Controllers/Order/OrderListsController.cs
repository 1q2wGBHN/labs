﻿using App.BLL.Order;
using App.BLL.PurchaseOrder;
using App.BLL.Site;
using App.Entities;
using DevExpress.XtraReports.UI;
using FloridoERPTX.Reports;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Web.Mvc;

namespace FloridoERPTX.Controllers.Order
{
    public class OrderListsController : Controller
    {
        // GET: OrderLists        
        private readonly PurchaseOrderBusiness __PurchaseOrderBusiness;
        private readonly OrderListBusiness _OrderListBusiness;
        private readonly OrderBusiness _OrderBusiness;
        private readonly SiteBusiness _siteBusiness;

        public OrderListsController()
        {
            _OrderListBusiness = new OrderListBusiness();
            _OrderBusiness = new OrderBusiness();
            __PurchaseOrderBusiness = new PurchaseOrderBusiness();
            _siteBusiness = new SiteBusiness();
        }

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult ActionOrderList(int Folio)
        {
            return Json(_OrderListBusiness.GetAll(Folio), JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult ActionCreate(List<ORDER_LIST> Order)
        {
            return Json(_OrderListBusiness.Post(Order, Session["User"].ToString()), JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult ActionPut(List<App.DAL.Order.OrderListDatesViewModel> OrderList)
        {
            if (Session["User"] != null)
            {
                bool OrderListIsNull = _OrderListBusiness.Put(OrderList, Session["User"].ToString());
                return Json(OrderListIsNull, JsonRequestBehavior.AllowGet);
            }
            return Json("SF", JsonRequestBehavior.AllowGet);
        }

        public ActionResult ActionReportOrder(int Folio)
        {
            SupplierOrdered report = new SupplierOrdered();
            (report.Bands["DetailReport1"] as DetailReportBand).DataSource = report.printTable(_OrderBusiness.Get(Folio), _OrderListBusiness.GetAll(Folio));
            (report.Bands["DetailReport1"] as DetailReportBand).DataMember = "xrTableRowFormat";

            var stream = new MemoryStream();
            report.ExportToPdf(stream);
            return Json(new { success = true, responseText = Convert.ToBase64String(stream.ToArray()) }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult ActionGetPurchasesOrderReport(string poNumber) //genera el reporte supplierOrdered
        {
            __PurchaseOrderBusiness.GetUpdatePurchasePrintStatus(poNumber);
            var status = __PurchaseOrderBusiness.GetPurchaseOrderByNoAllStatus(poNumber);
            if (status.purchase_status == 9)
            {
                PurchasesOrdered report = new PurchasesOrdered();
                if (status.print_status == 2)
                {
                    report.Watermark.Text = "RE-IMPRESION";
                    report.Watermark.TextDirection = DevExpress.XtraPrinting.Drawing.DirectionMode.ForwardDiagonal;
                    report.Watermark.Font = new Font(report.Watermark.Font.FontFamily, 40);
                    report.Watermark.ForeColor = Color.DodgerBlue;
                    report.Watermark.TextTransparency = 150;
                    report.Watermark.ShowBehind = false;
                    report.Watermark.PageRange = "1-99";
                }
                (report.Bands["DetailReport"] as DetailReportBand).DataSource = report.printTable(__PurchaseOrderBusiness.NewGetAllPurchasesDetail(poNumber), __PurchaseOrderBusiness.GetPurchaseOrderByPo(poNumber), _siteBusiness.GetInfoSite());
                (report.Bands["DetailReport"] as DetailReportBand).DataMember = "xrTableRowFormat";
                return PartialView("~/Views/ReportsViews/PurchasesOrder.cshtml", report);
            }
            if (status.purchase_status == 8)
            {
                PurchasesOrdered report = new PurchasesOrdered();
                report.Watermark.Text = "CANCELADO";
                report.Watermark.TextDirection = DevExpress.XtraPrinting.Drawing.DirectionMode.ForwardDiagonal;
                report.Watermark.Font = new Font(report.Watermark.Font.FontFamily, 60);
                report.Watermark.ForeColor = Color.Firebrick;
                report.Watermark.TextTransparency = 150;
                report.Watermark.ShowBehind = false;
                report.Watermark.PageRange = "1-99";

                (report.Bands["DetailReport"] as DetailReportBand).DataSource = report.printTable(__PurchaseOrderBusiness.NewGetAllPurchasesDetail(poNumber), __PurchaseOrderBusiness.GetPurchaseOrderByPo(poNumber), _siteBusiness.GetInfoSite());
                (report.Bands["DetailReport"] as DetailReportBand).DataMember = "xrTableRowFormat";
                return PartialView("~/Views/ReportsViews/PurchasesOrder.cshtml", report);
            }
            else
            {
                PurchasesOrder report = new PurchasesOrder();
                if (status.print_status == 2)
                {
                    report.Watermark.Text = "RE-IMPRESION";
                    report.Watermark.TextDirection = DevExpress.XtraPrinting.Drawing.DirectionMode.ForwardDiagonal;
                    report.Watermark.Font = new Font(report.Watermark.Font.FontFamily, 40);
                    report.Watermark.ForeColor = Color.DodgerBlue;
                    report.Watermark.TextTransparency = 150;
                    report.Watermark.ShowBehind = false;
                    report.Watermark.PageRange = "1-99";
                }
                (report.Bands["DetailReport"] as DetailReportBand).DataSource = report.printTable(__PurchaseOrderBusiness.NewGetAllPurchasesDetail(poNumber), __PurchaseOrderBusiness.GetPurchaseOrderByPo(poNumber), _siteBusiness.GetInfoSite());
                (report.Bands["DetailReport"] as DetailReportBand).DataMember = "xrTableRowFormat";
                return PartialView("~/Views/ReportsViews/PurchasesOrder.cshtml", report);
            }
        }
    }
}