﻿using App.BLL;
using App.BLL.Order;
using App.BLL.PurchaseOrder;
using App.BLL.Site;
using App.BLL.Supplier;
using App.Common;
using App.Entities;
using App.Entities.ViewModels.DamagedsGoods;
using DevExpress.XtraReports.UI;
using FloridoERPTX.Reports;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace FloridoERPTX.Controllers.Order
{
    public class OrdersController : Controller
    {
        private readonly OrderBusiness _OrderBusiness;
        private readonly PurchaseOrderBusiness _purchaseOrderBusiness;
        private readonly SupplierBusiness _SupplierBussines;
        private readonly OrderListBusiness _OrderListBusiness;
        private readonly Common common;
        private readonly SendMail information;
        private readonly SiteConfigBusiness _siteConfigBusiness;
        private readonly SiteBusiness _siteBusiness;
        private readonly UserMasterBusiness _userMasterBusiness;

        public OrdersController()
        {
            _OrderBusiness = new OrderBusiness();
            _purchaseOrderBusiness = new PurchaseOrderBusiness();
            _OrderListBusiness = new OrderListBusiness();
            _SupplierBussines = new SupplierBusiness();
            common = new Common();
            information = new SendMail();
            _siteConfigBusiness = new SiteConfigBusiness();
            _siteBusiness = new SiteBusiness();
            _userMasterBusiness = new UserMasterBusiness();
        }

        public ActionResult ActionOrder(int Folio)
        {
            return Json(_OrderBusiness.Get(Folio), JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult ActionCreate(ORDER_HEAD order)
        {
            if (Session["User"] != null)
            {
                order.cuser = Session["User"].ToString();
                order.cdate = DateTime.Now;
                order.program_id = "ORDE001.cshtml";
                int Folio = _OrderBusiness.Post(order);
                if (Folio > 0)
                    return Json(Folio, JsonRequestBehavior.AllowGet);

                return Json(0, JsonRequestBehavior.AllowGet);
            }
            return Json("SF", JsonRequestBehavior.AllowGet);
        }

        /*Nuevo*/
        [HttpPost]
        public ActionResult ActionNewOrderHeaderAndOrderList(ORDER_HEAD orderHead, List<ORDER_LIST> OrderList , string Currency)
        {
            if (Session["User"] != null)
            {
                orderHead.cuser = Session["User"].ToString();
                orderHead.cdate = DateTime.Now;
                orderHead.program_id = "ORDE001.cshtml";
                int Folio = _OrderBusiness.Post(orderHead);
                orderHead.folio = Folio;
                if (Folio > 0)
                {
                    if (_OrderListBusiness.NewGenerateOrderList(OrderList, Folio, Session["User"].ToString()))
                    {
                        try
                        {
                            //Todo bien
                            SupplierOrderedCurrency report = new SupplierOrderedCurrency();
                            (report.Bands["DetailReport"] as DetailReportBand).DataSource = report.printTable(_OrderBusiness.Get(Folio), _OrderListBusiness.GetAll(Folio),Currency);
                            (report.Bands["DetailReport"] as DetailReportBand).DataMember = "xrTableRowFormat";

                            var stream = new MemoryStream();
                            report.ExportToPdf(stream);
                            report.Dispose();
                            return Json(new { FolioOrder = Folio, Number = 1000, responseText = Convert.ToBase64String(stream.ToArray()) }, JsonRequestBehavior.AllowGet);
                        }
                        catch (Exception)
                        {
                            //Problema con el formato pedido generado
                            return Json(new { FolioOrder = Folio, Number = 1004 }, JsonRequestBehavior.AllowGet);
                        }
                    }
                    //Problema al guardar datos
                    if (_OrderBusiness.RemoveOrderHeader(orderHead))
                        return Json(new { FolioOrder = Folio, Number = 1002 }, JsonRequestBehavior.AllowGet);
                    //Problema al borrar una orden de compra
                    else
                        return Json(new { FolioOrder = 0, Number = 1003 });
                }
                //No Hay Folio
                return Json(new { FolioOrder = 0, Number = 1001 }, JsonRequestBehavior.AllowGet);
            }
            return Json("SF", JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult ActionNewPurchaseOrder(List<App.DAL.Order.OrderListDatesViewModel> OrderList, int Folio, DateTime? Date, string Comment, string Currency, int Supplier, string Site, int Option)
        {
            if (Session["User"] != null)
            {
                if (Option == 1)
                {
                    if (_OrderBusiness.UpdateOrderAndOrderList(Folio, Date, Comment, Session["User"].ToString(), Option))
                    {
                        if (OrderList != null)
                        {
                            if (OrderList.Count() > 0)
                            {
                                // Guardado de manera correcto
                                if (_OrderListBusiness.Put(OrderList, Session["User"].ToString()))
                                    return Json(new { Error = 1000 }, JsonRequestBehavior.AllowGet);
                                // Error al actualizar los datos
                                else
                                    return Json(new { Error = 1002 }, JsonRequestBehavior.AllowGet);
                            }
                        }
                        // se guardo de manera correcta
                        return Json(new { Error = 1000 }, JsonRequestBehavior.AllowGet);
                    }
                    // Problema al guardar los datos dela lista
                    else
                        return Json(new { Error = 1001 }, JsonRequestBehavior.AllowGet);
                }
                else if (Option == 2)
                {
                    if (_OrderListBusiness.Put(OrderList, Session["User"].ToString()))
                    {
                       var StoreProcedurePurchaseOrder = _OrderBusiness.Update(Folio, Date.Value, Comment, Currency, Session["User"].ToString(), Site, Supplier);
                        if (StoreProcedurePurchaseOrder.ReturnValue == 0)
                        {
                            string PurchaseNo = StoreProcedurePurchaseOrder.Document;
                            string user = Session["User"].ToString();
                            //por aqui paso pasillas y lo cambio
                            var purchases = _purchaseOrderBusiness.EmailPurchaseOrder(Supplier, StoreProcedurePurchaseOrder.Document);

                            var SupplierName = _SupplierBussines.GetNameSupplier(Supplier);
                            var SiteInfo = _siteConfigBusiness.GetSiteCodeAddress();
                            var UuserEmail = _userMasterBusiness.GetEmailByUserName(user);
                            //Todo salio perfecto                            
                            Task task = new Task(() =>
                            {
                                information.folio = purchases.Item1[0].PurchaseNo;
                                information.store = purchases.Item1[0].SiteCode;
                                information.date = purchases.Item1[0].PurchaseDate.Date.ToShortDateString();
                                information.currecy = purchases.Item1[0].Currency;
                                information.subject = SiteInfo.SiteName + " - Orden de compra para " + SupplierName;
                                information.from = SiteInfo.SiteName;
                                information.email = (!string.IsNullOrWhiteSpace(purchases.Item2) ? purchases.Item2 + "," : "") + (!string.IsNullOrWhiteSpace(UuserEmail) ? UuserEmail : "");

                                PurchasesOrder report = new PurchasesOrder();
                                (report.Bands["DetailReport"] as DetailReportBand).DataSource = report.printTable(_purchaseOrderBusiness.GetAllPurchasesDetail(PurchaseNo), _purchaseOrderBusiness.GetPurchaseOrderByPo(PurchaseNo), _siteBusiness.GetInfoSite());
                                (report.Bands["DetailReport"] as DetailReportBand).DataMember = "xrTableRowFormat";
                                Stream stream = new MemoryStream();
                                Stream streamExcel = new MemoryStream();
                                report.ExportToPdf(stream);
                                report.ExportToXlsx(streamExcel);

                                EmailDamagedGoodsModel Pdf = new EmailDamagedGoodsModel() { RMA = StoreProcedurePurchaseOrder.Document.ToString(), Rerpote = stream, ReporteExcel = streamExcel };
                                common.MailMessageOrderSingle(information, Pdf, SiteInfo.SiteName);
                            });
                            task.Start();
                            return Json(new { Error = 2000, Document = StoreProcedurePurchaseOrder.Document }, JsonRequestBehavior.AllowGet);
                        }
                        //Problema del store procedure
                        else
                            return Json(new { Error = 2002, Store = StoreProcedurePurchaseOrder.ReturnValue }, JsonRequestBehavior.AllowGet);
                    }
                    // Error al actualizar los datos
                    else
                        return Json(new { Error = 2003 }, JsonRequestBehavior.AllowGet);
                }
                // Problema con js al mandar la opcion
                return Json(new { Error = 1008 }, JsonRequestBehavior.AllowGet);
            }
            return Json("SF", JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult ActionUpdateStatusOrder(int Folio, DateTime Date, string Comment, string Currency, int Supplier, string Site)
        {
            var NumberPurchaseOrder = "SF";
            if (Session["User"] != null)
            {
                var NumberPurchaseOrder1 = _OrderBusiness.Update(Folio, Date, Comment, Currency, Session["User"].ToString(), Site, Supplier);
                var purchases = _purchaseOrderBusiness.SendEmailSupplier2(Supplier, NumberPurchaseOrder1.Document);
                var site = _siteBusiness.GetInfoSite();
                var SupplierName = _SupplierBussines.GetNameSupplier(Supplier);
                if (purchases.Item1.Count > 0 && !string.IsNullOrWhiteSpace(purchases.Item2.Trim()))
                {
                    Task task = new Task(() =>
                    {
                        information.folio = purchases.Item1[0].PurchaseNo;
                        information.store = purchases.Item1[0].SiteCode;
                        information.date = Convert.ToString(purchases.Item1[0].PurchaseDate.Date);
                        information.currecy = purchases.Item1[0].Currency;
                        information.subject = site.site_name + " - Orden de compra para " + SupplierName;
                        information.from = site.site_name;
                        information.email = purchases.Item2;

                        //Antiguo reporte
                        //OrderReportEmail report = new OrderReportEmail();
                        //report.DataSource = report.printTable(purchases.Item1, purchases.Item3, Currency, Date);
                        PurchasesOrder report = new PurchasesOrder();
                        (report.Bands["DetailReport"] as DetailReportBand).DataSource = report.printTable(_purchaseOrderBusiness.GetAllPurchasesDetail(purchases.Item1[0].PurchaseNo), _purchaseOrderBusiness.GetPurchaseOrderByPo(purchases.Item1[0].PurchaseNo), _siteBusiness.GetInfoSite());
                        (report.Bands["DetailReport"] as DetailReportBand).DataMember = "xrTableRowFormat";

                        Stream stream = new MemoryStream();
                        Stream streamExcel = new MemoryStream();
                        report.ExportToPdf(stream);
                        report.ExportToXlsx(streamExcel);

                        EmailDamagedGoodsModel Pdf = new EmailDamagedGoodsModel() { RMA = NumberPurchaseOrder1.Document.ToString(), Rerpote = stream, ReporteExcel = streamExcel };
                        common.MailMessageOrderSingle(information, Pdf, site.site_name);
                    });
                    task.Start();
                    return Json(new { responseText = NumberPurchaseOrder1, Error = false }, JsonRequestBehavior.AllowGet);
                }

                return Json(new { responseText = NumberPurchaseOrder1, TypeError = 0, TypeErrorPurchase = NumberPurchaseOrder1.ReturnValue, Error = true }, JsonRequestBehavior.AllowGet);
            }
            return Json(new { responseText1 = NumberPurchaseOrder }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult ActionUpdateOrder(int Folio, DateTime? Date, string Comment)
        {
            if (Session["User"] != null)
                return Json(_OrderBusiness.UpdateOrder(Folio, Date, Comment, Session["User"].ToString()), JsonRequestBehavior.AllowGet);

            return Json("SF", JsonRequestBehavior.AllowGet);
        }

        public ActionResult ActionCancelStatusOrder(int Folio)
        {
            if (Session["User"] != null)
                return Json(_OrderBusiness.Cancel(Folio, Session["User"].ToString()), JsonRequestBehavior.AllowGet);

            return Json("SF", JsonRequestBehavior.AllowGet);
        }

        public ActionResult ActionOrderStatus(int Folio)
        {
            if (Session["User"] != null)
                return Json(_OrderBusiness.Status(Folio), JsonRequestBehavior.AllowGet);

            return Json("SF", JsonRequestBehavior.AllowGet);
        }

        public ActionResult ActionOrderFilter()
        {
            return Json(_OrderBusiness.Filter(), JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public ActionResult ActionGetCurrencyPO(string folio)
        {
            if (Session["User"] != null)
                return Json(_purchaseOrderBusiness.GetCurrencyPO(folio), JsonRequestBehavior.AllowGet);

            return Json("SF", JsonRequestBehavior.AllowGet);
        }
    }
}