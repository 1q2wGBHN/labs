﻿using App.BLL.Invoices;
using System.Collections.Generic;
using System.Web.Mvc;
using App.BLL.Customer;
using App.BLL.Lookups;
using App.Entities.ViewModels.Payments;
using System.Linq;
using App.BLL.Payments;
using static App.Common.Common;
using App.Entities;
using FloridoERPTX.ViewModels.Invoices;
using System;
using App.BLL.Sales;
using FloridoERPTX.CommonMethods;
using App.BLL.DigitalTaxStamp;
using static App.Entities.ViewModels.Common.Constants;
using App.Entities.ViewModels.Common;
using FloridoERPTX.Timbrado;
using App.BLL.SysLog;
using App.Entities.ViewModels.TransactionsCustomers;
using App.BLL.Currency;
using App.BLL.Site;
using FloridoERPTX.Reports;
using DevExpress.XtraReports.UI;
using Ionic.Zip;
using System.Text;
using System.IO;
using System.Globalization;

namespace FloridoERPTX.Controllers.Payments
{
    public class PaymentsController : Controller
    {
        private InvoiceBusiness _InvoiceBusiness;
        private CustomerBusiness _CustomerBusiness;
        private ReferenceNumberBusiness _ReferenceNumberBusiness;
        private PaymentMethodBusiness _PaymentMethodBusiness;
        private PaymentsBusiness _PaymentsBusiness;
        private PaymentsPMBusiness _PaymentsPMBusiness;
        private PaymentsDetailsBusiness _PaymentsDetailsBusiness;
        private SalesBusiness _SalesBusiness;
        private StampDocument _StampDocument;
        private DigitalTaxStampBusiness _DigitalTaxStampBusiness;
        private SysLogBusiness _SysLogBusiness;
        private CurrencyBusiness _CurrencyBusiness;
        private SiteConfigBusiness _SiteConfigBusiness;
        private CustomerLookup _CustomerLookup;
        private BankBusiness _BankBusiness;

        public PaymentsController()
        {
            _InvoiceBusiness = new InvoiceBusiness();
            _CustomerBusiness = new CustomerBusiness();
            _ReferenceNumberBusiness = new ReferenceNumberBusiness();
            _PaymentMethodBusiness = new PaymentMethodBusiness();
            _PaymentsBusiness = new PaymentsBusiness();
            _PaymentsPMBusiness = new PaymentsPMBusiness();
            _PaymentsDetailsBusiness = new PaymentsDetailsBusiness();
            _SalesBusiness = new SalesBusiness();
            _StampDocument = new StampDocument();
            _DigitalTaxStampBusiness = new DigitalTaxStampBusiness();
            _SysLogBusiness = new SysLogBusiness();
            _CurrencyBusiness = new CurrencyBusiness();
            _SiteConfigBusiness = new SiteConfigBusiness();
            _CustomerLookup = new CustomerLookup();
            _BankBusiness = new BankBusiness();
        }

        public ActionResult PAMT001()
        {
            ViewBag.CustomerCode = _CustomerLookup.FillLookupCustomer(CustomersLookup.ByCreditInvoices);
            return View();
        }

        [HttpPost]
        public ActionResult PAMT001(PaymentIndex Model)
        {
            var PaymentModel = new Payment();
            PaymentModel = PopulateModel(Model);
            ViewBag.Bank = new SelectList(_BankBusiness.BanksList(), "id", "name");
            return View("PAMT002", PaymentModel);
        }

        [HttpPost]
        public ActionResult ActionCreatePayment(Payment Model)
        {
            Model.User = Session["User"].ToString();
            var ResponseStamp = new Response();
            if (Session["User"] != null)
            {                
                if (_PaymentsBusiness.CreatePayment(Model))
                    if (_PaymentsPMBusiness.CreatePaymentPM(Model))
                    {
                        ResponseStamp = _StampDocument.DocumentStamp(Model, false);
                        if (ResponseStamp.Status)
                        {
                            Model.IsStamped = true;
                            var newInstance = _PaymentsBusiness.GetPaymentByNumber(Model.Number);
                            Model.PdfEncoded = newInstance.pdf;
                            return Json(new { success = true, Json = Model, responseText = "Pago procesado correctamente" }, JsonRequestBehavior.AllowGet);
                        }
                        else if (_SysLogBusiness.CreateLogStampDocuments(Model, ResponseStamp.Message, ResponseStamp.ErrorNumber, false, false))
                        {
                            Model.IsStamped = false;
                            return Json(new { success = true, Json = Model, responseText = "Documento creado pero no timbrado." }, JsonRequestBehavior.AllowGet);
                        }
                        else
                            goto ErrorLabel;
                    }
            }
            else
                return Json(new { success = false, responseText = "Terminó tu sesión" }, JsonRequestBehavior.AllowGet);

            ErrorLabel:
            return Json(new { success = false, responseText = "Ocurrió un error, contacte a sistemas!" }, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public ActionResult ActionGetInvoicesByCurrency(int PaymentMethodId, string CustomerCode)
        {
            var Payment = _PaymentMethodBusiness.GetPaymentMethodById(PaymentMethodId);
            var Invoices = _InvoiceBusiness.InvoicesCreditByCustomer(CustomerCode, Payment.currency);
            UpdateInvoices Item = new UpdateInvoices();
            Item.Invoices = InvoicesCreditList(Invoices);
            Item.Balance = Invoices.Count > 0 ? Invoices.Sum(c => c.invoice_balance.Value) : 0;
            Item.BalanceDisplay = Invoices.Count > 0 ? string.Format("(${0})", Invoices.Sum(c => c.invoice_balance.Value)) : "($0)";
            Item.MinDate = Invoices.Count > 0 ? Invoices.Min(c => c.invoice_date).ToString(DateFormat.INT_DATE) : string.Empty;
            Item.Currency = Payment.currency;
            return Json(new { success = true, Json = Item, responseText = "Facturas actualizadas" }, JsonRequestBehavior.AllowGet);
        }               

        List<SelectListItem> InvoicesCreditList(List<INVOICES> Invoices)
        {            
            var InvoicesList = new List<SelectListItem>();
            if (Invoices != null)
            {
                foreach (var Item in Invoices)
                {
                    var Element = new SelectListItem();
                    Element.Text = string.Format("CFD {0}{1} - ${2}", Item.invoice_serie, Item.cfdi_id, Item.invoice_balance.Value);
                    Element.Value = string.Format("{0}{1}", Item.invoice_serie, Item.invoice_no.ToString());                    
                    InvoicesList.Add(Element);
                }
            }

            return InvoicesList;
        }

        #region Cancel Payments

        //Fill Lookup: only client that has payments
        public ActionResult PAMT003()
        {
            ViewBag.CustomerCode = _CustomerLookup.FillLookupCustomer(CustomersLookup.ByPaymentInvoices);
            return View();
        }

        [HttpGet] //Get payments per client selected.
        public ActionResult ActionGetPaymentByCustomer(string CustomerCode)
        {
            var Payments = _PaymentsBusiness.PaymentsByCustomer(CustomerCode);
            return Json(new { success = true, Json = Payments, responseText = "Pagos de Clientes" }, JsonRequestBehavior.AllowGet);
        }

        [HttpGet] //Get Invoice paid per payment(receipt number)
        public ActionResult ActionGetInvoicesPaidByPayment(long PaymentId)
        {
            var Payments = _PaymentsDetailsBusiness.PaymentsDetailsByPaymentMethodId(PaymentId);
            return Json(new { success = true, Json = Payments, responseText = "Pagos de facturas" }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost] //Process to cancel payment
        public ActionResult ActionCancelPayment(long PaymentId)
        {
            if (Session["User"] != null)
            {
                var PaymentPM = _PaymentsPMBusiness.GetPaymentMethodById(PaymentId);
                var Payment = _PaymentsBusiness.GetPaymentByNumber(PaymentPM.payment_no);
                var StampInfo = _DigitalTaxStampBusiness.GetStampInfo(Payment.payment_no, Payment.payment_serie, VoucherType.PAYMENTS);
                var RequestCancel = new CancelModel();
                bool PreviousCanceled = false;

                if (StampInfo != null)
                    PreviousCanceled = StampInfo.cancelled_request.HasValue && StampInfo.cancelled_accepted.HasValue && StampInfo.cancelled_request.Value && StampInfo.cancelled_accepted.Value;

                if (PreviousCanceled) //Used for payment substitution.
                    goto Success;

                RequestCancel.Number = Payment.payment_no.ToString();
                RequestCancel.Serie = Payment.payment_serie;
                RequestCancel.Uuid = StampInfo.UUID;
                RequestCancel.DocumentType = VoucherType.PAYMENTS;
                RequestCancel.User = Session["User"].ToString();

                var ResponseStamp = new Response();
                //Call web service to cancel stamp                 
                ResponseStamp = _StampDocument.DocumentStamp(RequestCancel, true); //IsCancel = true                

                if (ResponseStamp.Status || (StampInfo.cancelled_request.HasValue && StampInfo.cancelled_request.Value) || (StampInfo == null && Payment.payment_status))
                {
                    var SysLog = new SYS_LOG();
                    DateTime dt = DateTime.Now;
                    SysLog.Action = "CANCELÓ";
                    SysLog.Date = DateTime.Now.Date;
                    SysLog.Description = "CANCELACIÓN DE PAGO";
                    SysLog.idAfect = Payment.payment_no;
                    SysLog.TableName = "PAYMENTS";
                    SysLog.Menu = "VENTAS";
                    SysLog.time = dt.ToString("HH:mm");
                    SysLog.uuser = Session["User"].ToString();

                    Payment.udate = DateTime.Now;
                    Payment.uuser = Session["User"].ToString();
                    Payment.payment_status = false;
                    Payment.program_id = "PAMT003.cshtml";

                    if (_PaymentsBusiness.UpdatePayment(Payment))
                    {
                        foreach (var Item in PaymentPM.PAYMENTS_DETAIL)
                        {
                            var Invoice = _InvoiceBusiness.GetInvoiceByNumber(Item.invoice_no);
                            Invoice.invoice_balance += Item.amount;
                            Invoice.uuser = Session["User"].ToString();
                            Invoice.udate = DateTime.Now;
                            if (!_InvoiceBusiness.UpdateInvoice(Invoice, SysLog))
                            {
                                Payment.payment_status = true;
                                _PaymentsBusiness.UpdatePayment(Payment);
                                break;
                            }
                        }
                        goto Success;
                    }
                }
                else //Create error log.
                    _SysLogBusiness.CreateLogStampDocuments(RequestCancel, ResponseStamp.Message, ResponseStamp.ErrorNumber, true, false);

                return Json(new { success = false, Json = PaymentId, responseText = "Ocurrió un error, contacte a sistemas" }, JsonRequestBehavior.AllowGet);

                Success:
                return Json(new { success = true, Json = PaymentId, responseText = string.Format("Pago {0}{1} cancelada correctamente.", Payment.payment_serie, Payment.payment_no) }, JsonRequestBehavior.AllowGet);

            }
            return Json(new { success = false, Json = PaymentId, responseText = "Terminó tu sesión" }, JsonRequestBehavior.AllowGet);
        }
        #endregion

        public ActionResult PAMT004()
        {
            ViewBag.CustomerCode = _CustomerLookup.FillLookupCustomer(CustomersLookup.ByPaymentInvoices);         
            return View();            
        }

        [HttpPost]
        public ActionResult PAMT004(PaymentIndex Model)
        {
            var PaymentModel = new Payment();
            PaymentModel = PopulateModel(Model);
            ViewBag.Bank = new SelectList(_BankBusiness.BanksList(), "id", "name");
            return View("PAMT002", PaymentModel);         
        }           
        
        private Payment PopulateModel(PaymentIndex Model)
        {
            var PaymentModel = new Payment();

            var Reference = _ReferenceNumberBusiness.GetReferenceNumber(VoucherType.PAYMENTS);

            while(_PaymentsBusiness.PaymentExist(Reference.number))
            {
                _PaymentsBusiness.SprocUpdatePaymentNumber();
                var n = _ReferenceNumberBusiness.GetReferenceNumber(VoucherType.PAYMENTS);
                Reference = n;
            }

            var Payment = _PaymentsPMBusiness.GetPaymentMethodById(Model.PaymentPMId);
            var Invoices = Model.IsPayment ? _InvoiceBusiness.InvoicesCreditByCustomer(Model.CustomerCode, CURRENCY_DEFAULT) : _InvoiceBusiness.InvoicesCreditByCustomer(Model.CustomerCode, Payment.PAYMENTS.cur_code);
            bool HasInvoices = Invoices != null && Invoices.Count > 0;

            PaymentModel.Customer = _CustomerBusiness.GetCustomer(Model.CustomerCode);
            PaymentModel.BalanceDisplay = HasInvoices ? string.Format("(${0})", Invoices.Sum(c => c.invoice_balance.Value)) : string.Format("${0}", 0);
            PaymentModel.Balance = HasInvoices ? Invoices.Sum(c => c.invoice_balance.Value) : 0;
            PaymentModel.MinInvoiceDate = HasInvoices ? Invoices.Min(c => c.invoice_date).ToString(DateFormat.INT_DATE) : DateTime.Today.ToString(DateFormat.INT_DATE);
            PaymentModel.PaymentMethod = !Model.IsPayment ? Payment.payment_method_id : PaymentMethods.ELECTRONIC_TRANSFER;
            PaymentModel.Currency = !Model.IsPayment ? Payment.PAYMENTS.cur_code : CURRENCY_DEFAULT; //Currency value for 'Transferencia Electrónica'
            PaymentModel.PaymentMethods = _PaymentMethodBusiness.PaymentMethodModelList();
            PaymentModel.LastPayment = _PaymentsBusiness.GetDateLastPayment(Model.CustomerCode);
            PaymentModel.LastBuy = _SalesBusiness.GetDateLastSale(Model.CustomerCode);

            ViewBag.PaymentMethod = new SelectList(_PaymentMethodBusiness.PaymentMethodList(), "pm_id", "pm_name");
            ViewBag.Invoice = InvoicesCreditList(Invoices);  
                                                                
            if (!Model.IsPayment)
            {
                PaymentModel.TotalPayment = Payment.amount.Value;
                PaymentModel.TotalInvoices = Payment.amount.Value;
                PaymentModel.PreviousReceipt = string.Format("{0}{1}", Payment.payment_serie, Payment.payment_no);
                PaymentModel.PreviousPaymentNumber = Payment.payment_no;
                PaymentModel.IsPayment = false;
                PaymentModel.PaymentPMId = Model.PaymentPMId;
                PaymentModel.Invoices = _PaymentsDetailsBusiness.PaymentsDetailsByPaymentMethodId(Model.PaymentPMId);
                PaymentModel.Payments = _PaymentsPMBusiness.GetPaymentMethodDetails(Model.PaymentPMId);
                PaymentModel.AmountPayment = Payment.amount.Value;                
            }

            if (Reference == null)
                PaymentModel.HasSerieAndNumber = false;
            else
            {
                PaymentModel.Number = Reference.number;
                PaymentModel.Serie = Reference.serie;
            }

            return PaymentModel;
        }        
        
        public ActionResult PAMT005()
        {
            var Model = new TransactionsCustomersReport();
            Lookups();
            return View(Model);
        }

        [HttpPost]
        public ActionResult PAMT005(TransactionsCustomersReport Model)
        {            
            Model.PaymentsReport = _PaymentsBusiness.PaymentCustomerReport(Model, false);
            Model.PaymentsReport.ForEach(x => x.xml = string.Empty);
            Model.IsFromMenu = false;
            Model.SiteName = _SiteConfigBusiness.GetSiteCodeName();
            Lookups();
            return View(Model);
        }

        private void Lookups()
        {
            ViewBag.CustomerCode = _CustomerLookup.FillLookupCustomer(CustomersLookup.ByPaymentInvoices);            
            ViewBag.Currency = new SelectList(_CurrencyBusiness.CurrencyList(), "cur_code", "cur_name");            
        }

        [HttpPost]
        public ActionResult ActionGetCustomerPaymentReport(TransactionsCustomersReport Model)
        {
            PaymentCustomerReport report = new PaymentCustomerReport();

            (report.Bands["DetailReport"] as DetailReportBand).DataSource = report.printTable(Model);
            (report.Bands["DetailReport"] as DetailReportBand).DataMember = "xrTableRowFormat";
            return PartialView("~/Views/ReportsViews/ReportView.cshtml", report);
        }

        public ActionResult ActionGetXMLPayments(TransactionsCustomersReport Model)
         {
            var List = _PaymentsBusiness.PaymentCustomerReport(Model, true);
            using (ZipFile zip = new ZipFile())
            {
                foreach (var item in List.Distinct().Where(c => !string.IsNullOrEmpty(c.xml)))
                {
                    byte[] xmlFile = null;
                    string fileName = string.Format("{0}{1}.xml", item.Serie, item.Number);
                    xmlFile = Encoding.UTF8.GetBytes(item.xml);
                    zip.AddEntry(fileName, xmlFile);
                }

                if (zip.Count > 0)
                {
                    DateTime StartDate = DateTime.ParseExact(Model.StartDate, DateFormat.INT_DATE, CultureInfo.InvariantCulture);
                    DateTime EndDate = DateTime.ParseExact(Model.EndDate, DateFormat.INT_DATE, CultureInfo.InvariantCulture);
                    string zipName = string.Format("Abonos del {0} al {1}.zip", StartDate.ToString("d", CultureInfo.CreateSpecificCulture("de-DE")), EndDate.ToString("d", CultureInfo.CreateSpecificCulture("de-DE")));
                    using (MemoryStream output = new MemoryStream())
                    {
                        zip.Save(output);
                        return File(output.ToArray(), "application/zip", zipName);
                    }
                }
            }
            return null;
        }
    }
}
