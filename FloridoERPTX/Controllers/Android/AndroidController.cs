﻿using App.BLL;
using App.BLL.Barcodes;
using App.BLL.BlindCount;
using App.BLL.Configuration;
using App.BLL.EmptySpace;
using App.BLL.EntryFree;
using App.BLL.Inventory;
using App.BLL.Item;
using App.BLL.MaCode;
using App.BLL.PurchaseBaseCost;
using App.BLL.PurchaseOrder;
using App.BLL.RawMaterial;
using App.BLL.Rma;
using App.BLL.ScrapControl;
using App.BLL.Site;
using App.BLL.StorageLocation;
using App.BLL.Supplier;
using App.BLL.TokenPhone;
using App.BLL.Transfer;
using App.Common;
using App.DAL.Site;
using App.Entities.ViewModels.Item;
using FloridoERPTX.Reports;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using App.Entities.ViewModels.DamagedsGoods;
using App.DAL.Supplier;
using App.BLL.CostCenter;
using App.DAL.MaClass;
using App.BLL.DamagedsGoods;
using App.BLL.Sales;
using App.Entities.ViewModels.Android;

namespace FloridoERPTX.Controllers.Android
{
    public class AndroidController : Controller
    {
        private readonly DamagedsGoodsBusiness _RmaHeadBusiness;
        private readonly UserMasterBusiness _userMasterBusiness;
        private readonly SysLogConnectionBusiness _sysLogConnection;
        private readonly SysPageMasterBusiness _PageMasterRepo;
        private readonly PurchaseOrderBusiness _purchaseOrderBusiness;
        private readonly BarcodesBusiness _barcodesBusiness;
        private readonly BlindCountBusiness _blindCountBusiness;
        private readonly ItemSupplierBusiness _itemSupplierBusiness;
        private readonly PurchaseOrderItemBusiness _purchaseOrderItemBusiness;
        private readonly SupplierBusiness _supplierBusiness;
        private readonly SiteBusiness _siteBusiness;
        private readonly ItemBusiness _itemBusiness;
        private readonly PurchaseBaseCostBusiness _purchaseBaseCostBusiness;
        private readonly EntryFreeListBusiness _EntryFreeListBusiness;
        private readonly EntryFreeHeadBusiness _EntryFreeHeadBusiness;
        private readonly SiteConfigRepository _siteConfigRepository;
        private readonly ScrapControlBusiness _scrapControlBusiness;
        private readonly ItemStockBusiness _itemStockBusiness;
        private readonly MaCodeBusiness _maCodeBusiness;
        private readonly MaSupplierContactBusiness _MaSupplierContact;
        private readonly DamagedsGoodsBusiness _damageGoodBusiness;
        private readonly TransferHeaderBusiness _transferHeaderBusiness;
        private readonly TransferHeaderInBusiness _transHeaderInBusiness;
        private readonly RawMaterialBusiness _rawMaterialBusiness;
        private readonly TransferPalletInfoBusiness _TransferPalletInfoBussines;
        private readonly StorageLocationBusiness _storageLocationBusiness;
        private readonly TokenPhoneBusiness _tokenPhoneBusiness;
        private readonly TokenPhoneDetailBusiness _tokenPhoneDetailBusiness;
        private readonly EmptySpaceBusiness _emptySpaceBusiness;
        private readonly EmptySpaceDetailBusiness _emptySpaceDetailBusiness;
        private readonly PlanogramLocationBusiness _planogramLocationBusiness;
        private readonly InventoryDetailBusiness _InventoryDetailBusiness;
        private readonly InventoryPalletsBusiness _InventoryPalletsBusiness;
        private readonly SysRoleMasterBusiness _sysRoleMasterBusiness;
        private readonly DamagedsGoodsItemBusiness _DamagedsGoodsItemBusiness;
        private readonly MaClassRepository _maClassRepository;
        private readonly SendMail information;
        private readonly Common common;
        private readonly Notification _notifi;
        private readonly SalesPriceChangeBusiness _salesPriceChangeBusiness;
        App_Start.Authentication.Profile Perf = new App_Start.Authentication.Profile();
        private readonly SupplierBusiness _SupplierBusiness;
        private MaSupplierContactRepository _MaSupplierContactRepo;
        private readonly CostCenterBusiness _costCenterBusiness;
        private readonly InventoryHeaderBusiness _inventoryHeaderBusiness;
        private readonly DamagedsGoodsRestrictedBusiness _damagedsGoodsRestrictedBusiness;
        private readonly InventoryLocationBusiness _inventoryLocationBusiness;

        public AndroidController()
        {
            _userMasterBusiness = new UserMasterBusiness();
            _sysLogConnection = new SysLogConnectionBusiness();
            _PageMasterRepo = new SysPageMasterBusiness();
            _purchaseOrderBusiness = new PurchaseOrderBusiness();
            _barcodesBusiness = new BarcodesBusiness();
            _blindCountBusiness = new BlindCountBusiness();
            _itemSupplierBusiness = new ItemSupplierBusiness();
            _purchaseOrderItemBusiness = new PurchaseOrderItemBusiness();
            _supplierBusiness = new SupplierBusiness();
            _siteBusiness = new SiteBusiness();
            _itemBusiness = new ItemBusiness();
            _purchaseBaseCostBusiness = new PurchaseBaseCostBusiness();
            _EntryFreeListBusiness = new EntryFreeListBusiness();
            _EntryFreeHeadBusiness = new EntryFreeHeadBusiness();
            _siteConfigRepository = new SiteConfigRepository();
            _scrapControlBusiness = new ScrapControlBusiness();
            _itemStockBusiness = new ItemStockBusiness();
            _maCodeBusiness = new MaCodeBusiness();
            _damageGoodBusiness = new DamagedsGoodsBusiness();
            _transferHeaderBusiness = new TransferHeaderBusiness();
            _transHeaderInBusiness = new TransferHeaderInBusiness();
            _rawMaterialBusiness = new RawMaterialBusiness();
            _TransferPalletInfoBussines = new TransferPalletInfoBusiness();
            _storageLocationBusiness = new StorageLocationBusiness();
            _tokenPhoneDetailBusiness = new TokenPhoneDetailBusiness();
            _tokenPhoneBusiness = new TokenPhoneBusiness();
            _emptySpaceBusiness = new EmptySpaceBusiness();
            _emptySpaceDetailBusiness = new EmptySpaceDetailBusiness();
            _planogramLocationBusiness = new PlanogramLocationBusiness();
            _InventoryDetailBusiness = new InventoryDetailBusiness();
            _InventoryPalletsBusiness = new InventoryPalletsBusiness();
            _sysRoleMasterBusiness = new SysRoleMasterBusiness();
            information = new SendMail();
            common = new Common();
            _notifi = new Notification();
            _RmaHeadBusiness = new DamagedsGoodsBusiness();
            _MaSupplierContact = new MaSupplierContactBusiness();
            _DamagedsGoodsItemBusiness = new DamagedsGoodsItemBusiness();
            _SupplierBusiness = new SupplierBusiness();
            _MaSupplierContactRepo = new MaSupplierContactRepository();
            _costCenterBusiness = new CostCenterBusiness();
            _maClassRepository = new MaClassRepository();
            _damagedsGoodsRestrictedBusiness = new DamagedsGoodsRestrictedBusiness();
            _salesPriceChangeBusiness = new SalesPriceChangeBusiness();
            _inventoryHeaderBusiness = new InventoryHeaderBusiness();
            _inventoryLocationBusiness = new InventoryLocationBusiness();
        }

        [HttpPost]
        public ActionResult LoginEnter(string u, string p)
        {
            var employee = _userMasterBusiness.GetUserMasterByUsernameOrEmpNo(u);
            if (employee != null)
            {
                if (!_userMasterBusiness.IsAccountPending(employee))
                {
                    if (!_userMasterBusiness.IsAccountBlocked(employee))
                    {
                        if (!_userMasterBusiness.IsAccountInactive(employee))
                        {

                            if (_userMasterBusiness.IsPasswordCorrect(employee, p))
                            {
                                try
                                {
                                    var sysLogConnection = _sysLogConnection.GetConnectionByEmployeeNumber(employee.emp_no);
                                    if (sysLogConnection == null)
                                    {
                                        sysLogConnection = new App.Entities.SYS_LOG_CONNECTION
                                        {
                                            cdate = DateTime.Now,
                                            connlog_date = DateTime.Now,
                                            conn_status = true,
                                            emp_no = employee.emp_no,
                                            conn_code = 1
                                        };
                                        _sysLogConnection.AddSysLogConnection(sysLogConnection);
                                    }
                                }
                                catch (Exception ex)
                                {
                                    var msg = ex.Message;
                                }
                                Session["User"] = employee.user_name;
                                App_Start.Authentication.UserCache.AddPaginasToCache(employee.emp_no, _PageMasterRepo.GetAllPagesOfUser(employee.emp_no), System.Web.HttpContext.Current);
                                FormsAuthenticationTicket authTicket = new FormsAuthenticationTicket(2, employee.user_name, DateTime.Now, DateTime.Now.AddMinutes(1440), false, employee.emp_no, FormsAuthentication.FormsCookiePath);
                                string crypTicket = FormsAuthentication.Encrypt(authTicket);
                                HttpCookie authCookie = new HttpCookie(FormsAuthentication.FormsCookieName, crypTicket);
                                Response.Cookies.Add(authCookie);
                                var levels = _PageMasterRepo.GetMenuAndroidByEmployeeNumber(employee.emp_no);
                                return Json(new { success = 1, numero = employee.emp_no, user_name = employee.user_name, password = p, name = employee.first_name + " " + employee.last_name, email = employee.email, nivel1 = levels.Item1, nivel2 = levels.Item2 }, JsonRequestBehavior.AllowGet);
                            }
                            else
                            {
                                _userMasterBusiness.UpdateFailedAttempts(employee);
                                return Json(new { success = 2, responseText = "Contraseña Incorrecta, intenta de nuevo..." }, JsonRequestBehavior.AllowGet);
                            }
                        }
                        else
                            return Json(new { success = 3, responseText = "Esta cuenta se encuentra Inactiva, contacta a sistemas." }, JsonRequestBehavior.AllowGet);
                    }
                    else
                    {
                        return Json(new { success = 3, responseText = "Esta cuenta se encuentra bloqueada, contacta a sistemas." }, JsonRequestBehavior.AllowGet);
                    }
                }
                else
                {
                    return Json(new { success = 4, responseText = "Esta cuenta aun no se activa, contacta a sistemas" }, JsonRequestBehavior.AllowGet);
                }
            }
            else
            {
                return Json(new { success = 5, responseText = "Este usuario no existe" }, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult GetAllOCInStatus1()
        {
            var ordenes = _purchaseOrderBusiness.GetAllOCInStatus1();
            if (ordenes != null)
            {
                var json = JsonConvert.SerializeObject(ordenes, Formatting.Indented, new JsonSerializerSettings { ReferenceLoopHandling = ReferenceLoopHandling.Ignore });
                return Json(new { success = true, Json = json }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(new { success = false, responseText = "Error" }, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult GetAllOCInStatus1BySupplier(String supplier)
        {
            var ordenes = _purchaseOrderBusiness.GetAllOCInStatus1BySupplier(supplier);
            if (ordenes != null)
            {
                var json = JsonConvert.SerializeObject(ordenes, Formatting.Indented, new JsonSerializerSettings { ReferenceLoopHandling = ReferenceLoopHandling.Ignore });
                return Json(new { success = true, Json = json }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(new { success = false, responseText = "Error" }, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult GetBarcodes(string purchase_no, string site)
        {
            var items = _blindCountBusiness.GetBlindCountListview(purchase_no, site);
            if (items != null || items.Count() > 0)
            {
                var json = JsonConvert.SerializeObject(items, Formatting.Indented, new JsonSerializerSettings { ReferenceLoopHandling = ReferenceLoopHandling.Ignore });
                return Json(new { success = true, Json = json }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(new { success = false }, JsonRequestBehavior.AllowGet);
            }

        }

        public ActionResult UpdatePurchaseOrder(string purchase_no, string userr, string b, string site, string cantidad, string proveedor)
        {
            var purchase = _purchaseOrderItemBusiness.GetInfoByPurchaseOrderBasic(purchase_no);
            //busca barcode
            var barcodes = _barcodesBusiness.GetAllDataByBarcode(b);
            //valida que exista
            if (barcodes != null)
            {
                //trae detalle de oc
                var detail = _purchaseOrderItemBusiness.GetPurchaseOrderItem(purchase_no);
                if (detail != null)
                {
                    //busca relacion de proveedor / producto
                    var itemSupplier = _itemSupplierBusiness.GetItemSupplierByItemAndSupplier(barcodes.part_number, Int32.Parse(proveedor));
                    //valida que exista
                    if (itemSupplier != null)
                    {
                        if (_itemSupplierBusiness.ValidatePartNumberBySite(barcodes.part_number, ""))
                        {
                            var base_cost = _purchaseBaseCostBusiness.getPurchaseBaseCostBySupplierPartNumber(itemSupplier.supplier_item_id);
                            if (base_cost.currency != purchase.Currency)
                            {
                                return Json(new { success = 13, responseText = "Error" }, JsonRequestBehavior.AllowGet);
                            }
                            //valida que exista producto en esa order
                            bool has = detail.Any(x => x.ParthNumber == barcodes.part_number);
                            if (has != true)
                            {
                                //Si es menor la cantidad la primera vez
                                if (Decimal.Parse(cantidad) < 0)
                                    return Json(new { success = 12 }, JsonRequestBehavior.AllowGet);
                                //La ultima posicion del escaneo
                                var position = _purchaseOrderItemBusiness.GetItemPositionByOc(purchase_no);
                                //Inserta el nuevo registro
                                var CreatePartNumber = _purchaseOrderItemBusiness.CreatePurchaseNoItem(purchase_no, site, Decimal.Parse(cantidad), barcodes.part_number, barcodes.part_barcode, itemSupplier.supplier_item_id, position, userr);
                                if (CreatePartNumber) //Se guardo correctamente
                                {
                                    CreatePartNumber = _purchaseOrderItemBusiness.UpdatePurchaseOrderItemTime(purchase_no, barcodes.part_number, userr);
                                    if (CreatePartNumber)
                                        return Json(new { success = 3, part_description = itemSupplier.part_description, part_number = itemSupplier.part_number, quantity = cantidad }, JsonRequestBehavior.AllowGet);
                                    else
                                        return Json(new { success = 4 }, JsonRequestBehavior.AllowGet);
                                }
                                else // Hubo error al guardar el nuuevo producto
                                    return Json(new { success = 4 }, JsonRequestBehavior.AllowGet);
                            }
                            else
                            {
                                var blindExists = _blindCountBusiness.GetBlindCountByOCItemBarcodeSite(purchase_no, itemSupplier.part_number, b, site);
                                decimal? quantitySum = 0;
                                if (blindExists != null)
                                    quantitySum = blindExists.quantity + Decimal.Parse(cantidad);

                                //cantidades negativas
                                if (quantitySum < 0)
                                    return Json(new { success = 11 }, JsonRequestBehavior.AllowGet);

                                //Actualizar
                                var UpdateePartNumber = _purchaseOrderItemBusiness.UpdatePurchaseNoItem(blindExists, purchase_no, blindExists.quantity ?? 0, Decimal.Parse(cantidad), barcodes.part_number, itemSupplier.supplier_item_id, userr);
                                if (UpdateePartNumber)
                                {
                                    var blind = _blindCountBusiness.UpdateBlindCount(blindExists, blindExists.quantity.ToString(), cantidad.ToString(), 0, userr);//Blind count

                                    if (blind) //Se actualizo correctamente
                                    {
                                        blind = _purchaseOrderItemBusiness.UpdatePurchaseOrderItemTime(blindExists.purchase_no, blindExists.part_number, userr);//purchase_order
                                        if (blind)
                                            return Json(new { success = 1, part_description = itemSupplier.part_description, part_number = itemSupplier.part_number, quantity = quantitySum }, JsonRequestBehavior.AllowGet);
                                        else
                                            return Json(new { success = 2 }, JsonRequestBehavior.AllowGet);
                                    }
                                    else // Hubo error al guardar al actualizar el productos
                                        return Json(new { success = 2 }, JsonRequestBehavior.AllowGet);
                                }
                                else // Hubo error al guardar al actualizar el productos
                                    return Json(new { success = 2 }, JsonRequestBehavior.AllowGet);
                            }
                        } else
                            return Json(new { success = 14 }, JsonRequestBehavior.AllowGet);
                    }
                    //entra si no existe relacion item/supplier
                    else
                    {
                        return Json(new { success = 5 }, JsonRequestBehavior.AllowGet);
                    }
                }
                //orden no tiene items
                else
                {
                    return Json(new { success = 8 }, JsonRequestBehavior.AllowGet);
                }
            }
            //entra si no existe barcode
            else
            {
                return Json(new { success = 6 }, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult SaveBarcodes(string purchase_no, string userr, string b, string site, string cantidad, string proveedor)
        {

            var purchase = _purchaseOrderItemBusiness.GetInfoByPurchaseOrderBasic(purchase_no);

            if (purchase.purchase_status_value == 5)
            {
                return UpdatePurchaseOrder(purchase_no, userr, b, site, cantidad, proveedor);
            }
            else
            {
                //busca barcode
                var barcodes = _barcodesBusiness.GetAllDataByBarcode(b);
                //valida que exista
                if (barcodes != null)
                {
                    //trae detalle de oc
                    var detail = _purchaseOrderItemBusiness.GetPurchaseOrderItem(purchase_no);
                    if (detail != null)
                    {
                        //valida que exista producto en esa order
                        bool has = detail.Any(x => x.ParthNumber == barcodes.part_number);
                        if (has)
                        {
                            //busca relacion de proveedor / producto
                            var itemSupplier = _itemSupplierBusiness.GetItemSupplierByItemAndSupplier(barcodes.part_number, Int32.Parse(proveedor));
                            //valida que exista
                            if (itemSupplier != null)
                            {
                                var base_cost = _purchaseBaseCostBusiness.getPurchaseBaseCostBySupplierPartNumber(itemSupplier.supplier_item_id);
                                if (base_cost.currency != purchase.Currency)
                                {
                                    return Json(new { success = 13, responseText = "Error" }, JsonRequestBehavior.AllowGet);
                                }
                                var purchaseOrderItem = _purchaseOrderItemBusiness.GetPurchaseOrderItemByOCAndPartNumber(purchase_no, itemSupplier.part_number);
                                //cantidad en item de orden
                                var quantityBD = purchaseOrderItem.QuantityOC;
                                //valida que cantidad no sea mayor a OC
                                if (Decimal.Parse(cantidad) <= quantityBD)
                                {
                                    //busca si ya se encuentra un registro de ese escaneo
                                    var blindExists = _blindCountBusiness.GetBlindCountByOCItemBarcodeSite(purchase_no, itemSupplier.part_number, b, site);
                                    //si existe actualiza
                                    if (blindExists != null)
                                    {
                                        var quantitySum = blindExists.quantity + Decimal.Parse(cantidad);
                                        //cantidad mayor a OC Item
                                        if (quantitySum < 0)
                                        {
                                            return Json(new { success = 11 }, JsonRequestBehavior.AllowGet);
                                        }
                                        else if (quantitySum > quantityBD)
                                        {
                                            return Json(new { success = 10 }, JsonRequestBehavior.AllowGet);
                                        }
                                        else
                                        {
                                            //actualiza cantidad
                                            var blind = _blindCountBusiness.UpdateBlindCount(blindExists, blindExists.quantity.ToString(), cantidad, 0, userr);
                                            if (blind)
                                            {
                                                blind = _purchaseOrderItemBusiness.UpdatePurchaseOrderItemTime(blindExists.purchase_no, blindExists.part_number, userr);
                                                if (blind)
                                                {
                                                    return Json(new { success = 1, part_description = itemSupplier.part_description, part_number = itemSupplier.part_number, quantity = blindExists.quantity.ToString() }, JsonRequestBehavior.AllowGet);
                                                } else
                                                    return Json(new { success = 2 }, JsonRequestBehavior.AllowGet);
                                            }
                                            else
                                                return Json(new { success = 2 }, JsonRequestBehavior.AllowGet);
                                        }
                                    }
                                    //entra si no existe registro blind_count
                                    else
                                    {
                                        //Si es menor la cantidad la primera vez
                                        if (Decimal.Parse(cantidad) < 0)
                                        {
                                            return Json(new { success = 12 }, JsonRequestBehavior.AllowGet);
                                        }
                                        //Ultima posicion del escaneo
                                        var position = _purchaseOrderItemBusiness.GetItemPositionByOc(purchase_no);
                                        //Actualizacion del escaneo en purchase_orden_item
                                        var updatePostion = _purchaseOrderItemBusiness.UpdateItemPositionByOcAndPartNumber(purchase_no, itemSupplier.part_number, position);
                                        //insert blind count
                                        var blind = _blindCountBusiness.AddBlindCount(purchase_no, site, itemSupplier.part_number, barcodes.part_barcode, cantidad, position, userr);
                                        if (blind)
                                        {
                                            blind = _purchaseOrderItemBusiness.UpdatePurchaseOrderItemTime(purchase_no, itemSupplier.part_number, userr);

                                            if (blind)
                                                return Json(new { success = 3, part_description = itemSupplier.part_description, part_number = itemSupplier.part_number, quantity = cantidad }, JsonRequestBehavior.AllowGet);
                                            else
                                                return Json(new { success = 4 }, JsonRequestBehavior.AllowGet);
                                        }
                                        else
                                            return Json(new { success = 4 }, JsonRequestBehavior.AllowGet);
                                    }
                                }
                                //entra si la cantidad ingresada es mayor a OC en BD
                                else
                                {
                                    return Json(new { success = 9 }, JsonRequestBehavior.AllowGet);
                                }
                            }
                            //entra si no existe relacion item/supplier
                            else
                            {
                                return Json(new { success = 5 }, JsonRequestBehavior.AllowGet);
                            }
                        }
                        //entra si producto no pertenece a oc
                        else
                        {
                            return Json(new { success = 7 }, JsonRequestBehavior.AllowGet);
                        }
                    }
                    //orden no tiene items
                    else
                    {
                        return Json(new { success = 8 }, JsonRequestBehavior.AllowGet);
                    }
                }
                //entra si no existe barcode
                else
                {
                    return Json(new { success = 6 }, JsonRequestBehavior.AllowGet);
                }
            }

        }

        public ActionResult CloseOC(string purchase_no, string user)
        {
            var purchase = _purchaseOrderBusiness.GetPurchaseOrderByNo(purchase_no);
            if (purchase != null)
            {
                var blindRemoveZero = _blindCountBusiness.RemoveZeroBlindCount(purchase_no);
                if (blindRemoveZero)
                {
                    var blindList = _blindCountBusiness.GetBlindCountByOC(purchase_no);
                    if (blindList != null && blindList.Count > 0)
                    {
                        if (_purchaseOrderBusiness.UpdatePurchaseOrderFromAndroid(purchase, user))
                        {
                            return Json(new { success = 1 }, JsonRequestBehavior.AllowGet);
                        }
                        else
                        {
                            return Json(new { success = 2 }, JsonRequestBehavior.AllowGet);
                        }
                    }
                    //no tiene ningun registro en blind count
                    else
                    {
                        return Json(new { success = 4 }, JsonRequestBehavior.AllowGet);
                    }
                }
                else
                {
                    return Json(new { success = 5 }, JsonRequestBehavior.AllowGet);
                }
            }
            else
            {
                return Json(new { success = 3 }, JsonRequestBehavior.AllowGet);
            }
        }
        public ActionResult GetAllSupLike(String name)
        {
            var suppliers = _supplierBusiness.GetAllSupLike(name);
            if (suppliers != null)
            {
                var json = JsonConvert.SerializeObject(suppliers, Formatting.Indented, new JsonSerializerSettings { ReferenceLoopHandling = ReferenceLoopHandling.Ignore });
                return Json(new { success = true, Json = json }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(new { success = false, responseText = "Error" }, JsonRequestBehavior.AllowGet);
            }

        }
        public ActionResult GetAllSuppliersFootTruckInStatus1()
        {
            var suppliers = _supplierBusiness.GetAllSuppliersFootTruckInStatus1();
            if (suppliers != null)
            {
                var json = JsonConvert.SerializeObject(suppliers, Formatting.Indented, new JsonSerializerSettings { ReferenceLoopHandling = ReferenceLoopHandling.Ignore });
                return new JsonResult() { Data = new { success = true, Json = json }, JsonRequestBehavior = JsonRequestBehavior.AllowGet, MaxJsonLength = Int32.MaxValue };
            }
            else
            {
                return Json(new { success = false, responseText = "Error" }, JsonRequestBehavior.AllowGet);
            }
        }
        public ActionResult GetAllSuppliersApplyReturnInStatus1()
        {
            var suppliers = _supplierBusiness.GetAllSuppliersApplyReturnInStatus1();
            if (suppliers != null)
            {
                var json = JsonConvert.SerializeObject(suppliers, Formatting.Indented, new JsonSerializerSettings { ReferenceLoopHandling = ReferenceLoopHandling.Ignore });
                return new JsonResult() { Data = new { success = true, Json = json }, JsonRequestBehavior = JsonRequestBehavior.AllowGet, MaxJsonLength = Int32.MaxValue };
            }
            else
            {
                return Json(new { success = false, responseText = "Error" }, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult GetAllMerchandiseEntry(int MerchandiseEntry)
        {
            var suppliers = _supplierBusiness.GetAllMerchandiseEntry(MerchandiseEntry);
            if (suppliers != null && suppliers.Count > 0)
            {
                var json = JsonConvert.SerializeObject(suppliers, Formatting.Indented, new JsonSerializerSettings { ReferenceLoopHandling = ReferenceLoopHandling.Ignore });
                return new JsonResult() { Data = new { success = true, Json = json }, JsonRequestBehavior = JsonRequestBehavior.AllowGet, MaxJsonLength = Int32.MaxValue };
            }
            else
            {
                return Json(new { success = false, responseText = "Error" }, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult GetAllSitesFlorido()
        {
            var sites = _siteBusiness.GetAllSitesFlorido();
            if (sites != null && sites.Count > 0)
            {
                var json = JsonConvert.SerializeObject(sites, Formatting.Indented, new JsonSerializerSettings { ReferenceLoopHandling = ReferenceLoopHandling.Ignore });
                return Json(new { success = true, Json = json }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(new { success = false, responseText = "Error" }, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult GetAllSitesFloridoExcludeThisSite()
        {
            string siteCode = _siteConfigRepository.GetSiteCode();
            var sites = _siteBusiness.GetAllSitesFloridoExcludeThisSite(siteCode);
            if (sites != null && sites.Count > 0)
            {
                var json = JsonConvert.SerializeObject(sites, Formatting.Indented, new JsonSerializerSettings { ReferenceLoopHandling = ReferenceLoopHandling.Ignore });
                return Json(new { success = true, Json = json }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(new { success = false, responseText = "Error" }, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult ValidateBarcode(string b, string proveedor)
        {
            //busca barcode
            var barcodes = _barcodesBusiness.GetAllDataByBarcode(b);
            //valida que exista
            if (barcodes != null)
            {
                //busca relacion de proveedor / producto
                var itemSupplier = _itemSupplierBusiness.GetItemSupplierByItemAndSupplier(barcodes.part_number, Int32.Parse(proveedor));
                //valida que exista
                if (itemSupplier != null)
                {
                    var base_cost = _purchaseBaseCostBusiness.getPurchaseBaseCostBySupplierPartNumber(itemSupplier.supplier_item_id);
                    if (base_cost != null)
                    {
                        return Json(new { success = 1, numero = barcodes.part_number, costo = base_cost.base_cost, part_description = itemSupplier.part_description }, JsonRequestBehavior.AllowGet);
                    }
                    else
                    {
                        return Json(new { success = 3, responseText = "Error" }, JsonRequestBehavior.AllowGet);
                    }
                }
                else
                {
                    return Json(new { success = 2, responseText = "Error" }, JsonRequestBehavior.AllowGet);
                }
            }
            else
            {
                return Json(new { success = 0, responseText = "Error" }, JsonRequestBehavior.AllowGet);
            }
        }
        public ActionResult GetSupplierCurrency(int supplier)
        {
            var currency = _purchaseBaseCostBusiness.GetSupplierCurrency(supplier);
            if (currency.Count == 1)
            {
                if (currency[0] == "MXN")
                {
                    return Json(new { success = 1 }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(new { success = 2 }, JsonRequestBehavior.AllowGet);
                }
            }
            else
            {
                return Json(new { success = 0, responseText = "Error" }, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult ValidateBarcodeCurrency(string b, string proveedor, string currency)
        {
            //busca barcode
            var barcodes = _barcodesBusiness.GetAllDataByBarcode(b);
            //valida que exista
            if (barcodes != null)
            {
                //busca relacion de proveedor / producto
                var itemSupplier = _itemSupplierBusiness.GetItemSupplierByItemAndSupplier(barcodes.part_number, Int32.Parse(proveedor));
                //valida que exista
                if (itemSupplier != null)
                {
                    if (_itemSupplierBusiness.ValidatePartNumberBySite(barcodes.part_number, ""))
                    {
                        var base_cost = _purchaseBaseCostBusiness.getPurchaseBaseCostBySupplierPartNumber(itemSupplier.supplier_item_id);
                        if (base_cost.currency != currency)
                            return Json(new { success = 5, responseText = "Error" }, JsonRequestBehavior.AllowGet);
                        if (base_cost != null)
                            return Json(new { success = 1, numero = barcodes.part_number, costo = base_cost.base_cost, part_description = itemSupplier.part_description }, JsonRequestBehavior.AllowGet);
                        else
                            return Json(new { success = 3, responseText = "Error" }, JsonRequestBehavior.AllowGet);
                    }
                    else
                        return Json(new { success = 6, responseText = "Error" }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(new { success = 2, responseText = "Error" }, JsonRequestBehavior.AllowGet);
                }
            }
            else
            {
                return Json(new { success = 0, responseText = "Error" }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public ActionResult GeneratePurchaseOrder(string items, string username, string proveedor, string moneda)
        {
            string site = _siteConfigRepository.GetSiteCode();
            string resultado = _purchaseOrderBusiness.GeneratePOAndroid(items, username, proveedor, moneda, site);
            if (resultado == "")
            {
                return Json(new { success = 0 }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(new { success = 1, orden = resultado }, JsonRequestBehavior.AllowGet);
            }
        }
        public ActionResult CheckParhNomber(int Supplier, string ParthNumber, string Type)
        {
            var barcodes = _barcodesBusiness.GetAllDataByBarcode(ParthNumber);
            if (barcodes != null)
            {
                var ItemSupplier = _itemSupplierBusiness.GetEntryFree(barcodes.part_number, Supplier, Type);
                if (ItemSupplier != null)
                {
                    if (_itemSupplierBusiness.ValidatePartNumberBySite(ParthNumber, ""))
                    {
                        var descr = _itemBusiness.GetOneItem(barcodes.part_number);
                        return Json(new { success = 1, size = descr.UnitSize, description = descr.Description, part_number = barcodes.part_number }, JsonRequestBehavior.AllowGet);
                    }
                    else
                        return Json(new { success = 3 }, JsonRequestBehavior.AllowGet); //No esta disponible para esta tienda
                }
            }
            return Json(new { success = 0 }, JsonRequestBehavior.AllowGet);
        }
        public ActionResult ParnumberSize(string ParthNumber)
        {
            var Size = _itemBusiness.GetOneItem(ParthNumber);
            if (Size != null)
            {
                string Sizes = Size.UnitSize;
                return Json(new { size = Sizes }, JsonRequestBehavior.AllowGet);
            }
            return new JsonResult() { Data = new { size = "N/A" }, JsonRequestBehavior = JsonRequestBehavior.AllowGet, MaxJsonLength = Int32.MaxValue };
        }
        [HttpPost]
        public ActionResult CreateEntryFree(string EntryFree, int Supplier, string Type, string SubType, string Reference, string Commentary, string User)
        {
            var tupla = _EntryFreeHeadBusiness.PostEntryFreeHead(Supplier, Type, SubType, Reference, Commentary, User, EntryFree);
            if (tupla.Item1 > 0)
            {
                _EntryFreeListBusiness.PostEntryFreeList(tupla.Item2, tupla.Item1, User, Supplier);
                var EntryFreeList = _EntryFreeHeadBusiness.NewgetEntryFree(tupla.Item1);

                if (EntryFreeList.Item1 == 0)
                {
                    //var Email = "mpasillas@elflorido.com.mx";
                    var Recibo = _sysRoleMasterBusiness.GetEmailsByRol(ConfigurationManager.AppSettings["Recibo"].ToString());
                    var MesaControl = _sysRoleMasterBusiness.GetEmailsByRol(ConfigurationManager.AppSettings["MesaControl"].ToString());
                    //por aqui paso pasillas y lo modifico
                    var emailSupplier = _MaSupplierContactRepo.EmailsGeneric(Supplier, "Contabilidad");
                    if (emailSupplier == "")
                        emailSupplier = _MaSupplierContactRepo.EmailsGeneric(Supplier, "Ventas");
                    var site = _siteBusiness.GetInfoSite();
                    var Email = (!string.IsNullOrWhiteSpace(Recibo) ? Recibo + "," : "") + (!string.IsNullOrWhiteSpace(MesaControl) ? MesaControl + "," : "");
                    Task task = new Task(() =>
                    {
                        EntryReport report = new EntryReport();
                        report.DataSource = report.printTable(EntryFreeList.Item2);

                        Stream stream = new MemoryStream();
                        report.ExportToPdf(stream);
                        if (EntryFreeList.Item2.EmailSale != null && EntryFreeList.Item2.EmailSale != "N/A")
                        {
                            Email += ((!string.IsNullOrWhiteSpace(Email)) && !string.IsNullOrWhiteSpace(EntryFreeList.Item2.EmailSale) ? (", " + EntryFreeList.Item2.Emailpurchases) : EntryFreeList.Item2.Emailpurchases);
                        }
                        information.folio = tupla.Item1.ToString();
                        information.store = _SupplierBusiness.GetSupplierInfoById(Supplier).BusinessName;
                        information.subject = site.site_name + " - Entrada de mercancia de " + information.store;
                        information.from = site.site_name;
                        information.email = Email + "," + (!string.IsNullOrWhiteSpace(emailSupplier) ? emailSupplier + "," : "");

                        common.MailMessageEntryFree(information, stream, site.site_name);
                    });
                    task.Start();
                    return new JsonResult() { Data = new { folio = tupla.Item1 }, JsonRequestBehavior = JsonRequestBehavior.AllowGet, MaxJsonLength = Int32.MaxValue };
                }
                else
                {
                    return new JsonResult() { Data = new { folio = "N/A" }, JsonRequestBehavior = JsonRequestBehavior.AllowGet, MaxJsonLength = Int32.MaxValue };
                }
            }
            return new JsonResult() { Data = new { folio = "N/A" }, JsonRequestBehavior = JsonRequestBehavior.AllowGet, MaxJsonLength = Int32.MaxValue };
        }

        public ActionResult ValidateBarcodeInStockUnrestricted(string b, string c, string destinationCode)
        {
            //busca barcode
            var barcodes = _barcodesBusiness.GetAllDataByBarcode(b);
            //valida que exista
            if (barcodes != null)
            {
                if(destinationCode != null) //VAlidar los productos de las transferencias
                {
                    var value_part_number_site = _transferHeaderBusiness.ValidateDestinationCode(barcodes.part_number, destinationCode);
                    if (destinationCode != null && value_part_number_site >= 1001)
                    {
                        return Json(new { success = value_part_number_site, numero = "0", responseText = "Error" }, JsonRequestBehavior.AllowGet);
                    }
                }

                //valida el stock total
                var stockActual = _itemStockBusiness.GetItemStockSelectStockNegative(barcodes.part_number);
                if (stockActual != null)
                {
                    if (_itemStockBusiness.GetItemStock(barcodes.part_number, c) == 1)
                    {
                        //Validar las departamentos, familias permitibles
                        if (_damagedsGoodsRestrictedBusiness.GetKnowDamagedsGoodsRestricted("DAILY_SCRAP", barcodes.part_number))
                        {
                            string part_desc = _itemBusiness.PartDescription(barcodes.part_number);
                            return Json(new { success = 1, numero = barcodes.part_number, description = part_desc }, JsonRequestBehavior.AllowGet);
                        }
                        else
                        {
                            int famil = _itemBusiness.GetFamilyByProduct(barcodes.part_number);
                            int depar = _itemBusiness.GetDepartmentByProduct(barcodes.part_number);
                            string part_desc = _itemBusiness.PartDescription(barcodes.part_number);
                            return Json(new { success = 5, numero = "5", department = _maClassRepository.GetDepartmentById(depar), family = _maClassRepository.GetFamilyById(famil), description = part_desc }, JsonRequestBehavior.AllowGet);
                        }
                    }
                    else
                    {
                        string part_desc = _itemBusiness.PartDescription(barcodes.part_number);
                        return Json(new { success = 2, numero = "2", part = barcodes.part_number, description = part_desc }, JsonRequestBehavior.AllowGet);
                    }
                }
                else
                {
                    return Json(new { success = 4, numero = "4" }, JsonRequestBehavior.AllowGet);
                }
            }
            else
            {
                return Json(new { success = 0, numero = "0", responseText = "Error" }, JsonRequestBehavior.AllowGet);
            }
        }
        public ActionResult ValidateBarcodeInStockUnrestrictedValidate(string b, string c)
        {
            //busca barcode
            var barcodes = _barcodesBusiness.GetAllDataByBarcode(b);
            //valida que exista
            if (barcodes != null)
            {
                //valida el stock total
                var stockActual = _itemStockBusiness.GetItemStockSelectStockNegative(barcodes.part_number);
                if (stockActual != null)
                {
                    //Validar el stock
                    if (_itemStockBusiness.GetItemStock(barcodes.part_number, c) == 1)
                    {
                        //VAlida stock de bloqueo
                        if (_itemStockBusiness.GetValidationItemByBlocked(barcodes.part_number))
                        {
                            ////Validar las departamentos, familias permitibles
                            //if (_damagedsGoodsRestrictedBusiness.GetKnowDamagedsGoodsRestricted("SCRAP", barcodes.part_number))
                            //{
                            string part_desc = _itemBusiness.PartDescription(barcodes.part_number);
                            return Json(new { success = 1, numero = barcodes.part_number, description = part_desc }, JsonRequestBehavior.AllowGet);
                            //}
                            //else
                            //{
                            //    int famil = _itemBusiness.GetFamilyByProduct(barcodes.part_number);
                            //    int depar = _itemBusiness.GetDepartmentByProduct(barcodes.part_number);
                            //    string part_desc = _itemBusiness.PartDescription(barcodes.part_number);
                            //    return Json(new { success = 5, numero = "5", department = _maClassRepository.GetDepartmentById(depar), family = _maClassRepository.GetFamilyById(famil), description = part_desc }, JsonRequestBehavior.AllowGet);
                            //}
                        }
                        else
                        {
                            string part_desc = _itemBusiness.PartDescription(barcodes.part_number);
                            return Json(new { success = 2, numero = "2", part = barcodes.part_number, description = part_desc }, JsonRequestBehavior.AllowGet);
                        }

                    }
                    else
                    {
                        string part_desc = _itemBusiness.PartDescription(barcodes.part_number);
                        return Json(new { success = 2, numero = "2", part = barcodes.part_number, description = part_desc }, JsonRequestBehavior.AllowGet);
                    }

                }
                else
                {
                    return Json(new { success = 4, numero = "4" }, JsonRequestBehavior.AllowGet);
                }
            }
            else
            {
                return Json(new { success = 0, numero = "0", responseText = "Error" }, JsonRequestBehavior.AllowGet);
            }
        }
        [HttpPost]
        public ActionResult SendItemsToBlock(string items, string username)
        {
            string site = _siteConfigRepository.GetSiteCode();
            string resultado = _scrapControlBusiness.SendItemsToBlock(items, username, site);
            if (resultado == "")
            {
                return Json(new { success = 0 }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(new { success = 1 }, JsonRequestBehavior.AllowGet);
            }
        }
        [HttpPost]
        public ActionResult SendItemsToBlockDirect(string items, string username)
        {
            string site = _siteConfigRepository.GetSiteCode();
            string siteName = _siteConfigRepository.GetSiteCodeName();
            var resultadoTupla = _damageGoodBusiness.AddDamageGoodsDirect(items, username, site);
            var resultado = resultadoTupla.Item1;
            if (resultado != "ERROR")
            {
                var SiteInfo = _siteConfigRepository.GetSiteCodeAddress();
                var Gerencia = _sysRoleMasterBusiness.GetEmailsByRol(ConfigurationManager.AppSettings["Gerente"].ToString());
                Task task = new Task(() =>
                {
                    SolicitudAprobacionSalidaMerma reporte = new SolicitudAprobacionSalidaMerma();
                    reporte.DataSource = reporte.printTable(resultadoTupla.Item2, resultado, siteName);

                    Stream stream = new MemoryStream();
                    reporte.ExportToPdf(stream);

                    var Email = Gerencia;

                    information.folio = resultado;
                    information.subject = SiteInfo.SiteName + " - Solicitud Salida Mercancia por Merma";
                    information.from = SiteInfo.SiteName;
                    information.email = Email;
                    string status = common.MailMessageSalidaMerma(information, stream);
                });
                task.Start();
                return Json(new { success = 0 }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(new { success = 1 }, JsonRequestBehavior.AllowGet);
            }
        }
        //valida barcode en ITEM_STOCK bloked
        public ActionResult ValidateBarcodeInItemStockPharmacy(string b, string c)
        {
            //b = barcode c = cantidad
            //busca barcode
            var barcodes = _barcodesBusiness.GetAllDataByBarcode(b);
            //valida que exista
            if (barcodes != null)
            {
                //Validar que no sea un producto tipo: famaceutico, vinos y licores, cigarros.
                if (_damagedsGoodsRestrictedBusiness.GetKnowDamagedsGoodsRestricted("REMISION", barcodes.part_number))
                {
                    string part_desc = _itemBusiness.PartDescription(barcodes.part_number);
                    return Json(new { success = 1, numero = barcodes.part_number, descripcion = part_desc }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    int famil = _itemBusiness.GetFamilyByProduct(barcodes.part_number);
                    int depar = _itemBusiness.GetDepartmentByProduct(barcodes.part_number);
                    string part_desc = _itemBusiness.PartDescription(barcodes.part_number);
                    return Json(new { success = 4, numero = "4", department = _maClassRepository.GetDepartmentById(depar), family = _maClassRepository.GetFamilyById(famil), description = part_desc }, JsonRequestBehavior.AllowGet);
                }
            }
            else
            {
                return Json(new { success = 0, numero = "0" }, JsonRequestBehavior.AllowGet);
            }
        }
        //valida barcode en ITEM_STOCK bloked
        public ActionResult ValidateBarcodeInItemStock(string b, string c)
        {
            //b = barcode c = cantidad
            //busca barcode
            var barcodes = _barcodesBusiness.GetAllDataByBarcode(b);
            //valida que exista
            if (barcodes != null)
            {
                //Validar que no sea un producto no aceptado
                if (_damagedsGoodsRestrictedBusiness.GetKnowDamagedsGoodsRestricted("DAILY_SCRAP", barcodes.part_number))
                {
                    string part_desc = _itemBusiness.PartDescription(barcodes.part_number);
                    return Json(new { success = 1, numero = barcodes.part_number, descripcion = part_desc }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(new { success = 2, numero = "2" }, JsonRequestBehavior.AllowGet);
                }
            }
            else
            {
                return Json(new { success = 0, numero = "0" }, JsonRequestBehavior.AllowGet);
            }
        }
        public ActionResult GetAllDamagedMerchadiseReasons()
        {
            var causes = _maCodeBusiness.GetListTypeDamagedMerchadiseReasons();
            if (causes != null && causes.Count > 0)
            {
                var json = JsonConvert.SerializeObject(causes, Formatting.Indented, new JsonSerializerSettings { ReferenceLoopHandling = ReferenceLoopHandling.Ignore });
                return Json(new { success = true, Json = json }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(new { success = false, responseText = "Error" }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public ActionResult SendItemsToDamage(string items, string username)
        {
            string site = _siteConfigRepository.GetSiteCode();
            string siteName = _siteConfigRepository.GetSiteCodeName();
            var resultadoTupla = _damageGoodBusiness.AddDamageGoods(items, username, site);
            var resultado = resultadoTupla.Item1;
            var SiteInfo = _siteConfigRepository.GetSiteCodeAddress();
            if (resultado != "ERROR")
            {
                Task task = new Task(() =>
                {
                    SolicitudAprobacionSalidaMerma reporte = new SolicitudAprobacionSalidaMerma();
                    reporte.DataSource = reporte.printTable(resultadoTupla.Item2, resultado, siteName);

                    Stream stream = new MemoryStream();
                    reporte.ExportToPdf(stream);

                    var Gerencia = _sysRoleMasterBusiness.GetEmailsByRol(ConfigurationManager.AppSettings["Gerente"].ToString());
                    var Email = Gerencia;

                    information.folio = resultado;
                    information.subject = SiteInfo.SiteName + " - Solicitud Salida Mercancia por Merma";
                    information.from = SiteInfo.SiteName;
                    information.email = Email;
                    string status = common.MailMessageSalidaMerma(information, stream);
                });
                task.Start();

                return Json(new { success = 0 }, JsonRequestBehavior.AllowGet);
            }
            else
                return Json(new { success = 1 }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult SendItemsToRma(string items, string username, string Document, int Status, string Supplier)
        {
            //Pasar producto a bloqueo
            if (!_scrapControlBusiness.SendItemsToBlockRemision(items, username, _siteConfigRepository.GetSiteCode()))
                return Json(new { success = 1, numero = "1" }, JsonRequestBehavior.AllowGet);

            if (Document != "")
            {
                string v = "{\"Items\":" + items + "}";
                var result = JsonConvert.DeserializeObject<ScrapControlAndroidModel>(v);
                List<DamagedGoodsBanatiModel> list = new List<DamagedGoodsBanatiModel>();
                foreach (var item in result.Items)
                {
                    DamagedGoodsBanatiModel model = new DamagedGoodsBanatiModel
                    {
                        PartNumber = item.part_number,
                        Quantity = Convert.ToDecimal(item.quantity),
                        Comment = item.comment
                    };
                    list.Add(model);
                }

                if (_RmaHeadBusiness.UpdateBanati(list, Document, "", username, Status, "AndroidCMR"))
                {
                    if (Status == 1)
                        RmaBanatiEmail(Document);

                    return Json(new { success = 0 }, JsonRequestBehavior.AllowGet);
                }
                else
                    return Json(new { success = 1 }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                var RmaBanati = _damageGoodBusiness.AddDamageGoodsRma(items, username, Status, Supplier);
                if (RmaBanati != null)
                {
                    if (Status == 1)
                        RmaBanatiEmail(Document);

                    return Json(new { success = 0 }, JsonRequestBehavior.AllowGet);
                }
                else
                    return Json(new { success = 1 }, JsonRequestBehavior.AllowGet);
            }
        }

        private void RmaBanatiEmail(string Document)
        {
            Task task = new Task(() =>
            {
                string gerente = ConfigurationManager.AppSettings["Gerente"].ToString();
                string emails = _sysRoleMasterBusiness.GetEmailsByRol(gerente);

                var RmaBanati = _RmaHeadBusiness.GetRmaHeadByDocument(Document);

                RMABanati report = new RMABanati();
                report.DataSource = report.printTable(RmaBanati);
                Stream stream = new MemoryStream();
                report.ExportToPdf(stream);

                information.folio = RmaBanati.DamagedGoodsDoc.ToString();
                information.store = RmaBanati.Supplier;
                information.subject = "Remision de mercancias";
                information.from = "El florido area de compras";
                information.email = emails;

                string status = common.MailMessageRMA(information, stream);
            });
            task.Start();
        }

        [HttpPost]
        public ActionResult SendItemsToTransfer(string items, string username, string siteDestination)
        {
            string site = _siteConfigRepository.GetSiteCode();
            var resultado = _transferHeaderBusiness.SendItemsToTransfer(items, username, site, siteDestination);
            if (string.IsNullOrEmpty(resultado.Item1))
            {
                return Json(new { success = 2 }, JsonRequestBehavior.AllowGet);
            }
            else if (resultado.Item1 == "ERROR HEADER")
            {
                return Json(new { success = 1 }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                string siteDestinationName = _siteBusiness.GetSiteCodeName(siteDestination);
                string siteEnvio = _siteConfigRepository.GetSiteCodeName();
                var itemsTransfer = _transferHeaderBusiness.GetAllTransferDetailWithoutStatus(resultado.Item1);
                var Gerencia = _sysRoleMasterBusiness.GetEmailsByRol(ConfigurationManager.AppSettings["Gerente"].ToString());
                Task task = new Task(() =>
                {
                    TransferItemsDocument reporte = new TransferItemsDocument();
                    reporte.DataSource = reporte.printTable(itemsTransfer, resultado.Item1, siteDestinationName, siteEnvio);

                    Stream stream = new MemoryStream();
                    reporte.ExportToPdf(stream);

                    var Email = Gerencia;

                    information.folio = resultado.Item1;
                    information.subject = "Transferencia de Mercancia";
                    information.from = "El Florido, Area de Recibo";
                    information.email = Email;
                    string status = common.MailTransferSite(information, stream);
                });
                task.Start();

                return Json(new { success = 0 }, JsonRequestBehavior.AllowGet);
            }
        }
        [HttpPost]
        public ActionResult SendItemsToRawMaterial(string items, string username)
        {
            string site = _siteConfigRepository.GetSiteCode();
            var resultado = _rawMaterialBusiness.SendItemsToRawMaterial(items, username, site);
            if (resultado.Item1 == "")
            {
                return Json(new { success = 2 }, JsonRequestBehavior.AllowGet);
            }
            else if (resultado.Item1 == "ERROR HEADER")
            {
                return Json(new { success = 1 }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(new { success = 0 }, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult EditStatusTransferPalletInfo(string Pallet, string User)
        {
            if (!string.IsNullOrWhiteSpace(User.Trim()))
            {
                var pallets = _TransferPalletInfoBussines.EditStatusTransferPalletInfo(Pallet, User);
                return Json(new { success = 1, pallet = pallets.Item1, text = pallets.Item2 }, JsonRequestBehavior.AllowGet);
            }
            return Json(new { success = 1, pallet = false, text = "" }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetLocationsByType()
        {
            string siteCode = _siteConfigRepository.GetSiteCode();
            var locations = _storageLocationBusiness.GetLocationsByType("RAW", siteCode);
            if (locations != null && locations.Count > 0)
            {
                var json = JsonConvert.SerializeObject(locations, Formatting.Indented, new JsonSerializerSettings { ReferenceLoopHandling = ReferenceLoopHandling.Ignore });
                return Json(new { success = true, Json = json }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(new { success = false, responseText = "Error" }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public ActionResult ActionTokenUpdate(string u, string token, string id, string m, string b, string sdk)
        {
            if (token == "" || token == null)
                return Json(new { success = 2 }, JsonRequestBehavior.AllowGet);

            var TokenUser = _tokenPhoneBusiness.SearchUserByUserName(u);
            var TokenPhone = _tokenPhoneDetailBusiness.SearchToken(token);

            if (TokenPhone != null)
            {
                var TokenRepit = _tokenPhoneBusiness.SearchTokenRepeat(TokenPhone.id_token);
                if (TokenRepit < 1)
                    return Json(new { success = 2 }, JsonRequestBehavior.AllowGet);

                if (TokenUser != null)
                {
                    var resp = _tokenPhoneBusiness.UpdateUsertoken(TokenUser, TokenPhone.id_token);
                    if (resp > 0)
                        return Json(new { success = 1 }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    var resp = _tokenPhoneBusiness.CreationUserToken(u, TokenPhone.id_token);
                    if (resp > 0)
                        return Json(new { success = 1 }, JsonRequestBehavior.AllowGet);
                }
            }
            else
            {
                int lastId = _tokenPhoneDetailBusiness.CreationTokenPhone(u, token, m, b, sdk, id);
                if (TokenUser != null)
                {
                    var resp = _tokenPhoneBusiness.UpdateUsertoken(TokenUser, lastId);
                    if (resp > 0)
                        return Json(new { success = 1 }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    var resp = _tokenPhoneBusiness.CreationUserToken(u, lastId);
                    if (resp > 0)
                        return Json(new { success = 1 }, JsonRequestBehavior.AllowGet);
                }
            }
            return Json(new { success = 2 }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult ValidateBarcodeEmptySpace(string b, string p)
        {
            var barcodes = _barcodesBusiness.GetAllDataByBarcode(b);

            if (barcodes != null)
            {
                string part_desc = _itemBusiness.PartDescription(barcodes.part_number);
                var planogramReal = _planogramLocationBusiness.GetPlanogramLocationByLocationAndProduct(p, b);
                if (planogramReal != null)
                {
                    var Item = _emptySpaceDetailBusiness.GetInfoEmptyByPartNumber(b, p);
                    if (Item != null)
                    {
                        return Json(new { success = 1, numero = barcodes.part_number, descrip = part_desc, planogram = planogramReal.planogram_location1, id_empty_space = Item.id_empty_space, planogramS = p }, JsonRequestBehavior.AllowGet);
                    }
                    return Json(new { success = 1, numero = barcodes.part_number, descrip = part_desc, planogram = planogramReal.planogram_location1, id_empty_space = 0, planogramS = "" }, JsonRequestBehavior.AllowGet);
                }
                return Json(new { success = 2, numero = barcodes.part_number, descrip = part_desc, planogram = "", id_empty_space = 0, planogramS = "" }, JsonRequestBehavior.AllowGet);
            }
            return Json(new { success = 0, numero = "0", descrip = "", planogram = "", id_empty_space = 0, planogramS = "" }, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public ActionResult SendItemstoEmptySpace(string items, string username)
        {
            int id_old = 0;
            var ListIds = _emptySpaceDetailBusiness.CreateEmptySpaceScanDetail(items, username);
            if (ListIds != null)
            {
                Task task = new Task(() =>
                {
                    ListIds = ListIds.Distinct().ToList();
                    foreach (var item in ListIds)
                    {
                        var Distrital = _sysRoleMasterBusiness.GetEmailsByRol(ConfigurationManager.AppSettings["Distrital"].ToString());
                        var Gerente = _sysRoleMasterBusiness.GetEmailsByRol(ConfigurationManager.AppSettings["Gerente"].ToString());
                        var AndroidDistrital = _sysRoleMasterBusiness.GetEmailsInfoByRol(ConfigurationManager.AppSettings["Distrital"].ToString());
                        var AndroidGerente = _sysRoleMasterBusiness.GetEmailsInfoByRol(ConfigurationManager.AppSettings["Gerente"].ToString());
                        var Email = Distrital
                        + (!string.IsNullOrWhiteSpace(Distrital) && !string.IsNullOrWhiteSpace(Gerente) ? ", " + Gerente : Gerente);
                        if (item < 0)
                        {
                            id_old = item * -1;
                            foreach (var androidD in AndroidDistrital)
                            {
                                int noti = _notifi.NotificationTwo(_notifi.titleEmptySpaceIncidence + id_old.ToString(), _notifi.messsageEmptySpaceIncidence, androidD.UserName, username, "AndroidESS");
                            }

                            foreach (var androidG in AndroidGerente)
                            {
                                int noti = _notifi.NotificationTwo(_notifi.titleEmptySpaceIncidence + id_old.ToString(), _notifi.messsageEmptySpaceIncidence, androidG.UserName, username, "AndroidESS");
                            }


                            List<ItemEmptySpaceScanModel> list = new List<ItemEmptySpaceScanModel>();
                            list = _emptySpaceDetailBusiness.GetAllItemsByIdEmptySpace(id_old);
                            string nameFull = _userMasterBusiness.getCompleteName(username);
                            var resultEmptybyIncidence = _emptySpaceBusiness.GetEmptySpaceById(id_old);

                            EmptySpaceScanReport reporte = new EmptySpaceScanReport();
                            reporte.DataSource = reporte.printTable(list, resultEmptybyIncidence.cdate, resultEmptybyIncidence.incidence, nameFull);
                            Stream stream = new MemoryStream();
                            reporte.ExportToPdf(stream);

                            information.folio = id_old.ToString();
                            information.subject = _notifi.titleEmptySpaceIncidence;
                            information.from = "Reporte " + _notifi.titleEmptySpaceIncidence;
                            information.email = Email;
                            common.MailMessageEmptySpaceScan(information, stream);
                        }
                        else if (item > 0)
                        {
                            foreach (var androidD in AndroidDistrital)
                            {
                                int noti = _notifi.NotificationTwo(_notifi.titleEmptySpaceIncidence + id_old.ToString(), _notifi.messsageEmptySpaceIncidence, androidD.UserName, username, "AndroidESS");
                            }

                            foreach (var androidG in AndroidGerente)
                            {
                                int noti = _notifi.NotificationTwo(_notifi.titleEmptySpaceIncidence + id_old.ToString(), _notifi.messsageEmptySpaceIncidence, androidG.UserName, username, "AndroidESS");
                            }

                            List<ItemEmptySpaceScanModel> list = new List<ItemEmptySpaceScanModel>();
                            list = _emptySpaceDetailBusiness.GetAllItemsByIdEmptySpace(item);
                            string nameFull = _userMasterBusiness.getCompleteName(username);
                            var resultEmptybyIncidence = _emptySpaceBusiness.GetEmptySpaceById(item);
                            EmptySpaceScanReport reporte = new EmptySpaceScanReport();
                            reporte.DataSource = reporte.printTable(list, resultEmptybyIncidence.cdate, resultEmptybyIncidence.incidence, nameFull);

                            Stream stream = new MemoryStream();
                            reporte.ExportToPdf(stream);

                            information.folio = item.ToString();
                            information.subject = _notifi.titleEmptySpace;
                            information.from = "Reporte " + _notifi.titleEmptySpace;
                            information.email = Email;
                            common.MailMessageEmptySpaceScan(information, stream);
                        }
                    }
                });
                task.Start();

                return Json(new { success = 0 }, JsonRequestBehavior.AllowGet);
            }
            return Json(new { success = 1 }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetEmptySpaceScanHistory(string user)
        {
            var itemsHistory = _emptySpaceDetailBusiness.GetEmptySpaceScanHistoryByUser(user);

            if (itemsHistory != null)
            {
                var json = JsonConvert.SerializeObject(itemsHistory, Formatting.Indented, new JsonSerializerSettings { ReferenceLoopHandling = ReferenceLoopHandling.Ignore });
                return Json(new { success = 0, Json = json }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(new { success = 1, responseText = "Error" }, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult ValidateBarcodeInventory(string barcode, string quantity, string location, string username, int genericuser, string inventoryid)
        {
            if (_inventoryHeaderBusiness.GetInventoryById(inventoryid).inventory_status != 1)
            {
                return Json(new { Success = 5 }, JsonRequestBehavior.AllowGet);
            }

            var cyclic = _inventoryHeaderBusiness.CheckCyclicInventory(inventoryid);
            var check = false;
            var item = _itemBusiness.SearchCode(
                search: barcode,
                exact: true,
                barcode: true,
                presentation: false,
                planogram: false
                ).Data.FirstOrDefault();
            if (item != null)
            {
                //VALIDACION LOCACIONES
                if (!cyclic)
                {
                    if (_inventoryLocationBusiness.GetCountInventoryLocation() != 0)
                    {
                        if (!_inventoryLocationBusiness.CheckInventoryLocation(location, inventoryid))
                            return Json(new { Success = 6 }, JsonRequestBehavior.AllowGet);
                    }
                }
                //VALIDACION CICLICO

                if (cyclic)
                    check = _InventoryPalletsBusiness.CkItemInventoryTypeCyclic(inventoryid, item.ItemBarcode.PartNumber);
                else
                    check = _InventoryPalletsBusiness.CkItemInventoryType(inventoryid, item.ItemBarcode.PartNumber);
                /////////////////////
                if (check)
                {
                    if (_InventoryDetailBusiness.SetInventoryCount(inventoryid, item.ItemBarcode.PartNumber, quantity, location, username, genericuser))
                    {
                        return Json(new { Success = 1, item.ItemBarcode.PartNumber, item.ItemBarcode.Description }, JsonRequestBehavior.AllowGet);
                    }
                    else
                        return Json(new { Success = 4 }, JsonRequestBehavior.AllowGet);
                }
                else
                    return Json(new { Success = 3 }, JsonRequestBehavior.AllowGet);
            }
            else
                return Json(new { Success = 0 }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetInventoryItems(int genericuser, string inventoryid)
        {
            var items = _InventoryDetailBusiness.GetInventoryItems(genericuser, inventoryid);
            if (items != null)
                return Json(new { Success = 1, Items = items }, JsonRequestBehavior.AllowGet);
            else
                return Json(new { Success = 0 }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult ValidateBarcodeInItemStockSupplier(string b, string c, string s)
        {
            //b = barcode c = cantidad, s = supplier
            //Busca barcode
            var barcodes = _barcodesBusiness.GetAllDataByBarcode(b);
            // Valida que exista
            if (barcodes != null)
            {
                //Busca si existe relación con el proveedor
                var itemSupplier = _itemSupplierBusiness.GetItemSupplierByItemAndSupplier(barcodes.part_number, Int32.Parse(s));
                // Valida la relación que exista
                if (itemSupplier != null)
                {
                    var base_cost = _itemBusiness.GetLastPurchasePrice(barcodes.part_number, Int32.Parse(s), 1);
                    if (base_cost != null)
                    {
                        if (base_cost[0].t_cost != 0)
                        {
                            //Validar las departamentos, familias permitibles
                            if (_damagedsGoodsRestrictedBusiness.GetKnowDamagedsGoodsRestricted("RMA", barcodes.part_number))
                            {
                                //Si tiene cantidad para bloquear
                                string part_desc = _itemBusiness.PartDescription(barcodes.part_number);
                                return Json(new { success = 1, numero = barcodes.part_number, description = part_desc, cost = base_cost[0].t_cost }, JsonRequestBehavior.AllowGet);
                            }
                            else
                            {
                                int famil = _itemBusiness.GetFamilyByProduct(barcodes.part_number);
                                int depar = _itemBusiness.GetDepartmentByProduct(barcodes.part_number);
                                string part_desc = _itemBusiness.PartDescription(barcodes.part_number);
                                return Json(new { success = 6, numero = "6", department = _maClassRepository.GetDepartmentById(depar), family = _maClassRepository.GetFamilyById(famil), description = part_desc }, JsonRequestBehavior.AllowGet);
                            }
                        }
                        else
                        {
                            // No tiene costo base
                            return Json(new { success = 5, numero = "5" }, JsonRequestBehavior.AllowGet);
                        }
                    }
                    else
                    {
                        // No tiene costo base
                        return Json(new { success = 5, numero = "5" }, JsonRequestBehavior.AllowGet);
                    }
                }
                else
                {
                    // No existe relación de producto/proveedor
                    return Json(new { success = 3, numero = "3" }, JsonRequestBehavior.AllowGet);
                }
            }
            else
            {
                //No existe el producto ingresado
                return Json(new { success = 0, numero = "0" }, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult SendItemsRMA(string DamagedsGood, string username, string supplier)
        {
            var infoSupplier = _supplierBusiness.GetSupplierDamagedsGood(Int32.Parse(supplier));
            var SiteInfo = _siteConfigRepository.GetSiteCodeAddress();
            var UuserEmail = _userMasterBusiness.GetEmailByUserName(username);
            //Bloqueo de productos
            if (!_scrapControlBusiness.SendItemsToBlockRMA(DamagedsGood, username, _siteConfigRepository.GetSiteCode()))
                return Json(new { success = 0, numero = "0" }, JsonRequestBehavior.AllowGet);

            //Pasar los productos
            //Se creará petición a Compras (Aprobación)
            if (infoSupplier != null)
            {
                var DamagedsGoods = _RmaHeadBusiness.AddDamagedGoodsRequestAndroid(DamagedsGood, Int32.Parse(supplier), username);
                if (DamagedsGoods != null)
                {
                    var session = username;
                    Task task = new Task(() =>
                    {
                        RMAReport report = new RMAReport();
                        report.DataSource = report.printTable(DamagedsGoods);
                        Stream stream = new MemoryStream();
                        report.ExportToPdf(stream);
                        information.folio = DamagedsGoods.DamagedGoodsDoc.ToString();
                        information.store = DamagedsGoods.Supplier;
                        information.subject = SiteInfo.SiteName + " - Aprobacion de Devolucion de Material";
                        information.from = SiteInfo.SiteName;
                        information.email = UuserEmail; //_sysRoleMasterBusiness.GetEmailsByRol(ConfigurationManager.AppSettings["Compras"].ToString());

                        string status = common.MailMessageRMA(information, stream);
                    });
                    task.Start();
                    return Json(new { success = 1, numero = "1" }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(new { success = 0, numero = "0" }, JsonRequestBehavior.AllowGet);
                }
            }
            else
            {   //Mandar directamente a proveedor

                var DamagedsGoods = _RmaHeadBusiness.AddDamagedGoodsAndroid(DamagedsGood, Int32.Parse(supplier), username);
                List<DamagedsGoodsModel> documentItems = _DamagedsGoodsItemBusiness.NewGetItemsByDocumentReport(DamagedsGoods.DamagedGoodsDoc, 2);
                DamagedsGoods.DamageGoodsItem = documentItems;
                if (DamagedsGoods != null)
                {
                    //
                    //
                    // Realiza el paso de la operacion
                    //
                    //
                    //Task task = new Task(() =>
                    //{
                    //    EmailDamagedGoodsModel listaRMA = new EmailDamagedGoodsModel();
                    //    RMAReportSuppliercs report = new RMAReportSuppliercs();
                    //    report.DataSource = report.printTable("", DamagedsGoods);

                    //    var SupplierEmail = _MaSupplierContact.Email(DamagedsGoods.SupplierId, "Ventas");

                    //    var stream = new MemoryStream();
                    //    report.ExportToPdf(stream);
                    //    listaRMA = new EmailDamagedGoodsModel() { Rerpote = stream, RMA = DamagedsGoods.DamagedGoodsDoc };

                    //    information.folio = DamagedsGoods.DamagedGoodsDoc.ToString();
                    //    information.store = SiteInfo.SiteName;
                    //    information.Supplier = DamagedsGoods.Supplier;
                    //    information.subject = SiteInfo.SiteName + " - Aprobación de Devolucion de mercancias - Folio - " + DamagedsGoods.DamagedGoodsDoc;
                    //    information.from = SiteInfo.SiteName;
                    //    information.email = SupplierEmail + "," + UuserEmail;

                    //    string returnEmail = common.MailMessageRemisionConfirmacionSingleRM002(information, listaRMA);

                    //});
                    //task.Start();

                    var number = _RmaHeadBusiness.StoreProcedureDamagedGoods(DamagedsGoods.DamagedGoodsDoc, username, "AndroidRma");
                    if (number.ReturnValue == 0)
                    {
                        var SupplierEmail = _MaSupplierContact.Email(DamagedsGoods.SupplierId, "Ventas");
                        Task task = new Task(() =>
                        {
                            var stream = new MemoryStream();
                            RMAReportSuppliercs report = new RMAReportSuppliercs();
                            report.DataSource = report.printTable(number.Document, DamagedsGoods);
                            EmailDamagedGoodsModel listaRMA = new EmailDamagedGoodsModel();
                            report.ExportToPdf(stream);
                            listaRMA = new EmailDamagedGoodsModel() { Rerpote = stream, RMA = DamagedsGoods.DamagedGoodsDoc };
                            information.folio = DamagedsGoods.DamagedGoodsDoc;
                            information.store = _siteConfigRepository.GetSiteCodeName();
                            information.Supplier = DamagedsGoods.Supplier;
                            information.subject = SiteInfo.SiteName + " - Devolucion de mercancias - Folio - " + DamagedsGoods.DamagedGoodsDoc;
                            information.from = SiteInfo.SiteName;
                            information.email = SupplierEmail + "," + UuserEmail;
                            common.MailMessageRemisionConfirmacionSingle(information, listaRMA);
                        });
                        task.Start();
                        return Json(new { success = 2, numero = "2" }, JsonRequestBehavior.AllowGet);
                    }
                    else
                    {
                        return Json(new { success = 0, numero = "0" }, JsonRequestBehavior.AllowGet);
                    }

                }
                else
                {
                    return Json(new { success = 0, numero = "0" }, JsonRequestBehavior.AllowGet);
                }

            }
        }

        public ActionResult DevolucionProveedor(string DamagedsGood, string username, string supplier, string comments)
        {
            var infoSupplier = _supplierBusiness.GetSupplierDamagedsGood(int.Parse(supplier));
            var SiteInfo = _siteConfigRepository.GetSiteCodeAddress();
            var UuserEmail = _userMasterBusiness.GetEmailByUserName(username);

            //Bloqueo de productos
            if (!_scrapControlBusiness.SendItemsToBlockRMA(DamagedsGood, username, _siteConfigRepository.GetSiteCode()))
                return Json(new { success = 0, numero = "0" }, JsonRequestBehavior.AllowGet);

            //Pasar los productos
            //Se creará petición a Compras (Aprobación)
            if (infoSupplier != null)
            {
                var DamagedsGoods = _RmaHeadBusiness.AddDamagedGoodsRequestAndroid(DamagedsGood, int.Parse(supplier), username, comments);
                if (DamagedsGoods != null)
                {
                    var session = username;
                    var EmailsBuyer = _userMasterBusiness.GetEmailByBuyerDivision(DamagedsGoods.BuyerDivision);
                    Task task = new Task(() =>
                    {
                        RMAReport report = new RMAReport();
                        report.DataSource = report.printTable(DamagedsGoods);
                        Stream stream = new MemoryStream();
                        report.ExportToPdf(stream);
                        information.folio = DamagedsGoods.DamagedGoodsDoc.ToString();
                        information.store = DamagedsGoods.Supplier;
                        information.subject = SiteInfo.SiteName + " - Aprobacion de Devolucion de Material";
                        information.from = SiteInfo.SiteName;
                        information.email = (!string.IsNullOrWhiteSpace(UuserEmail) ? UuserEmail + "," : "") + (!string.IsNullOrWhiteSpace(EmailsBuyer) ? EmailsBuyer : "");
                        string status = common.MailMessageRMA(information, stream);
                    });
                    task.Start();
                    return Json(new { success = 1, numero = "1" }, JsonRequestBehavior.AllowGet);
                }
                else
                    return Json(new { success = 0, numero = "0" }, JsonRequestBehavior.AllowGet);
            }
            else
            {   //Mandar directamente cambios de costos 
                var DamagedsGoods = _RmaHeadBusiness.AddDamagedGoodsAndroid(DamagedsGood, int.Parse(supplier), username, comments);
                if (DamagedsGoods != null)
                {
                    return Json(new { success = 2, numero = "2" }, JsonRequestBehavior.AllowGet);
                }
                else
                    return Json(new { success = 0, numero = "0" }, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult ActionGetAllRMABanatiSuppliers()
        {
            var rmasSupplier = _supplierBusiness.RemisionSupplierId();
            if (rmasSupplier != null)
            {
                var json = JsonConvert.SerializeObject(rmasSupplier, Formatting.Indented, new JsonSerializerSettings { ReferenceLoopHandling = ReferenceLoopHandling.Ignore });
                return new JsonResult() { Data = new { success = true, Json = json }, JsonRequestBehavior = JsonRequestBehavior.AllowGet, MaxJsonLength = Int32.MaxValue };
            }
            else
            {
                return Json(new { success = false, responseText = "Error" }, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult ActionGetAllRMABanati()
        {
            var rmas = _damageGoodBusiness.GetAllRmaHeadStatus("Remision", DateTime.Now.AddDays(-60), DateTime.Now.AddDays(+31), 0);
            if (rmas != null)
            {
                var json = JsonConvert.SerializeObject(rmas, Formatting.Indented, new JsonSerializerSettings { ReferenceLoopHandling = ReferenceLoopHandling.Ignore });
                return new JsonResult() { Data = new { success = true, Json = json }, JsonRequestBehavior = JsonRequestBehavior.AllowGet, MaxJsonLength = Int32.MaxValue };
            }
            else
            {
                return Json(new { success = false, responseText = "Error" }, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult GetRMAItems(string document)
        {
            var items = _DamagedsGoodsItemBusiness.GetRmaBanatiItemsByDocument(document);
            if (items != null)
                return Json(new { Success = 1, Items = items }, JsonRequestBehavior.AllowGet);
            else
                return Json(new { Success = 0 }, JsonRequestBehavior.AllowGet);
        }

        #region Reception Transfer

        public ActionResult GetAllTransfersItems(string transferId, string transferDocument, string transferSiteCode)
        {
            var transfers = _transHeaderInBusiness.ListTransferDetailInScanQuantity(Int32.Parse(transferId ?? "0"), transferDocument, transferSiteCode);
            if (transfers != null)
            {
                var json = JsonConvert.SerializeObject(transfers, Formatting.Indented, new JsonSerializerSettings { ReferenceLoopHandling = ReferenceLoopHandling.Ignore });
                return Json(new { success = true, Json = json, transferAll = _transHeaderInBusiness.GetCompareList(Int32.Parse(transferId), transferDocument, transferSiteCode) }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(new { success = false, }, JsonRequestBehavior.AllowGet);

            }
        }

        public ActionResult GetAllTransfers(String name)
        {
            var transfers = _transHeaderInBusiness.GetAllTransfersInTransitExcludeCEDIS(name);
            if (transfers != null)
            {
                return Json(new { success = true, Json = transfers }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(new { success = false, responseText = "Error" }, JsonRequestBehavior.AllowGet);
            }

        }
        public ActionResult UpdateScanStatusTransferDetailIn(int transferId, string transferDocument, string partNumber, decimal quantityScan, string user, string transferSiteCode)
        {

            //Buscar el barcodes
            var barcodes = _barcodesBusiness.GetAllDataByBarcode(partNumber);
            if (barcodes != null)
            {
                //Valida la existencia de la transferencia
                var transferHeader = _transHeaderInBusiness.GetTransferInTransit(transferId, transferDocument, transferSiteCode);
                if (transferHeader)
                {
                    //trae detalle de la transferencia
                    var detail = _transHeaderInBusiness.GetAllItemsByTransferInTransit(transferId, transferDocument, transferSiteCode);
                    if (detail.Any())
                    {
                        //Valida que exista el producto en la transferencia
                        var item = detail.FirstOrDefault(a => a.part_number == barcodes.part_number);
                        if (item != null)
                        {

                            var quantityBD = item.quantity;
                            //Valida que la cantida no se mayor a la pedida
                            if (quantityScan <= quantityBD)
                            {
                                //Cantidad sumada
                                var quantitySum = (item.gr_quantity ?? 0) + quantityScan;
                                if (quantitySum < 0)
                                    return Json(new { success = 4 }, JsonRequestBehavior.AllowGet); //La cantidad sumada es una cantidad negativa -
                                else if (quantitySum > quantityBD)
                                    return Json(new { success = 3 }, JsonRequestBehavior.AllowGet); //La cantidad sumada es mayor a la pedida +
                                else
                                    if (_transHeaderInBusiness.UpdateTransferHeaderIn(transferId, barcodes.part_number, quantitySum, user, transferDocument, transferSiteCode)) //Actualizar la cantidad
                                    return Json(new { success = 1, part_number = barcodes.part_number, part_description = _itemBusiness.PartDescription(barcodes.part_number), quantity = quantitySum }, JsonRequestBehavior.AllowGet); //Registro completado
                                else
                                    return Json(new { success = 2 }, JsonRequestBehavior.AllowGet); //Error al actualizar
                            }
                            else
                                return Json(new { success = 5 }, JsonRequestBehavior.AllowGet); //La cantidad es mayor a la cantidad de transferencia
                        }
                        else
                            return Json(new { success = 6, part_description = _itemBusiness.PartDescription(barcodes.part_number) }, JsonRequestBehavior.AllowGet); //El producto no corresponde a esta transferncia
                    }
                    else
                        return Json(new { success = 7 }, JsonRequestBehavior.AllowGet); //La transferencia no tiene detalle (no tiene items)
                }
                else
                    return Json(new { success = 8 }, JsonRequestBehavior.AllowGet); //La transferencia no tiene el estatus correcto
            }
            else
                return Json(new { success = 9 }, JsonRequestBehavior.AllowGet);//No existe barcode

        }
        public ActionResult ProcessTransferIn(int transferId, string user, string transferDocument, string transferSiteCode)
        {
            //VALIDAR SI TODO LOS PRODUCTOS ESTAN ESCANEADOS
            var checks = _transHeaderInBusiness.CheckAllDetailsIn(transferId, transferDocument, transferSiteCode);
            if (checks)
                return Json(new { success = false, Json = "Faltan Productos por Escanear" }, JsonRequestBehavior.AllowGet);
            var transfers = _transHeaderInBusiness.ProcessTransferIn(transferId, user, "AndroidETR", transferDocument, transferSiteCode);
            if (transfers != null)
            {
                return Json(new { success = true, Json = transfers.Document }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(new { success = false, responseText = "Error" }, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult GetLocationsByCost()
        {
            var cost_center = _maCodeBusiness.GetListGeneric("COST_CENTER");
            if (cost_center != null && cost_center.Count > 0)
            {
                var json = JsonConvert.SerializeObject(cost_center, Formatting.Indented, new JsonSerializerSettings { ReferenceLoopHandling = ReferenceLoopHandling.Ignore });
                return Json(new { success = true, Json = json }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(new { success = false, responseText = "Error" }, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult ValidateBarcodeExists(string b, string c)
        {
            //busca barcode
            var barcodes = _barcodesBusiness.GetAllDataByBarcode(b);
            //valida que exista
            if (barcodes != null)
            {
                //valida el stock total
                var stockActual = _itemStockBusiness.GetItemStockSelectStockNegative(barcodes.part_number);
                if (stockActual != null)
                {
                    string part_desc = _itemBusiness.PartDescription(barcodes.part_number);
                    if (_itemStockBusiness.GetItemStock(barcodes.part_number, c) == 1)
                        return Json(new { success = 1, numero = barcodes.part_number, description = part_desc }, JsonRequestBehavior.AllowGet);
                    else
                        return Json(new { success = 2, numero = "2", part = barcodes.part_number, description = part_desc }, JsonRequestBehavior.AllowGet);
                }
                else
                    return Json(new { success = 4, numero = "4" }, JsonRequestBehavior.AllowGet);
            }
            else
                return Json(new { success = 0, numero = "0", responseText = "Error" }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult SendItemsCostCenter(string items, string username, string costCenter, int status)
        {
            if (_costCenterBusiness.GetProcessByAndroid(items, costCenter, status, username))
                return Json(new { success = 0 }, JsonRequestBehavior.AllowGet);
            else
                return Json(new { success = 1, document = "" }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult ValidatePricePocket(string b)
        {
            //busca barcode
            var barcodes = _barcodesBusiness.GetAllDataByBarcode(b);
            //valida que exista
            if (barcodes != null)
            {
                //Obtener el precio e información del prodcuto
                var item = _salesPriceChangeBusiness.GetSingleItemPrice(barcodes.part_number);
                if (item != null)
                {
                    var ItemJson = JsonConvert.SerializeObject(item, Formatting.Indented, new JsonSerializerSettings { ReferenceLoopHandling = ReferenceLoopHandling.Ignore });
                    //Obtener solo barcodes
                    var onlyBarcodes = _itemBusiness.GetOnlyBarcodes(barcodes.part_number);
                    //Obtener presentaciones
                    var presentation = _salesPriceChangeBusiness.GetPresentationPriceByPartNumber(barcodes.part_number);
                    if (presentation.Count() > 0)
                    {
                        var PresentationJson = JsonConvert.SerializeObject(presentation, Formatting.Indented, new JsonSerializerSettings { ReferenceLoopHandling = ReferenceLoopHandling.Ignore });
                        return Json(new { success = 3, numero = "3", ItemInfo = ItemJson, Barcodes = onlyBarcodes, Presentations = PresentationJson }, JsonRequestBehavior.AllowGet);
                    }
                    else
                        return Json(new { success = 2, numero = "2", ItemInfo = ItemJson, Barcodes = onlyBarcodes }, JsonRequestBehavior.AllowGet);
                }
                else
                    return Json(new { success = 1, numero = "1", part_desc = _itemBusiness.PartDescription(barcodes.part_number) }, JsonRequestBehavior.AllowGet);
            }
            else
                return Json(new { success = 0, numero = "0" }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult ValidateItemsBlockToUnrestricted(string b, string c)
        {
            var barcodes = _barcodesBusiness.GetAllDataByBarcode(b); //Valida que exista el producto
            if (barcodes != null)
            {
                var stockCurrentUnrestricted = _itemStockBusiness.GetItemStockSelectStockNegative(barcodes.part_number); //Valida que exista SL01
                if (stockCurrentUnrestricted != null)
                {
                    var stockCurrentBlocked = _itemStockBusiness.GetItemStockSelectStockBlock(barcodes.part_number); //Validate que exista MERM01
                    if (stockCurrentBlocked != null)
                    {
                        string part_desc = _itemBusiness.PartDescription(barcodes.part_number);
                        if (stockCurrentBlocked.Stock >= Decimal.Parse(c)) //Valida que tenga stock suficiente
                            return Json(new { success = 1, numero = barcodes.part_number, description = part_desc }, JsonRequestBehavior.AllowGet);
                        else
                            return Json(new { success = 2, numero = barcodes.part_number, description = part_desc, blocked = stockCurrentBlocked.Stock.ToString() }, JsonRequestBehavior.AllowGet);
                    }
                    else
                        return Json(new { success = 3, numero = "3" }, JsonRequestBehavior.AllowGet);
                }
                else
                    return Json(new { success = 4, numero = "4" }, JsonRequestBehavior.AllowGet);
            }
            else
                return Json(new { success = 5, numero = "5" }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult SendItemsBlockToUnrestricted(string items, string username)
        {
            var resultItems = _damageGoodBusiness.MoveBlockToUnrestricted(items, "AndroidBtoU", username);
            if (resultItems.Count() > 0)
            {
                var ItemJson = JsonConvert.SerializeObject(resultItems, Formatting.Indented, new JsonSerializerSettings { ReferenceLoopHandling = ReferenceLoopHandling.Ignore });
                return Json(new { success = 2, numero = "2", ItemInfo = ItemJson }, JsonRequestBehavior.AllowGet);
            }
            else
                return Json(new { success = 1, numero = "1" }, JsonRequestBehavior.AllowGet);
        }

        #endregion
    }
}

