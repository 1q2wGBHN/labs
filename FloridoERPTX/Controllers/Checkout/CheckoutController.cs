﻿using App.BLL;
using App.BLL.Checkout;
using App.Entities.ViewModels.Checkout;
using DevExpress.XtraReports.UI;
using FloridoERPTX.Reports;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace FloridoERPTX.Controllers.Checkout
{
    public class CheckoutController : Controller
    {
        private CheckoutBusiness _CheckoutBusiness;
        private UserMasterBusiness _UserMasterBusiness;

        public CheckoutController()
        {
            _CheckoutBusiness = new CheckoutBusiness();
            _UserMasterBusiness = new UserMasterBusiness();
        }

        // GET: Checkout
        public ActionResult CHOU001()
        {
            var Model = new CheckoutIndex();
            FillLookup();
            return View(Model);
        }

        [HttpPost]
        public ActionResult CHOU001(CheckoutIndex Model)
        {
            Model.IsFromMenu = false;
            Model.DataInfo = _CheckoutBusiness.GetZCutReportData(Model.Cashier, Model.Date);
            FillLookup();
            return View(Model);
        }

        void FillLookup()
        {
            var Employees = _UserMasterBusiness.GetAllUsers();
            var CashierList = new List<SelectListItem>();

            foreach (var item in Employees)
            {
                var element = new SelectListItem();
                element.Text = string.Format("{0} {1}", item.first_name, item.last_name);
                element.Value = item.emp_no;
                CashierList.Add(element);
            }

            ViewBag.Cashier = CashierList;
        }

        [HttpPost]
        public ActionResult ActionGetZCutRepor(CheckoutIndex Model)
        {
            var report = new ZCutReport();
            (report.Bands["DetailReport"] as DetailReportBand).DataSource = report.printTable(Model);
            (report.Bands["DetailReport"] as DetailReportBand).DataMember = "xrTableRowFormat";
            return PartialView("~/Views/ReportsViews/ReportView.cshtml", report);
        }
    }
}