﻿using App.BLL.DigitalTaxStamp;
using App.DAL.DigitalTaxStamp;
using FloridoERPTX.ViewModels.DigitalTaxStamp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace FloridoERPTX.Controllers.DigitalTaxStamp
{
    public class DigitalTaxStampController : Controller
    {
        private DigitalTaxStampBusiness _DigitalTaxStampBusiness;

        public DigitalTaxStampController()
        {
            _DigitalTaxStampBusiness = new DigitalTaxStampBusiness();
        }

        [HttpGet]
        public ActionResult ActionCheckInvoice(long Number)
        {
            return Json(new { success = _DigitalTaxStampBusiness.IsInvoiceStamped(Number), Json = Number }, JsonRequestBehavior.AllowGet);
        }
    }
}
