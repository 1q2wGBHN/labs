﻿using App.BLL.InternalRequirement;
using App.BLL.Site;
using App.Entities.ViewModels.InternalRequirement;
using System;
using System.Collections.Generic;
using System.Web.Mvc;

namespace FloridoERPTX.Controllers.InternalRequirement
{
    public class InternalRequirementController : Controller
    {
        private readonly SiteConfigBusiness _siteConfigBusiness;
        private readonly InternalRequirementBusiness _internalRequirementBusiness;

        public InternalRequirementController()
        {
            _siteConfigBusiness = new SiteConfigBusiness();
            _internalRequirementBusiness = new InternalRequirementBusiness();
        }

        [HttpPost]
        public ActionResult ActionGetAllItemsCreditors()
        {
            if (Session["User"] != null)
                return new JsonResult() { Data = _internalRequirementBusiness.GetAllItemsCreditors(), JsonRequestBehavior = JsonRequestBehavior.AllowGet, MaxJsonLength = Int32.MaxValue };

            return new JsonResult() { Data = "SF", JsonRequestBehavior = JsonRequestBehavior.AllowGet };
        }

        [HttpPost]
        public ActionResult ActionGetAllRequirementsHeaderBySite()
        {
            if (Session["User"] != null)
            {
                var site = _siteConfigBusiness.GetSiteCode();
                return new JsonResult() { Data = _internalRequirementBusiness.GetAllRequirementsHeaderBySite(site, DateTime.Now.AddDays(-7), DateTime.Now), JsonRequestBehavior = JsonRequestBehavior.AllowGet, MaxJsonLength = Int32.MaxValue };
            }

            return new JsonResult() { Data = "SF", JsonRequestBehavior = JsonRequestBehavior.AllowGet };
        }

        [HttpPost]
        public ActionResult ActionGetAllRequirementsHeaderBySiteByDate(DateTime DateInit, DateTime DateFin)
        {
            if (Session["User"] != null)
            {
                var site = _siteConfigBusiness.GetSiteCode();
                return new JsonResult() { Data = _internalRequirementBusiness.GetAllRequirementsHeaderBySiteByDate(site, DateInit, DateFin), JsonRequestBehavior = JsonRequestBehavior.AllowGet, MaxJsonLength = Int32.MaxValue };
            }

            return new JsonResult() { Data = "SF", JsonRequestBehavior = JsonRequestBehavior.AllowGet };
        }

        [HttpPost]
        public ActionResult ActionGetDetailsRequirement(string folio)
        {
            if (Session["User"] != null)
                return new JsonResult() { Data = _internalRequirementBusiness.GetAllRequirementsDetails(folio), JsonRequestBehavior = JsonRequestBehavior.AllowGet, MaxJsonLength = Int32.MaxValue };

            return new JsonResult() { Data = "SF", JsonRequestBehavior = JsonRequestBehavior.AllowGet };
        }

        [HttpPost]
        public ActionResult ActionAddInternalRequirement(List<InternalRequirementModel> items)
        {
            if (Session["User"] != null)
            {
                var site = _siteConfigBusiness.GetSiteCode();
                return new JsonResult() { Data = _internalRequirementBusiness.AddInternalRequierement(items, Session["User"].ToString(), site), JsonRequestBehavior = JsonRequestBehavior.AllowGet, MaxJsonLength = Int32.MaxValue };
            }

            return new JsonResult() { Data = "SF", JsonRequestBehavior = JsonRequestBehavior.AllowGet };
        }
    }
}