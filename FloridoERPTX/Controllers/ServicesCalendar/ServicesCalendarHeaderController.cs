﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using App.BLL.ServicesCalendar;
using App.BLL.Site;
using App.Entities.ViewModels.ServicesCalendar;

namespace FloridoERPTX.Controllers.ServicesCalendar
{
    public class ServicesCalendarHeaderController : Controller
    {
        private readonly ServicesCalendarHeaderBusiness _serviceBusiness;
        private readonly SiteConfigBusiness _siteConfigBusiness;
        public ServicesCalendarHeaderController()
        {
            _siteConfigBusiness = new SiteConfigBusiness();
            _serviceBusiness = new ServicesCalendarHeaderBusiness();
        }

        [HttpPost]
        public ActionResult ActionGetServicesDetail(int id)
        {
            if (Session["User"] != null)
            {
                return new JsonResult() { Data = _serviceBusiness.GetServicesDetail(id), JsonRequestBehavior = JsonRequestBehavior.AllowGet, MaxJsonLength = Int32.MaxValue };
            }
            else
            {
                return new JsonResult() { Data = "SF", JsonRequestBehavior = JsonRequestBehavior.AllowGet };
            }
        }

        [HttpPost]
        public ActionResult ActionGetQuotationDocument(int id, int supplier, int equipment)
        {
            if (Session["User"] != null)
            {
                var site = _siteConfigBusiness.GetSiteCode();
                return new JsonResult() { Data = _serviceBusiness.GetQuotationDocument(site, id, supplier, equipment), JsonRequestBehavior = JsonRequestBehavior.AllowGet, MaxJsonLength = Int32.MaxValue };
            }
            else
            {
                return new JsonResult() { Data = "SF", JsonRequestBehavior = JsonRequestBehavior.AllowGet };
            }
        }

        [HttpPost]
        public ActionResult ActionGetServicesHeaderByDate(DateTime start, DateTime finish)
        {
            if (Session["User"] != null)
            {
                var site = _siteConfigBusiness.GetSiteCode();
                return new JsonResult() { Data = _serviceBusiness.GetServicesHeaderByDate(site, start, finish), JsonRequestBehavior = JsonRequestBehavior.AllowGet, MaxJsonLength = Int32.MaxValue };
            }
            else
            {
                return new JsonResult() { Data = "SF", JsonRequestBehavior = JsonRequestBehavior.AllowGet };
            }
        }

        [HttpPost]
        public ActionResult ActionGetServicesHeaderByDateAndStatus(DateTime start, DateTime finish, int status)
        {
            if (Session["User"] != null)
            {
                var site = _siteConfigBusiness.GetSiteCode();
                return new JsonResult() { Data = _serviceBusiness.GetServicesHeaderByDateAndStatus(site, start, finish, status), JsonRequestBehavior = JsonRequestBehavior.AllowGet, MaxJsonLength = Int32.MaxValue };
            }
            else
            {
                return new JsonResult() { Data = "SF", JsonRequestBehavior = JsonRequestBehavior.AllowGet };
            }
        }

        [HttpPost]
        public ActionResult ActionAddXML(List<ServicesCalendarDetailModel> listDetails, List<ServicesCalendarModel> list, int status, int service_id)
        {
            if (Session["User"] != null)
            {
                var site = _siteConfigBusiness.GetSiteCode();
                var x = new errors();
                if (list != null)
                {
                    list[0].service_id = service_id;
                    x = _serviceBusiness.ActionAddXml(list, Session["User"].ToString());
                }
                if (x.pass == false || status == 8)
                {
                    if (x.clase == "Acreedor")
                    {
                        return new JsonResult() { Data = x.error, JsonRequestBehavior = JsonRequestBehavior.AllowGet, MaxJsonLength = Int32.MaxValue };
                    }
                    else if (x.clase == "Proveedor")
                    {
                        return new JsonResult() { Data = x.error, JsonRequestBehavior = JsonRequestBehavior.AllowGet, MaxJsonLength = Int32.MaxValue };
                    }
                    else if (status == 8)
                    {
                        return new JsonResult() { Data = _serviceBusiness.ModifyServicesDetail(listDetails, Session["User"].ToString(), status, x.expense_id, service_id), JsonRequestBehavior = JsonRequestBehavior.AllowGet, MaxJsonLength = Int32.MaxValue };
                    }
                    else
                    {
                        return new JsonResult() { Data = x.error, JsonRequestBehavior = JsonRequestBehavior.AllowGet, MaxJsonLength = Int32.MaxValue };
                    }
                }
                else if (x.pass == true || status == 8)
                {
                    return new JsonResult() { Data = _serviceBusiness.ModifyServicesDetail(listDetails, Session["User"].ToString(), status, x.expense_id, service_id), JsonRequestBehavior = JsonRequestBehavior.AllowGet, MaxJsonLength = Int32.MaxValue };
                }
                else
                {
                    return new JsonResult() { Data = false, JsonRequestBehavior = JsonRequestBehavior.AllowGet, MaxJsonLength = Int32.MaxValue };
                }
            }
            else
            {
                return new JsonResult() { Data = "SF", JsonRequestBehavior = JsonRequestBehavior.AllowGet };
            }
        }
        [HttpPost]
        public ActionResult ActionGetServices()
        {
            if (Session["User"] != null)
            {
                var x = _serviceBusiness.GetServicesHeaderByDateAndStatus(_siteConfigBusiness.GetSiteCode(), DateTime.Now, DateTime.Now.AddDays(7), 1);

                return new JsonResult() { Data = x, JsonRequestBehavior = JsonRequestBehavior.AllowGet, MaxJsonLength = Int32.MaxValue };
            }else
            {
                return new JsonResult() { Data = "SF", JsonRequestBehavior = JsonRequestBehavior.AllowGet };
            }
        }
        
    }
}