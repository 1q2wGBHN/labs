﻿using System.Web.Mvc;
using App.BLL.Customer;
using System;
using Newtonsoft.Json;
using App.BLL.Lookups;
using App.Entities.ViewModels.Customers;
using App.Entities.ViewModels.Common;
using App.BLL.Invoices;
using App.BLL.Site;
using FloridoERPTX.CommonMethods;
using App.BLL.CreditNotes;
using App.BLL.Payments;
using App.Entities;
using System.Collections.Generic;
using App.Entities.ViewModels.Invoices;
using System.Globalization;
using FloridoERPTX.Timbrado;
using App.BLL;
using App.BLL.SysLog;
using static App.Entities.ViewModels.Common.Constants;

namespace FloridoERPTX.Controllers.Costumers
{
    public class CustomersController : Controller
    {

        private CustomerBusiness _CustomerBussiness;
        private StateBusiness _StateBusiness;
        private CityBusiness _CityBusiness;
        private InvoiceBusiness _InvoiceBusiness;
        private SiteConfigBusiness _SiteConfigBusiness;
        private CreditNotesBusiness _CreditNoteBusiness;
        private PaymentsBusiness _PaymentsBusiness;
        private CreditNotesDetailBusiness _CreditNotesDetailBusiness;
        private UserMasterBusiness _UserMasterBussines;
        private SysLogBusiness _SysLogBusiness;
        private CustomerLookup _CustomerLookup;

        public CustomersController()
        {
            _CustomerBussiness = new CustomerBusiness();
            _StateBusiness = new StateBusiness();
            _CityBusiness = new CityBusiness();
            _InvoiceBusiness = new InvoiceBusiness();
            _SiteConfigBusiness = new SiteConfigBusiness();
            _CreditNoteBusiness = new CreditNotesBusiness();
            _PaymentsBusiness = new PaymentsBusiness();
            _CreditNotesDetailBusiness = new CreditNotesDetailBusiness();
            _UserMasterBussines = new UserMasterBusiness();
            _SysLogBusiness = new SysLogBusiness();
            _CustomerLookup = new CustomerLookup();
        }

        #region MainViews      

        //MainView Catálogos
        public ActionResult CTMS001()
        {
            return View();
        }

        //MainView Clientes
        public ActionResult CTMS000()
        {
            return View();
        }

        //MainView Movimientos
        public ActionResult CTMS005()
        {
            return View();
        }

        //MainView Reportes
        public ActionResult CTMS006()
        {
            return View();
        }

        #endregion

        #region Customers         

        // GET: Costumers/Create
        public ActionResult CTMS002()
        {
            string Uuser = Session["User"].ToString();
            CustomerViewModel Item = new CustomerViewModel();
            Item.StateId = 2;//State_Id for Baja California
            Item.CityId = 15;//City_Id for Tijuana
            Item.CanEdit = _UserMasterBussines.IsInRole(Uuser, Roles.IT_Department) || _UserMasterBussines.IsInRole(Uuser, Roles.DISTRITAL);
            FillLookups(2);
            return View(Item);
        }

        public ActionResult CTMS022()
        {
            CustomerViewModel Item = new CustomerViewModel();
            Item.StateId = 2;//State_Id for Baja California
            Item.CityId = 15;//City_Id for Tijuana
            FillLookups(2);
            return View(Item);
        }

        //POST: Customers Create
        [HttpPost]
        public ActionResult ActionCreateCustomer(CustomerViewModel Item)
        {
            if (ModelState.IsValid)
            {
                var Customer = Item.CUSTOMER;
                Customer.cdate = DateTime.Now;
                Customer.cuser = Session["User"].ToString();
                Customer.program_id = "CTMS002.cshtml";
                Customer.customer_rfc = Customer.customer_rfc.ToUpper();
                Customer.customer_name = Customer.customer_name.ToUpper();
                Customer.customer_code = "T" + Item.Code;
                if (_CustomerBussiness.Create(Customer))
                    return Json(new { success = true, responseText = "Cliente creado exitosamente." }, JsonRequestBehavior.AllowGet);

                return Json(new { success = false, responseText = "Ocurrio un error, contacte a sistemas." }, JsonRequestBehavior.AllowGet);
            }
            FillLookups(Item.StateId);
            return View(Item);
        }

        // GET: Costumers/Edit
        //Return list of all Clients
        public ActionResult CTMS003()
        {
            return View(_CustomerBussiness.FilterCustomers(true));
        }

        //Display modal to edit selected client
        [HttpGet]
        public ActionResult ActionEditCustomer(string CustomerCode)
        {
            string Uuser = Session["User"].ToString();
            CustomerViewModel Model = new CustomerViewModel();
            Model = _CustomerBussiness.GetCustomer(CustomerCode);
            Model.CanEdit= _UserMasterBussines.IsInRole(Uuser, Roles.IT_Department) || _UserMasterBussines.IsInRole(Uuser, Roles.DISTRITAL) || _UserMasterBussines.IsInRole(Uuser, Roles.MANAGER);
            FillLookups(Model.StateId);
            return PartialView("_CTMS003", Model);
        }             

        //Display modal to edit selected client
        [HttpGet]
        public ActionResult ActionEditCustomerSupervisor(string CustomerCode)
        {
            CustomerViewModel Model = new CustomerViewModel();
            Model = _CustomerBussiness.GetCustomer(CustomerCode);
            FillLookups(Model.StateId);
            return PartialView("_CTMS004", Model);
        }


        //Save changes made to selected client        
        [HttpPost]
        public ActionResult ActionEditCustomer(CustomerViewModel Item)
        {
            if (ModelState.IsValid)
            {
                var Customer = Item.CUSTOMER;
                Customer.udate = DateTime.Now;
                Customer.uuser = Session["User"].ToString();
                Customer.program_id = "CTMS003.cshtml";


                var SysLog = new SYS_LOG();
                DateTime dt = DateTime.Now;
                SysLog.Action = "EDITÓ";
                SysLog.Date = DateTime.Now.Date;
                SysLog.Description = Item.RFC;
                SysLog.idAfect = 0;
                SysLog.TableName = "CLIENTES";
                SysLog.Menu = "CLIENTES";
                SysLog.time = dt.ToString("HH:mm");
                SysLog.uuser = Session["User"].ToString();

                var json = JsonConvert.SerializeObject(_CustomerBussiness.FilterCustomers(true), Formatting.Indented, new JsonSerializerSettings()
                {
                    ReferenceLoopHandling = ReferenceLoopHandling.Ignore
                });
                if (_CustomerBussiness.Update(Customer, SysLog))
                {
                    return Json(new { success = true, Json = json, responseText = "Cliente editado correctamente" }, JsonRequestBehavior.AllowGet);
                }
                return Json(new { success = false, Json = json, responseText = "Ocurrio un error, contacte a sistemas." }, JsonRequestBehavior.AllowGet);
            }
            FillLookups(Item.StateId);
            return PartialView("_CTMS003", Item);
        }
        //
        [HttpPost]
        public ActionResult ActionEditCustomerSupervisor(CustomerViewModel Item)
        {
            if (ModelState.IsValid)
            {
                var Customer = Item.CUSTOMER;
                Customer.udate = DateTime.Now;
                Customer.uuser = Session["User"].ToString();
                Customer.program_id = "CTMS003.cshtml";


                var SysLog = new SYS_LOG();
                DateTime dt = DateTime.Now;
                SysLog.Action = "EDITÓ";
                SysLog.Date = DateTime.Now.Date;
                SysLog.Description = Item.RFC;
                SysLog.idAfect = 0;
                SysLog.TableName = "CLIENTES";
                SysLog.Menu = "CLIENTES";
                SysLog.time = dt.ToString("HH:mm");
                SysLog.uuser = Session["User"].ToString();

                var json = JsonConvert.SerializeObject(_CustomerBussiness.FilterCustomers(true), Formatting.Indented, new JsonSerializerSettings()
                {
                    ReferenceLoopHandling = ReferenceLoopHandling.Ignore
                });
                if (_CustomerBussiness.Update(Customer, SysLog))
                {
                    return Json(new { success = true, Json = json, responseText = "Cliente editado correctamente" }, JsonRequestBehavior.AllowGet);
                }
                return Json(new { success = false, Json = json, responseText = "Ocurrio un error, contacte a sistemas." }, JsonRequestBehavior.AllowGet);
            }
            FillLookups(Item.StateId);
            return PartialView("_CTMS003", Item);
        }
        #endregion

        #region Cajas        



        //Sustitucion Pago
        //public ActionResult CTMS009()
        //{
        //    return View();
        //}

        #endregion

        #region Catalogos
        //Forma de pago
        public ActionResult CTMS010()
        {
            return View();
        }

        //Facturas Vencidas
        public ActionResult CTMS008()
        {            
            ViewBag.customer_code = _CustomerLookup.FillLookupCustomer(CustomersLookup.ByInvoicesDue);
            return View();
        }
        [HttpPost]
        public ActionResult CTMS008(CustomerIndex model)
        {
            var modelInvoice = new InvoiceDueModel();
            modelInvoice.Invoices = _InvoiceBusiness.InvoicesDueByCustomer(model.customer_code);
            modelInvoice.Customer = _CustomerBussiness.GetCustomer(model.customer_code);
            return View("CTMS009", modelInvoice);
        }
        
        [HttpGet]
        public ActionResult ActionEditInvoice(long number)
        {
            var Model = new InvoiceModel();
            Model = _InvoiceBusiness.GetInvoiceEditByNumber(number);

            return PartialView("_CTMS009", Model);
        }
        [HttpPost]
        public ActionResult ActionEditInvoiceDueDate(InvoiceModel Model)
        {
            var Number = Model.InvoiceNumber;
            var Invoice = _InvoiceBusiness.GetInvoiceByNumber(Number);
            var SysLog = new SYS_LOG();
            DateTime dt = DateTime.Now;


            SysLog.Action = "ACTUALIZO";
            SysLog.Date = DateTime.Now.Date;
            SysLog.Description = "FECHA DE VENCIMIENTO";
            SysLog.idAfect = Model.InvoiceNumber;
            SysLog.TableName = "FACTURAS";
            SysLog.Menu = "CLIENTES";
            SysLog.time = dt.ToString("HH:mm");
            SysLog.uuser = Session["User"].ToString();

            Invoice.uuser = Session["User"].ToString();
            Invoice.udate = DateTime.Now;
            Invoice.invoice_duedate = DateTime.ParseExact(Model.DueDate, "dd/MM/yyyy", CultureInfo.InvariantCulture);

            if (_InvoiceBusiness.UpdateInvoice(Invoice, SysLog))
                return Json(new { success = true, Json = Number, responseText = string.Format("Factura {0} actualizada correctamente.", Number) }, JsonRequestBehavior.AllowGet);

            return Json(new { success = false, Json = Number, responseText = "Ocurrió un error, contacte a sistemas" }, JsonRequestBehavior.AllowGet);

        }
        #endregion

        #region Reportes

        //Catalogo General
        public ActionResult CTMS012()
        {
            return View();
        }

        //Cargos y Abonos
        public ActionResult CTMS013()
        {
            return View();
        }

        //Cargos
        public ActionResult CTMS014()
        {
            return View();
        }

        //Abonos
        public ActionResult CTMS015()
        {
            return View();
        }

        //Antiguedad de Saldos (IVA)
        public ActionResult CTMS016()
        {
            return View();
        }
        #endregion

        #region Methods and Functions for Customers
        public JsonResult ActionGetCityListBasedOnStateId(int StateId)
        {
            var cities = _CityBusiness.CitiesByStateId(StateId);
            return Json(cities, JsonRequestBehavior.AllowGet);
        }

        void FillLookups(int? StateId)
        {
            if (!StateId.HasValue)
                StateId = 2;

            ViewBag.StateId = new SelectList(_StateBusiness.StatesList(), "state_id", "state_name");
            ViewBag.CityId = new SelectList(_CityBusiness.CitiesByStateId(StateId.Value), "city_id", "city_nombre");
        }

        [HttpPost]
        public bool ActionExistRFC(string RFC)
        {
            //in case RFC exist return false so the validation from jqueryvalidate script can work on the view.
            bool CanRegisterRFC = false;
            CanRegisterRFC = !_CustomerBussiness.ExisitingRFC(RFC.ToUpper());
            return CanRegisterRFC;

        }

        [HttpPost]
        public bool ActionExistCode(string Code)
        {
            //in case Code already exist return false so the validation from jqueryvalidate script can work on the view.
            bool canRegisterCode = false;
            canRegisterCode = !_CustomerBussiness.ExisitingCode("T" + Code);
            return canRegisterCode;

        }
        #endregion

        #region Movimientos
        public ActionResult CTMS004()
        {
            var model = new SendDocumentModel();
            model.Number = "0";
            model.Document = Documents.INV;
            model = _CustomerBussiness.GetDocumentByNumberForSend(model);
            return View(model);

        }

        [HttpPost]
        public ActionResult CTMS004(SendDocumentModel model)
        {
            model = _CustomerBussiness.GetDocumentByNumberForSend(model);
            return View(model);

        }
        [HttpPost]
        public ActionResult ActionSendInvoice(SendDocumentModel model)
        {
            var senDocument = new TimbradoSoapClient();

            var ModelSend = new Request();
            ModelSend.SiteCode = _SiteConfigBusiness.GetSiteCode();
            ModelSend.Serie = model.Serie;
            ModelSend.Number = long.Parse(model.Number);
            ModelSend.Type = model.DocumentType;
            ModelSend.Customer = model.Customer;
            ModelSend.Email = model.EmailCustomerSend;

            var document = senDocument.SendDocument(ModelSend);

            return Json(new { success = document.Status, Json = document, responseText = document.Message }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult CTMS007()
        {
            var Model = new SendDocumentModel();
            return View(Model);
        }

        [HttpPost]
        public ActionResult CTMS007(SendDocumentModel Model)
        {
            Model = _CustomerBussiness.GetDocumentInfo(Model);
            return View(Model);
        }
        public ActionResult ActionReStampDocumentKiosco(SendDocumentModel Model)
        {
            var M = _CustomerBussiness.GetDocumentInfo(Model);
            var R = ActionReStampDocument(M);
            return R;
        }

        public ActionResult ActionReStampDocument(SendDocumentModel Model)
        {
            var StampDoc = new StampDocument();
            var Entity = new object();
            string PdfEncoded = string.Empty;
            var ResponseStamp = new Response();
            if (!Model.IsStamped)
            {
                if (Model.CanStampDocument)
                    ResponseStamp = StampDoc.DocumentStamp(Model, false);
                else
                {                    
                    ResponseStamp.Status = false;
                    ResponseStamp.ErrorNumber = "IVA 16";
                    ResponseStamp.Message = "Factura tiene IVA al 16% y no se puede timbrar";
                }

                if (!ResponseStamp.Status)
                {
                    Model.User = Session["User"].ToString();
                    _SysLogBusiness.CreateLogStampDocuments(Model, ResponseStamp.Message, ResponseStamp.ErrorNumber, false, true);
                    return Json(new { success = false, Json = Model, responseText = "Ocurrió un error, contacte a sistemas" }, JsonRequestBehavior.AllowGet);
                }
            }

            Entity = _CustomerBussiness.GetEntityValue(Model);
            PdfEncoded = Entity.GetType().GetProperty("pdf").GetValue(Entity, null).ToString();

            return Json(new { success = true, Json = PdfEncoded, responseText = "Documento Timbrado Correctamente" }, JsonRequestBehavior.AllowGet);
        }
        #endregion

        [HttpPost]
        public ActionResult ActionValidateDocument(long Number, Documents DocumentType)
        {
            var Document = DocumentType == Documents.BON ? "Bonificación" : DocumentType == Documents.CNOT ? "Nota de Crédito" : string.Empty;

            if (DocumentType == Documents.INV && !_InvoiceBusiness.InvoiceExist(Number))
                return Json(new { success = false, Json = Number, responseText = string.Format("{0} número {1} no existe.", "Factura", Number) }, JsonRequestBehavior.AllowGet);

            if (DocumentType == Documents.CNOT && !_CreditNoteBusiness.CreditNoteExist(Number))
                return Json(new { success = false, Json = Number, responseText = string.Format("{0} número {1} no existe.", Document, Number) }, JsonRequestBehavior.AllowGet);

            if (DocumentType == Documents.BON && !_CreditNotesDetailBusiness.BonificationExist(Number))
                return Json(new { success = false, Json = Number, responseText = string.Format("{0} número {1} no existe.", Document, Number) }, JsonRequestBehavior.AllowGet);

            if (DocumentType == Documents.PAY && _PaymentsBusiness.GetPaymentByNumber(Number) == null)
                return Json(new { success = false, Json = Number, responseText = string.Format("{0} número {1} no existe.", "Pago", Number) }, JsonRequestBehavior.AllowGet);

            return Json(new { success = true, Json = Number, responseText = string.Format("Documento {0} disponible para cancelar.", Number) }, JsonRequestBehavior.AllowGet);
        }
    }
}
