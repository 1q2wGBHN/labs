﻿using App.BLL.MaClass;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace FloridoERPTX.Controllers.MaClass
{
    public class MaClassController : Controller
    {
        private readonly MaClassBusiness _maClassBusiness;

        public MaClassController()
        {
            _maClassBusiness = new MaClassBusiness();
        }

        public ActionResult ActionGetFamily(string deptoo)
        {
            int level2 = Int32.Parse(deptoo);
            var levelsGet = _maClassBusiness.GetAllFamilyByParentClass(level2);
            if (levelsGet.Count != 0)
            {
                return Json(new { success = true, Json = levelsGet}, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(new { success = false }, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult ActionGetFamilyByDepartment(int DepartmentId)
        {
            var levelsGet = new List<App.Entities.MA_CLASS>();

            if (DepartmentId != 0)
                levelsGet = _maClassBusiness.GetAllFamilyByParentClass(DepartmentId);
            else
                levelsGet = _maClassBusiness.GetAllDepartments(2);

            return Json(new { success = true, Json = levelsGet }, JsonRequestBehavior.AllowGet);
        }

    }
}