﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using App.BLL.Site;
using App.Common;

namespace FloridoERPTX.Controllers.Kiosco
{
    public class KioscoController : Controller
    {
        [HttpPost]
        public JsonResult ActionAddComments(string name, string email,string phone,string work,string comment,bool isWork)
        {
            var cm = new App.Common.Common();
            var sitec = new SiteConfigBusiness();

            var m = cm.MailMessageHtml(new SendMail
            {
                from = "Distribuidora el florido",
                body = $@"
                                <ul>
                                    <li><strong>Nombre: </strong> {name}</li>
                                    <li><strong>Correo: </strong> {email}</li>
                                    <li><strong>telefono: </strong> {phone}</li>
                                    { (isWork ? $"<li><strong>Puesto solicitado: </strong> {work}</li>":"") }
                                    
                                </ul>
                                
                                <div>
                                    <div><strong>Mensaje: </strong></div>
                                    <p>{comment}</p>
                                </div>
                                ",
                
                
                date = DateTime.Now.ToString(),
                subject =  $"Kiosco {sitec.GetSiteCodeName()}: {(isWork ? "Reclutamiento": "Comentario")}",
                //email = "cguerra@elflorido.com.mx",
                email = isWork ? "cguerra@elflorido.com.mx,reclutamiento@elflorido.com.mx": "cguerra@elflorido.com.mx,kiosco@elflorido.com.mx" ,
                title = $"Kiosco {sitec.GetSiteCodeName()}: {(isWork ? "Reclutamiento" : "Comentario")}",
            });

            return Json(m=="success" ?  Result.OK() : Result.Error("Error al enviar mensaje") );
        }
    }
    public class Comment
    {
        public string name { get; set; }
        public string email { get; set; }
        public string phone { get; set; }
        public string work { get; set; }
        public string comment { get; set; }

        public bool isWork { get; set; }
    }
}