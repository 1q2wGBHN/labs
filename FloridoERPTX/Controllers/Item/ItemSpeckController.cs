﻿using App.BLL.Item;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace FloridoERPTX.Controllers.Item
{
    public class ItemSpeckController : Controller
    {
        // GET: ItemSpeck
        private readonly ItemSpeckBusiness _ItemSpeckBusiness;
        public ItemSpeckController()
        {
            _ItemSpeckBusiness = new ItemSpeckBusiness();
        }

        public ActionResult ActionPartSpeck(string PartNumber)
        {
            if (Session["User"] != null)
            {
                return Json(_ItemSpeckBusiness.ListItemSpeck(PartNumber), JsonRequestBehavior.AllowGet);
            }
            return Json("SF", JsonRequestBehavior.AllowGet);
        }
    }
}