﻿using App.BLL;
using App.BLL.Configuration;
using App.BLL.CostCenter;
using App.BLL.EntryFree;
using App.BLL.Inventory;
using App.BLL.Item;
using App.BLL.PurchaseOrder;
using App.BLL.Rma;
using App.BLL.Sales;
using App.BLL.Site;
using App.BLL.Transfer;
using App.Common;
using App.Entities;
using App.Entities.ViewModels.DamagedsGoods;
using App.Entities.ViewModels.Item;
using DevExpress.XtraReports.UI;
using FloridoERPTX.Reports;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Drawing;
using System.IO;
using System.Threading.Tasks;
using System.Web.Mvc;
using App.Entities.ViewModels.Transfer;
using FloridoERPTX.Controllers.Transfer;

namespace FloridoERPTX.Controllers.Item
{
    public class ItemsController : Controller
    {
        private readonly ItemBusiness _ItemBusiness;
        private readonly ItemStockBusiness _ItemStockBusiness;
        private readonly SiteConfigBusiness _siteConfigBusiness;
        private readonly Common common;
        private readonly SendMail information;
        private readonly PurchaseOrderBusiness _purchaseOrderBusiness;
        private readonly UserMasterBusiness _userMasterBusiness;
        private readonly DamagedsGoodsBusiness _RmaHeadBusiness;
        private readonly InventoryReportBusiness _inventoryReportBusiness;
        private readonly SysRoleMasterBusiness _sysRoleMasterBusiness;
        private readonly SiteBusiness _siteBusiness;
        private readonly TransferHeaderBusiness _transferHeaderBusiness;
        private readonly SalesDetailBusiness _salesDetailBusiness;
        private readonly SalesBusiness _salesBusiness;
        private readonly EntryFreeHeadBusiness _entryFreeBusiness;
        private readonly CostCenterBusiness _costCenterBusiness;
        private readonly DamagedsGoodsItemBusiness _DamagedsGoodsItemBusiness;
        private readonly InventoryAdjustmentsBusiness _inventoryAdjustmentBusiness;

        public ItemsController()
        {
            _ItemBusiness = new ItemBusiness();
            _ItemStockBusiness = new ItemStockBusiness();
            _siteConfigBusiness = new SiteConfigBusiness();
            _purchaseOrderBusiness = new PurchaseOrderBusiness();
            _userMasterBusiness = new UserMasterBusiness();
            _RmaHeadBusiness = new DamagedsGoodsBusiness();
            _inventoryReportBusiness = new InventoryReportBusiness();
            _sysRoleMasterBusiness = new SysRoleMasterBusiness();
            _siteBusiness = new SiteBusiness();
            common = new Common();
            information = new SendMail();
            _transferHeaderBusiness = new TransferHeaderBusiness();
            _salesDetailBusiness = new SalesDetailBusiness();
            _salesBusiness = new SalesBusiness();
            _entryFreeBusiness = new EntryFreeHeadBusiness();
            _DamagedsGoodsItemBusiness = new DamagedsGoodsItemBusiness();
            _costCenterBusiness = new CostCenterBusiness();
            _inventoryAdjustmentBusiness = new InventoryAdjustmentsBusiness();
        }

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult ActionListItem()
        {
            return Json(_ItemBusiness.GetAll(), JsonRequestBehavior.AllowGet);
        }

        public ActionResult ActionSearchItem(string Filter)
        {
            return new JsonResult() { Data = new { Json = _ItemBusiness.SerchItem(Filter) }, JsonRequestBehavior = JsonRequestBehavior.AllowGet, MaxJsonLength = int.MaxValue };
        }
        public ActionResult ActionSearchItemNegativeDet(string Filter , string negative)
        {
            return new JsonResult() { Data = new { Json = _ItemBusiness.SerchItemNegativeDet(Filter,negative) }, JsonRequestBehavior = JsonRequestBehavior.AllowGet, MaxJsonLength = int.MaxValue };
        }

        public ActionResult ActionSearchItemNegative(string Filter)
        {
            return new JsonResult() { Data = new { Json = _ItemBusiness.SerchItemNegative(Filter) }, JsonRequestBehavior = JsonRequestBehavior.AllowGet, MaxJsonLength = int.MaxValue };
        }

        public ActionResult ActionSearchItemValidateNo(string Filter, string NoFilter)
        {
            return new JsonResult() { Data = new { Json = _ItemBusiness.SearchItemValidateNo(Filter, NoFilter) }, JsonRequestBehavior = JsonRequestBehavior.AllowGet, MaxJsonLength = int.MaxValue };
        }

        public ActionResult ActionRemoveInventoryNegativa(int id_inventory_negative)
        {
            return new JsonResult() { Data = new { Json = _ItemStockBusiness.UpdateItemInventoryNegative(id_inventory_negative, Session["User"].ToString()), products = _inventoryAdjustmentBusiness.GetAllNegativeHeader() }, JsonRequestBehavior = JsonRequestBehavior.AllowGet, MaxJsonLength = int.MaxValue };
        }

        public ActionResult ActionRemoveInventoryNegativaInfo(string part_number)
        {
            return new JsonResult() { Data = new { Json = _ItemStockBusiness.NegativeInventoryBalanceByPartNumber(part_number) }, JsonRequestBehavior = JsonRequestBehavior.AllowGet, MaxJsonLength = int.MaxValue };
        }

        public ActionResult ActionAddItemInventoryNegative(string origin, string destination, decimal quantity, string reason)
        {
            return new JsonResult() { Data = new { Json = _ItemStockBusiness.AddItemInventoryNegative(origin, destination, quantity, reason, Session["User"].ToString()), products = _ItemStockBusiness.GetAllInvetoriesNegative() }, JsonRequestBehavior = JsonRequestBehavior.AllowGet, MaxJsonLength = int.MaxValue };
        }

        public ActionResult ActionSearchItemBarcode(string Filter)
        {
            return new JsonResult() { Data = new { Json = _ItemBusiness.SerchItemBarcode(Filter) }, JsonRequestBehavior = JsonRequestBehavior.AllowGet, MaxJsonLength = int.MaxValue };
        }
        public ActionResult ActionSearchItemBarcodePreInventory(string Filter)
        {
            return new JsonResult() { Data = new { Json = _ItemBusiness.SerchItemBarcodePreInventory(Filter) }, JsonRequestBehavior = JsonRequestBehavior.AllowGet, MaxJsonLength = int.MaxValue };
        }

        public ActionResult ActionItemBarcodeComboPresentation(string Filter)
        {
            return new JsonResult() { Data = new { Json = _ItemBusiness.SearchItemBarcodeComboPresentation(Filter) }, JsonRequestBehavior = JsonRequestBehavior.AllowGet, MaxJsonLength = int.MaxValue };
        }

        public ActionResult ActionSearchItemBarcodeSupplier(string Filter, int Supplier)
        {
            return new JsonResult() { Data = new { Json = _ItemBusiness.SearchItemBarcodeSupplier(Filter, Supplier) }, JsonRequestBehavior = JsonRequestBehavior.AllowGet, MaxJsonLength = int.MaxValue };
        }
        public ActionResult ActionSearchItemBarcodeSupplierCurrency(string Filter, int Supplier , string Currency)
        {
            return new JsonResult() { Data = new { Json = _ItemBusiness.SearchItemBarcodeSupplierCurrency(Filter, Supplier,Currency) }, JsonRequestBehavior = JsonRequestBehavior.AllowGet, MaxJsonLength = int.MaxValue };
        }

        public ActionResult ActionGetItemStockSelectStock(string PartNumber)
        {
            if (Session["User"] != null)
            {
                var Item = _ItemStockBusiness.GetItemStockSelectStock(PartNumber);

                if (Item != null)
                    return Json(new { Status = true, value = Item.Stock }, JsonRequestBehavior.AllowGet);

                return Json(new { Status = false, value = "" }, JsonRequestBehavior.AllowGet);
            }
            return Json(new { Status = false, value = "SF" }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult ActionGetItemStock(string PartNumber)
        {
            if (Session["User"] != null)
            {
                var Item = _ItemStockBusiness.GetItemStock(PartNumber);

                if (Item != null)
                    return Json(new { Status = true, Item = Item }, JsonRequestBehavior.AllowGet);

                return Json(new { Status = false }, JsonRequestBehavior.AllowGet);
            }
            return Json(new { Status = false }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult ActionGetInventoryAdjustmentRequestsByDate(DateTime beginDate, DateTime endDate)
        {
            return Json(new { success = true, Json = _ItemStockBusiness.GetInventoryAdjustmentRequestsByDate(beginDate, endDate) }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult ActionGetInventoryAdjustmentRequests()
        {
            return Json(new { success = true, Json = _ItemStockBusiness.GetInventoryAdjustmentRequests() }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult ActionGetInfoProducts(string part_numbers, int deparment, int family)
        {
            return Json(new { success = true, Json = _ItemBusiness.NewGetAllItems(part_numbers, deparment, family) }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult ActionSetItemStock(List<ItemModel> model)
        {
            if (Session["User"] != null)
            {
                string document;
                model = _ItemStockBusiness.SetItemStock(model, Session["User"].ToString(), out document);
                if (model != null)
                {
                    string Gerente = _sysRoleMasterBusiness.GetEmailsByRol(ConfigurationManager.AppSettings["Gerente"].ToString());
                    var infoIventoryCC = _ItemBusiness.GetInformationInventoryCC(document);
                    var infoSite = _siteBusiness.GetInfoSite();
                    Task task = new Task(() =>
                    {
                        SolicitudAjusteInventario reporte = new SolicitudAjusteInventario();
                        reporte.DataSource = reporte.printTable(model, infoIventoryCC, infoSite);
                        Stream stream = new MemoryStream();
                        reporte.ExportToPdf(stream);
                        var Email = Gerente;
                        information.folio = document;
                        information.subject = infoSite.site_name + " - Solicitud de Ajuste de Inventario";
                        information.from = infoSite.site_name;
                        information.email = Email;
                        common.MailMessageSolicitudAjusteInventario(information, stream);
                    });
                    task.Start();

                    return Json(new { Status = true, Document = document }, JsonRequestBehavior.AllowGet);
                }
            }
            return Json(false, JsonRequestBehavior.AllowGet);
        }

        public ActionResult ActionCancelInventoryAdjustment(string partNumber, string Document, string Comments)
        {
            if (Session["User"] != null)
            {
                if (_ItemStockBusiness.CancelInventoryAdjustment(partNumber, Document, Comments, Session["User"].ToString()))
                    return Json(true, JsonRequestBehavior.AllowGet);
            }
            return Json(false, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult ActionSetInventoryAdjustment(string partNumber, string Document, string StorageLocation, decimal Quantity, string Type, string Comments)
        {
            if (Session["User"] != null)
            {
                string document;
                var storage = _ItemStockBusiness.SetInventoryAdjustment(partNumber, Document, StorageLocation, Quantity, Type, Comments, Session["User"].ToString(), out document);
                if (storage != null)
                    return Json(new { Status = true, Document = storage }, JsonRequestBehavior.AllowGet);
            }
            return Json(false, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult ActionGetAllMovementsInventoryCC(DateTime? date1, DateTime? date2, string status, string part_number, string type_movent)
        {
            if (Session["User"] != null)
            {
                var movements = _ItemBusiness.GetInventoryCC(date1, date2, status, part_number, type_movent);
                if (movements != null)
                    return new JsonResult() { Data = new { success = true, ItemMovements = movements }, JsonRequestBehavior = JsonRequestBehavior.AllowGet, MaxJsonLength = int.MaxValue };
                else
                    return Json(new { success = false, responseText = "Sin movimientos en el rango de fechas." }, JsonRequestBehavior.AllowGet);
            }
            else
                return Json(new { success = false, responseText = "SF" }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult ActionGetAllMovementsByProduct(string Date1, string Date2, string part_number)
        {
            if (Session["User"] != null)
            {
                var movements = _ItemBusiness.GetAllMovementsByProduct(part_number, Date1, Date2);
                if (movements != null)
                    return new JsonResult() { Data = new { success = true, ItemMovements = movements }, JsonRequestBehavior = JsonRequestBehavior.AllowGet, MaxJsonLength = int.MaxValue };
                else
                    return Json(new { success = false, responseText = "Sin movimientos en el rango de fecha para el Producto Seleccionado." }, JsonRequestBehavior.AllowGet);
            }
            else
                return Json(new { success = false, responseText = "Termino tu sesión." }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult ActionGetReportByReference(string referenceNumber, string type, string document_no)
        {
            if (Session["User"] != null)
            {
                var stream = new MemoryStream();
                string siteName = _siteConfigBusiness.GetSiteCodeName();
                switch (type)
                {
                    case "GR_FROM_PO": //Entrada por Orden de Compra
                        PurchasesOrder report = new PurchasesOrder();
                        (report.Bands["DetailReport"] as DetailReportBand).DataSource = report.printTable(_purchaseOrderBusiness.NewGetAllPurchasesDetailStatus(referenceNumber, 1), _purchaseOrderBusiness.GetPurchaseOrderByPo(referenceNumber), _siteBusiness.GetInfoSite());
                        (report.Bands["DetailReport"] as DetailReportBand).DataMember = "xrTableRowFormat";
                        report.ExportToPdf(stream);
                        break;
                    case "Cancel_GR_PO": //Cancelacion de Entrada de Orden de Compra
                        CanceledPurchasesOrder cancelPurchaseOrderReport = new CanceledPurchasesOrder();
                        var orderDetail = _purchaseOrderBusiness.NewGetAllPurchasesDetailStatus(referenceNumber, 8);
                        var order = _purchaseOrderBusiness.GetPurchaseOrderByPo(referenceNumber);
                        var site = _siteBusiness.GetInfoSite();
                        cancelPurchaseOrderReport.Watermark.Text = "CANCELADO";
                        cancelPurchaseOrderReport.Watermark.TextDirection = DevExpress.XtraPrinting.Drawing.DirectionMode.ForwardDiagonal;
                        cancelPurchaseOrderReport.Watermark.Font = new Font(cancelPurchaseOrderReport.Watermark.Font.FontFamily, 60);
                        cancelPurchaseOrderReport.Watermark.ForeColor = Color.Firebrick;
                        cancelPurchaseOrderReport.Watermark.TextTransparency = 150;
                        cancelPurchaseOrderReport.Watermark.ShowBehind = false;
                        cancelPurchaseOrderReport.Watermark.PageRange = "1-99";
                        cancelPurchaseOrderReport.DataSource = cancelPurchaseOrderReport.printTable(orderDetail, order, site);
                        cancelPurchaseOrderReport.ExportToPdf(stream);
                        break;
                    case "CC Inventory"://Ajuste de inventario
                        AjusteInventario ajusteReport = new AjusteInventario();
                        var items = _ItemStockBusiness.GetInventoryAdjustmentRequestsByReference(referenceNumber);
                        if (items.Count != 0)
                        {
                            USER_MASTER username = _userMasterBusiness.GetUserMasterByUsername(items[0].User);
                            ajusteReport.DataSource = ajusteReport.printTable(items, siteName, username);
                            ajusteReport.ExportToPdf(stream);
                        }
                        else
                        {
                            InventoryCloseReport inventoryReport = new InventoryCloseReport();
                            var inventory = _inventoryReportBusiness.GetInventoryReportDetail(referenceNumber);
                            var endDate = _inventoryReportBusiness.GetInventoryHeader(referenceNumber);
                            if (inventory.Count != 0)
                            {
                                inventoryReport.DataSource = inventoryReport.printTable(inventory, siteName, referenceNumber, endDate.endDate.ToString());
                                inventoryReport.ExportToPdf(stream);
                            }
                            else { stream = null; }
                        }
                        break;
                    case "GI From Blocked (RMA)": // Devolucion RMA
                        var DamagedsGoods = _RmaHeadBusiness.GetRMAHeaderDetail(referenceNumber);
                        RMAReportSuppliercs RMAReport = new RMAReportSuppliercs();
                        if (DamagedsGoods[0].Print_Document != 0)
                        {
                            RMAReport.Watermark.Text = "RE-IMPRESION";
                            RMAReport.Watermark.TextDirection = DevExpress.XtraPrinting.Drawing.DirectionMode.ForwardDiagonal;
                            RMAReport.Watermark.Font = new Font(RMAReport.Watermark.Font.FontFamily, 40);
                            RMAReport.Watermark.ForeColor = Color.DodgerBlue;
                            RMAReport.Watermark.TextTransparency = 150;
                            RMAReport.Watermark.ShowBehind = false;
                            RMAReport.Watermark.PageRange = "1-99";
                            RMAReport.DataSource = RMAReport.printTable(DamagedsGoods[0].document_no, DamagedsGoods[0]);
                            var print = _RmaHeadBusiness.DamagedsGoodsEditStatusPrint(DamagedsGoods[0].document_no, DamagedsGoods[0].Print_Document, "RPD001.CSHTML");
                        }
                        else
                        {
                            RMAReport.DataSource = RMAReport.printTable(DamagedsGoods[0].document_no, DamagedsGoods[0]);
                            var print = _RmaHeadBusiness.DamagedsGoodsEditStatusPrint(DamagedsGoods[0].document_no, DamagedsGoods[0].Print_Document, "RPD001.CSHTML");
                        }
                        RMAReport.ExportToPdf(stream);
                        break;
                    case "REVERSE GI From Blocked (RMA)": // Reversa devolucion RMA
                        var DamagedsGoodsReverse = _RmaHeadBusiness.GetRMAHeaderDetail(referenceNumber);
                        RMAApproveCancelRequest RMAReportReverse = new RMAApproveCancelRequest();
                        RMAReportReverse.DataSource = RMAReportReverse.printTable(DamagedsGoodsReverse[0]);
                        RMAReportReverse.ExportToPdf(stream);
                        break;
                    case "GI From Blocked (REMISION)": // Reversa devolucion RMA
                        var Remision = _RmaHeadBusiness.DamagedGoodsPrint(referenceNumber);
                        List<DamagedsGoodsModel> documentItems = _DamagedsGoodsItemBusiness.GetItemsByDocumentReport(Remision.DamagedGoodsDoc, Remision.ProcessStatus);
                        ApprovalBanati RMAReportRemision = new ApprovalBanati();
                        Remision.DamageGoodsItem = documentItems;
                        if (Remision.Print_Document != 0)
                        {
                            RMAReportRemision.Watermark.Text = "RE-IMPRESION";
                            RMAReportRemision.Watermark.TextDirection = DevExpress.XtraPrinting.Drawing.DirectionMode.ForwardDiagonal;
                            RMAReportRemision.Watermark.Font = new Font(RMAReportRemision.Watermark.Font.FontFamily, 40);
                            RMAReportRemision.Watermark.ForeColor = Color.DodgerBlue;
                            RMAReportRemision.Watermark.TextTransparency = 150;
                            RMAReportRemision.Watermark.ShowBehind = false;
                            RMAReportRemision.Watermark.PageRange = "1-99";
                            RMAReportRemision.DataSource = RMAReportRemision.printTable(Remision.document_no, Remision);
                            var print = _RmaHeadBusiness.DamagedsGoodsEditStatusPrint(referenceNumber, Remision.Print_Document, "RPD001.CSHTML");
                        }
                        else
                        {
                            RMAReportRemision.DataSource = RMAReportRemision.printTable(Remision.document_no, Remision);
                            var print = _RmaHeadBusiness.DamagedsGoodsEditStatusPrint(referenceNumber, Remision.Print_Document, "RPD001.CSHTML");
                        }
                        RMAReportRemision.ExportToPdf(stream);
                        break;
                    case "GI From Blocked (SCRAP)":
                        ConfirmacionSalidaMerma reporteScrap = new ConfirmacionSalidaMerma();
                        List<DamagedsGoodsModel> ScrapItems = _DamagedsGoodsItemBusiness.NewGetItemsByDocumentReportStatus8or9(referenceNumber);
                        var ScrapHeader = _DamagedsGoodsItemBusiness.GetDocumentGoods(referenceNumber);
                        if (ScrapHeader.Print_Status != 0 && (ScrapHeader.Status == 9 && ScrapHeader.Status == 8))
                        {
                            reporteScrap.Watermark.Text = "RE-IMPRESION";
                            reporteScrap.Watermark.TextDirection = DevExpress.XtraPrinting.Drawing.DirectionMode.ForwardDiagonal;
                            reporteScrap.Watermark.Font = new Font(reporteScrap.Watermark.Font.FontFamily, 40);
                            reporteScrap.Watermark.ForeColor = Color.DodgerBlue;
                            reporteScrap.Watermark.TextTransparency = 150;
                            reporteScrap.Watermark.ShowBehind = false;
                            reporteScrap.Watermark.PageRange = "1-99";
                            reporteScrap.DataSource = reporteScrap.printTable(ScrapItems, referenceNumber, ScrapHeader.SiteName, ScrapHeader.Fecha);
                            var print = _RmaHeadBusiness.DamagedsGoodsEditStatusPrint(referenceNumber, ScrapHeader.Print_Status, "RPD001.CSHTML");
                        }
                        else
                        {
                            reporteScrap.DataSource = reporteScrap.printTable(ScrapItems, referenceNumber, ScrapHeader.SiteName, ScrapHeader.Fecha);
                            var print = _RmaHeadBusiness.DamagedsGoodsEditStatusPrint(referenceNumber, ScrapHeader.Print_Status, "RPD001.CSHTML");
                        }
                        reporteScrap.ExportToPdf(stream);
                        break;
                    case "REVERSA DE MERMA":
                        ConfirmacionSalidaMerma reporteScrapCanc = new ConfirmacionSalidaMerma();
                        List<DamagedsGoodsModel> ScrapItemsCanc = _DamagedsGoodsItemBusiness.NewGetItemsByDocumentReportStatus8or9(referenceNumber);
                        var ScrapHeaderCanc = _DamagedsGoodsItemBusiness.GetDocumentGoods(referenceNumber);
                        if (ScrapHeaderCanc.Print_Status != 0 && (ScrapHeaderCanc.Status == 9 && ScrapHeaderCanc.Status == 8))
                        {
                            reporteScrapCanc.Watermark.Text = "RE-IMPRESION";
                            reporteScrapCanc.Watermark.TextDirection = DevExpress.XtraPrinting.Drawing.DirectionMode.ForwardDiagonal;
                            reporteScrapCanc.Watermark.Font = new Font(reporteScrapCanc.Watermark.Font.FontFamily, 40);
                            reporteScrapCanc.Watermark.ForeColor = Color.DodgerBlue;
                            reporteScrapCanc.Watermark.TextTransparency = 150;
                            reporteScrapCanc.Watermark.ShowBehind = false;
                            reporteScrapCanc.Watermark.PageRange = "1-99";
                            reporteScrapCanc.DataSource = reporteScrapCanc.printTable(ScrapItemsCanc, referenceNumber, ScrapHeaderCanc.SiteName, ScrapHeaderCanc.Fecha);
                            var print = _RmaHeadBusiness.DamagedsGoodsEditStatusPrint(referenceNumber, ScrapHeaderCanc.Print_Status, "RPD001.CSHTML");
                        }
                        else
                        {
                            reporteScrapCanc.DataSource = reporteScrapCanc.printTable(ScrapItemsCanc, referenceNumber, ScrapHeaderCanc.SiteName, ScrapHeaderCanc.Fecha);
                            var print = _RmaHeadBusiness.DamagedsGoodsEditStatusPrint(referenceNumber, ScrapHeaderCanc.Print_Status, "RPD001.CSHTML");
                        }
                        reporteScrapCanc.ExportToPdf(stream);
                        break;
                    case "TRANSFER GR":
                        var destinationSiteCode = _siteConfigBusiness.GetSiteCode();
                        var transferGR = _transferHeaderBusiness.GetTransferHeaderDocumentGR(referenceNumber, destinationSiteCode);
                        var listTransferGR = _transferHeaderBusiness.GetAllTransferDetailWithoutCancelled(transferGR.TransferDocument, transferGR.TransferSiteCode, transferGR.DestinationSiteCode);
                        var reportGR = TransferHeaderDocumentReport(transferGR, listTransferGR);
                        reportGR.ExportToPdf(stream);
                        break;
                    case "TRANSFER GI":
                        var siteT = _siteConfigBusiness.GetSiteCode();
                        var transferGI = _transferHeaderBusiness.GetTransferHeaderDocumentGI(referenceNumber, siteT);
                        var listTransferGI = _transferHeaderBusiness.GetAllTransferDetailWithoutCancelled(transferGI.TransferDocument, transferGI.TransferSiteCode, transferGI.DestinationSiteCode);
                        var reportGI = TransferHeaderDocumentReport(transferGI, listTransferGI);
                        reportGI.ExportToPdf(stream);
                        break;
                    case "GI FOR SALE":
                        SalesDetailsKardexReport reportSale = new SalesDetailsKardexReport();
                        var salesDetails = _salesDetailBusiness.GetSalesDetailsBySaleId(Convert.ToInt64(referenceNumber));
                        reportSale.DataSource = reportSale.printTable(salesDetails, referenceNumber.ToString(), _salesBusiness.GetSaleByNumber(Convert.ToInt64(referenceNumber)).sale_date.Date.ToString("dd/MM/yyyy"), "", _siteConfigBusiness.GetSiteCodeName());
                        reportSale.ExportToPdf(stream);
                        break;
                    case "GI REVERSE FOR SALE":
                        SalesDetailsKardexReport reportSaleReverse = new SalesDetailsKardexReport();
                        var salesDetailsRev = _salesDetailBusiness.GetSalesDetailsBySaleIdReverse(Convert.ToInt64(referenceNumber));
                        reportSaleReverse.DataSource = reportSaleReverse.printTable(salesDetailsRev, referenceNumber.ToString(), _salesBusiness.GetSaleByNumber(Convert.ToInt64(referenceNumber)).sale_date.Date.ToString("dd/MM/yyyy"), "", _siteConfigBusiness.GetSiteCodeName());
                        reportSaleReverse.ExportToPdf(stream);
                        break;
                    case "GR_WITHOUT_COST":
                        var entryFree = _entryFreeBusiness.GetEntryFreeReport(Convert.ToInt32(referenceNumber));
                        EntryReport reportEntryFree = new EntryReport();
                        if (entryFree.Print_status != 0)
                        {
                            reportEntryFree.Watermark.Text = "RE-IMPRESION";
                            reportEntryFree.Watermark.TextDirection = DevExpress.XtraPrinting.Drawing.DirectionMode.ForwardDiagonal;
                            reportEntryFree.Watermark.Font = new Font(reportEntryFree.Watermark.Font.FontFamily, 40);
                            reportEntryFree.Watermark.ForeColor = Color.DodgerBlue;
                            reportEntryFree.Watermark.TextTransparency = 150;
                            reportEntryFree.Watermark.ShowBehind = false;
                            reportEntryFree.Watermark.PageRange = "1-99";
                            var print = _entryFreeBusiness.EntryFreeEditStatusPrint(Convert.ToInt32(referenceNumber), entryFree.Print_status, "RPD001.CSHTML");
                        }
                        else
                        {
                            var print = _entryFreeBusiness.EntryFreeEditStatusPrint(Convert.ToInt32(referenceNumber), entryFree.Print_status, "RPD001.CSHTML");
                        }
                        reportEntryFree.DataSource = reportEntryFree.printTable(entryFree);
                        reportEntryFree.ExportToPdf(stream);
                        break;
                    case "Salida Ajuste Combo":
                        SalesDetailsKardexReport reportSaleCombo = new SalesDetailsKardexReport();
                        var salesDetailsCombo = _salesDetailBusiness.GetSalesDetailsBySaleId(Convert.ToInt64(referenceNumber));
                        reportSaleCombo.DataSource = reportSaleCombo.printTable(salesDetailsCombo, referenceNumber.ToString(), _salesBusiness.GetSaleByNumber(Convert.ToInt64(referenceNumber)).sale_date.Date.ToString("dd/MM/yyyy"), "", _siteConfigBusiness.GetSiteCodeName());
                        reportSaleCombo.ExportToPdf(stream);
                        break;
                    case "Entrada Ajuste Combo":
                        SalesDetailsKardexReport reportSaleReverseCombo = new SalesDetailsKardexReport();
                        var salesDetailsRevCombo = _salesDetailBusiness.GetSalesDetailsBySaleIdReverse(Convert.ToInt64(referenceNumber));
                        reportSaleReverseCombo.DataSource = reportSaleReverseCombo.printTable(salesDetailsRevCombo, referenceNumber.ToString(), _salesBusiness.GetSaleByNumber(Convert.ToInt64(referenceNumber)).sale_date.Date.ToString("dd/MM/yyyy"), "", _siteConfigBusiness.GetSiteCodeName());
                        reportSaleReverseCombo.ExportToPdf(stream);
                        break;
                    case "GI COST CENTER":
                        CostCenterDocument reportCostCenter = new CostCenterDocument();
                        var modelCostCenter = _costCenterBusiness.GetGICostCenterReport(document_no);
                        reportCostCenter.DataSource = reportCostCenter.PrintTable(modelCostCenter);
                        reportCostCenter.ExportToPdf(stream);
                        break;
                    default:
                        stream = null;
                        break;
                }
                if (stream != null)
                    return Json(new { success = true, responseText = Convert.ToBase64String(stream.ToArray()) }, JsonRequestBehavior.AllowGet);
                else
                    return Json(new { success = false, responseText = "No podemos mostrar archivo, contacta a sistemas." }, JsonRequestBehavior.AllowGet);
            }
            else
                return Json(new { success = false, responseText = "Termino tu sesión." }, JsonRequestBehavior.AllowGet);
        }
        public TransferReport TransferHeaderDocumentReport(TransferHeaderModel transfer, List<TransferDetailModel> list)
        {
            TransferReport report = new TransferReport();
            if (transfer.TransferStatus == 8 || transfer.TransferStatus == 2)
            {
                report.Watermark.Text = "CANCELADO";
                report.Watermark.TextDirection = DevExpress.XtraPrinting.Drawing.DirectionMode.ForwardDiagonal;
                report.Watermark.Font = new Font(report.Watermark.Font.FontFamily, 60);
                report.Watermark.ForeColor = Color.Firebrick;
                report.Watermark.TextTransparency = 150;
                report.Watermark.ShowBehind = false;
                report.Watermark.PageRange = "1-99";
            }
            else
            {
                if (transfer.PrintStatus == 1 || transfer.PrintStatus == 2)
                {
                    report.Watermark.Text = "RE-IMPRESION";
                    report.Watermark.TextDirection = DevExpress.XtraPrinting.Drawing.DirectionMode.ForwardDiagonal;
                    report.Watermark.Font = new Font(report.Watermark.Font.FontFamily, 40);
                    report.Watermark.ForeColor = Color.DodgerBlue;
                    report.Watermark.TextTransparency = 150;
                    report.Watermark.ShowBehind = false;
                    report.Watermark.PageRange = "1-99";
                    _transferHeaderBusiness.TransferHeaderEditStatusPrint(transfer.TransferDocument, 2, Session["User"].ToString());
                }
                if (transfer.PrintStatus == 0)
                {
                    _transferHeaderBusiness.TransferHeaderEditStatusPrint(transfer.TransferDocument, 1, Session["User"].ToString());
                }
            }

            report.DataSource = report.printTable(list, transfer);
            return report;
        }
        [HttpPost]
        public ActionResult ActionGenerateReportMovementsItem(List<ItemKardexModel> items, string part_number, string part_description, string dateRange)
        {
            if (Session["User"] != null)
            {
                var stream = new MemoryStream();
                string siteName = _siteConfigBusiness.GetSiteCodeName();
                MovementsItemReport report = new MovementsItemReport();
                report.DataSource = report.printTable(items, siteName, part_number, part_description, dateRange);
                report.ExportToPdf(stream);
                if (stream != null)
                    return Json(new { success = true, responseText = Convert.ToBase64String(stream.ToArray()) }, JsonRequestBehavior.AllowGet);
                else
                    return Json(new { success = false, responseText = "No podemos mostrar archivo, contacta a sistemas." }, JsonRequestBehavior.AllowGet);
            }
            else
                return Json(new { success = false, responseText = "Termino tu sesión." }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult ActionGenerateReportInventoryDaysReport(string proveedor, string depto, string familia, string date1, string date2, string excessDays)
        {
            if (Session["User"] != null)
            {
                var movements = _ItemBusiness.GetAllItemsInventoryDays(proveedor, depto, familia, excessDays);

                var stream = new MemoryStream();
                string siteName = _siteConfigBusiness.GetSiteCodeName();
                InventoryDaysReport report = new InventoryDaysReport();
                report.DataSource = report.printTable(movements, siteName, excessDays);
                report.ExportToPdf(stream);
                if (movements.Count == 0)
                    return Json(new { success = false, responseText = "No se encontraron productos para imprimir" }, JsonRequestBehavior.AllowGet);
                else if (stream != null && movements.Count != 0)
                    return Json(new { success = true, responseText = Convert.ToBase64String(stream.ToArray()) }, JsonRequestBehavior.AllowGet);
                else
                    return Json(new { success = false, responseText = "No podemos mostrar archivo, contacta a sistemas." }, JsonRequestBehavior.AllowGet);
            }
            else
                return Json(new { success = false, responseText = "Terminó tu sesión." }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult ActionGetInventoryDays(string proveedor, string depto, string familia, string excessDays)
        {
            if (Session["User"] != null)
            {
                var movements = _ItemBusiness.GetAllItemsInventoryDays(proveedor, depto, familia, excessDays);
                if (movements != null)
                    return new JsonResult() { Data = new { success = true, ItemMovements = movements }, JsonRequestBehavior = JsonRequestBehavior.AllowGet, MaxJsonLength = int.MaxValue };
                else
                    return Json(new { success = false, responseText = "Sin movimientos en el rango de fecha para el Producto Seleccionado." }, JsonRequestBehavior.AllowGet);
            }
            else
                return Json(new { success = false, responseText = "Terminó tu sesión." }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult ActionGetProductsWithoutSales(int SupplierId, int DepartmentId, int FamilyId, int Days)
        {
            if (Session["User"] != null)
                return Json(new { Status = true, Model = _ItemBusiness.GetProductsWithoutSales(SupplierId, DepartmentId, FamilyId, Days) }, JsonRequestBehavior.AllowGet);

            return Json(new { Status = false }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult ActionGetHistoricInventory(DateTime Day)
        {
            if (Session["User"] != null)
                return Json(new { Status = true, Model = _ItemBusiness.GetHistoricInventory(Day) }, JsonRequestBehavior.AllowGet);

            return Json(new { Status = false }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult ActionGenerateReportInventoryWithoutSales(List<ItemSalesInventory> items)
        {
            if (Session["User"] != null)
            {
                var stream = new MemoryStream();
                string siteName = _siteConfigBusiness.GetSiteCodeName();
                InventoryWithoutSales report = new InventoryWithoutSales();
                report.DataSource = report.printTable(items, siteName);
                report.ExportToPdf(stream);
                if (stream != null)
                    return Json(new { success = true, responseText = Convert.ToBase64String(stream.ToArray()) }, JsonRequestBehavior.AllowGet);
                else
                    return Json(new { success = false, responseText = "No podemos mostrar archivo, contacta a sistemas." }, JsonRequestBehavior.AllowGet);
            }
            else
                return Json(new { success = false, responseText = "Terminó tu sesión." }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult ActionGetMissingProducts(int SupplierId, int DepartmentId, int FamilyId)
        {
            if (Session["User"] != null)
                return Json(new { Status = true, Model = _ItemBusiness.GetMissingProducts(SupplierId, DepartmentId, FamilyId) }, JsonRequestBehavior.AllowGet);

            return Json(new { Status = false }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult ActionGenerateReportMissingCodes(List<ItemSalesInventory> items)
        {
            if (Session["User"] != null)
            {
                var stream = new MemoryStream();
                string siteName = _siteConfigBusiness.GetSiteCodeName();
                MissingCodes report = new MissingCodes();
                report.DataSource = report.printTable(items, siteName);
                report.ExportToPdf(stream);
                if (stream != null)
                    return Json(new { success = true, responseText = Convert.ToBase64String(stream.ToArray()) }, JsonRequestBehavior.AllowGet);
                else
                    return Json(new { success = false, responseText = "No podemos mostrar archivo, contacta a sistemas." }, JsonRequestBehavior.AllowGet);
            }

            return Json(new { success = false, responseText = "Terminó tu sesión." }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult ActionCostInfo(string PartNumber, int Supplier)
        {
            if (Session["User"] != null) {
                var r = _ItemBusiness.GetCostItem(PartNumber, Supplier);
                return Json(r, JsonRequestBehavior.AllowGet);
            }
            else
                return Json("SF", JsonRequestBehavior.AllowGet);
        }

        public JsonResult ActionGetItemDescription(string part_number)
        {
            var item = _ItemBusiness.GetOneItem(part_number);

            if (item == null) return Json(new { success = false, error = $"No se encontro producto" }, JsonRequestBehavior.AllowGet);
            return Json(new { success = true, item.Description }, JsonRequestBehavior.AllowGet);
        }

        public JsonResult ActionGetItemIvaIepsValue(string part_number)
        {
            if (Session["User"] == null) return Json("SF", JsonRequestBehavior.AllowGet);
            var data = _ItemBusiness.StoreProcedureIvaIeps(part_number);
            if (data == null) return Json(new { success = false }, JsonRequestBehavior.AllowGet);
            data.Ieps /= 100;
            data.Iva /= 100;
            return Json(new { success = true, Data = data }, JsonRequestBehavior.AllowGet);
        }

        public JsonResult ActionSearchCode(string search, bool exact = false, bool barcode = true, bool presentation = true, bool planogram = true)
        {
            var data = _ItemBusiness.SearchCode(search, exact, barcode, presentation, planogram);
            return Json(data, JsonRequestBehavior.AllowGet);
        }

        public JsonResult ActionGetItemsByDepartment(int DepartmentId)
        {
            if (Session["User"] == null) return Json("SF", JsonRequestBehavior.AllowGet);

            List<ItemModelDropDown> data;
            if (DepartmentId != 0)
                data = _ItemBusiness.GetItemsByDepartment(DepartmentId);
            else
                data = _ItemBusiness.GetAllProductsToDropDown();

            return Json(new { success = true, Data = data }, JsonRequestBehavior.AllowGet);
        }

        public JsonResult ActionGetItemsByFamily(int FamilyId, int DepartmentId)
        {
            if (Session["User"] == null) return Json("SF", JsonRequestBehavior.AllowGet);

            List<ItemModelDropDown> data;
            if (FamilyId != 0)
                data = _ItemBusiness.GetItemsByFamily(FamilyId);
            else if (DepartmentId != 0)
                data = _ItemBusiness.GetItemsByDepartment(DepartmentId);
            else
                data = _ItemBusiness.GetAllProductsToDropDown();

            return Json(new { success = true, Data = data }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult ITEM002()
        {
            var Model = new ItemQuickCheckIndex();
            return View(Model);
        }

        [HttpPost]
        public ActionResult ITEM002(ItemQuickCheckIndex Model)
        {
            Model.IsFromMenu = false;
            Model.ItemList = _ItemBusiness.GetItemQuickCheckList(Model);
            return View(Model);
        }

        public JsonResult ActionGetItemsByDepartmentInventory(int id)
        {
            if (Session["User"] == null) return Json("SF", JsonRequestBehavior.AllowGet);
            var data = new List<ItemModelDropDown>();
            if (id != 0)
                data = _ItemBusiness.GetItemsByFamilyActive(id);
            return Json(new { success = true, Data = data }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult ActionReportInventoryCC(string document, string part_number)
        {
            if (Session["User"] != null)
            {
                var stream = new MemoryStream();
                var infoIventoryCC = _ItemBusiness.GetInformationInventoryCCFinal(document, part_number);
                var infoSite = _siteBusiness.GetInfoSite();
                var model = _ItemStockBusiness.SetIvaIepsInventoryCC(infoIventoryCC);
                SolicitudAjusteInventario reporte = new SolicitudAjusteInventario();

                if (infoIventoryCC.print_status >= 1 && infoIventoryCC.DocumentStatus != "Pendiente") //Estatus pendiente: no contarlo
                {
                    reporte.Watermark.Text = "RE-IMPRESION";
                    reporte.Watermark.TextDirection = DevExpress.XtraPrinting.Drawing.DirectionMode.ForwardDiagonal;
                    reporte.Watermark.Font = new Font(reporte.Watermark.Font.FontFamily, 40);
                    reporte.Watermark.ForeColor = Color.DodgerBlue;
                    reporte.Watermark.TextTransparency = 150;
                    reporte.Watermark.ShowBehind = false;
                    reporte.Watermark.PageRange = "1-99";
                    var print = _ItemBusiness.InventoryCCEditStatusPrint(document, part_number, infoIventoryCC.print_status, "RPD013.cshtml");
                }
                else if (infoIventoryCC.print_status == 0 && infoIventoryCC.DocumentStatus != "Pendiente")
                {
                    var print = _ItemBusiness.InventoryCCEditStatusPrint(document, part_number, infoIventoryCC.print_status, "RPD013.cshtml");
                }
                reporte.DataSource = reporte.printTable(model, infoIventoryCC, infoSite);
                reporte.ExportToPdf(stream);
                if (stream != null)
                    return Json(new { success = true, responseText = Convert.ToBase64String(stream.ToArray()) }, JsonRequestBehavior.AllowGet);
                else
                    return Json(new { success = false, responseText = "No podemos mostrar archivo, contacta a sistemas." }, JsonRequestBehavior.AllowGet);
            }
            return Json(new { success = false, responseText = "SF" }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult ITEM001()
        {
            return View();
        }

        [HttpPost]
        public ActionResult ActionSearchItems(string item)
        {
            if (Session["User"] != null)
                return Json(new { success = true, data = _ItemBusiness.GetItemQuickCheckListString(item) }, JsonRequestBehavior.AllowGet);

            return Json(new { success = false, data = "SF" }, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public ActionResult ActionGetItemsKardexHor(DateTime dateInitial, DateTime dateFinish, string department, string part_number)
        {
            if (Session["User"] != null)
                return new JsonResult() { Data = _ItemBusiness.GetItemsKardexHorizontal(dateInitial, dateFinish, department, part_number), JsonRequestBehavior = JsonRequestBehavior.AllowGet, MaxJsonLength = int.MaxValue };

            return Json(new { success = false, data = "SF" }, JsonRequestBehavior.AllowGet);
        }
        public ActionResult ActionSearchItemBarcodeNoFlags(string Filter)
        {
            return new JsonResult() { Data = new { Json = _ItemBusiness.SearchItemBarcodeNoFlags(Filter) }, JsonRequestBehavior = JsonRequestBehavior.AllowGet, MaxJsonLength = int.MaxValue };
        }
    }
}