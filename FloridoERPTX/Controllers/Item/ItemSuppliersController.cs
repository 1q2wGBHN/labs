﻿using App.BLL.Item;
using App.BLL.Promotions;
using App.BLL.Sales;
using App.Entities.ViewModels.Sales;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace FloridoERPTX.Controllers.Item
{
    public class ItemSuppliersController : Controller
    {
        private readonly ItemSupplierBusiness _ItemRepositoryBusiness;
        private readonly SalesPriceChangeBusiness _SalesPriceChangeBusiness;
        private readonly ItemStockBusiness _itemStockBusiness;
        private readonly ItemBusiness _itemBusines;
        private readonly PromotionBusiness _promotionBusiness;
        public ItemSuppliersController()
        {
            _ItemRepositoryBusiness = new ItemSupplierBusiness();
            _SalesPriceChangeBusiness = new SalesPriceChangeBusiness();
            _itemStockBusiness = new ItemStockBusiness();
            _itemBusines = new ItemBusiness();
            _promotionBusiness = new PromotionBusiness();
        }
        // GET: ItemSuppliers
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult ActionListItemSuppliers(int? Supplier)
        {
            if (Supplier == null || Supplier <= 0)
            {
                return Json(null, JsonRequestBehavior.AllowGet);
            }
            return new JsonResult() { Data = _ItemRepositoryBusiness.GetFindListItemSupplier(Supplier.Value), JsonRequestBehavior = JsonRequestBehavior.AllowGet, MaxJsonLength = Int32.MaxValue };
            //return Json(_ItemRepositoryBusiness.getFindListItemSupplier(Supplier.Value), JsonRequestBehavior.AllowGet);
        }

        public ActionResult ActionGetListItemCurrency(int Supplier, string Currency)
        {
            if (Session["User"] != null)
            {
                return new JsonResult() { Data = _ItemRepositoryBusiness.GetListItemCurrency(Supplier, Currency), JsonRequestBehavior = JsonRequestBehavior.AllowGet, MaxJsonLength = Int32.MaxValue };
            }
            else
                return Json("SF", JsonRequestBehavior.AllowGet);
            //return Json(_ItemRepositoryBusiness.getFindListItemSupplier(Supplier.Value), JsonRequestBehavior.AllowGet);
        }
        public ActionResult ActionGetListItemBaseCost(string PartNumber)
        {
            if (Session["User"] != null)
                return new JsonResult() { Data = new { success = true, Pre = _itemBusines.GetItemPresentationWithBarcode(PartNumber), Sup = _ItemRepositoryBusiness.GetListItemBaseCost(PartNumber), Base = _ItemRepositoryBusiness.GetItemHistoryBaseCost(PartNumber), Sale = _SalesPriceChangeBusiness.GetItemSaleHistory(PartNumber), Promo = _promotionBusiness.GetPromotionHistoryByPartNmber(PartNumber), Forced = _SalesPriceChangeBusiness.GetItemSaleForceHistory(PartNumber) }, JsonRequestBehavior = JsonRequestBehavior.AllowGet, MaxJsonLength = Int32.MaxValue };
            else
                return Json("SF", JsonRequestBehavior.AllowGet);
        }

        //trae los datos productos del proveedor 
        public ActionResult ActionListItem(int Supplier)
        {
            return Json(_ItemRepositoryBusiness.GetFindListItem(Supplier), JsonRequestBehavior.AllowGet);

        }
        public ActionResult ActionGetIeps(string parthNumber)
        {
            return Json(_ItemRepositoryBusiness.GetIeps(parthNumber), JsonRequestBehavior.AllowGet);
        }

        public ActionResult ActionGetPacking(string PathNumber)
        {
            return Json(_ItemRepositoryBusiness.GetPacking(PathNumber), JsonRequestBehavior.AllowGet);

        }
        public ActionResult ActionGetOrder(int Supplier, string Site , string Currency)
        {
            if (Session["User"] != null)
            {
                return Json(_ItemRepositoryBusiness.GetListOrder(Supplier, Site , Currency), JsonRequestBehavior.AllowGet);
            }
            return Json("SF", JsonRequestBehavior.AllowGet);
        }

        public ActionResult ActionGetItemsSupplierStock(int Supplier)
        {
            if (Session["User"] != null)
            {
                return new JsonResult() { Data = _ItemRepositoryBusiness.GetItemsSupplier(Supplier), JsonRequestBehavior = JsonRequestBehavior.AllowGet, MaxJsonLength = Int32.MaxValue };
            }
            return Json("SF", JsonRequestBehavior.AllowGet);
        }

        public ActionResult ActionGetItemsBySupplierCodeInSalePrice(string supplier_code)
        {
            if (Session["User"] != null)
            {
                var sCode = int.Parse(supplier_code);
                var supplier = _ItemRepositoryBusiness.GetItemsBySupplierCodeInSalePrice(sCode);
                if (supplier != null)
                    return Json(new { success = true, Json = supplier }, JsonRequestBehavior.AllowGet);
                else
                    return Json(new { success = false, responseText = "Proveedor sin productos con precio asignado." }, JsonRequestBehavior.AllowGet);
            }
            else
                return Json(new { success = false, responseText = "Termino tu sesion." }, JsonRequestBehavior.AllowGet);
        }
        
        public ActionResult ActionGetStockBySupplier(int supplier_id, int class_id, string category, int family_id)
        {
            if (Session["User"] != null)
            {
                return new JsonResult() { Data = new { success = true, Json = _itemStockBusiness.GetStockBySupplier(supplier_id, class_id, category, family_id) }, JsonRequestBehavior = JsonRequestBehavior.AllowGet, MaxJsonLength = Int32.MaxValue };
            }
            else
                return Json(new { success = false, responseText = "SF" }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult ActionSaveChangePriceStores(List<SalesPriceChangeModel> itemChangePrice, string ProgramId)
        {
            var result = 0;
            if (Session["User"] != null)
            {
                var Sesion = Session["User"].ToString();
                foreach (var InformationProduct in itemChangePrice)
                {
                    result = _SalesPriceChangeBusiness.AddItemSalesPriceChange(InformationProduct.partNumber, InformationProduct.margin, InformationProduct.priceUser, Sesion, ProgramId);
                    if (result == 0)
                        return Json(new { success = false, responseText = "Error Inesperado al cambiar el precio del código: " + InformationProduct.partNumber + " , Contacte a sistemas" }, JsonRequestBehavior.AllowGet);
                }
                var success = _SalesPriceChangeBusiness.ChangeSales();
                if (success == 0)
                    return Json(new { success = true, responseText = "Se ha Guardado exitosamente" }, JsonRequestBehavior.AllowGet);
                else
                    return Json(new { success = false, responseText = "Error al Actulizar los estatus. Contacte a sistemas." }, JsonRequestBehavior.AllowGet);



            }
            else
            {
                return Json(new { success = false, responseText = "Tu sesion termino." }, JsonRequestBehavior.AllowGet);
            }
        }
        public ActionResult ActionGetAllItemsSuppliers(int supplier_id)
        {
            if (Session["User"] != null)
            {
                return Json(new { success = true, Data = _ItemRepositoryBusiness.GetFindListItemSupplier(supplier_id) }, JsonRequestBehavior.AllowGet);
            }
            else
                return Json(new { success = false, responseText = "SF" }, JsonRequestBehavior.AllowGet);
        }
    }
}