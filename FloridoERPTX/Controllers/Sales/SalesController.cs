﻿using App.BLL.Sales;
using System;
using System.Web.Mvc;
using App.Entities.ViewModels.Sales;
using FloridoERPTX.Reports;
using DevExpress.XtraReports.UI;
using App.Entities.ViewModels.Common;
using App.BLL.Site;
using System.Collections.Generic;
using System.Linq;
using App.BLL.Lookups;
using App.BLL;
using App.Entities.ViewModels.SalesPriceChange;
using FloridoERP.Report;
using FloridoERPTX.ViewModels;
using App.Entities;
using CustomerSalesViewModel = App.Entities.ViewModels.Sales.CustomerSalesViewModel;
using App.BLL.MaClass;
using App.BLL.Item;
using static App.Entities.ViewModels.Common.Constants;
using FloridoERPTX.CommonMethods;

namespace FloridoERPTX.Controllers.Sales
{
    public class SalesController : Controller
    {
        private SalesBusiness _SalesBussines;
        private SalesDetailBusiness _SalesDetailBusiness;
        private SiteConfigBusiness _SiteConfigBusiness;
        private PortadaBusiness _PortadaBusiness;
        private PaymentMethodBusiness _PaymentMethodBusiness;
        private UserMasterBusiness _UserMasterBusiness;
        private SalesPaymentMethodBusiness _SalesPaymentMethodBusiness;
        private CurrencyDetailBusiness _CurrencyDetailBusiness;
        private SalesPriceChangeBusiness _SalesPriceChangeBusiness;
        private SalesCancellationBusiness _SalesCancellationBusiness;
        private MaClassBusiness _MaClassBusiness;
        private ItemBusiness _ItemBusiness;
        private CashierLookup _CashierLookup;
        private readonly ItemSalesBusiness _ItemSalesBusiness;

        public SalesController()
        {
            _SalesBussines = new SalesBusiness();
            _SalesDetailBusiness = new SalesDetailBusiness();
            _SiteConfigBusiness = new SiteConfigBusiness();
            _PortadaBusiness = new PortadaBusiness();
            _PaymentMethodBusiness = new PaymentMethodBusiness();
            _UserMasterBusiness = new UserMasterBusiness();
            _SalesPaymentMethodBusiness = new SalesPaymentMethodBusiness();
            _CurrencyDetailBusiness = new CurrencyDetailBusiness();
            _SalesPriceChangeBusiness = new SalesPriceChangeBusiness();
            _SalesCancellationBusiness = new SalesCancellationBusiness();
            _MaClassBusiness = new MaClassBusiness();
            _ItemBusiness = new ItemBusiness();
            _CashierLookup = new CashierLookup();
            _ItemSalesBusiness = new ItemSalesBusiness();
        }

        // GET: Sales/Details/5
        public ActionResult ActionGetSaleById(int SaleId)
        {
            var Sale = _SalesBussines.GetSaleByNumber(SaleId);
            return View();
        }

        public ActionResult SALE005()
        {
            //Return true when Cancel Bonification
            return RedirectToAction("ActionCRNO005", "CreditNotes", new { IsBonification = true });
        }

        public ActionResult SALE006()
        {
            //Return false when Cancel Credit Note
            return RedirectToAction("ActionCRNO005", "CreditNotes", new { IsBonification = false });
        }

        #region Reports

        #region Sales Report
        public ActionResult SALE001()
        {
            var model = new SalesIndex();
            model.StartDate = DateTime.Now.Date.ToString(DateFormat.INT_DATE);
            model.EndDate = DateTime.Now.Date.ToString(DateFormat.INT_DATE);
            FillLookupCajas(_SalesBussines.GetCajas());
            ViewBag.Cashier = _CashierLookup.FillLookupCashiers(CashiersLookup.BySale);
            return View(model);
        }

        [HttpPost]
        public ActionResult SALE001(SalesIndex model)
        {
            FillLookupCajas(_SalesBussines.GetCajas());
            ViewBag.Cashier = _CashierLookup.FillLookupCashiers(CashiersLookup.BySale);
            return View(model);
        }

        [HttpGet]
        public ActionResult ActionGetSales(SalesIndex Model)
        {
            var _salesList = _SalesBussines.GetSalesReport(Model);
            if (_salesList != null)
                return new JsonResult() { Data = new { success = true, Json = _salesList }, JsonRequestBehavior = JsonRequestBehavior.AllowGet, MaxJsonLength = Int32.MaxValue };
            else
                return Json(new { success = false }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult ActionGetSalesDetail(long saleId)
        {
            return Json(new { success = true, Json = _SalesDetailBusiness.getSalesDetailBySaleId(saleId) }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult ActionGetSalesDetailReport(long saId)
        {
            SalesReportDetail report = new SalesReportDetail();
            var saleD = _SalesBussines.GetSaleBySaleId((long)saId);
            string saDate = saleD[0].sale_date.ToString();
            string saInvoice = saleD[0].sale_factura.ToString();
            string siteName = _SiteConfigBusiness.GetSiteCodeName();
            (report.Bands["DetailReport"] as DetailReportBand).DataSource = report.printTable(_SalesDetailBusiness.getSalesDetailBySaleId(saId), saId.ToString(), saDate.ToString(), saInvoice, false, siteName, saleD[0].customer.ToString(), "");
            (report.Bands["DetailReport"] as DetailReportBand).DataMember = "xrTableRowFormat";
            return PartialView("~/Views/ReportsViews/SaleReportView.cshtml", report);

        }

        [HttpPost]
        public ActionResult ActionGetSalesReport(SalesIndex Model)
        {
            SalesReport report = new SalesReport();
            string siteName = _SiteConfigBusiness.GetSiteCodeName();
            var _salesList = _SalesBussines.GetSalesReport(Model);
            (report.Bands["DetailReport"] as DetailReportBand).DataSource = report.printTable(_salesList, Model.StartDate, Model.EndDate, siteName);
            (report.Bands["DetailReport"] as DetailReportBand).DataMember = "xrTableRowFormat";

            return PartialView("~/Views/ReportsViews/SaleReportList.cshtml", report);
        }
        #endregion

        #region Sales Concentrated Report
        public ActionResult SALE002()
        {
            var model = new SalesConcIndex();
            model.StartDate = DateTime.Now.Date.AddDays(-1).ToString(DateFormat.INT_DATE);
            model.EndDate = DateTime.Now.Date.ToString(DateFormat.INT_DATE);
            model.Sales = _SalesBussines.GetSalesConcretated(model);

            return View(model);

        }

        [HttpPost]
        public ActionResult SALE002(SalesConcIndex model)
        {
            model.Sales = _SalesBussines.GetSalesConcretated(model);
            return View(model);
        }

        [HttpPost]
        public ActionResult ActionReportGral(SalesConcIndex model)
        {
            //getEstados(model);
            SalesConcentrated report = new SalesConcentrated();
            string siteName = _SiteConfigBusiness.GetSiteCodeName();
            (report.Bands["DetailReport"] as DetailReportBand).DataSource = report.printTable(model.Sales, model.StartDate, model.EndDate, siteName);
            (report.Bands["DetailReport"] as DetailReportBand).DataMember = "xrTableRowFormat";

            return PartialView("~/Views/ReportsViews/SalesConcentrated.cshtml", report);
        }
        #endregion

        #region Chingon Report
        //reporte portada
        public ActionResult SALE008()
        {
            var model = new PortadaModel();

            var _date = DateTime.Now.Date.ToString(DateFormat.INT_DATE);
            model = _PortadaBusiness.PopulatePortadaModel(_date);
            return View(model);

        }
        [HttpPost]
        public ActionResult SALE008(PortadaModel model)
        {
            model = _PortadaBusiness.PopulatePortadaModel(model.ReportDate);
            return View(model);
        }

        [HttpPost]
        public ActionResult ActionReportGralPortada(PortadaModel model, decimal[] totalesMXN, decimal[] totalesUSD)
        {
            //SalesConcentrated report = new SalesConcentrated();
            model.TotalesMXN = new decimal[7, 3];
            model.TotalesDLLS = new decimal[7, 3];
            model.TotalesMXN[0, 0] = totalesMXN[0]; model.TotalesMXN[0, 1] = totalesMXN[1]; model.TotalesMXN[0, 2] = totalesMXN[2];
            model.TotalesMXN[1, 0] = totalesMXN[3]; model.TotalesMXN[1, 1] = totalesMXN[4]; model.TotalesMXN[1, 2] = totalesMXN[5];
            model.TotalesMXN[2, 0] = totalesMXN[6]; model.TotalesMXN[2, 1] = totalesMXN[7]; model.TotalesMXN[2, 2] = totalesMXN[8];
            model.TotalesMXN[3, 0] = totalesMXN[9]; model.TotalesMXN[3, 1] = totalesMXN[10]; model.TotalesMXN[3, 2] = totalesMXN[11];
            model.TotalesMXN[4, 0] = totalesMXN[12]; model.TotalesMXN[4, 1] = totalesMXN[13]; model.TotalesMXN[4, 2] = totalesMXN[14];
            model.TotalesMXN[5, 0] = totalesMXN[15]; model.TotalesMXN[5, 1] = totalesMXN[16]; model.TotalesMXN[5, 2] = totalesMXN[17];
            model.TotalesMXN[6, 0] = totalesMXN[18]; model.TotalesMXN[6, 1] = totalesMXN[19]; model.TotalesMXN[6, 2] = totalesMXN[20];

            model.TotalesDLLS[0, 0] = totalesUSD[0]; model.TotalesDLLS[0, 1] = totalesUSD[1]; model.TotalesDLLS[0, 2] = totalesUSD[2];
            model.TotalesDLLS[1, 0] = totalesUSD[3]; model.TotalesDLLS[1, 1] = totalesUSD[4]; model.TotalesDLLS[1, 2] = totalesUSD[5];
            model.TotalesDLLS[2, 0] = totalesUSD[6]; model.TotalesDLLS[2, 1] = totalesUSD[7]; model.TotalesDLLS[2, 2] = totalesUSD[8];
            model.TotalesDLLS[3, 0] = totalesUSD[9]; model.TotalesDLLS[3, 1] = totalesUSD[10]; model.TotalesDLLS[3, 2] = totalesUSD[11];
            model.TotalesDLLS[4, 0] = totalesUSD[12]; model.TotalesDLLS[4, 1] = totalesUSD[13]; model.TotalesDLLS[4, 2] = totalesUSD[14];
            model.TotalesDLLS[5, 0] = totalesUSD[15]; model.TotalesDLLS[5, 1] = totalesUSD[16]; model.TotalesDLLS[5, 2] = totalesUSD[17];
            model.TotalesDLLS[6, 0] = totalesUSD[16]; model.TotalesDLLS[6, 1] = totalesUSD[19]; model.TotalesDLLS[6, 2] = totalesUSD[20];


            Portada report = new Portada();
            string siteName = _SiteConfigBusiness.GetSiteCodeName();
            (report.Bands["DetailReport"] as DetailReportBand).DataSource = report.printTable(model, siteName);
            (report.Bands["DetailReport"] as DetailReportBand).DataMember = "xrTableRowFormat";
            return PartialView("~/Views/ReportsViews/PortadaView.cshtml", report);
        }
        #endregion

        #region Charge Relation Sales Report

        public ActionResult SALE009()
        {
            var Model = new ChargeRelationSalesIndex();
            ViewBag.Cashier = _CashierLookup.FillLookupCashiers(CashiersLookup.ByChargeRelation);
            ViewBag.PaymentMethod = new SelectList(_PaymentMethodBusiness.PaymentMethodList(), "pm_id", "pm_name");
            return View(Model);
        }

        [HttpPost]
        public ActionResult SALE009(ChargeRelationSalesIndex Model)
        {
            Model.DataReport = _SalesPaymentMethodBusiness.GetChargeRelationSalesData(Model);
            Model.IsFromMenu = false;
            Model.CurrencyRate = _CurrencyDetailBusiness.GetCurrencyRateByDate(Model.Date);
            ViewBag.Cashier = _CashierLookup.FillLookupCashiers(CashiersLookup.ByChargeRelation);
            ViewBag.PaymentMethod = new SelectList(_PaymentMethodBusiness.PaymentMethodList(), "pm_id", "pm_name");
            return View(Model);
        }

        [HttpPost]
        public ActionResult ActionGetChargeRelationSalesReport(ChargeRelationSalesIndex Model)
        {
            var report = new ChargeSaleRelationReport();
            Model.SiteName = _SiteConfigBusiness.GetSiteCodeName();
            (report.Bands["DetailReport"] as DetailReportBand).DataSource = report.printTable(Model);
            (report.Bands["DetailReport"] as DetailReportBand).DataMember = "xrTableRowFormat";
            return PartialView("~/Views/ReportsViews/ReportView.cshtml", report);
        }
        #endregion

        #region Sales Price Changes Report
        public ActionResult SALE010()
        {
            var Model = new SalePriceChangeIndex();
            return View(Model);
        }

        [HttpPost]
        public ActionResult SALE010(SalePriceChangeIndex Model)
        {
            Model.IsFromMenu = false;
            Model.DataReport = _SalesPriceChangeBusiness.GetSalesPriceChangeReport(Model);
            return View(Model);
        }

        [HttpPost]
        public ActionResult ActionGetSalePriceChangeReport(SalePriceChangeIndex Model)
        {
            var report = new SalePriceChangeReport();
            (report.Bands["DetailReport"] as DetailReportBand).DataSource = report.printTable(Model);
            (report.Bands["DetailReport"] as DetailReportBand).DataMember = "xrTableRowFormat";
            return PartialView("~/Views/ReportsViews/ReportView.cshtml", report);
        }
        #endregion

        #region Cancellated Sales Report
        public ActionResult SALE011()
        {
            var Model = new SalesReportIndex();
            ViewBag.Cashier = _CashierLookup.FillLookupCashiers(CashiersLookup.ByCancalletaionsSales);
            return View(Model);
        }

        [HttpPost]
        public ActionResult SALE011(SalesReportIndex Model)
        {
            Model.IsFromMenu = false;
            Model.CancellationsReport = _SalesCancellationBusiness.GetSalesCancellationReportData(Model);
            ViewBag.Cashier = _CashierLookup.FillLookupCashiers(CashiersLookup.ByCancalletaionsSales);
            return View(Model);
        }

        [HttpPost]
        public ActionResult ActionGetSalesCancelledReport(SalesReportIndex Model)
        {
            var report = new SalesCancelatedReport();
            (report.Bands["DetailReport"] as DetailReportBand).DataSource = report.printTable(Model);
            (report.Bands["DetailReport"] as DetailReportBand).DataMember = "xrTableRowFormat";
            return PartialView("~/Views/ReportsViews/ReportView.cshtml", report);
        }
        #endregion

        #region Cashback Report
        public ActionResult SALE015()
        {
            TransactionsCardIndex Model = new TransactionsCardIndex();
            ViewBag.Cashier = _CashierLookup.FillLookupCashiers(CashiersLookup.AllCashier);
            return View(Model);
        }

        [HttpPost]
        public ActionResult SALE015(TransactionsCardIndex Model)
        {
            var User = !string.IsNullOrEmpty(Model.Cashier) ? _UserMasterBusiness.GetUserMasterByEmployeeNumber(Model.Cashier) : new App.Entities.USER_MASTER();
            Model.IsFromMenu = false;
            Model.CashierName = string.IsNullOrEmpty(Model.Cashier) ? "Todos los Cajeros" : string.Format("{0} {1}", User.first_name, User.last_name);

            Model.DataReport = _SalesBussines.GetTransactionsCardData(Model)
                .GroupBy(g => g.Fecha.Value.Date)
                .Select(cl => new TransactionsCardData
                {
                    Fecha = cl.First().Fecha.Value.Date,
                    Monto = cl.Sum(c => c.Monto),
                    Retiro = cl.Sum(c => c.Retiro),
                    Total = cl.Sum(c => c.Total)
                }).ToList();

            ViewBag.Cashier = _CashierLookup.FillLookupCashiers(CashiersLookup.AllCashier);
            return View(Model);
        }

        public ActionResult ReportDetailTransactionsCardByCashier(TransactionsCardIndex Model)
        {
            TransactionsCardByCashierReport report = new TransactionsCardByCashierReport();

            var User = !string.IsNullOrEmpty(Model.Cashier) ? _UserMasterBusiness.GetUserMasterByEmployeeNumber(Model.Cashier) : new App.Entities.USER_MASTER();
            Model.IsFromMenu = false;
            Model.CashierName = string.IsNullOrEmpty(Model.Cashier) ? "Todos los Cajeros" : string.Format("{0} {1}", User.first_name, User.last_name);

            Model.CashierReport = _SalesBussines.GetTransactionsCardByCashier(Model);

            (report.Bands["DetailReport"] as DetailReportBand).DataSource = report.printTable(Model, _SiteConfigBusiness.GetSiteCodeName());
            (report.Bands["DetailReport"] as DetailReportBand).DataMember = "xrTableRowFormat";
            return PartialView("~/Views/ReportsViews/TransactionsCardByCashierReport.cshtml", report);

        }

        [HttpPost]
        public ActionResult ActionReportGralTransactionsCard(TransactionsCardIndex model)
        {
            var User = !string.IsNullOrEmpty(model.Cashier) ? _UserMasterBusiness.GetUserMasterByEmployeeNumber(model.Cashier) : new App.Entities.USER_MASTER();
            model.IsFromMenu = false;
            model.CashierName = string.IsNullOrEmpty(model.Cashier) ? "Todos los Cajeros" : string.Format("{0} {1}", User.first_name, User.last_name);

            model.DataReport = _SalesBussines.GetTransactionsCardData(model)
                .GroupBy(g => g.Fecha.Value.Date)
                .Select(cl => new TransactionsCardData
                {
                    Fecha = cl.First().Fecha.Value.Date,
                    Monto = cl.Sum(c => c.Monto),
                    Retiro = cl.Sum(c => c.Retiro),
                    Total = cl.Sum(c => c.Total)
                }).ToList();

            TransactionsCardReport report = new TransactionsCardReport();

            (report.Bands["DetailReport"] as DetailReportBand).DataSource = report.printTable(model, _SiteConfigBusiness.GetSiteCodeName());
            (report.Bands["DetailReport"] as DetailReportBand).DataMember = "xrTableRowFormat";
            return PartialView("~/Views/ReportsViews/TransactionsCardReport.cshtml", report);
        }
        [HttpGet]
        public ActionResult ActionGetTransactionsCardByCashier(TransactionsCardIndex Model)
        {
            var User = !string.IsNullOrEmpty(Model.Cashier) ? _UserMasterBusiness.GetUserMasterByEmployeeNumber(Model.Cashier) : new App.Entities.USER_MASTER();
            Model.IsFromMenu = false;
            Model.CashierName = string.IsNullOrEmpty(Model.Cashier) ? "Todos los Cajeros" : string.Format("{0} {1}", User.first_name, User.last_name);

            Model.CashierReport = _SalesBussines.GetTransactionsCardByCashier(Model);


            return Json(new { success = true, Json = Model.CashierReport }, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Cancellated Sales by Cashier Report
        public ActionResult SALE012()
        {
            var Model = new CancellationIndex();
            Model.Date = DateTime.Now.Date.ToString(DateFormat.INT_DATE);
            Model.Date2 = DateTime.Now.Date.ToString(DateFormat.INT_DATE);
            Model.CancellationsData = _SalesCancellationBusiness.GetSalesCancellationResume(Model);
            return View(Model);
        }
        [HttpPost]
        public ActionResult SALE012(CancellationIndex Model)
        {
            Model.CancellationsData = _SalesCancellationBusiness.GetSalesCancellationResume(Model);
            return View(Model);
        }
        public ActionResult ActionGetCancellationsDetail(string date1, string date2, string cashierId)
        {
            return Json(new { success = true, Json = _SalesCancellationBusiness.GetSalesCancellationReporByDatesAndCashier(date1, date2, cashierId) }, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public ActionResult ActionCancelledReport(CancellationIndex Model)
        {
            var report = new CancellationsCashier();
            Model.SiteName = _SiteConfigBusiness.GetSiteCodeName();
            (report.Bands["DetailReport"] as DetailReportBand).DataSource = report.printTable(Model);
            (report.Bands["DetailReport"] as DetailReportBand).DataMember = "xrTableRowFormat";
            return PartialView("~/Views/ReportsViews/CancellationsReport.cshtml", report);
        }
        #endregion

        #region Weighing Report
        public ActionResult SALE013()
        {
            var Model = new WeighingIndex();
            Model.Date = DateTime.Now.Date.ToString(DateFormat.INT_DATE);
            Model.Date2 = DateTime.Now.Date.ToString(DateFormat.INT_DATE);
            Model.WeighingReportData = _SalesBussines.GetWeighingList(Model);
            return View(Model);
        }

        [HttpPost]
        public ActionResult SALE013(WeighingIndex Model)
        {
            Model.WeighingReportData = _SalesBussines.GetWeighingList(Model);
            return View(Model);
        }

        [HttpPost]
        public ActionResult ActionWeighingReport(WeighingIndex Model)
        {
            var report = new Reports.WeighingReport();
            (report.Bands["DetailReport"] as DetailReportBand).DataSource = report.printTable(Model);
            (report.Bands["DetailReport"] as DetailReportBand).DataMember = "xrTableRowFormat";
            return PartialView("~/Views/ReportsViews/WeighingReport.cshtml", report);
        }
        #endregion

        #region Items Without Sale By Department
        public ActionResult SALE014()
        {

            ItemsWithOutSalesIndex Model = new ItemsWithOutSalesIndex()
            {
                Date = DateTime.Now.Date.ToString(DateFormat.INT_DATE),
                Date2 = DateTime.Now.Date.ToString(DateFormat.INT_DATE),
                DepartmentList = GetDepartmentList(),
                ItemsList = new List<ItemsWithOutSalesModel>(),
                IsFromMenu = true
            };

            return View(Model);

        }

        [HttpPost]
        public ActionResult SALE014(ItemsWithOutSalesIndex Model)
        {
            ItemsWithOutSalesIndex Model1 = new ItemsWithOutSalesIndex()
            {
                Date = Model.Date,
                Date2 = Model.Date2,
                Departments = Model.Departments,
                DepartmentList = Model.DepartmentList,
                ItemsList = _SalesBussines.GetItemsWithOutSalesList(Model.Date, Model.Date2, Model.SelectedDepartments),
                IsFromMenu = false

            };
            return View(Model1);
        }

        [HttpGet]
        public ActionResult ActionGetItemsWithOutSalesList(string Date1, string Date2, string DepartmentsIds)
        {
            return Json(new { success = true, Json = _SalesBussines.GetItemsWithOutSalesList(Date1, Date2, DepartmentsIds) }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult ActionItemsWithOutSalesReport(string Date1, string Date2, string DepartmentsIds)
        {
            var report = new ProductsWithOutSales();
            var Model = new ItemsWithOutSalesIndex();
            Model.Date = Date1;
            Model.Date2 = Date2;
            Model.ItemsList = _SalesBussines.GetItemsWithOutSalesList(Date1, Date2, DepartmentsIds);
            string siteName = _SiteConfigBusiness.GetSiteCodeName();

            (report.Bands["DetailReport"] as DetailReportBand).DataSource = report.printTable(Model, siteName);
            (report.Bands["DetailReport"] as DetailReportBand).DataMember = "xrTableRowFormat";
            return PartialView("~/Views/ReportsViews/ItemsWithOutSalesReport.cshtml", report);

        }
        #endregion

        #region Most saled Items
        public ActionResult SALE018()
        {
            var _date = DateTime.Now.Date.ToString(DateFormat.INT_DATE);
            ItemsWithSales Model = new ItemsWithSales()
            {

                Date = DateTime.Now.Date.ToString(DateFormat.INT_DATE),
                Date2 = DateTime.Now.Date.ToString(DateFormat.INT_DATE),
                ItemsList = _SalesBussines.GetItemsWithSalesList(_date, _date),
                IsFromMenu = true
            };

            return View(Model);
        }


        [HttpGet]
        public ActionResult ActionGetItemsWithSalesList(string Date1, string Date2)
        {
            return Json(new { success = true, Json = _SalesBussines.GetItemsWithSalesList(Date1, Date2) }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult ActionItemsWithSalesReport(string Date1, string Date2)
        {

            string siteName = _SiteConfigBusiness.GetSiteCodeName();
            var report = new ProductsWithSales();
            ItemsWithSales Model = new ItemsWithSales();
            Model.Date = Date1;
            Model.Date2 = Date2;
            Model.ItemsList = _SalesBussines.GetItemsWithSalesList(Model.Date, Model.Date2);
            (report.Bands["DetailReport"] as DetailReportBand).DataSource = report.printTable(Model, siteName);
            (report.Bands["DetailReport"] as DetailReportBand).DataMember = "xrTableRowFormat";
            return PartialView("~/Views/ReportsViews/ItemsWithSalesReport.cshtml", report);

        }
        #endregion

        #region Average Customers per hour Report
        public ActionResult SALE016()
        {
            var Model = new AverageCustomersByHourIndex();
            Model.DataList = _SalesBussines.GetAverageCustomertByHours(Model.Date);
            return View(Model);
        }

        [HttpPost]
        public ActionResult SALE016(AverageCustomersByHourIndex Model)
        {
            Model.IsFromMenu = false;
            Model.DataList = _SalesBussines.GetAverageCustomertByHours(Model.Date);
            return View(Model);
        }
        [HttpPost]
        public ActionResult ActionGetAvgCustomerReport(AverageCustomersByHourIndex Model)
        {
            AvgCustomersSalesReport report = new AvgCustomersSalesReport();
            Model.SiteName = _SiteConfigBusiness.GetSiteCodeName();
            (report.Bands["DetailReport"] as DetailReportBand).DataSource = report.printTable(Model);
            (report.Bands["DetailReport"] as DetailReportBand).DataMember = "xrTableRowFormat";
            return PartialView("~/Views/ReportsViews/TransactionsCardReport.cshtml", report);
        }
        #endregion

        #region Customers and Clients per hour
        public ActionResult SALE017()
        {
            var model = new SalesIndex();
            model.StartDate = DateTime.Now.Date.ToString(DateFormat.INT_DATE);
            model.EndDate = DateTime.Now.Date.ToString(DateFormat.INT_DATE);
            model.SalesByHour = _SalesBussines.SalesClientsByHourReport(model);

            return View(model);
        }

        [HttpPost]
        public ActionResult SALE017(SalesIndex model)
        {
            model.SalesByHour = _SalesBussines.SalesClientsByHourReport(model);
            return View(model);
        }

        [HttpPost]
        public ActionResult ActionGetSalesByHourReport(SalesIndex model)
        {
            SalesByHourReport report = new SalesByHourReport();
            (report.Bands["DetailReport"] as DetailReportBand).DataSource = report.printTable(model.SalesByHour, model.StartDate, model.EndDate, _SiteConfigBusiness.GetSiteCodeName());
            (report.Bands["DetailReport"] as DetailReportBand).DataMember = "xrTableRowFormat";

            return PartialView("~/Views/ReportsViews/SaleReportList.cshtml", report);
        }
        #endregion

        #region Transactions Card Report
        public ActionResult SALE020()
        {
            var Model = new TransactionsCardIndex();
            ViewBag.Cashier = _CashierLookup.FillLookupCashiers(CashiersLookup.AllCashier);
            return View(Model);
        }

        [HttpPost]
        public ActionResult SALE020(TransactionsCardIndex Model)
        {
            Model.IsFromMenu = false;
            Model.CashierReport = _SalesBussines.TransactionCardReport(Model);
            ViewBag.Cashier = _CashierLookup.FillLookupCashiers(CashiersLookup.AllCashier);
            return View(Model);
        }

        public ActionResult ActionGetTransReport(TransactionsCardIndex Model)
        {
            var report = new TransCardReport();
            string siteName = _SiteConfigBusiness.GetSiteCodeName();
            if (string.IsNullOrEmpty(Model.Cashier))
                Model.CashierName = "Todos los cajeros";
            else
            {
                var Employee = _UserMasterBusiness.GetUserMasterByEmployeeNumber(Model.Cashier);
                Model.CashierName = string.Format("{0} {1}", Employee.first_name, Employee.last_name);
            }
            (report.Bands["DetailReport"] as DetailReportBand).DataSource = report.printTable(Model, siteName);
            (report.Bands["DetailReport"] as DetailReportBand).DataMember = "xrTableRowFormat";
            return PartialView("~/Views/ReportsViews/ReportView.cshtml", report);
        }
        #endregion

        #region Accumulated Report
        public ActionResult SALE021()
        {
            var Model = new SalesReportIndex();
            ViewBag.Cashier = _CashierLookup.FillLookupCashiers(CashiersLookup.BySale);
            return View(Model);
        }

        [HttpPost]
        public ActionResult SALE021(SalesReportIndex Model)
        {
            Model.IsFromMenu = false;
            Model.AccumulatedData = _SalesBussines.GetAccumulatedData(Model.Date, Model.Cashier);
            ViewBag.Cashier = _CashierLookup.FillLookupCashiers(CashiersLookup.BySale);
            return View(Model);
        }

        [HttpGet]
        public ActionResult ActionUpdateAccumulatedData(string Cashier, string Date)
        {
            return Json(new { success = true, Data = _SalesBussines.GetAccumulatedData(Date, Cashier) }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult ActionGetAccumulatedReport(SalesReportIndex Model)
        {
            var report = new AccumulatedReport();
            Model.SiteName = _SiteConfigBusiness.GetSiteCodeName();
            (report.Bands["DetailReport"] as DetailReportBand).DataSource = report.printTable(Model);
            (report.Bands["DetailReport"] as DetailReportBand).DataMember = "xrTableRowFormat";
            return PartialView("~/Views/ReportsViews/ReportView.cshtml", report);
        }


        #endregion

        #region SalesDollars
        public ActionResult SALE022()
        {
            var model = new SalesIndex();
            model.StartDate = DateTime.Now.Date.ToString(DateFormat.INT_DATE);
            model.EndDate = DateTime.Now.Date.ToString(DateFormat.INT_DATE);
            model.SalesDlls = _SalesBussines.GetDollarSalesByDates(model);
            return View(model);
        }

        [HttpPost]
        public ActionResult SALE022(SalesIndex model)
        {
            model.SalesDlls = _SalesBussines.GetDollarSalesByDates(model);
            return View(model);
        }

        [HttpPost]
        public ActionResult ActionGetSalesReportDollar(SalesIndex model)
        {
            var report = new SalesReportDlls();
            string siteName = _SiteConfigBusiness.GetSiteCodeName();
            (report.Bands["DetailReport"] as DetailReportBand).DataSource = report.printTable(model.SalesDlls, model.StartDate, model.EndDate, siteName);
            (report.Bands["DetailReport"] as DetailReportBand).DataMember = "xrTableRowFormat";

            return PartialView("~/Views/ReportsViews/SaleReportList.cshtml", report);
        }
        #endregion

        #region Utilities Sales        
        public ActionResult SALE023()
        {
            var Model = new UtilitySalesIndex();
            ViewBag.Department = new SelectList(_MaClassBusiness.GetAllDepartments(1), "class_id", "class_name");
            ViewBag.Family = new SelectList(_MaClassBusiness.GetAllDepartments(2), "class_id", "class_name");
            ViewBag.PartNumber = new SelectList(_ItemBusiness.GetAllProductsToDropDown(), "PartNumber", "Description");
            return View(Model);
        }

        [HttpPost]
        public ActionResult SALE023(UtilitySalesIndex Model)
        {
            Model.IsFromMenu = false;
            Model.UtilitySales = _SalesBussines.UtilitySales(Model);
            ViewBag.Department = new SelectList(_MaClassBusiness.GetAllDepartments(1), "class_id", "class_name");

            //Fill Item Lookup
            if (Model.Family != 0)
                ViewBag.PartNumber = new SelectList(_ItemBusiness.GetItemsByFamily(Model.Family), "PartNumber", "Description");
            else if (Model.Department != 0 && Model.Family == 0)
                ViewBag.PartNumber = new SelectList(_ItemBusiness.GetItemsByDepartment(Model.Department), "PartNumber", "Description");
            else if (Model.Department == 0 && Model.Family == 0)
                ViewBag.PartNumber = new SelectList(_ItemBusiness.GetAllProductsToDropDown(), "PartNumber", "Description");

            //Fill Family Lookup
            if (Model.Department == 0)
                ViewBag.Family = new SelectList(_MaClassBusiness.GetAllDepartments(2), "class_id", "class_name");
            else
                ViewBag.Family = new SelectList(_MaClassBusiness.GetAllFamilyByParentClass(Model.Department), "class_id", "class_name");

            return View(Model);
        }

        [HttpPost]
        public ActionResult ActionGetUtilitiesReport(UtilitySalesIndex Model)
        {
            Model.UtilitySales = _SalesBussines.UtilitySales(Model);
            var report = new UtilitiesReport();            
            (report.Bands["DetailReport"] as DetailReportBand).DataSource = report.printTable(Model);
            (report.Bands["DetailReport"] as DetailReportBand).DataMember = "xrTableRowFormat";
            return PartialView("~/Views/ReportsViews/ReportView.cshtml", report);
        }
        #endregion

        #region SaleIEPS
        public ActionResult SALE024()
        {
            var model = new SalesIndex();
            model.StartDate = DateTime.Now.Date.ToString(DateFormat.INT_DATE);
            model.EndDate = DateTime.Now.Date.ToString(DateFormat.INT_DATE);
            return View(model);
        }

        [HttpPost]
        public ActionResult SALE024(SalesIndex model)
        {
            model.IEPSReport = _SalesBussines.GetSalesIEPSModel(model);
            model.IsFromMenu = false;
            return View(model);
        }

        [HttpPost]
        public ActionResult ActionGetSalesReportIEPS(SalesIndex Model)
        {
            Model.IEPSReport = _SalesBussines.GetSalesIEPSModel(Model);
            Reports.SalesIEPS report = new Reports.SalesIEPS();

            string siteName = _SiteConfigBusiness.GetSiteCodeName();

            (report.Bands["DetailReport"] as DetailReportBand).DataSource = report.printTable(Model, siteName);
            (report.Bands["DetailReport"] as DetailReportBand).DataMember = "xrTableRowFormat";

            return PartialView("~/Views/ReportsViews/SaleReportList.cshtml", report);
        }
        #endregion

        #region Gift Card Report
        public ActionResult SALE025()
        {
            var Model = new GiftCardReportIndex();
            return View(Model);
        }

        [HttpPost]
        public ActionResult SALE025(GiftCardReportIndex Model)
        {
            if (Model.ReportType == ReportType.ActiveCards)
                Model.DataReport = _SalesBussines.GetDataActiveCardsReport(Model);
            else
                Model.DataReport = _SalesBussines.GetDataGiftCardChargesReport(Model);

            Model.IsCharge = Model.ReportType == ReportType.Charges;
            Model.IsFromMenu = false;
            return View(Model);
        }

        public ActionResult ActionGetGiftCardReport(GiftCardReportIndex Model)
        {
            var report = new GiftCardReport();
            (report.Bands["DetailReport"] as DetailReportBand).DataSource = report.printTable(Model);
            (report.Bands["DetailReport"] as DetailReportBand).DataMember = "xrTableRowFormat";
            return PartialView("~/Views/ReportsViews/ReportView.cshtml", report);
        }
        #endregion

        #region Prepaid And Services Report
        public ActionResult SALE019()
        {

            var Model = new SalesServicesIndex();
            Model.StartDate = DateTime.Now.Date.ToString(DateFormat.INT_DATE);
            Model.EndDate = DateTime.Now.Date.ToString(DateFormat.INT_DATE);
            FillTypesServices();
            return View(Model);
        }

        [HttpGet]
        public ActionResult ActionGetCompanyServicesRecargas(string startDate, string endDate, string compañia, string idServicio, bool fromMenu, int detailServ)
        {

            var ServicesRecargasList = _SalesBussines.GetServicesRecargas(startDate, endDate, compañia, idServicio, fromMenu, detailServ);
            if (ServicesRecargasList != null)
            {
                return Json(new { success = true, Json = ServicesRecargasList }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(new { success = false }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public ActionResult ActionServicesRecargasReport(string startDate, string endDate, string compañia, string idServicio, bool fromMenu, int detailServ)
        {

            string siteName = _SiteConfigBusiness.GetSiteCodeName();

            var report = new ServRecargas();

            var ServicesRecargasList = _SalesBussines.GetServicesRecargas(startDate, endDate, compañia, idServicio, fromMenu, detailServ);
            //serviciosRecargas _ServiciosRecargas, string date1, string date2, string siteName
            (report.Bands["DetailReport"] as DetailReportBand).DataSource = report.printTable(ServicesRecargasList, startDate, endDate, siteName);
            (report.Bands["DetailReport"] as DetailReportBand).DataMember = "xrTableRowFormat";
            return PartialView("~/Views/ReportsViews/ItemsWithSalesReport.cshtml", report);

        }

        [HttpPost]
        public ActionResult ActionServicesReport(string startDate, string endDate, string compañia, string idServicio, bool fromMenu, int detailServ)
        {

            string siteName = _SiteConfigBusiness.GetSiteCodeName();

            var report = new ServicesSalesReport();

            var ServicesRecargasList = _SalesBussines.GetServicesRecargas(startDate, endDate, compañia, "2", fromMenu, detailServ);
            //serviciosRecargas _ServiciosRecargas, string date1, string date2, string siteName
            (report.Bands["DetailReport"] as DetailReportBand).DataSource = report.printTable(ServicesRecargasList, startDate, endDate, siteName);
            (report.Bands["DetailReport"] as DetailReportBand).DataMember = "xrTableRowFormat";
            return PartialView("~/Views/ReportsViews/ItemsWithSalesReport.cshtml", report);

        }

        [HttpPost]
        public ActionResult ActionServicesDetailReport(string startDate, string endDate, string compañia, string idServicio, bool fromMenu, int detailServ)
        {

            string siteName = _SiteConfigBusiness.GetSiteCodeName();

            var report = new ServDetailReport();

            var ServicesRecargasList = _SalesBussines.GetServicesRecargas(startDate, endDate, compañia, idServicio, fromMenu, detailServ);
            //serviciosRecargas _ServiciosRecargas, string date1, string date2, string siteName
            (report.Bands["DetailReport"] as DetailReportBand).DataSource = report.printTable(ServicesRecargasList, startDate, endDate, siteName, compañia);
            (report.Bands["DetailReport"] as DetailReportBand).DataMember = "xrTableRowFormat";
            return PartialView("~/Views/ReportsViews/ItemsWithSalesReport.cshtml", report);

        }

        [HttpPost]
        public ActionResult ActionRecargasReport(string startDate, string endDate, string compañia, string idServicio, bool fromMenu, int detailServ)
        {

            string siteName = _SiteConfigBusiness.GetSiteCodeName();

            var report = new RecargasReport();

            var ServicesRecargasList = _SalesBussines.GetServicesRecargas(startDate, endDate, compañia, "1", fromMenu, detailServ);
            //serviciosRecargas _ServiciosRecargas, string date1, string date2, string siteName
            (report.Bands["DetailReport1"] as DetailReportBand).DataSource = report.printTable(ServicesRecargasList, startDate, endDate, siteName);
            (report.Bands["DetailReport1"] as DetailReportBand).DataMember = "xrTableRowFormat";
            return PartialView("~/Views/ReportsViews/ItemsWithSalesReport.cshtml", report);

        }

        [HttpPost]
        public ActionResult ActionRecargasDetailReport(string startDate, string endDate, string compañia, string idServicio, bool fromMenu, int detailServ)
        {

            string siteName = _SiteConfigBusiness.GetSiteCodeName();

            var report = new RecargasDetailReport();

            var ServicesRecargasList = _SalesBussines.GetServicesRecargas(startDate, endDate, compañia, idServicio, fromMenu, detailServ);
            //serviciosRecargas _ServiciosRecargas, string date1, string date2, string siteName
            (report.Bands["DetailReport1"] as DetailReportBand).DataSource = report.printTable(ServicesRecargasList, startDate, endDate, siteName, compañia);
            (report.Bands["DetailReport1"] as DetailReportBand).DataMember = "xrTableRowFormat";
            return PartialView("~/Views/ReportsViews/ItemsWithSalesReport.cshtml", report);

        }
        #endregion

        #endregion

        //Re envio de facturas
        public ActionResult SALE000()
        {
            return View();
        }
        public ActionResult SALE004()
        {
            return View();
        }
        public ActionResult SALE007()
        {
            return View();
        }

        [HttpPost]
        public ActionResult SendInvoice(SendDocumentModel model)
        {
            var senDocument = new Timbrado.TimbradoSoapClient();

            var ModelSend = new Timbrado.Request();
            ModelSend.SiteCode = _SiteConfigBusiness.GetSiteCode();
            ModelSend.Serie = model.Serie;
            ModelSend.Number = long.Parse(model.Number);
            ModelSend.Type = model.DocumentType;
            ModelSend.Customer = model.Customer;
            ModelSend.Email = model.EmailCustomerSend;

            var document = senDocument.SendDocument(ModelSend);

            return Json(new { success = document.Status, Json = document, responseText = document.Message }, JsonRequestBehavior.AllowGet);
        }

        private List<DepartementsData> GetDepartmentList()
        {
            var listDepartments = new List<DepartementsData>();

            foreach (MA_CLASS dept in _SalesBussines.GetDepartaments())
            {
                var depto = new DepartementsData();
                depto.idDep = dept.class_id.ToString();
                depto.name = dept.class_name;
                depto.SelectedDep = false;
                listDepartments.Add(depto);
            }
            return listDepartments;
        }

        private void FillLookupCajas(List<string> Cajas)
        {
            var CajasList = new List<SelectListItem>();

            foreach (var Item in Cajas)
            {
                var Element = new SelectListItem();
                Element.Text = Item;
                Element.Value = Item;
                CajasList.Add(Element);
            }

            ViewBag.caja = CajasList;
        }

        void FillTypesServices()
        {
            var TypeList = new List<SelectListItem>();
            var Tipos = _SalesBussines.GetTipoServiciosList();


            foreach (var item in Tipos)
            {
                var element = new SelectListItem();
                element.Text = item.description;
                element.Value = item.id.ToString();
                TypeList.Add(element);
            }

            ViewBag.Type = TypeList;
        }

        public ActionResult SALE026()
        {
            return View();
        }

        public ActionResult ActionGetItemSalesByCashier(DateTime BeginDate, DateTime EndDate, string PartNumber)
        {
            return Json(new { success = true, Json = _ItemSalesBusiness.GetItemSalesByCashier(BeginDate, EndDate, PartNumber) }, JsonRequestBehavior.AllowGet);
        }
    }
}
