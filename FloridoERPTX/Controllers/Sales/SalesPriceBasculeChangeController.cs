﻿using App.BLL.Sales;
using System;
using System.IO;
using System.Text;
using System.Web.Mvc;


namespace FloridoERPTX.Controllers.Sales
{
    public class SalesPriceBasculeChangeController : Controller
    {
        private SalesPriceChangeBusiness _salesPriceChangeBusiness;

        public SalesPriceBasculeChangeController()
        {
            _salesPriceChangeBusiness = new SalesPriceChangeBusiness();
        }
        public ActionResult ActionReportOrder(int TypeOfBascule)
        {
            switch (TypeOfBascule)
            {
                case 1:
                    var ExcelTable = _salesPriceChangeBusiness.FileCreate(TypeOfBascule);
                    var byteArray = Encoding.ASCII.GetBytes(ExcelTable);
                    var stream = new MemoryStream(byteArray);
                    return File(stream, "application/vnd.ms-excel", "bascula_" + DateTime.Now + ".xls");
                case 2:
                    var retu2 = _salesPriceChangeBusiness.FileCreate(TypeOfBascule);
                    return Json(new { success = true, responseText = retu2 }, JsonRequestBehavior.AllowGet);
                case 3:
                    var retu3 = _salesPriceChangeBusiness.FileCreate(TypeOfBascule);
                    return Json(new { success = true, responseText = retu3 }, JsonRequestBehavior.AllowGet);
                default:
                    return Json(new { success = false, responseText = "SF" }, JsonRequestBehavior.AllowGet);
            }
        }
    }
}