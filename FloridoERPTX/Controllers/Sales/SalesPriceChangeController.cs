﻿using App.BLL.Sales;
using App.DAL.Sales;
using App.Entities.ViewModels.SalesPriceChange;
using DevExpress.XtraReports.UI;
using FloridoERPTX.Reports;
using FloridoERPTX.ViewModels.Purchases;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using App.BLL.MaClass;
using App.Common;
using Newtonsoft.Json;
using App.BLL.Site;
using App.BLL;
using App.DAL.MaClass;

namespace FloridoERPTX.Controllers.Sales
{

    public class SalesPriceChangeController : Controller
    {
        private SalesPriceChangeBusiness _salesPriceChangeBusiness;
        private MaClassBusiness _maClassBusiness;
        private SiteBusiness _siteBusiness;
        private UserMasterBusiness _userMasterBusiness;
        private MaClassRepository _maClassRepository;

        public SalesPriceChangeController()
        {
            _salesPriceChangeBusiness = new SalesPriceChangeBusiness();
            _maClassBusiness = new MaClassBusiness();
            _siteBusiness = new SiteBusiness();
            _userMasterBusiness = new UserMasterBusiness();
            _maClassRepository = new MaClassRepository();
        }

        [HttpPost]
        public ActionResult ActionGetSalesPriceChange(string fecha, string depto, string familia, string existencias)
        {
            if (Session["User"] != null)
            {
                if (ModelState.IsValid)
                {
                    var lista = _salesPriceChangeBusiness.GetAllSalesPricesChangeByParameters(fecha, depto, familia, existencias);
                    if (lista.Count > 0)
                    {
                        return Json(new { success = true, Json = lista }, JsonRequestBehavior.AllowGet);
                    }
                    else
                        return Json(new { success = false, responseText = "Sin registros en ese filtro." }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    IEnumerable<ModelError> allErrors = ModelState.Values.SelectMany(v => v.Errors);
                    return Json(new { success = false, responseText = allErrors.ToString() }, JsonRequestBehavior.AllowGet);
                }
            }
            else
            {
                return Json(new { success = false, responseText = "Termino tu sesión." }, JsonRequestBehavior.AllowGet);
            }
        }



        [HttpPost]
        public ActionResult ActionReportOrder(List<SalesPriceChangeViewModel> listProducts)
        {
            if (Session["User"] != null)
            {
                ArrayList listDataSource = new ArrayList();
                var precio = "";
                for (int i = 0; i < listProducts.Count; i++)
                {
                    precio = "$ " + listProducts[i].sale_price;
                    listDataSource.Add(new SalesPriceChange(listProducts[i].part_number, listProducts[i].part_description, precio));
                }

                Cenefa report = new Cenefa();
                report.DataSource = listDataSource;

                report.Imprimir("PART_NUMBER");
                report.Imprimir("PART_DESCRIPTION");
                report.Imprimir("SALE_PRICE");

                var stream = new MemoryStream();
                report.ExportToPdf(stream);

                return Json(new { success = true, responseText = Convert.ToBase64String(stream.ToArray()) }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(new { success = false, responseText = "Termino tu sesión." }, JsonRequestBehavior.AllowGet);
            }
        }


        public ActionResult ActionGetAllSalesPricesWithFilter(SalePriceFilter filter)
        {
            var r = _salesPriceChangeBusiness.GetAllSalesPricesWithFilter(filter);
            return Content(JsonConvert.SerializeObject(Result.OK(r),
                new JsonSerializerSettings { NullValueHandling = NullValueHandling.Ignore, DefaultValueHandling = DefaultValueHandling.Ignore }
            ), "application/json");
        }

        public ActionResult ActionGetDeptos()
        {

            var r = _maClassBusiness.GetAllDepartments(1);
            return Json(Result.OK(r), JsonRequestBehavior.AllowGet);
        }
        public ActionResult ActionGetFamilies(int id)
        {

            var r = _maClassBusiness.GetAllFamilyByParentClass(id);
            return Json(Result.OK(r), JsonRequestBehavior.AllowGet);
        }

        public ActionResult ActionGetReportSalePriceCurrent(string deparment, string family)
        {
            if (Session["User"] != null)
            {
                string dep, fam;
                var info = _salesPriceChangeBusiness.GetItemPriceSaleCurrent(deparment, family, Session["User"].ToString());
                var nameComplete = _userMasterBusiness.getCompleteName(Session["User"].ToString());
                if (deparment != "0")
                    dep = _maClassRepository.GetDepartmentById(Convert.ToInt32(deparment));
                else
                    dep = "Todas los Departamentos";

                if (family != "0")
                    fam = _maClassRepository.GetDepartmentById(Convert.ToInt32(family));
                else
                    fam = "Todas las Familias";

                //family = _maClassRepository.GetFamilyById(famil), description = part_desc
                SalesPriceChangeCurrent report = new SalesPriceChangeCurrent();
                var stream = new MemoryStream();
                report.DataSource = report.printTable(info, _siteBusiness.GetInfoSite(), nameComplete, dep, fam);
                report.ExportToPdf(stream);
                //return Json(new { success = true, responseText = Convert.ToBase64String(stream.ToArray())  }, JsonRequestBehavior.AllowGet);
                return new JsonResult() { Data = Convert.ToBase64String(stream.ToArray()), JsonRequestBehavior = JsonRequestBehavior.AllowGet, MaxJsonLength = Int32.MaxValue };
            }
            //return Json(new { success = false, responseText = "SF" }, JsonRequestBehavior.AllowGet);
            return new JsonResult() { Data = false, JsonRequestBehavior = JsonRequestBehavior.AllowGet, MaxJsonLength = Int32.MaxValue };
        }
    }
}