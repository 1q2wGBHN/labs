﻿using App.BLL.Sales;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace FloridoERPTX.Controllers.Sales
{
    public class SalesDetailsController : Controller
    {
        // GET: SalesDetails
        private SalesDetailBusiness _SalesDetailBusiness;
        public SalesDetailsController()
        {
            _SalesDetailBusiness = new SalesDetailBusiness();
        }
        public ActionResult ActionAverageSale(string Code,string Site)
        {
            if (Session["User"] != null)
            {
                return Json(_SalesDetailBusiness.getAverageSale(Code, Site), JsonRequestBehavior.AllowGet);
            }
            return Json("SF", JsonRequestBehavior.AllowGet);
        }

        public ActionResult ActionListOrderSales(int Supplier, string Site)
        {
            if (Session["User"] != null)
            {
                return Json(_SalesDetailBusiness.getAverageSales(Supplier, Site), JsonRequestBehavior.AllowGet);
            }
            return Json("SF", JsonRequestBehavior.AllowGet);
        }
    }
}