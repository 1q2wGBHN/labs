﻿using App.BLL.ConfigPosSales;
using App.BLL.Lookups;
using App.Entities.ViewModels.ConfigPosSales;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace FloridoERPTX.Controllers.ConfigPosSales
{
    public class ConfigPosSalesController : Controller
    {
        private ConfigPosSalesBusiness _ConfigPosSalesBusiness;
        private ConfigPosSectionBusiness _ConfigPosSectionBusiness;

        public ConfigPosSalesController()
        {
            _ConfigPosSalesBusiness = new ConfigPosSalesBusiness();
            _ConfigPosSectionBusiness = new ConfigPosSectionBusiness();
        }

        // GET: ConfigPosSales
        public ActionResult COPO001()
        {
            var Model = new PosSale();
            ViewBag.Section = new SelectList(_ConfigPosSectionBusiness.GetConfigSectionList(), "id", "section");
            return View(Model);
        }

        [HttpPost]
        public ActionResult COPO001(PosSale Model)
        {
            var Status = _ConfigPosSalesBusiness.CreateConfigPosSale(Model);

            if (Status)
                return Json(new { success = true, Json = Model, responseText = "Configuración Creada" }, JsonRequestBehavior.AllowGet);
            else
                return Json(new { success = false, Json = Model, responseText = "Ocurrió un error, contacte a sistemas!" }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult COPO002()
        {
            return View(_ConfigPosSalesBusiness.GetConfigPosSalesList());
        }

        public ActionResult ActionEditPosConfig(int ConfigPosId)
        {
            var Model = new PosSale();
            Model = _ConfigPosSalesBusiness.GetConfigPosSaleById(ConfigPosId);
            ViewBag.Section = new SelectList(_ConfigPosSectionBusiness.GetConfigSectionList(), "id", "section");
            return PartialView("_COPO002", Model);
        }

        [HttpPost]
        public ActionResult ActionEditPosConfig(PosSale Model)
        {
            if (_ConfigPosSalesBusiness.UpdateConfig(Model))
            {
                var json = JsonConvert.SerializeObject(_ConfigPosSalesBusiness.GetConfigPosSalesList(), Formatting.Indented, new JsonSerializerSettings()
                {
                    ReferenceLoopHandling = ReferenceLoopHandling.Ignore
                });
                return Json(new { success = true, Json = json, responseText = "Configuración editada correctamente" }, JsonRequestBehavior.AllowGet);
            }
                

            return Json(new { success = false, Json = Model, responseText = "Ocurrio un error, contacte a sistemas." }, JsonRequestBehavior.AllowGet);
        }
    }
}