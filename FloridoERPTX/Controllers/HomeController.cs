﻿using App.BLL;
using App.BLL.Configuration;
using App.Common;
using FloridoERPTX.ViewModels;
using FloridoERPTX.ViewModels.Home;
using System;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using App.Entities.ViewModels.User;
using App.BLL.Site;
using System.IO;
using App.BLL.MaCode;
using static App.Entities.ViewModels.Common.Constants;
using App.BLL.Lookups;
using App.DAL.ScrapControl;
using App.BLL.ScrapControl;
using System.Diagnostics;
using App.BLL.Rma;
namespace FloridoERPTX.Controllers
{
    public class HomeController : Controller
    {
        private readonly UserMasterBusiness _userMasterBusiness;
        private readonly UserPwdBusiness _userPwdBusiness;
        private readonly SysLogConnectionBusiness _sysLogConnection;
        private readonly SysPageMasterBusiness _PageMasterRepo;
        private readonly SysSiteBusiness _SiteBussines;
        private readonly SiteConfigBusiness _siteConfigRepository;
        private readonly Common _common;
        private readonly MaCodeBusiness _maCodeBusiness;
        private CurrencyDetailBusiness _CurrencyBusiness;
        private readonly ScrapControlBusiness _scrapControlRepository;
        private readonly DamagedsGoodsBusiness _damagedsGoodsBusiness;
        App_Start.Authentication.Profile Perf = new App_Start.Authentication.Profile();

        public HomeController()
        {
            _userMasterBusiness = new UserMasterBusiness();
            _userPwdBusiness = new UserPwdBusiness();
            _sysLogConnection = new SysLogConnectionBusiness();
            _PageMasterRepo = new SysPageMasterBusiness();
            _SiteBussines = new SysSiteBusiness();
            _siteConfigRepository = new SiteConfigBusiness();
            _common = new Common();
            _maCodeBusiness = new MaCodeBusiness();
            _CurrencyBusiness = new CurrencyDetailBusiness();
            _scrapControlRepository = new ScrapControlBusiness();
            _damagedsGoodsBusiness = new DamagedsGoodsBusiness();
        }

        [Authorize]
        public ActionResult Menu(NavigationViewModel Model)
        {
            if (HttpContext.User != null)
            {
                //Si el usuario esta Autenticado
                if (HttpContext.User.Identity.IsAuthenticated)
                {
                    if (HttpContext.User.Identity is FormsIdentity)
                    {
                        //desencriptar ticket de session y obtenemos las paginas del usuario y nombre de usuario
                        FormsIdentity _identity = (FormsIdentity)HttpContext.User.Identity;
                        FormsAuthenticationTicket ticket = _identity.Ticket;
                        string cookieName = System.Web.Security.FormsAuthentication.FormsCookieName;
                        string userData = System.Web.HttpContext.Current.Request.Cookies[cookieName].Value;
                        ticket = FormsAuthentication.Decrypt(userData);
                        Model.menu_user_dataset = _PageMasterRepo.GetAllPagesOfUser(ticket.UserData);
                        Model.user_name = ticket.Name;
                        Session["User"] = Model.user_name;
                        var employee = _userMasterBusiness.GetUserMasterByUsername(Model.user_name);
                        Model.photo = employee.photo;

                        return PartialView("_Navigation", Model);
                    }
                }
                else
                    HttpContext.Response.Redirect("~/Home/Login");

            }
            return PartialView("_Navigation", Model);
        }

        public ActionResult Logout()
        {
            if (Session["User"] != null)
            {
                var employee = _userMasterBusiness.GetUserMasterByUsernameOrEmpNo(Session["User"].ToString());
                var sysLogConnection = _sysLogConnection.GetConnectionByEmployeeNumber(employee.emp_no);
                if (sysLogConnection != null)
                {
                    sysLogConnection.connlogout_date = System.DateTime.Now;
                    sysLogConnection.conn_status = false;
                    _sysLogConnection.UpdateSession(sysLogConnection);
                }
            }
            FormsAuthentication.SignOut();
            Session.Abandon();
            Session.Contents.RemoveAll();
            HttpContext.Response.Redirect("~/Home/Login");
            return View();
        }
        public ActionResult NotAuthorized()
        {
            ViewBag.SiteName = _siteConfigRepository.GetSiteCodeName();
            return View();
        }
        public ActionResult Contacts()
        {
            ViewBag.SiteName = _siteConfigRepository.GetSiteCodeName();
            return View();
        }

        public ActionResult Login()
        {
            ViewBag.BasculaToledoServer = GetProcessWeightToledo();
            ViewBag.SiteName = _siteConfigRepository.GetSiteCodeName();
            ViewBag.MyPathInServer = GetFolderServerAndTimeUpdate();
            return View();
        }
        public ActionResult Index()
        {
            ViewBag.SiteName = _siteConfigRepository.GetSiteCodeName();
            return View();
        }
        [HttpGet]
        public ActionResult Register()
        {
            ViewBag.SiteName = _siteConfigRepository.GetSiteCodeName();
            return View();
        }

        public ActionResult SiteList()
        {
            return Json(_SiteBussines.GetAllSite(), JsonRequestBehavior.AllowGet);
        }
        public ActionResult Store()
        {
            ViewBag.SiteName = _siteConfigRepository.GetSiteCodeName();
            return View(_SiteBussines.GetAllSite());
        }
        [HttpPost]
        public ActionResult IsActive()
        {
            return Json(true, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public ActionResult LoginEnter(LoginViewModel loginVM)
        {
            var employee = _userMasterBusiness.GetUserMasterByUsernameOrEmpNo(loginVM.Username);
            if (employee != null)
            {
                if (!_userMasterBusiness.IsAccountPending(employee))
                {
                    if (!_userMasterBusiness.IsAccountBlocked(employee))
                    {
                        if (!_userMasterBusiness.IsAccountInactive(employee))
                        {

                            if (_userMasterBusiness.IsPasswordCorrect(employee, loginVM.Password))
                            {
                                try
                                {
                                    var sysLogConnection = _sysLogConnection.GetConnectionByEmployeeNumber(employee.emp_no);
                                    if (sysLogConnection == null)
                                    {
                                        sysLogConnection = new App.Entities.SYS_LOG_CONNECTION();
                                        sysLogConnection.cdate = System.DateTime.Now;
                                        sysLogConnection.connlog_date = System.DateTime.Now;
                                        sysLogConnection.conn_status = true;
                                        sysLogConnection.emp_no = employee.emp_no;
                                        _sysLogConnection.AddSysLogConnection(sysLogConnection);
                                    }
                                    else
                                    {
                                        sysLogConnection.connlog_date = System.DateTime.Now;
                                        _sysLogConnection.UpdateSession(sysLogConnection);
                                    }

                                }
                                catch (Exception ex)
                                {
                                    var msg = ex.Message;
                                }
                                Session["User"] = employee.user_name;

                                FloridoERPTX.App_Start.Authentication.UserCache.AddPaginasToCache(employee.emp_no, _PageMasterRepo.GetAllPagesOfUser(employee.emp_no), System.Web.HttpContext.Current);
                                FormsAuthenticationTicket authTicket = new FormsAuthenticationTicket(2, employee.user_name, DateTime.Now, DateTime.Now.AddMinutes(20), false, employee.emp_no, FormsAuthentication.FormsCookiePath);
                                string crypTicket = FormsAuthentication.Encrypt(authTicket);
                                HttpCookie authCookie = new HttpCookie(FormsAuthentication.FormsCookieName, crypTicket);

                                Response.Cookies.Add(authCookie);
                                return Json(new { success = true, responseText = "Bienvenido " + employee.first_name + " " + employee.last_name + " !", user = new UserModel(employee) }, JsonRequestBehavior.AllowGet);
                            }
                            else
                            {
                                _userMasterBusiness.UpdateFailedAttempts(employee);
                                return Json(new { success = false, responseText = "Contraseña Incorrecta, intenta de nuevo..." }, JsonRequestBehavior.AllowGet);
                            }
                        }
                        else
                            return Json(new { success = false, responseText = "Esta cuenta se encuentra Inactiva, Contacta a sistemas." }, JsonRequestBehavior.AllowGet);
                    }
                    else
                    {
                        return Json(new { success = false, responseText = "Esta cuenta se encuentra bloqueada, Contacta a sistemas." }, JsonRequestBehavior.AllowGet);
                    }
                }
                else
                {
                    return Json(new { success = false, responseText = "Esta cuenta aun no se activa, contacta a sistemas" }, JsonRequestBehavior.AllowGet);
                }
            }
            else
            {
                return Json(new { success = false, responseText = "Este usuario no existe" }, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult UsernameAlreadyExists(string username)
        {
            return Json(new { success = _userMasterBusiness.EmployeeUsernameExists(username) }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult Register(RegisterUserMaster registerUserMasterVM)
        {
            if (ModelState.IsValid)
            {
                var employee = registerUserMasterVM.Employee;
                var employeePw = registerUserMasterVM.EmployeePwd;

                //
                // Permitir diferentes correos con varios usuarios
                //
                //if (_userMasterBusiness.EmployeeEmailExists(employee.email))
                //{
                //    return Json(new { success = false, responseText = "Este correo ya se encuentra registrado" }, JsonRequestBehavior.AllowGet);
                //}
                //else
                //{

                if (!_userMasterBusiness.IsValidEmailAddress(employee.email))
                {
                    return Json(new { success = false, responseText = "Dominio de correo incorrecto. Ejemplo: correo@elflorido.com.mx" }, JsonRequestBehavior.AllowGet);
                }
                if (_userMasterBusiness.EmployeeUsernameExists(employee.user_name))
                {
                    return Json(new { success = false, responseText = "Este nombre de usuario ya se encuentra registrado" }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    employee.USER_PWD = employeePw;
                    if (_userMasterBusiness.AddUserMaster(employee))
                    {

                        return Json(new { success = true, responseText = "Registro Exitoso!" }, JsonRequestBehavior.AllowGet);
                    }
                    else
                    {
                        return Json(new { success = false, responseText = "Hubo un error y no pudimos guardar este empleado!" }, JsonRequestBehavior.AllowGet);
                    }
                }
                //}
            }
            else
            {
                var errors = ModelState.Select(x => x.Value.Errors).Where(y => y.Count > 0).ToList();

                return Json(new { success = false, responseText = "Error Desconocido, contacte a sistemas" }, JsonRequestBehavior.AllowGet);
            }
        }
        [HttpGet]
        public JsonResult FindByEmployeeId(string employeeId)
        {
            var employee = _userMasterBusiness.GetUserMasterByEmployeeNumber(employeeId);
            if (employee != null)
            {
                return Json(new { success = true, responseText = "Este numero de empleado ya existe!", employeeId = employee.emp_no, username = employee.user_name }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(new { success = false, responseText = "Este numero de empleado actualmente esta disponible!" }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpGet]
        public ActionResult RecoveryPassword(string code)
        {
            ViewBag.SiteName = _siteConfigRepository.GetSiteCodeName();
            ViewBag.codestring = code;
            TempData["Code"] = code;
            TempData.Keep("Code");
            return View();
        }
        [HttpGet]
        public ActionResult Unlocking(string code)
        {
            ViewBag.SiteName = _siteConfigRepository.GetSiteCodeName();
            ViewBag.codestring = code;
            TempData["Code"] = code;
            TempData.Keep("Code");
            return View();
        }


        [HttpPost]
        public JsonResult RecoveryPassword(string empno, string code, string password)
        {
            var employee = _userMasterBusiness.GetUserMasterByRecoveryPasswordCode(code);
            if (employee != null)
            {
                if (empno == employee.emp_no || empno == employee.user_name)
                {
                    employee.USER_PWD.password = Common.SetPassword(password);
                    //employee.USER_PWD.status = "A";
                    employee.USER_PWD.failed_attempts = 0;
                    employee.USER_PWD.pass_code = "";
                    if (_userMasterBusiness.UpdateUserMaster(employee))
                    {
                        return Json(new { success = true, responseText = "Se actualizo tu nueva contraseña" }, JsonRequestBehavior.AllowGet);
                    }
                    else
                    {
                        return Json(new { success = false, responseText = "Ocurrió un error al intentar recuperar tu contraseña, contacta a sistemas." }, JsonRequestBehavior.AllowGet);
                    }
                }
                else
                    return Json(new { success = false, responseText = "El numero de empleado no corresponde." }, JsonRequestBehavior.AllowGet);

            }
            else
            {
                return Json(new { success = false, responseText = "El link de recuparación ya no es valido, por favor inicie el procedimiento nuevamente." }, JsonRequestBehavior.AllowGet);
            }

        }

        [HttpPost]
        //1 = RecoveryPassword 0 = Unlocking User
        public JsonResult GetCodeRecoveryPassword(string employeeUsername, bool type)
        {
            var employee = _userMasterBusiness.GetUserMasterByUsernameOrEmpNo(employeeUsername);

            if (employee != null)
            {
                if (type)
                {
                    if (_userMasterBusiness.AddRecoveryCodePassword(employee))
                    {
                        var host = System.Web.HttpContext.Current?.Request.Url.GetLeftPart(UriPartial.Authority);

                        var information = new SendMail
                        {
                            body = "¿Olvidaste tu contraseña? No hay problema, sólo presiona el botón de abajo para restablecerla. Al ir a este enlace, podrás ingresar y confirmar tu nueva contraseña.",
                            buttonLink = $"{host}/home/RecoveryPassword?Code={employee.USER_PWD.pass_code}",
                            buttonText = "Recuperar contraseña",
                            subject = "Recuperación de contraseñas",
                            @from = "Florido ERP",
                            email = employee.email
                        };

                        string ReturnMessage = _common.MailMessageHtml(information);

                        if (ReturnMessage == "success")
                        {
                            return Json(new { success = true, responseText = "Se te ha enviado un correo con las instrucciones para recuperar tu contraseña" }, JsonRequestBehavior.AllowGet);
                        }
                        else
                        {
                            return Json(new { success = false, responseText = ReturnMessage + ", contacta a sistemas." }, JsonRequestBehavior.AllowGet);
                        }
                    }
                    else
                    {
                        return Json(new { success = false, responseText = "Ocurrió un error, contacta a sistemas." }, JsonRequestBehavior.AllowGet);
                    }
                }
                else
                {
                    if (employee.USER_PWD.status == "B")
                    {
                        if (_userMasterBusiness.AddRecoveryCodePassword(employee))
                        {
                            var host = System.Web.HttpContext.Current?.Request.Url.GetLeftPart(UriPartial.Authority);

                            var RolesValid = _maCodeBusiness.GetAllCodeByCode("USER_UNLOCKING");
                            bool hasMatch = RolesValid.Any(x => employee.SYS_ROLE_USER.Any(y => y.role_id == x.vkey));

                            if (hasMatch)
                            {

                                host = System.Web.HttpContext.Current?.Request.Url.GetLeftPart(UriPartial.Authority);

                                var information = new SendMail
                                {
                                    body = "¿Tu usuario se encuentra bloqueado? No hay problema, sólo presiona el botón de abajo para desbloquearlo.",
                                    buttonLink = $"{host}/home/Unlocking?Code={employee.USER_PWD.pass_code}",
                                    buttonText = "Desbloquear Usuario",
                                    subject = "Desbloquear Usuario",
                                    @from = "Florido ERP",
                                    email = employee.email
                                };

                                string ReturnMessage = _common.MailMessageHtml(information);
                     
                                if (ReturnMessage == "success")
                                {
                                    return Json(new { success = true, responseText = "Se te ha enviado un correo con el cual podrias desbloquear tu usuario." }, JsonRequestBehavior.AllowGet);
                                }
                                else
                                {
                                    return Json(new { success = false, responseText = ReturnMessage + ", contacta a sistemas." }, JsonRequestBehavior.AllowGet);
                                }

                            }
                            else
                                return Json(new { success = false, responseText = "Usuario sin permisos para desbloquear su propia cuenta, contacta a sistemas." }, JsonRequestBehavior.AllowGet);
                        }
                        else
                            return Json(new { success = false, responseText = "Ocurrió un error, contacta a sistemas." }, JsonRequestBehavior.AllowGet);
                    }
                    else
                    {
                        return Json(new { success = false, responseText = "Su usuario no se encuentra bloqueado." }, JsonRequestBehavior.AllowGet);
                    }
                }

            }
            else
            {
                return Json(new { success = false, responseText = "Este Usuario o Email no se encuentra en nuestra base de datos" }, JsonRequestBehavior.AllowGet);
            }
        }
        [HttpPost]
        public JsonResult Unlocking(string empno, string code)
        {
            var employee = _userMasterBusiness.GetUserMasterByRecoveryPasswordCode(code);
            if (employee != null)
            {
                if (empno == employee.emp_no)
                {
                    employee.USER_PWD.status = "A";
                    employee.USER_PWD.failed_attempts = 0;
                    employee.USER_PWD.pass_code = "";
                    employee.USER_PWD.udate = DateTime.Now;
                    if (_userMasterBusiness.UpdateUserMaster(employee))
                    {
                        return Json(new { success = true, responseText = "Su usuario fue desbloqueado con existo." }, JsonRequestBehavior.AllowGet);
                    }
                    else
                    {
                        return Json(new { success = false, responseText = "Ocurrió un error al intentar desbloquear." }, JsonRequestBehavior.AllowGet);
                    }
                }
                else
                    return Json(new { success = false, responseText = "El numero de empleado no corresponde." }, JsonRequestBehavior.AllowGet);

            }
            else
            {
                return Json(new { success = false, responseText = "Ocurrió un error al intentar debloquear su usuario, contacta a sistemas." }, JsonRequestBehavior.AllowGet);
            }

        }

        public ActionResult ActionGetPagesByUser()
        {
            if (Session["User"] != null)
            {
                var user = _userMasterBusiness.GetUserMasterByUsername(Session["User"].ToString());
                var pages = _userMasterBusiness.GetPagesByUser(user.emp_no);
                return Json(new { success = true, pages = pages }, JsonRequestBehavior.AllowGet);
            }
            else
                return Json("SF", JsonRequestBehavior.AllowGet);
        }

        public ActionResult _Header()
        {
            ViewBag.SiteName = _siteConfigRepository.GetSiteCodeName();
            return PartialView();
        }

        public ActionResult _Footer()
        {
            ViewBag.MyPathInServer = GetFolderServerAndTimeUpdate();
            ViewBag.SiteNameStore = _siteConfigRepository.GetSiteCodeName();
            return PartialView();
        }

        public string GetProcessWeightToledo()
        {
            if (TimeUpdateWeighingMachine())
            {
                System.Diagnostics.Process si = new System.Diagnostics.Process();
                //si.StartInfo.WorkingDirectory = "C:\\Windows\\system32";
                si.StartInfo.UseShellExecute = false;
                si.StartInfo.FileName = "cmd.exe";
                si.StartInfo.WindowStyle = System.Diagnostics.ProcessWindowStyle.Hidden;
                //si.StartInfo.FileName = "wmic";
                //si.StartInfo.Arguments = " process where \"commandline like '%serversab2%'\" get ExecutablePath";// wmic
                //si.StartInfo.Arguments = " path win32_process get commandline | find \"serverSAB\" "; //wmic
                si.StartInfo.Arguments = " /C netstat -a -n |find \"1100\" | find \"LISTENING\" "; //Revisar puerto
                si.StartInfo.CreateNoWindow = true;
                si.StartInfo.RedirectStandardInput = true;
                si.StartInfo.RedirectStandardOutput = true;
                si.StartInfo.RedirectStandardError = true;
                si.Start();
                string output = si.StandardOutput.ReadToEnd();
                si.Close();

                if (!output.ToLower().Contains("1100"))
                {
                    //System.Diagnostics.Process siJava = new System.Diagnostics.Process(); //Ejecutar el servidor de basculas
                    //siJava.StartInfo.WorkingDirectory = "C:\\TOLEDO";
                    //siJava.StartInfo.UseShellExecute = false;
                    //siJava.StartInfo.FileName = "java";
                    //siJava.StartInfo.Arguments = "-jar serverSAB2.9.jar";
                    //siJava.StartInfo.CreateNoWindow = true;
                    //siJava.StartInfo.RedirectStandardInput = false;
                    //siJava.StartInfo.RedirectStandardOutput = true;
                    //siJava.StartInfo.RedirectStandardError = true;
                    //siJava.Start();
                    ////string outputJava = siJava.StandardOutput.ReadToEnd(); //Evitar la output de la conola (no es necesario)
                    //siJava.Close();
                    return "4";
                    //wmic process where "commandline like '%servers%'" call terminate
                }
            }
            return "14";
        }

        public bool TimeUpdateWeighingMachine()
        {
            try
            {
                if (System.IO.Directory.Exists("C:\\TOLEDO\\"))
                    return true;
                else
                    return false;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public string GetFolderServerAndTimeUpdate()
        {
            try
            {
                var culture = new System.Globalization.CultureInfo("es-ES");
                string fullPath = System.Web.Hosting.HostingEnvironment.ApplicationPhysicalPath;
                string[] pathSplit = fullPath.Split('\\');
                string myPath = pathSplit[pathSplit.Length - 2];
                fullPath = fullPath + "\\bin\\";
                var directory = new DirectoryInfo(fullPath);
                var myFile = (from f in directory.GetFiles()
                              orderby f.LastWriteTime descending
                              select f).First();
                var x = myPath + " - (" + culture.DateTimeFormat.GetDayName(myFile.LastWriteTime.DayOfWeek).ToUpper() + ") " + myFile.LastWriteTime.ToString();
                return x;
            }
            catch (Exception)
            {
                return "Error";
            }
        }
        [Authorize]
        public ActionResult _Right_Sidebar(RightSidebarViewModel model)
        {
            try
            {
                model.ComercialCurrency="$" + _CurrencyBusiness.GetCurrencyRateByDate(DateTime.Now.Date.ToString(DateFormat.INT_DATE)).ToString("#.##");
                model.BanxicoCurrency = "$" + _CurrencyBusiness.GetCurrencySatRateByDate(DateTime.Now.Date.ToString(DateFormat.INT_DATE)).ToString("#.##");
                return PartialView("_Right_Sidebar", model);
            }
            catch (Exception)
            {
                model.BanxicoCurrency = "$";
                model.ComercialCurrency = "$";
                return PartialView("_Right_Sidebar", model);
            }

        }

    }
}