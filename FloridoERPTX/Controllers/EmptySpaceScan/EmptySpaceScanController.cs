﻿using App.BLL;
using App.BLL.EmptySpace;
using App.Entities.ViewModels.Item;
using FloridoERPTX.Reports;
using System;
using System.Collections.Generic;
using System.IO;
using System.Web.Mvc;

namespace FloridoERPTX.Controllers.EmptySpaceScan
{
    public class EmptySpaceScanController : Controller
    {
        private readonly EmptySpaceDetailBusiness _emptySpaceScanDetailBusiness;
        private readonly EmptySpaceBusiness _emptySpaceScanBusiness;
        private readonly UserMasterBusiness _userMasterBusiness;

        public EmptySpaceScanController()
        {
            _emptySpaceScanDetailBusiness = new EmptySpaceDetailBusiness();
            _emptySpaceScanBusiness = new EmptySpaceBusiness();
            _userMasterBusiness = new UserMasterBusiness();
        }

        public ActionResult ActionGetESS(DateTime DateIni, DateTime DateFin)
        {
            return Json(new { success = true, Json = _emptySpaceScanBusiness.GetEmptySpaceByDates(DateIni.ToString(), DateFin.ToString()) }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult ActionGetDetailsById(int id_empty_space)
        {
            return new JsonResult() { Data = _emptySpaceScanDetailBusiness.GetAllItemsByIdEmptySpace(id_empty_space), JsonRequestBehavior = JsonRequestBehavior.AllowGet, MaxJsonLength = Int32.MaxValue };
        }

        public ActionResult ActionPrintEmptySpace(int id_empty_scan)
        {
            List<ItemEmptySpaceScanModel> list = new List<ItemEmptySpaceScanModel>();
            list = _emptySpaceScanDetailBusiness.GetAllItemsByIdEmptySpace(id_empty_scan);
            string nameFull = _userMasterBusiness.getCompleteName(list[0].cuser);
            var resultEmptybyIncidence = _emptySpaceScanBusiness.GetEmptySpaceById(id_empty_scan);

            EmptySpaceScanReport reporte = new EmptySpaceScanReport();
            reporte.DataSource = reporte.printTable(list, resultEmptybyIncidence.cdate, resultEmptybyIncidence.incidence, nameFull);
            var stream = new MemoryStream();
            reporte.ExportToPdf(stream);

            return Json(new { success = true, responseText = Convert.ToBase64String(stream.ToArray()) }, JsonRequestBehavior.AllowGet);
        }
    }
}