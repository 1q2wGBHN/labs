﻿using App.BLL.BarCodeCaptureTime;
using App.BLL.Site;
using App.Entities.ViewModels.BarcodeCaptureTime;
using DevExpress.XtraReports.UI;
using FloridoERPTX.Reports;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace FloridoERPTX.Controllers.BarcodeCaptureTime
{
    public class BarcodeCaptureTimeController : Controller
    {
        private BarcodeCaptureTimeBusiness _BarcodeCaptureTimeBusiness;
        private SiteConfigBusiness _SiteConfigBusiness;

        public BarcodeCaptureTimeController()
        {
            _BarcodeCaptureTimeBusiness = new BarcodeCaptureTimeBusiness();
            _SiteConfigBusiness = new SiteConfigBusiness();
        }

        public ActionResult BACT001()
        {
            var Model = new BarcodeCaptureTimeIndex();
            return View(Model);
        }

        [HttpPost]
        public ActionResult BACT001(BarcodeCaptureTimeIndex Model)
        {
            Model.IsFromMenu = false;
            Model.DataInfo = _BarcodeCaptureTimeBusiness.GetDataReport(Model);
            return View(Model);
        }

        public ActionResult ActionGetBarcodeCaptureTimeReport(BarcodeCaptureTimeIndex Model)
        {
            var report = new BarcodeCaptureTimeReport();
            Model.SiteName = _SiteConfigBusiness.GetSiteCodeName();
            (report.Bands["DetailReport"] as DetailReportBand).DataSource = report.printTable(Model);
            (report.Bands["DetailReport"] as DetailReportBand).DataMember = "xrTableRowFormat";
            return PartialView("~/Views/ReportsViews/ReportView.cshtml", report);
        }   
    }
}