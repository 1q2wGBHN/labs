﻿using App.BLL.Inventory;
using App.Entities.ViewModels.Inventory;
using System;
using System.Collections.Generic;
using System.IO;
using System.Web;
using App.Common;
using System.Web.Mvc;
using App.BLL.Site;
using FloridoERP.Report;
using FloridoERPTX.Reports;
using App.BLL.Configuration;
using System.Configuration;
using App.BLL.Item;

namespace FloridoERPTX.Controllers.Inventory
{
    public class InventoryAdjustmentController : Controller
    {
        private readonly InventoryAdjustmentsBusiness _InventoryAdjustmentsBusiness;
        private readonly SysRoleMasterBusiness _sysRoleMasterBusiness;
        private readonly InventoryReportBusiness _InventoryReportBusiness;
        private readonly SendMail _sendMail;
        private readonly Common common;
        private readonly SiteConfigBusiness _siteConfigBusiness;
        private readonly InventoryHeaderBusiness _inventoryHeaderBusiness;
        private readonly ItemBusiness _itemBusiness;
        private readonly ItemStockBusiness _itemStockBusiness;
        public InventoryAdjustmentController()
        {
            _InventoryReportBusiness = new InventoryReportBusiness();
            _InventoryAdjustmentsBusiness = new InventoryAdjustmentsBusiness();
            _sendMail = new SendMail();
            common = new Common();
            _siteConfigBusiness = new SiteConfigBusiness();
            _inventoryHeaderBusiness = new InventoryHeaderBusiness();
            _sysRoleMasterBusiness = new SysRoleMasterBusiness();
            _itemBusiness = new ItemBusiness();
            _itemStockBusiness = new ItemStockBusiness();
        }
        [HttpGet]
        public ActionResult ActionGetInventoryItemQuantity(InventoryAdjustmentModel InventoryObj)
        {
            if (HttpContext.User.Identity.Name != null)
            {
                var updateReturn = _InventoryAdjustmentsBusiness.GetInventoryItemQuantity(InventoryObj.id_inventory_header, InventoryObj.part_number);
                if (updateReturn != null)
                {
                    return new JsonResult() { Data = new { success = true, quantity = updateReturn.previous_quantity }, JsonRequestBehavior = JsonRequestBehavior.AllowGet, MaxJsonLength = Int32.MaxValue };
                }
                else
                    return Json(new { success = false, responseText = "Error inesperado!" }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(new { success = false, responseText = "Termino tu sesión." }, JsonRequestBehavior.AllowGet);
            }
        }
        [HttpPost]
        public JsonResult ActionInsetAdjustment()
        {
            if (HttpContext.User.Identity.Name != null)
            {
                try
                {
                    InventoryAdjustmentModel InventoryObj = new InventoryAdjustmentModel();
                    var statusFile = Request["statusFile"];
                    InventoryObj.id_inventory_header = Request["id_inventory_header"];
                    InventoryObj.part_number = Request["part_number"];
                    InventoryObj.previous_quantity = Convert.ToDecimal(Request["previous_quantity"]);
                    InventoryObj.new_quantity = Convert.ToDecimal(Request["new_quantity"]);
                    InventoryObj.justification = Request["justification"];
                    InventoryObj.cuser = HttpContext.User.Identity.Name;
                    InventoryObj.uuser = null;
                    InventoryObj.udate = null;
                    InventoryObj.program_id = "INVE004.cshtml";
                    var exist = _InventoryAdjustmentsBusiness.ExistInventoryAdjustment(Request["id_inventory_header"], Request["part_number"]);
                    if (exist == 5)
                    {
                        if (_InventoryAdjustmentsBusiness.AddAdjusment(InventoryObj))
                            return Json(new { success = true, responseText = "Registro exitoso!" }, JsonRequestBehavior.AllowGet);
                        else
                            return Json(new { success = false, responseText = "Ocurrió un error al registrar los datos.!" }, JsonRequestBehavior.AllowGet);
                    }
                    else if (exist == 9)
                        return Json(new { success = false, responseText = "Ya existe un ajuste aprobado sobre el producto" }, JsonRequestBehavior.AllowGet);
                    else
                    {
                        if (_InventoryAdjustmentsBusiness.UpdateAdjusment(InventoryObj))
                        {

                            return Json(new { success = true, responseText = "Ajuste Modificado correctamente" }, JsonRequestBehavior.AllowGet);
                        }else
                        {
                            return Json(new { success = false, responseText = "Ocurrió un error al registrar los datos.!" }, JsonRequestBehavior.AllowGet);
                        }
                    }


                }
                catch (Exception df)
                {
                    var k = df.Message;

                    throw;
                }
            }
            else
            {
                return Json(new { success = false, responseText = "Termino tu sesión." }, JsonRequestBehavior.AllowGet);
            }
        }
        [HttpPost]
        public ActionResult ActionInsetAdjustmentList(InventoryAdjustmentModel AdjustmentList)
        {
            if (User.Identity.IsAuthenticated)
            {
                try
                {

                    AdjustmentList.cuser = User.Identity.Name;
                    AdjustmentList.cdate = DateTime.Now;
                    AdjustmentList.uuser = null;
                    AdjustmentList.udate = null;
                    AdjustmentList.program_id = "INVE004.cshtml";
                    //if (_InventoryAdjustmentsBusiness.ExistInventoryAdjustment(AdjustmentList.id_inventory_header, AdjustmentList.part_number))
                    //    return Json(new { success = false, responseText = $"Ya existe un ajuste sobre el producto {AdjustmentList.part_number}" }, JsonRequestBehavior.AllowGet);
                    if (_InventoryAdjustmentsBusiness.AddAdjusment(AdjustmentList)) { }
                    else
                        return Json(new { success = false, responseText = "Ocurrió un error al registrar los datos.!" }, JsonRequestBehavior.AllowGet);

                    ////var stream = new MemoryStream();
                    //var Distrital = _sysRoleMasterBusiness.GetEmailsByRol(ConfigurationManager.AppSettings["Distrital"].ToString());
                    //var Inventarios = _sysRoleMasterBusiness.GetEmailsByRol(ConfigurationManager.AppSettings["PersonalInventarios"].ToString());
                    //var Email = Distrital +","+ Inventarios;
                    //var idInventory = AdjustmentList.id_inventory_header;
                    //var inventoryDescription = _inventoryHeaderBusiness.GetInventoryById(idInventory).inventory_description;         
                    //string siteName = _siteConfigBusiness.GetSiteCodeName();
                    //InventoryAdjustmentReport report = new InventoryAdjustmentReport();
                    //report.DataSource = report.printTable(AdjustmentList,siteName,idInventory,inventoryDescription);
                    //report.ExportToPdf(stream);
                    ////mandar correo
                    ////var Email = "ovillegas@elflorido.com.mx";
                    //_sendMail.folio = idInventory;
                    //_sendMail.subject = "Ajuste de Inventario";
                    //_sendMail.from = "Inventario "+siteName;
                    //_sendMail.body = "Se le notifica por parte del Sistema del Florido que la tienda "+siteName+" ha enviado correcciones del inventario "+inventoryDescription+" para su revisión";
                    //_sendMail.email = Email;
                    ////common.MailMessageAdjustmentInventory(_sendMail,stream);

                    return Json(new { success = true, responseText = "Registro exitoso!", list = _InventoryReportBusiness.GetInventoryAdjustementAll() }, JsonRequestBehavior.AllowGet);

                }
                catch (Exception df)
                {
                    var k = df.Message;
                    throw;
                }
            }
            else
            {
                return Json("SF", JsonRequestBehavior.AllowGet);
            }
        }
        [HttpPost]
        public ActionResult ActionSetAdjustment(string InventoryHeaderId, string PartNumber, int AdjustmentStatus)
        {
            if (HttpContext.User.Identity.Name != null)
            {
                var model = new List<AdjustmentRequestModel>();

                if (_InventoryAdjustmentsBusiness.UpdateAdjustment(InventoryHeaderId, PartNumber, AdjustmentStatus, HttpContext.User.Identity.Name, out model))
                {
                    var exist = _InventoryAdjustmentsBusiness.ExistInventoryAdjustmentInPending();
                    if (!exist)
                    {
                        //mandrar correo              
                        var Recibo = _sysRoleMasterBusiness.GetEmailsByRol(ConfigurationManager.AppSettings["Recibo"].ToString());
                        var MesaDeControl = _sysRoleMasterBusiness.GetEmailsByRol(ConfigurationManager.AppSettings["MesaControl"].ToString());
                        var Gerente = _sysRoleMasterBusiness.GetEmailsByRol(ConfigurationManager.AppSettings["Gerente"].ToString());
                        var Email = Recibo + "," + MesaDeControl + "," + Gerente;
                        var inventoryDescription = _inventoryHeaderBusiness.GetInventoryById(InventoryHeaderId).inventory_description;
                        string partDescription = _itemBusiness.GetOneItem(PartNumber).Description;
                        string ip = _siteConfigBusiness.GetSiteCodeAddress().IP;
                        //var Email = "ovillegas@elflorido.com.mx";
                        _sendMail.folio = InventoryHeaderId;
                        _sendMail.subject = "Respuesta Ajuste de Inventario";
                        _sendMail.from = "Florido";
                        _sendMail.buttonLink = "http://" + ip.Trim() + ":35/Warehouse/INVE010";
                        _sendMail.body = "Se le informa por parte del Sistema de El Florido que han sido revisado la(s) solicitud(es) de ajuste de inventario " + inventoryDescription + "";
                        _sendMail.email = Email;
                        common.MailMessageAdjustmentInventoryResponse(_sendMail);
                    }
                    return Json(new { status = 1, Model = model }, JsonRequestBehavior.AllowGet);
                }
                else
                    return Json(new { status = 2 }, JsonRequestBehavior.AllowGet);
            }
            else
                return Json(new { status = 0, responseText = "Termino tu sesión." }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult ActionGetInventoryDoc(string InventoryHeaderId, string PartNumber)
        {
            var model = _InventoryAdjustmentsBusiness.GetFile(InventoryHeaderId, PartNumber);

            byte[] FileBytes = Convert.FromBase64String(model.File.Split(',')[1]);
            string ExtentionFile = Path.GetExtension(model.FileName);
            string ContenType = "application/" + ExtentionFile.Replace(".", "");
            string FileName = model.FileName;

            return File(FileBytes, ContenType, FileName);
        }
        [HttpPost]
        public ActionResult ActionFinishInventory(string InventoryNo)
        {
            if (HttpContext.User.Identity.Name != null)
            {
                var updateReturn = _InventoryAdjustmentsBusiness.FillInventoryFinal(InventoryNo, HttpContext.User.Identity.Name, 9);
                if (updateReturn)
                {
                    return new JsonResult() { Data = new { success = true, responseText = "Registro exitoso!" }, JsonRequestBehavior = JsonRequestBehavior.AllowGet, MaxJsonLength = Int32.MaxValue };
                }
                else
                    return Json(new { success = false, responseText = "Error inesperado!" }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(new { success = false, responseText = "Termino tu sesión." }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public ActionResult ActionHistoryInventory(int status)
        {
            if (User.Identity.IsAuthenticated)
                return new JsonResult() { Data = _InventoryAdjustmentsBusiness.HistoryInventoryAdjustment(status), JsonRequestBehavior = JsonRequestBehavior.AllowGet, MaxJsonLength = Int32.MaxValue };
            return Json("SF", JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public ActionResult ActionInsertNewNegative(InventoryNegativeHeaderModel modelHeader , List<InventoryNegativeDetailModel> modelDetail)
        {
            if (User.Identity.IsAuthenticated)
                return new JsonResult() { Data = new { result = _InventoryAdjustmentsBusiness.InsertNewNegative(modelHeader,modelDetail,User.Identity.Name) , products  = _InventoryAdjustmentsBusiness.GetAllNegativeHeader() , InventoriesNegative = _itemStockBusiness.NegativeInventoryBalance() }, JsonRequestBehavior = JsonRequestBehavior.AllowGet, MaxJsonLength = Int32.MaxValue };
            return Json("SF", JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public ActionResult ActionGetNegativeDetail(int id)
        {
            if (User.Identity.IsAuthenticated)
                return new JsonResult() { Data = new { result = _InventoryAdjustmentsBusiness.GetNegativeDetail(id) }, JsonRequestBehavior = JsonRequestBehavior.AllowGet, MaxJsonLength = Int32.MaxValue };
            return Json("SF", JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public ActionResult ActionAdjustmentNegatives()
        {
            if (User.Identity.IsAuthenticated)
                return new JsonResult() { Data = new { result = _InventoryAdjustmentsBusiness.AdjustNegatives(User.Identity.Name), products = _InventoryAdjustmentsBusiness.GetAllNegativeHeader() }, JsonRequestBehavior = JsonRequestBehavior.AllowGet, MaxJsonLength = Int32.MaxValue };
            return Json("SF", JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public ActionResult ActionGetAllNegativeHeaderByDateAndStatus(DateTime dateInit , DateTime dateFin , int status)
        {
            if (User.Identity.IsAuthenticated)
                return new JsonResult() { Data = new { result = _InventoryAdjustmentsBusiness.GetAllNegativeHeaderByDateAndStatus(dateInit,dateFin,status) }, JsonRequestBehavior = JsonRequestBehavior.AllowGet, MaxJsonLength = Int32.MaxValue };
            return Json("SF", JsonRequestBehavior.AllowGet);
        }
        


    }
}