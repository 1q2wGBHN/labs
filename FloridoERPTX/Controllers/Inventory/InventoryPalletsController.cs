﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using App.BLL.Inventory;
using App.DAL.Inventory;
using App.Entities.ViewModels.Inventory;

namespace FloridoERPTX.Controllers.Inventory
{
    public class InventoryPalletsController : Controller
    {
        private readonly InventoryPalletsBusiness inventoryPalletsBusiness = new InventoryPalletsBusiness();
        // GET: InventoryPallets
        public JsonResult ActionPalletList(PalletListFilter filter)
        {
            return Json(inventoryPalletsBusiness.PalletList(filter), JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public JsonResult ActionCreatePallet(PalletModel model, List<PalletDetailModel> details, string username, string program_id)
        {
            return Json(inventoryPalletsBusiness.CreatePallet(model, details,  username, program_id));
        }
        [HttpPost]
        public JsonResult ActionDeletePallet(string serial_number)
        {
            return Json(inventoryPalletsBusiness.DeletePallet(serial_number));
        }


        public JsonResult ActionPalletItemList(PalletItemListFilter filter)
        {
            return Json(inventoryPalletsBusiness.PalletItemList(filter), JsonRequestBehavior.AllowGet);
        }

        public JsonResult ActionExistInPallet(string serial_number, string barcode)
        {
            return Json(inventoryPalletsBusiness.ExistInPallet(serial_number, barcode), JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult ActionAddToPallet(string serial_number, PalletDetailModel detail,string username, string program_id)
        {
            return Json(inventoryPalletsBusiness.AddToPallet(serial_number, detail,username,program_id));
        }

        [HttpPost]
        public JsonResult ActionDeleteFromPallet(string serial_number, List<int> id_detail_list)
        {
            return Json(inventoryPalletsBusiness.DeleteFromPallet(serial_number, id_detail_list));
        }

        [HttpPost]
        public JsonResult ActionAddPalletToInventory(string serial_number,string username,string program_id,int user_no)
        {
            return Json(inventoryPalletsBusiness.AddPalletToInventory(serial_number, username,  program_id,user_no));
        }
        public JsonResult ActionPalletById(string serial_number)
        {
            return Json(inventoryPalletsBusiness.PalletById(serial_number), JsonRequestBehavior.AllowGet);
        }
        public JsonResult ActionAddItemToPallet(PalletDetailModel model , string serial_number)
        {
            return Json(inventoryPalletsBusiness.AddToPallet(serial_number,model, Session["User"].ToString(),"INVE009"),JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public JsonResult ActionCreatePalletWeb(PalletModel model)
        {
            return Json(inventoryPalletsBusiness.CreatePallet(model, null, Session["User"].ToString(), "INVE009"));
        }
    }
}