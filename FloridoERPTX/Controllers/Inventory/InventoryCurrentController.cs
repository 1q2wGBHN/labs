﻿using App.BLL.Inventory;
using App.BLL.Site;
using DevExpress.XtraReports.UI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using FloridoERPTX.Reports;

namespace FloridoERPTX.Controllers.Inventory
{
    public class InventoryCurrentController : Controller
    {
        private readonly InventoryCurrentBusiness _InventoryCurrentBusiness;
        private readonly SiteConfigBusiness _siteConfigBusiness;
        private readonly InventoryReportBusiness _inventoryReportBusiness;
        public InventoryCurrentController()
        {
            _InventoryCurrentBusiness = new InventoryCurrentBusiness();
            _siteConfigBusiness = new SiteConfigBusiness();
            _inventoryReportBusiness = new InventoryReportBusiness();
        }
        // GET: InventoryCurrent
        //public ActionResult Index()
        //{
        //    return View();
        //}

        public ActionResult ActionGetInventoryDetail(int IdInventoryHeader)
        {
            if (Session["User"] != null)
            {
                return new JsonResult() { Data = _InventoryCurrentBusiness.GetInventoryDetail(IdInventoryHeader), JsonRequestBehavior = JsonRequestBehavior.AllowGet, MaxJsonLength = Int32.MaxValue };
            }
            return new JsonResult() { Data = "SF", JsonRequestBehavior = JsonRequestBehavior.AllowGet, MaxJsonLength = Int32.MaxValue };
        }

        [HttpPost]
        public ActionResult ActionSaveInventoryCurrent(string IdInventoryHeader)
        {
            if (Session["User"] != null)
            {
                //if (_InventoryCurrentBusiness.IsDateInventory(IdInventoryHeader))
                //{    SE QUITO PARA METER CICLIOS
                //if (_InventoryCurrentBusiness.ExistInventory(IdInventoryHeader))
                //{
                    if (_InventoryCurrentBusiness.GetInventory(1).Count <= 0)
                    {
                        if (_InventoryCurrentBusiness.EditStatusInventory(IdInventoryHeader, 1, User.Identity.Name.ToString()))
                        {
                            //_InventoryCurrentBusiness.UpdateJobCloseInventory(1); --Activaba el job
                            return new JsonResult() { Data = _InventoryCurrentBusiness.SaveInventoryCurrentModel(IdInventoryHeader, User.Identity.Name.ToString()), JsonRequestBehavior = JsonRequestBehavior.AllowGet, MaxJsonLength = Int32.MaxValue };
                        }
                        return Json(new { problem = 1, text = "Error al activar el inventario" }, JsonRequestBehavior.AllowGet);
                    }
                    return Json(new { problem = 2, text = "Ya se encuentra un inventario activo" }, JsonRequestBehavior.AllowGet);
                //}
                //return Json(new { problem = 1, text = "No contiene productos ese tipo de inventario" }, JsonRequestBehavior.AllowGet);
                //}
                //return Json(new { problem = 1, text = "No es la fecha para iniciar el inventario" }, JsonRequestBehavior.AllowGet);
            }
            return new JsonResult() { Data = "SF", JsonRequestBehavior = JsonRequestBehavior.AllowGet, MaxJsonLength = Int32.MaxValue };
        }


        [HttpPost]
        public ActionResult ActionUpdateQuantity(int Inventory, string IdInventoryHeader, string PartNumber, decimal Quantity, string Location)
        {
            if (User.Identity.IsAuthenticated)
            {
                if (_InventoryCurrentBusiness.AvailableInventory(IdInventoryHeader))
                {
                    return Json(new { problem = _InventoryCurrentBusiness.NewUpdateInventoryQuantity(Inventory, IdInventoryHeader, PartNumber, Quantity, User.Identity.Name, Location), Info = _InventoryCurrentBusiness.GetInventoryDiferentDetailItem(IdInventoryHeader, PartNumber, Location) }, JsonRequestBehavior.AllowGet);
                }
                //if (_InventoryCurrentBusiness.AvailableInventory(IdInventoryHeader))
                //{
                //    return Json(new { problem = _InventoryCurrentBusiness.UpdateInventoryQuantity(IdInventoryHeader,PartNumber,Quantity,User.Identity.Name,Location),Info = _InventoryCurrentBusiness.GetInventoryDiferentDetailItem(IdInventoryHeader, PartNumber, Location)  }, JsonRequestBehavior.AllowGet);
                //}
                return Json(new { problem = 8 }, JsonRequestBehavior.AllowGet);
            }
            return new JsonResult() { Data = "SF", JsonRequestBehavior = JsonRequestBehavior.AllowGet, MaxJsonLength = Int32.MaxValue };
        }

        [HttpPost]
        public ActionResult ActionCancelInventoryCurrent(string IdInventoryHeader)
        {
            if (Session["User"] != null)
            {
                if (_InventoryCurrentBusiness.GetInventory(1).Count <= 0)
                {
                    return new JsonResult() { Data = _InventoryCurrentBusiness.EditStatusInventory(IdInventoryHeader, 8, User.Identity.Name.ToString()), JsonRequestBehavior = JsonRequestBehavior.AllowGet, MaxJsonLength = Int32.MaxValue };
                }
                return Json(new { problem = 2, text = "Ya se encuentra un inventario activo y no puede cancelarce" }, JsonRequestBehavior.AllowGet);
            }
            return new JsonResult() { Data = "SF", JsonRequestBehavior = JsonRequestBehavior.AllowGet, MaxJsonLength = Int32.MaxValue };
        }

        //[HttpPost]
        //public ActionResult ActionPalletFilterOrDetailInventory(bool Pallet, bool PartNumberDiferent, string Location, int Departament, bool NotCount , string id)
        //{
        //    if (Session["User"] != null)
        //    {
        //        if (Pallet)
        //        {
        //            return new JsonResult() { Data = _InventoryCurrentBusiness.GetInventoryDiferentDetail(PartNumberDiferent, Location, Departament, NotCount , id), JsonRequestBehavior = JsonRequestBehavior.AllowGet, MaxJsonLength = Int32.MaxValue };
        //        }
        //        else
        //        {
        //            return new JsonResult() { Data = _InventoryCurrentBusiness.GetInventoryDiferentPallet(), JsonRequestBehavior = JsonRequestBehavior.AllowGet, MaxJsonLength = Int32.MaxValue };
        //        }
        //    }
        //    return new JsonResult() { Data = "SF", JsonRequestBehavior = JsonRequestBehavior.AllowGet, MaxJsonLength = Int32.MaxValue };
        //}

        [HttpPost]
        public ActionResult ActionPalletFilterOrDetailInventory(bool Pallet, bool PartNumberDiferent, string Location, int Departament, bool NotCount, string id)
        {
            if (Session["User"] != null)
            {
                if (Pallet)
                {
                    return new JsonResult() { Data = _InventoryCurrentBusiness.GetInventoryDiferentDetail(PartNumberDiferent, Location, Departament, NotCount, id), JsonRequestBehavior = JsonRequestBehavior.AllowGet, MaxJsonLength = Int32.MaxValue };
                }
                else
                {
                    return new JsonResult() { Data = _InventoryCurrentBusiness.GetInventoryDiferentPallet(), JsonRequestBehavior = JsonRequestBehavior.AllowGet, MaxJsonLength = Int32.MaxValue };
                }
            }
            return new JsonResult() { Data = "SF", JsonRequestBehavior = JsonRequestBehavior.AllowGet, MaxJsonLength = Int32.MaxValue };
        }



        [HttpPost]
        public ActionResult ActionPalletFilterOrDetailInventorys(string PartNumber , string location)
        {
            if (Session["User"] != null)
            {
                return new JsonResult() { Data = _InventoryCurrentBusiness.GetInventoryDiferentDetailOption(PartNumber , location), JsonRequestBehavior = JsonRequestBehavior.AllowGet, MaxJsonLength = Int32.MaxValue };
            }
            return new JsonResult() { Data = "SF", JsonRequestBehavior = JsonRequestBehavior.AllowGet, MaxJsonLength = Int32.MaxValue };
        }
        [HttpPost]
        public ActionResult ActionSyncInventory(string idInventory)
        {
            if (Session["User"] != null)
            {
                return Json(new { success = true, data = _InventoryCurrentBusiness.SyncInventory(idInventory, Session["User"].ToString()) }, JsonRequestBehavior.AllowGet);
            }
            return Json(new { success = false, data = "SF" }, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public ActionResult ActionGetinventoryAdjustment(int id)
        {
            if (Session["User"] != null)
            {
                return Json(new { success = true, data = _InventoryCurrentBusiness.InventoryHistoryDetailAll(id) }, JsonRequestBehavior.AllowGet);
            }
            return Json(new { success = false, data = "SF" }, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public ActionResult ActionGetDifferencesReport(string id, string inventoryDescription)
        {
            var data = _InventoryCurrentBusiness.GetInventoryDiferentDetailByDepartment(id);
            var dates = _inventoryReportBusiness.GetScrapDates(id);
            InventoryDifference report = new InventoryDifference();           
            (report.Bands["DetailReport"] as DetailReportBand).DataSource = report.printTable(data, _siteConfigBusiness.GetSiteCodeName(), id.ToString(), inventoryDescription,dates);
            (report.Bands["DetailReport"] as DetailReportBand).DataMember = "xrTableRowFormat";
            return PartialView("~/Views/ReportsViews/InventoryDifference.cshtml", report);
        } 
    }
}
