﻿using App.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.IO;
using System.Web;
using System.Web.Mvc;
using App.BLL.Inventory;
using FloridoERPTX.Reports;
using App.BLL.Site;
using System.Configuration;
using App.BLL.Configuration;

namespace FloridoERPTX.Controllers.Inventory
{
    public class InventoryReportController : Controller
    {
        // GET: InventoryReport
        public ActionResult Index()
        {
            return View();
        }
        private readonly InventoryReportBusiness _inventoryReportBusiness;
        private readonly SiteConfigBusiness _siteConfigBusiness;
        private readonly InventoryAdjustmentsBusiness _InventoryAdjustmentsBusiness;
        private readonly SendMail information;
        private readonly Common common;
        private readonly SysRoleMasterBusiness _sysRoleMasterBusiness;
        private readonly InventoryCurrentBusiness _InventoryCurrentBusiness;
        public InventoryReportController()
        {
            _inventoryReportBusiness = new InventoryReportBusiness();
            _siteConfigBusiness = new SiteConfigBusiness();
            _InventoryAdjustmentsBusiness = new InventoryAdjustmentsBusiness();
            _sysRoleMasterBusiness = new SysRoleMasterBusiness();
            information = new SendMail();
            common = new Common();
            _InventoryCurrentBusiness = new InventoryCurrentBusiness();
        }
        [HttpPost]
        public ActionResult ActionGetInventoryReportDetail(string id , string endDate)
        {
            if (Session["User"] != null)
            {
                var detail = _inventoryReportBusiness.GetInventoryReportDetail(id);          
                var stream = new MemoryStream();
                string siteName = _siteConfigBusiness.GetSiteCodeName();
                InventoryCloseReport report = new InventoryCloseReport();
                report.DataSource = report.printTable(detail, siteName ,id , endDate);
                report.ExportToPdf(stream);
                if (stream != null)
                {
                    return Json(new { success = true, responseText = Convert.ToBase64String(stream.ToArray()) }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(new { success = false, responseText = "No podemos mostrar archivo, contacta a sistemas." }, JsonRequestBehavior.AllowGet);
                }
            }
            else
            {
                return Json(new { success = false, responseText = "Terminó tu sesión." }, JsonRequestBehavior.AllowGet);
            }
        }
        [HttpPost]
        public ActionResult ActionUpdateInventoryStatus(string id)
        {
            if (Session["User"] != null)
            {
                return new JsonResult() { Data = _inventoryReportBusiness.UpdateInventoryStatus(id, Session["User"].ToString()), JsonRequestBehavior = JsonRequestBehavior.AllowGet, MaxJsonLength = Int32.MaxValue };
            }
            return new JsonResult() { Data = "SF", JsonRequestBehavior = JsonRequestBehavior.AllowGet, MaxJsonLength = Int32.MaxValue };
        }
        [HttpPost]
        public ActionResult ActionGetInventoryActive()
        {
            if (Session["User"] != null)
            {
                var sync = _inventoryReportBusiness.GetInventoryActive();
                if(sync.Count > 0)
                {
                    var idInventory = _inventoryReportBusiness.GetInventoryActive()[0].id_inventory_header;
                    return Json(new { Data = _inventoryReportBusiness.GetInventoryActive(), DataSync = _InventoryCurrentBusiness.GetLastSync(idInventory),ScrapDates = _InventoryCurrentBusiness.GetScrapDatesActive(idInventory), JsonRequestBehavior = JsonRequestBehavior.AllowGet, MaxJsonLength = Int32.MaxValue });
                }
                return Json(new { Data = _inventoryReportBusiness.GetInventoryActive(),DataSync ="", JsonRequestBehavior = JsonRequestBehavior.AllowGet, MaxJsonLength = Int32.MaxValue });
            }
            return new JsonResult() { Data = "SF", JsonRequestBehavior = JsonRequestBehavior.AllowGet, MaxJsonLength = Int32.MaxValue };
        }
        [HttpPost]
        public ActionResult ActionSendMail(string id)
        {
            if (Session["User"] != null)
            {
                //var detail = _inventoryReportBusiness.GetInventoryReportDetail(id); //Lo hacia desde aqui el query
                var status = _inventoryReportBusiness.UpdateInventoryStatus(id, Session["User"].ToString());
                var pallets = _inventoryReportBusiness.FinishInventory(id);
                if(status == "error")
                {
                    return Json(new { success = false, responseText = "Ha ocurrido un error" }, JsonRequestBehavior.AllowGet);
                }else
                {
                    //_InventoryCurrentBusiness.UpdateJobCloseInventory(0); -- Era el que activaba el job
                    var updateReturn = _InventoryAdjustmentsBusiness.FillInventoryFinal(id, HttpContext.User.Identity.Name, 0);
                    if (updateReturn)
                    {                        
                        var detail = _inventoryReportBusiness.GetInventoryReportDetail(id);
                        var stream = new MemoryStream();
                        string siteName = _siteConfigBusiness.GetSiteCodeName();
                        InventoryCloseReport report = new InventoryCloseReport();
                        report.DataSource = report.printTable(detail, siteName, id, status);
                        report.ExportToPdf(stream);
                        var Distrital = _sysRoleMasterBusiness.GetEmailsByRol(ConfigurationManager.AppSettings["Distrital"].ToString());
                        var Email = Distrital;
                        information.folio = id.ToString();
                        information.subject = "Cierre de Conteo Inventario";
                        information.from = "Inventario";
                        information.email = Email;
                        common.MailMessageInventoryClose(information, stream);
                        if (stream != null)
                        {
                            return Json(new { success = true, responseText = Convert.ToBase64String(stream.ToArray()) }, JsonRequestBehavior.AllowGet);
                        }
                        else
                        {
                            return Json(new { success = false, responseText = "No podemos mostrar archivo, contacta a sistemas." }, JsonRequestBehavior.AllowGet);
                        }
                    }
                    else
                    { return Json(new { success = false, responseText = "Error inesperado al correr ajuste!" }, JsonRequestBehavior.AllowGet); }
                }                
            }
            else
            {
                return Json(new { success = false, responseText = "Terminó tu sesión." }, JsonRequestBehavior.AllowGet);
            }
        }
        [HttpPost]
        public ActionResult ActionGetInventoryById(string id)
        {
            if (Session["User"] != null)
            {
                return Json(new { Data = _inventoryReportBusiness.GetInventoryById(id),dates = _inventoryReportBusiness.GetScrapDates(id) , JsonRequestBehavior = JsonRequestBehavior.AllowGet, MaxJsonLength = Int32.MaxValue });
            }
            return new JsonResult() { Data = "SF", JsonRequestBehavior = JsonRequestBehavior.AllowGet, MaxJsonLength = Int32.MaxValue };
        }
    }
}