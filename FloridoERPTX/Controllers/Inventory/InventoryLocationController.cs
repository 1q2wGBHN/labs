﻿using App.BLL.Inventory;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using App.Entities.ViewModels.Inventory;

namespace FloridoERPTX.Controllers.Inventory
{
    public class InventoryLocationController : Controller
    {
        private readonly InventoryLocationBusiness _inventoryLocationBusiness = new InventoryLocationBusiness();
        // GET: InventoryLocation
        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public ActionResult ActionAddLocationInventory(InventoryLocationModel model)
        {
            if (Session["User"] != null)
            {
                model.cuser = Session["User"].ToString();
                var r = _inventoryLocationBusiness.AddInventoryLocation(model);
                if (r)
                {
                    return Json(new { success = true, data = _inventoryLocationBusiness.GetAllInventoryLocations() }, JsonRequestBehavior.AllowGet);
                }else
                {
                    return Json(new { success = false, data = _inventoryLocationBusiness.GetAllInventoryLocations() }, JsonRequestBehavior.AllowGet);
                }
            }
            return Json(new { success = false, data = "SF" }, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public ActionResult ActionUpdateLocationInventory(InventoryLocationModel model)
        {
            if (Session["User"] != null)
            {
                model.cuser = Session["User"].ToString();
                var r = _inventoryLocationBusiness.UpdateInventoryLocation(model);
                if (r)
                {
                    return Json(new { success = true, data = _inventoryLocationBusiness.GetAllInventoryLocations() }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(new { success = false, data = _inventoryLocationBusiness.GetAllInventoryLocations() }, JsonRequestBehavior.AllowGet);
                }
            }
            return Json(new { success = false, data = "SF" }, JsonRequestBehavior.AllowGet);
        }
        public ActionResult ActionGetÏtemsNoExhibited(string id)
        {
            if (Session["User"] != null)
                return new JsonResult() { Data = new { success = true, Data = _inventoryLocationBusiness.GetProductsNotExhibited(id) }, JsonRequestBehavior = JsonRequestBehavior.AllowGet, MaxJsonLength = int.MaxValue };
            else
                return Json(new { success = false, responseText = "SF" }, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public ActionResult ActionGetLocationsNoCounted(string id)
        {
            if (Session["User"] != null)
                return new JsonResult() { Data = new { success = true, Data = _inventoryLocationBusiness.GetLocationsNotCounted(id) }, JsonRequestBehavior = JsonRequestBehavior.AllowGet, MaxJsonLength = int.MaxValue };
            else
                return Json(new { success = false, responseText = "SF" }, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public ActionResult ActionGetInventoryFormat(string location)
        {
            if (Session["User"] != null)
                return new JsonResult() { Data = new { success = true, Data = _inventoryLocationBusiness.GetInventoryFormat(location) }, JsonRequestBehavior = JsonRequestBehavior.AllowGet, MaxJsonLength = int.MaxValue };
            else
                return Json(new { success = false, Data = "SF" }, JsonRequestBehavior.AllowGet);

        }
        [HttpPost]
        public ActionResult ActionAddInventoryFormat(string location, string part_number)
        {
            if (Session["User"] != null)
                if (_inventoryLocationBusiness.AddInventoryFormat(location, part_number, Session["User"].ToString() , "INVE008.cshtml"))
                    return new JsonResult() { Data = new { success = true, Data = _inventoryLocationBusiness.GetInventoryFormat(location) }, JsonRequestBehavior = JsonRequestBehavior.AllowGet, MaxJsonLength = int.MaxValue };
                else
                    return Json(new { success = false, Data = "" }, JsonRequestBehavior.AllowGet);

            else
                return Json(new { success = false, Data = "SF" }, JsonRequestBehavior.AllowGet);

        }
        [HttpPost]
        public ActionResult ActionDeleteInventoryFormat(string location, string part_number)
        {
            if (Session["User"] != null)
                if (_inventoryLocationBusiness.DeleteInventoryFormat(location, part_number))
                    return new JsonResult() { Data = new { success = true, Data = _inventoryLocationBusiness.GetInventoryFormat(location) }, JsonRequestBehavior = JsonRequestBehavior.AllowGet, MaxJsonLength = int.MaxValue };
                else
                    return Json(new { success = false, Data = "" }, JsonRequestBehavior.AllowGet);

            else
                return Json(new { success = false, Data = "SF" }, JsonRequestBehavior.AllowGet);

        }
    }
} 