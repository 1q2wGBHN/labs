﻿using App.BLL.Inventory;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using App.Entities.ViewModels.Inventory;
using System.IO;
using App.BLL.Site;
using App.BLL.StorageLocation;
using FloridoERPTX.Reports;

namespace FloridoERPTX.Controllers.Inventory
{
    public class InventoryHeaderController : Controller
    {
        private readonly InventoryHeaderBusiness _inventoryHeaderBusiness;
        private readonly InventoryUsersBusiness _inventoryUsersBusiness;
        private readonly InventoryPalletsBusiness _inventoryPalletsBusiness;
        private readonly InventoryDetailBusiness _inventoryDetailBusiness;
        private readonly SiteConfigBusiness _siteConfigBusiness;
        private readonly PlanogramLocationBusiness _planogramLocationBusiness;

        public InventoryHeaderController()
        {
            _inventoryHeaderBusiness = new InventoryHeaderBusiness();
            _inventoryUsersBusiness = new InventoryUsersBusiness();
            _inventoryPalletsBusiness = new InventoryPalletsBusiness();
            _inventoryDetailBusiness = new InventoryDetailBusiness();
            _siteConfigBusiness = new SiteConfigBusiness();
            _planogramLocationBusiness = new PlanogramLocationBusiness();
        }
        public ActionResult ActionGetInventorysByDate(DateTime startDate, DateTime endDate)
        {
            if (Session["User"] != null)
            {
                var DataInventory = _inventoryHeaderBusiness.GetInventorysByDate(startDate, endDate);
                if (DataInventory.Count > 0)
                    return Json(new { success = true, Inventories = DataInventory }, JsonRequestBehavior.AllowGet);
                else
                    return Json(new { success = false, responseText = "No existen inventarios en la feha seleccionada." }, JsonRequestBehavior.AllowGet);
            }
            return Json(new { success = false, responseText = "SF" }, JsonRequestBehavior.AllowGet);
        }

        public JsonResult ActionGetInventoryTagManager()
        {
            return Json(_inventoryHeaderBusiness.GetInventoryTagManager(), JsonRequestBehavior.AllowGet);
        }

        public JsonResult ActionInventoryStatus(string id_inventory)
        {
            return Json(_inventoryHeaderBusiness.InventoryStatus(id_inventory), JsonRequestBehavior.AllowGet);
        }

        public ActionResult ActionGetInventoryUsersCountPallets(string inventory_id)
        {
            if (Session["User"] != null)
                return Json(new { success = true, Data = _inventoryUsersBusiness.GetInventoryUsersCountPallets(inventory_id) }, JsonRequestBehavior.AllowGet);
            else
                return Json(new { success = false, responseText = "SF" }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult ActionGetInventoryUsersCountSerialNumber(string inventory_id, int emp_no)
        {
            if (Session["User"] != null)
                return Json(new { success = true, Data = _inventoryPalletsBusiness.GetInventoryUsersCountSerialNumber(inventory_id, emp_no) }, JsonRequestBehavior.AllowGet);
            else
                return Json(new { success = false, responseText = "SF" }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult ActionGetInventoryInfo(string inventory_id)
        {
            if (Session["User"] != null)
                return Json(new { success = true, Data = _inventoryHeaderBusiness.GetInvenotoryInfo(inventory_id) }, JsonRequestBehavior.AllowGet);
            else
                return Json(new { success = false, responseText = "SF" }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult ActionGetItemsDetail(string inventory_id)
        {
            if (Session["User"] != null)
            {
                var status = _inventoryHeaderBusiness.InventoryStatus(inventory_id);
                if (status.Data == 1)
                {
                    return Json(new { success = true, Data = _inventoryDetailBusiness.GetAllItemsDetailById(inventory_id) }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(new { success = true, Data = _inventoryDetailBusiness.GetAllItemsDetailAdjustmentById(inventory_id) }, JsonRequestBehavior.AllowGet);
                }
            }
            else
                return Json(new { success = false, responseText = "SF" }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult ActionGetPalletsHeader(string inventory_id)
        {
            if (Session["User"] != null)
                return Json(new { success = true, Data = _inventoryPalletsBusiness.GetInventoryPalletsById(inventory_id) }, JsonRequestBehavior.AllowGet);
            else
                return Json(new { success = false, responseText = "SF" }, JsonRequestBehavior.AllowGet);
        }


        public ActionResult ActionGetPalletsDetail(string serial_number)
        {
            if (Session["User"] != null)
                return Json(new { success = true, Data = _inventoryPalletsBusiness.GetInventoryPalletsDetail(serial_number) }, JsonRequestBehavior.AllowGet);
            else
                return Json(new { success = false, responseText = "SF" }, JsonRequestBehavior.AllowGet);
        }
        public ActionResult ActionAddRegisterInventoryDetail(string id_Inventory, string part_number, string location, decimal quantity)
        {
            if (Session["User"] != null)
                return Json(new { success = true, Data = _inventoryDetailBusiness.SetInventoryCountWeb(id_Inventory, part_number, quantity, location, Session["User"].ToString()) }, JsonRequestBehavior.AllowGet);
            else
                return Json(new { success = false, responseText = "SF" }, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public ActionResult ActionGetLocationsInventory(string id_Inventory)
        {
            if (Session["User"] != null)
                return Json(new { success = true, Data = _planogramLocationBusiness.GetLocationsPlanogramByInventory(id_Inventory) }, JsonRequestBehavior.AllowGet);
            else
                return Json(new { success = false, responseText = "SF" }, JsonRequestBehavior.AllowGet);
        }
        public ActionResult ActionGetReport(List<InventoryItemLocationModel> items, string desc , string type)
        {
            var stream = new MemoryStream();
            string siteName = _siteConfigBusiness.GetSiteCodeName();
            if(type == "location")
            {
                LocationProductsReport report = new LocationProductsReport();
                report.DataSource = report.printTable(items, siteName, DateTime.Now, desc, type);
                report.ExportToPdf(stream);
            }
            else
            {
                ItemsFormatInventory report = new ItemsFormatInventory();
                report.DataSource = report.printTable(items, siteName, DateTime.Now, desc, type);
                report.ExportToPdf(stream);
            }         
            return Json(new { success = true, Data = Convert.ToBase64String(stream.ToArray()) }, JsonRequestBehavior.AllowGet);

        }
        public ActionResult ActionGetInventoriesByDate(DateTime dateInitial , DateTime dateFinish)
        {
            if (Session["User"] != null)
                return Json(new { success = true, Data = _inventoryHeaderBusiness.GetInventorysByDatesNoPallets(dateInitial,dateFinish) }, JsonRequestBehavior.AllowGet);
            else
                return Json(new { success = false, responseText = "SF" }, JsonRequestBehavior.AllowGet);
        }
        public ActionResult ActionGetInventoryItemsByLocation(string id, List<string> location , string type , List<string> items)
        {
            if (Session["User"] != null)
                return new JsonResult() { Data = new { success = true, Data = _inventoryDetailBusiness.GetInventoryItemsByLocation(id, location, type, items) }, JsonRequestBehavior = JsonRequestBehavior.AllowGet, MaxJsonLength = int.MaxValue };
            else
                return Json(new { success = false, responseText = "SF" }, JsonRequestBehavior.AllowGet);
        }
        public ActionResult ActionGetInventoryById(string id)
        {
            if (Session["User"] != null)
                return new JsonResult() { Data = new { success = true, Data = _inventoryHeaderBusiness.GetInventoryById(id) }, JsonRequestBehavior = JsonRequestBehavior.AllowGet, MaxJsonLength = int.MaxValue };
            else
                return Json(new { success = false, responseText = "SF" }, JsonRequestBehavior.AllowGet);
        }





    }
}