﻿using App.BLL.Inventory;
using App.Entities.ViewModels.Inventory;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using App.BLL.Site;
using FloridoERPTX.Reports;

namespace FloridoERPTX.Controllers.Inventory
{
    public class InventoryLabelController : Controller
    {
        private readonly InventoryLabelBusiness _inventoryLabelBusiness;
        private readonly SiteConfigBusiness _siteConfigBusiness;
        private InventoryHeaderBusiness _inventoryHeaderBusiness;

        public InventoryLabelController()
        {
            _inventoryLabelBusiness = new InventoryLabelBusiness();
            _siteConfigBusiness = new SiteConfigBusiness();
            _inventoryHeaderBusiness = new InventoryHeaderBusiness();
        }

        public ActionResult ActionCreateLabels(string id, int quantity)
        {
            if (Session["User"] != null)
            {
                if (_inventoryLabelBusiness.CreateLabels(id, quantity, Session["User"].ToString()))
                {
                    var siteName = _siteConfigBusiness.GetSiteCodeName();
                    var list = _inventoryLabelBusiness.GetAllLabels(id);
                    var inventory = _inventoryHeaderBusiness.GetInventoryById(id);

                    List<InventoryLabelModel> newList = new List<InventoryLabelModel>();
                    foreach (var item in list)
                    {
                        newList.Add(new InventoryLabelModel { serial_partnumer = item });
                    }

                    InventoryLabelsReport report = new InventoryLabelsReport();
                    report.DataSource = newList;

                    report.Print(id, inventory.inventory_name, siteName);


                    var stream = new MemoryStream();
                    report.ExportToPdf(stream);

                    return Json(new { success = true, responseText = Convert.ToBase64String(stream.ToArray()) }, JsonRequestBehavior.AllowGet);

                }
                return Json(new { success = false }, JsonRequestBehavior.AllowGet);
            }
            return RedirectToAction("Login", "Home");
        }

        public ActionResult ActionReprintLabels(string id)
        {
            if (Session["User"] != null)
            {
                var siteName = _siteConfigBusiness.GetSiteCodeName();
                var list = _inventoryLabelBusiness.GetAllLabels(id);
                var inventory = _inventoryHeaderBusiness.GetInventoryById(id);

                List<InventoryLabelModel> newList = new List<InventoryLabelModel>();
                foreach (var item in list)
                {
                    newList.Add(new InventoryLabelModel { serial_partnumer = item });
                }

                InventoryLabelsReport report = new InventoryLabelsReport();
                report.DataSource = newList;
                report.Print(id, inventory.inventory_name, siteName);

                var stream = new MemoryStream();
                report.ExportToPdf(stream);

                return Json(new { success = true, responseText = Convert.ToBase64String(stream.ToArray()) }, JsonRequestBehavior.AllowGet);

            }
            return RedirectToAction("Login", "Home");
        }


        public JsonResult ActionGetInventoryLastLabel(string id_inventory)
        {
            return Json(_inventoryLabelBusiness.GetLastLabel(id_inventory), JsonRequestBehavior.AllowGet);
        }

        public JsonResult ActionSendInventoryLabels(List<InventoryLabelModel> labels)
        {
            return Json(_inventoryLabelBusiness.SendInventoryLabels(labels), JsonRequestBehavior.AllowGet);
        }
        public JsonResult ActionGetEmptyLabels(string id)
        {
            return Json(_inventoryLabelBusiness.GetEmptyLabels(id), JsonRequestBehavior.AllowGet);
        }
        public JsonResult ActionGetAllLabels(string id)
        {
            return Json(_inventoryLabelBusiness.GetAllLabels(id), JsonRequestBehavior.AllowGet);
        }
        public JsonResult ActionGetPalletHeaderByLabel(string serial_number)
        {
            return Json(_inventoryLabelBusiness.GetPalletHeaderByLabel(serial_number), JsonRequestBehavior.AllowGet);
        }
        public JsonResult ActionGetPreinventarioReport(List<InventoryItemLocationModel> items, string location, string serial_number)
        {
            var stream = new MemoryStream();
            string siteName = _siteConfigBusiness.GetSiteCodeName();
            PreinventarioReport report = new PreinventarioReport();
            report.DataSource = report.printTable(items, siteName, DateTime.Now, location, serial_number);
            report.ExportToPdf(stream);
            return Json(new { success = true, Data = Convert.ToBase64String(stream.ToArray()) }, JsonRequestBehavior.AllowGet);

        }
    }
}