﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using App.BLL.Inventory;
using App.Common;
using App.Entities.ViewModels.Inventory;

namespace FloridoERPTX.Controllers.Inventory
{
    public class InventoryUsersController : Controller
    {
        private readonly InventoryUsersBusiness _inventoryUsersBusiness;

        public InventoryUsersController()
        {
            _inventoryUsersBusiness = new InventoryUsersBusiness();
        }
        // GET: Inventory
        public ActionResult Index()
        {
            return View();
        }
        [HttpPost]
        public ActionResult ActionEditUserInventory(string id, int user_no, string user_name, string emp_no , int status)
        {
            if (Session["User"] != null)
            {
                return new JsonResult() { Data = _inventoryUsersBusiness.EditInventoryUser(id, user_no, user_name, emp_no, Session["User"].ToString(),status), JsonRequestBehavior = JsonRequestBehavior.AllowGet, MaxJsonLength = Int32.MaxValue };
            }
            return new JsonResult() { Data = "SF", JsonRequestBehavior = JsonRequestBehavior.AllowGet, MaxJsonLength = Int32.MaxValue };
        }
        [HttpPost]        
        public ActionResult ActionGetInventoryActive()
        {
            if (Session["User"] != null)
            {
                return new JsonResult() { Data = _inventoryUsersBusiness.GetInventoryActive(), JsonRequestBehavior = JsonRequestBehavior.AllowGet, MaxJsonLength = Int32.MaxValue };
            }
            return new JsonResult() { Data = "SF", JsonRequestBehavior = JsonRequestBehavior.AllowGet, MaxJsonLength = Int32.MaxValue };
        }
        [HttpPost]
        public ActionResult ActionGetInventoryUsers(string id)
        {
            if (Session["User"] != null)
            {
                return new JsonResult() { Data = _inventoryUsersBusiness.GetInventoryUsers(id), JsonRequestBehavior = JsonRequestBehavior.AllowGet, MaxJsonLength = Int32.MaxValue };
            }
            return new JsonResult() { Data = "SF", JsonRequestBehavior = JsonRequestBehavior.AllowGet, MaxJsonLength = Int32.MaxValue };
        }
        
        public JsonResult ActionGetInventoryUsersAndroid(string id_inventory)
        {
            return Json(Result.OK(_inventoryUsersBusiness.GetInventoryUsers(id_inventory)),
                JsonRequestBehavior.AllowGet);
        }
        public JsonResult ActionGetInventoryActiveAndroid()
        {
            List<InventoryHeaderModel> r = _inventoryUsersBusiness.GetInventoryActive().inventoryHeader; 

            return Json( r.Any() ? Result.OK(r) : Result.Error("No hay inventarios abiertos"), JsonRequestBehavior.AllowGet);
        }

        public JsonResult ActionGetInventoryCountActiveAndroid()
        {
            var r = _inventoryUsersBusiness.GetInventoryCountActive().inventoryHeader.FirstOrDefault();
            return Json(r != null ? Result.OK(r) : Result.Error("No hay inventarios activos"), JsonRequestBehavior.AllowGet);
        }
    }
}