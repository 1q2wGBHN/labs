﻿using App.BLL;
using App.BLL.EmptySpace;
using App.BLL.EntryFree;
using App.BLL.Inventory;
using App.BLL.Item;
using App.BLL.MaClass;
using App.BLL.MaCode;
using App.BLL.PurchaseOrder;
using App.BLL.RawMaterial;
using App.BLL.Rma;
using App.BLL.Site;
using App.BLL.StorageLocation;
using App.BLL.Supplier;
using App.BLL.Transfer;
using App.BLL.UserSpecs;
using App.DAL.Site;
using App.Entities.ViewModels.DamagedsGoods;
using App.Entities.ViewModels.EntryFree;
using App.Entities.ViewModels.Inventory;
using App.Entities.ViewModels.Item;
using App.Entities.ViewModels.PurchaseOrder;
using App.Entities.ViewModels.StorageLocation;
using FloridoERPTX.ViewModels.StorageLocation;
using FloridoERPTX.ViewModels.Warehouse;
using System;
using System.Configuration;
using System.Web.Mvc;

namespace FloridoERPTX.Controllers.Warehouse
{
    public class WarehouseController : Controller
    {
        private readonly SupplierBusiness _SupplierBussines;
        private readonly StorageLocationBusiness _storageLocationBusiness;
        private readonly MaCodeBusiness _maCodeBusiness;
        private readonly DamagedsGoodsBusiness _damagedsGoodsBusiness;
        private readonly TransferHeaderBusiness _TransferHeaderBusiness;
        private readonly ItemBusiness _itemMasterBussines;
        private readonly ItemStockBusiness _itemStockBusiness;
        private readonly SiteBusiness _siteBusiness;
        private readonly RawMaterialBusiness _rawMaterialBusiness;
        private readonly MaClassBusiness _maClassBusiness;
        private readonly PlanogramLocationBusiness _PlanogramLocationBusiness;
        private readonly EmptySpaceBusiness _emptySpaceBusiness;
        private readonly InventoryHeaderBusiness _InvetoryHeaderBusiness;
        private readonly InventoryCurrentBusiness _InventoryCurrentBusiness;
        private readonly SiteConfigRepository _siteConfigRepository;
        private readonly InventoryAdjustmentsBusiness _InventoryAdjustmentsBusiness;
        private readonly InventoryHeaderBusiness _inventoryHeaderBusiness;
        private readonly InventoryUsersBusiness _inventoryUsersBusiness;
        private readonly UserSpecsBusiness _UserSpecsBusiness;
        private readonly UserMasterBusiness _UserMasterBusiness;
        private readonly EntryFreeHeadBusiness _EntryFreeHeadBusiness;
        private readonly PurchaseOrderBusiness _PurchaseOrderBusiness;
        private readonly InventoryLocationBusiness _inventoryLocationBusiness;

        public WarehouseController()
        {
            _SupplierBussines = new SupplierBusiness();
            _storageLocationBusiness = new StorageLocationBusiness();
            _maCodeBusiness = new MaCodeBusiness();
            _damagedsGoodsBusiness = new DamagedsGoodsBusiness();
            _TransferHeaderBusiness = new TransferHeaderBusiness();
            _itemMasterBussines = new ItemBusiness();
            _itemStockBusiness = new ItemStockBusiness();
            _siteBusiness = new SiteBusiness();
            _rawMaterialBusiness = new RawMaterialBusiness();
            _maClassBusiness = new MaClassBusiness();
            _PlanogramLocationBusiness = new PlanogramLocationBusiness();
            _emptySpaceBusiness = new EmptySpaceBusiness();
            _InvetoryHeaderBusiness = new InventoryHeaderBusiness();
            _InventoryCurrentBusiness = new InventoryCurrentBusiness();
            _siteConfigRepository = new SiteConfigRepository();
            _InventoryAdjustmentsBusiness = new InventoryAdjustmentsBusiness();
            _inventoryHeaderBusiness = new InventoryHeaderBusiness();
            _inventoryUsersBusiness = new InventoryUsersBusiness();
            _UserSpecsBusiness = new UserSpecsBusiness();
            _UserMasterBusiness = new UserMasterBusiness();
            _EntryFreeHeadBusiness = new EntryFreeHeadBusiness();
            _PurchaseOrderBusiness = new PurchaseOrderBusiness();
            _inventoryLocationBusiness = new InventoryLocationBusiness();
        }

        public ActionResult OCRC001()
        {
            var Purchase = new PurchaseAndMaCode()
            {
                MaCodeList = _maCodeBusiness.GetListGeneric("PURCHASE_ORDER_DETAIL"),
                PurchaseList = _PurchaseOrderBusiness.GetAllOrders()
            };
            return View(Purchase);
        }

        public ActionResult MENWSM000()
        {
            return View();
        }

        public ActionResult WSM001()
        {
            return View();
        }

        public ActionResult EMSC001()
        {
            return View(_SupplierBussines.GetAll());
        }

        public ActionResult EMSC002()
        {
            EntryFreeAndSupplierModel EntreFreeAndSupplier = new EntryFreeAndSupplierModel
            {
                EntryFreeList = _EntryFreeHeadBusiness.GetAllEntryFreeOptions(DateTime.Now.AddDays(-30), DateTime.Now.AddDays(7), 0, 0, "0", "0", "", ""),
                SupplierList = _SupplierBussines.GetAll()
            };
            return View(EntreFreeAndSupplier);
        }

        public ActionResult STGL001()
        {
            StorageLocationViewModel model = new StorageLocationViewModel
            {
                Locations = _storageLocationBusiness.GetAllLocations(),
                TypeLocations = _maCodeBusiness.GetListTypeLocations()
            };
            return View(model);
        }

        public ActionResult WSM002()
        {
            return View();
        }

        public ActionResult RMA002()
        {
            return View(_damagedsGoodsBusiness.GetAllRmaHeadStatus("RMA", DateTime.Now.AddDays(-7), DateTime.Now, 1));
        }
        public ActionResult RMA011()
        {
            return View(_damagedsGoodsBusiness.GetAllRmaHeadStatus("RMA", DateTime.Now.AddDays(-7), DateTime.Now, 0));
        }

        public ActionResult RMA007()
        {
            return View(_damagedsGoodsBusiness.GetAllRmaHeadStatusToCancelRequest("RMA", 9));
        }

        public ActionResult RMA000()
        {
            return View();
        }

        public ActionResult RMA001()
        {
            return View(_SupplierBussines.GetAllDevolution());
        }

        public ActionResult RMA003()
        {
            if (Session["User"] != null)
            {
                DamagedGoodsViewModel model = new DamagedGoodsViewModel
                {
                    DamagedGoodsHeadListModel = _damagedsGoodsBusiness.GetAllRmaHeadStatus("Remision", DateTime.Now.AddDays(-60), DateTime.Now.AddDays(+30), 0),
                    DamagedGoodsBanatiModel = _damagedsGoodsBusiness.GetItemsBloqued()
                };
                return View(model);
            }

            return RedirectToAction("Login", "Home");
        }

        public ActionResult AI001()
        {
            if (Session["User"] != null)
                return View();

            return RedirectToAction("Login", "Home");
        }

        public ActionResult AI002()
        {
            if (Session["User"] != null)
                return View(_itemStockBusiness.GetInventoryAdjustmentRequests());

            return RedirectToAction("Login", "Home");
        }

        public ActionResult RMA004()
        {
            return View(_damagedsGoodsBusiness.GetAllRmaHeadStatus("RMA", DateTime.Now.AddDays(-7), DateTime.Now, 2));
        }

        public ActionResult RMA005()
        {
            return View(_damagedsGoodsBusiness.GetAllRmaHeadStatus("Remision", DateTime.Now.AddDays(-7), DateTime.Now, 1));
        }

        public ActionResult RMA006()
        {
            return View(_damagedsGoodsBusiness.GetAllRmaHeadStatus("Remision", DateTime.Now.AddDays(-7), DateTime.Now, 2));
        }

        public ActionResult RMA009()
        {
            return View();
        }

        public ActionResult RMA010()
        {
            return View();
        }

        //autorizacion Gerencia  transferencia Menu
        public ActionResult AGT000()
        {
            return View();
        }

        //autorizacion Gerencia  transferencia autorizado o rechaso
        public ActionResult AGT001()
        {
            return View(_TransferHeaderBusiness.GetAllTransferHeaderInit(DateTime.Now.AddDays(-7), DateTime.Now));
        }



        // reporte
        public ActionResult AGT003()
        {
            return View();
        }

        //salida merma
        public ActionResult SSM001()
        {
            return View(_damagedsGoodsBusiness.GetDocumentsByDates(DateTime.Now.AddDays(-7).ToString(), DateTime.Now.ToString()));
        }

        //aplicar salida merma
        public ActionResult SSM002()
        {
            return View(_damagedsGoodsBusiness.GetDocumentsByDatesInStatus2(DateTime.Now.AddDays(-7).ToString(), DateTime.Now.ToString()));
        }

        public ActionResult SSM003()
        {
            return View(_maClassBusiness.GetAllDepartments(1));
        }

        public ActionResult CT001()
        {
            return View(_TransferHeaderBusiness.GetAllTransferByMonth());
        }

        public ActionResult CT002()
        {
            if (Session["User"] != null)
            {
                CT002Model Model = new CT002Model
                {
                    Site = _siteBusiness.GetAllSitesFlorido()
                };
                return View(Model);
            }

            return RedirectToAction("Login", "Home");
        }

        public ActionResult WSM003()
        {
            return View();
        }

        public ActionResult RPD001()
        {
            return View();
        }

        //Recibo Cedis


        public ActionResult TMP001()
        {
            return View(_rawMaterialBusiness.GetAllRawMaterialHeader(0, DateTime.Now.AddDays(-7), DateTime.Now));
        }

        public ActionResult TMP002()
        {
            return View(_rawMaterialBusiness.GetAllRawMaterialHeader(2, DateTime.Now.AddDays(-7), DateTime.Now));
        }

        public ActionResult RPD002()
        {
            if (Session["User"] != null)
            {
                ItemSalesInventoryViewModel model = new ItemSalesInventoryViewModel
                {
                    Departments = _maClassBusiness.GetAllDepartments(1)
                };

                return View(model);
            }

            return RedirectToAction("Login", "Home");
        }

        public ActionResult RPD003()
        {
            if (Session["User"] != null)
            {
                RPD003Model Model = new RPD003Model
                {
                    Departamentos = _maClassBusiness.GetAllDepartments(1)
                };

                return View(Model);
            }

            return RedirectToAction("Login", "Home");
        }

        public ActionResult RPD004()
        {
            RPD003Model Model = new RPD003Model
            {
                Departamentos = _maClassBusiness.GetAllDepartments(1),
                Supplier = _SupplierBussines.GetAll()
            };
            return View(Model);
        }

        public ActionResult STGL002()
        {
            PlanogramLocationModel Model = new PlanogramLocationModel
            {
                PlanogramList = _PlanogramLocationBusiness.GetAllPlanogramLocations(),
                ItemsList = _itemMasterBussines.GetAllItemsActives()
            };
            return View(Model);
        }

        public ActionResult RPD005()
        {
            if (Session["User"] != null)
            {
                ItemSalesInventoryViewModel model = new ItemSalesInventoryViewModel
                {
                    Departments = _maClassBusiness.GetAllDepartments(1)
                };

                return View(model);
            }

            return RedirectToAction("Login", "Home");
        }

        public ActionResult ESS001()
        {
            return View(_emptySpaceBusiness.GetEmptySpaceByDates(DateTime.Now.AddDays(-7).ToString(), DateTime.Now.ToString()));
        }

        public ActionResult RMA008()
        {
            return View(_damagedsGoodsBusiness.GetAllRmaHeadStatus("RMA", DateTime.Now.AddDays(-30), DateTime.Now, 4));
        }

        public ActionResult INVE000()
        {
            return View();
        }

        public ActionResult INVE001()
        {
            if (Session["User"] != null)
            {
                DateTime date = DateTime.Today;
                var firstDayOfMonth = new DateTime(date.Year, date.Month, 1);
                var lastDayOfMonth = firstDayOfMonth.AddMonths(1).AddDays(-1).AddHours(23).AddMinutes(59);
                return View(_InvetoryHeaderBusiness.GetInventorysByDate(firstDayOfMonth, lastDayOfMonth));
            }

            return RedirectToAction("Login", "Home");
        }

        public ActionResult INVE002()
        {
            return View(_inventoryUsersBusiness.GetInventoryActive());
        }

        //Activacion de inventario
        public ActionResult INVE003()
        {
            var Inventory = new InventoryCurrentOptionModel
            {
                ListInventoryHeader = _InventoryCurrentBusiness.GetInventory(0),
                IsExistInventoryActive = _InventoryCurrentBusiness.GetInventoryExist().Count > 0,
                IsRoleCancele = _UserMasterBusiness.IsRolonUser(User.Identity.Name, "Jefe Inventario")
            };
            return View(Inventory);
        }

        //Cierre de conteo
        public ActionResult INVE004()
        {
            return View(new INVE004Model());
        }

        //Busquedas de diferencia
        public ActionResult INVE005()
        {
            string siteCode = _siteConfigRepository.GetSiteCode();
            var InventoryOption = new InventoryDifferentModel
            {
                //Location = _PlanogramLocationBusiness.GetAllPlanogramLocations(),//_storageLocationBusiness.GetLocationsByType("VENTA", siteCode);
                Location = _PlanogramLocationBusiness.GetLocationsPlanogramByInventoryActive(),
                FamilyActive = _maClassBusiness.GetAllFamilyByInventoryActive(),
                //InventoryOption.Location = _PlanogramLocationBusiness.GetAllPlanogramLocations();
                Family = _maClassBusiness.GetAllDepartments(2),
                Inventory = _inventoryHeaderBusiness.GetAllInventoryHeader()
            };
            return View(InventoryOption);
        }

        public ActionResult INVE007()
        {
            var r = _inventoryHeaderBusiness.GetInventory(new[] { 1, 2, 3, 9 });
            return View(r);
        }

        public ActionResult INVE006()
        {
            if (Session["User"] != null)
                return View(_InventoryAdjustmentsBusiness.GetAdjustmentRequests());

            return RedirectToAction("Login", "Home");
        }

        public ActionResult DRIV001()
        {
            return View(_UserSpecsBusiness.GetAllUserSpecsAndDriveFalse());
        }

        public ActionResult WSM005()
        {
            return View();
        }

        public ActionResult WSM006()
        {
            return View();
        }

        public ActionResult RPD006()
        {
            if (Session["User"] != null)
            {
                ItemSalesInventoryViewModel model = new ItemSalesInventoryViewModel
                {
                    Departments = _maClassBusiness.GetAllDepartments(1)
                };

                return View(model);
            }

            return RedirectToAction("Login", "Home");
        }

        public ActionResult RPD007()
        {
            if (Session["User"] != null)
            {
                RPD0StockReportViewModel model = new RPD0StockReportViewModel
                {
                    Departamentos = _maClassBusiness.GetAllDepartments(1),
                    LocationRaw = _storageLocationBusiness.GetLocationsByType("RAW", _siteConfigRepository.GetSiteCode())
                };
                return View(model);
            }

            return RedirectToAction("Login", "Home");
        }

        public ActionResult RPD008()
        {
            if (Session["User"] != null)
            {
                TransfersModels model = new TransfersModels
                {
                    Departamentos = _maClassBusiness.GetAllDepartments(1),
                    Suplier = _SupplierBussines.GetAllOnlySuppliers(),
                    Site = _siteBusiness.GetAllSitesFloridoExcludeThisSiteNoProcesadora(_siteConfigRepository.GetSiteCode())
                };
                return View(model);
            }

            return RedirectToAction("Login", "Home");
        }

        public ActionResult MENRMA0()
        {
            return View();
        }

        public ActionResult RPD011()
        {
            if (Session["User"] != null)
            {
                RPD0StockReportViewModel model = new RPD0StockReportViewModel
                {
                    Departamentos = _maClassBusiness.GetAllDepartments(1),
                    UserRoles = _UserMasterBusiness.GetUserRoles(Session["User"].ToString())
                };
                return View(model);
            }

            return RedirectToAction("Login", "Home");
        }

        public ActionResult RPD015()
        {
            if (Session["User"] != null)
            {
                RPD003Model Model = new RPD003Model
                {
                    Departamentos = _maClassBusiness.GetAllDepartments(1)
                };
                return View(Model);
            }
            return RedirectToAction("Login", "Home");
        }

        public ActionResult INVE008()
        {
            if (Session["User"] != null)
            {
                INVE008ViewModel model = new INVE008ViewModel
                {
                    ListSuppliers = _SupplierBussines.GetAll(),
                    ListMaClass = _maClassBusiness.GetAllDepartments(2)
                };
                return View(model);
            }

            return RedirectToAction("Login", "Home");
        }

        public ActionResult RECE002()
        {
            if (Session["User"] != null)
                return View();

            return RedirectToAction("Login", "Home");
        }

        public ActionResult INVE009()
        {
            if (Session["User"] != null)
                return View();

            return RedirectToAction("Login", "Home");
        }

        public ActionResult INVE010()
        {
            return View(new INVE004Model());
        }

        public ActionResult INVE011()
        {
            return View();
        }

        public ActionResult RPD012()
        {
            if (Session["User"] != null)
            {
                RPD0StockReportViewModel model = new RPD0StockReportViewModel
                {
                    Departamentos = _maClassBusiness.GetAllDepartments(1),
                    UserRoles = _UserMasterBusiness.GetUserRoles(Session["User"].ToString())
                };
                return View(model);
            }

            return RedirectToAction("Login", "Home");
        }

        public ActionResult RPD013()
        {
            if (Session["User"] != null)
                return View();

            return RedirectToAction("Login", "Home");
        }

        public ActionResult INVE012()
        {
            if (Session["User"] != null)
                return View(_inventoryHeaderBusiness.GetInventorysByDatesNoPallets(new DateTime(2019, 09, 15), DateTime.Now.AddDays(10)));

            return RedirectToAction("Login", "Home");
        }

        public ActionResult INVE013()
        {
            if (Session["User"] != null)
            {
                var user = Session["User"].ToString();
                INVE013ViewModelcs model = new INVE013ViewModelcs
                {
                    Negatives = _itemStockBusiness.NegativeInventoryBalance(),
                    NegativesBalances = _InventoryAdjustmentsBusiness.GetAllNegativeHeader(),
                    UserRole = _UserMasterBusiness.IsRolonUser(user, ConfigurationManager.AppSettings["DepartamentoInventarios"].ToString()) ||
                _UserMasterBusiness.IsRolonUser(user, ConfigurationManager.AppSettings["Sistemas"].ToString())
                };
                return View(model);
            }
            return RedirectToAction("Login", "Home");
        }
        public ActionResult INVE014()
        {
            if (Session["User"] != null)
            {
                return View(_InventoryAdjustmentsBusiness.GetAllNegativeHeaderByDateAndStatus(DateTime.Now.AddDays(-7), DateTime.Now, 0));
            }
            return RedirectToAction("Login", "Home");
        }
        public ActionResult INVE015()
        {
            if (Session["User"] != null)
            {
                return View(_inventoryHeaderBusiness.GetInventoryOrderByStart(new[] { 1, 2, 3, 9 }));
            }
            return RedirectToAction("Login", "Home");
        }
        public ActionResult INVE016()
        {
            if (Session["User"] != null)
            {
                INVE016Model model = new INVE016Model();
                model.InventoryClass = _maCodeBusiness.GetAllCodeByCode("INVENTORY_CLASS");
                model.InventoryType = _inventoryHeaderBusiness.GetInventoryTypeActive();
                model.InventoryLocations = _inventoryLocationBusiness.GetAllInventoryLocations();
                return View(model);
            }
            return RedirectToAction("Login", "Home");
        }

        public ActionResult INVE017()
        {
            if (Session["User"] != null)
            {

                return View(_inventoryHeaderBusiness.GetInventoryOrderByStart(new[] { 1, 2, 3, 9 }));
            }
            return RedirectToAction("Login", "Home");
        }


        public ActionResult INVE018(INVE018ViewModel model)
        {
            if (Session["User"] != null)
            {
                model.ListInventory = _inventoryHeaderBusiness.GetInventoryTagManager().Data;

                return View(model);
            }
            return RedirectToAction("Login", "Home");
        }



        //Aceptacion o Rechazo de Cancelacion de Transferencia
        public ActionResult CT003()
        {
            CT003ViewModel model = new CT003ViewModel
            {
                ListTransferStatus4 = _TransferHeaderBusiness.GetAllTransferHeaderStatus6(),
                ListTransferStatus9 = _TransferHeaderBusiness.GetAllTransferHeaderStatus7(),
                ListDrivers = _UserSpecsBusiness.GetAllUserSpecs()
            };
            return View(model);
        }

        public ActionResult RPD014()
        {
            if (Session["User"] != null)
                return View();

            return RedirectToAction("Login", "Home");
        }
        public ActionResult SSM004()
        {
            if (Session["User"] != null)
            {
                var date = DateTime.Now;
                var firstDayOfMonth = new DateTime(date.Year, date.Month, 1);
                var lastDayOfMonth = firstDayOfMonth.AddMonths(1).AddDays(-1);
                return View(_damagedsGoodsBusiness.GetItemsByDocumentApproval(firstDayOfMonth, lastDayOfMonth, 9));
            }

            return RedirectToAction("Login", "Home");
        }
        public ActionResult SSM005()
        {
            if (Session["User"] != null)
            {
                var date = DateTime.Now;
                var firstDayOfMonth = new DateTime(date.Year, date.Month, 1);
                var lastDayOfMonth = firstDayOfMonth.AddMonths(1).AddDays(-1);
                return View(_damagedsGoodsBusiness.GetItemsByDocumentReport(firstDayOfMonth, lastDayOfMonth, 6));
            }

            return RedirectToAction("Login", "Home");
        }


    }
}