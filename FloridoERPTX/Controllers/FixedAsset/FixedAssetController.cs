﻿using App.BLL.FixedAsset;
using App.BLL.Site;
using App.DAL.FixedAsset;
using FloridoERPTX.ViewModels.FixedAsset;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace FloridoERPTX.Controllers
{
    public class FixedAssetController : Controller
    {
        private FixedAssetRequisitionBusiness _fixedAssetRequisition;
        private FixedAssetBusiness _fixedAsset;
        private SiteConfigBusiness _siteConfig;
        private FixedAssetItemBusiness _fixedAssetItem;
        

        public FixedAssetController()
        {
            _fixedAsset = new FixedAssetBusiness();
            _fixedAssetRequisition = new FixedAssetRequisitionBusiness();
            _siteConfig = new SiteConfigBusiness();
            _fixedAssetItem = new FixedAssetItemBusiness();
        }

        public ActionResult RFA001(FixedAssetRequisitionViewModel model)
        {
            model.site_code = _siteConfig.GetSiteCode();
            model.site_name = _siteConfig.GetSiteCodeName();
            model.ListFixedAsset = _fixedAsset.GetAllFixedAsset();

            var query = _fixedAssetRequisition.GetRequisitionAssetBySite(model.site_code);
            foreach (var item in query)
            {
                item.requisition_date_string = item.requisition_date.Value.ToString("dd/MM/yyyy");
                item.delivery_date_string = item.delivery_date == null ? null : item.delivery_date.Value.ToString("dd/MM/yyyy");
            }
            model.ListRequisition = query;

            return View(model);
        }

        public ActionResult ActionAddRequisition(string remark, string date, int fixed_asset, List<FixedAssetItemModel> Items)
        {
            if (Session["User"] != null)
            {
                var site = _siteConfig.GetSiteCode();
                var requisition_code = _fixedAssetRequisition.AddRequisition(Session["User"].ToString(), remark, site);

                foreach (var item in Items)
                {
                    _fixedAssetItem.AddItem(Session["User"].ToString(), site, fixed_asset, item.description, item.brand, item.serie, item.model, requisition_code);
                }

                //Result
                var query = _fixedAssetRequisition.GetRequisitionAssetBySite(site);
                foreach (var item in query)
                {
                    item.requisition_date_string = item.requisition_date.Value.ToString("dd/MM/yyyy");
                    item.delivery_date_string = item.delivery_date == null ? null : item.delivery_date.Value.ToString("dd/MM/yyyy");
                }
                return Json(new { success = true, message = "Registro exitoso.", json =  query}, JsonRequestBehavior.AllowGet);
            }
            else
                return Json("SF", JsonRequestBehavior.AllowGet);
        }

        public ActionResult ActionGetAssetById(int id)
        {
            if (Session["User"] != null)
            {
                return Json(new { success = true, message = "Registro exitoso.", json = _fixedAsset.GetAssetById(id).category }, JsonRequestBehavior.AllowGet);
            }
            else
                return Json("SF", JsonRequestBehavior.AllowGet);
        }

        public ActionResult ActionGetItemsByRequisition(int id)
        {
            if (Session["User"] != null)
            {
                return Json(new { success = true, message = "Registro exitoso.", json = _fixedAssetItem.GetItemsByRequisition(id) }, JsonRequestBehavior.AllowGet);
            }
            else
                return Json("SF", JsonRequestBehavior.AllowGet);
        }


    }
}