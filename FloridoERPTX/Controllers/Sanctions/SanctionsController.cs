﻿using System;
using System.Collections.Generic;
using System.Web.Mvc;
using FloridoERPTX.Reports;
using DevExpress.XtraReports.UI;
using App.Entities.ViewModels.Sanctions;
using App.BLL.Sanctions;
using App.Entities;
using System.Globalization;
using App.BLL.Site;
using App.BLL;
using static App.Entities.ViewModels.Common.Constants;

namespace FloridoERPTX.Controllers.Sales
{
    public class SanctionsController : Controller
    {

        private SanctionsBusiness _SanctionsBusinnes;
        private SiteConfigBusiness _SiteConfigBussiness;
        private UserMasterBusiness _UserMasterBussines;

        public SanctionsController()
        {
            _SanctionsBusinnes = new SanctionsBusiness();
            _SiteConfigBussiness = new SiteConfigBusiness();
            _UserMasterBussines = new UserMasterBusiness();
        }
        
        public ActionResult SANC001()
        {
            var model = new SanctionIndex();
            model.IssueDate = DateTime.Now.Date.ToString(DateFormat.INT_DATE);
            model.CreateDate = DateTime.Now.Date.ToString(DateFormat.INT_DATE);
            model.SanctionsList = _SanctionsBusinnes.GetSantiosByModel(model);
            model.CanCancel = CanCancelSanctions();
            FillLookups();
            return View(model);
        }
       
        public ActionResult SANC002()
        {            
            var model = new SanctionIndex();
            model.CreateDate = DateTime.Now.Date.ToString(DateFormat.INT_DATE);
            model.CreateDate2 = DateTime.Now.Date.ToString(DateFormat.INT_DATE);
            model.SanctionsList = _SanctionsBusinnes.GetSantiosByModel(model);
            model.CanCancel = CanCancelSanctions();
            FillLookups();
            return View(model);
        }

        [HttpPost]
        public ActionResult SANC002(SanctionIndex model)
        {
            
            model.SanctionsList = _SanctionsBusinnes.GetSantiosByModel(model);
            model.CanCancel = CanCancelSanctions();
            FillLookups();
            return View(model);
        }

        [HttpPost]
        public ActionResult ActionCreateSanction(SanctionIndex model)
        {
            var _sanction = new SANCTIONS();
            _sanction.programid = "SANC001";
            _sanction.reason_no = long.Parse(model.Motive);
            _sanction.sanction_total = decimal.Parse(model.total);
            _sanction.site_cod = _SiteConfigBussiness.GetSiteCode();
            _sanction.status = (int)model.status;

            _sanction.cur_cod = model.currency;

            _sanction.emp_no = model.emp;
            _sanction.comments = model.Comments;
            _sanction.uuser = Session["User"].ToString();
            _sanction.sanction_date_app = DateTime.Now.Date;
            _sanction.santion_time = string.Format("{0:HH:mm:ss tt}", DateTime.Now);
            _sanction.sanction_date = DateTime.ParseExact(model.IssueDate, DateFormat.INT_DATE, CultureInfo.InvariantCulture);

            if (_SanctionsBusinnes.CreateSanction(_sanction))
                return Json(new { success = true, Json = _sanction, responseText = "Sanción Creada con éxito" }, JsonRequestBehavior.AllowGet);
            else
                return Json(new { success = false, Json = _sanction, responseText = "Ocurrió un error, Contacte a Sistemas" }, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public ActionResult ActionEditCustomer(long SanctionId)
        {
            if (Session["User"] != null)
            {
                var Model = new SanctionModel();
                Model = _SanctionsBusinnes.GetSanctionById(SanctionId);
                var list = new List<SelectListItem>();

                //Remove from customers list, General customer for daily Invoice
                var element1 = new SelectListItem();
                var element2 = new SelectListItem();

                element1.Text = "Pagada";
                element2.Text = "Pendiente";

                element1.Value = "1";
                element2.Value = "-1";

                list.Add(element1);
                list.Add(element2);
                var IssueDate = DateTime.ParseExact(Model.IssueDate, DateFormat.INT_DATE, CultureInfo.InvariantCulture);
                if (CanCancelSanctions() && IssueDate == DateTime.Now.Date)
                {
                    var element3 = new SelectListItem();
                    element3.Text = "Cancelada";
                    element3.Value = "0";
                    list.Add(element3);
                }

                ViewBag.status = list;
                return PartialView("_SANC001", Model);
            }
            return null;
        }

        //Save changes made to selected sanction       
        [HttpPost]
        public ActionResult ActionEditCustomer(SanctionModel model)
        {
            if (Session["User"] != null)
            {
                string User = Session["User"].ToString();
                var _sanction = new SANCTIONS();

                _sanction.programid = "SANC001";
                _sanction.reason_no = long.Parse(model.MotiveNo);
                _sanction.sanction_total = decimal.Parse(model.total);
                _sanction.site_cod = _SiteConfigBussiness.GetSiteCode();                

                _sanction.status = model.status;
                _sanction.cur_cod = model.currencyValue;

                _sanction.emp_no = model.emp_no.ToString();
                _sanction.comments = model.Comments;
                _sanction.uuser = User;
                _sanction.sanction_date = DateTime.Now.Date;
                _sanction.santion_time = string.Format("{0:HH:mm:ss tt}", DateTime.Now);
                _sanction.sanction_id = model.SanctionId;
                _sanction.sanction_date_app = DateTime.ParseExact(model.IssueDate, DateFormat.INT_DATE, CultureInfo.InvariantCulture);

                var SysLog = new SYS_LOG();
                DateTime dt = DateTime.Now;


                SysLog.Action = "ACTUALIZO";
                SysLog.Date = DateTime.Now.Date;
                SysLog.Description = "CAMBIO EL ESTADO DE LA SANCION";
                SysLog.idAfect = model.SanctionId;
                SysLog.TableName = "SANCIONES";
                SysLog.Menu = "CLIENTES";
                SysLog.time = dt.ToLongTimeString();
                SysLog.uuser = User;


                if (_SanctionsBusinnes.UpdateSanction(_sanction, SysLog))
                {
                    return Json(new { success = true, Json = _sanction, responseText = "Sanción Editada con éxito" }, JsonRequestBehavior.AllowGet);
                }

                return PartialView("_SANC001", model);
            }

            return Json(new { success = false, Json = model, responseText = "Sesión expiró" }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult ActionReportGral(SanctionIndex model)
        {
            SanctionReport report = new SanctionReport();

            string siteName = _SiteConfigBussiness.GetSiteCodeName();
            (report.Bands["DetailReport"] as DetailReportBand).DataSource = report.printTable(model.SanctionsList, model.CreateDate, model.CreateDate2, siteName);
            (report.Bands["DetailReport"] as DetailReportBand).DataMember = "xrTableRowFormat";
            return PartialView("~/Views/ReportsViews/SanctionsReport.cshtml", report);
        }

        void FillLookups()
        {
            var users = _SanctionsBusinnes.GetUsers();
            var motives = _SanctionsBusinnes.GetMotives();
            var currencys = _SanctionsBusinnes.GetCurrencys();
            var list = new List<SelectListItem>();
            var list2 = new List<SelectListItem>();
            var list3 = new List<SelectListItem>();

            //Remove from customers list, General customer for daily Invoice
            foreach (var item in users)
            {
                var element = new SelectListItem();
                element.Text = string.Format("{0} {1}", item.first_name, item.last_name);
                element.Value = item.emp_no;
                list.Add(element);
            }
            foreach (var item in motives)
            {
                var element = new SelectListItem();
                element.Text = string.Format("{0}", item.desc);
                element.Value = item.reason_no.ToString();
                list2.Add(element);
            }
            foreach (var item in currencys)
            {
                var element = new SelectListItem();
                element.Text = string.Format("{0} ({1})", item.cur_name, item.cur_code);
                element.Value = item.cur_code.ToString();
                list3.Add(element);
            }
            ViewBag.emp = list;
            ViewBag.Motive = list2;
            ViewBag.currency = list3;
        }

        private bool CanCancelSanctions()
        {
            string Uuser = Session["User"].ToString();
            return _UserMasterBussines.IsInRole(Uuser, Roles.GC) || _UserMasterBussines.IsInRole(Uuser, Roles.IT_Department);
        }

        [HttpPost]
        public ActionResult ActionCancelSanction(int SanctionId)
        {
            string User = Session["User"].ToString();
            var SysLog = new SYS_LOG();
            DateTime dt = DateTime.Now;            
            SysLog.Action = "CANCELAR";
            SysLog.Date = DateTime.Now.Date;
            SysLog.Description = string.Format("Usuario {0} canceló sanción {1}", User, SanctionId);
            SysLog.idAfect = SanctionId;
            SysLog.TableName = "SANCIONES";
            SysLog.Menu = "CLIENTES";
            SysLog.time = dt.ToLongTimeString();
            SysLog.uuser = User;

            var Sanction = _SanctionsBusinnes.GetSanctionEntityById(SanctionId);
            Sanction.uuser = User;
            Sanction.status = 0;

            if (_SanctionsBusinnes.UpdateSanction(Sanction, SysLog))
                return Json(new { success = true, Json = SanctionId, responseText = "Sanción Editada con éxito" }, JsonRequestBehavior.AllowGet);            

            return Json(new { success = false, Json = SanctionId, responseText = "Ocurrió un error, Contacte a Sistemas" }, JsonRequestBehavior.AllowGet);
        }
    }
}
