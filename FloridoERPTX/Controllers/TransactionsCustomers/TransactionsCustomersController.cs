﻿using App.BLL.Currency;
using App.BLL.Payments;
using App.BLL.TransactionsCustomers;
using App.Entities;
using App.Entities.ViewModels.Common;
using App.Entities.ViewModels.Payments;
using App.Entities.ViewModels.TransactionsCustomers;
using DevExpress.XtraReports.UI;
using FloridoERP.Report;
using FloridoERPTX.CommonMethods;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace FloridoERPTX.TransactionsCustomers
{
    public class TransactionsCustomersController : Controller
    {
        private CurrencyBusiness _CurrencyBusiness;
        private TransactionsCustomersBusiness _TransactionsCustomersBusiness;
        private PaymentsBusiness _PaymentsBusiness;
        private CustomerLookup _CustomerLookup;

        public TransactionsCustomersController()
        {
            _CurrencyBusiness = new CurrencyBusiness();
            _TransactionsCustomersBusiness = new TransactionsCustomersBusiness();
            _PaymentsBusiness = new PaymentsBusiness();
            _CustomerLookup = new CustomerLookup();
        }

        public ActionResult TRAC001()
        {
            var Model = new TransactionsCustomersReport();
            ViewBag.Currency = new SelectList(_CurrencyBusiness.CurrencyList(), "cur_code", "cur_name");            
            ViewBag.CustomerCode = _CustomerLookup.FillLookupCustomer(CustomersLookup.ByTransactions);
            return View(Model);
        }

        [HttpPost]
        public ActionResult TRAC001(TransactionsCustomersReport Model)
        {
            Model.ReportList = _TransactionsCustomersBusiness.GetReportData(Model);
            Model.IsFromMenu = false;
            ViewBag.Currency = new SelectList(_CurrencyBusiness.CurrencyList(), "cur_code", "cur_name");            
            ViewBag.CustomerCode = _CustomerLookup.FillLookupCustomer(CustomersLookup.ByTransactions);
            return View(Model);
        }        

        [HttpPost]
        public ActionResult ActionGetReport(TransactionsCustomersReport Model)
        {
            var Report = new ChargePaymentReport();
            (Report.Bands["DetailReport"] as DetailReportBand).DataSource = Report.printTable(Model);
            (Report.Bands["DetailReport"] as DetailReportBand).DataMember = "xrTableRowFormat";
            return PartialView("~/Views/ReportsViews/ChargePaymentReport.cshtml", Report);            
        }

        //pagos cancelados
        public ActionResult TRAC002()
        {
            ViewBag.CustomerCode = _CustomerLookup.FillLookupCustomer(CustomersLookup.ByCancelledPayments);            
            return View();
        }

        [HttpGet] //Get payments per client selected.
        public ActionResult ActionGetPaymentByCustomer(string CustomerCode)
        {
            var Payments = _PaymentsBusiness.PaymentsCancelledByCustomer(CustomerCode);
            return Json(new { success = true, Json = Payments, responseText = "Pagos de Clientes" }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult ActionPaymentsCacelledReport(TransactionsCustomersReport Model)
        {
            var Report = new ChargePaymentReport();
            (Report.Bands["DetailReport"] as DetailReportBand).DataSource = Report.printTable(Model);
            (Report.Bands["DetailReport"] as DetailReportBand).DataMember = "xrTableRowFormat";
            return PartialView("~/Views/ReportsViews/ChargePaymentReport.cshtml", Report);
        }
    }
}
