﻿using App.BLL;
using App.BLL.Donations;
using App.BLL.Site;
using App.Entities.ViewModels.Common;
using App.Entities.ViewModels.Donations;
using DevExpress.XtraReports.UI;
using FloridoERPTX.CommonMethods;
using FloridoERPTX.Reports;
using System.Collections.Generic;
using System.Web.Mvc;

namespace FloridoERPTX.Controllers.Donations
{
    public class DonationsController : Controller
    {
        private UserMasterBusiness _UserMasterBusiness;
        private DonationsBusiness _DonationsBusiness;
        private DonationsSalesBusiness _DonationsSalesBusiness;
        private DonationsSalesDetailsBusiness _DonationsSalesDetailsBusiness;
        private CashierLookup _CashierLookup;
        private SiteConfigBusiness _SiteConfigBusiness;
        private DonationsPaymentMethodBusiness _DonationsPaymentMethodBusiness;

        public DonationsController()
        {
            _UserMasterBusiness = new UserMasterBusiness();
            _DonationsBusiness = new DonationsBusiness();
            _DonationsSalesBusiness = new DonationsSalesBusiness();
            _DonationsSalesDetailsBusiness = new DonationsSalesDetailsBusiness();
            _CashierLookup = new CashierLookup();
            _SiteConfigBusiness = new SiteConfigBusiness();
            _DonationsPaymentMethodBusiness = new DonationsPaymentMethodBusiness();
        }

        public ActionResult DONA001()
        {
            var Model = new DonationIndex();
            FillLookups();
            return View(Model);
        }

        [HttpPost]
        public ActionResult DONA001(DonationIndex Model)
        {
            var User = !string.IsNullOrEmpty(Model.Cashier) ? _UserMasterBusiness.GetUserMasterByEmployeeNumber(Model.Cashier) : new App.Entities.USER_MASTER();
            var CampaignEntity = !string.IsNullOrEmpty(Model.Campaign) ? _DonationsBusiness.GetCampaignByDonationId(int.Parse(Model.Campaign)) : new App.Entities.DFLPOS_DONATIONS();

            Model.IsFromMenu = false;
            Model.CashierName = string.IsNullOrEmpty(Model.Cashier) ? "Todos los Cajeros" : string.Format("{0} {1}", User.first_name, User.last_name);
            Model.CampaignName = string.IsNullOrEmpty(Model.Campaign) ? "Todas las Campañas" : CampaignEntity.name;
            Model.DataReport = _DonationsBusiness.GetDonationsReportData(Model);
            if (Model.DataReport.Count > 0)
                Model.CashierTotals = _DonationsBusiness.GetTotalsByCashier(Model.DataReport);
            FillLookups();
            return View(Model);
        }

        [HttpPost]
        public ActionResult ActionGetDonationDetail(string UserId, string Date)
        {
            return Json(new { success = true, Json = _DonationsPaymentMethodBusiness.GetDonationsDetail(UserId, Date) }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult DONA002()
        {
            var Model = new DonationsTicketsIndex();
            FillLookups(true);
            return View(Model);
        }

        [HttpPost]
        public ActionResult DONA002(DonationsTicketsIndex Model)
        {
            Model.IsFromMenu = false;
            Model.DataReport = _DonationsSalesBusiness.GetReportInfo(Model);
            FillLookups(true);
            return View(Model);
        }

        public ActionResult ActionGetDonationsTicketsDetails(string DonationSaleId)
        {
            return Json(new { success = true, Json = _DonationsSalesDetailsBusiness.GetDonationsTicketsDetails(DonationSaleId) }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult ActionGetDonationReportBySale(DonationsTicketsHeader Model)
        {
            var report = new DonationTicketsDetailReport();
            var details = _DonationsSalesDetailsBusiness.GetDonationsTicketsDetails(Model.DonationSaleId);
            (report.Bands["DetailReport"] as DetailReportBand).DataSource = report.printTable(details, Model);
            (report.Bands["DetailReport"] as DetailReportBand).DataMember = "xrTableRowFormat";
            return PartialView("~/Views/ReportsViews/ReportView.cshtml", report);
        }

        [HttpPost]
        public ActionResult ActionGetDonationReportHeaderBySale(DonationsTicketsIndex Model)
        {
            var report = new DonationTicketsHeaderReport();            
            (report.Bands["DetailReport"] as DetailReportBand).DataSource = report.printTable(Model);
            (report.Bands["DetailReport"] as DetailReportBand).DataMember = "xrTableRowFormat";
            return PartialView("~/Views/ReportsViews/ReportView.cshtml", report);
        }

        [HttpPost]
        public ActionResult ActionGetDonationsReport(DonationIndex Model)
        {
            var report = new DonationsReport();
            string siteName = _SiteConfigBusiness.GetSiteCodeName();
            (report.Bands["DetailReport"] as DetailReportBand).DataSource = report.printTable(Model, siteName);
            (report.Bands["DetailReport"] as DetailReportBand).DataMember = "xrTableRowFormat";
            return PartialView("~/Views/ReportsViews/ReportView.cshtml", report);
        }

        void FillLookups(bool IsFromSales = false)
        {
            var CashierList = new List<SelectListItem>();            

            if (!IsFromSales)
            {
                CashierList = _CashierLookup.FillLookupCashiers(CashiersLookup.ByDonations);
                ViewBag.Campaign = new SelectList(_DonationsBusiness.GetCampaingDonations(), "donation_id", "name");
            }
            else
                CashierList = _CashierLookup.FillLookupCashiers(CashiersLookup.ByDonationsSales);            
            
            ViewBag.Cashier = CashierList;
        }
    }
}