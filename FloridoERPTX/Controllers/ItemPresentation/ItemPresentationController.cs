﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using App.BLL.ItemPresentationBusiness;
using App.Common;
using App.Entities.ViewModels.ItemPresentation;

namespace FloridoERPTX.Controllers.ItemPresentation
{

    public class ItemPresentationController : Controller
    {
        private readonly ItemPresentationBusiness _presentations;

        public ItemPresentationController()
        {
            _presentations = new ItemPresentationBusiness();
        }

        public JsonResult ActionGetAllPresentation()
        {
            if (Session["User"] == null) return Json(Result.SF(), JsonRequestBehavior.AllowGet);
            return Json(_presentations.GetAll(), JsonRequestBehavior.AllowGet);
        }
        public JsonResult ActionGetPresentationByProductId(string part_number)
        {
            if (Session["User"] == null) return Json(Result.SF(), JsonRequestBehavior.AllowGet);
            return Json(_presentations.GetPresentationByProductId(part_number), JsonRequestBehavior.AllowGet);
        }
        

    }
}