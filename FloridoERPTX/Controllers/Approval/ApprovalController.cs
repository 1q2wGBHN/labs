﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using App.BLL.Approvals;
using App.DAL.Approval;

namespace FloridoERPTX.Controllers.Approval
{
    public class ApprovalController : Controller
    {
        
        readonly ApprovalsBusiness approvalsBusiness = new ApprovalsBusiness();
        public JsonResult ActionProcessTypeList()
        {
            return Json(approvalsBusiness.ProcessTypeList(), JsonRequestBehavior.AllowGet);
        }

        public JsonResult ActionProcessList(string  process_type)
        {
            return Json(approvalsBusiness.ProcessList(process_type), JsonRequestBehavior.AllowGet);
        }

        


        public JsonResult ActionStepList(string process_name)
        {
            return Json(approvalsBusiness.StepList(process_name), JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult ActionStepAdd(ProcessStep step)
        {
            return Json(approvalsBusiness.StepAdd(step));
        }
        [HttpPost]
        public JsonResult ActionStepEdit(ProcessStep step)
        {
            return Json(approvalsBusiness.StepEdit(step));
        }

        public JsonResult ActionUserList(string process_name, int step_level)
        {
            return Json(approvalsBusiness.UserList(process_name,  step_level), JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult ActionUserAdd(StepUser user)
        {
            return Json(approvalsBusiness.UserAdd(user));
        }
        [HttpPost]
        public JsonResult ActionUserDel(StepUser user)
        {
            return Json(approvalsBusiness.UserDel(user));
        }
    }
}