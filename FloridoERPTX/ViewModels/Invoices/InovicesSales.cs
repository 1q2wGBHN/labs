﻿using App.Entities;
using System.ComponentModel;
using static App.Entities.ViewModels.Common.Constants;

namespace FloridoERPTX.ViewModels.Invoices
{
    public class InvoicesSales
    {
        public InvoicesSales()
        {
            DocumentType = VoucherType.INCOMES;
            IsPMCredit = false;
            CFDICode = SatUseCode.TO_DEFINE;
            Currency = Currencies.MXN;
        }

        [DisplayName("No. de Venta:")]
        public long SaleNumber { get; set; }

        [DisplayName("Cliente:")]
        public string Client { get; set; }        

        [DisplayName("Uso CFDI")]
        public string CFDICode { get; set; }

        [DisplayName("Tipo de Pago")]
        public int PaymentTypeCode { get; set; }

        [DisplayName("Fecha:")]
        public string Date { get; set; }

        [DisplayName("Factura Siguiente:")]
        public string NextInvoice { get; set; }       

        [DisplayName("Datos del cliente (RFC, Nombre, No. Tarjeta): ")]
        public string ClientCode { get; set; }

        [DisplayName("Asignar Tarjeta:")]
        public string NewCardNumber { get; set; }

        //Client Info display it on view.
        #region ClientInfo 
        [DisplayName("Número de Cliente:")]
        public string ClientNumber { get; set; }

        [DisplayName("Cliente:")]
        public string ClientName { get; set; }

        [DisplayName("Dirección:")]
        public string Address { get; set; }

        [DisplayName("Ciudad:")]
        public string City { get; set; }

        [DisplayName("Estado:")]
        public string State { get; set; }

        [DisplayName("Código Postal:")]
        public string ZipCode { get; set; }

        [DisplayName("Teléfono:")]
        public string PhoneNumber { get; set; }

        [DisplayName("Fax:")]
        public string FaxNumber { get; set; }

        [DisplayName("SubTotal:")]
        public decimal Subtotal { get; set; }

        [DisplayName("IVA:")]
        public decimal IVA { get; set; }

        [DisplayName("Total:")]
        public decimal Total { get; set; }
        #endregion

        public bool IsGlobal { get; set; }
        public string Serie { get; set; }
        public long Number { get; set; }
        public string SaleDate { get; set; }       
        public string User { get; set; }
        public string DocumentType { get; set; }
        public bool IsStamped { get; set; }
        public bool IsPMCredit { get; set; } //PM stand for PaymentMethod
        public string PdfEncoded { get; set; }

        [DisplayName("Moneda: ")]
        public string Currency { get; set; }

        public INVOICES INVOICE
        {
            get
            {
                return new INVOICES
                {
                    cfdi_id = Number,
                    invoice_serie = Serie,
                    invoice_no = Number,
                    customer_code = ClientCode,
                    invoice_total = Total,
                    invoice_tax = IVA,
                    sat_us_code = CFDICode,
                    cuser = User                 
                };
            }
        }
    }
}