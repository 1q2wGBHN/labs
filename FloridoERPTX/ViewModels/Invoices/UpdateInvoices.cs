﻿using System.Collections.Generic;
using System.Web.Mvc;

namespace FloridoERPTX.ViewModels.Invoices
{
    public class UpdateInvoices
    {
        public List<SelectListItem> Invoices { get; set; }
        public string MinDate { get; set; }
        public decimal Balance { get; set; }
        public string BalanceDisplay { get; set; }
        public string Currency { get; set; }
    }
}