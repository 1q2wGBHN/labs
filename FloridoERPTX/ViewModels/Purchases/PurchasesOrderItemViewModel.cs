﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FloridoERPTX.ViewModels.Purchases
{
    public class PurchasesOrderItemViewModel
    {
        public string partNumber { get; set; } 
        public decimal quantity { get; set; }// cantidad total 
        public string unitSize { get; set; } //unidades kg
        public decimal purchasePrice { get; set; }// precio del producto
        public int factor { get; set; }//es el empaque
        public decimal itemAmount { get; set; }// importe  sin impuestos
        public decimal resultIva { get; set; }//iva monto por producto
        public decimal resultTax { get; set; } // tax por producto
        public decimal resultTotal { get; set; } //pago total por producto

    }
}