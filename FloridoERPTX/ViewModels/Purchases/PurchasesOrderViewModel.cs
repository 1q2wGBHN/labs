﻿using App.Entities;
using App.Entities.ViewModels.MaCode;
using App.Entities.ViewModels.PurchaseOrder;
using App.Entities.ViewModels.Supplier;
using System;
using System.Collections.Generic;
using System.Web.Mvc;

namespace FloridoERPTX.ViewModels.Purchases
{
    public class PurchasesOrderViewModel
    {
        public int Supplier { get; set; }
        public DateTime orderDate { get; set; }
        public string currency { get; set; }
        public int status { get; set; }
        public string comment { get; set; }
        public virtual List<PurchaseOrderModel> purchasesOrder { get; set; }
        public string partName { get; set; }
        //cantidad solicitada
        public string requestedQuantity { get; set; }
        //cantidad surtidad
        public string assortedQuantity { get; set; }
        //eficiencia en monto
        public string efficiencyInAmount { get; set; }
        //eficiencia en cantidad
        public string efficiencyInQuantity { get; set; }
        //monto solicitado
        public string requestedAmount { get; set; }
        //monto surtido
        public string assortedAmount { get; set; }
        //numero orden
        public string purchase_no { get; set; }
        public virtual List<MaSupplierModel> Suppliers { get; set; }
        public List<MA_CLASS> Departaments { get; set; }
        public List<MaCodeModel> CurrencyList { get; set; }
        public PurchasesOrderViewModel()
        {
            Departaments = new List<MA_CLASS>();
            CurrencyList = new List<MaCodeModel>();
        }

        public IEnumerable<SelectListItem> SuppliersList
        {
            get
            {
                return FillSelectListItems();
            }
        }

        public IEnumerable<SelectListItem> DepartamentsList
        {
            get
            {
                return FillSelectListDepartaments();
            }
        }

        public List<SelectListItem> FillSelectListItems()
        {
            List<SelectListItem> suppliersL = new List<SelectListItem>
            {
                new SelectListItem { Text = "Seleccione una opción", Value = "" }
            };
            foreach (var n in Suppliers)
            {
                suppliersL.Add(new SelectListItem { Text = n.BusinessName, Value = n.SupplierId.ToString() });
            }
            return suppliersL;
        }

        public List<SelectListItem> FillSelectListDepartaments()
        {
            List<SelectListItem> departmentsL = new List<SelectListItem>
            {
                new SelectListItem { Text = "Seleccione un departamento", Value = "" }
            };
            foreach (var n in Departaments)
                departmentsL.Add(new SelectListItem { Text = n.description, Value = n.class_id.ToString() });

            return departmentsL;
        }
    }
}