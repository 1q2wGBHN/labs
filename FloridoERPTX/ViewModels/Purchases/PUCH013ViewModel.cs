﻿using App.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace FloridoERPTX.ViewModels.Purchases
{
    public class PUCH013ViewModel
    {
        public int supplier_id { get; set; }
        public string supplier_rfc { get; set; }
        public string supplier_name { get; set; }

        public virtual List<MA_SUPPLIER> Suplier { get; set; }

        public IEnumerable<SelectListItem> SupplierListItems
        {
            get
            {
                return FillSelectListItemSupplier();
            }
        }
        public List<SelectListItem> FillSelectListItemSupplier()
        {
            List<SelectListItem> items = new List<SelectListItem>();
            items.Add(new SelectListItem { Text = "Seleccione una opción", Value = "" });
            foreach (var n in Suplier)
            {
                items.Add(new SelectListItem { Text = n.business_name, Value = n.supplier_id.ToString() });
            }

            return items;
        }

        public MA_SUPPLIER MA_SUPPLIER
        {
            get
            {
                return new MA_SUPPLIER
                {
                    supplier_id = supplier_id,
                    rfc = supplier_rfc,
                    business_name = supplier_name,
                };
            }
        }
    }
}