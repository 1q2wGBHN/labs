﻿using App.Entities;
using System;
using System.Collections.Generic;
using System.Web.Mvc;

namespace FloridoERPTX.ViewModels.Purchases
{
    public class AddSupplierViewModel
    { 
        public string rfc { get; set; }
        public string commercial_name { get; set; }
        public string business_name { get; set; }
        public string supplier_address { get; set; }
        public string city { get; set; }
        public string zip_code { get; set; }
        public string supplier_state { get; set; }
        public string country { get; set; }
        public string supplier_type { get; set; }
        public string accounting_account { get; set; }
        public string cuser { get; set; }
        public DateTime cdate { get; set; }
        public string program { get; set; }
        public int? truck_foot { get; set; }
        public int? merchandise_entry { get; set; }
        public int? apply_return { get; set; }
        public string part_iva_purchase_supplier { get; set; }

        public App.GlobalEntities.MA_SUPPLIER_G supplier
        {
            get
            {
                return new App.GlobalEntities.MA_SUPPLIER_G
                {
                    rfc = rfc,
                    commercial_name = commercial_name,
                    business_name = business_name,
                    supplier_address = supplier_address,
                    city = city,
                    zip_code = zip_code,
                    supplier_state = supplier_state,
                    country = country,
                    supplier_type = supplier_type,
                    accounting_account = accounting_account,
                    cuser = cuser,
                    cdate = DateTime.Now,
                    part_iva_purchase_supplier = part_iva_purchase_supplier,
                    program = ""
                };
            }
        }
        public virtual List<MA_CODE> TipoProveedor { get; set; }
        public IEnumerable<SelectListItem> TypesList
        {
            get
            {
                return FillSelectListTypes();
            }
        }
        public List<SelectListItem> FillSelectListTypes()
        {
            List<SelectListItem> types = new List<SelectListItem>();
            types.Add(new SelectListItem { Text = "Selecciona un Tipo", Value = "" });
            foreach (var n in TipoProveedor)
            {
                types.Add(new SelectListItem { Text = n.description, Value = n.vkey.ToString() });
            }
            return types;
        }

        public virtual List<MA_TAX> TipoProveedorIVA { get; set; }
        public IEnumerable<SelectListItem> TypesListIVA
        {
            get
            {
                return FillSelectListTypesIVA();
            }
        }
        public List<SelectListItem> FillSelectListTypesIVA()
        {
            List<SelectListItem> types = new List<SelectListItem>();
            types.Add(new SelectListItem { Text = "Selecciona un tipo de IVA", Value = "" });
            foreach (var n in TipoProveedorIVA)
            {
                types.Add(new SelectListItem { Text = n.name, Value = n.tax_code.ToString() });
            }
            return types;
        }

    }
}