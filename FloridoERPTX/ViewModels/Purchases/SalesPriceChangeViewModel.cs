﻿using App.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace FloridoERPTX.ViewModels.Purchases
{
    public class SalesPriceChangeViewModel
    {
        public int sales_price_id { get; set; }
        public string part_number { get; set; }
        public string part_description { get; set; }
        public string applied_date { get; set; }
        public decimal sale_price { get; set; }
        public decimal existencias { get; set; }

        //public SalesPriceChangeViewModel(string part_number, string part_description, decimal sale_price)
        //{
        //    this.part_number = part_number;
        //    this.part_description = part_description;
        //    this.sale_price = sale_price;
        //}
        //public SalesPriceChangeViewModel()
        //{
        //}

        //public string PART_NUMBER
        //{
        //    get { return part_number; }
        //    set { part_number = value; }
        //}

        //public string PART_DESCRIPTION
        //{
        //    get { return part_description; }
        //    set { part_description = value; }
        //}
        //public decimal SALE_PRICE
        //{
        //    get { return sale_price; }
        //    set { sale_price = value; }
        //}

        public virtual List<MA_CLASS> Departamentos { get; set; }
        public IEnumerable<SelectListItem> DeptosList
        {
            get
            {
                return FillSelectListDeptos();
            }
        }
        public List<SelectListItem> FillSelectListDeptos()
        {
            List<SelectListItem> deptos = new List<SelectListItem>();
            deptos.Add(new SelectListItem { Text = "Selecciona un Departamento", Value = "" });
            foreach (var n in Departamentos)
            {
                deptos.Add(new SelectListItem { Text = n.description, Value =n.class_id.ToString() });
            }

            return deptos;
        }
    }
}