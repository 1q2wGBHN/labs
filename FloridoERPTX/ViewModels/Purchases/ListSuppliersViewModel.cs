﻿using App.BLL.Supplier;
using App.DAL.Supplier;
using App.Entities;
using App.GlobalEntities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace FloridoERPTX.ViewModels.Purchases
{
    public class ListSuppliersViewModel
    {
        private SupplierBusiness _business;
        public int supplier_id { get; set; }
        public string rfc { get; set; }
        public string commercial_name { get; set; }
        public string business_name { get; set; }
        public string supplier_address { get; set; }
        public string city { get; set; }
        public string zip_code { get; set; }
        public string supplier_state { get; set; }
        public string country { get; set; }
        public string supplier_type { get; set; }
        public string accounting_account { get; set; }
        public string part_iva_purchase_supplier { get; set; }
        public string cuser { get; set; }
        public Nullable<System.DateTime> cdate { get; set; }
        public string uuser { get; set; }
        public Nullable<System.DateTime> udate { get; set; }
        public string program_id { get; set; }
        public int truck_foot { get; set; }
        public int merchandise_entry { get; set; }
        public int apply_return { get; set; }
        public int purchase_applied_return { get; set; }
        public List<SupplierModel> listAllSuppliers { get { return _business.GetAllSuppliersAndCreditorInModel(); } }
        public virtual List<MA_CODE> TipoProveedor { get; set; }
        public virtual List<App.Entities.MA_TAX> TipoProveedorIVA { get; set; }
        public IEnumerable<SelectListItem> TypesList
        {
            get
            {
                return FillSelectListTypes();
            }
        }
        public List<SelectListItem> FillSelectListTypes()
        {
            List<SelectListItem> types = new List<SelectListItem>();
            types.Add(new SelectListItem { Text = "Selecciona un Tipo", Value = "" });
            foreach (var n in TipoProveedor)
            {
                types.Add(new SelectListItem { Text = n.description, Value = n.vkey.ToString() });
            }
            return types;
        }

        public IEnumerable<SelectListItem> TypesListIVA
        {
            get
            {
                return FillSelectListTypesIVA();
            }
        }
        public List<SelectListItem> FillSelectListTypesIVA()
        {
            List<SelectListItem> types = new List<SelectListItem>();
            types.Add(new SelectListItem { Text = "Selecciona un Tipo", Value = "" });
            foreach (var n in TipoProveedorIVA)
            {
                types.Add(new SelectListItem { Text = n.name, Value = n.tax_code.ToString() });
            }
            return types;
        }

        public ListSuppliersViewModel()
        {
            _business = new SupplierBusiness();
        }

        public App.GlobalEntities.MA_SUPPLIER_G SUPLIERS
        {
            get
            {
                return new App.GlobalEntities.MA_SUPPLIER_G
                {
                    supplier_id = supplier_id,
                    rfc = rfc,
                    commercial_name = commercial_name,
                    business_name = business_name,
                    supplier_address = supplier_address,
                    city = city,
                    zip_code = zip_code,
                    supplier_state = supplier_state,
                    country = country,
                    supplier_type = supplier_type,
                    accounting_account = accounting_account,
                    cuser = cuser,
                    cdate = cdate.Value,
                    udate = udate,
                    uuser = uuser,
                    part_iva_purchase_supplier = part_iva_purchase_supplier,
                    apply_return = apply_return,
                    merchandise_entry = merchandise_entry,
                    truck_foot = truck_foot,
                    purchase_applied_return = purchase_applied_return,
                    program = "PURCH003.cshtml"
                };
            }
        }
    }
}