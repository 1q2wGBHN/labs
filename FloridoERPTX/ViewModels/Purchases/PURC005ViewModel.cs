﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using App.BLL.Site;
using App.Entities.ViewModels.Site;
using App.Entities.ViewModels.InternalRequirement;


namespace FloridoERPTX.ViewModels.Purchases
{
    public class PURC005ViewModel
    {
        public virtual List<InternalRequirementModel> InternalRequierement { get; set; }
        public virtual List<InternalRequirementPOModel> InternalRequirementHeader { get; set; }

    }
}