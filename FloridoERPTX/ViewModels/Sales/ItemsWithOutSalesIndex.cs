﻿
using App.Entities.ViewModels.Sales;
using System.Collections.Generic;
using System.ComponentModel;
using System.Web.Mvc;
namespace FloridoERPTX.ViewModels
{
    public class ItemsWithOutSalesIndex
    {
        [DisplayName("Fecha Inicial: ")]
        public string Date { get; set; }
        [DisplayName("Fecha Final: ")]
        public string Date2 { get; set; }      
        public string SelectedDepartments { get; set; }
        public IEnumerable<SelectListItem> Departments { get; set; }
        public List<ItemsWithOutSalesModel> ItemsList { get; set; }
        public List<DepartementsData> DepartmentList { get; set; }
        public bool IsFromMenu { get; set; }
    }

    public class DepartementsData
    {
        public string name { get; set; }
        public string idDep { get; set; }
        public bool SelectedDep { get; set; }
    }

  
}
