﻿using App.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace FloridoERPTX.ViewModels.Sales
{
    public class SalesPriceChangeViewModel
    {
        //public SalesPriceChangeViewModel(string part_number, string part_description, decimal sale_price)
        //{
        //    this.part_number = part_number;
        //    this.part_description = part_description;
        //    this.sale_price = sale_price;
        //}
        //public SalesPriceChangeViewModel()
        //{
        //}

        //public string PART_NUMBER
        //{
        //    get { return part_number; }
        //    set { part_number = value; }
        //}

        //public string PART_DESCRIPTION
        //{
        //    get { return part_description; }
        //    set { part_description = value; }
        //}
        //public decimal SALE_PRICE
        //{
        //    get { return sale_price; }
        //    set { sale_price = value; }
        //}

        public virtual List<USER_MASTER> Cajeros { get; set; }
        public IEnumerable<SelectListItem> CajerosList
        {
            get
            {
                return FillSelectListCajeros();
            }
        }
        public List<SelectListItem> FillSelectListCajeros()
        {
            List<SelectListItem> cajeros = new List<SelectListItem>();
            cajeros.Add(new SelectListItem { Text = "Selecciona un Cajero", Value = "" });
            foreach (var n in Cajeros)
            {
                cajeros.Add(new SelectListItem { Text = n.first_name+" "+n.last_name, Value =n.emp_no });
            }

            return cajeros;
        }
    }
}