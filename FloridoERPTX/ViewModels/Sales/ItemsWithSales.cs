﻿
using App.Entities.ViewModels.Sales;
using System.Collections.Generic;
using System.ComponentModel;
using System.Web.Mvc;
namespace FloridoERPTX.ViewModels
{
    public class ItemsWithSales
    {
        [DisplayName("Fecha Inicial: ")]
        public string Date { get; set; }
        [DisplayName("Fecha Final: ")]
        public string Date2 { get; set; }  
        public List<ItemsWithSalesModel> ItemsList { get; set; }
        public bool IsFromMenu { get; set; }
    }

   
  
}
