﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FloridoERPTX.ViewModels.Sales
{
    public class CustomerSalesViewModel
    {
        public string Hora { get; set; }
        public decimal Subtotal { get; set; }
        public decimal Ieps { get; set; }
        public decimal Iva { get; set; }
        public decimal Ventas { get; set; }
        public int Clientes { get; set; }
        public decimal PromedioPorCliente { get; set; }
    }
}