﻿
namespace FloridoERPTX.ViewModels.Home
{
    public class RightSidebarViewModel
    {
        public string BanxicoCurrency { get; set; }
        public string ComercialCurrency { get; set; }
    }
}