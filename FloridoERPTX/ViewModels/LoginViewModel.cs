﻿using App.Common;

namespace FloridoERPTX.ViewModels
{
    public class LoginViewModel
    {
       
        public string Username { get; set; }
        public string Password { get; set; }
        public string FinalPassword { get { return Common.SetPassword(Password); } }

    }
}