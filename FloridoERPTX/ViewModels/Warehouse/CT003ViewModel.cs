﻿using App.Entities.ViewModels.Transfer;
using App.Entities.ViewModels.UserSpecs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace FloridoERPTX.ViewModels.Warehouse
{
    public class CT003ViewModel
    {
        public List<TransferHeaderModel> ListTransferStatus4 { get; set; }
        public List<TransferHeaderModel> ListTransferStatus9 { get; set; }
        public List<UserSpecsModel> ListDrivers { get; set; }

        public IEnumerable<SelectListItem> selectDriver
        {
            get { return FillSelectTypeListItem(); }
        }
        public List<SelectListItem> FillSelectTypeListItem()
        {
            List<SelectListItem> items = new List<SelectListItem>();
            items.Add(new SelectListItem { Text = "Selecciona un Chofer", Value = "" });
            foreach (var n in ListDrivers)
            {
                items.Add(new SelectListItem { Text = n.FullName, Value = n.EmpNo });
            }
            return items;
        }
    }
}