﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using App.Entities.ViewModels.Inventory;
using App.Entities;

namespace FloridoERPTX.ViewModels.Warehouse
{
    public class INVE016Model
    {
        public List<InventoryType> InventoryType { get; set; }
        public List<MA_CODE> InventoryClass { get; set; }
        public List<InventoryLocationModel> InventoryLocations { get; set; }
    }
}