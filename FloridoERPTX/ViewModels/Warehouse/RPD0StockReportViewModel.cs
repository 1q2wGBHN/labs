﻿using App.Entities;
using App.Entities.ViewModels.StorageLocation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace FloridoERPTX.ViewModels.Warehouse
{
    public class RPD0StockReportViewModel
    {
        public int supplier_id { get; set; }
        public string supplier_rfc { get; set; }
        public string supplier_name { get; set; }

        public List<string> UserRoles { get; set; }

        public virtual List<MA_SUPPLIER> Suplier { get; set; }

        public IEnumerable<SelectListItem> SupplierListItems
        {
            get
            {
                return FillSelectListItemSupplier();
            }
        }
        public List<SelectListItem> FillSelectListItemSupplier()
        {
            List<SelectListItem> items = new List<SelectListItem>();
            items.Add(new SelectListItem { Text = "Seleccione una opción", Value = "0" });
            foreach (var n in Suplier)
            {
                items.Add(new SelectListItem { Text = n.business_name, Value = n.supplier_id.ToString() });
            }

            return items;
        }

        public MA_SUPPLIER MA_SUPPLIER
        {
            get
            {
                return new MA_SUPPLIER
                {
                    supplier_id = supplier_id,
                    rfc = supplier_rfc,
                    business_name = supplier_name,
                };
            }
        }

        public virtual List<MA_CLASS> Departamentos { get; set; }
        public IEnumerable<SelectListItem> DeptosList
        {
            get
            {
                return FillSelectListDeptos();
            }
        }
        public List<SelectListItem> FillSelectListDeptos()
        {
            List<SelectListItem> deptos = new List<SelectListItem>();
            deptos.Add(new SelectListItem { Text = "Selecciona un Departamento", Value = "0" });
            foreach (var n in Departamentos)
            {
                deptos.Add(new SelectListItem { Text = n.description, Value = n.class_id.ToString() });
            }

            return deptos;
        }

        public virtual List<StorageLocationModel> LocationRaw { get; set; }
        public IEnumerable<SelectListItem> LocationRawList
        {
            get
            {
                return FillSelectListLocation();
            }
        }
        public List<SelectListItem> FillSelectListLocation()
        {
            List<SelectListItem> location = new List<SelectListItem>();
            location.Add(new SelectListItem { Text = "Selecciona una Locación", Value = "" });
            foreach (var n in LocationRaw)
            {
                location.Add(new SelectListItem { Text = n.storage_location, Value = n.storage_location });
            }

            return location;
        }        
    }
}