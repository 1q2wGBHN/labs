﻿using App.Entities;
using App.Entities.ViewModels.Supplier;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace FloridoERPTX.ViewModels.Warehouse
{
    public class ViewModelPurch014
    {
        public ViewModelPurch014()
        {
            MaCode = new List<MA_CODE>();
            Suplier = new List<MaSupplierModel>();
        }

        public int supplier_id { get; set; }
        public string rfc { get; set; }
        public string business_name { get; set; }
        public List<MA_CODE> MaCode { get; set; }
        public virtual List<MA_CODE> Code { get; set; }
        public virtual List<MaSupplierModel> Suplier { get; set; }
        public IEnumerable<SelectListItem> SupplierListItems
        {
            get
            {
                return FillSelectListItemSupplier();
            }
        }
        public List<SelectListItem> FillSelectListItemSupplier()
        {
            List<SelectListItem> items = new List<SelectListItem>();
            items.Add(new SelectListItem { Text = "Seleccione una opción", Value = "" });
            foreach (var n in Suplier)
            {
                items.Add(new SelectListItem { Text = n.BusinessName, Value = n.SupplierId.ToString() });
            }

            return items;
        }

        public MaSupplierModel MA_SUPPLIER
        {
            get
            {
                return new MaSupplierModel
                {
                    SupplierId = supplier_id,
                    Rfc = rfc,
                    BusinessName = business_name,
                };
            }
        }



        public IEnumerable<SelectListItem> Codes
        {
            get
            {
                return FillSelectListCodes();
            }
        }
        public List<SelectListItem> FillSelectListCodes()
        {
            List<SelectListItem> codesL = new List<SelectListItem>();
            codesL.Add(new SelectListItem { Text = "Seleccione un Departametno", Value = "" });
            foreach (var n in Code)
            {
                codesL.Add(new SelectListItem { Text = n.description, Value = n.vkey });
            }
            return codesL;
        }

    }


}