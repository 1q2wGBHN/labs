﻿using App.Entities.ViewModels.Item;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace FloridoERPTX.ViewModels.Warehouse
{
    public class RPD001Model
    {
        public string part_number { get; set; }
        public string part_description { get; set; }

        public virtual List<ItemModelDropDown> Items { get; set; }
        public IEnumerable<SelectListItem> ItemsList
        {
            get
            {
                return FillSelectListItems();
            }
        }
        public List<SelectListItem> FillSelectListItems()
        {
            List<SelectListItem> products = new List<SelectListItem>();
            products.Add(new SelectListItem { Text = "Seleccione una opción", Value = "" });
            foreach (var n in Items)
            {
                products.Add(new SelectListItem { Text = n.Description + " / "+ n.PartNumber, Value = n.PartNumber });
            }
            return products;
        }
        public ItemModelDropDown ITEM
        {
            get
            {
                return new ItemModelDropDown
                {
                    PartNumber = part_number,
                    Description = part_description
                };
            }
        }
    }
}