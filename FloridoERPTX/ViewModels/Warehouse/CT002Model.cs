﻿using App.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using App.Entities.ViewModels.Item;
using App.Entities.ViewModels.Site;
using App.BLL.Site;
using System.Globalization;

namespace FloridoERPTX.ViewModels.Warehouse
{ 
    public class CT002Model
    {

        private SiteConfigBusiness _siteConfigBusiness = new SiteConfigBusiness();
        //public string part_number { get; set; }
        //public string part_description { get; set; }

        //public virtual List<ItemModel> Items { get; set; }
        //public IEnumerable<SelectListItem> ItemsList
        //{
        //    get
        //    {
        //        return FillSelectListItems();
        //    }
        //}
        //public List<SelectListItem> FillSelectListItems()
        //{
        //    List<SelectListItem> products = new List<SelectListItem>();
        //    products.Add(new SelectListItem { Text = "Seleccione una opción", Value = "" });
        //    foreach (var n in Items)
        //    {
        //        products.Add(new SelectListItem { Text = n.Description, Value = n.PathNumber });
        //    }
        //    return products;
        //}

        public virtual List<SiteModel> Site { get; set; }
        public IEnumerable<SelectListItem> SiteList
        {
            get
            {
                return FillSelectListSite();
            }
        }
        public List<SelectListItem> FillSelectListSite()
        {

            string site_code_transfer = _siteConfigBusiness.GetSiteCode();
            List<SelectListItem> sites = new List<SelectListItem>();
            sites.Add(new SelectListItem { Text = "Seleccione una opción", Value = "" });
            foreach (var n in Site)
            {
                if (site_code_transfer != n.site_code)
                {
                    sites.Add(new SelectListItem { Text = n.site_name, Value = n.site_code });
                }
            }
            return sites;
        }

        //public ItemModel ITEM
        //{
        //    get
        //    {
        //        return new ItemModel
        //        {
        //            PathNumber = part_number,
        //            Description = part_description
        //        };
        //    }
        //}
    }
}