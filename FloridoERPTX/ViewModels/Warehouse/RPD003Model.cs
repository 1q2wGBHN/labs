﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using App.Entities.ViewModels.SalesPriceChange;
using App.Entities.ViewModels.Supplier;
using App.Entities;
using System.Web.Mvc;
using App.Entities.ViewModels.Item;

namespace FloridoERPTX.ViewModels.Warehouse
{
    public class RPD003Model
    {
        public virtual List<MA_CLASS> Departamentos { get; set; }
        public IEnumerable<SelectListItem> DeptosList
        {
            get
            {
                return FillSelectListDeptos();
            }
        }
        public List<SelectListItem> FillSelectListDeptos()
        {
            List<SelectListItem> deptos = new List<SelectListItem>();
            deptos.Add(new SelectListItem { Text = "Seleccione un departamento", Value = "" });
            foreach (var n in Departamentos)
            {
                deptos.Add(new SelectListItem { Text = n.description, Value = n.class_id.ToString() });
            }

            return deptos;
        }

        public virtual List<MaSupplierModel> Supplier { get; set; }
        public List<ItemInventoryDaysModel> InventoryItems { get; set; }
    }
}