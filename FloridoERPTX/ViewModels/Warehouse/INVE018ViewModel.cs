﻿using App.Common;
using App.Entities.ViewModels.Inventory;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace FloridoERPTX.ViewModels.Warehouse
{
    public class INVE018ViewModel
    {
        public int inventory_id { get; set; }


        public List<InventoryHeaderModel> ListInventory { get; set; }

        public IEnumerable<SelectListItem> InventorySelectList
        {
            get
            {
                List<SelectListItem> selectListItems = new List<SelectListItem>();
                selectListItems.Add(new SelectListItem { Text = "Seleccione una opción", Value = "" });

                if (ListInventory != null)
                {
                    foreach (var item in ListInventory)
                    {
                        selectListItems.Add(new SelectListItem { Text = item.inventory_name, Value = item.id_inventory_header});
                    }
                }

                return selectListItems;
            }
        }
    }
}