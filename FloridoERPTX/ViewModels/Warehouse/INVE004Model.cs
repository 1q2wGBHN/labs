﻿using App.BLL.Inventory;
using App.Entities.ViewModels.Inventory;
using App.Entities.ViewModels.Item;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace FloridoERPTX.ViewModels.Warehouse
{
    public class INVE004Model
    {
        public INVE004Model()
        {
            var InventoryActive = new InventoryReportBusiness();
            InventoryList = InventoryActive.GetInventoryActive();
            var InventoryProducts = new InventoryAdjustmentsBusiness();
            InventoryAdjustment = InventoryActive.GetInventoryAdjustementAll();
            if (InventoryList.Count >0)
                Items = InventoryProducts.GetInventoryProductsDetailDrop(InventoryList[0].id_inventory_header);
            else
                Items = InventoryProducts.GetInventoryProductsDetailDrop("");
        }
        public virtual List<DropItemModel> Items { get; set; }
        public virtual List<InventoryHeaderModel> InventoryList { get; set; }
        public List<InventoryAdjustmentModel> InventoryAdjustment { get; set; }
        public IEnumerable<SelectListItem> ItemsList
        {
            get
            {
                return FillSelectListItems();
            }
        }
        public List<SelectListItem> FillSelectListItems()
        {
            List<SelectListItem> products = new List<SelectListItem>();
            products.Add(new SelectListItem { Text = "Seleccione una opción", Value = "" });
            foreach (var n in Items)
            {
                products.Add(new SelectListItem { Text = n.Description, Value = n.PartNumber });
            }
            return products;
        }
    }
}