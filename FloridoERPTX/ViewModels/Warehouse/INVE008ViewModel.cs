﻿using System;
using System.Collections.Generic;
using System.Linq;
using App.Entities.ViewModels.Inventory;
using System.Web;
using App.Entities.ViewModels.Supplier;
using App.Entities;

namespace FloridoERPTX.ViewModels.Warehouse
{
    public class INVE008ViewModel
    {
        public List<MaSupplierModel> ListSuppliers { get; set; }
        public List<MA_CLASS> ListMaClass { get; set; }
    }
}