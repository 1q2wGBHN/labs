﻿using App.Entities.ViewModels.Item;
using App.Entities.ViewModels.Inventory;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FloridoERPTX.ViewModels.Warehouse
{
    public class INVE013ViewModelcs
    {
        public List<NegativeInventoryBalancesTemp> Negatives { get; set; }
        public List<InventoryNegativeHeaderModel> NegativesBalances { get; set; }
        public bool UserRole { get; set; }
    }
}