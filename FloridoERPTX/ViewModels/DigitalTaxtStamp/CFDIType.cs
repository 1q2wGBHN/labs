﻿using System.ComponentModel.DataAnnotations;

namespace FloridoERPTX.ViewModels.DigitalTaxStamp
{
    public enum CFDIType
    {
        [Display(Name = "Factura")]
        Invoice,

        [Display(Name="Nota de Crédito")]
        CreditNote,

        [Display(Name = "Bonificación")]
        Bonification,

        [Display(Name = "Complemento de Pago")]
        ComplementPay
    }
}