﻿using System.ComponentModel;

namespace FloridoERPTX.ViewModels.DigitalTaxStamp
{
    public class SendCFDI
    {
        public SendCFDI()
        {
            SaveEmail = true;
        }

        [DisplayName("Tipo: ")]
        public CFDIType CFDITypeId { get; set; }

        [DisplayName("Serie: ")]
        public string Serie { get; set; }

        [DisplayName("Número: ")]
        public string Number { get; set; }

        [DisplayName("Correo Electrónico:")]
        public string Email { get; set; }

        [DisplayName("Guardar Correo")]
        public bool SaveEmail { get; set; }
    }
}