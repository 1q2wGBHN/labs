﻿using App.DAL.FixedAsset;
using App.GlobalEntities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace FloridoERPTX.ViewModels.FixedAsset
{
    public class FixedAssetRequisitionViewModel
    {
        public List<FIXED_ASSET> ListFixedAsset { get; set; }
        public List<FixedAssetRequisitionModel> ListRequisition { get; set; }
        public string site_code { get; set; }
        public string site_name { get; set; }
        public int fixed_asset_code { get; set; }
        public DateTime? requisition_date { get; set; }

        public IEnumerable<SelectListItem> ListSelectFixedAsset
        {
            get
            {
                return FillSelectListFixedAsset();
            }
        }
        public List<SelectListItem> FillSelectListFixedAsset()
        {
            List<SelectListItem> items = new List<SelectListItem>();
            items.Add(new SelectListItem { Text = "Seleccione un Activo Fijo", Value = "" });
            foreach (var item in ListFixedAsset)
            {
                items.Add(new SelectListItem { Text = item.fixed_asset_name.Trim() , Value = item.fixed_asset_code.ToString() });
            }

            return items;
        }


    }
}