﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FloridoERPTX.ViewModels.Order
{
    public class OrderListDatesViewModel
    {
        public int Id { get; set; }
        public int Supplier { get; set; }
        public int Authorized { get; set; }
        public string ParthNumber { get; set; }
    }
}