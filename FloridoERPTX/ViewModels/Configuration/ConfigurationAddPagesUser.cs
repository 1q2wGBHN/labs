﻿using App.Entities;
using System.Collections.Generic;

namespace FloridoERPTX.ViewModels.Configuration
{
    public class ConfigurationAddPagesUser
    {
        public string EmployeeNumber { get; set; }
        public virtual List<USER_MASTER> Users { get; set; }
    }
}