﻿using App.Entities.ViewModels.SalesPriceChange;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FloridoERPTX.ViewModels.Configuration
{
    public class ConfigurationWeighingMachine
    {
        public virtual List<SalesPriceChangeBasculeModel> Products { get; set; }
    }
}