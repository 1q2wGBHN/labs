﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FloridoERPTX.ViewModels.TestModel
{
    public class TestModel
    {
        public string Name { get; set; }
        public Timbrado.Response Response { get; set; }
    }
}