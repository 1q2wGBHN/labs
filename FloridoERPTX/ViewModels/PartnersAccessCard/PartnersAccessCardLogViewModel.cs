﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FloridoERPTX.ViewModels.PartnersAccessCard
{
    public class PartnersAccessCardLogViewModel
    {
        public int Id { get; set; }
        public string prev_card_number { get; set; }
        public string new_card_number { get; set; }
        public System.DateTime time_stamp { get; set; }
        public string type { get; set; }
        public string prev_emp_id { get; set; }
        public string new_emp_id { get; set; }
        public int modify_id { get; set; }

        //public virtual PartnersAccessCardViewModel PartnersCardAccess { get; set; }
    }
}