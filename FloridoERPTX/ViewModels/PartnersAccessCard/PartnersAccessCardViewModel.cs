﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using App.Entities;

namespace FloridoERPTX.ViewModels.PartnersAccessCard
{
    public class PartnersAccessCardViewModel
    {
        [DisplayName("No. empleado:")]
        public string emp_id { get; set; }
        [DisplayName("No. de tarjeta:")]
        [MinLength(6,ErrorMessage = "Mínimo 6 dígitos")]
        [RegularExpression("([0-9]+)")]
        public string card_number { get; set; }
        public int id { get; set; }
        public USER_MASTER USER_MASTER { get; set; }
        //public virtual ICollection<PartnersAccessCardLogViewModel> PartnersCardAccessLogs { get; set; }
    }

   
}
