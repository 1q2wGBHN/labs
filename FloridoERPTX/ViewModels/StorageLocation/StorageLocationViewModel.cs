﻿using App.Entities;
using App.Entities.ViewModels.MaCode;
using App.Entities.ViewModels.StorageLocation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace FloridoERPTX.ViewModels.StorageLocation
{
    public class StorageLocationViewModel
    {
        public string site_code { get; set; }
        public string storage_location { get; set; }
        public string bin1 { get; set; }
        public string bin2 { get; set; }
        public string bin3 { get; set; }
        public string storage_type { get; set; }
        public bool mrp_ind { get; set; }
        public bool active_flag { get; set; }
        public Nullable<System.DateTime> cdate { get; set; }
        public string cuser { get; set; }
        public Nullable<System.DateTime> udate { get; set; }
        public string uuser { get; set; }
        public string program_id { get; set; }

        public virtual List<StorageLocationModel> Locations { get; set; }

        public STORAGE_LOCATION LOCATION
        {
            get
            {
                return new STORAGE_LOCATION
                {
                    site_code = site_code,
                    storage_location_name = storage_location,
                    storage_type = storage_type,
                    active_flag = active_flag,
                    cuser = cuser,
                    cdate = cdate,
                    uuser = uuser,
                    udate = udate,
                    program_id = ""
                };
            }
        }

        public virtual List<MaCodeModel> TypeLocations { get; set; }
        public IEnumerable<SelectListItem> TypeLocationsList
        {
            get
            {
                return FillSelectListTypeLocations();
            }
        }
        public List<SelectListItem> FillSelectListTypeLocations()
        {
            List<SelectListItem> types = new List<SelectListItem>();
            types.Add(new SelectListItem { Text = "Seleccione tipo de locación", Value = "" });
            foreach (var n in TypeLocations)
            {
                types.Add(new SelectListItem { Text = n.VKey, Value = n.VKey });
            }
            return types;
        }
    }
}