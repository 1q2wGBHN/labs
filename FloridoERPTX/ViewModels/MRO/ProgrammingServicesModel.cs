﻿using App.Entities;
using App.Entities.ViewModels.ServicesCalendar;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FloridoERPTX.ViewModels.MRO
{
    public class ProgrammingServicesModel
    {
        public int service_id { get; set; }
        public DateTime service_date { get; set; }
        public int service_status { get; set; }
        public string site_code { get; set; }
        public string service_department { get; set; }
        public string cuser { get; set; }
        public DateTime cdate { get; set; }
        public string uuser { get; set; }
        public DateTime udate { get; set; }
        public string program_id { get; set; }
        //detail
        public string equipment_id { get; set; }
        public string supplier_id { get; set; }
        public string service_cost { get; set; }
        public string quotation_document { get; set; }

        public virtual List<ServicesCalendarModel> ServicesCalendar { get; set; }
    }
}