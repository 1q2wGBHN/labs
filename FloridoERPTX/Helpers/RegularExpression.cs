﻿using System.ComponentModel.DataAnnotations;

namespace FloridoERPTX.Helpers
{
    public class RegularExpression 
    {
        public class ValidationLetters : RegularExpressionAttribute
        {
            public ValidationLetters() : base("^[a-zA-Z-ZáÁéÉíÍóÓúÚñÑüÜ ]+$")
            {
                ErrorMessage = "Debe contener solo letras";
            }
        }

        public class ValidationNumbers : RegularExpressionAttribute
        {
            public ValidationNumbers() : base("^[0-9]+$")
            {
                ErrorMessage = "Debe contener solo números";
            }
        }

        public class ValidationRFC : RegularExpressionAttribute
        {
            public ValidationRFC() : base(@"^([A-ZÑ&]{3,4}) ?(?:- ?)?(\d{2}(?:0[1-9]|1[0-2])(?:0[1-9]|[12]\d|3[01])) ?(?:- ?)?([A-Z\d]{2})([A\d])$")
            {
                ErrorMessage = "Formato RFC no valido";
                new MinLengthAttribute(12);
                new MaxLengthAttribute(13);
            }
        }
    }
}