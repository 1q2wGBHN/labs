﻿using App.BLL.Customer;
using App.Entities.ViewModels.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using App.Entities.ViewModels.Customers;

namespace FloridoERPTX.CommonMethods
{
    public class CustomerLookup
    {
        private CustomerBusiness _CustomerBusiness;

        public CustomerLookup()
        {
            _CustomerBusiness = new CustomerBusiness();
        }

        public List<SelectListItem> FillLookupCustomer(CustomersLookup Option)
        {
            var CustomersList = new List<SelectListItem>();
            var Customers = _CustomerBusiness.GetCustomers(Option);          

            if (Customers != null && Customers.Count > 0)
            {
                foreach (var Item in Customers)
                {
                    var Element = new SelectListItem();
                    Element.Text = string.Format("{0}, {1}, {2}", Item.RFC, Item.Name, Item.Code);
                    Element.Value = Item.Code;
                    CustomersList.Add(Element);
                }
            }

            return CustomersList;
        }
    }
}