﻿using App.BLL.Site;
using FloridoERPTX.Timbrado;
using System;

namespace FloridoERPTX.CommonMethods
{
    public class StampDocument
    {
        private SiteConfigBusiness _SiteConfigBusiness;
        private TimbradoSoapClient _TimbradoSoapClient;
        private Request _Request;
        private Response _Response;

        public StampDocument()
        {
            _SiteConfigBusiness = new SiteConfigBusiness();
            _TimbradoSoapClient = new TimbradoSoapClient();
            _Request = new Request();
            _Response = new Response();
        }

        public Response DocumentStamp(object Model, bool IsCancel)
        {
            try
            {
                PopulateRequest(Model, IsCancel);
                _Response = _TimbradoSoapClient.DocumentStatus(_Request);
            }
            catch (Exception ex)
            {
                _Response.Status = false;
                _Response.Message = ex.Message;
            }

            return _Response;
        }

        private void PopulateRequest(object Model, bool IsCancel)
        {
            _Request.SiteCode = _SiteConfigBusiness.GetSiteCode();
            _Request.Number = long.Parse(Model.GetType().GetProperty("Number").GetValue(Model, null).ToString());
            _Request.Serie = Model.GetType().GetProperty("Serie").GetValue(Model, null).ToString();
            _Request.Type = Model.GetType().GetProperty("DocumentType").GetValue(Model, null).ToString();

            if (IsCancel)
                _Request.Uuid = Model.GetType().GetProperty("Uuid").GetValue(Model, null).ToString();
        }
    }
}