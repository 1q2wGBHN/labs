﻿using App.BLL;
using App.Entities.ViewModels.Common;
using App.Entities.ViewModels.User;
using System.Collections.Generic;
using System.Web.Mvc;

namespace FloridoERPTX.CommonMethods
{
    public class CashierLookup
    {
        private UserMasterBusiness _UserMasterBusiness;
        public CashierLookup()
        {
            _UserMasterBusiness = new UserMasterBusiness();
        }

        public List<SelectListItem> FillLookupCashiers(CashiersLookup Option)
        {
            var CashierList = new List<SelectListItem>();
            var Cashiers = new List<UserModelFirst>();
            switch (Option)
            {
                case CashiersLookup.AllCashier:
                    Cashiers = _UserMasterBusiness.GetCashiersUsers();
                    break;

                case CashiersLookup.BySale:
                    Cashiers = _UserMasterBusiness.GetCashierBySales();
                    break;

                case CashiersLookup.ByChargeRelation:
                    Cashiers = _UserMasterBusiness.GetCashiersChargeRelations();
                    break;

                case CashiersLookup.ByCancalletaionsSales:
                    Cashiers = _UserMasterBusiness.GetCashiersCancellationsSales();
                    break;

                case CashiersLookup.ByDonations:
                    Cashiers = _UserMasterBusiness.GetCashiersDonations();
                    break;

                case CashiersLookup.ByDonationsSales:
                    Cashiers = _UserMasterBusiness.GetCashiersDonationsSales();
                    break;

                default:
                    Cashiers = _UserMasterBusiness.GetCashiersUsers();
                    break;
            }

            foreach (var item in Cashiers)
            {
                var element = new SelectListItem();
                element.Text = string.Format("{0} {1}", item.FirstName, item.LastName);
                element.Value = item.EmpNo;
                CashierList.Add(element);
            }

            return CashierList;
        }
    }
}