﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.Optimization;

namespace FloridoERPTX
{
    public class BundleConfig
    {
        // For more information on bundling, visit http://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            //BundleTable.EnableOptimizations = true;

            // Homer style
            bundles.Add(new StyleBundle("~/bundles/homer/css").Include("~/Content/style.css", new CssRewriteUrlTransform())
                .Include("~/styles/simple_check.css"));

            // Animate.css
            var bundle = new Bundle("~/bundles/animate/css")
            {
                Orderer = new AsIsBundleOrderer()
            };
            bundle.Include("~/Vendor/animate.css/animate.min.css", new CssRewriteUrlTransform())
                .Include("~/vendor/toastr/build/toastr.min.css", new CssRewriteUrlTransform())
                .Include("~/fonts/pe-icon-7-stroke/css/helper.css", new CssRewriteUrlTransform())
                .Include("~/styles/static_custom.css", new CssRewriteUrlTransform())
                .Include("~/Vendor/sweetalert/lib/sweet-alert.css", new CssRewriteUrlTransform());
            bundles.Add(bundle);

            // Pe-icon-7-stroke
            bundles.Add(new StyleBundle("~/bundles/peicon7stroke/css").Include("~/fonts/pe-icon-7-stroke/css/pe-icon-7-stroke.css", new CssRewriteUrlTransform()));

            // Font Awesome icons style
            bundles.Add(new StyleBundle("~/bundles/font-awesome/css").Include("~/Vendor/fontawesome/css/font-awesome.min.css", new CssRewriteUrlTransform()));

            // Bootstrap style
            bundles.Add(new StyleBundle("~/bundles/bootstrap/css").Include("~/Vendor/bootstrap/dist/css/bootstrap.min.css", new CssRewriteUrlTransform()));

            // Datatables style
            bundles.Add(new StyleBundle("~/bundles/datatables/css").Include("~/Vendor/datatables.net-bs/css/dataTables.bootstrap.min.css", new CssRewriteUrlTransform()));

            // Datepicker style
            bundles.Add(new StyleBundle("~/bundles/datepicker/css").Include("~/Vendor/bootstrap-datepicker-master/dist/css/bootstrap-datepicker3.min.css", new CssRewriteUrlTransform()));

            // Select 2 style
            bundle = new Bundle("~/bundles/select2/css")
            {
                Orderer = new AsIsBundleOrderer()
            };
            bundle.Include("~/Vendor/select2-3.5.2/select2.css", new CssRewriteUrlTransform())
                .Include("~/Vendor/select2-bootstrap/select2-bootstrap.css", new CssRewriteUrlTransform());
            bundles.Add(bundle);

            // Styles
            bundle = new Bundle("~/bundles/toogle/css")
            {
                Orderer = new AsIsBundleOrderer()
            };
            bundle.Include("~/styles/bootstrap-toggle.min.css", new CssRewriteUrlTransform())
                .Include("~/styles/switch.css", new CssRewriteUrlTransform());
            bundles.Add(bundle);

            // Datepicker styles
            bundle = new Bundle("~/bundles/summernote/css")
            {
                Orderer = new AsIsBundleOrderer()
            };
            bundle.Include("~/Vendor/summernote/dist/summernote.css", new CssRewriteUrlTransform())
                .Include("~/Vendor/summernote/dist/summernote-bs3.css", new CssRewriteUrlTransform());
            bundles.Add(bundle);

            // bootstrap-tour style
            bundles.Add(new StyleBundle("~/bundles/bootstrap-tour/css").Include("~/Vendor/bootstrap-tour/build/css/bootstrap-tour.min.css", new CssRewriteUrlTransform()));

            // bootstrap-touchspin style
            bundles.Add(new StyleBundle("~/bundles/bootstrap-touchspin/css").Include("~/Vendor/bootstrap-touchspin/dist/jquery.bootstrap-touchspin.min.css", new CssRewriteUrlTransform()));

            // Homer scripts
            bundle = new Bundle("~/bundles/homer/js")
            {
                Orderer = new AsIsBundleOrderer()
            };
            bundle.Include("~/Vendor/metisMenu/dist/metisMenu.min.js")
                .Include("~/Vendor/iCheck/icheck.min.js")
                .Include("~/Vendor/peity/jquery.peity.min.js")
                .Include("~/Vendor/sparkline/index.js")
                .Include("~/Scripts/homer.js")
                .Include("~/Scripts/charts.js")
                .Include("~/vendor/toastr/build/toastr.min.js")
                .Include("~/Vendor/sweetalert/lib/sweet-alert.js");
            bundles.Add(bundle);

            // Scripts
            bundles.Add(new ScriptBundle("~/bundles/axios/js").Include("~/Scripts/axios.min.js"));

            // Bootstrap scripts
            bundles.Add(new ScriptBundle("~/bundles/bootstrap/js").Include("~/Vendor/bootstrap/dist/js/bootstrap.min.js"));

            // jQuery scripts
            bundle = new Bundle("~/bundles/jquery/js")
            {
                Orderer = new AsIsBundleOrderer()
            };
            bundle.Include("~/Vendor/jquery/dist/jquery.min.js")
                .Include("~/Vendor/jquery-ui/jquery-ui.min.js");
            bundles.Add(bundle);

            // Datatables scripts
            bundles.Add(new ScriptBundle("~/bundles/datatables/js").Include("~/Scripts/Datatable.js"));

            // Datatables bootstrap scripts
            bundles.Add(new ScriptBundle("~/bundles/datatablesBootstrap/js").Include("~/Vendor/datatables.net-bs/js/dataTables.bootstrap.min.js"));

            // Datatables plugins scripts
            bundle = new Bundle("~/bundles/datatablesPlugins/js")
            {
                Orderer = new AsIsBundleOrderer()
            };
            bundle.Include("~/Vendor/pdfmake/build/pdfmake.min.js")
                .Include("~/Vendor/pdfmake/build/vfs_fonts.js")
                .Include("~/Vendor/datatables.net-buttons/js/buttons.html5.min.js")
                .Include("~/Vendor/datatables.net-buttons/js/buttons.print.min.js")
                .Include("~/Vendor/datatables.net-buttons/js/dataTables.buttons.min.js")
                .Include("~/Vendor/datatables.net-buttons-bs/js/buttons.bootstrap.min.js");
            bundles.Add(bundle);

            // Scripts
            bundle = new Bundle("~/bundles/select2/js")
            {
                Orderer = new AsIsBundleOrderer()
            };
            bundle.Include("~/Vendor/select2-3.5.2/select2.min.js")
                .Include("~/Vendor/select2-3.5.2/select2_locale_es.js");
            bundles.Add(bundle);

            // Datepicker scripts
            bundle = new Bundle("~/bundles/datepicker/js")
            {
                Orderer = new AsIsBundleOrderer()
            };
            bundle.Include("~/Vendor/bootstrap-datepicker-master/dist/js/bootstrap-datepicker.min.js")
                .Include("~/Vendor/bootstrap-datepicker-master/dist/locales/bootstrap-datepicker.es.min.js")
                .Include("~/Vendor/moment/moment.js");
            bundles.Add(bundle);

            // Scripts
            bundles.Add(new ScriptBundle("~/bundles/toogle/js").Include("~/Scripts/bootstrap-toggle.min.js"));

            // Knockout scripts
            bundles.Add(new ScriptBundle("~/bundles/knockout").Include("~/Scripts/knockout-min.js"));

            bundles.Add(new ScriptBundle("~/bundles/AI001/js").Include(
                "~/Scripts/knockout-min.js",
                "~/Scripts/Warehouse/AI001.js"));

            bundles.Add(new ScriptBundle("~/bundles/INVE006/js").Include(
                "~/Scripts/knockout-min.js",
                "~/Scripts/Warehouse/INVE006.js"));

            bundles.Add(new ScriptBundle("~/bundles/RMA003/js").Include(
                "~/Scripts/knockout-min.js",
                "~/Scripts/Warehouse/RMA003.js"));

            // Scripts
            bundles.Add(new ScriptBundle("~/bundles/layout/js").Include("~/Scripts/Shared/layout.js"));
            bundles.Add(new ScriptBundle("~/bundles/ADMS001/js").Include("~/Scripts/Configuration/ADMS001.js"));
            bundles.Add(new ScriptBundle("~/bundles/ADMS002/js").Include("~/Scripts/Configuration/ADMS002.js"));
            bundles.Add(new ScriptBundle("~/bundles/ADMS003/js").Include("~/Scripts/Configuration/ADMS003.js"));
            bundles.Add(new ScriptBundle("~/bundles/ADMS004/js").Include("~/Scripts/Configuration/ADMS004.js"));
            bundles.Add(new ScriptBundle("~/bundles/ADMS006/js").Include("~/Scripts/Configuration/ADMS006.js"));
            bundles.Add(new ScriptBundle("~/bundles/ADMS021/js").Include("~/Scripts/Configuration/ADMS021.js"));
            bundles.Add(new ScriptBundle("~/bundles/ITEM001/js").Include("~/Scripts/Items/ITEM001.js"));
            bundles.Add(new ScriptBundle("~/bundles/MRO001/js").Include("~/Scripts/MRO/MRO001.js"));
            bundles.Add(new ScriptBundle("~/bundles/PURC000/js").Include("~/Scripts/Purchases/PURC000.js"));
            bundles.Add(new ScriptBundle("~/bundles/PURC001/js").Include("~/Scripts/Purchases/PURC001.js"));
            bundles.Add(new ScriptBundle("~/bundles/PURC002/js").Include("~/Scripts/Purchases/PURC002.js"));
            bundles.Add(new ScriptBundle("~/bundles/PURC003/js").Include("~/Scripts/Purchases/PURC003.js"));
            bundles.Add(new ScriptBundle("~/bundles/PURC004/js").Include("~/Scripts/Purchases/PURC004.js"));
            bundles.Add(new ScriptBundle("~/bundles/PURC005-2/js").Include("~/Scripts/Purchases/PURC005-2.js"));
            bundles.Add(new ScriptBundle("~/bundles/PURC008/js").Include("~/Scripts/Purchases/PURC008.js"));
            bundles.Add(new ScriptBundle("~/bundles/PURC0010/js").Include("~/Scripts/Purchases/PURC0010.js"));
            bundles.Add(new ScriptBundle("~/bundles/PURC011/js").Include("~/Scripts/Purchases/PURC011.js"));
            bundles.Add(new ScriptBundle("~/bundles/PURC012/js").Include("~/Scripts/Purchases/PURC012.js"));
            bundles.Add(new ScriptBundle("~/bundles/PURC013/js").Include("~/Scripts/Purchases/PURC013.js"));
            bundles.Add(new ScriptBundle("~/bundles/PURC014/js").Include("~/Scripts/Purchases/PURC014.js"));
            bundles.Add(new ScriptBundle("~/bundles/PURC0014/js").Include("~/Scripts/Purchases/PURC0014.js"));
            bundles.Add(new ScriptBundle("~/bundles/PURCH015/js").Include("~/Scripts/Purchases/PURCH015.js"));
            bundles.Add(new ScriptBundle("~/bundles/PURC016/js").Include("~/Scripts/Purchases/PURC016.js"));
            bundles.Add(new ScriptBundle("~/bundles/PURC017/js").Include("~/Scripts/Purchases/PURC017.js"));
            bundles.Add(new ScriptBundle("~/bundles/PURC018/js").Include("~/Scripts/Purchases/PURC018.js"));
            bundles.Add(new ScriptBundle("~/bundles/PURC019/js").Include("~/Scripts/Purchases/PURC019.js"));
            bundles.Add(new ScriptBundle("~/bundles/PURC020/js").Include("~/Scripts/Purchases/PURC020.js"));
            bundles.Add(new ScriptBundle("~/bundles/PURC021/js").Include("~/Scripts/Purchases/PURC021.js"));
            bundles.Add(new ScriptBundle("~/bundles/PURCH022/js").Include("~/Scripts/Purchases/PURCH022.js"));
            bundles.Add(new ScriptBundle("~/bundles/PURCH023/js").Include("~/Scripts/Purchases/PURCH023.js"));
            bundles.Add(new ScriptBundle("~/bundles/PURCH024/js").Include("~/Scripts/Purchases/PURCH024.js"));
            bundles.Add(new ScriptBundle("~/bundles/PURCH025/js").Include("~/Scripts/Purchases/PURCH025.js"));
            bundles.Add(new ScriptBundle("~/bundles/PURCH026/js").Include("~/Scripts/Purchases/PURCH026.js"));
            bundles.Add(new ScriptBundle("~/bundles/ORDE001/js").Include("~/Scripts/Purchases/ORDE001.js"));
            bundles.Add(new ScriptBundle("~/bundles/ORDE001-2/js").Include("~/Scripts/Purchases/ORDE001-2.js"));
            bundles.Add(new ScriptBundle("~/bundles/ORDE002/js").Include("~/Scripts/Purchases/ORDE002.js"));
            bundles.Add(new ScriptBundle("~/bundles/COST001/js").Include("~/Scripts/Purchases/COST001.js"));
            bundles.Add(new ScriptBundle("~/bundles/COST002/js").Include("~/Scripts/Purchases/COST002.js"));
            bundles.Add(new ScriptBundle("~/bundles/AGT001/js").Include("~/Scripts/Warehouse/AGT001.js"));
            bundles.Add(new ScriptBundle("~/bundles/AGT002/js").Include("~/Scripts/Warehouse/AGT002.js"));
            bundles.Add(new ScriptBundle("~/bundles/AGT003/js").Include("~/Scripts/Warehouse/AGT003.js"));
            bundles.Add(new ScriptBundle("~/bundles/AI002/js").Include("~/Scripts/Warehouse/AI002.js"));
            bundles.Add(new ScriptBundle("~/bundles/CT001/js").Include("~/Scripts/Warehouse/CT001.js"));
            bundles.Add(new ScriptBundle("~/bundles/CT002/js").Include("~/Scripts/Warehouse/CT002.js"));
            bundles.Add(new ScriptBundle("~/bundles/CT003/js").Include("~/Scripts/Warehouse/CT003.js"));
            bundles.Add(new ScriptBundle("~/bundles/DRIV001/js").Include("~/Scripts/Warehouse/DRIV001.js"));
            bundles.Add(new ScriptBundle("~/bundles/EMSC001/js").Include("~/Scripts/Warehouse/EMSC001.js"));
            bundles.Add(new ScriptBundle("~/bundles/EMSC002/js").Include("~/Scripts/Warehouse/EMSC002.js"));
            bundles.Add(new ScriptBundle("~/bundles/ESS001/js").Include("~/Scripts/Warehouse/ESS001.js"));
            bundles.Add(new ScriptBundle("~/bundles/EXP001/js").Include("~/Scripts/Warehouse/EXP001.js"));
            bundles.Add(new ScriptBundle("~/bundles/OCRC001/js").Include("~/Scripts/Warehouse/OCRC001.js"));
            bundles.Add(new ScriptBundle("~/bundles/OCRG001/js").Include("~/Scripts/Warehouse/OCRG001.js"));
            bundles.Add(new ScriptBundle("~/bundles/RECE002/js").Include("~/Scripts/Warehouse/RECE002.js"));
            bundles.Add(new ScriptBundle("~/bundles/RMA001/js").Include("~/Scripts/Warehouse/RMA001.js"));
            bundles.Add(new ScriptBundle("~/bundles/RMA002/js").Include("~/Scripts/Warehouse/RMA002.js"));
            bundles.Add(new ScriptBundle("~/bundles/RMA004/js").Include("~/Scripts/Warehouse/RMA004.js"));
            bundles.Add(new ScriptBundle("~/bundles/RMA005/js").Include("~/Scripts/Warehouse/RMA005.js"));
            bundles.Add(new ScriptBundle("~/bundles/RMA006/js").Include("~/Scripts/Warehouse/RMA006.js"));
            bundles.Add(new ScriptBundle("~/bundles/RMA007/js").Include("~/Scripts/Warehouse/RMA007.js"));
            bundles.Add(new ScriptBundle("~/bundles/RMA008/js").Include("~/Scripts/Warehouse/RMA008.js"));
            bundles.Add(new ScriptBundle("~/bundles/RMA009/js").Include("~/Scripts/Warehouse/RMA009.js"));
            bundles.Add(new ScriptBundle("~/bundles/RMA010/js").Include("~/Scripts/Warehouse/RMA010.js"));
            bundles.Add(new ScriptBundle("~/bundles/RMA011/js").Include("~/Scripts/Warehouse/RMA011.js"));
            bundles.Add(new ScriptBundle("~/bundles/RPD001/js").Include("~/Scripts/Warehouse/RPD001.js"));
            bundles.Add(new ScriptBundle("~/bundles/RPD002/js").Include("~/Scripts/Warehouse/RPD002.js"));
            bundles.Add(new ScriptBundle("~/bundles/RPD003/js").Include("~/Scripts/Warehouse/RPD003.js"));
            bundles.Add(new ScriptBundle("~/bundles/RPD005/js").Include("~/Scripts/Warehouse/RPD005.js"));
            bundles.Add(new ScriptBundle("~/bundles/RPD006/js").Include("~/Scripts/Warehouse/RPD006.js"));
            bundles.Add(new ScriptBundle("~/bundles/RPD007/js").Include("~/Scripts/Warehouse/RPD007.js"));
            bundles.Add(new ScriptBundle("~/bundles/RPD008/js").Include("~/Scripts/Warehouse/RPD008.js"));
            bundles.Add(new ScriptBundle("~/bundles/RPD011/js").Include("~/Scripts/Warehouse/RPD011.js"));
            bundles.Add(new ScriptBundle("~/bundles/RPD013/js").Include("~/Scripts/Warehouse/RPD013.js"));
            bundles.Add(new ScriptBundle("~/bundles/RPD014/js").Include("~/Scripts/Warehouse/RPD014.js"));
            bundles.Add(new ScriptBundle("~/bundles/SSM001/js").Include("~/Scripts/Warehouse/SSM001.js"));
            bundles.Add(new ScriptBundle("~/bundles/SSM002/js").Include("~/Scripts/Warehouse/SSM002.js"));
            bundles.Add(new ScriptBundle("~/bundles/SSM003/js").Include("~/Scripts/Warehouse/SSM003.js"));
            bundles.Add(new ScriptBundle("~/bundles/STGL001/js").Include("~/Scripts/Warehouse/STGL001.js"));
            bundles.Add(new ScriptBundle("~/bundles/STGL002/js").Include("~/Scripts/Warehouse/STGL002.js"));
            bundles.Add(new ScriptBundle("~/bundles/TMP001/js").Include("~/Scripts/Warehouse/TMP001.js"));
            bundles.Add(new ScriptBundle("~/bundles/TMP002/js").Include("~/Scripts/Warehouse/TMP002.js"));
            bundles.Add(new ScriptBundle("~/bundles/PURCH002/js").Include("~/Scripts/Purchases/PURCH002.js"));
            bundles.Add(new ScriptBundle("~/bundles/PURCH003/js").Include("~/Scripts/Purchases/PURCH003.js"));
            bundles.Add(new ScriptBundle("~/bundles/SALE026/js").Include("~/Scripts/Sales/SALE026.js"));
            bundles.Add(new ScriptBundle("~/bundles/Unlocking/js").Include("~/Scripts/Home/Unlocking.js"));

            // jQuery Validation
            bundles.Add(new ScriptBundle("~/bundles/validation/js").Include("~/Scripts/jquery.validate.js"));

            //request
            bundles.Add(new ScriptBundle("~/bundles/request").Include("~/Scripts/requests/*.js"));

            // scripts
            bundles.Add(new ScriptBundle("~/bundles/expenses").Include("~/Scripts/expenses/*.js"));
            bundles.Add(new ScriptBundle("~/bundles/approval").Include("~/Scripts/approval/*.js"));

            // scripts bootstrap-tour
            bundles.Add(new ScriptBundle("~/bundles/bootstrap-tour/js").Include("~/Vendor/bootstrap-tour/build/js/bootstrap-tour.min.js"));

            // scripts bootstrap-touchspin
            bundles.Add(new ScriptBundle("~/bundles/bootstrap-touchspin/js").Include("~/Vendor/bootstrap-touchspin/dist/jquery.bootstrap-touchspin.min.js"));

            // scripts slimscroll
            bundles.Add(new ScriptBundle("~/bundles/slimscroll/js").Include("~/Vendor/bootstrap-touchspin/dist/jquery.bootstrap-touchspin.min.js"));
        }
    }

    public class BundlesTypes
    {
        public const string Module = @"<script href=""{0}"" type=""module"" ></script>";
    }

    public class AsIsBundleOrderer : IBundleOrderer
    {
        public virtual IEnumerable<BundleFile> OrderFiles(BundleContext context, IEnumerable<BundleFile> files)
        {
            return files;
        }
    }
}