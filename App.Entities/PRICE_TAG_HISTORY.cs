//------------------------------------------------------------------------------
// <auto-generated>
//     Este código se generó a partir de una plantilla.
//
//     Los cambios manuales en este archivo pueden causar un comportamiento inesperado de la aplicación.
//     Los cambios manuales en este archivo se sobrescribirán si se regenera el código.
// </auto-generated>
//------------------------------------------------------------------------------

namespace App.Entities
{
    using System;
    using System.Collections.Generic;
    
    public partial class PRICE_TAG_HISTORY
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public PRICE_TAG_HISTORY()
        {
            this.PRICE_TAG_HISTORY_DETAIL = new HashSet<PRICE_TAG_HISTORY_DETAIL>();
        }
    
        public int history_id { get; set; }
        public string print_status { get; set; }
        public bool reprint { get; set; }
        public string process_type { get; set; }
        public int tag_count { get; set; }
        public string cuser { get; set; }
        public Nullable<System.DateTime> cdate { get; set; }
        public string uuser { get; set; }
        public Nullable<System.DateTime> udate { get; set; }
    
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<PRICE_TAG_HISTORY_DETAIL> PRICE_TAG_HISTORY_DETAIL { get; set; }
    }
}
