//------------------------------------------------------------------------------
// <auto-generated>
//     Este código se generó a partir de una plantilla.
//
//     Los cambios manuales en este archivo pueden causar un comportamiento inesperado de la aplicación.
//     Los cambios manuales en este archivo se sobrescribirán si se regenera el código.
// </auto-generated>
//------------------------------------------------------------------------------

namespace App.Entities
{
    using System;
    using System.Collections.Generic;
    
    public partial class INVENTORY_DETAIL_HISTORY
    {
        public int id_history { get; set; }
        public int id_inventory_detail { get; set; }
        public string id_inventory_header { get; set; }
        public decimal after_quantity { get; set; }
        public decimal before_quantity { get; set; }
        public string part_number { get; set; }
        public string storage_location { get; set; }
        public string cuser { get; set; }
        public System.DateTime cdate { get; set; }
        public string program_id { get; set; }
    }
}
