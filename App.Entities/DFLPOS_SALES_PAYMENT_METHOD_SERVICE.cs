//------------------------------------------------------------------------------
// <auto-generated>
//     Este código se generó a partir de una plantilla.
//
//     Los cambios manuales en este archivo pueden causar un comportamiento inesperado de la aplicación.
//     Los cambios manuales en este archivo se sobrescribirán si se regenera el código.
// </auto-generated>
//------------------------------------------------------------------------------

namespace App.Entities
{
    using System;
    using System.Collections.Generic;
    
    public partial class DFLPOS_SALES_PAYMENT_METHOD_SERVICE
    {
        public string site_code { get; set; }
        public string pos_id { get; set; }
        public long sale_id { get; set; }
        public int pm_id { get; set; }
        public decimal amount { get; set; }
        public string payment_method_status { get; set; }
        public string invoice_serie { get; set; }
        public Nullable<long> invoice_number { get; set; }
        public string userr_id { get; set; }
        public Nullable<decimal> amount_entered { get; set; }
    
        public virtual DFLPOS_SALES DFLPOS_SALES { get; set; }
        public virtual USER_MASTER USER_MASTER { get; set; }
        public virtual PAYMENT_METHOD PAYMENT_METHOD { get; set; }
    }
}
