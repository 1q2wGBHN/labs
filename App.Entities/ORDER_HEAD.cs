//------------------------------------------------------------------------------
// <auto-generated>
//     Este código se generó a partir de una plantilla.
//
//     Los cambios manuales en este archivo pueden causar un comportamiento inesperado de la aplicación.
//     Los cambios manuales en este archivo se sobrescribirán si se regenera el código.
// </auto-generated>
//------------------------------------------------------------------------------

namespace App.Entities
{
    using System;
    using System.Collections.Generic;
    
    public partial class ORDER_HEAD
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public ORDER_HEAD()
        {
            this.ORDER_LIST = new HashSet<ORDER_LIST>();
        }
    
        public int folio { get; set; }
        public int supplier_id { get; set; }
        public string site_id { get; set; }
        public int refill_days { get; set; }
        public Nullable<System.DateTime> datee { get; set; }
        public string comment { get; set; }
        public int status { get; set; }
        public string program_id { get; set; }
        public string cuser { get; set; }
        public Nullable<System.DateTime> cdate { get; set; }
        public string uuser { get; set; }
        public Nullable<System.DateTime> udate { get; set; }
    
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ORDER_LIST> ORDER_LIST { get; set; }
    }
}
