//------------------------------------------------------------------------------
// <auto-generated>
//     Este código se generó a partir de una plantilla.
//
//     Los cambios manuales en este archivo pueden causar un comportamiento inesperado de la aplicación.
//     Los cambios manuales en este archivo se sobrescribirán si se regenera el código.
// </auto-generated>
//------------------------------------------------------------------------------

namespace App.Entities
{
    using System;
    using System.Collections.Generic;
    
    public partial class DFLPOS_TYPE_CHANGE_PRICE
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public DFLPOS_TYPE_CHANGE_PRICE()
        {
            this.DFLPOS_CHANGES_PRICES = new HashSet<DFLPOS_CHANGES_PRICES>();
        }
    
        public int id { get; set; }
        public string description { get; set; }
        public bool visible { get; set; }
    
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<DFLPOS_CHANGES_PRICES> DFLPOS_CHANGES_PRICES { get; set; }
    }
}
