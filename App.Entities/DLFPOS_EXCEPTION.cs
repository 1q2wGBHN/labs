//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace App.Entities
{
    using System;
    using System.Collections.Generic;
    
    public partial class DLFPOS_EXCEPTION
    {
        public long sale_id { get; set; }
        public string exception { get; set; }
        public System.DateTime timestampp { get; set; }
        public string process { get; set; }
        public string reference { get; set; }
        public string userr_id { get; set; }
        public string pos_id { get; set; }
    
        public virtual DFLPOS_SALES DFLPOS_SALES { get; set; }
        public virtual USER_MASTER USER_MASTER { get; set; }
    }
}
