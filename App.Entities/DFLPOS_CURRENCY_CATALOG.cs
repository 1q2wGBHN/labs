//------------------------------------------------------------------------------
// <auto-generated>
//     Este código se generó a partir de una plantilla.
//
//     Los cambios manuales en este archivo pueden causar un comportamiento inesperado de la aplicación.
//     Los cambios manuales en este archivo se sobrescribirán si se regenera el código.
// </auto-generated>
//------------------------------------------------------------------------------

namespace App.Entities
{
    using System;
    using System.Collections.Generic;
    
    public partial class DFLPOS_CURRENCY_CATALOG
    {
        public decimal denomination { get; set; }
        public string currency { get; set; }
        public string descriptionn { get; set; }
        public Nullable<int> pm_id { get; set; }
        public string active { get; set; }
    
        public virtual PAYMENT_METHOD PAYMENT_METHOD { get; set; }
    }
}
