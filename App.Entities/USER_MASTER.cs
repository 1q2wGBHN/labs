//------------------------------------------------------------------------------
// <auto-generated>
//     Este código se generó a partir de una plantilla.
//
//     Los cambios manuales en este archivo pueden causar un comportamiento inesperado de la aplicación.
//     Los cambios manuales en este archivo se sobrescribirán si se regenera el código.
// </auto-generated>
//------------------------------------------------------------------------------

namespace App.Entities
{
    using System;
    using System.Collections.Generic;
    
    public partial class USER_MASTER
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public USER_MASTER()
        {
            this.DFLPOS_ACCUMULATED = new HashSet<DFLPOS_ACCUMULATED>();
            this.DFLPOS_BARCODE_CAPTURE_TIME = new HashSet<DFLPOS_BARCODE_CAPTURE_TIME>();
            this.DFLPOS_CASH_WITHDRAWAL = new HashSet<DFLPOS_CASH_WITHDRAWAL>();
            this.DFLPOS_CASH_WITHDRAWAL1 = new HashSet<DFLPOS_CASH_WITHDRAWAL>();
            this.DFLPOS_CHANGES_PRICES = new HashSet<DFLPOS_CHANGES_PRICES>();
            this.DFLPOS_CUTZ_HEADER = new HashSet<DFLPOS_CUTZ_HEADER>();
            this.DFLPOS_DONATION_SALE = new HashSet<DFLPOS_DONATION_SALE>();
            this.DFLPOS_DONATIONS_PAYMENT_METHOD = new HashSet<DFLPOS_DONATIONS_PAYMENT_METHOD>();
            this.DFLPOS_EXCEPTION = new HashSet<DFLPOS_EXCEPTION>();
            this.DFLPOS_SALES = new HashSet<DFLPOS_SALES>();
            this.DFLPOS_SALES_PAYMENT_METHOD = new HashSet<DFLPOS_SALES_PAYMENT_METHOD>();
            this.DFLPOS_SALES_PAYMENT_METHOD_SERVICE = new HashSet<DFLPOS_SALES_PAYMENT_METHOD_SERVICE>();
            this.DFLPOS_TRANSACTIONS_CARD = new HashSet<DFLPOS_TRANSACTIONS_CARD>();
            this.DFLPOS_USER_CARD_ACCESS = new HashSet<DFLPOS_USER_CARD_ACCESS>();
            this.SYS_ROLE_USER = new HashSet<SYS_ROLE_USER>();
            this.USER_SITES = new HashSet<USER_SITES>();
            this.DFLPOS_CANCELLATION = new HashSet<DFLPOS_CANCELLATION>();
            this.DFLPOS_CANCELLATION1 = new HashSet<DFLPOS_CANCELLATION>();
            this.DFLPOS_INCIDENT = new HashSet<DFLPOS_INCIDENT>();
            this.SANCTIONS = new HashSet<SANCTIONS>();
        }
    
        public string emp_no { get; set; }
        public string user_name { get; set; }
        public string first_name { get; set; }
        public string last_name { get; set; }
        public string office_tel { get; set; }
        public string mobile_tel { get; set; }
        public string email { get; set; }
        public Nullable<System.DateTime> join_date { get; set; }
        public Nullable<System.DateTime> birth_date { get; set; }
        public byte[] photo { get; set; }
        public Nullable<int> department_id { get; set; }
        public Nullable<System.DateTime> cdate { get; set; }
        public string cuser { get; set; }
        public Nullable<System.DateTime> udate { get; set; }
        public string uuser { get; set; }
        public string program_id { get; set; }
        public string buyer_division { get; set; }
        public string district_code { get; set; }
    
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<DFLPOS_ACCUMULATED> DFLPOS_ACCUMULATED { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<DFLPOS_BARCODE_CAPTURE_TIME> DFLPOS_BARCODE_CAPTURE_TIME { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<DFLPOS_CASH_WITHDRAWAL> DFLPOS_CASH_WITHDRAWAL { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<DFLPOS_CASH_WITHDRAWAL> DFLPOS_CASH_WITHDRAWAL1 { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<DFLPOS_CHANGES_PRICES> DFLPOS_CHANGES_PRICES { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<DFLPOS_CUTZ_HEADER> DFLPOS_CUTZ_HEADER { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<DFLPOS_DONATION_SALE> DFLPOS_DONATION_SALE { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<DFLPOS_DONATIONS_PAYMENT_METHOD> DFLPOS_DONATIONS_PAYMENT_METHOD { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<DFLPOS_EXCEPTION> DFLPOS_EXCEPTION { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<DFLPOS_SALES> DFLPOS_SALES { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<DFLPOS_SALES_PAYMENT_METHOD> DFLPOS_SALES_PAYMENT_METHOD { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<DFLPOS_SALES_PAYMENT_METHOD_SERVICE> DFLPOS_SALES_PAYMENT_METHOD_SERVICE { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<DFLPOS_TRANSACTIONS_CARD> DFLPOS_TRANSACTIONS_CARD { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<DFLPOS_USER_CARD_ACCESS> DFLPOS_USER_CARD_ACCESS { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<SYS_ROLE_USER> SYS_ROLE_USER { get; set; }
        public virtual USER_MASTER USER_MASTER1 { get; set; }
        public virtual USER_MASTER USER_MASTER2 { get; set; }
        public virtual USER_PWD USER_PWD { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<USER_SITES> USER_SITES { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<DFLPOS_CANCELLATION> DFLPOS_CANCELLATION { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<DFLPOS_CANCELLATION> DFLPOS_CANCELLATION1 { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<DFLPOS_INCIDENT> DFLPOS_INCIDENT { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<SANCTIONS> SANCTIONS { get; set; }
    }
}
