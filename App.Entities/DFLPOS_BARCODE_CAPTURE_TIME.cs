//------------------------------------------------------------------------------
// <auto-generated>
//     Este código se generó a partir de una plantilla.
//
//     Los cambios manuales en este archivo pueden causar un comportamiento inesperado de la aplicación.
//     Los cambios manuales en este archivo se sobrescribirán si se regenera el código.
// </auto-generated>
//------------------------------------------------------------------------------

namespace App.Entities
{
    using System;
    using System.Collections.Generic;
    
    public partial class DFLPOS_BARCODE_CAPTURE_TIME
    {
        public int id { get; set; }
        public string userr_id { get; set; }
        public long sale_id { get; set; }
        public string expresion { get; set; }
        public decimal ms { get; set; }
        public System.DateTime start_time { get; set; }
        public System.DateTime finish_time { get; set; }
        public int keyed_type { get; set; }
    
        public virtual DFLPOS_SALES DFLPOS_SALES { get; set; }
        public virtual USER_MASTER USER_MASTER { get; set; }
    }
}
