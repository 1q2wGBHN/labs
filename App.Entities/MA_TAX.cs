//------------------------------------------------------------------------------
// <auto-generated>
//     Este código se generó a partir de una plantilla.
//
//     Los cambios manuales en este archivo pueden causar un comportamiento inesperado de la aplicación.
//     Los cambios manuales en este archivo se sobrescribirán si se regenera el código.
// </auto-generated>
//------------------------------------------------------------------------------

namespace App.Entities
{
    using System;
    using System.Collections.Generic;
    
    public partial class MA_TAX
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public MA_TAX()
        {
            this.INVOICE_DETAIL_TAX = new HashSet<INVOICE_DETAIL_TAX>();
            this.ITEM = new HashSet<ITEM>();
            this.ITEM1 = new HashSet<ITEM>();
            this.ITEM2 = new HashSet<ITEM>();
            this.CREDIT_NOTE_DETAIL_TAX = new HashSet<CREDIT_NOTE_DETAIL_TAX>();
        }
    
        public string tax_code { get; set; }
        public string name { get; set; }
        public Nullable<decimal> tax_value { get; set; }
        public string tax_type { get; set; }
        public string acc_no { get; set; }
        public string cuser { get; set; }
        public Nullable<System.DateTime> cdate { get; set; }
        public string uuser { get; set; }
        public Nullable<System.DateTime> udate { get; set; }
        public string type_stc { get; set; }
        public string factor_type { get; set; }
    
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<INVOICE_DETAIL_TAX> INVOICE_DETAIL_TAX { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ITEM> ITEM { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ITEM> ITEM1 { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ITEM> ITEM2 { get; set; }
        public virtual SAT_TAX_CODE SAT_TAX_CODE { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<CREDIT_NOTE_DETAIL_TAX> CREDIT_NOTE_DETAIL_TAX { get; set; }
    }
}
