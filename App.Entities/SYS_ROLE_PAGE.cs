//------------------------------------------------------------------------------
// <auto-generated>
//     Este código se generó a partir de una plantilla.
//
//     Los cambios manuales en este archivo pueden causar un comportamiento inesperado de la aplicación.
//     Los cambios manuales en este archivo se sobrescribirán si se regenera el código.
// </auto-generated>
//------------------------------------------------------------------------------

namespace App.Entities
{
    using System;
    using System.Collections.Generic;
    
    public partial class SYS_ROLE_PAGE
    {
        public string page_id { get; set; }
        public string role_id { get; set; }
        public Nullable<System.DateTime> cdate { get; set; }
        public string cuser { get; set; }
        public Nullable<System.DateTime> udate { get; set; }
        public string uuser { get; set; }
        public string program_id { get; set; }
    
        public virtual SYS_PAGE_MASTER SYS_PAGE_MASTER { get; set; }
        public virtual SYS_ROLE_MASTER SYS_ROLE_MASTER { get; set; }
    }
}
