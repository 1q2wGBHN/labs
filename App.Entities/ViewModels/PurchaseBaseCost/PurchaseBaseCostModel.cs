﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App.Entities.ViewModels.PurchaseBaseCost
{
    public class PurchaseBaseCostModel
    {
        public string purchase_base_cost_id { get; set; }
        public string supplier_part_number { get; set; }
        public decimal base_cost { get; set; }
        public string currency { get; set; }
        public decimal unit_of_price { get; set; }
    }
}
