﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App.Entities.ViewModels.MaClass
{
    public class MaClassModel
    {
        public int class_id { get; set; }
        public string class_name { get; set; }
        public string description { get; set; }
    }
}
