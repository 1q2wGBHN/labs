﻿namespace App.Entities.ViewModels.Withdrawal
{
    public class DollarsConciliation
    {
        public string Cashier { get; set; }
        public decimal Received { get; set; }
        public decimal Cancelled { get; set; }
        public decimal Withdrawal { get; set; }
        public decimal Difference { get; set; }
        public string CashierId { get; set; }

    }
}
