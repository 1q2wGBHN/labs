﻿using System.ComponentModel;

namespace App.Entities.ViewModels.Withdrawal
{
    public class WithdrawalHeader
    {
        [DisplayName("Cajero")]
        public string Cashier { get; set; }

        [DisplayName("Moneda")]
        public string Currency { get; set; }

        [DisplayName("Monto")]
        public decimal Amount { get; set; }

        public string EmployeeNo { get; set; }
        public decimal TotalAmount { get; set; }
    }
}
