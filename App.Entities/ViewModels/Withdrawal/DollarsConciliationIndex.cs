﻿using System;
using System.Collections.Generic;
using System.ComponentModel;

namespace App.Entities.ViewModels.Withdrawal
{
    public class DollarsConciliationIndex
    {
        public DollarsConciliationIndex()
        {
            Date = DateTime.Now.Date.ToString("dd/MM/yyyy");
            IsFromMenu = true;
        }
        [DisplayName("Fecha: ")]
        public string Date { get; set; }    
        public bool IsFromMenu { get; set; }
        public List<DollarsConciliation> DataReport { get; set; }
        public string SiteName { get; set; }


    }
}
