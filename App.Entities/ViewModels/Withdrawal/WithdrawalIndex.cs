﻿using System;
using System.Collections.Generic;
using System.ComponentModel;

namespace App.Entities.ViewModels.Withdrawal
{
    public class WithdrawalIndex
    {
        public WithdrawalIndex()
        {
            Date = DateTime.Now.Date.ToString("dd/MM/yyyy");
            IsFromMenu = true;
            TotalAmount = 0;
            ExchangeRate = 0;
        }

        [DisplayName("Cajero: ")]
        public string Cashier { get; set; }

        [DisplayName("Fecha: ")]
        public string Date { get; set; }

        public bool IsFromMenu { get; set; }

        public List<WithdrawalHeader> DataReport { get; set; }
        public List<WithdrawalHeader> AcumulateData { get; set; }

        public decimal TotalAmount { get; set; }
        public decimal ExchangeRate { get; set; }

        public string SiteName { get; set; }
    }
}
