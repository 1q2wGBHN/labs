﻿using System.ComponentModel;

namespace App.Entities.ViewModels.Withdrawal
{
    public class WithdrawalDetails
    {
        [DisplayName("Fecha y Hora: ")]
        public string Date { get; set; }

        [DisplayName("Caja: ")]
        public string Site { get; set; }

        [DisplayName("Monto: ")]
        public decimal Amount { get; set; }

        [DisplayName("Tipo: ")]
        public string WithdrawalType { get; set; }
    }
}
