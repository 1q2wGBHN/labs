﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App.Entities.ViewModels.ConfigPosSales
{
    public class PosSale
    {
        public int Id { get; set; }

        [DisplayName("Nombre:")]
        public string Key { get; set; }

        [DisplayName("Valor:")]
        public string Value { get; set; }

        [DisplayName("Caja:")]
        public string PosId { get; set; }

        [DisplayName("Sección: ")]
        public int Section { get; set; }
    }
}
