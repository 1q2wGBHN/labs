﻿using System;
using System.ComponentModel;

namespace App.Entities.ViewModels.CreditNotes
{
    public class CreditNoteIndex
    {
        public CreditNoteIndex()
        {
            Date = DateTime.Now.ToString("dd/MM/yyyy");
            SelectItems = false;
        }

        [DisplayName("No. Factura: ")]
        public string InvoiceNumber { get; set; }

        [DisplayName("No. Venta: ")]
        public string SaleNumber { get; set; }

        [DisplayName("Tipo:")]
        public CreditNoteType Type { get; set; }

        [DisplayName("Fecha:")]
        public string Date { get; set; }

        [DisplayName("Seleccionar Artículos")]
        public bool SelectItems { get; set; }
    }
}