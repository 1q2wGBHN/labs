﻿using System.ComponentModel.DataAnnotations;

namespace App.Entities.ViewModels.CreditNotes
{
    public enum CreditNoteStatus
    {
        [Display(Name = "Activas y Canceladas")]
        All,
        [Display(Name="Activas")]
        Active,

        [Display(Name = "Canceladas")]
        Cancelled,

       
    }
    public enum CreditNoteStatusFiscal
    {
        [Display(Name = "Timbradas y Sin Timbrar")]
        All,
        [Display(Name = "Timbradas")]
        Timbrada,

        [Display(Name = "Sin Timbrar")]
        SinTimbre,


    }
}