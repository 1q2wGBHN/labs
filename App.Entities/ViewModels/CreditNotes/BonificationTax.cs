﻿namespace App.Entities.ViewModels.CreditNotes
{
    public class BonificationTax
    {        
        public string TaxName { get; set; }
        public string TaxCode { get; set; }
        public decimal Bonification { get; set; }
        public decimal TaxValue { get; set; }
    }
}
