﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using App.Entities.ViewModels.Currency;
using System.ComponentModel;

namespace App.Entities.ViewModels.CreditNotes
{
    
    public class CreditNotesBonIndex
    {
        [DisplayName("Entre: ")]
        public string StartDate { get; set; }
        [DisplayName("Y: ")]
        public string  EndDate { get; set; }
        [DisplayName("Moneda: ")]    
        public Currencys Currency { get; set; }
        [DisplayName("Tipo: ")]
        public CreditNoteTypesReports Type { get; set; }
        [DisplayName("Estado: ")]
        public CreditNoteStatus Status { get; set; }
        [DisplayName("Timbre: ")]
        public CreditNoteStatusFiscal Uuid { get; set; }
        [DisplayName("Número: ")]
        public string CreNoBoId { get; set; }
        public List<CreditNotesBon> CreditNotesBonList { get; set; }
        public string modeda { get; set; }
        public string tipo { get; set; }
        public string estado { get; set; }
        public string timbre { get; set; }
        public bool CanEditDate { get; set; }
    }
}
