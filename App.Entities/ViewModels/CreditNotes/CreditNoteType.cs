﻿using System.ComponentModel.DataAnnotations;

namespace App.Entities.ViewModels.CreditNotes
{
    public enum CreditNoteType
    {
        [Display(Name="Nota de Crédito")]
        CreditNote,

        [Display(Name = "Bonificación")]
        Bonification,

        [Display(Name = "Bonificación Descuento Empleados")]
        EmployeeBonification
    }
    public enum CreditNoteTypesReports
    {
        [Display(Name = "Nota de Crédito y Bonificaciones")]
        All,
        [Display(Name = "Nota de Crédito")]
        CreditNote,

        [Display(Name = "Bonificación")]
        Bonification
    }
    public enum Currencys
    {
        //[Display(Name = "TODAS")]
        //ALL,
        [Display(Name = "Pesos Mexicanos (MXN)")]
        MXN,
        [Display(Name = "Dolares Americanos (USD)")]
        USD
    }

    public enum CurrencysCoonc
    {
        [Display(Name = "TODAS")]
        ALL,
        [Display(Name = "Pesos Mexicanos (MXN)")]
        MXN,
        [Display(Name = "Dolares Americanos (USD)")]
        USD
    }
}