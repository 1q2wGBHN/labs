﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App.Entities.ViewModels.CreditNotes
{
    public class CreditNotesBonDetail
    {
        public string CfdNotaNo { get; set; }
        public string Date { get; set; }
        public string Cause { get; set; }
        public long InvId { get; set; }
        public string Customer { get; set; }
        public decimal Import { get; set; }
        public decimal IVA { get; set; }
        public decimal IEPS { get; set; }
        public decimal Total { get; set; }
        public string User { get; set; }
        public string Status { get; set; }
        public string Uuid { get; set; }
        public string Type { get; set; }
    }
}
