﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App.Entities.ViewModels.CreditNotes
{
    public class CreditNoteDetailsProducts
    {

        [DisplayName("No. Artículo")]
        public string ItemNumber { get; set; }

        [DisplayName("Artículo")]
        public string ItemName { get; set; }

        [DisplayName("Cantidad")]
        public decimal Quantity { get; set; }

        [DisplayName("Precio Unitario")]
        public decimal ItemPrice { get; set; }

        [DisplayName("Extensión")]
        public decimal TotalItemPrice { get; set; }

        public long? InoviceDetailId { get; set; }
    }
}
