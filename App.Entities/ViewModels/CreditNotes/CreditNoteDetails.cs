﻿using System.Collections.Generic;
using System.ComponentModel;

namespace App.Entities.ViewModels.CreditNotes
{
    public class CreditNoteDetails
    {
        public CreditNoteDetails()
        {
            Products = new List<CreditNoteDetailsProducts>();
        } 

        [DisplayName("Nota de Crédito: ")]
        public string CreditNote { get; set; }

        [DisplayName("Fecha: ")]
        public string IssueDate { get; set; }

        [DisplayName("Total: ")]
        public decimal Total { get; set; }

        [DisplayName("IVA: ")]
        public decimal Tax { get; set; }

        [DisplayName("Factura: ")]
        public string Invoice { get; set; }

        [DisplayName("Cliente: ")]
        public string ClientName { get; set; }
        
        public List<CreditNoteDetailsProducts> Products { get; set; }

        public bool IsBonification { get; set; }

        public string Serie { get; set; }
        public long Number { get; set; }
        public string Uuid { get; set; }
        public string DocumentType { get; set; }
    }
}
