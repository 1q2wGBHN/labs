﻿using App.Entities.ViewModels.Invoices;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using static App.Entities.ViewModels.Common.Constants;

namespace App.Entities.ViewModels.CreditNotes
{
    public class CreditNote
    {
        public CreditNote()
        {
            IssuedDate = DateTime.Now.Date.ToString(DateFormat.INT_DATE);
            NewIssuedDate = DateTime.Now.Date.ToString(DateFormat.INT_DATE);
            IsSaleFromGlobal = false;
            SaleNumber = 0;
            DocumentType = VoucherType.EXPENSES;
            InvoiceDetails = new InvoiceDetails();
            BonificationTaxes = new List<BonificationTax>();
            IsReadOnly = false;
            IsGlobalInvoice = false;
            IsSaleInvoiced = true;
            IsEmployeeBon = false;
            SelectAll = false;
        }

        [DisplayName("Fecha de Aplicación: ")]
        public string IssuedDate { get; set; }

        [DisplayName("Motivo: ")]
        public string Cause { get; set; }

        [DisplayName("Comentarios: ")]
        public string Comments { get; set; }

        [DisplayName("Nota de Crédito No.")]
        public string CreditNoteReference { get; set; }

        public string Serie { get; set; }
        public long Number { get; set; }
        public string User { get; set; }
        public bool IsBonification { get; set; }
        public bool IsSaleFromGlobal { get; set; }
        public long SaleNumber { get; set; }
        public string DocumentType { get; set; }
        public bool IsStamped { get; set; }
        public bool IsReadOnly { get; set; }
        public string PdfEncoded { get; set; }
        public bool CanSelectItems { get; set; }
        public bool IsGlobalInvoice { get; set; }
        public bool IsSaleInvoiced { get; set; }
        public decimal Total { get; set; }
        public string NewIssuedDate { get; set; }
        public bool IsEmployeeBon { get; set; }
        public bool SelectAll { get; set; }
        public long? InvoiceNumber { get; set; }

        [DisplayName("Saldo Factura: ")]
        public decimal InvoiceBalance { get; set; }

        public InvoiceDetails InvoiceDetails { get; set; }
        public List<BonificationTax> BonificationTaxes { get; set; }        
    }
}
