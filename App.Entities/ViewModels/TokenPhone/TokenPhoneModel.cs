﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App.Entities.ViewModels.TokenPhone
{
    public class TokenPhoneModel
    {
        public int id_token { get; set; }
        public string id_phone { get; set; }
        public string token { get; set; }
        public string brand { get; set; }
        public string model { get; set; }
        public string dsk { get; set; }
        public string cuser { get; set; }
        public System.DateTime cdate { get; set; }
    }
}
