﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App.Entities.ViewModels.Site
{
    public class SiteConfigModel
    {
        public string Site { get; set; }
        public string SiteName { get; set; }
        public string SiteAddress { get; set; }
        public string IP { get; set; }
        public string User { get; set; }
        public string pwd { get; set; }
    }
}
