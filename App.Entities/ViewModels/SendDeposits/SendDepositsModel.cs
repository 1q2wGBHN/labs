﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using static App.Entities.ViewModels.Common.Constants;

namespace App.Entities.ViewModels.SendDeposits
{
    public class SendDepositsModel
    {
        public SendDepositsModel()
        {
            SiteCode = GetSiteCode();
            Date = DateTime.Now.ToString(DateFormat.INT_DATE);
            Status = true;
            CDate = DateTime.Now;
            CanEdit = false;            
        }

        [DisplayName("Tipo: ")]
        public string Type { get; set; }

        [DisplayName("Origen: ")]
        public string Origin { get; set; }

        [DisplayName("Monto: ")]
        public decimal Amount { get; set; }

        [DisplayName("Comentarios: ")]
        public string Comments { get; set; }

        [DisplayName("Moneda: ")]
        public string Currency { get; set; }

        [DisplayName("Fecha de Aplicación: ")]
        public string Date { get; set; }        

        public int Id { get; set; }
        public string SiteCode { get; set; }        
        public bool Status { get; set; }
        public string CUser { get; set; }
        public DateTime CDate { get; set; }
        public string ProgramId { get; set; }
        public DateTime AplicationDate { get; set; }

        public bool CanEdit { get; set; }

        public string GetSiteCode()
        {
            var _context = new DFL_SAIEntities();
            var SiteConfig = _context.SITE_CONFIG.FirstOrDefault();

            if (SiteConfig != null)
                return SiteConfig.site_code;

            return string.Empty;
        }

        public SEND_DEPOSITS SEND_DEPOSIT
        {
            get
            {
                return new SEND_DEPOSITS
                {
                    type = Type,
                    origin = Origin,
                    amount = Amount,
                    cur_code = Currency,
                    site_code = SiteCode,
                    comments = Comments,                    
                    cuser = CUser,
                    cdate = CDate,
                    status = Status,
                    program_id = ProgramId,
                    date = AplicationDate
                };
            }
        }
    }    
}
