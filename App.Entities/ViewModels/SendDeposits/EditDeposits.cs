﻿using System;
using System.Collections.Generic;
using System.ComponentModel;

namespace App.Entities.ViewModels.SendDeposits
{
    public class EditDeposits
    {
        public EditDeposits()
        {
            Date = DateTime.Now.Date.ToString("dd/MM/yyyy");
            DepositsList = new List<SendDepositsModel>();
        }

        [DisplayName("Fecha: ")]
        public string Date { get; set; }
        public List<SendDepositsModel> DepositsList { get; set; }
    }
}
