﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App.Entities.ViewModels.ServicesCalendar
{
    public class ServicesCalendarDetailModel
    {
        public int service_id { get; set; }
        public int equipment_id { get; set; }
        public int supplier_id { get; set; }
        public decimal service_cost { get; set; }
        public string quotation_document { get; set; }
        public string service_department { get; set; }
        public string cuser { get; set; }
        public DateTime cdate { get; set; }
        public string uuser { get; set; }
        public DateTime udate { get; set; }
        public string program_id { get; set; }
        public string equipment_serial { get; set; }
        public string equipment_description { get; set; }
        public string supplier_name { get; set; }
        public string service_document { get; set; }
        public DateTime service_date_finished { get; set; }
        public string service_date_finished2 { get; set; }
        public string service_commentary { get; set; }
        public int service_status { get; set; }
        public string currency { get; set; }

    }
}
