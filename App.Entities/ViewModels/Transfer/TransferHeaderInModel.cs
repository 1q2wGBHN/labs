﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App.Entities.ViewModels.Transfer
{
    public class TransferHeaderInModel
    {
        public int Id { get; set; }
        public string Document { get; set; }
        public string SiteName { get; set; }
        public string SiteCode { get; set; }
        public string SiteAdress { get; set; }
    }
}
