﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App.Entities.ViewModels.Transfer
{
    public class TransferCedis
    {
        public string TransferDocument { get; set; }

        public int PalletCount { get; set; }

        public int PalletCountScanned { get; set; }
        public string TransferDate { get; set; }
    }

    public class TransferCedisPallet
    {
        public string PalletSN { get; set; }
        public bool ScanFlag { get; set; }

    }
}
