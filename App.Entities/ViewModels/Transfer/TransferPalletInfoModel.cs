﻿namespace App.Entities.ViewModels.Transfer
{
    public class TransferPalletInfoModel
    {
        public string PalletSn { get; set; }
        public string PartNumber { get; set; }
        public int TransferId { get; set; }
        public decimal Cost { get; set; }
        public decimal Iva { get; set; }
        public decimal Ieps { get; set; }
        public decimal? QuantityP { get; set; }
        public decimal QuantityT { get; set; }
        public int IsDiferent { get; set; }
    }

    public class NewTransferReport
    {
        public int TransferId { get; set; }
        public string PalletSn { get; set; }
        public int TransferStatus { get; set; }
        public string Status { get; set; }
        public string TransferDocument { get; set; }
        public string Site { get; set; }
        public string SiteName { get; set; }
        public decimal Amount { get; set; }
        public decimal Ieps { get; set; }
        public decimal IepsTotal
        {
            get
            {
                return Amount * (Ieps / 100);
            }
        }
        public decimal Iva { get; set; }
        public decimal IvaTotal
        {
            get
            {
                return (Amount + IepsTotal) * (Iva / 100);
            }
        }
        public decimal Total
        {
            get
            {
                return Amount + IvaTotal + IepsTotal;
            }
        }

        public bool ScanFlag { get; set; }
    }
    public class NewTransferDetailReport
    {
        public string PartNumber { get; set; }
        public string PartDescription { get; set; }
        public decimal Quantity { get; set; }
        public decimal GrQuantity { get; set; }
        public decimal Cost { get; set; }
        public decimal Ieps { get; set; }
        public decimal Iva { get; set; }
        public decimal SubTotal
        {
            get
            {
                return Quantity * Cost;
            }
        }
        public decimal IepsTotal
        {
            get
            {
                return SubTotal * (Ieps / 100);
            }
        }
        public decimal IvaTotal
        {
            get
            {
                return (SubTotal + IepsTotal) * (Iva / 100);
            }
        }
        public decimal Total
        {
            get
            {
                return SubTotal + IvaTotal + IepsTotal;
            }
        }
    }
}
