﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App.Entities.ViewModels.Transfer
{
    public class TransferDetailInModel
    {
        public int Id { get; set; }
        public string PartNumber { get; set; }
        public string Description { get; set; }
        public decimal Quantity { get; set; }
        public decimal GRQuantity { get; set; }
        public decimal Cost { get; set; }
        public decimal SubTotal
        {
            get
            {
                return Quantity * Cost;
            }
        }
        public decimal Iva { get; set; }
        public decimal Ieps { get; set; }
        public decimal Total
        {
            get
            {
                return SubTotal + Iva + Ieps;
            }
        }
        public int ScanStatus { get; set; }
    }
}