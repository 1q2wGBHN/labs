﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App.Entities.ViewModels.Transfer
{
    public class TransferDetailModel
    {
        public DateTime? TransferDate { get; set; }
        public string TransferDocument { get; set; }
        public string TransferSiteCode { get; set; }
        public string DestinationSiteCode { get; set; }
        public string DestinationSiteName { get; set; }
        public string DestinationSiteAddress { get; set; }
        public string PartNumber { get; set; }
        public string Description { get; set; }
        public decimal Quantity { get; set; }
        public decimal? GrQuantity { get; set; }
        public int ScanStatus { get; set; }
        public int TransferStatus { get; set; }
        public decimal UnitCost { get; set; }
        public decimal IEPS { get; set; }
        public decimal SubTotal
        {
            get
            {
                return UnitCost * Quantity;
            }
        }
        public decimal Iva { get; set; }
        public decimal Amount
        {
            get
            {
                return SubTotal + (IEPS + Iva) * Quantity;
            }
        }
        public int? transfer_status { get; set; }
        public string transfer_status_desc
        {
            get
            {
                switch (transfer_status)
                {
                    case 0:
                        return "Creado";
                    case 1:
                        return "Aprobado";
                    case 2:
                        return "Rechazado (Tienda origen)";
                    case 4:
                        return "Transito";
                    case 8:
                        return "Cancelado";
                    case 7:
                        return "Solicitud Cancelacion";
                    case 9:
                        return "Completado";
                    default:
                        return transfer_status.ToString();
                }
            }
        }
    }
}