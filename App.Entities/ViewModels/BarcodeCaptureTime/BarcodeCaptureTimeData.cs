﻿namespace App.Entities.ViewModels.BarcodeCaptureTime
{
    public class BarcodeCaptureTimeData
    {
        public string Cashier { get; set; }
        public long Ticket { get; set; }
        public string Date { get; set; }
        public string Time { get; set; }
        public string CheckoutNo { get; set; }
        public string CaptureCode { get; set; }
        public string ItemNumber { get; set; }
        public string ItemDescription { get; set; }
        public string Department { get; set; }
    }
}
