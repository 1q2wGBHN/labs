﻿using System;
using System.Collections.Generic;
using System.ComponentModel;

namespace App.Entities.ViewModels.BarcodeCaptureTime
{
    public class BarcodeCaptureTimeIndex
    {
        public BarcodeCaptureTimeIndex()
        {
            StartDate = DateTime.Now.ToString("dd/MM/yyyy");
            EndDate = DateTime.Now.ToString("dd/MM/yyyy");
            IsFromMenu = true;
            DataInfo = new BarcodeCaptureTimeInfo();
        }

        [DisplayName("Fecha Inicial: ")]
        public string StartDate { get; set; }

        [DisplayName("Fecha Final: ")]
        public string EndDate { get; set; }

        public bool IsFromMenu { get; set; }

        public string SiteName { get; set; }

        public BarcodeCaptureTimeInfo DataInfo { get; set; }
    }
}
