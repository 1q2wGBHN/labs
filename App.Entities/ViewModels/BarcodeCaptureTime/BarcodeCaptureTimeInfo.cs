﻿using System.Collections.Generic;

namespace App.Entities.ViewModels.BarcodeCaptureTime
{
    public class BarcodeCaptureTimeInfo
    {
        public BarcodeCaptureTimeInfo()
        {
            ScannedTotal = new List<BarcodeCaptureTimeTotalScanned>();
            IssuesCount = new List<BarcodeCaptureTimeIssues>();
            MainData = new List<BarcodeCaptureTimeData>();
        }
        public List<BarcodeCaptureTimeTotalScanned> ScannedTotal { get; set; }
        public List<BarcodeCaptureTimeIssues> IssuesCount { get; set; }
        public List<BarcodeCaptureTimeData> MainData { get; set; }
    }
}
