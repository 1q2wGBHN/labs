﻿namespace App.Entities.ViewModels.BarcodeCaptureTime
{
    public class BarcodeCaptureTimeTotalScanned
    {
        public string Cashier { get; set; }
        public int Scanned { get; set; }
        public int NotScanned { get; set; }
        public decimal Percentage { get; set; }
    }
}
