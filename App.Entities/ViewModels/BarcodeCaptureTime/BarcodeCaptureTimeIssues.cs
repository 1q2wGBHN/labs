﻿namespace App.Entities.ViewModels.BarcodeCaptureTime
{
    public class BarcodeCaptureTimeIssues
    {
        public string ItemNumber { get; set; }
        public string ItemDescription { get; set; }
        public string Department { get; set; }
        public int Issues { get; set; }
    }
}
