﻿using System;

namespace App.Entities.ViewModels.Sales
{
    public class GiftCardData
    {
        public int CardId { get; set; }
        public string CardNumber { get; set; }
        public decimal Amount { get; set; }
        public string Date { get; set; }
        public string StoreMarket { get; set; }
        public string SaleCheckpoint { get; set; }
        public string SaleId { get; set; }
        public string StatusType { get; set; }
        public string SiteCode { get; set; }
    }
}
