﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App.Entities.ViewModels.Sales
{
    public class WeighingIndex
    {
        [DisplayName("Fecha Inicial: ")]
        public string Date { get; set; }
        [DisplayName("Fecha Final: ")]
        public string Date2 { get; set; }
        [DisplayName("Diferencia Mínima: ")]
        public decimal Difer { get; set; }
        public List<WeighingReport> WeighingReportData { get; set; }
    }
}
