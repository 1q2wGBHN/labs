﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using static App.Entities.ViewModels.Common.Constants;

namespace App.Entities.ViewModels.Sales
{
    public class GiftCardReportIndex
    {
        public GiftCardReportIndex()
        {
            StartDate = DateTime.Now.ToString(DateFormat.INT_DATE);
            EndDate = DateTime.Now.ToString(DateFormat.INT_DATE);
            IsFromMenu = true;
            SiteCode = GetSiteCode();
            DataReport = new List<GiftCardData>();
            IsCharge = true;
        }        
        [DisplayName("Fecha Inicial: ")]
        public string StartDate { get; set; }
        
        [DisplayName("Fecha Final: ")]
        public string EndDate { get; set; }

        [DisplayName("Tipo De Reporte: ")]
        public ReportType ReportType { get; set; }

        public string SiteCode { get; set; }
        public bool IsFromMenu { get; set; }
        public bool IsCharge { get; set; }

        public List<GiftCardData> DataReport { get; set; }        

        public string GetSiteCode()
        {
            var _context = new DFL_SAIEntities();
            var SiteConfig = _context.SITE_CONFIG.FirstOrDefault();

            if (SiteConfig != null)
                return SiteConfig.site_code;

            return string.Empty;
        }
    }

    public enum ReportType
    {
        [Display(Name = "Tarjetas Activadas")]
        ActiveCards,

        [Display(Name = "Cargos")]
        Charges        
    }
}
