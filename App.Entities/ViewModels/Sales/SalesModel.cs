﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;

namespace App.Entities.ViewModels.Sales
{
    public class SalesModel
    {
        public SalesModel()
        {
          
        }
        [DisplayName("# de Venta: ")]
        public long sale_id{ get; set; }

        [DisplayName("Fecha de Venta: ")]
        public string sale_date{ get; set; }
        [DisplayName("Hora de Venta: ")]
        public string sale_time{ get; set; }
        [DisplayName("Total: ")]
        public decimal sale_total{ get; set; }
        [DisplayName("IVA: ")]
        public decimal sale_iva{ get; set; }

        public decimal sale_ieps { get; set; }
        [DisplayName("FACTURA: ")]
        public string sale_factura { get; set; }
        public decimal sale_importe{ get; set; }
        public string tipo { get; set; }
        public string customer { get; set; }

    }

    //dollar sales model
    public class DollarSalesModel
    {
        public DollarSalesModel()
        {

        }
       
        public long sale_id { get; set; }        
        public string sale_date { get; set; }      
        public string sale_time { get; set; }        
        public decimal recibido { get; set; }     
        public decimal cobrado { get; set; }
        public decimal tipoCambio { get; set; }
        public string usuario { get; set; }

    }
    //VENTAS IEPS
    public class SalesIEPS
    {
        public SalesIEPS()
        {
           
        }

        public string saleDate { get; set; }
        public string departamento { get; set; }
        public string codigo { get; set; }
        public string descripcion { get; set; }
        public string familia { get; set; }
        public string tasa_ieps { get; set; }
        public decimal venta { get; set; }
        public decimal importe_ieps { get; set; }
        public decimal iva { get; set; }
        public decimal total { get; set; }

    }
}
