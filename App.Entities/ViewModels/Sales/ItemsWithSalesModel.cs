﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App.Entities.ViewModels.Sales
{
    public class ItemsWithSalesModel
    {
        public string part_number { get; set; }
        public string part_description { get; set; }
     
        public decimal quantity { get; set; }
        public decimal iva { get; set; }
      
        public decimal mount { get; set; }
        public string supplier { get; set; }
        public string department { get; set; }


    }
}
