﻿using App.Entities.ViewModels.Sanctions;
using App.Entities.ViewModels.SendDeposits;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App.Entities.ViewModels.Sales
{
    public class PortadaModel
    {
        public PortadaModel()
        {

        }
        public decimal Difference { get; set; }
        public string ReportDate { get; set; }
        public decimal Currency_Rate { get; set; }
        public decimal[,] TotalesMXN { get; set; }
        public decimal[,] TotalesDLLS { get; set; }

        public decimal totalTasao { get; set; }
        public decimal totalGravado0 { get; set; }
        public decimal totalExento { get; set; }
        public decimal totalIEPS0 { get; set; }
        public decimal totalIEPS { get; set; }
        public decimal totalIVA { get; set; }
        public decimal totalTOTAL { get; set; }


        public decimal CreditNotesMXN { get; set; }
        public decimal CreditNotesDLLS { get; set; }

        public decimal BONIMXN { get; set; }
        public decimal BONIDLLS { get; set; }

        public decimal DESCMXN { get; set; }
        public decimal DESCDLLS { get; set; }

        public decimal PAYSCUSTMXN { get; set; }
        public decimal PAYSCUSTDLLS { get; set; }
        public decimal PAYSCUSTDLLSMXN { get; set; }
        public decimal PAYSCUSTTOTAL { get; set; }

        public decimal COMISMXN { get; set; }
        public decimal COMIS_IVA { get; set; }
        public decimal COMIS_DLLS_MXN { get; set; }
        public decimal COMIS_TOTAL { get; set; }

        public decimal _cancTarjetas_0 { get; set; }

        public decimal _cancTarjetaC { get; set; }
        public decimal _cancTarjetaD { get; set; }
        public decimal RENTA_MXN { get; set; }
        public decimal RENTA_IVA { get; set; }
        public decimal RENTA_DLLS { get; set; }
        public decimal RENTA_TOTAL { get; set; }

        public List<SanctionsObj> Sanctions { get; set; }
        public List<Deposits> Deposits { get; set; }

        public decimal MXNDeposits { get; set; }
        public decimal DLLSDeposits { get; set; }
        public decimal CARDSDeposits { get; set; }
        public decimal VALESDeposits { get; set; }
        public decimal VALES_ELECT_Deposits { get; set; }
        public decimal TotalDeposits { get; set; }

        public decimal DON_MXN { get; set; }
        public decimal DON_DLLS { get; set; }
        public decimal CANC_ELECT { get; set; }
        public decimal CANC_CASH { get; set; }
        public decimal CASHBACK_TOTAL { get; set; }

        public List<PaymentMethod> PaymentsMethodsList { get; set; }

        public List<ServicesDetail> ServicesDetailList { get; set; }

        public List<SaleDiscounts> SaleDiscountslList { get; set; }
        public List<IEPS_Detail> IEPS_DetaillList { get; set; }
    }
    public class Deposits
    {
        public Deposits() { }
        public string Destino { get; set; }
        public decimal Importe { get; set; }
        public string Type { get; set; }
        public string Currency { get; set; }
        public decimal ImporteDLLS { get; set; }
        public decimal Importe_DLLS_MXN { get; set; }
        public decimal Complement { get; set; }


    }
    public class SanctionsObj
    {
        public string Empleado { get; set; }
        public string Motivo { get; set; }
        public decimal Importe { get; set; }
       
    }
    public class PaymentMethod
    {
        public string PaymentName { get; set; }
        public string Typev { get; set; }
        public decimal Importe { get; set; }
        public int PMid { get; set; }
    }

    public class ServicesDetail
    {
        public string ServiceName { get; set; }
       
        public decimal Importe { get; set; }
    }
    public class SaleDiscounts
    {
        public string Customername { get; set; }
        public decimal Importe { get; set; }
    }

    public class IEPS_Detail
    {
        public string IEPS { get; set; }
        public decimal Importe { get; set; }
    }
    public class Cancellations
    {
        public string Type { get; set; }
        public decimal Total { get; set; }
        public bool CardC { get; set; }
        public bool CardD { get; set; }
    }
    public class Rent
    {
        public string Currency { get; set; }
        public decimal Impporte { get; set; }
        public decimal Tax { get; set; }
    }
}
