﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App.Entities.ViewModels.Sales
{
    public class ItemsWithOutSalesModel
    {
        public string part_number { get; set; }
        public string part_description { get; set; }
        public decimal total_stock { get; set; }
        public string fecha { get; set; }
        public decimal quantity { get; set; }

      
    }
}
