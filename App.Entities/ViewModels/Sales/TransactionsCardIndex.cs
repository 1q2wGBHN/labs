﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using App.Entities.ViewModels.Currency;
using System.ComponentModel;

namespace App.Entities.ViewModels.Sales
{
    
    public class TransactionsCardIndex
    {
        public TransactionsCardIndex()
        {
            StartDate = DateTime.Now.ToString("dd/MM/yyyy");
            EndDate = DateTime.Now.ToString("dd/MM/yyyy");
            IsFromMenu = true;
            DataReport = new List<TransactionsCardData>();
            CashierReport = new List<TransactionsCardByCashier>();
        }

        [DisplayName("Fecha Inicial: ")]
        public string StartDate { get; set; }

        [DisplayName("Fecha Final: ")]
        public string EndDate { get; set; }

        [DisplayName("Cajero: ")]
        public string Cashier { get; set; }
        
        public bool IsFromMenu { get; set; }

        public string CashierName { get; set; }

        public List<TransactionsCardData> DataReport { get; set; }
        public List<TransactionsCardByCashier> CashierReport { get; set; }

    }
}
