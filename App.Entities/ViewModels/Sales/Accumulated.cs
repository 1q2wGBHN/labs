﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App.Entities.ViewModels.Sales
{
    public class Accumulated
    {
        public string Cashier { get; set; }
        public decimal Total { get; set; }
        public string Date { get; set; }

        public string Color { get; set; }
    }
}
