﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App.Entities.ViewModels.Sales
{
    public class WeighingReport
    {
        public string Codigo { get; set; }
        public string Descripcion { get; set; }
        public string Ingresado { get; set; }
        public string Bascula{ get; set; }
        public decimal Diferencia { get; set; }
        public string Fecha { get; set; }
        public long Venta { get; set; }
        public string Cajero { get; set; }
    }
}
