﻿namespace App.Entities.ViewModels.Sales
{
    public class SalesCancellationData
    {
        public long SaleNumber { get; set; }
        public long CancellationNumber { get; set; }
        public string ItemDescription { get; set; }
        public decimal Sales { get; set; }
        public decimal SalesCancelated { get; set; }
        public decimal UnitPrice { get; set; }
        public decimal SubTotal { get; set; }        
        public decimal Tax { get; set; }
        public decimal Total { get; set; }
        public string Cashier { get; set; } 
        public string Supervisor { get; set; }
        public string Motive { get; set; }
        public int PaymentMethod { get; set; }
        public bool IsCredit { get; set; }
    }
}
