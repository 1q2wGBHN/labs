﻿using System;
using System.Collections.Generic;
using System.ComponentModel;

namespace App.Entities.ViewModels.Sales
{
    public class ChargeRelationSalesIndex
    {
        public ChargeRelationSalesIndex()
        {
            IsFromMenu = true;
            Date = DateTime.Now.Date.ToString("dd/MM/yyyy");
            EndDate = DateTime.Now.Date.ToString("dd/MM/yyyy");
            CurrencyRate = 0;
            DataReport = new List<ChargeSalesRelationInfo>();
        }

        public bool IsFromMenu { get; set; }

        [DisplayName("Cajero: ")]
        public string Cashier { get; set; }

        [DisplayName("Fecha Inicial: ")]
        public string Date { get; set; }

        [DisplayName("Fecha Final:")]
        public string EndDate { get; set; }

        [DisplayName("Tipo de Pago:")]
        public int? PaymentMethod { get; set; }

        public List<ChargeSalesRelationInfo> DataReport { get; set; }

        public decimal CurrencyRate { get; set; }

        public string SiteName { get; set; }
    }
}
