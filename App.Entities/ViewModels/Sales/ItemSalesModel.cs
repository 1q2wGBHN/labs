﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App.Entities.ViewModels.Sales
{
    public class ItemSalesModel
    {
        public string PartNumber { get; set; }
        public string PartDescription { get; set; }
        public string Cashier { get; set; }
        public decimal Quantity { get; set; }
        public decimal Price { get; set; }
        public decimal Amount { get; set; }
        public decimal IVA { get; set; }
        public decimal IEPS { get; set; }
        public decimal Total { get; set; }
    }
}