﻿using System;
using System.ComponentModel;

namespace App.Entities.ViewModels.Sales
{
    public class ChargeSalesRelationInfo
    {
        [DisplayName("Cajero: ")]
        public string Cashier { get; set; }

        [DisplayName("Monto: ")]
        public decimal Amount { get; set; }

        [DisplayName("Tipo de Pago: ")]
        public string PaymentType { get; set; }

        [DisplayName("Número de Empleado: ")]
        public string EmployeeNo { get; set; }

        [DisplayName("Fecha: ")]
        public DateTime Date { get; set; }

        public string Currency { get; set; }
    }
}
