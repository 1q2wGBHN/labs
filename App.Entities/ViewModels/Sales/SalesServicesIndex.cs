﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App.Entities.ViewModels.Sales
{
    public class SalesServicesIndex
    {

        public SalesServicesIndex() { }

        [DisplayName("Entre")]
        public string StartDate { get; set; }

        [DisplayName("Y")]
        public string EndDate { get; set; }

        [DisplayName("Tipo")]
        public string Type { get; set; }

        [DisplayName("Compañia")]
        public string Company { get; set; }

        public List<ServicesModel> Services { get; set; }

        public List<RecargasModel> Recargas { get; set; }
    }
    public class serviciosRecargas
    {
        public serviciosRecargas()
        {
            Services = new List<ServicesModel>();
            Recargas = new List<RecargasModel>();
            ServicesDetail = new List<ServicesModelDetail>();
            RecargasDetail = new List<RecargasModelDetail>();
            CompaniasList = new List<Companias>();
        }
        public List<ServicesModel> Services { get; set; }

        public List<RecargasModel> Recargas { get; set; }
        public List<ServicesModelDetail> ServicesDetail { get; set; }

        public List<RecargasModelDetail> RecargasDetail { get; set; }
        public List<Companias> CompaniasList { get; set; }

    

    }
    public class ServicesModel
    {
        public ServicesModel() { }

     
        public string company { get; set; }
        public decimal comision { get; set; }
        public decimal IVAcomision { get; set; }
        public decimal TotalComision { get; set; }
        public decimal PagoServicio { get; set; }
        public List<ServicesModelDetail> ServicesDetail { get; set; }


    }
    public class ServicesModelDetail
    {
        public ServicesModelDetail() { } 
        public string fecha { get; set; }
        public string hora { get; set; }
        public Int64 venta { get; set; }
        public decimal comision { get; set; }
        public decimal IVAcomision { get; set; }
        public decimal TotalComision { get; set; }
        public decimal PagoServicio { get; set; }

    }
    public class RecargasModel
    {
        public RecargasModel() { }
       
        public string company { get; set; }
        public decimal total { get; set; }
        public List<RecargasModelDetail> RecargasDetail { get; set; }


    }
    public class RecargasModelDetail
    {
        public RecargasModelDetail() { }
        public string fecha { get; set; }
        public string hora { get; set; }
        public Int64 venta { get; set; }
        public decimal total { get; set; }

    }
    public class Companias
    {
        public Companias() { }
        public string company { get; set; }
      

    }
}
