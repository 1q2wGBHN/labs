﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;
using App.Entities.ViewModels.CreditNotes;

namespace App.Entities.ViewModels.Sales
{
    public class SalesConcIndex
    {
        public SalesConcIndex()
        {
        }
           
        [DisplayName("Entre")]
        public string StartDate { get; set; }

        [DisplayName("Y")]
        public string EndDate { get; set; }
        [DisplayName("Moneda:")]
        public CurrencysCoonc Currency { get; set; }       
        public List<SalesModelConc> Sales { get; set; }
    }
}
