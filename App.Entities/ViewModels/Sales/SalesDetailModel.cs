﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App.Entities.ViewModels.Sales
{
    public class SalesDetailModel
    {
        public int Id { get; set; }
        public string PartNumber { get; set; }
        public decimal AverageSale { get; set; }
        public string Barcode { get; set; }
    }
}
