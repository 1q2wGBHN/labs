﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App.Entities.ViewModels.Sales
{
    public class SalesModelConc
    {
        public SalesModelConc()
        {
        }
        public string fecha { get; set; }

        public decimal cn_subtotal { get; set; }
        public decimal cn_iva { get; set; }
        public decimal cn_ieps { get; set; }
        public decimal cn_total { get; set; }

        public decimal bon_subtotal { get; set; }
        public decimal bon_iva { get; set; }
        public decimal bon_ieps { get; set; }
        public decimal bon_total { get; set; }

        public decimal fc_subtotal { get; set; }
        public decimal fc_iva { get; set; }
        public decimal fc_ieps { get; set; }
        public decimal fc_total { get; set; }

        public decimal dv_total { get; set; }
        public decimal cu_total { get; set; }

        public decimal fac_subtotal { get; set; }
        public decimal fac_iva { get; set; }
        public decimal fac_ieps { get; set; }
        public decimal fac_total { get; set; }

        public decimal gt_subtotal { get; set; }
        public decimal gt_iva { get; set; }
        public decimal gt_ieps { get; set; }
        public decimal gt_total { get; set; }


    }
}
