﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App.Entities.ViewModels.Sales
{
    public class CancellationResume

    {
        public string Cashier { get; set; }
        public string CashierId { get; set; }
        public decimal Amount { get; set; }
        public int NumberCancellations { get; set; }
    }
}
