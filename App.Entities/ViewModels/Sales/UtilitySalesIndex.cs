﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App.Entities.ViewModels.Sales
{
    public class UtilitySalesIndex
    {
        public UtilitySalesIndex()
        {
            StartDate = DateTime.Now.AddDays(-1).ToString("dd/MM/yyyy");
            EndDate = DateTime.Now.ToString("dd/MM/yyyy");
            IsFromMenu = true;
            UtilitySales = new List<Sales.UtilitySales>();
        }
        public bool IsFromMenu { get; set; }

        [DisplayName("Departamento: ")]
        public int Department { get; set; }

        [DisplayName("Familia: ")]
        public int Family { get; set; }

        [DisplayName("Artículo: ")]
        public string PartNumber { get; set; }

        [DisplayName("Fecha Inicial: ")]
        public string  StartDate { get; set; }

        [DisplayName("Fecha Final: ")]
        public string EndDate { get; set; }

        [DisplayName("Concentrado Por Departamento")]
        public bool GroupByDepartment { get; set; }

        public List<UtilitySales> UtilitySales { get; set; }
    }
}
