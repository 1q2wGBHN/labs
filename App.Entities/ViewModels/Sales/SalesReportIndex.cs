﻿using System;
using System.Collections.Generic;
using System.ComponentModel;

namespace App.Entities.ViewModels.Sales
{
    public class SalesReportIndex
    {
        public SalesReportIndex()
        {
            IsFromMenu = true;
            Date = DateTime.Now.Date.ToString("dd/MM/yyyy");
            CancellationsReport = new List<SalesCancellationData>();
            AccumulatedData = new List<Accumulated>();
        }
        public bool IsFromMenu { get; set; }

        [DisplayName("Cajero: ")]
        public string Cashier { get; set; }

        [DisplayName("Fecha: ")]
        public string Date { get; set; }

        public string SiteName { get; set; }

        public List<SalesCancellationData> CancellationsReport { get; set; }

        public List<Accumulated> AccumulatedData { get; set; }
    }
}
