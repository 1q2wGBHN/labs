﻿namespace App.Entities.ViewModels.Sales
{
    public class AverageCustomersByHour
    {
        public string Day { get; set; }
        public int Hr6 { get; set; }
        public int Hr7 { get; set; }
        public int Hr8 { get; set; }
        public int Hr9 { get; set; }
        public int Hr10 { get; set; }
        public int Hr11 { get; set; }
        public int Hr12 { get; set; }
        public int Hr13 { get; set; }
        public int Hr14 { get; set; }
        public int Hr15 { get; set; }
        public int Hr16 { get; set; }
        public int Hr17 { get; set; }
        public int Hr18 { get; set; }
        public int Hr19 { get; set; }
        public int Hr20 { get; set; }
        public int Hr21 { get; set; }
        public int Hr22 { get; set; }
        public int Hr23 { get; set; }        
    }
}
