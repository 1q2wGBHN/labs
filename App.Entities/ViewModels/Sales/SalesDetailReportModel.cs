﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel;

namespace App.Entities.ViewModels.Sales
{
     public class SalesDetailReportModel
    {
        public SalesDetailReportModel()
        {
       
        }
        public string part_number { get; set; }
        public string part_name { get; set; }
        public decimal part_quantity { get; set; }
        public decimal part_price { get; set; }
        public decimal part_importe { get; set; }
        public decimal part_iva { get; set; }
        public decimal part_iva_percentage { get; set; }
        public decimal part_ieps { get; set; }
        public decimal part_total { get; set; }
        public decimal part_ieps_percentage { get; set; }
    }
}
