﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App.Entities.ViewModels.Sales
{
    public class TransactionsCardData
    {
        public int TransactionsCardId { get; set; }
        public DateTime? Fecha { get; set; }
        public decimal? Monto { get; set; }
        public decimal? Retiro { get; set; }
        public string Cashier { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public decimal? Total { get; set; }
    }
}
