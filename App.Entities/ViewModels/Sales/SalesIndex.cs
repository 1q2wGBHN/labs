﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;
using App.Entities.ViewModels.Sales;
using System.ComponentModel.DataAnnotations;

namespace App.Entities.ViewModels.Sales
{
    public class SalesIndex
    {
        public SalesIndex()
        {
            SubstractIEPS = true;
            IsFromMenu = true;
            Search = SearchType.Department;
            GroupByItem = false;
        }

        [DisplayName("Entre")]
        public string StartDate { get; set; }

        [DisplayName("Y")]
        public string EndDate { get; set; }

        [DisplayName("Importe")]
        public decimal Importe { get; set; }

        [DisplayName("Caja")]
        public string caja { get; set; }

        [DisplayName("Tipo")]
        public SalesType Type { get; set; }

        [DisplayName("Estado")]
        public SalesStatus Status { get; set; }

        public SearchType Search { get; set; }

        [DisplayName("Restar IEPS de Notas De Crédito")]
        public bool SubstractIEPS { get; set; }

        [DisplayName("Cajero")]
        public string Cashier { get; set; }

        public bool IsFromMenu { get; set; }

        public decimal IVA { get; set; }
        public decimal IEPS { get; set; }
        public decimal Total { get; set; }        
        public List<SalesModel> Sales { get; set; }
        public List<DollarSalesModel> SalesDlls { get; set; }        
        public List<CustomerSalesViewModel> SalesByHour { get; set; }             
        public SalesIEPSModel IEPSReport { get; set; }

        [DisplayName("Agrupar por artículos")]
        public bool GroupByItem { get; set; }
    }
    public enum SalesType
    {
        [Display(Name = "TODOS")]
        All = 0,
        [Display(Name = "Crédito")]
        Credit = 1,

        [Display(Name = "Contado")]
        Cash = 2
    }

    public enum SearchType
    {
        [Display(Name = "Departamento")]
        Department = 1,

        [Display(Name = "Familia")]
        Family = 2,

        [Display(Name = "Artículo")]
        Item = 3
    }

    public enum SalesStatus
    {
        [Display(Name = "TODOS")]
        All,
        [Display(Name = "Facturada")]
        Fact,

        [Display(Name = "Sín Facturar")]
        SinFact
    }
    public class SalesIEPSModel
    {
        public SalesIEPSModel()
        {
            SalesIepsTax = new List<SalesIEPS>();
            SalesIepsTaxZero = new List<SalesIEPS>();
        }
        public List<SalesIEPS> SalesIepsTax { get; set; }
        public List<SalesIEPS> SalesIepsTaxZero { get; set; }
    }
}
