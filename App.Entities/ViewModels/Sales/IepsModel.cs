﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App.Entities.ViewModels.Sales
{
    public class IepsModel
    {
        public decimal  price { get; set; }
        public decimal value { get; set; }
        public long sale_id { get; set; }
        public string serie { get; set; }
        public string nameIeps { get; set; }
        public decimal tax { get; set; }
        public bool iscredit { get; set; }
    }
}
