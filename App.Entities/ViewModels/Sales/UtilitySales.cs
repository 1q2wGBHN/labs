﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App.Entities.ViewModels.Sales
{
    public class UtilitySales
    {        
        public string part_number { get; set; }
        public string part_description { get; set; }
        public int grupo_id { get; set; }
        public string department { get; set; }
        public int level1_code { get; set; }
        public decimal QTY { get; set; }
        public decimal COSTO_STD { get; set; }
        //public decimal part_lastCost { get; set; }
        //public string part_lastCost_cur { get; set; }
        public decimal VENTA { get; set; }
        public decimal IEPS { get; set; }
        public decimal IVA { get; set; }

    }
}
