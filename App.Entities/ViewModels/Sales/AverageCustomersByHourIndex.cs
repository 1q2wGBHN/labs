﻿using System;
using System.Collections.Generic;
using System.ComponentModel;

namespace App.Entities.ViewModels.Sales
{
    public class AverageCustomersByHourIndex
    {
        public AverageCustomersByHourIndex()
        {
            Date = DateTime.Now.ToString("dd/MM/yyyy");
            IsFromMenu = true;
            DataList = new List<AverageCustomersByHour>();
        }

        [DisplayName("Fecha: ")]
        public string Date { get; set; }

        public bool IsFromMenu { get; set; }

        public string SiteName { get; set; }
        public List<AverageCustomersByHour> DataList { get; set; }
    }
}
