﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App.Entities.ViewModels.Sales
{
    public class TransactionsCardByCashier
    {
        public int TransactionsCardId { get; set; }
        public DateTime? Fecha { get; set; }
        public string FechaS { get; set; }
        public string HoraS { get; set; }
        public decimal? Amount { get; set; }
        public string CardNumber { get; set; }
        public string AuthCode { get; set; }
        public string FolioVenta { get; set; }
        public string PosId { get; set; }
        public string NombreCajero { get; set; }
        public decimal? CashBack { get; set; }
        public string EmpNo { get; set; }
        public decimal? Total { get; set; }
        public long SaleId { get; set; }
        public string SaleType { get; set; }
        public string PaymentMethod { get; set; }
        public string ResponseCode { get; set; }
        public string ResponseMessage { get; set; }
    }
}
