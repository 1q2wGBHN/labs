﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App.Entities.ViewModels.Sales
{
    public class SalesPriceChangeModel
    {
        public string partNumber { get; set; }

        public DateTime appliedDate { get; set; }

        public decimal margin { get; set; }

        public decimal priceUser { get; set; }
    }
}
