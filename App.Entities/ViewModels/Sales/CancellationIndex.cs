﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App.Entities.ViewModels.Sales
{
    public class CancellationIndex
    {
        [DisplayName("Fecha Inicial: ")]
        public string Date { get; set; }
        [DisplayName("Fecha Final: ")]
        public string Date2 { get; set; }

        public string SiteName { get; set; }

        public List<CancellationResume> CancellationsData { get; set; }
    }
}
