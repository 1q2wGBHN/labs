﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App.Entities.ViewModels.User
{
    public class UserModelFirst
    {
        public string EmpNo { get; set; }
        public string UserName { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public string mobile_tel { get; set; }
        public string office_tel { get; set; }
    }
}
