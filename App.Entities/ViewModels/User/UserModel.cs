﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App.Entities.ViewModels.User
{
    public class UserModel
    {
        public string EmpNo { get; set; }
        public string UserName { get; set; }
        public string FirstName { get; set; }
        public  string LastName { get; set; }
        public string Email { get; set; }

        public UserModel(USER_MASTER user)
        {
            EmpNo = user.emp_no;
            UserName = user.user_name;
            FirstName = user.first_name;
            LastName = user.last_name;
            Email = user.email;
        }
    }
}
