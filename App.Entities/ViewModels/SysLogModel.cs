﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App.Entities.ViewModels
{
    public class SysLogModel
    {
        public string TableName { get; set; }
        public string RelatedId { get; set; }
        public string Action { get; set; }
        public string Description { get; set; }
        public string User { get; set; }
        public DateTime Date { get; set; }
        public string Time { get; set; }
        public string Menu { get; set; }        
    }
}
