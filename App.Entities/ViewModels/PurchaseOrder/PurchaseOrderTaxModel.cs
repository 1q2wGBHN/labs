﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App.Entities.ViewModels.PurchaseOrder
{
    public class PurchaseOrderTaxModel
    {
        public decimal Ieps { get; set; }
        public decimal Iva { get; set; }
    }
}
