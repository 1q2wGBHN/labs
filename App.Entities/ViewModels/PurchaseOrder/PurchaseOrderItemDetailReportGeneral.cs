﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App.Entities.ViewModels.PurchaseOrder
{
    public class PurchaseOrderItemDetailReportGeneral
    {
        public string Department { get; set; }
        public string Family { get; set; }
        public string PartNumber { get; set; }
        public string UnitSize { get; set; }
        public string DescriptionC { get; set; }
        public decimal Quantity { get; set; }
        public decimal CostUnit { get; set; }
        public decimal SubTotal { get; set; }
        public decimal IVA { get; set; }
        public decimal IEPS { get; set; }
        public decimal Amount { get; set; }
        public string Supplier { get; set; }
        public string Folio { get; set; }
        public string MerchandiseEntry { get; set; }
        public string MerchandiseEntryEta { get; set; }
        public string Item_Status { get; set; }
        public string Currency { get; set; }
        public string TransationType { get; set; }
    }
}
