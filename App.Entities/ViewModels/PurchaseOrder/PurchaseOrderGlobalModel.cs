﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App.Entities.ViewModels.PurchaseOrder
{
    public class PurchaseOrderGlobalModel
    {
        //PURCHASE_ORDER
        public int purchase_no { get; set; }
        public int supplier_id { get; set; }
        public string supplier { get; set; }
        public string purchase_date { get; set; }
        public int purchase_status { get; set; }
        public int folio_requierement { get; set; }
        public string xml { get; set; }
        public string xml_name { get; set; }
        //PURCHASE_ORDER_ITEM
        public string part_number { get; set; }
        public string part_description { get; set; }
        public decimal quantity { get; set; }
        public decimal purchase_price { get; set; }
        public decimal? item_total_amount { get; set; }

    }
}
