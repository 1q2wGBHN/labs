﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App.Entities.ViewModels.PurchaseOrder
{
    public class PurchaseOrderItemOCModel
    {
        public string CommercialName { get; set; }
        public string PurchaseDate { get; set; }
        public string Adress { get; set; }
        public string Email { get; set; }
        public string Phone { get; set; }
        public string Currency { get; set; }
        public string NameContact { get; set; }
        public string RFC { get; set; }
        public string InvoiceNo { get; set; }
        public List<PurchaseOrderItemListModel> PurchaseOrderList { get; set; }
    }
}
