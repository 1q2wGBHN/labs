﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App.Entities.ViewModels.PurchaseOrder
{
    public class PurchaseOrderRCModel
    {
        public string PurchaseNo { get; set; }
        public string SiteCode { get; set; }
        public int SupplierId { get; set; }
    }
}
