﻿using App.Entities.ViewModels.Site;
using App.Entities.ViewModels.Supplier;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App.Entities.ViewModels.PurchaseOrder
{
    public class PurchaseOrderDepartmentModel
    {
        public SiteModel site { get; set; }
        public List<PurchaseOrderViewModel> Items { get; set; }
        public List<MA_CLASS> Departaments { get; set; }
        public List<MaSupplierModel> Suppliers { get; set; }

        public PurchaseOrderDepartmentModel()
        {
            Items = new List<PurchaseOrderViewModel>();
            Departaments = new List<MA_CLASS>();
            Suppliers = new List<MaSupplierModel>();
        }
    }

    public class PurchaseOrderViewModel
    {
        public int PartNumber { get; set; }
        public string Description { get; set; }
        public decimal Quantity { get; set; }
        public decimal Amount { get; set; }
        public decimal Ieps { get; set; }
        public decimal TotalAmount { get; set; }
        public string Department { get; set; }
        public string Family { get; set; }
    }
}