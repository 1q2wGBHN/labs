﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App.Entities.ViewModels.PurchaseOrder
{
    public class PurchaseOrderItemModel
    {
        public string PurchaseNo { get; set; }
        public string ParthNumber { get; set; }
        public decimal QuantityOC { get; set; }
        public decimal QuantityRC { get; set; }
        public string Description { get; set; }
        public int Id { get; set; }
    }
}
