﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App.Entities.ViewModels.Quotes
{
    public class QuoteReport
    {
        public string Id { get; set; }
        public string Customer { get; set; }
        public decimal Total { get; set; }
        public decimal Tax { get; set; }
        public string Date { get; set; }
        public string Serie { get; set; }
        public long Number { get; set; }
    }
}
