﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using static App.Entities.ViewModels.Common.Constants;

namespace App.Entities.ViewModels.Quotes
{
    public class QuoteIndex
    {
        public QuoteIndex()
        {
            StartDate = DateTime.Now.ToString(DateFormat.INT_DATE);
            EndDate = DateTime.Now.ToString(DateFormat.INT_DATE);
            IsFromMenu = true;
            DataReport = new List<QuoteReport>();
        }

        [DisplayName("Fecha Inicial: ")]
        public string StartDate { get; set; }

        [DisplayName("Fecha Final: ")]
        public string EndDate { get; set; }

        [DisplayName("Cliente: ")]
        public string ClientCode { get; set; }

        public bool IsFromMenu { get; set; }
        public List<QuoteReport> DataReport { get; set; }
    }
}
