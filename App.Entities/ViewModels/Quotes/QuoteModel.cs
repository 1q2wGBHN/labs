﻿using App.Entities.ViewModels.Customers;
using App.Entities.ViewModels.Item;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data.SqlClient;
using System.Linq;

namespace App.Entities.ViewModels.Quotes
{
    public class QuoteModel
    {
        private DFL_SAIEntities _context;
        public QuoteModel()
        {
            IsFromMenu = true;
            SiteCode = GetSiteCode();
            ItemsList = new List<ItemQuotation>();
            CustomersList = new List<CustomersDDL>();
            HasSerieAndNumber = true;              
        }

        [DisplayName("Vendedor:")]
        public string Seller { get; set; }        

        [DisplayName("Fecha:")]
        public string Date { get; set; }       

        [DisplayName("Número de Cliente:")]
        public string ClientNumber { get; set; }

        [DisplayName("Cliente:")]
        public string ClientName { get; set; }

        [DisplayName("Dirección:")]
        public string Address { get; set; }        

        [DisplayName("Teléfono:")]
        public string PhoneNumber { get; set; }

        [DisplayName("Fax:")]
        public string FaxNumber { get; set; }

        [DisplayName("Correo:")]
        public string Email { get; set; }        

        [DisplayName("No. Cotización:")]
        public string Quote { get; set; }

        [DisplayName("Buscar Cliente")]
        public string ClientCode { get; set; }

        [DisplayName("Artículo: ")]
        public string Item { get; set; }        

        public string QuoteSerie { get; set; }
        public long QuoteNo { get; set; }
        public bool IsFromMenu { get; set; }
        public string SiteCode { get; set; }
        public decimal Total { get; set; }
        public decimal SubTotal { get; set; }
        public decimal Tax { get; set; }
        public string PdfBase64 { get; set; }
        public string UserName { get; set; }
        public string SellerEmail { get; set; }
        public List<ItemQuotation> ItemsList { get; set; }
        public List<CustomersDDL> CustomersList { get; set; }
        public bool HasSerieAndNumber { get; set; }


        public string GetSiteCode()
        {
            _context = new DFL_SAIEntities();
            var SiteConfig = _context.SITE_CONFIG.FirstOrDefault();

            if (SiteConfig != null)
                return SiteConfig.site_code;

            return string.Empty;
        }        
    }
}
