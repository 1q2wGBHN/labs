﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App.Entities.ViewModels.UserSpecs
{
    public class UserSpecsModel
    {
        public string EmpNo { get; set; }
        public string FullName { get; set; }
        public string Site { get; set; }
        public string SiteName { get; set; }
        public string SiteAddress { get; set; }
        public bool DriverFlag { get; set; }
        public string MobileTel { get; set; }
    }
}
