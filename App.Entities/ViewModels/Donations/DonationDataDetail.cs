﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App.Entities.ViewModels.Donations
{
    public class DonationDataDetail
    {
        public string Hour { get; set; }
        public long Sale { get; set; }
        public string CheckOutNo { get; set; }
        public decimal Amount { get; set; }
        public string PaymentMethod { get; set; }

    }
}
