﻿namespace App.Entities.ViewModels.Donations
{
    public class DonationsTicketsHeader
    {
        public string Date { get; set; }
        public string Time { get; set; }
        public string Cashier { get; set; }
        public decimal Total { get; set; }
        public string CheckoutNo { get; set; }
        public string DonationSaleId { get; set; }
    }
}
