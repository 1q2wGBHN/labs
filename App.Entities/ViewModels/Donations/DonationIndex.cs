﻿using System;
using System.Collections.Generic;
using System.ComponentModel;

namespace App.Entities.ViewModels.Donations
{
    public class DonationIndex
    {
        public DonationIndex()
        {
            StartDate = DateTime.Now.ToString("dd/MM/yyyy");
            EndDate = DateTime.Now.ToString("dd/MM/yyyy");
            IsFromMenu = true;
            DataReport = new List<DonationData>();
            CashierTotals = new List<DonationByCashier>();
        }

        [DisplayName("Fecha Inicial: ")]
        public string StartDate { get; set; }

        [DisplayName("Fecha Final: ")]
        public string EndDate { get; set; }

        [DisplayName("Cajero: ")]
        public string Cashier { get; set; }

        [DisplayName("Campaña: ")]
        public string Campaign { get; set; }

        public bool IsFromMenu { get; set; }

        public string CashierName { get; set; }
        public string CampaignName { get; set; }

        public List<DonationData> DataReport { get; set; }
        public List<DonationByCashier> CashierTotals { get; set; }
    }
}
