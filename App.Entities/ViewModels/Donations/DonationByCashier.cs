﻿namespace App.Entities.ViewModels.Donations
{
    public class DonationByCashier
    {
        public DonationByCashier()
        {
            Amount = 0;
        }

        public string Cashier { get; set; }
        public decimal Amount { get; set; }
    }
}
