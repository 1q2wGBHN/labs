﻿namespace App.Entities.ViewModels.Donations
{
    public class DonationsTicketsDetail
    {
        public string Description { get; set; }
        public int Quantity { get; set; }
        public decimal UnitPrice { get; set; }
        public decimal TotalPrice { get; set; }
    }
}
