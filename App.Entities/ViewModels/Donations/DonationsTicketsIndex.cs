﻿using System;
using System.Collections.Generic;
using System.ComponentModel;

namespace App.Entities.ViewModels.Donations
{
    public class DonationsTicketsIndex
    {
        public DonationsTicketsIndex()
        {
            StartDate = DateTime.Now.ToString("dd/MM/yyyy");
            EndDate = DateTime.Now.ToString("dd/MM/yyyy");
            IsFromMenu = true;
        }

        [DisplayName("Fecha Inicial: ")]
        public string StartDate { get; set; }

        [DisplayName("Fecha Final: ")]
        public string EndDate { get; set; }

        [DisplayName("Cajero: ")]
        public string Cashier { get; set; }

        public bool IsFromMenu { get; set; }

        public List<DonationsTicketsHeader> DataReport { get; set; }
    }
}
