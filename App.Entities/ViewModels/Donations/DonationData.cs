﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App.Entities.ViewModels.Donations
{
    public class DonationData
    {
        public string Date { get; set; }
        public string Cashier { get; set; }
        public decimal AmountMXN { get; set; }
        public decimal AmountUSD { get; set; }
        public decimal Total { get; set; }
        public decimal CurrencyRate { get; set; }
        public string Campaign { get; set; }
        public string CurrencyCode { get; set; }
        public int DonationId { get; set; }
        public string CashierId { get; set; }
    }
}
