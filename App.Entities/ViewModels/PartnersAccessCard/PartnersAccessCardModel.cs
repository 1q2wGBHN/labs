﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using App.Entities.ViewModels.PartnersAccessCard.Validaciones;

namespace App.Entities.ViewModels.PartnersAccessCard
{
    public class PartnersAccessCardModel
    {
    }
   
    [MetadataType(typeof(PartnersAccessCardMetaData))]
    public partial class DFLPOS_USER_CARD_ACCESS
    {

    }

   
}


namespace App.Entities
{
    

    [MetadataType(typeof(PartnersAccessCardMetaData))]
    public partial class DFLPOS_USER_CARD_ACCESS
    {

    }

    public class PartnersAccessCardMetaData
    {
        [DisplayName("No. empleado")]
        [Required(ErrorMessage = "Este campo es requerido")]
        //[Index(IsUnique = true)]
        //[Unique]
        public string emp_id { get; set; }
        [DisplayName("No. de tarjeta")]
        [MinLength(5, ErrorMessage = "Mínimo 5 dígitos")]
        [RegularExpression("([0-9]+)", ErrorMessage = "Solo números")]
        [Required(ErrorMessage = "Este campo es requerido")]
        //[Unique]
        //[Index(IsUnique = true)]
        public string card_number { get; set; }
    }
    [MetadataType(typeof(USER_MASTERMetaData))]
    public partial class USER_MASTER
    {
        [NotMapped]
        public string full_name
        {
            get
            {
                return $"{first_name } {last_name} ({user_name})";
            }
        }
    }

    public class USER_MASTERMetaData
    {
        [DisplayName("Nombre de empleado")]
        public string user_name { get; set; }
       
    }
}