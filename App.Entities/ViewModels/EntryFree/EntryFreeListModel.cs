﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App.Entities.ViewModels.EntryFree
{
    public class EntryFreeListModel
    {
        public int Id { get; set; }
        public string PartNumber { get; set; }
        public string Description { get; set; }
        public string Size { get; set; }
        public decimal Quantity { get; set; }
        public decimal Price { get; set; }
        public decimal Import { get { return Price * Quantity; } }
        public decimal Iva { get; set; }
        public decimal Ieps { get; set; }
        public int Print_Status { get; set; }
    }
}
