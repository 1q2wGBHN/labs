﻿using App.Entities.ViewModels.Supplier;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App.Entities.ViewModels.EntryFree
{
    public class EntryFreeModel
    {
        public int Id { get; set; }
        public int SupplierId { get; set; }
        public int Print_status { get; set; }
        public string Supplier { get; set; }
        public string Type { get; set; }
        public string SubType { get; set; }
        public string Date { get; set; }
        public string Reference { get; set; }
        public string Commentary { get; set; }
        public string UserName { get; set; }
        public string Phone { get; set; }
        public string User { get; set; }
        public string Emailpurchases { get; set; }
        public string EmailSale { get; set; }
        public string Site { get; set; }
        public string AddressSite { get; set; }
        public int Status { get; set; }
        public List<EntryFreeListModel> EntryFreeList { get; set; }
        public EntryFreeModel()
        {
            EntryFreeList = new List<EntryFreeListModel>();
        }
    }

    public class EntryFreeAndSupplierModel
    {
        public List<EntryFreeModel> EntryFreeList { get; set; }
        public List<MaSupplierModel> SupplierList { get; set; }
        public EntryFreeAndSupplierModel()
        {
            EntryFreeList = new List<EntryFreeModel>();
            SupplierList = new List<MaSupplierModel>();
        }
    }
}
