﻿using App.Entities.ViewModels.CreditNotes;
using App.Entities.ViewModels.Sales;
using System.Collections.Generic;
using System.ComponentModel;


namespace App.Entities.ViewModels.Invoices
{
    public class InvoiceIndex
    {
        public InvoiceIndex()
        {
            IepsNamesList = new List<string>();
        }

        [DisplayName("Por No. Factura: ")]
        public string InvoiceNo { get; set; }
        [DisplayName("Entre: ")]
        public string StartDate { get; set; }
        [DisplayName("Y: ")]
        public string EndDate { get; set; }
        [DisplayName("Moneda: ")]
        public Currencys Currency { get; set; }       
        [DisplayName("Estado: ")]
        public CreditNoteStatus Status { get; set; }
        [DisplayName("Timbre: ")]
        public CreditNoteStatusFiscal Uuid { get; set; }
        [DisplayName("Por Código de Cliente: ")]
        public string CustomerCode { get; set; }
        [DisplayName("Por Monto: ")]
        public Mounts OtionsMounts { get; set; }
        [DisplayName("Tipo de Pago ")]
        public InvoiceType OptionsPay { get; set; }
        public decimal Monto { get; set; }
        public List<InvoiceModel> InvoiceList { get; set; }     
        public decimal[,] Totales { get; set; }
        public bool IsDataForReport { get; set; }
        public List<string> IepsNamesList { get; set; }
    }
   

}
