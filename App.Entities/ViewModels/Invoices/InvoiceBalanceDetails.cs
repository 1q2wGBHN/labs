﻿namespace App.Entities.ViewModels.Invoices
{
    public class InvoiceBalanceDetails
    {
        public long Number { get; set; }
        public string Serie { get; set; }
        public decimal Balance { get; set; }
        public decimal PaymentInvoice { get; set; }
        public decimal Payment { get; set; }
        public string Invoice { get; set; }
        public string PaymentId { get; set; }
        public decimal PreviousBalance { get; set; }
    }
}
