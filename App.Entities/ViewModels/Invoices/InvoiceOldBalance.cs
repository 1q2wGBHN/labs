﻿namespace App.Entities.ViewModels.Invoices
{
    public class InvoiceOldBalance
    {
        public long CFDI { get; set; }
        public string Number { get; set; }
        public string Date { get; set; }
        public string DueDate { get; set; }
        public int Days { get; set; }
        public string PaymentMethod { get; set; }
        public decimal Total { get; set; }
        public decimal SubTotal { get; set; }
        public decimal IEPS { get; set; }
        public decimal Tax { get; set; }
        public decimal Balance { get; set; }        
    }
}
