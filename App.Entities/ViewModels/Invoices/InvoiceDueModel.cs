﻿using App.Entities.ViewModels.Customers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App.Entities.ViewModels.Invoices
{
    public class InvoiceDueModel
    {
        public CustomerViewModel Customer { get; set; }
        public List<InvoiceModel> Invoices { get; set; }
    }
}
