﻿namespace App.Entities.ViewModels.Invoices
{
    public class InvoiceDetailsInsert
    {
        public InvoiceDetailsInsert()
        {
            DetailDiscount = 0;
        }

        public long InvoiceNumber { get; set; }
        public string InvoiceSerie { get; set; }
        public string ItemPartNumber { get; set; }
        public string ItemDescription { get; set; }
        public decimal? ItemPartPercentage { get; set; }
        public decimal DetailQty { get; set; }
        public decimal QtyAvailable { get; set; }
        public decimal DetailPrice { get; set; }
        public decimal? DetailDiscount { get; set; }
        public string ImportDoc { get; set; }
        public string ProductCode { get; set; }
        public string UnitCode { get; set; }
        public long? PreviousDetailId { get; set; }
        public long? SaleDetailId { get; set; }
        public INVOICE_DETAIL INVOICE_DETAIL
        {
            get
            {
                return new INVOICE_DETAIL
                {
                    invoice_no = InvoiceNumber,
                    invoice_serie = InvoiceSerie,
                    part_number = ItemPartNumber,
                    description = ItemDescription,                    
                    detail_qty = DetailQty,
                    detail_qty_available = QtyAvailable,
                    detail_price = DetailPrice,
                    detail_discount = DetailDiscount,
                    import_doc = ImportDoc,
                    sat_prodct_code = ProductCode,
                    sat_unt_code = UnitCode,
                    sale_detail_id = SaleDetailId,
                    previous_detail_id = PreviousDetailId
                };
            }
        }
    }
}
