﻿using System;
using System.Collections.Generic;

using System.Linq.Expressions;

using System.Data.Entity;


namespace App.Entities.ViewModels.Invoices
{

    public class InvoiceReportModel
    {

        //F I L T R O S
        public string CurrencyCodeOption { get; set; }
        public DateTime? StartDate { get; set; }
        public DateTime? FinishDate { get; set; }
        public string CustomerCode { get; set; }
        public string AmountOption { get; set; }
        public List<InvoiceOption> AmountOptions { get; set; }
        public List<InvoiceOption> StatusOptions { get; set; }
        public List<InvoiceOption> CurrencyOptions { get; set; }
        public decimal? InvoiceTotal { get; set; }
        public string StatusOption { get; set; }
        public List<InvoiceReport> InvoicesReport { get; set; }        

        //T O T A L E S
        public decimal[,] Totales { get; set; }
        
        
    }
    public class InvoiceOption
    {
        public string OptionId { get; set; }
        public string OptionName { get; set; }
    }
    public class InvoiceReport
    {
        public long InvoiceNumber { get; set; }
        public string InvoiceSerie { get; set; }
        public DateTime CreateDate { get; set; }
        public string CustomerCode { get; set; }
        public string CustomerName { get; set; }
        public DateTime DueDate { get; set; }
        public decimal Amount { get; set; }
        public string IepsPredicate { get; set; }
        public decimal Ieps { get; set; }
        public decimal Tax { get; set; }
        public decimal TotalAmount { get; set; }
        public string CreditOrCash { get; set; }
        public decimal Balance { get; set; }
        public string CurrencyCode { get; set; }
        public string FiscalNumber { get; set; }
        public string Status { get; set; }
        public string PaymentMethod { get; set; }
        public List<InvoiceReportDetails> Detail { get; set; }
    }

    public class InvoiceReportDetails
    {
        public string ProductNumber { get; set; }
        public string ProductName { get; set; }
        public decimal Quantity { get; set; }
        public decimal UnitPrice { get; set; }
        public decimal Extension { get; set; }
    }

    public class DetailTaxes
    {
        public long id { get; set; }
        public long detail_id { get; set; }
        public string tax_code { get; set; }
        public decimal tax_amount { get; set; }
        public string tax_name { get; set; }
        public string invo_serie { get; set; }
        public long invo_id { get; set; }
    }
    public class ToolsForReportInvoice
    {
        public static DetailTaxes ConvertToDetailTax(INVOICE_DETAIL_TAX DetailTaxFDB)
        {
            var Tax = new DetailTaxes();

            Tax.id =  DetailTaxFDB.id;
            Tax.detail_id = DetailTaxFDB.detail_id;
            Tax.tax_code = DetailTaxFDB.tax_code;
            Tax.tax_amount = DetailTaxFDB.tax_amount;
            Tax.tax_name = DetailTaxFDB.MA_TAX.name;
            return Tax;
        }
        
        public static InvoiceReportDetails ConvertToDetail(INVOICE_DETAIL detailFBD)
        {
            var detail = new InvoiceReportDetails();
            detail.ProductNumber = detailFBD.part_number;
            detail.ProductName = detailFBD.description;
            detail.Quantity = detailFBD.detail_qty;
            detail.UnitPrice = detailFBD.detail_qty > 0 ? detailFBD.detail_price / detailFBD.detail_qty : 0;
            detail.Extension = (detailFBD.detail_qty > 0 ? detailFBD.detail_price / detailFBD.detail_qty : 0) * detailFBD.detail_qty;
            return detail;
        }
        public static List<InvoiceReportDetails> ConvertToDetails(List<INVOICE_DETAIL> detailsFBD)
        {
            var details = new List<InvoiceReportDetails>();
            foreach (INVOICE_DETAIL detailFDB in detailsFBD)
                details.Add(ConvertToDetail(detailFDB));

            return details;
        }
        
        public static decimal[,] GetTotals(List<InvoiceReport> Headers)
        {            
            decimal[,] Totales = new decimal[5, 3];            

            // C A R G A R     D A T O S
            foreach (InvoiceReport Header in Headers)
            {

                if (Header.CreditOrCash == "Contado")
                {// B I N D     C A S H    

                    // R A T E     Z E R O
                    if (Header.Tax != 0)
                        Totales[0, 1] += Header.Amount;
                    else// T A X E D
                        Totales[1, 1] += Header.Amount;
                    // T A X
                    Totales[2, 1] += Header.Tax;
                    // I E P S
                    Totales[3, 1] += Header.Ieps;
                    
                }
                else
                {// B I N D     C R E D I T

                    // R A T E     Z E R O
                    if (Header.Tax != 0)
                        Totales[0, 0] += Header.Amount;
                    else// T A X E D
                        Totales[1, 0] += Header.Amount;
                    // T A X
                    Totales[2, 0] += Header.Tax;
                    // I E P S
                    Totales[3, 0] += Header.Ieps;
                    
                }
            }

            // S U M A R    T O T A L E S
            for (int f = 0; f < Totales.GetLength(0); f++)
            {
                for (int c = 0; c < Totales.GetLength(1); c++)
                {
                    if (f != Totales.GetLength(0) - 1)
                        Totales[Totales.GetLength(0) - 1, c] += Totales[f, c];
                    if (c != Totales.GetLength(1) - 1)
                        Totales[f, Totales.GetLength(1) - 1] += Totales[f, c];
                }

            }
            Totales[Totales.GetLength(0) - 1, Totales.GetLength(1) - 1] /= 2;



            return Totales;
        }
        public static List<InvoiceOption>BindDDLCurrencyOptions(List<CURRENCY> CurrencysFDB)
        {
            var lst_ICO = new List<InvoiceOption>();

            var option_001 = new InvoiceOption();
            option_001.OptionId = "SIN_FILTRAR";
            option_001.OptionName = "SIN_FILTRAR";
            lst_ICO.Add(option_001);

            foreach (CURRENCY currencyfdb in CurrencysFDB)
            {
                var option_000 = new InvoiceOption();
                option_000.OptionId = currencyfdb.cur_code;
                option_000.OptionName = currencyfdb.cur_code;
                lst_ICO.Add(option_000);
            }

            return lst_ICO;
        }
        public static List<InvoiceOption> BindDDLAmountOptions()
        {
            var lst_IAO = new List<InvoiceOption>();
            var option_001 = new InvoiceOption();
            option_001.OptionId = "SIN_FILTRAR";
            option_001.OptionName = "SIN_FILTRAR";
            lst_IAO.Add(option_001);

            var option_002 = new InvoiceOption();
            option_002.OptionId = "MAYOR_A";
            option_002.OptionName = "MAYOR_A";
            lst_IAO.Add(option_002);

            var option_003 = new InvoiceOption();
            option_003.OptionId = "IGUAL_A";
            option_003.OptionName = "IGUAL_A";
            lst_IAO.Add(option_003);

            var option_004 = new InvoiceOption();
            option_004.OptionId = "MENOR_A";
            option_004.OptionName = "MENOR_A";
            lst_IAO.Add(option_004);
            
            return lst_IAO;

        }
        public static List<InvoiceOption> BindDDLStateOptions()
        {
            var lst_ISO = new List<InvoiceOption>();

            var option_005 = new InvoiceOption();
            option_005.OptionId = "SIN_FILTRAR";
            option_005.OptionName = "SIN_FILTRAR";
            lst_ISO.Add(option_005);

            var option_006 = new InvoiceOption();
            option_006.OptionId = "ACTIVAS";
            option_006.OptionName = "ACTIVAS";
            lst_ISO.Add(option_006);

            var option_007 = new InvoiceOption();
            option_007.OptionId = "CANCELADAS";
            option_007.OptionName = "CANCELADAS";
            lst_ISO.Add(option_007);

            return lst_ISO;
        }
        
    }

    public class InvoiceTax
    {
        public long InvoiceNumber { get; set; }
        public string InvoiceSerie { get; set; }
        public Boolean Status { get; set; }
        public decimal IVA { get; set; }
        public decimal IEPS { get; set; }
        public DateTime InvoiceDate { get; set; }
        public decimal Total { get; set; }
    }
}
