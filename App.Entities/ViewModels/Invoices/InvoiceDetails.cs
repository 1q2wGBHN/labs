﻿using System.Collections.Generic;
using System.ComponentModel;

namespace App.Entities.ViewModels.Invoices
{
    public class InvoiceDetails
    {
        public InvoiceDetails()
        {
            Products = new List<InvoiceProductsDetails>();
        }

        [DisplayName("No. Factura: ")]
        public long InvoiceNumber { get; set; }

        [DisplayName("No. Cliente: ")]
        public string ClientNumber { get; set; }

        [DisplayName("Forma de Pago:")]
        public string PaymentMethod { get; set; }

        [DisplayName("Cliente: ")]
        public string ClientName { get; set; }

        [DisplayName("Tickets:")]
        public string Tickets { get; set; }

        [DisplayName("Formato:")]
        public string Format { get; set; }

        [DisplayName("Fecha: ")]
        public string Date { get; set; }
        
        [DisplayName("Moneda: ")]
        public string Currency { get; set; }

        [DisplayName("Subtotal: ")]
        public decimal SubTotal { get; set; }

        [DisplayName("IVA: ")]
        public decimal Tax { get; set; }

        [DisplayName("Total: ")]
        public decimal Total { get; set; }

        [DisplayName("Saldo Factura: ")]
        public decimal InvoiceBalance { get; set; }

        public List<InvoiceProductsDetails> Products { get; set; }

        public bool IsInvoiceDaily { get; set; }
        public string InvoiceSerie { get; set; }
        public string Serie { get; set; }
        public long Number { get; set; }
        public string Uuid { get; set; }
        public string DocumentType { get; set; }

        public string SatPMCode { get; set; }
        public decimal CurrencyRate { get; set; }
    }
}
