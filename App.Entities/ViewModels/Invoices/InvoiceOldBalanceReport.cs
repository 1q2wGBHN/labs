﻿using System.Collections.Generic;
using System.ComponentModel;

namespace App.Entities.ViewModels.Invoices
{
    public class InvoiceOldBalanceReport
    {
        public InvoiceOldBalanceReport()
        {
            InvoicesList = new List<InvoiceOldBalance>();
        }

        [DisplayName("Cliente: ")]
        public string CustomerName { get; set; }

        [DisplayName("Teléfono: ")]
        public string Phone { get; set; }

        [DisplayName("Correo: ")]
        public string Email { get; set; }

        [DisplayName("Saldo Total: ")]
        public decimal TotalBalance { get; set; }
        
        public string CustomerRFC { get; set; }
        public string CustomerCode { get; set; }
        public List<InvoiceOldBalance> InvoicesList { get; set; }
    }
}
