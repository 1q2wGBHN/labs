﻿using System.ComponentModel;

namespace App.Entities.ViewModels.Invoices
{
    public class InvoiceProductsDetails
    {
        [DisplayName("No. Artículo")]
        public string ItemNumber { get; set; }

        [DisplayName("Artículo")]
        public string ItemName { get; set; }

        [DisplayName("Cantidad")]
        public decimal Quantity { get; set; }

        [DisplayName("Precio Unitario")]
        public decimal ItemPrice { get; set; }

        [DisplayName("Extensión")]
        public decimal TotalPrice { get; set; }

        [DisplayName("Devolución")]
        public decimal QuantityReturn { get; set; }

        [DisplayName("Cantidad")]
        public string QuantityBon { get; set; }       

        public int IsWeightAvailable { get; set; }
        public decimal QuantityAvailable { get; set; }
        public long DetailId { get; set; }                
        public string IEPSCode { get; set; }
        public string IVACode { get; set; }
        public decimal IVAValue { get; set; }
        public decimal IEPSValue { get; set; }
        public decimal ItemPriceNoTax { get; set; } //This will be used for Credit Notes, save on DB the same price to match Invoice Details Price.
        public decimal ItemTax { get; set; }
        public decimal ItemIEPS { get; set; }
        public string SalePartNumber { get; set; }
        public string QuantityAvailableBon { get; set; }
    }
}
