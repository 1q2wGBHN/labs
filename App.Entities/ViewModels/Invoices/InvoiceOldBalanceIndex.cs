﻿using System.Collections.Generic;
using System.ComponentModel;

namespace App.Entities.ViewModels.Invoices
{
    public class InvoiceOldBalanceIndex
    {
        public InvoiceOldBalanceIndex()
        {
            IsFromMenu = true;
            ReportData = new List<InvoiceOldBalanceReport>();
        }

        [DisplayName("Moneda: ")]
        public string Currency { get; set; }

        [DisplayName("Fecha: ")]
        public string Date { get; set; }

        [DisplayName("Sucursal: ")]
        public string Store { get; set; }

        public bool IsFromMenu { get; set; }

        public List<InvoiceOldBalanceReport> ReportData { get; set; }
    }
}
