﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App.Entities.ViewModels.Invoices
{
    public class Cedulas
    {
        public string _month { get; set; }
    }
    public class CedulaModel
    {
        public CedulaModel() { }

        public string Uuid { get; set; }
        public string FolioRel { get; set; }
        public string Folio { get; set; }

        public string Serie { get; set; }
        public string Sucursal { get; set; }

        public string Cliente { get; set; }
        public string Rfc { get; set; }
        public string FechaEmi { get; set; }
        public string FolioFiscal { get; set; }
        public string ServicioPrestado { get; set; }
        public string Moneda { get; set; }
        public string TipoCambio { get; set; }

        public string MontoOperacion { get; set; }
        public string MontoBaseT0 { get; set; }
        public string MontoBaseT16 { get; set; }
        public string IvaTrasladado16 { get; set; }
        public string MontoBaseT8 { get; set; }
        public string IvaTrasladado8 { get; set; }
        public string Tasa { get; set; }
        public string IepsTrasladado { get; set; }
        public string TotalImpuestos { get; set; }
        public string ValorTotalFactura { get; set; }
        public string Clave { get; set; }
        public string MetodoPago { get; set; }
        public string NumeroParcialidad { get; set; }
        //Complemento
        public string FolioFiscaC { get; set; }
        public string FechaEmisionC { get; set; }
        public string ImporteSaldoAnterior { get; set; }
        public string ImportePagado { get; set; }
        public string ImporteSaldoInsoluto { get; set; }
        public string UsoCfdi { get; set; }
        public string FormaPago { get; set; }
        public string AbonadoPorc { get; set; }
        public string MontoOperacionCobrado { get; set; }
        public string MontoBaseT0C { get; set; }
        public string MontoBaseT0AdicionadoImp { get; set; }
        public string MontoBaseT16C { get; set; }
        public string IvaTrasladadoCobrado16 { get; set; }
        public string MontoBaseT8C { get; set; }
        public string IvaTrasladadoCobrado8 { get; set; }
        public string IepsladadoCobrado { get; set; }
        public string ImporteTotalCobrado { get; set; }
        public string ImporteComplemento { get; set; }
        public string FechaCobro { get; set; }
        public string ReferenciaBancaria { get; set; }
        public string NombreInstitucionFinanciera { get; set; }
        public string FechaPublicacion { get; set; }
        public string Notas { get; set; }
        public string TotalPorRFC { get; set; }








    }

}
