﻿using App.Entities.ViewModels.Sales;
using System.Collections.Generic;

namespace App.Entities.ViewModels.Invoices
{

    public class InvoiceModel
    {
        public InvoiceModel()
        {
            IepsList = new List<IepsModel>();
        }

        public long InvoiceNumber { get; set; }
        public string InvoiceSerie { get; set; }
        public string CreateDate { get; set; }
        public string CustomerCode { get; set; }
        public string CustomerName { get; set; }
        public string DueDate { get; set; }
        public decimal Amount { get; set; }
        public string IepsPredicate { get; set; }
        public string IepsPredicateReport { get; set; }
        public decimal Ieps { get; set; }
        public decimal Tax { get; set; }
        public decimal TotalAmount { get; set; }
        public decimal TotalCredit { get; set; }
        public decimal TotalCash { get; set; }
        public string CreditOrCash { get; set; }
        public decimal Balance { get; set; }
        public string CurrencyCode { get; set; }
        public string FiscalNumber { get; set; }
        public string FiscalNumberReport { get; set; }
        public bool FiscalStatus { get; set; }
        public bool StatusValue { get; set; }
        public string Status { get; set; }
        public decimal gravado { get; set; }
        public decimal Tax0 { get; set; }
        public decimal IEPSgravado { get; set; }
        public decimal IEPS0 { get; set; }
        public bool HasXmlFile { get; set; }
        public string SerieAndNumber { get; set; }

        public List<IepsModel> IepsList { get; set; }
    }
}
