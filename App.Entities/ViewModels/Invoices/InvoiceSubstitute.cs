﻿using System.ComponentModel;

namespace App.Entities.ViewModels.Invoices
{
    public class InvoiceSubstitute
    {
        public InvoiceSubstitute()
        {
            IsFromMenu = true;
            InvoiceModel = new InvoicesSales();
        }

        [DisplayName("No. Factura: ")]
        public string InvoiceNumber { get; set; }
        public bool IsFromMenu { get; set; }

        public InvoicesSales InvoiceModel { get; set; }
    }
}
