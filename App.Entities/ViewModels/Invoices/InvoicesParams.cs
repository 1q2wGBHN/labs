﻿using System.ComponentModel.DataAnnotations;

namespace App.Entities.ViewModels.Invoices
{

    public enum Mounts
        {
            [Display(Name = "CUALQUIERA")]
            CUAL,
            [Display(Name = "Mayor a:")]
            MAYOR,
            [Display(Name = "Menor a:")]
            MENOR,
            [Display(Name = "Igual a:")]
            IGUAL
        }
        public enum InvoiceType
        {
            [Display(Name = "TODAS")]
            All,
            [Display(Name = "Crédito")]
            Credit,

            [Display(Name = "Contado")]
            Cash,

            [Display(Name = "Compensación")]
            Conp
    }
    
}
