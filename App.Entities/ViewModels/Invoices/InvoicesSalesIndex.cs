﻿using System;
using System.ComponentModel;

namespace App.Entities.ViewModels.Invoices
{
    public class InvoicesSalesIndex
    {
        public InvoicesSalesIndex()
        {
            SaleDate = DateTime.Now.Date.ToString("dd/MM/yyyy");            
        }

        [DisplayName("Fecha")]
        public string SaleDate { get; set; }

        [DisplayName("Y")]
        public string EndDate { get; set; }

        [DisplayName("No. Venta")]
        public string SalesNumber { get; set; }

        public bool IsGlobal { get; set; }
    }
}