﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App.Entities.ViewModels.Item
{
    public class ItemEmptySpaceScanModel
    {
        public int id_empty_space { get; set; }
        public string part_number { get; set; }
        public string description { get; set; }
        public decimal? stock_existing { get; set; }
        public string planogram { get; set; }
        public System.DateTime? last_entry { get; set; }
        public decimal? last_entry_quantity { get; set; }
        public decimal? average_sale { get; set; }
        public int incidence { get; set; }
        public string cuser { get; set; }
        public System.DateTime cdate { get; set; }
        public string program_id { get; set; }

        public List<ItemEmptySpaceScanModel> Items { get; set; }
    }
}
