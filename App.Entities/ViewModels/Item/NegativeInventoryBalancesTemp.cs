﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App.Entities.ViewModels.Item
{
    public class NegativeInventoryBalancesTemp
    {
        public int id_inventory_negative { get; set; }
        public string part_number_origin { get; set; }
        public string part_number_origin_description { get; set; }
        public decimal part_number_origin_stock_unrestricted { get; set; }
        public decimal part_number_origin_last_purchase_price { get; set; }
        public decimal part_number_origin_amount { get; set; }
        public string part_number_destination { get; set; }
        public string part_number_destination_description { get; set; }
        public decimal part_number_destination_stock_unrestricted { get; set; }
        public decimal part_number_destination_last_purchase_price { get; set; }
        public decimal part_number_destination_amount { get; set; }
        public decimal move_quantity { get; set; }
        public string reason { get; set; }
        public bool active_flag { get; set; }
        public decimal stock_destination { get; set; }
        public decimal stock_origin { get; set; }
    }
}
