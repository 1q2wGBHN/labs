﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App.Entities.ViewModels.Item
{
    public class ItemReportModel
    {
        public string part_number { get; set; }
        public string division { get; set; }
        public string department { get; set; }
        public string part_description { get; set; }
        public bool? active_flag { get; set; }
        public string buyer_division { get; set; }
        public string part_iva { get; set; }
        public string part_ieps { get; set; }
        public string part_number_sat { get; set; }
        public string part_iva_sale { get; set; }
        public string part_type { get; set; }
        public string unit { get; set; }
        public string unit_sat { get; set; }
        public bool? weight_flag { get; set; }
        public bool? combo_flag { get; set; }
        public bool? extra_item_flag { get; set; }
        public decimal price { get; set; }
        public decimal iva { get; set; }
        public decimal iva_sale { get; set; }
        public decimal ieps { get; set; }
        public decimal total { get { return price + iva + ieps; } }
        public decimal last_purchase_price { get; set; }
        public decimal unrestricted { get; set; }
        public int expirations_days { get; set; }
        public decimal? map_price { get; set; }


    }
}
