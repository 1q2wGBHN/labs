﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App.Entities.ViewModels.Item
{
    public class ItemKardexModel
    {
        public string MovementDate { get; set; }
        public string MovementDocument { get; set; }
        public string MovementReference { get; set; }
        public string MovementReferenceType { get; set; }
        public string DocumentType { get; set; }
        public string MovementType { get; set; }
        public string StorageLocation { get; set; }
        public string StockType { get; set; }
        public string DebitCredit { get; set; }
        public decimal? MovementQuantity { get; set; }
        public string Currency { get; set; }
        public decimal? AmountDocument { get; set; }
        public decimal? StorageLocationStock { get; set; }
        public decimal? StorageLocationAmount { get; set; }
        public decimal? MapPrice { get; set; }
        public decimal? TotalStock { get; set; }
        public decimal? TotalAmount { get; set; }
        public string AuthorizationBy { get; set; }
        public string CreateBy { get; set; }
        public string PartNumber { get; set; }
        public string Reason { get; set; }
        public string PartDescription { get; set; }
        public string DocumentStatus { get; set; }
        public int print_status { get; set; }
        public DateTime? TimeDocumentDate { get; set; }
        //public decimal ExistenceBeforeMovement { get; set; }
        //public decimal ExistenceAfterMovement { get; set; }
    }
}
