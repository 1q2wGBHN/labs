﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App.Entities.ViewModels.Item
{
    public class ItemModel
    {
        public string PathNumber { get; set; }
        public string Description { get; set; }
        public string UnitSize { get; set; }
        public string department { get; set; }
        public string buyerdivision { get; set; }
        public string family { get; set; }
        public decimal packing { get; set; }
        public decimal PartIva { get; set; }
        public decimal BaseCost { get; set; }
        public decimal TaxValue { get; set; }
        public string IVADescription { get; set; }
        public string IEPSDescription { get; set; }
        public decimal? Stock { get; set; }
        public string StorageLocation { get; set; }
        public bool MovementType { get; set; }
        public decimal? RequestedStock { get; set; }
        public string Comment { get; set; }
        public decimal IEPS { get; set; }
        public string Document { get; set; }
        public string SMovementType { get; set; }
        public decimal FStock { get; set; }
        public decimal Quantity { get; set; }
        public string User { get; set; }
        public decimal SalePrice { get; set; }
        public decimal Amount { get; set; }
        public string Storage_name { get; set; }
        public int id { get; set; }
    }

    public class ItemSupplierBaseCost
    {
        public string Supplier { get; set; }
        public string PartNumber { get; set; }
        public decimal Cost { get; set; }
    }
}
