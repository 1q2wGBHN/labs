﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App.Entities.ViewModels.Item
{
    public class ItemPurchase
    {
        public string PartNumber { get; set; }
        public string PartDescription { get; set; }
        public int Box { get; set; }
        public decimal Quantity { get; set; }
        public string UnitSize { get; set; }
        public decimal PurchasePrice { get; set; }
        public int PackingOfSize { get; set; }
        public decimal ItemAmount { get; set; }
        public decimal Iva { get; set; }
        public decimal Ieps { get; set; }
        public decimal IvaTotal { get; set; }
        public decimal IepsTotal { get; set; }
        public decimal ItemTotalAmount { get; set; }
        public string Currency { get; set; }
        public int ItemStatus { get; set; }
        public decimal GrQuantity { get; set; }
        public decimal BaseCost { get; set; }
    }
    public class CostInfo
    {
        public decimal BaseCost { get; set; }
        public decimal IEPS { get; set; }
        public decimal IVA { get; set; }
        public decimal Iva { get; set; }
        public decimal Ieps { get; set; }
        public string UnitSize { get; set; }
    }
    public class PurchaseHeader
    {
        public string SiteCode { get; set; }
        public int SupplierId { get; set; }
        //dia que se hace
        public DateTime PurchaseDate { get; set; }
        //fecha estimada
        public DateTime Eta { get; set; }
        public DateTime Ata { get; set; }
        public string Currency { get; set; }
        public int PurchaseStatus { get; set; }
        public string PurchaseRemark { get; set; }
    }
}
