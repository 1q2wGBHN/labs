﻿using App.Entities.ViewModels.DamagedsGoods;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App.Entities.ViewModels.Item
{
    public class ItemViewRma
    {
        public int Supplier_Id { get; set; }
        public string PartNumber { get; set; }
        public string Description { get; set; }
        public string Size { get; set; }
        public string Storage { get; set; }
        public Decimal Quantity { get; set; }
        public Decimal? Amount_Doc { get; set; }
        public Decimal? Price { get; set; }
        public Decimal? Iva { get; set; }
        public Decimal? Ieps { get; set; }
        public Decimal? IepsPorcentage { get; set; }
        public Decimal? IvaPorcentage { get; set; }
        public List<DamagedsGoodsLastCost> listcost { get; set; }
    }

    public class ItemViewCost
    {
        public string DescriptionDrop { get; set; }
        public string ValueDrop { get; set; }
        public DateTime? dateDrop { get; set; }
    }
}
