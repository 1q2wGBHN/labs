﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App.Entities.ViewModels.Item
{
    public class ItemSalesAverageModel
    {
        public string Part_number { get; set; }
        public decimal ? Average { get; set; }

    }
}
