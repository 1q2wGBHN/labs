﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App.Entities.ViewModels.Item
{
    public class ItemCurrent
    {
        public string PartNumber { get; set; }
        public string DescriptionReal { get; set; }
        public bool PresentationFlag { get; set; }
        public decimal TaxPrice { get; set; }
        public decimal Price { get; set; }
        public decimal PriceUnit { get; set; }
        public string IVA_Description { get; set; }
        public string PackingType { get; set; }
        public decimal Quantity { get; set; }
        public string FactorUnitSize { get; set; }
        public string Description { get; set; }
    
    }
}
