﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App.Entities.ViewModels.Item
{
    public class ItemInventoryDaysModel
    {
        public string Part_number { get; set; }
        public string Storage_location { get; set; }
        public decimal Unrestricted { get; set; }
        public string Part_description { get; set; }
        public decimal Avg_Sale { get; set; }
        public decimal Days_avg { get; set; }
        public decimal UnitCost { get; set; }
        public decimal IEPS { get; set; }
        public string Last_Sale { get; set; }
        public string Last_Purchase { get; set; }
        public decimal  SubTotal
        {
            get
            {
                return UnitCost * Unrestricted;
            }
        }
        public decimal SubTotal2 { get; set; }
        public decimal Iva { get; set; }
        public decimal Amount
        {
            get
            {
                return Math.Round(SubTotal + IEPS + Iva,2);
            }
        }
    }
}
