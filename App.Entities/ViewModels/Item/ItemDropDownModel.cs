﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App.Entities.ViewModels.Item
{
    public class ItemDropDownModel
    {
        public string part_number { get; set; }
        public string part_description { get; set; }
    }
}
