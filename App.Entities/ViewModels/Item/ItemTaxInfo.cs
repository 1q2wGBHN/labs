﻿namespace App.Entities.ViewModels.Item
{
    public class ItemTaxInfo
    {
        public string IEPS { get; set; }
        public decimal IEPSValue { get; set; }
        public string IVASale { get; set; }
        public decimal IVASaleValue { get; set; }
        public string IVAPurchase { get; set; }
        public decimal IVAPurchaseValue { get; set; }
        public string Number { get; set; }
        public string Description { get; set; }
    }
}
