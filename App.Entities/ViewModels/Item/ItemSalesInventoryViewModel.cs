﻿using App.Entities.ViewModels.Supplier;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App.Entities.ViewModels.Item
{
    public class ItemSalesInventoryViewModel
    {
        public List<ItemSalesInventory> ItemSalesInventory { get; set; }
        public List<MaSupplierModel> Suppliers { get; set; }
        public List<MA_CLASS> Departments { get; set; }
        public List<MA_CLASS> Family { get; set; }

        public ItemSalesInventoryViewModel()
        {
            ItemSalesInventory = new List<ItemSalesInventory>();
            Suppliers = new List<MaSupplierModel>();
            Departments = new List<MA_CLASS>();
            Family = new List<MA_CLASS>();
        }
    }
}