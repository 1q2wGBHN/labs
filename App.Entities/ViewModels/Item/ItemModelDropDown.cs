﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App.Entities.ViewModels.Item
{
    public class ItemModelDropDown
    {
        public string PartNumber { get; set; }
        public string Description { get; set; }
    }
}
