﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App.Entities.ViewModels.Item
{
    public class ItemSupplierModelPrice
    {
        public string item_number { get; set; }
        public string item_name { get; set; }
        public string item_cost { get; set; }
        public string item_currency { get; set; }
        public string item_base_cost_id { get; set; }
        public string item_supplier_id { get; set; }
        public decimal quantity { get; set; }
        public string type { get; set; }
        public DateTime fecha { get; set; }
        public string uDate { get; set; }
        public string margen { get; set; }
        public string iva { get; set; }
        public string ieps { get; set; }
        public string price { get; set; }
        public DateTime? fechainicio { get; set; }
        public string proveedor { get; set; }
        public string user { get; set; }
        public decimal base_cost { get; set; }
        public decimal item_price { get; set; }
        public decimal item_price_iva { get; set; }
        public string active_flag { get; set; }
        public decimal margin { get; set; }
    }
}
