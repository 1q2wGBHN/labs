﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App.Entities.ViewModels.Item
{
    public class ItemQuickCheckIndex
    {
        public ItemQuickCheckIndex()
        {
            ItemList = new List<ItemQuickCheck>();
            IsFromMenu = true;
        }

        [DisplayName("Artículo a buscar: ")]
        public string ItemSearch { get; set; }

        public bool IsFromMenu { get; set; }

        public List<ItemQuickCheck> ItemList { get; set; }
    }
}
