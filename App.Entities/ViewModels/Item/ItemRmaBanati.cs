﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App.Entities.ViewModels.Item
{
    public class ItemRmaBanati
    {
        public string PartNumber { get; set; }
        public string Description { get; set; }
        public string Storage { get; set; }
        public Decimal? Quantity { get; set; }
        public int? SupplierId { get; set; }
        public string Supplier { get; set; }
    }
}
