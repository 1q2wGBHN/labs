﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App.Entities.ViewModels.Item
{
    public class ItemBlindCount
    {
        public string part_description { get; set; }
        public string part_number { get; set; }
        public decimal quantity { get; set; }
        public string reason { get; set; }
    }
}
