﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App.Entities.ViewModels.Item
{
    public class ItemStockModel
    {
        public string Storage { get; set; }
        public decimal Block { get; set; }
        public string PartNumber { get; set; }
        public string Size { get; set; }
        public string Description { get; set; }
        public decimal Price { get; set; }
        public int Status { get; set; }
        public string GiDocument { get; set; }
    }
}
