﻿using System.Collections.Generic;

namespace App.Entities.ViewModels.Item
{
    public class ItemQuickCheck
    {
        public string PartNumber { get; set; }
        public string Description { get; set; }
        public decimal Stock { get; set; }
        public decimal Total_Stock { get; set; }
        public decimal Blocked { get; set; }
        public decimal Wip { get; set; }
        public decimal BasePrice { get; set; }
        public decimal TaxPrice { get; set; }
        public List<QuickPresentation> Presentations { get; set; }
    }

    public class QuickPresentation
    {
        public string PartNumber { get; set; }
        public string Description => $"{PackingType} {Quantity:F2} {FactorUnitSize}";
        public decimal TaxPrice { get; set; }
        public string PackingType { get; set; }
        public decimal Quantity { get; set; }
        public string FactorUnitSize { get; set; }
    }
}
