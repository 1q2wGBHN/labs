﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App.Entities.ViewModels.Item
{
    public class DropItemModel
    {
        public string PathNumber { get; set; }
        public string PartNumber { get; set; }// Se agrego para corregir el nombre
        public string Description { get; set; }
    }
}
