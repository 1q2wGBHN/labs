﻿namespace App.Entities.ViewModels.Item
{
    public class ItemQuotation
    {
        public string ItemNumber { get; set; }
        public string ItemDescription { get; set; }
        public decimal Stock { get; set; }
        public decimal BasePrice { get; set; }
        public decimal QuotePrice { get; set; }
        public decimal Quantity { get; set; }
        public decimal UnitPrice { get; set; }
        public string UnitMeasure { get; set; }
        public decimal ItemTax { get; set; }
        public decimal ItemTaxPercentage { get; set; }
        public bool IsPackage { get; set; }
        public string PresentationCode { get; set; }
        public string PresentationCodeText { get; set; }
        public string ItemDescriptionReport { get; set; }
    }
}
