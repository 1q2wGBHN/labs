﻿using App.Entities.ViewModels.Supplier;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App.Entities.ViewModels.Item
{
    public class ItemSalesInventory
    {
        public string PartNumber { get; set; }
        public string PartDescription { get; set; }
        public decimal Unrestricted { get; set; }
        public decimal? MapPrice { get; set; }
        public string StorageLocation { get; set; }
        public DateTime? LastPurchase { get; set; }
        public int Days { get; set; }
    }
}