﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App.Entities.ViewModels.Item
{
    public class ItemKardexHorModel
    {
        public string part_number { get; set; }
        public string part_description { get; set; }
        public decimal? Saldo_Inicial { get; set; }
        public decimal? Saldo_Final { get; set; }
        public decimal? ENTRADA_AJUSTE_COMBO_1394 { get; set; }
        public decimal? GI_FROM_BLOCKED_REMISION { get; set; }
        public decimal? SALIDA_AJUSTE_COMBO_1308 { get; set; }
        public decimal? SALIDA_AJUSTE_PACKS { get; set; }
        public decimal? ENTRADA_AJUSTE_COMBO_1574 { get; set; }
        public decimal? ENTRADA_AJUSTE_COMBO_1303 { get; set; }
        public decimal? GR_WITHOUT_COST { get; set; }
        public decimal? ENTRADA_AJUSTE_COMBO_1572 { get; set; }
        public decimal? SALIDA_TRASNFORMACION_A_PIEZAS { get; set; }
        public decimal? SALIDA_AJUSTE_COMBO { get; set; }
        public decimal? ENTRADA_TRASNFORMACION_A_PIEZAS { get; set; }
        public decimal? SALIDA_AJUSTE_COMBO_1309 { get; set; }
        public decimal? SALIDA_AJUSTE_COMBO_1381 { get; set; }
        public decimal? ENTRADA_AJUSTE_COMBO { get; set; }
        public decimal? GI_FOR_SALE { get; set; }
        public decimal? SALIDA_AJUSTE_COMBO_1395 { get; set; }
        public decimal? SALIDA_MATERIA_PRIMA { get; set; }
        public decimal? ENTRADA_AJUSTE_COMBO_1395 { get; set; }
        public decimal? ENTRADA_AJUSTE_COMBO_1573 { get; set; }
        public decimal? ENTRADA_AJUSTE_NEGATIVO { get; set; }
        public decimal? ENTRADA_AJUSTE_RECETA { get; set; }
        public decimal? SALIDA_AJUSTE_COMBO_1394 { get; set; }
        public decimal? ENTRADA_AJUSTE_COMBO_1576 { get; set; }
        public decimal? GI_FROM_BLOCKED_RMA { get; set; }
        public decimal? ENTRADA_AJUSTE_COMBO_1309 { get; set; }
        public decimal? CANCEL_GR_PO { get; set; }
        public decimal? SALIDA_AJUSTE_COMBO_1576 { get; set; }
        public decimal? GI_REVERSE_FOR_SALE { get; set; }
        public decimal? REVERSA_NOTA_DE_CREDITO { get; set; }
        public decimal? ENTRADA_AJUSTE_COMBO_1575 { get; set; }
        public decimal? ENTRADA_AJUSTE_COMBO_1381 { get; set; }
        public decimal? GI_COST_CENTER { get; set; }
        public decimal? ENTRADA_TRANSFORMACION_A_PIEZAS { get; set; }
        public decimal? ENTRADA_AJUSTE_PACKS { get; set; }
        public decimal? TRANSFER_GR { get; set; }
        public decimal? TRANSFER_GI { get; set; }
        public decimal? GR_FROM_PO { get; set; }
        public decimal? SALIDA_AJUSTE_RECETAS { get; set; }
        public decimal? SALIDA_TRANSFORMACION_A_PIEZAS { get; set; }
        public decimal? GI_FROM_BLOCKED_SCRAP { get; set; }
        public decimal? REVERSE_GI_FROM_BLOCKED_RMA { get; set; }
        public decimal? SALIDA_AJUSTE_COMBO_1572 { get; set; }
        public decimal? SALIDA_AJUSTE_NEGATIVO { get; set; }
        public decimal? SALIDA_AJUSTE_COMBO_1303 { get; set; }
        public decimal? SALIDA_POR_PV { get; set; }
        public decimal? SALIDA_AJUSTE_COMBO_1575 { get; set; }
        public decimal? CC_INVENTORY { get; set; }
        public decimal? ENTRADA_AJUSTE_COMBO_1308 { get; set; }
        public decimal? NOTA_DE_CREDITO { get; set; }
        public decimal? REVERSA_DE_SALIDA_POR_VENTA { get; set; }
        public decimal? REVERSE_TRANSFER { get; set; }
        public decimal? SALIDA_AJUSTE_COMBO_1573 { get; set; }
        public decimal? ENTRADA_AJUSTE_COMBO_1347 { get; set; }
        public decimal? SALIDA_AJUSTE_COMBO_1384 { get; set; }
        public decimal? SALIDA_AJUSTE_COMBO_1347 { get; set; }
        public decimal? SALIDA_AJUSTE_COMBO_1344 { get; set; }
        public decimal? ENTRADA_AJUSTE_COMBO_1344 { get; set; }
        public decimal? ENTRADA_AJUSTE_COMBO_1384 { get; set; }
        public decimal? ENTRADA_AJUSTE_COMBO_TOTAL
        {
            get
            {
                return ENTRADA_AJUSTE_COMBO_1303 + ENTRADA_AJUSTE_COMBO_1308 + ENTRADA_AJUSTE_COMBO_1309 + ENTRADA_AJUSTE_COMBO_1344 + ENTRADA_AJUSTE_COMBO_1347 + ENTRADA_AJUSTE_COMBO_1381
                      + ENTRADA_AJUSTE_COMBO_1384 + ENTRADA_AJUSTE_COMBO_1394 + ENTRADA_AJUSTE_COMBO_1395 + ENTRADA_AJUSTE_COMBO_1572 + ENTRADA_AJUSTE_COMBO_1573 + ENTRADA_AJUSTE_COMBO_1574 +
                      ENTRADA_AJUSTE_COMBO_1575 + ENTRADA_AJUSTE_COMBO_1576;
            }
        }
        public decimal? ENTRADA_PACKS_TOTAL
        {
            get
            {
                return ENTRADA_TRANSFORMACION_A_PIEZAS + ENTRADA_TRASNFORMACION_A_PIEZAS;
            }
        }
        public decimal? SALIDA_PACKS_TOTAL { get { return SALIDA_TRANSFORMACION_A_PIEZAS + SALIDA_TRASNFORMACION_A_PIEZAS; } }
        public decimal? SALIDA_AJUSTE_COMBO_TOTAL
        {
            get
            {
                return SALIDA_AJUSTE_COMBO_1303 + SALIDA_AJUSTE_COMBO_1308 + SALIDA_AJUSTE_COMBO_1309 + SALIDA_AJUSTE_COMBO_1344 + SALIDA_AJUSTE_COMBO_1347 +
SALIDA_AJUSTE_COMBO_1381 + SALIDA_AJUSTE_COMBO_1384 + SALIDA_AJUSTE_COMBO_1394 + SALIDA_AJUSTE_COMBO_1395 + SALIDA_AJUSTE_COMBO_1572 + SALIDA_AJUSTE_COMBO_1573 + SALIDA_AJUSTE_COMBO_1575 +
SALIDA_AJUSTE_COMBO_1576;
            }
        }
        //public decimal? SALIDA_VENTA_TOTAL { get { return GI_FOR_SALE + SALIDA_POR_PV; } }
        public decimal? REVERSA_VENTA_TOTAL { get { return GI_REVERSE_FOR_SALE ?? 0 + REVERSA_DE_SALIDA_POR_VENTA ?? 0; } }

        /// <summary>
        /// ////////////MONTOS
        /// </summary>

        public decimal? ENTRADA_AJUSTE_COMBO_1394_A { get; set; }
        public decimal? GI_FROM_BLOCKED_REMISION_A { get; set; }
        public decimal? SALIDA_AJUSTE_COMBO_1308_A { get; set; }
        public decimal? SALIDA_AJUSTE_PACKS_A { get; set; }
        public decimal? ENTRADA_AJUSTE_COMBO_1574_A { get; set; }
        public decimal? ENTRADA_AJUSTE_COMBO_1303_A { get; set; }
        public decimal? GR_WITHOUT_COST_A { get; set; }
        public decimal? ENTRADA_AJUSTE_COMBO_1572_A { get; set; }
        public decimal? SALIDA_TRASNFORMACION_A_PIEZAS_A { get; set; }
        public decimal? SALIDA_AJUSTE_COMBO_A { get; set; }
        public decimal? ENTRADA_TRASNFORMACION_A_PIEZAS_A { get; set; }
        public decimal? SALIDA_AJUSTE_COMBO_1309_A { get; set; }
        public decimal? SALIDA_AJUSTE_COMBO_1381_A { get; set; }
        public decimal? ENTRADA_AJUSTE_COMBO_A { get; set; }
        public decimal? GI_FOR_SALE_A { get; set; }
        public decimal? SALIDA_AJUSTE_COMBO_1395_A { get; set; }
        public decimal? SALIDA_MATERIA_PRIMA_A { get; set; }
        public decimal? ENTRADA_AJUSTE_COMBO_1395_A { get; set; }
        public decimal? ENTRADA_AJUSTE_COMBO_1573_A { get; set; }
        public decimal? ENTRADA_AJUSTE_NEGATIVO_A { get; set; }
        public decimal? ENTRADA_AJUSTE_RECETA_A { get; set; }
        public decimal? SALIDA_AJUSTE_COMBO_1394_A { get; set; }
        public decimal? ENTRADA_AJUSTE_COMBO_1576_A { get; set; }
        public decimal? GI_FROM_BLOCKED_RMA_A { get; set; }
        public decimal? ENTRADA_AJUSTE_COMBO_1309_A { get; set; }
        public decimal? CANCEL_GR_PO_A { get; set; }
        public decimal? SALIDA_AJUSTE_COMBO_1576_A { get; set; }
        public decimal? GI_REVERSE_FOR_SALE_A { get; set; }
        public decimal? REVERSA_NOTA_DE_CREDITO_A { get; set; }
        public decimal? ENTRADA_AJUSTE_COMBO_1575_A { get; set; }
        public decimal? ENTRADA_AJUSTE_COMBO_1381_A { get; set; }
        public decimal? GI_COST_CENTER_A { get; set; }
        public decimal? ENTRADA_TRANSFORMACION_A_PIEZAS_A { get; set; }
        public decimal? ENTRADA_AJUSTE_PACKS_A { get; set; }
        public decimal? TRANSFER_GR_A { get; set; }
        public decimal? TRANSFER_GI_A { get; set; }
        public decimal? GR_FROM_PO_A { get; set; }
        public decimal? SALIDA_AJUSTE_RECETAS_A { get; set; }
        public decimal? SALIDA_TRANSFORMACION_A_PIEZAS_A { get; set; }
        public decimal? GI_FROM_BLOCKED_SCRAP_A { get; set; }
        public decimal? REVERSE_GI_FROM_BLOCKED_RMA_A { get; set; }
        public decimal? SALIDA_AJUSTE_COMBO_1572_A { get; set; }
        public decimal? SALIDA_AJUSTE_NEGATIVO_A { get; set; }
        public decimal? SALIDA_AJUSTE_COMBO_1303_A { get; set; }
        public decimal? SALIDA_POR_PV_A { get; set; }
        public decimal? SALIDA_AJUSTE_COMBO_1575_A { get; set; }
        public decimal? CC_INVENTORY_A { get; set; }
        public decimal? ENTRADA_AJUSTE_COMBO_1308_A { get; set; }
        public decimal? NOTA_DE_CREDITO_A { get; set; }
        public decimal? REVERSA_DE_SALIDA_POR_VENTA_A { get; set; }
        public decimal? REVERSE_TRANSFER_A { get; set; }
        public decimal? SALIDA_AJUSTE_COMBO_1573_A { get; set; }
        public decimal? ENTRADA_AJUSTE_COMBO_1347_A { get; set; }
        public decimal? SALIDA_AJUSTE_COMBO_1384_A { get; set; }
        public decimal? SALIDA_AJUSTE_COMBO_1347_A { get; set; }
        public decimal? SALIDA_AJUSTE_COMBO_1344_A { get; set; }
        public decimal? ENTRADA_AJUSTE_COMBO_1344_A { get; set; }
        public decimal? ENTRADA_AJUSTE_COMBO_1384_A { get; set; }
        public decimal? ENTRADA_AJUSTE_COMBO_TOTAL_A
        {
            get
            {
                return ENTRADA_AJUSTE_COMBO_1303_A + ENTRADA_AJUSTE_COMBO_1308_A + ENTRADA_AJUSTE_COMBO_1309_A + ENTRADA_AJUSTE_COMBO_1344_A + ENTRADA_AJUSTE_COMBO_1347_A + ENTRADA_AJUSTE_COMBO_1381_A
                      + ENTRADA_AJUSTE_COMBO_1384_A + ENTRADA_AJUSTE_COMBO_1394_A + ENTRADA_AJUSTE_COMBO_1395_A + ENTRADA_AJUSTE_COMBO_1572_A + ENTRADA_AJUSTE_COMBO_1573_A + ENTRADA_AJUSTE_COMBO_1574_A +
                      ENTRADA_AJUSTE_COMBO_1575_A + ENTRADA_AJUSTE_COMBO_1576_A;
            }
        }
        public decimal? ENTRADA_PACKS_TOTAL_A
        {
            get
            {
                return ENTRADA_TRANSFORMACION_A_PIEZAS_A + ENTRADA_TRASNFORMACION_A_PIEZAS_A;
            }
        }
        public decimal? SALIDA_PACKS_TOTAL_A { get { return SALIDA_TRANSFORMACION_A_PIEZAS_A + SALIDA_TRASNFORMACION_A_PIEZAS_A; } }
        public decimal? SALIDA_AJUSTE_COMBO_TOTAL_A
        {
            get
            {
                return SALIDA_AJUSTE_COMBO_1303_A + SALIDA_AJUSTE_COMBO_1308_A + SALIDA_AJUSTE_COMBO_1309_A + SALIDA_AJUSTE_COMBO_1344_A + SALIDA_AJUSTE_COMBO_1347_A +
SALIDA_AJUSTE_COMBO_1381_A + SALIDA_AJUSTE_COMBO_1384_A + SALIDA_AJUSTE_COMBO_1394_A + SALIDA_AJUSTE_COMBO_1395_A + SALIDA_AJUSTE_COMBO_1572_A + SALIDA_AJUSTE_COMBO_1573_A + SALIDA_AJUSTE_COMBO_1575_A +
SALIDA_AJUSTE_COMBO_1576_A;
            }
        }
        //public decimal? SALIDA_VENTA_TOTAL { get { return GI_FOR_SALE + SALIDA_POR_PV; } }
        public decimal? REVERSA_VENTA_TOTAL_A { get { return GI_REVERSE_FOR_SALE_A ?? 0 + REVERSA_DE_SALIDA_POR_VENTA_A ?? 0; } }

    }
}
