﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App.Entities.ViewModels.Item
{
    public class ItemSpeckModel
    {
        public string PartNumber { get; set; }
        public decimal PackingOfSize { get; set; }
        public decimal PalletOfSize { get; set; }
        public decimal WeightOfPacking { get; set; }
        public string GrStorageLocation { get; set; }
    }
}
