﻿namespace App.Entities.ViewModels.Common
{
    public enum CustomersLookup
    {
        AllCustomer,
        ByQuotation,
        ForInvoices,
        ByInvoicesDue,
        Filter,
        ByCreditInvoices,
        ByPaymentInvoices,
        ByCancelledPayments,
        ByTransactions
    }
}
