﻿namespace App.Entities.ViewModels.Common
{
    public class PaymentsMethods
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Currency { get; set; }
    }
}
