﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App.Entities.ViewModels.Common
{
    public class DeleteModel
    {
        public string InvoiceSerie { get; set; }
        public long InvoiceNumber { get; set; }
        public string DocumentType { get; set; }
        public long DocumentNumber { get; set; }
    }
}
