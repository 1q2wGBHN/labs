﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace App.Entities.ViewModels.Common
{
    public class SendDocumentModel
    {
        public SendDocumentModel()
        {
            CanStampDocument = true;
        }

        [DisplayName("Número")]
        public string Number { get; set; }
        [DisplayName("Factura")]
        public string Invoice { get; set; }
        public string Serie { get; set; }
        [DisplayName("Cliente")]
        public string Customer { get; set; }
        [DisplayName("E-mail")]
        public string EmailCustomer { get; set; }
        [DisplayName("Enviar a")]
        public string EmailCustomerSend { get; set; }
        public string DocumentType { get; set; }
        [DisplayName ("Tipo de CFDI")]
        public Documents Document { get; set; }
        public string Total { get; set; }
        [DisplayName("Fecha")]
        public string Date { get; set; }
        public string FolioFiscal { get; set; }
        public string Uuid { get; set; }
        public bool ReSendDocument { get; set; }
        public bool IsStamped { get; set; }
        public bool CanStampDocument { get; set; }
        public string User { get; set; }
    }
    public enum Documents
    {
        [Display(Name = "FACTURA")]
        INV,
        [Display(Name = "NOTA DE CREDITO")]
        CNOT,
        [Display(Name = "BONIFICACION")]
        BON,
        [Display(Name = "PAGO")]
        PAY
    }
}
