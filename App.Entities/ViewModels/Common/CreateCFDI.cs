﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static App.Entities.ViewModels.Common.Constants;

namespace App.Entities.ViewModels.Common
{
    public class CreateCFDI
    {
        public CreateCFDI()
        {
            CFDICode = SatUseCode.TO_DEFINE;
            CFDIType = VoucherType.INCOMES;
            Currency = Currencies.MXN;
            PaymentTypeCode = PaymentMethods.MXN_CASH;
            IEPSCode = TaxCodes.NOT_AVAILABLE;
            TAXCode = TaxCodes.NOT_AVAILABLE;
            IsStamped = false;
        }

        [DisplayName("Tipo de CFDI:")]
        public string CFDIType { get; set; }

        [DisplayName("Uso de CFDI:")]
        public string CFDICode { get; set; }

        [DisplayName("Moneda:")]
        public string Currency { get; set; }

        [DisplayName("Tipo De Cambio:")]
        public decimal CurrencyExchange { get; set; }

        [DisplayName("Forma de Pago:")]
        public string IRSPaymentMethod { get; set; }

        [DisplayName("Método de Pago:")]
        public int PaymentTypeCode { get; set; }

        [DisplayName("Serie:")]
        public string Serie { get; set; }

        [DisplayName("Folio:")]
        public long Number { get; set; }

        [DisplayName("Cliente:")]
        public string ClientCode { get; set; }

        [DisplayName("Factura Siguiente:")]
        public string NextInvoice { get; set; }

        [DisplayName("Condiciones de Pago: ")]
        public string PaymentConditions { get; set; }

        public List<CFDIConcept> Concepts { get; set; }

        public bool IsStamped { get; set; }
        public string User { get; set; }

        // These fields will be used to display textboxs on view.   
        [DisplayName("Cantidad: ")]
        public decimal Quantity { get; set; }

        [DisplayName("Concepto: ")]
        public string Description { get; set; }

        [DisplayName("Precio Unitario: ")]
        public decimal UnitPrice { get; set; }       

        [DisplayName("Clave Sat:")]
        public string UnitSatCode { get; set; }

        [DisplayName("Código Sat:")]
        public string ItemSatCode { get; set; }

        [DisplayName("IVA:")]
        public string TAXCode { get; set; }

        [DisplayName("IEPS:")]
        public string IEPSCode { get; set; }

        public string DocumentType { get; set; }

        [DisplayName("Tipo de Pago:")]
        public PaymentType PM_Type { get; set; }

        public INVOICES INVOICE
        {
            get
            {
                return new INVOICES
                {
                    cfdi_id = Number,
                    invoice_serie = Serie,
                    invoice_no = Number,
                    customer_code = ClientCode,
                    invoice_total = 0,
                    invoice_tax = 0,
                    sat_us_code = CFDICode,
                    cuser = User,
                    invoice_status = true,
                    invoice_date = DateTime.Now.Date,
                    cdate = DateTime.Now.Date,
                    is_daily = false,
                    cur_code = Currency
                };
            }
        }
    }

    public class CFDIConcept
    {
        public string TaxCode { get; set; }
        public string IEPSCode { get; set; }
        public string ItemSatCode { get; set; }
        public string UnitSatCode { get; set; }
        public decimal UnitPrice { get; set; }
        public decimal Quantity { get; set; }
        public string Description { get; set; }
    }
}
