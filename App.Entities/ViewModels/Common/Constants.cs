﻿namespace App.Entities.ViewModels.Common
{
    public class Constants
    {
        public class VoucherType
        {
            public const string EXPENSES = "E"; //Credit Notes and Bonifications
            public const string INCOMES = "I"; //Invoice
            public const string PAYMENTS = "P";
            public const string QUOTES = "Q";
        }

        public class GenericCustomer
        {
            public const string RFC = "XAXX010101000";
            public const string CODE = "0";
        }

        public class GenericItem
        {
            public const string ITEM_NUMBER = "0";
        }

        public class DateFormat
        {
            public const string INT_DATE = "dd/MM/yyyy"; //International Date (Example 08/12/2019)
        }

        public class StampPac
        {
            public const string PAC_NAME = "PROFACT";
        }

        public class PaymentMethods
        {
            public const int USD_CASH = 8;
            public const int MXN_CASH = 9;
            public const int COUPON_FLORIWOOD = 17; //Ok, me la prolongué
            public const int EMPLOYEE_DISCOUNT = 18;
            public const int ELECTRONIC_TRANSFER = 20;
            public const int GIFT_CARD = 28;
            public const int CREDIT = -1;
            public const int COMPENSATION = -3;
            public const int COMBO_CARD = 27;
        }

        public class Currencies
        {
            public const string USD = "USD";
            public const string MXN = "MXN";
        }

        public class TaxCodes
        {
            public const string NOT_AVAILABLE = "N/A";
            public const string TAX_8 = "TAX_8";
            public const string TAX_16 = "TAX_16";
            public const string SUB_TOTAL = "Sub_Total";
            public const string IRS = "IRS";
            public const string EXE_0 = "EXE_0";
        }

        public class TaxTypes
        {
            public const string IEPS = "IEPS";
            public const string TAX = "TAX";
            public const string SUBTOTAL = "SubTotal";
            public const string EXEMPT = "EXENTO";
        }

        public class SatPaymentMethods
        {
            public const string CASH = "01";
            public const string UNDEFINED = "99";            
        }

        public class SatUseCode
        {
            public const string TO_DEFINE = "P01";
            public const string GENERAL_EXPENSES = "G03";
        }

        public class SalesConfig
        {
            //WD = Withdrawal
            public const string WD_ALERT = "RetiroAlerta";
            public const string WD_MANDATORY = "RetiroObligatorio";
        }

        public class MoneyDifference
        {
            public const decimal DifferenceRange = 0.05m;
            public const decimal AmountToOperate = 0.01m;
        }     
        
        public class Roles
        {
            public const string Cobranza = "COORD";
            public const string IT_Department = "Rol1"; //Sistemas
            public const string GC = "00041"; //Gerente Cobranza
            public const string SC = "Rol6"; //Supervisor Cobranza
            public const string SCGRAL = "SCGRAL"; //Supervisor Caja General
            public const string MANAGER = "RolG01"; //Gerente Tienda
            public const string ACGRAL = "00002"; //Auxiliar Caja General
            public const string SUBMANAGER = "RolG02";//Sub Gerente Tienda
            public const string DISTRITAL = "Rol8";
        }

        public class PaymentTypes
        {
            public const string PPD = "PPD"; //Pago en parcialidades
            public const string PUE = "PUE"; //Pago en unica exhibición
        }
    }
}
