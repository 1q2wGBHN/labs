﻿namespace App.Entities.ViewModels
{
    public class DetailTax
    {
        public long DetailId { get; set; }
        public string IEPSCode { get; set; }
        public decimal? IEPSValue { get; set; }
        public string IVACode { get; set; }
        public decimal? IVAValue { get; set; }
        public decimal ItemPrice { get; set; }
        public string TaxCode { get; set; }
        public decimal TaxAmount { get; set; }

        public INVOICE_DETAIL_TAX INVOICEDETAILTAX
        {
            get
            {
                return new INVOICE_DETAIL_TAX
                {
                    detail_id = DetailId,
                    tax_code = TaxCode,
                    tax_amount = TaxAmount
                };
            }
        }

        public CREDIT_NOTE_DETAIL_TAX CreditNoteDetailTax
        {
            get
            {
                return new CREDIT_NOTE_DETAIL_TAX
                {
                    detail_id = DetailId,
                    tax_code = TaxCode,
                    tax_amount = TaxAmount
                };
            }
        }
    }
}
