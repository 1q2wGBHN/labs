﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App.Entities.ViewModels.Common
{
    public enum PaymentType
    {
        [Display(Name = "Pago en parcialidades")]
        PPD,

        [Display(Name = "Pago en una exhibicion")]
        PUE,
    }
}
