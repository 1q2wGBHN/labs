﻿namespace App.Entities.ViewModels.Common
{
    public enum CashiersLookup
    {
        AllCashier,
        BySale,
        ByCancalletaionsSales,
        ByChargeRelation,
        ByDonations,
        ByDonationsSales
    }    
}
