﻿using App.Entities.ViewModels.Invoices;

namespace App.Entities.ViewModels.Common
{
    public class CancelModel
    {
        public CancelModel()
        {
            IsFromMenu = true;
            InvoDetails = new InvoiceDetails();
        }
        public string Number { get; set; }
        public string Serie { get; set; }
        public bool IsBonification { get; set; }
        public string Uuid { get; set; }
        public string DocumentType { get; set; }
        public string User { get; set; }
        public bool IsFromMenu { get; set; }

        public InvoiceDetails InvoDetails { get; set; }
    }
}
