﻿using App.Entities.ViewModels.Item;
using System;
using System.Collections.Generic;

namespace App.Entities.ViewModels.StorageLocation
{
    public class PlanogramLocationModel
    {

        public string planogram_location { get; set; }
        public string part_number { get; set; }
        public string part_description { get; set; }
        public string bin1 { get; set; }
        public string bin2 { get; set; }
        public string bin3 { get; set; }
        public decimal quantity { get; set; }
        public string planogram_description { get; set; }
        public string planogram_type { get; set; }
        public string planogram_status { get; set; }
        public Nullable<System.DateTime> cdate { get; set; }
        public string cuser { get; set; }
        public Nullable<System.DateTime> udate { get; set; }
        public string uuser { get; set; }
        public string program_id { get; set; }
        public  List<PlanogramLocationModel> PlanogramList { get; set; }
        public  List<DropItemModel> ItemsList { get; set; }
        public string barcode { get; set; }


        //public SYS_PAGE_MASTER PAGES { get { return new SYS_PAGE_MASTER { page_id = page_id, page_name = page_name, description = description, url = url, active_flag = active_flag, cdate = cdate, cuser = cuser, udate = udate, uuser = uuser, program_id = "ADMS001.cshtml" }; } }
    }
}
