﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App.Entities.ViewModels.StorageLocation
{
    public class StorageLocationModel
    {
        public string site_code { get; set; }
        public string storage_location { get; set; }
        public string bin1 { get; set; }
        public string bin2 { get; set; }
        public string bin3 { get; set; }
        public string storage_type { get; set; }
        public bool mrp_ind { get; set; }
        public bool active_flag { get; set; }
        public Nullable<System.DateTime> cdate { get; set; }
        public string cuser { get; set; }
        public Nullable<System.DateTime> udate { get; set; }
        public string uuser { get; set; }
        public string program_id { get; set; }
    }
}
