﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App.Entities.ViewModels.Inventory
{
    public class InventoryLabelTagModel
    {
        public string site_code { get; set; }
        public int last_number { get; set; }
    }
}
