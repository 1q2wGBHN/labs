﻿using System;
using System.Collections.Generic;
using System.Data.SqlTypes;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App.Entities.ViewModels.Inventory
{
    public class PalletModel
    {
        public string serial_number { get; set; }
        public string id_inventory { get; set; }
        public int user_no { get; set; }
        public int pallet_status { get; set; }
        public string storage_location_name { get; set; }
        public string cuser { get; set; }
        public DateTime? cdate { get; set; }
        public string uuser { get; set; }
        public DateTime? udate { get; set; }
        public string program_id { get; set; }
        public decimal quantity { get; set; }
        public decimal? price_amount { get; set; }

        public PalletModel() { }
        public PalletModel(INVENTORY_PALLET_HEADER m)
        {
            if(m==null) return;
            serial_number = m.serial_number;
            id_inventory = m.id_inventory;
            user_no = m.user_no;
            pallet_status = m.pallet_status;
            storage_location_name = m.storage_location_name;
            cuser = m.cuser;
            cdate = m.cdate;
            uuser = m.cuser;
            udate = m.udate;
            program_id = m.program_id;
        }

        public INVENTORY_PALLET_HEADER ToModel()
        {
            return new INVENTORY_PALLET_HEADER
            {
                serial_number = serial_number,
                id_inventory = id_inventory,
                user_no = user_no,
                pallet_status = pallet_status,
                storage_location_name = storage_location_name,
                cuser = cuser,
                cdate = cdate,
                uuser = uuser,
                udate = udate,
                program_id = program_id
            };

        }

}

}
