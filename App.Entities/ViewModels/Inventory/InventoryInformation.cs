﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App.Entities.ViewModels.Inventory
{
    public class InventoryInformation
    {
        public int LastLabelInventory { get; set; }
        public int GetInventoryPalletsPreInventoried { get; set; }
        public int GetInventoryPalletsCancelled { get; set; }
        public int GetInventoryPalletsFinished { get; set; }
        public decimal GetInventoryDetailCountItems { get; set; }
        public List<InventoryUsersModel> InventoryPallets { get; set; }
        public decimal InventoryTotalPriceAmount { get; set; }
        public decimal InventoryTotalPriceAmountTeoric { get; set; }
    }
}