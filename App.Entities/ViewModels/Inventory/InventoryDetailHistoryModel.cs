﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App.Entities.ViewModels.Inventory
{
    public class InventoryDetailHistoryModel
    {
        public string IdInventoryHeader { get; set; }
        public int IdHistory { get; set; }
        public decimal Quantity { get; set; }
        public decimal BeforeQuantity { get; set; }
        public string PartNumber { get; set; }
        public string PartDescription { get; set; }
        public string StorageLocation { get; set; }
        public string Cuser { get; set; }
        public string Cdate { get; set; }
    }
}
