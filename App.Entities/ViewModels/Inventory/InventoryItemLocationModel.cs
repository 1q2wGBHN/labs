﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App.Entities.ViewModels.Inventory
{
    public class InventoryItemLocationModel
    {
        public string part_number { get; set; }
        public string part_description { get; set; }
        public decimal quantity { get; set; }
        public string product_name { get; set; }
        public string cuser { get; set; }
        public string captured { get; set; }
        public string id_inventory_header { get; set; }
        public string storage_location { get; set; }
        public decimal cost { get; set; }
        public decimal amount { get { return quantity * cost; } }
        public string cdate { get; set; }

    }
}
