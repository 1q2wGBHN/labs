﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App.Entities.ViewModels.Inventory
{
    public class InventoryCurrentOptionModel
    {
        public List<InventoryHeaderModel> ListInventoryHeader { get; set; }
        public bool IsExistInventoryActive { get; set; }
        public bool IsRoleCancele { get; set; }
    }
}
