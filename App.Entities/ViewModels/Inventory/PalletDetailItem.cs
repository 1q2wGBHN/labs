﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App.Entities.ViewModels.Inventory
{
    public class PalletDetailItem
    {
        public string id_inventory_header { get; set; }
        public string part_number { get; set; }
        public string product_name { get; set; }
        public decimal quantity { get; set; }
        public string name_user { get; set; }
        public string storage_location { get; set; }
        public decimal? price_amount { get; set; }
        public string barcode { get; set; }
        public int id_detail { get; set; }
    }
}
