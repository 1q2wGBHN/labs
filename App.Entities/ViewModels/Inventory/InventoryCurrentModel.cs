﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App.Entities.ViewModels.Inventory
{
    public class InventoryCurrentModel
    {
        public int IdInventoryHeader { get; set; }
        public string PartNumber { get; set; }
        public string PartDescription { get; set; }
        public decimal Quantity { get; set; }
        public string Location { get; set; }
        public decimal Stock { get; set; }
        public decimal MapPrice { get; set; } //item valuation
        public string userName { get; set; }
        public string dateSync { get; set; }
        public string startDate { get; set; }
        public string endDate { get; set; }
    }
}
