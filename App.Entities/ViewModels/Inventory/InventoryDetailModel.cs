﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App.Entities.ViewModels.Inventory
{
    public class InventoryDetailModel
    {
        public List<INVENTORY_DETAIL> InventoryDetails { get; set; }        
    }
}
