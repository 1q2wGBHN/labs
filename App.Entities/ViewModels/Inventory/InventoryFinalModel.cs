﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App.Entities.ViewModels.Inventory
{
    public class InventoryFinalModel
    {
        public string part_number { get; set; }
        public decimal counted { get; set; }
        public string part_description { get; set; }
        public decimal total_amount { get; set; }
    }
}
