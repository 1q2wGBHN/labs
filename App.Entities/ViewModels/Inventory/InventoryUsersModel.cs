﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App.Entities.ViewModels.Inventory
{
    public class InventoryUsersModel
    {
        public int id_inventory_header { get; set; }
        public int user_no { get; set; }
        public string user_name { get; set; }
        public string emp_no { get; set; }
        public int PalletsQuantity { get; set; }
    }
}
