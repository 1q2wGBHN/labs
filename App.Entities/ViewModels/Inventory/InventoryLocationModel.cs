﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App.Entities.ViewModels.Inventory
{
    public class InventoryLocationModel
    {
        public string storage_location { get; set; }
        public string bin1 { get; set; }
        public string bin2 { get; set; }
        public string bin3 { get; set; }
        public string inventory_class { get; set; }
        public bool inventory_status { get; set; }
        public string cuser { get; set; }
        public DateTime cdate { get; set; }
        public string uuser { get; set; }
        public DateTime udate { get; set; }
        public List<int> id_inventory_type { get; set; }

        // PARA OBTENER TIPOS DE INVENTARIO
        public int inventory_type_id { get; set; }
        public string inventory_name { get; set; }
        public int id_location { get; set; }

        public List<InventoryLocationModel> inventory_location_type { get; set; }

    }
}
