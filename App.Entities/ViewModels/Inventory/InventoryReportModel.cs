﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App.Entities.ViewModels.Inventory
{
    public class InventoryReportModel
    {
        public string part_number { get; set; }
        public string department { get; set; }
        public string StorageDescription { get; set; }
        public string part_description { get; set; }
        public decimal quantity_current { get; set; }
        public decimal quantity_detail { get; set; }
        public string family { get; set; }
        public string idInventory_header_current { get; set; }
        public string serial_number { get; set; }
        public string part_number_detail { get; set; }
        public string inventory_user { get; set; }
        public string emp_no { get; set; }
        public decimal quantity_adjusten { get; set; }
        public decimal quantity_adjusten_difference { get; set; }
        public int IdInventoryDetail { get; set; }
        public decimal quantityScrap { get; set; }
        public int adjustment_status { get; set; }
        public decimal amountScrap { get; set; }
        public string lastUser { get; set; }
        public decimal quantity_difference
        {
            get
            {
                if (adjustment_status == 9)
                    return quantity_adjusten - quantity_current;
                else
                    return quantity_detail - quantity_current;
            }
        }
        public string location { get; set; }
        public decimal map_price { get; set; }
        public decimal differenceAmount
        {
            get
            {
                return map_price * quantity_difference;
            }
        }
        public decimal currentAmount
        {
            get
            {
                return map_price * quantity_current;
            }
        }
        public decimal detailAmount
        {
            get
            {
                return map_price * quantity_detail;
            }
        }
        public string supplier { get; set; }
        public decimal difScrapQuantity
        {
            get
            {
                return quantity_difference - quantityScrap;
            }
        }
        public decimal difScrapAmount
        {
            get
            {
                return differenceAmount - amountScrap;
            }
        }
    }
}