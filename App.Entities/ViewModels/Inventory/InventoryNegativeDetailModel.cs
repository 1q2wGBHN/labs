﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App.Entities.ViewModels.Inventory
{
    public class InventoryNegativeDetailModel
    {
        public int id_inventory_negative { get; set; }
        public string part_number_detail { get; set; }
        public string part_description { get; set; }
        public decimal cost { get; set; }
        public decimal unrestricted { get; set; }
        public decimal unrestricted_actual { get; set; }
        public decimal quantity { get; set; }
        public decimal amount { get { return quantity * cost; } }
        public string document_adjustment { get; set; }
        public string cuser { get; set; }
    }
}
