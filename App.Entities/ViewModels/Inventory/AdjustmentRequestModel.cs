﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App.Entities.ViewModels.Inventory
{
    public class AdjustmentRequestModel
    {
        public string InventoryHeaderId { get; set; }
        public string InventoryName { get; set; }
        public string PartNumber { get; set; }
        public string ProductDescription { get; set; }
        public decimal PreviousQuantity { get; set; }
        public decimal NewQuantity { get; set; }
        public string Justification { get; set; }        
        public string File { get; set; }
        public string FileName { get; set; }
        public string Document { get; set; }
        public decimal MapPrice { get; set; }
        //parametros nuevo agregados
        public string Cuser { get; set; }
        public string Uuser { get; set; }
        public int AdjustmentStatus { get; set; }
        public decimal unrestricted { get; set; }
    }
}
