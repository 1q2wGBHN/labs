﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App.Entities.ViewModels.Inventory
{
    public class InventoryLabelModel
    {
        public string id_inventory { get; set; }
        public string serial_partnumer { get; set; }
        public int last_number { get; set; }
        public string cuser { get; set; }

    }
}
