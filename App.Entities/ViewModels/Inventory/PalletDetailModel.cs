﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FloridoERPTX.Controllers.Inventory
{
    public class PalletDetailModel
    {
       
            public int id_detail { get; set; }
            public string serial_number { get; set; }
            public string part_number { get; set; }
            public string barcode { get; set; }
            public decimal quantity { get; set; }
            public string cuser { get; set; }
            public DateTime? cdate { get; set; }
            public string uuser { get; set; }
            public DateTime? udate { get; set; }
            public string program_id { get; set; }
            public string description { get; set; }
    }
}