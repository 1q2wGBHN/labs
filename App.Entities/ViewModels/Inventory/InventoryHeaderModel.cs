﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App.Entities.ViewModels.Inventory
{
    public class InventoryHeaderModel
    {
        public string id_inventory_header { get; set; }
        public string inventory_name { get; set; }
        public string inventory_description { get; set; }
        public int inventory_status { get; set; }
        public DateTime inventory_date_start { get; set; }
        public DateTime? inventory_date_end { get; set; }
        public int inventory_type { get; set; }
        public string inventory_type_name { get; set; }
        public int countPallets { get; set; }
        public DateTime? endDate { get; set; }
        public List<InventoryHeaderModel> InventoryHeaderModellist { get; set; }
    }
}
