﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App.Entities.ViewModels.Inventory
{
    public class InventoryNegativeHeaderModel
    {
        public string part_number_header { get; set; }
        public string part_description { get; set; }
        public decimal cost_header { get; set; }
        public decimal unrestricted_header { get; set; }
        public decimal packing_size { get; set; }
        public decimal counted { get; set; }
        public decimal total_quantity
        {
            get
            {
                return counted + (unrestricted_header * -1);
            }
        }
        public decimal total_amount
        {
            get
            {
                return total_quantity * cost_header;
            }
        }
        public int negative_status { get; set; }
        public string user_approval { get; set; }
        public string document_adjustment { get; set; }
        public string cuser { get; set; }
        public string reason { get; set; }
        public decimal unrestricted_actual { get; set; }
        public int id_inventory_negative { get; set; }
        public string negative_date { get; set; }
        public decimal amount_detail { get; set; }
        public string part_number_detail { get; set; }
        public string part_description_detail { get; set; }
        public decimal unrestricted_detail { get; set; }
        public decimal cost_detail { get; set; }
        public decimal quantity_detail { get; set; }
        public decimal difference_negative
        {
            get
            {
                return total_amount - amount_detail;
            }
        }

    }
}
