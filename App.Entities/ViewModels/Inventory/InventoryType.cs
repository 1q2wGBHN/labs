﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App.Entities.ViewModels.Inventory
{
    public class InventoryType
    {
        public string day { get; set; }
        public string type_review { get; set; }
        public DateTime start_date { get; set; }
        public string inventory_name { get; set; }
        public int id_inventory_type { get; set; }
    }
}
