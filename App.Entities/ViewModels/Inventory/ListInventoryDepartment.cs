﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using App.Entities.ViewModels.Inventory;

namespace App.Entities.ViewModels.Inventory
{
    public class ListInventoryDepartment
    {
        public string Department { get; set; }
        public InventoryReportModel InventoryReport { get; set; }
    }
}
