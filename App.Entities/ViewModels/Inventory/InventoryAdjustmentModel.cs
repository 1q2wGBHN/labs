﻿using System;

namespace App.Entities.ViewModels.Inventory
{
    public class InventoryAdjustmentModel
    {
        public string id_inventory_header { get; set; }
        public string part_number { get; set; }
        public string part_description { get; set; }
        public decimal previous_quantity { get; set; }
        public decimal new_quantity { get; set; }
        public string justification { get; set; }
        public string evidence_document_base64 { get; set; }
        public string evidence_document_ext { get; set; }
        public int adjustment_status { get; set; }
        public string cuser { get; set; }
        public DateTime cdate { get; set; }
        public string uuser { get; set; }
        public DateTime? udate { get; set; }
        public string program_id { get; set; }
        public decimal quantity_stock { get; set; }
        public decimal quantity_difference
        {
            get
            {
                return (quantity_stock - previous_quantity);
            }
        }

    }
}