﻿using App.Entities.ViewModels.StorageLocation;
using App.Entities.ViewModels.MaClass;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App.Entities.ViewModels.Inventory
{
    public class InventoryDifferentModel
    {
        public List<MA_CLASS> Family { get; set; }
        //public List<StorageLocationModel> Location { get; set; }
        public List<PlanogramLocationModel> Location { get; set; }
        public List<InventoryHeaderModel> Inventory { get; set; }
        public List<MaClassModel> FamilyActive { get; set; }
    }
}
