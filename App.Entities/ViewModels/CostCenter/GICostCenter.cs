﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App.Entities.ViewModels.CostCenter
{
    public class GICostCenter
    {
        public int GiCostCenterId { get; set; }
        public string OriginSite { get; set; }
        public string DestinationSite { get; set; }
        public string UserAuthorize { get; set; }
        public string CostCenter { get; set; }
        public string GIDocument { get; set; }
        public DateTime? GIDate { get; set; }
        public decimal Amount { get; set; }
        public decimal Ieps { get; set; }
        public decimal Iva { get; set; }
        public string Remark { get; set; }
        public string Status { get; set; }
        public string UserCreate { get; set; }
        public DateTime CreateDate { get; set; }
        public List<GICostCenterItem> Items { get; set; }

        public GICostCenter()
        {
            Items = new List<GICostCenterItem>();
        }
    }
}