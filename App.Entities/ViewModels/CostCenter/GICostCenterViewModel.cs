﻿using App.Entities.ViewModels.MaCode;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App.Entities.ViewModels.CostCenter
{
    public class GICostCenterViewModel
    {
        public List<GICostCenter> GICostCenterRecords { get; set; }
        public List<MA_CODE> CostCenters { get; set; }

        public GICostCenterViewModel()
        {
            GICostCenterRecords = new List<GICostCenter>();
            CostCenters = new List<MA_CODE>();
        }
    }
}