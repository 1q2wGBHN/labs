﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App.Entities.ViewModels.CostCenter
{
    public class GICostCenterItem
    {
        public int GiCostCenterId { get; set; }
        public string CostCenter { get; set; }
        public string PartNumber { get; set; }
        public string Description { get; set; }
        public string Department { get; set; }
        public string Family { get; set; }
        public decimal Quantity { get; set; }
        public decimal UnitCost { get; set; }
        public decimal Ieps { get; set; }
        public decimal Iva { get; set; }
        public string GiDocument { get; set; }
        public string ReverseGiDocument { get; set; }
        public string UserCreate { get; set; }
        public DateTime CreateDate { get; set; }
        public string Status { get; set; }
        public decimal Amount { get; set; }
        public decimal Total { get; set; }
    }
}