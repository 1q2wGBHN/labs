﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App.Entities.ViewModels.DamagedsGoods
{
    public class DamagedsGoodsModel
    {
        public int id_single { get; set; }
        public string PartNumber { get; set; }
        public string Description { get; set; }
        public string Size { get; set; }
        public decimal Block { get; set; }
        public string Storage { get; set; }
        public decimal Quantity { get; set; }
        public decimal RealQuantity { get; set; }
        public decimal Price { get; set; }
        public decimal IEPS { get; set; }
        public decimal IVA { get; set; }
        public decimal Amount { get; set; }
        public decimal TotalAmount { get; set; }
        public decimal Weight { get; set; }
        public string ScrapReason { get; set; }
        public string SiteName { get; set; }
        public string Document { get; set; }
        public string Fecha { get; set; }
        public string User { get; set; }
        public int Status { get; set; }
        public int Print_Status { get; set; }
        public string Date { get; set; }
        public string Department { get; set; }
        public string UnitSize { get; set; }
        public string Family { get; set; }
    }
}
