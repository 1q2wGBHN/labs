﻿using App.Entities.ViewModels.Item;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App.Entities.ViewModels.DamagedsGoods
{
    public class DamagedGoodsBanatiModel
    {
        public int SupplierId { get; set; }
        public string Comment { get; set; }
        public string PartNumber { get; set; }
        public decimal Quantity { get; set; }
        public decimal? Stock { get; set; }
        public string Description { get; set; }
        public decimal? Price { get; set; }
        public decimal? Amount_total { get; set; }
    }
}