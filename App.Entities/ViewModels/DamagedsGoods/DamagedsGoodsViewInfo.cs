﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App.Entities.ViewModels.DamagedsGoods
{
    class DamagedsGoodsViewInfo
    {
        public string PartNumber{ get; set; }
        public string Description { get; set; }
        public string Size { get; set; }
        public string Storage { get; set;  }
        public decimal Quantity { get; set; }
        public decimal Amount_Doc { get; set; }
        public decimal price { get; set; }

    }
}
