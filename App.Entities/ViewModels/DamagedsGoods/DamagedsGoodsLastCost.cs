﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App.Entities.ViewModels.DamagedsGoods
{
    public class DamagedsGoodsLastCost
    {
        public decimal? t_cost { get; set; }
    }
}
