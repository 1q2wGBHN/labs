﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App.Entities.ViewModels.DamagedsGoods
{
    public class EmailDamagedGoodsModel
    {
        public Stream Rerpote { get; set; }
        public string RMA { get; set; }

        public Stream ReporteExcel { get; set; }
    }
}
