﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App.Entities.ViewModels.DamagedsGoods
{
    public class DamagedsGoodsAndroid
    {
        public string part_number { get; set; }
        public string part_description { get; set; }
        public string quantity { get; set; }
        public decimal quantityDecimal { get; set; }
        public string price { get; set; }
        public string reason { get; set; }

        public List<DamagedsGoodsAndroid> Items { get; set; }
    }
}
