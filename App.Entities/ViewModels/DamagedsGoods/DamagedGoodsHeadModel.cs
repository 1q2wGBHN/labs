﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App.Entities.ViewModels.DamagedsGoods
{
    public class DamagedGoodsHeadModel
    {
        public List<DamagedsGoodsModel> DamageGoodsItem { get; set; }
        public List<DamagedsGoodsAndroid> DamageGoodsItemAndroid { get; set; }
        public DamagedGoodsHeadModel()
        {
            DamageGoodsItem = new List<DamagedsGoodsModel>();
            DamageGoodsItemAndroid = new List<DamagedsGoodsAndroid>();
        }
        public string DamagedGoodsDoc { get; set; }
        public string Site { get; set; }
        public string ProcessType { get; set; }
        public int SupplierId { get; set; }
        public string Supplier { get; set; }
        public int ProcessStatus { get; set; }
        public int Print_Document { get; set; }
        public string ProcessStatusDescription { get; set; }
        public string Comment { get; set; }
        public string SiteName { get; set; }
        public string SiteAdress { get; set; }
        public string cuser { get; set; }
        public DateTime cdate { get; set; }
        public string Uuser { get; set; }
        public string BuyerDivision { get; set; }
        public DateTime Udate { get; set; }
        public decimal? Weight { get; set; }
        public string document_no { get; set; }
        public decimal Total { get; set; }
        public DateTime? processDate { get; set; }
    }
}
