﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App.Entities.ViewModels.DamagedsGoods
{
    public class DamagedGoodsViewModel
    {
        public DamagedGoodsHeadModel DamagedGoodsHeadModel { get; set; }
        public List<DamagedGoodsHeadModel> DamagedGoodsHeadListModel { get; set; }
        public List<DamagedGoodsBanatiModel> DamagedGoodsBanatiModel { get; set; }

        public DamagedGoodsViewModel()
        {
            DamagedGoodsHeadModel = new DamagedGoodsHeadModel();
            DamagedGoodsHeadListModel = new List<DamagedGoodsHeadModel>();
            DamagedGoodsBanatiModel = new List<DamagedGoodsBanatiModel>();
        }
    }
}