﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App.Entities.ViewModels.Sanctions
{
    public class SanctionIndex
    {
        public SanctionIndex()
        {
            CanCancel = false;
            status = Status.PAGADA;
        }

        [DisplayName("Fecha de Aplicación: ")]
        public string IssueDate { get; set; }
        [DisplayName("Entre: ")]
        public string CreateDate { get; set; }
        [DisplayName("Y: ")]
        public string CreateDate2 { get; set; }
        [DisplayName("Motivo: ")]
        public string Motive { get; set; }
        [DisplayName("Comentarios: ")]
        public string Comments { get; set; }
        [DisplayName("Empleado: ")]
        public string emp { get; set; }
        public int emp_no { get; set; }
        [DisplayName("Importe Sanción: ")]
        public string total { get; set; }
        [DisplayName("Estatus: ")]
        public Status status { get; set; }
      
        [DisplayName("Moneda: ")]
        public string currency { get; set; }
        public int statusValue { get; set; }

        public string currencyValue { get; set; }
        public List<SanctionModel> SanctionsList { get; set; }
        public bool CanCancel { get; set; }


    }
    public enum Currencys
    {

        [Display(Name = "Pesos  (MXN)")]
        MXN,
        [Display(Name = "Dolares  (USD)")]
        USD
    }
    [DefaultValue(1)]
    public enum Status
    {

        [Display(Name = "Pendiente")]
        PENDIENTE = -1,
        [Display(Name = "Pagada")]
        PAGADA = 1
    }
}
