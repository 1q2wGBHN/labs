﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App.Entities.ViewModels.Sanctions
{
   
    public class SanctionModel


    {
        public SanctionModel()
        {
            CreateDate = DateTime.Now.Date.ToString("dd/MM/yyyy");
           
        }
        public long SanctionId { get; set; }
        [DisplayName("Fecha: ")]
        public string IssueDate { get; set; }
      
        public string CreateDate { get; set; }
        public string CreateTime { get; set; }
        [DisplayName("Motivo: ")]
        public string Motive { get; set; }
        public string MotiveNo { get; set; }
        [DisplayName("Comentarios: ")]
        public string Comments { get; set; }

        [DisplayName("Empleado: ")]
        public string emp { get; set; }
        public int emp_no { get; set; }
        [DisplayName("Importe: ")]
        public string total { get; set; }
        public bool IsCancel { get; set; }
        public int status { get; set; }
        public string statusVal { get; set; }
        [DisplayName("Moneda: ")]
        public string currencyValue { get; set; }
        [DisplayName("Estatus: ")]
        public StatusEdit statusList { get; set; }

    }
    public enum StatusEdit
    {

        [Display(Name = "Pendiente")]
        PENDIENTE=-1,
        [Display(Name = "Pagada")]
        PAGADA=1,
        [Display(Name = "Cancelada")]
        CANCELADA=0
    }
}
