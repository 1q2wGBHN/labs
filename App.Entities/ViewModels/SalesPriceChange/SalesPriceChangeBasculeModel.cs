﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App.Entities.ViewModels.SalesPriceChange
{
    public class SalesPriceChangeBasculeModel
    {
        public string part_number { get; set; }
        public string part_description { get; set; }
        public string department { get; set; }
        public string expiration_days { get; set; }
        public decimal Price { get; set; }
        public int part_caducidad { get; set; }
        public int Weight_flag { get; set; }

        //TABLE WEB
        public string part_number_origin { get; set; }
        public int level_header { get; set; }
        public int level1_code { get; set; }
        public int level2_code { get; set; }
        public bool? flag_visible { get; set; }
        public DateTime? last_update { get; set; }



    }
}
