﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using App.Entities.ViewModels.ItemPresentation;

namespace App.Entities.ViewModels.SalesPriceChange
{
    public class SalePriceChangeModel
    {
        public int sales_price_id;
        public string part_number;
        public string part_description;
        public string applied_date;
        public decimal sale_price;
        public decimal existencias;
        public string Department { get; set; }
        public string Family { get; set; }
        public string barcode { get; set; }
        public ItemPresentationModel presentation { get; set; }

        public PromotionItemModel promotion_spc { get; set; }
        public SalesPriceChangeForcedPrice forced { get; set; }
        public int? presentation_id { get; set; }
        public string unit_size { get; set; }
        public PromotionItemModel promotion { get; set; }
        public int pchanstatus { get; set; }
    }

    public class PromotionItemModel
    {
        public decimal promotion_price { get; set; }
        public string start_date { get; set; }
        public string end_date { get; set; }
        public DateTime start_date_date { get; set; }
    }

    public class SalesPriceChangeForcedPrice
    {
        public decimal sale_price { get; set; }
    }
}
