﻿namespace App.Entities.ViewModels.SalesPriceChange
{
    public class SalesPriceChangeDataReport
    {
        public string ItemNumber { get; set; }
        public string ItemDescription { get; set; }
        public decimal Quantity { get; set; }
        public decimal OriginalPrice { get; set; }
        public decimal TotalOriginalPrice { get; set; }
        public decimal NewPrice { get; set; }
        public decimal TotalNewPrice { get; set; }
        public decimal Difference { get; set; }
        public decimal Percentage { get; set; }
        public long SaleNumber { get; set; }
        public string EmployeeName { get; set; }
        public string Concept { get; set; }
        public string Date { get; set; }
    }
}
