﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App.Entities.ViewModels.SalesPriceChange
{
    public class SalePriceChangeIndex
    {
        public SalePriceChangeIndex()
        {
            IsFromMenu = true;
            MinPercentage = 0;
            MaxPercentage = 100;
            StartDate = DateTime.Now.Date.ToString("dd/MM/yyyy");
            EndDate = DateTime.Now.Date.ToString("dd/MM/yyyy");
            DataReport = new List<SalesPriceChangeDataReport>();
        }

        public bool IsFromMenu { get; set; }

        [DisplayName("Fecha Inicial: ")]
        public string StartDate { get; set; }

        [DisplayName("Fecha Final: ")]
        public string EndDate { get; set; }

        [DisplayName("Min: ")]
        public decimal MinPercentage { get; set; }

        [DisplayName("Max: ")]
        public decimal MaxPercentage { get; set; }

        public List<SalesPriceChangeDataReport> DataReport { get; set; }
    }
}
