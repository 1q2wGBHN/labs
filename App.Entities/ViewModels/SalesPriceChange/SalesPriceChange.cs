﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App.Entities.ViewModels.SalesPriceChange
{
    public class SalesPriceChange
    {
        public string part_number { get; set; }
        public string part_description { get; set; }
        public string sale_price { get; set; }

        public SalesPriceChange(string part_number, string part_description, string sale_price)
        {
            this.part_number = part_number;
            this.part_description = part_description;
            this.sale_price = sale_price;
        }
        public SalesPriceChange() { }

        public string PART_NUMBER
        {
            get { return part_number; }
            set { part_number = value; }
        }

        public string PART_DESCRIPTION
        {
            get { return part_description; }
            set { part_description = value; }
        }
        public string SALE_PRICE
        {
            get { return sale_price; }
            set { sale_price = value; }
        }
    }
}
