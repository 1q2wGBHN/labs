﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App.Entities.ViewModels.Payments
{
    public class PaymentCustomerReportModel
    {
        public string ClientCode { get; set; }
        public string ClientName { get; set; }
        public string PaymentDate { get; set; }
        public decimal PaymentAmount { get; set; }
        public string InvoiceNo { get; set; }
        public string InvoiceDate { get; set; }
        public decimal InvoiceSubTotal { get; set; }
        public decimal InvoiceTax { get; set; }
        public decimal InvoiceTotal { get; set; }
        public string CUser { get; set; }
        public string PaymentNo { get; set; }
        public string xml { get; set; }
        public string Serie { get; set; }
        public long Number { get; set; }
    }
}
