﻿using System.ComponentModel;

namespace App.Entities.ViewModels.Payments
{
    public class PaymentIndex
    {
        public PaymentIndex()
        {
            IsPayment = true;
        }

        [DisplayName("Buscar Cliente")]
        public string CustomerCode { get; set; }
        public bool IsPayment { get; set; }
        public long PaymentPMId { get; set; }
    }
}
