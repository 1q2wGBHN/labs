﻿using System;

namespace App.Entities.ViewModels.Payments
{
    public class PaymentMethod
    {
        public decimal Amount { get; set; }
        public string PaymentDate { get; set; }
        public long PaymentPMId { get; set; }
        public string ViewPaymentPMId { get; set; }
        public int PaymentMethodId { get; set; }
        public string OperationNumber { get; set; }
        public int? Bank { get; set; }
        public string BankReference { get; set; }
        public string BankName { get; set; }
    }
}
