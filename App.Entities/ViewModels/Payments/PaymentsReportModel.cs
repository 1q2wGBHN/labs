﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App.Entities.ViewModels.Payments
{
   public  class PaymentsReportModel
    {
        [DisplayName("Fecha Inicial:")]
        public string StartDate { get; set; }

        [DisplayName("Fecha Final:")]
        public string EndDate { get; set; }

        public List <PaymentsDetail> PaymentsList{ get; set; }
    }

    public class PaymentsDetail
    {
      
        public string ClientName { get; set; }
        public string DateCreate { get; set; }
        public string DateUpdate { get; set; }
        public decimal Total { get; set; }
        public string UsuarioCreate { get; set; }
        public string UsuarioUpdate { get; set; }
        public string paymentNo { get; set; }
        public string PaymentSerie { get; set; }
       
    }
}
