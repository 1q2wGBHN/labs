﻿namespace App.Entities.ViewModels.Payments
{
    public class PaymentsCustomer
    {
        public long PMId { get; set; }
        public string Receipt { get; set; }
        public string ReceiptDate { get; set; }
        public string PaymentMethod { get; set; }
        public string OperationNumber { get; set; }
        public decimal Amount { get; set; }
        public string PaymentDate { get; set; }
        public string PaymentDateCancel { get; set; }
    }
}
