﻿using App.Entities.ViewModels.Common;
using App.Entities.ViewModels.Customers;
using App.Entities.ViewModels.Invoices;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using static App.Entities.ViewModels.Common.Constants;

namespace App.Entities.ViewModels.Payments
{
    public class Payment
    {       
        public Payment()
        {
            Date = DateTime.Now.Date.ToString("dd/MM/yyyy");
            AmountPaymentDate = DateTime.Now.Date.ToString("dd/MM/yyyy");
            IsPayment = true;
            Customer = new CustomerViewModel();
            DocumentType = VoucherType.PAYMENTS;
            HasSerieAndNumber = true;            
        }

        public CustomerViewModel Customer { get; set; }

        [DisplayName("Último Pago: ")]
        public string LastPayment { get; set; }

        [DisplayName("Última Compra: ")]
        public string LastBuy { get; set; }

        [DisplayName("Fecha: ")]
        public string Date { get; set; }

        [DisplayName("Referencia: ")]
        public string Reference { get; set; }

        [DisplayName("Total Pago: ")]
        public decimal TotalPayment { get; set; }

        [DisplayName("Total Facturas: ")]
        public decimal TotalInvoices { get; set; }

        [DisplayName("Monto: ")]
        public decimal AmountPayment { get; set; }

        [DisplayName("Fecha Pago: ")]
        public string AmountPaymentDate { get; set; }

        [DisplayName("Saldo: ")]
        public string BalanceDisplay { get; set; }

        [DisplayName("Monto: ")]
        public decimal AmountInvoice { get; set; }

        [DisplayName("No. de Operación: ")]
        public string OperationNumber { get; set; }

        [DisplayName("Número de Recibo Previo: ")]
        public string PreviousReceipt { get; set; }

        [DisplayName("Monto Restante:")]
        public decimal Difference { get; set; }

        [DisplayName("Banco: ")]
        public string Bank { get; set; }

        [DisplayName("No. Referencia:")]
        public string BankReference { get; set; }

        public string Serie { get; set; }
        public long Number { get; set; }
        public int PaymentMethod { get; set; }
        public string Invoice { get; set; }
        public string MinInvoiceDate { get; set; }
        public string User { get; set; }
        public decimal Balance { get; set; }
        public string Currency { get; set; }
        public bool IsPayment { get; set; }
        public long PaymentPMId { get; set; }
        public long PreviousPaymentNumber { get; set; }
        public string DocumentType { get; set; }
        public bool IsStamped { get; set; }
        public string PdfEncoded { get; set; }

        public bool HasSerieAndNumber { get; set; }

        public List<PaymentMethod> Payments { get; set; }
        public List<InvoiceBalanceDetails> Invoices { get; set; }
        public List<PaymentsMethods> PaymentMethods { get; set; }        
    }
}
