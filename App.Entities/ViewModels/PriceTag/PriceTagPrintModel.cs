﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using App.Entities.ViewModels.ItemPresentation;
using App.Entities.ViewModels.SalesPriceChange;

namespace App.Entities.ViewModels.PriceTag
{
    public class PriceTagPrintModel
    {
        public int PrintId { get; set; }
        public bool Printed { get; set; }
        public string CUser { get; set; }
        public String CDate { get; set; }

        public string UUser { get; set; }
        public String UDate { get; set; }
        public IEnumerable<PriceTagPrintDetailsModel> Tags { get; set; }
        public bool Fav { get; set; }
        public string Name { get; set; }
        public int TagCount { get; set; }
    }

    public class PriceTagPrintDetailsModel
    {
        public int PrintId { get; set; }
        public string Barcode { get; set; }
        public string PartDescription { get; set; }
        public decimal PartPrice { get; set; }
        public int SequenceNum { get; set; }
        public ItemPresentationModel Presentation { get; set; }
        public int copies { get; set; }
        public string UnitSize { get; set; }
        public PromotionItemModel promotion { get; set; }
        public string PartNumber { get; set; }

    }

}
