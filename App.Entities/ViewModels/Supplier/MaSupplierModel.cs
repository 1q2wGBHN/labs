﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App.Entities.ViewModels.Supplier
{
    public class MaSupplierModel
    {
        // MA_SUPPLIER
        public int SupplierId { get; set; }
        public string Rfc { get; set; }
        public string CommercialName { get; set; }
        public string BusinessName { get; set; }
        public string SupplierAddress { get; set; }
        public string City { get; set; }
        public string ZipCode { get; set; }
        public string supplier_state { get; set; }
        public string Country { get; set; }
        public string SupplierType { get; set; }
        public string AccountingAccount { get; set; }
        public int PurchaseAppliedReturn { get; set; }
        public string Program { get; set; }
        public string Cser { get; set; }
        public string Cdate { get; set; }
        public string Uuser { get; set; }
        public string Udate { get; set; }
        public int truck_food { get; set; }

        //MA_SUPPLIER_CONTACT
        public int supplier_contact_id { get; set; }
        public string name { get; set; }
        public string last_name { get; set; }
        public string phone { get; set; }
        public string email { get; set; }
        public string departament { get; set; }
    }
}
