﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App.Entities.ViewModels.CodesPackages
{
    public class CodesPackagesModel
    {
        public int Id { get; set; }
        public string OriginPartNumber { get; set; }
        public string OriginPartDescription { get; set; }
        public string UnitSize { get; set; }
        public string PartNumber { get; set; }
        public string PartDescription { get; set; }
        public decimal Quantity { get; set; }
        public decimal Cost { get; set; }
        public bool Active { get; set; }

        public CodesPackagesModel()
        {

        }
    }
}