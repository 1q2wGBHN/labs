﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App.Entities.ViewModels.Order
{
    public class OrderListDataModel
    {
        public int Id { get; set; }
        public int Supplier { get; set; }
        public int Authorized { get; set; }
    }
}
