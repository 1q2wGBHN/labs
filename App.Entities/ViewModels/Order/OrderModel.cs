﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App.Entities.ViewModels.Order
{
    public class OrderModel
    {
        public int Folio { get; set; }
        public int SupplierId { get; set; }
        public string Supplier { get; set; }
        public string SiteId { get; set; }
        public string Site { get; set; }
        public int RefillDays { get; set; }
        public int Status { get; set; }
        public string Date { get; set; }
        public string Comment { get; set; }
        public string PartNumber { get; set; }
        public decimal Authorized { get; set; }
    }
}
