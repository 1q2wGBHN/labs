﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App.Entities.ViewModels.Order
{
    public class OrderListModel
    {
        public long Id { get; set; }
        public string ParthNumber { get; set; }
        public string BarCode { get; set; }
        public string Department { get; set; }
        public int Dep { get; set; }
        public string Family { get; set; }
        public int Fam { get; set; }
        public string Description { get; set; }
        public decimal Stock { get; set; }
        public decimal PrePruchase { get; set; }
        public decimal AverageSale { get; set; }
        public decimal SuggestedSystems { get; set; }
        public decimal SuggestedSupplier { get; set; }
        public int Authorize { get; set; }
        public int Folio { get; set; }
        public decimal Packing { get; set; }
        public string Currency { get; set; }
    }
}
