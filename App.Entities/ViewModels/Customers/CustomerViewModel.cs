﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;

namespace App.Entities.ViewModels.Customers
{
    public class CustomerViewModel
    {
        public CustomerViewModel()
        {
            Status = true;
            Web = false;
            SiteCode = GetSiteCode();
            Country = "México";
            Credit_Days = 0;
            CanEdit = false;
        }
        public bool CanEdit { get; set; }
        [DisplayName("No. Tarjeta"), Required(ErrorMessage = "No Tarjeta Requerido")]
        public string Code { get; set; } // Numero de tarjeta El Florido

        [DisplayName("Razón Social"), Required(ErrorMessage = "Razón Social Requerido")]
        public string Name { get; set; }

        [DisplayName("RFC"), Required(ErrorMessage = "RFC Requerido")]
        public string RFC { get; set; }

        [DisplayName("Dirección")]
        public string Address { get; set; }

        [DisplayName("Dirección 2")]
        public string Address_2 { get; set; }

        [DisplayName("Ciudad o Municipio")]
        public int CityId { get; set; }

        [DisplayName("Estado")]
        public int StateId { get; set; }

        [DisplayName("Código Postal")]
        public string ZipCode { get; set; }

        [DisplayName("Teléfono")]
        public string Phone { get; set; }

        [DisplayName("Teléfono 2")]
        public string Phone_2 { get; set; }

        [DisplayName("Teléfono 3")]
        public string Phone_3 { get; set; }

        [EmailAddress(ErrorMessage = "Formato de email inválido")]
        [DisplayName("Email")]
        public string Email { get; set; }

        [DisplayName("Dirección de Envío")]
        public string Ship_To { get; set; }

        [DisplayName("Días de Crédito")]
        public short? Credit_Days { get; set; }

        [DisplayName("Limite de Crédito (MXP)")]
        public decimal? Credit_Limit { get; set; }

        [DisplayName("Limite de Crédito (USD)")]
        public decimal? Credit_Limit_Alt { get; set; }

        [DisplayName("Contacto")]
        public string Contact { get; set; }

        [DisplayName("Contacto 2")]
        public string Contact_2 { get; set; }

        [DisplayName("Cuenta C")]
        public string Cuenta_C { get; set; } //investigar que es Cuenta_C

        [DisplayName("País")]
        public string Country { get; set; }

        [DisplayName("Sitio Web")]
        public bool Web { get; set; }

        [DisplayName("Activo")]
        public bool Status { get; set; }

        public string SiteCode { get; set; }

        [DisplayName("Ciudad o Municipio")]
        public string CityName { get; set; }

        [DisplayName("Estado")]
        public string StateName { get; set; }

        public string GetSiteCode()
        {
            var _context = new DFL_SAIEntities();
            var SiteConfig = _context.SITE_CONFIG.FirstOrDefault();

            if (SiteConfig != null)
                return SiteConfig.site_code;

            return string.Empty;
        }

        public CUSTOMERS CUSTOMER
        {
            get
            {
                return new CUSTOMERS
                {
                    customer_code = Code,
                    customer_name = Name,
                    customer_rfc = RFC,
                    customer_address = Address,
                    customer_address2 = Address_2,
                    city_id = CityId,
                    state_id = StateId,
                    customer_zip = ZipCode,
                    customer_phone = Phone,
                    customer_phone2 = Phone_2,
                    customer_phone3 = Phone_3,
                    customer_email = Email,
                    customer_ship_to = Ship_To,
                    customer_credit_days = Credit_Days,
                    customer_credit_limit = Credit_Limit,
                    customer_credit_limit_alt = Credit_Limit_Alt,
                    customer_contact = Contact,
                    customer_contact2 = Contact_2,
                    customer_cuenta_c = Cuenta_C,
                    customer_country = Country,
                    customer_web = Web,
                    customer_status = Status,
                    site_code = SiteCode
                };
            }
        }
    }
}
