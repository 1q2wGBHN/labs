﻿namespace App.Entities.ViewModels.Customers
{
    public class CustomersDDL
    {
        public string RFC { get; set; }
        public string Code { get; set; }
        public string Name { get; set; }
        public bool Status { get; set; }
    }
}
