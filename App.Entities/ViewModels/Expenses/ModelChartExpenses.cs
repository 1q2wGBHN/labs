﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App.Entities.ViewModels.Expenses
{
    public class ModelChartExpenses
    {
        public List<ExpensesChart> CreditorsExpensive { get; set; }
        public List<ExpensesChart> CreditorsExpensiveQuantity { get; set; }
        public List<ExpensesChart> CreditorsExpensiveReferenceType { get; set; }

        public ModelChartExpenses()
        {
            CreditorsExpensive = new List<ExpensesChart>();
            CreditorsExpensiveQuantity = new List<ExpensesChart>();
            CreditorsExpensiveReferenceType = new List<ExpensesChart>();
        }
    }
}
