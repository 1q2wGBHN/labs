﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App.Entities.ViewModels.Expenses
{
    public class ExpenseModelFolio
    {
        public string invoice { get; set; }
        public string currency { get; set; }
        public decimal subtotal { get; set; }
        public decimal ieps { get; set; }
        public decimal iva { get; set; }
        public decimal total { get; set; }
        public decimal discount { get; set; }
        public decimal quantity { get; set; }
        public decimal ivaRetenido { get; set; }
        public decimal isrRetenidos { get; set; }
        public string comment { get; set; }
        public string area { get; set; }
        public int type_expense { get; set; }
        public int xml_id { get; set; }
    }
}
