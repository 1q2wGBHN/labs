﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App.Entities.ViewModels.Expenses
{
    public class ItemXMLDetail
    {

        public int id { get; set; }
        public int xml_id { get; set; }
        public DateTime? invoice_date { get; set; }
        public DateTime? cdate { get; set; }
        public string expense_type { get; set; }
        public string expense_type_exactly { get; set; }
        public string invoice { get; set; }
        public int print_status { get; set; }
        public int status { get; set; }
        public string cuser { get; set; }
        public string currency { get; set; }
        public string remark { get; set; }
        public int supplier_id { get; set; }
        public int expense_id { get; set; }
        public string item_no { get; set; }
        public string item_description { get; set; }
        public decimal subtotal { get; set; }
        public decimal quantity { get; set; }
        public decimal unit_cost { get; set; }
        public decimal item_amount { get; set; }
        public decimal discount { get; set; }
        public decimal iva { get; set; }
        public decimal ieps { get; set; }
        public decimal ieps_retained { get; set; }
        public decimal iva_retained { get; set; }
        public decimal isr_retained { get; set; }
        public decimal total_amount { get; set; }
    }
}
