﻿namespace App.Entities.ViewModels.Checkout
{
    public class CheckoutWithdrawal
    {
        public int pm_id { get; set; }
        public string pm_name { get; set; }
        public string currency { get; set; }
        public decimal Retiro { get; set; }
    }
}
