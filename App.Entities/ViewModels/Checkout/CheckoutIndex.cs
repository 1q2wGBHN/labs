﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App.Entities.ViewModels.Checkout
{
    public class CheckoutIndex
    {
        public CheckoutIndex()
        {
            IsFromMenu = true;
            Date = DateTime.Now.Date.ToString("dd/MM/yyyy");
            DataInfo = new CheckoutDataList();
        }

        public bool IsFromMenu { get; set; }

        [DisplayName("Cajero: ")]
        public string Cashier { get; set; }

        [DisplayName("Fecha: ")]
        public string Date { get; set; }

        public CheckoutDataList DataInfo { get; set; }
    }
}
