﻿using System;

namespace App.Entities.ViewModels.Checkout
{
    public class CheckoutExchangeRate
    {
        public decimal cur_rate1 { get; set; }
        public decimal cur_rate2 { get; set; }
        public DateTime cur_date { get; set; }
    }
}
