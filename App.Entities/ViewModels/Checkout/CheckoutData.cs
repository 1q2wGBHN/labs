﻿namespace App.Entities.ViewModels.Checkout
{
    public class CheckoutData
    {        
        public int pm_id { get; set; }
        public string pm_name { get; set; }
        public string currency { get; set; }
        public decimal Monto { get; set; }
        public decimal MontoIngresado { get; set; }
    }
}
