﻿namespace App.Entities.ViewModels.Checkout
{
    public class CheckoutDonations
    {
        public int pm_id { get; set; }
        public string pm_name { get; set; }
        public string currency { get; set; }
        public decimal Donativos { get; set; }
    }
}
