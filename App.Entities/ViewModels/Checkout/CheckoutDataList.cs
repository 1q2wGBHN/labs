﻿using System.Collections.Generic;

namespace App.Entities.ViewModels.Checkout
{
    public class CheckoutDataList
    {
        public CheckoutDataList()
        {
            Charge = new List<CheckoutData>();
            Returns = new List<CheckoutData>();
            Withdrawal = new List<CheckoutWithdrawal>();
            Donations = new List<CheckoutDonations>();
            ExchangeRate = new List<CheckoutExchangeRate>();
        }

        public List<CheckoutData> Charge { get; set; }
        public List<CheckoutData> Returns { get; set; }
        public List<CheckoutWithdrawal> Withdrawal { get; set; }
        public List<CheckoutDonations> Donations{ get; set; }
        public List<CheckoutExchangeRate> ExchangeRate { get; set; }

        public decimal TotalCharge { get; set; }
        public decimal TotalReturns { get; set; }
        public decimal TotalWithdrawal { get; set; }
        public decimal TotalDonations { get; set; }
        public decimal Balance { get; set; }
    }
}
