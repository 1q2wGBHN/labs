﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App.Entities.ViewModels.Configuration
{
    public class UserRoleModel
    {
        public string role_id { get; set; }
        public string role_name { get; set; }
    }
}
