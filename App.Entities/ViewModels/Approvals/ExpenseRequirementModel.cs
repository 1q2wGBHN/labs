﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App.Entities.ViewModels.Approvals
{
    public class ExpenseRequirementModel
    {
        public string approval_id { get; set; }
        public string process_name { get; set; }
        public int step_level { get; set; }
        public string emp_no { get; set; }
        public string approval_type { get; set; }
        public string approval_status { get; set; }
        public string remark { get; set; }
        public DateTime? approval_datetime { get; set; }
        public bool finish_flag { get; set; }
        public int requirement_status { get; set; }
        public int folio { get; set; }
        public int step_total { get; set; }
        public decimal total_quantity { get; set; }
        public decimal total_estimate_price { get; set; }
        public string process_group { get; set; }
    }
}
