﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App.Entities.ViewModels.MaEmail
{
    public class MaEmailModel
    {
        public string site_code { get; set; }
        public string mesa_control_email { get; set; }
        public string gerencia_email { get; set; }
        public string recibo_email { get; set; }
        public string cajagral_email { get; set; }
    }
}
