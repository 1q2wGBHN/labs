﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App.Entities.ViewModels.Creditors
{
    public class CreditorPOModel
    {
        public string CreditorNameBusiness { get; set; }
        public string CreditorName { get; set; }
        public string CreditorPhone { get; set; }
        public string CreditorEmail { get; set; }
    }
}
