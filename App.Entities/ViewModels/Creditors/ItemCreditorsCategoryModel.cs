﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App.Entities.ViewModels.Creditors
{
    public class ItemCreditorsCategoryModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}
