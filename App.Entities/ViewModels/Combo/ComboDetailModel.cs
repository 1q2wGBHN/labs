﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App.Entities.ViewModels.Combo
{
    public class ComboDetailModel
    {
        public string PartNumberCombo { get; set; }
        public string PartNumber { get; set; }
        public string Description { get; set; }
        public decimal Quantity { get; set; }
        public decimal Iva { get; set; }
        public bool ActiveStatus { get; set; }
        public decimal TotalPriceSale { get; set; }
    }
}
