﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App.Entities.ViewModels.Combo
{
    public class ComboHeaderModel
    {
        public string PartNumberCombo { get; set; }
        public string Description { get; set; }
        public decimal Iva { get; set; }
        public decimal Margin { get; set; }
        public string ComboStatus { get; set; }
        public decimal ComboCost { get; set; }
        public decimal ComboPrice { get; set; }
        public int CountCombo { get; set; }
    }
}
