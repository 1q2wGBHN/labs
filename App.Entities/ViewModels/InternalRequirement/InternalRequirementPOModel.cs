﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App.Entities.ViewModels.InternalRequirement
{
    public class InternalRequirementPOModel
    {
        public int folio { get; set; }
        public string requirement_status { get; set; }
        public string site_code { get; set; }
        public string cuser { get; set; }
        public string cdate { get; set; }
        public string cdate2 { get; set; }
        public string uuser { get; set; }
        public DateTime? udate { get; set; }
        public string program_id { get; set; }
    }
}
