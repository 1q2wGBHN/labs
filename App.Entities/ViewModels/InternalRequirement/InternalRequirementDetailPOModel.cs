﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App.Entities.ViewModels.InternalRequirement
{
    public class InternalRequirementDetailPOModel
    {
        public int folio { get; set; }
        public string part_number { get; set; }
        public string part_description { get; set; }
        public string category_name { get; set; }
        public int id_supplier { get; set; }
        public decimal quantity { get; set; }
        public decimal price { get; set; }
        public decimal? total { get; set; }

        public string site_name { get; set; }
        public string supplier_name { get; set; }
        public DateTime cdate { get; set; }
        public string cuser { get; set;}
        public int supplier_part_number_creditors { get; set; }
        public string program_id { get; set; }

        public decimal quantity_total { get; set; }
        public decimal? total_total { get; set; }
    }
}
