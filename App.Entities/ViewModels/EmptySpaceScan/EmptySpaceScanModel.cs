﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App.Entities.ViewModels.EmptySpaceScan
{
    public class EmptySpaceScanModel
    {
        public int id_empty_space { get; set; }
        public int empty_space_status { get; set; }
        public int incidence { get; set; }
        public string cuser { get; set; }
        public string cdate { get; set; }
        public string uuser { get; set; }
        public string udate { get; set; }
        public int produQuantity { get; set; }

    }
}
