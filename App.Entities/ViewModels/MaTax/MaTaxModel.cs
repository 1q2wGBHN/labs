﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App.Entities.ViewModels.MaTax
{
    public class MaTaxModel
    {
        public string tax_code { get; set; }
        public decimal tax_value { get; set; }
        public string name { get; set; }
    }
}
