﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App.Entities.ViewModels.RawMaterial
{
    public class RawMaterialDetail
    {
        public int Folio { get; set; }
        public string Part_Number { get; set; }
        public string Description { get; set; }
        public string TransferDate { get; set; }
        public decimal Quantity { get; set; }
        public string Gr_storage_location { get; set; }
        public decimal UnitCost { get; set; }
        public decimal IEPS { get; set; }
        public string TASA_IEPS { get; set; }
        public decimal SubTotal
        {
            get
            {
                return UnitCost * Quantity;
            }
        }
        public decimal Iva { get; set; }
        public string TASA_Iva { get; set; }
        public decimal Amount
        {
            get
            {
                return SubTotal + IEPS + Iva;
            }
        }
        public int level_header;
        public int level1_code;
        public string Item_Status { get; set; }
        public string Autothized_by { get; set; }
        public string Received_by { get; set; }
        public string StorageLocation { get; set; }
        public string Department { get; set; }
        public string Family { get; set; }
    }
}
