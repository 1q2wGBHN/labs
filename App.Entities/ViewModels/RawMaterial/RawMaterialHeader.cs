﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App.Entities.ViewModels.RawMaterial
{
    public class RawMaterialHeader
    {
        public int Folio { get; set; }
        public string SiteCode { get; set; }
        public string TransferDate { get; set; }
        public string Cuser { get; set; }
        public string Comment { get; set; }
        public string Autothized_by { get; set; }
        public string Received_by { get; set; }
        public string SiteName { get; set; }
        public string StorageLocation { get; set; }
        public int Status { get; set; }
        public int Print_Status { get; set; }
        public decimal Iva { get; set; }
        public decimal Ieps { get; set; }
        public decimal SubTotal { get; set; }
        public decimal Total { get; set; }
        public List<RawMaterialDetail> listRaw { get; set; }
    }
}
