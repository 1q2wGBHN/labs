﻿using System;

namespace App.Entities.ViewModels.TransactionsCustomers
{
    public class TransactionsCustomersModel
    {
        public string CustomerCode { get; set; }
        public string Type { get; set; }        
        public decimal? Amount { get; set; }
        public DateTime Date { get; set; }
        public string Reference { get; set; }
        public long? ReceiptNo { get; set; }        
        public long? CreditNoteNo { get; set; }
        public long? InvoiceNo { get; set; }
        public string InvoiceSerie { get; set; }
        public string UserName { get; set; }
        public string Currency { get; set; }        
        public long? SaleId{ get; set; }
        public string SiteCode { get; set; }

        public TRANSACTIONS_CUSTOMERS TSCUSTOMER
        {
            get
            {
                return new TRANSACTIONS_CUSTOMERS
                {
                    customer_code = CustomerCode,                    
                    trans_type = Type,
                    trans_amount = Amount,
                    trans_date = Date,
                    trans_reference = Reference,
                    recibo_no = ReceiptNo,                    
                    cn_id = CreditNoteNo,
                    invoice_no = InvoiceNo,
                    invoice_serie = InvoiceSerie,
                    user_name = UserName,
                    cur_code = Currency,                    
                    saleId = SaleId,
                    site_code = SiteCode
                };
            }
        }
    }
}
