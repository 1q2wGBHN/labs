﻿namespace App.Entities.ViewModels.TransactionsCustomers
{
    public class TransactionsCustomersItems
    {
        public string ClientNumber { get; set; }
        public string ClientName { get; set; }
        public string Date { get; set; }
        public string Reference { get; set; }
        public string ChargeText { get; set; }
        public string PaymentText { get; set; }
        public decimal Charge { get; set; }
        public decimal Payment { get; set; }
    }
}
