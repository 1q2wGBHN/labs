﻿using App.Entities.ViewModels.Payments;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using static App.Entities.ViewModels.Common.Constants;

namespace App.Entities.ViewModels.TransactionsCustomers
{
    public class TransactionsCustomersReport
    {
        public TransactionsCustomersReport()
        {
            StartDate = DateTime.Now.Date.ToString(DateFormat.INT_DATE);
            EndDate = DateTime.Now.Date.ToString(DateFormat.INT_DATE);
            IsFromMenu = true;
        }

        [DisplayName("Fecha Inicial:")]
        public string StartDate { get; set; }

        [DisplayName("Fecha Final:")]
        public string EndDate { get; set; }

        [DisplayName("Moneda:")]
        public string Currency { get; set; }

        [DisplayName("Cliente:")]
        public string CustomerCode { get; set; }

        public bool IsFromMenu { get; set; }
        public decimal TotalCharge { get; set; }
        public decimal TotalPayment { get; set; }
        public string SiteName { get; set; }

        public List<TransactionsCustomersItems> ReportList { get; set; }
        public List<PaymentCustomerReportModel> PaymentsReport { get; set; }
    }
}
