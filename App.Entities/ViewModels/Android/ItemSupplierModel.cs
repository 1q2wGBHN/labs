﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App.Entities.ViewModels.Android
{
    public class ItemSupplierModel
    {
        public int supplier_item_id { get; set; }
        public string part_number { get; set; }
        public int supplier_id { get; set; }
        public string part_description { get; set; }
    }
}
