﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App.Entities.ViewModels.Android
{
    public class TransferHeaderAndroidModel
    {
        public string part_number { get; set; }
        public string part_description { get; set; }
        public string c { get; set; }
        public string quantity { get; set; }
        public int transfer_header_in_folio { get; set; }
        public string transfer_document { get; set; }
        public string origin_site { get; set; }
        public string destination_site { get; set; }
        public decimal cost { get; set; }
        public decimal ieps { get; set; }
        public decimal iva { get; set; }
        public List<TransferHeaderAndroidModel> Items { get; set; }
    }
}
