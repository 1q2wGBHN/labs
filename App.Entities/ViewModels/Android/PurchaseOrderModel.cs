﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App.Entities.ViewModels.Android
{
    public class PurchaseOrderModel
    {
        public string purchase_no { get; set; }
        public string site_name { get; set; }
        public string site_code { get; set; }
        public string supplier_id { get; set; }
        public string supplier_name { get; set; }
        public DateTime purchase_date { get; set; }
    }
}
