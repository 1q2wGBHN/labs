﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App.Entities.ViewModels.Android
{
    public class EmptySpaceScanModel
    {
        public int id_empty_space { get; set; }
        public string part_number { get; set; }
        public string description { get; set; }
        public string planogram { get; set; }
        public System.DateTime? udate { get; set; }
        public decimal? Average { get; set; }
        public string planogramS { get; set; }
        public decimal? last_entry_quantity { get; set; }

        public List<EmptySpaceScanModel> Items { get; set; }
    }
}

