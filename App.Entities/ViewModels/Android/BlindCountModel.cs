﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App.Entities.ViewModels.Android
{
    public class BlindCountModel
    {
        public int blind_count_id { get; set; }
        public string purchase_no { get; set; }
        public string site_code { get; set; }
        public string part_number { get; set; }
        public string part_barcode { get; set; }
        public string quantity { get; set; }
        public string cuser { get; set; }
        public DateTime cdate { get; set; }
        public string uuser { get; set; }
        public DateTime udate { get; set; }
    }
}
