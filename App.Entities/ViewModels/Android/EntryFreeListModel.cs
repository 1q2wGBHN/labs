﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App.Entities.ViewModels.Android
{
    public class EntryFreeListModel
    {
        public string Parth_Number { get; set; }
        public decimal Quantity { get; set; }

        public decimal IVA { get; set; }
        public decimal IEPS { get; set; }
        public decimal base_cost { get; set; }

        public List<EntryFreeListModel> Entry { get; set; }
    }
}
