﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App.Entities.ViewModels.Android
{
    public class ItemsPurchaseOrderModel
    {
        public string part_number { get; set; }
        public string quantity { get; set; }
        public string cost { get; set; }

        public List<ItemsPurchaseOrderModel> Items { get; set; }
    }
}
