﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App.Entities.ViewModels.Android
{
    public class ScrapControlAndroidModel
    {
        public string part_number { get; set; }
        public string part_description { get; set; }
        public string quantity { get; set; }
        public string comment { get; set; }
        public decimal amount_total { get; set; }
        public decimal price { get; set; }
        public string cause_blocking { get; set; }

        public List<ScrapControlAndroidModel> Items { get; set; }
    }
}
