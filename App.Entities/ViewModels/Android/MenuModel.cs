﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App.Entities.ViewModels.Android
{
    public class MenuModel
    {
        public string level_1_menu { get; set; }
        public string level_2_menu { get; set; }
        public int menu_sequence { get; set; }
    }
}
