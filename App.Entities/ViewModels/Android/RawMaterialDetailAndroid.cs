﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App.Entities.ViewModels.Android
{
    public class RawMaterialDetailAndroid
    {
        public int Folio { get; set; }
        public string Part_number { get; set; }
        public decimal Quantity { get; set; }
        public string Gr_storage_location { get; set; }
        public decimal? Cost { get; set; }
        public decimal? Iva { get; set; }
        public decimal? Ieps { get; set; }
        public string CUser { get; set; }
        public string UUser { get; set; }

    }
}
