﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App.Entities.ViewModels.Android
{
    public class RawMaterialHeaderAndroid
    {
        public string part_number { get; set; }
        public string part_description { get; set; }
        public string c { get; set; }
        public decimal quantity { get; set; }
        public int Folio { get; set; }
        public int Site_code { get; set; }
        public int Transfer_Status { get; set; }
        public string received_by { get; set; }
        public string Authotized_by { get; set; }
        public string CUser { get; set; }
        public string UUser { get; set; }
        public decimal UnitCost { get; set; }
        public decimal IEPS { get; set; }
        public decimal SubTotal
        {
            get
            {
                return UnitCost * quantity;
            }
        }
        public decimal Iva { get; set; }
        public decimal Amount
        {
            get
            {
                return SubTotal + IEPS + Iva;
            }
        }
        public string gr_storage_location { get; set; }

        public List<RawMaterialHeaderAndroid> Items { get; set; }
    }
}
