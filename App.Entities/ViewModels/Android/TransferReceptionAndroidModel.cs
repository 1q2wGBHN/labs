﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App.Entities.ViewModels.Android
{
    public class TransferReceptionAndroidModel
    {
        #region Header
        public int transfer_id { get; set; }
        public string transfer_document { get; set; }
        public string origin_site_code { get; set; }
        public string origin_site_name { get; set; }
        public string transfer_status { get; set; }
        #endregion
        #region Detail
        public string part_number { get; set; }
        public string part_description { get; set; }
        public decimal quantity { get; set; }
        public decimal ? gr_quantity { get; set; }
        public decimal ? cost { get; set; }
        public decimal ? iva { get; set; }
        public decimal ? ieps { get; set; }
        public int scan_status { get; set; }
        #endregion
        public List<TransferReceptionAndroidModel> Transfers { get; set; }
    }
}
