﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App.Entities.ViewModels.Android
{
    public class BarcodesModel
    {
        public string part_number { get; set; }
        public string part_barcode { get; set; }
    }
}
