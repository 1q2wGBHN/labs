﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App.Entities.ViewModels.Promotion
{
    public class PromotionModel
    {
        public string promotion_code { get; set; }
        public string promotion_type { get; set; }
        public string promotion_descrip { get; set; }
        public DateTime promotion_date_start { get; set; }
        public DateTime promotion_date_end { get; set; }
        public DateTime promotion_date_create { get; set; }
        public string promotion_create_user { get; set; }
        public DateTime cdate { get; set; }
    }
}
