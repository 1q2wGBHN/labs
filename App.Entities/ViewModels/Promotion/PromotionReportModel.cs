﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App.Entities.ViewModels.Promotion
{
    public class PromotionReportModel
    {
        public string promotion_code { get; set; }
        public string promotion_description { get; set; }
        public string promotion_type { get; set; }
        public DateTime promotion_start_date { get; set; }
        public DateTime promotion_end_date { get; set; }
    }
}
