namespace App.Entities.ViewModels.Promotion
{
    public class PromotionItem
    {
        public string promotion_code { get; set; }
        public string part_number { get; set; }
        public string description { get; set; }
        public int? quantity { get; set; }
        public decimal price { get; set; }
        public decimal price_original { get; set; }
        public int common { get; set; }
    }
}