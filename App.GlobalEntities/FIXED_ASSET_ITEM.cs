//------------------------------------------------------------------------------
// <auto-generated>
//     Este código se generó a partir de una plantilla.
//
//     Los cambios manuales en este archivo pueden causar un comportamiento inesperado de la aplicación.
//     Los cambios manuales en este archivo se sobrescribirán si se regenera el código.
// </auto-generated>
//------------------------------------------------------------------------------

namespace App.GlobalEntities
{
    using System;
    using System.Collections.Generic;
    
    public partial class FIXED_ASSET_ITEM
    {
        public int fixed_asset_id { get; set; }
        public string fixed_asset_header { get; set; }
        public string fixed_asset_folio { get; set; }
        public Nullable<int> fixed_asset_code { get; set; }
        public string site_code { get; set; }
        public string description { get; set; }
        public string brand { get; set; }
        public string serie { get; set; }
        public string model { get; set; }
        public Nullable<System.DateTime> acquisition_date { get; set; }
        public Nullable<int> status_asset { get; set; }
        public Nullable<bool> requisition_flag { get; set; }
        public Nullable<int> requisiton_code { get; set; }
        public string location { get; set; }
        public string cuser { get; set; }
        public Nullable<System.DateTime> cdate { get; set; }
        public string uuser { get; set; }
        public Nullable<System.DateTime> udate { get; set; }
        public string program_id { get; set; }
    
        public virtual FIXED_ASSET FIXED_ASSET { get; set; }
        public virtual FIXED_ASSET_REQUISITION FIXED_ASSET_REQUISITION { get; set; }
    }
}
