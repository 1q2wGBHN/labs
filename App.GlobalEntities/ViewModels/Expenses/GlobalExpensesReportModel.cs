﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App.GlobalEntities.ViewModels.Expenses
{
    public class GlobalExpensesReportModel
    {
        public int ReferenceFolio { get; set; }
        public string SiteName { get; set; }
        public string Category { get; set; }
        public string ReferenceType { get; set; }
        public string Supplier { get; set; }
        public DateTime InvoiceDate { get; set; }
        public decimal TotalAmount { get; set; }
        public decimal? TotalIVA { get; set; }
        public decimal? TotalIEPS { get; set; }
    }
}
