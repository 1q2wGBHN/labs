﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App.GlobalEntities.ViewModels.Expenses
{
    public class GlobalItemXMLDetail
    {
        public int id { get; set; }
        public int xml_id { get; set; }
        public string item_no { get; set; }
        public string item_description { get; set; }
        public decimal quantity { get; set; }
        public decimal unit_cost { get; set; }
        public decimal item_amount { get; set; }
        public decimal discount { get; set; }
        public decimal iva { get; set; }
        public decimal ieps { get; set; }
        public decimal ieps_retained { get; set; }
        public decimal iva_retained { get; set; }
    }
}
