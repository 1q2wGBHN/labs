//------------------------------------------------------------------------------
// <auto-generated>
//     Este código se generó a partir de una plantilla.
//
//     Los cambios manuales en este archivo pueden causar un comportamiento inesperado de la aplicación.
//     Los cambios manuales en este archivo se sobrescribirán si se regenera el código.
// </auto-generated>
//------------------------------------------------------------------------------

namespace App.GlobalEntities
{
    using System;
    using System.Collections.Generic;
    
    public partial class ITEM_CREDITORS_PRICE
    {
        public int id { get; set; }
        public string currency { get; set; }
        public int supplier_part_number_creditors { get; set; }
        public bool flag_active { get; set; }
        public decimal price { get; set; }
        public string cuser { get; set; }
        public Nullable<System.DateTime> cdate { get; set; }
        public string uuser { get; set; }
        public Nullable<System.DateTime> udate { get; set; }
        public string program_id { get; set; }
    
        public virtual ITEM_SUPPLIER_CREDITORS ITEM_SUPPLIER_CREDITORS { get; set; }
    }
}
