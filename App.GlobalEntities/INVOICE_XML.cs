//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace App.Entities
{
    using System;
    using System.Collections.Generic;
    
    public partial class INVOICE_XML
    {
        public int id { get; set; }
        public string uuid { get; set; }
        public string invoice_no { get; set; }
        public string xml_name { get; set; }
        public string xml { get; set; }
        public string invoice_type { get; set; }
        public Nullable<int> supplier_id { get; set; }
        public string customer_code { get; set; }
        public string site_code { get; set; }
        public Nullable<int> xml_status { get; set; }
        public Nullable<bool> active { get; set; }
        public string cuser { get; set; }
        public Nullable<System.DateTime> cdate { get; set; }
        public string uuser { get; set; }
        public Nullable<System.DateTime> udate { get; set; }
    }
}
