﻿using App.DAL.Lookups;
using App.Entities;
using App.Entities.ViewModels.Payments;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App.DAL.Payments
{
    public class PaymentsPMRepository
    {
        private DFL_SAIEntities _context;
        private BankRepository _BankRepository;

        public PaymentsPMRepository()
        {
            _context = new DFL_SAIEntities();
            _BankRepository = new BankRepository();
        }

        public bool CreatePaymentPM (PAYMENTS_PM PaymentPm)
        {
            try
            {
                _context.PAYMENTS_PM.Add(PaymentPm);
                _context.SaveChanges();
                               
                return true;
            }
            catch (Exception ex)
            {
                string msg = ex.Message;
                return false;
            }
        }

        public PAYMENTS_PM GetPaymentMethodById(long PaymentMethodId)
        {
            return _context.PAYMENTS_PM.Where(p => p.id == PaymentMethodId).FirstOrDefault();
        }

        public List<PaymentMethod> GetPaymentMethodDetails(long PaymentMethodId)
        {
            var Details = (from pm in _context.PAYMENTS_PM
                           where pm.id == PaymentMethodId
                           select pm).AsEnumerable().Select(p => new PaymentMethod
                           {
                               Amount = p.amount.Value,
                               PaymentDate = p.date.Value.ToString("dd/MM/yyyy"),
                               PaymentPMId = p.id,
                               ViewPaymentPMId = p.PAYMENT_METHOD.pm_name,
                               PaymentMethodId = p.payment_method_id,
                               OperationNumber = p.operation_number,
                               Bank = p.bank,
                               BankReference = p.bank_reference,
                               BankName = _BankRepository.GetBankNameById(p.bank)
                           }).ToList();

            return Details;
        }
    }
}
