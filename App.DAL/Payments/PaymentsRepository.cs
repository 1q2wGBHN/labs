﻿using App.DAL.DigitalTaxStamp;
using App.Entities;
using App.Entities.ViewModels.Common;
using App.Entities.ViewModels.Payments;
using App.Entities.ViewModels.TransactionsCustomers;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Core.Objects;
using System.Globalization;
using System.Linq;
using static App.Entities.ViewModels.Common.Constants;

namespace App.DAL.Payments
{
    public class PaymentsRepository
    {
        private DFL_SAIEntities _context;
        private DigitalTaxStampRepository _DigitalTaxStampRepository;

        public PaymentsRepository()
        {
            _context = new DFL_SAIEntities();
            _DigitalTaxStampRepository = new DigitalTaxStampRepository();
        }

        public bool CreatePayment(PAYMENTS Payment)
        {
            try
            {
                _context.PAYMENTS.Add(Payment);
                _context.SaveChanges();

                //Update Internal Number and Number in DB from Reference Number Table.
                ObjectParameter Reference = new ObjectParameter("Reference", typeof(string));
                _context.sp_Get_Reference_Number("PAYMENT", "ALL", Reference);

                return true;
            }
            catch (Exception ex)
            {
                string msg = ex.Message;
                return false;
            }
        }       
        
        public List<PaymentsCustomer> PaymentsByCustomer(string CustomerCode)
        {
            var PaymentsList = new List<PaymentsCustomer>();

            PaymentsList = (from p in _context.PAYMENTS
                            join pm in _context.PAYMENTS_PM on new { p.payment_serie, p.payment_no } equals new { pm.payment_serie, pm.payment_no }
                            where p.customer_code == CustomerCode
                                  && p.payment_status
                            select new
                            {
                                PMId = pm.id,
                                Serie = p.payment_serie,
                                Number = p.payment_no,
                                ReceiptDate = p.payment_date.Value,
                                Method = pm.PAYMENT_METHOD.pm_name,
                                OperationNumber = pm.operation_number,
                                Amount = pm.amount.Value,
                                PaymentDate = pm.date.Value
                            }).Distinct().AsEnumerable().Select(c => new PaymentsCustomer
                            {
                                PMId = c.PMId,
                                Receipt = string.Format("{0}{1}", c.Serie, c.Number),
                                ReceiptDate = c.ReceiptDate.ToString(DateFormat.INT_DATE),
                                PaymentMethod = c.Method,
                                OperationNumber = c.OperationNumber,
                                Amount = c.Amount,
                                PaymentDate = c.PaymentDate.ToString(DateFormat.INT_DATE)
                            }).ToList();

            return PaymentsList;
        }

        public List<PaymentsCustomer> PaymentsCancelledByCustomer(string CustomerCode)
        {
            var PaymentsList = new List<PaymentsCustomer>();

            PaymentsList = (from p in _context.PAYMENTS
                            join pm in _context.PAYMENTS_PM on new { p.payment_serie, p.payment_no } equals new { pm.payment_serie, pm.payment_no }
                            where p.customer_code == CustomerCode
                                  && !p.payment_status
                            select new
                            {
                                PMId = pm.id,
                                Serie = p.payment_serie,
                                Number = p.payment_no,
                                ReceiptDate = p.payment_date.Value,
                                Method = pm.PAYMENT_METHOD.pm_name,
                                OperationNumber = pm.operation_number,
                                Amount = pm.amount.Value,
                                PaymentDate = pm.date.Value,
                                PaymentDateCancel = p.udate.Value
                            }).Distinct().AsEnumerable().Select(c => new PaymentsCustomer
                            {
                                PMId = c.PMId,
                                Receipt = string.Format("{0}{1}", c.Serie, c.Number),
                                ReceiptDate = c.ReceiptDate.ToString(DateFormat.INT_DATE),
                                PaymentMethod = c.Method,
                                OperationNumber = c.OperationNumber,
                                Amount = c.Amount,
                                PaymentDate = c.PaymentDate.ToString(DateFormat.INT_DATE),
                                PaymentDateCancel=c.PaymentDateCancel.ToString(DateFormat.INT_DATE)
                            }).ToList();

            return PaymentsList;
        }
        public bool UpdatePayment(PAYMENTS Payment)
        {
            var PaymentEdit = _context.PAYMENTS.Where(p => p.payment_no == Payment.payment_no).FirstOrDefault();
            if (PaymentEdit != null)
            {
                _context.Entry(PaymentEdit).State = EntityState.Detached;
                _context.Entry(Payment).State = EntityState.Modified;
                _context.SaveChanges();
                return true;
            }
            return false;
        }

        public PAYMENTS GetPaymentByNumber(long PaymentNumber)
        {
            return _context.PAYMENTS.Where(p => p.payment_no == PaymentNumber).FirstOrDefault();
        }

        public string GetDateLastPayment(string CustomerCode)
        {
            var Payments = _context.PAYMENTS.Where(c => c.customer_code == CustomerCode && c.payment_status).OrderByDescending(d => d.payment_date).ToList();
            string PaymentDate = string.Empty;
            if(Payments.Count > 0)
                PaymentDate = Payments.First().payment_date.Value.ToString(DateFormat.INT_DATE);

            return PaymentDate;
        }

        public SendDocumentModel GetSendDocumentByNumberPayment(long Number)
        {            
            var response = new SendDocumentModel();
            response = ((from inv in _context.PAYMENTS
                         join dts in _context.DIGITAL_TAX_STAMP on new { cfdi = inv.payment_no, serie = inv.payment_serie } equals new { cfdi = dts.cfdi_id, serie = dts.cfdi_serie }
                         where inv.payment_no == Number && dts.sat_document_code == VoucherType.PAYMENTS

                         select new SendDocumentModel
                         {
                             Customer = inv.CUSTOMERS.customer_name,
                             EmailCustomerSend = inv.CUSTOMERS.customer_email,
                             FolioFiscal = dts.UUID.Substring(dts.UUID.Length - 10, dts.UUID.Length),
                             Date = inv.payment_date.ToString(),
                             EmailCustomer = inv.CUSTOMERS.customer_email,
                             Number = inv.payment_no.ToString(),
                             Invoice = inv.payment_serie + inv.payment_no,
                             Serie = inv.payment_serie,
                             Total = inv.payment_total.ToString(),
                             DocumentType = dts.sat_document_code

                         })).FirstOrDefault();

            return response;
        }

        public SendDocumentModel GetPayment(long Number)
        {
            var Document = new SendDocumentModel();
            Document = (from p in _context.PAYMENTS
                        where p.payment_no == Number
                        select new
                        {
                            Invoice = p.payment_no,
                            Serie = p.payment_serie,
                            Customer = p.CUSTOMERS.customer_name,
                            EmailCustomer = p.CUSTOMERS.customer_email,
                            Date = p.payment_date.Value,
                            DocumentType = VoucherType.PAYMENTS,
                            Number = p.payment_no,
                            Total = p.payment_total,
                            xml = p.xml
                        }).AsEnumerable().Select(c => new SendDocumentModel
                        {
                            Invoice = c.Invoice.ToString(),
                            Serie = c.Serie,
                            Customer = c.Customer,
                            EmailCustomer = c.EmailCustomer,
                            Date = c.Date.ToString(DateFormat.INT_DATE),
                            DocumentType = c.DocumentType,
                            Number = c.Number.ToString(),
                            Total = c.Total.ToString(),
                            Document = Documents.PAY,
                            IsStamped = string.IsNullOrEmpty(c.xml) ? false : true
                        }).FirstOrDefault();

            Document.IsStamped = _DigitalTaxStampRepository.IsPaymentStamped(Number);
            return Document;
        }

        public List<PaymentCustomerReportModel> PaymentCustomerReport (TransactionsCustomersReport Model)
        {
            DateTime StartDate = DateTime.ParseExact(Model.StartDate, DateFormat.INT_DATE, CultureInfo.InvariantCulture);
            DateTime EndDate = DateTime.ParseExact(Model.EndDate, DateFormat.INT_DATE, CultureInfo.InvariantCulture);
            var DataList = new List<PaymentCustomerReportModel>();
            DataList = (from p in _context.PAYMENTS
                        join pd in _context.PAYMENTS_DETAIL on new { p.payment_no, p.payment_serie } equals new { pd.payment_no, pd.payment_serie }
                        join i in _context.INVOICES on new { pd.invoice_no, pd.invoice_serie } equals new { i.invoice_no, i.invoice_serie }
                        join u in _context.USER_MASTER on p.cuser equals u.user_name
                        where p.payment_status
                              && DbFunctions.TruncateTime(p.payment_date) >= StartDate
                              && DbFunctions.TruncateTime(p.payment_date) <= EndDate
                              && p.cur_code == Model.Currency
                              && (string.IsNullOrEmpty(Model.CustomerCode) || p.customer_code == Model.CustomerCode)
                        select new
                        {
                            p.customer_code,
                            p.CUSTOMERS.customer_name,
                            p.payment_date,
                            pd.amount,
                            i.invoice_no,
                            i.invoice_serie,
                            i.invoice_date,
                            i.invoice_tax,
                            i.invoice_total,
                            user_first_name = u.first_name,
                            user_last_name = u.last_name,
                            p.payment_no,
                            p.payment_serie,
                            p.xml,
                            i.cfdi_id
                        }).OrderBy(d => d.payment_date).AsEnumerable().Select(c => new PaymentCustomerReportModel
                        {
                            ClientCode = c.customer_code,
                            ClientName = c.customer_name,
                            PaymentDate = c.payment_date.Value.ToString("d", new CultureInfo("es-ES")),
                            PaymentAmount = c.amount.Value,
                            InvoiceNo = string.Format("CFD-{0}{1}", c.invoice_serie, c.cfdi_id),
                            InvoiceDate = c.invoice_date.ToString("d", new CultureInfo("es-ES")),
                            InvoiceSubTotal = c.invoice_total - c.invoice_tax,
                            InvoiceTax = c.invoice_tax,
                            InvoiceTotal = c.invoice_total,
                            CUser = string.Format("{0} {1}", c.user_first_name, c.user_last_name),
                            PaymentNo = string.Format("Recibo No. {0}{1}", c.payment_serie, c.payment_no),
                            xml = c.xml,
                            Serie = c.payment_serie,
                            Number = c.payment_no
                        }).ToList();
            return DataList;
        }

        public bool SprocUpdatePaymentNumber()
        {
            DFL_SAIEntities context = new DFL_SAIEntities();
            context.Configuration.AutoDetectChangesEnabled = false;
            try
            {
                //Update Internal Number and Number in DB from Reference Number Table.
                ObjectParameter Reference = new ObjectParameter("Reference", typeof(string));
                _context.sp_Get_Reference_Number("PAYMENT", "ALL", Reference);
                return true;
            }
            catch (Exception ex)
            {
                string msg = ex.Message;
                return false;
            }
            finally
            {
                if (context != null)
                {
                    context.Configuration.AutoDetectChangesEnabled = true;
                    context.Dispose();
                }
            }
        }

        public bool PaymentExist(long Number)
        {
            return _context.PAYMENTS.Where(c => c.payment_no == Number).Any();
        }
    }
}
