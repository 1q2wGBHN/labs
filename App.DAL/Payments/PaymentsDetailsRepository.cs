﻿using App.Entities;
using App.Entities.ViewModels.Invoices;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App.DAL.Payments
{
    public class PaymentsDetailsRepository
    {
        private DFL_SAIEntities _context;

        public PaymentsDetailsRepository()
        {
            _context = new DFL_SAIEntities();
        }
        public bool CreatePaymentDetail(PAYMENTS_DETAIL PaymentDetail)
        {
            var _localContext = new DFL_SAIEntities();
            try
            {                
                _localContext.PAYMENTS_DETAIL.Add(PaymentDetail);
                _localContext.SaveChanges();

                return true;
            }
            catch (Exception ex)
            {
                string msg = ex.Message;
                return false;
            }
            finally
            {
                if (_localContext != null)
                {
                    _localContext.Configuration.AutoDetectChangesEnabled = true;
                    _localContext.Dispose();
                }
            }
        }

        public List<InvoiceBalanceDetails> PaymentsDetailsByPaymentMethodId(long PaymentId)
        {
            var Details = (from pd in _context.PAYMENTS_DETAIL
                           join i in _context.INVOICES on new { pd.invoice_no, pd.invoice_serie } equals new { i.invoice_no, i.invoice_serie }
                           where pd.payment_pm_id == PaymentId
                           select new
                           {
                               InvoiceNo = i.invoice_no,
                               InvoiceSerie = i.invoice_serie,
                               InvoiceBalance = i.invoice_balance,
                               PaymentInvoiceAmount = pd.amount,
                               Serie = pd.payment_serie,
                               Number = pd.payment_no,
                               Payment = pd.PAYMENTS_PM.amount,
                               Previous = pd.outstanding_balance                               
                           }).AsEnumerable().Select(c => new InvoiceBalanceDetails
                           {
                               Invoice = string.Format("{0}{1}", c.InvoiceSerie, c.InvoiceNo),
                               PaymentInvoice = c.PaymentInvoiceAmount.Value,
                               Balance = c.InvoiceBalance.Value,
                               Number = c.Number,
                               Serie = c.Serie,
                               PaymentId = PaymentId.ToString(),
                               Payment = c.Payment.Value,
                               PreviousBalance = c.Previous.Value
                           }).ToList();

            return Details;
        }

        public PAYMENTS_DETAIL GetPaymentDetailById(long PaymentDetailId)
        {
            return _context.PAYMENTS_DETAIL.Where(p => p.detail_id == PaymentDetailId).FirstOrDefault();
        }
    }
}
