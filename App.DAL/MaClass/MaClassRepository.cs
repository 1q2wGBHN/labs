﻿using App.Entities;
using System.Collections.Generic;
using System.Linq;
using App.Entities.ViewModels.MaClass;

namespace App.DAL.MaClass
{
    public class MaClassRepository
    {
        private readonly DFL_SAIEntities _context;

        public MaClassRepository()
        {
            _context = new DFL_SAIEntities();
        }

        public List<MA_CLASS> GetAllDepartments(int level1)
        {
            return _context.MA_CLASS.Where(x => x.level_category == level1).ToList();
        }

        public List<MA_CLASS> GetAllFamilyByParentClass(int level2)
        {
            return _context.MA_CLASS.Where(x => x.parent_class == level2).ToList();
        }

        public List<MaClassModel> GetAllFamilyByAcitveInventory()
        {
            var list = new[] { 1, 2, 3, 4 };
            return _context.INVENTORY_HEADER
                .Join(_context.INVENTORY_TYPE_DETAILS, id => id.inventory_type, it => it.id_inventory_type, (ih, itd) => new { ih, itd })
                .Join(_context.MA_CLASS, id => id.itd.level_ma_class, it => it.class_id, (ih2, ma) => new { ih2, ma })
                .Where(a => list.Contains(a.ih2.ih.inventory_status))
                .Select(x => new MaClassModel
                {
                    class_id = x.ma.class_id,
                    class_name = x.ma.class_name,
                    description = x.ma.description
                }).ToList();
        }

        public string GetDepartmentById(int level_header)
        {
            return _context.MA_CLASS.Where(x => x.class_id == level_header).FirstOrDefault().class_name;
        }

        public string GetFamilyById(int level1_code)
        {
            return _context.MA_CLASS.Where(x => x.class_id == level1_code).FirstOrDefault().class_name;
        }
    }
}