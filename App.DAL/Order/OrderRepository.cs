﻿using App.Entities;
using App.Entities.ViewModels.Order;
using System;
using System.Collections.Generic;
using System.Linq;

namespace App.DAL.Order
{
    public class OrderRepository
    {
        private readonly DFL_SAIEntities _context;

        public OrderRepository()
        {
            _context = new DFL_SAIEntities();
        }

        public List<OrderModel> GetAll()
        {
            return _context.ORDER_HEAD.Join(_context.MA_SUPPLIER, x => x.supplier_id, y => y.supplier_id, (x, y) => new { x, y }).Join(_context.SITE_CONFIG, n => n.x.site_id, z => z.site_code, (n, z) => new { n, z })
                .Select(s => new OrderModel
                {
                    Folio = s.n.x.folio,
                    Site = s.z.site_name,
                    SiteId = s.z.site_code,
                    RefillDays = s.n.x.refill_days,
                    Status = s.n.x.status,
                    Supplier = s.n.y.business_name != null ? s.n.y.business_name : "nada",
                    SupplierId = s.n.y.supplier_id
                }).ToList();
        }

        public OrderModel Get(int Folio)
        {
            return _context.ORDER_HEAD.Join(_context.MA_SUPPLIER, x => x.supplier_id, y => y.supplier_id, (x, y) => new { x, y }).Join(_context.SITE_CONFIG, n => n.x.site_id, z => z.site_code, (n, z) => new { n, z })
                .Where(s => s.n.x.folio == Folio).ToList().Select(s => new OrderModel
                {
                    Folio = Folio,
                    Site = s.z.site_name,
                    SiteId = s.z.site_code,
                    RefillDays = s.n.x.refill_days,
                    Status = s.n.x.status,
                    Supplier = s.n.y.business_name != null ? s.n.y.business_name : "nada",
                    SupplierId = s.n.x.supplier_id,
                    Date = s.n.x.datee != null ? s.n.x.datee.Value.ToString("MM/dd/yyyy") : "",
                    Comment = s.n.x.comment
                }).SingleOrDefault();
        }

        public int Post(ORDER_HEAD Order)
        {
            _context.ORDER_HEAD.Add(Order);
            _context.SaveChanges();
            return Order.folio;
        }

        public bool RemoveOrderHeader(ORDER_HEAD Order)
        {
            try
            {
                _context.ORDER_HEAD.Remove(Order);
                _context.SaveChanges();
                return true;
            }
            catch (Exception)
            {
                return false;
                throw;
            }
        }

        public bool Update(int Folio, DateTime Date, string Comment, string User)
        {
            var Order = _context.ORDER_HEAD.Where(x => x.folio == Folio).SingleOrDefault();
            if (Order != null)
            {
                Order.comment = Comment;
                Order.datee = Date;
                Order.status = 2;
                Order.uuser = User;
                Order.udate = DateTime.Now;
                Order.program_id = "ORDE002.cshtml";
                _context.SaveChanges();
                return true;
            }
            return false;
        }

        public bool UpdateOrder(int Folio, DateTime? Date, string Comment, string User)
        {
            var Order = _context.ORDER_HEAD.Where(x => x.folio == Folio).SingleOrDefault();
            if (Order != null)
            {
                Order.comment = Comment;
                Order.uuser = User;
                Order.udate = DateTime.Now;
                Order.program_id = "ORDE002.cshtml";
                if (Date != null)
                    Order.datee = Date.Value;
                else
                    Order.datee = null;

                _context.SaveChanges();
                return true;
            }
            return false;
        }

        public bool UpdateOrderAndOrderList(int Folio, DateTime? Date, string Comment, string User, int status)
        {
            var Order = _context.ORDER_HEAD.Where(x => x.folio == Folio).SingleOrDefault();
            if (Order != null)
            {
                Order.comment = Comment;
                Order.uuser = User;
                Order.status = status;
                Order.udate = DateTime.Now;
                Order.program_id = "ORDE002.cshtml";
                if (Date != null)
                    Order.datee = Date.Value;
                else
                    Order.datee = null;

                _context.SaveChanges();
                return true;
            }
            return false;
        }

        public bool Cancel(int Folio, string User)
        {
            var Order = _context.ORDER_HEAD.Where(x => x.folio == Folio && x.status != 2 && x.status != 8).SingleOrDefault();
            if (Order != null)
            {
                Order.status = 8;
                Order.uuser = User;
                Order.udate = DateTime.Now;
                Order.program_id = "ORDE002.cshtml";
                _context.SaveChanges();
                return true;
            }
            return false;
        }

        public List<OrderModel> Filter()
        {
            var date = DateTime.Now.AddDays(-2);
            var Order = _context.ORDER_HEAD.Join(_context.MA_SUPPLIER, x => x.supplier_id, y => y.supplier_id, (x, y) => new { x, y })
                .Where(x => (x.x.status == 1 || x.x.status == 0) && x.x.cdate > date).Select(x => new OrderModel
            {
                Folio = x.x.folio,
                Supplier = x.y.business_name
            }).OrderByDescending(x => x.Folio).ToList();
            return Order ?? new List<OrderModel>();
        }

        public int Status(int Folio)
        {
            var number = _context.ORDER_HEAD.Where(x => x.folio == Folio).SingleOrDefault();
            if (number != null)
                return number.status;

            return 5;
        }
    }
}