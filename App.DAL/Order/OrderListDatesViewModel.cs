﻿namespace App.DAL.Order
{
    public class OrderListDatesViewModel
    {
        public int Id { get; set; }
        public int Supplier { get; set; }
        public int Authorized { get; set; }
        public string ParthNumber { get; set; }
    }
}