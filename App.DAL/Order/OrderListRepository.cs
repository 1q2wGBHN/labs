﻿using App.Entities;
using App.Entities.ViewModels.Order;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Xml;

namespace App.DAL.Order
{
    public class OrderListRepository
    {
        private readonly DFL_SAIEntities _context;

        public OrderListRepository()
        {
            _context = new DFL_SAIEntities();
        }

        public List<OrderListModel> GetAll(int Folio)
        {
            var Order = _context.ORDER_LIST.Join(_context.ITEM, x => x.part_number, y => y.part_number, (x, y) => new { x, y }).Where(x => x.x.folio == Folio).ToList().Select(x => new OrderListModel
            {
                Id = 0,
                Authorize = x.x.authorized != null ? Convert.ToInt32(x.x.authorized) : 0,
                AverageSale = x.x.average_sale,
                ParthNumber = x.x.part_number,
                Description = x.y.part_description,
                Folio = x.x.folio,
                PrePruchase = x.x.pre_purchase,
                Stock = x.x.stock,
                SuggestedSupplier = x.x.suggested_supplier != null ? Convert.ToInt32(x.x.suggested_supplier) : 0,
                SuggestedSystems = x.x.suggested_systems,
                Packing = x.x.packing != null ? x.x.packing.Value : 0
            }).ToList();
            return Order;
        }

        public List<ORDER_LIST> Post(List<ORDER_LIST> OrderList)
        {
            var Order = _context.ORDER_LIST.AddRange(OrderList);
            _context.SaveChanges();

            return OrderList;
        }

        public bool NewPost(List<ORDER_LIST> OrderList)
        {
            try
            {
                XmlDocument xDoc = new XmlDocument();
                XmlReaderSettings readerSettings = new XmlReaderSettings
                {
                    ConformanceLevel = ConformanceLevel.Fragment
                };

                StringBuilder xml = new StringBuilder();
                xml.Append("<Employees>");

                foreach (var id in OrderList)
                {
                    xml.Append("<Employee>");

                    xml.Append("<part_number>" + id.part_number + "</part_number>");
                    xml.Append("<stock>" + id.stock + "</stock>");
                    xml.Append("<packing>" + id.packing + "</packing>");
                    xml.Append("<pre_purchase>" + id.pre_purchase + "</pre_purchase>");
                    xml.Append("<average_sale>" + id.average_sale + "</average_sale>");
                    xml.Append("<suggested_systems>" + id.suggested_systems + "</suggested_systems>");
                    xml.Append("<folio>" + id.folio + "</folio>");
                    xml.Append("<cuser>" + id.cuser + "</cuser>");
                    xml.Append("<cdate>" + id.cdate + "</cdate>");
                    xml.Append("<program_id>" + id.program_id + "</program_id>");

                    xml.Append("</Employee>");
                }

                xml.Append("</Employees>");
                xDoc.LoadXml(xml.ToString());

                var result = _context.Database.ExecuteSqlCommand("insert_order_list @xml ", new SqlParameter("@xml", xDoc.OuterXml));

                return true;
            }
            catch (Exception)
            {
                return false;
                throw;
            }
        }

        public bool Put(int Id, string ParthNumber, int Supplier, int Authorized, string User)
        {
            var OrderList = _context.ORDER_LIST.Where(x => x.folio == Id && x.part_number == ParthNumber).SingleOrDefault();
            if (OrderList != null)
            {
                OrderList.suggested_supplier = Supplier;
                OrderList.authorized = Authorized;
                OrderList.uuser = User;
                OrderList.udate = DateTime.Now;
                OrderList.program_id = "ORDE002.cshtml";
                _context.SaveChanges();
                return true;
            }
            return false;
        }

        public bool UpdateOrderList(List<OrderListDatesViewModel> OrderList, string User)
        {
            try
            {
                XmlDocument xDoc = new XmlDocument();
                XmlReaderSettings readerSettings = new XmlReaderSettings
                {
                    ConformanceLevel = ConformanceLevel.Fragment
                };

                StringBuilder xml = new StringBuilder();
                xml.Append("<Employees>");

                foreach (var id in OrderList)
                {
                    xml.Append("<Employee>");
                    xml.Append("<part_number>" + id.ParthNumber + "</part_number>");
                    xml.Append("<suggested_supplier>" + id.Supplier + "</suggested_supplier>");
                    xml.Append("<authorized>" + id.Authorized + "</authorized>");
                    xml.Append("<folio>" + id.Id + "</folio>");
                    xml.Append("<uuser>" + User + "</uuser>");
                    xml.Append("<program_id>ORDE002.cshtml</program_id>");
                    xml.Append("</Employee>");
                }

                xml.Append("</Employees>");
                xDoc.LoadXml(xml.ToString());

                var result = _context.Database.ExecuteSqlCommand("update_order_list @xml ", new SqlParameter("@xml", xDoc.OuterXml));

                return true;
            }
            catch (Exception)
            {
                return false;
                throw;
            }
        }

        public List<OrderListModel> GetAllByFolio(int Folio)
        {
            return _context.ORDER_LIST.Join(_context.ITEM, x => x.part_number, y => y.part_number, (x, y) => new { x, y })
                .Where(x => x.x.folio == Folio)
                .Select(x => new OrderListModel
                {
                    Authorize = x.x.authorized != null ? Convert.ToInt32(x.x.authorized) : 0,
                    ParthNumber = x.x.part_number,
                    Folio = x.x.folio,
                }).ToList();
        }
    }
}