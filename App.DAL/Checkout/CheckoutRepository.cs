﻿using App.Entities;
using App.Entities.ViewModels.Checkout;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dapper;
using System.Data;

namespace App.DAL.Checkout
{
    public class CheckoutRepository
    {
        private DFL_SAIEntities _context;

        public CheckoutRepository()
        {
            _context = new DFL_SAIEntities();
        }

        public CheckoutDataList GetZCutReportData(string EmployeeNo, string Date)
        {
            var DataInfo = new CheckoutDataList();
            EmployeeNo = EmployeeNo == null ? string.Empty : EmployeeNo;
            DateTime DateValue = DateTime.ParseExact(Date, "dd/MM/yyyy", CultureInfo.InvariantCulture);
            SqlMapper.GridReader reader = _context.Database.Connection.QueryMultiple("sp_DFLPOS_GenerateZCut", param: new { date = DateValue, partner_id = EmployeeNo }, commandType: CommandType.StoredProcedure);

            DataInfo.Charge = reader.Read<CheckoutData>().ToList();
            DataInfo.Returns = reader.Read<CheckoutData>().ToList();
            DataInfo.Withdrawal = reader.Read<CheckoutWithdrawal>().ToList();
            DataInfo.Donations = reader.Read<CheckoutDonations>().ToList();
            DataInfo.ExchangeRate = reader.Read<CheckoutExchangeRate>().ToList();

            return DataInfo;
        }
    }
}
