﻿namespace App.DAL
{
    public class StoreProcedureIvaIeps
    {
        public decimal Iva { get; set; }
        public decimal Ieps { get; set; }
        public decimal IvaPurchase { get; set; }
    }
}