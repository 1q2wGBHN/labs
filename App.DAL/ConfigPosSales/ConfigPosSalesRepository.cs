﻿using App.Entities;
using App.Entities.ViewModels.ConfigPosSales;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App.DAL.ConfigPosSales
{
    public class ConfigPosSalesRepository
    {
        private DFL_SAIEntities _context;

        public ConfigPosSalesRepository()
        {
            _context = new DFL_SAIEntities();
        }

        public bool CreateConfigPosSale (DFLPOS_CONFIG Item)
        {
            try
            {
                _context.DFLPOS_CONFIG.Add(Item);
                _context.SaveChanges();
                return true;
            }
            catch (Exception ex)
            {
                string msg = ex.Message;
                return false;
            }
        }

        public List<PosSale> GetConfigPosSalesList()
        {
            var PosSaleList = (from c in _context.DFLPOS_CONFIG
                               select new PosSale
                               {
                                   Id = c.config_id,
                                   Key = c.config_key,
                                   Value = c.config_value,
                                   PosId = c.config_pos_id
                               }).ToList();

            return PosSaleList;
        }

        public PosSale GetConfigPosSaleById(int ConfigPosSaleId)
        {
            var Config = (from c in _context.DFLPOS_CONFIG
                          where c.config_id == ConfigPosSaleId
                          select new PosSale
                          {
                              Id = c.config_id,
                              Key = c.config_key,
                              Value = c.config_value,
                              PosId = c.config_pos_id
                          }).FirstOrDefault();

            return Config;
        }

        public bool UpdateConfig(DFLPOS_CONFIG Item)
        {
            var ItemEdit = _context.DFLPOS_CONFIG.Find(Item.config_id);
            if(ItemEdit != null)            
            {
                _context.Entry(ItemEdit).State = EntityState.Detached;
                _context.Entry(Item).State = EntityState.Modified;
                _context.SaveChanges();
                return true;
            }
            return false;
        }
    }
}
