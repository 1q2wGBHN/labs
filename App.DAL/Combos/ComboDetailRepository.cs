﻿using App.Entities;
using App.Entities.ViewModels.Combo;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;

namespace App.DAL.Combos
{
    public class ComboDetailRepository
    {
        private readonly DFL_SAIEntities _context;

        public ComboDetailRepository()
        {
            _context = new DFL_SAIEntities();
        }

        public List<ComboDetailModel> GetComboDetail(string PartNumber)
        {
            return _context.Database.SqlQuery<ComboDetailModel>(@"select 
                cd.part_number_combo as PartNumberCombo ,
                cd.part_number as PartNumber , 
                cd.quantity as Quantity,
                it.part_description as Description,
                cd.iva as Iva,
                cd.active_status as ActiveStatus,
                cd.total_price_sale as TotalPriceSale 
                from COMBO_DETAIL  cd
                inner join ITEM it on it.part_number = cd.part_number
                where cd.part_number_combo  = @partnumber and cd.active_status = 1", new SqlParameter("@partnumber", PartNumber)).ToList();
        }
    }
}