﻿using App.Entities;
using App.Entities.ViewModels.Combo;
using System.Collections.Generic;
using System.Linq;

namespace App.DAL.Combos
{
    public class ComboHeaderRepository
    {
        private readonly DFL_SAIEntities _context;

        public ComboHeaderRepository()
        {
            _context = new DFL_SAIEntities();
        }

        public List<ComboHeaderModel> GetComboHeder()
        {
            return _context.Database.SqlQuery<ComboHeaderModel>(@"select 
                ch.part_number_combo as PartNumberCombo, 
                it.part_description as Description,
                ch.combo_status as ComboStatus, 
                ISNULL( ch.combo_cost,0) as ComboCost,  
                isNull(ch.combo_price,0) as ComboPrice,
                case when combo_cost >= 0 and combo_price >= 0 then cast(Round(((1-(ISNULL(ch.combo_cost,0)/ISNULL(ch.combo_price,1)))*100),4) as decimal(10,4))  else 0 end as Margin,
                Isnull(cast(ch.combo_cost  - (ch.combo_cost / (1+(ma.tax_value/100))) as decimal(10,4)),0) as Iva,
                (select count(*) from COMBO_DETAIL  cd where cd.part_number_combo  = ch.part_number_combo) as CountCombo                
                from COMBO_HEADER ch
                inner join ITEM it on it.part_number = ch.part_number_combo
                inner join MA_TAX ma on ma.tax_code = it.part_iva_purchase
                where combo_status = 1 and (select count(*) from COMBO_DETAIL  cd where cd.part_number_combo  = ch.part_number_combo) > 0").ToList();
        }
    }
}