﻿using App.Entities;
using App.Entities.ViewModels.Android;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;

namespace App.DAL
{
    public class SysPageMasterRepository
    {
        private readonly DFL_SAIEntities _context;

        public SysPageMasterRepository()
        {
            _context = new DFL_SAIEntities();
        }

        public List<SYS_PAGE_MASTER> GetAllPagesAdmin()
        {
            return _context.SYS_PAGE_MASTER.Select(x => x).ToList();
        }

        public SYS_PAGE_MASTER GetPageByID(string page_id)
        {
            return _context.SYS_PAGE_MASTER.FirstOrDefault(x => x.page_id == page_id);
        }

        public int AddPagesMaster(SYS_PAGE_MASTER page)
        {
            try
            {
                _context.SYS_PAGE_MASTER.Add(page);
                return _context.SaveChanges();
            }
            catch (Exception ex)
            {
                var msg = ex.Message;
                return 0;
            }
        }

        public SYS_PAGE_MASTER SearchPage(string id_role)
        {
            var page = _context.SYS_PAGE_MASTER.Where(w => w.page_id == id_role).FirstOrDefault();
            if (page != null)
                return page;
            else
                return null;
        }

        public int UpdatePagesMaster(SYS_PAGE_MASTER page)
        {
            _context.Entry(page).State = System.Data.Entity.EntityState.Modified;
            return _context.SaveChanges();
        }

        public ICollection<SYS_PAGE_MASTER> GetAllPagesOfRole(string rol)
        {
            return _context.SYS_PAGE_MASTER.Where(x => !_context.SYS_ROLE_PAGE.Any(v => v.role_id == rol && x.page_id == v.page_id)).OrderBy(x => x.page_id).ThenByDescending(x => x.page_id).ToList();
        }

        public ICollection<SYS_PAGE_MASTER> GetAllPagesAvailable(string rol)
        {
            return _context.SYS_PAGE_MASTER.Where(x => x.SYS_ROLE_PAGE.Any(y => y.role_id == rol)).OrderBy(x => x.page_id).ThenByDescending(x => x.page_id).ToList();
        }

        public ICollection<SYS_PAGE_MASTER> GetAllPagesOfUser(string employeeNumber)
        {
            return _context.SYS_PAGE_MASTER.Where(x => x.SYS_ROLE_PAGE.Any(y => y.SYS_ROLE_MASTER.SYS_ROLE_USER.Any(k => k.emp_no == employeeNumber && x.active_flag == true))).OrderBy(x => x.menu_sequence).ThenBy(x => x.level_2_menu).ThenBy(x => x.level_3_menu).ThenByDescending(x => x.menu_sequence).ToList();
        }

        public List<MenuModel> GetAllLevel1PagesByUserAndroid(string employeeNumber)
        {
            //var level1 = _context.SYS_ROLE_USER.Join(_context.SYS_ROLE_PAGE, sru => sru.role_id, srp => srp.role_id, (sru, srp) => new { sru, srp })
            //    .Join(_context.SYS_PAGE_MASTER, join => join.srp.page_id, spm => spm.page_id, (join, spm) => new { join, spm })
            //    .Where(x => x.spm.type == "android" && x.join.sru.emp_no == employeeNumber && x.spm.level_1_menu != "" && x.spm.active_flag == true)
            //    .Select(s => new MenuModel
            //    {
            //        level_1_menu = s.spm.level_1_menu,
            //        menu_sequence = s.spm.menu_sequence.Value
            //    })
            //    .OrderByDescending(x => x.menu_sequence)
            //    .Distinct()
            //    .ToList();
            var level1 = _context.Database.SqlQuery<MenuModel>(@"select distinct spm.level_1_menu, spm.menu_sequence from SYS_ROLE_USER as srs 
                    JOIN SYS_ROLE_PAGE as srp on srs.role_id = srp.role_id
                    JOIN SYS_PAGE_MASTER as spm on spm.page_id = srp.page_id
                    Where spm.type = 'android' and emp_no = @emp and spm.level_1_menu != '' and active_flag = 1
                    order by menu_sequence", new SqlParameter("emp",employeeNumber)).ToList();
            if (level1.Count > 0)
                return level1;
            else
                return null;
        }

        public List<MenuModel> GetAllLevel2PagesByUserAndLevel1Android(string employeeNumber, string level1)
        {
            var level2 = _context.SYS_ROLE_USER.Join(_context.SYS_ROLE_PAGE, sru => sru.role_id, srp => srp.role_id, (sru, srp) => new { sru, srp })
                .Join(_context.SYS_PAGE_MASTER, join => join.srp.page_id, spm => spm.page_id, (join, spm) => new { join, spm })
                .Where(x => x.spm.type == "android" && x.join.sru.emp_no == employeeNumber && x.spm.level_2_menu == level1 && x.spm.active_flag == true)
                .Select(s => new MenuModel
                {
                    level_2_menu = s.spm.level_2_menu,
                    menu_sequence = s.spm.menu_sequence.Value
                })
                .OrderBy(x => x.level_2_menu).ThenBy(x => x.menu_sequence)
                .Distinct()
                .ToList();
            if (level2.Count > 0)
                return level2;
            else
                return null;
        }
    }
}