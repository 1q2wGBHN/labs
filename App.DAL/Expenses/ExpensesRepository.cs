﻿using App.Entities.ViewModels.Creditors;
using App.Entities.ViewModels.Expenses;
using App.GlobalEntities;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;

namespace App.DAL.Expenses
{
    public class ExpensesRepository
    {
        private readonly GlobalERPEntities _context;

        public ExpensesRepository()
        {
            _context = new GlobalERPEntities();
        }

        public List<ExpensesReportModel> ExpensesReport(string Site_code, int SupplierId, string Category, DateTime? BeginDate, DateTime? EndDate, string Currency, string typeExpense, int Status)
        {
            var site_code = new SqlParameter { ParameterName = "SITEID", Value = Site_code };
            var supplierid = new SqlParameter { ParameterName = "SUPPLIERID", Value = SupplierId };
            var currency = new SqlParameter { ParameterName = "CURRENCY", Value = Currency };
            var typeexpense = new SqlParameter { ParameterName = "TYPEEXPENSE", Value = typeExpense };
            var category = new SqlParameter { ParameterName = "CATEGORYID", Value = Category };
            var status = new SqlParameter { ParameterName = "STATUS", Value = Status };
            var begindate = new SqlParameter { ParameterName = "BEGINDATE", IsNullable = true, SqlDbType = SqlDbType.DateTime, Value = BeginDate };
            var enddate = new SqlParameter { ParameterName = "ENDDATE", IsNullable = true, SqlDbType = SqlDbType.DateTime, Value = EndDate };

            if (BeginDate.HasValue)
                begindate.Value = begindate.Value;
            else
                begindate.Value = DBNull.Value;

            if (EndDate.HasValue)
                enddate.Value = enddate.Value;
            else
                enddate.Value = DBNull.Value;

            var model = _context.Database.SqlQuery<ExpensesReportModel>("sp_Get_ExpenseseReport3 @SITEID, @SUPPLIERID, @CATEGORYID, @BEGINDATE, @ENDDATE, @CURRENCY, @TYPEEXPENSE, @STATUS", site_code, supplierid, category, begindate, enddate, currency, typeexpense, status).ToList();

            return model;
        }

        public List<ExpensesReportModel> ExpensesReportNew(string Site_code, string SupplierId, string Category, DateTime? BeginDate, DateTime? EndDate, string Currency, string typeExpense, string Status)
        {
            var consultaGenerico = @" SELECT XH.xml_id 'ReferenceFolio', S.site_name 'SiteName', E.expense_category 'Category', XH.xml_header_status 'Expense_Status', XH.remark 'RemarkXML',
                CASE WHEN XH.invoice_no is null THEN 'SIN FOLIO'  WHEN XH.invoice_no is not null THEN invoice_no END AS 'Invoice', 
                CASE WHEN XH.uuid_invoice = '' THEN CAST(0 AS BIT) WHEN XH.uuid_invoice != '' THEN CAST(1 AS BIT) END AS 'TypeXML',
                CASE WHEN CONVERT(VARCHAR, XH.cdate, 23) = CONVERT(VARCHAR, GETDATE(), 23) THEN CAST(1 AS BIT) WHEN CONVERT(VARCHAR, XH.cdate, 23) <> CONVERT(VARCHAR, GETDATE(), 23) THEN CAST(0 AS BIT) END AS 'ExpenseToday',
                CASE WHEN (DATEPART(YY, XH.cdate) = DATEPART(YY, GETDATE()) AND DATEPART(month, XH.cdate) = DATEPART(month, GETDATE())) THEN CAST(1 AS BIT) ELSE CAST(0 AS BIT) END AS 'ExpenseMonth',
                CASE WHEN XH.xml_header_status = 9 THEN 'Terminado' WHEN XH.xml_header_status = 8 THEN 'Cancelado' WHEN XH.xml_header_status = 1 THEN 'Solicitud de Cancelación' WHEN XH.xml_header_status = 2 THEN 'Aprobación de Cancelación'  WHEN XH.xml_header_status = 3 THEN 'Pendiente de Cancelación' END AS 'StatusText',
                CASE WHEN E.reference_type = 'GASTO FIJO' THEN  E.remark
                WHEN E.reference_type != 'GASTO FIJO' THEN  E.reference_type END AS 'ReferenceType',  MS.business_name 'Supplier', XH.cdate 'InvoiceDate', XH.subtotal 'SubTotal', XH.iva 'TotalIVA', XH.ieps 'TotalIEPS', XH.currency 'Currency', XH.discount 'Discount', XH.iva_retained 'IvaRet', XH.isr_retained 'IsrRet', XH.total_amount'TotalAmount' 
                FROM EXPENSE E
		        JOIN XML_HEADER XH ON XH.expense_id = e.expense_id
		        JOIN SITES S on S.site_code = E.site_code
		        JOIN MA_SUPPLIER MS ON MS.supplier_id = XH.supplier_id AND MS.supplier_type = 'Acreedor' 
                WHERE convert(varchar, XH.cdate, 23)  BETWEEN convert(varchar,@BEGINDATE, 23) AND convert(varchar,  @ENDDATE, 23) ";
            var consultaGroupBy = " GROUP BY XH.xml_id, S.site_name, E.expense_category, XH.invoice_no, XH.xml_header_status, XH.uuid_invoice, E.remark, XH.remark, E.reference_type, XH.invoice_no , XH.subtotal, MS.business_name, XH.cdate , XH.total_amount, XH.iva, XH.ieps, XH.currency, XH.discount, XH.iva_retained, XH.isr_retained ";

            var firstDayOfMonth = new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1);
            var lastDayOfMonth = firstDayOfMonth.AddMonths(1).AddDays(-1);

            if (BeginDate.HasValue)
            {
                firstDayOfMonth = BeginDate.Value;
                lastDayOfMonth = EndDate.Value;
            }


            List<SqlParameter> parameters = new List<SqlParameter>();
            parameters.Add(new SqlParameter("@BEGINDATE", firstDayOfMonth.Date));
            parameters.Add(new SqlParameter("@ENDDATE", lastDayOfMonth.Date));
            if (!string.IsNullOrWhiteSpace(Status))
            {
                consultaGenerico += " AND XH.xml_header_status = @STATUS ";
                int numberStatus = Convert.ToInt32(Status);
                parameters.Add(new SqlParameter("@STATUS", numberStatus));//Destino
            }
            if (!string.IsNullOrWhiteSpace(Site_code))
            {
                consultaGenerico += " AND E.site_code = @SITEID ";
                parameters.Add(new SqlParameter("@SITEID", Site_code));//Destino
            }
            if (!string.IsNullOrWhiteSpace(SupplierId))
            {
                consultaGenerico += " AND XH.supplier_id = @SUPPLIERID ";
                parameters.Add(new SqlParameter("@SUPPLIERID", Convert.ToInt32(SupplierId)));
            }
            if (!string.IsNullOrWhiteSpace(Category))
            {
                consultaGenerico += " AND E.reference_type = @CATEGORYID ";
                parameters.Add(new SqlParameter("@CATEGORYID", Category));
            }
            if (!string.IsNullOrWhiteSpace(Currency))
            {
                consultaGenerico += " AND XH.currency = @CURRENCY ";
                parameters.Add(new SqlParameter("@CURRENCY", Currency));
            }
            if (!string.IsNullOrWhiteSpace(typeExpense))
            {
                consultaGenerico += " AND E.expense_category = @TYPEEXPENSE ";
                parameters.Add(new SqlParameter("@TYPEEXPENSE", typeExpense));
            }
            var r = _context.Database.SqlQuery<ExpensesReportModel>(consultaGenerico + consultaGroupBy, parameters.ToArray()).ToList();
            return r;
        }

        public List<ExpensesReportModel> ExpensesReportDetailNew(string Site_code, string SupplierId, string Category, DateTime? BeginDate, DateTime? EndDate, string Currency, string typeExpense, string Status)
        {
            var consultaGenerico = @" SELECT XH.xml_id 'ReferenceFolio', S.site_name 'SiteName',  E.expense_category 'Category', XH.invoice_no 'Invoice', 
                CASE WHEN E.reference_type = 'GASTO FIJO' THEN   E.remark
                WHEN E.reference_type != 'GASTO FIJO' THEN  E.reference_type END AS 'ReferenceType',  MS.business_name 'Supplier', E.cdate 'InvoiceDate', XD.item_no 'PartNumber', XD.item_description 'Description', XD.quantity 'Quantity', XD.unit_cost 'UnitCost', XD.item_amount 'SubTotal', XD.discount 'Discount', XD.iva_amount 'TotalIVA', XD.ieps_amount 'TotalIEPS', XH.currency 'Currency', XD.iva_retained 'IvaRet', XD.isr_retained 'IsrRet', (XD.item_amount + XD.iva_amount + XD.ieps_amount - XD.discount - ( XD.iva_retained + XD.isr_retained ) ) 'TotalAmount' 
                FROM EXPENSE E
                JOIN XML_HEADER XH ON XH.expense_id = e.expense_id
                JOIN XML_DETAIL XD ON XD.xml_id = XH.xml_id
                JOIN SITES S on S.site_code = E.site_code
                JOIN MA_SUPPLIER MS ON MS.supplier_id = XH.supplier_id AND MS.supplier_type = 'Acreedor'
                WHERE convert(varchar, E.cdate, 23)  BETWEEN convert(varchar,@BEGINDATE, 23) AND convert(varchar,  @ENDDATE, 23)";

            var firstDayOfMonth = new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1);
            var lastDayOfMonth = firstDayOfMonth.AddMonths(1).AddDays(-1);

            if (BeginDate.HasValue)
            {
                firstDayOfMonth = BeginDate.Value;
                lastDayOfMonth = EndDate.Value;
            }

            List<SqlParameter> parameters = new List<SqlParameter>();
            parameters.Add(new SqlParameter("@BEGINDATE", firstDayOfMonth.Date));
            parameters.Add(new SqlParameter("@ENDDATE", lastDayOfMonth.Date));
            if (!string.IsNullOrWhiteSpace(Site_code))
            {
                consultaGenerico += " AND E.site_code = @SITEID ";
                parameters.Add(new SqlParameter("@SITEID", Site_code));//Destino
            }
            if (!string.IsNullOrWhiteSpace(SupplierId))
            {
                consultaGenerico += " AND XH.supplier_id = @SUPPLIERID ";
                parameters.Add(new SqlParameter("@SUPPLIERID", Convert.ToInt32(SupplierId)));
            }
            if (!string.IsNullOrWhiteSpace(Category))
            {
                consultaGenerico += " AND E.reference_type = @CATEGORYID ";
                parameters.Add(new SqlParameter("@CATEGORYID", Category));
            }
            if (!string.IsNullOrWhiteSpace(Currency))
            {
                consultaGenerico += " AND XH.currency = @CURRENCY ";
                parameters.Add(new SqlParameter("@CURRENCY", Currency));
            }
            if (!string.IsNullOrWhiteSpace(typeExpense))
            {
                consultaGenerico += " AND E.expense_category = @TYPEEXPENSE ";
                parameters.Add(new SqlParameter("@TYPEEXPENSE", typeExpense));
            }
            var xx = _context.Database.SqlQuery<ExpensesReportModel>(consultaGenerico, parameters.ToArray()).ToList();
            return xx;
        }

        public List<ItemCreditorsCategoryModel> AllCreditorsCategories()
        {
            var model = _context.Database.SqlQuery<ItemCreditorsCategoryModel>("sp_Get_AllCreditorsCategories").ToList();

            return model;
        }

        public bool ValidateSupplier(int id, int supplierid)
        {
            var order = _context.PURCHASE_ORDER.Where(X => X.purchase_no == id & X.supplier_id == supplierid).FirstOrDefault();
            if (order != null)
                return true;
            else
                return false;
        }

        public bool ValidateFile(string uuid)
        {
            var xml = _context.XML_HEADER.Where(X => X.uuid_invoice == uuid && (X.xml_header_status == 9 || X.xml_header_status == 1 || X.xml_header_status == 2 || X.xml_header_status == 3)).FirstOrDefault();
            if (xml != null)
                return false;
            else
                return true;
        }

        public bool ValidateInvoiceExpense(string Invoice, int supplier)
        {
            var expenseExists = _context.XML_HEADER.Where(w => w.invoice_no == Invoice && w.supplier_id == supplier && (w.xml_header_status == 9 || w.xml_header_status == 1 || w.xml_header_status == 2 || w.xml_header_status == 3)).FirstOrDefault();
            if (expenseExists != null)
                return true;
            else
                return false;
        }

        public bool ValidateInvoiceExpenseWithId(string Invoice, int supplier, int xml_id)
        {
            var expenseExists = _context.XML_HEADER.Where(w => w.xml_id != xml_id && w.invoice_no == Invoice && w.supplier_id == supplier && (w.xml_header_status == 9 || w.xml_header_status == 1 || w.xml_header_status == 2 || w.xml_header_status == 3)).FirstOrDefault();
            if (expenseExists != null)
                return true;
            else
                return false;
        }


        public EXPENSE CreateExpense(EXPENSE expense, List<XML_HEADER> xml_header)
        {
            using (var trans = _context.Database.BeginTransaction())
            {
                try
                {
                    _context.EXPENSE.Add(expense);
                    _context.SaveChanges();

                    foreach (var d in xml_header)
                        d.expense_id = expense.expense_id;

                    _context.XML_HEADER.AddRange(xml_header);
                    _context.SaveChanges();

                    trans.Commit();
                    return expense;
                }
                catch (Exception e)
                {
                    Console.WriteLine(e);
                    trans.Rollback();
                    return null;
                }
            }
        }
        public int ExpenseCancelAuthorization(int xml_id, bool expense_month, bool expense_today, string comment, string user, string site_code_current)
        {
            int new_status = 0;
            using (var trans = _context.Database.BeginTransaction())
            {
                try
                {
                    var xml_header = _context.XML_HEADER.Where(w => w.xml_id == xml_id && w.xml_header_status == 9).FirstOrDefault();
                    if (xml_header == null)
                        return 1000;
                    var expense = _context.EXPENSE.Where(w => w.expense_id == xml_header.expense_id && w.expense_status == 9).FirstOrDefault();
                    if (expense == null)
                        return 1001;

                    if (expense.site_code != site_code_current)
                        return 1002;

                    if (expense_month) //Gasto dentro del mismo mes se cancela sin necesidad de avisar a contabilidad-distrital
                        if (expense_today)
                        {
                            new_status = 8; //si el gasto es del dia de hoy 
                            expense.expense_cancel_date = DateTime.Now;
                            xml_header.xml_header_cancel_date = DateTime.Now;
                        }
                        else
                            new_status = 1; //Se manda a contabilidad
                    else
                        new_status = 1; // se manda a contabilidad

                    expense.expense_status = new_status;
                    expense.udate = DateTime.Now;
                    expense.uuser = user;
                    expense.program_id = "PURCH023";
                    _context.SaveChanges();

                    xml_header.xml_header_status = new_status;
                    xml_header.udate = DateTime.Now;
                    xml_header.uuser = user;
                    xml_header.remark = "S: " + comment;
                    xml_header.program_id = "PURCH023";
                    _context.SaveChanges();

                    trans.Commit();

                    return new_status;
                }
                catch (Exception e)
                {
                    Console.WriteLine(e);
                    trans.Rollback();
                    return 8001;
                }
            }
        }

        public int ExpenseCancelAproval(int xml_id, string comment, string user, bool expense_month, string site_code_current)
        {
            int new_status = 0;
            using (var trans = _context.Database.BeginTransaction())
            {
                try
                {
                    var xml_header = _context.XML_HEADER.Where(w => w.xml_id == xml_id && w.xml_header_status == 1).FirstOrDefault();
                    if (xml_header == null)
                        return 1000;
                    var expense = _context.EXPENSE.Where(w => w.expense_id == xml_header.expense_id && w.expense_status == 1).FirstOrDefault();
                    if (expense == null)
                        return 1001;
                    if (expense.site_code != site_code_current)
                        return 1002;

                    if (expense_month) //Si el gasto es del mismo mes
                        new_status = 3; //Pendiente de autorizar la sucursal (gerente/mesa de control)
                    else
                        new_status = 2; //Se manda autorización a distrital

                    expense.expense_status = new_status;
                    expense.udate = DateTime.Now;
                    expense.uuser = user;
                    expense.confirmation_cancel = user;
                    expense.program_id = "PURCH024";
                    _context.SaveChanges();

                    xml_header.xml_header_status = new_status;
                    xml_header.udate = DateTime.Now;
                    xml_header.uuser = user;
                    xml_header.confirmation_cancel = user;
                    xml_header.remark += "C: " + comment;
                    xml_header.program_id = "PURCH024";
                    _context.SaveChanges();

                    trans.Commit();

                    return new_status;
                }
                catch (Exception e)
                {
                    Console.WriteLine(e);
                    trans.Rollback();
                    return 8001;
                }
            }
        }

        public int ExpenseCancelPendingStore(int xml_id, string user, string site_code_current)
        {
            using (var trans = _context.Database.BeginTransaction())
            {
                try
                {
                    var xml_header = _context.XML_HEADER.Where(w => w.xml_id == xml_id && w.xml_header_status == 3).FirstOrDefault();
                    if (xml_header == null)
                        return 1000;
                    var expense = _context.EXPENSE.Where(w => w.expense_id == xml_header.expense_id && w.expense_status == 3).FirstOrDefault();
                    if (expense == null)
                        return 1001;
                    if (expense.site_code != site_code_current)
                        return 1002;

                    expense.expense_status = 8;
                    expense.udate = DateTime.Now;
                    expense.uuser = user;
                    expense.authorization_cancel = user;
                    expense.expense_cancel_date = DateTime.Now;
                    _context.SaveChanges();

                    xml_header.xml_header_status = 8;
                    xml_header.udate = DateTime.Now;
                    xml_header.uuser = user;
                    xml_header.xml_header_cancel_date = DateTime.Now;
                    xml_header.authorization_cancel = user;
                    _context.SaveChanges();

                    trans.Commit();
                    return 8;
                }
                catch (Exception e)
                {
                    Console.WriteLine(e);
                    trans.Rollback();
                    return 8001;
                }
            }
        }

        public int ExpenseCancelFinal(int xml_id, string comment, string user, string site_code_current)
        {
            using (var trans = _context.Database.BeginTransaction())
            {
                try
                {
                    var xml_header = _context.XML_HEADER.Where(w => w.xml_id == xml_id && w.xml_header_status == 2).FirstOrDefault();
                    if (xml_header == null)
                        return 1000;
                    var expense = _context.EXPENSE.Where(w => w.expense_id == xml_header.expense_id && w.expense_status == 2).FirstOrDefault();
                    if (expense == null)
                        return 1001;
                    if (expense.site_code != site_code_current)
                        return 1002;

                    expense.expense_status = 8;
                    expense.udate = DateTime.Now;
                    expense.uuser = user;
                    expense.authorization_cancel = user;
                    expense.program_id = "PURCH025";
                    _context.SaveChanges();

                    xml_header.xml_header_status = 8;
                    xml_header.udate = DateTime.Now;
                    xml_header.uuser = user;
                    xml_header.authorization_cancel = user;
                    xml_header.remark += "D: " + comment;
                    xml_header.program_id = "PURCH025";
                    _context.SaveChanges();

                    trans.Commit();
                    return 8;
                }
                catch (Exception e)
                {
                    Console.WriteLine(e);
                    trans.Rollback();
                    return 8001;
                }
            }
        }

        public int ExpenseReverseStatus(int xml_id, string comment, int old_status, string program_id, string user, string site_code_current)
        {
            using (var trans = _context.Database.BeginTransaction())
            {
                try
                {
                    var xml_header = _context.XML_HEADER.Where(w => w.xml_id == xml_id && w.xml_header_status == old_status).FirstOrDefault();
                    if (xml_header == null)
                        return 1000;
                    var expense = _context.EXPENSE.Where(w => w.expense_id == xml_header.expense_id && w.expense_status == old_status).FirstOrDefault();
                    if (expense == null)
                        return 1001;
                    if (expense.site_code != site_code_current)
                        return 1002;

                    expense.expense_status = 9;
                    expense.udate = DateTime.Now;
                    expense.uuser = user;
                    expense.program_id = program_id;
                    _context.SaveChanges();

                    xml_header.xml_header_status = 9;
                    xml_header.udate = DateTime.Now;
                    xml_header.uuser = user;
                    xml_header.remark = "Cancelacion Rechazada: " + comment;
                    xml_header.program_id = program_id;
                    _context.SaveChanges();

                    trans.Commit();
                    return 9;
                }
                catch (Exception e)
                {
                    Console.WriteLine(e);
                    trans.Rollback();
                    return 8001;
                }
            }
        }
        public bool CreateExpenseOneDetail(EXPENSE expense, XML_HEADER xml_header, XML_DETAIL xml_detail)
        {
            using (var trans = _context.Database.BeginTransaction())
            {
                try
                {
                    _context.EXPENSE.Add(expense);
                    _context.SaveChanges();
                    xml_header.expense_id = expense.expense_id;
                    _context.XML_HEADER.Add(xml_header);
                    _context.SaveChanges();
                    xml_detail.xml_id = xml_header.xml_id;
                    _context.XML_DETAIL.Add(xml_detail);
                    _context.SaveChanges();
                    trans.Commit();
                    return true;
                }
                catch (Exception e)
                {
                    Console.WriteLine(e);
                    trans.Rollback();
                    return false;
                }
            }
        }
        public int EditExpenseOneDetail(ExpenseModelFolio model, string user)
        {
            using (var trans = _context.Database.BeginTransaction())
            {
                try
                {
                    var xml_header = _context.XML_HEADER.Where(w => w.xml_id == model.xml_id && w.xml_header_status == 9).FirstOrDefault();
                    if (xml_header == null)
                        return 1001;
                    if (ValidateInvoiceExpenseWithId(model.invoice, xml_header.supplier_id ?? 0, xml_header.xml_id))
                        return 1004;
                    var expense = _context.EXPENSE.Where(w => w.expense_id == xml_header.expense_id && w.expense_status == 9).FirstOrDefault();
                    if (expense == null)
                        return 1002;
                    var xml_detail = _context.XML_DETAIL.Where(w => w.xml_id == xml_header.xml_id).FirstOrDefault();
                    if (xml_header == null)
                        return 1003;
                    xml_header.invoice_no = model.invoice;
                    xml_header.currency = model.currency;
                    xml_header.subtotal = model.subtotal;
                    xml_header.iva = model.iva;
                    xml_header.ieps = model.ieps;
                    xml_header.total_amount = model.total;
                    xml_header.discount = model.discount;
                    xml_header.isr_retained = model.isrRetenidos;
                    xml_header.iva_retained = model.ivaRetenido;
                    xml_header.uuser = user;
                    xml_header.udate = DateTime.Now;
                    xml_header.program_id = "PURCH022";
                    _context.SaveChanges();
                    expense.expense_category = model.area;
                    expense.uuser = user;
                    expense.udate = DateTime.Now;
                    expense.program_id = "PURCH022";
                    _context.SaveChanges();
                    xml_detail.item_description = model.comment;
                    xml_detail.discount = model.discount;
                    xml_detail.ieps_amount = model.ieps;
                    xml_detail.iva_amount = model.iva;
                    xml_detail.unit_cost = model.subtotal / model.quantity;
                    xml_detail.isr_retained = model.isrRetenidos;
                    xml_detail.iva_retained = model.ivaRetenido;
                    xml_detail.quantity = model.quantity;
                    xml_detail.item_amount = model.subtotal;
                    _context.SaveChanges();
                    trans.Commit();
                    return 0;
                }
                catch (Exception e)
                {
                    Console.WriteLine(e);
                    trans.Rollback();
                    return 8001;
                }
            }
        }

        public List<ExpensesChart> CreditorsExpensive(string site_code)
        {
            try
            {
                var item = _context.Database.SqlQuery<ExpensesChart>(@"	SELECT TOP 5 MS.business_name AS ColumnString, SUM(XH.total_amount) AS Value FROM EXPENSE E
	                JOIN XML_HEADER XH ON XH.expense_id = e.expense_id
	                JOIN MA_SUPPLIER MS ON MS.supplier_id = XH.supplier_id AND MS.supplier_type = 'Acreedor'
	                WHERE E.site_code = @siteCode
	                GROUP BY MS.business_name
	                ORDER BY Value DESC",
                new SqlParameter("siteCode", site_code)).ToList();
                return item;
            }
            catch (Exception ex)
            {
                var msg = ex.Message;
                return null;
            }
        }

        public List<ExpensesChart> CreditorsExpensiveQuantity(string site_code)
        {
            try
            {
                var item = _context.Database.SqlQuery<ExpensesChart>(@"	SELECT TOP 5 MS.business_name AS ColumnString, SUM(XD.quantity) AS Value FROM EXPENSE E
	            JOIN XML_HEADER XH ON XH.expense_id = e.expense_id
	            JOIN MA_SUPPLIER MS ON MS.supplier_id = XH.supplier_id AND MS.supplier_type = 'Acreedor'
	            JOIN XML_DETAIL XD ON XD.xml_id = XH.xml_id 
	                WHERE E.site_code = @siteCode
	                GROUP BY MS.business_name
	                ORDER BY Value DESC",
                new SqlParameter("siteCode", site_code)).ToList();
                return item;
            }
            catch (Exception ex)
            {
                var msg = ex.Message;
                return null;
            }
        }

        public List<ExpensesChart> CreditorsExpensiveReferenceType(string site_code)
        {
            try
            {
                var item = _context.Database.SqlQuery<ExpensesChart>(@"	SELECT DISTINCT E.reference_type AS ColumnString, SUM(XH.total_amount) AS Value FROM EXPENSE E
	            JOIN XML_HEADER XH ON XH.expense_id = e.expense_id
	                WHERE E.site_code = @siteCode
	                	GROUP BY E.reference_type
	                    ORDER BY Value DESC",
                new SqlParameter("siteCode", site_code)).ToList();
                return item;
            }
            catch (Exception ex)
            {
                var msg = ex.Message;
                return null;
            }
        }

        public List<ItemXMLDetail> GetAllItemsById(int xml_id)
        {
            var ItemXMLDetail = _context.XML_DETAIL.Where(x => x.xml_id == xml_id)
                .Select(s => new ItemXMLDetail
                {
                    id = s.id,
                    xml_id = s.xml_id ?? 0,
                    item_no = s.item_no,
                    item_description = s.item_description,
                    quantity = s.quantity ?? 0,
                    unit_cost = s.unit_cost ?? 0,
                    item_amount = s.item_amount ?? 0,
                    discount = s.discount ?? 0,
                    iva = s.iva_amount ?? 0,
                    ieps = s.ieps_amount ?? 0,
                    iva_retained = s.iva_retained ?? 0,
                    ieps_retained = s.ieps_amount ?? 0,
                    isr_retained = s.isr_retained ?? 0,
                    total_amount = (s.item_amount + s.iva_amount ?? 0 + s.ieps_amount ?? 0 - s.discount ?? 0 - (s.iva_retained ?? 0 + s.isr_retained ?? 0))
                }).ToList();
            return ItemXMLDetail;
        }

        public ItemXMLDetail GetAllItemHeaderById(int xml_id)
        {
            try
            {
                var ItemHeaderXml = _context.XML_HEADER
                .Join(_context.EXPENSE, x => x.expense_id, e => e.expense_id, (x, e) => new { x, e })
                .Join(_context.XML_DETAIL, d => d.x.xml_id, dd => dd.xml_id, (d, dd) => new { d, dd })
                .Where(w => w.d.x.xml_id == xml_id)
                .Select(s => new ItemXMLDetail
                {
                    id = s.d.x.xml_id,
                    expense_id = s.d.x.expense_id ?? 0,
                    status = s.d.e.expense_status ?? 0,
                    invoice_date = s.d.x.invoice_date,
                    invoice = s.d.x.invoice_no,
                    supplier_id = s.d.x.supplier_id ?? 0,
                    subtotal = s.d.x.subtotal ?? 0,
                    iva = s.d.x.iva ?? 0,
                    ieps = s.d.x.ieps ?? 0,
                    iva_retained = s.d.x.iva_retained ?? 0,
                    isr_retained = s.d.x.isr_retained ?? 0,
                    discount = s.d.x.discount ?? 0,
                    total_amount = s.d.x.total_amount ?? 0,
                    currency = s.d.x.currency,
                    cdate = s.d.x.cdate,
                    expense_type = s.d.e.reference_type,
                    expense_type_exactly = s.d.e.remark,
                    cuser = s.d.e.cuser,
                    remark = s.dd.item_description,
                    print_status = s.d.e.print_status ?? 0 //Modificar
                }).FirstOrDefault();
                return ItemHeaderXml;
            }
            catch (Exception e)
            {
                var msg = e.Message;
                return null;
            }

        }

        /*uuid*/
        public int ExistUuid(string uuid)
        {
            return _context.XML_HEADER.Where(x => x.uuid_invoice == uuid && (x.xml_header_status == 9 || x.xml_header_status == 1 || x.xml_header_status == 2 || x.xml_header_status == 3)).Count();
        }

        public bool UpdatePrintXMLHeader(int xml_id)
        {
            try
            {
                int value = 0;
                var update = _context.EXPENSE.Where(w => w.expense_id == xml_id).FirstOrDefault();
                if (update == null)
                    return false;
                value = update.print_status ?? 0;
                update.print_status = value + 1;
                _context.SaveChanges();
                return true;
            }
            catch (Exception e)
            {
                var msg = e.Message;
                return false;
            }
        }
    }
}

