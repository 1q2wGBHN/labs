﻿using App.Entities.ViewModels.Approvals;
using App.Entities.ViewModels.InternalRequirement;
using App.GlobalEntities;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;

namespace App.DAL.Expenses
{
    public class ExpenseRequisitionsRepository
    {
        private readonly GlobalERPEntities _context;
        private readonly UserMasterRepository _userMasterRepository;

        public ExpenseRequisitionsRepository()
        {
            _context = new GlobalERPEntities();
            _userMasterRepository = new UserMasterRepository();
        }

        //lista de pendientes
        public List<ExpenseRequirementModel> ApprovalHistory(ExpenseRequisitionFilter filter)
        {
            var user = _userMasterRepository.GetUserMasterByUsername(filter.user_name);
            if (user == null) return new List<ExpenseRequirementModel>();

            var nreq = (
                from header in _context.INTERNAL_REQUIREMENT_HEADER
                join hist in _context.APPROVAL_HISTORY
                    on new { header.approval_id } equals new { hist.approval_id }
                join route in _context.APPROVAL_MASTER_ROUTE
                    on new { hist.process_name, hist.step_level } equals new { route.process_name, route.step_level }
                join auser in _context.APPROVAL_PROCESS_USER //el que pide (el pedidor)
                        on new { hist.process_name, step_level = 1 }
                        equals new { auser.process_name, step_level = 1 }
                join auser2 in _context.APPROVAL_PROCESS_USER // el autoriza (el autorizador)
                    on new { hist.process_name }
                    equals new { auser2.process_name }
                where new[] { "RV", "AP", "CN" }.Contains(hist.approval_type)
                    && hist.approval_status == "en espera"
                    && auser.step_level == 1
                    && auser.active_flag == true
                    && auser2.active_flag == true
                    && auser2.emp_no == user.emp_no

                    && (auser.process_group == auser2.process_group || auser2.process_group == "all")
                    && auser2.step_level == hist.step_level
                select new ExpenseRequirementModel
                {
                    step_level = hist.step_level,
                    process_name = hist.process_name,
                    approval_id = hist.approval_id,
                    approval_status = hist.approval_status,
                    emp_no = hist.emp_no,
                    approval_type = hist.approval_type,
                    approval_datetime = hist.approval_datetime,
                    remark = hist.remark,
                    finish_flag = route.finish_flag ?? false,
                    requirement_status = header.requirement_status,
                    folio = header.folio,
                    step_total = _context.APPROVAL_MASTER_ROUTE.Count(e => e.process_name == hist.process_name),
                    total_quantity = _context.INTERNAL_REQUIREMENT_DETAIL.Where(e => e.folio == header.folio).Sum(e => e.quantity),
                    total_estimate_price = _context.INTERNAL_REQUIREMENT_DETAIL.Where(e => e.folio == header.folio).Sum(e => e.quantity * e.estimated_price ?? 0),
                    process_group = hist.process_group
                }
            ).Distinct();
            var list = nreq.ToList();
            return list;
        }

        //lista de peticiones 
        public List<ExpenseRequirementModel> RequestsHistory(ExpenseRequisitionFilter filter)
        {
            var user = _userMasterRepository.GetUserMasterByUsername(filter.user_name);
            if (user == null) return new List<ExpenseRequirementModel>();

            CultureInfo enUS = new CultureInfo("en-US");
            DateTime date0;
            DateTime date1;
            var b0 = DateTime.TryParseExact(filter.date0, "MM/dd/yyyy", enUS, DateTimeStyles.None, out date0);
            var b1 = DateTime.TryParseExact(filter.date1, "MM/dd/yyyy", enUS, DateTimeStyles.None, out date1);
            var req = (from header in _context.INTERNAL_REQUIREMENT_HEADER
                       join hist in _context.APPROVAL_HISTORY
                           on new { header.approval_id } equals new { hist.approval_id }
                       join route in _context.APPROVAL_MASTER_ROUTE
                           on new { hist.process_name, hist.step_level }
                           equals new { route.process_name, route.step_level }
                       join auser in _context.APPROVAL_PROCESS_USER on new { hist.process_name, hist.step_level }
                           equals new { auser.process_name, auser.step_level }
                       where header.requirement_type == "gasto"
                             && (filter.show_ended == true || header.requirement_status < 8)
                             && auser.emp_no == user.emp_no
                             && auser.active_flag == true
                             && hist.approval_type == "RQ"
                             && hist.emp_no == user.emp_no
                             && (!b0 || hist.approval_datetime > date0)
                             && (!b1 || hist.approval_datetime < date1)
                       select hist.approval_id).Distinct();

            var res = (
                from r in req
                join header in _context.INTERNAL_REQUIREMENT_HEADER on r equals header.approval_id
                join hist in _context.APPROVAL_HISTORY on new { header.approval_id }
                    equals new { hist.approval_id }
                join route in _context.APPROVAL_MASTER_ROUTE on new { hist.process_name, hist.step_level }
                    equals new { route.process_name, route.step_level }
                where header.step_level == hist.step_level
                select new ExpenseRequirementModel
                {
                    step_level = hist.step_level,
                    process_name = hist.process_name,
                    approval_id = hist.approval_id,
                    approval_status = hist.approval_status,
                    emp_no = hist.emp_no,
                    approval_type = hist.approval_type,
                    approval_datetime = hist.approval_datetime,
                    remark = hist.remark,
                    finish_flag = route.finish_flag ?? false,
                    requirement_status = header.requirement_status,
                    folio = header.folio,
                    step_total = _context.APPROVAL_MASTER_ROUTE.Count(e => e.process_name == hist.process_name),
                    total_quantity = _context.INTERNAL_REQUIREMENT_DETAIL.Where(e => e.folio == header.folio).Sum(e => e.quantity),
                    total_estimate_price = _context.INTERNAL_REQUIREMENT_DETAIL.Where(e => e.folio == header.folio).Sum(e => e.quantity * e.estimated_price ?? 0),
                    process_group = hist.process_group
                });

            return res.ToList();
        }

        public INTERNAL_REQUIREMENT_HEADER GetByApprovalId(string id)
        {
            try
            {
                return _context.INTERNAL_REQUIREMENT_HEADER.Single(e => e.approval_id == id);
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                return null;
            }
        }

        public bool ExpenseStart(INTERNAL_REQUIREMENT_HEADER model, IEnumerable<INTERNAL_REQUIREMENT_DETAIL> details, string processName)
        {
            using (var trans = _context.Database.BeginTransaction())
            {
                try
                {
                    _context.INTERNAL_REQUIREMENT_HEADER.Add(model);
                    _context.SaveChanges();
                    details = details.Select(e =>
                    {
                        e.folio = model.folio;
                        return e;
                    });
                    _context.INTERNAL_REQUIREMENT_DETAIL.AddRange(details);
                    _context.SaveChanges();
                    trans.Commit();
                    return true;
                }
                catch (Exception e)
                {
                    Console.WriteLine(e);
                    trans.Rollback();
                    return false;
                }
            }
        }

        public bool SetApprovalInfo(int folioHeader, string approvalId, int step, int reqStatus)
        {
            try
            {
                var r = _context.INTERNAL_REQUIREMENT_HEADER.SingleOrDefault(e => e.folio == folioHeader);
                r.requirement_status = reqStatus;
                r.step_level = step;
                r.approval_id = approvalId;
                _context.SaveChanges();
                return true;
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                return false;
            }
        }

        public bool SetExpenseStatus(INTERNAL_REQUIREMENT_HEADER model, int status)
        {
            try
            {
                var r = _context.INTERNAL_REQUIREMENT_HEADER.SingleOrDefault(e => e.folio == model.folio);
                r.requirement_status = status;
                _context.SaveChanges();
                return true;
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                return false;
            }
        }

        public List<InternalRequirementModel> GetDetailsByFolio(int id)
        {
            return (from details in _context.INTERNAL_REQUIREMENT_DETAIL
                    join isup in _context.ITEM_SUPPLIER_CREDITORS on details.supplier_part_number_creditors equals isup.supplier_part_number_creditors
                    join i in _context.ITEM_CREDITORS on isup.part_number_creditors equals i.part_number
                    join cat in _context.ITEM_CREDITORS_CATEGORY on i.category equals cat.category_id
                    join sup in _context.MA_SUPPLIER on isup.supplier_id equals sup.supplier_id
                    join price in _context.ITEM_CREDITORS_PRICE on isup.supplier_part_number_creditors equals price.supplier_part_number_creditors
                        into tt
                    from price2 in tt.DefaultIfEmpty()
                    where i.active_flag == true && i.requisition_flag == true && isup.flag_active == true && price2.flag_active
                    where details.folio == id

                    select new InternalRequirementModel
                    {
                        part_number = i.part_number,
                        description = i.description,
                        category = cat.name,
                        id_item_supplier = isup.supplier_part_number_creditors,
                        supplier_name = sup.commercial_name,
                        price = price2 != null ? price2.price : 0,
                        estimate_price = details.estimated_price ?? 0,
                        folio = details.folio,
                        quantity = details.quantity
                    }).ToList();
        }

        public INTERNAL_REQUIREMENT_HEADER SetExpense(int folio, int expense_id)
        {
            try
            {
                var r = _context.INTERNAL_REQUIREMENT_HEADER.SingleOrDefault(e => e.folio == folio);
                if (r == null) return null;
                r.expense_id = expense_id;
                _context.SaveChanges();
                return r;
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                return null;
            }
        }

        public bool ConfirmCancel(int folio)
        {
            try
            {
                var r = _context.INTERNAL_REQUIREMENT_HEADER.SingleOrDefault(e => e.folio == folio);
                if (r == null || (r.requirement_status != 7)) return false;

                r.requirement_status = 8;
                _context.SaveChanges();
                return true;
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                return false;
            }
        }
    }

    public class ExpenseRequisitionFilter
    {
        public string user_name { get; set; }
        public bool? show_ended { get; set; }
        public string date0 { get; set; }
        public string date1 { get; set; }
    }
}