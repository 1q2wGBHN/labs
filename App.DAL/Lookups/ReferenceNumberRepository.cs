﻿using App.Entities;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static App.Entities.ViewModels.Common.Constants;

namespace App.DAL.Lookups
{
    public class ReferenceNumberRepository
    {
        private DFL_SAIEntities _context;

        public ReferenceNumberRepository()
        {
            _context = new DFL_SAIEntities();
        }

        public REFERENCE_NUMBER GetReferenceNumber(string Document)
        {
            DFL_SAIEntities context = new DFL_SAIEntities();
            var Reference = new REFERENCE_NUMBER();
            context.Configuration.AutoDetectChangesEnabled = false;
            switch (Document)
            {
                case (VoucherType.INCOMES):
                    {
                        Reference = context.REFERENCE_NUMBER.Where(c => c.type_name == "INVOICE" && c.owner_id == "ALL").SingleOrDefault();
                    };
                    break;

                case (VoucherType.EXPENSES):
                    {
                        Reference = context.REFERENCE_NUMBER.Where(c => c.type_name == "CREDIT_NOTES" && c.owner_id == "ALL").SingleOrDefault();
                    };
                    break;

                case (VoucherType.PAYMENTS):
                    {
                        Reference = context.REFERENCE_NUMBER.Where(c => c.type_name == "PAYMENT" && c.owner_id == "ALL").SingleOrDefault();
                    };
                    break;

                case (VoucherType.QUOTES):
                    {
                        Reference = context.REFERENCE_NUMBER.Where(c => c.type_name == "QUOTES" && c.owner_id == "ALL").SingleOrDefault();
                    }
                    break;
            }
            if (context != null)
            {
                context.Configuration.AutoDetectChangesEnabled = true;
                context.Dispose();
            }

            return Reference;
        }

        public bool UpdateReferenceNumber(long Number, string Document)
        {
            try
            {                
                switch (Document)
                {
                    case (VoucherType.INCOMES):
                        {
                            _context.Database.ExecuteSqlCommand("UPDATE REFERENCE_NUMBER SET internal_number = @Number, number = @Number WHERE type_name = 'INVOICE'", new SqlParameter("Number", Number));
                        };
                        break;

                    case (VoucherType.EXPENSES):
                        {
                            _context.Database.ExecuteSqlCommand("UPDATE REFERENCE_NUMBER SET internal_number = @Number, number = @Number WHERE type_name = 'CREDIT_NOTES'", new SqlParameter("Number", Number));
                        };
                        break;

                    case (VoucherType.PAYMENTS):
                        {
                            _context.Database.ExecuteSqlCommand("UPDATE REFERENCE_NUMBER SET internal_number = @Number, number = @Number WHERE type_name = 'PAYMENT'", new SqlParameter("Number", Number));
                        };
                        break;
                }
                return true;
            }
            catch (Exception ex)
            {
                string msg = ex.Message;
                return false;
            }
        }
    }
}
