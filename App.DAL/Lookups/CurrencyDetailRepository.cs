﻿using App.Entities;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static App.Entities.ViewModels.Common.Constants;

namespace App.DAL.Lookups
{
    public class CurrencyDetailRepository
    {
        private DFL_SAIEntities _context;

        public CurrencyDetailRepository()
        {
            _context = new DFL_SAIEntities();
        }

        public decimal CurrencyRate(string CurrencyCode)
        {
            DateTime _Date = DateTime.ParseExact(DateTime.Now.ToString(DateFormat.INT_DATE), DateFormat.INT_DATE, CultureInfo.InvariantCulture);
            var Rate = _context.EXCHANGE_CURRENCY_COMMERCIAL.Where(C => C.currency_date <= _Date).OrderByDescending(O => O.currency_date).Select(S => S.price).FirstOrDefault();
            return Rate;
        }

        public decimal GetCurrencyRateByDate(string date)
        {
            DateTime _Date = DateTime.ParseExact(date, DateFormat.INT_DATE, CultureInfo.InvariantCulture);

            var _CurrencyRate = _context.EXCHANGE_CURRENCY_COMMERCIAL.Where(C => C.currency_date <= _Date).OrderByDescending(O => O.currency_date).Select(S => S.price).FirstOrDefault();
            return _CurrencyRate;
        }

        public decimal GetCurrencySatRateByDate(string date)
        {
            DateTime _Date = DateTime.ParseExact(date, DateFormat.INT_DATE, CultureInfo.InvariantCulture);            
            decimal _CurrencyRate = 0;            
            var _Currency = _context.EXCHANGE_CURRENCY.Where(c => c.currency_date == _Date).FirstOrDefault();

            if (_Currency != null)
                _CurrencyRate = _Currency.price;

            return _CurrencyRate;
        }
    }
}
