﻿using App.Entities;
using System.Collections.Generic;
using System.Linq;

namespace App.DAL.Lookups
{
    public class BankRepository
    {
        private DFL_SAIEntities _context;

        public BankRepository()
        {
            _context = new DFL_SAIEntities();
        }

        public List<BANKS> BanksList()
        {
            return _context.BANKS.Where(c => c.is_active).ToList();
        }

        public string GetBankNameById(int? id)
        {
            if (id.HasValue)
                return _context.BANKS.Where(c => c.id == id.Value).FirstOrDefault().name;
            else
                return string.Empty;
        }
    }
}
