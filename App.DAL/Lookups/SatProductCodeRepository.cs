﻿using App.Entities;
using App.Entities.ViewModels.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App.DAL.Lookups
{
    public class SatProductCodeRepository
    {
        private DFL_SAIEntities _context;

        public SatProductCodeRepository()
        {
            _context = new DFL_SAIEntities();
        }

        public List<SatLookup> SatProductCodeListSPROC()
        {
            var SatProductList = _context.Database.SqlQuery<SatLookup>("sp_Get_SatProductCodes").ToList();
            return SatProductList;
        }
    }
}
