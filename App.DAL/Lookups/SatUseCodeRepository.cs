﻿using App.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App.DAL.Lookups
{
    public class SatUseCodeRepository
    {
        private DFL_SAIEntities _context;

        public SatUseCodeRepository()
        {
            _context = new DFL_SAIEntities();
        }

        public string GetSatUseCodeByDescription(string Description)
        {
            return _context.SAT_USE_CODE.Where(x => x.sat_use_description.ToLower().Replace(" ", "") == Description.ToLower().Replace(" ", "")).Select(x => x.sat_us_code).SingleOrDefault();
        }

        public List<SAT_USE_CODE> SatUseCodeList()
        {
            return _context.SAT_USE_CODE.Where(x => x.active_flag.HasValue && x.active_flag.Value).ToList();
        }
    }
}
