﻿using App.Entities;
using System.Collections.Generic;
using System.Linq;

namespace App.DAL.Lookups
{
    public class StateRepository
    {
        private DFL_SAIEntities _context;

        public StateRepository()
        {
            _context = new DFL_SAIEntities();
        }

        public List<STATES> StatesList()
        {
            return _context.STATES.ToList();
        }
    }
}
