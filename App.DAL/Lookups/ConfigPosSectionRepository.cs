﻿using App.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App.DAL.Lookups
{
    public class ConfigPosSectionRepository
    {
        private DFL_SAIEntities _context;

        public ConfigPosSectionRepository()
        {
            _context = new DFL_SAIEntities();
        }

        public List<DFLPOS_CONFIG_SECTION> GetConfigSectionList()
        {
            return _context.DFLPOS_CONFIG_SECTION.ToList();
        }
    }
}
