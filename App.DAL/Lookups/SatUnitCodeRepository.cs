﻿using App.Entities;
using App.Entities.ViewModels.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App.DAL.Lookups
{
    public class SatUnitCodeRepository
    {
        private DFL_SAIEntities _context;

        public SatUnitCodeRepository()
        {
            _context = new DFL_SAIEntities();
        }

        public List<SAT_UNIT_CODE> SatUnitCodeList()
        {
            return _context.SAT_UNIT_CODE.Where(x => x.active_flag.HasValue && x.active_flag.Value).ToList();
        }

        public List<SatLookup> SatUseCodeListSPROC()
        {
            var UnitSatList = _context.Database.SqlQuery<SatLookup>("sp_Get_UnitSatCodes").ToList();
            return UnitSatList;
        }
    }
}
