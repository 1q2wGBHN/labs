﻿using App.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App.DAL.Lookups
{
    public class SatPaymentMethodCodeRepository
    {
        private DFL_SAIEntities _context;

        public SatPaymentMethodCodeRepository()
        {
            _context = new DFL_SAIEntities();
        }

        public string GetSatPaymentMethodCodeByDescription(string Description)
        {
            return _context.SAT_PAYMENT_METHOD_CODE.Where(x => x.sat_pm_description.ToLower().Replace(" ", "") == Description.ToLower().Replace(" ", "")).Select(x => x.sat_pm_code).SingleOrDefault();
        }

        public List<SAT_PAYMENT_METHOD_CODE> SatPaymentMethodCodeList()
        {            
            return _context.SAT_PAYMENT_METHOD_CODE.Where(c=>c.active_flag).ToList();
        }
    }
}
