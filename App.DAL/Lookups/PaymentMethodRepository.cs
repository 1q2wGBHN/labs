﻿using App.Entities;
using App.Entities.ViewModels.Common;
using System.Collections.Generic;
using System.Linq;

namespace App.DAL.Lookups
{
    public class PaymentMethodRepository
    {
        private DFL_SAIEntities _context;

        public PaymentMethodRepository()
        {
            _context = new DFL_SAIEntities();
        } 

        public List<PAYMENT_METHOD> PaymentMethodList()
        {
            return _context.PAYMENT_METHOD.Where(c => c.pm_menu).ToList();
        }
        
        public PAYMENT_METHOD GetPaymentMethodById(int PaymentMethodId)
        {
            return _context.PAYMENT_METHOD.Find(PaymentMethodId);
        }

        public List<PaymentsMethods> PaymentMethodModelList()
        {
            var PaymentMethodList = (from pm in _context.PAYMENT_METHOD
                                     where pm.pm_menu
                                     select new PaymentsMethods
                                     {
                                         Id = pm.pm_id,
                                         Name = pm.pm_name,
                                         Currency = pm.currency
                                     }).ToList();

            return PaymentMethodList;
        }
    }
}
