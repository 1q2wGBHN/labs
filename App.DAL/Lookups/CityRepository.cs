﻿using App.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App.DAL.Lookups
{
    public class CityRepository
    {
        private DFL_SAIEntities _context;

        public CityRepository()
        {
            _context = new DFL_SAIEntities();
        }

        public List<CITIES> CitiesByStateId(int StateId)
        {
            _context.Configuration.ProxyCreationEnabled = false;
            return _context.CITIES.Where(c => c.state_id == StateId).ToList();
        }

        public List<CITIES> CitiesList()
        {            
            return _context.CITIES.ToList();
        }
    }
}
