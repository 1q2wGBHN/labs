﻿using App.Entities.ViewModels.ServicesCalendar;
using App.GlobalEntities;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;

namespace App.DAL.ServicesCalendar
{
    public class ServicesCalendarHeaderRepository
    {
        private readonly GlobalERPEntities _contextGlobal;

        public ServicesCalendarHeaderRepository()
        {
            _contextGlobal = new GlobalERPEntities();
        }

        public List<ServicesCalendarModel> GetServicesHeaderBySiteCode(string site)
        {
            return _contextGlobal.SERVICES_CALENDAR
                .Where(x => x.site_code == site)
                .Select(x => new ServicesCalendarModel
                {
                    service_id = x.service_id,
                    service_date = x.service_date.ToString(),
                    service_status_ = x.service_status == 1 ? "Programado"
                    : x.service_status == 8 ? "Cancelado"
                    : x.service_status == 9 ? "Terminado"
                    : null,
                    site_code = x.site_code
                }).ToList();
        }

        public List<ServicesCalendarModel> GetServicesHeaderByDate(string site, DateTime start, DateTime finish)
        {
            var y = _contextGlobal.SERVICES_CALENDAR
                .Where(x => x.site_code == site && x.service_date >= start.Date && x.service_date <= finish.Date)
                .Select(x => new ServicesCalendarModel
                {
                    service_id = x.service_id,
                    service_date = x.service_date.ToString(),
                    service_status_ = x.service_status == 1 ? "Programado"
                    : x.service_status == 8 ? "Cancelado"
                    : x.service_status == 9 ? "Terminado"
                    : null,
                    site_code = x.site_code
                }).ToList();
            return y;
        }

        public List<ServicesCalendarModel> GetServicesHeaderByDateAndStatus(string site, DateTime start, DateTime finish, int status)
        {
            var y = _contextGlobal.SERVICES_CALENDAR
                .Where(x => x.site_code == site && x.service_date >= start.Date && x.service_date <= finish.Date && x.service_status == status)
                .Select(x => new ServicesCalendarModel
                {
                    service_id = x.service_id,
                    service_date = x.service_date.ToString(),
                    service_status_ = x.service_status == 1 ? "Programado"
                    : x.service_status == 8 ? "Cancelado"
                    : x.service_status == 9 ? "Terminado"
                    : null,
                    site_code = x.site_code
                }).ToList();
            return y;
        }

        public List<ServicesCalendarDetailModel> GetServicesDetail(int id)
        {
            try
            {
                var detail = _contextGlobal.SERVICES_CALENDAR_DETAIL
                    .Join(_contextGlobal.MAQUINERY_EQUIPMENT, scd => scd.equipment_id, me => me.equipment_id, (scd, me) => new { scd, me })
                    .Join(_contextGlobal.MA_SUPPLIER, scdMa => scdMa.scd.supplier_id, ma => ma.supplier_id, (scdMa, ma) => new { scdMa, ma })
                    .Where(x => x.scdMa.scd.service_id == id)
                    .Select(x => new ServicesCalendarDetailModel
                    {
                        equipment_id = x.scdMa.scd.equipment_id,
                        service_id = x.scdMa.scd.service_id,
                        supplier_id = x.scdMa.scd.supplier_id,
                        equipment_description = x.scdMa.me.equipment_description,
                        supplier_name = x.ma.commercial_name,
                        service_cost = x.scdMa.scd.service_cost,
                        service_department = x.scdMa.scd.service_department,
                        quotation_document = x.scdMa.scd.quotation_document,
                        service_date_finished2 = x.scdMa.scd.service_date_finished.ToString(),
                        currency = x.scdMa.scd.currency ?? "N/A"
                    }).ToList();
                return detail;
            }
            catch (Exception)
            {
                return null;
            }
        }

        public List<ServicesCalendarDetailModel> GetQuotationDocument(string site, int id, int supplier, int equipment)
        {
            try
            {
                var detail = _contextGlobal.SERVICES_CALENDAR
                    .Join(_contextGlobal.SERVICES_CALENDAR_DETAIL, sc => sc.service_id, scd => scd.service_id, (sc, scd) => new { sc, scd })
                    .Where(x => x.sc.site_code == site && x.sc.service_id == id && x.scd.supplier_id == supplier && x.scd.equipment_id == equipment)
                    .Select(x => new ServicesCalendarDetailModel
                    {
                        service_status = x.sc.service_status,
                        quotation_document = x.scd.quotation_document
                    }).ToList();
                return detail;
            }
            catch (Exception)
            {
                return null;
            }
        }

        public bool ModifyServicesDetail(ServicesCalendarDetailModel model, int id, int service_id, string user)
        {
            try
            {
                var service = _contextGlobal.SERVICES_CALENDAR_DETAIL.Where(elements => elements.equipment_id == id && elements.service_id == service_id).SingleOrDefault();
                service.service_commentary = model.service_commentary;
                service.service_date_finished = model.service_date_finished;
                if (model.service_document != null) { service.service_document = model.service_document.Split(',')[1] ?? ""; }
                service.udate = DateTime.Now;
                service.uuser = user;
                _contextGlobal.SaveChanges();
                return true;
            }
            catch (Exception ex)
            {
                var msg = ex.Message;
                return false;
            }
        }

        public bool ModifyServicesHeader(int service_id, int status, string user, int expense_id)
        {
            try
            {
                var service = _contextGlobal.SERVICES_CALENDAR.Where(elements => elements.service_id == service_id).SingleOrDefault();
                if (expense_id != 0)
                    service.expense_id = expense_id;

                service.service_status = status;
                service.udate = DateTime.Now;
                service.uuser = user;
                service.program_id = "MRO001.cshtml";
                _contextGlobal.SaveChanges();
                return true;
            }
            catch (Exception ex)
            {
                var msg = ex.Message;
                return false;
            }
        }

        public bool UpdateServicesHeader()
        {
            try
            {
                var save = _contextGlobal.SaveChanges();
                return true;
            }
            catch (Exception ex)
            {
                var msg = ex.Message;
                return false;
            }
        }

        public List<string> RFCOptions(int service_id)
        {
            return _contextGlobal.Database.SqlQuery<string>(@"select rfc from SERVICES_CALENDAR_DETAIL scd inner join MA_SUPPLIER ma on ma.supplier_id = scd.supplier_id
            where service_id = @service", new SqlParameter("@service", service_id)).ToList();
        }
    }
}