﻿using App.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App.DAL.SysLog
{
    public class SysLogRepository
    {
        private DFL_SAIEntities _context;

        public SysLogRepository()
        {
            _context = new DFL_SAIEntities();
        }

        public bool CreateLogStampDocuments(SYS_LOG Item)
        {
            try
            {
                _context.SYS_LOG.Add(Item);
                _context.SaveChanges();
                return true;
            }
            catch (Exception ex)
            {
                string msg = ex.Message;
                return false;
            }
        }
    }
}
