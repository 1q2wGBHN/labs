﻿using App.Entities;
using System;
using System.Data.Entity.Core.Objects;

namespace App.DAL.MovementNumber
{
    public class MovementNumberRepository
    {
        private readonly DFL_SAIEntities _context;

        public MovementNumberRepository()
        {
            _context = new DFL_SAIEntities();
        }

        public string ExecuteSp_Get_Next_Document()
        {
            ObjectParameter document = new ObjectParameter("Document", typeof(int));

            _context.sp_Get_Next_Document("PURCHASE_ORDER", document);

            var numberFolio = Convert.ToString(document.Value);

            return numberFolio;
        }

        public string ExecSp_Get_Next_DocumentScrap()
        {
            ObjectParameter document = new ObjectParameter("Document", typeof(int));
            _context.sp_Get_Next_Document("SCRAP_MATERIAL", document);
            var documentNumber = Convert.ToString(document.Value);
            return documentNumber;
        }

        public string ExecSp_Get_Next_Document_Transfer()
        {
            ObjectParameter document = new ObjectParameter("Document", typeof(int));
            _context.sp_Get_Next_Document("STOCK_TRANSFER", document);
            if (document.Value == null) return null;
            var ret = Convert.ToString(document.Value);
            if (ret == "1513") return null;
            return ret;
        }
    }
}