﻿using App.DAL.Site;
using App.DAL.StorageLocation;
using App.Entities;
using App.Entities.ViewModels.DamagedsGoods;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.SqlClient;
using System.Linq;

namespace App.DAL.Item
{
    public class DamagedsGoodsRepository
    {
        private readonly DFL_SAIEntities _context;
        private readonly SiteConfigRepository _SiteConfigRepo;
        private readonly ItemStockRepository _ItemStockRepository;
        private readonly DamagedsGoodsItemRepository _DamagedsGoodsItemRepository;
        private readonly StorageLocationRepository _storageLocationRepository;

        public DamagedsGoodsRepository()
        {
            _context = new DFL_SAIEntities();
            _SiteConfigRepo = new SiteConfigRepository();
            _ItemStockRepository = new ItemStockRepository();
            _DamagedsGoodsItemRepository = new DamagedsGoodsItemRepository();
            _storageLocationRepository = new StorageLocationRepository();
        }

        public List<DAMAGEDS_GOODS> GetAll() => _context.DAMAGEDS_GOODS.ToList();

        public bool UpdateDamageGoodsItemStatus(List<DamagedsGoodsModel> Model, string document, string msj, string user)
        {
            if (msj == "APROBADO")
            {
                foreach (var item in Model)
                {
                    var DamagedGoodsItem = _context.DAMAGEDS_GOODS_ITEM.Where(x => x.damaged_goods_doc == document & x.part_number == item.PartNumber).FirstOrDefault();
                    if (DamagedGoodsItem != null)
                    {
                        DamagedGoodsItem.process_status = 2;
                        DamagedGoodsItem.uuser = user;
                        DamagedGoodsItem.udate = DateTime.Now;
                        _context.SaveChanges();
                    }
                }

                var Items = _context.DAMAGEDS_GOODS_ITEM.Where(x => x.damaged_goods_doc == document & x.process_status == 1).ToList();
                foreach (var item in Items)
                {
                    item.process_status = 3;
                    item.uuser = user;
                    item.udate = DateTime.Now;
                    _context.SaveChanges();
                }
            }
            else
            {
                var Items = _context.DAMAGEDS_GOODS_ITEM.Where(x => x.damaged_goods_doc == document).ToList();
                foreach (var item in Items)
                {
                    item.process_status = 3;
                    item.uuser = user;
                    item.udate = DateTime.Now;
                    _context.SaveChanges();
                }
            }

            return true;
        }

        public List<DAMAGEDS_GOODS> GetRmaHeadByStatus(int statusId) => _context.DAMAGEDS_GOODS.Where(x => x.process_status == statusId).ToList();

        public DamagedGoodsHeadModel GetRmaHeadByDocument(string Document)
        {
            var model = _context.DAMAGEDS_GOODS
                .GroupJoin(_context.MA_SUPPLIER, a => a.supplier_id, b => b.supplier_id, (a, b) => new { a, b })
                .SelectMany(x => x.b.DefaultIfEmpty(), (x, y) => new { x, y })
                .Where(o => o.x.a.damaged_goods_doc == Document)
                .Select(p => new DamagedGoodsHeadModel
                {
                    DamagedGoodsDoc = p.x.a.damaged_goods_doc,
                    Site = p.x.a.site_code,
                    ProcessType = p.x.a.process_type,
                    Supplier = p.y.business_name == null ? "N/A" : p.y.business_name,
                    ProcessStatus = p.x.a.process_status,
                    Comment = p.x.a.comment,

                }).FirstOrDefault();

            var DamageGoodsItemList = _context.DAMAGEDS_GOODS_ITEM
                                            .Join(_context.ITEM, a => a.part_number, b => b.part_number, (a, b) => new { a, b })
                                            .Where(x => x.a.damaged_goods_doc == Document).ToList();

            foreach (var item in DamageGoodsItemList)
                model.DamageGoodsItem.Add(new DamagedsGoodsModel { PartNumber = item.a.part_number, Description = item.b.part_description, Quantity = item.a.quantity, Storage = _ItemStockRepository.GetItemStockByPartNumber(item.a.part_number).Storage });

            var Site = _SiteConfigRepo.GetSiteCodeAddress();
            model.SiteName = Site.SiteName;
            model.SiteAdress = Site.SiteAddress;

            return model;
        }

        public List<DamagedGoodsHeadModel> GetAllRmaHeadByType(string Type, DateTime date1, DateTime date2)
        {
            return _context.DAMAGEDS_GOODS.Join(_context.MA_SUPPLIER, a => a.supplier_id, b => b.supplier_id, (a, b) => new { a, b })
           .Where(o => o.a.cdate >= date1 && o.a.cdate <= date2 && o.a.process_type == Type)
           .Select(p => new DamagedGoodsHeadModel
           {
               DamagedGoodsDoc = p.a.damaged_goods_doc,
               Site = p.a.site_code,
               ProcessType = p.a.process_type,
               Supplier = p.b.business_name,
               ProcessStatus = p.a.process_status,
               Comment = p.a.comment,
               SupplierId = p.b.supplier_id
           }).ToList();
        }

        public List<DamagedGoodsHeadModel> GetAllRmaHeadStatus(string Type, DateTime date1, DateTime date2, int status)
        {
            var model = _context.DAMAGEDS_GOODS.Join(_context.MA_SUPPLIER, a => a.supplier_id, b => b.supplier_id, (a, b) => new { a, b })
           .Where(o => DbFunctions.TruncateTime(o.a.cdate) >= DbFunctions.TruncateTime(date1) && DbFunctions.TruncateTime(o.a.cdate) <= DbFunctions.TruncateTime(date2) && o.a.process_type == Type && o.a.process_status == status)
           .Select(p => new DamagedGoodsHeadModel
           {
               DamagedGoodsDoc = p.a.damaged_goods_doc,
               Site = p.a.site_code,
               ProcessType = p.a.process_type,
               Supplier = p.b.business_name,
               ProcessStatus = p.a.process_status,
               Comment = p.a.comment,
               SupplierId = p.b.supplier_id
           }).ToList();

            return model;
        }

        public List<DamagedGoodsHeadModel> GetAllRma(string Type, DateTime? date1, DateTime? date2, int status)
        {
            var model = @"select 
                    dg.damaged_goods_doc as DamagedGoodsDoc, site_code as Site ,
                    dg.process_type as ProcessType, ms.business_name as Supplier,
                    dg.process_status as ProcessStatus, dg.comment as Comment,
                    ms.supplier_id  as SupplierId , sum(amount_doc) as Total,
                    CONVERT (date,dg.cdate,101) as cdate,
					CONVERT (date,dg.udate,101) as processDate
                    from DAMAGEDS_GOODS dg
                    inner join MA_SUPPLIER ms on ms.supplier_id = dg.supplier_id
                    inner join  DAMAGEDS_GOODS_ITEM dgi on dgi.damaged_goods_doc = dg.damaged_goods_doc and dg.process_status  = dgi.process_status 
                    where dg.process_type = @type 
                    ";
            var count = 0;
            if (date1.HasValue && date2.HasValue)
            {
                count += 1;
                model += @"and  CONVERT (date,ISNULL( dg.udate, dg.cdate),101) >= @date1 and
                    CONVERT(date, ISNULL( dg.udate, dg.cdate), 101) <= @date2 ";
            }
            if (status >= 0)
            {
                count += 2;
                model += @" and dg.process_status = @status ";
            }
            if (count == 2)
            {
                return _context.Database.SqlQuery<DamagedGoodsHeadModel>($@"{model} group by 
                    dg.damaged_goods_doc ,site_code ,
                    dg.process_type , ms.business_name ,
                    dg.process_status , dg.comment ,
                    ms.supplier_id, dg.cdate, dg.udate ",
                    new SqlParameter("@type", Type),
                    new SqlParameter("@status", status)).ToList();
            }
            if (count == 1)
            {
                return _context.Database.SqlQuery<DamagedGoodsHeadModel>($@"{model} group by 
                    dg.damaged_goods_doc ,site_code ,
                    dg.process_type , ms.business_name ,
                    dg.process_status , dg.comment ,
                    ms.supplier_id, dg.cdate, dg.udate",
                    new SqlParameter("@date1", date1.Value.Date),
                    new SqlParameter("@date2", date2.Value.Date),
                    new SqlParameter("@type", Type)
                    ).ToList();
            }
            if (count == 3)
            {
                return _context.Database.SqlQuery<DamagedGoodsHeadModel>($@"{model} group by 
                    dg.damaged_goods_doc ,site_code ,
                    dg.process_type , ms.business_name ,
                    dg.process_status , dg.comment ,
                    ms.supplier_id, dg.cdate, dg.udate",
                  new SqlParameter("@date1", date1.Value.Date),
                  new SqlParameter("@date2", date2.Value.Date),
                  new SqlParameter("@type", Type),
                  new SqlParameter("@status", status)).ToList();
            }
            return _context.Database.SqlQuery<DamagedGoodsHeadModel>($@"{model} group by 
                    dg.damaged_goods_doc ,site_code ,
                    dg.process_type , ms.business_name ,
                    dg.process_status , dg.comment ,
                    ms.supplier_id, dg.cdate, dg.udate ",
                    new SqlParameter("@type", Type)).ToList();
        }

        public string CreateDamagedsGoodsBanati(string User, string Comments, int Status, string siteCode)
        {
            try
            {
                int remision = 0;

                if (siteCode == "0008")
                {
                    remision = _DamagedsGoodsItemRepository.BanatiIdSupplier("DE LA FAMILIA");//SISTEMA PARA EL DESARROLLO INTEGRAL DE LA FAMILIA 13328
                }
                else
                {
                    remision = _DamagedsGoodsItemRepository.BanatiIdSupplier("BANATI");//Banco de Alimentos de Tijuana, A.C (BANATI)
                }

                var Damaged = new DAMAGEDS_GOODS()
                {
                    cuser = User,
                    process_status = Status,
                    comment = Comments,
                    site_code = _context.SITE_CONFIG.ToList().SingleOrDefault().site_code,
                    process_type = "REMISION",
                    supplier_id = remision,
                    program_id = "RMA003.cshtml",
                    cdate = DateTime.Now
                };

                var DateReturn = _context.Database.SqlQuery<StoreProcedureResult>("DECLARE @return_value int, @Document nvarchar(10) EXEC @return_value = [dbo].[sp_Get_Next_Document] @Process_Name = N'SCRAP_MATERIAL', @Document = @Document OUTPUT SELECT 'Document'= @Document, 'ReturnValue' = @return_value").ToList();
                Damaged.damaged_goods_doc = DateReturn[0].Document;

                _context.DAMAGEDS_GOODS.Add(Damaged);
                _context.SaveChanges();

                return Damaged.damaged_goods_doc;
            }
            catch (Exception ex)
            {
                var msg = ex.Message;
                return "";
            }
        }

        public bool UpdateDamagedsGoodsBanati(string Document, string Comments, string User, int Status, string ProgramId)
        {
            try
            {
                var Damaged = _context.DAMAGEDS_GOODS.Where(x => x.damaged_goods_doc == Document).FirstOrDefault();
                Damaged.process_status = Status;
                Damaged.comment = Comments;
                Damaged.uuser = User;
                Damaged.udate = DateTime.Now;
                Damaged.program_id = ProgramId;

                _context.Entry(Damaged).State = EntityState.Modified;
                _context.SaveChanges();

                return true;
            }
            catch (Exception ex)
            {
                var msg = ex.Message;
                return false;
            }
        }

        public DamagedGoodsHeadModel CreateDamagedGoodsHeadModel(int Supplier, string User, int Status)
        {
            var Damaged = new DAMAGEDS_GOODS()
            {
                cuser = User,
                process_status = Status,
                site_code = _context.SITE_CONFIG.ToList().SingleOrDefault().site_code,
                supplier_id = Supplier,
                process_type = "RMA",
                program_id = "RMA001.cshtml",
                cdate = DateTime.Now
            };
            var DateReturn = _context.Database.SqlQuery<StoreProcedureResult>("DECLARE	@return_value int, @Document nvarchar(10) EXEC    @return_value = [dbo].[sp_Get_Next_Document] @Process_Name = N'SCRAP_MATERIAL', @Document = @Document OUTPUT SELECT 'Document'= @Document, 'ReturnValue' = @return_value").ToList(); //SELECT 'Document'= @Document,'ReturnValue' = @return_value
            Damaged.damaged_goods_doc = DateReturn[0].Document;
            _context.DAMAGEDS_GOODS.Add(Damaged);
            _context.SaveChanges();
            var DamagedsGoods = new DamagedGoodsHeadModel()
            {
                Comment = "",
                DamagedGoodsDoc = Damaged.damaged_goods_doc,
                ProcessStatus = Damaged.process_status,
                Site = Damaged.site_code,
                ProcessType = Damaged.process_type,
                SupplierId = Supplier
            };
            return DamagedsGoods;
        }

        public DamagedGoodsHeadModel CreateDamagedGoodsHeadModelAndroid(int Supplier, string User, int value)
        {
            var Damaged = new DAMAGEDS_GOODS()
            {
                cuser = User,
                process_status = value,
                site_code = _context.SITE_CONFIG.ToList().SingleOrDefault().site_code,
                supplier_id = Supplier,
                process_type = "RMA",
                program_id = "Android",
                cdate = DateTime.Now
            };
            var DateReturn = _context.Database.SqlQuery<StoreProcedureResult>("DECLARE	@return_value int, @Document nvarchar(10) EXEC    @return_value = [dbo].[sp_Get_Next_Document] @Process_Name = N'SCRAP_MATERIAL', @Document = @Document OUTPUT SELECT 'Document'= @Document, 'ReturnValue' = @return_value").ToList(); //SELECT 'Document'= @Document,'ReturnValue' = @return_value
            Damaged.damaged_goods_doc = DateReturn[0].Document;
            _context.DAMAGEDS_GOODS.Add(Damaged);
            _context.SaveChanges();
            var DamagedsGoods = new DamagedGoodsHeadModel()
            {
                Comment = "",
                DamagedGoodsDoc = Damaged.damaged_goods_doc,
                ProcessStatus = Damaged.process_status,
                Site = Damaged.site_code,
                ProcessType = Damaged.process_type,
                SupplierId = Supplier
            };
            return DamagedsGoods;
        }

        public DamagedGoodsHeadModel CreateDamagedGoodsHeadModelAndroid(int Supplier, string User, int value, string comments)
        {
            var Damaged = new DAMAGEDS_GOODS()
            {
                cuser = User,
                process_status = value,
                site_code = _context.SITE_CONFIG.ToList().SingleOrDefault().site_code,
                supplier_id = Supplier,
                comment = comments,
                process_type = "RMA",
                program_id = "Android",
                cdate = DateTime.Now
            };
            var DateReturn = _context.Database.SqlQuery<StoreProcedureResult>("DECLARE	@return_value int, @Document nvarchar(10) EXEC    @return_value = [dbo].[sp_Get_Next_Document] @Process_Name = N'SCRAP_MATERIAL', @Document = @Document OUTPUT SELECT 'Document'= @Document, 'ReturnValue' = @return_value").ToList(); //SELECT 'Document'= @Document,'ReturnValue' = @return_value
            Damaged.damaged_goods_doc = DateReturn[0].Document;
            _context.DAMAGEDS_GOODS.Add(Damaged);
            _context.SaveChanges();
            var DamagedsGoods = new DamagedGoodsHeadModel()
            {
                Comment = Damaged.comment,
                DamagedGoodsDoc = Damaged.damaged_goods_doc,
                ProcessStatus = Damaged.process_status,
                Site = Damaged.site_code,
                ProcessType = Damaged.process_type,
                SupplierId = Supplier
            };
            return DamagedsGoods;
        }

        public DamagedGoodsHeadModel CreateDamagedGoodsHeadModelRequest(int Supplier, string User)
        {
            var Damaged = new DAMAGEDS_GOODS()
            {
                cuser = User,
                process_status = 2,
                site_code = _context.SITE_CONFIG.ToList().SingleOrDefault().site_code,
                supplier_id = Supplier,
                process_type = "RMA",
                program_id = "RMA001.cshtml",
                cdate = DateTime.Now
            };
            var DateReturn = _context.Database.SqlQuery<StoreProcedureResult>("DECLARE	@return_value int, @Document nvarchar(10) EXEC    @return_value = [dbo].[sp_Get_Next_Document] @Process_Name = N'SCRAP_MATERIAL', @Document = @Document OUTPUT SELECT 'Document'= @Document, 'ReturnValue' = @return_value").ToList(); //SELECT 'Document'= @Document,'ReturnValue' = @return_value
            Damaged.damaged_goods_doc = DateReturn[0].Document;
            _context.DAMAGEDS_GOODS.Add(Damaged);
            _context.SaveChanges();
            var DamagedsGoods = new DamagedGoodsHeadModel()
            {
                Comment = "",
                DamagedGoodsDoc = Damaged.damaged_goods_doc,
                ProcessStatus = Damaged.process_status,
                Site = Damaged.site_code,
                ProcessType = Damaged.process_type,
                SupplierId = Supplier
            };
            return DamagedsGoods;
        }

        public int MethodReverseBlockToUnrestricted(string siteCode, string partNumber, int quantity, string sL, string to_sL, string program_id, string User)
        {
            try
            {
                var DateReturn = _context.Database.SqlQuery<StoreProcedureResult>("DECLARE	@return_value int, @Document nvarchar(10) EXEC    @return_value = [dbo].[sproc_ST_BlockedToUnrestricted] @site_code = N'" + siteCode + "', @part_number = N'" + partNumber + "', @quantity = " + quantity + ", @sl = N'" + sL + "', @to_sl = N'" + to_sL + "', @program_id = N'" + program_id + "', @user = N'" + User + "', @Document = @Document OUTPUT SELECT 'Document'= @Document, 'ReturnValue' = @return_value").ToList(); //SELECT 'Document'= @Document,'ReturnValue' = @return_value
                if (DateReturn != null)
                {
                    if (DateReturn[0].Document != null || DateReturn[0].Document != "")
                        return 0;

                    if (DateReturn[0].ReturnValue == 6001) //No existe locación MERMA
                        return 1;
                    else if (DateReturn[0].ReturnValue == 6002) //No existe locación VENTA
                        return 2;
                    else if (DateReturn[0].ReturnValue == 6003) //No tiene stock suficiente
                        return 3;
                    else
                        return 5;
                }
                return 5;
            }
            catch (Exception e)
            {
                var msg = e.Message;
                return 5;
            }
        }

        public List<DamagedGoodsHeadModel> GetRMAHeaderDetail(string id)
        {
            var DamagedGood = _context.DAMAGEDS_GOODS.Where(x => x.damaged_goods_doc == id)
                .Join(_context.MA_SUPPLIER, a => a.supplier_id, b => b.supplier_id, (damage, supplier) => new { damage, supplier })
                .Join(_context.SITES, a => a.damage.site_code, b => b.site_code, (damage2, site) => new { damage2, site })
                .Select(x => new DamagedGoodsHeadModel
                {
                    Comment = x.damage2.damage.comment,
                    DamagedGoodsDoc = x.damage2.damage.damaged_goods_doc,
                    ProcessType = x.damage2.damage.process_type,
                    ProcessStatus = x.damage2.damage.process_status,
                    SiteName = x.site.site_name,
                    Supplier = x.damage2.supplier.commercial_name,
                    SupplierId = x.damage2.supplier.supplier_id,
                    document_no = x.damage2.damage.gi_document ?? "-",
                    Weight = x.damage2.damage.weight_of_goods,
                    Print_Document = x.damage2.damage.print_status ?? 0,
                    processDate = x.damage2.damage.udate
                }).ToList();
            foreach (var item in DamagedGood)
            {
                item.DamageGoodsItem = _context.DAMAGEDS_GOODS_ITEM.Where(x => x.damaged_goods_doc == item.DamagedGoodsDoc && x.process_status == item.ProcessStatus).Join(_context.ITEM, x => x.part_number, y => y.part_number, (x, y) => new { x, y }).Select(x => new DamagedsGoodsModel
                {
                    PartNumber = x.x.part_number,
                    Quantity = x.x.quantity,
                    Price = x.x.price != null ? x.x.price.Value : 0,
                    Amount = x.x.price != null ? (x.x.price.Value * x.x.quantity) : 0,
                    Description = x.y.part_description
                }).ToList();
            }
            return DamagedGood;
        }
        
        public List<DAMAGEDS_GOODS> GetFoliosBySupplierByDates(int Supplier, string SiteCode)
        {
            return _context.DAMAGEDS_GOODS.ToList().Where(x => x.supplier_id == Supplier && x.site_code == SiteCode && x.process_status == 0)
                .Select(x => new DAMAGEDS_GOODS
                {
                    damaged_goods_doc = x.damaged_goods_doc,
                    process_type = "Pendiente"
                }).ToList();
        }

        public List<DAMAGEDS_GOODS> GetFoliosBySupplierByDates(string SiteCode)
        {
            return _context.DAMAGEDS_GOODS.ToList()
                .Join(_context.MA_SUPPLIER, x => x.supplier_id, y => y.supplier_id, (x, y) => new { x, y })
                .Where(x => x.x.site_code == SiteCode && x.x.process_status == 0)
            .Select(x => new DAMAGEDS_GOODS
            {
                damaged_goods_doc = x.x.damaged_goods_doc,
                process_type = x.y.commercial_name
            }).ToList();
        }

        //contiene los damagegoods con status 1 que son los aprobados
        public List<DamagedGoodsHeadModel> DamagedGoodsApproved(int Supplier, string ProcessType)
        {
            var DamagedGood = _context.DAMAGEDS_GOODS.Where(x => x.process_status == 2 && x.supplier_id == Supplier && x.process_type == ProcessType).Select(x => new DamagedGoodsHeadModel
            {
                Comment = x.comment,
                DamagedGoodsDoc = x.damaged_goods_doc,
                ProcessStatus = 2,
                ProcessType = x.process_type,
                Site = x.site_code,
                SupplierId = x.supplier_id.Value,
            }).ToList();
            foreach (var item in DamagedGood)
            {
                item.DamageGoodsItem = _context.DAMAGEDS_GOODS_ITEM.Where(x => x.damaged_goods_doc == item.DamagedGoodsDoc).Join(_context.ITEM, x => x.part_number, y => y.part_number, (x, y) => new { x, y }).Select(x => new DamagedsGoodsModel
                {
                    PartNumber = x.x.part_number,
                    Quantity = x.x.quantity,
                    Price = x.x.price != null ? x.x.price.Value : 0,
                    Amount = x.x.price != null ? (x.x.price.Value * x.x.quantity) : 0,
                    Description = x.y.part_description
                }).ToList();
            }
            return DamagedGood;
        }

        //filtro los datos mediante el rma para su imprecion 
        public DamagedGoodsHeadModel DamagedGoodsApprovedPrint(string RMA, int status)
        {
            var DamagedGood = _context.DAMAGEDS_GOODS.Where(x => x.process_status == status && x.damaged_goods_doc == RMA).Select(x => new DamagedGoodsHeadModel
            {
                Comment = x.comment,
                DamagedGoodsDoc = x.damaged_goods_doc,
                ProcessStatus = status,
                ProcessType = x.process_type,
                Site = x.site_code,
                SupplierId = x.supplier_id.Value,
                Weight = x.weight_of_goods
            }).SingleOrDefault();
            if (DamagedGood != null)
            {
                DamagedGood.DamageGoodsItem = _context.DAMAGEDS_GOODS_ITEM.Where(x => x.damaged_goods_doc == RMA && x.process_status == 2).Join(_context.ITEM, x => x.part_number, y => y.part_number, (x, y) => new { x, y }).Select(x => new DamagedsGoodsModel
                {
                    PartNumber = x.x.part_number,
                    Quantity = x.x.quantity,
                    Price = x.x.price != null ? x.x.price.Value : 0,
                    Amount = x.x.price != null ? (x.x.price.Value * x.x.quantity) : 0,
                    Description = x.y.part_description
                }).ToList();
            }
            return DamagedGood;
        }

        public DamagedGoodsHeadModel DamagedGoodsApprovedPrint(int supplier, string RMA, int status)
        {
            var DamagedGood = _context.DAMAGEDS_GOODS.Where(x => x.process_status == status && x.damaged_goods_doc == RMA && x.supplier_id == supplier).Select(x => new DamagedGoodsHeadModel
            {
                Comment = x.comment,
                DamagedGoodsDoc = x.damaged_goods_doc,
                ProcessStatus = 1,
                ProcessType = x.process_type,
                Site = x.site_code,
                SupplierId = x.supplier_id.Value,
                cuser = x.cuser,
                Weight = x.weight_of_goods
            }).SingleOrDefault();
            if (DamagedGood != null)
            {
                DamagedGood.DamageGoodsItem = _context.DAMAGEDS_GOODS_ITEM.Where(x => x.damaged_goods_doc == RMA).Join(_context.ITEM, x => x.part_number, y => y.part_number, (x, y) => new { x, y }).Select(x => new DamagedsGoodsModel
                {
                    PartNumber = x.x.part_number,
                    Quantity = x.x.quantity,
                    Price = x.x.price != null ? x.x.price.Value : 0,
                    Amount = x.x.price != null ? (x.x.price.Value * x.x.quantity) : 0,
                    Description = x.y.part_description
                }).ToList();
            }
            return DamagedGood;
        }

        public DamagedGoodsHeadModel DamagedGoodsApprovedPrintRMA(int supplier, string RMA)
        {
            var DamagedGood = _context.DAMAGEDS_GOODS.Where(x => x.damaged_goods_doc == RMA && x.supplier_id == supplier).Select(x => new DamagedGoodsHeadModel
            {
                Comment = x.comment,
                DamagedGoodsDoc = x.damaged_goods_doc,
                ProcessStatus = x.process_status,
                ProcessType = x.process_type,
                Site = x.site_code,
                SupplierId = x.supplier_id.Value,
                cuser = x.cuser,
                document_no = x.gi_document,
                Udate = x.udate ?? DateTime.Now,
                Weight = x.weight_of_goods
            }).SingleOrDefault();
            if (DamagedGood != null)
            {
                DamagedGood.DamageGoodsItem = _context.DAMAGEDS_GOODS_ITEM.Where(x => x.damaged_goods_doc == RMA).Join(_context.ITEM, x => x.part_number, y => y.part_number, (x, y) => new { x, y }).Select(x => new DamagedsGoodsModel
                {
                    PartNumber = x.x.part_number,
                    Quantity = x.x.quantity,
                    Price = x.x.price != null ? x.x.price.Value : 0,
                    Amount = x.x.price != null ? (x.x.price.Value * x.x.quantity) : 0,
                    Description = x.y.part_description
                }).ToList();
            }
            return DamagedGood;
        }

        public DamagedGoodsHeadModel DamagedGoodsPrint(string RMA)
        {
            var DamagedGood = _context.DAMAGEDS_GOODS.Where(x => x.damaged_goods_doc == RMA).Select(x => new DamagedGoodsHeadModel
            {
                Comment = x.comment,
                DamagedGoodsDoc = x.damaged_goods_doc,
                ProcessStatus = x.process_status,
                ProcessType = x.process_type,
                Site = x.site_code,
                SupplierId = x.supplier_id.Value,
                cuser = x.cuser,
                document_no = x.gi_document,
                Udate = x.udate ?? DateTime.Now,
                Print_Document = x.print_status ?? 0,
                Weight = x.weight_of_goods
            }).SingleOrDefault();
            if (DamagedGood != null)
            {
                DamagedGood.DamageGoodsItem = _context.DAMAGEDS_GOODS_ITEM.Where(x => x.damaged_goods_doc == RMA).Join(_context.ITEM, x => x.part_number, y => y.part_number, (x, y) => new { x, y }).Select(x => new DamagedsGoodsModel
                {
                    PartNumber = x.x.part_number,
                    Quantity = x.x.quantity,
                    Price = x.x.price != null ? x.x.price.Value : 0,
                    Amount = x.x.price != null ? (x.x.price.Value * x.x.quantity) : 0,
                    Description = x.y.part_description
                }).ToList();
            }
            return DamagedGood;
        }

        public bool UpdateRmaAprovalCancel(string Folio, bool Flag, string Comment, string User, string type, string program)
        {
            var RmaAprovalCancel = _context.DAMAGEDS_GOODS.Where(x => x.damaged_goods_doc == Folio && x.process_status == 1).SingleOrDefault();
            if (RmaAprovalCancel != null)
            {
                RmaAprovalCancel.uuser = User;
                RmaAprovalCancel.udate = DateTime.Now;
                RmaAprovalCancel.comment = Comment;
                RmaAprovalCancel.process_type = type;//"RMA";
                RmaAprovalCancel.program_id = program;//"RMA002.cshtml";                

                if (Flag)
                {
                    RmaAprovalCancel.process_status = 2;
                }
                else
                {
                    RmaAprovalCancel.process_status = 3;
                }
                _context.SaveChanges();
                return true;
            }
            return false;

        }

        public bool UpdateRmaAprovalCancel(List<DamagedsGoodsModel> Model, string Folio, bool Flag, string Comment, string User, string type, string program, Decimal? weight)
        {
            var RmaAprovalCancel = _context.DAMAGEDS_GOODS.Where(x => x.damaged_goods_doc == Folio && x.process_status == 1).SingleOrDefault();
            if (RmaAprovalCancel != null)
            {
                RmaAprovalCancel.weight_of_goods = weight;
                RmaAprovalCancel.uuser = User;
                RmaAprovalCancel.udate = DateTime.Now;
                RmaAprovalCancel.comment = Comment;
                RmaAprovalCancel.process_type = type;//"RMA";  
                RmaAprovalCancel.program_id = program;//"RMA002.cshtml";              

                if (Flag)
                    RmaAprovalCancel.process_status = 2;
                else
                    RmaAprovalCancel.process_status = 3;

                _context.SaveChanges();

                //Regresar a Punto de Venta --RMA005BtoU
                string siteCode = _SiteConfigRepo.GetSiteCode();
                string merma = _storageLocationRepository.GetLocationaMain("MERMA", siteCode).storage_location;
                string venta = _storageLocationRepository.GetLocationaMain("VENTA", siteCode).storage_location;

                if (Flag)
                {
                    foreach (var item in Model)
                    {
                        var DamagedsGoodsItem = _context.DAMAGEDS_GOODS_ITEM.Where(x => x.damaged_goods_doc == Folio & x.part_number == item.PartNumber).FirstOrDefault();

                        if (DamagedsGoodsItem != null)
                        {
                            DamagedsGoodsItem.process_status = 2;
                            DamagedsGoodsItem.program_id = program;
                            _context.SaveChanges();
                        }
                    }
                    //Rechazados
                    var items = _context.DAMAGEDS_GOODS_ITEM.Where(x => x.damaged_goods_doc == Folio & x.process_status == 1).ToList();

                    foreach (var item in items)
                    {
                        var result = MethodReverseBlockToUnrestricted(siteCode, item.part_number, Decimal.ToInt32(item.quantity), merma, venta, "RMA005BtoU", User);
                    }

                    foreach (var item in items)
                    {
                        item.process_status = 3;
                        item.program_id = program;
                        _context.SaveChanges();
                    }
                }
                else
                {
                    //Rechazados
                    var items = _context.DAMAGEDS_GOODS_ITEM.Where(x => x.damaged_goods_doc == Folio).ToList();
                    foreach (var item in items)
                    {
                        var result = MethodReverseBlockToUnrestricted(siteCode, item.part_number, Decimal.ToInt32(item.quantity), merma, venta, "RMA005BtoU", User);
                    }
                    foreach (var item in items)
                    {
                        item.process_status = 3;
                        item.program_id = program;
                        _context.SaveChanges();
                    }
                }

                return true;
            }

            return false;
        }

        public StoreProcedureResult StoreProcedureDamagedGoods(string Document, string User, string ProgramId)
        {
            var Store = _context.Database.SqlQuery<StoreProcedureResult>("DECLARE	@return_value int, @Document varchar(10) EXEC    @return_value = [dbo].[sporc_GI_Damageds_goods] @damaged_goods_doc = @DocumentV,	@cancel_flag = 0 , @user = @User,  @program_id = @ProgramId, @Document = @Document OUTPUT SELECT 'Document' = @Document, 'ReturnValue' = @return_value"
                , new SqlParameter("@DocumentV", Document), new SqlParameter("@User", User), new SqlParameter("@ProgramId", ProgramId)).SingleOrDefault();
            return Store;
        }

        public int AddDamageGoodsAndroid(DAMAGEDS_GOODS header)
        {
            try
            {
                _context.DAMAGEDS_GOODS.Add(header);
                var save = _context.SaveChanges();
                return save;
            }
            catch (Exception ex)
            {
                var msg = ex.Message;
                return 0;
            }
        }

        public List<DAMAGEDS_GOODS> GetFoliosByRemisionAprovalByDates(int Supplier)
        {
            return _context.DAMAGEDS_GOODS.ToList().Where(x => x.supplier_id == Supplier && x.process_status == 1 && x.process_type == "REMISION")
                .Select(x => new DAMAGEDS_GOODS
                {
                    damaged_goods_doc = x.damaged_goods_doc,
                    process_type = x.process_type

                }).ToList();
        }

        public List<DAMAGEDS_GOODS> GetFoliosByRemisionAprovalByDates()
        {
            return _context.DAMAGEDS_GOODS.ToList().Join(_context.MA_SUPPLIER, x => x.supplier_id, y => y.supplier_id, (x, y) => new { x, y })
                .Where(x => x.x.process_status == 1 && x.x.process_type == "REMISION")
            .Select(x => new DAMAGEDS_GOODS
            {
                damaged_goods_doc = x.x.damaged_goods_doc,
                process_type = x.y.commercial_name
            }).ToList();
        }

        public List<DamagedsGoodsModel> GetDocumentsByDates(DateTime fechaIni, DateTime fechaFin)
        {
            var items = _context.DAMAGEDS_GOODS.ToList()
                .Join(_context.SITE_CONFIG, s => s.site_code, x => x.site_code, (s, x) => new { s, x })
                .Where(x => x.s.cdate.Date >= fechaIni.Date && x.s.cdate.Date <= fechaFin.Date && x.s.process_status == 1 && x.s.process_type == "SCRAP")
                .Select(x => new DamagedsGoodsModel
                {
                    Document = x.s.damaged_goods_doc,
                    User = x.s.cuser,
                    SiteName = x.x.site_name,
                    Fecha = x.s.cdate.ToString()
                }).ToList();
            for (int o = 0; o < items.Count; o++)
            {
                var user_name = items[o].User;
                var name = _context.USER_MASTER.Where(x => x.user_name == user_name).SingleOrDefault();
                items[o].User = name.first_name + " " + name.last_name;
            }
            return items;
        }

        /*reporte*/
        public List<DamagedsGoodsModel> GetAllDocumentsScraptReport(DateTime? fechaIni, DateTime? fechaFin, int status)
        {
            var query = @"select 
                        dg.damaged_goods_doc as Document, 
                        sc.site_name as SiteName, 
                        um.user_name as 'User', 
                        dg.process_status as Status, 
                        CONVERT (nvarchar(50), dg.cdate,101)  as Fecha, 
                        sum(dgi.price* dgi.quantity) as Amount, 
                        sum((dgi.price * dgi.quantity )* (ieps.tax_value/100))as IEPS, 
                        sum(((dgi.price* dgi.quantity )+((dgi.price * dgi.quantity )* (ieps.tax_value/100)))*(iva.tax_value/100)) as IVA, 
                        sum((((dgi.price * dgi.quantity )+((dgi.price * dgi.quantity )* (ieps.tax_value/100)))*(iva.tax_value/100))+((dgi.price * dgi.quantity )* (ieps.tax_value/100))+(dgi.price * dgi.quantity)) as TotalAmount 
                        from DAMAGEDS_GOODS dg 
                        inner join  DAMAGEDS_GOODS_ITEM dgi on dgi.damaged_goods_doc = dg.damaged_goods_doc and dg.process_status  = dgi.process_status 
                        inner join SITE_CONFIG sc on sc.site_code = dg.site_code 
                        inner join USER_MASTER um on um.user_name = dg.cuser 
                        inner join item  it on it.part_number = dgi.part_number 
                        inner join MA_TAX iva on iva.tax_code = it.part_iva_purchase 
                        inner join MA_TAX ieps on ieps.tax_code = it.part_ieps 
                        where dg.process_type = 'SCRAP' ";

            var option = 0;
            if (fechaFin.HasValue && fechaIni.HasValue)
            {
                option += 1;
                query += @" and CONVERT(date, dg.cdate, 101) between CONVERT(date, @date1, 101) and CONVERT(date, @date2, 101) ";
            }

            if (status > 0)
            {
                if(status == 9)
                {
                    query += @" and dg.process_status in (6,9) ";
                }else
                {
                    query += @" and dg.process_status = @status ";
                }
                option += 2;
            }

            if (option == 0)
            {
                return _context.Database.SqlQuery<DamagedsGoodsModel>($@"{query} group by 
                        dg.damaged_goods_doc ,sc.site_name, 
                        dg.cdate, 
                        dg.process_status, um.user_name ").ToList();
            }

            if (option == 1)
            {
                return _context.Database.SqlQuery<DamagedsGoodsModel>($@"{query} group by 
                        dg.damaged_goods_doc ,sc.site_name, 
                        dg.cdate, 
                        dg.process_status, um.user_name ", new SqlParameter("@date1", fechaIni.Value.Date), new SqlParameter("@date2", fechaFin.Value.Date)).ToList();
            }

            if (option == 2)
            {
                return _context.Database.SqlQuery<DamagedsGoodsModel>($@"{query} group by 
                        dg.damaged_goods_doc ,sc.site_name, 
                        dg.cdate, 
                        dg.process_status, um.user_name ", new SqlParameter("@status", status)).ToList();
            }

            return _context.Database.SqlQuery<DamagedsGoodsModel>($@"{query} group by 
                        dg.damaged_goods_doc ,sc.site_name, 
                        dg.cdate, 
                        dg.process_status, um.user_name ",
                        new SqlParameter("@status", status),
                        new SqlParameter("@date1", fechaIni.Value.Date),
                        new SqlParameter("@date2", fechaFin.Value.Date)).ToList();
        }
        public List<DamagedsGoodsModel> GetAllDocumentsScrapApproval(DateTime? fechaIni, DateTime? fechaFin, int status)
        {
            var query = @"select 
                        dg.damaged_goods_doc as Document, 
                        sc.site_name as SiteName, 
                        um.user_name as 'User', 
                        dg.process_status as Status, 
                        CONVERT (nvarchar(50), dg.cdate,101)  as Fecha, 
                        sum(dgi.price* dgi.quantity) as Amount, 
                        sum((dgi.price * dgi.quantity )* (ieps.tax_value/100))as IEPS, 
                        sum(((dgi.price* dgi.quantity )+((dgi.price * dgi.quantity )* (ieps.tax_value/100)))*(iva.tax_value/100)) as IVA, 
                        sum((((dgi.price * dgi.quantity )+((dgi.price * dgi.quantity )* (ieps.tax_value/100)))*(iva.tax_value/100))+((dgi.price * dgi.quantity )* (ieps.tax_value/100))+(dgi.price * dgi.quantity)) as TotalAmount 
                        from DAMAGEDS_GOODS dg 
                        inner join  DAMAGEDS_GOODS_ITEM dgi on dgi.damaged_goods_doc = dg.damaged_goods_doc and dg.process_status  = dgi.process_status 
                        inner join SITE_CONFIG sc on sc.site_code = dg.site_code 
                        inner join USER_MASTER um on um.user_name = dg.cuser 
                        inner join item  it on it.part_number = dgi.part_number 
                        inner join MA_TAX iva on iva.tax_code = it.part_iva_purchase 
                        inner join MA_TAX ieps on ieps.tax_code = it.part_ieps 
                        where dg.process_type = 'SCRAP' ";

            var option = 0;
            if (fechaFin.HasValue && fechaIni.HasValue)
            {
                option += 1;
                query += @" and CONVERT(date, dg.cdate, 101) between CONVERT(date, @date1, 101) and CONVERT(date, @date2, 101) ";
            }

            if (status > 0)
            {
                query += @" and dg.process_status = @status ";
                
                option += 2;
            }

            if (option == 0)
            {
                return _context.Database.SqlQuery<DamagedsGoodsModel>($@"{query} group by 
                        dg.damaged_goods_doc ,sc.site_name, 
                        dg.cdate, 
                        dg.process_status, um.user_name ").ToList();
            }

            if (option == 1)
            {
                return _context.Database.SqlQuery<DamagedsGoodsModel>($@"{query} group by 
                        dg.damaged_goods_doc ,sc.site_name, 
                        dg.cdate, 
                        dg.process_status, um.user_name ", new SqlParameter("@date1", fechaIni.Value.Date), new SqlParameter("@date2", fechaFin.Value.Date)).ToList();
            }

            if (option == 2)
            {
                return _context.Database.SqlQuery<DamagedsGoodsModel>($@"{query} group by 
                        dg.damaged_goods_doc ,sc.site_name, 
                        dg.cdate, 
                        dg.process_status, um.user_name ", new SqlParameter("@status", status)).ToList();
            }

            return _context.Database.SqlQuery<DamagedsGoodsModel>($@"{query} group by 
                        dg.damaged_goods_doc ,sc.site_name, 
                        dg.cdate, 
                        dg.process_status, um.user_name ",
                        new SqlParameter("@status", status),
                        new SqlParameter("@date1", fechaIni.Value.Date),
                        new SqlParameter("@date2", fechaFin.Value.Date)).ToList();
        }

        public List<DamagedsGoodsModel> GetDocumentsByDatesInStatus2(DateTime fechaIni, DateTime fechaFin)
        {
            var items = _context.DAMAGEDS_GOODS.ToList()
            .Join(_context.SITE_CONFIG, s => s.site_code, x => x.site_code, (s, x) => new { s, x })
            .Where(x => x.s.cdate.Date >= fechaIni.Date && x.s.cdate.Date <= fechaFin.Date && x.s.process_status == 2 && x.s.process_type == "SCRAP")
            .Select(x => new DamagedsGoodsModel
            {
                Document = x.s.damaged_goods_doc,
                SiteName = x.x.site_name,
                User = x.s.cuser,
                Fecha = x.s.cdate.ToString()
            }).ToList();
            for (int o = 0; o < items.Count; o++)
            {
                var user_name = items[o].User;
                var name = _context.USER_MASTER.Where(x => x.user_name == user_name).SingleOrDefault();
                items[o].User = name.first_name + " " + name.last_name;
            }
            return items;
        }

        public int UpdateDamageGoodsStatus(DAMAGEDS_GOODS damage)
        {
            _context.Entry(damage).State = System.Data.Entity.EntityState.Modified;
            return _context.SaveChanges();
        }

        public DAMAGEDS_GOODS GetDamageGoodsHeader(string Document) => _context.DAMAGEDS_GOODS.Where(o => o.damaged_goods_doc == Document).SingleOrDefault();

        public bool UpdateDamegeGoodsConfirmacionRemision(string Folio, Decimal? Weight, string User, string program_id)
        {
            var RemisionAproval = _context.DAMAGEDS_GOODS.Where(x => x.damaged_goods_doc == Folio).SingleOrDefault();
            if (RemisionAproval != null)
            {
                RemisionAproval.uuser = User;
                RemisionAproval.udate = DateTime.Now;
                RemisionAproval.weight_of_goods = Weight;
                RemisionAproval.program_id = program_id;
                RemisionAproval.process_status = 9;
                _context.SaveChanges();
                return true;
            }
            return false;
        }

        public bool UpdateDamegeGoodsConfirmacionRemision(List<DamagedsGoodsModel> Model, string Folio, decimal? Weight, string User, string program_id)
        {
            var RemisionAproval = _context.DAMAGEDS_GOODS.Where(x => x.damaged_goods_doc == Folio).SingleOrDefault();
            if (RemisionAproval != null)
            {
                RemisionAproval.uuser = User;
                RemisionAproval.udate = DateTime.Now;
                RemisionAproval.weight_of_goods = Weight;
                RemisionAproval.program_id = program_id;
                RemisionAproval.process_status = 9;
                _context.SaveChanges();

                foreach (var item in Model)
                {
                    var DamagedsGoodsItem = _context.DAMAGEDS_GOODS_ITEM.Where(x => x.damaged_goods_doc == Folio & x.part_number == item.PartNumber).FirstOrDefault();

                    if (DamagedsGoodsItem != null)
                        DamagedsGoodsItem.process_status = 9;

                    _context.SaveChanges();
                }

                return true;
            }
            return false;
        }

        public bool UpdateRmaRemisionAprovalReject(string Folio, int Status, string Comment, string User, string type, string program)
        {
            var RmaAprovalCancel = _context.DAMAGEDS_GOODS.Where(x => x.damaged_goods_doc == Folio && x.process_status == 1).SingleOrDefault();
            if (RmaAprovalCancel != null)
            {
                RmaAprovalCancel.uuser = User;
                RmaAprovalCancel.udate = DateTime.Now;
                RmaAprovalCancel.comment = Comment;
                RmaAprovalCancel.process_type = type;
                RmaAprovalCancel.program_id = program;
                RmaAprovalCancel.process_status = Status;
                _context.SaveChanges();
                return true;
            }
            return false;
        }

        public bool UpdateRmaRemisionAprovalReject(List<DamagedsGoodsModel> Model, string Folio, int Status, string Comment, string User, string type, string program)
        {
            var RmaAprovalCancel = _context.DAMAGEDS_GOODS.Where(x => x.damaged_goods_doc == Folio && x.process_status == 1).SingleOrDefault();
            if (RmaAprovalCancel != null)
            {
                RmaAprovalCancel.uuser = User;
                RmaAprovalCancel.udate = DateTime.Now;
                RmaAprovalCancel.comment = Comment;
                RmaAprovalCancel.process_type = type;
                RmaAprovalCancel.program_id = program;
                RmaAprovalCancel.process_status = Status;
                _context.SaveChanges();

                //Regresar a Punto de Venta --RMA002BtoU
                string siteCode = _SiteConfigRepo.GetSiteCode();
                string merma = _storageLocationRepository.GetLocationaMain("MERMA", siteCode).storage_location;
                string venta = _storageLocationRepository.GetLocationaMain("VENTA", siteCode).storage_location;
                if (Status == 2)
                {
                    foreach (var item in Model)
                    {
                        var DamagedsGoodsItem = _context.DAMAGEDS_GOODS_ITEM.Where(x => x.damaged_goods_doc == Folio & x.part_number == item.PartNumber).FirstOrDefault();

                        if (DamagedsGoodsItem != null)
                        {
                            if (DamagedsGoodsItem.quantity != item.RealQuantity)
                            {
                                //Regresar a Punto de Venta --RMA002BtoU
                                var quantityRemove = DamagedsGoodsItem.quantity - item.RealQuantity;
                                var result = MethodReverseBlockToUnrestricted(siteCode, item.PartNumber, Decimal.ToInt32(quantityRemove), merma, venta, "RMA002BtoU", User);
                            }
                        }
                    }
                    foreach (var item in Model)
                    {
                        var DamagedsGoodsItem = _context.DAMAGEDS_GOODS_ITEM.Where(x => x.damaged_goods_doc == Folio & x.part_number == item.PartNumber).FirstOrDefault();

                        if (DamagedsGoodsItem != null)
                        {
                            if (item.RealQuantity != 0)
                            {

                                DamagedsGoodsItem.quantity = item.RealQuantity;
                                DamagedsGoodsItem.price = item.Price;
                                DamagedsGoodsItem.amount_doc = item.RealQuantity * item.Price;
                            }
                            DamagedsGoodsItem.process_status = Status;
                            _context.SaveChanges();
                        }
                    }
                }

                var items = _context.DAMAGEDS_GOODS_ITEM.Where(x => x.damaged_goods_doc == Folio & x.process_status == 1).ToList();
                foreach (var item in items)
                {
                    //Regresar a Punto de Venta --RMA002BtoU
                    var result = MethodReverseBlockToUnrestricted(siteCode, item.part_number, Decimal.ToInt32(item.quantity), merma, venta, "RMA002BtoU", User);
                }
                foreach (var item in items)
                {
                    item.process_status = 3;
                    _context.SaveChanges();
                }

                return true;
            }
            return false;
        }

        public bool RmaCancelRequest(string Folio, string Comment, string User)
        {
            try
            {
                var RmaAprovalCancel = _context.DAMAGEDS_GOODS.Where(x => x.damaged_goods_doc == Folio).FirstOrDefault();
                if (RmaAprovalCancel != null)
                {
                    RmaAprovalCancel.uuser = User;
                    RmaAprovalCancel.udate = DateTime.Now;
                    RmaAprovalCancel.comment = Comment;
                    RmaAprovalCancel.process_type = "RMA";
                    RmaAprovalCancel.program_id = "RMA007.cshtml";
                    RmaAprovalCancel.process_status = 4;
                    _context.SaveChanges();

                    var Items = _context.DAMAGEDS_GOODS_ITEM.Where(x => x.damaged_goods_doc == Folio).ToList();
                    foreach (var item in Items)
                    {
                        // Verificar que tnega el estatus anterior (9 terminado)
                        if (item.process_status == 9)
                        {
                            item.process_status = 4;
                            _context.SaveChanges();
                        }
                    }

                    return true;
                }
                else
                    return false;
            }
            catch (Exception ex)
            {
                string msg = ex.Message;
                return false;
            }
        }

        public bool UpdateRmaRemisionInitial(List<DamagedsGoodsModel> Model, string Folio, int Status, string Comment, string User, string program)
        {
            var RmaAprovalCancel = _context.DAMAGEDS_GOODS.Where(x => x.damaged_goods_doc == Folio && x.process_status == 0).SingleOrDefault();
            if (RmaAprovalCancel != null)
            {
                RmaAprovalCancel.uuser = User;
                RmaAprovalCancel.udate = DateTime.Now;
                RmaAprovalCancel.comment = Comment;
                RmaAprovalCancel.program_id = program;
                RmaAprovalCancel.process_status = Status;
                _context.SaveChanges();

                //Regresar a Punto de Venta --RMA002BtoU
                string siteCode = _SiteConfigRepo.GetSiteCode();
                string merma = _storageLocationRepository.GetLocationaMain("MERMA", siteCode).storage_location;
                string venta = _storageLocationRepository.GetLocationaMain("VENTA", siteCode).storage_location;
                //Aprobado
                if (Status == 2)
                {
                    foreach (var item in Model)
                    {
                        var DamagedsGoodsItem = _context.DAMAGEDS_GOODS_ITEM.Where(x => x.damaged_goods_doc == Folio & x.part_number == item.PartNumber).FirstOrDefault();

                        if (DamagedsGoodsItem != null)
                        {
                            if (DamagedsGoodsItem.quantity != item.RealQuantity)
                            {
                                //Regresar a Punto de Venta --RMA002BtoU
                                var quantityRemove = DamagedsGoodsItem.quantity - item.RealQuantity;
                                var result = MethodReverseBlockToUnrestricted(siteCode, item.PartNumber, Decimal.ToInt32(quantityRemove), merma, venta, "RMA002BtoU", User);
                            }
                        }
                    }
                    foreach (var item in Model)
                    {
                        var DamagedsGoodsItem = _context.DAMAGEDS_GOODS_ITEM.Where(x => x.damaged_goods_doc == Folio & x.part_number == item.PartNumber).FirstOrDefault();

                        if (DamagedsGoodsItem != null)
                        {
                            if (item.RealQuantity != 0)
                            {

                                DamagedsGoodsItem.quantity = item.RealQuantity;
                                DamagedsGoodsItem.price = item.Price;
                                DamagedsGoodsItem.amount_doc = item.RealQuantity * item.Price;
                            }
                            DamagedsGoodsItem.process_status = Status;
                            _context.SaveChanges();
                        }
                    }
                }
                //Cancelados
                var items = _context.DAMAGEDS_GOODS_ITEM.Where(x => x.damaged_goods_doc == Folio & x.process_status == 0).ToList();
                foreach (var item in items)
                {
                    //Regresar a Punto de Venta --RMA002BtoU
                    var result = MethodReverseBlockToUnrestricted(siteCode, item.part_number, Decimal.ToInt32(item.quantity), merma, venta, "RMA002BtoU", User);
                }
                foreach (var item in items)
                {
                    item.process_status = 3;
                    _context.SaveChanges();
                }

                return true;
            }
            return false;
        }

        public bool UpdateRmaRemisionCancel(string Folio, string Comment, string User, string program)
        {
            var RmaAprovalCancel = _context.DAMAGEDS_GOODS.Where(x => x.damaged_goods_doc == Folio).SingleOrDefault();
            if (RmaAprovalCancel != null)
            {
                RmaAprovalCancel.uuser = User;
                RmaAprovalCancel.udate = DateTime.Now;
                RmaAprovalCancel.comment = RmaAprovalCancel.comment+", " +Comment;
                RmaAprovalCancel.program_id = program;
                RmaAprovalCancel.process_status = 8;
                _context.SaveChanges();

                //Regresar a Punto de Venta --RMA002BtoU
                string siteCode = _SiteConfigRepo.GetSiteCode();
                string merma = _storageLocationRepository.GetLocationaMain("MERMA", siteCode).storage_location;
                string venta = _storageLocationRepository.GetLocationaMain("VENTA", siteCode).storage_location;
               
                //Cancelados
                var items = _context.DAMAGEDS_GOODS_ITEM.Where(x => x.damaged_goods_doc == Folio).ToList();
                foreach (var item in items)
                {
                    //Regresar a Punto de Venta --RMA002BtoU
                    var result = MethodReverseBlockToUnrestricted(siteCode, item.part_number, Decimal.ToInt32(item.quantity), merma, venta, "RMA002BtoU", User);
                }
                foreach (var item in items)
                {
                    item.process_status = 8;
                    _context.SaveChanges();
                }

                return true;
            }
            return false;
        }

        public int RmaRejectCancelRequest(string Folio, string Comment, string User)
        {
            try
            {
                var RmaAprovalCancel = _context.DAMAGEDS_GOODS.Where(x => x.damaged_goods_doc == Folio).FirstOrDefault();
                if (RmaAprovalCancel != null)
                {
                    RmaAprovalCancel.uuser = User;
                    RmaAprovalCancel.udate = DateTime.Now;
                    RmaAprovalCancel.comment = Comment;
                    RmaAprovalCancel.process_type = "RMA";
                    RmaAprovalCancel.program_id = "RMA007.cshtml";
                    RmaAprovalCancel.process_status = 5;
                    _context.SaveChanges();

                    var Items = _context.DAMAGEDS_GOODS_ITEM.Where(x => x.damaged_goods_doc == Folio).ToList();
                    foreach (var item in Items)
                    {
                        // Verificar que este en el estatus anterior
                        if (item.process_status == 4)
                        {
                            item.process_status = 5;
                            _context.SaveChanges();
                        }
                    }

                    return RmaAprovalCancel.supplier_id ?? 0;
                }
                return 0;
            }
            catch (Exception ex)
            {
                string msg = ex.Message;
                return 0;
            }
        }

        public bool RmaApproveCancelRequest(string Folio, string Comment, string User)
        {
            try
            {
                var damaged_goods_doc = new SqlParameter { ParameterName = "damaged_goods_doc", SqlDbType = SqlDbType.VarChar, Size = 10, Value = Folio };
                var cancel_flag = new SqlParameter { ParameterName = "cancel_flag", SqlDbType = SqlDbType.Bit, Value = true };
                var user = new SqlParameter { ParameterName = "user", SqlDbType = SqlDbType.VarChar, Size = 50, Value = User };
                var program_id = new SqlParameter { ParameterName = "program_id", SqlDbType = SqlDbType.VarChar, Size = 20, Value = "RMA008.cshtml" };
                var document = new SqlParameter { ParameterName = "@Document", SqlDbType = SqlDbType.VarChar, Size = 10, Direction = ParameterDirection.Output };
                var data = _context.Database.ExecuteSqlCommand("sporc_GI_Damageds_goods @damaged_goods_doc, @cancel_flag, @user, @program_id, @Document OUTPUT", damaged_goods_doc, cancel_flag, user, program_id, document);

                if (document.Value.ToString() != "")
                {
                    var RmaAprovalCancel = _context.DAMAGEDS_GOODS.Where(x => x.damaged_goods_doc == Folio).FirstOrDefault();
                    if (RmaAprovalCancel != null)
                    {
                        RmaAprovalCancel.uuser = User;
                        RmaAprovalCancel.udate = DateTime.Now;
                        RmaAprovalCancel.comment = Comment;
                        RmaAprovalCancel.process_type = "RMA";
                        RmaAprovalCancel.program_id = "RMA007.cshtml";
                        RmaAprovalCancel.process_status = 8;
                        _context.SaveChanges();

                        var Items = _context.DAMAGEDS_GOODS_ITEM.Where(x => x.damaged_goods_doc == Folio).ToList();
                        foreach (var item in Items)
                        {
                            //Verificar el estatus anterior
                            if (item.process_status == 4)
                            {
                                item.process_status = 8;
                                _context.SaveChanges();
                            }
                        }

                        return true;
                    }
                    return false;
                }
                else
                    return false;
            }
            catch (Exception ex)
            {
                string msg = ex.Message;
                return false;
            }
        }

        public bool UpdateWeight(string folio, decimal? weight)
        {
            var RmaAprovalCancel = _context.DAMAGEDS_GOODS.Where(x => x.damaged_goods_doc == folio && x.process_status == 1).SingleOrDefault();
            if (RmaAprovalCancel != null)
            {
                RmaAprovalCancel.weight_of_goods = weight;
                _context.SaveChanges();
                return true;
            }
            return false;
        }

        public List<DamagedsGoodsModel> GetScrapItemsDepartment(DateTime dateInitial, DateTime dateFinish, string department, string status)
        {
            string whereDepartment = "";
            if (department != "0")
                whereDepartment = " AND I.level_header = " + department;

            string whereStatus = "";
            if (status == "8")
                whereStatus = " AND DGI.process_status in (3,8) AND DG.process_status in (3,8) ";
            else if (status == "9")
                whereStatus = " AND DGI.process_status in (6,9) AND DG.process_status in (6,9) ";
            else if (status != "0")
                whereStatus = " AND DGI.process_status = " + status + "AND DG.process_status = " + status;

            var result = _context.Database.SqlQuery<DamagedsGoodsModel>(@"SELECT DGI.damaged_goods_doc as 'Document', DG.process_status 'Status', 
                ISNULL(I.unit_size,0) as 'UnitSize' ,DGI.part_number AS 'PartNumber', 
                I.part_description as 'Description',DGI.quantity as 'Quantity',DGI.price as 'Price', 
                DGI.amount_doc as 'Amount',MC.class_name as 'Department', MC2.class_name as 'Family', DGI.scrap_reason as 'ScrapReason', 
                CONCAT(UM.first_name,' ',UM.last_name) AS 'User' , convert(varchar,DGI.udate,100) as 'Date' 
                FROM DAMAGEDS_GOODS DG 
                INNER JOIN DAMAGEDS_GOODS_ITEM DGI ON DG.damaged_goods_doc = DGI.damaged_goods_doc and DG.process_status = DGI.process_status                 
                INNER JOIN ITEM I ON I.part_number = DGI.part_number 
                INNER JOIN MA_CLASS MC ON MC.class_id = I.level_header 
                INNER JOIN MA_CLASS MC2 ON MC2.class_id = I.level1_code 
                LEFT JOIN USER_MASTER UM ON UM.user_name = DGI.uuser 
                WHERE DG.process_type = 'SCRAP' 
                AND CONVERT(date, DG.cdate,101) BETWEEN CONVERT(date,@dateInitial,101) AND CONVERT(date,@dateFinish,101) " + whereDepartment + whereStatus, new SqlParameter("dateInitial", dateInitial.Date), new SqlParameter("dateFinish", dateFinish.Date)).ToList();

            return result;
        }

        public int DamagedsGoodsStatusPrintDocument(string Document) => _context.DAMAGEDS_GOODS.Where(w => w.damaged_goods_doc == Document).Select(x => x.print_status ?? 0).FirstOrDefault();

        public int DamagedsGoodsStatusPrintReference(string Reference) => _context.DAMAGEDS_GOODS.Where(w => w.gi_document == Reference).Select(x => x.print_status ?? 0).FirstOrDefault();

        public bool DamagedsGoodsEditStatusPrint(string Document, int Status, string program_id)
        {
            try
            {
                var damageds = _context.DAMAGEDS_GOODS.Where(elements => elements.damaged_goods_doc == Document).SingleOrDefault();
                damageds.print_status = Status + 1;
                _context.SaveChanges();
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public List<DamagedsGoodsModel> GetScrapDepartmentTotal(DateTime dateInitial, DateTime dateFinish, int Status, int Department, bool Detail)
        {
            if (Detail)
            {
                string whereDepartment = "";
                if (Department != 0)
                    whereDepartment = " AND I.level_header = " + Department;

                string whereStatus = "";
                if (Status == 8)
                    whereStatus = " AND DGI.process_status in (3,8) AND DG.process_status in (3,8) ";
                else if (Status == 9)
                    whereStatus = " AND DGI.process_status in (6,9) AND DG.process_status in (6,9) ";
                else if (Status != 0)
                    whereStatus = " AND DGI.process_status = " + Status + "AND DG.process_status = " + Status;

                var result = _context.Database.SqlQuery<DamagedsGoodsModel>(@"SELECT
                SUM(DGI.amount_doc) AS 'TotalAmount', MC.class_name AS 'Department' 
                FROM DAMAGEDS_GOODS DG
                INNER JOIN DAMAGEDS_GOODS_ITEM DGI ON DG.damaged_goods_doc = DGI.damaged_goods_doc and DG.process_status = DGI.process_status 
                INNER JOIN ITEM I ON I.part_number = DGI.part_number
                INNER JOIN MA_CLASS MC ON MC.class_id = I.level_header
                INNER JOIN MA_TAX iva on iva.tax_code = I.part_iva_purchase 
                INNER JOIN MA_TAX ieps on ieps.tax_code = I.part_ieps                                 
                WHERE DG.process_type = 'SCRAP'         
                AND CONVERT(date, DG.cdate, 101) BETWEEN CONVERT(date, @dateInitial, 101) AND CONVERT(date, @dateFinish, 101) "
                + whereDepartment + whereStatus + " GROUP BY MC.class_name ", new SqlParameter("dateInitial", dateInitial), new SqlParameter("dateFinish", dateFinish)).ToList();

                return result;
            }
            else
            {
                string whereStatus = "";
                if (Status == 8)
                    whereStatus = " AND DGI.process_status in (3,8) AND DG.process_status in (3,8) ";
                else if (Status == 9)
                    whereStatus = " AND DGI.process_status in (6,9) AND DG.process_status in (6,9) ";
                else if (Status != 0)
                    whereStatus = " AND DGI.process_status = " + Status + "AND DG.process_status = " + Status;

                var result = _context.Database.SqlQuery<DamagedsGoodsModel>(@"SELECT
                /*SUM((((DGI.price * DGI.quantity )+((DGI.price * DGI.quantity )* (ieps.tax_value/100)))*(iva.tax_value/100))+((DGI.price * DGI.quantity )* (ieps.tax_value/100))+(DGI.price * DGI.quantity)) AS 'TotalAmount', MC.class_name AS 'Department'*/
                SUM((DGI.price * DGI.quantity)) AS 'TotalAmount', MC.class_name AS 'Department' 
                FROM DAMAGEDS_GOODS DG
                INNER JOIN DAMAGEDS_GOODS_ITEM DGI ON DG.damaged_goods_doc = DGI.damaged_goods_doc and DG.process_status = DGI.process_status 
                INNER JOIN ITEM I ON I.part_number = DGI.part_number
                INNER JOIN MA_CLASS MC ON MC.class_id = I.level_header
                INNER JOIN MA_TAX iva on iva.tax_code = I.part_iva_purchase 
                INNER JOIN MA_TAX ieps on ieps.tax_code = I.part_ieps                                 
                WHERE DG.process_type = 'SCRAP'                
                AND CONVERT(date, DG.cdate, 101) BETWEEN CONVERT(date, @dateInitial, 101) AND CONVERT(date, @dateFinish, 101) "
                + whereStatus + " GROUP BY MC.class_name ", new SqlParameter("dateInitial", dateInitial), new SqlParameter("dateFinish", dateFinish)).ToList();

                return result;
            }
        }

        public bool UpdateDamageGoodsItemStatusSingle(List<DamagedsGoodsModel> Model, string document, string msj, string user)
        {
            if (msj == "APROBADO")
            {
                //Sumar cantidades
                List<DamagedsGoodsModel> newModelSum = new List<DamagedsGoodsModel>();
                foreach (var item in Model)
                {
                    var index = newModelSum.FindIndex(w => w.PartNumber == item.PartNumber);
                    if (index >= 0)
                    {
                        newModelSum[index].Quantity = newModelSum[index].Quantity + item.Quantity;
                    }
                    else
                    {
                        DamagedsGoodsModel itemSum = new DamagedsGoodsModel
                        {
                            PartNumber = item.PartNumber,
                            Quantity = item.Quantity,
                            Price = item.Price
                        };
                        newModelSum.Add(itemSum);
                    }
                }
                //Aprobar los unicos
                foreach (var item in Model)
                {
                    var DamagedsGoodsItemSingle = _context.DAMAGEDS_GOODS_ITEM_SINGLE.Where(w => w.id_damageds_goods_single == item.id_single).SingleOrDefault();
                    if (DamagedsGoodsItemSingle != null)
                    {
                        DamagedsGoodsItemSingle.process_status = 2;
                        DamagedsGoodsItemSingle.uuser = user;
                        DamagedsGoodsItemSingle.udate = DateTime.Now;
                        _context.SaveChanges();
                    }
                }

                //Rechazar los unicos
                var ItemsSingle = _context.DAMAGEDS_GOODS_ITEM_SINGLE.Where(x => x.damaged_goods_doc_unique == document & x.process_status == 1).ToList();
                foreach (var item in ItemsSingle)
                {
                    item.process_status = 3;
                    item.uuser = user;
                    item.udate = DateTime.Now;
                    _context.SaveChanges();
                }

                foreach (var item in newModelSum)
                {
                    var DamagedGoodsItem = _context.DAMAGEDS_GOODS_ITEM.Where(x => x.damaged_goods_doc == document & x.part_number == item.PartNumber).FirstOrDefault();
                    if (DamagedGoodsItem != null)
                    {
                        DamagedGoodsItem.process_status = 2;
                        DamagedGoodsItem.quantity = item.Quantity;
                        DamagedGoodsItem.amount_doc = item.Quantity * DamagedGoodsItem.price;
                        DamagedGoodsItem.uuser = user;
                        DamagedGoodsItem.udate = DateTime.Now;
                        _context.SaveChanges();
                    }
                }
                //Rechazar los códigos que no se elegieron
                var Items = _context.DAMAGEDS_GOODS_ITEM.Where(x => x.damaged_goods_doc == document & x.process_status == 1).ToList();

                //Regresar a Punto de Venta --SSM001BtoU
                string siteCode = _SiteConfigRepo.GetSiteCode();
                string merma = _storageLocationRepository.GetLocationaMain("MERMA", siteCode).storage_location;
                string venta = _storageLocationRepository.GetLocationaMain("VENTA", siteCode).storage_location;
                foreach (var item in Items)
                {
                    var result = MethodReverseBlockToUnrestricted(siteCode, item.part_number, Decimal.ToInt32(item.quantity), merma, venta, "SSM001BtoU", user);
                }

                foreach (var item in Items)
                {
                    item.process_status = 3;
                    item.uuser = user;
                    item.udate = DateTime.Now;
                    _context.SaveChanges();
                }
            }
            else
            {
                var Items = _context.DAMAGEDS_GOODS_ITEM.Where(x => x.damaged_goods_doc == document).ToList();
                foreach (var item in Items)
                {
                    item.process_status = 3;
                    item.uuser = user;
                    item.udate = DateTime.Now;
                    _context.SaveChanges();
                }

                var ItemsSingle = _context.DAMAGEDS_GOODS_ITEM_SINGLE.Where(x => x.damaged_goods_doc_unique == document).ToList();
                foreach (var item in ItemsSingle)
                {
                    item.process_status = 3;
                    item.uuser = user;
                    item.udate = DateTime.Now;
                    _context.SaveChanges();
                }
                //Regresar a Punto de Venta --SSM001BtoU
                //Los regresa despues de terminar este metodo
            }

            return true;
        }
        public bool UpdateScrapHeaderStatusCancelation(string doc, int status, string user)
        {
            try
            {
                var header = _context.DAMAGEDS_GOODS.Where(x => x.damaged_goods_doc == doc).FirstOrDefault();
                header.process_status = status;
                header.uuser = user;
                header.udate = DateTime.Now;
                _context.SaveChanges();
            }
            catch (Exception e)
            {
                return false;
            }
            return true;
        }
        public bool UpdateScrapDetailStatusCancelation(string doc, int new_status ,int old_status, string user)
        {
            try
            {
                var detail = _context.DAMAGEDS_GOODS_ITEM.Where(x => x.damaged_goods_doc == doc && x.process_status == old_status).ToList();

                foreach (var item in detail)
                {
                    item.process_status = new_status;
                    item.uuser = user;
                    item.udate = DateTime.Now;
                    _context.SaveChanges();
                }
            }
            catch (Exception e)
            {
                return false;
            }
            return true;
        }
         public Tuple<bool,string> CancelScrapDocumentSP(string damaged_goods_doc, string Site, string User, string ProgramId)
        {
            try
            {
                _context.Database.CommandTimeout = 1000;
                var DataReturn = _context.Database.SqlQuery<StoreProcedureResult>("DECLARE @return_value int, @Document nvarchar(10) EXEC @return_value = [dbo].[sproc_GR_Reverse_From_DG] @damaged_goods_doc = '" + damaged_goods_doc + "', @site_code = '" + Site + "', @cuser = '" + User + "', @program_id = '" + ProgramId + "', @Document = @Document OUTPUT SELECT 'Document'= @Document, 'ReturnValue' = @return_value").ToList();
                if (DataReturn[0].ReturnValue == 0 & DataReturn[0].Document != "")
                {
                    return new Tuple<bool, string>(true, DataReturn[0].Document);
                }
                else
                {

                    return new Tuple<bool, string>(false, DataReturn[0].ReturnValue.ToString());
                }
            }
            catch (Exception ex)
            {
                string msg = ex.Message;
                return new Tuple<bool, string>(false, "Ha ocurrido un error");
            }
        }
    }
}