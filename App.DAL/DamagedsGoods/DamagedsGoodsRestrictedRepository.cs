﻿using App.Entities;
using System.Linq;

namespace App.DAL.DamagedsGoods
{
    public class DamagedsGoodsRestrictedRepository
    {
        private readonly DFL_SAIEntities _context;

        public DamagedsGoodsRestrictedRepository()
        {
            _context = new DFL_SAIEntities();
        }

        public bool GetKnowDamagedsGoodsRestricted(string damageds_goods_type, string part_number)
        {
            var value = _context.DAMAGEDS_GOODS_RESTRICTED.Join(_context.ITEM, r => r.level_header, i => i.level_header, (r, i) => new { r, i })
                .Join(_context.ITEM, re => re.r.level1_code, it => it.level1_code, (re, it) => new { re, it })
                .Join(_context.ITEM, res => res.re.r.level2_code, ite => ite.level2_code, (res, ite) => new { res, ite })
                .Join(_context.ITEM, rest => rest.res.re.r.level3_code, item => item.level3_code, (rest, item) => new { rest, item })
                .Where(w => w.rest.res.re.i.part_number == part_number && w.rest.res.re.r.damaged_type == damageds_goods_type && w.rest.res.re.r.active_flag == true).FirstOrDefault();
            if (value != null)
                return false; // El producto esta restringido
            else
                return true; // El producto no tiene restriciones
        }
    }
}