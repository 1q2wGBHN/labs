﻿using App.Entities;
using App.Entities.ViewModels.DamagedsGoods;
using App.Entities.ViewModels.Item;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.SqlClient;
using System.Linq;

namespace App.DAL.Item
{
    public class DamagedsGoodsItemRepository
    {
        private readonly DFL_SAIEntities _context;

        public DamagedsGoodsItemRepository()
        {
            _context = new DFL_SAIEntities();
        }

        public List<DAMAGEDS_GOODS_ITEM> GetAll()
        {
            return _context.DAMAGEDS_GOODS_ITEM.ToList();
        }

        public List<DAMAGEDS_GOODS_ITEM> GetRmaListById(string damagedsgoodsitemId)
        {
            return _context.DAMAGEDS_GOODS_ITEM.Where(x => x.damaged_goods_doc == damagedsgoodsitemId).ToList();
        }

        public List<DAMAGEDS_GOODS_ITEM> GetRmaListByRmaHeaderId(string DamagedsgoodsId)
        {
            return _context.DAMAGEDS_GOODS_ITEM.Where(x => x.damaged_goods_doc == DamagedsgoodsId).ToList();
        }

        public bool AddDamagedGoodsItemBanati(DAMAGEDS_GOODS_ITEM model)
        {
            try
            {
                _context.DAMAGEDS_GOODS_ITEM.Add(model);
                _context.SaveChanges();
                return true;
            }
            catch (Exception ex)
            {
                var msg = ex.Message;
                return false;
            }
        }

        public bool UpdateDamagedGoodsItemBanati(List<DamagedGoodsBanatiModel> model, string User, string document, int Status, string ProgramId)
        {
            try
            {
                var items = _context.DAMAGEDS_GOODS_ITEM.Where(x => x.damaged_goods_doc == document).ToList();
                foreach (var item in items)
                {
                    _context.Entry(item).State = EntityState.Deleted;
                    _context.SaveChanges();
                }

                foreach (var item in model)
                {
                    DAMAGEDS_GOODS_ITEM DamagedGood = new DAMAGEDS_GOODS_ITEM { damaged_goods_doc = document, part_number = item.PartNumber, scrap_reason = item.Comment, quantity = item.Quantity, amount_doc = item.Amount_total, price = item.Price, cuser = User, cdate = DateTime.Now, program_id = ProgramId, process_status = Status };
                    _context.DAMAGEDS_GOODS_ITEM.Add(DamagedGood);
                    _context.SaveChanges();
                }

                return true;
            }
            catch (Exception ex)
            {
                var msg = ex.Message;
                return false;
            }
        }

        public void PostDamagesGoodsItem(DamagedsGoodsModel DamagedsGoods, string document, string User)
        {
            var DamagedGoodsItem = new DAMAGEDS_GOODS_ITEM()
            {
                amount_doc = DamagedsGoods.Price * DamagedsGoods.Quantity,
                damaged_goods_doc = document,
                part_number = DamagedsGoods.PartNumber,
                price = DamagedsGoods.Price,
                quantity = DamagedsGoods.Quantity,
                cdate = DateTime.Now,
                cuser = User,
                program_id = "RMA001.cshtml"
            };
            _context.DAMAGEDS_GOODS_ITEM.Add(DamagedGoodsItem);
            _context.SaveChanges();
        }

        public void PostDamagesGoodsItem(DamagedsGoodsModel DamagedsGoods, string document, string User, int Status)
        {
            var DamagedGoodsItem = new DAMAGEDS_GOODS_ITEM()
            {
                amount_doc = DamagedsGoods.Price * DamagedsGoods.Quantity,
                damaged_goods_doc = document,
                part_number = DamagedsGoods.PartNumber,
                price = DamagedsGoods.Price,
                quantity = DamagedsGoods.Quantity,
                cdate = DateTime.Now,
                cuser = User,
                program_id = "RMA001.cshtml",
                process_status = Status
            };
            _context.DAMAGEDS_GOODS_ITEM.Add(DamagedGoodsItem);
            _context.SaveChanges();
        }

        public void PostDamagesGoodsItemAndroid(DamagedsGoodsModel DamagedsGoods, string document, string User, int status)
        {
            var DamagedGoodsItem = new DAMAGEDS_GOODS_ITEM()
            {
                amount_doc = DamagedsGoods.Price * DamagedsGoods.Quantity,
                damaged_goods_doc = document,
                part_number = DamagedsGoods.PartNumber,
                price = DamagedsGoods.Price,
                quantity = DamagedsGoods.Quantity,
                cdate = DateTime.Now,
                cuser = User,
                program_id = "Android",
                process_status = status //Estatus para mandar compras
            };
            _context.DAMAGEDS_GOODS_ITEM.Add(DamagedGoodsItem);
            _context.SaveChanges();
        }

        public DAMAGEDS_GOODS_ITEM SearchPartNumberApproval(string PartNumber, string Damaged_id)
        {
            return _context.DAMAGEDS_GOODS_ITEM.Where(x => x.part_number == PartNumber && x.damaged_goods_doc == Damaged_id).Select(x => new DAMAGEDS_GOODS_ITEM
            {
                part_number = x.part_number,
                quantity = x.quantity,
                amount_doc = x.amount_doc,
                price = x.price
            }).SingleOrDefault();
        }

        public List<ItemViewRma> GetRMAItemsByFolio(string Folio, string type)
        {
            var Items = _context.DAMAGEDS_GOODS_ITEM
            .Join(_context.DAMAGEDS_GOODS, a => a.damaged_goods_doc, aa => aa.damaged_goods_doc, (a, aa) => new { a, aa })
            .Join(_context.ITEM, s => s.a.part_number, ss => ss.part_number, (s, ss) => new { s, ss })
            .Join(_context.ITEM_STOCK, it => it.s.a.part_number, itt => itt.part_number, (it, itt) => new { it, itt })
            .Join(_context.STORAGE_LOCATION, st => st.itt.storage_location, stt => stt.storage_location_name, (st, stt) => new { st, stt })
            .Where(s => s.st.it.s.a.damaged_goods_doc == Folio && s.st.it.s.aa.process_type == type && s.stt.storage_type == "MERMA" && s.st.it.s.a.process_status == 1)
            .OrderBy(o => o.st.it.s.a.part_number)
            .GroupBy(g => new { g.st.it.s.a.part_number, g.st.it.ss.part_description, g.st.it.ss.unit_size, g.st.it.s.a.quantity, g.st.it.s.a.amount_doc, g.st.it.s.a.price, g.st.itt.storage_location })
           .Select(s => new ItemViewRma
           {
               PartNumber = s.Key.part_number,
               Description = s.Key.part_description,
               Size = s.Key.unit_size,
               Storage = s.Key.storage_location,
               Quantity = s.Key.quantity,
               Amount_Doc = s.Key.amount_doc,
               Price = s.Key.price != null ? s.Key.price.Value : 0
           }).ToList();

            return Items;
        }

        public List<ItemViewRma> GetRMAItemsByFolio(string Folio, string Type, int Status)
        {
            var Items = _context.DAMAGEDS_GOODS_ITEM
            .Join(_context.DAMAGEDS_GOODS, a => a.damaged_goods_doc, aa => aa.damaged_goods_doc, (a, aa) => new { a, aa })
            .Join(_context.ITEM, s => s.a.part_number, ss => ss.part_number, (s, ss) => new { s, ss })
            .Join(_context.ITEM_STOCK, it => it.s.a.part_number, itt => itt.part_number, (it, itt) => new { it, itt })
            .Join(_context.STORAGE_LOCATION, st => st.itt.storage_location, stt => stt.storage_location_name, (st, stt) => new { st, stt })
            .Where(s => s.st.it.s.a.damaged_goods_doc == Folio && s.st.it.s.aa.process_type == Type && s.stt.storage_type == "MERMA" && s.st.it.s.a.process_status == Status)
            .OrderBy(o => o.st.it.s.a.part_number)
            .GroupBy(g => new { g.st.it.s.a.part_number, g.st.it.ss.part_description, g.st.it.ss.unit_size, g.st.it.s.a.quantity, g.st.it.s.a.amount_doc, g.st.it.s.a.price, g.st.itt.storage_location, g.st.it.s.aa.supplier_id })
           .Select(s => new ItemViewRma
           {
               PartNumber = s.Key.part_number,
               Description = s.Key.part_description,
               Size = s.Key.unit_size,
               Storage = s.Key.storage_location,
               Quantity = s.Key.quantity,
               Amount_Doc = s.Key.amount_doc,
               Price = s.Key.price != null ? s.Key.price.Value : 0,
               Supplier_Id = s.Key.supplier_id ?? 0
           }).ToList();

            return Items;
        }

        public List<ItemViewCost> GetListFiveCostBySupplier(string Folio, string part_number)
        {
            var supplier = _context.DAMAGEDS_GOODS.Where(w => w.damaged_goods_doc == Folio).Select(s => s.supplier_id).FirstOrDefault();
            var items = _context.PURCHASE_ORDER.Join(_context.PURCHASE_ORDER_ITEM, p => p.purchase_no, pi => pi.purchase_no, (p, pi) => new { p, pi })
                .Where(w => w.p.supplier_id == supplier && w.p.purchase_status == 9 && w.pi.item_status == 1 && w.pi.part_number == part_number)
                .Select(s => new ItemViewCost
                {
                    ValueDrop = s.pi.purchase_price.ToString(),
                    DescriptionDrop = "$"+s.pi.purchase_price.ToString(),
                    dateDrop = s.p.ata
                    
                })
                .OrderByDescending(o => o.dateDrop)
                .Take(5)
                .ToList();
            return items;
        }
        

        public DamagedGoodsViewModel GetRMAItemsByDocument(string Folio, string Type, int Status)
        {
            var DamagedGoodsViewModel = new DamagedGoodsViewModel
            {
                DamagedGoodsHeadModel = _context.DAMAGEDS_GOODS
                .Select(s => new DamagedGoodsHeadModel
                {
                    DamagedGoodsDoc = s.damaged_goods_doc,
                    ProcessStatusDescription = s.process_status == 0 ? "Inicial" : s.process_status == 1 ? "Pendiente" : s.process_status == 2 ? "Aprobado" : s.process_status == 3 ? "Rechazado" : s.process_status == 4 ? "Solicitud de Cancelación" : s.process_status == 5 ? "Aprobación Cancelada (Cancelado)" : s.process_status == 8 ? "Cancelado" : s.process_status == 9 ? "Terminado" : "",
                    Comment = s.comment
                })
                .Where(s => s.DamagedGoodsDoc == Folio).FirstOrDefault(),

                DamagedGoodsBanatiModel = _context.DAMAGEDS_GOODS_ITEM
                .Join(_context.DAMAGEDS_GOODS, a => a.damaged_goods_doc, aa => aa.damaged_goods_doc, (a, aa) => new { a, aa })
                .Join(_context.ITEM, s => s.a.part_number, ss => ss.part_number, (s, ss) => new { s, ss })
                .Join(_context.ITEM_STOCK, it => it.s.a.part_number, itt => itt.part_number, (it, itt) => new { it, itt })
                .Join(_context.STORAGE_LOCATION, st => st.itt.storage_location, stt => stt.storage_location_name, (st, stt) => new { st, stt })
                .Where(s => s.st.it.s.a.damaged_goods_doc == Folio && s.st.it.s.aa.process_type == Type && s.stt.storage_type == "MERMA" && s.st.it.s.a.process_status == Status)
                .OrderBy(o => o.st.it.s.a.part_number)
                .GroupBy(g => new { g.st.it.s.a.part_number, g.st.it.ss.part_description, g.st.it.ss.unit_size, g.st.it.s.a.quantity, g.st.it.s.a.amount_doc, g.st.it.s.a.price, g.st.itt.storage_location, g.st.it.s.a.scrap_reason })
               .Select(s => new DamagedGoodsBanatiModel
               {
                   PartNumber = s.Key.part_number,
                   Description = s.Key.part_description,
                   Quantity = s.Key.quantity,
                   Comment = s.Key.scrap_reason
               }).ToList()
            };

            return DamagedGoodsViewModel;
        }

        public List<ItemViewRma> GetAllRMAItemsByFolio(string Folio, string Type, int status)
        {
            var Items = _context.DAMAGEDS_GOODS_ITEM
            .Join(_context.DAMAGEDS_GOODS, a => a.damaged_goods_doc, aa => aa.damaged_goods_doc, (a, aa) => new { a, aa })
            .Join(_context.ITEM, s => s.a.part_number, ss => ss.part_number, (s, ss) => new { s, ss })
            .Join(_context.ITEM_STOCK, it => it.s.a.part_number, itt => itt.part_number, (it, itt) => new { it, itt })
            .Join(_context.STORAGE_LOCATION, st => st.itt.storage_location, stt => stt.storage_location_name, (st, stt) => new { st, stt })
            .Where(s => s.st.it.s.a.damaged_goods_doc == Folio && s.st.it.s.aa.process_type == Type && s.stt.storage_type == "MERMA" && s.st.it.s.a.process_status == status)
            .OrderBy(o => o.st.it.s.a.part_number)
            .GroupBy(g => new { g.st.it.s.a.part_number, g.st.it.ss.part_description, g.st.it.ss.unit_size, g.st.it.s.a.quantity, g.st.it.s.a.amount_doc, g.st.it.s.a.price, g.st.itt.storage_location })
           .Select(s => new ItemViewRma
           {
               PartNumber = s.Key.part_number,
               Description = s.Key.part_description,
               Size = s.Key.unit_size,
               Storage = s.Key.storage_location,
               Quantity = s.Key.quantity,
               Amount_Doc = s.Key.amount_doc,
               Price = s.Key.price != null ? s.Key.price.Value : 0
           }).ToList();

            return Items;
        }

        public int AddDamageGoodsItemAndroid(DAMAGEDS_GOODS_ITEM item)
        {
            try
            {
                _context.DAMAGEDS_GOODS_ITEM.Add(item);
                var save = _context.SaveChanges();
                return save;
            }
            catch (Exception ex)
            {
                var msg = ex.Message;
                return 0;
            }
        }

        public List<DamagedsGoodsModel> GetItemsByDocument(string document)
        {
            var Items = _context.DAMAGEDS_GOODS_ITEM
            .Join(_context.ITEM, damageItem => damageItem.part_number, item => item.part_number, (damageItem, item) => new { damageItem, item })
            .Where(s => s.damageItem.damaged_goods_doc == document & s.damageItem.process_status == 9)
           .Select(s => new DamagedsGoodsModel
           {
               PartNumber = s.damageItem.part_number,
               Description = s.item.part_description,
               Quantity = s.damageItem.quantity,
               Amount = s.damageItem.amount_doc != null ? s.damageItem.amount_doc.Value : 0,
               Price = s.damageItem.price != null ? s.damageItem.price.Value : 0,
               ScrapReason = s.damageItem.scrap_reason
           }).ToList();
            return Items;
        }

        public List<DamagedsGoodsModel> GetItemsByDocument(string document, int status)
        {
            var Items = _context.DAMAGEDS_GOODS_ITEM
            .Join(_context.ITEM, damageItem => damageItem.part_number, item => item.part_number, (damageItem, item) => new { damageItem, item })
            .Where(s => s.damageItem.damaged_goods_doc == document & s.damageItem.process_status == status)
           .Select(s => new DamagedsGoodsModel
           {
               PartNumber = s.damageItem.part_number,
               Description = s.item.part_description,
               Quantity = s.damageItem.quantity,
               Amount = s.damageItem.amount_doc != null ? s.damageItem.amount_doc.Value : 0,
               Price = s.damageItem.price != null ? s.damageItem.price.Value : 0,
               ScrapReason = s.damageItem.scrap_reason
           }).ToList();
            return Items;
        }

        public List<DamagedsGoodsModel> GetItemsByDocumentSingle(string document, int status)
        {
            var Items = _context.DAMAGEDS_GOODS_ITEM_SINGLE
            .Join(_context.ITEM, damageItem => damageItem.part_number, item => item.part_number, (damageItem, item) => new { damageItem, item })
            .Where(s => s.damageItem.damaged_goods_doc_unique == document & s.damageItem.process_status == status)
           .Select(s => new DamagedsGoodsModel
           {
               PartNumber = s.damageItem.part_number,
               Description = s.item.part_description,
               Quantity = s.damageItem.quantity,
               Amount = s.damageItem.amount_doc,
               Price = s.damageItem.price,
               ScrapReason = s.damageItem.scrap_reason,
               id_single = s.damageItem.id_damageds_goods_single
           }).ToList();
            return Items;
        }

        public List<DamagedsGoodsModel> NewGetItemsByDocumentReport(string document, int status)
        {
            var Items = _context.DAMAGEDS_GOODS_ITEM
                .Join(_context.ITEM, damageItem => damageItem.part_number, item => item.part_number, (damageItem, item) => new { damageItem, item })
                .Where(s => s.damageItem.damaged_goods_doc == document && s.damageItem.process_status == status)
                .Select(s => new DamagedsGoodsModel
                {
                    PartNumber = s.damageItem.part_number,
                    Description = s.item.part_description,
                    Quantity = s.damageItem.quantity,
                    Amount = s.damageItem.amount_doc != null ? s.damageItem.amount_doc.Value : 0,
                    Price = s.damageItem.price != null ? s.damageItem.price.Value : 0,
                    ScrapReason = s.damageItem.scrap_reason
                }).ToList();
            return Items;
        }
        public List<DamagedsGoodsModel> NewGetItemsByDocumentReportStatus8or9(string document)
        {
            var Items = _context.DAMAGEDS_GOODS_ITEM
                .Join(_context.ITEM, damageItem => damageItem.part_number, item => item.part_number, (damageItem, item) => new { damageItem, item })
                .Where(s => s.damageItem.damaged_goods_doc == document && (s.damageItem.process_status == 8 || (s.damageItem.process_status == 9)))
                .Select(s => new DamagedsGoodsModel
                {
                    PartNumber = s.damageItem.part_number,
                    Description = s.item.part_description,
                    Quantity = s.damageItem.quantity,
                    Amount = s.damageItem.amount_doc != null ? s.damageItem.amount_doc.Value : 0,
                    Price = s.damageItem.price != null ? s.damageItem.price.Value : 0,
                    ScrapReason = s.damageItem.scrap_reason
                }).ToList();
            return Items;
        }

        public List<DamagedsGoodsModel> GetItemsByDocumentReport(string document)
        {
            var Items = _context.DAMAGEDS_GOODS_ITEM
                .Join(_context.ITEM, damageItem => damageItem.part_number, item => item.part_number, (damageItem, item) => new { damageItem, item })
                .Where(s => s.damageItem.damaged_goods_doc == document)
                .Select(s => new DamagedsGoodsModel
                {
                    PartNumber = s.damageItem.part_number,
                    Description = s.item.part_description,
                    Quantity = s.damageItem.quantity,
                    Amount = s.damageItem.amount_doc != null ? s.damageItem.amount_doc.Value : 0,
                    Price = s.damageItem.price != null ? s.damageItem.price.Value : 0,
                    ScrapReason = s.damageItem.scrap_reason
                }).ToList();
            return Items;
        }

        public DamagedsGoodsModel GetDocumentGoods(string document)
        {
            var items = _context.DAMAGEDS_GOODS.ToList()
                 .Join(_context.SITE_CONFIG, s => s.site_code, x => x.site_code, (s, x) => new { s, x })
                 .Where(x => x.s.damaged_goods_doc == document && x.s.process_type == "SCRAP")
                 .Select(x => new DamagedsGoodsModel
                 {
                     Document = x.s.damaged_goods_doc,
                     User = x.s.cuser,
                     SiteName = x.x.site_name,
                     Fecha = x.s.udate.Value != null ? x.s.udate.Value.ToString("dd/MM/yyyy") : DateTime.Now.ToString("dd/MM/yyyy"),
                     Print_Status = x.s.print_status ?? 0,
                     Status = x.s.process_status
                 }).SingleOrDefault();
            return items;
        }

        public List<DamagedsGoodsModel> GetScrapItemsByFolio(string Folio)
        {
            var x = _context.Database.SqlQuery<DamagedsGoodsModel>(@"select it.part_number as PartNumber, it.part_description as Description,
                it.unit_size as Size ,its.storage_location as Storage,
                dgi.quantity as Quantity, case when  dgi.price is null then 0 else dgi.price end  as Price,
                dgi.scrap_reason as ScrapReason,
                (case when  dgi.price is null then 0 else ROUND ((dgi.price*dgi.quantity),4)   end) as Amount,
                (dbo.fnc_Get_Tax( it.part_number ,1))as IVA,
                (dbo.fnc_Get_Tax( it.part_number ,2)) as Ieps,
                convert(varchar,td.document_date, 103) as Fecha
                from DAMAGEDS_GOODS_ITEM dgi 
                inner join DAMAGEDS_GOODS dg on dg.damaged_goods_doc = dgi.damaged_goods_doc
                inner join ITEM  it on it.part_number = dgi.part_number
                inner join ITEM_STOCK its on its.part_number = it.part_number
                inner join TRANSACTION_DOCUMENT td on dg.damaged_goods_doc = td.reference_document
                inner join STORAGE_LOCATION stl on stl.storage_location_name = its.storage_location 
                and dg.process_type = 'SCRAP' and stl.storage_type = 'MERMA' 
                where  dg.damaged_goods_doc = @folio and dgi.process_status != 3 and dgi.process_status != 8", new SqlParameter("@folio", Folio)).ToList();
            return x;
        }

        //BANATi
        public int BanatiIdSupplier(string name)
        {
            var Banati = _context.MA_SUPPLIER.Where(x => x.business_name.ToLower().Contains(name.ToLower()) || x.commercial_name.ToLower().Contains(name.ToLower())).FirstOrDefault();
            if (Banati != null)
                return Banati.supplier_id;
            else
                return 0;
        }

        public int AddDamageGoodsItemAndroidSingle(DAMAGEDS_GOODS_ITEM_SINGLE item)
        {
            try
            {
                _context.DAMAGEDS_GOODS_ITEM_SINGLE.Add(item);
                var save = _context.SaveChanges();
                return save;
            }
            catch (Exception ex)
            {
                var msg = ex.Message;
                return 0;
            }
        }

        public List<DamagedGoodsBanatiModel> GetRmaBanatiItemsByDocument(string document)
        {
            var Items = _context.DAMAGEDS_GOODS_ITEM
            .Join(_context.ITEM, damageItem => damageItem.part_number, item => item.part_number, (damageItem, item) => new { damageItem, item })
            .Where(s => s.damageItem.damaged_goods_doc == document)
            .Select(s => new DamagedGoodsBanatiModel
            {
                PartNumber = s.damageItem.part_number,
                Description = s.item.part_description,
                Quantity = s.damageItem.quantity,
                Comment = s.damageItem.scrap_reason
            }).ToList();
            return Items;
        }
    }
}