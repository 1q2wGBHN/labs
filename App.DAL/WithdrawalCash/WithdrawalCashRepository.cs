﻿using App.Entities;
using App.Entities.ViewModels.Withdrawal;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App.DAL.WithdrawalCash
{
    public class WithdrawalCashRepository
    {
        private DFL_SAIEntities _context;

        public WithdrawalCashRepository()
        {
            _context = new DFL_SAIEntities();
        }

        public List<WithdrawalHeader> GetReportData(string Date, string CashierNumber)
        {            
            DateTime DateValue = DateTime.ParseExact(Date, "dd/MM/yyyy", CultureInfo.InvariantCulture);
            var predicate = PredicateBuilder.True<DFLPOS_CASH_WITHDRAWAL>();

            if(!string.IsNullOrEmpty(CashierNumber))
                predicate = predicate.And(c => c.userr_id == CashierNumber);

            predicate = predicate.And(c => DbFunctions.TruncateTime(c.datee) == DateValue);

            var WithDrawalList = _context.DFLPOS_CASH_WITHDRAWAL.Where(predicate).ToList();

            var ReportList = new List<WithdrawalHeader>();

            ReportList = (from cw in WithDrawalList
                          join cwd in _context.DFLPOS_DETAIL_CASH_WITHDRAWAL on cw.withdrawal_id equals cwd.withdrawal_id                          
                          select new { cw, cwd } into t1
                          group t1 by new { t1.cw.USER_MASTER1.first_name, t1.cw.USER_MASTER1.last_name, t1.cwd.cur_code, t1.cw.userr_id } into gp
                          select new
                          {
                              FirstName = gp.Key.first_name,
                              LastName = gp.Key.last_name,
                              Currency = gp.Key.cur_code,
                              EmployeeNo = gp.Key.userr_id,
                              Amount = gp.Sum(x => x.cwd.denomination * x.cwd.quantity.Value)
                          }).AsEnumerable().Select(c => new WithdrawalHeader
                          {
                              Currency = c.Currency,
                              Cashier = string.Format("{0} {1}", c.FirstName, c.LastName),
                              Amount = c.Amount,
                              EmployeeNo = c.EmployeeNo
                          }).ToList();         

            return ReportList;
        }

        public decimal GetTotalAmount(string Date, string CashierNumber)
        {
            DateTime DateValue = DateTime.ParseExact(Date, "dd/MM/yyyy", CultureInfo.InvariantCulture);
            var predicate = PredicateBuilder.True<DFLPOS_CASH_WITHDRAWAL>();

            if (!string.IsNullOrEmpty(CashierNumber))
                predicate = predicate.And(c => c.userr_id == CashierNumber);

            predicate = predicate.And(c => DbFunctions.TruncateTime(c.datee) == DateValue);

            var WithDrawalList = _context.DFLPOS_CASH_WITHDRAWAL.Where(predicate).ToList();

            decimal TotalAmount = 0;            

            TotalAmount = WithDrawalList.Sum(c => c.amount_base_currency.Value);

            return TotalAmount;            
        }

        public List<WithdrawalHeader> GetAcumulateData(string Date, string CashierNumber)
        {
            DateTime DateValue = DateTime.ParseExact(Date, "dd/MM/yyyy", CultureInfo.InvariantCulture);
            var predicate = PredicateBuilder.True<DFLPOS_CASH_WITHDRAWAL>();

            if (!string.IsNullOrEmpty(CashierNumber))
                predicate = predicate.And(c => c.userr_id == CashierNumber);

            predicate = predicate.And(c => DbFunctions.TruncateTime(c.datee) == DateValue);

            var WithDrawalList = _context.DFLPOS_CASH_WITHDRAWAL.Where(predicate).ToList();

            var ReportList = new List<WithdrawalHeader>();

            ReportList = (from wl in WithDrawalList
                          group wl by wl.USER_MASTER1 into gp
                          select new
                          {
                              FirstName = gp.Key.first_name,
                              LastName = gp.Key.last_name,                              
                              EmployeeNo = gp.Key.emp_no,
                              Amount = gp.Sum(x => x.amount_base_currency.Value)                              
                          }).AsEnumerable().Select(c => new WithdrawalHeader
                          {                              
                              Cashier = string.Format("{0} {1}", c.FirstName, c.LastName),
                              Amount = c.Amount,
                              EmployeeNo = c.EmployeeNo                              
                          }).ToList();

            return ReportList;
        }
    }
}
