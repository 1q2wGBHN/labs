﻿using App.Entities;
using App.Entities.ViewModels.Withdrawal;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static App.Entities.ViewModels.Common.Constants;

namespace App.DAL.WithdrawalCash
{
    public class WithdrawalCashDetailsRepository
    {
        private DFL_SAIEntities _context;

        public WithdrawalCashDetailsRepository()
        {
            _context = new DFL_SAIEntities();
        }

        public List<WithdrawalDetails> WithDrawalDetails(string Date, string Currency, string EmployeeNo)
        {
            DateTime DateValue = DateTime.ParseExact(Date, DateFormat.INT_DATE, CultureInfo.InvariantCulture);
            var DataList = new List<WithdrawalDetails>();
            DataList = (from cw in _context.DFLPOS_CASH_WITHDRAWAL
                        join dcw in _context.DFLPOS_DETAIL_CASH_WITHDRAWAL on cw.withdrawal_id equals dcw.withdrawal_id
                        where DbFunctions.TruncateTime(cw.datee.Value) == DateValue
                              && dcw.cur_code == Currency
                              && cw.userr_id == EmployeeNo
                        //&& dcw.boucher
                        select new
                        {
                            Date = cw.datee.Value,
                            WithdrawalType = dcw.PAYMENT_METHOD.pm_name,
                            Amount = dcw.quantity.Value * dcw.denomination,
                            Site = cw.site_code
                        }).AsEnumerable().Select(c => new WithdrawalDetails
                        {
                            Date = c.Date.ToString("dd/MM/yyyy HH:mm:ss tt"),
                            Site = c.Site,
                            Amount = c.Amount,
                            WithdrawalType = c.WithdrawalType
                        }).ToList();

            return DataList;
        }

        public List<DollarsConciliation> DollarsConciliation(string DateValue)
        {
            DateTime Date = DateTime.ParseExact(DateValue, DateFormat.INT_DATE, CultureInfo.InvariantCulture);
            var DataList = new List<DollarsConciliation>();
            var ReceivedList = (from s in _context.DFLPOS_SALES
                                join spm in _context.DFLPOS_SALES_PAYMENT_METHOD on s.sale_id equals spm.sale_id
                                where s.sale_date == Date
                                      && spm.PAYMENT_METHOD.currency == Currencies.USD
                                      && s.sale_total != 0
                                select new { s, spm } into t
                                group t by new { t.spm.emp_no, t.s.sale_date, t.spm.PAYMENT_METHOD.currency } into gp
                                select new
                                {
                                    Received = gp.Sum(l => l.spm.amount_entered),
                                    EmployeeId = gp.Key.emp_no
                                }).ToList();

            var DirectDonations = (from d in _context.DFLPOS_DONATIONS_PAYMENT_METHOD
                                         where d.sale_id.Value == 0
                                               && d.payment_method_id.Value == PaymentMethods.USD_CASH
                                               && DbFunctions.TruncateTime(d.sale_date.Value) == Date
                                         group d by new { d.userr_id } into gp
                                         select new
                                         {
                                             Received = gp.Sum(l => l.amount),
                                             EmployeeId = gp.Key.userr_id
                                         }).ToList();

            if (DirectDonations != null && DirectDonations.Count > 0)
                ReceivedList.AddRange(DirectDonations);

            var ReceivedListEmployees = (from r in ReceivedList
                                         group r by new { r.EmployeeId } into gp
                                         select new
                                         {
                                             Received = gp.Sum(l => l.Received),
                                             EmployeeId = gp.Key.EmployeeId
                                         }).ToList();

            var CancelledList = (from s in _context.DFLPOS_SALES
                                 join spm in _context.DFLPOS_SALES_PAYMENT_METHOD on s.sale_id equals spm.sale_id
                                 where spm.pm_id == PaymentMethods.USD_CASH
                                       && s.sale_date == Date
                                       && s.sale_total == 0
                                 select new { s, spm } into t
                                 group t by new { t.spm.emp_no, t.s.sale_date } into gp
                                 select new
                                 {
                                     Cancelled = gp.Sum(l => l.spm.amount_entered.Value),
                                     EmployeeId = gp.Key.emp_no
                                 }).ToList();

            var WithdrawalsList = (from cw in _context.DFLPOS_CASH_WITHDRAWAL
                                   join cwd in _context.DFLPOS_DETAIL_CASH_WITHDRAWAL on cw.withdrawal_id equals cwd.withdrawal_id
                                   where cwd.pm_id == PaymentMethods.USD_CASH
                                         && DbFunctions.TruncateTime(cw.datee.Value) == Date
                                   select new { cw, cwd } into t
                                   group t by new { t.cw.userr_id, t.cwd.denomination } into gp
                                   select new
                                   {
                                       EmployeeId = gp.Key.userr_id,
                                       Withdrawal = gp.Sum(x => x.cwd.quantity),
                                       Denomination = gp.Key.denomination
                                   }).ToList();

            var WithdrawalsGroupList = (from wl in WithdrawalsList
                                        group wl by new { wl.Denomination, wl.EmployeeId } into gp
                                        select new
                                        {
                                            EmployeeId = gp.Key.EmployeeId,
                                            WithdrawalSingle = gp.Key.Denomination * gp.Sum(x => x.Withdrawal)
                                        }).ToList();

            var WithdrawalsTotalsList = (from wg in WithdrawalsGroupList
                                         group wg by new { wg.EmployeeId } into gp
                                         select new
                                         {
                                             EmployeeId = gp.Key.EmployeeId,
                                             TotalAmount = gp.Sum(x => x.WithdrawalSingle.Value)
                                         }).ToList();

            DataList = (from r in ReceivedListEmployees
                        join u in _context.USER_MASTER on r.EmployeeId equals u.emp_no
                        join wl in WithdrawalsTotalsList on r.EmployeeId equals wl.EmployeeId into LeftJoinWithdrawal
                        from w in LeftJoinWithdrawal.DefaultIfEmpty()
                        select new DollarsConciliation
                        {
                            CashierId = r.EmployeeId,
                            Cashier = u.first_name + " " + u.last_name,
                            Received = r.Received.Value,
                            Cancelled = 0,
                            Withdrawal = w != null ? w.TotalAmount : 0,
                            Difference = w != null ? r.Received.Value - w.TotalAmount : r.Received.Value
                        }).ToList();

            if (CancelledList.Count > 0)
            {
                foreach (var c in CancelledList)
                {
                    var Cashier = DataList.Where(d => d.CashierId == c.EmployeeId).FirstOrDefault();
                    if (Cashier != null)
                    {
                        DataList.Remove(Cashier);
                        Cashier.Cancelled = c.Cancelled;
                        Cashier.Received += c.Cancelled;
                        Cashier.Difference = Cashier.Received - (c.Cancelled + Cashier.Withdrawal);
                        DataList.Add(Cashier);
                    }
                    else
                    {
                        var u = _context.USER_MASTER.Where(d => d.emp_no == c.EmployeeId).FirstOrDefault();
                        Cashier = new DollarsConciliation();
                        Cashier.CashierId = c.EmployeeId;
                        Cashier.Cashier = u.first_name + " " + u.last_name;
                        Cashier.Received = c.Cancelled;
                        Cashier.Cancelled = c.Cancelled;
                        Cashier.Withdrawal = 0;
                        Cashier.Difference = Cashier.Received - (c.Cancelled + Cashier.Withdrawal);
                        DataList.Add(Cashier);
                    }
                }
            }


            return DataList;
        }
    }
}
