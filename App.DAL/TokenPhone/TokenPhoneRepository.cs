﻿using App.Entities;
using System;
using System.Linq;

namespace App.DAL.TokenPhone
{
    public class TokenPhoneRepository
    {
        private readonly DFL_SAIEntities _context;

        public TokenPhoneRepository()
        {
            _context = new DFL_SAIEntities();
        }

        /// <summary>
        /// Buscar al usuario si existe en la tabla TOKEN_PHONE
        /// </summary>
        /// <param name="param">string usuario</param>
        /// <returns>Regresa informacion del usuario</returns>
        public TOKEN_PHONE SearchUserByUserName(string user)
        {
            return _context.TOKEN_PHONE.SingleOrDefault(x => x.user_name == user);
        }

        /// <summary>
        /// Busca el si existen mas de uno con el mismo Id_token y cambia el Id_token
        /// </summary>
        /// <param name="param">int id token</param>
        /// <returns>Regresa 1 si todos los cambios fueron existosos o 0 si hubo errores</returns>
        public int SearchTokenRepeat(int id_token)
        {
            try
            {
                var items = _context.TOKEN_PHONE.Where(x => x.id_token == id_token).ToList();

                foreach (var item in items)
                {
                    item.id_token = 1; //Siempre el primer Registro sera el erroneo
                    _context.SaveChanges();
                }
            }
            catch (Exception)
            {
                return 0;
            }
            return 1;
        }

        /// <summary>
        /// Actualiza la informacion del usuario modicando el token por uno vacio
        /// </summary>
        /// <param name="param">objeto del usuario</param>
        /// <param name="param">int id_token</param>
        /// <returns>Regresa un 0 si hubo errores o un 1 si todo fue exitoso</returns>
        public int UpdateUserToken(TOKEN_PHONE userInfo, int id_token)
        {
            try
            {
                userInfo.udate = DateTime.Now;
                userInfo.id_token = id_token;
                _context.SaveChanges();
            }
            catch (Exception)
            {
                return 0;
            }

            return 1;
        }

        /// <summary>
        /// Guarda un nuevo usuario con su token correspondiente
        /// </summary>
        /// <param name="param">string usuario</param>
        /// <param name="param">id_token</param>
        /// <returns>Regresa un 0 si hubo errores o un 1 si todo fue exitoso</returns>
        public int CreationUserToken(string user, int id_token)
        {
            try
            {
                var UserCreate = new TOKEN_PHONE()
                {
                    user_name = user,
                    udate = DateTime.Now,
                    id_token = id_token
                };
                _context.TOKEN_PHONE.Add(UserCreate);
                _context.SaveChanges();
            }
            catch (Exception)
            {
                return 0;
            }

            return 1;
        }
    }
}