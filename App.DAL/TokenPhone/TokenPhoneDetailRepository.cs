﻿using App.Entities;
using App.Entities.ViewModels.TokenPhone;
using System;
using System.Linq;

namespace App.DAL.TokenPhone
{
    public class TokenPhoneDetailRepository
    {
        private readonly DFL_SAIEntities _context;

        public TokenPhoneDetailRepository()
        {
            _context = new DFL_SAIEntities();
        }

        /// <summary>
        /// Buscar un token si es que existe
        /// </summary>
        /// <param name="param">string token</param>
        /// <returns>Regresa informacion del usuario</returns>
        public TOKEN_PHONE_DETAIL SearchToken(string token)
        {
            return _context.TOKEN_PHONE_DETAIL.SingleOrDefault(x => x.token == token);
        }

        /// <summary>
        /// Registra una nuevo token con toda la informacion del celular
        /// </summary>
        /// <param name="param">string user</param>
        /// <param name="param">string token</param>
        /// <param name="param">string modelo del celular</param>
        /// <param name="param">string marca del celular</param>
        /// <param name="param">string sdk del celular</param>
        /// <param name="param">string identificador unico del celular</param>
        /// <returns>Regresa el id generado por la insercion o un 0 si hubo un error</returns>
        public int CreationTokenPhone(string user, string token, string model, string brand, string sdk, string id_phone)
        {
            int lastId;
            try
            {
                var TokenCreate = new TOKEN_PHONE_DETAIL()
                {
                    id_phone = id_phone,
                    cdate = DateTime.Now,
                    token = token,
                    brand = brand,
                    model = model,
                    sdk = sdk,
                    cuser = user,
                };
                _context.TOKEN_PHONE_DETAIL.Add(TokenCreate);
                _context.SaveChanges();
                lastId = TokenCreate.id_token;
                return lastId;
            }
            catch (Exception)
            {
                return 0;
            }
        }

        /// <summary>
        /// Busca un token por su identificador
        /// </summary>
        /// <param name="param">string token</param>
        /// <returns>Regresa informacion del usuario</returns>
        public TOKEN_PHONE_DETAIL SearchTokenByIdToken(int id_token)
        {
            return _context.TOKEN_PHONE_DETAIL.SingleOrDefault(x => x.id_token == id_token);
        }

        /// <summary>
        /// Busca un token por su usuario (join)
        /// </summary>
        /// <param name="param">string usuario</param>
        /// <returns>Regresa el token</returns>
        public TokenPhoneModel SearchTokenByUser(string user)
        {
            return _context.TOKEN_PHONE_DETAIL
                .Join(_context.TOKEN_PHONE, tDetail => tDetail.id_token, tPhone => tPhone.id_token, (tDetail, tPhone) => new { tDetail, tPhone })
                .Where(elements => elements.tPhone.user_name == user)
                .Select(elements => new TokenPhoneModel
                {
                    token = elements.tDetail.token
                }).SingleOrDefault();
        }
    }
}