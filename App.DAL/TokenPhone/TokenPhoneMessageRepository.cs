﻿using App.Entities;
using System;

namespace App.DAL.TokenPhone
{
    public class TokenPhoneMessageRepository
    {
        private readonly DFL_SAIEntities _context;

        public TokenPhoneMessageRepository()
        {
            _context = new DFL_SAIEntities();
        }

        /// <summary>
        /// Registra la informacion de una notificacion que se mandara
        /// </summary>
        /// <param name="param">string titulo</param>
        /// <param name="param">string menssaje</param>
        /// <param name="param">string tipo de notificacion</param>
        /// <param name="param">string usuario destinario (user_send_to)</param>
        /// <param name="param">string quien mande el mensaje (cuser)</param>
        /// <param name="param">string pagina desde donde se mando el mensaje(program_id)</param>
        /// <returns>Regresa el id generado por la insercion o un 0 si hubo un error</returns>
        public TOKEN_PHONE_MESSAGE SaveMessage(int id_token_send_to, string title, string message, int type_notification, string user_send_to, string cuser, string program_id)
        {
            try
            {
                var Message = new TOKEN_PHONE_MESSAGE()
                {
                    title = title,
                    message = message,
                    type_notification = type_notification,
                    id_token_sent_to = id_token_send_to,
                    user_send_to = user_send_to,
                    successful = true,
                    cdate = DateTime.Now,
                    cuser = cuser,
                    program_id = program_id,
                };
                _context.TOKEN_PHONE_MESSAGE.Add(Message);
                _context.SaveChanges();
                return Message;
            }
            catch (Exception)
            {
                return null;
            }
        }

        /// <summary>
        /// Actualiza el estatus del mensaje por hubo un error
        /// </summary>
        /// <param name="param">objeto de TOKEN_PHONE_MESSAGE</param>
        /// <returns>Regresa 1 si todo fue correcto o 0 si hubo error</returns>
        public int MessegeNotSent(TOKEN_PHONE_MESSAGE message)
        {
            try
            {
                message.successful = false;
                message.cdate = DateTime.Now;
                _context.SaveChanges();
                return 1;
            }
            catch (Exception)
            {
                return 0;
            }
        }
    }
}