﻿using App.Entities;
using App.Entities.ViewModels.Site;
using System.Linq;

namespace App.DAL.Configuration
{
    public class SysSiteConfigRepository
    {
        private readonly DFL_SAIEntities _context;

        public SysSiteConfigRepository()
        {
            _context = new DFL_SAIEntities();
        }

        public SiteViewModel GetSite()
        {
            var Site = _context.SITE_CONFIG.SingleOrDefault();
            var SiteView = new SiteViewModel()
            {
                site_code = Site.site_code,
                site_name = Site.site_name
            };
            if (SiteView != null)
                return SiteView;

            return new SiteViewModel();
        }

        public SiteModel GetSiteModel()
        {
            var Site = _context.SITE_CONFIG.SingleOrDefault();
            var SiteView = new SiteModel()
            {
                site_code = Site.site_code,
                site_name = Site.site_name
            };
            if (SiteView != null)
                return SiteView;

            return new SiteModel();
        }
    }
}