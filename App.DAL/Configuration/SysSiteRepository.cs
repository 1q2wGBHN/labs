﻿using App.Entities;
using System.Collections.Generic;
using System.Linq;

namespace App.DAL.Configuration
{
    public class SysSiteRepository
    {
        private readonly DFL_SAIEntities _context;

        public SysSiteRepository()
        {
            _context = new DFL_SAIEntities();
        }

        public List<SiteViewModel> GetAllSites()
        {
            var Sites = _context.SITES.Select(x => new SiteViewModel
            {
                site_code = x.site_code,
                address = x.address,
                ip_address = x.ip_address.Trim() + ":" + x.port.Trim(),
                site_name = x.site_name
            }).ToList();
            if (Sites != null)
                return Sites;

            return new List<SiteViewModel>();
        }

        public SiteViewModel GetSite()
        {
            var Site = _context.SITE_CONFIG.SingleOrDefault();
            var SiteView = new SiteViewModel()
            {
                site_code = Site.site_code,
                site_name = Site.site_name
            };
            if (SiteView != null)
                return SiteView;

            return new SiteViewModel();
        }
    }
}