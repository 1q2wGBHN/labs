﻿using App.Entities;
using App.Entities.ViewModels.BarcodeCaptureTime;
using Dapper;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App.DAL.BarcodeCaptureTime
{
    public class BarcodeCaptureTimeRepository
    {
        private DFL_SAIEntities _context;

        public BarcodeCaptureTimeRepository()
        {
            _context = new DFL_SAIEntities();
        }

        public BarcodeCaptureTimeInfo GetDataReport(BarcodeCaptureTimeIndex Model)
        {
            DateTime DateStart = DateTime.ParseExact(Model.StartDate, "dd/MM/yyyy", CultureInfo.InvariantCulture);
            DateTime DateEnd = DateTime.ParseExact(Model.EndDate, "dd/MM/yyyy", CultureInfo.InvariantCulture);
            var ReportInfo = new BarcodeCaptureTimeInfo();

            SqlMapper.GridReader reader = _context.Database.Connection.QueryMultiple("DFLPOS_ScanInfo_Report", param: new { StartDate = DateStart, EndDate = DateEnd }, commandType: CommandType.StoredProcedure);

            ReportInfo.MainData = reader.Read<BarcodeCaptureTimeData>().ToList();
            ReportInfo.IssuesCount = reader.Read<BarcodeCaptureTimeIssues>().ToList();
            ReportInfo.ScannedTotal = reader.Read<BarcodeCaptureTimeTotalScanned>().ToList();            

            return ReportInfo;
        }        
    }
}
