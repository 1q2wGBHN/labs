﻿using App.Entities;
using App.Entities.ViewModels.Common;
using App.Entities.ViewModels.Customers;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.SqlClient;
using System.Globalization;
using System.Linq;
using static App.Entities.ViewModels.Common.Constants;

namespace App.DAL.Customer
{
    public class CustomerRepository
    {
        private DFL_SAIEntities _context;

        public CustomerRepository()
        {
            _context = new DFL_SAIEntities();
        }        

        public CustomerViewModel GetCustomer(string customer_code)
        {
            return (from c in _context.CUSTOMERS
                    where c.customer_code == customer_code                          
                    select new CustomerViewModel
                    {
                        Code = c.customer_code,
                        Name = c.customer_name.ToUpper(),
                        RFC = c.customer_rfc,
                        Address = c.customer_address,
                        Address_2 = c.customer_address2,
                        CityId = c.city_id,
                        StateId = c.state_id,
                        ZipCode = c.customer_zip,
                        Phone = c.customer_phone,
                        Phone_2 = c.customer_phone2,
                        Phone_3 = c.customer_phone3,
                        Email = c.customer_email,
                        Ship_To = c.customer_ship_to,
                        Credit_Days = c.customer_credit_days,
                        Credit_Limit = c.customer_credit_limit,
                        Credit_Limit_Alt = c.customer_credit_limit_alt,
                        Contact = c.customer_contact,
                        Contact_2 = c.customer_contact2,
                        Cuenta_C = c.customer_cuenta_c,
                        Country = c.customer_country,
                        Web = c.customer_web,
                        Status = c.customer_status,
                        CityName = c.CITIES.city_nombre,
                        StateName = c.STATES.state_name,
                    }).FirstOrDefault();
        }

        public CustomerViewModel GetCustomerByCode(string ClientNumber)
        {            
            var Model = new CustomerViewModel();

            var Customer = _context.CUSTOMERS.FirstOrDefault(c => c.customer_code == ClientNumber);
            if (Customer == null)
                Customer = _context.CUSTOMERS.Where(c => c.customer_code.Contains(ClientNumber)).FirstOrDefault();

            if (Customer != null)
            {
                Model.Code = Customer.customer_code;
                Model.Name = Customer.customer_name;
                Model.RFC = Customer.customer_rfc;
                Model.Address = Customer.customer_address;
                Model.Address_2 = Customer.customer_address2;
                Model.CityId = Customer.city_id;
                Model.StateId = Customer.state_id;
                Model.ZipCode = Customer.customer_zip;
                Model.Phone = Customer.customer_phone;
                Model.Phone_2 = Customer.customer_phone2;
                Model.Phone_3 = Customer.customer_phone3;
                Model.Email = Customer.customer_email;
                Model.Ship_To = Customer.customer_ship_to;
                Model.Credit_Days = Customer.customer_credit_days;
                Model.Credit_Limit = Customer.customer_credit_limit;
                Model.Credit_Limit_Alt = Customer.customer_credit_limit_alt;
                Model.Contact = Customer.customer_contact;
                Model.Contact_2 = Customer.customer_contact2;
                Model.Cuenta_C = Customer.customer_cuenta_c;
                Model.Country = Customer.customer_country;
                Model.Web = Customer.customer_web;
                Model.Status = Customer.customer_status;
                Model.CityName = Customer.CITIES.city_nombre;
                Model.StateName = Customer.STATES.state_name;
            }
            return Model;
        }

        public CUSTOMERS GetByName(string CustomerName)
        {
            return _context.CUSTOMERS.Where(c => c.customer_name.Replace(" ", "").ToLower() == CustomerName.ToLower()).FirstOrDefault();
        }

        public bool Create(CUSTOMERS Customer)
        {
            try
            {
                _context.CUSTOMERS.Add(Customer);
                _context.SaveChanges();
                return true;
            }
            catch(Exception ex)
            {
                string msg = ex.Message;
                return false;
            }
        }

        public bool Update(CUSTOMERS Customer, SYS_LOG SysLog)
        {
            var CustomerEdit = _context.CUSTOMERS.Find(Customer.customer_code);
            if (CustomerEdit != null)
            {
                _context.Entry(CustomerEdit).State = EntityState.Detached;
                _context.Entry(Customer).State = EntityState.Modified;
                _context.SaveChanges();
                if (SysLog != null)
                {
                    _context.SYS_LOG.Add(SysLog);
                    _context.SaveChanges();
                }
                return true;
            }
            return false;
        }

        public bool ExisitingRFC(object CustomerRFC)
        {
            var exist = _context.CUSTOMERS.Find(CustomerRFC);
            if (exist != null)
                return true;
            return false;
        }

        public bool ExisitingCode(string CustomerCode)
        {
            var exist = _context.CUSTOMERS.Where(c=>c.customer_code == CustomerCode).FirstOrDefault();
            if (exist != null)
                return true;
            return false;
        }      
        
        public List<CustomersDDL> FilterCustomers(bool AllCostumers)
        {
            var CustomerList = _context.Database.SqlQuery<CustomersDDL>("sp_Get_Customers @AllCostumers", new SqlParameter("AllCostumers", AllCostumers)).ToList();
            return CustomerList;
        }

        public List<CustomersDDL> GetCustomers(CustomersLookup Option)
        {
            var CustomerList = new List<CustomersDDL>();
            switch(Option)
            {
                case CustomersLookup.AllCustomer:                    
                        CustomerList = _context.Database.SqlQuery<CustomersDDL>("sp_Get_Customers @AllCostumers", new SqlParameter("AllCostumers", true)).ToList();
                    break;

                case CustomersLookup.ForInvoices:
                    CustomerList = _context.Database.SqlQuery<CustomersDDL>("sp_Get_Customers @AllCostumers", new SqlParameter("AllCostumers", false)).ToList();
                    break;

                case CustomersLookup.ByInvoicesDue:
                    {
                        var date = DateTime.Now.Date.ToString(DateFormat.INT_DATE, CultureInfo.InvariantCulture);
                        DateTime DateEnd = DateTime.ParseExact(date, DateFormat.INT_DATE, CultureInfo.InvariantCulture);
                        CustomerList = (from i in _context.INVOICES
                                            where i.invoice_duedate < DateEnd
                                                  && i.invoice_status
                                                  && i.is_credit
                                                  //&& i.invoice_balance > 0
                                            select new CustomersDDL
                                            {
                                                RFC = i.CUSTOMERS.customer_rfc,
                                                Name = i.CUSTOMERS.customer_name,
                                                Code = i.customer_code,
                                                Status = i.CUSTOMERS.customer_status
                                            }).Distinct().ToList();
                    }
                    break;

                case CustomersLookup.Filter:
                    CustomerList = _context.Database.SqlQuery<CustomersDDL>("sp_Get_Customers @AllCostumers", new SqlParameter("AllCostumers", false)).ToList();
                    break;

                case CustomersLookup.ByCreditInvoices:
                    CustomerList = (from i in _context.INVOICES
                                    where i.is_credit && i.invoice_status && i.invoice_balance > 0
                                    select new CustomersDDL
                                    {
                                        RFC = i.CUSTOMERS.customer_rfc,
                                        Name = i.CUSTOMERS.customer_name,
                                        Code = i.customer_code,
                                        Status = i.CUSTOMERS.customer_status
                                    }).Distinct().ToList();
                    break;

                case CustomersLookup.ByPaymentInvoices:
                    CustomerList = (from p in _context.PAYMENTS
                                    where p.payment_status
                                    select new CustomersDDL
                                    {
                                        RFC = p.CUSTOMERS.customer_rfc,
                                        Name = p.CUSTOMERS.customer_name,
                                        Code = p.customer_code,
                                        Status = p.CUSTOMERS.customer_status
                                    }).Distinct().ToList();
                    break;

                case CustomersLookup.ByCancelledPayments:
                    CustomerList = (from p in _context.PAYMENTS
                                    where !p.payment_status
                                    select new CustomersDDL
                                    {
                                        RFC = p.CUSTOMERS.customer_rfc,
                                        Name = p.CUSTOMERS.customer_name,
                                        Code = p.customer_code,
                                        Status = p.CUSTOMERS.customer_status
                                    }).Distinct().ToList();
                    break;

                case CustomersLookup.ByQuotation:
                    CustomerList = (from c in _context.CUSTOMERS
                                    join q in _context.QUOTES on c.customer_code equals q.customer_code
                                    select new CustomersDDL
                                    {
                                        RFC = c.customer_rfc,
                                        Name = c.customer_name,
                                        Code = c.customer_code,
                                        Status = c.customer_status
                                    }).Distinct().ToList();
                    break;

                case CustomersLookup.ByTransactions:
                    CustomerList = (from tc in _context.TRANSACTIONS_CUSTOMERS
                                    select new CustomersDDL
                                    {
                                        RFC = tc.CUSTOMERS.customer_rfc,
                                        Name = tc.CUSTOMERS.customer_name,
                                        Code = tc.customer_code,
                                        Status = tc.CUSTOMERS.customer_status
                                    }).Distinct().ToList();
                    break;
            }
            return CustomerList;
        }
    }
}
