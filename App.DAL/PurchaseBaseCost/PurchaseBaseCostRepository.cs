﻿using App.Entities;
using App.Entities.ViewModels.PurchaseBaseCost;
using System.Linq;
using System.Collections.Generic;

namespace App.DAL.PurchaseBaseCost
{
    public class PurchaseBaseCostRepository
    {
        private readonly DFL_SAIEntities _context;

        public PurchaseBaseCostRepository()
        {
            _context = new DFL_SAIEntities();
        }

        public PurchaseBaseCostModel GetPurchaseBaseCostBySupplierPartNumber(int supplier_part_number)
        {
            return _context.PURCHASE_BASE_COST.Where(x => x.supplier_part_number == supplier_part_number && x.status == 1)
                .Select(x => new PurchaseBaseCostModel
                {
                    purchase_base_cost_id = x.base_cost_id.ToString(),
                    supplier_part_number = x.supplier_part_number.ToString(),
                    base_cost = x.base_cost.Value,
                    currency = x.currency,
                    unit_of_price = x.unit_of_price.Value
                }).FirstOrDefault();
        }
        public List<string> GetSupplierCurrency(int supplier_id)
        {
            var r = _context.Database.SqlQuery<string>(@"select distinct(pbc.currency) from ITEM_SUPPLIER its
                join ITEM i on i.part_number = its.part_number
                join PURCHASE_BASE_COST pbc on pbc.supplier_part_number = its.supplier_part_number and pbc.status = 1
                where
                i.extra_items_flag != 1 and i.combo_flag != 1 and i.active_flag = 1 and its.supplier_id = '"+supplier_id+@"'
                and its.supplier_part_number not in
                (select supplier_part_number from ITEM_SUPPLIER_INACTIVE isi where isi.supplier_part_number = its.supplier_part_number)").ToList();
            return r;
        }
    }
}