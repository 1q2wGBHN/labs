﻿using App.Entities;
using App.Entities.ViewModels.CodesPackages;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App.DAL.CodesPackages
{
    public class CodesPackageRepository
    {
        private readonly DFL_SAIEntities _context;

        public CodesPackageRepository()
        {
            _context = new DFL_SAIEntities();
        }

        public List<CodesPackagesModel> GetCodesPackages()
        {
            var model = _context.Database.SqlQuery<CodesPackagesModel>(@"SELECT CP.id_codes_packages 'Id', CP.part_number_origin 'OriginPartNumber', IT.part_description 'OriginPartDescription', IT.unit_size 'UnitSize', PBC.base_cost 'Cost' 
                FROM ( 
                	SELECT *, ROW_NUMBER() OVER (PARTITION BY part_number_origin ORDER BY part_number_origin) AS RN 
	                FROM CODES_PACKAGES) CP 
                INNER JOIN ITEM IT ON CP.part_number_origin = IT.part_number 
                INNER JOIN ( 
                    SELECT base_cost, part_number FROM PURCHASE_BASE_COST 
                    INNER JOIN ITEM_SUPPLIER ON PURCHASE_BASE_COST.supplier_part_number = ITEM_SUPPLIER.supplier_part_number AND PURCHASE_BASE_COST.[status] = 1 
                    GROUP BY base_cost, part_number 
                ) AS PBC ON CP.part_number_origin = PBC.part_number 
                WHERE RN = 1 ").ToList();

            return model;
        }

        public List<CodesPackagesModel> GetCodePackagesByOriginPartNumber(string OriginPartNumber)
        {
            var model = _context.CODES_PACKAGES
                .Join(_context.ITEM, cp => cp.part_number, it => it.part_number, (cp, it) => new { cp, it })
                .Where(x => x.cp.part_number_origin == OriginPartNumber)
                .Select(x => new CodesPackagesModel
                {
                    Id = x.cp.id_codes_packages,
                    PartNumber = x.cp.part_number,
                    PartDescription = x.it.part_description,
                    Quantity = x.cp.quantity,
                    Cost = x.cp.cost
                }).ToList();

            return model;
        }
    }
}