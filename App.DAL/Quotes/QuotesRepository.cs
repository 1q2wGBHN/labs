﻿using App.Entities;
using App.Entities.ViewModels.Quotes;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Core.Objects;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static App.Entities.ViewModels.Common.Constants;

namespace App.DAL.Quotes
{
    public class QuotesRepository
    {
        private DFL_SAIEntities _context;

        public QuotesRepository()
        {
            _context = new DFL_SAIEntities();
        }

        public bool CreateQuote(QUOTES Item)
        {
            try
            {
                _context.QUOTES.Add(Item);
                _context.SaveChanges();

                //Update Internal Number and Number in DB from Reference Number Table.
                ObjectParameter Reference = new ObjectParameter("Reference", typeof(string));
                _context.sp_Get_Reference_Number("QUOTES", "ALL", Reference);

                return true;
            }
            catch (Exception ex)
            {
                string msg = ex.Message;
                return false;
            }
        }

        public List<QuoteReport> QuoteReport(string DateStart, string DateEnd, string CustomerCode)
        {
            DateTime StartDate = DateTime.ParseExact(DateStart, DateFormat.INT_DATE, CultureInfo.InvariantCulture);
            DateTime EndDate = DateTime.ParseExact(DateEnd, DateFormat.INT_DATE, CultureInfo.InvariantCulture);
            var ReportList = new List<QuoteReport>();

            ReportList = (from q in _context.QUOTES
                          where DbFunctions.TruncateTime(q.quote_date) >= StartDate
                              && DbFunctions.TruncateTime(q.quote_date) <= EndDate
                              && (string.IsNullOrEmpty(CustomerCode) || q.customer_code == CustomerCode)
                          select new
                          {
                              q.quote_serie,
                              q.quote_no,
                              q.quote_date,
                              q.CUSTOMERS.customer_name,
                              q.quote_total,
                              q.quote_tax
                          }).AsEnumerable().Select(c => new QuoteReport
                          {
                              Id = string.Format("{0}{1}", c.quote_serie, c.quote_no),
                              Customer = c.customer_name,
                              Total = c.quote_total,
                              Tax = c.quote_tax,
                              Date = c.quote_date.ToString(DateFormat.INT_DATE),
                              Serie = c.quote_serie,
                              Number = c.quote_no
                          }).ToList();

            return ReportList;
        }

        public QUOTES GetQuotation(long Number, string Serie)
        {
            var Quote = new QUOTES();
            Quote = _context.QUOTES.Where(c => c.quote_no == Number && c.quote_serie == Serie).FirstOrDefault();
            return Quote;
        }
    }
}
