﻿using App.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App.DAL.Quotes
{
    public class QuotesDetailsRepository
    {
        private DFL_SAIEntities _context;

        public QuotesDetailsRepository()
        {
            _context = new DFL_SAIEntities();
        }

        #region CreateQuotesDetail
        public bool CreateQuoteDetail(List<QUOTES_DETAILS> QuotesDetails)
        {
            DFL_SAIEntities context = new DFL_SAIEntities();
            try
            {
                context.Configuration.AutoDetectChangesEnabled = false;
                int cont = 0;
                foreach (var QuoteDetailItem in QuotesDetails)
                {
                    ++cont;
                    context = AddToContext(context, QuoteDetailItem, cont, 1000, true);
                }
                context.SaveChanges();
                return true;
            }
            catch (Exception ex)
            {
                string msg = ex.Message;
                return false;
            }
            finally
            {
                if (context != null)
                {
                    context.Configuration.AutoDetectChangesEnabled = true;
                    context.Dispose();
                }
            }
        }

        private DFL_SAIEntities AddToContext(DFL_SAIEntities context, QUOTES_DETAILS entity, int count, int commitCount, bool recreateContext)
        {
            context.Set<QUOTES_DETAILS>().Add(entity);

            if (count % commitCount == 0)
            {
                context.SaveChanges();
                if (recreateContext)
                {
                    context.Dispose();
                    context = new DFL_SAIEntities();
                    context.Configuration.AutoDetectChangesEnabled = false;
                }
            }

            return context;
        }
        #endregion        
    }
}
