﻿using App.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App.DAL.CancelInovicesSales
{
    public class CancelInovicesSalesRepository
    {
        private DFL_SAIEntities _context;

        public CancelInovicesSalesRepository()
        {
            _context = new DFL_SAIEntities();
        }

        public bool CreateRercord(CANCEL_INVOICES_SALES Item)
        {
            try
            {
                _context.CANCEL_INVOICES_SALES.Add(Item);
                _context.SaveChanges();
                return true;
            }
            catch (Exception ex)
            {
                string msg = ex.Message;
                return false;
            }
        }        

        public bool HasSaleCancelInovice(long SaleId)
        {
            return _context.CANCEL_INVOICES_SALES.Any(c => c.sale_id == SaleId);
        }
    }
}
