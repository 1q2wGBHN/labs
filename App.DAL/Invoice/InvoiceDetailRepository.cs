﻿using App.Entities;
using App.Entities.ViewModels;
using App.Entities.ViewModels.Common;
using App.Entities.ViewModels.Invoices;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Globalization;
using System.Linq;
using static App.Entities.ViewModels.Common.Constants;

namespace App.DAL.Invoice
{
    public class InvoiceDetailRepository
    {
        private DFL_SAIEntities _context;

        public InvoiceDetailRepository()
        {
            _context = new DFL_SAIEntities();
        }

        #region CreateInvoiceDetail
        public bool CreateInvoiceDetail(List<InvoiceDetailsInsert> InvoiceDetailsList)
        {
            DFL_SAIEntities context = new DFL_SAIEntities();
            try
            {
                context.Configuration.AutoDetectChangesEnabled = false;
                //var list = new List<INVOICE_DETAIL>();
                //foreach (var InvoiceDetailItem in InvoiceDetailsList)
                //{                    
                //    list.Add(InvoiceDetailItem.INVOICE_DETAIL);
                //}
                //context.INVOICE_DETAIL.AddRange(list);
                //context.SaveChanges();
                for (int i = 0; i < InvoiceDetailsList.Count; i++)
                {
                    context.INVOICE_DETAIL.Add(InvoiceDetailsList[i].INVOICE_DETAIL);

                    if (i % 30 == 0)
                        context.SaveChanges();
                }
                context.SaveChanges();
                return true;
            }
            catch (Exception ex)
            {
                //EventLogEntry bla = new EventLogEntry();
                string msg = ex.Message;
                EventLog appLog = new EventLog();
                appLog.Source = "ERP InvoiceDetails";
                appLog.WriteEntry(msg);
                return false;
            }
            finally
            {
                if (context != null)
                {
                    context.Configuration.AutoDetectChangesEnabled = true;
                    context.Dispose();
                }
            }
        }

        //private DFL_SAIEntities AddToContext(DFL_SAIEntities context, INVOICE_DETAIL entity, int count, int commitCount, bool recreateContext)
        //{
        //    context.Set<INVOICE_DETAIL>().Add(entity);

        //    if (count % commitCount == 0)
        //    {
        //        context.SaveChanges();
        //        if (recreateContext)
        //        {
        //            context.Dispose();
        //            context = new DFL_SAIEntities();
        //            context.Configuration.AutoDetectChangesEnabled = false;
        //        }
        //    }

        //    return context;
        //}
        #endregion        

        public INVOICE_DETAIL GetInvoiceDetailsById(long InvoiceDetailId)
        {
            return _context.INVOICE_DETAIL.Find(InvoiceDetailId);
        }

        public List<InvoiceProductsDetails> InvoiceProductsList(long? InvoiceNumber, long? SaleNumber)
        {
            //TODO: Find a better solution for this.
            var InvoiceDetails = new List<InvoiceProductsDetails>();
            if (InvoiceNumber.HasValue)
            {
                bool IsDaily = _context.INVOICES.Where(c => c.invoice_no == InvoiceNumber.Value).First().is_daily;
                if (IsDaily)
                {
                    InvoiceDetails = (from indt in _context.INVOICE_DETAIL
                                      join it in _context.ITEM on indt.part_number equals it.part_number
                                      where indt.invoice_no == InvoiceNumber.Value
                                      select new InvoiceProductsDetails
                                      {
                                          DetailId = indt.detail_id,
                                          ItemNumber = indt.part_number,
                                          ItemName = indt.description,
                                          Quantity = indt.detail_qty,
                                          ItemPrice = indt.detail_qty > 0 ? indt.detail_price / indt.detail_qty : indt.detail_qty,
                                          TotalPrice = indt.detail_price,
                                          ItemPriceNoTax = indt.detail_qty > 0 ? indt.detail_price / indt.detail_qty : indt.detail_qty,
                                          IsWeightAvailable = (it.weight_flag.HasValue && it.weight_flag.Value) ? 1 : 0,//This is for control in datatable, render a checkbox on boolean type.                                                                                                
                                          QuantityAvailable = indt.detail_qty_available,
                                          IEPSCode = !string.IsNullOrEmpty(it.part_ieps) && it.part_ieps != TaxCodes.NOT_AVAILABLE ? it.part_ieps : string.Empty,
                                          IVACode = !string.IsNullOrEmpty(it.part_iva_sale) && it.part_iva_sale != TaxCodes.NOT_AVAILABLE ? it.part_iva_sale : string.Empty,
                                          IVAValue = !string.IsNullOrEmpty(it.part_iva_sale) && it.part_iva_sale != TaxCodes.NOT_AVAILABLE ? it.MA_TAX_IVA_SALE.tax_value.Value : 0,
                                          IEPSValue = !string.IsNullOrEmpty(it.part_ieps) && it.part_ieps != TaxCodes.NOT_AVAILABLE ? it.MA_TAX_IEPS.tax_value.Value : 0
                                      }).ToList();
                }
                else
                {
                    InvoiceDetails = (from indt in _context.INVOICE_DETAIL
                                      join it in _context.ITEM on indt.part_number equals it.part_number
                                      where indt.invoice_no == InvoiceNumber.Value
                                      select new InvoiceProductsDetails
                                      {
                                          DetailId = indt.detail_id,
                                          ItemNumber = indt.part_number,
                                          ItemName = indt.description,
                                          Quantity = indt.detail_qty,
                                          ItemPrice = indt.detail_qty > 0 ? indt.detail_price / indt.detail_qty : indt.detail_qty,
                                          TotalPrice = indt.detail_price,
                                          ItemPriceNoTax = indt.detail_qty > 0 ? indt.detail_price / indt.detail_qty : indt.detail_qty,
                                          IsWeightAvailable = (it.weight_flag.HasValue && it.weight_flag.Value) ? 1 : 0,//This is for control in datatable, render a checkbox on boolean type.                                                                                                
                                          QuantityAvailable = indt.detail_qty_available
                                      }).ToList();
                }
            }
            else
            {
                InvoiceDetails = (from indt in _context.INVOICE_DETAIL
                                  join it in _context.ITEM on indt.part_number equals it.part_number
                                  join s in _context.DFLPOS_SALES on new { InvoSerie = indt.invoice_serie, InvoNumber = indt.invoice_no } equals new { InvoSerie = s.invoice_serie, InvoNumber = s.invoice_no.Value }
                                  where s.sale_id == SaleNumber.Value
                                  select new InvoiceProductsDetails
                                  {
                                      DetailId = indt.detail_id,
                                      ItemNumber = indt.part_number,
                                      ItemName = indt.description,
                                      Quantity = indt.detail_qty,
                                      ItemPrice = indt.detail_qty > 0 ? indt.detail_price / indt.detail_qty : indt.detail_qty,
                                      TotalPrice = indt.detail_price,
                                      ItemPriceNoTax = indt.detail_qty > 0 ? indt.detail_price / indt.detail_qty : indt.detail_qty,
                                      IsWeightAvailable = (it.weight_flag.HasValue && it.weight_flag.Value) ? 1 : 0,//This is for control in datatable, render a checkbox on boolean type.                                                                                                
                                      QuantityAvailable = indt.detail_qty_available,
                                      IEPSCode = !string.IsNullOrEmpty(it.part_ieps) && it.part_ieps != TaxCodes.NOT_AVAILABLE ? it.part_ieps : string.Empty,
                                      IVACode = !string.IsNullOrEmpty(it.part_iva_sale) && it.part_iva_sale != TaxCodes.NOT_AVAILABLE ? it.part_iva_sale : string.Empty,
                                      IVAValue = !string.IsNullOrEmpty(it.part_iva_sale) && it.part_iva_sale != TaxCodes.NOT_AVAILABLE ? it.MA_TAX_IVA_SALE.tax_value.Value : 0,
                                      IEPSValue = !string.IsNullOrEmpty(it.part_ieps) && it.part_ieps != TaxCodes.NOT_AVAILABLE ? it.MA_TAX_IEPS.tax_value.Value : 0
                                  }).ToList();
            }

            if (InvoiceDetails.Count > 0)
            {
                var detailsId = InvoiceDetails.Select(c => c.DetailId).ToList();
                var TaxList = (from idt in _context.INVOICE_DETAIL_TAX
                               where detailsId.Contains(idt.detail_id)
                               select idt).ToList();
                foreach (var item in InvoiceDetails)
                {
                    //item.ItemPriceNoTax = Math.Round(item.ItemPriceNoTax, 2);
                    item.QuantityAvailableBon = item.QuantityAvailable.ToString();
                    item.QuantityBon = item.Quantity.ToString();

                    if (TaxList.Where(c => c.detail_id == item.DetailId && c.tax_code.Contains(TaxTypes.TAX)).Any())
                    {
                        var TaxItem = TaxList.Where(c => c.detail_id == item.DetailId && c.tax_code.Contains(TaxTypes.TAX)).First();
                        decimal Tax = TaxItem.tax_amount / item.Quantity;//Math.Round(TaxItem.tax_amount / item.Quantity, 3);
                        item.ItemPrice = item.ItemPriceNoTax + Tax;//Math.Round(item.ItemPriceNoTax + Tax, 2);
                        item.TotalPrice = item.ItemPrice * item.Quantity;// Math.Round(item.ItemPrice * item.Quantity, 2);// + TaxList.Where(c => c.detail_id == item.DetailId).Select(x => x.tax_amount).Sum();
                        item.ItemTax = Tax;// Math.Round(Tax, 2);
                        item.IVACode = TaxItem.tax_code;
                        item.IVAValue = TaxItem.MA_TAX.tax_value.Value;
                    }

                    if (TaxList.Where(c => c.detail_id == item.DetailId && c.tax_code.Contains(TaxTypes.IEPS)).Any())
                    {
                        var IEPSItem = TaxList.Where(c => c.detail_id == item.DetailId && c.tax_code.Contains(TaxTypes.IEPS)).First();
                        decimal IEPS = Math.Round(IEPSItem.tax_amount / item.Quantity, 3);
                        item.ItemIEPS = Math.Round(IEPS, 2);
                        item.IEPSCode = IEPSItem.tax_code;
                        item.IEPSValue = IEPSItem.MA_TAX.tax_value.Value;
                    }
                }

                //InvoiceDetails.ForEach(c => { c.QuantityAvailableBon = c.QuantityAvailable.ToString(); c.QuantityBon = c.Quantity.ToString(); });

                return InvoiceDetails;
            }

            return new List<InvoiceProductsDetails>();
        }

        public bool Update(INVOICE_DETAIL InvoiceDetail)
        {
            var InvoiceDetailEdit = _context.INVOICE_DETAIL.Find(InvoiceDetail.detail_id);
            if (InvoiceDetailEdit != null)
            {
                _context.Entry(InvoiceDetailEdit).State = System.Data.Entity.EntityState.Detached;
                _context.Entry(InvoiceDetail).State = System.Data.Entity.EntityState.Modified;
                _context.SaveChanges();
                return true;
            }
            return false;
        }

        //Function to check if the Invoice has any item available for credit note.
        public bool HasInvoiceItemsAvailable(long InvoiceNumber)
        {
            return _context.INVOICE_DETAIL.Where(c => c.detail_qty_available > 0 && c.invoice_no == InvoiceNumber).Any();
        }

        public List<InvoiceDetailsInsert> GetInvoiceDetailsFromSalesDetails(string InvoSerie, long InvoNumber, long SaleNumber, bool isCurrencyUSD)
        {
            decimal Iva = 1.08m;
            var saleRate = isCurrencyUSD ? _context.DFLPOS_SALES_DETAIL.First(s => s.sale_id == SaleNumber).currency_rate : 0;
            var InvoiceDetailsList = new List<InvoiceDetailsInsert>();
            InvoiceDetailsList = (from sd in _context.DFLPOS_SALES_DETAIL
                                  join s in _context.DFLPOS_SALES on sd.sale_id equals s.sale_id
                                  join combHe in _context.COMBO_HEADER on sd.part_number equals combHe.part_number_combo into LeftJoinComboHeader
                                  from combH in LeftJoinComboHeader.DefaultIfEmpty()
                                  join combDe in _context.COMBO_DETAIL on combH.part_number_combo equals combDe.part_number_combo into LeftJoinComboDetail
                                  from combD in LeftJoinComboDetail.Where(combD => combD.active_status.HasValue && combD.active_status.Value).DefaultIfEmpty()
                                  where s.sale_id == SaleNumber
                                        && sd.quantity > 0
                                  select new
                                  {
                                      InvoiceNumber = InvoNumber,
                                      InvoiceSerie = InvoSerie,
                                      ItemPartNumber = string.IsNullOrEmpty(combD.part_number) ? sd.part_number : combD.part_number,
                                      ItemDescription = string.IsNullOrEmpty(combD.part_number) ? sd.ITEM.part_description : combD.ITEM.part_description,
                                      ItemPartPercentage = string.IsNullOrEmpty(combD.part_number) ? sd.part_tax_percentage : (combD.iva.Value * 100) / combD.total_price_sale.Value,
                                      DetailQty = string.IsNullOrEmpty(combD.part_number) ? sd.quantity : combD.quantity.Value * sd.quantity,
                                      QtyAvailable = string.IsNullOrEmpty(combD.part_number) ? sd.quantity : combD.quantity.Value * sd.quantity,
                                      DetailPrice = string.IsNullOrEmpty(combD.part_number) ? (isCurrencyUSD ? sd.part_price / saleRate : sd.part_price) : combD.iva > 0 ? (combD.total_price_sale.Value * sd.quantity / Iva) : combD.total_price_sale.Value * sd.quantity,
                                      ProductCode = string.IsNullOrEmpty(combD.part_number) ? (string.IsNullOrEmpty(sd.ITEM.part_number_sat) ? null : sd.ITEM.part_number_sat) : combD.ITEM.part_number_sat,
                                      UnitCode = string.IsNullOrEmpty(combD.part_number) ? (string.IsNullOrEmpty(sd.ITEM.unit_sat) ? null : sd.ITEM.unit_sat) : combD.ITEM.unit_sat,
                                      DetailId = string.IsNullOrEmpty(combD.part_number) ? sd.detail_id : 0,
                                      PartNumberCombo = !string.IsNullOrEmpty(combD.part_number) ? combD.part_number_combo : sd.part_number
                                  }).AsEnumerable().Select(c => new InvoiceDetailsInsert
                                  {
                                      InvoiceNumber = c.InvoiceNumber,
                                      InvoiceSerie = c.InvoiceSerie,
                                      ItemPartNumber = c.ItemPartNumber,
                                      ItemDescription = c.ItemDescription,
                                      ItemPartPercentage = c.ItemPartPercentage,
                                      DetailQty = c.DetailQty,
                                      QtyAvailable = c.QtyAvailable,
                                      DetailPrice = Math.Round(c.DetailPrice, 2),
                                      ProductCode = c.ProductCode,
                                      UnitCode = c.UnitCode,
                                      SaleDetailId = c.DetailId == 0 ? long.Parse(c.PartNumberCombo) : c.DetailId
                                  }).ToList();

            if (InvoiceDetailsList.Count > 0)
                //InvoiceDetailsList.ForEach(c => c.DetailPrice = Math.Round(c.DetailPrice, 2));
                return InvoiceDetailsList;
            else
                return new List<InvoiceDetailsInsert>();
        }

        public List<InvoiceDetailsInsert> InvoiceGlobal(string InvoSerie, long InvoNumber, decimal InvoTotal)
        {
            var InvoiceDetailsList = new List<InvoiceDetailsInsert>();
            var Insert = new InvoiceDetailsInsert();

            Insert.InvoiceNumber = InvoNumber;
            Insert.InvoiceSerie = InvoSerie;
            Insert.ItemPartNumber = "GENERAL";
            Insert.ItemDescription = "Venta Diaria";
            Insert.DetailQty = 1;
            Insert.QtyAvailable = 1;
            Insert.DetailPrice = InvoTotal;
            Insert.ProductCode = "01010101";
            Insert.UnitCode = "ACT";

            InvoiceDetailsList.Add(Insert);

            return InvoiceDetailsList;
        }

        public List<InvoiceDetailsInsert> InvoiceDetailsSubstitution(string Serie, long InvoiceNumber, long NewInvoiceNumber)
        {
            var InvoiceDetailsList = new List<InvoiceDetailsInsert>();
            InvoiceDetailsList = (from id in _context.INVOICE_DETAIL
                                  where id.invoice_serie == Serie && id.invoice_no == InvoiceNumber
                                  select new InvoiceDetailsInsert
                                  {
                                      InvoiceNumber = NewInvoiceNumber,
                                      InvoiceSerie = Serie,
                                      ItemPartNumber = id.part_number,
                                      ItemDescription = id.description,
                                      DetailQty = id.detail_qty,
                                      QtyAvailable = id.detail_qty_available,
                                      DetailPrice = id.detail_price,
                                      DetailDiscount = id.detail_discount,
                                      ImportDoc = id.import_doc,
                                      ProductCode = id.sat_prodct_code,
                                      UnitCode = id.sat_unt_code,
                                      PreviousDetailId = id.detail_id
                                  }).ToList();

            return InvoiceDetailsList;
        }

        public List<DetailTax> InvoiceDetailsWithTax(string InvoSerie, long InvoNumber, long SaleNumber, bool isCurrencyUSD)
        {
            var saleRate = isCurrencyUSD ? _context.DFLPOS_SALES_DETAIL.First(s => s.sale_id == SaleNumber).currency_rate : 0;
            var List = (from id in _context.INVOICE_DETAIL
                        join i in _context.ITEM on id.part_number equals i.part_number
                        join sd in _context.DFLPOS_SALES_DETAIL on new { id.part_number, SaleDetailId = id.sale_detail_id.Value } equals new { sd.part_number, SaleDetailId = sd.detail_id }
                        join maIEPS in _context.MA_TAX on i.part_ieps equals maIEPS.tax_code into LeftJoinIEPS
                        from IEPS in LeftJoinIEPS.DefaultIfEmpty()
                        join maIVA in _context.MA_TAX on i.part_iva_sale equals maIVA.tax_code into LeftJoinIVA
                        from IVA in LeftJoinIVA.DefaultIfEmpty()
                        where id.invoice_no == InvoNumber
                              && sd.sale_id == SaleNumber
                                && !string.IsNullOrEmpty(i.part_iva_sale) && i.part_iva_sale != TaxCodes.EXE_0
                                && ((!string.IsNullOrEmpty(i.part_ieps) && i.part_ieps != TaxCodes.NOT_AVAILABLE)
                                    || (!string.IsNullOrEmpty(i.part_iva_sale) && i.part_iva_sale != TaxCodes.NOT_AVAILABLE) || sd.part_tax > 0)
                        select new
                        {
                            DetailId = id.detail_id,
                            IEPSCode = IEPS.tax_code,
                            IEPSValue = IEPS.tax_value,
                            IVACode = IVA.tax_code,
                            IVAValue = sd.part_tax_percentage.Value,
                            ItemPrice = id.detail_price,
                            TaxAmount = sd.part_tax
                        }).AsEnumerable().Select(c => new DetailTax
                        {
                            DetailId = c.DetailId,
                            IEPSCode = c.IEPSCode,
                            IEPSValue = c.IEPSValue,
                            IVACode = c.IVACode,
                            IVAValue = c.IVAValue,
                            ItemPrice = c.ItemPrice,
                            TaxAmount = isCurrencyUSD ? Math.Round(c.TaxAmount / saleRate, 2) : c.TaxAmount
                        }).Distinct().ToList();

            var ComboList = (from id in _context.INVOICE_DETAIL
                             join sd in _context.DFLPOS_SALES_DETAIL on id.sale_detail_id.ToString() equals sd.part_number
                             join combDe in _context.COMBO_DETAIL on new { V = id.sale_detail_id.ToString(), id.part_number } equals new { V = combDe.part_number_combo, combDe.part_number }
                             where id.invoice_no == InvoNumber
                                   && combDe.ITEM.part_iva_sale != TaxCodes.EXE_0
                                   && sd.sale_id == SaleNumber
                             select new
                             {
                                 id.detail_id,
                                 id.detail_price,
                                 IEPSCode = combDe.ITEM.part_ieps,
                                 IEPSValue = combDe.ITEM.MA_TAX_IEPS.tax_value,
                                 IVACode = combDe.iva > 0 ? TaxCodes.TAX_8 : combDe.ITEM.part_iva_sale,
                                 IVAValue = combDe.iva > 0 ? sd.part_tax_percentage : combDe.ITEM.MA_TAX_IVA_SALE.tax_value,
                                 ItemPrice = combDe.iva > 0 ? (isCurrencyUSD ? ((combDe.total_price_sale * sd.quantity) / 1.08m) / saleRate : (combDe.total_price_sale * sd.quantity) / 1.08m) : combDe.total_price_sale * sd.quantity,
                                 combIvaAmount = combDe.iva > 0 ? id.detail_price * 0.08m : 0,
                             }).AsEnumerable().Select(c => new DetailTax
                             {
                                 DetailId = c.detail_id,
                                 IEPSCode = c.IEPSCode,
                                 IEPSValue = c.IEPSValue,
                                 IVACode = c.IVACode,
                                 IVAValue = c.IVAValue,
                                 ItemPrice = c.ItemPrice.Value,
                                 TaxAmount = Math.Round(c.combIvaAmount, 2)
                             }).ToList();

            if (ComboList.Count() > 0)
                List.AddRange(ComboList);

            if (List.Count > 0)
                return List;
            else
                return new List<DetailTax>();
        }

        public List<DetailTax> SpecialInvoiceDetailsTax(string Serie, long Number, List<CFDIConcept> Concepts)
        {

            var InvoiceDetails = _context.INVOICE_DETAIL.Where(c => c.invoice_no == Number && c.invoice_serie == Serie).ToList();

            var List = (from id in InvoiceDetails
                        join c in Concepts on id.part_number equals c.ItemSatCode
                        join maIEPS in _context.MA_TAX on c.IEPSCode equals maIEPS.tax_code into LeftJoinIEPS
                        from IEPS in LeftJoinIEPS.DefaultIfEmpty()
                        join maIVA in _context.MA_TAX on c.TaxCode equals maIVA.tax_code into LeftJoinIVA
                        from IVA in LeftJoinIVA.DefaultIfEmpty()
                        select new DetailTax
                        {
                            DetailId = id.detail_id,
                            IEPSCode = IEPS.tax_code,
                            IEPSValue = IEPS.tax_value,
                            IVACode = IVA.tax_code,
                            IVAValue = IVA.tax_value,
                            ItemPrice = id.detail_price,
                            TaxAmount = Math.Round(id.detail_price * (IVA.tax_value.Value / 100), 2)
                        }).ToList();

            if (List.Count > 0)
                return List;
            else
                return new List<DetailTax>();
        }

        public INVOICE_DETAIL GetDetailByInvoice(long Number, string Serie)
        {
            return _context.INVOICE_DETAIL.FirstOrDefault(c => c.invoice_no == Number && c.invoice_serie == Serie);
        }

        public List<INVOICE_DETAIL> GetDetailsByInvoice(long Number, string Serie)
        {
            return _context.INVOICE_DETAIL.Where(c => c.invoice_no == Number && c.invoice_serie == Serie).ToList(); ;
        }

        public bool DeleteInvoiceDetails(long Number, string Serie)
        {
            try
            {
                _context.Database.ExecuteSqlCommand("DELETE FROM INVOICE_DETAIL WHERE invoice_no = @Number AND invoice_serie = @Serie",
                     new SqlParameter("Number", Number),
                     new SqlParameter("Serie", Serie));

                return true;
            }
            catch (Exception ex)
            {
                string msg = ex.Message;
                return false;
            }
        }

        public List<INVOICE_DETAIL> GetDetailsWithoutTaxByInvoice(long InvoiceNo, string Serie)
        {
            var DetailsList = (from id in _context.INVOICE_DETAIL
                               join i in _context.ITEM on id.part_number equals i.part_number
                               where !(from id2 in _context.INVOICE_DETAIL_TAX select id2.detail_id).Contains(id.detail_id)
                                     && id.invoice_no == InvoiceNo
                                     && id.invoice_serie == Serie
                               //&& i.weight_flag.Value
                               select id).ToList();

            return DetailsList;
        }
    }
}
