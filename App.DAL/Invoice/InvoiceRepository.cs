﻿using App.DAL.DigitalTaxStamp;
using App.DAL.Lookups;
using App.Entities;
using App.Entities.ViewModels.Common;
using App.Entities.ViewModels.CreditNotes;
using App.Entities.ViewModels.Invoices;
using App.Entities.ViewModels.Sales;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.Entity;
using System.Data.Entity.Core.Objects;
using System.Data.SqlClient;
using System.Globalization;
using System.Linq;
using System.Linq.Expressions;
using static App.Entities.ViewModels.Common.Constants;

namespace App.DAL.Invoice
{
    public class InvoiceRepository
    {
        private DFL_SAIEntities _context;
        private DigitalTaxStampRepository _DigitalTaxStampRepository;
        private InvoiceDetailTaxRepository _InvoiceDetailTaxRepository;
        private InvoiceDetailRepository _InvoiceDetailRepository;
        private PaymentMethodRepository _PaymentMethodRepository;

        public InvoiceRepository()
        {
            _context = new DFL_SAIEntities();
            _DigitalTaxStampRepository = new DigitalTaxStampRepository();
            _InvoiceDetailTaxRepository = new InvoiceDetailTaxRepository();
            _InvoiceDetailRepository = new InvoiceDetailRepository();
            _PaymentMethodRepository = new PaymentMethodRepository();
    }

        public bool CreateInvoice(INVOICES InvoiceData)
        {
            try
            {
                _context.INVOICES.Add(InvoiceData);
                _context.SaveChanges();

                //Update Internal Number and Number in DB from Reference Number Table.
                ObjectParameter Reference = new ObjectParameter("Reference", typeof(string));
                _context.sp_Get_Reference_Number("INVOICE", "ALL", Reference);

                return true;
            }
            catch (Exception ex)
            {
                string msg = ex.Message;
                return false;
            }
        }

        public bool InvoiceExist(long InvoiceNumber)
        {
            var exist = _context.INVOICES.Where(c => c.invoice_no == InvoiceNumber).FirstOrDefault();
            if (exist != null)
                return true;

            return false;
        }

        public bool IsInvoiceActive(long InvoiceNumber)
        {
            bool IsActive = _context.INVOICES.FirstOrDefault(c => c.invoice_no == InvoiceNumber).invoice_status;
            return IsActive;
        }

        public bool IsInvoiceOverDue(long InvoiceNumber)
        {
            var Invoice = _context.INVOICES.FirstOrDefault(c => c.invoice_no == InvoiceNumber);
            if (Invoice != null && Invoice.invoice_duedate < DateTime.Today.Date)
                return true;
            return false;
        }

        public bool IsInvoiceDaily(long InvoiceNumber)
        {
            return _context.INVOICES.FirstOrDefault(c => c.invoice_no == InvoiceNumber).is_daily;
        }

        public bool InvoiceHasCreditNotesOrBonifications(long InvoiceNumber)
        {
            var Something = _context.CREDIT_NOTES.Where(c => c.invoice_no == InvoiceNumber && c.cn_status).ToList();
            if (Something.Any())
                return true;

            return false;
        }

        public bool InvoiceHasPayments(long InvoiceNumber)
        {
            var Invoice = GetInvoiceByNumber(InvoiceNumber);
            if (Invoice.is_credit)
            {
                var Payment = (from p in _context.PAYMENTS
                               join pd in _context.PAYMENTS_DETAIL on new { p.payment_no, p.payment_serie } equals new { pd.payment_no, pd.payment_serie }
                               where p.payment_status && pd.invoice_no == InvoiceNumber
                               select p).ToList();
                if (Payment.Any())
                    return true;
            }
            return false;
        }

        public InvoiceDetails InvoiceDetailsByDate(string DateValue)
        {

            DateTime Date = DateTime.ParseExact(DateValue, DateFormat.INT_DATE, CultureInfo.InvariantCulture);
            InvoiceDetails Invoice = new InvoiceDetails();
            var Item = _context.INVOICES.Where(c => c.invoice_date == Date && c.customer_code == GenericCustomer.CODE && c.invoice_status).FirstOrDefault();
            Invoice = (from i in _context.INVOICES
                       join c in _context.CUSTOMERS on i.customer_code equals c.customer_code
                       where i.invoice_no == Item.invoice_no
                       select new InvoiceDetails
                       {
                           InvoiceNumber = i.invoice_no,
                           ClientNumber = c.customer_code,
                           ClientName = c.customer_name,
                           PaymentMethod = i.SAT_PAYMENT_METHOD_CODE.sat_pm_description,
                           Currency = i.cur_code,
                           SubTotal = i.invoice_total - i.invoice_tax,
                           Tax = i.invoice_tax,
                           Total = i.invoice_total,
                           InvoiceBalance = i.invoice_balance.HasValue ? i.invoice_balance.Value : 0,
                           IsInvoiceDaily = i.is_daily,
                           InvoiceSerie = i.invoice_serie,
                           Serie = i.invoice_serie,
                           Number = i.invoice_no
                       }).FirstOrDefault();
            if (Item != null)
            {
                Invoice.Date = Item.invoice_date.ToString(DateFormat.INT_DATE);

                var Sale = _context.DFLPOS_SALES.Where(s => s.invoice_no == Item.invoice_no).ToList();
                if (Sale.Count > 1)
                    Invoice.Tickets = "Factura de fin de día";
                else if (Sale.Count == 1)
                    Invoice.Tickets = Sale.First().sale_id.ToString();
                else
                    Invoice.Tickets = string.Empty;
            }




            return Invoice;
        }

        public InvoiceDetails InvoiceDetails(long InvoiceNumber)
        {
            InvoiceDetails Invoice = new InvoiceDetails();
            var Item = _context.INVOICES.Where(c => c.invoice_no == InvoiceNumber).FirstOrDefault();
            Invoice = (from i in _context.INVOICES
                       join c in _context.CUSTOMERS on i.customer_code equals c.customer_code
                       where i.invoice_no == InvoiceNumber
                       select new InvoiceDetails
                       {
                           InvoiceNumber = i.invoice_no,
                           //Date = i.invoice_date
                           ClientNumber = c.customer_code,
                           ClientName = c.customer_name,
                           PaymentMethod = i.SAT_PAYMENT_METHOD_CODE.sat_pm_description,
                           Currency = i.cur_code,
                           SubTotal = i.invoice_total - i.invoice_tax,
                           Tax = i.invoice_tax,
                           Total = i.invoice_total,
                           InvoiceBalance = i.invoice_balance ?? 0,
                           IsInvoiceDaily = i.is_daily,
                           InvoiceSerie = i.invoice_serie,
                           Serie = i.invoice_serie,
                           Number = i.invoice_no
                       }).FirstOrDefault();

            if (Item != null)
            {
                Invoice.Date = Item.invoice_date.ToString(DateFormat.INT_DATE);

                var Sale = _context.DFLPOS_SALES.Where(s => s.invoice_no == InvoiceNumber).ToList();
                if (Sale.Count > 1)
                    Invoice.Tickets = "Factura de fin de día";
                else if (Sale.Count == 1)
                    Invoice.Tickets = Sale.First().sale_id.ToString();
                else
                    Invoice.Tickets = string.Empty;
            }

            return Invoice;
        }

        public INVOICES GetInvoiceByNumber(long InvoiceNumber)
        {
            return _context.INVOICES.Where(c => c.invoice_no == InvoiceNumber || c.cfdi_id == InvoiceNumber).FirstOrDefault();
        }

        public decimal InvoiceTotal(long InvoiceNumber)
        {
            return _context.INVOICES.FirstOrDefault(c => c.invoice_no == InvoiceNumber || c.cfdi_id == InvoiceNumber).invoice_total;
        }

        public bool UpdateInvoice(INVOICES Invoice, SYS_LOG SysLog)
        {
            var InvoiceEdit = _context.INVOICES.Where(i => i.invoice_no == Invoice.invoice_no).FirstOrDefault();
            if (InvoiceEdit != null)
            {
                _context.Entry(InvoiceEdit).State = EntityState.Detached;
                _context.Entry(Invoice).State = EntityState.Modified;
                _context.SaveChanges();
                if (SysLog != null)
                {
                    _context.SYS_LOG.Add(SysLog);
                    _context.SaveChanges();
                }
                return true;
            }
            return false;
        }

        public List<INVOICES> GetAllInvoiceHeaders(Expression<Func<INVOICES, bool>> predicate)
        {
            return _context.INVOICES.Where(predicate).OrderBy(o => o.invoice_date).ToList();
        }

        public SendDocumentModel GetSendDocumentByNumberInvoice(long InvoiceNumber)
        {
            var response = new SendDocumentModel();
            response = ((from inv in _context.INVOICES
                         join dts in _context.DIGITAL_TAX_STAMP on new { cfdi = inv.cfdi_id.Value, serie = inv.invoice_serie } equals new { cfdi = dts.cfdi_id, serie = dts.cfdi_serie }
                         where inv.cfdi_id == InvoiceNumber && dts.sat_document_code == VoucherType.INCOMES

                         select new SendDocumentModel
                         {
                             Customer = inv.CUSTOMERS.customer_name,
                             EmailCustomerSend = inv.CUSTOMERS.customer_email,
                             FolioFiscal = dts.UUID.Substring(dts.UUID.Length - 10, dts.UUID.Length),
                             Date = inv.invoice_date.ToString(),
                             EmailCustomer = inv.CUSTOMERS.customer_email,
                             Number = inv.cfdi_id.ToString(),
                             Invoice = inv.invoice_serie + inv.cfdi_id,
                             Serie = inv.invoice_serie,
                             Total = inv.invoice_total.ToString(),
                             DocumentType = dts.sat_document_code
                         })).FirstOrDefault();

            return response;
        }

        //INVOICES REPORTS PATRICIO 
        public InvoiceIndex GetInvoicesByModel(InvoiceIndex Modelo) //recibe el modelo index y lo filtra en base a los valores contenidos en todos los campos
        {
            var listInvoices = GetInvoicesListByFilterReport(Modelo); //LIST INVOICES

            var date1Inv = new DateTime();
            var date2Inv = new DateTime();
            var listIeps = new List<IepsModel>();
            var AllIeps = new List<IepsModel>();
            decimal _amountExentoCash = 0;
            decimal _amountExentoCredit = 0;


            InvoiceModel invoiceModel;
            var listInvoiceModel = new List<InvoiceModel>();
            if (listInvoices.Count > 0)
            {
                date1Inv = listInvoices.Min(dt => dt.invoice_date);
                date2Inv = listInvoices.Max(dt => dt.invoice_date);
                listIeps = GetIepsFromInvoicesByDates(date1Inv, date2Inv);
                string _invoicesLIstCredit = "";
                string _invoicesLIstCash = "";
                foreach (INVOICES _inv in listInvoices)
                {
                    if (_inv.is_credit)
                        _invoicesLIstCredit += _inv.cfdi_id + ",";
                    else
                        _invoicesLIstCash += _inv.cfdi_id + ",";
                }
                if (_invoicesLIstCredit.Length > 0)
                {
                    _invoicesLIstCredit = _invoicesLIstCredit.Substring(0, _invoicesLIstCredit.Length - 1);
                }
                else
                    _invoicesLIstCredit = "0";


                if (_invoicesLIstCash.Length > 0)
                    _invoicesLIstCash = _invoicesLIstCash.Substring(0, _invoicesLIstCash.Length - 1);
                else
                    _invoicesLIstCash = "0";


                var date = new SqlParameter("@date", date1Inv.ToString("yyyy/MM/dd"));
                var date2 = new SqlParameter("@date2", date2Inv.ToString("yyyy/MM/dd"));
                var clauseCash = new SqlParameter("@clause", _invoicesLIstCash);
                var clauseCredit = new SqlParameter("@clause", _invoicesLIstCredit);
                _amountExentoCash = _context.Database.SqlQuery<decimal>("sp_GetAmountExentoFromSales @date, @date2, @clause", date, date2, clauseCash).Sum();

                date = new SqlParameter("@date", date1Inv.ToString("yyyy/MM/dd"));
                date2 = new SqlParameter("@date2", date2Inv.ToString("yyyy/MM/dd"));

                _amountExentoCredit = _context.Database.SqlQuery<decimal>("sp_GetAmountExentoFromSales @date, @date2, @clause", date, date2, clauseCredit).Sum();


                //_amountExento = GetAmountExento(date1Inv, date2Inv);
                foreach (INVOICES invo in listInvoices)
                {
                    invoiceModel = new InvoiceModel();
                    var ieps = listIeps.Where(s => s.sale_id == invo.cfdi_id && s.serie == invo.invoice_serie).Select(c => c.value).Sum();
                    string ieps_predicate = null;
                    var GroupedTaxes = (from p in listIeps
                                        where p.sale_id == invo.cfdi_id && p.serie == invo.invoice_serie
                                        group p by p.nameIeps into g
                                        select new IepsModel
                                        {
                                            nameIeps = g.First().nameIeps,
                                            value = g.Sum(s => s.value),
                                            serie = g.First().serie,
                                            sale_id = g.First().sale_id

                                        }).ToList();

                    if (Modelo.IsDataForReport)
                    {
                        invoiceModel.IepsList.AddRange(GroupedTaxes);
                        AllIeps.AddRange(GroupedTaxes);
                    }

                    foreach (IepsModel iepsm in GroupedTaxes)
                    {
                        ieps_predicate += "<td><div style ='width:60px'><font size=1><p>" + iepsm.nameIeps + " </br>" + iepsm.value.ToString("C") + "</p></font></div></td>";
                        invoiceModel.IepsPredicateReport += iepsm.nameIeps + "»»»" + iepsm.value.ToString("C") + "@";
                    }
                    var DigitalTaxStampFDB = _context.DIGITAL_TAX_STAMP.Where(d => d.cfdi_id == invo.cfdi_id && d.cfdi_serie == invo.invoice_serie && d.sat_document_code == "I").Select(s => new { s.UUID, s.cancelled_accepted, s.cancelled_request }).FirstOrDefault();
                    invoiceModel.InvoiceSerie = invo.invoice_serie;
                    invoiceModel.InvoiceNumber = invo.cfdi_id.Value;
                    invoiceModel.CreateDate = invo.invoice_date.ToString(DateFormat.INT_DATE);
                    invoiceModel.CustomerCode = invo.CUSTOMERS.customer_code;
                    invoiceModel.CustomerName = invo.CUSTOMERS.customer_name;
                    invoiceModel.DueDate = invo.invoice_duedate.ToString(DateFormat.INT_DATE);
                    invoiceModel.Amount = invo.invoice_total - invo.invoice_tax - ieps;
                    invoiceModel.Ieps = ieps;
                    invoiceModel.CreditOrCash = invo.is_credit ? "Crédito" : "Contado";
                    invoiceModel.Balance = invo.invoice_balance ?? 0;
                    invoiceModel.TotalCredit = invo.is_credit ? invo.invoice_total : 0;
                    invoiceModel.TotalCash = !invo.is_credit ? invo.invoice_total : 0;
                    invoiceModel.CurrencyCode = invo.cur_code != null ? invo.cur_code : "MXP";
                    ieps_predicate = "<table><tr>" + ieps_predicate + "</tr></table>";
                    invoiceModel.IepsPredicate = ieps_predicate;
                    invoiceModel.StatusValue = invo.invoice_status;
                    invoiceModel.SerieAndNumber = string.Format("{0}{1}", invo.invoice_serie, invo.cfdi_id);
                    var fiscalNumber = "";
                    var fiscalNumberPredicate = "";
                    if (DigitalTaxStampFDB == null)
                    {
                        fiscalNumberPredicate = "<td><div style = 'width:60px' ><font size = 1><p>  SIN TIMBRAR </br> " + fiscalNumber + " </ p ></font></div></td>";
                        invoiceModel.FiscalStatus = false;
                        invoiceModel.FiscalNumberReport = "SIN TIMBRAR";
                    }
                    else
                    {
                        invoiceModel.FiscalStatus = true;
                        var StartIndex = DigitalTaxStampFDB.UUID.Length - 10;
                        var EndIndex = DigitalTaxStampFDB.UUID.Length;
                        fiscalNumber = DigitalTaxStampFDB.UUID.Substring(StartIndex);
                        invoiceModel.FiscalNumber = fiscalNumber;
                        invoiceModel.FiscalNumberReport = fiscalNumber;
                        if (DigitalTaxStampFDB.cancelled_request == true)
                        {
                            fiscalNumberPredicate = "<td><div style = 'width:60px' ><font size = 1><p>" + fiscalNumber + "</br> Por Cancelar </ p ></font></div></td>";
                            invoiceModel.FiscalNumberReport = fiscalNumber + "@ Por Cancelar";

                        }
                        if (DigitalTaxStampFDB.cancelled_accepted == true)
                        {
                            fiscalNumberPredicate = "<td><div style = 'width:60px' ><font size = 1><p>" + fiscalNumber + "</br> Cancelada </ p ></font></div></td>";
                            invoiceModel.FiscalNumberReport = fiscalNumber + "@ Cancelada";
                        }
                        if (DigitalTaxStampFDB.cancelled_request == false || DigitalTaxStampFDB.cancelled_request == null)
                        {
                            fiscalNumberPredicate = "<td><div style = 'width:60px' ><font size = 1><p>" + fiscalNumber + "</br> Vigente </ p ></font></div></td>";
                            invoiceModel.FiscalNumberReport = fiscalNumber + "@ Vigente";

                        }
                    }
                    invoiceModel.FiscalNumber = "<table><tr>" + fiscalNumberPredicate + "</tr></table>";
                    if (!invo.invoice_status)
                        invoiceModel.Status = "Cancelada";
                    else
                    {
                        if (invo.is_credit)
                        {
                            if (invo.invoice_balance <= 0)
                                invoiceModel.Status = "Pagada";
                            else if (invo.invoice_duedate.Date < DateTime.Now.Date)
                                invoiceModel.Status = "Vencida";
                            else
                                invoiceModel.Status = "Vigente";
                        }
                        else                        
                            invoiceModel.Status = "Pagada";                        
                    }

                    if (invoiceModel.Status == "")
                        invoiceModel.Status = "Indefinida";                    

                    invoiceModel.Tax = invo.invoice_tax;
                    invoiceModel.TotalAmount = invo.invoice_total;
                    invoiceModel.HasXmlFile = !string.IsNullOrEmpty(invo.xml);
                    listInvoiceModel.Add(invoiceModel);

                }
            }

            Modelo.InvoiceList = listInvoiceModel;
            Modelo.Totales = GetTotals(Modelo.InvoiceList, _amountExentoCash, _amountExentoCredit);

            if (Modelo.IsDataForReport)
                Modelo.IepsNamesList = AllIeps.OrderBy(d => d.nameIeps).Select(c => c.nameIeps).Distinct().ToList();

            return Modelo;
        }

        public static decimal[,] GetTotals(List<InvoiceModel> Headers, decimal exentoCash, decimal exentoCredit)
        {
            decimal[,] Totales = new decimal[6, 3];

            // C A R G A R     D A T O S
            foreach (InvoiceModel Header in Headers)
            {
                if (Header.StatusValue)
                {
                    if (Header.CreditOrCash == "Contado")
                    {// B I N D     C A S H    

                        // R A T E     Z E R O

                        Totales[0, 1] += Header.Amount - (Header.Tax * 100 / 8);
                        // T A X E D
                        Totales[1, 1] += (Header.Tax * 100 / 8);
                        // T A X
                        Totales[3, 1] += Header.Tax;
                        // I E P S
                        Totales[4, 1] += Header.Ieps;

                    }
                    else
                    {// B I N D     C R E D I T

                        // R A T E     Z E R O


                        Totales[0, 0] += Header.Amount - (Header.Tax * 100 / 8);
                        // T A X E D
                        Totales[1, 0] += (Header.Tax * 100 / 8);
                        // T A X
                        Totales[3, 0] += Header.Tax;
                        // I E P S
                        Totales[4, 0] += Header.Ieps;

                    }
                }



            }
            Totales[0, 1] = Totales[0, 1] - exentoCash;
            Totales[0, 0] = Totales[0, 0] - exentoCredit;
            Totales[2, 1] = exentoCash;
            Totales[2, 0] = exentoCredit;
            for (int f = 0; f < Totales.GetLength(0); f++)
            {
                for (int c = 0; c < Totales.GetLength(1); c++)
                {
                    if (f != Totales.GetLength(0) - 1)
                        Totales[Totales.GetLength(0) - 1, c] += Totales[f, c];
                    if (c != Totales.GetLength(1) - 1)
                        Totales[f, Totales.GetLength(1) - 1] += Totales[f, c];
                }

            }
            Totales[Totales.GetLength(0) - 1, Totales.GetLength(1) - 1] /= 2;





            return Totales;
        }

        private List<IepsModel> GetIepsFromInvoicesByDates(DateTime startDate, DateTime endDate)
        {
            var iepsr = (from inv in _context.INVOICES
                         join invd in _context.INVOICE_DETAIL on new { inv.invoice_no, inv.invoice_serie } equals new { invd.invoice_no, invd.invoice_serie }
                         join invdt in _context.INVOICE_DETAIL_TAX on invd.detail_id equals invdt.detail_id
                         where inv.invoice_date >= startDate && inv.invoice_date <= endDate
                        && invdt.tax_code.Contains(TaxTypes.IEPS)
                         select new IepsModel
                         {
                             value = invdt.tax_amount,
                             serie = inv.invoice_serie,
                             sale_id = inv.invoice_no,
                             nameIeps = invdt.MA_TAX.name
                         }).ToList();

            return iepsr;

        }

        //END INVOICES REPORTS PATRICIO
        public List<INVOICES> GetConstructPredicate(InvoiceReportModel RM)
        {
            //     F I L T R O S
            var predicate = PredicateBuilder.True<INVOICES>();

            // POR CODIGO DE CLIENTE
            if (!string.IsNullOrEmpty(RM.CustomerCode))
            {
                predicate = predicate.And(c => c.CUSTOMERS.customer_code == RM.CustomerCode);
            }
            // POR CODIGO DE MONEDA
            if (RM.CurrencyCodeOption != "SIN_FILTRAR")
            {
                if (RM.CurrencyCodeOption != "MXP")
                    predicate = predicate.And(i => i.CURRENCY.cur_code == RM.CurrencyCodeOption);
                else
                    predicate = predicate.And
                        (i =>
                            i.CURRENCY.cur_code == RM.CurrencyCodeOption ||
                            i.CURRENCY.cur_code == null
                        );
            }
            // POR FECHAS
            if (RM.StartDate != null)
            {
                DateTime start_date_no_null = (RM.StartDate ?? DateTime.Now).Date;
                DateTime finish_date_no_null = (RM.FinishDate ?? DateTime.Now).Date;

                if (RM.FinishDate != null)
                    predicate = predicate.And
                        (i =>
                            DbFunctions.TruncateTime(i.invoice_date) >= start_date_no_null &&
                            DbFunctions.TruncateTime(i.invoice_date) <= finish_date_no_null
                        );
                else
                    predicate = predicate.And(i => DbFunctions.TruncateTime(i.invoice_date) == start_date_no_null);
            }
            // POR MONTO DE FACTURA
            switch (RM.AmountOption)
            {
                case "IGUAL_A":
                    predicate = predicate.And(i => i.invoice_total == RM.InvoiceTotal);
                    break;
                case "MAYOR_A":
                    predicate = predicate.And(i => i.invoice_total > RM.InvoiceTotal);
                    break;
                case "MENOR_A":
                    predicate = predicate.And(i => i.invoice_total < RM.InvoiceTotal);
                    break;
                case "SIN_FILTRAR":
                    break;
                default:
                    break;
            }
            // POR ESTADO DE FACTURA
            switch (RM.StatusOption)
            {
                case "ACTIVAS":
                    predicate = predicate.And(i => i.invoice_status == true);
                    break;
                case "CANCELADAS":
                    predicate = predicate.And(i => i.invoice_status == false);
                    break;
                case "SIN_FILTRAR":
                    break;
                default:
                    break;
            }
            List<INVOICES> InvoicesFDB = _context.INVOICES.Where(predicate).ToList();
            return InvoicesFDB;
        }

        public DIGITAL_TAX_STAMP GetDigitalTaxStampByIdAndSerie(long cfdi_id, string cfdi_serie)
        {
            DIGITAL_TAX_STAMP digital = _context.DIGITAL_TAX_STAMP.Where(dts => dts.cfdi_id == cfdi_id && dts.cfdi_serie == cfdi_serie).FirstOrDefault();
            return digital;
        }

        public List<INVOICE_DETAIL_TAX> GetInvoiceDetailTaxByIdDetail(long detail_id)
        {
            return _context.INVOICE_DETAIL_TAX.Where(tax => tax.detail_id == detail_id).ToList();
        }

        public List<SalesDetailReportModel> GetAllInvoiceReportDetail(long InvoNO)
        {
            var _salesdETAIL = (from ind in _context.INVOICE_DETAIL
                                where ind.invoice_no == InvoNO
                                select new
                                {
                                    id = ind.detail_id,
                                    part_number = ind.part_number,
                                    part_name = ind.description,
                                    part_quantity = ind.detail_qty,
                                    part_price = ind.detail_qty != 0 ? ind.detail_price / ind.detail_qty : 0,
                                    part_importe = ind.detail_price,
                                    part_iva = 0,
                                    part_iva_percentage = 0,
                                    part_ieps = 0,
                                    part_ieps_percentage = 0
                                }).AsEnumerable().
                                Select(x => new SalesDetailReportModel
                                {

                                    part_number = x.part_number,
                                    part_name = x.part_name,
                                    part_quantity = x.part_quantity,
                                    part_price = x.part_price,
                                    part_importe = x.part_importe,
                                    part_iva = GetIvaIepsCInvoDetail(x.id, TaxTypes.TAX),
                                    part_iva_percentage = 0,
                                    part_ieps = GetIvaIepsCInvoDetail(x.id, TaxTypes.IEPS),
                                    part_ieps_percentage = 0
                                }).ToList();
            if (_salesdETAIL != null)
                return _salesdETAIL;
            else
                return new List<SalesDetailReportModel>();
        }

        private decimal GetIvaIepsCInvoDetail(long idtId, string ivaIeps)
        {

            var listtax = _context.INVOICE_DETAIL_TAX.Where(s => s.detail_id == idtId && s.tax_code.Contains(ivaIeps)).Select(s => s.tax_amount).ToList();
            if (listtax != null)
                return listtax.Sum();

            else
                return 0;

        }

        private decimal GetAmountExento(DateTime startDate, DateTime endDate)
        {

            var listExento = (from slsd in _context.DFLPOS_SALES_DETAIL
                              join itmm in _context.ITEM on slsd.part_number equals itmm.part_number
                              where itmm.part_iva_sale == TaxCodes.EXE_0 && slsd.trans_date >= startDate && slsd.trans_date <= endDate
                              select new
                              {
                                  value = slsd.part_price,
                                  iscredit = slsd.DFLPOS_SALES.INVOICES.is_credit

                              }).ToList();

            if (listExento != null)
                return listExento.Sum(e => e.value);

            else
                return 0;

        }

        public List<CURRENCY> GetAllCurrency()
        {
            return _context.CURRENCY.ToList();
        }        

        public List<INVOICES> InvoicesCreditByCustomer(string CustomerCode, string Currency)
        {
            var Info = new List<INVOICES>();
            Info = (from i in _context.INVOICES
                    where i.customer_code == CustomerCode
                          && i.is_credit
                          && i.invoice_status
                          && i.invoice_balance > 0
                          && i.cur_code == Currency
                    select i).ToList();

            return Info;
        }

        public List<InvoiceModel> InvoicesDueByCustomer(string CustomerCode)
        {
            var date = DateTime.Now.Date.ToString(DateFormat.INT_DATE, CultureInfo.InvariantCulture);
            DateTime DateEnd = DateTime.ParseExact(date, DateFormat.INT_DATE, CultureInfo.InvariantCulture);
            var Info = new List<InvoiceModel>();
            Info = (from i in _context.INVOICES
                    where i.customer_code == CustomerCode
                          && i.invoice_status
                          && i.invoice_balance > 0
                          && i.invoice_duedate < DateEnd
                    select new
                    {
                        InvoiceNumber = i.invoice_no,
                        InvoiceSerie = i.invoice_serie,
                        DueDate = i.invoice_duedate,
                        TotalAmount = i.invoice_total,
                        Balance = i.invoice_balance.Value,
                        CreateDate = i.invoice_date
                    }).AsEnumerable().Select(c => new InvoiceModel
                    {
                        InvoiceNumber = c.InvoiceNumber,
                        InvoiceSerie = c.InvoiceSerie,
                        DueDate = c.DueDate.ToString(DateFormat.INT_DATE),
                        TotalAmount = c.TotalAmount,
                        Balance = c.Balance,
                        CreateDate = c.CreateDate.ToString(DateFormat.INT_DATE)
                    }).ToList();

            return Info;
        }
        public List<DetailTaxes> GetIepsFromInvoice(List<InvoiceReport> Invoices)
        {
            var iepsr = (from inv in _context.INVOICES.ToList()
                         join invd in _context.INVOICE_DETAIL.ToList() on new { inv.invoice_no, inv.invoice_serie } equals new { invd.invoice_no, invd.invoice_serie }
                         join invdt in _context.INVOICE_DETAIL_TAX.ToList() on invd.detail_id equals invdt.detail_id
                         join ma in _context.MA_TAX.ToList() on invdt.tax_code equals ma.tax_code
                         join invo in Invoices on new { inv.invoice_no, inv.invoice_serie } equals new { invoice_no = invo.InvoiceNumber, invoice_serie = invo.InvoiceSerie }
                         where invdt.tax_code.Contains(TaxTypes.IEPS)
                         select new DetailTaxes
                         {
                             id = invdt.id,
                             detail_id = invd.detail_id,
                             tax_code = ma.tax_code,
                             tax_amount = invdt.tax_amount,
                             tax_name = ma.name,
                             invo_serie = inv.invoice_serie,
                             invo_id = inv.invoice_no
                         }).ToList();

            return iepsr;

        }

        public SendDocumentModel GetInvoice(long Number)
        {
            var Document = new SendDocumentModel();
            Document = (from i in _context.INVOICES
                        where i.invoice_no == Number
                        select new
                        {
                            Invoice = i.invoice_no,
                            Serie = i.invoice_serie,
                            Customer = i.CUSTOMERS.customer_name,
                            EmailCustomer = i.CUSTOMERS.customer_email,
                            Date = i.invoice_date,
                            DocumentType = VoucherType.INCOMES,
                            Number = i.invoice_no,
                            Total = i.invoice_total,
                            xml = i.xml
                        }).AsEnumerable().Select(c => new SendDocumentModel
                        {
                            Invoice = c.Invoice.ToString(),
                            Serie = c.Serie,
                            Customer = c.Customer,
                            EmailCustomer = c.EmailCustomer,
                            Date = c.Date.ToString(DateFormat.INT_DATE),
                            DocumentType = c.DocumentType,
                            Number = c.Number.ToString(),
                            Total = c.Total.ToString(),
                            Document = Documents.INV                            
                        }).FirstOrDefault();

            Document.IsStamped = _DigitalTaxStampRepository.IsInvoiceStamped(Number);
            Document.CanStampDocument = InvoiceHasTax16(Number);

            return Document;
        }      

        public List<InvoiceOldBalance> OldBalanceReportDataByCustomer(string Currency, string CustomerCode)
        {
            var IepsList = new List<IepsModel>();
            var InvoiceList = new List<InvoiceOldBalance>();
            IepsList = GetIepsFromCreditInvoices(Currency);

            InvoiceList = (from i in _context.INVOICES
                           where i.cur_code == Currency
                                 && i.is_credit
                                 && i.invoice_status
                                 && i.invoice_balance > 0
                                 && i.customer_code == CustomerCode
                           select new
                           {
                               i.cfdi_id,
                               i.invoice_no,
                               i.invoice_serie,
                               i.invoice_date,
                               i.invoice_duedate,
                               i.invoice_total,
                               i.invoice_tax,
                               i.invoice_balance,
                               payment_method = i.SAT_PAYMENT_METHOD_CODE.sat_pm_description
                           }).AsEnumerable().Select(c => new InvoiceOldBalance
                           {
                               CFDI = c.cfdi_id.Value,
                               Number = string.Format("CFD-{0}{1}", c.invoice_serie, c.cfdi_id),
                               Date = c.invoice_date.ToString(DateFormat.INT_DATE),
                               DueDate = c.invoice_duedate.ToString(DateFormat.INT_DATE),
                               Total = c.invoice_total,
                               SubTotal = c.invoice_total - c.invoice_tax,
                               Tax = c.invoice_tax,
                               Balance = c.invoice_balance.Value,
                               Days = (int)(DateTime.Today - c.invoice_duedate).TotalDays,
                               PaymentMethod = c.payment_method,
                               IEPS = IepsList.Where(d => d.sale_id == c.cfdi_id).Select(x => x.value).Sum()
                           }).ToList();



            return InvoiceList;
        }

        private List<IepsModel> GetIepsFromCreditInvoices(string Currency)
        {
            var iepsr = (from inv in _context.INVOICES
                         join invd in _context.INVOICE_DETAIL on new { inv.invoice_no, inv.invoice_serie } equals new { invd.invoice_no, invd.invoice_serie }
                         join invdt in _context.INVOICE_DETAIL_TAX on invd.detail_id equals invdt.detail_id
                         where inv.cur_code == Currency
                               && inv.is_credit
                               && inv.invoice_status
                               && inv.invoice_balance > 0
                               && invdt.tax_code.Contains(TaxTypes.IEPS)
                         select new IepsModel
                         {
                             value = invdt.tax_amount,
                             serie = inv.invoice_serie,
                             sale_id = inv.invoice_no,
                             nameIeps = invdt.MA_TAX.name
                         }).ToList();

            return iepsr;
        }

        public List<InvoiceOldBalanceReport> CustomerBalanceByCurrency(string Currency)
        {
            var DataList = new List<InvoiceOldBalanceReport>();

            DataList = (from i in _context.INVOICES
                        where i.cur_code == Currency
                               && i.is_credit
                               && i.invoice_status
                               && i.invoice_balance > 0
                        group i by i.CUSTOMERS into g
                        select new InvoiceOldBalanceReport
                        {
                            CustomerName = g.Key.customer_name,
                            Phone = g.Key.customer_phone,
                            Email = g.Key.customer_email,
                            CustomerRFC = g.Key.customer_rfc,
                            CustomerCode = g.Key.customer_code,
                            TotalBalance = g.Sum(c => c.invoice_balance.Value)
                        }).ToList();

            foreach(var Item in DataList)
            {
                Item.InvoicesList = OldBalanceReportDataByCustomer(Currency, Item.CustomerCode);
            }

            return DataList;
        }

        public INVOICES GetGlobalInvoiceByDate(string DateValue)
        {
            DateTime Date = DateTime.ParseExact(DateValue, DateFormat.INT_DATE, CultureInfo.InvariantCulture);
            var Invoice = _context.INVOICES.Where(c => c.customer_code == GenericCustomer.CODE && c.invoice_date == Date && c.invoice_status).FirstOrDefault();
            return Invoice;
        }

        public bool DeleteInvoice(long Number, string Serie)
        {
            try
            {
                var Invoice = _context.INVOICES.Where(c => c.invoice_no == Number && c.invoice_serie == Serie).FirstOrDefault();

                if (Invoice != null)
                {
                    _context.INVOICES.Remove(Invoice);
                    _context.SaveChanges();
                }

                return true;
            }
            catch (Exception ex)
            {
                string msg = ex.Message;
                return false;
            }
        }

        public List<INVOICES> GetInvoicesListByFilterReport(InvoiceIndex Modelo)
        {
            var listInvoices = new List<INVOICES>();
            var predicate = PredicateBuilder.True<INVOICES>();

            // x No DE FACTURA
            if (!string.IsNullOrEmpty(Modelo.InvoiceNo))
            {
                var InvoiceNo = long.Parse(Modelo.InvoiceNo);
                predicate = predicate.And(c => c.invoice_no == InvoiceNo);
                goto GetInvoices;
            }

            // x CODIGO DE CLIENTE
            if (!string.IsNullOrEmpty(Modelo.CustomerCode))
            {
                predicate = predicate.And(c => c.CUSTOMERS.customer_code == Modelo.CustomerCode);
            }

            // * X FECHAS
            if (!string.IsNullOrEmpty(Modelo.StartDate) && !string.IsNullOrEmpty(Modelo.EndDate))
            {
                DateTime DateStart = DateTime.ParseExact(Modelo.StartDate, DateFormat.INT_DATE, CultureInfo.InvariantCulture);
                DateTime DateEnd = DateTime.ParseExact(Modelo.EndDate, DateFormat.INT_DATE, CultureInfo.InvariantCulture);

                predicate = predicate.And
                         (i =>
                             DbFunctions.TruncateTime(i.invoice_date) >= DateStart &&
                             DbFunctions.TruncateTime(i.invoice_date) <= DateEnd
                         );
            }

            // * X MONEDA
            //if (Modelo.Currency != Currencys.ALL)
            //{
                if (Modelo.Currency == Currencys.MXN)
                    predicate = predicate.And(c => c.cur_code == Currencies.MXN);
                else
                    predicate = predicate.And(c => c.cur_code == Currencies.USD);
            //}

            // * X STATUS
            if (Modelo.Status != CreditNoteStatus.All)
            {
                if (Modelo.Status == CreditNoteStatus.Active)
                    predicate = predicate.And(c => c.invoice_status);
                else
                    predicate = predicate.And(c => !c.invoice_status);
            }

            // * X MONTO
            if (Modelo.OtionsMounts != Mounts.CUAL)
            {
                switch (Modelo.OtionsMounts)
                {
                    case Mounts.IGUAL:
                        predicate = predicate.And(i => i.invoice_total == Modelo.Monto);
                        break;
                    case Mounts.MAYOR:
                        predicate = predicate.And(i => i.invoice_total > Modelo.Monto);
                        break;
                    case Mounts.MENOR:
                        predicate = predicate.And(i => i.invoice_total < Modelo.Monto);
                        break;

                    default:
                        break;
                }
            }

            // * X TIPO PAGO (CREDITO O CONTADO)
            if (Modelo.OptionsPay != InvoiceType.All)
            {
                if (Modelo.OptionsPay == InvoiceType.Cash)
                    predicate = predicate.And(c => !c.is_credit);
                if (Modelo.OptionsPay == InvoiceType.Credit)
                    predicate = predicate.And(c => c.is_credit);
                if (Modelo.OptionsPay == InvoiceType.Conp)
                    predicate = predicate.And(c => c.sat_pm_code=="17");
            }
            // * x UUID
            if (Modelo.Uuid != CreditNoteStatusFiscal.All)
            {
                if (Modelo.Uuid == CreditNoteStatusFiscal.SinTimbre)
                    predicate = predicate.And(c => !(_context.DIGITAL_TAX_STAMP.Where(d => d.cfdi_id == c.cfdi_id && d.cfdi_serie == c.invoice_serie && d.sat_document_code == VoucherType.INCOMES).
                                                    Select(an => new { an.cfdi_id, an.cfdi_serie })).ToList().Contains(new { cfdi_id = c.cfdi_id.Value, cfdi_serie = c.invoice_serie }));
                else
                    predicate = predicate.And(c => (_context.DIGITAL_TAX_STAMP.Where(d => d.cfdi_id == c.cfdi_id && d.cfdi_serie == c.invoice_serie && d.sat_document_code == VoucherType.INCOMES).
                                                   Select(an => new { an.cfdi_id, an.cfdi_serie })).ToList().Contains(new { cfdi_id = c.cfdi_id.Value, cfdi_serie = c.invoice_serie }));


            }

            GetInvoices:
            return listInvoices = _context.INVOICES.Where(predicate).ToList(); //LIST INVOICES
        }

        public bool InvoiceHasTax16(long InvoiceNo)
        {
            var HasTax16 = (from idt in _context.INVOICE_DETAIL_TAX
                            join id in _context.INVOICE_DETAIL on idt.detail_id equals id.detail_id
                            where idt.tax_code == TaxCodes.TAX_16
                                  && id.invoice_no == InvoiceNo
                            select idt).Any();
            bool Stamp16 = ConfigurationManager.AppSettings["Stamp16"].ToString() == "1";

            if (!HasTax16 || (HasTax16 && Stamp16))
                return true;

            return false;
        }

        public INVOICES SubstitutionInvoice(InvoicesSales Model, long InvoiceSubst)
        {
            var NewInvoice = new INVOICES();
            var Invoice = GetInvoiceByNumber(InvoiceSubst);
            var PM = _PaymentMethodRepository.GetPaymentMethodById(Model.PaymentTypeCode);     

            NewInvoice.invoice_no = Model.Number;
            NewInvoice.invoice_serie = Model.Serie;
            NewInvoice.cfdi_id = Model.Number;
            NewInvoice.invoice_date = Invoice.invoice_date;
            NewInvoice.customer_code = Invoice.customer_code;
            NewInvoice.invoice_total = Invoice.invoice_total;
            NewInvoice.invoice_tax = Invoice.invoice_tax;
            NewInvoice.invoice_balance = Invoice.invoice_balance;
            NewInvoice.invoice_duedate = Invoice.invoice_duedate;
            NewInvoice.invoice_status = true;
            NewInvoice.invoice_comments = Invoice.invoice_comments;
            NewInvoice.is_credit = Invoice.is_credit;
            NewInvoice.is_daily = Invoice.is_daily;
            NewInvoice.cur_code = Invoice.cur_code;
            NewInvoice.invoice_cur_rate = Invoice.invoice_cur_rate;
            NewInvoice.site_code = Invoice.site_code;
            NewInvoice.sat_pm_code = PM != null ? PM.sat_pm_code : SatPaymentMethods.UNDEFINED;
            NewInvoice.pm_reference = Invoice.pm_reference;
            NewInvoice.sat_us_code = Model.CFDICode;
            NewInvoice.invoice_no_subst = InvoiceSubst;
            NewInvoice.cuser = Model.User;
            NewInvoice.cdate = DateTime.Now.Date;
            NewInvoice.program_id = "INVO007.cshtml";

            return NewInvoice;
        }

        public bool CheckTotalAndSubtotalInvoice(INVOICES Invoice)
        {            
            decimal TaxSum = 0, TotalDetails = 0;
            var DetailsList = _InvoiceDetailRepository.GetDetailsByInvoice(Invoice.invoice_no, Invoice.invoice_serie);
            var TaxsList = _InvoiceDetailTaxRepository.GetTaxesByInvoice(Invoice.invoice_no, Invoice.invoice_serie);
            if (TaxsList.Count > 0)
                TaxSum = TaxsList.Sum(c => c.TaxAmount);

            if (DetailsList.Count > 0)
                TotalDetails = DetailsList.Sum(c => c.detail_price);

            bool MatchTotal = Invoice.invoice_total == (TotalDetails + TaxSum);

            return MatchTotal;
        }

        public bool CheckInvoiceTax(INVOICES Invoice)
        {
            decimal TaxSum = 0;
            var TaxsList = _InvoiceDetailTaxRepository.GetTaxesByInvoice(Invoice.invoice_no, Invoice.invoice_serie);

            if(TaxsList.Count > 0)
                TaxSum = TaxsList.Sum(c => c.TaxAmount);

            bool MatchTax = Invoice.invoice_tax == TaxSum;

            return MatchTax;
        }

        public bool SprocUpdateInvoiceNumber()
        {
            DFL_SAIEntities context = new DFL_SAIEntities();
            context.Configuration.AutoDetectChangesEnabled = false;
            try
            {
                //Update Internal Number and Number in DB from Reference Number Table.
                ObjectParameter Reference = new ObjectParameter("Reference", typeof(string));
                context.sp_Get_Reference_Number("INVOICE", "ALL", Reference);
                return true;
            }
            catch (Exception ex)
            {
                string msg = ex.Message;
                return false;
            }
            finally
            {
                if (context != null)
                {
                    context.Configuration.AutoDetectChangesEnabled = true;
                    context.Dispose();
                }
            }

        }
    }
}
