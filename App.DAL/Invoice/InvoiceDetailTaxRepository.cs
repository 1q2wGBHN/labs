﻿using App.Entities;
using App.Entities.ViewModels;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;

namespace App.DAL.Invoice
{
    public class InvoiceDetailTaxRepository
    {
        private DFL_SAIEntities _context;

        public InvoiceDetailTaxRepository()
        {
            _context = new DFL_SAIEntities();
        }

        #region CreateInvoiceDetailTax
        public bool CreateInvoiceDetailTax(List<INVOICE_DETAIL_TAX> InvoiceDetailTaxList)
        {
            DFL_SAIEntities context = new DFL_SAIEntities();
            try
            {
                context.Configuration.AutoDetectChangesEnabled = false;
                int cont = 0;
                foreach (var Item in InvoiceDetailTaxList)
                {
                    ++cont;
                    context = AddToContext(context, Item, cont, 1000, true);
                }
                context.SaveChanges();
                return true;
            }
            catch (Exception ex)
            {
                string msg = ex.Message;
                return false;
            }
            finally
            {
                if (context != null)
                {
                    context.Configuration.AutoDetectChangesEnabled = true;
                    context.Dispose();
                }
            }
        }

        private DFL_SAIEntities AddToContext(DFL_SAIEntities context, INVOICE_DETAIL_TAX entity, int count, int commitCount, bool recreateContext)
        {
            context.Set<INVOICE_DETAIL_TAX>().Add(entity);

            if (count % commitCount == 0)
            {
                context.SaveChanges();
                if (recreateContext)
                {
                    context.Dispose();
                    context = new DFL_SAIEntities();
                    context.Configuration.AutoDetectChangesEnabled = false;
                }
            }

            return context;
        }
        #endregion        

        public bool DeleteDetailTax(long InvoiceNumber, string InvoiceSerie)
        {
            try
            {
                var Delete = _context.Database.ExecuteSqlCommand("DELETE idt FROM INVOICE_DETAIL_TAX idt JOIN INVOICE_DETAIL id ON idt.detail_id = id.detail_id WHERE id.invoice_no = @InvoiceNumber AND id.invoice_serie = @InvoiceSerie",
                    new SqlParameter("InvoiceSerie", InvoiceSerie),
                    new SqlParameter("InvoiceNumber", InvoiceNumber));

                return true;
            }
            catch (Exception ex)
            {
                string msg = ex.Message;
                return false;
            }
        }

        public List<INVOICE_DETAIL_TAX> DetailsTaxForInvoiceSubstitute (string Serie, long InvoiceNo, long NewInvoiceNo)
        {
            var DetailsTax = new List<INVOICE_DETAIL_TAX>();
            DetailsTax = (from id in _context.INVOICE_DETAIL
                          join idt in _context.INVOICE_DETAIL_TAX on id.detail_id equals idt.detail_id
                          where id.invoice_no == InvoiceNo && id.invoice_serie == Serie
                          select idt).ToList();

            if (DetailsTax.Count > 0)
            {
                //Get the details from the new invoice
                var DetailsInvoice = (from id in _context.INVOICE_DETAIL
                                      where id.invoice_serie == Serie && id.invoice_no == NewInvoiceNo
                                      select id).ToList();               

                foreach (var item in DetailsTax)
                {
                    var NewDetailInvoice = DetailsInvoice.Where(d => d.previous_detail_id == item.detail_id).FirstOrDefault();
                    if (NewDetailInvoice != null)
                        item.detail_id = NewDetailInvoice.detail_id;                    
                }
            }

            _context.Dispose();
            return DetailsTax;
        }

        public List<DetailTax> GetTaxesByInvoice(long Number, string Serie)
        {
            var DetailTaxs = new List<DetailTax>();
            DetailTaxs = (from idt in _context.INVOICE_DETAIL_TAX
                              join id in _context.INVOICE_DETAIL on idt.detail_id equals id.detail_id
                              where id.invoice_serie == Serie && id.invoice_no == Number
                                    && idt.tax_code.Contains("TAX")
                              select new DetailTax
                              {
                                  DetailId = idt.detail_id,
                                  IVACode = idt.tax_code,
                                  IVAValue = idt.MA_TAX.tax_value,
                                  TaxAmount = idt.tax_amount
                              }).ToList();
            return DetailTaxs;
        }

        public List<DetailTax> GetTaxesInfoByInvoiceDetailId(long InvoiceDetailid)
        {
            var DetailTaxs = new List<DetailTax>();
            DetailTaxs = (from idt in _context.INVOICE_DETAIL_TAX
                          join id in _context.INVOICE_DETAIL on idt.detail_id equals id.detail_id
                          where id.detail_id == InvoiceDetailid
                          select new DetailTax
                          {
                              DetailId = idt.detail_id,                              
                              TaxAmount = idt.tax_amount,
                              TaxCode = idt.tax_code
                          }).ToList();

            return DetailTaxs;
        }
    }
}
