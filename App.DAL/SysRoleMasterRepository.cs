﻿using App.Entities;
using App.Entities.ViewModels.User;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;

namespace App.DAL
{
    public class SysRoleMasterRepository
    {
        private readonly DFL_SAIEntities _context;

        public SysRoleMasterRepository()
        {
            _context = new DFL_SAIEntities();
        }

        public ICollection<SYS_ROLE_MASTER> GetAllRolesAdmin()
        {
            return _context.SYS_ROLE_MASTER.Select(x => x).ToList();
        }

        public SYS_ROLE_MASTER getRoleByID(string rolId)
        {
            return _context.SYS_ROLE_MASTER.FirstOrDefault(x => x.role_id == rolId);
        }

        public int AddRolesMaster(SYS_ROLE_MASTER rol)
        {
            try
            {
                _context.SYS_ROLE_MASTER.Add(rol);
                return _context.SaveChanges();
            }
            catch (Exception ex)
            {
                var msg = ex.Message;
                return 0;
            }
        }

        public SYS_ROLE_MASTER SearchRole(string id_role)
        {
            var role = _context.SYS_ROLE_MASTER.Where(w => w.role_id == id_role).FirstOrDefault();
            if (role != null)
                return role;
            else
                return null;
        }

        public int updateRoleMaster(SYS_ROLE_MASTER rol)
        {
            _context.Entry(rol).State = System.Data.Entity.EntityState.Modified;
            return _context.SaveChanges();
        }

        public string GetEmailsByRol(string rol)
        {
            try
            {
                List<UserModelFirst> users = new List<UserModelFirst>();
                List<UserModelFirst> ExcludeDistrital = new List<UserModelFirst>();
                var rolId = _context.SYS_ROLE_MASTER.Where(w => w.role_name == rol).Select(s => new UserModelFirst { FirstName = s.role_id }).FirstOrDefault();

                if (rol != ConfigurationManager.AppSettings["Gerente"].ToString())
                {
                    users = _context.SYS_ROLE_MASTER.Join(_context.SYS_ROLE_USER, r => r.role_id, rr => rr.role_id, (r, rr) => new { r, rr })
                        .Join(_context.USER_MASTER, u => u.rr.emp_no, uu => uu.emp_no, (u, uu) => new { u, uu })
                        .Join(_context.USER_PWD, x => x.u.rr.emp_no, pwd => pwd.emp_no, (x, pwd) => new { x, pwd })
                        .Where(w => w.x.u.r.role_id == rolId.FirstName && w.x.u.rr.role_id == rolId.FirstName && (w.pwd.status == "A" || w.pwd.status == "B"))
                        .Select(s => new UserModelFirst { Email = s.x.uu.email }).ToList();
                }
                else
                {
                    string distrial = ConfigurationManager.AppSettings["Distrital"].ToString();
                    var rolIdDistrital = _context.SYS_ROLE_MASTER.Where(w => w.role_name == distrial).Select(s => new UserModelFirst { FirstName = s.role_id }).FirstOrDefault();
                    //Para exluir al distrital en correos donde no tiene nada que ver .Where(w => w.role_name == distrial)
                    ExcludeDistrital = _context.SYS_ROLE_MASTER.Join(_context.SYS_ROLE_USER, r => r.role_id, rr => rr.role_id, (r, rr) => new { r, rr })
                        .Join(_context.USER_MASTER, u => u.rr.emp_no, uu => uu.emp_no, (u, uu) => new { u, uu })
                        .Join(_context.USER_PWD, x => x.u.rr.emp_no, pwd => pwd.emp_no, (x, pwd) => new { x, pwd })
                        .Where(w => w.x.u.r.role_id == rolIdDistrital.FirstName && w.x.u.rr.role_id == rolIdDistrital.FirstName && (w.pwd.status == "A" || w.pwd.status == "B"))
                        .Select(s => new UserModelFirst { Email = s.x.uu.email }).ToList();
                    users = _context.SYS_ROLE_MASTER.Join(_context.SYS_ROLE_USER, r => r.role_id, rr => rr.role_id, (r, rr) => new { r, rr })
                        .Join(_context.USER_MASTER, u => u.rr.emp_no, uu => uu.emp_no, (u, uu) => new { u, uu })
                        .Join(_context.USER_PWD, x => x.u.rr.emp_no, pwd => pwd.emp_no, (x, pwd) => new { x, pwd })
                        .Where(w => w.x.u.r.role_id == rolId.FirstName && w.x.u.rr.role_id == rolId.FirstName && (w.pwd.status == "A" || w.pwd.status == "B"))
                        .Select(s => new UserModelFirst { Email = s.x.uu.email }).ToList();
                }
                var Exclude = ExcludeDistrital.Select(s => s.Email).Distinct().ToList();
                var user = users.Select(s => s.Email).Distinct().ToList();

                if (Exclude.Count() > 0)
                {
                    //Remover a los distritales cuando sean gerentes
                    user.RemoveAll(i => Exclude.Contains(i));
                }

                string emails = "";
                if (user.Count == 1)
                    emails = user[0];
                else
                {
                    var lastEmail = user.Last();
                    foreach (var item in user)
                    {

                        if (lastEmail.Equals(item))
                            emails += item;
                        else
                        {
                            if (emails == "")
                                emails = item + ",";
                            else
                                emails = emails + item + ",";
                        }
                    }
                }
                return emails;
            }
            catch (Exception e)
            {
                var messsage = e.Message;
                return null;
            }
        }

        public List<UserModelFirst> GetEmailsInfoByRol(string rol)
        {
            try
            {
                var rolId = _context.SYS_ROLE_MASTER.Where(w => w.role_name == rol).Select(s => new UserModelFirst { FirstName = s.role_id }).FirstOrDefault();
                return _context.SYS_ROLE_MASTER.Join(_context.SYS_ROLE_USER, r => r.role_id, rr => rr.role_id, (r, rr) => new { r, rr })
                    .Join(_context.USER_MASTER, u => u.rr.emp_no, uu => uu.emp_no, (u, uu) => new { u, uu })
                    .Where(w => w.u.r.role_id == rolId.FirstName && w.u.rr.role_id == rolId.FirstName)
                    .Select(s => new UserModelFirst
                    {
                        EmpNo = s.uu.emp_no,
                        UserName = s.uu.user_name,
                        FirstName = s.uu.first_name,
                        LastName = s.uu.last_name,
                        mobile_tel = s.uu.mobile_tel,
                        office_tel = s.uu.office_tel,
                        Email = s.uu.email
                    }).ToList();
            }
            catch (Exception e)
            {
                var messsage = e.Message;
                return null;
            }
        }
    }
}