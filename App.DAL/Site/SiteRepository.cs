﻿using App.Entities;
using App.Entities.ViewModels.Site;
using System.Collections.Generic;
using System.Linq;

namespace App.DAL.Site
{
    public class SiteRepository
    {
        private readonly DFL_SAIEntities _context;

        public SiteRepository()
        {
            _context = new DFL_SAIEntities();
        }

        public List<SiteModel> GetAllSitesFlorido()
        {
            return _context.SITES.Where(x => x.site_type != "COST CENTER" && x.site_name != "PROCESADORA")
                .Select(x => new SiteModel
                {
                    site_code = x.site_code,
                    site_name = x.site_name
                }).ToList();
        }

        public List<SiteModel> GetAllSitesFloridoExcludeThisSite(string siteCode)
        {
            return _context.SITES.Where(x => x.site_type != "COST CENTER" && x.site_name != "PROCESADORA" && x.site_code != siteCode)
                .Select(x => new SiteModel
                {
                    site_code = x.site_code,
                    site_name = x.site_name
                }).ToList();
        }

        public List<SiteModel> GetAllSitesFloridoExcludeThisSiteNoProcesadora(string siteCode)
        {
            return _context.SITES.Where(x => x.site_type != "COST CENTER" && x.site_code != siteCode)
                .Select(x => new SiteModel
                {
                    site_code = x.site_code,
                    site_name = x.site_name
                }).ToList();
        }

        public string GetSiteCodeName(string siteDestination)
        {
            var site = _context.SITES.Where(y => y.site_code == siteDestination).Select(x => new SiteConfigModel
            {
                Site = x.site_code,
                SiteName = x.site_name,
                SiteAddress = x.address
            }).FirstOrDefault();
            return site.SiteName;
        }

        public string GetSiteCodeByName(string siteDestination)
        {
            var site = _context.SITES.Where(y => y.site_name == siteDestination).Select(x => new SiteConfigModel
            {
                Site = x.site_code,
                SiteName = x.site_name,
                SiteAddress = x.address
            }).FirstOrDefault();
            return site.Site;
        }

        public string GetSiteCodeAddress(string siteDestination)
        {
            var site = _context.SITES.Where(y => y.site_code == siteDestination).Select(x => new SiteConfigModel
            {
                Site = x.site_code,
                SiteName = x.site_name,
                SiteAddress = x.address
            }).FirstOrDefault();
            return site.SiteAddress;
        }

        public SITES GetInfoSite(string site_code)
        {
            return _context.SITES.Where(w => w.site_code == site_code).FirstOrDefault();
        }

        public bool SiteHasNewErp(string site_code)
        {
            return _context.SITES.Any(e => e.site_code == site_code && e.new_erp);
        }
    }
}