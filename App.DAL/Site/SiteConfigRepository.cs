﻿using App.Entities;
using App.Entities.ViewModels.Site;
using System.Linq;

namespace App.DAL.Site
{
    public class SiteConfigRepository
    {
        private readonly DFL_SAIEntities _context;


        public SiteConfigRepository()
        {
            _context = new DFL_SAIEntities();
        }

        public string GetSiteCode()
        {
            var site = _context.SITE_CONFIG.Select(x => new SiteModel
            {
                site_code = x.site_code
            }).FirstOrDefault();
            return site.site_code;
        }

        public string GetSiteCodeName()
        {
            var site = _context.SITE_CONFIG.Join(_context.SITES, x => x.site_code, y => y.site_code, (x, y) => new { x, y }).Select(x => new SiteConfigModel
            {
                Site = x.x.site_code,
                SiteName = x.x.site_name,
                SiteAddress = x.y.address
            }).FirstOrDefault();
            return site.SiteName;
        }

        public SiteConfigModel GetSiteCodeAddress()
        {
            var site = _context.SITE_CONFIG.Join(_context.SITES, x => x.site_code, y => y.site_code, (x, y) => new { x, y }).Select(x => new SiteConfigModel
            {
                Site = x.x.site_code.Trim(),
                SiteName = x.x.site_name.Trim(),
                SiteAddress = x.y.address,
                IP = x.y.ip_address
            }).FirstOrDefault();
            return site;
        }

        public SiteConfigModel GetSiteConfigInfo()
        {
            var ip = _context.SITE_CONFIG.Join(_context.SITES, s => s.site_code, c => c.site_code, (s, c) => new { s, c })
                .Select(s => new SiteConfigModel
                {
                    IP = s.c.ip_address.Trim(),
                    User = s.s.site_user_server,
                    pwd = s.s.site_user_pwd,
                    Site = s.c.site_code
                }).FirstOrDefault();
            return ip;
        }

        public SiteConfigModel GetSiteConfig()
        {
            var ip = _context.SITE_CONFIG
                .Select(s => new SiteConfigModel
                {
                    SiteName = s.site_name,
                    Site = s.site_code,
                }).FirstOrDefault();
            return ip;
        }
    }
}