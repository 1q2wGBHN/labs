﻿using App.Entities;
using App.Entities.ViewModels.EntryFree;
using App.Entities.ViewModels.PurchaseOrder;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.SqlClient;
using System.Linq;

namespace App.DAL.EntryFree
{
    public class EntryFreeHeadRepository
    {
        private readonly DFL_SAIEntities _context;

        public EntryFreeHeadRepository()
        {
            _context = new DFL_SAIEntities();
        }

        public int PostEntryFreeHead(int Supplier, string Type, string SubType, string Reference, string Commentary, string User)
        {
            var EntryFree = new ENTRY_FREE_HEAD()
            {
                supplier_id = Supplier,
                type_register = Type,
                subtype_register = SubType,
                reference = Reference,
                commentary = Commentary,
                cuser = User,
                cdate = DateTime.Now,
                program_id = "Android",
                status = 1,
                currency = "MXN",
                date_entry_free = DateTime.Now.Date,
                site_code = _context.SITE_CONFIG.ToList()[0].site_code,
                print_status = 0
            };
            _context.ENTRY_FREE_HEAD.Add(EntryFree);
            _context.SaveChanges();
            return EntryFree.id;
        }

        public EntryFreeModel GetEntryFree(int EntryFreeId)
        {
            var EntryFreeHead = _context.ENTRY_FREE_HEAD.Join(_context.MA_SUPPLIER, x => x.supplier_id, y => y.supplier_id, (x, y) => new { x, y }).Where(x => x.x.id == EntryFreeId && x.x.status == 1).ToList().Select(x => new EntryFreeModel
            {
                Date = x.x.date_entry_free != null ? x.x.date_entry_free.Value.ToString("dd/MM/yyyy") : "",
                Id = x.x.id,
                SubType = x.x.subtype_register,
                Type = x.x.type_register,
                SupplierId = x.x.supplier_id,
                Supplier = x.y.business_name,
                Commentary = x.x.commentary,
                Reference = x.x.reference,
                User = x.x.cuser,
                Print_status = x.x.print_status ?? 0
            }).SingleOrDefault();
            if (EntryFreeHead != null)
            {
                EntryFreeHead.EntryFreeList = _context.ENTRY_FREE_LIST.Join(_context.ITEM, x => x.part_number, y => y.part_number, (x, y) => new { x, y }).Where(x => x.x.entry_free_id == EntryFreeId)
                    .Select(x => new EntryFreeListModel
                    {
                        Description = x.y.part_description,
                        PartNumber = x.y.part_number,
                        Quantity = x.x.quantity,
                        Id = x.x.id,
                        Size = x.y.unit_size ?? "N/A",
                        Price = x.x.entry_free_price ?? 0,
                        Ieps = x.x.entry_free_ieps ?? 0,
                        Iva = x.x.entry_free_iva ?? 0
                    }).ToList();
                return EntryFreeHead;
            }
            return new EntryFreeModel();
        }

        public EntryFreeModel NewGetEntryFree(int EntryFreeId)
        {
            var EntryFreeHead = _context.ENTRY_FREE_HEAD.Join(_context.MA_SUPPLIER, x => x.supplier_id, y => y.supplier_id, (x, y) => new { x, y }).Where(x => x.x.id == EntryFreeId).ToList().Select(x => new EntryFreeModel
            {
                Date = x.x.date_entry_free != null ? x.x.date_entry_free.Value.ToString("dd/MM/yyyy") : "",
                Id = x.x.id,
                SubType = x.x.subtype_register,
                Type = x.x.type_register,
                SupplierId = x.x.supplier_id,
                Supplier = x.y.business_name,
                Commentary = x.x.commentary,
                Reference = x.x.reference,
                User = x.x.cuser,
                Print_status = x.x.print_status ?? 0
            }).SingleOrDefault();
            if (EntryFreeHead != null)
            {
                EntryFreeHead.EntryFreeList = _context.ENTRY_FREE_LIST.Join(_context.ITEM, x => x.part_number, y => y.part_number, (x, y) => new { x, y }).Where(x => x.x.entry_free_id == EntryFreeId)
                    .Select(x => new EntryFreeListModel
                    {
                        Description = x.y.part_description,
                        PartNumber = x.y.part_number,
                        Quantity = x.x.quantity,
                        Id = x.x.id,
                        Size = x.y.unit_size ?? "N/A",
                        Price = x.x.entry_free_price ?? 0,
                        Ieps = x.x.entry_free_ieps ?? 0,
                        Iva = x.x.entry_free_iva ?? 0
                    }).ToList();
                return EntryFreeHead;
            }
            return new EntryFreeModel();
        }

        public EntryFreeModel GetEntryFreeReport2(int EntryFreeId)
        {
            var EntryFreeHead = _context.ENTRY_FREE_HEAD.Join(_context.MA_SUPPLIER, x => x.supplier_id, y => y.supplier_id, (x, y) => new { x, y }).Where(x => x.x.id == EntryFreeId).ToList().Select(x => new EntryFreeModel
            {
                Date = x.x.date_entry_free != null ? x.x.date_entry_free.Value.ToString("dd/MM/yyyy") : "",
                Id = x.x.id,
                SubType = x.x.subtype_register,
                Type = x.x.type_register,
                SupplierId = x.x.supplier_id,
                Supplier = x.y.business_name,
                Commentary = x.x.commentary,
                Reference = x.x.reference,
                Status = x.x.status ?? 0,
                User = x.x.cuser,
                Print_status = x.x.print_status ?? 0
            }).SingleOrDefault();
            if (EntryFreeHead != null)
            {
                EntryFreeHead.EntryFreeList = _context.ENTRY_FREE_LIST.Join(_context.ITEM, x => x.part_number, y => y.part_number, (x, y) => new { x, y }).Where(x => x.x.entry_free_id == EntryFreeId)
                    .Select(x => new EntryFreeListModel
                    {
                        Description = x.y.part_description,
                        PartNumber = x.y.part_number,
                        Quantity = x.x.quantity,
                        Id = x.x.id,
                        Size = x.y.unit_size ?? "N/A",
                        Price = x.x.entry_free_price ?? 0,
                        Ieps = x.x.entry_free_ieps ?? 0,
                        Iva = x.x.entry_free_iva ?? 0
                    }).ToList();
                return EntryFreeHead;
            }
            return new EntryFreeModel();
        }

        public List<EntryFreeListModel> GetEntryFreeReport(int EntryFreeId)
        {
            var EntryFreeHead = _context.ENTRY_FREE_LIST.Join(_context.ITEM, x => x.part_number, y => y.part_number, (x, y) => new { x, y }).Where(x => x.x.entry_free_id == EntryFreeId)
                    .Select(x => new EntryFreeListModel
                    {
                        Description = x.y.part_description,
                        PartNumber = x.y.part_number,
                        Quantity = x.x.quantity,
                        Id = x.x.id,
                        Size = x.y.unit_size ?? "N/A",
                        Price = x.x.entry_free_price ?? 0,
                        Ieps = x.x.entry_free_ieps ?? 0,
                        Iva = x.x.entry_free_iva ?? 0
                    }).ToList();
            return EntryFreeHead;
        }

        public bool EntryFreeEditStatusPrint(int Document, int Status, string program_id)
        {
            try
            {
                var damageds = _context.ENTRY_FREE_HEAD.Where(elements => elements.id == Document).SingleOrDefault();
                damageds.program_id = program_id + "+Print";
                damageds.print_status = Status + 1;
                _context.SaveChanges();
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public bool PutEntryFree(int EntryFreeId, string User)
        {
            var EntryFreeHead = _context.ENTRY_FREE_HEAD.Where(x => x.id == EntryFreeId && x.status == 1).SingleOrDefault();
            if (EntryFreeHead != null)
            {
                EntryFreeHead.status = 8;
                EntryFreeHead.uuser = User;
                EntryFreeHead.udate = DateTime.Now;
                EntryFreeHead.program_id = "EMSC001.cshtml";
                _context.SaveChanges();
                return true;
            }
            return false;
        }

        public bool NewPutEntryFree(int EntryFreeId, string User)
        {
            var EntryFreeHead = _context.ENTRY_FREE_HEAD.Where(x => x.id == EntryFreeId && x.status != 8).SingleOrDefault();
            if (EntryFreeHead != null)
            {
                EntryFreeHead.status = 8;
                EntryFreeHead.uuser = User;
                EntryFreeHead.udate = DateTime.Now;
                EntryFreeHead.program_id = "EMSC002.cshtml";
                _context.SaveChanges();
                return true;
            }
            return false;
        }

        public List<EntryFreeModel> GetFoliosBySupplierAndFechaRange(int supplier, DateTime fechaIni, DateTime fechaFin)
        {
            return _context.ENTRY_FREE_HEAD.Where(x => x.supplier_id == supplier && x.date_entry_free >= fechaIni && x.date_entry_free <= fechaFin && x.status == 1)
                .Select(x => new EntryFreeModel
                {
                    Id = x.id,
                    Type = x.type_register
                }).ToList();
        }

        public List<EntryFreeModel> GetAllEntryFree(DateTime initial, DateTime final)
        {
            return _context.ENTRY_FREE_HEAD.Join(_context.MA_SUPPLIER, entryFree => entryFree.supplier_id, supplier => supplier.supplier_id, (entryFree, supplier) => new { entryFree, supplier })
                .Join(_context.SITES, entryFree => entryFree.entryFree.site_code, sites => sites.site_code, (entryFree, sites) => new { entryFree, sites })
                .Where(x => DbFunctions.TruncateTime(x.entryFree.entryFree.date_entry_free.Value) >= DbFunctions.TruncateTime(initial) && DbFunctions.TruncateTime(x.entryFree.entryFree.date_entry_free.Value) <= DbFunctions.TruncateTime(final))
                .ToList()
                .Select(x => new EntryFreeModel
                {
                    Date = x.entryFree.entryFree.date_entry_free != null ? x.entryFree.entryFree.date_entry_free.Value.ToLongDateString() : "",
                    Id = x.entryFree.entryFree.id,
                    SubType = x.entryFree.entryFree.subtype_register,
                    Type = x.entryFree.entryFree.type_register,
                    SupplierId = x.entryFree.entryFree.supplier_id,
                    Supplier = x.entryFree.supplier.business_name,
                    Commentary = x.entryFree.entryFree.commentary,
                    Reference = x.entryFree.entryFree.reference,
                    User = x.entryFree.entryFree.cuser,
                    Status = x.entryFree.entryFree.status ?? 1
                }).ToList();
        }

        public List<EntryFreeModel> GetAllEntryFreeOptions(DateTime? initial, DateTime? final, int supplierId, int status, string deparment, string family, string currency, string part_number)
        {
            /*var EntryFreeList = _context.ENTRY_FREE_HEAD
                .Join(_context.MA_SUPPLIER, entryFree => entryFree.supplier_id, supplier => supplier.supplier_id, (entryFree, supplier) => new { entryFree, supplier })
                .Join(_context.SITES, entryFree => entryFree.entryFree.site_code, sites => sites.site_code, (entryFree, sites) => new { entryFree, sites })
                .Join(_context.ENTRY_FREE_LIST, entryFree => entryFree.entryFree.entryFree.id, entryFreeList => entryFreeList.entry_free_id, (entryFree, entryFreeList) => new { entryFree, entryFreeList })
                .Join(_context.ITEM, entryFree => entryFree.entryFreeList.part_number, entryFreeItem => entryFreeItem.part_number, (entryFree, entryFreeItem) => new { entryFree, entryFreeItem });

            if (initial.HasValue && final.HasValue)
                EntryFreeList = EntryFreeList.Where(x => DbFunctions.TruncateTime(x.entryFree.entryFree.entryFree.entryFree.date_entry_free.Value) >= DbFunctions.TruncateTime(initial) && DbFunctions.TruncateTime(x.entryFree.entryFree.entryFree.entryFree.date_entry_free.Value) <= DbFunctions.TruncateTime(final));

            if (supplierId > 0)
                EntryFreeList = EntryFreeList.Where(x => x.entryFree.entryFree.entryFree.supplier.supplier_id == supplierId);

            if (status > 0)
                EntryFreeList = EntryFreeList.Where(x => x.entryFree.entryFree.entryFree.entryFree.status == status);

            if (!string.IsNullOrWhiteSpace(part_number))
                EntryFreeList = EntryFreeList.Where(x => x.entryFree.entryFreeList.part_number == part_number);

            if (!string.IsNullOrWhiteSpace(deparment))
            {
                int level_header = Convert.ToInt32(deparment);
                EntryFreeList = EntryFreeList.Where(x => x.entryFreeItem.level_header == level_header);
            }

            if (!string.IsNullOrWhiteSpace(family))
            {
                int level1code = Convert.ToInt32(family);
                EntryFreeList = EntryFreeList.Where(x => x.entryFreeItem.level1_code == level1code);
            }

            if (!string.IsNullOrWhiteSpace(currency))
                EntryFreeList = EntryFreeList.Where(x => x.entryFree.entryFree.entryFree.entryFree.currency == currency);
            
            var n = EntryFreeList.Select(x => new EntryFreeModel
            {
                Date = x.entryFree.entryFree.entryFree.entryFree.date_entry_free != null ? x.entryFree.entryFree.entryFree.entryFree.date_entry_free.Value.ToShortDateString() : "",                            
                Id = x.entryFree.entryFree.entryFree.entryFree.id,
                SubType = x.entryFree.entryFree.entryFree.entryFree.subtype_register,
                Type = x.entryFree.entryFree.entryFree.entryFree.type_register,
                SupplierId = x.entryFree.entryFree.entryFree.entryFree.supplier_id,
                Supplier = x.entryFree.entryFree.entryFree.supplier.business_name,
                Commentary = x.entryFree.entryFree.entryFree.entryFree.commentary,
                Reference = x.entryFree.entryFree.entryFree.entryFree.reference,
                User = x.entryFree.entryFree.entryFree.entryFree.cuser,
                Status = x.entryFree.entryFree.entryFree.entryFree.status ?? 1,
                Site = x.entryFree.entryFree.sites.site_name
            }).ToList();
            
            return n.GroupBy(g => g.Reference).Select(s => s.First()).ToList();*/

            var consultaGenerico = @"
                SELECT convert(varchar, ENTRY_FREE_HEAD.date_entry_free, 23) 'Date',
                ENTRY_FREE_HEAD.id 'Id',
                ENTRY_FREE_HEAD.subtype_register 'SubType',
                ENTRY_FREE_HEAD.type_register 'Type',
                ENTRY_FREE_HEAD.supplier_id 'SupplierId',
                MA_SUPPLIER.business_name 'Supplier',
                ENTRY_FREE_HEAD.commentary 'Commentary',
                ENTRY_FREE_HEAD.reference 'Reference',
                ENTRY_FREE_HEAD.cuser 'User',
                ENTRY_FREE_HEAD.status 'Status',
                SITES.site_name 'Site'
                FROM ENTRY_FREE_HEAD 
                INNER JOIN MA_SUPPLIER on ENTRY_FREE_HEAD.supplier_id = MA_SUPPLIER.supplier_id
                INNER JOIN SITES on ENTRY_FREE_HEAD.site_code = SITES.site_code
                INNER JOIN ENTRY_FREE_LIST on ENTRY_FREE_HEAD.id = ENTRY_FREE_LIST.id 
                INNER JOIN ITEM on ENTRY_FREE_LIST.part_number = ITEM.part_number 
                WHERE ENTRY_FREE_HEAD.date_entry_free BETWEEN @initial AND @final ";

            //Parametros
            List<SqlParameter> parameters = new List<SqlParameter>
            {
                new SqlParameter("@initial", initial),
                new SqlParameter("@final", final)
            };

            if (supplierId > 0)
            {
                consultaGenerico += " and MA_SUPPLIER.supplier_id = @supplier_id ";
                parameters.Add(new SqlParameter("@supplier_id", supplierId));
            }

            if (status > 0)
            {
                consultaGenerico += " and ENTRY_FREE_HEAD.status = @status ";
                parameters.Add(new SqlParameter("@status", status));
            }

            if (!string.IsNullOrWhiteSpace(part_number))
            {
                consultaGenerico += " and ENTRY_FREE_LIST.part_number = @part_number ";
                parameters.Add(new SqlParameter("@part_number", part_number));
            }

            if (!string.IsNullOrWhiteSpace(deparment) && deparment != "0")
            {
                int level_header = Convert.ToInt32(deparment);
                consultaGenerico += " and ITEM.level_header = @deparment ";
                parameters.Add(new SqlParameter("@deparment", level_header));
            }

            if (!string.IsNullOrWhiteSpace(family) && family != "0")
            {
                int level1code = Convert.ToInt32(family);
                consultaGenerico += " and ITEM.level1_code = @family ";
                parameters.Add(new SqlParameter("@family", level1code));
            }

            if (!string.IsNullOrWhiteSpace(currency))
            {
                consultaGenerico += " and ENTRY_FREE_HEAD.currency = @currency ";
                parameters.Add(new SqlParameter("@currency", currency));
            }

            var model = _context.Database.SqlQuery<EntryFreeModel>(consultaGenerico, parameters.ToArray()).ToList();

            return model;
        }

        public List<PurchaseOrderItemDetailReportGeneral> EntryFreeListDetail(DateTime date1, DateTime date2, int status, int supp, string partNumber, string department, string family, string currency)
        {
            var consultaGenerico = @"
                SELECT 'ENTRADA: SIN CARGO' TransationType,
                CASE WHEN EH.status = 9 THEN 'Terminado'
	                 WHEN EH.status = 8 THEN 'Cancelado'
                END AS Item_Status, MC1.class_name Department, MC2.class_name Family,
                EL.part_number PartNumber, I.part_description DescriptionC,
                I.unit_size, EH.currency Currency, EH.id Folio, MA.business_name Supplier, EL.quantity Quantity,
                ISNULL(EL.entry_free_price, 0) CostUnit,
                CAST(ISNULL(EL.entry_free_price, 0)*EL.quantity AS DECIMAL(18,4)) SubTotal,
                CAST(ISNULL(EL.entry_free_iva, 0)*EL.quantity AS DECIMAL(18,4)) IVA,
                CAST(ISNULL(EL.entry_free_ieps, 0)*EL.quantity AS DECIMAL(18,4)) IEPS,
                (CAST(ISNULL(EL.entry_free_price, 0)*EL.quantity AS DECIMAL(18,2)) + CAST(ISNULL(EL.entry_free_ieps, 0)*EL.quantity AS DECIMAL(18,4)) + CAST(ISNULL(EL.entry_free_iva, 0)*EL.quantity AS DECIMAL(18,4))) Amount,
                CONVERT(VARCHAR,EH.date_entry_free,101) MerchandiseEntry
                FROM ENTRY_FREE_LIST EL
                INNER JOIN ENTRY_FREE_HEAD EH ON EH.id = EL.entry_free_id
                INNER JOIN MA_SUPPLIER MA ON MA.supplier_id = EH.supplier_id
                INNER JOIN ITEM I ON I.part_number = EL.part_number
                INNER JOIN MA_TAX M1 ON M1.tax_code = I.part_iva_sale
                INNER JOIN MA_TAX M2 ON M2.tax_code = I.part_ieps
                INNER JOIN MA_CLASS MC1 ON MC1.class_id = I.level_header
                INNER JOIN MA_CLASS MC2 ON MC2.class_id = I.level1_code
                WHERE CONVERT(date, EL.cdate, 101) BETWEEN @date1 AND @date2 ";

            //Parametros
            List<SqlParameter> parameters = new List<SqlParameter>();
            parameters.Add(new SqlParameter("@date1", date1.Date));
            parameters.Add(new SqlParameter("@date2", date2.Date));
            if (status != 0)
            {
                consultaGenerico += " and EH.status = @status ";
                parameters.Add(new SqlParameter("@status", status));
            }
            if (!string.IsNullOrWhiteSpace(department))
            {
                consultaGenerico += " and I.level_header = @depart";
                parameters.Add(new SqlParameter("@depart", Convert.ToInt32(department)));
            }
            if (!string.IsNullOrWhiteSpace(family))
            {
                consultaGenerico += " and I.level1_code = @fam";
                parameters.Add(new SqlParameter("@fam", Convert.ToInt32(family)));
            }
            if (!string.IsNullOrWhiteSpace(partNumber))
            {
                consultaGenerico += " and EL.part_number = @part_number";
                parameters.Add(new SqlParameter("@part_number", partNumber));
            }
            if (!string.IsNullOrWhiteSpace(currency))
            {
                consultaGenerico += " and EH.currency = @currency";
                parameters.Add(new SqlParameter("@currency", currency));
            }
            if (supp != 0)
            {
                consultaGenerico += " AND EH.supplier_id = @supplier";
                parameters.Add(new SqlParameter("@supplier", supp));
            }
            return _context.Database.SqlQuery<PurchaseOrderItemDetailReportGeneral>(consultaGenerico, parameters.ToArray()).ToList();
        }

        public List<EntryFreeModel> GetFoliosBySupplierAndFechaRange(DateTime fechaIni, DateTime fechaFin)
        {
            return _context.ENTRY_FREE_HEAD.Join(_context.MA_SUPPLIER, x => x.supplier_id, y => y.supplier_id, (x, y) => new { x, y }).Where(x => x.x.date_entry_free >= fechaIni && x.x.date_entry_free <= fechaFin && x.x.status == 1)
                .Select(x => new EntryFreeModel
                {
                    Id = x.x.id,
                    Type = (x.y.commercial_name + " - " + x.x.type_register)
                }).ToList();
        }

        public int GetTypeEntryFree(int EntryFreeId)
        {
            try
            {
                var EntryFree = _context.ENTRY_FREE_HEAD.Where(x => x.id == EntryFreeId).SingleOrDefault();
                return EntryFree.type_register == "Con registro en inventario" ? 0 : 1;
            }
            catch (Exception)
            {
                return 1002;
            }
        }

        public int UpdateEntryFree(int EntryFreeId, string User)
        {
            try
            {
                var EntryFree = _context.ENTRY_FREE_HEAD.Where(x => x.id == EntryFreeId).SingleOrDefault();
                if (EntryFree.status != 9 && EntryFree.status != 8)
                {
                    EntryFree.status = 9;
                    EntryFree.udate = DateTime.Now;
                    EntryFree.uuser = User;
                    EntryFree.program_id = "EMSC001.cshtml";
                    _context.SaveChanges();
                    return 0;
                }
                return 1001;
            }
            catch (Exception)
            {
                return 8001;
            }
        }

        public int NewUpdateEntryFree(int EntryFreeId, string User)
        {
            try
            {
                var EntryFree = _context.ENTRY_FREE_HEAD.Where(x => x.id == EntryFreeId).SingleOrDefault();
                if (EntryFree.status != 9)
                {
                    EntryFree.status = 9;
                    EntryFree.udate = DateTime.Now;
                    EntryFree.uuser = User;
                    EntryFree.program_id = "EMSC002.cshtml";
                    _context.SaveChanges();
                    return 0;
                }
                return 1001;
            }
            catch (Exception)
            {
                return 8001;
            }
        }

        public StoreProcedureResult StoreProcedureEntryFree(int EntryFreeId, string User)
        {
            return _context.Database.SqlQuery<StoreProcedureResult>(@"DECLARE	@return_value int,  @Document nvarchar(10)
            EXEC	@return_value = [dbo].[sproc_GR_without_cost]
		            @entry_free_id	 = @EntryFreeId,
		            @cuser = @User,
		            @program_id = @ProgramId, 
                    @Document = @Document OUTPUT 
            SELECT 'Document' = @Document, 'ReturnValue' = @return_value",
            new SqlParameter("@EntryFreeId", EntryFreeId),
            new SqlParameter("@User", User),
            new SqlParameter("@ProgramId", "EMSC001.cshtml")).SingleOrDefault();
        }
    }
}