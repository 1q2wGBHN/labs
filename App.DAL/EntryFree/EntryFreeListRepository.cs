﻿using App.Entities;
using App.Entities.ViewModels.Android;
using System;

namespace App.DAL.EntryFree
{
    public class EntryFreeListRepository
    {
        private readonly DFL_SAIEntities _context;

        public EntryFreeListRepository()
        {
            _context = new DFL_SAIEntities();
        }

        public void PostEntryFreeList(EntryFreeListModel EntryFree, int EntryFreeId, int position, string User)
        {
            var EntryFreeList = new ENTRY_FREE_LIST()
            {
                program_id = "Android",
                cuser = User,
                cdate = DateTime.Now,
                part_number = EntryFree.Parth_Number,
                quantity = EntryFree.Quantity,
                entry_free_id = EntryFreeId,
                entry_free_ieps = EntryFree.IEPS,
                entry_free_iva = EntryFree.IVA,
                entry_free_price = EntryFree.base_cost
            };
            _context.ENTRY_FREE_LIST.Add(EntryFreeList);
            _context.SaveChanges();
        }
    }
}