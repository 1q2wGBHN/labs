﻿using App.Entities;
using App.Entities.ViewModels.StorageLocation;
using System;
using System.Collections.Generic;
using System.Linq;

namespace App.DAL.StorageLocation
{
    public class StorageLocationRepository
    {
        private readonly DFL_SAIEntities _context;

        public StorageLocationRepository()
        {
            _context = new DFL_SAIEntities();
        }

        public List<StorageLocationModel> GetAllLocationsInStatusActive()
        {
            var locaciones = _context.STORAGE_LOCATION.Where(x => x.active_flag == true)
                .Select(x => new StorageLocationModel
                {
                    site_code = x.site_code,
                    storage_location = x.storage_location_name,
                    storage_type = x.storage_type,
                    active_flag = x.active_flag.Value
                }).ToList();
            return locaciones;
        }

        public List<StorageLocationModel> GetAllLocations()
        {
            var locaciones = _context.STORAGE_LOCATION
                .Select(x => new StorageLocationModel
                {
                    site_code = x.site_code,
                    storage_location = x.storage_location_name,
                    storage_type = x.storage_type,
                    active_flag = x.active_flag.Value
                }).ToList();
            return locaciones;
        }

        public StorageLocationModel GetLocationById(string storageId)
        {
            return _context.STORAGE_LOCATION.Where(x => x.storage_location_name == storageId).Select(x => new StorageLocationModel
            {
                site_code = x.site_code,
                storage_location = x.storage_location_name,
                storage_type = x.storage_type,
                active_flag = x.active_flag.Value
            }).FirstOrDefault();
        }

        public STORAGE_LOCATION GetLocationByNameAndSite(string storage, string site)
        {
            return _context.STORAGE_LOCATION.Where(x => x.storage_location_name == storage && x.site_code == site).FirstOrDefault();
        }

        public bool IsExistPlanogramLocation(string storage)
        {
            return _context.PLANOGRAM_LOCATION.Where(x => x.planogram_location1 == storage).Count() > 0;
        }

        public int AddLocation(STORAGE_LOCATION storage_location)
        {
            try
            {
                _context.STORAGE_LOCATION.Add(storage_location);
                return _context.SaveChanges();
            }
            catch (Exception ex)
            {
                var msg = ex.Message;
                return 0;
            }
        }

        public int UpdateLocation(STORAGE_LOCATION storage_location)
        {
            try
            {
                _context.Entry(storage_location).State = System.Data.Entity.EntityState.Modified;
                return _context.SaveChanges();
            }
            catch (Exception ex)
            {
                var msg = ex.Message;
                return 0;
            }
        }

        public List<StorageLocationModel> GetLocationsByType(string Type, string Site)
        {
            var locaciones = _context.STORAGE_LOCATION
                .Where(x => x.storage_type == Type && x.site_code == Site && x.active_flag == true)
                .Select(x => new StorageLocationModel
                {
                    site_code = x.site_code,
                    storage_location = x.storage_location_name,
                    storage_type = x.storage_type,
                    active_flag = x.active_flag.Value
                }).ToList();
            return locaciones;
        }

        public StorageLocationModel GetLocationaMain(string storage_type, string site_code)
        {
            return _context.STORAGE_LOCATION
                .Where(w => w.storage_type == storage_type && w.site_code == site_code && w.active_flag == true)
                .OrderBy(o => o.cdate)
                .Select(s => new StorageLocationModel
                {
                    site_code = s.site_code,
                    storage_location = s.storage_location_name,
                    storage_type = s.storage_type
                }).FirstOrDefault();
        }
    }
}