﻿using App.Entities;
using App.Entities.ViewModels.StorageLocation;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Validation;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Linq;
using App.Entities.ViewModels.SalesPriceChange;

namespace App.DAL.StorageLocation
{
    public class PlanogramLocationRepository
    {
        private readonly DFL_SAIEntities _context;

        public PlanogramLocationRepository()
        {
            _context = new DFL_SAIEntities();
        }

        public List<PlanogramLocationModel> GetAllPlanogramLocations()
        {
            var locaciones = _context.PLANOGRAM_LOCATION.Join(_context.ITEM, planogram => planogram.part_number, item => item.part_number, (planogram, item) => new { planogram, item })
            .Select(x => new PlanogramLocationModel
            {
                planogram_location = x.planogram.planogram_location1,
                part_number = x.planogram.part_number,
                part_description = x.item.part_description,
                bin1 = x.planogram.bin1,
                bin2 = x.planogram.bin2,
                bin3 = x.planogram.bin3,
                quantity = x.planogram.quantity.Value,
                planogram_description = x.planogram.planogram_description,
                planogram_type = x.planogram.planogram_type,
                planogram_status = x.planogram.planogram_status
            }).ToList();
            return locaciones;
        }

        public List<PlanogramLocationModel> GetLocationsPlanogramByInventoryActive(string id)
        {
            var r = _context.INVENTORY_DETAIL.Where(x => x.id_inventory_header == id).Select(x => new PlanogramLocationModel
            {
                planogram_location = x.storage_location.Trim()
            }).Distinct().ToList();
            return r;
        }

        public PLANOGRAM_LOCATION GetPlanogramLocationByLocationAndProduct(string location, string partNumber)
        {
            return _context.PLANOGRAM_LOCATION.Where(x => x.planogram_location1 == location && x.part_number == partNumber).FirstOrDefault();
        }

        public int AddPlanogramLocation(PLANOGRAM_LOCATION location)
        {
            try
            {
                _context.PLANOGRAM_LOCATION.Add(location);
                return _context.SaveChanges();
            }
            catch (DbEntityValidationException dbEx)
            {
                foreach (var validationErrors in dbEx.EntityValidationErrors)
                {
                    foreach (var validationError in validationErrors.ValidationErrors)
                    {
                        Trace.TraceInformation("Property: {0} Error: {1}",
                                                validationError.PropertyName,
                                                validationError.ErrorMessage);
                    }
                }
                return 0;
            }
            catch (Exception ex)
            {
                var msg = ex.Message;
                return 0;
            }
        }

        public int UpdatePlanogramLocation(PlanogramLocationModel location, string oldLocation, string oldPartNumber)
        {
            try
            {
                var DateReturn = _context.Database.ExecuteSqlCommand(@"update PLANOGRAM_LOCATION set planogram_location=@planogram_location, part_number=@part_number, bin1=@bin1,bin2=@bin2,
                bin3=@bin3,quantity=@quantity,planogram_description=@planogram_description,planogram_type=@planogram_type,
                uuser=@uuser,udate=GETDATE(),program_id=@program_id 
                where planogram_location =@planogram_locationEdit and part_number =@part_numberEdit",
                new SqlParameter("planogram_location", location.planogram_location),
                new SqlParameter("part_number", location.part_number),
                new SqlParameter("bin1", location.bin1),
                new SqlParameter("bin2", location.bin2),
                new SqlParameter("bin3", location.bin3),
                new SqlParameter("quantity", location.quantity),
                new SqlParameter("planogram_description", location.planogram_description),
                new SqlParameter("planogram_type", location.planogram_type ?? ""),
                new SqlParameter("uuser", location.uuser),
                new SqlParameter("program_id", location.program_id),
                new SqlParameter("part_numberEdit", oldPartNumber),
                new SqlParameter("planogram_locationEdit", oldLocation));
                return DateReturn;
            }
            catch (Exception ex)
            {
                var msg = ex.Message;
                return 0;
            }
        }

        public PlanogramLocationModel getPlanogramByPartNumber(string part_number)
        {
            return _context.PLANOGRAM_LOCATION.Where(x => x.part_number == part_number)
                .Select(s => new PlanogramLocationModel
                {
                    planogram_location = s.planogram_location1
                }).FirstOrDefault();
        }

        public List<string> PlanogramLocation1()
        {
            return _context.PLANOGRAM_LOCATION.Select(e => e.bin1).Distinct().OrderBy(e => e).ToList();
        }

        public List<string> PlanogramLocation2(string loc1)
        {
            return _context.PLANOGRAM_LOCATION.Where(e => e.bin1 == loc1).Select(e => e.bin2).Distinct().OrderBy(e => e).ToList();
        }

        public List<Location3> PlanogramLocation3(string loc1, string loc2)
        {
            var r = _context.PLANOGRAM_LOCATION.Where(e => e.bin1 == loc1 && e.bin2 == loc2)
                .Select(e => new Location3 { code = e.bin3, part_number = e.part_number }).Distinct().OrderBy(e => e.code).ToList();
            return r;
        }

        public Locations PlanogramLocations(string loc1, string loc2)
        {
            var r = new Locations();
            r.loc1 = PlanogramLocation1();
            if (!string.IsNullOrEmpty(loc1) && !string.IsNullOrEmpty(loc2))
            {
                r.loc2 = PlanogramLocation2(loc1);
                r.loc3 = PlanogramLocation3(loc1, loc2);
            }
            else if (!string.IsNullOrEmpty(loc1) && string.IsNullOrEmpty(loc2))
            {
                r.loc2 = PlanogramLocation2(loc1);
                r.loc3 = r.loc2.Any() ? PlanogramLocation3(loc1, r.loc2.First()) : new List<Location3>();
            }
            else
            {
                r.loc2 = r.loc1.Any() ? PlanogramLocation2(r.loc1.First()) : new List<string>();
                r.loc3 = r.loc2.Any() ? PlanogramLocation3(loc1, r.loc2.First()) : new List<Location3>();
            }

            return r;
        }

        public PlanogramLocationModel GetLocation(string loc1, string loc2, string loc3, string part_number)
        {
            var r = _context.PLANOGRAM_LOCATION
                .Join(_context.BARCODES, e => e.part_number, e => e.part_number, (location, barcodes) => new { location, barcodes })
                .FirstOrDefault(e => e.location.planogram_location1 == loc1 + loc2 + loc3 && e.location.part_number == part_number);
            if (r == null) return null;
            return new PlanogramLocationModel
            {
                planogram_location = r.location.planogram_location1,
                part_number = r.location.part_number,
                barcode = r.barcodes.part_barcode,
                part_description = _context.ITEM.FirstOrDefault(e => e.part_number == r.location.part_number)?.part_number,
                bin1 = r.location.bin1,
                bin2 = r.location.bin2,
                bin3 = r.location.bin3,
                quantity = r.location.quantity ?? 0,
                planogram_description = r.location.planogram_description,
                planogram_type = r.location.planogram_type,
                planogram_status = r.location.planogram_status,
                cdate = r.location.cdate,
                cuser = r.location.cuser,
                udate = r.location.udate,
                uuser = r.location.uuser,
                program_id = r.location.program_id
            };
        }

        public List<PlanogramTagViewModel> TagsWithFilter(PlanogramFilter filter)
        {
            var query = _context.PLANOGRAM_LOCATION.AsQueryable();
            if (!string.IsNullOrEmpty(filter.loc1))
                query = query.Where(e => e.bin1 == filter.loc1);

            if (!string.IsNullOrEmpty(filter.loc2))
                query = query.Where(e => e.bin2 == filter.loc2);

            if (!string.IsNullOrEmpty(filter.loc3))
                query = query.Where(e => e.bin3 == filter.loc3);

            query = query.OrderBy(e => e.planogram_location1);

            var r = query.Select(e => new PlanogramTagViewModel
            {
                part_number = e.part_number,
                part_description = _context.ITEM.FirstOrDefault(e2 => e2.part_number == e.part_number).part_description,
                sale_price = _context.ITEM_VALUATION.FirstOrDefault(e2 => e2.part_number == e.part_number).price_sale ?? 0,
                unit_size = _context.ITEM.FirstOrDefault(e2 => e2.part_number == e.part_number).unit_size,
                loc1 = e.bin1,
                loc2 = e.bin2,
                loc3 = e.bin3,
                promotion = _context.PROMOTION_DETAIL
                    .Join(_context.PROMOTION_HEADER, e2 => e2.promotion_code, e2 => e2.promotion_code, (d, h) => new { d, h })
                    .Join(_context.BARCODES, e2 => e2.d.part_number, e2 => e2.part_number, (e2, b) => new { e2.d, e2.h, b })
                    .Join(_context.ITEM, e2 => e2.b.part_number, e2 => e2.part_number, (e2, i) => new { e2.d, e2.h, e2.b, i })
                    .Join(_context.MA_TAX, e2 => e2.i.part_iva_sale, e2 => e2.tax_code, (e2, tax) => new { e2.d, e2.h, e2.b, tax })

                    .Where(e2 => (e2.b.presentation_id != null ? (e2.b.presentation_id == e2.d.presentation_id) : (e2.d.part_number == e2.b.part_number))
                                 && DbFunctions.TruncateTime(DateTime.Today) >= DbFunctions.TruncateTime(e2.h.promotion_start_date)
                                 && DbFunctions.TruncateTime(DateTime.Today) <= DbFunctions.TruncateTime(e2.h.promotion_end_date))


                    .Select(e2 => new PromotionItemModel
                    {
                        promotion_price = e2.d.promotion_price * (1 + (e2.tax.tax_value ?? 0) / 100),
                        start_date = e2.h.promotion_start_date.ToString(),
                        end_date = e2.h.promotion_end_date.ToString()
                    })
                    .FirstOrDefault(),
            }).ToList();
            return r;
        }

        public class Location3
        {
            public string code { get; set; }
            public string part_number { get; set; }
        }

        public class Locations
        {
            public List<string> loc1 { get; set; }
            public List<string> loc2 { get; set; }
            public List<Location3> loc3 { get; set; }
        }

        public class PlanogramTagViewModel
        {
            public string part_number { get; set; }
            public string part_description { get; set; }
            public decimal sale_price { get; set; }
            public string loc1 { get; set; }
            public string loc2 { get; set; }
            public string loc3 { get; set; }
            public string unit_size { get; set; }
            public PromotionItemModel promotion { get; set; }
        }

        public class PlanogramFilter
        {
            public string loc1 { get; set; }
            public string loc2 { get; set; }
            public string loc3 { get; set; }
        }
    }
}