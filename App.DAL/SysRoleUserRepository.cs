﻿using App.Entities;
using App.Entities.ViewModels.Configuration;
using App.Entities.ViewModels.SysInfo;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;

namespace App.DAL
{
    public class SysRoleUserRepository
    {
        private readonly DFL_SAIEntities _context;

        public SysRoleUserRepository()
        {
            _context = new DFL_SAIEntities();
        }

        public ICollection<SYS_ROLE_MASTER> GetAllRolesOfUser(string EmployeeNumber)
        {
            return _context.SYS_ROLE_MASTER.Where(x => _context.SYS_ROLE_USER.Any(v => v.role_id == x.role_id && v.emp_no == EmployeeNumber)).OrderBy(x => x.role_id).ThenByDescending(x => x.role_id).ToList();
        }

        public ICollection<SYS_ROLE_MASTER> GetAllRolesAvailable(string EmployeeNumber)
        {
            return _context.SYS_ROLE_MASTER.Where(x => !_context.SYS_ROLE_USER.Any(v => v.role_id == x.role_id && v.emp_no == EmployeeNumber)).OrderBy(x => x.role_id).ThenByDescending(x => x.role_id).ToList();
        }

        public int AddRolesToUser(string EmployeeNumber, string Roles, string user)
        {
            try
            {
                var pagesArray = Roles.Split(',');
                var list = _context.SYS_ROLE_MASTER.Where(x => pagesArray.Contains(x.role_id) && !x.SYS_ROLE_USER.Any(y => y.emp_no == EmployeeNumber && pagesArray.Contains(y.role_id))).ToList();

                foreach (var id in list)
                {
                    SYS_ROLE_USER role = new SYS_ROLE_USER();
                    role.role_id = id.role_id;
                    role.emp_no = EmployeeNumber;
                    role.cdate = DateTime.Now;
                    role.cuser = user;
                    role.program_id = "ADMS004.cshtml";

                    _context.SYS_ROLE_USER.Add(role);
                    _context.SaveChanges();
                }
                return 1;
            }
            catch (Exception ex)
            {
                var msg = ex.Message;
                return 0;
            }
        }

        public int RemoveRolesFromUser(string EmployeeNumber, string roles)
        {
            try
            {
                var pagesArray = roles.Split(',');
                _context.SYS_ROLE_USER.RemoveRange(_context.SYS_ROLE_USER.Where(x => pagesArray.Contains(x.role_id) && x.emp_no == EmployeeNumber));
                _context.SaveChanges();
                return 1;
            }
            catch (Exception ex)
            {
                var msg = ex.Message;
                return 0;
            }
        }

        public List<SysPageMaster> GetPagesByUser(string emp_no)
        {
            var x = _context.SYS_ROLE_USER.Join(_context.SYS_ROLE_PAGE, SRU => SRU.role_id, SRP => SRP.role_id, (SRU, SRP) => new { SRU, SRP })
                .Join(_context.SYS_PAGE_MASTER, SPM2 => SPM2.SRP.page_id, SPM => SPM.page_id, (SPM2, SPM) => new { SPM2, SPM })
                .Where(w => w.SPM2.SRU.emp_no == emp_no && w.SPM.active_flag == true && w.SPM.type != "android")
                .Select(s => new SysPageMaster
                {
                    page_name = s.SPM.page_name,
                    url = s.SPM.url,
                    description = s.SPM.description
                }).Distinct().ToList();
            return x;
        }       

        public List<UserRoleModel>GetRolByEmpNo(string emp_no)
        {
            return _context.Database.SqlQuery<UserRoleModel>(@"select sru.role_id, srm.role_name from SYS_ROLE_MASTER as srm
                    JOIN SYS_ROLE_USER sru on srm.role_id = sru.role_id
                    where sru.emp_no = @EMP", new SqlParameter("EMP", emp_no)).ToList();
        }
    }
}