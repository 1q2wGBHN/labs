﻿using App.Entities;
using App.Entities.ViewModels.Donations;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Globalization;
using System.Linq;
using static App.Entities.ViewModels.Common.Constants;

namespace App.DAL.Donations
{
    public class DonationsSalesRepository
    {
        private DFL_SAIEntities _context;

        public DonationsSalesRepository()
        {
            _context = new DFL_SAIEntities();
        }

        public List<DonationsTicketsHeader> GetReportInfo(DonationsTicketsIndex Model)
        {
            DateTime DateStart = DateTime.ParseExact(Model.StartDate, DateFormat.INT_DATE, CultureInfo.InvariantCulture);
            DateTime DateEnd = DateTime.ParseExact(Model.EndDate, DateFormat.INT_DATE, CultureInfo.InvariantCulture);

            var ReportList = new List<DonationsTicketsHeader>();

            ReportList = (from ds in _context.DFLPOS_DONATION_SALE
                          join dpm in _context.DFLPOS_DONATIONS_PAYMENT_METHOD on ds.ds_id equals dpm.reference
                          where DbFunctions.TruncateTime(ds.datee) >= DateStart
                                && DbFunctions.TruncateTime(ds.datee) <= DateEnd
                                && (string.IsNullOrEmpty(Model.Cashier) || ds.userr_id == Model.Cashier)
                          select new
                          {
                              Date = ds.datee,
                              FirstName = ds.USER_MASTER.first_name,
                              LastName = ds.USER_MASTER.last_name,
                              Total = ds.total,
                              DonationId = ds.ds_id,
                              Pos_Id = dpm.pos_id
                          }).Distinct().AsEnumerable().Select(c => new DonationsTicketsHeader
                          {
                              Date = c.Date.ToString(DateFormat.INT_DATE),
                              Time = c.Date.ToString("HH:mm:ss"),
                              Cashier = string.Format("{0} {1}", c.FirstName, c.LastName),
                              Total = c.Total,
                              DonationSaleId = c.DonationId,
                              CheckoutNo = c.Pos_Id
                          }).ToList();

            return ReportList;
        }
    }
}
