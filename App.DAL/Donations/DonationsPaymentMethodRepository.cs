﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;
using static App.Entities.ViewModels.Common.Constants;
using App.Entities;
using App.Entities.ViewModels.Donations;
using System.Globalization;

namespace App.DAL.Donations
{
    public class DonationsPaymentMethodRepository
    {
        private DFL_SAIEntities _context;

        public DonationsPaymentMethodRepository()
        {
            _context = new DFL_SAIEntities();
        }

        public List<DonationDataDetail> GetDonationsDetail (string UserId, string Date)
        {
            var DataInfo = new List<DonationDataDetail>();
            DateTime SelectedDate = DateTime.ParseExact(Date, DateFormat.INT_DATE, CultureInfo.InvariantCulture);

            DataInfo = (from dpm in _context.DFLPOS_DONATIONS_PAYMENT_METHOD
                        join d in _context.DFLPOS_DONATIONS on dpm.donation_id equals d.donation_id
                        where DbFunctions.TruncateTime(dpm.sale_date) == SelectedDate && dpm.userr_id == UserId
                        select new 
                        {
                            Hour = dpm.sale_date.Value,
                            Sale = dpm.sale_id.Value,
                            CheckOutNo = dpm.pos_id,
                            Amount = dpm.amount.Value,
                            PaymentMethod = dpm.PAYMENT_METHOD.pm_name
                        }).AsEnumerable().Select(c => new DonationDataDetail {
                            Hour = c.Hour.ToString("hh:mm:ss tt"),
                            Sale = c.Sale,
                            CheckOutNo = c.CheckOutNo,
                            Amount = c.Amount,
                            PaymentMethod = c.PaymentMethod
                        }).ToList();

            return DataInfo;
        }
    }
}
