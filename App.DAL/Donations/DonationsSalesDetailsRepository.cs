﻿using App.Entities;
using App.Entities.ViewModels.Donations;
using System.Collections.Generic;
using System.Linq;

namespace App.DAL.Donations
{
    public class DonationsSalesDetailsRepository
    {
        private DFL_SAIEntities _context;

        public DonationsSalesDetailsRepository()
        {
            _context = new DFL_SAIEntities();
        }

        public List<DonationsTicketsDetail> GetDonationsTicketsDetails(string DonationSaleId)
        {
            var DataList = new List<DonationsTicketsDetail>();
            DataList = (from dsd in _context.DFLPOS_DONATION_SALE_DETAIL
                        where dsd.ds_id == DonationSaleId
                        select new DonationsTicketsDetail
                        {
                            Description = dsd.DFLPOS_DONATION_ITEMS.item_description,
                            Quantity = dsd.item_qty,
                            UnitPrice = dsd.item_unit_price,
                            TotalPrice = dsd.item_unit_price * dsd.item_qty
                        }).ToList();
            return DataList;
        }
    }
}
