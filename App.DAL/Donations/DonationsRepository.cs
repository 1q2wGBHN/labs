﻿using App.Entities;
using App.Entities.ViewModels.Donations;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Globalization;
using System.Linq;
using static App.Entities.ViewModels.Common.Constants;

namespace App.DAL.Donations
{
    public class DonationsRepository
    {
        private DFL_SAIEntities _context;

        public DonationsRepository()
        {
            _context = new DFL_SAIEntities();
        }

        public List<DFLPOS_DONATIONS> GetCampaingDonations()
        {
            return _context.DFLPOS_DONATIONS.ToList();
        }

        public DFLPOS_DONATIONS GetCampaignByDonationId(int DonationId)
        {
            return _context.DFLPOS_DONATIONS.Find(DonationId);
        }

        public List<DonationData> GetDonationsReportData(DonationIndex Model)
        {
            DateTime DateStart = DateTime.ParseExact(Model.StartDate, DateFormat.INT_DATE, CultureInfo.InvariantCulture);
            DateTime DateEnd = DateTime.ParseExact(Model.EndDate, DateFormat.INT_DATE, CultureInfo.InvariantCulture);
            var ReportList = new List<DonationData>();

            var DataInfo = (from d in _context.DFLPOS_DONATIONS
                            join dpm in _context.DFLPOS_DONATIONS_PAYMENT_METHOD on d.donation_id equals dpm.donation_id
                            //join ex in _context.EXCHANGE_CURRENCY_COMMERCIAL on DbFunctions.TruncateTime(dpm.sale_date) equals DbFunctions.TruncateTime(ex.currency_date)
                            where DbFunctions.TruncateTime(dpm.sale_date) >= DateStart
                                  && DbFunctions.TruncateTime(dpm.sale_date) <= DateEnd
                                  && (string.IsNullOrEmpty(Model.Cashier) || dpm.userr_id == Model.Cashier)
                                  && (string.IsNullOrEmpty(Model.Campaign) || d.name == Model.CampaignName)
                            select new
                            {
                                Date = DbFunctions.TruncateTime(dpm.sale_date.Value),
                                Cashier = dpm.USER_MASTER.first_name + " " + dpm.USER_MASTER.last_name,
                                AmountMXN = dpm.PAYMENT_METHOD.currency == Currencies.MXN ? dpm.amount.Value : 0,
                                AmountUSD = dpm.PAYMENT_METHOD.currency == Currencies.USD ? dpm.amount.Value : 0,
                                //CurrencyRate = dpm.DFLPOS_SALES.DFLPOS_SALES_DETAIL.Where(s => DbFunctions.TruncateTime(s.trans_date) == DbFunctions.TruncateTime(dpm.sale_date)).FirstOrDefault().currency_rate,//dpm.sale_id != 0 ? dpm.DFLPOS_SALES.DFLPOS_SALES_DETAIL.FirstOrDefault().currency_rate : dpm.DFLPOS_SALES.DFLPOS_SALES_DETAIL.Where(s => DbFunctions.TruncateTime(s.trans_date) == DbFunctions.TruncateTime(dpm.sale_date)).FirstOrDefault().currency_rate,
                                CashierId = dpm.userr_id
                            }).ToList();

            if (DataInfo.Count > 0)
            {
                ReportList = (from d in DataInfo
                              group d by new { d.Cashier, d.Date, d.CashierId } into gp
                              select new
                              {
                                  Date = gp.Key.Date.Value.ToString(DateFormat.INT_DATE),
                                  gp.Key.Cashier,
                                  AmountMXN = gp.Sum(x => x.AmountMXN),
                                  AmountUSD = gp.Sum(x => x.AmountUSD),
                                  CurrencyRate = (from c in _context.EXCHANGE_CURRENCY_COMMERCIAL where c.currency_date <= gp.Key.Date.Value select new { c.price, c.currency_date }).OrderByDescending(d => d.currency_date).Select(x => x.price).FirstOrDefault(),
                                  gp.Key.CashierId
                                  //Total = gp.Sum(x => x.AmountMXN) + (gp.Sum(x => x.AmountUSD) * gp.Key.CurrencyRate)
                              }).AsEnumerable().Select(c => new DonationData
                              {
                                  Date = c.Date,
                                  Cashier = c.Cashier,
                                  AmountMXN = c.AmountMXN,
                                  AmountUSD = c.AmountUSD,
                                  CurrencyRate = c.CurrencyRate,
                                  CashierId = c.CashierId,
                                  Total = c.AmountMXN + (c.AmountUSD * c.CurrencyRate)
                              }).ToList();
            }
            return ReportList;
        }
    }
}
