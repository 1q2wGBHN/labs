﻿using App.Entities;
using App.Entities.ViewModels.Android;
using App.Entities.ViewModels.Item;
using System;
using System.Collections.Generic;
using System.Linq;

namespace App.DAL.BlindCount
{
    public class BlindCountRepository
    {
        private readonly DFL_SAIEntities _context;

        public BlindCountRepository()
        {
            _context = new DFL_SAIEntities();
        }

        public DateTime? GetBlincCountExists(string purchase_no) //Tomar la fecha del ultimo escaneo
        {
            var date_blind_count = _context.BLIND_COUNT.OrderByDescending(o => o.udate).Where(w => w.purchase_no == purchase_no).ToList();
            if (date_blind_count.Count() > 0)
            {
                if (date_blind_count[0].udate != null)
                    return date_blind_count[0].udate;
                else
                {
                    var new_date_blind = date_blind_count.OrderByDescending(o => o.cdate).ToList();
                    return new_date_blind[0].cdate;
                }
            }
            else
                return null;

        }
        public BLIND_COUNT GetBlindCountByOCItemBarcodeSite(string purchase_no, string part_number, string barcode, string site)
        {
            return _context.BLIND_COUNT.Where(f => f.purchase_no == purchase_no && f.part_number == part_number && f.site_code == site || f.purchase_no == purchase_no && f.part_barcode == barcode && f.site_code == site)
                .Select(x => x).SingleOrDefault();
        }

        public List<ItemBlindCount> GetBlindCountListview(string purchase_no, string site)
        {
            return _context.BLIND_COUNT
                .Join(_context.ITEM, i => i.part_number, ii => ii.part_number, (i, ii) => new { i, ii })
                .Where(f => f.i.purchase_no == purchase_no && f.i.site_code == site)
                .OrderBy(o => o.i.item_position)
                .Select(x => new ItemBlindCount
                {
                    part_number = x.i.part_number,
                    part_description = x.ii.part_description,
                    quantity = x.i.quantity ?? 0
                }).ToList();
        }

        public List<BlindCountModel> GetBlindCountByOC(string purchase_no)
        {
            return _context.BLIND_COUNT.Where(f => f.purchase_no == purchase_no)
                .Select(x => new BlindCountModel
                {
                    blind_count_id = x.blind_count_id,
                    purchase_no = x.purchase_no,
                    site_code = x.site_code,
                    part_number = x.part_number,
                    part_barcode = x.part_barcode,
                    quantity = x.quantity.ToString()
                }).ToList();
        }

        public bool RemoveZeroBlindCount(string purchase_no)
        {
            var blindCount = _context.BLIND_COUNT.Where(w => w.purchase_no == purchase_no && w.quantity == 0).ToList();
            if (blindCount.Count() > 0)
            {
                try
                {
                    _context.BLIND_COUNT.RemoveRange(blindCount);
                    _context.SaveChanges();
                }
                catch (Exception e)
                {
                    var msg = e.Message;
                    return false;
                }
            }
            return true;

        }

        public int UpdateBlindCount(BLIND_COUNT blindCount)
        {
            _context.Entry(blindCount).State = System.Data.Entity.EntityState.Modified;
            return _context.SaveChanges();
        }

        public bool AddBlindCount(BLIND_COUNT blindCount)
        {
            try
            {
                _context.BLIND_COUNT.Add(blindCount);
                _context.SaveChanges();
                return true;
            }
            catch (Exception ex)
            {
                var msg = ex.Message;
                return false;
            }
        }
    }
}