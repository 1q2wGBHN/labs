﻿using App.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using App.Entities.ViewModels.Currency;
using System.Globalization;

namespace App.DAL.Currency
{
    public class CurrencyRepository
    {
        private DFL_SAIEntities _context;

        public CurrencyRepository()
        {
            _context = new DFL_SAIEntities();
        }
        public List<Currencys> GetCurrencys()
        {
            return (from cu in _context.CURRENCY
                    select new Currencys
                    {
                        cur_code = cu.cur_code,
                        cur_name = cu.cur_name,
                        cur_symbol = cu.cur_symbol
                    }).ToList();
        }

        public List<CURRENCY> CurrencyList()
        {
            return _context.CURRENCY.ToList();
        }

        public decimal GetCurrencyRateByDate(string date)
        {
            DateTime _Date = DateTime.ParseExact(date, "dd/MM/yyyy", CultureInfo.InvariantCulture);
            
            var _CurrencyRate = _context.EXCHANGE_CURRENCY_COMMERCIAL.Where(C => C.currency_date <= _Date).OrderByDescending(O => O.currency_date).Select(S => S.price).FirstOrDefault();
           
                return _CurrencyRate;
          
        }
    }
    
}
