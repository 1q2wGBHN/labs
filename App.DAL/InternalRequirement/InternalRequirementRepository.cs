﻿using App.Entities.ViewModels.InternalRequirement;
using App.GlobalEntities;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;

namespace App.DAL.InternalRequirement
{
    public class InternalRequirementRepository
    {
        private readonly GlobalERPEntities _contextGlobal;

        public InternalRequirementRepository()
        {
            _contextGlobal = new GlobalERPEntities();
        }

        public List<InternalRequirementModel> GetAllItemsCreditors()
        {
            try
            {
                var item = _contextGlobal.Database.SqlQuery<InternalRequirementModel>(@"select ITEM_CREDITORS.part_number as part_number,ITEM_CREDITORS.description as description,ITEM_CREDITORS.type as type , ITEM_CREDITORS_CATEGORY.name as category , ITEM_SUPPLIER_CREDITORS.supplier_part_number_creditors as id_item_supplier , MA_SUPPLIER.commercial_name as supplier_name, ISNULL(ITEM_CREDITORS_PRICE.price,0) as price, ITEM_CREDITORS_CATEGORY.buyer_division as buyer_division from ITEM_CREDITORS
                    join ITEM_CREDITORS_CATEGORY on ITEM_CREDITORS.category = ITEM_CREDITORS_CATEGORY.category_id
                    join ITEM_SUPPLIER_CREDITORS on ITEM_SUPPLIER_CREDITORS.part_number_creditors = ITEM_CREDITORS.part_number
                    join MA_SUPPLIER on ITEM_SUPPLIER_CREDITORS.supplier_id = MA_SUPPLIER.supplier_id
                    LEFT JOIN ITEM_CREDITORS_PRICE on ITEM_CREDITORS_PRICE.supplier_part_number_creditors = ITEM_SUPPLIER_CREDITORS.supplier_part_number_creditors
                    where ITEM_CREDITORS.active_flag = 1 and ITEM_CREDITORS.requisition_flag = 1 and ITEM_SUPPLIER_CREDITORS.flag_active = 1 and ITEM_CREDITORS_PRICE.flag_active = 1 and ITEM_CREDITORS.type = 'Producto'").ToList();
                return item;
            }
            catch (Exception ex)
            {
                var msg = ex.Message;
                return null;
            }
        }

        public List<InternalRequirementDetailPOModel> GetRequirementsAll()
        {
            try
            {
                var item = _contextGlobal.Database.SqlQuery<InternalRequirementDetailPOModel>(@"SELECT DISTINCT
	                INTERNAL_REQUIREMENT_DETAIL.supplier_id,
	                MA_SUPPLIER.commercial_name AS name_creditor,
	                SUM(INTERNAL_REQUIREMENT_DETAIL.quantity) AS quantity_total,
	                SUM(INTERNAL_REQUIREMENT_DETAIL.total) AS total_total
	                FROM INTERNAL_REQUIREMENT_HEADER 
		                JOIN INTERNAL_REQUIREMENT_DETAIL ON INTERNAL_REQUIREMENT_DETAIL.folio = INTERNAL_REQUIREMENT_HEADER.folio
		                JOIN MA_SUPPLIER ON MA_SUPPLIER.supplier_id = INTERNAL_REQUIREMENT_DETAIL.supplier_id
	                WHERE INTERNAL_REQUIREMENT_HEADER.requirement_status = 0
	                GROUP BY INTERNAL_REQUIREMENT_DETAIL.supplier_id, MA_SUPPLIER.commercial_name;").ToList();
                return item;
            }
            catch (Exception ex)
            {
                var msg = ex.Message;
                return null;
            }
        }

        public int AddInternalRequirementHeader(INTERNAL_REQUIREMENT_HEADER model)
        {
            try
            {
                var x = _contextGlobal.INTERNAL_REQUIREMENT_HEADER.Add(model);
                _contextGlobal.SaveChanges();
                var id = x.folio;
                return id;
            }
            catch (Exception ex)
            {
                var msg = ex.Message;
                return 0;
            }
        }

        public bool AddInternalRequirementDetail(InternalRequirementDetailPOModel model)
        {
            try
            {
                _contextGlobal.Database.ExecuteSqlCommand(@"INSERT INTO INTERNAL_REQUIREMENT_DETAIL VALUES ('" + model.folio + "','" + model.supplier_part_number_creditors + "','" + model.quantity + "',NULL,'" + model.cuser + "',GETDATE(),null,null,'PURC005.cshtml',NULL);");
                return true;
            }
            catch (Exception e)
            {
                var msg = e.Message;
                return false;
            }
        }

        public int GetCountRequirementFolios()
        {
            return _contextGlobal.INTERNAL_REQUIREMENT_HEADER.Where(x => x.requirement_status == 0).Count();
        }

        public decimal GetCountRequirementsQuantity()
        {
            return _contextGlobal.INTERNAL_REQUIREMENT_DETAIL
                .Join(_contextGlobal.INTERNAL_REQUIREMENT_HEADER, d => d.folio, h => h.folio, (d, h) => new { d, h })
                .Where(w => w.h.requirement_status == 0)
                .Sum(x => x.d.quantity);
        }

        public List<InternalRequirementPOModel> GetAllRequirementsHeaderBySite(string site, DateTime DateInit, DateTime DateFin)
        {
            try
            {
                var item = _contextGlobal.Database.SqlQuery<InternalRequirementPOModel>(@"select folio ,CAST(requirement_status as varchar(10)) as requirement_status,cuser,CONVERT(nvarchar(10), cdate, 103) as cdate from INTERNAL_REQUIREMENT_HEADER where site_code = '" + site + "'  and requirement_type = 'INTERNO' and cdate >= '" + DateInit.Date.Year + "-" + DateInit.Date.Month + "-" + DateInit.Date.Day + " 00:00:00' and cdate <= '" + DateFin.Date.Year + "-" + DateFin.Date.Month + "-" + DateFin.Date.Day + " 23:59:00' order by folio desc;").ToList();
                return item;
            }
            catch (Exception ex)
            {
                var msg = ex.Message;
                return null;
            }
        }

        public List<InternalRequirementPOModel> GetAllRequirementsHeaderBySiteByDate(string site, DateTime DateInit, DateTime DateFin)
        {
            try
            {
                var item = _contextGlobal.Database.SqlQuery<InternalRequirementPOModel>(@"select folio ,CAST(requirement_status as varchar(10)) as requirement_status,cuser,CONVERT(nvarchar(10), cdate, 103) as cdate from INTERNAL_REQUIREMENT_HEADER where site_code = '" + site + "'  and requirement_type = 'INTERNO' and cdate >= '" + DateInit.Date.Year + "-" + DateInit.Date.Month + "-" + DateInit.Date.Day + " 00:00:00' and cdate <= '" + DateFin.Date.Year + "-" + DateFin.Date.Month + "-" + DateFin.Date.Day + " 23:59:00' order by folio desc;").ToList();
                return item;
            }
            catch (Exception ex)
            {
                var msg = ex.Message;
                return null;
            }
        }

        public List<InternalRequirementDetailPOModel> GetAllRequirementsDetails(string folio)
        {
            try
            {
                var item = _contextGlobal.Database.SqlQuery<InternalRequirementDetailPOModel>(@"select  ITEM_CREDITORS.part_number as part_number,ITEM_CREDITORS.description as part_description , ITEM_CREDITORS_CATEGORY.description as category_name , MA_SUPPLIER.commercial_name as supplier_name, ISNULL(ITEM_CREDITORS_PRICE.price,0) as price, INTERNAL_REQUIREMENT_DETAIL.quantity as quantity from ITEM_CREDITORS
                    join ITEM_CREDITORS_CATEGORY on ITEM_CREDITORS.category = ITEM_CREDITORS_CATEGORY.category_id
                    join ITEM_SUPPLIER_CREDITORS on ITEM_SUPPLIER_CREDITORS.part_number_creditors = ITEM_CREDITORS.part_number
                    join MA_SUPPLIER on ITEM_SUPPLIER_CREDITORS.supplier_id = MA_SUPPLIER.supplier_id
                    JOIN INTERNAL_REQUIREMENT_DETAIL on ITEM_SUPPLIER_CREDITORS.supplier_part_number_creditors = INTERNAL_REQUIREMENT_DETAIL.supplier_part_number_creditors
                    LEFT JOIN ITEM_CREDITORS_PRICE on ITEM_CREDITORS_PRICE.supplier_part_number_creditors = ITEM_SUPPLIER_CREDITORS.supplier_part_number_creditors
                    WHERE ITEM_CREDITORS_PRICE.flag_active = 1 and INTERNAL_REQUIREMENT_DETAIL.folio = @folio ;",
                new SqlParameter("folio", folio)).ToList();
                return item;
            }
            catch (Exception ex)
            {
                var msg = ex.Message;
                return null;
            }
        }

        public List<InternalRequirementDetailPOModel> GetRequirementsInSiteAll()
        {
            return _contextGlobal.INTERNAL_REQUIREMENT_HEADER.Where(w => w.requirement_status == 0)
                .Select(s => new InternalRequirementDetailPOModel
                {
                    site_name = s.site_code
                })
                .Distinct()
                .ToList();
        }

        public List<InternalRequirementDetailPOModel> GetRequirementsInSupplierAll()
        {
            try
            {
                var item = _contextGlobal.Database.SqlQuery<InternalRequirementDetailPOModel>(@"select distinct INTERNAL_REQUIREMENT_DETAIL.supplier_id from INTERNAL_REQUIREMENT_DETAIL
					JOIN INTERNAL_REQUIREMENT_HEADER ON INTERNAL_REQUIREMENT_HEADER.folio = INTERNAL_REQUIREMENT_DETAIL.folio
					where INTERNAL_REQUIREMENT_HEADER.requirement_status = 0;").ToList();
                return item;
            }
            catch (Exception ex)
            {
                var msg = ex.Message;
                return null;
            }
        }

        public List<InternalRequirementDetailPOModel> GetRequirementsDetailsBySupplierBySite(int supplier_id, string site_code)
        {
            try
            {
                var item = _contextGlobal.Database.SqlQuery<InternalRequirementDetailPOModel>(@"SELECT INTERNAL_REQUIREMENT_DETAIL.folio,
                        INTERNAL_REQUIREMENT_DETAIL.part_number,
                        ITEM.part_description,
                        INTERNAL_REQUIREMENT_DETAIL.quantity,
                        INTERNAL_REQUIREMENT_DETAIL.total
                        FROM INTERNAL_REQUIREMENT_DETAIL
                        JOIN INTERNAL_REQUIREMENT_HEADER ON INTERNAL_REQUIREMENT_HEADER.folio = INTERNAL_REQUIREMENT_DETAIL.folio
                        JOIN ITEM ON ITEM.part_number = INTERNAL_REQUIREMENT_DETAIL.part_number
                        WHERE INTERNAL_REQUIREMENT_HEADER.requirement_status = 0 AND INTERNAL_REQUIREMENT_DETAIL.supplier_id = @supplier_id AND INTERNAL_REQUIREMENT_HEADER.site_code = @site_code;",
                new SqlParameter("supplier_id", supplier_id),
                new SqlParameter("site_code", site_code)).ToList();
                return item;
            }
            catch (Exception ex)
            {
                var msg = ex.Message;
                return null;
            }
        }

        public List<InternalRequirementModel> SearchItemsCreditors(string search)
        {
            var itemCreditors = from i in _contextGlobal.ITEM_CREDITORS
                                join cat in _contextGlobal.ITEM_CREDITORS_CATEGORY on i.category equals cat.category_id
                                join isup in _contextGlobal.ITEM_SUPPLIER_CREDITORS on i.part_number equals isup.part_number_creditors
                                join sup in _contextGlobal.MA_SUPPLIER on isup.supplier_id equals sup.supplier_id
                                join price in _contextGlobal.ITEM_CREDITORS_PRICE on isup.supplier_part_number_creditors equals price.supplier_part_number_creditors
                                    into tt
                                from price2 in tt.DefaultIfEmpty()
                                where i.active_flag == true && i.requisition_flag == true && isup.flag_active == true && price2.flag_active
                                where i.description.Contains(search) || i.part_number.Contains(search) || cat.description.Contains(search) || sup.commercial_name.Contains(search)
                                select new InternalRequirementModel
                                {
                                    part_number = i.part_number,
                                    description = i.description,
                                    category = cat.name,
                                    id_item_supplier = isup.supplier_part_number_creditors,
                                    supplier_name = sup.commercial_name,
                                    price = price2 != null ? price2.price : 0,
                                    estimate_price = price2 != null ? price2.price : 0,

                                };
            var r = itemCreditors.ToList();
            return r;
        }
    }
}