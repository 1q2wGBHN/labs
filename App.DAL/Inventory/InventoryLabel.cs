﻿using App.Entities;
using App.Entities.ViewModels.Inventory;
using System;
using System.Linq;
using System.Collections.Generic;
using System.Data.SqlClient;

namespace App.DAL.Inventory
{
    public class InventoryLabel
    {
        private readonly DFL_SAIEntities _context;

        public InventoryLabel()
        {
            _context = new DFL_SAIEntities();
        }

        public int GetLastLabel(string id_inventory)
        {
            var number = _context.INVENTORY_LABEL.Where(w => w.id_inventory == id_inventory)
                .Select(s => new InventoryLabelModel
                {
                    serial_partnumer = s.serial_number,
                    id_inventory = s.id_inventory,
                    last_number = s.last_number
                }).ToList();

            if (number == null || number.Count() < 1)
                return 0;
            else
                return number.Max(s => s.last_number);
        }

        public int SendInventoryLabels(InventoryLabelModel label)
        {
            try
            {
                var model = new INVENTORY_LABEL
                {
                    id_inventory = label.id_inventory,
                    serial_number = label.serial_partnumer,
                    last_number = label.last_number,
                    cuser = label.cuser,
                    cdate = DateTime.Now,
                    program_id = "TagManagerInventory",
                };
                _context.INVENTORY_LABEL.Add(model);
                _context.SaveChanges();
                return 1;
            }
            catch (Exception e)
            {
                var r = e.Message;
                return 0;
            }
        }

        public INVENTORY_LABEL LabelById(string serial_number)
        {
            return _context.INVENTORY_LABEL.SingleOrDefault(w => w.serial_number == serial_number);
        }
        public List<string> GetEmptyLabels(string id)
        {
            return _context.Database.SqlQuery<string>(@"select il.serial_number from INVENTORY_LABEL il
             left join INVENTORY_PALLET_HEADER iph on il.serial_number = iph.serial_number
             where iph.serial_number is null
             and il.id_inventory = @id", new SqlParameter("id", id)).ToList();
        }
        public List<string> GetAllLabels(string id)
        {
            return _context.INVENTORY_LABEL.Where(x => x.id_inventory == id).Select(a => a.serial_number).ToList();
        }
        public PalletModel GetPalletHeaderByLabel(string serial_number)
        {
            return _context.INVENTORY_PALLET_HEADER.Where(x => x.serial_number == serial_number)
                .Select(a => new PalletModel
                {
                    storage_location_name = a.storage_location_name,
                    id_inventory = a.id_inventory,
                    pallet_status = a.pallet_status
                })
                .FirstOrDefault();
        }
    }
}