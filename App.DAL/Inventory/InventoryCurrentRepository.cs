﻿using App.Entities;
using App.Entities.ViewModels.Inventory;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;

namespace App.DAL.Inventory
{
    public class InventoryCurrentRepository
    {
        private readonly DFL_SAIEntities _context;

        public InventoryCurrentRepository()
        {
            _context = new DFL_SAIEntities();
        }

        public List<InventoryCurrentModel> GetInventoryDetail(int IdInventoryHeader)
        {
            return _context.Database.SqlQuery<InventoryCurrentModel>(@"select i.id_inventory_header as IdInventoryHeader, i.storage_location as Location,
                ISNULL(it.map_price, 0) as MapPrice, i.part_number as PartNumber, ite.part_description as PartDescription,
                i.quantity as Quantity, ISNULL(its.unrestricted, 0) as Stock
                from INVENTORY_DETAIL i
                left
                join ITEM_VALUATION it on it.part_number = i.part_number
                left
                  join ITEM ite on ite.part_number = i.part_number               
                left
                  join ITEM_STOCK its on its.part_number = i.part_number and its.storage_location = i.location
                where i.id_inventory_header = @id", new SqlParameter { ParameterName = "id", Value = IdInventoryHeader }).ToList();
        }

        public int GetPriceItem(string IdInventory, string Location, string User, string ProgramId)
        {
            return _context.Database.SqlQuery<int>(@"  
            DECLARE	@return_value int
            exec @return_value = sp_fill_inventory_current @id_inventory, @location, @user,@program_id
            SELECT	'ReturnValue' = @return_value", new SqlParameter { ParameterName = "id_inventory", Value = IdInventory },
            new SqlParameter { ParameterName = "location", Value = Location },
            new SqlParameter { ParameterName = "user", Value = User },
            new SqlParameter { ParameterName = "program_id", Value = ProgramId }).SingleOrDefault();
        }

        public List<InventoryReportModel> GetInventoryDiferentDetail(bool PartNumberDiferent, string Location, int Departament, bool NotCount , int inventoryType , string idInventory /*,bool PalletsNoCount*/)
        { 
            var query = @" SELECT  b.part_number as part_number_detail ,a.part_number as part_number, isnull(E.quantityScrap,0) as 'quantityScrap', isnull(E.amountScrap,0) as 'amountScrap',
                        c.part_description as part_description,
						case 
						when not exists ((select  new_quantity from INVENTORY_ADJUSTMENT_HISTORY where id_inventory_header = a.id_inventory_header and part_number = a.part_number and adjustment_status = 9)) then 
						isnull(b.Detail_qty,0)
						else
						ISNULL((select  new_quantity from INVENTORY_ADJUSTMENT_HISTORY where id_inventory_header = a.id_inventory_header and part_number = a.part_number and adjustment_status = 9),0)
						end
						as quantity_detail,
                        ISNULL((select  new_quantity from INVENTORY_ADJUSTMENT_HISTORY where id_inventory_header = a.id_inventory_header and part_number = a.part_number and adjustment_status = 9),0) as  quantity_adjusten,
                        ISNULL((select  new_quantity - previous_quantity from INVENTORY_ADJUSTMENT_HISTORY where id_inventory_header = a.id_inventory_header and part_number = a.part_number and adjustment_status = 9),0) as quantity_adjusten_difference,
                        d.class_name as family,
            			a.quantity as quantity_current,
                        a.id_inventory_header as idInventory_header_current ,isnull(a.map_price,0) as map_price,
						(select commercial_name from MA_SUPPLIER where supplier_id = (
						select top 1 supplier_id from ITEM_SUPPLIER where part_number = A.part_number order by cdate desc)) as 'supplier'
                        FROM INVENTORY_CURRENT A 
                        FULL OUTER JOIN (SELECT id_inventory_header, part_number, sum(quantity) Detail_qty 
                        FROM INVENTORY_DETAIL locationWhere
                        GROUP BY id_inventory_header, part_number)B ON (a.id_inventory_header = b.id_inventory_header and A.part_number = b.part_number)
						FULL OUTER JOIN (SELECT DGI.part_number ,SUM(DGI.quantity) as quantityScrap , SUM(DGI.amount_doc) as amountScrap FROM DAMAGEDS_GOODS DG
                        JOIN DAMAGEDS_GOODS_ITEM DGI ON DG.damaged_goods_doc = DGI.damaged_goods_doc
                        JOIN ITEM I ON I.part_number = DGI.part_number
                        WHERE DG.process_type = 'SCRAP'
                        AND DGI.process_status in (6,9)
                        AND DG.process_status in (6,9)
                        AND cast(DGI.udate as date) BETWEEN (SELECT TOP 1 DATEADD(DAY, 1, start_date) from INVENTORY_HEADER WHERE inventory_type = " + inventoryType + @" and inventory_status = 9
                        order by start_date desc) AND GETDATE()
                        AND I.level_header in (SELECT level_ma_class FROM INVENTORY_TYPE_DETAILS WHERE id_inventory_type = " + inventoryType+ @")
                        GROUP BY DGI.part_number)E on E.part_number = a.part_number
                        INNER JOIN ITEM C ON A.part_number = C.part_number
                        INNER JOIN MA_CLASS D ON (c.level_header = d.class_id)
                        where a.id_inventory_header =  '" + idInventory+ "'  ";

            if (PartNumberDiferent)
                query += "and a.quantity <> ISNULL(b.Detail_qty,0)";
            if (NotCount)
                query += " and  ISNULL( b.Detail_qty,0) = 0 ";
            if (Departament > 0 && !string.IsNullOrWhiteSpace(Location.Trim()))
            {
                query += " and d.class_id = @departament and  @location in (select storage_location from INVENTORY_DETAIL where b.part_number = part_number)";
                return _context.Database.SqlQuery<InventoryReportModel>(query,
                    new SqlParameter { ParameterName = "location", Value = Location },
                    new SqlParameter { ParameterName = "departament", Value = Departament }).ToList();
            }
            if (Departament > 0)
            {
                query += " and d.class_id = @departament ";
                return _context.Database.SqlQuery<InventoryReportModel>(query,
                   new SqlParameter { ParameterName = "departament", Value = Departament }).ToList();
            }
            if (!string.IsNullOrWhiteSpace(Location.Trim()))
            {
                //query += " and  @location in (select storage_location from INVENTORY_DETAIL where b.part_number = part_number)";
                query = query.Replace("locationWhere", " where storage_location = '" + Location + "' ");
            }else
            {
                query = query.Replace("locationWhere", " ");
            }
            var r = _context.Database.SqlQuery<InventoryReportModel>(query).ToList();
            return r;
        }

        public List<InventoryReportModel> GetInventoryDiferentDetailOption(string PartNumber , string location)
        {
            var query = @"SELECT 	
                        distinct(isnull(b.id,0)) as IdInventoryDetail,
						case isnull(E.id_inventory_detail,0)
						when 0 then 
						case isnull(iu.user_no ,0)
						when 0 then (select concat(first_name,' ',last_name) from user_master where user_name =  b.cuser)
						else  iu.inventory_user end 
						else (select concat(first_name,' ',last_name) from user_master where user_name =  E.cuser) end as 'lastUser'
						,
						 pl.planogram_description as StorageDescription, b.part_number as part_number_detail ,a.part_number as part_number,
                        c.part_description as part_description,  isnull(b.Detail_qty,0) as quantity_detail,
                        ISNULL((select  new_quantity from INVENTORY_ADJUSTMENT_HISTORY where id_inventory_header = a.id_inventory_header and part_number = a.part_number and adjustment_status = 9),0) as  quantity_adjusten,
                        d.class_name as family,b.storage_location as location,a.quantity as quantity_current,
                        a.id_inventory_header as idInventory_header_current ,a.map_price as map_price
                        FROM INVENTORY_CURRENT A
                        FULL OUTER JOIN (SELECT id,id_inventory_header, part_number, sum(quantity) Detail_qty, storage_location  , user_no , cuser , quantity
                        FROM INVENTORY_DETAIL 
                        GROUP BY id_inventory_header, part_number, storage_location,id , user_no , cuser , quantity)B ON (a.id_inventory_header = b.id_inventory_header and A.part_number = b.part_number)
                        INNER JOIN ITEM C ON A.part_number = C.part_number
                        INNER JOIN MA_CLASS D ON (c.level1_code = d.class_id)
                        left join PLANOGRAM_LOCATION pl on pl.planogram_location = b.storage_location and pl.part_number = b.part_number
						left join INVENTORY_USERS iu on (iu.id_inventory_header = a.id_inventory_header and b.user_no = iu.user_no)
						FULL outer JOIN (select id_inventory_detail , cuser , after_quantity from INVENTORY_DETAIL_HISTORY)E ON (b.id = e.id_inventory_detail and b.quantity = e.after_quantity)
                        where a.id_inventory_header in  
                        (select id_inventory_header from INVENTORY_HEADER 
                        where inventory_status  != 0 and inventory_status  
                        != 8 and inventory_status  != 9 and inventory_status != 11 ) and   a.part_number = @partNumber";
            if (location != "0")
                query += " and B.storage_location = '" + location + "' ";


            return _context.Database.SqlQuery<InventoryReportModel>(query, new SqlParameter("@partNumber", PartNumber)).ToList();
        }

        public List<InventoryReportModel> GetInventoryDiferentPallet()
        {
            return _context.Database.SqlQuery<InventoryReportModel>(@"select inu.inventory_user as inventory_user,inu.emp_no as emp_no, a.serial_number as serial_number,c.part_description as part_description , storage_location_name as location, 
                c.part_number as part_number, quantity as quantity_detail  from INVENTORY_PALLET_HEADER a 
                inner join INVENTORY_PALLET_DETAIL b on b.serial_number = a.serial_number
                INNER JOIN ITEM C ON b.part_number = C.part_number
				inner join INVENTORY_USERS inu on  inu.user_no = a.user_no and a.id_inventory = inu.id_inventory_header
                where  a.pallet_status = 0").ToList();
        }

        public int ExistInventory(string IdInventory, string Location)
        {
            try
            {
                return _context.Database.SqlQuery<int>(@"select count(*) from ITEM it
                    left join ITEM_VALUATION itv on itv.part_number = it.part_number
                    left join (select unrestricted,part_number,ITEM_STOCK.storage_location from ITEM_STOCK left join STORAGE_LOCATION b on b.storage_type = @location where b.storage_location_name = storage_location) its on its.part_number = it.part_number 
                    where it.level_header in (select c.level_ma_class from INVENTORY_HEADER r inner join INVENTORY_TYPE_DETAILS c on c.id_inventory_type = r.inventory_type  where r.id_inventory_header = @id_inventory and c.active_details = 1) and isnull(it.combo_flag,0) <> 1 and isnull(it.extra_items_flag,0) <> 1", new SqlParameter { ParameterName = "id_inventory", Value = IdInventory },
            new SqlParameter { ParameterName = "location", Value = Location }).SingleOrDefault();
            }
            catch (Exception)
            {
                return 0;
            }
        }

        public int AvailableInventory(string IdInventory)
        {
            try
            {
                return _context.INVENTORY_HEADER.Where(x => x.id_inventory_header == IdInventory && x.inventory_status == 1).Count();
            }
            catch (Exception)
            {
                return 0;
            }
        }

        public int UpdateInventoryQuantity(string IdInventory, string PartNumber, int Quantity, string User, string location)
        {
            try
            {
                var Inventory = _context.INVENTORY_DETAIL.Where(x => x.id_inventory_header == IdInventory && x.part_number == PartNumber && x.storage_location == location).SingleOrDefault();
                if (Inventory != null)
                {
                    Inventory.quantity = Quantity;
                    Inventory.uuser = User;
                    Inventory.udate = DateTime.Now;
                    Inventory.program_id = "INVE005.cshtml";
                    _context.SaveChanges();
                    return 1;
                }
                return 2;
            }
            catch (Exception)
            {
                return 0;
            }
        }

        public int PostInveryDetailHistory(string IdInventory, string PartNumber, decimal Quantity, string User, string location)
        {
            try
            {
                var InventoryHeader = _context.INVENTORY_DETAIL.Where(x => x.id_inventory_header == IdInventory && x.part_number == PartNumber && x.storage_location == location).SingleOrDefault();
                if (InventoryHeader != null)
                {
                    _context.Database.ExecuteSqlCommand(@"INSERT INTO INVENTORY_DETAIL_HISTORY
                        VALUES (@idinventory, @after,@before,@partnumber,@storage,@cuser,@cdate,@programid);
                        update INVENTORY_DETAIL set quantity = @quantity where id_inventory_header = @idinventory and part_number = @partnumber and storage_location = @storage;",
                        new SqlParameter("@idinventory", IdInventory),
                        new SqlParameter("@after", Quantity),
                        new SqlParameter("@before", InventoryHeader.quantity),
                        new SqlParameter("@quantity", Quantity),
                        new SqlParameter("@partnumber", PartNumber),
                        new SqlParameter("@storage", location),
                        new SqlParameter("@cuser", User),
                        new SqlParameter("@cdate", DateTime.Now),
                        new SqlParameter("@programid", "INVE005.cshtml"));
                    return 3;
                }
                return 2;
            }
            catch (Exception)
            {
                return 108;
            }
        }

        public int NewPostInveryDetailHistory(int InventoryDetail, string IdInventory, string PartNumber, decimal Quantity, string User, string location)
        {
            try
            {
                var InventoryHeader = _context.INVENTORY_DETAIL.Where(x => x.id == InventoryDetail).SingleOrDefault();
                if (InventoryHeader != null)
                {
                    _context.Database.ExecuteSqlCommand(@"INSERT INTO INVENTORY_DETAIL_HISTORY
                        VALUES (@inventoryDetail, @idinventory, @after,@before,@partnumber,@storage,@cuser,getdate(),@programid);
                        update INVENTORY_DETAIL set quantity = @quantity where id = @inventoryDetail",
                        new SqlParameter("@idinventory", IdInventory),
                        new SqlParameter("@after", Quantity),
                        new SqlParameter("@before", InventoryHeader.quantity),
                        new SqlParameter("@quantity", Quantity),
                        new SqlParameter("@partnumber", PartNumber),
                        new SqlParameter("@storage", location),
                        new SqlParameter("@cuser", User),
                        new SqlParameter("@inventoryDetail", InventoryDetail),
                        new SqlParameter("@programid", "INVE005.cshtml"));
                    return 3;
                }
                return 2;
            }
            catch (Exception)
            {
                return 108;
            }
        }

        public int ExistHistoryInventoryDetail(string IdInventory, string PartNumber, string location)
        {
            var Inventory = _context.INVENTORY_DETAIL.Where(x => x.id_inventory_header == IdInventory && x.part_number == PartNumber && x.storage_location == location).SingleOrDefault();
            if (Inventory != null)
            {
                return _context.Database.SqlQuery<int>(@"select COUNT(*)  from INVENTORY_DETAIL_HISTORY where id_inventory_header = @idinventory and part_number = @partnumber and storage_location = @storage",
                    new SqlParameter("@idinventory", IdInventory),
                    new SqlParameter("@partnumber", PartNumber),
                    new SqlParameter("@storage", location)).SingleOrDefault();
            }
            return 1;
        }

        public List<InventoryDetailHistoryModel> InventoryHistoryDetailAll(int id)
        {


            return _context.Database.SqlQuery<InventoryDetailHistoryModel>(@"select  CONVERT(VARCHAR(10),idh.cdate, 101) as  Cdate,  idh.cuser as Cuser, idh.id_history as IdHistory , idh.id_inventory_header as  IdInventoryHeader ,  idh.part_number as PartNumber,  idh.after_quantity  as   Quantity,  idh.before_quantity as BeforeQuantity,  idh.storage_location as StorageLocation,
                    it.part_description  as   PartDescription
                    from INVENTORY_DETAIL_HISTORY idh inner join ITEM it on it.part_number = idh.part_number
                    where idh.id_inventory_header = @idinventory", new SqlParameter("@idinventory", id)).ToList();


            //return new List<InventoryDetailHistoryModel>();
        }

        public InventoryReportModel GetInventoryDiferentDetailItem(string Id, string PartNumber, string location)
        {
            var query = @" SELECT  b.part_number as part_number_detail ,a.part_number as part_number,
                        c.part_description as part_description,  isnull(b.Detail_qty,0) as quantity_detail,
                        ISNULL((select  new_quantity from INVENTORY_ADJUSTMENT_HISTORY where id_inventory_header = a.id_inventory_header and part_number = a.part_number and adjustment_status = 9),0) as  quantity_adjusten,

                        d.class_name as family,
            			a.quantity as quantity_current,
                        a.id_inventory_header as idInventory_header_current ,a.map_price as map_price
                        FROM INVENTORY_CURRENT A
                        FULL OUTER JOIN (SELECT id_inventory_header, part_number, sum(quantity) Detail_qty 
                        FROM INVENTORY_DETAIL 
                        GROUP BY id_inventory_header, part_number)B ON (a.id_inventory_header = b.id_inventory_header and A.part_number = b.part_number)
                        INNER JOIN ITEM C ON A.part_number = C.part_number
                        INNER JOIN MA_CLASS D ON (c.level1_code = d.class_id)
                        where a.id_inventory_header in  
                        (select id_inventory_header from INVENTORY_HEADER 
                        where inventory_status  != 0 and inventory_status  
                        != 8 and inventory_status  != 9 )   and a.part_number = @partNumber";
            var m = _context.Database.SqlQuery<InventoryReportModel>(query,
                new SqlParameter { ParameterName = "partNumber", Value = PartNumber }).SingleOrDefault();
            return m;
        }

        public InventoryType DateInventory(string IdInventoryHeader)
        {
            return _context.Database.SqlQuery<InventoryType>(@"
                select a.day,a.type_review,b.start_date from INVENTORY_HEADER b
                join INVENTORY_TYPE a on a.id_inventory_type = b.inventory_type 
                where b.id_inventory_header = @idInventoryHeader", new SqlParameter { ParameterName = "idInventoryHeader", Value = IdInventoryHeader }).SingleOrDefault();
        }
        public bool SyncInventory(string idInventory, string user)
        {
            try
            {
                _context.Database.ExecuteSqlCommand(@"UPDATE INVENTORY_CURRENT SET quantity = isnull(b.total_stock,0), map_price = isnull(b.map_price,0), uuser = '" + user + @"' , udate = GETDATE() , program_id = 'INVE005'
FROM INVENTORY_CURRENT A
INNER JOIN (
SELECT part_number, total_stock, map_price
FROM
(
SELECT A.part_number,a.total_stock, A.map_price
FROM ITEM_VALUATION A
INNER JOIN ITEM B ON (a.part_number = b.part_number)
WHERE level_header in (SELECT level_ma_class FROM INVENTORY_HEADER A
INNER JOIN INVENTORY_TYPE B ON (a.inventory_type = b.id_inventory_type)
INNER  JOIN INVENTORY_TYPE_DETAILS C ON (b.id_inventory_type = c.id_inventory_type)
WHERE A.id_inventory_header = " + idInventory + @"
GROUP BY level_ma_class)
)AA
)B ON (a.part_number = B.part_number)
WHERE id_inventory_header = " + idInventory);
            }
            catch (Exception e)
            {
                return false;
            }
            return true;
        }
        public List<List<InventoryReportModel>> GetInventoryDiferentDetailByDepartment(string id)
        {
            var query = @"SELECT b.part_number as part_number_detail ,a.part_number as part_number, isnull(E.quantityScrap, 0) as 'quantityScrap', isnull(E.amountScrap, 0) as 'amountScrap',
                        c.part_description as part_description,
						case 
						when not exists((select  new_quantity from INVENTORY_ADJUSTMENT_HISTORY where id_inventory_header = a.id_inventory_header and part_number = a.part_number and adjustment_status = 9)) then

                        isnull(b.Detail_qty, 0)
						else
						ISNULL((select  new_quantity from INVENTORY_ADJUSTMENT_HISTORY where id_inventory_header = a.id_inventory_header and part_number = a.part_number and adjustment_status = 9),0)
						end
                        as quantity_detail,
                        ISNULL((select  new_quantity from INVENTORY_ADJUSTMENT_HISTORY where id_inventory_header = a.id_inventory_header and part_number = a.part_number and adjustment_status = 9),0) as quantity_adjusten,
                        ISNULL((select  new_quantity - previous_quantity from INVENTORY_ADJUSTMENT_HISTORY where id_inventory_header = a.id_inventory_header and part_number = a.part_number and adjustment_status = 9),0) as quantity_adjusten_difference,
                        d.class_name as family,
            			a.quantity as quantity_current,
                        a.id_inventory_header as idInventory_header_current ,isnull(a.map_price, 0) as map_price,
						(select commercial_name from MA_SUPPLIER where supplier_id = (
                        select top 1 supplier_id from ITEM_SUPPLIER where part_number = A.part_number order by cdate desc)) as 'supplier'
                        FROM INVENTORY_CURRENT A
                        FULL OUTER JOIN (SELECT id_inventory_header, part_number, sum(quantity)Detail_qty
                        FROM INVENTORY_DETAIL
                        GROUP BY id_inventory_header, part_number)B ON (a.id_inventory_header = b.id_inventory_header and A.part_number = b.part_number)
						FULL OUTER JOIN(SELECT DGI.part_number, SUM(DGI.quantity) as quantityScrap, SUM(DGI.amount_doc) as amountScrap FROM DAMAGEDS_GOODS DG
                        JOIN DAMAGEDS_GOODS_ITEM DGI ON DG.damaged_goods_doc = DGI.damaged_goods_doc
                        JOIN ITEM I ON I.part_number = DGI.part_number
                        WHERE DG.process_type = 'SCRAP'
                        AND DGI.process_status in (6,9)
                        AND DG.process_status in (6,9)
                        AND cast(DGI.udate as date) BETWEEN(SELECT TOP 1 DATEADD(DAY, 1, start_date) from INVENTORY_HEADER WHERE inventory_type =
                        (select inventory_type from INVENTORY_HEADER where id_inventory_header = '" + id + @"') and inventory_status = 9
                        order by start_date desc) AND GETDATE()
                        AND I.level_header in (SELECT level_ma_class FROM INVENTORY_TYPE_DETAILS WHERE id_inventory_type =(select inventory_type from INVENTORY_HEADER where id_inventory_header = '" + id + @"'))
                        GROUP BY DGI.part_number)E on E.part_number = a.part_number
                        INNER JOIN ITEM C ON A.part_number = C.part_number
                        INNER JOIN MA_CLASS D ON(c.level_header = d.class_id)
                        where a.id_inventory_header = '" + id + "' order by family desc";

            

            
            var r = _context.Database.SqlQuery<InventoryReportModel>(query).ToList();
            var aa = r.GroupBy(u => u.family).Select(a => a.ToList()).ToList();

            return aa.OrderByDescending(a => a.Count).ToList();
        }
        public InventoryCurrentModel GetLastSync(string id)
        {
            var r = _context.Database.SqlQuery<InventoryCurrentModel>(@"select top 1 convert(varchar, ic.udate , 22) 'dateSync' , CONCAT(um.first_name,' ' , um.last_name) 'userName' from inventory_header ih
join INVENTORY_CURRENT ic on ih.id_inventory_header = ic.id_inventory_header
join USER_MASTER um on um.user_name = ic.uuser
where ih.id_inventory_header = '" + id+"'").FirstOrDefault();
            return r;
        }
        public InventoryCurrentModel GetScrapDatesActive(string id)
        {
            var r = _context.Database.SqlQuery<InventoryCurrentModel>(@"SELECT TOP 1 convert(varchar,DATEADD(DAY, 1, start_date), 101) 'startDate', convert(varchar, getdate(), 101) 'endDate' from INVENTORY_HEADER WHERE inventory_type =
                        (select inventory_type from INVENTORY_HEADER where id_inventory_header = '" + id+@"') and inventory_status = 9
                        order by start_date desc ").FirstOrDefault();
            return r;
        }

    }
}