﻿using App.Entities;
using App.Entities.ViewModels.Inventory;
using App.Entities.ViewModels.Item;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;

namespace App.DAL.Inventory
{
    public class InventoryDetailRepository
    {
        private readonly DFL_SAIEntities _context;

        public InventoryDetailRepository()
        {
            _context = new DFL_SAIEntities();
        }

        public bool ValidateBarcodeInventory(string barcode, string inventoryId)
        {
            if (_context.INVENTORY_DETAIL.Where(x => x.part_number == barcode & x.id_inventory_header == inventoryId).Count() > 0)
                return false;
            else
                return true;
        }

        public List<ItemModel> GetInventoryItems(int genericuser, string inventoryid)
        {
            var model = _context.INVENTORY_DETAIL
                .Join(_context.ITEM, id => id.part_number, it => it.part_number, (id, it) => new { id, it })
                .Where(x => x.id.user_no == genericuser & x.id.id_inventory_header == inventoryid)
                .Select(x => new ItemModel
                {
                    PathNumber = x.id.part_number,
                    Description = x.it.part_description,
                    Quantity = x.id.quantity,
                    id = x.id.id
                }).OrderByDescending(x => x.id).ToList();

            return model;
        }

        public bool SetInventoryCount(INVENTORY_DETAIL model, string user)
        {
            try
            {
                _context.INVENTORY_DETAIL.Add(model);
                _context.SaveChanges();

                return true;
            }
            catch (Exception ex)
            {
                string msg = ex.Message;
                return false;
            }
        }

        public List<InventoryFinalModel> GetAllItemsDetailById(string inventoryId)
        {
            return _context.INVENTORY_DETAIL
                .Join(_context.ITEM, i => i.part_number, ii => ii.part_number, (i, ii) => new { i, ii })
                .Join(_context.INVENTORY_USERS, u => u.i.user_no, uu => uu.user_no, (u, uu) => new { u, uu })
                .Join(_context.ITEM_VALUATION, v => v.u.i.part_number, vv => vv.part_number, (v, vv) => new { v, vv })
                .Where(w => w.v.u.i.id_inventory_header == inventoryId && w.v.uu.id_inventory_header == inventoryId)
                .Select(s => new InventoryFinalModel
                {
                    part_number = s.v.u.i.part_number,
                    part_description = s.v.u.ii.part_description,
                    counted = s.v.u.i.quantity,
                    total_amount = s.vv.map_price != null ? s.v.u.i.quantity * s.vv.map_price ?? 0 : 0,
                }).ToList();
        }

        public List<InventoryFinalModel> GetAllItemsDetailAdjustmentById(string inventoryId)
        {
            try
            {
                var item = _context.Database.SqlQuery<InventoryFinalModel>(@"SELECT INVENTORY_FINAL.part_number,
	                CASE
	                WHEN INVENTORY_FINAL.part_number in (SELECT part_number FROM INVENTORY_ADJUSTMENT_HISTORY WHERE part_number = INVENTORY_FINAL.part_number AND id_inventory_header = @inventory_id AND adjustment_status = 9) 
		                THEN (SELECT new_quantity FROM INVENTORY_ADJUSTMENT_HISTORY WHERE part_number = INVENTORY_FINAL.part_number AND id_inventory_header = @inventory_id AND adjustment_status = 9)
	                ELSE
		                counted
	                END 
	                AS counted, ITEM.part_description, INVENTORY_FINAL.map_price * INVENTORY_FINAL.counted AS total_amount FROM INVENTORY_FINAL
	                JOIN ITEM on ITEM.part_number = INVENTORY_FINAL.part_number 
	                WHERE INVENTORY_FINAL.id_inventory_header = @inventory_id AND counted > 0;",
                    new SqlParameter("inventory_id", inventoryId)).ToList();
                return item;
            }
            catch (Exception ex)
            {
                var msg = ex.Message;
                return null;
            }
        }

        public int GetAllItemsCount(string inventoryId)
        {
            var items = _context.INVENTORY_DETAIL.Where(w => w.id_inventory_header == inventoryId).ToList();
            return Convert.ToInt32(items.Sum(c => c.quantity));
        }

        public List<InventoryItemLocationModel> GetInventoryItemsByLocation(string id , string location , string type , string items)
        {
            var w = "";
            if (items != "" || location != "")
            {
                if (type == "location")
                    w = " and ltrim(id.storage_location) in (" + location + @") ";
                else
                    w = "and id.part_number in (" + items + @")";
            }
            var r = _context.Database.SqlQuery<InventoryItemLocationModel>(@"
            select id.id_inventory_header , id.part_number , i.part_description , id.quantity ,
            case id.user_no when 0 then (select CONCAT(first_name , ' ' , last_name) from USER_MASTER where user_name =  id.cuser) else
            (select inventory_user from INVENTORY_USERS where id_inventory_header = id.id_inventory_header and user_no = id.user_no) end as 'cuser',
            case id.user_no when 0 then 'WEB' else 'POCKET' end as 'captured',
            id.storage_location , convert(nvarchar(100),id.cdate,120) as 'cdate',
            (select top 1 map_price from INVENTORY_CURRENT
            where id_inventory_header = id.id_inventory_header and part_number = id.part_number) as cost
            from INVENTORY_DETAIL id
            join ITEM i on i.part_number = id.part_number
            where id.id_inventory_header =  '" + id+"' "+w+" and quantity > 0 ").ToList();
            return r;
        }
    }
}