﻿using App.Entities;
using App.Entities.ViewModels.Inventory;
using App.Entities.ViewModels.Item;
using App.Entities.ViewModels.Site;
using System;
using System.Collections.Generic;
using System.Data.Entity.Validation;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Linq;

namespace App.DAL.Inventory
{
    public class InventoryAdjustmentsRepository
    {
        private readonly DFL_SAIEntities _context;

        public InventoryAdjustmentsRepository()
        {
            _context = new DFL_SAIEntities();
        }

        //public List<DropItemModel> GetInventoryProductsDetailDrop(string IdInventoryHeader)
        //{
        //    return _context.Database.SqlQuery<DropItemModel>(@"select A.part_number AS PartNumber,part_description AS Description from INVENTORY_DETAIL AS A INNER JOIN ITEM AS B ON A.part_number=B.part_number
        //    where id_inventory_header = @id_inventory_header group by A.part_number,part_description", new SqlParameter { ParameterName = "id_inventory_header", Value = IdInventoryHeader }).ToList();
        //}

        public List<DropItemModel> GetInventoryProductsDetailDrop(string IdInventoryHeader)
        {
            return _context.Database.SqlQuery<DropItemModel>(@"select ic.part_number as PartNumber, part_description as Description from INVENTORY_CURRENT ic 
                join ITEM i on ic.part_number = i.part_number
                 where id_inventory_header = @id_inventory_header and ic.part_number not in (select part_number from inventory_adjustment_history where id_inventory_header = @id_inventory_header and adjustment_status = 9)", new SqlParameter { ParameterName = "id_inventory_header", Value = IdInventoryHeader }).ToList();
        }

        //public InventoryAdjustmentModel GetInventoryItemQuantity(string IdInventoryHeader, string PartNumber)
        //{
        //    return _context.Database.SqlQuery<InventoryAdjustmentModel>(@"select sum(quantity) as previous_quantity from INVENTORY_DETAIL 
        //    where id_inventory_header = @id_inventory_header and part_number =@part_number",
        //    new SqlParameter { ParameterName = "id_inventory_header", Value = IdInventoryHeader },
        //    new SqlParameter { ParameterName = "part_number", Value = PartNumber }).SingleOrDefault();
        //}

        public InventoryAdjustmentModel GetInventoryItemQuantity(string IdInventoryHeader, string PartNumber)
        {
            return _context.Database.SqlQuery<InventoryAdjustmentModel>(@"select isnull(sum(quantity),0) as previous_quantity from INVENTORY_DETAIL 
            where id_inventory_header = @id_inventory_header and part_number =@part_number",
            new SqlParameter { ParameterName = "id_inventory_header", Value = IdInventoryHeader },
            new SqlParameter { ParameterName = "part_number", Value = PartNumber }).SingleOrDefault();
        }

        public InventoryAdjustmentModel GetInventoryAdjustmentItemByInventoryAndPartNumber(string IdInventoryHeader, string PartNumber)
        {
            return _context.Database.SqlQuery<InventoryAdjustmentModel>(@"select id_inventory_header, part_number,previous_quantity,new_quantity,justification,evidence_document_base64,evidence_document_ext,adjustment_status,cuser,cdate,uuser,udate,program_id
            from INVENTORY_ADJUSTMENT_HISTORY where id_inventory_header = @id_inventory_header and part_number = @part_number",
            new SqlParameter { ParameterName = "id_inventory_header", Value = IdInventoryHeader },
            new SqlParameter { ParameterName = "part_number", Value = PartNumber }).SingleOrDefault();
        }

        public bool AddAdjusment(INVENTORY_ADJUSTMENT_HISTORY model)
        {
            try
            {
                _context.INVENTORY_ADJUSTMENT_HISTORY.Add(model);
                _context.SaveChanges();
                return true;
            }
            catch (DbEntityValidationException dbEx)
            {
                foreach (var validationErrors in dbEx.EntityValidationErrors)
                {
                    foreach (var validationError in validationErrors.ValidationErrors)
                    {
                        Trace.TraceInformation("Property: {0} Error: {1}",
                                                validationError.PropertyName,
                                                validationError.ErrorMessage);
                    }
                }
                return false;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public bool UpdateAdjusment(INVENTORY_ADJUSTMENT_HISTORY model)
        {
            try
            {
                var m = _context.INVENTORY_ADJUSTMENT_HISTORY
                    .Where(x => x.id_inventory_header == model.id_inventory_header && x.part_number == model.part_number && x.adjustment_status != 9)
                    .FirstOrDefault();
                m.justification = model.justification;
                m.new_quantity = model.new_quantity;
                m.uuser = model.uuser;
                m.udate = model.udate;
                m.program_id = model.program_id;
                m.adjustment_status = model.adjustment_status;
                _context.SaveChanges();
                return true;
            }
            catch (Exception e)
            {
                return false;
            }
        }

        public List<AdjustmentRequestModel> GetAdjustmentRequests()
        {
            var model = _context.Database.SqlQuery<AdjustmentRequestModel>
                (@"SELECT IAH.id_inventory_header 'InventoryHeaderId', IH.inventory_name 'InventoryName', IAH.part_number 'PartNumber', 
                 I.part_description 'ProductDescription', IAH.previous_quantity 'PreviousQuantity', IAH.new_quantity 'NewQuantity', 
                IAH.justification 'Justification', ISNULL(IAH.evidence_document_ext,'') 'Document',
				isnull(itm.map_price,0) as MapPrice , INF.unrestricted
                FROM INVENTORY_ADJUSTMENT_HISTORY IAH 
                INNER JOIN INVENTORY_HEADER IH ON IAH.id_inventory_header = IH.id_inventory_header 
                INNER JOIN ITEM I ON IAH.part_number = I.part_number 
				INNER JOIN INVENTORY_FINAL INF ON INF.id_inventory_header = IH.id_inventory_header AND INF.part_number = IAH.part_number
				inner join ITEM_VALUATION itm on itm.part_number = i.part_number
                WHERE IAH.adjustment_status = 0 AND IH.inventory_status = 3").ToList(); /*_context.Database.SqlQuery<AdjustmentRequestModel>
                (@"SELECT IAH.id_inventory_header 'InventoryHeaderId', IH.inventory_name 'InventoryName', IAH.part_number 'PartNumber', 
                 I.part_description 'ProductDescription', IAH.previous_quantity 'PreviousQuantity', IAH.new_quantity 'NewQuantity', 
                IAH.justification 'Justification', ISNULL(IAH.evidence_document_ext,'') 'Document'
                FROM INVENTORY_ADJUSTMENT_HISTORY IAH 
                INNER JOIN INVENTORY_HEADER IH ON IAH.id_inventory_header = IH.id_inventory_header 
                INNER JOIN ITEM I ON IAH.part_number = I.part_number 
                WHERE IAH.adjustment_status = 0 AND IH.inventory_status = 3 AND DATEDIFF(HOUR, IH.end_date,GETDATE()) < 36").ToList();*/

            return model;
        }

        public bool UpdateAdjustment(string InventoryHeaderId, string PartNumber, int AdjustmentStatus, string User, string ProgramId, out List<AdjustmentRequestModel> model)
        {
            try
            {
                var adjustment = _context.INVENTORY_ADJUSTMENT_HISTORY.Where(x => x.id_inventory_header == InventoryHeaderId & x.part_number == PartNumber & x.adjustment_status == 0).FirstOrDefault();
                if (adjustment != null)
                {
                    adjustment.adjustment_status = AdjustmentStatus;
                    adjustment.uuser = User;
                    adjustment.udate = DateTime.Now;
                    adjustment.program_id = ProgramId;
                    _context.SaveChanges();

                    model = GetAdjustmentRequests();
                    return true;
                }
                else
                {
                    model = null;
                    return false;
                }
            }
            catch (Exception ex)
            {
                string msg = ex.Message;
                model = null;
                return false;
            }
        }

        public AdjustmentRequestModel GetFile(string InventoryHeaderId, string PartNumber)
        {
            try
            {
                var model = _context.INVENTORY_ADJUSTMENT_HISTORY
                    .Where(x => x.id_inventory_header == InventoryHeaderId & x.part_number == PartNumber & x.evidence_document_base64 != null)
                    .Select(x => new AdjustmentRequestModel
                    {
                        File = x.evidence_document_base64,
                        FileName = x.evidence_document_ext
                    }).FirstOrDefault();

                return model;
            }
            catch (Exception ex)
            {
                string msg = ex.Message;
                return null;
            }
        }

        public bool fillInventoryFinal(string inventoryNo, string uuser, int typeAj)
        {
            try
            {
                var site = _context.SITE_CONFIG.Select(x => new SiteModel
                {
                    site_code = x.site_code
                }).FirstOrDefault();
                _context.Database.CommandTimeout = 100000000;
                var returnValues = _context.Database.SqlQuery<StoreProcedureResult>("DECLARE	@return_value int, @Document varchar(10) EXEC    @return_value = [dbo].[sproc_Inventory_Adjustment] @site_code = @site,	@id_inventory_header = @Inventory , @user = @User, @typeAj = @typeAj,  @program_id = @ProgramId, @Document = @Document OUTPUT SELECT 'Document' = @Document, 'ReturnValue' = @return_value"
                , new SqlParameter("@site", site.site_code)
                , new SqlParameter("@Inventory", inventoryNo)
                , new SqlParameter("@typeAj", typeAj)
                , new SqlParameter("@User", uuser)
                , new SqlParameter("@ProgramId", "INVE004.cshtml")).SingleOrDefault();
                if (returnValues.ReturnValue == 0 && returnValues.Document != null)
                    return true;
                else
                    return false;
            }
            catch (Exception ex)
            {
                string msg = ex.Message;
                return false;
            }
        }

        public List<AdjustmentRequestModel> historyInventoryAdjustment(int status)
        {
            try
            {
                var Adjustment = @"select iah.id_inventory_header as InventoryHeaderId, 
                    ih.inventory_name as InventoryName,
                    iah.part_number as PartNumber , 
                    it.part_description as ProductDescription,
                    iah.previous_quantity as PreviousQuantity ,
                    iah.new_quantity as NewQuantity,
                    iah.justification as Justification,
                    iah.adjustment_status as AdjustmentStatus,
                    --iah.flag_adjusted,
                    iah.cuser as Cuser,
                    iah.uuser as Uuser
                    from INVENTORY_ADJUSTMENT_HISTORY iah
                    inner
                    join INVENTORY_HEADER ih on ih.id_inventory_header = iah.id_inventory_header
                    and ih.inventory_status <> 9 and ih.inventory_status <> 8
                    inner join ITEM it on it.part_number = iah.part_number";
                if (status >= 0)
                {
                    Adjustment += " where adjustment_status = @status";
                    return _context.Database.SqlQuery<AdjustmentRequestModel>(Adjustment, new SqlParameter("@status", status)).ToList();
                }
                return _context.Database.SqlQuery<AdjustmentRequestModel>(Adjustment).ToList();
            }
            catch (Exception ex)
            {
                string msg = ex.Message;
                return null;
            }
        }

        public INVENTORY_ADJUSTMENT_HISTORY existInventoryAdjustment(string id, string part_number)
        {
            var r = _context.INVENTORY_ADJUSTMENT_HISTORY.Where(x => x.id_inventory_header == id && x.part_number == part_number).FirstOrDefault();
            return r;
        }
        public bool existInventoryAdjustmentInPending()
        {
            var r = _context.INVENTORY_ADJUSTMENT_HISTORY
                .Join(_context.INVENTORY_HEADER, iah => iah.id_inventory_header, ih => ih.id_inventory_header, (iah, ih) => new { iah, ih })
                .Where(x => x.ih.inventory_status == 3 && x.iah.adjustment_status == 0).Any();
            return r;
        }
        public string AdjustNegatives(string user)
        {
            try
            {
                var site = _context.SITE_CONFIG.Select(x => new SiteModel
                {
                    site_code = x.site_code
                }).FirstOrDefault();
                _context.Database.CommandTimeout = 100000000;
                var returnValues = _context.Database.SqlQuery<StoreProcedureResult>(@"
DECLARE	@return_value int,
        @result nvarchar(100)

EXEC    @return_value = [dbo].[sproc_Adjust_All_Negatives]
        @site_code = @site,
        @user = @userr,
        @program_id = @ProgramId,
        @result = @result OUTPUT

SELECT  @result as N'result'

SELECT  'ReturnValue' = @return_value"
                , new SqlParameter("@site", site.site_code)
                , new SqlParameter("@userr", user)
                , new SqlParameter("@ProgramId", "INVE013.cshtml")).SingleOrDefault();

                return returnValues.result;
            }
            catch (Exception ex)
            {
                string msg = ex.Message;
                return msg;
            }
        }
        public int InsertNegativeHeader(InventoryNegativeHeaderModel model , string user)
        {
            try
            {
                var x = _context.Database.SqlQuery<int>(@"
                INSERT INTO  INVENTORY_NEGATIVE_HEADER VALUES(
                @part_number , @cost,@unrestricted,@packing_size,@counted,@total_quantity,
                @total_amount,0,@reason,NULL,NULL,GETDATE(),@cuser,NULL,NULL,'INVE013.cshtml');
                SELECT cast(SCOPE_IDENTITY() as int);
                "
            , new SqlParameter("@part_number", model.part_number_header)
            , new SqlParameter("@cost", model.cost_header)
            , new SqlParameter("@unrestricted", model.unrestricted_header)
            , new SqlParameter("@packing_size", model.packing_size)
            , new SqlParameter("@counted", model.counted)
            , new SqlParameter("@total_quantity", model.total_quantity)
            , new SqlParameter("@total_amount", model.total_amount)
            , new SqlParameter("@reason", model.reason)
            , new SqlParameter("@cuser", user)).FirstOrDefault();

                return x;
            }
            catch (Exception e)
            {
                return 0;
            }
        }
        public bool InsertNegativeDetail(InventoryNegativeDetailModel model , string user)
        {
            try
            {
                var x = _context.Database.ExecuteSqlCommand(@"
                INSERT INTO INVENTORY_NEGATIVE_DETAIL VALUES(
                @id , @part_number,@cost , @unrestricted , @quantity , @amount , Null , @cuser , getdate() , null , null , 'INVE013.cshtml'
                )"
            , new SqlParameter("@id", model.id_inventory_negative)
            , new SqlParameter("@part_number", model.part_number_detail)
            , new SqlParameter("@cost", model.cost)
            , new SqlParameter("@unrestricted", model.unrestricted)
            , new SqlParameter("@quantity", model.quantity)
            , new SqlParameter("@amount", model.amount)
            , new SqlParameter("@cuser", user)
            );
                if (x == 1)
                    return true;
                else
                    return false;
            }
            catch (Exception e)
            {
                return false;
            }
        }
        public List<InventoryNegativeHeaderModel> GetAllNegativeHeader()
        {
            var r = _context.Database.SqlQuery<InventoryNegativeHeaderModel>(@"
             select ih.id_inventory_negative, ih.part_number_header , i.part_description , iv.unrestricted as 'unrestricted_actual',
            ih.unrestricted as 'unrestricted_header', ih.counted , ih.packing_size , ih.cost as 'cost_header' , ih.reason ,
			case (select COUNT (part_number_detail) from INVENTORY_NEGATIVE_DETAIL ihd where ihd.id_inventory_negative = ih.id_inventory_negative)
			when 1 then (select part_number_detail from INVENTORY_NEGATIVE_DETAIL ihd where ihd.id_inventory_negative = ih.id_inventory_negative) 
			else 'ContraAjuste Multiple' end as 'part_number_detail',
			case (select COUNT (part_number_detail) from INVENTORY_NEGATIVE_DETAIL ihd where ihd.id_inventory_negative = ih.id_inventory_negative)
			when 1 then (select (select part_description from ITEM where part_number = ihd.part_number_detail) 
			from INVENTORY_NEGATIVE_DETAIL ihd where ihd.id_inventory_negative = ih.id_inventory_negative) 
			else 'ContraAjuste Multiple' end as 'part_description_detail',
			case (select COUNT (part_number_detail) from INVENTORY_NEGATIVE_DETAIL ihd where ihd.id_inventory_negative = ih.id_inventory_negative)
			when 1 then (select (select unrestricted from ITEM_VALUATION where part_number = ihd.part_number_detail) 
			from INVENTORY_NEGATIVE_DETAIL ihd where ihd.id_inventory_negative = ih.id_inventory_negative) 
			else 0 end as 'unrestricted_detail',
			case (select COUNT (part_number_detail) from INVENTORY_NEGATIVE_DETAIL ihd where ihd.id_inventory_negative = ih.id_inventory_negative)
			when 1 then (select cost from INVENTORY_NEGATIVE_DETAIL ihd where ihd.id_inventory_negative = ih.id_inventory_negative) 
			else 0 end as 'cost_detail',
			case (select COUNT (part_number_detail) from INVENTORY_NEGATIVE_DETAIL ihd where ihd.id_inventory_negative = ih.id_inventory_negative)
			when 1 then (select quantity from INVENTORY_NEGATIVE_DETAIL ihd where ihd.id_inventory_negative = ih.id_inventory_negative) 
			else 0 end as 'quantity_detail',
			case (select COUNT (part_number_detail) from INVENTORY_NEGATIVE_DETAIL ihd where ihd.id_inventory_negative = ih.id_inventory_negative)
			when 1 then (select amount from INVENTORY_NEGATIVE_DETAIL ihd where ihd.id_inventory_negative = ih.id_inventory_negative) 
			else (select sum(amount) from INVENTORY_NEGATIVE_DETAIL ihd where ihd.id_inventory_negative = ih.id_inventory_negative)
			end as 'amount_detail'
			from INVENTORY_NEGATIVE_HEADER ih
            join ITEM_VALUATION iv on ih.part_number_header = iv.part_number
            join ITEM i on i.part_number = ih.part_number_header
            where ih.negative_status = 0").ToList();
            return r;
        }
        public List<InventoryNegativeHeaderModel> GetAllNegativeHeaderByDateAndStatus(DateTime dateInit , DateTime dateFin , int status)
        {
            var sta = "";
            if(status != 0)
            {
                sta = " and ih.negative_status = " + status + " ";
            }
            var r = _context.Database.SqlQuery<InventoryNegativeHeaderModel>(@"
            select ih.id_inventory_negative, ih.part_number_header , i.part_description , convert(nvarchar(100),ih.cdate,103) as 'negative_date',
            ih.unrestricted as 'unrestricted_header', ih.counted , ih.packing_size , ih.cost as 'cost_header' , ih.reason ,
			case (select COUNT (part_number_detail) from INVENTORY_NEGATIVE_DETAIL ihd where ihd.id_inventory_negative = ih.id_inventory_negative)
			when 1 then (select part_number_detail from INVENTORY_NEGATIVE_DETAIL ihd where ihd.id_inventory_negative = ih.id_inventory_negative) 
			else 'ContraAjuste Multiple' end as 'part_number_detail',
			case (select COUNT (part_number_detail) from INVENTORY_NEGATIVE_DETAIL ihd where ihd.id_inventory_negative = ih.id_inventory_negative)
			when 1 then (select (select part_description from ITEM where part_number = ihd.part_number_detail) 
			from INVENTORY_NEGATIVE_DETAIL ihd where ihd.id_inventory_negative = ih.id_inventory_negative) 
			else 'ContraAjuste Multiple' end as 'part_description_detail',
			case (select COUNT (part_number_detail) from INVENTORY_NEGATIVE_DETAIL ihd where ihd.id_inventory_negative = ih.id_inventory_negative)
			when 1 then (select cost from INVENTORY_NEGATIVE_DETAIL ihd where ihd.id_inventory_negative = ih.id_inventory_negative) 
			else 0 end as 'cost_detail',
			case (select COUNT (part_number_detail) from INVENTORY_NEGATIVE_DETAIL ihd where ihd.id_inventory_negative = ih.id_inventory_negative)
			when 1 then (select quantity from INVENTORY_NEGATIVE_DETAIL ihd where ihd.id_inventory_negative = ih.id_inventory_negative) 
			else 0 end as 'quantity_detail',
			case (select COUNT (part_number_detail) from INVENTORY_NEGATIVE_DETAIL ihd where ihd.id_inventory_negative = ih.id_inventory_negative)
			when 1 then (select amount from INVENTORY_NEGATIVE_DETAIL ihd where ihd.id_inventory_negative = ih.id_inventory_negative) 
			else (select sum(amount) from INVENTORY_NEGATIVE_DETAIL ihd where ihd.id_inventory_negative = ih.id_inventory_negative)
			end as 'amount_detail' , isnull(ih.user_approval,'') as 'user_approval' , ih.negative_status
			from INVENTORY_NEGATIVE_HEADER ih
            join ITEM i on i.part_number = ih.part_number_header
            where cast(ih.cdate as date) between '" + dateInit.Date+"' and '"+dateFin.Date+"' " + sta + " order by ih.cdate ").ToList();
            return r;
        }
        public List<InventoryNegativeDetailModel> GetNegativeDetail(int id)
        {
            var r = _context.Database.SqlQuery<InventoryNegativeDetailModel>(@"
            select id.part_number_detail , i.part_description , iv.unrestricted as 'unrestricted_actual' ,
            id.cost , id.quantity , id.amount  from INVENTORY_NEGATIVE_DETAIL id
            join ITEM_VALUATION iv on id.part_number_detail = iv.part_number
            join ITEM i on i.part_number = id.part_number_detail
            where id.id_inventory_negative = " + id).ToList();
            return r;
        }
    }
}