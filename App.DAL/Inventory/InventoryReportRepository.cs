﻿using App.Entities;
using App.Entities.ViewModels.Inventory;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;

namespace App.DAL.Inventory
{
    public class InventoryReportRepository
    {
        private readonly DFL_SAIEntities _context;

        public InventoryReportRepository()
        {
            _context = new DFL_SAIEntities();
        }

        public List<InventoryHeaderModel> GetInventoryActive()
        {
            try
            {
                return _context.INVENTORY_HEADER.Where(x => x.inventory_status == 1 || x.inventory_status == 2 || x.inventory_status == 3)
                    .Join(_context.INVENTORY_TYPE, header => header.inventory_type, type => type.id_inventory_type, (header, type) => new { header, type })
                    .Select(s => new InventoryHeaderModel
                    {
                        inventory_name = s.header.inventory_name,
                        id_inventory_header = s.header.id_inventory_header,
                        inventory_description = s.header.inventory_description,
                        inventory_status = s.header.inventory_status,
                        inventory_type_name = s.type.inventory_name,
                        inventory_date_end = s.header.end_date,
                        inventory_date_start = s.header.start_date
                    }).ToList();
            }
            catch (Exception)
            {
                throw;
            }
        }

        public List<InventoryAdjustmentModel> GetInventoryAdjustementAll()
        {
            try
            {
                return _context.Database.SqlQuery<InventoryAdjustmentModel>(@"SELECT distinct a.part_number as part_number,
                    c.part_description as part_description,  
                    isnull(b.Detail_qty,0) as previous_quantity,
                    isnull(A.quantity,0) as quantity_stock,
                    a.id_inventory_header as id_Inventory_header
                    FROM INVENTORY_CURRENT A
                    FULL OUTER JOIN (SELECT id_inventory_header, part_number, sum(quantity) Detail_qty
                    FROM INVENTORY_DETAIL 
                    GROUP BY id_inventory_header, part_number)B ON (a.id_inventory_header = b.id_inventory_header and A.part_number = b.part_number)
                    INNER JOIN ITEM C ON A.part_number = C.part_number
                    INNER JOIN MA_CLASS D ON (c.level1_code = d.class_id)
                    where a.id_inventory_header in  
                    (select id_inventory_header from INVENTORY_HEADER 
                    where inventory_status  != 0 and inventory_status  
                    != 8 and inventory_status  != 9 and inventory_status  != 11 ) and a.part_number not in  (select part_number from INVENTORY_ADJUSTMENT_HISTORY where id_inventory_header = a.id_inventory_header)").ToList();
            }
            catch (Exception)
            {
                throw;
            }
        }

        public List<InventoryReportModel> GetInventoryReportDetail(string id)
        {
            try
            {
                var idInv = new SqlParameter { ParameterName = "id", Value = id };
                var x = _context.Database.SqlQuery<InventoryReportModel>(@"select INVENTORY_FINAL.part_number, ITEM.part_description , INVENTORY_FINAL.unrestricted as quantity_current , INVENTORY_FINAL.map_price,   CASE
                    WHEN INVENTORY_FINAL.part_number in (select part_number from INVENTORY_ADJUSTMENT_HISTORY where INVENTORY_FINAL.part_number = part_number and id_inventory_header = @id and adjustment_status = 9) THEN
                    (select new_quantity from INVENTORY_ADJUSTMENT_HISTORY where INVENTORY_FINAL.part_number = part_number and id_inventory_header =  @id and adjustment_status = 9)
                    ELSE counted
                    END AS quantity_detail from INVENTORY_FINAL
                    join ITEM on INVENTORY_FINAL.part_number = ITEM.part_number
                     where id_inventory_header = @id", idInv).ToList();
                return x;
            }
            catch (Exception)
            {
                return null;
                throw;
            }
        }

        public string UpdateInventoryStatus(string id, string user)
        {
            try
            {
                var x = _context.INVENTORY_HEADER.Where(e => e.id_inventory_header == id).SingleOrDefault();
                x.inventory_status = 2;
                x.udate = DateTime.Now;
                x.uuser = user;
                x.end_date = DateTime.Now;
                x.program_id = "INVE004.cshtml";
                _context.SaveChanges();
                return x.end_date.ToString();
            }
            catch (Exception)
            {
                return "error";
                throw;
            }
        }

        public List<InventoryReportModel> GetInventoryById(string id)
        {
            try
            {
                var idInv = new SqlParameter { ParameterName = "id", Value = id };
                //           var x = _context.Database.SqlQuery<InventoryReportModel>(@"select INVENTORY_FINAL.part_number, ITEM.part_description,ma.class_name AS department, ma2.class_name as family , INVENTORY_FINAL.unrestricted as quantity_current , INVENTORY_FINAL.map_price,   CASE
                //               WHEN INVENTORY_FINAL.part_number in (select part_number from INVENTORY_ADJUSTMENT_HISTORY where INVENTORY_FINAL.part_number = part_number and id_inventory_header = @id and adjustment_status = 9) THEN
                //               (select new_quantity from INVENTORY_ADJUSTMENT_HISTORY where INVENTORY_FINAL.part_number = part_number and id_inventory_header =  @id and adjustment_status = 9)
                //               ELSE counted
                //               END AS quantity_detail from INVENTORY_FINAL
                //               join ITEM on INVENTORY_FINAL.part_number = ITEM.part_number
                //left join MA_CLASS ma on ma.class_id = ITEM.level_header
                //left join MA_CLASS ma2 on ma2.class_id = ITEM.level1_code
                //               where id_inventory_header = @id", idInv).ToList();
                var x = _context.Database.SqlQuery<InventoryReportModel>(@"select isnull(E.quantityScrap,0) as 'quantityScrap', isnull(E.amountScrap,0) as 'amountScrap', inf.part_number, i.part_description,ma.class_name AS department, ma2.class_name as family , inf.unrestricted as quantity_current , inf.map_price,   CASE
                    WHEN inf.part_number in (select part_number from INVENTORY_ADJUSTMENT_HISTORY where inf.part_number = part_number and id_inventory_header = @id and adjustment_status = 9) THEN
                    (select new_quantity from INVENTORY_ADJUSTMENT_HISTORY where inf.part_number = part_number and id_inventory_header =  @id and adjustment_status = 9)
                    ELSE counted
                    END AS quantity_detail from INVENTORY_FINAL inf
                    join ITEM i on inf.part_number = i.part_number
					left join MA_CLASS ma on ma.class_id = i.level_header
					left join MA_CLASS ma2 on ma2.class_id = i.level1_code
						FULL OUTER JOIN (SELECT DGI.part_number ,SUM(DGI.quantity) as quantityScrap , SUM(DGI.amount_doc) as amountScrap FROM DAMAGEDS_GOODS DG
                        JOIN DAMAGEDS_GOODS_ITEM DGI ON DG.damaged_goods_doc = DGI.damaged_goods_doc
                        JOIN ITEM I ON I.part_number = DGI.part_number
                        WHERE DG.process_type = 'SCRAP'
                        AND DGI.process_status = 9
                        AND DG.process_status = 9
                        AND DGI.udate BETWEEN (isnull((select top 1 start_date from INVENTORY_HEADER where inventory_type = (select inventory_type from INVENTORY_HEADER where id_inventory_header = @id) and end_date < 
						(select end_date from INVENTORY_HEADER where id_inventory_header = @id)
						order by start_date desc),'2019-09-18')) AND (SELECT TOP 1 end_date from INVENTORY_HEADER WHERE id_inventory_header = @id
                        order by start_date desc)
                        AND I.level_header in (SELECT level_ma_class FROM INVENTORY_TYPE_DETAILS WHERE id_inventory_type = (select inventory_type from INVENTORY_HEADER where id_inventory_header = @id))
                        GROUP BY DGI.part_number )E on E.part_number = inf.part_number
                    where id_inventory_header = @id", idInv).ToList();
                return x;
            }
            catch (Exception e)
            {
                return null;
                throw;
            }
        }

        public InventoryHeaderModel getInventoryHeader(string id)
        {
            var x = _context.INVENTORY_HEADER.Where(y => y.id_inventory_header == id).Select(
                s => new InventoryHeaderModel
                {
                    endDate = s.end_date
                }).FirstOrDefault();
            return x;
        }
        public InventoryCurrentModel GetScrapDates(string id)
        {
            try
            {
                var idInv = new SqlParameter { ParameterName = "id", Value = id };
                var r = _context.Database.SqlQuery<InventoryCurrentModel>(@"select convert(varchar,isnull((select top 1 DATEADD(DAY, 1, start_date) from INVENTORY_HEADER where inventory_type = (select inventory_type from INVENTORY_HEADER where id_inventory_header = @id) and end_date < 
                        (select end_date from INVENTORY_HEADER where id_inventory_header = @id)
                        order by start_date desc),'2019-09-18'),0) as 'startDate' ,convert(varchar,(SELECT TOP 1 end_date from INVENTORY_HEADER WHERE id_inventory_header = @id
                        order by start_date desc), 0) as 'endDate'", idInv).FirstOrDefault();
                return r;
            }
            catch (Exception e)
            {
                return null;
            }

        }
    }
}