﻿using App.Entities;
using App.Entities.ViewModels.Inventory;
using FloridoERPTX.Controllers.Inventory;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.SqlClient;
using System.Linq;

namespace App.DAL.Inventory
{
    public class InventoryPalletsRepository
    {
        private readonly DFL_SAIEntities context = new DFL_SAIEntities();

        public List<PalletModel> PalletList(PalletListFilter filter)
        {
            //se agrego los pallets creados en web
            var r = context.INVENTORY_PALLET_HEADER.Where(
                    e =>
                        e.id_inventory == filter.id_inventory
                        && ( e.user_no == filter.user_no || e.user_no == 0)
                )
                .Select(e => new PalletModel
                {
                    serial_number = e.serial_number,
                    id_inventory = e.id_inventory,
                    user_no = e.user_no,
                    pallet_status = e.pallet_status,
                    storage_location_name = e.storage_location_name,
                    cuser = e.cuser,
                    cdate = e.cdate,
                    uuser = e.uuser,
                    udate = e.udate,
                    program_id = e.program_id
                }).OrderByDescending(e => e.serial_number).ToList();
            return r;
        }

        public PalletModel PalletById(string serial_number)
        {
            var r = context.INVENTORY_PALLET_HEADER.SingleOrDefault(e => e.serial_number == serial_number);
            return r != null ? new PalletModel(r) : null;
        }

        public List<INVENTORY_PALLET_DETAIL> DetailsByIds(IEnumerable<int> id_detail)
        {
            return context.INVENTORY_PALLET_DETAIL.Where(e => id_detail.Contains(e.id_detail)).ToList();
        }

        public PalletModel CreatePallet(PalletModel model, List<PalletDetailModel> details, string username,
            string programId)
        {
            try
            {
                var m = model.ToModel();
                m.cdate = DateTime.Now;
                m.program_id = programId;
                m.cuser = username;
                if (details != null)
                    m.INVENTORY_PALLET_DETAIL = details.Select(e => new INVENTORY_PALLET_DETAIL
                    {
                        quantity = e.quantity,
                        cuser = model.cuser,
                        cdate = model.cdate,
                        uuser = model.uuser,
                        udate = model.udate,
                        program_id = model.program_id,
                    }).ToList();

                context.INVENTORY_PALLET_HEADER.Add(m);
                context.SaveChanges();
                return model;
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                return null;
            }
        }

        public bool DeletePallet(string serialNumber)
        {
            try
            {
                context.INVENTORY_PALLET_DETAIL.RemoveRange(
                    context.INVENTORY_PALLET_DETAIL.Where(e => e.serial_number == serialNumber)
                );
                context.INVENTORY_PALLET_HEADER.Remove(
                    context.INVENTORY_PALLET_HEADER
                        .Single(e => e.serial_number == serialNumber)
                );
                context.SaveChanges();
                return true;
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                return false;
            }
        }

        public List<PalletDetailModel> PalletItemList(PalletItemListFilter filter)
        {
            var r = context.INVENTORY_PALLET_DETAIL
                .Join(context.ITEM, e => e.part_number, e => e.part_number, (detail, user) => new { detail, user })
                .Where(
                    e =>
                        e.detail.serial_number == filter.serial_number
                ).Select(e => new PalletDetailModel
                {
                    id_detail = e.detail.id_detail,
                    serial_number = e.detail.serial_number,
                    part_number = e.detail.part_number,
                    quantity = e.detail.quantity,
                    cuser = e.detail.cuser,
                    cdate = e.detail.cdate,
                    uuser = e.detail.uuser,
                    udate = e.detail.udate,
                    program_id = e.detail.program_id,
                    description = e.user.part_description
                }).ToList();
            return r;
        }

        public PalletDetailModel AddOrReplaceToPallet(string serialNumber, PalletDetailModel detail, string username,
            string program_id)
        {
            try
            {
                if (detail.quantity <= 0) return null;
                var exist = context.INVENTORY_PALLET_DETAIL.Where(e =>
                        e.serial_number == serialNumber && detail.part_number == e.part_number)
                    .Select(e => e).SingleOrDefault();

                if (exist != null)
                {
                    exist.quantity = detail.quantity;
                }
                else
                {
                    exist = new INVENTORY_PALLET_DETAIL
                    {
                        serial_number = serialNumber,
                        part_number = detail.part_number,
                        quantity = detail.quantity,
                        cuser = username,
                        cdate = DateTime.Now,
                        program_id = program_id,
                    };
                    context.INVENTORY_PALLET_DETAIL.Add(exist);
                }

                context.SaveChanges();
                return new PalletDetailModel
                {
                    id_detail = exist.id_detail,
                    serial_number = exist.serial_number,
                    part_number = exist.part_number,
                    quantity = exist.quantity,
                    cuser = exist.cuser,
                    cdate = exist.cdate,
                    uuser = exist.uuser,
                    udate = exist.udate,
                    program_id = exist.program_id,
                    description = ""
                };
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                return null;
            }
        }

        public bool DeleteFromPallet(string serialNumber, List<int> idDetailList)
        {
            try
            {
                context.INVENTORY_PALLET_DETAIL.RemoveRange(
                    context.INVENTORY_PALLET_DETAIL.Where(
                        e => idDetailList.Contains(e.id_detail)
                    ));
                context.SaveChanges();
                return true;
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                return false;
            }
        }

        public bool AddPalletToInventory(string serial_number, string username, string program_id, int userNo)
        {
            using (var trans = context.Database.BeginTransaction())
            {
                try
                {
                    var pallet = context.INVENTORY_PALLET_HEADER.Where(
                            e => e.serial_number == serial_number && e.pallet_status == PalletStatus.Initial)
                        .Include(e => e.INVENTORY_PALLET_DETAIL).Single();

                    pallet.pallet_status = PalletStatus.Finished;

                    context.INVENTORY_DETAIL
                        .AddRange(pallet.INVENTORY_PALLET_DETAIL.Select(e => new INVENTORY_DETAIL
                        {
                            id_inventory_header = pallet.id_inventory,
                            part_number = e.part_number,
                            quantity = e.quantity,
                            storage_location = pallet.storage_location_name,
                            cuser = username,
                            cdate = DateTime.Now,
                            program_id = program_id,
                            user_no = userNo,
                            INVENTORY_HEADER = null
                        }));
                    context.SaveChanges();
                    trans.Commit();
                    return true;
                }
                catch (Exception e)
                {
                    Console.WriteLine(e);
                    trans.Rollback();
                    return false;
                }
            }
        }

        public List<PalletModel> GetInventoryUsersCountSerialNumber(string inventory_id, int user_no)
        {
            try
            {
                var item = context.Database.SqlQuery<PalletModel>(
                    @"SELECT IPH.serial_number, IPH.pallet_status, IPH.storage_location_name, SUM(IPD.quantity) AS quantity FROM INVENTORY_PALLET_HEADER IPH
	                JOIN INVENTORY_PALLET_DETAIL IPD ON IPD.serial_number = IPH.serial_number 
                    WHERE id_inventory = @inventory_id AND user_no = @user_no
	                GROUP BY IPH.serial_number, IPH.pallet_status, IPH.storage_location_name;",
                    new SqlParameter("inventory_id", inventory_id),
                    new SqlParameter("user_no", user_no)).ToList();
                return item;
            }
            catch (Exception ex)
            {
                var msg = ex.Message;
                return null;
            }
        }

        public int GetInventoryPreInventoried(string id_inventory)
        {
            return context.INVENTORY_PALLET_HEADER.Count(c => c.id_inventory == id_inventory && c.pallet_status != 8);
        }
        public decimal GetInventoryTotalTeoric(string id_inventory)
        {
            try
            {
                var count =   context.Database.SqlQuery<decimal>(@"select SUM(quantity * map_price) from INVENTORY_CURRENT where id_inventory_header = @id", new SqlParameter("@id", id_inventory)).SingleOrDefault();
                return count;
            }
            catch (Exception)
            {
                return 0;
            }
        }
        public int GetInventoryCancelled(string id_inventory)
        {
            return context.INVENTORY_PALLET_HEADER.Count(c => c.id_inventory == id_inventory && c.pallet_status == 8);
        }

        public int GetInventoryFinished(string id_inventory)
        {
            return context.INVENTORY_PALLET_HEADER.Count(c => c.id_inventory == id_inventory && c.pallet_status == 9);
        }

        public bool CkItemInventoryType(string id_inventory, string part_number)
        {
            var r = (from iheader in context.INVENTORY_HEADER
                     join itype_detail in context.INVENTORY_TYPE_DETAILS on iheader.inventory_type equals itype_detail
                         .id_inventory_type
                     join ma in context.MA_CLASS on itype_detail.level_ma_class equals ma.class_id
                     join i in context.ITEM on ma.class_id equals i.level_header
                     where itype_detail.active_details && i.part_number == part_number &&
                           iheader.id_inventory_header == id_inventory && i.extra_items_flag != true && i.combo_flag != true
                     select i).Any();
            //var r = context.INVENTORY_CURRENT.Where(x => x.id_inventory_header == id_inventory && x.part_number == part_number).Any();
            return r;
        }
        public bool CkItemInventoryTypeCyclic(string id_inventory, string part_number)
        {         
            var r = context.INVENTORY_CURRENT.Where(x => x.id_inventory_header == id_inventory && x.part_number == part_number).Any();
            return r;
        }

        public bool FinishInventory(string id)
        {
            try
            {
                var pallets = context.INVENTORY_PALLET_HEADER.Where(
                    e => e.pallet_status != PalletStatus.Finished && e.pallet_status != PalletStatus.Cancelled && e.id_inventory == id);
                foreach (var p in pallets) p.pallet_status = PalletStatus.Cancelled;

                context.SaveChanges();
                return true;
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                return false;
            }
        }

        public List<PalletModel> GetInventoryPalletsById(string inventory_id)
        {
            return context.INVENTORY_PALLET_HEADER
                .Join(context.INVENTORY_USERS, u => u.user_no, uu => uu.user_no, (u, uu) => new { u, uu })
                .Where(w => w.u.id_inventory == inventory_id && w.uu.id_inventory_header == inventory_id)
                .Select(s => new PalletModel
                {
                    serial_number = s.u.serial_number,
                    storage_location_name = s.u.storage_location_name,
                    cuser = s.uu.inventory_user,
                    pallet_status = s.u.pallet_status,
                }).ToList();
        }

        public List<PalletDetailItem> GetInventoryPalletsDetail(string serial_number)
        {
            return context.INVENTORY_PALLET_DETAIL
                .Join(context.ITEM, i => i.part_number, ii => ii.part_number, (i, ii) => new { i, ii })
                .Join(context.ITEM_VALUATION, v => v.i.part_number, vv => vv.part_number, (v, vv) => new { v, vv })
                .Where(w => w.v.i.serial_number == serial_number)
                .Select(s => new PalletDetailItem
                {
                    id_detail = s.v.i.id_detail,
                    part_number = s.v.i.part_number,
                    product_name = s.v.ii.part_description,
                    quantity = s.v.i.quantity,
                    price_amount = s.vv.map_price != null ? s.v.i.quantity * s.vv.map_price : 0
                }).ToList();
        }

        public PalletDetailItem ExistInPallet(string serialNumber, string barcode)
        {
            var s = context.INVENTORY_PALLET_DETAIL
                .Join(context.BARCODES, e => e.part_number, e => e.part_number, (pd, b) => new { pd, b })
                .FirstOrDefault(e =>
                    e.pd.serial_number == serialNumber && e.b.part_barcode == barcode);
            if (s == null) return null;
            return new PalletDetailItem
            {
                part_number = s.pd.part_number,
                quantity = s.pd.quantity,
                barcode = s.b.part_barcode,
            };
        }
    }

    public class PalletItemListFilter
    {
        public string serial_number { get; set; }
    }

    public static class PalletStatus
    {
        public const int Initial = 0;
        public const int Cancelled = 8;
        public const int Finished = 9;
    }

    public class PalletListFilter
    {
        public int user_no { get; set; }
        public string id_inventory { get; set; }
    }
}