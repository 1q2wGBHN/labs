﻿using App.Entities;
using App.Entities.ViewModels.Inventory;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;

namespace App.DAL.Inventory
{
    public class InventoryUsersRepository
    {
        private readonly DFL_SAIEntities _context;

        public InventoryUsersRepository()
        {
            _context = new DFL_SAIEntities();
        }

        public string EditInventoryUser(string id, int user_no, string user_name, string emp_no, string user)
        {
            try
            {
                var editUser = _context.INVENTORY_USERS.Where(elements => elements.id_inventory_header == id && elements.user_no == user_no).SingleOrDefault();
                editUser.inventory_user = user_name;
                editUser.emp_no = emp_no;
                editUser.udate = DateTime.Now;
                editUser.uuser = user;
                editUser.program_id = "INVE002.cshtml";
                _context.SaveChanges();
                return "true";
            }
            catch (Exception)
            {
                return "false";
            }
        }

        public bool CheckEmpNo(string id, string emp_no, int user_no, int status)
        {
            try
            {
                if (status == 0)
                    return _context.INVENTORY_USERS.Any(e => e.emp_no == emp_no && e.id_inventory_header == id);
                else
                    return _context.INVENTORY_USERS.Any(e => e.emp_no == emp_no && e.id_inventory_header == id && e.user_no != user_no);
            }
            catch (Exception)
            {
                return false;
            }
        }

        public bool CheckUserIsValid(string id_inventory_header, int user_no)
        {
            return _context.INVENTORY_USERS.Any(e => e.id_inventory_header == id_inventory_header && e.user_no == user_no && e.emp_no != null);
        }

        /*No se que hace esto porfavor ayudame :v atte. OCD*/
        public bool CheckUserPalletHeader(int user, string id) => !_context.INVENTORY_PALLET_HEADER.Any(e => e.user_no == user && e.id_inventory == id);

        public bool CheckUserInventoryDetail(int user, string id) => !_context.INVENTORY_DETAIL.Any(e => e.user_no == user && e.id_inventory_header == id);

        public class InventoryActive
        {
            public string status { get; set; }
            public List<InventoryHeaderModel> inventoryHeader;
        }

        public InventoryActive GetInventoryActive()
        {
            try
            {
                var inventory = _context.INVENTORY_HEADER.Where(x => x.inventory_status == 0 || x.inventory_status == 1)
                    .Select(x => new InventoryHeaderModel
                    {
                        id_inventory_header = x.id_inventory_header,
                        inventory_name = x.inventory_name,
                        inventory_description = x.inventory_description,
                        inventory_date_start = x.start_date,
                        inventory_status = x.inventory_status
                    }).ToList();
                if (!inventory.Any())
                    return new InventoryActive() { status = "inactivo", inventoryHeader = inventory };

                return new InventoryActive() { status = "activo", inventoryHeader = inventory };
            }
            catch (Exception)
            {
                return null;
                throw;
            }
        }

        public InventoryActive GetInventoryCountActive()
        {
            try
            {
                var inventory = _context.INVENTORY_HEADER.Where(x => x.inventory_status == 1)
                    .Select(x => new InventoryHeaderModel
                    {
                        id_inventory_header = x.id_inventory_header,
                        inventory_name = x.inventory_name,
                        inventory_description = x.inventory_description,
                        inventory_date_start = x.start_date,
                        inventory_status = x.inventory_status
                    }).ToList();
                if (!inventory.Any())
                    return new InventoryActive() { status = "inactivo", inventoryHeader = inventory };

                return new InventoryActive() { status = "activo", inventoryHeader = inventory };
            }
            catch (Exception)
            {
                return null;
                throw;
            }
        }

        public List<InventoryUsersModel> GetInventoryUsers(string id)
        {
            try
            {
                var users = _context.INVENTORY_USERS.Where(x => x.id_inventory_header == id)
                    .Select(x => new InventoryUsersModel
                    {
                        user_name = x.inventory_user,
                        user_no = x.user_no,
                        emp_no = x.emp_no
                    }).ToList();
                return users;
            }
            catch (Exception)
            {
                return null;
                throw;
            }
        }

        public List<InventoryUsersModel> GetInventoryUsersCountPallets(string inventory_id)
        {
            try
            {
                var item = _context.Database.SqlQuery<InventoryUsersModel>(@"  SELECT IU.user_no, IU.emp_no, IU.inventory_user AS user_name, COUNT(IPH.user_no) AS PalletsQuantity FROM INVENTORY_USERS IU
		            JOIN INVENTORY_PALLET_HEADER IPH ON IPH.user_no = IU.user_no
		            WHERE IPH.id_inventory = @inventory_id AND IU.id_inventory_header = @inventory_id
		            GROUP BY IU.inventory_user, IU.emp_no, IU.user_no
		            ORDER BY PalletsQuantity DESC",
                    new SqlParameter("inventory_id", inventory_id)).ToList();
                return item;
            }
            catch (Exception ex)
            {
                var msg = ex.Message;
                return null;
            }
        }

        public List<InventoryUsersModel> GetInventoryTopUsers(string inventory_id)
        {
            try
            {
                var item = _context.Database.SqlQuery<InventoryUsersModel>(@"SELECT DISTINCT TOP 5 IU.inventory_user AS user_name, COUNT(IPH.user_no) AS PalletsQuantity FROM INVENTORY_PALLET_HEADER IPH
                    JOIN INVENTORY_USERS IU ON IU.user_no = IPH.user_no
                    WHERE IPH.id_inventory = @inventory_id AND IU.id_inventory_header = @inventory_id
                    GROUP BY IU.inventory_user
                    ORDER BY PalletsQuantity DESC", new SqlParameter("inventory_id", inventory_id)).ToList();
                return item;
            }
            catch (Exception ex)
            {
                var msg = ex.Message;
                return null;
            }
        }
    }
}