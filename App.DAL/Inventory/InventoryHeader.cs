﻿using App.Entities;
using App.Entities.ViewModels.Inventory;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;

namespace App.DAL.Inventory
{
    public class InventoryHeader
    {
        private readonly DFL_SAIEntities _context;

        public InventoryHeader()
        {
            _context = new DFL_SAIEntities();
        }

        public List<InventoryHeaderModel> GetInventorysByDate(DateTime startDate, DateTime endDate)
        {
            return _context.INVENTORY_HEADER
                .Join(_context.INVENTORY_TYPE, t => t.inventory_type, tt => tt.id_inventory_type, (t, tt) => new { t, tt })
                .Where(x => x.t.start_date >= startDate && x.t.start_date <= endDate)
                .Select(s => new InventoryHeaderModel
                {
                    inventory_name = s.t.inventory_name,
                    id_inventory_header = s.t.id_inventory_header,
                    inventory_description = s.t.inventory_description,
                    inventory_status = s.t.inventory_status,
                    inventory_type = s.t.inventory_type,
                    inventory_date_end = s.t.end_date,
                    inventory_date_start = s.t.start_date,
                    inventory_type_name = s.tt.inventory_name
                }).OrderByDescending(x => x.id_inventory_header).ToList();
        }

        public List<InventoryHeaderModel> GetInventory(int Status)
        {
            return _context.INVENTORY_HEADER.Where(x => x.inventory_status == Status)
                .Select(s => new InventoryHeaderModel
                {
                    inventory_name = s.inventory_name,
                    id_inventory_header = s.id_inventory_header,
                    inventory_description = s.inventory_description,
                    inventory_status = s.inventory_status,
                    inventory_type = s.inventory_type,
                    inventory_date_end = s.end_date,
                    inventory_date_start = s.start_date
                }).ToList();
        }
        public List<InventoryHeaderModel> TagGetInventory(List<int> Status)
        {
            return _context.INVENTORY_HEADER.Where(x => Status.Contains(x.inventory_status))
                .Select(s => new InventoryHeaderModel
                {
                    inventory_name = s.inventory_name,
                    id_inventory_header = s.id_inventory_header,
                    inventory_description = s.inventory_description,
                    inventory_status = s.inventory_status,
                    inventory_type = s.inventory_type,
                    inventory_date_end = s.end_date,
                    inventory_date_start = s.start_date
                }).ToList();
        }

        public List<InventoryHeaderModel> GetInventory(IEnumerable<int> statusList)
        {
            return _context.INVENTORY_HEADER.Where(x => statusList.Contains(x.inventory_status)).Select(s => new InventoryHeaderModel
            {
                inventory_name = s.inventory_name,
                id_inventory_header = s.id_inventory_header,
                inventory_description = s.inventory_description,
                inventory_status = s.inventory_status,
                inventory_type = s.inventory_type,
                inventory_date_end = s.end_date,
                inventory_date_start = s.start_date
            }).ToList();
        }

        public List<InventoryHeaderModel> GetInventoryExist()
        {
            return _context.INVENTORY_HEADER.Where(x => x.inventory_status != 9 && x.inventory_status != 8 && x.inventory_status != 0 && x.inventory_status != 11)
                .Select(s => new InventoryHeaderModel
                {
                    inventory_name = s.inventory_name,
                    id_inventory_header = s.id_inventory_header,
                    inventory_description = s.inventory_description,
                    inventory_status = s.inventory_status,
                    inventory_type = s.inventory_type,
                    inventory_date_end = s.end_date,
                    inventory_date_start = s.start_date
                }).ToList();
        }
        public InventoryHeaderModel GetInventoryExistInCount()
        {
            return _context.INVENTORY_HEADER.Where(x => x.inventory_status == 1)
                .Select(s => new InventoryHeaderModel
                {
                    inventory_name = s.inventory_name,
                    id_inventory_header = s.id_inventory_header,
                    inventory_description = s.inventory_description,
                    inventory_status = s.inventory_status,
                    inventory_type = s.inventory_type,
                    inventory_date_end = s.end_date,
                    inventory_date_start = s.start_date
                }).FirstOrDefault();
        }

        public int? InventoryStatus(string idInventory)
        {
            return _context.INVENTORY_HEADER
                .SingleOrDefault(e => e.id_inventory_header == idInventory)?
                .inventory_status;
        }

        public bool EditStatusInventory(string IdInventoryHeader, int Status, string User)
        {
            var Inventory = _context.INVENTORY_HEADER.SingleOrDefault(x => x.id_inventory_header == IdInventoryHeader);
            if (Inventory != null)
            {
                Inventory.inventory_status = Status;
                Inventory.uuser = User;
                Inventory.udate = DateTime.Now;
                Inventory.start_date = DateTime.Now;
                Inventory.program_id = "INVE003.cshtml";
                _context.SaveChanges();
                return true;
            }
            return false;
        }

        public void UpdateJobCloseInventory(int status)
        {
            var DateReturn = _context.Database.ExecuteSqlCommand(@"exec msdb.dbo.sp_update_job @job_name = 'CLOSE_INVENTORY' , @enabled = @status;", new SqlParameter("@status", status));
        }
        public InventoryHeaderModel GetInventoryById(string id)
        {
            return _context.INVENTORY_HEADER.Where(x => x.id_inventory_header == id).Select(x => new InventoryHeaderModel
            {
                id_inventory_header = x.id_inventory_header,
                inventory_type = x.inventory_type,
                inventory_description = x.inventory_description,
                inventory_name = x.inventory_name,
                inventory_status = x.inventory_status
            }).FirstOrDefault();
        }
        public bool CheckCyclicInventory(string id)
        {
            return _context.INVENTORY_HEADER.Where(x => x.id_inventory_header == id && x.flag_cyclic == true).Any();
        }
        public List<InventoryHeaderModel> GetInventoryOrderByStart(IEnumerable<int> statusList)
        {
            return _context.INVENTORY_HEADER.Where(x => statusList.Contains(x.inventory_status)).Select(s => new InventoryHeaderModel
            {
                inventory_name = s.inventory_name,
                id_inventory_header = s.id_inventory_header,
                inventory_description = s.inventory_description,
                inventory_status = s.inventory_status,
                inventory_type = s.inventory_type,
                inventory_date_end = s.end_date,
                inventory_date_start = s.start_date
            }).OrderByDescending(x => x.inventory_date_start).ToList();
        }
        public List<InventoryType> GetInventoryTypeActive()
        {
            var r = _context.Database.SqlQuery<InventoryType>(@"
            select id_inventory_type , inventory_name
            from INVENTORY_TYPE
            where active_inventory = 1").ToList();
            return r;
        }
    }

    public static class InventoryStatus
    {
        public const int initial = 0;
        public const int active = 1;
        public const int closed = 2;
        public const int adjustment = 3;
        public const int cancelled = 8;
        public const int finished = 9;
    }
}