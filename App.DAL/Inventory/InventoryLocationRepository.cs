﻿using App.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using App.Entities.ViewModels.Inventory;

namespace App.DAL.Inventory
{
    public class InventoryLocationRepository
    {
        private readonly DFL_SAIEntities _context;

        public InventoryLocationRepository()
        {
            _context = new DFL_SAIEntities();
        }

        public int AddInventoryLocation(InventoryLocationModel model)
        {
            try
            {
                var status = 0;
                if (model.inventory_status) { status = 1; }
                var r = _context.Database.SqlQuery<int>(@"INSERT INVENTORY_LOCATIONS (storage_location,bin1,bin2,bin3,inventory_class,inventory_status,cuser,cdate,program_id)
                VALUES ('" + model.storage_location + "' , '" + model.bin1 + "' , '" + model.bin2 + "' , '" + model.bin3 + "' , '" + model.inventory_class + "' , " + status + @" 
                , '" + model.cuser + "' , GETDATE() , 'INVE016.cshtml'); SELECT cast(SCOPE_IDENTITY() as int);").FirstOrDefault();
                return r;
            }
            catch (Exception e)
            {
                return 0;
            }
        }

        public bool AddInventoryLocationType(int id_location , int id_inventory_type)
        {
            try
            {
                var r = _context.Database.ExecuteSqlCommand(@"INSERT INVENTORY_LOCATIONS_TYPE (id_location,id_inventory_type)
                VALUES ("+id_location+" , "+id_inventory_type+")");
                return true;
            }
            catch (Exception e)
            {
                return false;
            }
        }
        public List<InventoryLocationModel> GetAllInventoryLocations()
        {
            var r = _context.Database.SqlQuery<InventoryLocationModel>(@"SELECT id_location , storage_location , bin1 , bin2,bin3 , inventory_class , inventory_status
            FROM INVENTORY_LOCATIONS").ToList();

            foreach(var d in r)
            {
                var list = _context.Database.SqlQuery<InventoryLocationModel>(@"select A.id_inventory_type as 'inventory_type_id' , inventory_name 
                from INVENTORY_LOCATIONS_TYPE A
                JOIN INVENTORY_TYPE B ON A.id_inventory_type = B.id_inventory_type
                WHERE A.id_location = " + d.id_location).ToList();

                d.inventory_location_type = list;
            }
            return r;
        }
        public bool DeleteInventoryLocationsType(int id)
        {
            try
            {
                _context.Database.ExecuteSqlCommand("DELETE FROM INVENTORY_LOCATIONS_TYPE WHERE id_location = " + id);
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }
        public bool UpdateInventoryLocation(InventoryLocationModel model)
        {
            try
            {
                var status = 0;
                if (model.inventory_status) { status = 1; }
                _context.Database.ExecuteSqlCommand(@"
                UPDATE INVENTORY_LOCATIONS 
                SET storage_location = '"+model.storage_location+ "', bin1 = '" + model.bin1 + "', bin2 = '" + model.bin2 + @"',
                bin3 = '" + model.bin3 + "', inventory_class = '" + model.inventory_class + @"',
                inventory_status = "+status+ ", uuser = '" + model.cuser + "', udate = GETDATE(), program_id = 'INVE015.cshtml' WHERE id_location = " + model.id_location);

                return true;
            }
            catch (Exception e)
            {
                return false;
            }
        }

        public int GetCountInventoryLocation()
        {
            var r = _context.Database.SqlQuery<int>(@"select COUNT(id_location) from INVENTORY_LOCATIONS where inventory_status = 1").FirstOrDefault();
            return r;
        }
        public bool CheckInventoryLocation(string location , string id)
        {
            return _context.Database.SqlQuery<int>(@"
            SELECT A.id_location
            FROM INVENTORY_LOCATIONS A
            JOIN INVENTORY_LOCATIONS_TYPE B ON A.id_location = B.id_location
            WHERE B.id_inventory_type = (SELECT inventory_type FROM INVENTORY_HEADER WHERE id_inventory_header = '"+id+@"')
            AND A.inventory_status = 1
            AND A.storage_location = '"+location+"'").Any();
        }
        public List<InventoryItemLocationModel> GetProductsNotExhibited(string id)
        {
            var r = _context.Database.SqlQuery<InventoryItemLocationModel>(@"
            SELECT A.part_number , C.part_description , A.quantity , A.storage_location,
            case A.user_no when 0 then (select CONCAT(first_name , ' ' , last_name) from USER_MASTER where user_name =  A.cuser) else
            (select inventory_user from INVENTORY_USERS where id_inventory_header = A.id_inventory_header and user_no = A.user_no) end as 'cuser',
            convert(nvarchar(100),A.cdate,120) as 'cdate',
            case A.user_no when 0 then 'WEB' else 'POCKET' end as 'captured'
            FROM INVENTORY_DETAIL A
            JOIN INVENTORY_LOCATIONS B ON A.storage_location = B.storage_location
            JOIN ITEM C ON C.part_number = A.part_number
            WHERE A.id_inventory_header = '"+id+ @"'
            AND B.inventory_class = 'Bodega'
            AND A.part_number NOT IN (
            SELECT DISTINCT(part_number)
            FROM INVENTORY_DETAIL A
            JOIN INVENTORY_LOCATIONS B ON A.storage_location = B.storage_location
            WHERE A.id_inventory_header = '" + id + @"'
            AND B.inventory_class = 'Punto De Venta') order by A.storage_location").ToList();
            return r;
        }
        public List<InventoryLocationModel> GetLocationsNotCounted(string id)
        {
            return _context.Database.SqlQuery<InventoryLocationModel>(@"
            SELECT A.storage_location , A.inventory_class
            FROM INVENTORY_LOCATIONS A
            JOIN INVENTORY_LOCATIONS_TYPE B ON A.id_location = B.id_location
            WHERE B.id_inventory_type = 
	            (SELECT id_inventory_type FROM INVENTORY_HEADER WHERE id_inventory_header = '"+id+@"')
            AND A.storage_location NOT IN (
	            SELECT distinct(storage_location) 
	            FROM INVENTORY_DETAIL 
	            WHERE id_inventory_header = '"+id+@"')
            AND A.inventory_status = 1").ToList();
        }
        public List<InventoryItemLocationModel> GetInventoryFormat(string location)
        {
            return _context.Database.SqlQuery<InventoryItemLocationModel>(@"
            SELECT A.part_number , A.storage_location , B.part_description
            FROM INVENTORY_FORMAT A
            JOIN ITEM B ON A.part_number = B.part_number
            WHERE A.storage_location = '" + location + "'").ToList();
        }
        public bool AddInventoryFormat(string location , string part_number , string cuser , string program)
        {
            try
            {
                _context.Database.ExecuteSqlCommand("INSERT INTO INVENTORY_FORMAT VALUES('"+location+"', '"+part_number+ "' , '" + cuser + "', getdate() , '" + program + "')");
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }
        public bool DeleteInventoryFormat(string location , string part_number)
        {
            try
            {
                _context.Database.ExecuteSqlCommand("DELETE FROM INVENTORY_FORMAT WHERE storage_location = '" + location + "' and part_number = '" + part_number + "'");
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

    }
}
