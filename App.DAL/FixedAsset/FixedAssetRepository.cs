﻿using App.GlobalEntities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App.DAL.FixedAsset
{
    public class FixedAssetRepository
    {
        private readonly GlobalERPEntities _context;

        public FixedAssetRepository()
        {
            _context = new GlobalERPEntities();
        }

        public List<FIXED_ASSET> GetAllFixedAsset()
        {
            return _context.FIXED_ASSET.Where(w => w.status_flag == true).ToList();
        }

        public FIXED_ASSET GetAssetById(int id)
        {
            return _context.FIXED_ASSET.Where(w => w.fixed_asset_code == id).FirstOrDefault();
        }


    }
}
