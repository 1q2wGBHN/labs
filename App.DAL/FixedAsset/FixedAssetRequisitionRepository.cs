﻿using App.GlobalEntities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App.DAL.FixedAsset
{
    public class FixedAssetRequisitionRepository
    {
        private readonly GlobalERPEntities _context;

        public FixedAssetRequisitionRepository()
        {
            _context = new GlobalERPEntities();
        }

        public int AddRequisition(FIXED_ASSET_REQUISITION requisition)
        {
            try
            {
                _context.FIXED_ASSET_REQUISITION.Add(requisition);
                _context.SaveChanges();
                return requisition.requisition_code;
            }
            catch (Exception e)
            {
                var msg = e.Message;
                return 0;
            }
        }

        public List<FixedAssetRequisitionModel> GetRequisitionAssetBySite(string site_code)
        {
            var query = _context.FIXED_ASSET_REQUISITION.Where(w => w.site_code == site_code).Select(s => new FixedAssetRequisitionModel {
                requisition_code = s.requisition_code,
                site_code = s.site_code,
                requisition_status = s.requisition_status,
                requisition_date = s.requisition_date,
                delivery_date = s.delivery_date,
                requisition_remark = s.requisition_remark
            }).ToList();

            foreach (var item in query)
            {
                switch (item.requisition_status)
                {
                    case 1:
                        item.requisition_status_str = "En espera";
                        break;
                    case 2:
                        item.requisition_status_str = "Aprobado";
                        break;
                    case 8:
                        item.requisition_status_str = "Cancelado";
                        break;
                    case 9:
                        item.requisition_status_str = "Terminado";
                        break;
                }
            }

            return query;
        }

    }
}
