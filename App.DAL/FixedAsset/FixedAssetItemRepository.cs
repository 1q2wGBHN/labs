﻿using App.GlobalEntities;
using System;
using System.Collections.Generic;
using System.Linq;

namespace App.DAL.FixedAsset
{
    public class FixedAssetItemRepository
    {
        private readonly GlobalERPEntities _context;

        public FixedAssetItemRepository()
        {
            _context = new GlobalERPEntities();
        }

        public int AddItem(FIXED_ASSET_ITEM item)
        {
            try
            {
                _context.FIXED_ASSET_ITEM.Add(item);
                return _context.SaveChanges();
            }
            catch (Exception e)
            {
                var msg = e.Message;
                return 0;
            }
        }

        public List<FixedAssetItemModel> GetItemsByRequisition(int id)
        {
            return _context.FIXED_ASSET_ITEM.Where(w => w.requisiton_code == id)
                .Select(s => new FixedAssetItemModel {
                    description = s.description,
                    serie = s.serie,
                    model = s.model,
                    brand = s.brand
                }).ToList();

        }


    }
}
