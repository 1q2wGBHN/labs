﻿using App.Entities;
using App.Entities.ViewModels.Item;
using App.Entities.ViewModels.PurchaseBaseCost;
using App.Entities.ViewModels.PurchaseOrder;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;

namespace App.DAL.PurchaseOrder
{
    public class PurchaseOrderItemRepository
    {
        private readonly DFL_SAIEntities _context;

        public PurchaseOrderItemRepository()
        {
            _context = new DFL_SAIEntities();
        }

        public int AddPurchaseOrderItem(PURCHASE_ORDER_ITEM item)
        {
            try
            {
                _context.PURCHASE_ORDER_ITEM.Add(item);
                var save = _context.SaveChanges();
                return save;
            }
            catch (Exception ex)
            {
                var msg = ex.Message;
                return 0;
            }
        }

        public PurchaseOrderItemOCModel GetPurchaseOrderItemList(string PurchaseNo)
        {
            var Purchase = _context.PURCHASE_ORDER.Where(x => x.purchase_no == PurchaseNo).SingleOrDefault();
            if (Purchase != null)
            {
                var Supplier = _context.MA_SUPPLIER.Where(x => x.supplier_id == Purchase.supplier_id).SingleOrDefault();
                var PurchaseList = _context.PURCHASE_ORDER_ITEM.Join(_context.ITEM, c => c.part_number,
                    it => it.part_number, (c, it) => new { c, it }).Where(x => x.c.purchase_no == PurchaseNo && x.c.item_status == 1)
                    .OrderBy(o => o.c.item_position)
                    .Select(x => new PurchaseOrderItemListModel
                    {
                        PartNumber = x.c.part_number,
                        Description = x.it.part_description,
                        Quantity = x.c.quantity != null ? x.c.quantity.Value : 0,
                        UnitSize = x.c.unit_size,
                        PurchasePrice = x.c.purchase_price != null ? x.c.purchase_price.Value : 0,
                        ItemAmount = x.c.item_amount != null ? x.c.item_amount.Value : 0,
                        Iva = x.c.iva != null ? x.c.iva.Value : 0,
                        Ieps = x.c.ieps != null ? x.c.ieps.Value : 0,
                        Parcking = x.c.packing_of_size != null ? x.c.packing_of_size.Value : 1,
                        ItemTotalAmount = x.c.item_total_amount != null ? x.c.item_total_amount.Value : 0,
                        TotalPacking = 1,
                    }).ToList();

                var SupplierInfo = _context.MA_SUPPLIER_CONTACT.Where(x => x.departament == "Contabilidad" && x.supplier_id == Supplier.supplier_id && x.flagActive == true).FirstOrDefault();
                var Email = "";
                var Phone = "";
                var NameContact = "";

                if (SupplierInfo != null)
                {
                    Email = SupplierInfo.email;
                    Phone = SupplierInfo.phone;
                    NameContact = SupplierInfo.name + " " + SupplierInfo.last_name;
                }

                var PurchaseViewModel = new PurchaseOrderItemOCModel()
                {
                    CommercialName = Supplier.commercial_name,
                    Currency = Purchase.currency,
                    PurchaseDate = Purchase.purchase_date != null ? Purchase.purchase_date.Value.ToString("MM/dd/yyyy") : "",
                    PurchaseOrderList = PurchaseList,
                    Adress = Supplier.supplier_address,
                    Email = Email,
                    NameContact = NameContact,
                    Phone = Phone,
                    RFC = Supplier.rfc,
                    InvoiceNo = Purchase.invoice_no
                };

                return PurchaseViewModel;
            }
            return null;
        }

        public Tuple<int, string> SupplierInfo(string PurchaseNo)
        {
            var Purchase = _context.PURCHASE_ORDER.Where(x => x.purchase_no == PurchaseNo).SingleOrDefault();
            var Supplier = _context.MA_SUPPLIER.Where(x => x.supplier_id == Purchase.supplier_id).SingleOrDefault();

            return new Tuple<int, string>(Supplier.supplier_id, Supplier.business_name);
        }

        public StoreProcedureResult StoreProcedureOCRG(string PurchaseNo, string User)
        {
            _context.Database.CommandTimeout = 240;
            var DateReturn = _context.Database.SqlQuery<StoreProcedureResult>("DECLARE	@return_value int, @Document nvarchar(10) EXEC  @return_value = [dbo].[sproc_GR_From_PurchaseOrder] @po_no = N'" + PurchaseNo + "', @cuser = N'" + User + "', @program_id = N'OCRC001.cshtml', @Document = @Document OUTPUT SELECT 'Document'= @Document,'ReturnValue' = @return_value").ToList(); //SELECT 'Document'= @Document,'ReturnValue' = @return_value
            return DateReturn[0];
        }

        public bool SearchFolioBySupplier(string purchase_no, string invoice)
        {
            var purchase_info = _context.PURCHASE_ORDER.Where(w => w.purchase_no == purchase_no).FirstOrDefault();

            int supplier = purchase_info.supplier_id ?? 0;

            var purchase_exists = _context.PURCHASE_ORDER.Where(w => w.invoice_no == invoice && w.supplier_id == supplier && w.purchase_status == 9).FirstOrDefault();

            if (purchase_exists != null)
                return true;
            else
                return false;
        }

        public int ExistUuid(string Uuid)
        {
            return _context.PURCHASE_ORDER.Where(x => x.uuid == Uuid).Count();
        }

        public bool SaveExchangeRateDate(string PurchaseNo, DateTime Fecha, string User)
        {
            try
            {
                var PurchaseOrder = _context.PURCHASE_ORDER.Where(x => x.purchase_no == PurchaseNo).SingleOrDefault();
                PurchaseOrder.uuser = User;
                PurchaseOrder.exchange_rate_date = Fecha;
                PurchaseOrder.program_id = "OCRC001.cshtml";
                _context.SaveChanges();
                return true;
            }
            catch (Exception)
            {
                return false;
                throw;
            }
        }

        public bool AddXMLPurchaseOrderItem(string PurchaseNo, string Base64, string Uuid, string User, string Invoice, string Comment)
        {
            try
            {
                var PurchaseOrder = _context.PURCHASE_ORDER.Where(x => x.purchase_no == PurchaseNo).SingleOrDefault();
                PurchaseOrder.uuid = Uuid;
                PurchaseOrder.purchase_xml = Base64;
                PurchaseOrder.uuser = User;
                PurchaseOrder.purchase_remark = $"{PurchaseOrder.purchase_remark} --{Comment}";
                PurchaseOrder.ata = DateTime.Now;
                PurchaseOrder.udate = DateTime.Now;
                PurchaseOrder.program_id = "OCRC001.cshtml";
                PurchaseOrder.invoice_no = Invoice;
                _context.SaveChanges();
                return true;
            }
            catch (Exception e)
            {
                return false;
                throw;
            }
        }

        public bool UpdatePurchaseOrderItem(string PurchaseNo, string Invoice, string User, string Comment)
        {
            try
            {
                var PurchaseOrder = _context.PURCHASE_ORDER.Where(x => x.purchase_no == PurchaseNo).SingleOrDefault();
                PurchaseOrder.uuser = User;
                PurchaseOrder.invoice_no = Invoice;
                PurchaseOrder.ata = DateTime.Now;
                PurchaseOrder.udate = DateTime.Now;
                PurchaseOrder.purchase_remark = $"{PurchaseOrder.purchase_remark} --{Comment}";
                PurchaseOrder.program_id = "OCRC001.cshtml";
                _context.SaveChanges();
                return true;
            }
            catch (Exception)
            {
                return false;
                throw;
            }
        }

        public bool UpdatePurchaseOrderItemAndTotal(string PurchaseNo, string Invoice, string User, string Comment, decimal Total)
        {
            try
            {
                var PurchaseOrder = _context.PURCHASE_ORDER.Where(x => x.purchase_no == PurchaseNo).SingleOrDefault();
                PurchaseOrder.uuser = User;
                PurchaseOrder.invoice_no = Invoice;
                PurchaseOrder.total_purchase = Total;
                PurchaseOrder.ata = DateTime.Now;
                PurchaseOrder.udate = DateTime.Now;
                PurchaseOrder.purchase_remark = $"{PurchaseOrder.purchase_remark} --{Comment}";
                PurchaseOrder.program_id = "OCRC001.cshtml";
                _context.SaveChanges();
                return true;
            }
            catch (Exception)
            {
                return false;
                throw;
            }
        }

        public PurchaseOrderModel GetInfoByPurchaseOrderBasic(string PurchaseNo)
        {
            return _context.PURCHASE_ORDER.Where(w => w.purchase_no == PurchaseNo)
                .Select(s => new PurchaseOrderModel
                {
                    PurchaseNo = s.purchase_no,
                    purchase_status_value = s.purchase_status ?? 0,
                    Currency = s.currency
                }).FirstOrDefault();
        }

        public List<PurchaseOrderItemModel> GetPurchaseOrderItem(string PurchaseNo)
        {
            List<PurchaseOrderItemModel> PurchaseItemOrder = new List<PurchaseOrderItemModel>();

            var PurchaseItem = _context.PURCHASE_ORDER_ITEM
                .Join(_context.ITEM, poi => poi.part_number, it => it.part_number, (poi, it) => new { poi, it })
                .OrderBy(o => o.poi.item_position)
                .Where(s => s.poi.purchase_no == PurchaseNo && s.poi.item_status == 1).Select(s => new PurchaseOrderItemModel
                {
                    ParthNumber = s.poi.part_number,
                    PurchaseNo = s.poi.purchase_no,
                    QuantityOC = s.poi.quantity != null ? s.poi.quantity.Value : 0,
                    QuantityRC = 0,
                    Description = s.it.part_description
                }).ToList();

            var Blind = _context.BLIND_COUNT.Where(x => x.purchase_no == PurchaseNo)
                .OrderBy(o => o.item_position)
                .Select(s => new PurchaseOrderItemModel
                {
                    Id = s.blind_count_id,
                    ParthNumber = s.part_number,
                    PurchaseNo = s.purchase_no,
                    QuantityOC = 0,
                    QuantityRC = s.quantity != null ? s.quantity.Value : 0,
                }).ToList();

            if (PurchaseItem.Count() >= Blind.Count())
            {
                foreach (var item in PurchaseItem)
                {
                    var Blinds = _context.BLIND_COUNT.Where(x => x.purchase_no == PurchaseNo && x.part_number == item.ParthNumber).SingleOrDefault();
                    if (Blinds != null)
                    {
                        item.Id = Blinds.blind_count_id;
                        item.QuantityRC = Blinds.quantity != null ? Blinds.quantity.Value : 0;
                        PurchaseItemOrder.Add(item);
                        continue;
                    }
                    PurchaseItemOrder.Add(item);
                }
            }
            else
            {
                foreach (var item in Blind)
                {
                    var Purchase = _context.PURCHASE_ORDER_ITEM.Where(x => x.purchase_no == PurchaseNo && x.part_number == item.ParthNumber).SingleOrDefault();
                    if (Purchase != null)
                    {
                        item.QuantityOC = Purchase.quantity != null ? Purchase.quantity.Value : 0;
                        PurchaseItemOrder.Add(item);
                        continue;
                    }
                    PurchaseItemOrder.Add(item);
                }
            }

            return PurchaseItemOrder;
        }

        public PurchaseOrderItemModel GetPurchaseOrderItemByOCAndPartNumber(string oc, string part)
        {
            var PurchaseItem = _context.PURCHASE_ORDER_ITEM.Where(s => s.purchase_no == oc && s.part_number == part)
                .Select(s => new PurchaseOrderItemModel
                {
                    ParthNumber = s.part_number,
                    PurchaseNo = s.purchase_no,
                    QuantityOC = s.quantity != null ? s.quantity.Value : 0,
                }).SingleOrDefault();

            return PurchaseItem;
        }

        public int GetItemPositionByOc(string oc)
        {
            var PurchaseItem = _context.PURCHASE_ORDER_ITEM.Where(s => s.purchase_no == oc)
                .OrderByDescending(o => o.item_position)
                .Take(1)
                .Select(s => s.item_position).SingleOrDefault();
            PurchaseItem = (PurchaseItem ?? 0) + 1;
            return PurchaseItem ?? 0;
        }

        public int GetItemPositionByOcAndPartNumber(string oc, string part_number)
        {
            var PurchaseItem = _context.PURCHASE_ORDER_ITEM.Where(s => s.purchase_no == oc && s.part_number == part_number)
                .Select(s => s.item_position).SingleOrDefault();
            return PurchaseItem ?? 0;
        }

        public bool UpdateItemPositionByOcAndPartNumber(string oc, string part_number, int position)
        {
            try
            {
                var PurchaseItem = _context.PURCHASE_ORDER_ITEM.Where(s => s.purchase_no == oc && s.part_number == part_number).SingleOrDefault();
                PurchaseItem.item_position = position;
                _context.SaveChanges();
                return true;
            }
            catch (Exception ex)
            {
                var msg = ex.Message;
                return false;
            }
        }

        public bool UpdatePuchaseNoItem(string purchase_order, decimal quantityReal, decimal quantity, string part_number, int supplier_part_number, string user, PurchaseBaseCostModel baseModel, StoreProcedureIvaIeps Tax)
        {
            try
            {
                var purchase_item = _context.PURCHASE_ORDER_ITEM.Where(w => w.purchase_no == purchase_order && w.part_number == part_number).FirstOrDefault();
                purchase_item.quantity = quantity + quantityReal;
                if (purchase_item.quantity == 0)
                    purchase_item.item_status = 7;
                else
                    purchase_item.item_status = 1;
                purchase_item.original_quantity = quantity + quantityReal;
                purchase_item.purchase_price = baseModel.base_cost;
                purchase_item.item_amount = purchase_item.quantity * baseModel.base_cost;
                purchase_item.ieps = ((purchase_item.quantity * baseModel.base_cost) * (Tax.Ieps / 100));
                purchase_item.iva = (((purchase_item.quantity * baseModel.base_cost) + purchase_item.ieps) * (Tax.Iva / 100));
                purchase_item.item_total_amount = purchase_item.item_amount + purchase_item.ieps + purchase_item.iva;
                purchase_item.uuser = user;
                purchase_item.udate = DateTime.Now;
                purchase_item.program_id = "AndroidRPC";
                _context.SaveChanges();
                return true;
            }
            catch (Exception ex)
            {
                var msg = ex.Message;
                return false;
            }
        }

        public bool CreatePurchaseNoItem(PURCHASE_ORDER_ITEM itemOrder)
        {
            try
            {
                _context.PURCHASE_ORDER_ITEM.Add(itemOrder);
                var save = _context.SaveChanges();
                return true;
            }
            catch (Exception ex)
            {
                var msg = ex.Message;
                return false;
            }
        }

        public decimal DiferenceOCRG(string Purchase)
        {
            var PurchaseItem = _context.CREDIT_NOTE_CLAIM.Where(x => x.puchase_no == Purchase).SingleOrDefault();

            return PurchaseItem == null ? 0 : PurchaseItem.total_amount ?? 0;
        }

        public int RemovePurchaseOrder(string Purchase, string User)
        {
            try
            {
                var PurchaseItem = _context.PURCHASE_ORDER.Where(x => x.purchase_no == Purchase).SingleOrDefault();
                PurchaseItem.uuid = "";
                PurchaseItem.purchase_status = 8;
                PurchaseItem.uuser = User;
                PurchaseItem.udate = DateTime.Now;
                _context.SaveChanges();
                return 1;
            }
            catch (Exception)
            {
                return 2;
            }
        }

        public bool UpdateOC(List<PurchaseOrderItemModel> OCRC, string User)
        {
            bool IsValid = true;
            foreach (var item in OCRC)
            {
                var Purchase = _context.PURCHASE_ORDER_ITEM.Where(x => x.purchase_no == item.PurchaseNo && x.part_number == item.ParthNumber).SingleOrDefault();
                var Blind = _context.BLIND_COUNT.Where(x => x.purchase_no == item.PurchaseNo && x.part_number == item.ParthNumber).SingleOrDefault();

                if (Purchase != null && Blind != null)
                {
                    if (Purchase.quantity != Blind.quantity && item.QuantityOC > 0 && item.QuantityRC > 0)
                    {
                        Blind.quantity = item.QuantityRC;
                        Purchase.quantity = item.QuantityRC;
                        Purchase.item_amount = Purchase.purchase_price * item.QuantityRC;
                        var IepsIva = new PurchaseOrderTaxModel()
                        {
                            Ieps = 0,
                            Iva = 0
                        };

                        var TaxDefault = _context.ITEM.Where(x => x.part_number == item.ParthNumber).SingleOrDefault();
                        var Ieps = _context.MA_TAX.Where(x => x.tax_code == TaxDefault.part_ieps).SingleOrDefault();
                        var IVA = _context.MA_TAX.Where(x => x.tax_code == TaxDefault.part_iva_purchase).SingleOrDefault();

                        if (Ieps != null)
                            IepsIva.Ieps = Ieps.tax_value != null ? Ieps.tax_value.Value : 0;

                        if (IVA != null)
                            IepsIva.Iva = IVA.tax_value != null ? IVA.tax_value.Value : 0;

                        if (IepsIva.Ieps > 0)
                        {
                            Purchase.ieps = Purchase.item_amount * (IepsIva.Ieps / 100);
                            Purchase.iva = (Purchase.item_amount + Purchase.ieps) * (IepsIva.Iva / 100);
                        }
                        else
                        {
                            Purchase.ieps = 0;
                            Purchase.iva = Purchase.item_amount * (IepsIva.Iva / 100);
                        }

                        Purchase.item_total_amount = (Purchase.ieps != null ? Purchase.ieps : 0) + (Purchase.iva != null ? Purchase.iva : 0) + Purchase.item_amount;
                        Purchase.udate = DateTime.Now;
                        Purchase.uuser = User;
                        Purchase.program_id = "OCRC001.cshtml";
                        Blind.udate = DateTime.Now;
                        Blind.uuser = User;
                        Blind.program_id = "OCRC001.cshtml";
                        _context.SaveChanges();
                    }
                }
                else if (item.QuantityRC == 0)
                {
                    Purchase.item_status = 7;
                    Purchase.quantity = 0;
                    _context.SaveChanges();
                }
                else
                {
                    IsValid = false;
                    break;
                }
            }
            return IsValid;
        }

        public Tuple<bool, decimal> NewUpdateOC(List<PurchaseOrderItemModel> OCRC, string User, string Comment)
        {
            bool IsValid = true;

            foreach (var item in OCRC)
            {
                var Purchase = _context.PURCHASE_ORDER_ITEM.Where(x => x.purchase_no == item.PurchaseNo && x.part_number == item.ParthNumber).SingleOrDefault();
                var Blind = _context.BLIND_COUNT.Where(x => x.purchase_no == item.PurchaseNo && x.part_number == item.ParthNumber).SingleOrDefault();
                if (Purchase != null && Blind != null)
                {
                    if (Purchase.quantity != Blind.quantity)
                    {
                        Blind.quantity = item.QuantityRC;
                        Purchase.quantity = item.QuantityRC;
                        Purchase.item_amount = Purchase.purchase_price * item.QuantityRC;

                        var IepsIva = new PurchaseOrderTaxModel()
                        {
                            Ieps = 0,
                            Iva = 0
                        };

                        var TaxDefault = _context.ITEM.Where(x => x.part_number == item.ParthNumber).SingleOrDefault();
                        var Ieps = _context.MA_TAX.Where(x => x.tax_code == TaxDefault.part_ieps).SingleOrDefault();
                        var IVA = _context.MA_TAX.Where(x => x.tax_code == TaxDefault.part_iva_purchase).SingleOrDefault();

                        if (Ieps != null)
                            IepsIva.Ieps = Ieps.tax_value != null ? Ieps.tax_value.Value : 0;

                        if (IVA != null)
                            IepsIva.Iva = IVA.tax_value != null ? IVA.tax_value.Value : 0;

                        if (IepsIva.Ieps > 0)
                        {
                            Purchase.ieps = Purchase.item_amount * (IepsIva.Ieps / 100);
                            Purchase.iva = (Purchase.item_amount + Purchase.ieps) * (IepsIva.Iva / 100);
                        }
                        else
                        {
                            Purchase.ieps = 0;
                            Purchase.iva = Purchase.item_amount * (IepsIva.Iva / 100);
                        }

                        Purchase.item_total_amount = (Purchase.ieps != null ? Purchase.ieps : 0) + (Purchase.iva != null ? Purchase.iva : 0) + Purchase.item_amount;
                        Purchase.item_status = 1;
                        Purchase.udate = DateTime.Now;
                        Purchase.uuser = User;
                        Purchase.program_id = "OCRC001.cshtml";
                        Blind.udate = DateTime.Now;
                        Blind.uuser = User;
                        Blind.program_id = "OCRC001.cshtml";
                        _context.SaveChanges();
                    }
                }
                else if (item.QuantityRC == 0)
                {
                    Purchase.item_status = 7;
                    Purchase.quantity = 0;
                    _context.SaveChanges();
                }
                else
                {
                    IsValid = false;
                    break;
                }
            }

            return new Tuple<bool, decimal>(IsValid,/*Total*/0);
        }

        public bool CreateCreditNote(string purcheNo, string User, decimal diference)
        {
            var PurchaseHeader = _context.PURCHASE_ORDER.Where(x => x.purchase_no == purcheNo).SingleOrDefault();
            var SiteCode = _context.SITE_CONFIG.SingleOrDefault();

            CREDIT_NOTE_CLAIM Credit = new CREDIT_NOTE_CLAIM()
            {
                puchase_no = PurchaseHeader.purchase_no,
                cdate = DateTime.Now,
                cuser = User,
                claim_status = 1,
                claim_date = DateTime.Now,
                currency = PurchaseHeader != null ? PurchaseHeader.currency : "",
                supplier_id = PurchaseHeader != null ? PurchaseHeader.supplier_id ?? 0 : 0,
                ieps = 0,
                iva = 0,
                total_amount = diference,
                subtotal = 0,// diference,
                site_code = SiteCode.site_code,
                program_id = "OCRC001.cshtml",
            };

            _context.CREDIT_NOTE_CLAIM.Add(Credit);
            _context.SaveChanges();
            return true;
        }

        public List<ItemPurchase> GetAllPurchasesDetailMod(string poNumber)
        {
            var ss = _context.Database.SqlQuery<ItemPurchase>(@"select                 
                poi.part_number as  PartNumber,
                isnull(poi.quantity,0) as Quantity,
                isnull(poi.gr_quantity,0) as GrQuantity,
                isnull(poi.unit_size,'N/A') as UnitSize,
                isnull(poi.purchase_price ,0) as PurchasePrice,
                isnull(pbc.base_cost,0) as BaseCost,
                isnull(poi.packing_of_size,1)  as PackingOfSize,
                isnull(poi.item_amount ,0) as ItemAmount,
                isnull(iva.tax_value,0) as Iva,
                isnull(ieps.tax_value,0) as Ieps,
                isnull(poi.item_total_amount,0) as ItemTotalAmount,
                isnull(poi.item_status, 0) as ItemStatus,
                it.part_description as PartDescription,
                isnull(poi.iva, 0) as IvaTotal,
                isnull(poi.ieps , 0) as IepsTotal,
                po.currency as Currency
                from PURCHASE_ORDER_ITEM poi 
                join PURCHASE_ORDER po on po.purchase_no = poi.purchase_no
                inner join item it on it.part_number = poi.part_number
                inner join MA_TAX iva on iva.tax_code = it.part_iva_purchase
                inner join MA_TAX ieps on ieps.tax_code = it.part_ieps
                inner join ITEM_SUPPLIER its on its.part_number = it.part_number and its.supplier_id = (select supplier_id from PURCHASE_ORDER where purchase_no = poi.purchase_no)
                left join PURCHASE_BASE_COST pbc on pbc.supplier_part_number = its.supplier_part_number and pbc.status = 1
                where poi.purchase_no = @purchaseNo and poi.item_status = 1 
                ORDER by poi.item_position ", new SqlParameter("@purchaseNo", poNumber)).ToList();
            return ss;
        }

        public List<PurchaseOrderItemListModel> GetAllPurchasesDetail(string poNumber)
        {
            return _context.PURCHASE_ORDER_ITEM.Join(_context.ITEM, a => a.part_number, b => b.part_number, (a, b) => new { a, b }).Where(x => x.a.purchase_no == poNumber)
                .OrderBy(o => o.a.item_position)
               .Select(p => new PurchaseOrderItemListModel
               {
                   PartNumber = p.a.part_number,
                   Quantity = p.a.quantity ?? 0,
                   grQuantity = p.a.gr_quantity ?? 0,
                   UnitSize = p.a.unit_size.Trim() != "" && p.a.unit_size != null ? p.a.unit_size : "N/A",
                   PurchasePrice = p.a.purchase_price ?? 0,
                   Parcking = p.a.packing_of_size ?? 1, //cambiar a 0 por si falla
                   ItemAmount = p.a.item_amount ?? 0,
                   Iva = p.a.iva ?? 0,
                   Ieps = p.a.ieps ?? 0,
                   ItemTotalAmount = p.a.item_total_amount ?? 0,
                   itemStatus = p.a.item_status ?? 0,
                   Description = p.b.part_description
               }).ToList();
        }

        public List<PurchaseItemReportModel> GetAllProducts(string part_number, DateTime date, DateTime date2)
        {
            return _context.Database.SqlQuery<PurchaseItemReportModel>(@"
                select po.purchase_date as Date, poi.purchase_no as PurchaseNo, po.site_code as SiteCode,  
                st.site_name as SiteName,  poi.part_number as PartNumber, it.part_description as Description, 
                poi.quantity as Quantity, dbo.fnc_Get_Tax( poi.part_number,2) as Ieps, 
                dbo.fnc_Get_Tax( poi.part_number,1) as Iva,
                poi.purchase_price as Price
                from PURCHASE_ORDER_ITEM poi
                inner join PURCHASE_ORDER po on po.purchase_no = poi.purchase_no
                inner join ITEM it on it.part_number = poi.part_number
                inner join SITES st on st.site_code = po.site_code
                where  po.purchase_status = 9 and  poi.part_number = @part_number and po.purchase_date >= @date1 and  po.purchase_date<= @date2",
                new SqlParameter("@part_number", part_number),
                new SqlParameter("@date1", date.Date),
                new SqlParameter("@date2", date2.Date)).ToList();
        }

        public List<PurchaseItemReportModel> GetAllProductsSupplier(int supplier, DateTime date, DateTime date2)
        {
            return _context.Database.SqlQuery<PurchaseItemReportModel>(@"
               select po.purchase_date as Date, po.purchase_no as PurchaseNo, po.site_code as SiteCode,  
                st.site_name as SiteName,  po.invoice_no as InvoiceNo, po.supplier_id as SupplierId, 
                ms.business_name as Supplier, poi.item_amount as SubTotal, poi.iva as Iva, 
                poi.ieps as Ieps, poi.item_total_amount as Total,  po.currency as Currency
                from PURCHASE_ORDER po 
                inner join (select purchase_no,sum(item_total_amount) as item_total_amount,sum(ieps) as ieps, sum(iva) as iva,sum(item_amount)as item_amount from PURCHASE_ORDER_ITEM group by purchase_no) poi on po.purchase_no = poi.purchase_no  
                inner join SITES st on st.site_code = po.site_code
                inner join MA_SUPPLIER ms on ms.supplier_id = po.supplier_id
                where po.purchase_status = 9 and po.supplier_id = @supplier and po.purchase_date >= @date1 and  po.purchase_date<= @date2",
                new SqlParameter("@supplier", supplier),
                new SqlParameter("@date1", date.Date),
                new SqlParameter("@date2", date2.Date)).ToList();
        }

        public List<PurchaseOrderItemListModel> NewGetAllPurchasesDetail(string poNumber)
        {
            return _context.PURCHASE_ORDER_ITEM.Join(_context.ITEM, a => a.part_number, b => b.part_number, (a, b) => new { a, b }).Where(x => x.a.purchase_no == poNumber && x.a.item_status == 1)
            .OrderBy(o => o.a.item_position)
               .Select(p => new PurchaseOrderItemListModel
               {
                   PartNumber = p.a.part_number,
                   Quantity = p.a.original_quantity ?? 0,
                   grQuantity = p.a.gr_quantity ?? 0,
                   UnitSize = p.a.unit_size.Trim() != "" && p.a.unit_size != null ? p.a.unit_size : "N/A",
                   PurchasePrice = p.a.purchase_price ?? 0,
                   Parcking = p.a.packing_of_size ?? 1, //cambiar a 0 por si falla
                   ItemAmount = p.a.item_amount ?? 0,
                   Iva = p.a.iva ?? 0,
                   Ieps = p.a.ieps ?? 0,
                   ItemTotalAmount = p.a.item_total_amount ?? 0,
                   itemStatus = p.a.item_status ?? 0,
                   Description = p.b.part_description
               }).ToList();
        }

        public List<PurchaseOrderItemListModel> NewGetAllPurchasesDetailStatus(string poNumber, int status)
        {
            return _context.PURCHASE_ORDER_ITEM.Join(_context.ITEM, a => a.part_number, b => b.part_number, (a, b) => new { a, b }).Where(x => x.a.purchase_no == poNumber && x.a.item_status == status)
                .OrderBy(o => o.a.item_position)
               .Select(p => new PurchaseOrderItemListModel
               {
                   PartNumber = p.a.part_number,
                   Quantity = p.a.quantity ?? 0,
                   grQuantity = p.a.gr_quantity ?? 0,
                   UnitSize = p.a.unit_size.Trim() != "" && p.a.unit_size != null ? p.a.unit_size : "N/A",
                   PurchasePrice = p.a.purchase_price ?? 0,
                   Parcking = p.a.packing_of_size ?? 1, //cambiar a 0 por si falla
                   ItemAmount = p.a.item_amount ?? 0,
                   Iva = p.a.iva ?? 0,
                   Ieps = p.a.ieps ?? 0,
                   ItemTotalAmount = p.a.item_total_amount ?? 0,
                   itemStatus = p.a.item_status ?? 0,
                   Description = p.b.part_description
               }).ToList();
        }

        public List<PurchaseOrderItemListModel> GetAllPurchasesDetailByPurchaseNo(string poNumber)
        {
            return _context.PURCHASE_ORDER_ITEM.Join(_context.ITEM, a => a.part_number, b => b.part_number, (a, b) => new { a, b })
                .Join(_context.PURCHASE_ORDER, o => o.a.purchase_no, io => io.purchase_no, (o, io) => new { o, io })
                .Where(w => w.o.a.purchase_no == poNumber)
                .OrderBy(o => o.o.a.item_position)
                .Select(s => new PurchaseOrderItemListModel
                {
                    PartNumber = s.o.a.part_number,
                    requestedQuantity = s.o.a.original_quantity ?? 0,
                    iva_text = s.o.b.part_iva_purchase,
                    ieps_text = s.o.b.part_ieps,
                    assortedQuantity = s.o.a.gr_quantity ?? 0,
                    UnitSize = s.o.a.unit_size,
                    PurchasePrice = s.o.a.purchase_price ?? 0,
                    ItemAmount = s.o.a.item_amount ?? 0,
                    Iva = s.o.a.iva ?? 0,
                    Ieps = s.o.a.ieps ?? 0,
                    assortedAmount = s.o.a.item_total_amount ?? 0,
                    Description = s.o.b.part_description
                }).ToList();
        }


        public bool UpdatePurchaseOrderItemTime(string purchase_no, string part_number, string cuser)
        {
            try
            {

                var purchase_order = _context.PURCHASE_ORDER.Where(w => w.purchase_no == purchase_no).FirstOrDefault();
                var purchase_item = _context.PURCHASE_ORDER_ITEM.Where(w => w.purchase_no == purchase_no && w.part_number == part_number).FirstOrDefault();
                var blindCount = _context.BLIND_COUNT.Where(w => w.purchase_no == purchase_no).ToList();
                if (blindCount.Count() == 1) //Cuando no tenga productos se actualizará la fecha
                    purchase_order.purchase_date = DateTime.Now;
                purchase_order.udate = DateTime.Now;
                purchase_item.udate = DateTime.Now;
                purchase_order.uuser = cuser;
                purchase_item.uuser = cuser;
                purchase_order.program_id = "AndroidRecCieg";
                purchase_item.program_id = "AndroidRecCieg";

                _context.SaveChanges();

                return true;
            }
            catch (Exception e)
            {
                var msg = e.Message;
                return false;
            }
        }
    }
}