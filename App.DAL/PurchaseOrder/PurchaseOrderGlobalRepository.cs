﻿using App.Entities.ViewModels.PurchaseOrder;
using App.GlobalEntities;
using System;
using System.Collections.Generic;
using System.Linq;

namespace App.DAL.PurchaseOrder
{
    public class PurchaseOrderGlobalRepository
    {
        private readonly GlobalERPEntities _context;

        public PurchaseOrderGlobalRepository()
        {
            _context = new GlobalERPEntities();
        }

        public List<PurchaseOrderGlobalModel> GetPurchaseOrder(DateTime startDate, DateTime finishDate, string site)
        {
            try
            {
                var x = _context.PURCHASE_ORDER
                    .Join(_context.MA_SUPPLIER, a => a.supplier_id, b => b.supplier_id, (order, supplier) => new { order, supplier })
                    .Where(e => e.order.purchase_date <= finishDate && e.order.purchase_date >= startDate && e.order.site_code == site)
                    .Select(order => new PurchaseOrderGlobalModel
                    {
                        supplier_id = order.order.supplier_id,
                        purchase_no = order.order.purchase_no,
                        folio_requierement = order.order.folio_requirement,
                        supplier = order.supplier.commercial_name,
                        purchase_date = order.order.purchase_date.ToString(),
                        purchase_status = order.order.purchase_status
                    }).ToList();
                return x;
            }
            catch (Exception)
            {
                return null;
            }
        }

        public List<PurchaseOrderGlobalModel> GetPurchaseOrderByStatus(DateTime startDate, DateTime finishDate, int status, string site)
        {
            try
            {
                var x = _context.PURCHASE_ORDER
                    .Join(_context.MA_SUPPLIER, a => a.supplier_id, b => b.supplier_id, (order, supplier) => new { order, supplier })
                    .Where(e => e.order.purchase_date <= finishDate && e.order.purchase_date >= startDate && e.order.purchase_status == status && e.order.site_code == site)
                    .Select(order => new PurchaseOrderGlobalModel
                    {
                        supplier_id = order.order.supplier_id,
                        purchase_no = order.order.purchase_no,
                        folio_requierement = order.order.folio_requirement,
                        supplier = order.supplier.commercial_name,
                        purchase_date = order.order.purchase_date.ToString(),
                        purchase_status = order.order.purchase_status
                    }).ToList();
                return x;
            }
            catch (Exception)
            {
                return null;
            }
        }

        public List<PurchaseOrderGlobalModel> GetDetailOrder(int purchase_no)
        {
            try
            {
                var x = _context.PURCHASE_ORDER_ITEM.Where(e => e.purchase_no == purchase_no)
                .Join(_context.ITEM_CREDITORS, a => a.part_number, b => b.part_number, (order, item) => new { order, item })
                .Join(_context.PURCHASE_ORDER, a => a.order.purchase_no, b => b.purchase_no, (order2, header) => new { order2, header })
                .Where(c => c.order2.order.purchase_no == purchase_no)
                .Select(detail => new PurchaseOrderGlobalModel
                {
                    part_number = detail.order2.order.part_number,
                    part_description = detail.order2.item.description,
                    quantity = detail.order2.order.quantity,
                    item_total_amount = detail.order2.order.item_total_amount,
                    supplier_id = detail.header.supplier_id
                }).ToList();
                return x;
            }
            catch (Exception e)
            {
                var msg = e.Message;
                return null;
            }
        }

        public bool UpdatePurchaseOrder(string user, int expense_id, int id)
        {
            try
            {
                var order = _context.PURCHASE_ORDER.Where(elements => elements.purchase_no == id).SingleOrDefault();
                order.purchase_status = 9;
                order.udate = DateTime.Now;
                order.uuser = user;
                order.program_id = "PURC008.cshtml";
                order.expense_id = expense_id;
                _context.SaveChanges();
                return true;
            }
            catch (Exception)
            {
                return false;
                throw;
            }
        }
    }
}