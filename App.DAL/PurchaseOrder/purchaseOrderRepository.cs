﻿using App.DAL.Item;
using App.Entities;
using App.Entities.ViewModels.Item;
using App.Entities.ViewModels.PurchaseOrder;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.SqlClient;
using System.Linq;

namespace App.DAL.PurchaseOrder
{
    public class PurchaseOrderRepository
    {
        private readonly DFL_SAIEntities _context;
        private readonly ItemRepository _ItemRepo;

        public PurchaseOrderRepository()
        {
            _context = new DFL_SAIEntities();
            _ItemRepo = new ItemRepository();
        }

        public int AddOrderPurchase(string numberFolio, int Folio, string SiteCode, int supplier, DateTime orderDate, string currency, string comment, string sesion, string program_id)
        {
            try
            {
                var Purchase = new PURCHASE_ORDER()
                {
                    purchase_no = numberFolio,
                    folio = Folio,
                    site_code = SiteCode,
                    supplier_id = supplier,
                    purchase_date = DateTime.Now,
                    eta = orderDate,
                    currency = currency,
                    purchase_status = 1,
                    purchase_remark = comment,
                    cuser = sesion,
                    cdate = DateTime.Now,
                    program_id = program_id
                };

                _context.PURCHASE_ORDER.Add(Purchase);
                var save = _context.SaveChanges();

                return save;
            }

            catch (Exception ex)
            {
                var msg = ex.Message;
                return 0;
            }
        }

        public bool AddNewPurchaseOrder(PURCHASE_ORDER PurchaseOrder)
        {
            try
            {
                _context.PURCHASE_ORDER.Add(PurchaseOrder);
                _context.SaveChanges();
                return true;
            }
            catch (Exception)
            {
                return false;
                throw;
            }
        }

        public bool RemovePurchaseOrder(string Document)
        {
            try
            {
                var Purchase = _context.PURCHASE_ORDER.Where(x => x.purchase_no == Document).SingleOrDefault();
                _context.PURCHASE_ORDER.Remove(Purchase);
                _context.SaveChanges();
                return true;
            }
            catch (Exception)
            {
                return false;
                throw;
            }
        }

        public int AddPurchaseOrderAndroid(PURCHASE_ORDER order)
        {
            try
            {
                _context.PURCHASE_ORDER.Add(order);
                var save = _context.SaveChanges();
                return save;
            }
            catch (Exception ex)
            {
                var msg = ex.Message;
                return 0;
            }
        }

        public int AddPurchaseOrderItem(string NumberFolio, string PartNumber, decimal Quantity, string UnitSize, decimal PurchasePrice, int PackingOfSize, decimal ItemAmount, decimal Iva, decimal Ieps, decimal ItemTotalAmount, string sesion)
        {
            try
            {
                var Purchase = new PURCHASE_ORDER_ITEM()
                {
                    purchase_no = NumberFolio,
                    part_number = PartNumber,
                    quantity = Quantity,
                    unit_size = UnitSize,
                    purchase_price = PurchasePrice,
                    packing_of_size = PackingOfSize,
                    item_amount = ItemAmount,
                    iva = Iva,
                    ieps = Ieps,
                    item_total_amount = ItemTotalAmount,
                    item_status = 1,
                    cuser = sesion,
                    cdate = DateTime.Now,
                    program_id = "PURC001.cshtml"

                };
                _context.PURCHASE_ORDER_ITEM.Add(Purchase);
                var save = _context.SaveChanges();

                return save;
            }
            catch (Exception ex)
            {
                var msg = ex.Message;
                return 0;
            }
        }

        public bool AddNewPurchaseOrderItem(List<PURCHASE_ORDER_ITEM> PurchaseOrderItem)
        {
            try
            {
                _context.PURCHASE_ORDER_ITEM.AddRange(PurchaseOrderItem);
                _context.SaveChanges();
                return true;
            }
            catch (Exception ex)
            {
                var msg = ex.Message;
                return false;
            }
        }

        public string GetContactSupplier(int supplier)
        {
            try
            {
                string email = _context.MA_SUPPLIER_CONTACT.Where(x => x.supplier_id == supplier && x.flagActive == true && x.departament == "Ventas").Select(u => u.email).FirstOrDefault();

                return email;
            }
            catch (Exception ex)
            {
                return ex.ToString();
            }
        }

        public List<PurchaseOrderModel> GetPurchaseOrder(string PurchaseNo)
        {
            return _context.Database.SqlQuery<PurchaseOrderModel>(@"select po.purchase_no as PurchaseNo, 
                    po.site_code as SiteCode,
                    case  when po.purchase_date is null then 'N/A' else po.purchase_date end as PurchaseDate  ,
                    po.currency as Currency,
                    it.part_number as PartNumber,
                    it.part_description as Description,
                    poi.quantity as Quantity,
                    poi.unit_size as UnitSize,
                    poi.purchase_price as PurchasePrice,
                    poi.item_amount as ItemAmount,
                    poi.iva as Iva,
                    poi.ieps as Ieps
                    from PURCHASE_ORDER po
                    join PURCHASE_ORDER_ITEM poi on poi.purchase_no = po.purchase_no
                    join ITEM it on it.part_number = poi.part_number
                    where po.purchase_no = @partNumber", new SqlParameter("@partNumber", PurchaseNo)).ToList();
        }

        public List<Entities.ViewModels.Android.PurchaseOrderModel> GetAllOCInStatus1()
        {
            return _context.PURCHASE_ORDER.Join(_context.SITES, a => a.site_code, b => b.site_code, (a, b) => new { a, b })
                .Join(_context.MA_SUPPLIER, x => x.a.supplier_id, supplier => supplier.supplier_id, (x, supplier) => new { x, supplier })
                .Where(o => o.x.a.purchase_status == 1)
                .OrderBy(y => y.x.a.purchase_date)
                .Select(p => new Entities.ViewModels.Android.PurchaseOrderModel
                {
                    purchase_no = p.x.a.purchase_no,
                    site_code = p.x.b.site_code,
                    site_name = p.x.b.site_name,
                    supplier_id = p.supplier.supplier_id.ToString(),
                    supplier_name = p.supplier.commercial_name,
                    purchase_date = p.x.a.purchase_date.Value
                })
                .ToList();
        }

        public List<Entities.ViewModels.Android.PurchaseOrderModel> GetAllOCInStatus1BySupplier(String supplierr)
        {
            return _context.PURCHASE_ORDER.Join(_context.SITES, a => a.site_code, b => b.site_code, (a, b) => new { a, b })
                .Join(_context.MA_SUPPLIER, x => x.a.supplier_id, supplier => supplier.supplier_id, (x, supplier) => new { x, supplier })
                .Where(o => (o.x.a.purchase_status == 1 || o.x.a.purchase_status == 5) && o.x.a.supplier_id.ToString() == supplierr) //Pedido normal y Pie de camion (reescaneo)
                .OrderBy(y => y.x.a.purchase_date)
                .Select(p => new Entities.ViewModels.Android.PurchaseOrderModel
                {
                    purchase_no = p.x.a.purchase_no,
                    site_code = p.x.b.site_code,
                    site_name = p.x.b.site_name,
                    supplier_id = p.supplier.supplier_id.ToString(),
                    supplier_name = p.supplier.commercial_name,
                    purchase_date = p.x.a.purchase_date.Value
                }).ToList();
        }

        public List<PurchaseOrderRCModel> GetAllOC()
        {
            return _context.PURCHASE_ORDER.Where(x => x.purchase_status == 3 || x.purchase_status == 4)
                .Select(x => new PurchaseOrderRCModel
                {
                    PurchaseNo = x.purchase_no,
                    SiteCode = x.site_code,
                    SupplierId = x.supplier_id != null ? x.supplier_id.Value : 0
                }).ToList();
        }

        public PURCHASE_ORDER GetPurchaseOrderByNo(string purchase_no)
        {
            return _context.PURCHASE_ORDER.Where(x => x.purchase_no == purchase_no && (x.purchase_status == 1 || x.purchase_status == 5)) //Reescaneo(Pie de Camión) /Pedido
                .Select(d => d).SingleOrDefault();
        }

        public PURCHASE_ORDER GetPurchaseOrderByNoAllStatus(string purchase_no)
        {
            return _context.PURCHASE_ORDER.Where(x => x.purchase_no == purchase_no)
                .Select(d => d).SingleOrDefault();
        }

        public int GetUpdatePurchasePrintStatus(string purchase_no)
        {
            var purchase = _context.PURCHASE_ORDER.Where(x => x.purchase_no == purchase_no).SingleOrDefault();
            if (purchase != null)
            {
                if (purchase.print_status == null)
                {
                    purchase.print_status = 1;
                }
                else if (purchase.print_status == 1)
                {
                    purchase.print_status = 2;
                }
                _context.SaveChanges();
                return purchase.print_status ?? 0;
            }
            return 0;
        }

        public int UpdatePurchaseOrderFromAndroid(PURCHASE_ORDER purchase)
        {
            _context.Entry(purchase).State = System.Data.Entity.EntityState.Modified;
            return _context.SaveChanges();
        }

        public string GetOCRC(string PurchaseNo)
        {
            var Purchase = _context.PURCHASE_ORDER.Where(x => x.purchase_no == PurchaseNo).SingleOrDefault();
            if (Purchase != null)
            {
                if (Purchase.purchase_status == 3)
                    return "S3";
                if (Purchase.purchase_status == 4)
                    return "S4";
                else if (Purchase.purchase_status == 1)
                    return "S1";
                else if (Purchase.purchase_status == 5)
                    return "S5";
                else if (Purchase.purchase_status == 9)
                    return "S9";

                return "S0";
            }
            return "N/A";
        }

        public ItemPurchase ValidationPurchaseOrder(string purchaseNo, string partNumber)
        {
            return _context.Database.SqlQuery<ItemPurchase>(@"select top 1
                isnull(poi.purchase_price ,0) as PurchasePrice,
                isnull(pbc.base_cost,0) as BaseCost
                from PURCHASE_ORDER_ITEM poi 
                inner join item it on it.part_number = poi.part_number
                inner join MA_TAX iva on iva.tax_code = it.part_iva_purchase
                inner join MA_TAX ieps on ieps.tax_code = it.part_ieps
                inner join ITEM_SUPPLIER its on its.part_number = it.part_number and its.supplier_id = (select supplier_id from PURCHASE_ORDER where purchase_no = poi.purchase_no)
                left join PURCHASE_BASE_COST pbc on pbc.supplier_part_number = its.supplier_part_number and pbc.status = 1
                where purchase_no = @purchaseNo and poi.part_number = @partNumber
                ", new SqlParameter("@purchaseNo", purchaseNo), new SqlParameter("@partNumber", partNumber)).SingleOrDefault();
        }

        public Tuple<ItemPurchase, int> EditPurchaseOrder(string purchaseNo, decimal price, string partNumber, string user)
        {
            try
            {
                var purchases = _context.Database.SqlQuery<ItemPurchase>(@"
                update PURCHASE_ORDER_ITEM set purchase_price = @numero,
                item_amount = (@numero * isnull(poi.quantity,1) ),
                ieps = ((@numero * isnull(poi.quantity,1) ) * (mta.tax_value /100)) ,
                iva = ((@numero * isnull(poi.quantity,1) ) +((@numero * isnull(poi.quantity,1) ) * (mta.tax_value /100)))* (mt.tax_value /100),
                item_total_amount = (@numero * isnull(poi.quantity,1) ) + ((@numero * isnull(poi.quantity,1) ) * (mta.tax_value /100))+ ((@numero * isnull(poi.quantity,1) ) +((@numero * isnull(poi.quantity,1) ) * (mta.tax_value /100)))* (mt.tax_value /100),
                uuser = @user,
                udate = GETDATE(), 
                program_id = 'PURC0014.cshtml'
                from PURCHASE_ORDER_ITEM poi  
                inner join ITEM it on it.part_number = poi.part_number
                inner join MA_TAX mt on mt.tax_code = it.part_iva_purchase
                inner join MA_TAX mta on mta.tax_code = it.part_ieps
                where purchase_no = @purchaseNo and poi.part_number = @partNumber
                
                select top 1
                poi.part_number as  PartNumber,
                isnull(poi.quantity,0) as Quantity,
                isnull(poi.gr_quantity,0) as GrQuantity,
                isnull(poi.unit_size,'N/A') as UnitSize,
                isnull(poi.purchase_price ,0) as PurchasePrice,
                isnull(pbc.base_cost,0) as BaseCost,
                isnull(poi.packing_of_size,1)  as PackingOfSize,
                isnull(poi.item_amount ,0) as ItemAmount,
                isnull(iva.tax_value,0) as Iva,
                isnull(ieps.tax_value,0) as Ieps,
                isnull(poi.item_total_amount,0) as ItemTotalAmount,
                isnull(poi.item_status, 0) as ItemStatus,
                it.part_description as PartDescription,
                isnull(poi.iva, 0) as IvaTotal,
                isnull(poi.ieps , 0) as IepsTotal
                
                from PURCHASE_ORDER_ITEM poi 
                inner join item it on it.part_number = poi.part_number
                inner join MA_TAX iva on iva.tax_code = it.part_iva_purchase
                inner join MA_TAX ieps on ieps.tax_code = it.part_ieps
                inner join ITEM_SUPPLIER its on its.part_number = it.part_number and its.supplier_id = (select supplier_id from PURCHASE_ORDER where purchase_no = poi.purchase_no)
                left join PURCHASE_BASE_COST pbc on pbc.supplier_part_number = its.supplier_part_number and pbc.status = 1
                where purchase_no = @purchaseNo and poi.part_number = @partNumber
                ", new SqlParameter("@numero", price),
                new SqlParameter("@purchaseNo", purchaseNo),
                new SqlParameter("@partNumber", partNumber),
                new SqlParameter("@user", user)).SingleOrDefault();
                if (purchases != null)
                    return new Tuple<ItemPurchase, int>(purchases, 0);

                return new Tuple<ItemPurchase, int>(new ItemPurchase(), 2);
            }
            catch (Exception)
            {
                return new Tuple<ItemPurchase, int>(new ItemPurchase(), 1);
                throw;
            }
        }

        public int DeleteProductPurchaseOrder(string purchaseNo, string partNumber, string user)
        {
            var purchaseOrden = _context.PURCHASE_ORDER.Where(w => w.purchase_no == purchaseNo).FirstOrDefault();
            if (purchaseOrden.purchase_status == 4 || purchaseOrden.purchase_status == 5)
                return 6; //Es necesario eliminar el producto desde la pocket
            var product = _context.PURCHASE_ORDER_ITEM.Where(x => x.purchase_no == purchaseNo && x.part_number == partNumber).SingleOrDefault();
            if (product != null)
            {
                product.item_status = 7;
                product.uuser = user;
                product.udate = DateTime.Now;
                product.program_id = "PURCH0014.cshtml7";
                _context.SaveChanges();
                return 1;
            }
            return 2;
        }

        public int SupplierPurchaseOrder(string purchaseNo)
        {
            var purchase = _context.PURCHASE_ORDER.Where(x => x.purchase_no == purchaseNo).SingleOrDefault();
            if (purchase != null)
            {
                return purchase.supplier_id ?? 0;
            }
            return 0;
        }

        public int UpdatePurchaseOrder(string purchaseNo, int supplier, string user)
        {
            try
            {
                var purchase = _context.PURCHASE_ORDER_ITEM.Where(x => x.purchase_no == purchaseNo && x.item_status == 1).ToList();
                foreach (var item in purchase)
                {
                    var Cost = _ItemRepo.GetCostItem(item.part_number, supplier);
                    item.ieps = Cost.IEPS * item.quantity;
                    item.iva = Cost.IVA * item.quantity;
                    item.item_amount = Cost.BaseCost * item.quantity;
                    item.purchase_price = Cost.BaseCost;
                    item.item_total_amount = (Cost.IEPS * item.quantity) + (Cost.IVA * item.quantity) + (Cost.BaseCost * item.quantity);
                    item.uuser = user;
                    item.udate = DateTime.Now;
                    item.program_id = "PURC0014.cshtml";
                }
                _context.SaveChanges();
                return 1;
            }
            catch (Exception)
            {
                return 2;
            }
        }

        public int PurchaseOrderAddPartNumber(string purchaseNo, string partNumber, decimal Quantity, string User, CostInfo Cost)
        {
            try
            {
                var purchaseOrden = _context.PURCHASE_ORDER.Where(w => w.purchase_no == purchaseNo).FirstOrDefault();
                if (purchaseOrden.purchase_status == 4 || purchaseOrden.purchase_status == 5)
                    return 6; //Es necesario agregar el producto desde la pocket
                var purchase = _context.PURCHASE_ORDER_ITEM.Where(x => x.purchase_no == purchaseNo && x.part_number == partNumber).SingleOrDefault();
                if (purchase != null)
                {
                    if (purchase.item_status == 7)
                    {
                        purchase.quantity = Quantity;
                        purchase.ieps = Cost.IEPS * Quantity;
                        purchase.iva = Cost.IVA * Quantity;
                        purchase.item_amount = Cost.BaseCost * Quantity;
                        purchase.purchase_price = Cost.BaseCost;
                        purchase.item_total_amount = (Cost.IEPS * Quantity) + (Cost.IVA * Quantity) + (Cost.BaseCost * Quantity);
                        purchase.uuser = User;
                        purchase.udate = DateTime.Now;
                        purchase.item_status = 1;
                        purchase.program_id = "PURC0014.cshtml";
                        _context.SaveChanges();
                        return 5;
                    }
                    return 2;
                }
                else
                {
                    _context.PURCHASE_ORDER_ITEM.Add(new PURCHASE_ORDER_ITEM()
                    {
                        cdate = DateTime.Now,
                        cuser = User,
                        program_id = "PURC0014.cshtml",
                        quantity = Quantity,
                        original_quantity = Quantity,
                        ieps = Cost.IEPS * Quantity,
                        iva = Cost.IVA * Quantity,
                        item_amount = Cost.BaseCost * Quantity,
                        item_status = 1,
                        item_total_amount = (Cost.IEPS * Quantity) + (Cost.IVA * Quantity) + (Cost.BaseCost * Quantity),
                        packing_of_size = 1,
                        part_number = partNumber,
                        unit_size = Cost.UnitSize,
                        purchase_price = Cost.BaseCost,
                        purchase_no = purchaseNo
                    });
                    _context.SaveChanges();
                    return 1;
                }
            }
            catch (Exception e)
            {
                return 4;
            }
        }

        public int ExecuteSprocPurchaseOrderAddItem(int Folio, string Document, string User)
        {
            return _context.Database.SqlQuery<int>(@"
                DECLARE	
                @return_value int
                EXEC	@return_value = [dbo].[sproc_PurchaseOrder_AddItem]
                		@po_no = @PoNo,
                		@folio = @Folio,
                		@cuser = @Cuser,
                		@program_id = @ProgramId
                
                SELECT	'Return Value' = @return_value",
                new SqlParameter("@PoNo", Document),
                new SqlParameter("@Folio", Folio),
                new SqlParameter("@Cuser", User),
                new SqlParameter("@ProgramId", "ORDE002.cshtml")
                ).SingleOrDefault();
        }

        public int ExecuteSpCreatePurchaseOrder(int Folio, string Document, int Supplier)
        {
            var Order = _context.ORDER_LIST.Where(x => x.folio == Folio && x.authorized > 0).ToList();
            int Valor = 1;
            foreach (var item in Order)
            {
                var NumberPurchase = _context.Database.SqlQuery<StoreProcedureResult>("DECLARE	@return_value int, @Document varchar(10) SELECT  @Document = N'" + Document + "' EXEC    @return_value = [dbo].[sp_Create_PurchaseOrder] @supplier_id=" + Supplier.ToString() + ", @Folio = " + Folio + ", @Document = @Document, @part_number = N'" + item.part_number + "' SELECT  'Document' = @Document, 'ReturnValue' = @return_value").ToList();
                if (NumberPurchase[0].ReturnValue != 6001)
                    Valor = 2;
            }
            return Valor;
        }

        public bool StatusPurchase(string NumberPruchase, List<PurchaseOrderItemModel> OCRC, string User)
        {
            var Purchase = _context.PURCHASE_ORDER.Where(x => x.purchase_no == NumberPruchase).SingleOrDefault();
            if (Purchase != null)
            {
                if (Purchase.purchase_status == 4) // Pie de camión
                {
                    Purchase.purchase_status = 5; //Reescaneo Pie de Camión
                    _context.SaveChanges();
                }
                else
                {
                    Purchase.purchase_status = 1; //Pedido normal
                    _context.SaveChanges();

                    if (OCRC != null)
                    {
                        foreach (var item in OCRC)
                        {
                            if (item.QuantityOC != item.QuantityRC)
                            {
                                var ResetRc = _context.BLIND_COUNT.Where(x => x.purchase_no == NumberPruchase && x.part_number == item.ParthNumber).SingleOrDefault();
                                if (ResetRc != null)
                                {
                                    ResetRc.udate = DateTime.Now;
                                    ResetRc.uuser = User;
                                    ResetRc.program_id = "OCRC001.cshtmlRC";
                                    ResetRc.quantity = 0;
                                    _context.SaveChanges();
                                }
                                var PurchaseItems = _context.PURCHASE_ORDER_ITEM.Where(w => w.purchase_no == NumberPruchase && w.part_number == item.ParthNumber).SingleOrDefault();
                                PurchaseItems.quantity = PurchaseItems.original_quantity;
                                PurchaseItems.item_amount = PurchaseItems.purchase_price * PurchaseItems.original_quantity;
                                var IepsIva = new PurchaseOrderTaxModel()
                                {
                                    Ieps = 0,
                                    Iva = 0
                                };

                                var TaxDefault = _context.ITEM.Where(x => x.part_number == item.ParthNumber).SingleOrDefault();
                                var Ieps = _context.MA_TAX.Where(x => x.tax_code == TaxDefault.part_ieps).SingleOrDefault();
                                var IVA = _context.MA_TAX.Where(x => x.tax_code == TaxDefault.part_iva_purchase).SingleOrDefault();

                                if (Ieps != null)
                                    IepsIva.Ieps = Ieps.tax_value != null ? Ieps.tax_value.Value : 0;

                                if (IVA != null)
                                    IepsIva.Iva = IVA.tax_value != null ? IVA.tax_value.Value : 0;

                                if (IepsIva.Ieps > 0)
                                {
                                    PurchaseItems.ieps = PurchaseItems.item_amount * (IepsIva.Ieps / 100);
                                    PurchaseItems.iva = (PurchaseItems.item_amount + PurchaseItems.ieps) * (IepsIva.Iva / 100);
                                }
                                else
                                {
                                    PurchaseItems.ieps = 0;
                                    PurchaseItems.iva = PurchaseItems.item_amount * (IepsIva.Iva / 100);
                                }

                                PurchaseItems.item_total_amount = (PurchaseItems.ieps != null ? PurchaseItems.ieps : 0) + (PurchaseItems.iva != null ? PurchaseItems.iva : 0) + PurchaseItems.item_amount;
                                PurchaseItems.item_status = 1;
                                PurchaseItems.udate = DateTime.Now;
                                PurchaseItems.uuser = User;
                                PurchaseItems.program_id = "OCRC001.cshtmlRC";
                                _context.SaveChanges();
                            }
                        }
                    }
                }
                return true;
            }
            return false;
        }

        public List<PurchaseOrderModel> GetAllGenericPurchasesOrders(DateTime? date1, DateTime? date2, int status, int supp, string partNumber, string department, string family, string currency)
        {
            var purchase = _context.PURCHASE_ORDER.Join(_context.SITES, a => a.site_code, b => b.site_code, (a, b) => new { a, b })
            .Join(_context.MA_SUPPLIER, x => x.a.supplier_id, supplier => supplier.supplier_id, (x, supplier) => new { x, supplier })
            .Join(_context.PURCHASE_ORDER_ITEM, y => y.x.a.purchase_no, h => h.purchase_no, (y, h) => new { y, h })
            .Join(_context.ITEM, n => n.h.part_number, o => o.part_number, (n, o) => new { n, o })
            .Where(o => o.n.h.item_status == 1 || o.n.h.item_status == 8);
            if (date1.HasValue && date2.HasValue)
                purchase = purchase.Where(o => o.n.y.x.a.ata != null ? (o.n.y.x.a.ata >= date1 && o.n.y.x.a.ata <= date2) : (o.n.y.x.a.purchase_date >= date1 && o.n.y.x.a.purchase_date <= date2));

            if (status > 0)
            {
                if (status != 3)
                    purchase = purchase.Where(o => o.n.y.x.a.purchase_status == status);
                else
                    purchase = purchase.Where(o => (o.n.y.x.a.purchase_status == 3 || o.n.y.x.a.purchase_status == 4 || o.n.y.x.a.purchase_status == 5)); //PEdidos y todos sus derivados
            }

            if (supp > 0)
                purchase = purchase.Where(o => o.n.y.x.a.supplier_id == supp);

            if (!string.IsNullOrWhiteSpace(partNumber))
                purchase = purchase.Where(o => o.n.h.part_number == partNumber);

            if (!string.IsNullOrWhiteSpace(department))
            {
                int level_header = Convert.ToInt32(department);
                purchase = purchase.Where(o => o.o.level_header == level_header);
            }

            if (!string.IsNullOrWhiteSpace(family))
            {
                int level1_code = Convert.ToInt32(family);
                purchase = purchase.Where(o => o.o.level1_code == level1_code);
            }

            if (!string.IsNullOrWhiteSpace(currency))
                purchase = purchase.Where(o => o.n.y.x.a.currency == currency);

            return purchase.OrderBy(y => y.n.y.x.a.purchase_no).GroupBy(g => new { g.n.y.x.a.purchase_no, g.n.y.x.b.site_code, g.n.y.x.b.site_name, g.n.y.x.a.supplier_id, g.n.y.supplier.business_name, g.n.y.x.a.purchase_date, g.n.y.x.a.currency, g.n.y.x.a.purchase_status, g.n.y.x.a.cdate, g.n.y.x.a.invoice_no, g.n.y.x.a.eta, g.n.y.x.a.ata, g.n.y.x.a.udate })
                .Select(p => new PurchaseOrderModel
                {
                    PurchaseNo = p.Key.purchase_no,
                    SiteCode = p.Key.site_code,
                    SiteName = p.Key.site_name,
                    supplier_id = p.Key.supplier_id.Value.ToString(),
                    supplier_name = p.Key.business_name,
                    PurchaseDate = p.Key.purchase_date.Value,
                    //Eta = p.Key.ata.HasValue ? p.Key.ata.Value : p.Key.purchase_date.Value,
                    Eta = p.Key.purchase_status == 8 ? p.Key.udate.Value : p.Key.ata.HasValue ? p.Key.ata.Value : p.Key.purchase_date.Value,
                    invoice = p.Key.invoice_no ?? "SIN FOLIO",
                    Currency = p.Key.currency,
                    Iva = p.Sum(f => f.n.h.iva.Value),
                    Ieps = p.Sum(f => f.n.h.ieps.Value),
                    ItemAmount = p.Sum(f => f.n.h.item_amount.Value),
                    ItemTotalAmount = p.Sum(f => f.n.h.item_total_amount.Value),
                    purchase_status_value = p.Key.purchase_status.Value
                }).OrderBy(o => o.supplier_name).ToList();
        }

        public List<PurchaseOrderModel> GetAllGenericPurchasesOrdersXML(DateTime? date1, DateTime? date2, int supp, string partNumber, string department, string family, string currency)
        {
            var purchase = _context.PURCHASE_ORDER
            .Join(_context.SITES, a => a.site_code, b => b.site_code, (a, b) => new { a, b })
            .Join(_context.MA_SUPPLIER, x => x.a.supplier_id, supplier => supplier.supplier_id, (x, supplier) => new { x, supplier })
            .Join(_context.PURCHASE_ORDER_ITEM, y => y.x.a.purchase_no, h => h.purchase_no, (y, h) => new { y, h })
            .Join(_context.ITEM, n => n.h.part_number, o => o.part_number, (n, o) => new { n, o })
            .Where(o => o.n.h.item_status == 1 || o.n.h.item_status == 8);

            if (date1.HasValue && date2.HasValue)
                purchase = purchase.Where(o => o.n.y.x.a.ata != null ? (o.n.y.x.a.ata >= date1 && o.n.y.x.a.ata <= date2) : (o.n.y.x.a.purchase_date >= date1 && o.n.y.x.a.purchase_date <= date2));

            purchase = purchase.Where(o => o.n.y.x.a.purchase_status == 9 && (o.n.y.x.a.uuid == null || o.n.y.x.a.uuid == ""));

            if (supp > 0)
                purchase = purchase.Where(o => o.n.y.x.a.supplier_id == supp);

            if (!string.IsNullOrWhiteSpace(partNumber))
                purchase = purchase.Where(o => o.n.h.part_number == partNumber);

            if (!string.IsNullOrWhiteSpace(department))
            {
                int level_header = Convert.ToInt32(department);
                purchase = purchase.Where(o => o.o.level_header == level_header);
            }

            if (!string.IsNullOrWhiteSpace(family))
            {
                int level1_code = Convert.ToInt32(family);
                purchase = purchase.Where(o => o.o.level1_code == level1_code);
            }

            if (!string.IsNullOrWhiteSpace(currency))
                purchase = purchase.Where(o => o.n.y.x.a.currency == currency);

            return purchase.OrderBy(y => y.n.y.x.a.purchase_no).GroupBy(g => new { g.n.y.x.a.purchase_no, g.n.y.x.b.site_code, g.n.y.x.b.site_name, g.n.y.x.a.supplier_id, g.n.y.supplier.business_name, g.n.y.x.a.purchase_date, g.n.y.x.a.currency, g.n.y.x.a.purchase_status, g.n.y.x.a.cdate, g.n.y.x.a.invoice_no, g.n.y.x.a.eta, g.n.y.x.a.ata })
                .Select(p => new PurchaseOrderModel
                {
                    PurchaseNo = p.Key.purchase_no,
                    SiteCode = p.Key.site_code,
                    SiteName = p.Key.site_name,
                    supplier_id = p.Key.supplier_id.Value.ToString(),
                    supplier_name = p.Key.business_name,
                    PurchaseDate = p.Key.purchase_date.Value,
                    Eta = p.Key.ata ?? p.Key.eta.Value,
                    invoice = p.Key.invoice_no ?? "SIN FOLIO",
                    Currency = p.Key.currency,
                    Iva = p.Sum(f => f.n.h.iva.Value),
                    Ieps = p.Sum(f => f.n.h.ieps.Value),
                    ItemAmount = p.Sum(f => f.n.h.item_amount.Value),
                    ItemTotalAmount = p.Sum(f => f.n.h.item_total_amount.Value),
                    purchase_status_value = p.Key.purchase_status.Value
                }).OrderBy(o => o.supplier_name).ToList();
        }

        public List<PurchaseOrderItemDetailReportGeneral> GetAllGenericPurchasesOrdersDetail(DateTime date1, DateTime date2, int status, int supp, string partNumber, string department, string family, string currency)
        {
            var consultaGenerico = @"SELECT MC1.class_name Department, MC2.class_name Family,
                TD.part_number PartNumber, I.part_description DescriptionC, 'ENTRADA: ORDEN DE COMPRA' TransationType,
                ISNULL(TD.gr_quantity, TD.original_quantity) Quantity,
                TD.purchase_price CostUnit,
                CAST(ISNULL(TD.gr_quantity, TD.original_quantity)*TD.purchase_price AS decimal(18,4)) SubTotal,
                M1.name TASA_IVA,
                CAST(TD.iva AS decimal(18,4)) IVA,
                M2.name TASA_IEPS,
                CAST(TD.ieps AS decimal(18,4)) IEPS,
                CAST(TD.item_total_amount AS decimal(18,4)) Amount,
                CASE
	                WHEN TD.item_status = 1 and TH.purchase_status = 1 THEN 'Iniciado'
	                WHEN TD.item_status = 1 and (TH.purchase_status = 1 OR TH.purchase_status = 3 OR TH.purchase_status = 4 OR TH.purchase_status = 5) THEN 'Escaneando'
	                WHEN TD.item_status = 7 THEN 'Producto Cancelado'
	                WHEN TD.item_status = 8 THEN 'Cancelado'
	                WHEN TD.item_status = 1 and TH.purchase_status = 9 THEN 'Terminando'
                END as Item_Status, 
                M.business_name Supplier, TH.purchase_no Folio, CONVERT(VARCHAR,TH.ata,101) MerchandiseEntry, CONVERT(VARCHAR,TH.eta,101) MerchandiseEntryEta, TH.currency Currency
                FROM PURCHASE_ORDER_ITEM TD
                JOIN PURCHASE_ORDER TH ON TH.purchase_no = TD.purchase_no
                JOIN ITEM I ON I.part_number = TD.part_number
                JOIN MA_SUPPLIER M ON M.supplier_id = TH.supplier_id
                JOIN MA_TAX M1 ON M1.tax_code = I.part_iva_sale
                JOIN MA_TAX M2 ON M2.tax_code = I.part_ieps
                JOIN MA_CLASS MC1 ON MC1.class_id = I.level_header
                JOIN MA_CLASS MC2 ON MC2.class_id = I.level1_code
                WHERE CONVERT(date, ISNULL(TH.ata, TH.purchase_date),101) >= @date1 and  CONVERT(date, ISNULL(TH.ata, TH.purchase_date),101)<= @date2 ";
            //Parametros
            List<SqlParameter> parameters = new List<SqlParameter>();
            parameters.Add(new SqlParameter("@date1", date1.Date));
            parameters.Add(new SqlParameter("@date2", date2.Date));
            if (status != 0)
            {
                if (status == 8)
                    consultaGenerico += " and TH.purchase_status = @status AND TD.item_status = @status";
                else if (status == 9)
                    consultaGenerico += " and TH.purchase_status = @status AND TD.item_status = 1";
                else
                    consultaGenerico += " and TH.purchase_status = @status ";

                parameters.Add(new SqlParameter("@status", status));
            }
            if (!string.IsNullOrWhiteSpace(department))
            {
                consultaGenerico += " and I.level_header = @depart";
                parameters.Add(new SqlParameter("@depart", Convert.ToInt32(department)));
            }
            if (!string.IsNullOrWhiteSpace(family))
            {
                consultaGenerico += " and I.level1_code = @fam";
                parameters.Add(new SqlParameter("@fam", Convert.ToInt32(family)));
            }
            if (!string.IsNullOrWhiteSpace(partNumber))
            {
                consultaGenerico += " and TD.part_number = @part_number";
                parameters.Add(new SqlParameter("@part_number", partNumber));
            }
            if (!string.IsNullOrWhiteSpace(currency))
            {
                consultaGenerico += " and TH.currency = @currency";
                parameters.Add(new SqlParameter("@currency", currency));
            }
            if (supp != 0)
            {
                consultaGenerico += " AND TH.supplier_id = @supplier";
                parameters.Add(new SqlParameter("@supplier", supp));
            }
            return _context.Database.SqlQuery<PurchaseOrderItemDetailReportGeneral>(consultaGenerico, parameters.ToArray()).ToList();
        }

        public List<PurchaseOrderItemDetailReportGeneral> GetAllGenericPurchasesOrdersDetailXML(DateTime date1, DateTime date2, int supp, string partNumber, string department, string family, string currency)
        {
            var consultaGenerico = @"SELECT MC1.class_name Department, MC2.class_name Family,
                TD.part_number PartNumber, I.part_description DescriptionC, 'ENTRADA: ORDEN DE COMPRA' TransationType,
                ISNULL(TD.gr_quantity, TD.original_quantity) Quantity,
                TD.purchase_price CostUnit,
                CAST(ISNULL(TD.gr_quantity, TD.original_quantity)*TD.purchase_price AS decimal(18,4)) SubTotal,
                M1.name TASA_IVA,
                CAST(TD.iva AS decimal(18,4)) IVA,
                M2.name TASA_IEPS,
                CAST(TD.ieps AS decimal(18,4)) IEPS,
                CAST(TD.item_total_amount AS decimal(18,4)) Amount,
                CASE
	                WHEN TD.item_status = 1 and TH.purchase_status = 1 THEN 'Iniciado'
	                WHEN TD.item_status = 1 and (TH.purchase_status = 1 OR TH.purchase_status = 3 OR TH.purchase_status = 4 OR TH.purchase_status = 5) THEN 'Escaneando'
	                WHEN TD.item_status = 7 THEN 'Producto Cancelado'
	                WHEN TD.item_status = 8 THEN 'Cancelado'
	                WHEN TD.item_status = 1 and TH.purchase_status = 9 THEN 'Terminando'
                END as Item_Status, 
                M.business_name Supplier, TH.purchase_no Folio, CONVERT(VARCHAR,TH.ata,101) MerchandiseEntry, CONVERT(VARCHAR,TH.eta,101) MerchandiseEntryEta, TH.currency Currency
                FROM PURCHASE_ORDER_ITEM TD
                JOIN PURCHASE_ORDER TH ON TH.purchase_no = TD.purchase_no
                JOIN ITEM I ON I.part_number = TD.part_number
                JOIN MA_SUPPLIER M ON M.supplier_id = TH.supplier_id
                JOIN MA_TAX M1 ON M1.tax_code = I.part_iva_sale
                JOIN MA_TAX M2 ON M2.tax_code = I.part_ieps
                JOIN MA_CLASS MC1 ON MC1.class_id = I.level_header
                JOIN MA_CLASS MC2 ON MC2.class_id = I.level1_code
                WHERE CONVERT(date, ISNULL(TH.ata, TH.purchase_date),101) >= @date1 and  CONVERT(date, ISNULL(TH.ata, TH.purchase_date),101)<= @date2 AND (TH.uuid IS NULL OR TH.uuid = '') ";

            //Parametros
            List<SqlParameter> parameters = new List<SqlParameter>();
            parameters.Add(new SqlParameter("@date1", date1.Date));
            parameters.Add(new SqlParameter("@date2", date2.Date));

            consultaGenerico += " and TH.purchase_status = 9 AND TD.item_status = 1";

            if (!string.IsNullOrWhiteSpace(department))
            {
                consultaGenerico += " and I.level_header = @depart";
                parameters.Add(new SqlParameter("@depart", Convert.ToInt32(department)));
            }

            if (!string.IsNullOrWhiteSpace(family))
            {
                consultaGenerico += " and I.level1_code = @fam";
                parameters.Add(new SqlParameter("@fam", Convert.ToInt32(family)));
            }

            if (!string.IsNullOrWhiteSpace(partNumber))
            {
                consultaGenerico += " and TD.part_number = @part_number";
                parameters.Add(new SqlParameter("@part_number", partNumber));
            }

            if (!string.IsNullOrWhiteSpace(currency))
            {
                consultaGenerico += " and TH.currency = @currency";
                parameters.Add(new SqlParameter("@currency", currency));
            }

            if (supp != 0)
            {
                consultaGenerico += " AND TH.supplier_id = @supplier";
                parameters.Add(new SqlParameter("@supplier", supp));
            }

            return _context.Database.SqlQuery<PurchaseOrderItemDetailReportGeneral>(consultaGenerico, parameters.ToArray()).ToList();
        }

        public List<PurchaseOrderModel> GetAllPurchasesOrders(DateTime date1, DateTime date2)
        {
            return _context.PURCHASE_ORDER.Join(_context.SITES, a => a.site_code, b => b.site_code, (a, b) => new { a, b })
            .Join(_context.MA_SUPPLIER, x => x.a.supplier_id, supplier => supplier.supplier_id, (x, supplier) => new { x, supplier })
            .Join(_context.PURCHASE_ORDER_ITEM, y => y.x.a.purchase_no, h => h.purchase_no, (y, h) => new { y, h })
            .Where(o => o.y.x.a.purchase_date >= date1 && o.y.x.a.purchase_date <= date2)
            .OrderBy(y => y.y.x.a.purchase_no).GroupBy(g => new { g.y.x.a.purchase_no, g.y.x.b.site_code, g.y.x.b.site_name, g.y.x.a.supplier_id, g.y.supplier.business_name, g.y.x.a.purchase_date, g.y.x.a.currency, g.y.x.a.purchase_status, g.y.x.a.cdate, g.y.x.a.invoice_no, g.y.x.a.eta, g.y.x.a.ata })
            .Select(p => new PurchaseOrderModel
            {
                PurchaseNo = p.Key.purchase_no,
                SiteCode = p.Key.site_code,
                SiteName = p.Key.site_name,
                supplier_id = p.Key.supplier_id.Value.ToString(),
                supplier_name = p.Key.business_name,
                PurchaseDate = p.Key.purchase_date.Value,
                invoice = p.Key.invoice_no ?? "SIN FOLIO",
                Eta = p.Key.ata.HasValue ? p.Key.ata.Value : p.Key.purchase_date.Value,
                Currency = p.Key.currency,
                Iva = p.Sum(f => f.h.iva.Value),
                Ieps = p.Sum(f => f.h.ieps.Value),
                ItemAmount = p.Sum(f => f.h.item_amount.Value),
                ItemTotalAmount = p.Sum(f => f.h.item_total_amount.Value),
                purchase_status_value = p.Key.purchase_status.Value
            }).ToList();
        }

        public List<PurchaseOrderModel> NewGetAllPurchasesOrders(DateTime date1, DateTime date2)
        {
            return _context.PURCHASE_ORDER
            .Join(_context.SITES, a => a.site_code, b => b.site_code, (po, site) => new { po, site })
            .Join(_context.MA_SUPPLIER, x => x.po.supplier_id, supplier => supplier.supplier_id, (x, supplier) => new { x.po, x.site, supplier })
            .Join(_context.PURCHASE_ORDER_ITEM, y => y.po.purchase_no, h => h.purchase_no, (y, poi) => new { y.po, y.site, y.supplier, poi })
            .Where(o => (o.po.ata != null ? (o.po.ata >= date1 && o.po.ata <= date2) : (o.po.eta >= date1 && o.po.eta <= date2)) && o.poi.item_status == 1)
            .OrderBy(y => y.po.purchase_no)
            .GroupBy(g => new { g.po.purchase_no, g.site.site_code, g.site.site_name, g.po.supplier_id, g.supplier.business_name, g.po.purchase_date, g.po.currency, g.po.purchase_status, g.po.cdate, g.po.invoice_no, g.po.eta, g.po.ata })
            .Select(p => new PurchaseOrderModel
            {
                PurchaseNo = p.Key.purchase_no,
                SiteCode = p.Key.site_code,
                SiteName = p.Key.site_name,
                supplier_id = p.Key.supplier_id.Value.ToString(),
                supplier_name = p.Key.business_name,
                PurchaseDate = p.Key.purchase_date.Value,
                invoice = p.Key.invoice_no ?? "SIN FOLIO",
                Eta = p.Key.ata.HasValue ? p.Key.ata.Value : p.Key.purchase_date.Value,
                Currency = p.Key.currency,
                Iva = p.Sum(f => f.poi.iva.Value),
                Ieps = p.Sum(f => f.poi.ieps.Value),
                ItemAmount = p.Sum(f => f.poi.item_amount.Value),
                ItemTotalAmount = p.Sum(f => f.poi.item_total_amount.Value),
                purchase_status_value = p.Key.purchase_status.Value
            }).ToList();
        }

        public List<PurchaseOrderModel> NewGetAllPurchasesOrdersXML(DateTime date1, DateTime date2)
        {
            var model = _context.PURCHASE_ORDER
            .Join(_context.SITES, a => a.site_code, b => b.site_code, (po, site) => new { po, site })
            .Join(_context.MA_SUPPLIER, x => x.po.supplier_id, supplier => supplier.supplier_id, (x, supplier) => new { x.po, x.site, supplier })
            .Join(_context.PURCHASE_ORDER_ITEM, y => y.po.purchase_no, h => h.purchase_no, (y, poi) => new { y.po, y.site, y.supplier, poi })
            .Where(o => (DbFunctions.TruncateTime(o.po.ata) != null ? (DbFunctions.TruncateTime(o.po.ata) >= DbFunctions.TruncateTime(date1) && DbFunctions.TruncateTime(o.po.ata) <= DbFunctions.TruncateTime(date2)) : (DbFunctions.TruncateTime(o.po.eta) >= DbFunctions.TruncateTime(date1) && DbFunctions.TruncateTime(o.po.eta) <= DbFunctions.TruncateTime(date2))) && o.poi.item_status == 1 && o.po.purchase_status == 9 && (o.po.uuid == null || o.po.uuid == ""))
            .OrderBy(y => y.po.purchase_no)
            .GroupBy(g => new { g.po.purchase_no, g.site.site_code, g.site.site_name, g.po.supplier_id, g.supplier.business_name, g.po.purchase_date, g.po.currency, g.po.purchase_status, g.po.cdate, g.po.invoice_no, g.po.eta, g.po.ata })
            .Select(p => new PurchaseOrderModel
            {
                PurchaseNo = p.Key.purchase_no,
                SiteCode = p.Key.site_code,
                SiteName = p.Key.site_name,
                supplier_id = p.Key.supplier_id.Value.ToString(),
                supplier_name = p.Key.business_name,
                PurchaseDate = p.Key.purchase_date.Value,
                invoice = p.Key.invoice_no ?? "SIN FOLIO",
                Eta = p.Key.ata ?? p.Key.eta.Value,
                Currency = p.Key.currency,
                Iva = p.Sum(f => f.poi.iva.Value),
                Ieps = p.Sum(f => f.poi.ieps.Value),
                ItemAmount = p.Sum(f => f.poi.item_amount.Value),
                ItemTotalAmount = p.Sum(f => f.poi.item_total_amount.Value),
                purchase_status_value = p.Key.purchase_status.Value
            }).ToList();

            return model;
        }

        public List<PurchaseOrderModel> GetAllPurchasesOrdersStatusSupplier(DateTime date1, DateTime date2, int status, int supp)
        {
            return _context.PURCHASE_ORDER.Join(_context.SITES, a => a.site_code, b => b.site_code, (a, b) => new { a, b })
            .Join(_context.MA_SUPPLIER, x => x.a.supplier_id, supplier => supplier.supplier_id, (x, supplier) => new { x, supplier })
            .Join(_context.PURCHASE_ORDER_ITEM, y => y.x.a.purchase_no, h => h.purchase_no, (y, h) => new { y, h })
            .Where(o => o.y.x.a.purchase_date >= date1 && o.y.x.a.purchase_date <= date2 && o.y.x.a.purchase_status == status && o.y.x.a.supplier_id == supp)
            .OrderBy(y => y.y.x.a.purchase_date).GroupBy(g => new { g.y.x.a.purchase_no, g.y.x.b.site_code, g.y.x.b.site_name, g.y.x.a.supplier_id, g.y.supplier.business_name, g.y.x.a.purchase_date, g.y.x.a.currency, g.y.x.a.purchase_status, g.y.x.a.invoice_no })
            .Select(p => new PurchaseOrderModel
            {
                PurchaseNo = p.Key.purchase_no,
                SiteCode = p.Key.site_code,
                SiteName = p.Key.site_name,
                supplier_id = p.Key.supplier_id.Value.ToString(),
                supplier_name = p.Key.business_name,
                PurchaseDate = p.Key.purchase_date.Value,
                invoice = p.Key.invoice_no ?? "SIN FOLIO",
                Currency = p.Key.currency,
                Iva = p.Sum(f => f.h.iva.Value),
                Ieps = p.Sum(f => f.h.ieps.Value),
                ItemAmount = p.Sum(f => f.h.item_amount.Value),
                ItemTotalAmount = p.Sum(f => f.h.item_total_amount.Value),
                purchase_status_value = p.Key.purchase_status.Value
            }).ToList();
        }

        public List<PurchaseOrderModel> GetAllPurchasesOrdersStatus(DateTime date1, DateTime date2, int status)
        {
            return _context.PURCHASE_ORDER.Join(_context.SITES, a => a.site_code, b => b.site_code, (a, b) => new { a, b })
            .Join(_context.MA_SUPPLIER, x => x.a.supplier_id, supplier => supplier.supplier_id, (x, supplier) => new { x, supplier })
            .Join(_context.PURCHASE_ORDER_ITEM, y => y.x.a.purchase_no, h => h.purchase_no, (y, h) => new { y, h })
            .Where(o => o.y.x.a.purchase_date >= date1 && o.y.x.a.purchase_date <= date2 && o.y.x.a.purchase_status == status)
            .OrderBy(y => y.y.x.a.purchase_date).GroupBy(g => new { g.y.x.a.purchase_no, g.y.x.b.site_code, g.y.x.b.site_name, g.y.x.a.supplier_id, g.y.supplier.business_name, g.y.x.a.purchase_date, g.y.x.a.currency, g.y.x.a.purchase_status, g.y.x.a.invoice_no })
            .Select(p => new PurchaseOrderModel
            {
                PurchaseNo = p.Key.purchase_no,
                SiteCode = p.Key.site_code,
                SiteName = p.Key.site_name,
                supplier_id = p.Key.supplier_id.Value.ToString(),
                supplier_name = p.Key.business_name,
                PurchaseDate = p.Key.purchase_date.Value,
                invoice = p.Key.invoice_no ?? "SIN FOLIO",
                Currency = p.Key.currency,
                Iva = p.Sum(f => f.h.iva.Value),
                Ieps = p.Sum(f => f.h.ieps.Value),
                ItemAmount = p.Sum(f => f.h.item_amount.Value),
                ItemTotalAmount = p.Sum(f => f.h.item_total_amount.Value),
                purchase_status_value = p.Key.purchase_status.Value
            }).ToList();
        }

        public List<PurchaseOrderModel> GetAllPurchasesOrdersSupplier(DateTime date1, DateTime date2, int supp)
        {
            return _context.PURCHASE_ORDER.Join(_context.SITES, a => a.site_code, b => b.site_code, (a, b) => new { a, b })
            .Join(_context.MA_SUPPLIER, x => x.a.supplier_id, supplier => supplier.supplier_id, (x, supplier) => new { x, supplier })
            .Join(_context.PURCHASE_ORDER_ITEM, y => y.x.a.purchase_no, h => h.purchase_no, (y, h) => new { y, h })
            .Where(o => o.y.x.a.purchase_date >= date1 && o.y.x.a.purchase_date <= date2 && o.y.x.a.supplier_id == supp)
            .OrderBy(y => y.y.x.a.purchase_date).GroupBy(g => new { g.y.x.a.purchase_no, g.y.x.b.site_code, g.y.x.b.site_name, g.y.x.a.supplier_id, g.y.supplier.business_name, g.y.x.a.purchase_date, g.y.x.a.currency, g.y.x.a.purchase_status, g.y.x.a.invoice_no })
            .Select(p => new PurchaseOrderModel
            {
                PurchaseNo = p.Key.purchase_no,
                SiteCode = p.Key.site_code,
                SiteName = p.Key.site_name,
                supplier_id = p.Key.supplier_id.Value.ToString(),
                supplier_name = p.Key.business_name,
                PurchaseDate = p.Key.purchase_date.Value,
                invoice = p.Key.invoice_no ?? "SIN FOLIO",
                Currency = p.Key.currency,
                Iva = p.Sum(f => f.h.iva.Value),
                Ieps = p.Sum(f => f.h.ieps.Value),
                ItemAmount = p.Sum(f => f.h.item_amount.Value),
                ItemTotalAmount = p.Sum(f => f.h.item_total_amount.Value),
                purchase_status_value = p.Key.purchase_status.Value
            }).ToList();
        }

        public List<PurchaseOrderModel> GetFinishedPurchasesOrders(DateTime BeginDate, DateTime EndDate)
        {
            var test = _context.PURCHASE_ORDER.Join(_context.SITES, a => a.site_code, b => b.site_code, (a, b) => new { a, b })
            .Join(_context.MA_SUPPLIER, x => x.a.supplier_id, supplier => supplier.supplier_id, (x, supplier) => new { x, supplier })
            .Join(_context.PURCHASE_ORDER_ITEM, y => y.x.a.purchase_no, h => h.purchase_no, (y, h) => new { y, h })
            .Where(o => o.y.x.a.ata >= BeginDate && o.y.x.a.ata <= EndDate && o.y.x.a.purchase_status == 9)
            .OrderBy(y => y.y.x.a.purchase_date).GroupBy(g => new { g.y.x.a.purchase_no, g.y.x.b.site_code, g.y.x.b.site_name, g.y.x.a.supplier_id, g.y.supplier.business_name, g.y.x.a.purchase_date, g.y.x.a.currency, g.y.x.a.purchase_status, g.y.x.a.invoice_no })
            .Select(p => new PurchaseOrderModel
            {
                PurchaseNo = p.Key.purchase_no,
                SiteCode = p.Key.site_code,
                SiteName = p.Key.site_name,
                supplier_id = p.Key.supplier_id.Value.ToString(),
                supplier_name = p.Key.business_name,
                PurchaseDate = p.Key.purchase_date.Value,
                invoice = p.Key.invoice_no ?? "SIN FOLIO",
                Currency = p.Key.currency,
                Iva = p.Sum(f => f.h.iva.Value),
                Ieps = p.Sum(f => f.h.ieps.Value),
                ItemAmount = p.Sum(f => f.h.item_amount.Value),
                ItemTotalAmount = p.Sum(f => f.h.item_total_amount.Value),
                purchase_status_value = p.Key.purchase_status.Value
            }).ToList();

            return test;
        }

        public List<PurchaseOrderModel> GetAllFinishedPurchasesOrders(DateTime firstDate, DateTime lastDate)
        {
            var model = _context.PURCHASE_ORDER.Join(_context.SITES, a => a.site_code, b => b.site_code, (a, b) => new { a, b })
            .Join(_context.MA_SUPPLIER, x => x.a.supplier_id, supplier => supplier.supplier_id, (x, supplier) => new { x, supplier })
            .Join(_context.PURCHASE_ORDER_ITEM, y => y.x.a.purchase_no, h => h.purchase_no, (y, h) => new { y, h })
            .Where(o => o.y.x.a.purchase_status == 9 && DbFunctions.TruncateTime(o.y.x.a.ata) >= DbFunctions.TruncateTime(firstDate) && DbFunctions.TruncateTime(o.y.x.a.ata) <= DbFunctions.TruncateTime(lastDate))
            .OrderBy(y => y.y.x.a.purchase_date).GroupBy(g => new { g.y.x.a.purchase_no, g.y.x.b.site_code, g.y.x.b.site_name, g.y.x.a.supplier_id, g.y.supplier.business_name, g.y.x.a.purchase_date, g.y.x.a.currency, g.y.x.a.purchase_status, g.y.x.a.invoice_no })
            .Select(p => new PurchaseOrderModel
            {
                PurchaseNo = p.Key.purchase_no,
                SiteCode = p.Key.site_code,
                SiteName = p.Key.site_name,
                supplier_id = p.Key.supplier_id.ToString() ?? "N/A",
                supplier_name = p.Key.business_name,
                PurchaseDate = p.Key.purchase_date ?? DateTime.Now,
                Currency = p.Key.currency,
                invoice = p.Key.invoice_no ?? "SIN FOLIO",
                Iva = p.Sum(f => f.h.iva ?? 0),
                Ieps = p.Sum(f => f.h.ieps ?? 0),
                ItemAmount = p.Sum(f => f.h.item_amount ?? 0),
                ItemTotalAmount = p.Sum(f => f.h.item_total_amount ?? 0),
                purchase_status_value = p.Key.purchase_status ?? 0
            }).ToList();

            return model;
        }

        public bool CancelFinishedPurchaseOrder(string PurchaseOrder, string Site, string User, string ProgramId, out string Document, out int Response)
        {
            try
            {
                var DataReturn = _context.Database.SqlQuery<StoreProcedureResult>("DECLARE @return_value int, @Document nvarchar(10) EXEC @return_value = [dbo].[sproc_GR_Reverse_From_PO] @po_no = '" + PurchaseOrder + "', @site_code = '" + Site + "', @cuser = '" + User + "', @program_id = '" + ProgramId + "', @Document = @Document OUTPUT SELECT 'Document'= @Document, 'ReturnValue' = @return_value").ToList();
                if (DataReturn[0].ReturnValue == 0 & DataReturn[0].Document != "")
                {
                    Document = DataReturn[0].Document;
                    Response = 0;
                    return true;
                }
                else
                {
                    Document = "";
                    Response = DataReturn[0].ReturnValue;
                    return false;
                }
            }
            catch (Exception ex)
            {
                string msg = ex.Message;
                Document = "";
                Response = 1;
                return false;
            }
        }

        public bool UpdateCommentInCancelPO(string purchase_order, string comment, string user)
        {
            try
            {
                var po = _context.PURCHASE_ORDER.Where(w => w.purchase_no == purchase_order).SingleOrDefault();
                if (po == null)
                    return false;
                po.purchase_remark = po.purchase_remark + ", " + comment;
                po.udate = DateTime.Now;
                po.uuser = user;
                _context.SaveChanges();
                return true;
            }
            catch (Exception ex)
            {
                var msg = ex.Message;
                return false;
            }
        }

        public List<PurchaseOrderModel> GetCanceledPurchasesOrders(DateTime begindate, DateTime enddate)
        {
            return _context.PURCHASE_ORDER.Join(_context.SITES, a => a.site_code, b => b.site_code, (a, b) => new { a, b })
            .Join(_context.MA_SUPPLIER, x => x.a.supplier_id, supplier => supplier.supplier_id, (x, supplier) => new { x, supplier })
            .Join(_context.PURCHASE_ORDER_ITEM, y => y.x.a.purchase_no, h => h.purchase_no, (y, h) => new { y, h })
            .Where(o => o.y.x.a.purchase_date >= begindate && o.y.x.a.purchase_date <= enddate && o.y.x.a.purchase_status == 8)
            .OrderBy(y => y.y.x.a.purchase_date).GroupBy(g => new { g.y.x.a.purchase_no, g.y.x.b.site_code, g.y.x.b.site_name, g.y.x.a.supplier_id, g.y.supplier.business_name, g.y.x.a.purchase_date, g.y.x.a.currency, g.y.x.a.purchase_status })
            .Select(p => new PurchaseOrderModel
            {
                PurchaseNo = p.Key.purchase_no,
                SiteCode = p.Key.site_code,
                SiteName = p.Key.site_name,
                supplier_id = p.Key.supplier_id.Value.ToString(),
                supplier_name = p.Key.business_name,
                PurchaseDate = p.Key.purchase_date.Value,
                Currency = p.Key.currency,
                Iva = p.Sum(f => f.h.iva.Value),
                Ieps = p.Sum(f => f.h.ieps.Value),
                ItemAmount = p.Sum(f => f.h.item_amount.Value),
                ItemTotalAmount = p.Sum(f => f.h.item_total_amount.Value),
                purchase_status_value = p.Key.purchase_status.Value
            }).ToList();
        }

        public PURCHASE_ORDER GetPurchaseOrderByPo(string purchase_no)
        {
            return _context.PURCHASE_ORDER.Where(x => x.purchase_no == purchase_no)
                .Select(d => d).SingleOrDefault();
        }

        public List<PurchaseOrderModel> GetAllPurchasesOrdersBySupplier(DateTime date1, DateTime date2, int supplierId)
        {
            return _context.PURCHASE_ORDER
            .Where(o => o.purchase_date >= date1 && o.purchase_date <= date2 && o.supplier_id == supplierId && o.purchase_status == 9 && o.folio != null)
            .Select(p => new PurchaseOrderModel
            {
                PurchaseNo = p.purchase_no,
                PurchaseDate = p.purchase_date.Value,
                Folio = p.folio.Value,
                assortedQuantity = 0,
                assortedAmount = 0,
                requestedQuantity = 0,
                requestedAmount = 0
            }).ToList();
        }

        public List<PurchaseOrderModel> GetAllOrders()
        {
            return _context.PURCHASE_ORDER.Where(x => x.purchase_status == 3 || x.purchase_status == 4)
                .Join(_context.MA_SUPPLIER, x => x.supplier_id, supplier => supplier.supplier_id, (x, supplier) => new { x, supplier })
                .Select(x => new PurchaseOrderModel
                {
                    PurchaseNo = x.x.purchase_no,
                    supplier_name = x.supplier.business_name
                }).ToList();
        }

        public List<PurchaseOrderModel> GetAllOrdersWithoutXML()
        {
            DateTime date = DateTime.Now;
            var firstDayOfMonth = new DateTime(date.Year, date.Month, 1);
            var lastDayOfMonth = firstDayOfMonth.AddMonths(1).AddDays(-1);

            var model = _context.PURCHASE_ORDER
                .Where(x => x.purchase_status == 9 && (x.uuid == null || x.uuid == "") && (DbFunctions.TruncateTime(x.purchase_date) >= DbFunctions.TruncateTime(firstDayOfMonth) && DbFunctions.TruncateTime(x.purchase_date) <= DbFunctions.TruncateTime(lastDayOfMonth)))
                .Join(_context.MA_SUPPLIER, x => x.supplier_id, supplier => supplier.supplier_id, (x, supplier) => new { x, supplier })
                .Select(x => new PurchaseOrderModel
                {
                    PurchaseNo = x.x.purchase_no,
                    supplier_name = x.supplier.business_name
                }).ToList();

            return model;
        }

        public List<PurchaseOrderModel> GetAllOrdersEdit()
        {
            return _context.PURCHASE_ORDER.Where(x => x.purchase_status != 9 && x.purchase_status != 8)
                .Join(_context.MA_SUPPLIER, x => x.supplier_id, supplier => supplier.supplier_id, (x, supplier) => new { x, supplier })
                .Select(x => new PurchaseOrderModel
                {
                    PurchaseNo = x.x.purchase_no,
                    supplier_name = x.supplier.business_name
                }).ToList();
        }

        public bool UpdatePurchaseOrderInvoice(string PurchaseOrder, string InvoiceNo, string Comments, string User)
        {
            try
            {
                var purchase = _context.PURCHASE_ORDER.Where(x => x.purchase_no == PurchaseOrder).FirstOrDefault();
                purchase.invoice_no = InvoiceNo;
                purchase.purchase_remark += " --" + Comments;
                purchase.uuser = User;
                purchase.udate = DateTime.Now;
                _context.SaveChanges();
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public int GetSupplierIdByPurchase(string purchase_order)
        {
            var supplier = _context.PURCHASE_ORDER.Where(w => w.purchase_no == purchase_order).FirstOrDefault();
            if (supplier != null)
                return supplier.supplier_id ?? 0;
            return 0;
        }
        public string GetCurrencyPO(string folio)
        {
            return _context.Database.SqlQuery<string>(@"select top 1 p.currency from ORDER_LIST o 
            join ORDER_HEAD oh on oh.folio = o.folio
            join ITEM_SUPPLIER i on i.supplier_id = oh.supplier_id and i.part_number = o.part_number
            join MA_SUPPLIER m on m.supplier_id = i.supplier_id
            join PURCHASE_BASE_COST p on p.supplier_part_number = i.supplier_part_number
            where p.status = 1 and o.folio = '" + folio + "'").FirstOrDefault();
        }
    }
}