﻿namespace App.DAL
{
    public class StoreProcedureResult
    {
        public string Document { get; set; }
        public int ReturnValue { get; set; }
        public string result { get; set; }
    }
}