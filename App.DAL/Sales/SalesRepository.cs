﻿using App.Entities;
using App.Entities.ViewModels.Sales;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.SqlClient;
using System.Globalization;
using System.Linq;
using App.DAL.CreditNotes;
using App.DAL.Invoice;
using App.Entities.ViewModels.CreditNotes;
using App.Entities.ViewModels.Invoices;
using static App.Entities.ViewModels.Common.Constants;
using System.Data;
using App.Entities.ViewModels.User;
using App.DAL.Currency;

namespace App.DAL.Sales
{
    public class SalesRepository
    {
        private DFL_SAIEntities _context;
        private InvoiceRepository _InvoiceRepository;
        private UserMasterRepository _UserMasterRepository;
        private CurrencyRepository _CurrencyRepository;

        public SalesRepository()
        {
            _context = new DFL_SAIEntities();
            _InvoiceRepository = new InvoiceRepository();
            _UserMasterRepository = new UserMasterRepository();
            _CurrencyRepository = new CurrencyRepository();
        }

        public DFLPOS_SALES GetSaleByNumber(long SaleId)
        {
            var Sale = _context.DFLPOS_SALES.Find(SaleId);            
            return Sale;
        }

        public SalesModel GetGlobalInvoiceTotal(string SaleDate)
        {
            DateTime DateSale = DateTime.ParseExact(SaleDate, DateFormat.INT_DATE, CultureInfo.InvariantCulture);
            var Sales = (from s in _context.DFLPOS_SALES
                         where s.sale_date == DateSale
                               && !s.invoice_no.HasValue
                               && string.IsNullOrEmpty(s.invoice_serie)
                         group s by s.sale_date into gp
                         select new SalesModel
                         {
                             sale_iva = gp.Sum(d => d.sale_tax),
                             sale_total = gp.Sum(d => d.sale_total)
                         }).First();

            var SalesPM = (from spm in _context.DFLPOS_SALES_PAYMENT_METHOD
                           join s in _context.DFLPOS_SALES on spm.sale_id equals s.sale_id
                           where spm.pm_id == PaymentMethods.GIFT_CARD
                                 && s.sale_date == DateSale
                                 && s.sale_total > 0
                                 && !s.invoice_no.HasValue
                           select new
                           {
                               spm.amount,
                               s.sale_tax
                           }).ToList();

            var SalesPMCredit = (from spm in _context.DFLPOS_SALES_PAYMENT_METHOD
                                 join s in _context.DFLPOS_SALES on spm.sale_id equals s.sale_id
                                 where spm.pm_id == PaymentMethods.CREDIT
                                       && s.sale_date == DateSale
                                       && s.sale_total > 0
                                       && !s.invoice_no.HasValue                                 
                                 select new
                                 {
                                     spm.amount,
                                     s.sale_tax
                                 }).ToList();

            if (SalesPM != null && SalesPM.Count > 0)
            {
                Sales.sale_total -= SalesPM.Sum(s => s.amount);
                Sales.sale_iva -= SalesPM.Sum(s => s.sale_tax);
            }

            if (SalesPMCredit != null && SalesPMCredit.Count > 0)
            {
                Sales.sale_total -= SalesPMCredit.Sum(s => s.amount);
                Sales.sale_iva -= SalesPMCredit.Sum(s => s.sale_tax);
            }

            return Sales;
        }

        public List<SalesModel> GetUnInvoicedSalesByDate(string SaleDate)
        {
            DateTime DateSale = DateTime.ParseExact(SaleDate, DateFormat.INT_DATE, CultureInfo.InvariantCulture);
            var Sales = (from s in _context.DFLPOS_SALES
                         where s.sale_date == DateSale
                               && !s.invoice_no.HasValue
                               && string.IsNullOrEmpty(s.invoice_serie)
                         select new SalesModel
                         {
                             sale_iva = s.sale_tax,
                             sale_id = s.sale_id,
                             sale_total = s.sale_total
                         }).ToList();

            return Sales;
        }

        public bool IsSaleInvoiced(long SaleNumber)
        {
            var Sale = _context.DFLPOS_SALES.Where(c => c.sale_id == SaleNumber).SingleOrDefault();
            if (Sale != null && (Sale.invoice_no.HasValue || !string.IsNullOrEmpty(Sale.invoice_serie)))
                return true;

            return false;
        }

        public bool IsSaleFromToday(long SaleNumber)
        {
            var Sale = _context.DFLPOS_SALES.Where(c => c.sale_id == SaleNumber).SingleOrDefault();
            if (Sale != null && Sale.sale_date == DateTime.Now.Date)
                return true;

            return false;
        }

        public bool ExistingSale(long SaleNumber)
        {
            var exist = _context.DFLPOS_SALES.Find(SaleNumber);
            if (exist != null)
                return true;

            return false;
        }

        public bool SalePMForbidden(long SaleNumber)
        {
            var PMForbidden = (from s in _context.DFLPOS_SALES
                               join spm in _context.DFLPOS_SALES_PAYMENT_METHOD on s.sale_id equals spm.sale_id
                               where (spm.pm_id == PaymentMethods.COUPON_FLORIWOOD
                                      || spm.pm_id == PaymentMethods.EMPLOYEE_DISCOUNT
                                      || spm.pm_id == PaymentMethods.GIFT_CARD)
                                    && s.sale_id == SaleNumber
                               select s).FirstOrDefault();

            if (PMForbidden != null)
                return true;

            return false;
        }

        public bool IsSaleCancelled (long SaleNumber)
        {
            bool IsCancelled = true;
            var Sale = _context.DFLPOS_SALES.Find(SaleNumber);
            if (Sale != null && Sale.sale_total == 0)
                return IsCancelled;
            else
                return !IsCancelled;
        }

        public bool UpdateSale(DFLPOS_SALES Sale)
        {
            try
            {
                _context.Entry(Sale).State = EntityState.Modified;
                _context.SaveChanges();
                return true;
            }
            catch (Exception ex)
            {
                string msg = ex.Message;
                return false;
            }
        }
        //Sales Reports
        #region Reports
        public List<SalesModel> GetSalesBySaleId(long saId)
        {
            var ItemList = ((from sls in _context.DFLPOS_SALES
                             join ctms in _context.CUSTOMERS on sls.customer_code equals ctms.customer_code
                             where sls.sale_id == saId
                             select new
                             {
                                 sls.sale_id,
                                 sls.sale_date,
                                 sale_time = sls.sale_time.ToString(),
                                 sls.sale_total,
                                 sale_iva = sls.sale_tax,
                                 sale_importe = sls.sale_total - sls.sale_tax,
                                 sale_factura = sls.invoice_no,
                                 sale_ieps = 0,
                                 customer = ctms.customer_name
                             }).AsEnumerable()
                              .Select(x => new SalesModel
                              {
                                  sale_id = x.sale_id,
                                  sale_date = x.sale_date.ToString(DateFormat.INT_DATE),
                                  sale_time = x.sale_time,
                                  sale_total = x.sale_total,// - GetIepsFromSalesDetailItemsBySaleId(x.sale_id),
                                  sale_iva = x.sale_iva,
                                  sale_importe = x.sale_importe,
                                  sale_factura = GetCfdiNoBySaleId(x.sale_factura == null ? 0 : (long)x.sale_factura),
                                  sale_ieps = 0,// GetIepsFromSalesDetailItemsBySaleId(x.sale_id).ToString()
                                  customer = x.customer
                              })).ToList();
            if (ItemList != null)
                return ItemList;
            else
                return new List<SalesModel>();


        }
        
        public List<SalesModel> GetSalesReport(SalesIndex Model)
        {
            var result = new List<SalesModel>();

            DateTime Date = DateTime.ParseExact(Model.StartDate, DateFormat.INT_DATE, CultureInfo.InvariantCulture);
            DateTime Date2 = DateTime.ParseExact(Model.EndDate, DateFormat.INT_DATE, CultureInfo.InvariantCulture);
            var _date = new SqlParameter("@date", Date);
            var _date2 = new SqlParameter("@date2", Date2);
            var _tipo = new SqlParameter("@tipo", Model.Type);
            var _invoiced = new SqlParameter("@invoiced", Model.Status);
            var _pos = new SqlParameter("@pos", Model.caja);
            var _cashier = new SqlParameter("@cashier", Model.Cashier);

            var sproc = "sp_DFLPOS_SalesReport @date, @date2, @tipo, @invoiced, @pos, @cashier";

            result = _context.Database
              .SqlQuery<SalesModel>(sproc, _date, _date2, _tipo, _invoiced, _pos, _cashier)
              .ToList();

            return result;

        }

        public List<CustomerSalesViewModel> SalesClientsByHourReport(SalesIndex Model) //recibe el modelo index y lo filtra en base a los valores contenidos en todos los campos
        {
            var predicateSales = PredicateBuilder.True<DFLPOS_SALES>();
            var StartDate = new DateTime();
            var EndDate = new DateTime();
            var reportList = new List<CustomerSalesViewModel>();

            StartDate = DateTime.ParseExact(Model.StartDate, DateFormat.INT_DATE, CultureInfo.InvariantCulture);
            EndDate = DateTime.ParseExact(Model.EndDate, DateFormat.INT_DATE, CultureInfo.InvariantCulture);

            predicateSales = predicateSales.And(c => c.sale_date >= StartDate && c.sale_date <= EndDate);
           
            if (Model.Status != SalesStatus.All)
            {
                if (Model.Status == SalesStatus.SinFact)

                    predicateSales = predicateSales.And(s => !s.invoice_no.HasValue && string.IsNullOrEmpty(s.invoice_serie));
                else
                    predicateSales = predicateSales.And(s => s.invoice_no.HasValue && !string.IsNullOrEmpty(s.invoice_serie));
            }

            if (Model.Type != SalesType.All)
            {
                if (Model.Type == SalesType.Credit)

                    predicateSales = predicateSales.And(s => (s.DFLPOS_SALES_PAYMENT_METHOD.Where(pm => pm.pm_id == PaymentMethods.CREDIT).Select(sel => sel.sale_id)).
                                                                ToList().Contains(s.sale_id));
                else
                    predicateSales = predicateSales.And(s => (s.DFLPOS_SALES_PAYMENT_METHOD.Where(pm => pm.pm_id != PaymentMethods.CREDIT).Select(sel => sel.sale_id)).
                                                                ToList().Contains(s.sale_id));
            }

            var Sales = _context.DFLPOS_SALES.Where(predicateSales).ToList();            

            if (Sales.Count > 0)
            {                
                var ListIeps = GetIepsFromSalesDetailItemsBySaleId(StartDate, EndDate);
                var ItemList = new List<SalesModel>();

                var SaleDetails = (from sld in _context.DFLPOS_SALES_DETAIL
                                   where DbFunctions.TruncateTime(sld.trans_date) >= StartDate && DbFunctions.TruncateTime(sld.trans_date) <= EndDate
                                   group sld by sld.sale_id into g
                                   select new
                                   {
                                       sale_id = g.Key,
                                       TotalItems = g.Sum(c => c.quantity)
                                   }).ToList();

                ItemList = (from sls in Sales
                            join li in ListIeps on sls.sale_id equals li.sale_id into LeftJoinIeps
                             from l in LeftJoinIeps.DefaultIfEmpty()                                 
                             select new
                             {
                                 sls.sale_id,
                                 sls.sale_date,
                                 sale_time = sls.sale_time.ToString(),
                                 sls.sale_total,
                                 sale_iva = sls.sale_tax,
                                 sale_importe = sls.sale_total - sls.sale_tax,
                                 sale_ieps = l != null ? l.price : 0
                             }).AsEnumerable()
                                  .Select(x => new SalesModel
                                  {
                                      sale_id = x.sale_id,
                                      sale_date = x.sale_date.ToString(DateFormat.INT_DATE),
                                      sale_time = x.sale_time,
                                      sale_total = x.sale_total,
                                      sale_iva = x.sale_iva,
                                      sale_ieps = x.sale_ieps,
                                      sale_importe = x.sale_importe - x.sale_ieps,
                                  }).ToList();

                reportList = (from sls in ItemList
                              join sd in SaleDetails on sls.sale_id equals sd.sale_id
                              select new
                              {
                                  sls.sale_id,
                                  sls.sale_date,
                                  sale_time = DateTime.ParseExact(sls.sale_time, "hh:mm tt", null).Hour,
                                  sls.sale_total,
                                  sls.sale_iva,
                                  sls.sale_ieps,
                                  sls.sale_importe,
                                  Articulos = sd.TotalItems
                              }).GroupBy(g => g.sale_time)
                .Select(x => new CustomerSalesViewModel
                {
                    Clientes = x.Count(),
                    Hora = x.Key.ToString(),
                    Ventas = Math.Round(x.Sum(s => s.sale_total), 2),
                    Iva = Math.Round(x.Sum(s => s.sale_iva), 2),
                    Ieps = Math.Round(x.Sum(s => s.sale_ieps), 2),
                    Subtotal = Math.Round(x.Sum(s => s.sale_importe), 2),
                    PromedioPorCliente = Math.Round(x.Average(a => a.sale_total), 2),
                    ArticulosPromedio = Math.Round(x.Average(a => a.Articulos), 2),
                    Articulos = Math.Round(x.Sum(s => s.Articulos), 2)
                }).ToList();                
            }

            return reportList;
        }        

        public List<DollarSalesModel> GetDollarSalesByDates(string StartDate, string EndDate)
        {
            var DateStart = DateTime.ParseExact(StartDate, DateFormat.INT_DATE, CultureInfo.InvariantCulture);
            var DateEnd = DateTime.ParseExact(EndDate, DateFormat.INT_DATE, CultureInfo.InvariantCulture);
            var ItemList = ((from sls in _context.DFLPOS_SALES
                             join sdpm in _context.DFLPOS_SALES_PAYMENT_METHOD on sls.sale_id equals sdpm.sale_id
                             where sls.sale_date >= DateStart && sls.sale_date <= DateEnd && sdpm.pm_id == PaymentMethods.USD_CASH
                             select new
                             {
                                 sls.sale_id,
                                 sls.sale_date,
                                 sale_time = sls.sale_time.ToString(),
                                 recibido = sdpm.amount_entered.Value,
                                 cobrado = sdpm.amount,
                                 tipoCambio = sls.DFLPOS_SALES_DETAIL.Select(s => s.currency_rate).FirstOrDefault(),
                                 usuario = sls.USER_MASTER.first_name + " " + sls.USER_MASTER.last_name,
                             }).AsEnumerable()
                              .Select(x => new DollarSalesModel
                              {
                                  sale_id = x.sale_id,
                                  sale_date = x.sale_date.ToString(DateFormat.INT_DATE),
                                  sale_time = x.sale_time,
                                  recibido = x.recibido,
                                  cobrado = x.cobrado,
                                  tipoCambio = x.tipoCambio,
                                  usuario = x.usuario,
                              })).ToList();
            if (ItemList != null)
                return ItemList;
            else
                return new List<DollarSalesModel>();
        }

        private List<IepsModel> GetIepsFromSalesDetailItemsBySaleId(DateTime startDate, DateTime endDate)
        {
            var iepsr = (from sld in _context.DFLPOS_SALES_DETAIL
                         join it in _context.ITEM on sld.part_number equals it.part_number
                         join mt in _context.MA_TAX on it.part_ieps equals mt.tax_code                         
                         where DbFunctions.TruncateTime(sld.trans_date) >= startDate && DbFunctions.TruncateTime(sld.trans_date) <= endDate
                         && (!string.IsNullOrEmpty(it.part_ieps) && it.part_ieps != TaxCodes.NOT_AVAILABLE)
                         group new { sld.part_price, mt.tax_value } by new { sld.sale_id } into g
                         select new IepsModel
                         {
                             price = g.Sum(c => (c.tax_value.Value / 100) * c.part_price),
                             sale_id = g.Key.sale_id
                         }).ToList();

            return iepsr;
        }
        
        public List<string> GetCajas()
        {
            var CajasList = new List<string>();
            CajasList = _context.DFLPOS_SALES.Select(s => s.pos_id).Distinct().ToList();
            return CajasList;
        }

        private string GetCfdiNoBySaleId(long saleId)
        {
            if (saleId > 0)
            {
                var _invoive = _context.INVOICES.Where(inv => inv.invoice_no == saleId).Select(s => s.cfdi_id).FirstOrDefault();

                if (_invoive != null)
                    return _invoive.ToString();
                else
                    return "S/F";
            }
            else
                return "S/F";
        }

        public List<SalesDetailReportModel> GetSailDetailsBySaleId(long saleId)
        {
            var _salesdETAIL = (from sd in _context.DFLPOS_SALES_DETAIL
                                where sd.sale_id == saleId && sd.quantity>0
                                select new
                                {
                                    sd.part_number,
                                    part_name = sd.ITEM.part_description,
                                    sd.quantity,
                                    sd.detail_captured,
                                    sd.part_tax,
                                    tax_value = sd.ITEM.MA_TAX_IVA_SALE.tax_value.Value,
                                    ieps_value = sd.ITEM.MA_TAX_IEPS.tax_value.Value,
                                    sd.part_price,
                                    PriceNoIeps = sd.part_price - (sd.part_price / (1 + sd.ITEM.MA_TAX_IEPS.tax_value.Value / 100))
                                }).AsEnumerable().Select(c => new SalesDetailReportModel
                                {
                                    part_number = c.part_number,
                                    part_name = c.part_name,
                                    part_quantity = c.quantity,
                                    part_price = c.detail_captured,
                                    part_importe = Math.Round(c.part_price - c.PriceNoIeps, 2),
                                    part_iva = c.part_tax,
                                    part_iva_percentage = c.tax_value,
                                    part_ieps = c.ieps_value != 0 ? Math.Round((c.ieps_value / 100) * (c.part_price / (1 + c.ieps_value / 100)), 2) : 0,
                                    part_ieps_percentage = c.ieps_value,
                                    part_total = (c.detail_captured + Math.Round(c.part_tax / c.quantity, 2)) * c.quantity
                                }).ToList();

            if (_salesdETAIL != null)
                return _salesdETAIL;
            else
                return new List<SalesDetailReportModel>();
        }
        public List<SalesModelConc> GetSalesConcretated(SalesConcIndex Model) //recibe el modelo index y lo filtra en base a los valores contenidos en todos los campos
        {

            var predicateCreditNotes = PredicateBuilder.True<CREDIT_NOTES>();
            var predicateInvoices = PredicateBuilder.True<INVOICES>();
            var predicateSales = PredicateBuilder.True<DFLPOS_SALES>();
            var DateStart = new DateTime();
            var DateEnd = new DateTime();
            if (!string.IsNullOrEmpty(Model.StartDate) && !string.IsNullOrEmpty(Model.EndDate))
            {
                DateStart = DateTime.ParseExact(Model.StartDate, DateFormat.INT_DATE, CultureInfo.InvariantCulture);
                DateEnd = DateTime.ParseExact(Model.EndDate, DateFormat.INT_DATE, CultureInfo.InvariantCulture);
                //DateEnd = DateEnd.AddDays(1);
                predicateCreditNotes = predicateCreditNotes.And(c => DbFunctions.TruncateTime(c.cn_date) >= DateStart && DbFunctions.TruncateTime(c.cn_date) <= DateEnd);
                predicateInvoices = predicateInvoices.And(c => c.invoice_date >= DateStart && c.invoice_date <= DateEnd);
                predicateSales = predicateSales.And(c => c.sale_date >= DateStart && c.sale_date <= DateEnd);
            }
            // * X MONEDA
           if (Model.Currency != CurrencysCoonc.ALL)
            {
                var moneda = "";
                if (Model.Currency == CurrencysCoonc.MXN)
                    moneda = Currencies.MXN;
                else
                    moneda = Currencies.USD;
                predicateCreditNotes = predicateCreditNotes.And(c => c.cur_code.Equals(moneda));
                predicateInvoices = predicateInvoices.And(c => c.cur_code.Equals(moneda));
                predicateSales = predicateSales.And(c => (c.DFLPOS_SALES_DETAIL.Select(s => s.sale_id)).ToList().Contains(c.sale_id));
           }
            var listSalesConcentrated = new List<SalesModelConc>();
            var _creditNotesRepository = new CreditNotesRepository();
            var _invoiceRepository = new InvoiceRepository();

            List<INVOICES> listInvoices = _invoiceRepository.GetAllInvoiceHeaders(predicateInvoices);
            List<CreditNotesBon> listCreditNotesBoni = _creditNotesRepository.GetCreditNotesBoniList(predicateCreditNotes);
            var listSales = _context.DFLPOS_SALES.Where(predicateSales).ToList();
            var listInvoicesConIeps = new List<InvoiceTax>();
            var date1Inv = new DateTime();
            var date2Inv = new DateTime();
            var listIeps = new List<IepsModel>();
            if (listInvoices.Count > 0)
            {
                date1Inv = listInvoices.Min(dt => dt.invoice_date);
                date2Inv = listInvoices.Max(dt => dt.invoice_date);
                listIeps = GetIepsFromInvoices(date1Inv, date2Inv);

                foreach (INVOICES inv in listInvoices)
                {
                    var CurrencyRate = _CurrencyRepository.GetCurrencyRateByDate(inv.invoice_date.ToString(DateFormat.INT_DATE));
                    var _ieps = listIeps.Where(s => s.sale_id == inv.invoice_no && s.serie == inv.invoice_serie).Select(c => c.value).Sum();
                    var invo = new InvoiceTax();
                    invo.InvoiceNumber = inv.invoice_no;
                    invo.InvoiceSerie = inv.invoice_serie;
                    //invo.IVA = inv.invoice_tax;
                    //invo.IEPS = listIeps.Where(s => s.sale_id == inv.invoice_no && s.serie == inv.invoice_serie).Select(c => c.value).Sum();
                    invo.IVA = Model.Currency == CurrencysCoonc.ALL && inv.cur_code== Currencies.USD ? inv.invoice_tax * CurrencyRate : inv.invoice_tax;
                    invo.IEPS = Model.Currency == CurrencysCoonc.ALL && inv.cur_code == Currencies.USD ? _ieps * CurrencyRate : _ieps ;
                    invo.Status = inv.invoice_status;
                    invo.InvoiceDate = inv.invoice_date;
                    //invo.Total = inv.invoice_total;
                    invo.Total = Model.Currency == CurrencysCoonc.ALL && inv.cur_code == Currencies.USD ? inv.invoice_total * CurrencyRate : inv.invoice_total ;
                    listInvoicesConIeps.Add(invo);
                }
            }

            var query = (from p in listSales
                         group p by p.sale_date into g
                         select new DFLPOS_SALES
                         {
                             sale_date = g.Key

                         }).ToList();

            /* para buscar por todas las fechas incluidas en el rango de busqueda
            List<DateTime> allDates = new List<DateTime>();
            for (DateTime i = DateStart; i <= DateEnd; i = i.AddDays(1))
            {

                allDates.Add(i); 
                Console.WriteLine(allDates.ToString());
            }*/

            foreach (DFLPOS_SALES sls in query)
            {

                var smc = new SalesModelConc();
                //facturas
                var fecha = sls.sale_date.ToString(DateFormat.INT_DATE);

                var ivaFac = listInvoicesConIeps.Where(i => i.InvoiceDate.ToString(DateFormat.INT_DATE).Equals(fecha)).
                    Select(c => c.IVA).Sum();
                var totalFac = listInvoicesConIeps.Where(i => i.InvoiceDate.ToString(DateFormat.INT_DATE).Equals(fecha)).
                    Select(c => c.Total).Sum();
                var iepsFac = listInvoicesConIeps.Where(i => i.InvoiceDate.ToString(DateFormat.INT_DATE).Equals(fecha)).
                    Select(c => c.IEPS).Sum();

                //facturas canceladas
                var iepsFacCan = listInvoicesConIeps.Where(i => i.InvoiceDate.ToString(DateFormat.INT_DATE).Equals(fecha) && !i.Status).
                    Select(c => c.IEPS).Sum();
                var ivaFacCan = listInvoicesConIeps.Where(i => i.InvoiceDate.ToString(DateFormat.INT_DATE).Equals(fecha) && !i.Status).
                    Select(c => c.IVA).Sum();
                var totalFacCan = listInvoicesConIeps.Where(i => i.InvoiceDate.ToString(DateFormat.INT_DATE).Equals(fecha) && !i.Status).
                    Select(c => c.Total).Sum();
                //notas de credito
                var ivaCN = listCreditNotesBoni.Where(cn => cn.Date.Equals(fecha) && cn.Type.Equals("NOTA DE CREDITO") && cn.Status.Equals("ACTIVA")).
                   Select(s => s.IVA).Sum();
                var iepsCN = listCreditNotesBoni.Where(cn => cn.Date.Equals(fecha) && cn.Type.Equals("NOTA DE CREDITO") && cn.Status.Equals("ACTIVA")).
                  Select(s => s.IEPS).Sum();
                var subtotalCN = listCreditNotesBoni.Where(cn => cn.Date.Equals(fecha) && cn.Type.Equals("NOTA DE CREDITO") && cn.Status.Equals("ACTIVA")).
                   Select(s => s.Import).Sum();
                var totalCN = listCreditNotesBoni.Where(cn => cn.Date.Equals(fecha) && cn.Type.Equals("NOTA DE CREDITO") && cn.Status.Equals("ACTIVA")).
                   Select(s => s.Total).Sum();
                // bonificaciones
                var ivaBON = listCreditNotesBoni.Where(cn => cn.Date.Equals(fecha) && cn.Type.Equals("BONIFICACION") && cn.Status.Equals("ACTIVA")).
                   Select(s => s.IVA).Sum();
                var iepsBON = listCreditNotesBoni.Where(cn => cn.Date.Equals(fecha) && cn.Type.Equals("BONIFICACION") && cn.Status.Equals("ACTIVA")).
                   Select(s => s.IEPS).Sum();
                var subtotalBON = listCreditNotesBoni.Where(cn => cn.Date.Equals(fecha) && cn.Type.Equals("BONIFICACION") && cn.Status.Equals("ACTIVA")).
                   Select(s => s.Import).Sum();
                var totalBON = listCreditNotesBoni.Where(cn => cn.Date.Equals(fecha) && cn.Type.Equals("BONIFICACION") && cn.Status.Equals("ACTIVA")).
                  Select(s => s.Total).Sum();

                smc.fecha = fecha;
                smc.cn_ieps = iepsCN;
                smc.cn_iva = ivaCN;
                smc.cn_subtotal = subtotalCN;
                smc.cn_total = totalCN;

                smc.bon_ieps = iepsBON;
                smc.bon_iva = ivaBON;
                smc.bon_subtotal = subtotalBON;
                smc.bon_total = totalBON;

                smc.fc_ieps = iepsFacCan;
                smc.fc_iva = ivaFacCan;
                smc.fc_subtotal = totalFacCan - ivaFacCan - iepsFacCan;
                smc.fc_total = totalFacCan;

                smc.dv_total = 0;
                smc.cu_total = 0;

                smc.fac_ieps = iepsFac;
                smc.fac_iva = ivaFac;
                smc.fac_subtotal = totalFac - ivaFac - iepsFac;
                smc.fac_total = totalFac;

                smc.gt_ieps = iepsFac - iepsBON - iepsCN - iepsFacCan;
                smc.gt_iva = ivaFac - ivaBON - ivaCN - ivaFacCan;
                smc.gt_subtotal = (totalFac - ivaFac - iepsFac) - (totalFacCan - ivaFacCan - iepsFacCan) - subtotalBON - subtotalCN;
                smc.gt_total = totalFac - totalFacCan - totalBON - totalCN;

                listSalesConcentrated.Add(smc);
            }

            return listSalesConcentrated;
        }

        private List<IepsModel> GetIepsFromInvoices(DateTime startDate, DateTime endDate)
        {
            var iepsr = (from inv in _context.INVOICES
                         join invd in _context.INVOICE_DETAIL on new { inv.invoice_no, inv.invoice_serie } equals new { invd.invoice_no, invd.invoice_serie }
                         join invdt in _context.INVOICE_DETAIL_TAX on invd.detail_id equals invdt.detail_id
                         where inv.invoice_date >= startDate && inv.invoice_date <= endDate
                        && invdt.tax_code.Contains(TaxTypes.IEPS)
                         select new IepsModel
                         {
                             value = invdt.tax_amount,
                             serie = inv.invoice_serie,
                             sale_id = inv.invoice_no
                         }).ToList();

            return iepsr;

        }

        public List<WeighingReport> GetWeighingList(WeighingIndex Model)
        {
            DateTime Date = DateTime.ParseExact(Model.Date, DateFormat.INT_DATE, CultureInfo.InvariantCulture);
            DateTime Date2 = DateTime.ParseExact(Model.Date2, DateFormat.INT_DATE, CultureInfo.InvariantCulture);
            var date = new SqlParameter("@date", Date);
            var date2 = new SqlParameter("@date2", Date2);
            var diferen = new SqlParameter("@diferen", Model.Difer);

            var result = _context.Database
                .SqlQuery<WeighingReport>("sp_DFLPOS_Weighing @date, @date2, @diferen", date, date2, diferen)
                .ToList();
            return result;
        }

        public List<MA_CLASS> GetDepartaments()
        {
            return _context.MA_CLASS.Where(d => d.level_category == 1).ToList();
        }

        public List<DFLPOS_SERVICES> GetTipoServicios()
        {
            var ServicesList = new List<DFLPOS_SERVICES>();
            ServicesList = _context.DFLPOS_SERVICES.Where(s => s.enabled == true).ToList();
            return ServicesList;
        }

        public SalesIEPSModel GetSalesIEPSModel(string startDate, string endDate, SalesType SalesType, SearchType SearchType)
        {
            var result = new SalesIEPSModel();

            DateTime Date = DateTime.ParseExact(startDate, DateFormat.INT_DATE, CultureInfo.InvariantCulture);
            DateTime Date2 = DateTime.ParseExact(endDate, DateFormat.INT_DATE, CultureInfo.InvariantCulture);
            var _date = new SqlParameter("@date", Date);
            var _date2 = new SqlParameter("@date2", Date2);
            var _consult = new SqlParameter("@consulta", SearchType);
            var _tipo = new SqlParameter("@tipo", SalesType);
            var _tax = new SqlParameter("@tax", TaxTypes.TAX);

            var sproc = "sp_DFLPOS_SalesIEPS @date, @date2, @tipo, @consulta, @tax";

            result.SalesIepsTax = _context.Database
              .SqlQuery<SalesIEPS>(sproc, _date, _date2, _tipo, _consult, _tax)
              .ToList();
            _date = new SqlParameter("@date", Date);
            _date2 = new SqlParameter("@date2", Date2);
            _consult = new SqlParameter("@consulta", SearchType);
            _tipo = new SqlParameter("@tipo", SalesType);
            _tax = new SqlParameter("@tax", TaxCodes.NOT_AVAILABLE);
            result.SalesIepsTaxZero = _context.Database
             .SqlQuery<SalesIEPS>(sproc, _date, _date2, _tipo, _consult, _tax)
             .ToList();

            result.SalesIepsTax.OrderBy(c => c.departamento);
            result.SalesIepsTaxZero.OrderBy(x => x.departamento);

            return result;
        }
        public serviciosRecargas GetServicesModelList(string startDate, string endDate, string compañia, string idServicio, bool fromMenu, int detailServ)
        {
            var result = new serviciosRecargas();
            var consulta = 0;
            DateTime Date = DateTime.ParseExact(startDate, DateFormat.INT_DATE, CultureInfo.InvariantCulture);
            DateTime Date2 = DateTime.ParseExact(endDate, DateFormat.INT_DATE, CultureInfo.InvariantCulture);
            var date = new SqlParameter("@date", Date);
            var date2 = new SqlParameter("@date2", Date2);

            var company = new SqlParameter();
            var consult1 = new SqlParameter("@consulta", 1);
            var consult2 = new SqlParameter("@consulta", 2);
            var consult3 = new SqlParameter("@consulta", 3);
            var consult4 = new SqlParameter("@consulta", 4);
            var consult5 = new SqlParameter("@consulta", 5);

            if (compañia == "0")
                company = new SqlParameter("@company", ""); //@company = N'SKY',
            else
                company = new SqlParameter("@company", compañia); //@company = N'SKY',
            var idservice = new SqlParameter("@idservicio", idServicio);


            var store = "sp_DFLPOS_ServicesSales @date, @date2, @company, @consulta, @idservicio";

            if (fromMenu)
            {
                result.CompaniasList = _context.Database
              .SqlQuery<Companias>(store, date, date2, company, consult5, idservice)
              .ToList();
            }
            else // la peticion viene del botón de buscar 
            {
                if (detailServ == 0)
                {
                    //  if (idServicio == "")
                    //  {

                    //      result.Services = _context.Database
                    //    .SqlQuery<ServicesModel>(store, date, date2, company, consult1, idservice)
                    //    .ToList();

                    //      date = new SqlParameter("@date", Date);
                    //      date2 = new SqlParameter("@date2", Date2);
                    //      if (compañia == "0")
                    //          company = new SqlParameter("@company", ""); //@company = N'SKY',
                    //      else
                    //          company = new SqlParameter("@company", compañia); //@company = N'SKY',
                    //      idservice = new SqlParameter("@idservicio", idServicio);



                    //      result.Recargas = _context.Database
                    //.SqlQuery<RecargasModel>(store, date, date2, company, consult3, idservice)
                    //.ToList();
                    //  }

                    if (idServicio == "")
                    {

                        var _litRecargas = new List<RecargasModel>();
                        RecargasModel _recargasModel;
                        var _recargas = _context.Database
                      .SqlQuery<RecargasModel>(store, date, date2, company, consult3, idservice)
                      .ToList();
                        foreach (RecargasModel rm in _recargas)
                        {
                            company = new SqlParameter("@company", rm.company); //@company = N'SKY',
                            date = new SqlParameter("@date", Date);
                            date2 = new SqlParameter("@date2", Date2);
                            idservice = new SqlParameter("@idservicio", idServicio);
                            consult4 = new SqlParameter("@consulta", 4);
                            _recargasModel = new RecargasModel();
                            _recargasModel = rm;
                            _recargasModel.RecargasDetail = _context.Database
                           .SqlQuery<RecargasModelDetail>(store, date, date2, company, consult4, idservice)
                           .ToList();
                            _litRecargas.Add(_recargasModel);
                        }
                        result.Recargas = _litRecargas;

                        var _litServices = new List<ServicesModel>();
                        ServicesModel _servicesModel;
                        if (compañia == "0")
                            company = new SqlParameter("@company", ""); //@company = N'SKY',
                        else
                            company = new SqlParameter("@company", compañia); //@company = N'SKY',
                        date = new SqlParameter("@date", Date);
                        date2 = new SqlParameter("@date2", Date2);
                        idservice = new SqlParameter("@idservicio", idServicio);
                        var _Services = _context.Database
                      .SqlQuery<ServicesModel>(store, date, date2, company, consult1, idservice)
                      .ToList();

                        foreach (ServicesModel _ser in _Services)
                        {
                            company = new SqlParameter("@company", _ser.company); //@company = N'SKY',
                            date = new SqlParameter("@date", Date);
                            date2 = new SqlParameter("@date2", Date2);
                            idservice = new SqlParameter("@idservicio", idServicio);
                            consult2 = new SqlParameter("@consulta", 2);
                            _servicesModel = new ServicesModel();
                            _servicesModel = _ser;
                            _servicesModel.ServicesDetail = _context.Database
                           .SqlQuery<ServicesModelDetail>(store, date, date2, company, consult2, idservice)
                           .ToList();
                            _litServices.Add(_servicesModel);
                        }
                        result.Services = _litServices;
                    }


                    if (idServicio == "1")
                    {
                        var _litRecargas = new List<RecargasModel>();
                        RecargasModel _recargasModel;
                        var _recargas = _context.Database
                      .SqlQuery<RecargasModel>(store, date, date2, company, consult3, idservice)
                      .ToList();
                        foreach (RecargasModel rm in _recargas)
                        {
                            company = new SqlParameter("@company", rm.company); //@company = N'SKY',
                            date = new SqlParameter("@date", Date);
                            date2 = new SqlParameter("@date2", Date2);
                            idservice = new SqlParameter("@idservicio", idServicio);
                            consult4 = new SqlParameter("@consulta", 4);
                            _recargasModel = new RecargasModel();
                            _recargasModel = rm;
                            _recargasModel.RecargasDetail = _context.Database
                           .SqlQuery<RecargasModelDetail>(store, date, date2, company, consult4, idservice)
                           .ToList();
                            _litRecargas.Add(_recargasModel);
                        }
                        result.Recargas = _litRecargas;
                    }
                    if (idServicio == "2")
                    {
                        var _litServices = new List<ServicesModel>();
                        ServicesModel _servicesModel;
                        var _Services = _context.Database
                      .SqlQuery<ServicesModel>(store, date, date2, company, consult1, idservice)
                      .ToList();

                        foreach (ServicesModel _ser in _Services)
                        {
                            company = new SqlParameter("@company", _ser.company); //@company = N'SKY',
                            date = new SqlParameter("@date", Date);
                            date2 = new SqlParameter("@date2", Date2);
                            idservice = new SqlParameter("@idservicio", idServicio);
                            consult2 = new SqlParameter("@consulta", 2);
                            _servicesModel = new ServicesModel();
                            _servicesModel = _ser;
                            _servicesModel.ServicesDetail = _context.Database
                           .SqlQuery<ServicesModelDetail>(store, date, date2, company, consult2, idservice)
                           .ToList();
                            _litServices.Add(_servicesModel);
                        }
                        result.Services = _litServices;
                    }
                }

            }
            if (detailServ == 1)
            {
                result.ServicesDetail = _context.Database
               .SqlQuery<ServicesModelDetail>(store, date, date2, company, consult2, idservice)
               .ToList();
            }
            if (detailServ == 2)
            {
                result.RecargasDetail = _context.Database
               .SqlQuery<RecargasModelDetail>(store, date, date2, company, consult4, idservice)
               .ToList();
            }
            return result;

        }

        public List<ItemsWithOutSalesModel> GetItemsWithOutSalesList1(string Date1, string Date2, IEnumerable<int> DepartmentsIds)
        {
            DataTable tvp = new DataTable();
            tvp.Columns.Add(new DataColumn("Item", typeof(int)));

            // populate DataTable from your List here
            foreach (var id in DepartmentsIds)
                tvp.Rows.Add(id);

            DateTime DateIni = DateTime.ParseExact(Date1, DateFormat.INT_DATE, CultureInfo.InvariantCulture);
            DateTime DateEnd = DateTime.ParseExact(Date2, DateFormat.INT_DATE, CultureInfo.InvariantCulture);
            var date = new SqlParameter("@date", DateIni);
            var date2 = new SqlParameter("@date2", DateEnd);
            var listIDS = new SqlParameter("@List", tvp);
            listIDS.SqlDbType = SqlDbType.Structured;
            listIDS.TypeName = "dbo.DepartmentList";


            var result = _context.Database
                .SqlQuery<ItemsWithOutSalesModel>("sp_DFLPOS_ItemsWithOutSalesByDepartments @date, @date2, @List", date, date2, listIDS)
                .ToList();
            return result;
        }

        public List<ItemsWithOutSalesModel> GetItemsWithOutSalesList(string Date1, string Date2, string DepartmentsIds)
        {
            DateTime DateIni = DateTime.ParseExact(Date1, DateFormat.INT_DATE, CultureInfo.InvariantCulture);
            DateTime DateEnd = DateTime.ParseExact(Date2, DateFormat.INT_DATE, CultureInfo.InvariantCulture);
            var date = new SqlParameter("@date", DateIni);
            var date2 = new SqlParameter("@date2", DateEnd);
            var listIDS = new SqlParameter("@List", DepartmentsIds);
            _context.Database.CommandTimeout = 1000;
            var result = _context.Database
                .SqlQuery<ItemsWithOutSalesModel>("sp_DFLPOS_ItemsWithOutSalesByDepartments @date, @date2, @List", date, date2, listIDS)
                .ToList();
            return result;
        }

        public List<ItemsWithSalesModel> GetItemsWithSalesList(string Date1, string Date2)
        {
            DateTime DateIni = DateTime.ParseExact(Date1, DateFormat.INT_DATE, CultureInfo.InvariantCulture);
            DateTime DateEnd = DateTime.ParseExact(Date2, DateFormat.INT_DATE, CultureInfo.InvariantCulture);
            var date = new SqlParameter("@date", DateIni);
            var date2 = new SqlParameter("@date2", DateEnd);


            var result = _context.Database
                .SqlQuery<ItemsWithSalesModel>("sp_DFLPOS_ItemsWithSalesByDepartments @date, @date2 ", date, date2).OrderByDescending(o => o.quantity)
                .ToList();
            return result;
        }
        #endregion
        //fin de reportes
        public long GetInvoiceBySaleNumber(long Sale)
        {
            return _context.DFLPOS_SALES.Find(Sale).invoice_no.Value;
        }

        public bool UpdateSalesWithInvoiceData(string CustomerCode, string InvoiceSerie, long InvoiceNumber, string SaleDate)
        {
            DateTime DateStart = DateTime.ParseExact(SaleDate, DateFormat.INT_DATE, CultureInfo.InvariantCulture);

            try
            {
                var update = _context.Database.ExecuteSqlCommand("UPDATE DFLPOS_SALES SET customer_code = @CustomerCode, invoice_serie = @InvoiceSerie, invoice_no = @InvoiceNumber WHERE sale_date = @DateStart AND invoice_no IS NULL AND invoice_serie IS NULL AND sale_id NOT IN (SELECT s.sale_id FROM DFLPOS_SALES_PAYMENT_METHOD spm JOIN DFLPOS_SALES s ON s.sale_id = spm.sale_id WHERE spm.pm_id = -1)",
                    new SqlParameter("CustomerCode", CustomerCode),
                    new SqlParameter("InvoiceSerie", InvoiceSerie),
                    new SqlParameter("InvoiceNumber", InvoiceNumber),
                    new SqlParameter("DateStart", DateStart));

                return true;
            }
            catch (Exception ex)
            {
                string msg = ex.Message;
                return false;
            }
        }

        public bool UpdateSalesWithSubstitutedInvoice(string Serie, long NewInvoice, long OldInvoice)
        {
            try
            {
                var update = _context.Database.ExecuteSqlCommand("UPDATE DFLPOS_SALES SET invoice_serie = @InvoiceSerie, invoice_no = @NewInvoice WHERE invoice_no = @OldInvoice AND invoice_serie = @InvoiceSerie",                    
                    new SqlParameter("InvoiceSerie", Serie),
                    new SqlParameter("NewInvoice", NewInvoice),
                    new SqlParameter("OldInvoice", OldInvoice));

                return true;
            }
            catch (Exception ex)
            {
                string msg = ex.Message;
                return false;
            }
        }

        public bool RollbackUpdateSales(string InvoiceSerie, long InvoiceNumber, long? OldInvoiceNumber, bool IsSubstitution)
        {
            try
            {
                if (!IsSubstitution)
                {
                    var update = _context.Database.ExecuteSqlCommand("UPDATE DFLPOS_SALES SET customer_code = '0', invoice_serie = null, invoice_no = null WHERE invoice_no = @InvoiceNumber AND invoice_serie = @InvoiceSerie",
                        new SqlParameter("InvoiceSerie", InvoiceSerie),
                        new SqlParameter("InvoiceNumber", InvoiceNumber));
                }
                else
                {
                    var update = _context.Database.ExecuteSqlCommand("UPDATE DFLPOS_SALES SET invoice_serie = @InvoiceSerie, invoice_no = @InvoiceNumber WHERE invoice_no = @NewInvoiceNumber AND invoice_serie = @InvoiceSerie",
                        new SqlParameter("InvoiceSerie", InvoiceSerie),
                        new SqlParameter("NewInvoiceNumber", InvoiceNumber),
                        new SqlParameter("InvoiceNumber", OldInvoiceNumber));
                }
                return true;
            }
            catch (Exception ex)
            {
                string msg = ex.Message;
                return false;
            }
        }

        public bool UpdateSalesFromCanceledlInvoice(long InvoiceNumber, string InvoiceSerie, bool IsCredit)
        {
            try
            {
                if (IsCredit)
                    _context.Database.ExecuteSqlCommand("UPDATE DFLPOS_SALES SET invoice_serie = NULL, invoice_no = NULL WHERE invoice_no = @InvoiceNumber AND invoice_serie = @InvoiceSerie",
                    new SqlParameter("InvoiceSerie", InvoiceSerie),
                    new SqlParameter("InvoiceNumber", InvoiceNumber));
                else
                    _context.Database.ExecuteSqlCommand("UPDATE DFLPOS_SALES SET invoice_serie = NULL, invoice_no = NULL, customer_code = '0' WHERE invoice_no = @InvoiceNumber AND invoice_serie = @InvoiceSerie",
                    new SqlParameter("InvoiceSerie", InvoiceSerie),
                    new SqlParameter("InvoiceNumber", InvoiceNumber));

                _context.SaveChanges();
                return true;
            }
            catch (Exception ex)
            {
                string msg = ex.Message;
                return false;
            }
        }

        public List<DFLPOS_SALES> GetSalesByInvoiceNumber(long InvoiceNumber)
        {
            var SalesList = new List<DFLPOS_SALES>();
            SalesList = _context.DFLPOS_SALES.Where(s => s.invoice_no == InvoiceNumber).ToList();
            return SalesList;
        }

        public string GetDateLastSale(string CustomerCode)
        {
            var Sales = _context.DFLPOS_SALES.Where(s => s.customer_code == CustomerCode).OrderByDescending(d => d.sale_date).ToList();
            string SaleDate = string.Empty;
            if (Sales.Count > 0)
                SaleDate = Sales.First().sale_date.ToString(DateFormat.INT_DATE);

            return SaleDate;
        }

        public bool IsSaleFromGlobalInvoice(long SaleNumber)
        {
            var Sale = _context.DFLPOS_SALES.Find(SaleNumber);
            bool IsFromGlobal = false;

            if(Sale.invoice_no.HasValue)
                IsFromGlobal = _InvoiceRepository.IsInvoiceDaily(Sale.invoice_no.Value);

            return IsFromGlobal;
        }

        public decimal SaleTotal(long SaleNumber)
        {
            var Sale = _context.DFLPOS_SALES.Find(SaleNumber);
            decimal Total = Sale != null ? Sale.sale_total : 0;
            return Total;
        }

        public List<USER_MASTER> GetUsersCashiersList()
        {
            List<USER_MASTER> CashiersList = _context.USER_MASTER.Where(s => s.uuser == null).ToList();
            var CashiersList2 = _context.DFLPOS_TRANSACTIONS_CARD.Include(x => x.USER_MASTER).ToList();
            return CashiersList;
        }

        public List<TransactionsCardData> GetTransactionsCardData(TransactionsCardIndex Model)
        {
            DateTime DateStart = DateTime.ParseExact(Model.StartDate, DateFormat.INT_DATE, CultureInfo.InvariantCulture);
            DateTime DateEnd = DateTime.ParseExact(Model.EndDate, DateFormat.INT_DATE, CultureInfo.InvariantCulture);
            var ReportList = new List<TransactionsCardData>();

            var DataInfo = (from d in _context.DFLPOS_TRANSACTIONS_CARD
                            where DbFunctions.TruncateTime(d.Fecha) >= DateStart
                                  && DbFunctions.TruncateTime(d.Fecha) <= DateEnd
                                  && (string.IsNullOrEmpty(Model.Cashier) || d.USER_MASTER.emp_no == Model.Cashier)
                                  && d.responseCode == "00"
                                  && d.cash_back > 0
                            select new TransactionsCardData
                            {
                                Fecha = d.Fecha,
                                FirstName = d.USER_MASTER.first_name,
                                LastName = d.USER_MASTER.last_name,
                                Monto = d.Amount,
                                Retiro = d.cash_back,
                                TransactionsCardId = d.Id,
                                Cashier = d.emp_no,
                                Total = d.Amount + d.cash_back
                            }).ToList();
            return DataInfo;
        }

        public List<TransactionsCardByCashier> GetTransactionsCardByCashier(TransactionsCardIndex Model)
        {
            DateTime DateStart = DateTime.ParseExact(Model.StartDate, DateFormat.INT_DATE, CultureInfo.InvariantCulture);
            DateTime DateEnd = DateTime.ParseExact(Model.EndDate, DateFormat.INT_DATE, CultureInfo.InvariantCulture);

            var DataInfo = (from d in _context.DFLPOS_TRANSACTIONS_CARD
                            where DbFunctions.TruncateTime(d.Fecha) >= DateStart
                                  && DbFunctions.TruncateTime(d.Fecha) <= DateEnd
                                  && (string.IsNullOrEmpty(Model.Cashier) || d.USER_MASTER.emp_no == Model.Cashier)
                                  && d.responseCode == "00"
                                  && d.cash_back > 0
                            select new
                            {
                                d.Fecha,
                                NombreCajero = d.USER_MASTER.first_name + " " + d.USER_MASTER.last_name,
                                d.Amount,
                                d.CardNumber,
                                d.AuthCode,
                                d.FolioVenta,
                                d.PosId,
                                CashBack = d.cash_back,
                                EmpNo = d.emp_no,
                                TransactionsCardId = d.Id,
                                Total = d.Amount + d.cash_back
                            }).AsEnumerable()
                .Select(s => new TransactionsCardByCashier
                {
                    FechaS = s.Fecha.Value.ToString(DateFormat.INT_DATE),
                    HoraS = s.Fecha.Value.ToLongTimeString(),
                    NombreCajero = s.NombreCajero,
                    Amount = s.Amount,
                    CardNumber = s.CardNumber,
                    AuthCode = s.AuthCode,
                    FolioVenta = s.FolioVenta,
                    PosId = s.PosId,
                    CashBack = s.CashBack,
                    EmpNo = s.EmpNo,
                    TransactionsCardId = s.TransactionsCardId,
                    Total = s.Amount + s.CashBack
                }).ToList();

            return DataInfo;
        }

        public List<TransactionsCardByCashier> TransactionCardReport(TransactionsCardIndex Model)
        {
            DateTime DateStart = DateTime.ParseExact(Model.StartDate, DateFormat.INT_DATE, CultureInfo.InvariantCulture);
            DateTime DateEnd = DateTime.ParseExact(Model.EndDate, DateFormat.INT_DATE, CultureInfo.InvariantCulture);
            var ReportList = new List<TransactionsCardByCashier>();

            ReportList = (from d in _context.DFLPOS_TRANSACTIONS_CARD
                          join p in _context.PAYMENT_METHOD on d.pm_id equals p.pm_id
                          where DbFunctions.TruncateTime(d.Fecha) >= DateStart
                                && DbFunctions.TruncateTime(d.Fecha) <= DateEnd
                                && (string.IsNullOrEmpty(Model.Cashier) || d.USER_MASTER.emp_no == Model.Cashier)
                          select new
                          {
                              d.Fecha,
                              NombreCajero = d.USER_MASTER.first_name + " " + d.USER_MASTER.last_name,
                              d.Amount,
                              d.CardNumber,
                              d.AuthCode,
                              d.FolioVenta,
                              d.PosId,
                              CashBack = d.cash_back,
                              PM = p.pm_name,
                              d.SaleId,
                              RespCode = d.responseCode,
                              RespMessage = d.responseMsg,
                              TypeV = d.Type
                          }).AsEnumerable()
                .Select(s => new TransactionsCardByCashier
                {
                    FechaS = s.Fecha.Value.ToString("dd/MM/yyyy HH:mm:ss tt"),
                    NombreCajero = s.NombreCajero,
                    Amount = s.Amount,
                    CardNumber = s.CardNumber,
                    AuthCode = s.AuthCode,
                    FolioVenta = s.FolioVenta,
                    PosId = s.PosId,
                    CashBack = s.CashBack,
                    SaleId = s.SaleId.Value,
                    PaymentMethod = s.PM,
                    ResponseCode = s.RespCode,
                    ResponseMessage = s.RespMessage,
                    SaleType = s.TypeV == "V" ? "Venta" : s.TypeV == "C" ? "Cancelación" : "Devolución"
                }).ToList();

            return ReportList;
        }

        public decimal GetBonificationEmployeeTotalByDate(string SaleDate)
        {
            decimal SumTotal = 0;
            DateTime DateSale = DateTime.ParseExact(SaleDate, DateFormat.INT_DATE, CultureInfo.InvariantCulture);

            SumTotal = (from s in _context.DFLPOS_SALES
                        join spm in _context.DFLPOS_SALES_PAYMENT_METHOD on s.sale_id equals spm.sale_id
                        where s.sale_date == DateSale
                              && !s.invoice_no.HasValue
                              && string.IsNullOrEmpty(s.invoice_serie)
                              && spm.pm_id == PaymentMethods.EMPLOYEE_DISCOUNT
                        select spm).ToList().Sum(c => c.amount_entered.Value);

            return SumTotal;
        }

        public decimal GetBonificationEmployeeTotalByInvoice(long InvoiceNumber, string InvoiceSerie)
        {
            decimal SumTotal = 0;
            SumTotal = (from s in _context.DFLPOS_SALES
                        join spm in _context.DFLPOS_SALES_PAYMENT_METHOD on s.sale_id equals spm.sale_id
                        where s.invoice_no == InvoiceNumber
                              && s.invoice_serie == InvoiceSerie
                              && spm.pm_id == PaymentMethods.EMPLOYEE_DISCOUNT
                        select spm).ToList().Sum(c => c.amount_entered.Value);

            return SumTotal;
        }

        public decimal GetBonificationEmployeeTotalByInvoiceDate(DateTime InvoiceDate)
        {
            decimal SumTotal = 0;            
            SumTotal = (from s in _context.DFLPOS_SALES
                        join spm in _context.DFLPOS_SALES_PAYMENT_METHOD on s.sale_id equals spm.sale_id
                        where s.sale_date == InvoiceDate                              
                              && spm.pm_id == PaymentMethods.EMPLOYEE_DISCOUNT
                        select spm).ToList().Sum(c => c.amount_entered.Value);

            return SumTotal;
        }

        public List<AverageCustomersByHour> GetAverageCustomertByHours(string SaleDate)
        {
            DateTime Date = DateTime.ParseExact(SaleDate, DateFormat.INT_DATE, CultureInfo.InvariantCulture);
            var DataList = _context.Database.SqlQuery<AverageCustomersByHour>("sp_customer_avg_hour @SaleDate", new SqlParameter("SaleDate", Date)).ToList();
            return DataList;
        }

        public List<Accumulated> GetAccumulatedData(string DateCheck, string EmployeeNumber)
        {
            DateTime Date = DateTime.ParseExact(DateCheck, DateFormat.INT_DATE, CultureInfo.InvariantCulture);
            var List = new List<Accumulated>();
            var element = new Accumulated();
            var Alert = _context.DFLPOS_CONFIG.Where(c => c.config_key == SalesConfig.WD_ALERT).SingleOrDefault();
            var Mandatory = _context.DFLPOS_CONFIG.Where(c => c.config_key == SalesConfig.WD_MANDATORY).SingleOrDefault();

            decimal RateAlert = decimal.Parse(Alert.config_value);
            decimal RateMandatory = decimal.Parse(Mandatory.config_value);
            if (!string.IsNullOrEmpty(EmployeeNumber))
            {
                var Cashier = _context.USER_MASTER.Find(EmployeeNumber);
                element.Cashier = string.Format("{0} {1}", Cashier.first_name, Cashier.last_name);
                element.Total = _context.Database.SqlQuery<decimal>("sp_DFLPOS_Get_Accumulated @fecha, @emp_no", new SqlParameter("fecha", Date), new SqlParameter("emp_no", EmployeeNumber)).SingleOrDefault();
                element.Color = element.Total < RateAlert ? "Green" : (element.Total >= RateAlert && element.Total < RateMandatory ? "Yellow" : "Red");
                element.Date = DateCheck;

                List.Add(element);
            }
            else
            {
                var Cashiers = (from s in _context.DFLPOS_SALES
                                join u in _context.USER_MASTER on s.emp_no equals u.emp_no
                                where s.sale_date == Date
                                select new UserModelFirst
                                {
                                    EmpNo = u.emp_no,
                                    UserName = u.user_name,
                                    FirstName = u.first_name,
                                    LastName = u.last_name,
                                    Email = u.email,
                                    mobile_tel = u.mobile_tel,
                                    office_tel = u.office_tel
                                }).Distinct().ToList();

                foreach (var item in Cashiers)
                {
                    element = new Accumulated();
                    element.Cashier = string.Format("{0} {1}", item.FirstName, item.LastName);
                    element.Total = _context.Database.SqlQuery<decimal>("sp_DFLPOS_Get_Accumulated @fecha, @emp_no", new SqlParameter("fecha", Date), new SqlParameter("emp_no", item.EmpNo)).SingleOrDefault();
                    element.Date = DateCheck;
                    element.Color = element.Total < RateAlert ? "Green" : (element.Total >= RateAlert && element.Total < RateMandatory ? "Yellow" : "Red");
                    List.Add(element);
                }
            }

            return List;
        }

        public bool CheckSalesWithPMEpmloyeeBon(string DateValue)
        {
            DateTime Date = DateTime.ParseExact(DateValue, DateFormat.INT_DATE, CultureInfo.InvariantCulture);
            bool Exist = (from spm in _context.DFLPOS_SALES_PAYMENT_METHOD
                          join s in _context.DFLPOS_SALES on spm.sale_id equals s.sale_id
                          where s.sale_date == Date
                                && (spm.pm_id == PaymentMethods.COUPON_FLORIWOOD || spm.pm_id == PaymentMethods.EMPLOYEE_DISCOUNT)
                          select spm).Any();

            return Exist;
        }
        public List<SalesDetailReportModel> GetSalesDetailsBySaleId(long saleId)
        {
            var _salesdETAIL = _context.DFLPOS_SALES_DETAIL.Join(_context.ITEM, a => a.part_number, b => b.part_number,
                 (a, b) => new { a, b }).Where(x => x.a.sale_id == saleId)
                  .Join(_context.MA_TAX, x => x.b.part_ieps, mt => mt.tax_code, (x, mt) => new { x, mt })
                  .Select(sd => new SalesDetailReportModel
                  {
                      part_number = sd.x.a.part_number,
                      part_name = sd.x.b.part_description,
                      part_quantity = sd.x.a.original_quantity,
                      part_price = sd.x.a.detail_captured - ((decimal)(sd.mt.tax_value / 100) * sd.x.a.detail_captured),
                      part_importe = sd.x.a.part_price - ((decimal)(sd.mt.tax_value / 100) * (decimal)sd.x.a.part_price),
                      part_iva = sd.x.a.part_tax,
                      part_iva_percentage = sd.x.a.part_tax_percentage ?? 0,
                  //part_iva_percentage = (sd.x.a.part_tax / (sd.x.a.part_price != 0 ? sd.x.a.part_price : 1)) * 100 ANTES
                  part_ieps = (decimal)(sd.mt.tax_value / 100) * (decimal)sd.x.a.part_price,
                      part_ieps_percentage = (decimal)sd.mt.tax_value,
                      part_total = sd.x.a.part_price + sd.x.a.part_tax
                  }).ToList();
            return _salesdETAIL;
        }
        public List<SalesDetailReportModel> GetSalesDetailsBySaleIdReverse(long saleId)
        {
            var _salesdETAIL = _context.DFLPOS_SALES_DETAIL.Join(_context.ITEM, a => a.part_number, b => b.part_number,
                 (a, b) => new { a, b }).Where(x => x.a.sale_id == saleId)
                  .Join(_context.MA_TAX, x => x.b.part_ieps, mt => mt.tax_code, (x, mt) => new { x, mt })
                  .Select(sd => new SalesDetailReportModel
                  {
                      part_number = sd.x.a.part_number,
                      part_name = sd.x.b.part_description,
                      part_quantity = sd.x.a.quantity,
                      part_price = sd.x.a.detail_captured - ((decimal)(sd.mt.tax_value / 100) * sd.x.a.detail_captured),
                      part_importe = sd.x.a.part_price - ((decimal)(sd.mt.tax_value / 100) * (decimal)sd.x.a.part_price),
                      part_iva = sd.x.a.part_tax,
                      part_iva_percentage = (sd.x.a.part_tax / (sd.x.a.part_price != 0 ? sd.x.a.part_price : 1)) * 100,
                      part_ieps = (decimal)(sd.mt.tax_value / 100) * (decimal)sd.x.a.part_price,
                      part_ieps_percentage = (decimal)sd.mt.tax_value,
                      part_total = sd.x.a.part_price + sd.x.a.part_tax
                  }).ToList();

            if (_salesdETAIL != null)
                return _salesdETAIL;
            else
                return new List<SalesDetailReportModel>();
        }

        public List<UtilitySales> UtilitySales(UtilitySalesIndex Model)
        {
            DateTime StartDate = DateTime.ParseExact(Model.StartDate, DateFormat.INT_DATE, CultureInfo.InvariantCulture);
            DateTime EndDate = DateTime.ParseExact(Model.EndDate, DateFormat.INT_DATE, CultureInfo.InvariantCulture);
            var List = new List<UtilitySales>();

            List = _context.Database.SqlQuery<UtilitySales>("sp_DW_FACT_SALES_ART_DAY @StartDate, @EndDate, @DepartmentId, @ItemNumber, @FamilyId",
                                                            new SqlParameter("StartDate", StartDate),
                                                            new SqlParameter("EndDate", EndDate),
                                                            new SqlParameter("DepartmentId", Model.Department.ToString()),
                                                            new SqlParameter("ItemNumber", Model.PartNumber),
                                                            new SqlParameter("FamilyId", Model.Family.ToString())).ToList();
            if (Model.GroupByDepartment)
            {
                var Grouped = (from l in List
                               group l by new { l.grupo_id, l.department } into g
                               select new UtilitySales
                               {
                                   part_number = string.Empty,
                                   part_description = g.Key.department,
                                   QTY = g.Sum(x => x.QTY),
                                   COSTO_STD = g.Sum(x => x.COSTO_STD * x.QTY),
                                   VENTA = g.Sum(x => x.VENTA),
                                   IEPS = g.Sum(x => x.IEPS),
                                   IVA = g.Sum(x => x.IVA)
                               }).ToList();

                List = Grouped;
            }

            return List;
        }

        public List<GiftCardData> GetDataActiveCardsReport(GiftCardReportIndex Model)
        {
            DateTime StartDate = DateTime.ParseExact(Model.StartDate, DateFormat.INT_DATE, CultureInfo.InvariantCulture);
            DateTime EndDate = DateTime.ParseExact(Model.EndDate, DateFormat.INT_DATE, CultureInfo.InvariantCulture);
            var DataList = new List<GiftCardData>();
            DataList = _context.Database.SqlQuery<GiftCardData>(@"SELECT [tarjeta_id] CardId,
	                                                               [Numero Tarjeta] CardNumber,
	                                                               [Monto Compra] Amount,
	                                                               CONVERT(VARCHAR(50),[Fecha Compra],22) Date,
	                                                               [Sucursal Compra] StoreMarket,
	                                                               [Caja Compra] SaleCheckpoint,
	                                                               [Venta Compra] SaleId, 
	                                                               [Estado] StatusType,
	                                                               [site_code] SiteCode 
                                                            FROM [TARJETAS].[TARJETA].[dbo].vw_CompraTarjetasRegalo 
                                                            WHERE site_code =  @SiteCode
                                                            AND [Fecha Compra] >= @StartDate
                                                            AND [Fecha Compra] <= @EndDate",
                                                                new SqlParameter("@SiteCode", Model.SiteCode),
                                                                new SqlParameter("@StartDate", StartDate),
                                                                new SqlParameter("@EndDate", EndDate)).ToList();

            //return DataList.Where(c => DbFunctions.TruncateTime(c.BuyDate) >= StartDate && DbFunctions.TruncateTime(c.BuyDate) <= EndDate).ToList();
            return DataList;
        }

        public List<GiftCardData> GetDataGiftCardChargesReport(GiftCardReportIndex Model)
        {
            DateTime StartDate = DateTime.ParseExact(Model.StartDate, DateFormat.INT_DATE, CultureInfo.InvariantCulture);
            DateTime EndDate = DateTime.ParseExact(Model.EndDate, DateFormat.INT_DATE, CultureInfo.InvariantCulture);
            var DataList = new List<GiftCardData>();
            DataList = _context.Database.SqlQuery<GiftCardData>(@"SELECT [tarjeta_id] CardId,
	                                                                [tarjeta_numero] CardNumber,
	                                                                [Monto Cargo] Amount,
	                                                                CONVERT(VARCHAR(50),[Fecha Cargo],22) Date,
	                                                                [Sucursal Cargo] StoreMarket,
	                                                                [Caja Cargo] SaleCheckpoint,
	                                                                [Venta Cargo] SaleId, 
	                                                                [Tipo] StatusType,
	                                                                [site_code] SiteCode 
                                                            FROM [TARJETAS].[TARJETA].[dbo].wv_MovimientosTarjetasRegalo
                                                            WHERE site_code =  @SiteCode
                                                            AND [Fecha Cargo] >= @StartDate
                                                            AND [Fecha Cargo] <= @EndDate",
                                                                new SqlParameter("@SiteCode", Model.SiteCode),
                                                                new SqlParameter("@StartDate", StartDate),
                                                                new SqlParameter("@EndDate", EndDate)).ToList();
            
            return DataList;
        }

        public InvoiceDetails GetSaleDetailForBonificationCreditNotes(long SaleNumber)
        {
            DFL_SAIEntities context = new DFL_SAIEntities();
            context.Configuration.AutoDetectChangesEnabled = false;
            var SiteConfig = context.SITE_CONFIG.FirstOrDefault();
            var serie = context.SITES.Where(c => c.site_code == SiteConfig.site_code).FirstOrDefault().site_serie;
            InvoiceDetails Details;            

            Details = (from s in context.DFLPOS_SALES
                       join c in context.CUSTOMERS on s.customer_code equals c.customer_code
                       join spm in context.DFLPOS_SALES_PAYMENT_METHOD on s.sale_id equals spm.sale_id
                       join pm in context.PAYMENT_METHOD on spm.pm_id equals pm.pm_id
                       where s.sale_id == SaleNumber
                       select new InvoiceDetails
                       {
                           InvoiceNumber = s.sale_id,
                           ClientNumber = c.customer_code,
                           ClientName = c.customer_name,
                           PaymentMethod = pm.SAT_PAYMENT_METHOD_CODE.sat_pm_description,
                           Currency = s.DFLPOS_SALES_DETAIL.FirstOrDefault().cur_code,
                           SubTotal = s.sale_total - s.sale_tax,
                           Tax = s.sale_tax,
                           Total = s.sale_total,
                           InvoiceBalance = 0,
                           IsInvoiceDaily = false,
                           Number = s.sale_id,
                           InvoiceSerie = serie,
                           Serie = serie,
                           SatPMCode = pm.sat_pm_code,
                           CurrencyRate = s.DFLPOS_SALES_DETAIL.FirstOrDefault().currency_rate
                       }).FirstOrDefault();

            context.Configuration.AutoDetectChangesEnabled = true;
            context.Dispose();

            if (Details == null)
                Details = new InvoiceDetails();

            return Details;
        }
    }
}
