﻿using App.DAL.Currency;
using App.Entities;
using App.Entities.ViewModels.CreditNotes;
using App.Entities.ViewModels.Donations;
using App.Entities.ViewModels.Invoices;
using App.Entities.ViewModels.Sales;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.SqlClient;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static App.Entities.ViewModels.Common.Constants;

namespace App.DAL.Sales
{
    public class PortadaRepository
    {
        private DFL_SAIEntities _context;
        private CurrencyRepository _CurrencyRepository;
        public PortadaRepository()
        {
            _context = new DFL_SAIEntities();
            _CurrencyRepository = new CurrencyRepository();

        }
        static decimal _ieps0Contado;
        static decimal _ieps0Credito = 0;
        static decimal _iepsGravadoContado = 0;
        static decimal _iepsGravadoCredito = 0;
        public List<INVOICES> getInvoices(string Date)
        {
             DateTime DateStart = DateTime.ParseExact(Date, "dd/MM/yyyy", CultureInfo.InvariantCulture);
        
            return _context.INVOICES.Where(i => i.invoice_date == DateStart && i.invoice_status == true && i.sat_pm_code=="17" && i.is_credit==false).ToList();

        }
        public decimal[,] GetTotals(string Date, string Currency)
        {
            DateTime DateStart = DateTime.ParseExact(Date, "dd/MM/yyyy", CultureInfo.InvariantCulture);
            var listInvoices = _context.INVOICES.Where(i => i.invoice_date == DateStart && i.cur_code == Currency && i.invoice_status == true).ToList();
            var listIeps = new List<IepsModel>();
            var listIepsNT = new List<IepsModel>();

            decimal _amountExentoCash = 0;
            decimal _amountExentoCredit = 0;
            if (listInvoices.Count > 0)
            {
                string _invoicesLIstCredit = "";
                string _invoicesLIstCash = "";
                foreach (INVOICES _inv in listInvoices)
                {
                    if (_inv.is_credit)
                        _invoicesLIstCredit += _inv.invoice_no + ",";
                    else
                        _invoicesLIstCash += _inv.invoice_no + ",";
                }
                if (_invoicesLIstCredit.Length > 0)
                {
                    _invoicesLIstCredit = _invoicesLIstCredit.Substring(0, _invoicesLIstCredit.Length - 1);
                }
                else
                    _invoicesLIstCredit = "0";


                if (_invoicesLIstCash.Length > 0)
                    _invoicesLIstCash = _invoicesLIstCash.Substring(0, _invoicesLIstCash.Length - 1);
                else
                    _invoicesLIstCash = "0";


                var date = new SqlParameter("@date", DateStart.ToString("yyyy/MM/dd"));
                var date2 = new SqlParameter("@date2", DateStart.ToString("yyyy/MM/dd"));
                var clauseCash = new SqlParameter("@clause", _invoicesLIstCash);
                var clauseCredit = new SqlParameter("@clause", _invoicesLIstCredit);
                _amountExentoCash = _context.Database.SqlQuery<decimal>("sp_GetAmountExentoFromSales @date, @date2, @clause", date, date2, clauseCash).Sum();

                date = new SqlParameter("@date", DateStart.ToString("yyyy/MM/dd"));
                date2 = new SqlParameter("@date2", DateStart.ToString("yyyy/MM/dd"));

                _amountExentoCredit = _context.Database.SqlQuery<decimal>("sp_GetAmountExentoFromSales @date, @date2, @clause", date, date2, clauseCredit).Sum();

            }
                InvoiceModel invoiceModel;
            var listInvoiceModel = new List<InvoiceModel>();
            if (listInvoices != null)
            {
                listIeps = GetIepsFromInvoices(DateStart);
                listIepsNT = GetIepsFromCreditNotes(DateStart);
            }

            foreach (INVOICES invo in listInvoices)
            {
                invoiceModel = new InvoiceModel();
                var ieps = listIeps.Where(s => s.sale_id == invo.cfdi_id && s.serie == invo.invoice_serie).Select(c => c.value).Sum();
                invoiceModel.InvoiceNumber = invo.cfdi_id.Value;
                invoiceModel.Amount = invo.invoice_total - invo.invoice_tax - ieps;
                invoiceModel.CreditOrCash = invo.is_credit ? "Crédito" : "Contado";
                invoiceModel.Tax = invo.invoice_tax;
                invoiceModel.Tax0 = invo.invoice_total - invo.invoice_tax - (invo.invoice_tax * 100 / 8);
                invoiceModel.gravado = (invo.invoice_tax * 100 / 8);
                listInvoiceModel.Add(invoiceModel);
            }


            _ieps0Contado = listIeps.Where(s => s.iscredit == false && s.price == 8).Select(c => c.value).Sum();// - listIepsNT.Where(s => s.iscredit == false && s.price == 8).Select(c => c.value).Sum();
            _iepsGravadoContado = listIeps.Where(s => s.iscredit == false && s.price != 8).Select(c => c.value).Sum();// - listIepsNT.Where(s => s.iscredit == false && s.price != 8).Select(c => c.value).Sum();

            _ieps0Credito = listIeps.Where(s => s.iscredit == true && s.price == 8).Select(c => c.value).Sum();// - listIepsNT.Where(s => s.iscredit == true && s.price == 8).Select(c => c.value).Sum();
            _iepsGravadoCredito = listIeps.Where(s => s.iscredit == true && s.price != 8).Select(c => c.value).Sum();// - listIepsNT.Where(s => s.iscredit == true && s.price != 8).Select(c => c.value).Sum();


            return GetTotalsPortada(listInvoiceModel,_amountExentoCash, _amountExentoCredit);
        }




        public List<IEPS_Detail> GetIeps(string Datee)
        {
            DateTime DateStart = DateTime.ParseExact(Datee, "dd/MM/yyyy", CultureInfo.InvariantCulture);
            var listIeps = GetIepsFromInvoices(DateStart);
            var GroupedTaxes = (from p in listIeps
                                group p by p.nameIeps into g
                                select new IEPS_Detail
                                {
                                    IEPS = g.First().nameIeps,
                                    Importe = g.Sum(s => s.value)

                                }).ToList();

          /*  var listIepsCN = GetIepsFromCreditNotes(DateStart);
            var GroupedTaxesCN = (from p in listIepsCN
                                  group p by p.nameIeps into g
                                  select new IEPS_Detail
                                  {
                                      IEPS = g.First().nameIeps,
                                      Importe = g.Sum(s => s.value)

                                  }).ToList();
            var _GroupTaxesFinalList = new List<IEPS_Detail>();
            var _GroupTaxesFinal = new IEPS_Detail();
            foreach (IEPS_Detail _iepsD in GroupedTaxes)
            {
                _GroupTaxesFinal = new IEPS_Detail();
                _GroupTaxesFinal.IEPS = _iepsD.IEPS;
                foreach (IEPS_Detail _iepsDcn in GroupedTaxesCN)
                {
                    if (_iepsD.IEPS == _iepsDcn.IEPS)
                        _iepsD.Importe = _iepsD.Importe - _iepsDcn.Importe;
                }
                _GroupTaxesFinal.Importe = _iepsD.Importe;

                _GroupTaxesFinalList.Add(_GroupTaxesFinal);
            }*/

            if (GroupedTaxes != null)
                return GroupedTaxes;
            else
                return new List<IEPS_Detail>();

        }
        private List<IepsModel> GetIepsFromInvoices(DateTime startDate)
        {
            var iepsr = (from inv in _context.INVOICES
                         join invd in _context.INVOICE_DETAIL on new { inv.invoice_no, inv.invoice_serie } equals new { invd.invoice_no, invd.invoice_serie }
                         join invdt in _context.INVOICE_DETAIL_TAX on invd.detail_id equals invdt.detail_id
                         where inv.invoice_date == startDate && inv.invoice_status
                        && invdt.tax_code.Contains("IEPS")
                         select new IepsModel
                         {
                             value = invdt.tax_amount,
                             serie = inv.invoice_serie,
                             sale_id = inv.invoice_no,
                             nameIeps = invdt.MA_TAX.name,
                             price = invdt.MA_TAX.tax_value.Value,
                             iscredit = inv.is_credit

                         }).ToList();

            return iepsr;

        }
        private List<IepsModel> GetIepsFromCreditNotes(DateTime startDate)
        {
            var iepsr = (from inv in _context.CREDIT_NOTES
                         join invd in _context.CREDIT_NOTE_DETAIL on inv.cn_id equals invd.cn_id
                         join invdt in _context.CREDIT_NOTE_DETAIL_TAX on invd.detail_id equals invdt.detail_id
                         join invo in _context.INVOICES on inv.invoice_no equals invo.invoice_no
                         where DbFunctions.TruncateTime(inv.cn_date) == startDate && inv.cn_status
                        && invdt.tax_code.Contains(TaxTypes.IEPS)
                         select new IepsModel
                         {
                             value = invdt.tax_amount,
                             serie = inv.invoice_serie,
                             sale_id = inv.invoice_no.Value,
                             nameIeps = invdt.MA_TAX.name,
                             price = invdt.MA_TAX.tax_value.Value,
                             iscredit = invo.is_credit

                         }).ToList();

            return iepsr;

        }
        public static decimal[,] GetTotalsPortada(List<InvoiceModel> Headers, decimal exentoCash, decimal exentoCredit)
        {
            decimal[,] Totales = new decimal[7, 3];
            decimal _ieps = 0;
            decimal _iepsCred = 0;

            decimal _Gravado = 0;
            // C A R G A R     D A T O S
            foreach (InvoiceModel Header in Headers)
            {

                if (Header.CreditOrCash == "Contado")
                {// B I N D     C A S H    
                    _ieps += Header.Ieps;
                    // R A T E     Z E R O
                    

                    _Gravado += Header.gravado;

                    Totales[0, 1] += Header.Tax0;

                    Totales[1, 1] += Header.gravado;
                    // I E P S 0
                    // if (Header.Tax != 0 && Header.Ieps != 0)
                    Totales[3, 1] = _ieps0Contado;
                    // else       // I E P S
                    Totales[4, 1] = _iepsGravadoContado;
                    // T A X
                    Totales[5, 1] += Header.Tax;

                }
                else
                {// B I N D     C R E D I T

                    // R A T E     Z E R O
                    _iepsCred += Header.Ieps;
                    Totales[0, 0] += Header.Tax0;

                    Totales[1, 0] += Header.gravado;
                    // I E P S 0
                    // if (Header.Tax != 0 && Header.Ieps != 0)
                    Totales[3, 0] = _ieps0Credito;
                    // else       // I E P S
                    Totales[4, 0] = _iepsGravadoCredito;
                    // T A X
                    Totales[5, 0] += Header.Tax;

                }
            }


            Totales[0, 1] = Totales[0, 1] - Totales[3, 1];
            Totales[1, 1] = Totales[1, 1] - Totales[4, 1];




            Totales[0, 0] = Totales[0, 0] - Totales[3, 0];
            Totales[1, 0] = Totales[1, 0] - Totales[4, 0];


            Totales[0, 1] = Totales[0, 1] - exentoCash;
            Totales[0, 0] = Totales[0, 0] - exentoCredit;

            Totales[2, 1] = exentoCash;
            Totales[2, 0] = exentoCredit;

            // S U M A R    T O T A L E S
            for (int f = 0; f < Totales.GetLength(0); f++)
            {
                for (int c = 0; c < Totales.GetLength(1); c++)
                {
                    if (f != Totales.GetLength(0) - 1)
                        Totales[Totales.GetLength(0) - 1, c] += Totales[f, c];
                    if (c != Totales.GetLength(1) - 1)
                        Totales[f, Totales.GetLength(1) - 1] += Totales[f, c];
                }

            }
            Totales[Totales.GetLength(0) - 1, Totales.GetLength(1) - 1] /= 2;




            return Totales;
        }



        public List<Deposits> GetDepositsLists(string Datee)
        {
            var CurrencyRate = _CurrencyRepository.GetCurrencyRateByDate(Datee);
            var listDeposits = _context.SEND_DEPOSITS.AsEnumerable().Where(c => String.Format("{0:dd/MM/yyyy}", c.date) == Datee && c.status == true).ToList();
            Deposits dposit;
            var listdposit = new List<Deposits>();
            foreach (SEND_DEPOSITS depos in listDeposits)
            {
                dposit = new Deposits();
                dposit.Currency = depos.cur_code;
                dposit.Destino = depos.origin;

                dposit.Type = depos.SEND_DEPOSITS_TYPE.description;
                if (depos.cur_code == Currencies.USD)
                {
                    dposit.ImporteDLLS = depos.amount;
                    dposit.Importe_DLLS_MXN = depos.amount * CurrencyRate;//multiplicar por el tipo de cambio
                    dposit.Complement = 0;
                }
                else
                {
                    dposit.Importe = depos.amount;
                    dposit.ImporteDLLS = 0;
                    dposit.Importe_DLLS_MXN = 0;
                }
                listdposit.Add(dposit);
            }
            if (listDeposits != null)
                return listdposit;
            else
                return new List<Deposits>();
        }
        public List<PAYMENTS> GetPayments(string Datee)
        {
            var Pays = new List<PAYMENTS>();
            Pays = _context.PAYMENTS.AsEnumerable().Where(p => String.Format("{0:dd/MM/yyyy}", p.payment_date) == Datee && p.payment_status==true).ToList();

            return Pays;

        }
        public List<CreditNotesBon> GetCreditNotesBoniList(string Datee)
        {
            DateTime DateStart = DateTime.ParseExact(Datee, "dd/MM/yyyy", CultureInfo.InvariantCulture);
            //var cnbl = _context.CREDIT_NOTES.AsEnumerable().Where(c => String.Format("{0:dd/MM/yyyy}", c.cn_date) == Datee).ToList();
            var cnbl1 = (from cn in _context.CREDIT_NOTES
                         join inv in _context.INVOICES on cn.invoice_no equals inv.invoice_no
                         where DbFunctions.TruncateTime(cn.cn_date) == DateStart && cn.cn_status == true && cn.sale_id == null
                         select new CreditNotesBon
                         {
                             Currency = cn.cur_code,
                             Type = cn.CREDIT_NOTE_DETAIL.Where(s => s.cn_id == cn.cn_id).Select(s => s.part_number).FirstOrDefault() == "0" ? "BONIFICACION" : "NOTA DE CREDITO",
                             Total = cn.cn_total,
                             Iscredit = inv.is_credit
                         }).ToList();
            var cnblSls = (from cn in _context.CREDIT_NOTES
                         join slsPM in _context.DFLPOS_SALES_PAYMENT_METHOD on cn.sale_id equals slsPM.sale_id
                         where DbFunctions.TruncateTime(cn.cn_date) == DateStart && cn.cn_status == true && cn.sale_id!=null
                          select  new CreditNotesBon
                          {
                              Currency = cn.cur_code,
                              Type = cn.CREDIT_NOTE_DETAIL.Where(s => s.cn_id == cn.cn_id).Select(s => s.part_number).FirstOrDefault() == "0" ? "BONIFICACION" : "NOTA DE CREDITO",
                              Total = cn.cn_total,
                              Iscredit = slsPM.pm_id == -1 ? true : false,
                              cnId=cn.cn_id
                          }).Distinct().ToList();
            var _group = (from _cnbL in cnblSls
                          group _cnbL by _cnbL.Total into g
                          select new CreditNotesBon
                          {
                              Currency = g.FirstOrDefault().Currency,
                              Type = g.FirstOrDefault().Type,
                              Total = g.FirstOrDefault().Total,
                              Iscredit = g.FirstOrDefault().Iscredit,
                              cnId=g.FirstOrDefault().cnId
                          });

           // if (_group != null)
                cnbl1.AddRange(cnblSls);

            //var listIeps = new List<IepsModel>();
            //CreditNotesBon cnb;
            //List<CreditNotesBon> creditnotesBonList = new List<CreditNotesBon>();
            //if (cnbl.Count > 0)
            //{
            //    foreach (CREDIT_NOTES crnb in cnbl)
            //    {
            //        if (crnb.cn_status)
            //        {
            //            cnb = new CreditNotesBon();
            //            cnb.Currency = crnb.cur_code;
            //            cnb.Type = crnb.CREDIT_NOTE_DETAIL.Where(s => s.cn_id == crnb.cn_id).Select(s => s.part_number).FirstOrDefault() == "0" ? "BONIFICACION" : "NOTA DE CREDITO";
            //            cnb.Total = crnb.cn_total;
            //            creditnotesBonList.Add(cnb);

            //        }
            //    }
            //}
            if (cnbl1 != null)
                return cnbl1;
            else
                return new List<CreditNotesBon>();
        }
        public List<SanctionsObj> GetSantionsList(string Datee)
        {
            DateTime DateStart = DateTime.ParseExact(Datee, "dd/MM/yyyy", CultureInfo.InvariantCulture);
            SanctionsObj Sanction;
            var SanctionsList = new List<SanctionsObj>();
            var SANCLIST = _context.SANCTIONS.Where(S => S.sanction_date.Value == DateStart && S.missing_vouchers != 0 && S.status==1).ToList();
            foreach (SANCTIONS sanc in SANCLIST)
            {
                Sanction = new SanctionsObj();
                Sanction.Empleado = sanc.USER_MASTER.first_name.ToUpper() + " " + sanc.USER_MASTER.last_name.ToUpper();
                Sanction.Importe = sanc.sanction_total.Value;
                Sanction.Motivo = sanc.SANCTIONS_MOTIVES.desc;
                SanctionsList.Add(Sanction);
            }
            return SanctionsList;

        }

        public List<Cancellations> GetCancellations(string Datee)
        {
            DateTime DateStart = DateTime.ParseExact(Datee, "dd/MM/yyyy", CultureInfo.InvariantCulture);
            var _Cancellations = (from can in _context.DFLPOS_CANCELLATION
                                  join sal in _context.DFLPOS_SALES on can.sale_id equals sal.sale_id
                                  join salpm in _context.DFLPOS_SALES_PAYMENT_METHOD on sal.sale_id equals salpm.sale_id
                                  where can.cancellation_date == DateStart
                                  group can by can.cancellation_id into g
                                  select new Cancellations
                                  {
                                      Type = g.FirstOrDefault().DFLPOS_SALES.DFLPOS_SALES_PAYMENT_METHOD.Where(w=> w.amount<0).FirstOrDefault().pm_id == PaymentMethods.MXN_CASH ? "C" : g.FirstOrDefault().DFLPOS_SALES.DFLPOS_SALES_PAYMENT_METHOD.Where(w => w.amount < 0).FirstOrDefault().pm_id == PaymentMethods.USD_CASH ? "C" : "E",
                                      Total = g.FirstOrDefault().cancellation_total.Value,
                                      CardC = g.FirstOrDefault().DFLPOS_SALES.DFLPOS_SALES_PAYMENT_METHOD.FirstOrDefault().pm_id == 1 ? true : false,
                                      CardD = g.FirstOrDefault().DFLPOS_SALES.DFLPOS_SALES_PAYMENT_METHOD.FirstOrDefault().pm_id == 19? true:false
                                  }).ToList();
            if (_Cancellations != null)
                return _Cancellations;
            else
                return new List<Cancellations>();

        }
        public List<PaymentMethod> GetPaymentMethods(string Datee)
        {
            DateTime DateStart = DateTime.ParseExact(Datee, "dd/MM/yyyy", CultureInfo.InvariantCulture);


            return _context.Database.SqlQuery<PaymentMethod>("select pmList.PMid, pmList.PaymentName, pmList.Typev, sum(pmList.Importe) Importe from(" +
                "select  TOP (100) PERCENT pm.pm_id PMid,pm.pm_name PaymentName , pm.typev Typev, CASE " +
                                                                    " WHEN pm.pm_id = 1 OR pm.typev = 'E' OR pm.pm_id = 19" +
                                                                    "  THEN" +
                                                                    " ( case WHEN  slsp.amount_entered = 0" +
                                                                     " then isnull(sum(slsp.amount_entered), 0)" +
                                                                     " ELSE" +
                                                                     " isnull(sum(slsp.amount_entered), 0)" +
                                                                     " end)" +
                                                                   " ELSE isnull(sum(slsp.amount_entered), 0)" +
                                                              " END as Importe      " +
                                                                        "from DFLPOS_SALES sls join DFLPOS_SALES_PAYMENT_METHOD slsP on slsP.sale_id =sls.sale_id " +
                                                                        "and  sls.sale_date= @datee" +
                                                                       " right join PAYMENT_METHOD pm on pm.pm_id= slsP.pm_id" +
                                                                       " where pm.pm_id <12 or pm.pm_id>13" +
                                                                       " group by pm.pm_name,pm.typev,pm.pm_id ,slsp.amount_entered" +
                                                                       " union all " +
                                                                        "SELECT TOP (100) PERCENT pm.pm_id PMid, pm.pm_name PaymentName, pm.typev Typev, ISNULL(sum(vc.amount), 0) Importe" +
                                                                        " FROM VOUCHER_CLASSIFICATION vc" +
                                                                         " right join PAYMENT_METHOD pm on pm.pm_id = vc.pm_id" +
                                                                         " and vc.date= @datee" +
                                                                          " where pm.pm_id between 12 and 13" +
                                                                          " group by pm.pm_name, pm.typev, pm.pm_id" +
                                                                          " order by pm.pm_id ) as pmList" +
                                                                          "  group by pmList.PMid, pmList.PaymentName, pmList.Typev" +
                                                                          " order by pmList.PMid ", new SqlParameter("@datee", DateStart)).ToList();


        }
        public List<SaleDiscounts> GetDiscountsList(string date)
        {
            DateTime DateStart = DateTime.ParseExact(date, "dd/MM/yyyy", CultureInfo.InvariantCulture);


            /*.
             * (from sal in _context.DFLPOS_SALES
                              join salpm in _context.DFLPOS_SALES_PAYMENT_METHOD on sal.sale_id equals salpm.sale_id
                              where sal.sale_date == DateStart && salpm.pm_id == 18
                              select new SaleDiscounts
                              {
                                  Customername = "",
                                  Importe = salpm.amount

                              }).ToList();
             */

            var _Discounts = _context.Database.SqlQuery<SaleDiscounts>("sp_SalesDiscountsByCustomers @date,@date2", new SqlParameter("date", DateStart), new SqlParameter("date2", DateStart)).ToList();

            if (_Discounts != null)
                return _Discounts;
            else
                return new List<SaleDiscounts>();
        }

        public decimal GetCashBack(string date)
        {
            DateTime DateStart = DateTime.ParseExact(date, "dd/MM/yyyy", CultureInfo.InvariantCulture);

            var cashBack = _context.DFLPOS_TRANSACTIONS_CARD.Where(TC => TC.DFLPOS_SALES.sale_date == DateStart && TC.responseCode == "00" && TC.cash_back > 0).Sum(S => S.cash_back);
            if (cashBack != null)
                return cashBack.Value;
            else
                return 0;
        }
        public decimal GetCanceletedMountFromTarjetas(string date1, string site_code)
        {
            DateTime DateStart = DateTime.ParseExact(date1, "dd/MM/yyyy", CultureInfo.InvariantCulture);
            var _date = DateStart.ToString("yyyy/MM/dd");
            var result= _context.Database.SqlQuery<decimal>("SELECT isnull (sum(monto),0) monto " +
                "FROM[TARJETAS].[TARJETA].[dbo].[Historial] H " +
                "where fecha between '"+ _date + " 00:00:00' and  '"+_date+" 23:59:59' and site_code = '"+site_code+"' " +
                "and historial_tipo = 8").ToList();
            if (result != null)
                return result.IndexOf(0);
            else return 0;

        }
        public List<ServicesDetail> GetServiceDetailList(string date)
        {
            DateTime DateStart = DateTime.ParseExact(date, "dd/MM/yyyy", CultureInfo.InvariantCulture);

            return _context.Database.SqlQuery<ServicesDetail>("select  it.part_description ServiceName, " +
                                                                        "isnull(SUM(case when ss.cur_code = 'USD' then	ss.part_price*ss.currency_rate else ss.part_price end),0) Importe " +
                                                                        "from DFLPOS_SALES_DETAIL_SERVICES ss " +
                                                                         "join ITEM it on it.part_number = ss.part_number " +
                                                                         "and CONVERT(VARCHAR(10), ss.trans_date, 103)= @datee" +
                                                                      " group by it.part_description ", new SqlParameter("@datee", date)).ToList();

        }
        public List<Rent> GetRentList(string date)
        {
            DateTime DateStart = DateTime.ParseExact(date, "dd/MM/yyyy", CultureInfo.InvariantCulture);




            var _Rent = (from invd in _context.INVOICE_DETAIL
                         join inv in _context.INVOICES on new { invd.invoice_no, invd.invoice_serie } equals new { inv.invoice_no, inv.invoice_serie }
                         where inv.invoice_date == DateStart && invd.part_number == "5555-R"
                         select new Rent
                         {
                             Impporte = inv.invoice_total,
                             Currency = inv.cur_code,
                             Tax = inv.invoice_tax


                         }).ToList();
            if (_Rent != null)
                return _Rent;
            else
                return new List<Rent>();
        }

        public List<DonationData> GetDonations(string date)
        {
            DateTime DateStart = DateTime.ParseExact(date, "dd/MM/yyyy", CultureInfo.InvariantCulture);

            var ReportList = new List<DonationData>();

            var DataInfo = (from d in _context.DFLPOS_DONATIONS
                            join dpm in _context.DFLPOS_DONATIONS_PAYMENT_METHOD on d.donation_id equals dpm.donation_id
                            where DbFunctions.TruncateTime(dpm.sale_date) == DateStart

                            select new
                            {

                                AmountMXN = dpm.PAYMENT_METHOD.currency == Currencies.MXN ? dpm.amount : 0,
                                AmounUSD = dpm.PAYMENT_METHOD.currency == Currencies.USD ? dpm.amount : 0,

                            }).ToList();

            if (DataInfo.Count > 0)
            {
                ReportList = (from d in DataInfo
                              select new DonationData
                              {

                                  AmountMXN = d.AmountMXN.Value,
                                  AmountUSD = d.AmounUSD.Value

                              }).ToList();
            }
            return ReportList;
        }
    }
}
