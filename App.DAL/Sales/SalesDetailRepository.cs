﻿using App.DAL.Item;
using App.Entities;
using App.Entities.ViewModels;
using App.Entities.ViewModels.Invoices;
using App.Entities.ViewModels.Order;
using System;
using System.Collections.Generic;
using System.Linq;
using static App.Entities.ViewModels.Common.Constants;

namespace App.DAL.Sales
{
    public class SalesDetailRepository
    {
        private DFL_SAIEntities _context;
        private ItemRepository Item;
        public SalesDetailRepository()
        {
            _context = new DFL_SAIEntities();
            Item = new ItemRepository();
        }

        public List<DFLPOS_SALES_DETAIL> getAll()
        {
            return _context.DFLPOS_SALES_DETAIL.ToList();
        }
        public List<DFLPOS_SALES_DETAIL> getSalesDetailBySaleId(long SaleId)
        {
            var _listSalesDetail = _context.DFLPOS_SALES_DETAIL.Where(sd => sd.sale_id == SaleId).ToList();
            if (_listSalesDetail != null)
                return _listSalesDetail;
            else return new List<DFLPOS_SALES_DETAIL>().ToList();

        }
        public List<OrderListModel> getListSales(int Supplier, string Site)
        {
            var Order = _context.DFLPOS_SALES_DETAIL.OrderBy(x => x.part_number).ToList()
                      .Join(_context.ITEM_SUPPLIER, x => x.part_number, y => y.part_number, (x, y) => new { x, y })
                      .Join(_context.ITEM_SPECS, p => p.x.part_number, r => r.part_number, (p, r) => new { p, r })
                      .Where(s => s.p.x.site_code == Site && s.p.y.supplier_id == Supplier
                        && s.p.x.trans_date.Date <= DateTime.Now.Date
                        && s.p.x.trans_date.Date >= DateTime.Now.Date.AddDays(-49)
                        && s.p.x.part_number == s.p.y.part_number).Select(s => new OrderListModel
                        {
                            ParthNumber = s.p.x.part_number,
                            Description = Item.ParthNumber(s.p.x.part_number),
                            BarCode = s.p.x.barcode,
                            Id = s.p.x.detail_id,
                            Packing = s.r.packing_of_size != null ? s.r.packing_of_size.Value : 0//.ToString("N0"):"0"
                        }).ToList();
            List<OrderListModel> OrderList = new List<OrderListModel>();
            string CodeNumber = "";
            foreach (var item in Order)
            {
                if (item.ParthNumber != CodeNumber)
                {
                    CodeNumber = item.ParthNumber;
                    var AverageSales = _context.DFLPOS_SALES_DETAIL
                       .ToList()
                       .Join(_context.ITEM_SUPPLIER, x => x.part_number, y => y.part_number, (x, y) => new { x, y })
                       .Where(s => s.x.site_code == Site
                       && s.y.supplier_id == Supplier
                       && s.x.trans_date.Date <= DateTime.Now.Date
                       && s.x.trans_date.Date >= DateTime.Now.Date.AddDays(-49)
                       && s.x.part_number == item.ParthNumber);

                    var Stock = _context.ITEM_VALUATION
                            .Where(x => x.part_number == item.ParthNumber && x.site_code == Site)
                            .SingleOrDefault();

                    var Prepurchase = _context.PURCHASE_ORDER_ITEM
                            .Join(_context.PURCHASE_ORDER, l => l.purchase_no, r => r.purchase_no, (l, r) => new { l, r })
                            .ToList()
                            .Where(x => x.l.part_number == item.ParthNumber
                            && x.r.purchase_status == 1
                            && x.r.purchase_date <= DateTime.Now.Date
                            && x.r.purchase_date <= DateTime.Now.AddDays(-30).Date
                            && x.r.site_code == Site);
                    var StockValue = new ITEM_VALUATION();
                    if (Stock != null)
                        StockValue = Stock;
                    else
                        StockValue.total_stock = 0;
                    OrderList.Add(
                    new OrderListModel
                    {
                        ParthNumber = item.ParthNumber,

                        AverageSale = AverageSales != null ? int.Parse((AverageSales.Sum(x => x.x.quantity) / 49).ToString("N0")) : 0,

                        Stock = StockValue.total_stock != null ? int.Parse((StockValue.total_stock.Value).ToString("N0")) : 0,

                        PrePruchase = Prepurchase != null ? int.Parse((Prepurchase.Sum(x => x.l.quantity).Value).ToString("N0")) : 0,

                        SuggestedSystems = 0,
                        Packing = item.Packing,
                        Id = item.Id,
                        Description = item.Description,
                        BarCode = item.BarCode,

                    });
                }

            }

            return OrderList;
        }

        public List<DetailTax> SalesDetailsWithTax(string InvoSerie, long InvoNumber, long GlobalDetailId)
        {
            var List = new List<DetailTax>();

            List = (from sd in _context.DFLPOS_SALES_DETAIL
                    join s in _context.DFLPOS_SALES on sd.sale_id equals s.sale_id
                    join i in _context.ITEM on sd.part_number equals i.part_number
                    where s.invoice_no == InvoNumber
                          && s.invoice_serie == InvoSerie
                          && i.part_iva_sale != TaxCodes.EXE_0
                          && sd.part_price > 0
                          && !(from c in _context.COMBO_HEADER select c.part_number_combo).Contains(sd.part_number)
                    select new DetailTax
                    {
                        DetailId = GlobalDetailId,
                        IEPSCode = i.part_ieps,
                        IEPSValue = i.MA_TAX_IEPS.tax_value,
                        IVACode = i.part_iva_sale,
                        IVAValue = sd.part_tax_percentage.Value,
                        ItemPrice = sd.part_price,
                        TaxAmount = sd.part_tax
                    }).ToList();

            var ComboList = (from sd in _context.DFLPOS_SALES_DETAIL
                             join cd in _context.COMBO_DETAIL on sd.part_number equals cd.part_number_combo
                             where (from s in _context.DFLPOS_SALES where s.invoice_no == InvoNumber select s.sale_id).Contains(sd.sale_id)
                                   && cd.ITEM.part_iva_sale != TaxCodes.EXE_0
                             select new
                             {
                                 DetailId = GlobalDetailId,
                                 IEPSCode = cd.ITEM.part_ieps,
                                 IEPSValue = cd.ITEM.MA_TAX_IEPS.tax_value,
                                 IVACode = cd.iva > 0 ? TaxCodes.TAX_8 : cd.ITEM.part_iva_sale,
                                 IVAValue = cd.iva > 0 ? sd.part_tax_percentage : cd.ITEM.MA_TAX_IVA_SALE.tax_value,
                                 ItemPrice = cd.iva > 0 ? ((cd.total_price_sale * sd.quantity) / 1.08m) : cd.total_price_sale * sd.quantity,
                                 cd.iva
                             }).AsEnumerable().Select(c => new DetailTax
                             {
                                 DetailId = c.DetailId,
                                 IEPSCode = c.IEPSCode,
                                 IEPSValue = c.IEPSValue,
                                 IVACode = c.IVACode,
                                 IVAValue = c.IVAValue,
                                 ItemPrice = c.ItemPrice.Value,
                                 TaxAmount = c.iva > 0 ? Math.Round(c.ItemPrice.Value * 0.08m, 2) : 0
                             }).ToList();

            if (ComboList.Count > 0)
                List.AddRange(ComboList);

            if (List.Count > 0)
                return List;
            else
                return new List<DetailTax>();
        }

        public List<InvoiceProductsDetails> InvoiceProductsList(long SaleNumber)
        {
            var Details = ProductsList(SaleNumber);
            return Details;
        }

        public DFLPOS_SALES_DETAIL GetSaleDetailById(long SaleDetailId)
        {
            return _context.DFLPOS_SALES_DETAIL.Find(SaleDetailId);
        }

        public bool HasSaleItemsAvailable(long SaleNumber)
        {
            var Details = ProductsList(SaleNumber);
            bool HasItems = Details.Any(c => c.QuantityAvailable > 0);
            return HasItems;
        }

        private List<InvoiceProductsDetails> ProductsList(long SaleNumber)
        {
            var QuantityData = (from cns in _context.CREDIT_NOTES_SALES
                                where cns.sale_id == SaleNumber
                                      && cns.CREDIT_NOTES.cn_status
                                group cns by cns.sale_detail_id into g
                                select new
                                {
                                    DetailId = g.Key,
                                    Quantity = g.Sum(c => c.detail_qty)
                                }).ToList();

            var Details = (from sd in _context.DFLPOS_SALES_DETAIL
                           join i in _context.ITEM on sd.part_number equals i.part_number
                           join combHe in _context.COMBO_HEADER on sd.part_number equals combHe.part_number_combo into LeftJoinComboHeader
                           from combH in LeftJoinComboHeader.DefaultIfEmpty()
                           join combDe in _context.COMBO_DETAIL on combH.part_number_combo equals combDe.part_number_combo into LeftJoinComboDetail
                           from combD in LeftJoinComboDetail.Where(combD => combD.active_status.HasValue && combD.active_status.Value).DefaultIfEmpty()
                           where sd.sale_id == SaleNumber
                           select new
                           {
                               DetailId = sd.detail_id,
                               SalePartNumber = sd.part_number,
                               ItemNumber = string.IsNullOrEmpty(combD.part_number) ? sd.part_number : combD.part_number,
                               ItemName = string.IsNullOrEmpty(combD.part_number) ? i.part_description : combD.ITEM.part_description,
                               Quantity = string.IsNullOrEmpty(combD.part_number) ? sd.quantity : combD.quantity.Value * sd.quantity,
                               ItemPrice = string.IsNullOrEmpty(combD.part_number) ? (sd.part_price > 0 ? sd.part_price / sd.quantity : sd.part_original_price) : ((combD.total_price_sale.Value - combD.iva.Value) / combD.quantity.Value) * sd.quantity,
                               TotalPrice = string.IsNullOrEmpty(combD.part_number) ? (sd.part_price > 0 ? sd.part_price : sd.part_original_price * sd.original_quantity) : (((combD.total_price_sale.Value - combD.iva.Value) / combD.quantity.Value) * sd.quantity) * combD.quantity.Value,
                               IsWeightAvailable = string.IsNullOrEmpty(combD.part_number) ? ((i.weight_flag.HasValue && i.weight_flag.Value) ? 1 : 0) : ((combD.ITEM.weight_flag.HasValue && combD.ITEM.weight_flag.Value) ? 1 : 0),
                               IEPSCode = string.IsNullOrEmpty(combD.part_number) ? (!string.IsNullOrEmpty(i.part_ieps) && i.part_ieps != TaxCodes.NOT_AVAILABLE ? i.part_ieps : string.Empty) : (!string.IsNullOrEmpty(combD.ITEM.part_ieps) && combD.ITEM.part_ieps != TaxCodes.NOT_AVAILABLE ? combD.ITEM.part_ieps : string.Empty),
                               IVACode = string.IsNullOrEmpty(combD.part_number) ? (!string.IsNullOrEmpty(i.part_iva_sale) && i.part_iva_sale != TaxCodes.NOT_AVAILABLE ? i.part_iva_sale : string.Empty) : (!string.IsNullOrEmpty(combD.ITEM.part_iva_sale) && combD.ITEM.part_iva_sale != TaxCodes.NOT_AVAILABLE ? combD.ITEM.part_iva_sale : string.Empty),
                               ItemTax = string.IsNullOrEmpty(combD.part_number) ? (sd.quantity > 0 ? sd.part_tax / sd.quantity : sd.part_tax / sd.original_quantity) : (combD.iva.Value / combD.quantity.Value) * sd.quantity,
                               IVAValue = string.IsNullOrEmpty(combD.part_number) ? (!string.IsNullOrEmpty(i.part_iva_sale) && i.part_iva_sale != TaxCodes.NOT_AVAILABLE ? i.MA_TAX_IVA_SALE.tax_value.Value : 0) : (!string.IsNullOrEmpty(combD.ITEM.part_iva_sale) && combD.ITEM.part_iva_sale != TaxCodes.NOT_AVAILABLE ? combD.ITEM.MA_TAX_IVA_SALE.tax_value.Value : 0),
                               IEPSValue = string.IsNullOrEmpty(combD.part_number) ? (!string.IsNullOrEmpty(i.part_ieps) && i.part_ieps != TaxCodes.NOT_AVAILABLE ? i.MA_TAX_IEPS.tax_value.Value : 0) : (!string.IsNullOrEmpty(combD.ITEM.part_ieps) && combD.ITEM.part_ieps != TaxCodes.NOT_AVAILABLE ? combD.ITEM.MA_TAX_IEPS.tax_value.Value : 0),
                               BaseIEPS = string.IsNullOrEmpty(combD.part_number) ? (!string.IsNullOrEmpty(i.part_ieps) && i.part_ieps != TaxCodes.NOT_AVAILABLE ? (sd.part_original_price / (1 + (i.MA_TAX_IEPS.tax_value / 100))) : 0) : (!string.IsNullOrEmpty(combD.ITEM.part_ieps) && combD.ITEM.part_ieps != TaxCodes.NOT_AVAILABLE ? (combD.total_price_sale / (1 + (i.MA_TAX_IEPS.tax_value / 100))) : 0)
                           }).AsEnumerable().Select(c => new InvoiceProductsDetails
                           {
                               DetailId = c.DetailId,
                               SalePartNumber = c.SalePartNumber,
                               ItemNumber = c.ItemNumber,
                               ItemName = c.ItemName,
                               Quantity = c.Quantity,
                               QuantityBon = c.Quantity.ToString().Substring(0, c.Quantity.ToString().IndexOf(".") + 4),
                               ItemPrice = Math.Round(c.ItemPrice + c.ItemTax, 3),
                               TotalPrice = 0,
                               ItemPriceNoTax = Math.Round(c.ItemPrice, 2),
                               IsWeightAvailable = c.IsWeightAvailable,
                               QuantityAvailable = QuantityData.Count > 0 && QuantityData.Where(x => x.DetailId == c.DetailId).Any() ? c.Quantity - QuantityData.First(d => d.DetailId == c.DetailId).Quantity : c.Quantity,
                               QuantityAvailableBon = QuantityData.Count > 0 && QuantityData.Where(x => x.DetailId == c.DetailId).Any() ? (c.Quantity - QuantityData.First(d => d.DetailId == c.DetailId).Quantity).ToString() : c.Quantity.ToString(),
                               IEPSCode = c.IEPSCode,
                               IVACode = c.IVACode,
                               IVAValue = c.IVAValue,
                               IEPSValue = c.IEPSValue,
                               ItemTax = c.ItemTax,
                               ItemIEPS = Math.Round(c.BaseIEPS.Value * (c.IEPSValue / 100), 2)
                           }).ToList();

            if (Details.Count == 0 || Details == null)
                Details = new List<InvoiceProductsDetails>();

            Details.ForEach(c => c.ItemPrice = Math.Round(c.ItemPrice, 2));
            Details.ForEach(c => c.TotalPrice = Math.Round(c.ItemPrice * c.Quantity, 2));
            Details.ForEach(c => c.QuantityAvailableBon = c.QuantityAvailableBon.Substring(0, c.QuantityAvailableBon.IndexOf(".") + 4));
            return Details;
        }
    }
}