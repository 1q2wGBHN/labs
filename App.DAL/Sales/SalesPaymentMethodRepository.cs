﻿using App.Entities;
using App.Entities.ViewModels.Sales;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.SqlClient;
using System.Globalization;
using System.Linq;
using static App.Entities.ViewModels.Common.Constants;

namespace App.DAL.Sales
{
    public class SalesPaymentMethodRepository
    {
        private DFL_SAIEntities _context;

        public SalesPaymentMethodRepository()
        {
            _context = new DFL_SAIEntities();
        }

        public int GetPaymentMethodBySaleId(long SaleId)
        {
            decimal CurrencyRate = 0, UsdToMxn = 0;                        
            var pms = (from s in _context.DFLPOS_SALES_PAYMENT_METHOD
                       where s.sale_id == SaleId
                       select new {
                           PM = s.pm_id,
                           Amount = s.amount
                       }).ToList(); 
            if (pms.Count > 1)
            {
                if (pms.Where(x => x.PM == PaymentMethods.USD_CASH).Any())
                {
                    CurrencyRate = _context.DFLPOS_SALES_DETAIL.Where(c => c.sale_id == SaleId).Select(d => d.currency_rate).FirstOrDefault();
                    UsdToMxn = CurrencyRate * pms.Where(x => x.PM == PaymentMethods.USD_CASH).Select(c => c.Amount).SingleOrDefault();
                }

                var Highest = (from p in pms select p).Where(c => c.PM != PaymentMethods.USD_CASH).OrderByDescending(c => c.Amount).FirstOrDefault();

                if (Highest.Amount > UsdToMxn)
                    return Highest.PM;
                else
                    return PaymentMethods.USD_CASH;
            }
            return pms.Select(c => c.PM).FirstOrDefault();
        }

        public bool UpdatePaymentMethodsWithInvoiceData(string InvoiceSerie, long InvoiceNumber)
        {
            try
            {
                var update = _context.Database.ExecuteSqlCommand("UPDATE spm SET spm.invoice_serie = @InvoiceSerie,spm.invoice_number = @InvoiceNumber FROM DFLPOS_SALES s JOIN DFLPOS_SALES_PAYMENT_METHOD spm ON spm.sale_id = s.sale_Id WHERE s.invoice_no = @InvoiceNumber",
                 new SqlParameter("InvoiceSerie", InvoiceSerie),
                 new SqlParameter("InvoiceNumber", InvoiceNumber));

                return true;
            }
            catch (Exception ex)
            {
                string msg = ex.Message;
                return false;
            }
        }

        public bool RollbackUpdatePaymentMethods(string InvoiceSerie, long InvoiceNumber)
        {
            try
            {
                var update = _context.Database.ExecuteSqlCommand("UPDATE spm SET spm.invoice_serie = NULL,spm.invoice_number = NULL FROM DFLPOS_SALES s JOIN DFLPOS_SALES_PAYMENT_METHOD spm ON spm.sale_id = s.sale_Id WHERE s.invoice_no = @InvoiceNumber AND s.invoice_serie = @InvoiceSerie",
                 new SqlParameter("InvoiceSerie", InvoiceSerie),
                 new SqlParameter("InvoiceNumber", InvoiceNumber));

                return true;
            }
            catch (Exception ex)
            {
                string msg = ex.Message;
                return false;
            }
        }

        public List<DFLPOS_SALES_PAYMENT_METHOD> GetPaymentMethodList(string InvoSerie, long InvoNumber)
        {
            var SalesMethodPaymentList = new List<DFLPOS_SALES_PAYMENT_METHOD>();
            SalesMethodPaymentList = (from s in _context.DFLPOS_SALES
                                      join spm in _context.DFLPOS_SALES_PAYMENT_METHOD on s.sale_id equals spm.sale_id
                                      where s.invoice_serie == InvoSerie
                                            && s.invoice_no == InvoNumber
                                      select spm).ToList();

            if (SalesMethodPaymentList.Count > 0)
                return SalesMethodPaymentList;
            else
                return new List<DFLPOS_SALES_PAYMENT_METHOD>();
        }

        public List<ChargeSalesRelationInfo> GetChargeRelationSalesData(ChargeRelationSalesIndex Model)
        {
            var ReportList = new List<ChargeSalesRelationInfo>();

            DateTime StarDate = DateTime.ParseExact(Model.Date, DateFormat.INT_DATE, CultureInfo.InvariantCulture);
            DateTime EndDate = DateTime.ParseExact(Model.EndDate, DateFormat.INT_DATE, CultureInfo.InvariantCulture);

            ReportList = (from spm in _context.DFLPOS_SALES_PAYMENT_METHOD
                          join s in _context.DFLPOS_SALES on spm.sale_id equals s.sale_id
                          join u in _context.USER_MASTER on spm.emp_no equals u.emp_no
                          join pm in _context.PAYMENT_METHOD on spm.pm_id equals pm.pm_id
                          where s.sale_date >= StarDate
                                && s.sale_date <= EndDate
                                && pm.pm_menu
                                && (string.IsNullOrEmpty(Model.Cashier) || spm.emp_no == Model.Cashier)
                                && (!Model.PaymentMethod.HasValue || spm.pm_id == Model.PaymentMethod.Value)
                          select new { spm, u, s } into t1
                          group t1 by new { t1.spm.emp_no, t1.spm.PAYMENT_METHOD, t1.u.first_name, t1.u.last_name, t1.spm.PAYMENT_METHOD.currency, t1.s.sale_date } into gp
                          select new ChargeSalesRelationInfo
                          {
                              Cashier = gp.Key.first_name + " " + gp.Key.last_name,
                              EmployeeNo = gp.Key.emp_no,
                              PaymentType = gp.Key.PAYMENT_METHOD.pm_name,
                              Currency = gp.Key.currency,
                              Amount = gp.Sum(x => x.spm.amount),
                              Date = gp.Key.sale_date
                          }).ToList();

            return ReportList;
        }
    }
}
