﻿using App.Entities;
using App.Entities.ViewModels.Sales;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Globalization;
using System.Linq;

namespace App.DAL.Sales
{
    public class SalesCancellationRepository
    {
        private DFL_SAIEntities _context;

        public SalesCancellationRepository()
        {
            _context = new DFL_SAIEntities();
        }

        public List<SalesCancellationData> GetSalesCancellationReportData(SalesReportIndex Model)
        {
            DateTime Date = DateTime.ParseExact(Model.Date, "dd/MM/yyyy", CultureInfo.InvariantCulture);
            var ReportList = new List<SalesCancellationData>();
            ReportList = (from c in _context.DFLPOS_CANCELLATION
                          join cd in _context.DFLPOS_CANCELLATION_DETAIL on c.cancellation_id equals cd.cancellation_id
                          //join spm in _context.DFLPOS_SALES_PAYMENT_METHOD on c.sale_id equals spm.sale_id
                          where DbFunctions.TruncateTime(c.cancellation_date.Value) == Date
                                && (string.IsNullOrEmpty(Model.Cashier) || c.cashier_id == Model.Cashier)
                          select new SalesCancellationData
                          {
                              SaleNumber = c.sale_id.Value,
                              CancellationNumber = c.cancellation_id,
                              ItemDescription = cd.ITEM.part_description,
                              Sales = cd.DFLPOS_SALES_DETAIL.quantity,
                              SalesCancelated = cd.cancellation_quantity.Value,
                              UnitPrice = cd.DFLPOS_SALES_DETAIL.part_original_price,
                              SubTotal = cd.cancellation_amount.Value,
                              Tax = cd.cancellation_iva.Value,
                              Total = cd.cancellation_amount.Value + cd.cancellation_iva.Value,
                              Cashier = c.USER_MASTER.first_name + " " + c.USER_MASTER.last_name,
                              Supervisor = c.USER_MASTER1.first_name + " " + c.USER_MASTER1.last_name,
                              Motive = c.DFLPOS_TYPE_CANCELLATION.description,
                              IsCredit = c.DFLPOS_SALES.DFLPOS_SALES_PAYMENT_METHOD.Any(d => d.pm_id == -1) ? true : false                             
                          }).ToList();
            return ReportList;
        }

        public List<CancellationResume> GetCancellationsResumen(CancellationIndex model)
        {
            DateTime Date1 = DateTime.ParseExact(model.Date, "dd/MM/yyyy", CultureInfo.InvariantCulture);
            DateTime Date2 = DateTime.ParseExact(model.Date2, "dd/MM/yyyy", CultureInfo.InvariantCulture);
            var Report = new List<CancellationResume>();

            Report = (from c in _context.DFLPOS_CANCELLATION
                      where DbFunctions.TruncateTime(c.cancellation_date.Value) >= Date1 && DbFunctions.TruncateTime(c.cancellation_date.Value) <= Date2
                      group c by c.cashier_id into g
                      select new CancellationResume
                      {
                          CashierId=g.FirstOrDefault().cashier_id,
                          Cashier = g.FirstOrDefault().USER_MASTER.first_name + " " + g.FirstOrDefault().USER_MASTER.last_name,
                          Amount = g.Sum(s => s.cancellation_total.Value)+ g.Sum(s => s.cancellation_iva.Value),
                          NumberCancellations =  g.Count()
            
                      }).ToList();
            return Report;
        }
        public List<SalesCancellationData> GetSalesCancellationReportDataByDatesAandCashier(string date1, string date2, string cashierID)
        {
            DateTime Date = DateTime.ParseExact(date1, "dd/MM/yyyy", CultureInfo.InvariantCulture);
            DateTime Date2 = DateTime.ParseExact(date2, "dd/MM/yyyy", CultureInfo.InvariantCulture);

            var ReportList = new List<SalesCancellationData>();
            ReportList = (from c in _context.DFLPOS_CANCELLATION
                           where DbFunctions.TruncateTime(c.cancellation_date.Value) >= Date && DbFunctions.TruncateTime(c.cancellation_date.Value) <= Date2
                                &&  c.cashier_id == cashierID
                          select new SalesCancellationData
                          {
                              SaleNumber = c.sale_id.Value,
                              Total = c.cancellation_total.Value + c.cancellation_iva.Value,
                              Supervisor = c.USER_MASTER1.first_name + " " + c.USER_MASTER1.last_name,
                              Motive = c.DFLPOS_TYPE_CANCELLATION.description,
                             
                          }).Distinct().ToList();
            return ReportList;
        }
    }
}
