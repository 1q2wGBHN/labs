﻿using App.Entities;
using App.Entities.ViewModels.Sales;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App.DAL.Sales
{
    public class ItemSalesRepository
    {
        private readonly DFL_SAIEntities _context;

        public ItemSalesRepository()
        {
            _context = new DFL_SAIEntities();
        }

        public List<ItemSalesModel> GetItemSalesByCashier(DateTime BeginDate, DateTime EndDate, string PartNumber)
        {
            var model = _context.Database.SqlQuery<ItemSalesModel>(@" SELECT
                DFLPOS_SALES.emp_no 'EmpNo', 
	            D.part_number 'PartNumber',
	            ITEM.part_description 'PartDescription',
	            USER_MASTER.first_name + ' ' + USER_MASTER.last_name 'Cashier',
                SUM(D.quantity) 'Quantity',
	            SUM(D.detail_captured * D.quantity) 'Amount',
	            SUM(D.part_tax) 'IVA',
	            SUM(CASE WHEN MA_TAX.tax_value > 0 THEN ROUND(((MA_TAX.tax_value / 100) * (D.part_price / (1 + MA_TAX.tax_value / 100))), 2) ELSE 0 END) 'IEPS',
	            SUM( (D.detail_captured + ROUND( ( ISNULL(D.part_tax,0) / D.quantity), 2)) * D.quantity ) 'Total'
                FROM DFLPOS_SALES_DETAIL D
                INNER JOIN DFLPOS_SALES ON D.sale_id = DFLPOS_SALES.sale_id 
                INNER JOIN ITEM ON D.part_number = ITEM.part_number
                INNER JOIN MA_TAX ON ITEM.part_ieps = MA_TAX.tax_code
                INNER JOIN USER_MASTER ON DFLPOS_SALES.emp_no = USER_MASTER.emp_no
                WHERE DFLPOS_SALES.sale_date BETWEEN @BeginDate AND @EndDate AND D.part_number = @PartNumber
                GROUP BY DFLPOS_SALES.emp_no, USER_MASTER.first_name, USER_MASTER.last_name, D.part_number, ITEM.part_description ", new SqlParameter("@BeginDate", BeginDate), new SqlParameter("@EndDate", EndDate), new SqlParameter("@PartNumber", PartNumber)).ToList();

            //var _salesdETAIL = (from sd in _context.DFLPOS_SALES_DETAIL
            //                    where sd.part_number == PartNumber & sd.trans_date >= BeginDate & sd.trans_date <= EndDate
            //                    select new
            //                    {
            //                        sd.part_number,
            //                        part_name = sd.ITEM.part_description,
            //                        sd.quantity,
            //                        sd.detail_captured,
            //                        sd.part_tax,
            //                        tax_value = sd.ITEM.MA_TAX_IVA_SALE.tax_value.Value,
            //                        ieps_value = sd.ITEM.MA_TAX_IEPS.tax_value.Value,
            //                        sd.part_price,
            //                        PriceNoIeps = sd.part_price - (sd.part_price / (1 + sd.ITEM.MA_TAX_IEPS.tax_value.Value / 100))
            //                    }).AsEnumerable().Select(c => new SalesDetailReportModel
            //                    {
            //                        part_number = c.part_number,
            //                        part_name = c.part_name,
            //                        part_quantity = c.quantity,
            //                        part_price = c.detail_captured,
            //                        part_importe = Math.Round(c.part_price - c.PriceNoIeps, 2),
            //                        part_iva = c.part_tax,
            //                        part_iva_percentage = c.tax_value,
            //                        part_ieps = c.ieps_value != 0 ? Math.Round((c.ieps_value / 100) * (c.part_price / (1 + c.ieps_value / 100)), 2) : 0,
            //                        part_ieps_percentage = c.ieps_value,
            //                        part_total = (c.detail_captured + Math.Round(c.part_tax / c.quantity, 2)) * c.quantity
            //                    }).ToList();

            return model;
        }
    }
}
