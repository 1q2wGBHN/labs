﻿using App.Entities;
using App.Entities.ViewModels.ItemPresentation;
using App.Entities.ViewModels.SalesPriceChange;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Globalization;
using System.Linq;
using DevExpress.DataAccess.Native.Sql;
using App.Entities.ViewModels.Item;
using System.Data.SqlClient;

namespace App.DAL.Sales
{
    public class SalePriceFilter
    {

        public bool unique_code { get; set; }
        public string dateChange { get;  set; }
        public int depto { get;  set; }
        public int family { get;  set; }
        public bool? exist { get;  set; }
        public string search { get;  set; }
        public bool exact { get;  set; }
        public bool? presentation { get; set; }

        public bool? promotions { get; set; }
    }

    public class SalesPriceChangeRepository
    {
        private readonly DFL_SAIEntities _context;

        public SalesPriceChangeRepository()
        {
            _context = new DFL_SAIEntities();
        }

        public List<SalePriceChangeModel> GetAllSalesPricesChangeByDate(DateTime fechaAplicacion)
        {
            return _context.SALES_PRICE_CHANGE
                .Join(_context.ITEM_VALUATION, salePrice => salePrice.part_number, itemValuation => itemValuation.part_number, (salePrice, itemValuation) => new { salePrice, itemValuation })
                .Join(_context.ITEM, spiv => spiv.salePrice.part_number, item => item.part_number, (spiv, item) => new { spiv, item })
                .Join(_context.MA_CLASS, itemn => itemn.item.level_header, ma => ma.class_id, (ma, itemn) => new { ma, itemn })
                .Join(_context.MA_CLASS, itemnn => itemnn.ma.item.level1_code, ma => ma.class_id, (ma, itemn) => new { ma, itemn })
                .Where(x => x.ma.ma.spiv.salePrice.applied_date == fechaAplicacion && x.ma.ma.spiv.salePrice.status == 1)
                .Select(q => new SalePriceChangeModel
                {
                    sales_price_id = q.ma.ma.spiv.salePrice.sales_price_id,
                    part_number = q.ma.ma.spiv.salePrice.part_number,
                    Department = q.ma.itemn.class_name,
                    Family = q.itemn.class_name,
                    part_description = q.ma.ma.item.part_description,
                    applied_date = q.ma.ma.spiv.salePrice.applied_date.ToString(),
                    sale_price = q.ma.ma.spiv.salePrice.sale_price,
                    existencias = q.ma.ma.spiv.itemValuation.total_stock.Value
                }).ToList();
        }

        public List<SalePriceChangeModel> GetAllSalesPricesChangeRemoveExist0AndDate(DateTime fechaAplicacion)
        {
            return _context.SALES_PRICE_CHANGE.Join(_context.ITEM_VALUATION, salePrice => salePrice.part_number, itemValuation => itemValuation.part_number, (salePrice, itemValuation) => new { salePrice, itemValuation })
                .Join(_context.ITEM, spiv => spiv.salePrice.part_number, item => item.part_number, (spiv, item) => new { spiv, item })
                .Join(_context.MA_CLASS, itemn => itemn.item.level_header, ma => ma.class_id, (ma, itemn) => new { ma, itemn })
                .Join(_context.MA_CLASS, itemnn => itemnn.ma.item.level1_code, ma => ma.class_id, (ma, itemn) => new { ma, itemn })
                .Where(x => x.ma.ma.spiv.itemValuation.total_stock != 0 && x.ma.ma.spiv.salePrice.applied_date == fechaAplicacion && x.ma.ma.spiv.salePrice.status == 1)
                .Select(q => new SalePriceChangeModel
                {
                    sales_price_id = q.ma.ma.spiv.salePrice.sales_price_id,
                    part_number = q.ma.ma.spiv.salePrice.part_number,
                    part_description = q.ma.ma.item.part_description,
                    Department = q.ma.itemn.class_name,
                    Family = q.itemn.class_name,
                    applied_date = q.ma.ma.spiv.salePrice.applied_date.ToString(),
                    sale_price = q.ma.ma.spiv.salePrice.sale_price,
                    existencias = q.ma.ma.spiv.itemValuation.total_stock.Value
                }).ToList();
        }

        public List<SalePriceChangeModel> GetAllSalesPricesChangeRemoveExist0AndDeptoAndDate(DateTime fechaAplicacion, int depto)
        {
            return _context.SALES_PRICE_CHANGE.Join(_context.ITEM_VALUATION, salePrice => salePrice.part_number, itemValuation => itemValuation.part_number, (salePrice, itemValuation) => new { salePrice, itemValuation })
                .Join(_context.ITEM, spiv => spiv.salePrice.part_number, item => item.part_number, (spiv, item) => new { spiv, item })
                .Join(_context.MA_CLASS, itemn => itemn.item.level_header, ma => ma.class_id, (ma, itemn) => new { ma, itemn })
                .Join(_context.MA_CLASS, itemnn => itemnn.ma.item.level1_code, ma => ma.class_id, (ma, itemn) => new { ma, itemn })
                .Where(x => x.ma.ma.spiv.itemValuation.total_stock != 0 && x.ma.ma.spiv.salePrice.applied_date == fechaAplicacion && x.ma.ma.item.level_header == depto && x.ma.ma.spiv.salePrice.status == 1)
                .Select(q => new SalePriceChangeModel
                {
                    sales_price_id = q.ma.ma.spiv.salePrice.sales_price_id,
                    part_number = q.ma.ma.spiv.salePrice.part_number,
                    Department = q.ma.itemn.class_name,
                    Family = q.itemn.class_name,
                    part_description = q.ma.ma.item.part_description,
                    applied_date = q.ma.ma.spiv.salePrice.applied_date.ToString(),
                    sale_price = q.ma.ma.spiv.salePrice.sale_price,
                    existencias = q.ma.ma.spiv.itemValuation.total_stock.Value
                }).ToList();
        }

        public List<SalePriceChangeModel> GetAllSalesPricesChangeRemoveExist0AndDeptoAndFamilyAndDate(DateTime fechaAplicacion, int depto, int family)
        {
            return _context.SALES_PRICE_CHANGE.Join(_context.ITEM_VALUATION, salePrice => salePrice.part_number, itemValuation => itemValuation.part_number, (salePrice, itemValuation) => new { salePrice, itemValuation })
                .Join(_context.ITEM, spiv => spiv.salePrice.part_number, item => item.part_number, (spiv, item) => new { spiv, item })
                .Join(_context.MA_CLASS, itemn => itemn.item.level_header, ma => ma.class_id, (ma, itemn) => new { ma, itemn })
                .Join(_context.MA_CLASS, itemnn => itemnn.ma.item.level1_code, ma => ma.class_id, (ma, itemn) => new { ma, itemn })
                .Where(x => x.ma.ma.spiv.itemValuation.total_stock != 0 && x.ma.ma.spiv.salePrice.applied_date == fechaAplicacion && x.ma.ma.item.level_header == depto && x.ma.ma.item.level1_code == family && x.ma.ma.spiv.salePrice.status == 1)
                .Select(q => new SalePriceChangeModel
                {
                    sales_price_id = q.ma.ma.spiv.salePrice.sales_price_id,
                    part_number = q.ma.ma.spiv.salePrice.part_number,
                    part_description = q.ma.ma.item.part_description,
                    Department = q.ma.itemn.class_name,
                    Family = q.itemn.class_name,
                    applied_date = q.ma.ma.spiv.salePrice.applied_date.ToString(),
                    sale_price = q.ma.ma.spiv.salePrice.sale_price,
                    existencias = q.ma.ma.spiv.itemValuation.total_stock.Value
                }).ToList();
        }

        public List<SalePriceChangeModel> GetAllSalesPricesChangeByDeptoAndDate(DateTime fechaAplicacion, int depto)
        {
            return _context.SALES_PRICE_CHANGE.Join(_context.ITEM_VALUATION, salePrice => salePrice.part_number, itemValuation => itemValuation.part_number, (salePrice, itemValuation) => new { salePrice, itemValuation })
                .Join(_context.ITEM, spiv => spiv.salePrice.part_number, item => item.part_number, (spiv, item) => new { spiv, item })
                .Join(_context.MA_CLASS, itemn => itemn.item.level_header, ma => ma.class_id, (ma, itemn) => new { ma, itemn })
                .Join(_context.MA_CLASS, itemnn => itemnn.ma.item.level1_code, ma => ma.class_id, (ma, itemn) => new { ma, itemn })
                .Where(x => x.ma.ma.spiv.salePrice.applied_date == fechaAplicacion && x.ma.ma.item.level_header == depto && x.ma.ma.spiv.salePrice.status == 1)
                .Select(q => new SalePriceChangeModel
                {
                    sales_price_id = q.ma.ma.spiv.salePrice.sales_price_id,
                    part_number = q.ma.ma.spiv.salePrice.part_number,
                    Department = q.ma.itemn.class_name,
                    Family = q.itemn.class_name,
                    part_description = q.ma.ma.item.part_description,
                    applied_date = q.ma.ma.spiv.salePrice.applied_date.ToString(),
                    sale_price = q.ma.ma.spiv.salePrice.sale_price,
                    existencias = q.ma.ma.spiv.itemValuation.total_stock.Value
                }).ToList();
        }

        public List<SalePriceChangeModel> GetAllSalesPricesChangeByDeptoAndFamilyAndDate(DateTime fechaAplicacion, int depto, int family)
        {
            return _context.SALES_PRICE_CHANGE.Join(_context.ITEM_VALUATION, salePrice => salePrice.part_number, itemValuation => itemValuation.part_number, (salePrice, itemValuation) => new { salePrice, itemValuation })
                .Join(_context.ITEM, spiv => spiv.salePrice.part_number, item => item.part_number, (spiv, item) => new { spiv, item }).Join(_context.MA_CLASS, itemn => itemn.item.level_header, ma => ma.class_id, (ma, itemn) => new { ma, itemn })
                .Join(_context.MA_CLASS, itemnn => itemnn.ma.item.level1_code, ma => ma.class_id, (ma, itemn) => new { ma, itemn })
                .Where(x => x.ma.ma.spiv.salePrice.applied_date == fechaAplicacion && x.ma.ma.item.level_header == depto && x.ma.ma.item.level1_code == family && x.ma.ma.spiv.salePrice.status == 1)
                .Select(q => new SalePriceChangeModel
                {
                    sales_price_id = q.ma.ma.spiv.salePrice.sales_price_id,
                    part_number = q.ma.ma.spiv.salePrice.part_number,
                    Department = q.ma.itemn.class_name,
                    Family = q.itemn.class_name,
                    part_description = q.ma.ma.item.part_description,
                    applied_date = q.ma.ma.spiv.salePrice.applied_date.ToString(),
                    sale_price = q.ma.ma.spiv.salePrice.sale_price,
                    existencias = q.ma.ma.spiv.itemValuation.total_stock.Value
                }).ToList();
        }

        //CAS - CARNICERIA
        public List<SalesPriceChangeBasculeModel> GetAllSalesPricesForBasculeCas()
        {
            try
            {
                var item = _context.Database.SqlQuery<SalesPriceChangeBasculeModel>("sproc_SalesPriceWeighingMachineCAS").ToList();
                return item;
            }
            catch (Exception ex)
            {
                var msg = ex.Message;
                return null;
            }
        }

        public List<SalesPriceChangeBasculeModel> GetAllSalesPricesForBasculeToledo()
        {
            try
            {
                var item = _context.Database.SqlQuery<SalesPriceChangeBasculeModel>("sproc_SalesPriceWeighingMachineToledo").ToList();
                return item;
            }
            catch (Exception ex)
            {
                var msg = ex.Message;
                return null;
            }
        }

        public List<SalesPriceChangeBasculeModel> GetAllSalesPricesForBasculeToledoBPlus()
        {
            try
            {
                var item = _context.Database.SqlQuery<SalesPriceChangeBasculeModel>("sproc_SalesPriceWeighingMachineBPlus").ToList();
                return item;
            }
            catch (Exception ex)
            {
                var msg = ex.Message;
                return null;
            }
        }

        public List<SalesPriceChangeDataReport> GetSalesPriceChangeReport(SalePriceChangeIndex Model)
        {
            DateTime DateStart = DateTime.ParseExact(Model.StartDate, "dd/MM/yyyy", CultureInfo.InvariantCulture);
            DateTime DateEnd = DateTime.ParseExact(Model.EndDate, "dd/MM/yyyy", CultureInfo.InvariantCulture);
            var ReportList = new List<SalesPriceChangeDataReport>();

            ReportList = (from cp in _context.DFLPOS_CHANGES_PRICES
                          where DbFunctions.TruncateTime(cp.datee) >= DateStart
                                && DbFunctions.TruncateTime(cp.datee) <= DateEnd
                                && cp.quantity > 0
                          select new
                          {
                              ItemNumber = cp.ITEM.part_number,
                              ItemDescription = cp.ITEM.part_description,
                              Quantity = cp.quantity,
                              OriginalPrice = cp.original_price,
                              NewPrice = cp.new_price,
                              SaleNumber = cp.sale_id,
                              Concept = cp.DFLPOS_TYPE_CHANGE_PRICE.description,
                              Date = cp.datee,
                              FirstName = cp.USER_MASTER.first_name,
                              LastName = cp.USER_MASTER.last_name
                          }).AsEnumerable().Select(c => new SalesPriceChangeDataReport
                          {
                              ItemNumber = c.ItemNumber,
                              ItemDescription = c.ItemDescription,
                              Quantity = c.Quantity,
                              OriginalPrice = c.OriginalPrice,
                              TotalOriginalPrice = c.OriginalPrice * c.Quantity,
                              NewPrice = c.NewPrice,
                              TotalNewPrice = c.NewPrice * c.Quantity,
                              Difference = (c.OriginalPrice * c.Quantity) - (c.NewPrice * c.Quantity),
                              Percentage = 100 - (((c.NewPrice * c.Quantity) * 100) / (c.OriginalPrice * c.Quantity)),
                              SaleNumber = c.SaleNumber,
                              EmployeeName = string.Format("{0} {1}", c.FirstName, c.LastName),
                              Concept = c.Concept,
                              Date = c.Date.ToString("dd/MM/yyyy")
                          }).Where(x => x.Percentage >= Model.MinPercentage && x.Percentage <= Model.MaxPercentage).ToList();

            return ReportList;
        }
        public List<SalePriceChangeModel> GetAllSalesPricesWithFilterSimple(SalePriceFilter salePriceFilter)
        {
            DateTime date;
            if (string.IsNullOrEmpty(salePriceFilter.dateChange) || !DateTime.TryParse(salePriceFilter.dateChange, out date))
            {
                var q = from barcode in _context.BARCODES
                        join iv in _context.ITEM_VALUATION on barcode.part_number equals iv.part_number
                        join item in _context.ITEM on barcode.part_number equals item.part_number
                        join dc in _context.MA_CLASS on item.level_header equals dc.class_id into dt
                        from dclass in dt.DefaultIfEmpty()
                        join fc in _context.MA_CLASS on item.level1_code equals fc.class_id into ft
                        from fclass in ft.DefaultIfEmpty()
                        join p in _context.ITEM_PRESENTATION on barcode.presentation_id equals p.presentation_id into pt
                        from pre in pt.DefaultIfEmpty()
                        join pp in _context.ITEM_PRESENTATION_PRICE on pre.presentation_id equals pp.presentation_id into ppt
                        from pre_price in ppt.DefaultIfEmpty()
                        select new { iv, item, barcode, dclass, fclass, pre, pre_price };
                q = q.Where(e => e.barcode.presentation_id == null);
                if (salePriceFilter.depto != 0)
                    q = q.Where(x => x.item.level_header == salePriceFilter.depto);

                if (salePriceFilter.family != 0)
                    q = q.Where(x => x.item.level1_code == salePriceFilter.family);

                if (salePriceFilter.exist.HasValue)
                    q = q.Where(x => salePriceFilter.exist.Value ? x.iv.total_stock > 0 : x.iv.total_stock == 0);

                if (!string.IsNullOrWhiteSpace(salePriceFilter.search))
                {
                    if (!salePriceFilter.exact)
                    {
                        q = q.Where(e =>
                            e.barcode.part_barcode.StartsWith(salePriceFilter.search) || e.item.part_number.StartsWith(salePriceFilter.search) ||
                            e.barcode.part_barcode.EndsWith(salePriceFilter.search) || e.item.part_number.EndsWith(salePriceFilter.search) ||
                            e.item.part_description.Contains(salePriceFilter.search));// || e.pre.presentation_name.Contains(salePriceFilter.search));
                    }
                    else
                    {
                        q = q.Where(e => e.barcode.part_barcode == salePriceFilter.search || e.item.part_number == salePriceFilter.search);
                    }
                }

                var r = q.Select(x => new SalePriceChangeModel
                {

                    part_number = x.item.part_number,
                    part_description = x.item.part_description,
                    sale_price = x.iv.price_sale_iva ?? 0,
                    barcode = x.barcode.part_barcode,
                    unit_size = x.item.unit_size,
                    presentation = _context.ITEM_PRESENTATION
                        .Join(_context.BARCODES, pre => pre.part_number, barcodes => barcodes.part_number, (pre, barcodes) => new { pre, barcodes })
                        .Join(_context.ITEM_PRESENTATION_PRICE, e => e.pre.presentation_id, e => e.presentation_id, (e, price) => new { e.pre, e.barcodes, price })
                        .Select(
                            (e) => new ItemPresentationModel
                            {
                                PresentationId = e.pre.presentation_id,
                                Name = e.pre.presentation_name,
                                PartNumber = e.pre.part_number,
                                PartDescription = x.item.part_description,
                                Quantity = e.pre.quantity,
                                PresentationCode = e.pre.presentation_code,
                                FactorUnitSize = e.pre.factor_unit_size,
                                TotalPrice = e.price.price_sale_iva,
                                Barcode = e.barcodes.part_barcode,
                                PresentationName = e.pre.presentation_name,
                                PackingType = e.pre.packing_type,
                                ShowInLabel = e.pre.show_in_label,
                                ActiveFlag = e.pre.active_flag,

                            })
                        .FirstOrDefault(e => e.Barcode == x.barcode.part_barcode && e.ActiveFlag && x.barcode.presentation_id == null),

                    promotion = _context.PROMOTION_DETAIL
                           .Join(_context.PROMOTION_HEADER, e => e.promotion_code, e => e.promotion_code, (d, h) => new { d, h })
                           .Join(_context.BARCODES, e => e.d.part_number, e => e.part_number, (e, b) => new { e.d, e.h, b })
                           .Join(_context.ITEM, e => e.b.part_number, e => e.part_number, (e, i) => new { e.d, e.h, e.b, i })
                           .Join(_context.MA_TAX, e => e.i.part_iva_sale, e => e.tax_code, (e, tax) => new { e.d, e.h, e.b, tax })

                           .Where(e => e.d.part_number == x.item.part_number && e.d.presentation_id == null && e.d.delete_flag == false && e.h.delete_flag == false && e.d.returned_flag == false && e.h.returned_flag == false
                                    && DbFunctions.TruncateTime(DateTime.Today) >= DbFunctions.TruncateTime(e.h.promotion_start_date)
                                    && DbFunctions.TruncateTime(DateTime.Today) <= DbFunctions.TruncateTime(e.h.promotion_end_date)
                                    )

                           .Select(e => new PromotionItemModel
                           {
                               promotion_price = e.d.promotion_price * (1 + (e.tax.tax_value ?? 0) / 100),
                               start_date = e.h.promotion_start_date.ToString(),
                               end_date = e.h.promotion_end_date.ToString()
                           })
                           .FirstOrDefault(),

                }).ToList();

                //presentaciones
                if (salePriceFilter.presentation == false)
                {
                    if (salePriceFilter.unique_code)
                        r = r.DistinctBy(e => new { e.part_number, e.presentation_id }).ToList();
                    return r;
                }
                var q2 = from barcode in _context.BARCODES
                         join iv in _context.ITEM_VALUATION on barcode.part_number equals iv.part_number
                         join item in _context.ITEM on barcode.part_number equals item.part_number
                         join dc in _context.MA_CLASS on item.level_header equals dc.class_id into dt
                         from dclass in dt.DefaultIfEmpty()
                         join fc in _context.MA_CLASS on item.level1_code equals fc.class_id into ft
                         from fclass in ft.DefaultIfEmpty()
                         join p in _context.ITEM_PRESENTATION on barcode.presentation_id equals p.presentation_id into pt
                         from pre in pt.DefaultIfEmpty()
                         join pp in _context.ITEM_PRESENTATION_PRICE on pre.presentation_id equals pp.presentation_id into ppt
                         from pre_price in ppt.DefaultIfEmpty()
                         select new { iv, item, barcode, dclass, fclass, pre, pre_price };
                q2 = q2.Where(e => e.barcode.presentation_id != null && e.pre.active_flag);
                if (salePriceFilter.depto != 0)
                    q2 = q2.Where(x => x.item.level_header == salePriceFilter.depto);

                if (salePriceFilter.family != 0)
                    q2 = q2.Where(x => x.item.level1_code == salePriceFilter.family);

                if (salePriceFilter.exist.HasValue)
                    q2 = q2.Where(x => salePriceFilter.exist.Value ? x.iv.total_stock > 0 : x.iv.total_stock == 0);

                if (!string.IsNullOrWhiteSpace(salePriceFilter.search))
                {
                    if (!salePriceFilter.exact)
                    {
                        q2 = q2.Where(e =>
                            e.barcode.part_barcode.StartsWith(salePriceFilter.search) || e.item.part_number.StartsWith(salePriceFilter.search) ||
                            e.barcode.part_barcode.EndsWith(salePriceFilter.search) || e.item.part_number.EndsWith(salePriceFilter.search) ||
                            e.item.part_description.Contains(salePriceFilter.search) || e.pre.presentation_name.Contains(salePriceFilter.search));
                    }
                    else
                    {
                        q2 = q2.Where(e => e.barcode.part_barcode == salePriceFilter.search || e.item.part_number == salePriceFilter.search || e.pre.presentation_name == salePriceFilter.search);
                    }
                }
                var r2 = q2.Select(x => new SalePriceChangeModel
                {

                    part_number = x.item.part_number,
                    part_description = x.item.part_description,
                    sale_price = x.iv.price_sale_iva ?? 0,
                    barcode = x.barcode.part_barcode,
                    presentation_id = x.barcode.presentation_id,
                    unit_size = x.item.unit_size,
                    presentation = new ItemPresentationModel
                    {
                        PresentationId = x.pre.presentation_id,
                        Name = x.pre.presentation_name,
                        PartNumber = x.pre.part_number,
                        PartDescription = x.item.part_description,
                        Quantity = x.pre.quantity,
                        PresentationCode = x.pre.presentation_code,
                        FactorUnitSize = x.pre.factor_unit_size,
                        TotalPrice = x.pre_price.price_sale_iva,
                        Barcode = x.pre.presentation_code,
                        PresentationName = x.pre.presentation_name,
                        PackingType = x.pre.packing_type,
                        ShowInLabel = x.pre.show_in_label,
                        ActiveFlag = x.pre.active_flag,
                    },

                    promotion = _context.PROMOTION_DETAIL
                            .Join(_context.PROMOTION_HEADER, e => e.promotion_code, e => e.promotion_code, (d, h) => new { d, h })
                            .Join(_context.BARCODES, e => e.d.part_number, e => e.part_number, (e, b) => new { e.d, e.h, b })
                            .Join(_context.ITEM, e => e.b.part_number, e => e.part_number, (e, i) => new { e.d, e.h, e.b, i })
                            .Join(_context.MA_TAX, e => e.i.part_iva_sale, e => e.tax_code, (e, tax) => new { e.d, e.h, e.b, tax })

                            .Where(e => e.d.presentation_id == x.barcode.presentation_id && e.d.delete_flag == false && e.h.delete_flag == false && e.h.delete_flag == false && e.d.returned_flag == false && e.h.returned_flag == false
                                     && DbFunctions.TruncateTime(DateTime.Today) >= DbFunctions.TruncateTime(e.h.promotion_start_date)
                                     && DbFunctions.TruncateTime(DateTime.Today) <= DbFunctions.TruncateTime(e.h.promotion_end_date)
                                     )

                            .Select(e => new PromotionItemModel
                            {
                                promotion_price = e.d.promotion_price * (1 + (e.tax.tax_value ?? 0) / 100),
                                start_date = e.h.promotion_start_date.ToString(),
                                end_date = e.h.promotion_end_date.ToString()
                            })
                            .FirstOrDefault(),

                }).ToList();


                r.AddRange(r2);



                if (salePriceFilter.unique_code)
                    r = r.DistinctBy(e => new { e.part_number, e.presentation_id }).ToList();
                return r;
            }
            else
            {// cambio de precio
                var q = from item in _context.ITEM
                        join iv in _context.ITEM_VALUATION on item.part_number equals iv.part_number
                        join ma in _context.MA_TAX on item.part_iva_sale equals ma.tax_code
                        join barcode in _context.BARCODES on item.part_number equals barcode.part_number
                        join dclass in _context.MA_CLASS on item.level_header equals dclass.class_id
                        join fclass in _context.MA_CLASS on item.level1_code equals fclass.class_id

                        join pc in (
                            from pc2 in _context.SALES_PRICE_CHANGE
                            where pc2.status == 1 && DbFunctions.TruncateTime(pc2.applied_date) == DbFunctions.TruncateTime(date)
                            select pc2
                            ) on item.part_number equals pc.part_number into pct
                        from pchan in pct.DefaultIfEmpty()

                        join pclocal in (
                            from pclocal2 in _context.ITEM_VALUATION
                            where  DbFunctions.TruncateTime(pclocal2.last_update_sales_price) == DbFunctions.TruncateTime(date) && pclocal2.program_id == "SystemLocal"
                            select pclocal2
                        ) on item.part_number equals pclocal.part_number into pctlocal
                        from pchanlocal in pctlocal.DefaultIfEmpty()

                        where pchan != null || pchanlocal !=null

                        select new { iv, item, barcode, dclass, fclass, pchan, ma,pchanlocal};
                q = q.Where(e => e.barcode.presentation_id == null);
                if (salePriceFilter.depto != 0)
                    q = q.Where(x => x.item.level_header == salePriceFilter.depto);

                if (salePriceFilter.family != 0)
                    q = q.Where(x => x.item.level1_code == salePriceFilter.family);

                if (salePriceFilter.exist.HasValue)
                    q = q.Where(x => salePriceFilter.exist.Value ? x.iv.total_stock > 0 : x.iv.total_stock == 0);
                if (!string.IsNullOrWhiteSpace(salePriceFilter.search))
                    q = q.Where(e => e.barcode.part_barcode.StartsWith(salePriceFilter.search) || e.item.part_number.StartsWith(salePriceFilter.search) ||
                                     e.barcode.part_barcode.EndsWith(salePriceFilter.search) || e.item.part_number.EndsWith(salePriceFilter.search) ||
                                     e.item.part_description.Contains(salePriceFilter.search));

                var r = q.Select(x => new SalePriceChangeModel
                {

                    part_number = x.item.part_number,
                    part_description = x.item.part_description,

                    sale_price = x.pchan.sale_price_iva ?? x.pchanlocal.price_sale_iva ?? 8888888,
                    unit_size = x.item.unit_size,
                    barcode = x.barcode.part_barcode,

                    presentation = _context.ITEM_PRESENTATION
                    .Join(_context.BARCODES, pre => pre.part_number, barcodes => barcodes.part_number, (pre, barcodes) => new { pre, barcodes })
                    .Join(_context.ITEM_PRESENTATION_PRICE, e => e.pre.presentation_id, p_price => p_price.presentation_id, (e, p_price) => new { e.pre, e.barcodes, p_price })
                    .SelectMany(e => _context.SALES_PRICE_CHANGE_PRESENTATION // left join
                       .Where(e2 => e2.presentation_id == x.barcode.presentation_id
                                    && DbFunctions.TruncateTime(e2.applied_date) >= DbFunctions.TruncateTime(date)
                                    ).OrderByDescending(e2 => e2.cdate)
                           .DefaultIfEmpty().Select(pchan => new { e.pre, e.barcodes, e.p_price, pchan })
                       .Take(1))

                    .SelectMany(e => // left join
                       (from pd2 in _context.PROMOTION_DETAIL
                        join ph2 in _context.PROMOTION_HEADER on pd2.promotion_code equals ph2.promotion_code
                        where pd2.presentation_id != null &&
                               pd2.delete_flag == false &&
                               DbFunctions.TruncateTime(ph2.promotion_start_date) >= DbFunctions.TruncateTime(date) &&
                               DbFunctions.TruncateTime(ph2.promotion_end_date) <= DbFunctions.TruncateTime(date) &&
                               ph2.delete_flag == false
                        select new { prom_d = pd2, prom_h = ph2 }).DefaultIfEmpty()
                       .OrderByDescending(e2 => e2.prom_h.promotion_start_date)
                       .Select(prom => new { e.pre, e.barcodes, e.pchan, e.p_price, prom })
                       .Take(1))


                    .Where(e =>
                        e.pre.presentation_id == x.barcode.presentation_id && e.pre.active_flag
                        )

                    .Select((e) => new ItemPresentationModel
                    {
                        PresentationId = e.pre.presentation_id,
                        Name = e.pre.presentation_name,
                        PartNumber = e.pre.part_number,
                        PartDescription = x.item.part_description,
                        Quantity = e.pre.quantity,
                        PresentationCode = e.pre.presentation_code,


                        FactorUnitSize = e.pre.factor_unit_size,
                        TotalPrice = e.prom != null ? e.prom.prom_d.promotion_price * (1 + (x.ma.tax_value ?? 0) / 100) :
                            e.pchan.sale_price_iva != null ? e.pchan.sale_price_iva :
                            e.p_price.price_sale_iva,
                        Barcode = e.barcodes.part_barcode,
                        PresentationName = e.pre.presentation_name,
                        PackingType = e.pre.packing_type,
                        ShowInLabel = e.pre.show_in_label,
                        ActiveFlag = e.pre.active_flag,

                    }).FirstOrDefault(),



                }).ToList();
                if (salePriceFilter.presentation == false)
                {
                    if (salePriceFilter.unique_code)
                        r = r.DistinctBy(e => new { e.part_number, e.presentation_id }).ToList();
                    return r;
                }

                // presentaciones
                var q2 = from item in _context.ITEM
                         join ma in _context.MA_TAX on item.part_iva_sale equals ma.tax_code
                         join iv in _context.ITEM_VALUATION on item.part_number equals iv.part_number
                         join barcode in _context.BARCODES on item.part_number equals barcode.part_number
                         join dclass in _context.MA_CLASS on item.level_header equals dclass.class_id
                         join fclass in _context.MA_CLASS on item.level1_code equals fclass.class_id
                         join pre in _context.ITEM_PRESENTATION on barcode.presentation_id equals pre.presentation_id

                         join pp in (
                             from pp2 in _context.SALES_PRICE_CHANGE_PRESENTATION
                             where pp2.status == 1 && DbFunctions.TruncateTime(pp2.applied_date) == DbFunctions.TruncateTime(date)
                             select pp2
                             ) on pre.presentation_id equals pp.presentation_id into ppt
                         from pre_price in ppt.DefaultIfEmpty()
                         join pclocal in (
                             from pclocal2 in _context.ITEM_PRESENTATION_PRICE
                             where DbFunctions.TruncateTime(pclocal2.udate) == DbFunctions.TruncateTime(date) && pclocal2.program_id == "SystemLocal"
                             select pclocal2
                         ) on pre.presentation_id equals pclocal.presentation_id into pclocalt
                         from pchanlocal in pclocalt.DefaultIfEmpty()

                         where pre_price != null || pchanlocal != null
                         select new { iv, item, barcode, dclass, fclass, pre, pre_price, ma, pchanlocal }; 



                if (salePriceFilter.depto != 0)
                    q2 = q2.Where(x => x.item.level_header == salePriceFilter.depto);

                if (salePriceFilter.family != 0)
                    q2 = q2.Where(x => x.item.level1_code == salePriceFilter.family);

                if (salePriceFilter.exist.HasValue)
                    q2 = q2.Where(x => salePriceFilter.exist.Value ? x.iv.total_stock > 0 : x.iv.total_stock == 0);
                if (!string.IsNullOrWhiteSpace(salePriceFilter.search))
                    q2 = q2.Where(e => e.barcode.part_barcode.StartsWith(salePriceFilter.search) || e.item.part_number.StartsWith(salePriceFilter.search) ||
                                       e.barcode.part_barcode.EndsWith(salePriceFilter.search) || e.item.part_number.EndsWith(salePriceFilter.search) ||

                                       e.item.part_description.Contains(salePriceFilter.search) || e.pre.presentation_name.Contains(salePriceFilter.search));

                var r2 = q2.Select(x => new SalePriceChangeModel
                {

                    part_number = x.item.part_number,
                    part_description = x.item.part_description,

                    sale_price = 8888888, //sin iva : sin iva
                    unit_size = x.item.unit_size,
                    barcode = x.barcode.part_barcode,
                    presentation_id = x.barcode.presentation_id,
                    presentation = new ItemPresentationModel
                    {
                        PresentationId = x.pre.presentation_id,
                        Name = x.pre.presentation_name,
                        PartNumber = x.pre.part_number,
                        PartDescription = x.item.part_description,
                        Quantity = x.pre.quantity,
                        PresentationCode = x.pre.presentation_code,
                        FactorUnitSize = x.pre.factor_unit_size,
                        TotalPrice = x.pre_price != null ? x.pre_price.sale_price_iva :
                            x.pchanlocal  != null ? x.pchanlocal.price_sale_iva :
                                    9999999,
                        Barcode = x.pre.presentation_code,
                        PresentationName = x.pre.presentation_name,
                        PackingType = x.pre.packing_type,
                        ShowInLabel = x.pre.show_in_label,
                        ActiveFlag = x.pre.active_flag,
                    },
                }).ToList();

                r.AddRange(r2);

                if (salePriceFilter.unique_code)
                    r = r.DistinctBy(e => new { e.part_number, e.presentation_id }).ToList();
                return r;


            }
        }
        public List<SalePriceChangeModel> GetAllSalesPricesWithFilterProm(SalePriceFilter salePriceFilter)
        {
            DateTime date;
            if (string.IsNullOrEmpty(salePriceFilter.dateChange) || !DateTime.TryParse(salePriceFilter.dateChange, out date))
            {
               return new List<SalePriceChangeModel>();
            }

            // cambio de precio
            var q = from item in _context.ITEM
                join iv in _context.ITEM_VALUATION on item.part_number equals iv.part_number
                join ma in _context.MA_TAX on item.part_iva_sale equals ma.tax_code
                join barcode in _context.BARCODES on item.part_number equals barcode.part_number
                join dclass in _context.MA_CLASS on item.level_header equals dclass.class_id 
                join fclass in _context.MA_CLASS on item.level1_code equals fclass.class_id 
                join prom in (
                    from pd2 in _context.PROMOTION_DETAIL
                    join ph2 in _context.PROMOTION_HEADER on pd2.promotion_code equals ph2.promotion_code 
                    where pd2.presentation_id == null &&
                          pd2.delete_flag == false &&
                          DbFunctions.TruncateTime(ph2.promotion_start_date) == DbFunctions.TruncateTime(date) &&
                          ph2.delete_flag == false && ph2.returned_flag == false
                    select new { prom_d = pd2, prom_h = ph2}
                ) on barcode.part_number equals prom.prom_d.part_number
                

                select new { iv,item, barcode, dclass, fclass, ma, prom.prom_d, prom.prom_h };
            q = q.Where(e => e.barcode.presentation_id == null);
            if (salePriceFilter.depto != 0)
                q = q.Where(x => x.item.level_header == salePriceFilter.depto);

            if (salePriceFilter.family != 0)
                q = q.Where(x => x.item.level1_code == salePriceFilter.family);

            if (salePriceFilter.exist.HasValue)
                q = q.Where(x => salePriceFilter.exist.Value ? x.iv.total_stock > 0 : x.iv.total_stock == 0);
            if (!string.IsNullOrWhiteSpace(salePriceFilter.search))
                q = q.Where(e => e.barcode.part_barcode.StartsWith(salePriceFilter.search) || e.item.part_number.StartsWith(salePriceFilter.search) ||
                                 e.barcode.part_barcode.EndsWith(salePriceFilter.search) || e.item.part_number.EndsWith(salePriceFilter.search) ||
                                 e.item.part_description.Contains(salePriceFilter.search) );
                
            var r = q.Select(x => new SalePriceChangeModel
            {
                    
                part_number = x.item.part_number,
                part_description = x.item.part_description,
                    
                sale_price =  8888888,
                unit_size = x.item.unit_size,
                barcode = x.barcode.part_barcode,

                presentation = _context.ITEM_PRESENTATION
                    .Join(_context.BARCODES, pre => pre.part_number, barcodes => barcodes.part_number, (pre, barcodes) => new { pre, barcodes })
                    .Join(_context.ITEM_PRESENTATION_PRICE , e => e.pre.presentation_id, p_price => p_price.presentation_id ,(e,p_price)=> new { e.pre, e.barcodes,p_price })
                    .SelectMany( e=> _context.SALES_PRICE_CHANGE_PRESENTATION // left join
                        .Where(e2 => e2.presentation_id == x.barcode.presentation_id 
                                     && DbFunctions.TruncateTime(e2.applied_date) >= DbFunctions.TruncateTime(date)
                                     ).OrderByDescending(e2=>e2.cdate)
                            .DefaultIfEmpty().Select(pchan=> new {e.pre,e.barcodes,e.p_price,pchan} )
                        .Take(1))

                    .SelectMany( e => // left join
                        (from pd2 in _context.PROMOTION_DETAIL
                        join ph2 in _context.PROMOTION_HEADER on pd2.promotion_code equals ph2.promotion_code
                        where pd2.presentation_id != null &&
                              pd2.delete_flag == false &&
                              DbFunctions.TruncateTime(ph2.promotion_start_date) >= DbFunctions.TruncateTime(date) &&
                              DbFunctions.TruncateTime(ph2.promotion_end_date) <= DbFunctions.TruncateTime(date) &&
                              ph2.delete_flag == false
                        select new { prom_d = pd2, prom_h = ph2 }).DefaultIfEmpty()
                        .OrderByDescending(e2=>e2.prom_h.promotion_start_date)
                        .Select(prom=> new { e.pre, e.barcodes, e.pchan,e.p_price,prom })
                        .Take(1))
                        

                    .Where(e =>
                        e.pre.presentation_id == x.barcode.presentation_id && e.pre.active_flag
                        )

                    .Select((e) => new ItemPresentationModel
                    {
                        PresentationId = e.pre.presentation_id,
                        Name = e.pre.presentation_name,
                        PartNumber = e.pre.part_number,
                        PartDescription = x.item.part_description,
                        Quantity = e.pre.quantity,
                        PresentationCode = e.pre.presentation_code,


                        FactorUnitSize = e.pre.factor_unit_size,
                        TotalPrice = e.prom != null ? e.prom.prom_d.promotion_price * (1 + (x.ma.tax_value ?? 0) / 100) :
                            e.pchan.sale_price_iva != null ? e.pchan.sale_price_iva :
                            e.p_price.price_sale_iva,
                        Barcode = e.barcodes.part_barcode,
                        PresentationName = e.pre.presentation_name,
                        PackingType = e.pre.packing_type,
                        ShowInLabel = e.pre.show_in_label,
                        ActiveFlag = e.pre.active_flag,

                    }).FirstOrDefault(),

                promotion_spc = new PromotionItemModel
                {
                    promotion_price = x.prom_d.promotion_price * (1 + (x.ma.tax_value ?? 0) / 100),
                    start_date = x.prom_h.promotion_start_date.ToString(),
                    end_date = x.prom_h.promotion_end_date.ToString(),
                    start_date_date = x.prom_h.promotion_start_date,
                } 


            }).ToList();
            if (salePriceFilter.presentation == false)
            {
                if (salePriceFilter.unique_code)
                    r = r.DistinctBy(e => new { e.part_number, e.presentation_id }).ToList();
                return r;
            }

            // presentaciones
            var q2 = from item in _context.ITEM
                join ma in _context.MA_TAX on item.part_iva_sale equals ma.tax_code
                join iv in _context.ITEM_VALUATION on item.part_number equals iv.part_number
                join barcode in _context.BARCODES on item.part_number equals barcode.part_number
                join dclass in _context.MA_CLASS on item.level_header equals dclass.class_id 
                join fclass in _context.MA_CLASS on item.level1_code equals fclass.class_id 
                join pre in _context.ITEM_PRESENTATION on barcode.presentation_id equals pre.presentation_id 

                join prom in (
                    from pd2 in _context.PROMOTION_DETAIL
                    join ph2 in _context.PROMOTION_HEADER on pd2.promotion_code equals ph2.promotion_code
                    where
                        pd2.delete_flag == false &&
                        DbFunctions.TruncateTime(ph2.promotion_start_date) == DbFunctions.TruncateTime(date) &&
                        ph2.delete_flag == false && ph2.returned_flag == false
                    select new { prom_d = pd2, prom_h = ph2 }
                ) on barcode.presentation_id equals prom.prom_d.presentation_id 
                select new { iv, item, barcode, dclass, fclass, pre, ma, prom.prom_d, prom.prom_h };



            if (salePriceFilter.depto != 0)
                q2 = q2.Where(x => x.item.level_header == salePriceFilter.depto);

            if (salePriceFilter.family != 0)
                q2 = q2.Where(x => x.item.level1_code == salePriceFilter.family);

            if (salePriceFilter.exist.HasValue)
                q2 = q2.Where(x => salePriceFilter.exist.Value ? x.iv.total_stock > 0 : x.iv.total_stock == 0);
            if (!string.IsNullOrWhiteSpace(salePriceFilter.search))
                q2 = q2.Where(e => e.barcode.part_barcode.StartsWith(salePriceFilter.search) || e.item.part_number.StartsWith(salePriceFilter.search) ||
                                   e.barcode.part_barcode.EndsWith(salePriceFilter.search) || e.item.part_number.EndsWith(salePriceFilter.search) ||

                                   e.item.part_description.Contains(salePriceFilter.search) || e.pre.presentation_name.Contains(salePriceFilter.search));
                
            var r2 = q2.Select(x => new SalePriceChangeModel
            {

                part_number = x.item.part_number,
                part_description = x.item.part_description,

                sale_price =  8888888, //sin iva : sin iva
                unit_size = x.item.unit_size,
                barcode = x.barcode.part_barcode,
                presentation_id = x.barcode.presentation_id,

                promotion_spc = new PromotionItemModel
                {
                    promotion_price = x.prom_d.promotion_price * (1 + (x.ma.tax_value ?? 0) / 100),
                    start_date = x.prom_h.promotion_start_date.ToString(),
                    end_date = x.prom_h.promotion_end_date.ToString()
                },

            }).ToList();

            r.AddRange(r2);

            if (salePriceFilter.unique_code)
                r = r.DistinctBy(e => new { e.part_number, e.presentation_id }).ToList();
            return r;
        }
        
        public int addItemSalesPriceChange(string partNumber, decimal margin, decimal salePrice, string Sesion, string ProgramId)
        {
            try
            {
                var Product = _context.SALES_PRICE_CHANGE_FORCED.Where(w => w.part_number == partNumber && w.status == 1).FirstOrDefault();
                var ProductOriginal = _context.ITEM_VALUATION.Where(w => w.part_number == partNumber).FirstOrDefault();
                decimal PriceOriginal = 0;
                if (Product != null)
                {
                    PriceOriginal = Product.original_price;
                    Product.status = 8;
                    Product.udate = DateTime.Now;
                    Product.uuser = Sesion;
                    Product.program_id = ProgramId;
                }
                else
                {
                    PriceOriginal = ProductOriginal.price_sale_iva.HasValue ? ProductOriginal.price_sale_iva.Value : salePrice;
                }
                var ItemChangePrice = new SALES_PRICE_CHANGE_FORCED()
                {
                    part_number = partNumber,
                    presentation_id = null,
                    applied_date = DateTime.Now,
                    margin = margin,
                    sale_price = salePrice,
                    original_price = PriceOriginal,
                    status = 0,
                    cdate = DateTime.Now,
                    cuser = Sesion,
                    program_id = ProgramId
                };
                _context.SALES_PRICE_CHANGE_FORCED.Add(ItemChangePrice);
                var save = _context.SaveChanges();
                return save;
            }
            catch (Exception e)
            {
                var msg = e.Message;
                return 0;
            }
        }

        public int ChangeSales()
        {
            var DateReturn = _context.Database.SqlQuery<int>("DECLARE	@return_value int  EXEC    @return_value = [dbo].[sproc_change_sales_price_forced] @situation = N'1'  SELECT  'Return Value' = @return_value").ToList();
            return DateReturn[0];
        }

        public List<ItemSupplierModelPrice> GetItemSaleHistory(string part_number)
        {
            var x = _context.SALES_PRICE_CHANGE.Where(w => w.part_number == part_number)
                .OrderByDescending(o => o.cdate)
                .Take(100)
                .Select(s => new ItemSupplierModelPrice
                {
                    item_number = s.part_number,
                    item_price = s.sale_price,
                    item_price_iva = s.sale_price_iva ?? 0,
                    margin = s.margin,
                    user = s.cuser,
                    fechainicio = s.applied_date,
                    active_flag = s.status == 1 ? "Activo" : "Desactivado"
                }).ToList();
            return x;
        }
        public List<ItemSupplierModelPrice> GetItemSaleForceHistory(string part_number)
        {
            var x = _context.SALES_PRICE_CHANGE_FORCED.Where(w => w.part_number == part_number)
                .OrderByDescending(o => o.cdate)
                .Select(s => new ItemSupplierModelPrice
                {
                    item_number = s.part_number,
                    item_price = s.original_price, //precio original con iva
                    item_price_iva = s.sale_price, // precio nuevo con iva
                    margin = s.margin,
                    user = s.cuser,
                    fechainicio = s.applied_date,
                    active_flag = s.status == 9 ? "Terminado"
                    : s.status == 8 ? "Cancelado"
                    : s.status == 1 ? "Activo"
                    : s.status == 0 ? "Iniciado"
                    : null,
                }).ToList();
            return x;
        }

        public List<ItemCurrent> GetItemPriceSaleCurrent(string department, string family, string user)
        {
            //Convert Int´s
            int d = Convert.ToInt32(department);
            int f = Convert.ToInt32(family);

            var queryItems = _context.ITEM_VALUATION.Join(_context.ITEM, IV => IV.part_number, I => I.part_number, (IV, I) => new { IV, I });

            if (f != 0)
                queryItems = queryItems.Where(w => w.I.level1_code == f && w.I.level_header == d && w.I.active_flag == true);
            else
                queryItems = queryItems.Where(w => w.I.level_header == d && w.I.active_flag == true);

            var queryItemsPrice = queryItems
                .Take(350)
                .OrderBy(o => o.I.part_description)
                .Select(s => new ItemCurrent
            {
                PresentationFlag = false,
                DescriptionReal = s.I.part_description,
                FactorUnitSize = s.I.unit_size,
                PartNumber = s.I.part_number,
                TaxPrice = s.IV.price_sale_iva ?? 0,
                Description = "",
            }).ToList();

            var presentationItem = new List<ItemCurrent>();


            foreach (var item in queryItemsPrice)
            {
                //regresar falso en bit
                var modelQuickPresentation = _context.Database.SqlQuery<ItemCurrent>(@"SELECT ITEM_PRESENTATION.presentation_code 'PartNumber', ITEM_PRESENTATION.presentation_name 'DescriptionReal', ITEM_PRESENTATION.packing_type 'PackingType', ITEM_PRESENTATION.quantity 'Quantity', 
                ITEM_PRESENTATION.factor_unit_size 'FactorUnitSize', ITEM_PRESENTATION_PRICE.price_sale_iva 'TaxPrice'  FROM ITEM 
                INNER JOIN BARCODES ON ITEM.part_number = BARCODES.part_number 
                INNER JOIN ITEM_PRESENTATION ON ITEM_PRESENTATION.presentation_id = BARCODES.presentation_id
                INNER JOIN ITEM_PRESENTATION_PRICE ON BARCODES.presentation_id = ITEM_PRESENTATION_PRICE.presentation_id 
                WHERE ITEM_PRESENTATION.active_flag = 1 AND ITEM.part_number = @partnumber ", new SqlParameter("@partnumber", item.PartNumber)).ToList();
                if (modelQuickPresentation != null)
                {
                    presentationItem.AddRange(modelQuickPresentation);
                }
            }
            queryItemsPrice.AddRange(presentationItem);
            return queryItemsPrice;
        }

        public ItemCurrent GetSingleItemPrice(string part_number)
        {
            return _context.ITEM.Join(_context.ITEM_VALUATION, I => I.part_number, IV => IV.part_number, (I, IV) => new { I, IV })
                .Join(_context.MA_TAX, IV2 => IV2.I.part_iva_sale, M => M.tax_code, (IV2, M) => new { IV2, M })
                .Where(w => w.IV2.I.part_number == part_number)
                .Select(s => new ItemCurrent
                {
                    PartNumber = s.IV2.I.part_number,
                    DescriptionReal = s.IV2.I.part_description,
                    IVA_Description = s.M.name,
                    Price = s.IV2.IV.price_sale ?? 0,
                    TaxPrice = s.IV2.IV.price_sale_iva ?? 0
                }).FirstOrDefault();
        }

        public List<ItemCurrent> GetPresentationPriceByPartNumber(string part_number)
        {
            return _context.Database.SqlQuery<ItemCurrent>(@"SELECT ITEM_PRESENTATION.presentation_code 'PartNumber', ITEM_PRESENTATION.presentation_name 'DescriptionReal', ITEM_PRESENTATION.packing_type 'PackingType', ITEM_PRESENTATION.quantity 'Quantity', 
                ITEM_PRESENTATION.factor_unit_size 'FactorUnitSize', ITEM_PRESENTATION_PRICE.price_sale_iva 'TaxPrice', ITEM_PRESENTATION_PRICE.price_sale_iva/ITEM_PRESENTATION.quantity 'PriceUnit'  FROM ITEM 
                INNER JOIN BARCODES ON ITEM.part_number = BARCODES.part_number 
                INNER JOIN ITEM_PRESENTATION ON ITEM_PRESENTATION.presentation_id = BARCODES.presentation_id
                INNER JOIN ITEM_PRESENTATION_PRICE ON BARCODES.presentation_id = ITEM_PRESENTATION_PRICE.presentation_id 
                WHERE ITEM_PRESENTATION.active_flag = 1 AND ITEM.part_number = @partnumber ", new SqlParameter("@partnumber", part_number)).ToList();
        }
    }
}