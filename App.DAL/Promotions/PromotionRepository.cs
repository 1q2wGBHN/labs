using App.Entities;
using App.Entities.ViewModels.Promotion;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;

namespace App.DAL.Promotions
{
    public class PromotionRepository
    {
        private readonly DFL_SAIEntities _context = new DFL_SAIEntities();

        public List<PromotionReportModel> PromotionReport(string TypePromotion, string PartNumber, DateTime? BeginDate, DateTime? EndDate)
        {
            DateTime date = DateTime.Today;
            DateTime BeginDate2 = BeginDate ?? date;
            var Type_Promotion = new SqlParameter { ParameterName = "TypePromotion", Value = TypePromotion };
            var Part_Number = new SqlParameter { ParameterName = "PartNumber", Value = PartNumber };
            var begindate = new SqlParameter { ParameterName = "BeginDate", IsNullable = true, SqlDbType = SqlDbType.DateTime, Value = BeginDate2 };
            var enddate = new SqlParameter { ParameterName = "EndDate", IsNullable = true, SqlDbType = SqlDbType.DateTime, Value = EndDate };

            if (BeginDate.HasValue)
                begindate.Value = begindate.Value;
            else
                begindate.Value = DBNull.Value;

            if (EndDate.HasValue)
                enddate.Value = enddate.Value;
            else
                enddate.Value = DBNull.Value;

            var model = _context.Database.SqlQuery<PromotionReportModel>("sp_Get_Promotions @BeginDate, @EndDate, @TypePromotion, @PartNumber", begindate, enddate, Type_Promotion, Part_Number).ToList();

            return model;
        }

        public List<PromotionItem> GetPromotionAdvancedByCode(string promotion_code)
        {
            var promotions = _context.PROMOTION_ADVANCED_DETAIL
               .Join(_context.ITEM, i => i.part_number, ii => ii.part_number, (i, ii) => new { i, ii })
               .Join(_context.ITEM_VALUATION, ii2 => ii2.ii.part_number, IV => IV.part_number, (ii2, IV) => new { ii2, IV })
               .Where(w => w.ii2.i.promotion_code == promotion_code && w.ii2.i.delete_flag == false)
               .Select(s => new PromotionItem
               {
                   promotion_code = s.ii2.i.promotion_code,
                   part_number = s.ii2.i.part_number,
                   description = s.ii2.ii.part_description,
                   quantity = s.ii2.i.promotion_quantity,
                   price = s.ii2.i.promotion_price,
                   price_original = (s.IV.price_sale_iva ?? 0) * s.ii2.i.promotion_quantity
               }).ToList();
            //Busca si hay comunes
            foreach (var promotion in promotions)
            {
                var commons = _context.PROMOTION_ADVANCED_COMMON.Where(w => w.promotion_advanced_code == promotion.promotion_code && w.common == promotion.part_number && w.delete_flag == false).ToList();

                if (commons.Count() > 0)
                    promotion.common = 1;
                else
                    promotion.common = 0;
            }
            return promotions;
        }

        public List<PromotionItem> GetPromotionAdvancedCommonByCode(string promotion_code, string common)
        {
            var xx = _context.PROMOTION_ADVANCED_COMMON
                .Join(_context.ITEM, PAC => PAC.item, I => I.part_number, (PAC, I) => new { PAC, I })
                .Join(_context.ITEM_VALUATION, II => II.I.part_number, IV => IV.part_number, (II, IV) => new { II, IV })
                .Where(w => w.II.PAC.promotion_advanced_code == promotion_code && w.II.PAC.common == common && w.II.PAC.delete_flag == false)
                .Select(s => new PromotionItem
                {
                    part_number = s.II.PAC.item,
                    description = s.II.I.part_description,
                    price = s.IV.price_sale_iva ?? 0 //Precio original
                }).ToList();
            return xx;
        }

        public List<PromotionItem> GetPromotionByCode(string promotion_code)
        {
            //Productos normales
            var products = _context.PROMOTION_DETAIL
               .Join(_context.ITEM, PD => PD.part_number, I => I.part_number, (PD, I) => new { PD, I })
               .Join(_context.MA_TAX, II => II.I.part_iva_sale, MM => MM.tax_code, (II, MM) => new { II, MM })
               .Where(w => w.II.PD.promotion_code == promotion_code && w.II.PD.delete_flag == false)
               .Select(s => new PromotionItem
               {
                   part_number = s.II.PD.part_number,
                   description = s.II.I.part_description,
                   price = s.II.PD.promotion_price * (1 + ((s.MM.tax_value ?? 0) / 100)),
                   price_original = s.II.PD.original_price ?? 0
               }).ToList();
            // Presentaciones
            var presentations = _context.PROMOTION_DETAIL
               .Join(_context.ITEM_PRESENTATION, PD => PD.presentation_id, IT => IT.presentation_id, (PD, IT) => new { PD, IT })
               .Join(_context.ITEM, IIT => IIT.IT.part_number, I => I.part_number, (IIT, I) => new { IIT, I })
               .Join(_context.MA_TAX, II => II.I.part_iva_sale, MM => MM.tax_code, (II, MM) => new { II, MM })
               .Where(w => w.II.IIT.PD.promotion_code == promotion_code && w.II.IIT.PD.delete_flag == false)
               .Select(s => new PromotionItem
               {
                   part_number = s.II.IIT.IT.presentation_code,
                   description = s.II.IIT.IT.presentation_name,
                   price = s.II.IIT.PD.promotion_price * (1 + (s.MM.tax_value ?? 0) / 100),
                   price_original = s.II.IIT.PD.original_price ?? 0
               }).ToList();
            //sumarlos
            products.AddRange(presentations);
            return products;
        }

        public List<PromotionModel> GetPromotionHistoryByPartNmber(string part_number)
        {
            var normal = _context.PROMOTION_DETAIL.Join(_context.PROMOTION_HEADER, PD => PD.promotion_code, PH => PH.promotion_code, (PD, PH) => new { PD, PH })
                .Where(w => w.PD.part_number == part_number && w.PD.delete_flag == false)
                .OrderByDescending(o => o.PH.cdate)
                .Take(50)
                .Select(s => new PromotionModel
                {
                    promotion_code = s.PH.promotion_code,
                    promotion_descrip = "Promoción Normal",
                    promotion_date_start = s.PH.promotion_start_date,
                    promotion_date_end = s.PH.promotion_end_date,
                    promotion_create_user = s.PH.cuser,
                    promotion_type = "Normal",
                    cdate = s.PH.cdate,
                }).ToList();
            var avanzada = _context.Database.SqlQuery<PromotionModel>(@"
            SELECT TOP 50 PAH.promotion_advanced_code promotion_code, PAH.promotion_description promotion_descrip, PAH.promotion_start_date promotion_date_start, 
            PAH.promotion_end_date promotion_date_end, PAH.cuser promotion_create_user, 'Avanzada' promotion_type, PAH.cdate  FROM PROMOTION_ADVANCED_HEADER PAH
            JOIN PROMOTION_ADVANCED_DETAIL PAD ON PAD.promotion_code = PAH.promotion_advanced_code
            LEFT JOIN PROMOTION_ADVANCED_COMMON PAC ON PAC.promotion_advanced_code = PAD.promotion_code
            WHERE PAD.delete_flag = 0 AND (PAD.part_number = @part_number AND PAD.delete_flag = 0) OR (PAC.item = @part_number AND PAC.delete_flag = 0)
            ORDER BY PAH.cdate DESC
            ", new SqlParameter("@part_number", part_number)).ToList();
            normal.AddRange(avanzada);
            normal.OrderByDescending(o => o.cdate);
            return normal;
        }
    }
}