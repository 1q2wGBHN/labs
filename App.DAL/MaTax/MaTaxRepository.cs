﻿using App.Entities;
using App.Entities.ViewModels.CreditNotes;
using App.Entities.ViewModels.MaTax;
using System.Collections.Generic;
using System.Linq;
using static App.Entities.ViewModels.Common.Constants;

namespace App.DAL.MaTax
{
    public class MaTaxRepository
    {
        private readonly DFL_SAIEntities _context;

        public MaTaxRepository()
        {
            _context = new DFL_SAIEntities();
        }

        public MaTaxModel GetTaxValueByCode(string tax_code)
        {
            return _context.MA_TAX.Where(x => x.tax_code == tax_code)
                .Select(x => new MaTaxModel
                {
                    tax_code = x.tax_code,
                    tax_value = x.tax_value.Value
                }).FirstOrDefault();
        }

        public MaTaxModel GetTaxValue(string tax_code)
        {
            return _context.MA_TAX.Where(x => x.tax_code == tax_code)
                .Select(x => new MaTaxModel
                {
                    tax_code = x.tax_code,
                    tax_value = x.tax_value != null ? x.tax_value.Value : 0
                }).FirstOrDefault();
        }

        public List<BonificationTax> GetTaxesForBonifications()
        {
            return _context.MA_TAX.Where(t => t.tax_code != TaxCodes.NOT_AVAILABLE && t.tax_code != TaxCodes.IRS && t.tax_code != TaxCodes.EXE_0)
                .Select(t => new BonificationTax
                {
                    TaxCode = t.tax_code,
                    TaxName = t.name,
                    TaxValue = t.tax_value.HasValue ? t.tax_value.Value : 0,
                    Bonification = 0
                }).ToList();
        }

        public List<MaTaxModel> GetAllTaxes()
        {
            return _context.MA_TAX.Select(x => new MaTaxModel
            {
                tax_code = x.tax_code.ToString(),
                tax_value = x.tax_value ?? 0,
                name = x.name
            }).ToList();
        }

        public List<MA_TAX> GetTaxList()
        {
            return _context.MA_TAX.Where(c => c.tax_code == TaxCodes.NOT_AVAILABLE || c.tax_code.Contains(TaxTypes.TAX)).ToList();
        }

        public List<MA_TAX> GetIEPSList()
        {
            return _context.MA_TAX.Where(c => c.tax_code == TaxCodes.NOT_AVAILABLE || c.tax_code.Contains(TaxTypes.IEPS)).ToList();
        }

        public List<BonificationTax> GetTaxForGlobalInvoiceBonification(long InvoiceNo)
        {
            var List = new List<BonificationTax>();
            List = (from mt in _context.MA_TAX
                    join idt in _context.INVOICE_DETAIL_TAX on mt.tax_code equals idt.tax_code
                    join id in _context.INVOICE_DETAIL on idt.detail_id equals id.detail_id
                    where id.invoice_no == InvoiceNo
                          && mt.tax_code != TaxCodes.EXE_0
                    select new BonificationTax
                    {
                        TaxCode = mt.tax_code,
                        TaxName = mt.name,
                        TaxValue = mt.tax_value.HasValue ? mt.tax_value.Value : 0,
                        Bonification = 0
                    }).Distinct().ToList();

            return List;
        }
        public List<MA_TAX> GetAllIVANormal()
        {
            return _context.MA_TAX.Where(x => (x.tax_type == "1" || x.tax_type == "0") && (x.type_stc == "001" || x.type_stc == "002") && x.tax_code != "EXE_0" && x.tax_code != "IRS").OrderBy(x => x.name).ThenByDescending(x => x.name).ToList();
        }
    }
}