﻿using App.Entities;
using System;
using System.Linq;

public class UserPwdRepository
{
    private readonly DFL_SAIEntities _context;

    public UserPwdRepository()
    {
        _context = new DFL_SAIEntities();
    }

    public USER_PWD GetUserPwdByEmployeeNumber(string employeeNumber)
    {
        return _context.USER_PWD.FirstOrDefault(x => x.emp_no == employeeNumber);
    }

    public int UpdateUserPwd(USER_PWD user)
    {
        _context.Entry(user).State = System.Data.Entity.EntityState.Modified;
        return _context.SaveChanges();
    }

    public int UpdateStatusUser(string userNumber, string statusUser)
    {
        try
        {
            var user = _context.USER_PWD.FirstOrDefault(x => x.emp_no == userNumber);
            user.status = statusUser;
            _context.SaveChanges();
            return 1;
        }
        catch (Exception e)
        {
            var message = e.Message;
            return 0;
        }
    }
}