﻿using App.Entities;
using App.Entities.ViewModels.RawMaterial;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.SqlClient;
using System.Linq;

namespace App.DAL.RawMaterial
{
    public class RawMaterialRepository
    {
        private readonly DFL_SAIEntities _context;

        public RawMaterialRepository()
        {
            _context = new DFL_SAIEntities();
        }

        public int AddRawMaterialHeader(RAW_MATERIAL_HEADER folio)
        {
            try
            {
                _context.RAW_MATERIAL_HEADER.Add(folio);
                var save = _context.SaveChanges();
                int id = folio.folio;
                return id;
            }
            catch (Exception ex)
            {
                var msg = ex.Message;
                return 0;
            }
        }

        public int AddRawMaterialDetail(RAW_MATERIAL_DETAIL folio)
        {
            try
            {
                _context.RAW_MATERIAL_DETAIL.Add(folio);
                var save = _context.SaveChanges();
                return save;
            }
            catch (Exception ex)
            {
                var msg = ex.Message;
                return 0;
            }
        }

        public List<RawMaterialHeader> GetAllRawMaterialHeader(int Status, DateTime DateInit, DateTime DateFin)
        {
            var rawMaterial = _context.RAW_MATERIAL_HEADER
                .Join(_context.SITES, site => site.site_code, siteName => siteName.site_code, (raw, Site) => new { raw, Site })
                .Where(raw => raw.raw.transfer_status == Status && raw.raw.transfer_date >= DateInit && raw.raw.transfer_date <= DateFin)
                .Select(raw => new RawMaterialHeader
                {
                    Folio = raw.raw.folio,
                    SiteCode = raw.raw.site_code,
                    TransferDate = DbFunctions.Right("00" + raw.raw.transfer_date.Day, 2) + "/" + DbFunctions.Right("00" + raw.raw.transfer_date.Month, 2) + "/" + DbFunctions.Right("00" + raw.raw.transfer_date.Year, 4),
                    Cuser = raw.raw.cuser,
                    Received_by = raw.raw.received_by,
                    SiteName = raw.Site.site_name,
                    StorageLocation = raw.raw.gr_storage_location,
                    Comment = raw.raw.comment
                }).ToList();
            return rawMaterial;
        }

        public List<RawMaterialDetail> GetAllRawMaterialDetail(int folio)
        {
            return _context.RAW_MATERIAL_DETAIL
                 .Join(_context.ITEM, raw => raw.part_number, item => item.part_number, (raw, item) => new { raw, item })
                 .Join(_context.RAW_MATERIAL_HEADER, detail => detail.raw.folio, header => header.folio, (detail, header) => new { detail, header })
                 .Join(_context.SITES, site => site.header.site_code, siteName => siteName.site_code, (siteCode, siteName) => new { siteCode, siteName })
                 .Join(_context.ITEM_VALUATION, rawMaterialValuetion => rawMaterialValuetion.siteCode.detail.raw.part_number, itemValuetion => itemValuetion.part_number, (rawMaterialValuetion, itemValuetion) => new { rawMaterialValuetion, itemValuetion })
                .Where(elements => elements.rawMaterialValuetion.siteCode.detail.raw.folio == folio)
                .Select(elements => new RawMaterialDetail
                {
                    Description = elements.rawMaterialValuetion.siteCode.detail.item.part_description,
                    Quantity = elements.rawMaterialValuetion.siteCode.detail.raw.quantity,
                    Part_Number = elements.rawMaterialValuetion.siteCode.detail.raw.part_number,
                    Folio = elements.rawMaterialValuetion.siteCode.detail.raw.folio,
                    UnitCost = elements.itemValuetion.map_price ?? 0,
                }).ToList();
        }

        public List<RawMaterialDetail> GetAllRawMaterialDetail(int folio, int status)
        {
            return _context.RAW_MATERIAL_DETAIL
                 .Join(_context.ITEM, raw => raw.part_number, item => item.part_number, (raw, item) => new { raw, item })
                 .Join(_context.RAW_MATERIAL_HEADER, detail => detail.raw.folio, header => header.folio, (detail, header) => new { detail, header })
                 .Join(_context.SITES, site => site.header.site_code, siteName => siteName.site_code, (siteCode, siteName) => new { siteCode, siteName })
                 .Join(_context.ITEM_VALUATION, rawMaterialValuetion => rawMaterialValuetion.siteCode.detail.raw.part_number, itemValuetion => itemValuetion.part_number, (rawMaterialValuetion, itemValuetion) => new { rawMaterialValuetion, itemValuetion })
                .Where(elements => elements.rawMaterialValuetion.siteCode.detail.raw.folio == folio & elements.rawMaterialValuetion.siteCode.detail.raw.transfer_status == status)
                .Select(elements => new RawMaterialDetail
                {
                    Description = elements.rawMaterialValuetion.siteCode.detail.item.part_description,
                    Quantity = elements.rawMaterialValuetion.siteCode.detail.raw.quantity,
                    Part_Number = elements.rawMaterialValuetion.siteCode.detail.raw.part_number,
                    Folio = elements.rawMaterialValuetion.siteCode.detail.raw.folio,
                    UnitCost = elements.itemValuetion.map_price ?? 0,
                }).ToList();
        }

        public List<RawMaterialDetail> GetAllRawMaterialDetailByDates(DateTime BeginDate, DateTime EndDate, string status, string department, string family, string location)
        {
            if (department == "0")
                department = "";

            var consultaGenerico = @"SELECT RMD.folio Folio, RMD.part_number Part_Number, MC1.class_name Department, MC2.class_name Family,
	            I.part_description Description, RMD.quantity Quantity,
	            CONVERT(VARCHAR, rmh.transfer_date,101) TransferDate,
	            RMD.cost UnitCost, CAST(RMD.cost*RMD.quantity AS DECIMAL(18, 4)) SubTotal, 
	            CAST(RMD.ieps AS DECIMAL(18, 4)) IEPS,  M2.name TASA_IEPS,  
	            CAST(RMD.iva AS DECIMAL(18, 4)) Iva, m1.name TASA_Iva,  
	            CAST(RMD.cost*RMD.quantity AS DECIMAL(18, 4))+CAST(RMD.ieps AS DECIMAL(18, 4))+CAST(RMD.iva AS DECIMAL(18, 4)) Amount,
	            CASE
		            WHEN RMD.transfer_status = 0 THEN 'Iniciado'
		            WHEN RMD.transfer_status = 2 THEN 'Aprobado'
		            WHEN RMD.transfer_status = 8 THEN 'Cancelado'
		            WHEN RMD.transfer_status = 9 THEN 'Terminando'
	            END as Item_Status, 
	            US1.first_name + ' ' + US1.last_name Autothized_by, US2.first_name + ' ' + US2.last_name Received_by, RMH.gr_storage_location StorageLocation
	            FROM RAW_MATERIAL_DETAIL RMD
	            JOIN RAW_MATERIAL_HEADER RMH ON RMH.folio = RMD.folio
	            JOIN ITEM I ON I.part_number = RMD.part_number
	            JOIN MA_TAX M1 ON M1.tax_code = I.part_iva_sale
	            JOIN MA_TAX M2 ON M2.tax_code = I.part_ieps
	            JOIN MA_CLASS MC1 ON MC1.class_id = I.level_header
	            JOIN MA_CLASS MC2 ON MC2.class_id = I.level1_code
	            LEFT JOIN USER_MASTER US1 ON US1.user_name = RMH.authorized_by
	            LEFT JOIN USER_MASTER US2 ON US2.user_name = RMH.received_by
                    where  CONVERT (date, rmh.transfer_date,101)>= @date1 and CONVERT (date, rmh.transfer_date,101) <= @date2 ";
            //Parametros
            List<SqlParameter> parameters = new List<SqlParameter>();
            parameters.Add(new SqlParameter("@date1", BeginDate.Date));
            parameters.Add(new SqlParameter("@date2", EndDate.Date));
            if (!string.IsNullOrWhiteSpace(status))
            {
                consultaGenerico += " and RMD.transfer_status = @status and RMD.transfer_status = @status";
                parameters.Add(new SqlParameter("@status", Convert.ToInt32(status)));
            }
            if (!string.IsNullOrWhiteSpace(department))
            {
                consultaGenerico += " and I.level_header = @depart";
                parameters.Add(new SqlParameter("@depart", Convert.ToInt32(department)));
            }
            if (!string.IsNullOrWhiteSpace(family))
            {
                consultaGenerico += " and I.level1_code = @fam";
                parameters.Add(new SqlParameter("@fam", Convert.ToInt32(family)));
            }
            if (!string.IsNullOrWhiteSpace(location))
            {
                consultaGenerico += " and RMH.gr_storage_location = @location";
                parameters.Add(new SqlParameter("@location", location));
            }
            return _context.Database.SqlQuery<RawMaterialDetail>(consultaGenerico, parameters.ToArray()).ToList();
        }

        //RA = Receipt Aproval / Aprobacion recibo
        public bool RawMaterialEditStatusRA(int folio, string User, string comment)
        {
            try
            {
                var rawMaterial = _context.RAW_MATERIAL_HEADER.Where(elements => elements.folio == folio).SingleOrDefault();
                rawMaterial.program_id = "TMP001.cshtml";
                rawMaterial.uuser = User;
                if (comment != "") { rawMaterial.comment = comment; }
                rawMaterial.udate = DateTime.Now;
                rawMaterial.received_by = User;
                rawMaterial.transfer_status = 2;
                _context.SaveChanges();
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public bool RawMaterialEditStatusRA(List<RawMaterialDetail> model, int folio, string User, string comment)
        {
            try
            {
                var rawMaterial = _context.RAW_MATERIAL_HEADER.Where(elements => elements.folio == folio).SingleOrDefault();
                rawMaterial.program_id = "TMP001.cshtml";
                rawMaterial.uuser = User;
                if (comment != "") { rawMaterial.comment = comment; }
                rawMaterial.udate = DateTime.Now;
                rawMaterial.received_by = User;
                rawMaterial.transfer_status = 2;
                _context.SaveChanges();

                foreach (var item in model)
                {
                    var rawMaterialDetail = _context.RAW_MATERIAL_DETAIL.Where(x => x.folio == folio & x.part_number == item.Part_Number).FirstOrDefault();
                    if (rawMaterialDetail != null)
                    {
                        rawMaterialDetail.program_id = "TMP001.cshtml";
                        rawMaterialDetail.uuser = User;
                        rawMaterialDetail.udate = DateTime.Now;
                        rawMaterialDetail.transfer_status = 2;
                        _context.SaveChanges();
                    }
                }

                var items = _context.RAW_MATERIAL_DETAIL.Where(x => x.folio == folio & x.transfer_status != 2).ToList();
                foreach (var item in items)
                {
                    item.program_id = "TMP001.cshtml";
                    item.uuser = User;
                    item.udate = DateTime.Now;
                    item.transfer_status = 8;
                    _context.SaveChanges();
                }

                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        //RC = Receipt Cancel / Cancelacion recibo
        public bool RawMaterialEditStatusRC(int folio, string User, string comment)
        {
            try
            {
                var rawMaterial = _context.RAW_MATERIAL_HEADER.Where(elements => elements.folio == folio).SingleOrDefault();
                rawMaterial.program_id = "TMP001.cshtml";
                rawMaterial.uuser = User;
                if (comment != "") { rawMaterial.comment = comment; }
                rawMaterial.udate = DateTime.Now;
                rawMaterial.received_by = User;
                rawMaterial.transfer_status = 8;
                _context.SaveChanges();

                var rawMaterialDetail = _context.RAW_MATERIAL_DETAIL.Where(x => x.folio == folio).ToList();
                foreach (var item in rawMaterialDetail)
                {
                    item.program_id = "TMP001.cshtml";
                    item.uuser = User;
                    item.udate = DateTime.Now;
                    item.transfer_status = 8;
                    _context.SaveChanges();
                }

                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        //MA = Management Aproval / Aprobacion gerencia
        public bool RawMaterialEditStatusMA(int folio, string User, string comment)
        {
            try
            {
                var rawMaterial = _context.RAW_MATERIAL_HEADER.Where(elements => elements.folio == folio).SingleOrDefault();
                if (comment != "") { rawMaterial.comment = comment; }
                rawMaterial.udate = DateTime.Now;
                rawMaterial.authorized_by = User;
                rawMaterial.transfer_status = 9;
                _context.SaveChanges();
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public bool RawMaterialEditStatusMA(List<RawMaterialDetail> model, int folio, string User, string comment)
        {
            try
            {
                var rawMaterialHead = _context.RAW_MATERIAL_HEADER.Where(x => x.folio == folio).FirstOrDefault();
                rawMaterialHead.comment = comment;
                rawMaterialHead.authorized_by = User;
                rawMaterialHead.uuser = User;
                rawMaterialHead.udate = DateTime.Now;
                rawMaterialHead.program_id = "TMP002.cshtml";
                _context.SaveChanges();

                List<RAW_MATERIAL_DETAIL> newItems = new List<RAW_MATERIAL_DETAIL>();
                foreach (var item in model)
                {
                    var rawMaterialDetail = _context.RAW_MATERIAL_DETAIL.Where(x => x.folio == folio & x.part_number == item.Part_Number).FirstOrDefault();
                    newItems.Add(rawMaterialDetail);
                }

                var Items = _context.RAW_MATERIAL_DETAIL.Where(x => x.folio == folio).ToList();
                var newList = Items.Except(newItems).ToList();

                foreach (var item in newList)
                {
                    item.program_id = "TMP002.cshtml";
                    item.uuser = User;
                    item.udate = DateTime.Now;
                    item.transfer_status = 8;
                    _context.SaveChanges();
                }

                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        //MC = Management Cancel / Cancelacion gerencia
        public bool RawMaterialEditStatusMC(int folio, string User, string comment)
        {
            try
            {
                var rawMaterial = _context.RAW_MATERIAL_HEADER.Where(elements => elements.folio == folio).SingleOrDefault();
                rawMaterial.program_id = "TMP002.cshtml";
                rawMaterial.uuser = User;
                if (comment != "") { rawMaterial.comment = comment; }
                rawMaterial.udate = DateTime.Now;
                rawMaterial.authorized_by = User;
                rawMaterial.transfer_status = 8;
                _context.SaveChanges();

                var items = _context.RAW_MATERIAL_DETAIL.Where(x => x.folio == folio).ToList();
                foreach (var item in items)
                {
                    item.program_id = "TMP002.cshtml";
                    item.uuser = User;
                    item.udate = DateTime.Now;
                    item.transfer_status = 8;
                    _context.SaveChanges();
                }

                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public StoreProcedureResult RawMaterialProcedure(int folio, string User)
        {
            var raw = _context.RAW_MATERIAL_HEADER.Where(elements => elements.folio == folio).SingleOrDefault();
            var DateReturn = _context.Database.SqlQuery<StoreProcedureResult>("DECLARE	@return_value int, @document nvarchar(10) EXEC    @return_value = [dbo].[sproc_ST_Raw] @site_code = N'" + raw.site_code + "', @folio = N'" + folio + "', @cuser = N'" + User + "', @program_id = N'TMP002.cshtml', @document = @document OUTPUT SELECT 'Document'= @Document, 'ReturnValue' = @return_value").ToList();
            return DateReturn[0];
        }

        public StoreProcedureResult RawMaterialGIProcedure(int folio, string User)
        {
            var raw = _context.RAW_MATERIAL_HEADER.Where(elements => elements.folio == folio).SingleOrDefault();
            var DateReturn = _context.Database.SqlQuery<StoreProcedureResult>("DECLARE	@return_value int, @document nvarchar(10) EXEC    @return_value = [dbo].[sproc_GI_RAW] @site_code = N'" + raw.site_code + "', @raw_id = N'" + folio + "', @user = N'" + User + "', @program_id = N'TMP002.cshtml', @document = @document OUTPUT SELECT 'Document'= @Document, 'ReturnValue' = @return_value").ToList();
            return DateReturn[0];
        }


        public List<RawMaterialHeader> GetAllTransfersToRaw(DateTime initial, DateTime finish, string status, string location)
        {
            var consultaGenerica = @"select U1.first_name + ' ' + U1.last_name as Autothized_by,
	            rwh.comment as Comment,
	            rwh.cuser as Cuser,
	            rwh.folio as Folio,
	            U2.first_name + ' ' + U2.last_name as Received_by,
                rwh.gr_storage_location as StorageLocation,
                rwh.transfer_status as Status,
                CONVERT (nvarchar(50),rwh.transfer_date,101) as TransferDate,
                rwh.folio, iva,ieps,SubTotal,total from RAW_MATERIAL_HEADER rwh
                inner join (
	            select rmd.folio,
                sum(CAST(rmd.cost * rmd.quantity AS decimal(18,4))) as SubTotal, 
	            sum(CAST(rmd.ieps AS decimal(18,4))) as Ieps,
	            sum(CAST(rmd.iva AS decimal(18,4))) as Iva,
	            sum(CAST(rmd.cost * rmd.quantity AS decimal(18,4))) + sum(CAST(rmd.ieps  AS decimal(18,4))) + sum((CAST(rmd.iva AS decimal(18,4)))) as Total
                from RAW_MATERIAL_DETAIL rmd
                inner join ITEM_VALUATION  itv on itv.part_number = rmd.part_number
                inner join ITEM it on it.part_number = rmd.part_number
                inner join MA_TAX iva on iva.tax_code = it.part_iva_purchase
                inner join MA_TAX ieps on ieps.tax_code = it.part_ieps ";
            if (!string.IsNullOrWhiteSpace(status))
                consultaGenerica += " where rmd.transfer_status = @status ";
            var ConsultaGenericaNext = @" group by folio) b on b.folio = rwh.folio
	            left jOIN USER_MASTER U1 on U1.user_name = rwh.authorized_by
	            left jOIN USER_MASTER U2 on U2.user_name = rwh.received_by
                where CONVERT (date, rwh.transfer_date,101)>= @date1 and CONVERT (date, rwh.transfer_date,101) <= @date2 ";
            //Lista de Parametros
            List<SqlParameter> parameters = new List<SqlParameter>();
            parameters.Add(new SqlParameter("@date1", initial.Date));
            parameters.Add(new SqlParameter("@date2", finish.Date));
            if (!string.IsNullOrWhiteSpace(status))
            {
                ConsultaGenericaNext += " and rwh.transfer_status = @status ";
                parameters.Add(new SqlParameter("@status", Convert.ToInt32(status)));
            }
            if (!string.IsNullOrWhiteSpace(location))
            {
                ConsultaGenericaNext += " and rwh.gr_storage_location = @location ";
                parameters.Add(new SqlParameter("@location", location));
            }
            //Junto toda la consulta y ejecutamos
            consultaGenerica += ConsultaGenericaNext;
            return _context.Database.SqlQuery<RawMaterialHeader>(consultaGenerica, parameters.ToArray()).ToList();
        }

        public RawMaterialHeader GetRawHeaderByFolio(int folio)
        {
            var raw = _context.RAW_MATERIAL_HEADER.Where(e => e.folio == folio)
                .Join(_context.SITES, sites => sites.site_code, raws => raws.site_code, (raws, sites) => new { raws, sites })
                .Select(e => new RawMaterialHeader
                {
                    Folio = e.raws.folio,
                    TransferDate = e.raws.udate != null ? (DbFunctions.Right("00" + e.raws.udate.Value.Day, 2) + "/" + DbFunctions.Right("00" + e.raws.udate.Value.Month, 2) + "/" + DbFunctions.Right("00" + e.raws.udate.Value.Year, 4)) : DbFunctions.Right("00" + e.raws.transfer_date.Day, 2) + "/" + DbFunctions.Right("00" + e.raws.transfer_date.Month, 2) + "/" + DbFunctions.Right("00" + e.raws.transfer_date.Year, 4),
                    StorageLocation = e.raws.gr_storage_location,
                    SiteName = e.sites.site_name,
                    Comment = e.raws.comment,
                    Status = e.raws.transfer_status,
                    Print_Status = e.raws.print_status ?? 0
                }).First();
            return raw;
        }


        public bool DamagedsGoodsEditStatusPrint(int folio, int Status, string program_id)
        {
            try
            {
                var damageds = _context.RAW_MATERIAL_HEADER.Where(elements => elements.folio == folio).SingleOrDefault();
                damageds.program_id = program_id + "+Print";
                damageds.print_status = Status + 1;
                _context.SaveChanges();
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }
    }
}