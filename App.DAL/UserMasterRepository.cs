﻿using App.Entities;
using App.Entities.ViewModels.Configuration;
using App.Entities.ViewModels.User;
using Microsoft.Ajax.Utilities;
using System;
using System.Collections.Generic;
using System.Data.Entity.Core;
using System.Data.SqlClient;
using System.Linq;

public class UserMasterRepository
{
    private readonly DFL_SAIEntities _context;

    //constructor
    public UserMasterRepository()
    {
        _context = new DFL_SAIEntities();
    }

    //functions
    public USER_MASTER GetUserMasterByEmployeeNumber(string employeeNumber)
    {
        return _context.USER_MASTER.FirstOrDefault(x => x.emp_no == employeeNumber);
    }

    public int AddUserMaster(USER_MASTER user)
    {
        try
        {
            _context.USER_MASTER.Add(user);
            return _context.SaveChanges();
        }
        catch (Exception ex)
        {
            var msg = ex.Message;
            return 0;
        }
    }

    public int ReloadCountUser(USER_MASTER user)
    {
        try
        {
            var User = _context.USER_PWD.Find(user.emp_no);
            User.failed_attempts = 0;
            return _context.SaveChanges();
        }
        catch (Exception ex)
        {
            var msg = ex.Message;
            return 0;
        }
    }

    public USER_MASTER GetUserMasterByEmail(string email)
    {
        return _context.USER_MASTER.FirstOrDefault(x => x.email == email);
    }

    public USER_MASTER GetUserMasterByUsername(string username)
    {
        return _context.USER_MASTER.FirstOrDefault(x => x.user_name == username);
    }


    public USER_MASTER GetUserMasterByUsernameOrEmpNo(string usernameOrNo)
    {
        return _context.USER_MASTER
            .FirstOrDefault(x => x.user_name == usernameOrNo || x.emp_no == usernameOrNo);
    }
    public int DeleteUserMaster(USER_MASTER user)
    {
        _context.Entry(user).State = System.Data.Entity.EntityState.Deleted;
        return _context.SaveChanges();
    }

    public int UpdateUserMaster(USER_MASTER user)
    {
        _context.Entry(user).State = System.Data.Entity.EntityState.Modified;
        return _context.SaveChanges();
    }

    public USER_MASTER GetUserMasterByRecoveryPasswordCode(string code)
    {
        return _context.USER_MASTER.FirstOrDefault(x => x.USER_PWD.pass_code.Equals(code));
    }

    public List<USER_MASTER> GetAllUsers()
    {
        return _context.USER_MASTER.ToList();
    }

    public List<UserMasterDrop> GetAllUsersByDrop()
    {
        return _context.USER_MASTER.Select(s => new UserMasterDrop
        {
            emp_no = s.emp_no,
            first_name = s.first_name,
            last_name = s.last_name,
            user_name = s.user_name
        }).ToList();
    }

    public string getCompleteName(string user_name)
    {
        var user = _context.USER_MASTER.Where(x => x.user_name == user_name).FirstOrDefault();
        if (user != null)
            return user.last_name + " " + user.first_name;
        else
            return user_name;
    }

    public bool IsRolonUser(string user_name, string role)
    {
        if (user_name == "")
            return false;
        var count = _context.Database.SqlQuery<int>(@"
            select count(*) from SYS_ROLE_USER sru inner join SYS_ROLE_MASTER srm on srm.role_id = sru.role_id
            inner join USER_MASTER um on um.emp_no = sru.emp_no
            where role_name = @role_name and um.user_name = @user ", new SqlParameter("@role_name", role), new SqlParameter("@user", user_name)).SingleOrDefault();
        return count > 0;
    }

    public List<string> GetUserRoles(string user_name)
    {
        var roles = _context.Database.SqlQuery<string>(@"
            select srm.role_name from SYS_ROLE_USER sru inner join SYS_ROLE_MASTER srm on srm.role_id = sru.role_id
            inner join USER_MASTER um on um.emp_no = sru.emp_no
            where um.user_name = @user ", new SqlParameter("@user", user_name)).ToList();
        return roles;
    }

    public string getEmailByUserName(string user_name)
    {
        var user = _context.USER_MASTER.Where(w => w.user_name == user_name).Select(s => new UserModelFirst { Email = s.email }).FirstOrDefault();
        if (user == null)
            return "";
        return user.Email;
    }

    public bool DeletePicture(USER_MASTER model)
    {
        try
        {
            model.photo = null;
            _context.SaveChanges();
            return true;
        }
        catch (Exception ex)
        {
            string msg = ex.Message;
            return false;
        }
    }

    public List<UserModelFirst> GetCashiersUsers()
    {
        var Cashiers = (from c in _context.DFLPOS_CASH_WITHDRAWAL
                        join u in _context.USER_MASTER on c.userr_id equals u.emp_no
                        select new UserModelFirst
                        {
                            EmpNo = u.emp_no,
                            UserName = u.user_name,
                            Email = u.email,
                            FirstName = u.first_name,
                            LastName = u.last_name
                        }).Distinct().ToList();

        return Cashiers;
    }

    public List<UserModelFirst> GetCashiersChargeRelations()
    {
        var Cashiers = (from spm in _context.DFLPOS_SALES_PAYMENT_METHOD
                        join u in _context.USER_MASTER on spm.emp_no equals u.emp_no
                        select new UserModelFirst
                        {
                            EmpNo = u.emp_no,
                            UserName = u.user_name,
                            Email = u.email,
                            FirstName = u.first_name,
                            LastName = u.last_name
                        }).Distinct().ToList();

        return Cashiers;
    }

    public List<UserModelFirst> GetCashiersDonations()
    {
        var Cashiers = (from dpm in _context.DFLPOS_DONATIONS_PAYMENT_METHOD
                        join u in _context.USER_MASTER on dpm.userr_id equals u.emp_no
                        select new UserModelFirst
                        {
                            EmpNo = u.emp_no,
                            UserName = u.user_name,
                            Email = u.email,
                            FirstName = u.first_name,
                            LastName = u.last_name
                        }).Distinct().ToList();

        return Cashiers;
    }

    public List<UserModelFirst> GetCashiersDonationsSales()
    {
        var Cashiers = (from dpm in _context.DFLPOS_DONATION_SALE
                        join u in _context.USER_MASTER on dpm.userr_id equals u.emp_no
                        select new UserModelFirst
                        {
                            EmpNo = u.emp_no,
                            UserName = u.user_name,
                            Email = u.email,
                            FirstName = u.first_name,
                            LastName = u.last_name
                        }).Distinct().ToList();

        return Cashiers;
    }

    public List<UserModelFirst> GetCashiersCancellationsSales()
    {
        var Cashiers = (from c in _context.DFLPOS_CANCELLATION
                        join u in _context.USER_MASTER on c.cashier_id equals u.emp_no
                        select new UserModelFirst
                        {
                            EmpNo = u.emp_no,
                            UserName = u.user_name,
                            Email = u.email,
                            FirstName = u.first_name,
                            LastName = u.last_name
                        }).Distinct().ToList();

        return Cashiers;
    }

    public List<UserModelFirst> GetCashierBySales()
    {
        var Cashiers = (from c in _context.DFLPOS_SALES
                        join u in _context.USER_MASTER on c.emp_no equals u.emp_no
                        select new UserModelFirst
                        {
                            EmpNo = u.emp_no,
                            UserName = u.user_name,
                            Email = u.email,
                            FirstName = u.first_name,
                            LastName = u.last_name
                        }).Distinct().ToList();

        return Cashiers;
    }

    public List<USER_MASTER> GetUsersMasterByRol(List<string> roleIdList)
    {
        var users = (from srm in _context.SYS_ROLE_USER
                     where roleIdList.Contains(srm.role_id) && srm.SYS_ROLE_MASTER.role_id == srm.SYS_ROLE_MASTER.role_id && srm.USER_MASTER.emp_no == srm.emp_no
                     select srm.USER_MASTER).AsEnumerable().DistinctBy(p => p.emp_no).ToList();
        return users;
    }

    public bool IsInRole(string user_name, string role_id)
    {
        var any = (from ru in _context.SYS_ROLE_USER
                   join rm in _context.SYS_ROLE_MASTER on ru.role_id equals rm.role_id
                   join u in _context.USER_MASTER on ru.emp_no equals u.emp_no
                   where u.user_name == user_name
                         && rm.role_id == role_id
                   select u).Any();
        return any;

    }

    public string GetEmailByBuyerDivision(string buyer_division)
    {
        var emails = _context.USER_MASTER.Where(w => w.buyer_division == buyer_division).ToList();
        if (emails != null)
            return string.Join(",", emails.Select(s => s.email).ToArray());
        return "";
    }
}