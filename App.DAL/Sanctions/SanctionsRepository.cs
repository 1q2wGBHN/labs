﻿using App.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using App.Entities.ViewModels.Currency;
using App.Entities.ViewModels.Sanctions;
using System.Globalization;
using static App.Entities.ViewModels.Common.Constants;

namespace App.DAL.Sanctions
{
    public class SanctionsRepository
    {
        private DFL_SAIEntities _context;

        public SanctionsRepository()
        {
            _context = new DFL_SAIEntities();
        }
        public List<USER_MASTER> GetUsers()
        {
            return _context.USER_MASTER.Select(s => s).ToList();
        }
        public List<SANCTIONS_MOTIVES> GetMotives()
        {
            return _context.SANCTIONS_MOTIVES.Select(s => s).ToList();
        }
        public List<CURRENCY> GetCurrencyList()
        {
            var currencys = _context.CURRENCY.Select(s => s).ToList();
            if (currencys != null)
            {
                return currencys;
            }
            else
                return new List<CURRENCY>();
        }

        public SanctionModel GetSanctionById(long Id)
        {
            SANCTIONS sanc = _context.SANCTIONS.Where(s => s.sanction_id == Id).FirstOrDefault();
            var _sanction = new SanctionModel();
            _sanction.SanctionId = sanc.sanction_id;
            _sanction.Comments = sanc.comments;
            _sanction.CreateDate = sanc.sanction_date.Value.ToString(DateFormat.INT_DATE);
            _sanction.currencyValue = sanc.cur_cod;
            _sanction.CreateTime = sanc.santion_time;
            _sanction.emp = sanc.USER_MASTER.first_name + " " + sanc.USER_MASTER.last_name;
            _sanction.emp_no = Int32.Parse(sanc.emp_no);
            _sanction.IssueDate = sanc.sanction_date_app.Value.ToString(DateFormat.INT_DATE);
            if (sanc.status == 0)
                _sanction.statusVal = "Cancelada";
            if (sanc.status == 1)
                _sanction.statusVal = "Pagada";
            if (sanc.status == -1)
                _sanction.statusVal = "Pendiente";

            _sanction.status = sanc.status;

            _sanction.total = sanc.sanction_total.Value.ToString("0.00");
            _sanction.Motive = sanc.SANCTIONS_MOTIVES.desc;
            _sanction.MotiveNo = sanc.reason_no.ToString();

            return _sanction;
        }

        public List<SanctionModel> GetSantiosByModel(SanctionIndex model)
        {
            var predicate = PredicateBuilder.True<SANCTIONS>();
            if (!string.IsNullOrEmpty(model.CreateDate) && string.IsNullOrEmpty(model.CreateDate2))
            {
                DateTime DateStart = DateTime.ParseExact(model.CreateDate, DateFormat.INT_DATE, CultureInfo.InvariantCulture);

                //DateEnd = DateEnd.AddDays(1);
                predicate = predicate.And(c => c.sanction_date_app == DateStart);
            }
            if (!string.IsNullOrEmpty(model.CreateDate) && !string.IsNullOrEmpty(model.CreateDate2))
            {
                DateTime DateStart = DateTime.ParseExact(model.CreateDate, DateFormat.INT_DATE, CultureInfo.InvariantCulture);
                DateTime DateEnd = DateTime.ParseExact(model.CreateDate2, DateFormat.INT_DATE, CultureInfo.InvariantCulture);
                DateEnd = DateEnd.AddDays(1);
                predicate = predicate.And(c => c.sanction_date >= DateStart && c.sanction_date <= DateEnd);
            }
            if (!string.IsNullOrEmpty(model.emp))
            {
                predicate = predicate.And(c => c.emp_no == model.emp);
            }
            var _sanctionsFiltrated = _context.SANCTIONS.Where(predicate).ToList();
            var _listSanctions = new List<SanctionModel>();
            SanctionModel _sanction;
            foreach (SANCTIONS sanc in _sanctionsFiltrated)
            {
                _sanction = new SanctionModel();
                _sanction.SanctionId = sanc.sanction_id;
                _sanction.Comments = sanc.comments;
                _sanction.CreateDate = sanc.sanction_date.Value.ToString(DateFormat.INT_DATE);
                _sanction.currencyValue = sanc.cur_cod;
                _sanction.CreateTime = sanc.santion_time;
                _sanction.emp = sanc.USER_MASTER.first_name + " " + sanc.USER_MASTER.last_name;
                _sanction.IssueDate = sanc.sanction_date_app != null ? sanc.sanction_date_app.Value.ToString(DateFormat.INT_DATE) : sanc.sanction_date.Value.ToString(DateFormat.INT_DATE);
                if (sanc.status == 0)
                    _sanction.statusVal = "Cancelada";
                if (sanc.status == 1)
                    _sanction.statusVal = "Pagada";
                if (sanc.status == -1)
                    _sanction.statusVal = "Pendiente";
                _sanction.status = sanc.status;
                _sanction.total = sanc.sanction_total.Value.ToString("0.00");
                _sanction.Motive = sanc.SANCTIONS_MOTIVES.desc;
                _sanction.IsCancel = sanc.status == 0;
                _listSanctions.Add(_sanction);
            }

            return _listSanctions;
        }

        public bool CreateSanction(SANCTIONS sanction)
        {
            try
            {
                _context.SANCTIONS.Add(sanction);
                _context.SaveChanges();

                return true;
            }
            catch (Exception ex)
            {
                string msg = ex.Message;
                return false;
            }
        }

        public bool UpdateSanction(SANCTIONS Sanction, SYS_LOG SysLog)
        {
            var SanctionEdit = _context.SANCTIONS.Find(Sanction.sanction_id);
            if (SanctionEdit != null)
            {
                _context.Entry(SanctionEdit).State = System.Data.Entity.EntityState.Detached;
                _context.Entry(Sanction).State = System.Data.Entity.EntityState.Modified;
                _context.SaveChanges();
                if (SysLog != null)
                {
                    _context.SYS_LOG.Add(SysLog);
                    _context.SaveChanges();
                }
                return true;
            }
            return false;
        }

        public SANCTIONS GetSanctionEntityById(long SanctionId)
        {
            return _context.SANCTIONS.Find(SanctionId);
        }
    }


}
