﻿using App.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using App.Entities.ViewModels.CreditNotes;
using System.Globalization;
using System.Data.SqlClient;
using static App.Entities.ViewModels.Common.Constants;
using System.Data.Entity;
using App.Entities.ViewModels.Sales;

namespace App.DAL.CreditNotes
{
    public class CreditNotesDetailTaxRepository
    {
        private DFL_SAIEntities _context;

        public CreditNotesDetailTaxRepository()
        {
            _context = new DFL_SAIEntities();
        }

        public bool CreateCreditNoteTax (CREDIT_NOTE_DETAIL_TAX Item)
        {
            try
            {
                _context.CREDIT_NOTE_DETAIL_TAX.Add(Item);
                _context.SaveChanges();

                return true;
            }
            catch(Exception ex)
            {
                string msg = ex.Message;
                return false;
            }            
        }
      
        public bool DeleteDetailTaxByInvoices(long InvoiceNumber, string InvoiceSerie)
        {
            try
            {
                var Delete = _context.Database.ExecuteSqlCommand("DELETE cndt FROM CREDIT_NOTE_DETAIL_TAX cndt JOIN CREDIT_NOTE_DETAIL cnd ON cnd.detail_id = cndt.detail_id JOIN CREDIT_NOTES cn ON cn.cn_id = cnd.cn_id AND cn.cn_serie = cnd.cn_serie WHERE cn.invoice_no = @InvoiceNumber AND cn.invoice_serie = @InvoiceSerie",
                    new SqlParameter("InvoiceSerie", InvoiceSerie),
                    new SqlParameter("InvoiceNumber", InvoiceNumber));

                return true;
            }
            catch (Exception ex)
            {
                string msg = ex.Message;
                return false;
            }
        }

        public bool DeleteCreditNoteDetailTax(long Number, string Serie)
        {
            try
            {
                var Delete = _context.Database.ExecuteSqlCommand("DELETE cndt FROM CREDIT_NOTE_DETAIL_TAX cndt JOIN CREDIT_NOTE_DETAIL cnd ON cnd.detail_id = cndt.detail_id JOIN CREDIT_NOTES cn ON cn.cn_id = cnd.cn_id AND cn.cn_serie = cnd.cn_serie WHERE cn.cn_serie = @Serie AND cn.cn_id = @Number",
                    new SqlParameter("Serie", Serie),
                    new SqlParameter("Number", Number));

                return true;
            }
            catch (Exception ex)
            {
                string msg = ex.Message;
                return false;
            }
        }

        public List<CREDIT_NOTE_DETAIL_TAX> GetDetailTaxByNumber(long Number)
        {
            var TaxList = new List<CREDIT_NOTE_DETAIL_TAX>();
            TaxList = (from cndt in _context.CREDIT_NOTE_DETAIL_TAX
                       join cnd in _context.CREDIT_NOTE_DETAIL on cndt.detail_id equals cnd.detail_id
                       where cnd.cn_id == Number
                       select cndt).ToList();

            return TaxList;
        }

        public List<SalesIEPS> GetDetailTaxsByDate(string Date1, string Date2)
        {
            DateTime StartDate = DateTime.ParseExact(Date1, DateFormat.INT_DATE, CultureInfo.InvariantCulture);
            DateTime EndDate = DateTime.ParseExact(Date2, DateFormat.INT_DATE, CultureInfo.InvariantCulture);
            var TaxList = new List<SalesIEPS>();
            TaxList = (from cndt in _context.CREDIT_NOTE_DETAIL_TAX
                       join cnd in _context.CREDIT_NOTE_DETAIL on cndt.detail_id equals cnd.detail_id
                       join cn in _context.CREDIT_NOTES on new { cnd.cn_id, cnd.cn_serie } equals new { cn.cn_id, cn.cn_serie }
                       where DbFunctions.TruncateTime(cn.cn_date) >= StartDate && DbFunctions.TruncateTime(cn.cn_date) <= EndDate
                       select new SalesIEPS
                       {
                           tasa_ieps = cndt.MA_TAX.name,
                           venta = cnd.CREDIT_NOTE_DETAIL_TAX.Where(x => x.tax_code.Contains(TaxTypes.IEPS)).Any() ? (cnd.detail_price - cnd.CREDIT_NOTE_DETAIL_TAX.Where(x => x.tax_code.Contains(TaxTypes.IEPS)).FirstOrDefault().tax_amount) : cnd.detail_price,
                           importe_ieps = cnd.CREDIT_NOTE_DETAIL_TAX.Where(x => x.tax_code.Contains(TaxTypes.IEPS)).Any() ? cnd.CREDIT_NOTE_DETAIL_TAX.Where(x => x.tax_code.Contains(TaxTypes.IEPS)).FirstOrDefault().tax_amount : 0,
                           iva = cnd.CREDIT_NOTE_DETAIL_TAX.Where(x => x.tax_code.Contains(TaxTypes.TAX)).Any() ? cnd.CREDIT_NOTE_DETAIL_TAX.Where(x => x.tax_code.Contains(TaxTypes.TAX)).FirstOrDefault().tax_amount : 0,
                           total = cnd.CREDIT_NOTE_DETAIL_TAX.Where(x => x.tax_code.Contains(TaxTypes.TAX)).Any() ? cnd.detail_price + cnd.CREDIT_NOTE_DETAIL_TAX.Where(x => x.tax_code.Contains(TaxTypes.TAX)).FirstOrDefault().tax_amount : cnd.detail_price
                       }).ToList();

            return TaxList;
        }
    }
}
