﻿using App.DAL.DigitalTaxStamp;
using App.DAL.Site;
using App.Entities;
using App.Entities.ViewModels.Common;
using App.Entities.ViewModels.CreditNotes;
using App.Entities.ViewModels.Sales;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Core.Objects;
using System.Globalization;
using System.Linq;
using System.Linq.Expressions;
using static App.Entities.ViewModels.Common.Constants;

namespace App.DAL.CreditNotes
{
    public class CreditNotesRepository
    {
        private DFL_SAIEntities _context;
        private DigitalTaxStampRepository _DigitalTaxStampRepository;
        private SiteConfigRepository _SiteConfigRepository;

        public CreditNotesRepository()
        {
            _context = new DFL_SAIEntities();
            _DigitalTaxStampRepository = new DigitalTaxStampRepository();
            _SiteConfigRepository = new SiteConfigRepository();
        }

        public bool CreateCreditNote(CREDIT_NOTES Item)
        {
            try
            {
                _context.CREDIT_NOTES.Add(Item);
                _context.SaveChanges();

                //Update Internal Number and Number in DB from Reference Number Table.
                ObjectParameter Reference = new ObjectParameter("Reference", typeof(string));
                _context.sp_Get_Reference_Number("CREDIT_NOTES", "ALL", Reference);

                return true;
            }
            catch (Exception ex)
            {
                string msg = ex.Message;
                return false;
            }
        }

        public bool UpdateStock(CreditNote Model, bool IsCancel)
        {
            try
            {
                var siteCode = _SiteConfigRepository.GetSiteCode();
                ObjectParameter Document = new ObjectParameter("document", typeof(string));
                if (!Model.IsBonification)
                {
                    if (!IsCancel)
                        _context.sproc_credit_notes(siteCode, Model.Number, true, Model.User, "CRNO002.cshtml", Document);
                    else
                        _context.sproc_credit_notes(siteCode, Model.Number, false, Model.User, "CRNO003.cshtml", Document);                    
                }

                return true;
            }
            catch (Exception ex)
            {
                string msg = ex.Message;
                return false;
            }
        }

        public decimal TotalBonificationInvoice(long InvoiceNumber)
        {
            decimal TotalBonificationValue = 0;

            var Bonifications = (from cn in _context.CREDIT_NOTES
                                 join cnd in _context.CREDIT_NOTE_DETAIL on new { cn.cn_serie, cn.cn_id } equals new { cnd.cn_serie, cnd.cn_id }
                                 where cn.invoice_no == InvoiceNumber
                                       && cnd.part_number == GenericItem.ITEM_NUMBER
                                 select new
                                 {
                                     Total = cn.cn_total
                                 }).ToList();

            TotalBonificationValue = Bonifications.Sum(c => c.Total);

            return TotalBonificationValue;
        }

        public decimal TotalCreditNoteInvoice(long InvoiceNumber)
        {
            decimal TotalCreditNoteValue = 0;

            var CreditNotes = (from cn in _context.CREDIT_NOTES
                                 join cnd in _context.CREDIT_NOTE_DETAIL on new { cn.cn_serie, cn.cn_id } equals new { cnd.cn_serie, cnd.cn_id }
                                 where cn.invoice_no == InvoiceNumber
                                       && cnd.part_number != GenericItem.ITEM_NUMBER
                                 select new
                                 {
                                     Total = cn.cn_total
                                 }).ToList();

            TotalCreditNoteValue = CreditNotes.Sum(c => c.Total);

            return TotalCreditNoteValue;
        }

        public decimal TotalBonificationSales(long SaleNumber)
        {
            decimal TotalBonificationValue = 0;
            var Bonifications = (from cn in _context.CREDIT_NOTES
                                 join cnd in _context.CREDIT_NOTE_DETAIL on new { cn.cn_serie, cn.cn_id } equals new { cnd.cn_serie, cnd.cn_id }
                                 where cn.invoice_no == SaleNumber
                                       && cnd.part_number == GenericItem.ITEM_NUMBER
                                 select new
                                 {
                                     Total = cn.cn_total
                                 }).ToList();

            TotalBonificationValue = Bonifications.Sum(c => c.Total);

            return TotalBonificationValue;
        }

        /*REPORTES*/
        public List<CreditNotesBon> GetCreNotesBonPre(CreditNotesBonIndex Model) //recibe el modelo index y lo filtra en base a los valores contenidos en todos los campos
        {
            var predicate = PredicateBuilder.True<CREDIT_NOTES>();
            var status = false;
            if (Model.Status == CreditNoteStatus.Active)
                status = true;

            //* X NUMERO
            if (!string.IsNullOrEmpty(Model.CreNoBoId))
            {
                var id = long.Parse(Model.CreNoBoId);
                predicate = predicate.And(c => c.cn_id == id);
            }
            // * X FECHAS
            if (!string.IsNullOrEmpty(Model.StartDate) && !string.IsNullOrEmpty(Model.EndDate))
            {
                DateTime DateStart = DateTime.ParseExact(Model.StartDate, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                DateTime DateEnd = DateTime.ParseExact(Model.EndDate, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                DateEnd = DateEnd.AddHours(23);
                DateEnd = DateEnd.AddMinutes(59);
                DateEnd = DateEnd.AddSeconds(59);
                DateEnd = DateEnd.AddMilliseconds(999);
                predicate = predicate.And(c => c.cn_date >= DateStart && c.cn_date <= DateEnd);
            }
            // * X MONEDA
            //if (Model.Currency != Currencys.ALL)
            //{
                var moneda = "";
                if (Model.Currency == Currencys.MXN)
                    moneda = "MXN";
                else
                    moneda = "USD";
                predicate = predicate.And(c => c.cur_code == moneda);
            //}
            // * X STATUS
            if (Model.Status != CreditNoteStatus.All)
            {
                predicate = predicate.And(c => c.cn_status == status);
            }
            // * X TIPO
            if (Model.Type != CreditNoteTypesReports.All)
            {
                if (Model.Type == CreditNoteTypesReports.CreditNote)
                    predicate = predicate.And(c => (_context.CREDIT_NOTE_DETAIL.Where(d => !d.part_number.Equals(GenericItem.ITEM_NUMBER)).Select(s => s.cn_id)).ToList().Contains(c.cn_id));
                else
                    predicate = predicate.And(c => (_context.CREDIT_NOTE_DETAIL.Where(d => d.part_number.Equals(GenericItem.ITEM_NUMBER)).Select(s => s.cn_id)).ToList().Contains(c.cn_id));
            }
            // * x UUID
            if (Model.Uuid != CreditNoteStatusFiscal.All)
            {
                if (Model.Uuid == CreditNoteStatusFiscal.SinTimbre)
                    predicate = predicate.And(c => !(_context.DIGITAL_TAX_STAMP.Where(d => d.cfdi_id == c.cfdi_id && d.cfdi_serie == c.cn_serie && d.sat_document_code == VoucherType.EXPENSES).
                                                    Select(an => new { an.cfdi_id, an.cfdi_serie })).ToList().Contains(new { c.cfdi_id, cfdi_serie = c.cn_serie }));
                else
                    predicate = predicate.And(c => (_context.DIGITAL_TAX_STAMP.Where(d => d.cfdi_id == c.cfdi_id && d.cfdi_serie == c.cn_serie && d.sat_document_code == VoucherType.EXPENSES).
                                                   Select(an => new { an.cfdi_id, an.cfdi_serie })).ToList().Contains(new { c.cfdi_id, cfdi_serie = c.cn_serie }));
            }

            return GetCreditNotesBoniList(predicate);
        }

        public List<CREDIT_NOTES> GetCreditNotes(Expression<Func<CREDIT_NOTES, bool>> predicate)
        {

            var cnbl = new List<CREDIT_NOTES>();
            cnbl = _context.CREDIT_NOTES.Where(predicate).ToList();
            return cnbl;
        }

        public List<CreditNotesBon> GetCreditNotesBoniList(Expression<Func<CREDIT_NOTES, bool>> predicate)
        {
            var cnbl = _context.CREDIT_NOTES.Where(predicate).ToList();
            //listIeps.Where(s => s.sale_id == x.sale_id).Select(c => (c.value/100) * c.price).Sum()
            long minId = 0;
            long maxId = 0;
            var listIeps = new List<IepsModel>();
            CreditNotesBon cnb;
            List<CreditNotesBon> creditnotesBonList = new List<CreditNotesBon>();
            if (cnbl.Count > 0)
            {
                minId = cnbl.Min(c => c.cn_id);
                maxId = cnbl.Max(c => c.cn_id);
                listIeps = GetIepsFromCreditNotesDetailTax(minId, maxId);
                foreach (CREDIT_NOTES crnb in cnbl)
                {
                    cnb = new CreditNotesBon();
                    var ieps = listIeps.Where(s => s.sale_id == crnb.cn_id).Select(c => c.value).Sum();
                    cnb.cnId = crnb.cn_id;
                    cnb.CfdNotaNo = crnb.cn_serie + crnb.cfdi_id;
                    cnb.Type = crnb.CREDIT_NOTE_DETAIL.Where(s => s.cn_id == crnb.cn_id).Select(s => s.part_number).FirstOrDefault() == GenericItem.ITEM_NUMBER ? "BONIFICACION" : "NOTA DE CREDITO";
                    cnb.Date = crnb.cn_date.ToString(DateFormat.INT_DATE);
                    cnb.Cause = crnb.cn_cause;
                    cnb.InvId = crnb.invoice_no ?? 0;
                    cnb.Customer = crnb.CUSTOMERS.customer_name;
                    cnb.Import = crnb.cn_total - crnb.cn_tax - ieps;//restar IEPS
                    cnb.IVA = crnb.cn_tax;
                    cnb.IEPS = ieps; // calcular IEPS
                    cnb.Total = crnb.cn_total;
                    cnb.User = crnb.cuser;
                    cnb.Status = crnb.cn_status ? "ACTIVA" : "CANCELADA";
                    cnb.Uuid = _context.DIGITAL_TAX_STAMP.Where(d => d.cfdi_id == crnb.cfdi_id && d.cfdi_serie == crnb.cn_serie && d.sat_document_code == VoucherType.EXPENSES).Select(s => s.UUID).FirstOrDefault() == null ? "SIN TIMBRAR" :
                    _context.DIGITAL_TAX_STAMP.Where(d => d.cfdi_id == crnb.cfdi_id && d.cfdi_serie == crnb.cn_serie).Select(s => s.UUID).FirstOrDefault();// de la tabla digital_tax_stam donde el sat_document_code sea 'E' 
                    cnb.Uuid = cnb.Uuid != "SIN TIMBRAR" ? cnb.Uuid.Substring(cnb.Uuid.Length - 10) : cnb.Uuid = cnb.Uuid;
                    cnb.xml = crnb.xml;
                    cnb.HasXmlFile = !string.IsNullOrEmpty(crnb.xml);
                    cnb.CanEditDate = CanEditDate(crnb.cn_status, crnb.cn_date);
                    creditnotesBonList.Add(cnb);
                }
            }

            return creditnotesBonList;
        }

        public List<SalesDetailReportModel> GetCreditNotesBonDetailsByCrediNoteId(long crediNoteId)
        {
            var _salesdETAIL = (from cnd in _context.CREDIT_NOTE_DETAIL
                                where cnd.cn_id == crediNoteId
                                select new
                                {
                                    id = cnd.detail_id,
                                    part_number = cnd.part_number,
                                    part_name = cnd.description,
                                    part_quantity = cnd.detail_qty,
                                    part_price = cnd.detail_price,//-((decimal)(mt.tax_value / 100) * sld.detail_captured),
                                    part_importe = 0,
                                    part_iva = 0,
                                    part_iva_percentage = 0,//(sld.part_tax / (sld.part_price != 0?  sld.part_price : 1))*100,
                                    part_ieps = 0,// (decimal)(mt.tax_value / 100) * (decimal)sld.detail_captured,
                                    part_ieps_percentage = 0//(int)(mt.tax_value)
                                }).AsEnumerable().
                                Select(x => new SalesDetailReportModel
                                {

                                    part_number = x.part_number,
                                    part_name = x.part_name,
                                    part_quantity = x.part_quantity,
                                    part_price = x.part_price,
                                    part_importe = x.part_importe,
                                    part_iva = GetIvaIepsCreditNotBoni(x.id, TaxTypes.TAX),
                                    part_iva_percentage = 0,//(GetIvaIepsCreditNotBoni(crediNoteId, "IVA")/(x.part_price!=0? x.part_price:1))*100,
                                    part_ieps = GetIvaIepsCreditNotBoni(x.id, TaxTypes.IEPS),
                                    part_ieps_percentage = 0//Int32.Parse(((GetIvaIepsCreditNotBoni(crediNoteId, "IVA") / (x.part_price != 0 ? x.part_price : 1)) * 100).ToString())

                                }).ToList();
            if (_salesdETAIL != null)
                return _salesdETAIL;
            else
                return new List<SalesDetailReportModel>();
        }

        private decimal GetIvaIepsCreditNotBoni(long cn, string ivaIeps)
        {
            return _context.CREDIT_NOTE_DETAIL_TAX.Where(s => s.detail_id == cn && s.tax_code.Contains(ivaIeps)).Select(s => s.tax_amount) == null ? 0 :
                                      _context.CREDIT_NOTE_DETAIL_TAX.Where(s => s.detail_id == cn && s.tax_code.Contains(ivaIeps)).Select(s => s.tax_amount).FirstOrDefault();

        }
        
        public List<IepsModel> GetIepsFromCreditNotesDetailTax(long cnIdMin, long cnIdMax)
        {
            var iepsr = (from cn in _context.CREDIT_NOTES
                         join cnd in _context.CREDIT_NOTE_DETAIL on cn.cn_id equals cnd.cn_id
                         join cndt in _context.CREDIT_NOTE_DETAIL_TAX on cnd.detail_id equals cndt.detail_id
                         where cn.cn_id >= cnIdMin && cn.cn_id <= cnIdMax && cndt.tax_code.Contains(TaxTypes.IEPS)
                         select new IepsModel
                         {
                             value = cndt.tax_amount,
                             price = 0,
                             sale_id = cn.cn_id
                         }).ToList();

            return iepsr;

        }

        public CreditNoteDetails GetCreditNoteDetailsByNumber(long CreditNoteNumber)
        {
            var Document = new CreditNoteDetails();
            Document = (from cn in _context.CREDIT_NOTES
                        join dts in _context.DIGITAL_TAX_STAMP on new { Number = cn.cfdi_id, Serie = cn.cn_serie } equals new { Number = dts.cfdi_id, Serie = dts.cfdi_serie }
                        where cn.cfdi_id == CreditNoteNumber
                              && dts.sat_document_code == VoucherType.EXPENSES
                        select new
                        {
                            Serie = cn.cn_serie,
                            Number = cn.cfdi_id,
                            Date = cn.cn_date,
                            Total = cn.cn_total,
                            Tax = cn.cn_tax,
                            InvoiceNumber = cn.invoice_no,
                            InvoiceSerie = cn.invoice_serie,
                            ClientName = cn.CUSTOMERS.customer_name,
                            DocumentType = dts.sat_document_code,
                            Uuid = dts.UUID
                        }).AsEnumerable().Select(c => new CreditNoteDetails
                        {
                            CreditNote = string.Format("{0}{1}", c.Serie, c.Number),
                            IssueDate = c.Date.ToString(DateFormat.INT_DATE),
                            Total = c.Total,
                            Tax = c.Tax,
                            Invoice = string.Format("{0}{1}", c.InvoiceSerie, c.InvoiceNumber),
                            ClientName = c.ClientName,
                            Serie = c.Serie,
                            Number = c.Number,
                            DocumentType = c.DocumentType,
                            Uuid = c.Uuid
                        }).FirstOrDefault();

            if (Document == null)
                Document = (from cn in _context.CREDIT_NOTES
                            where cn.cfdi_id == CreditNoteNumber
                            select new
                            {
                                Serie = cn.cn_serie,
                                Number = cn.cfdi_id,
                                Date = cn.cn_date,
                                Total = cn.cn_total,
                                Tax = cn.cn_tax,
                                InvoiceNumber = cn.invoice_no,
                                InvoiceSerie = cn.invoice_serie,
                                ClientName = cn.CUSTOMERS.customer_name,
                            }).AsEnumerable().Select(c => new CreditNoteDetails
                            {
                                CreditNote = string.Format("{0}{1}", c.Serie, c.Number),
                                IssueDate = c.Date.ToString(DateFormat.INT_DATE),
                                Total = c.Total,
                                Tax = c.Tax,
                                Invoice = string.Format("{0}{1}", c.InvoiceSerie, c.InvoiceNumber),
                                ClientName = c.ClientName,
                                Serie = c.Serie,
                                Number = c.Number,
                                DocumentType = VoucherType.EXPENSES,
                                Uuid = string.Empty
                            }).FirstOrDefault();

            return Document;
        }

        public bool IsDocumentActive(long Number)
        {
            bool IsActive = _context.CREDIT_NOTES.First(c => c.cn_id == Number).cn_status;
            return IsActive;
        }

        public bool CreditNoteExist(long Number)
        {
            var exist = (from cn in _context.CREDIT_NOTES
                         join cnd in _context.CREDIT_NOTE_DETAIL on new { cn.cn_id, cn.cn_serie } equals new { cnd.cn_id, cnd.cn_serie }
                         where cn.cn_id == Number
                               && cnd.part_number != GenericItem.ITEM_NUMBER
                         select cn).FirstOrDefault();
            if (exist != null)
                return true;

            return false;
        }

        public bool UpdateCreditNote(CREDIT_NOTES Document)
        {
            var DocumentEdit = _context.CREDIT_NOTES.FirstOrDefault(i => i.cn_id == Document.cn_id);
            if (DocumentEdit != null)
            {
                _context.Entry(DocumentEdit).State = EntityState.Detached;
                _context.Entry(Document).State = EntityState.Modified;
                _context.SaveChanges();
                return true;
            }
            return false;
        }

        public CREDIT_NOTES GetCreditNoteByNumber(long Number)
        {
            return _context.CREDIT_NOTES.FirstOrDefault(c => c.cn_id == Number);
        }

        public SendDocumentModel GetSendDocumentByNumberCreditNoteBoni(long Number)
        {
            var response = new SendDocumentModel();
            response = ((from inv in _context.CREDIT_NOTES
                         join dts in _context.DIGITAL_TAX_STAMP on new { cfdi = inv.cfdi_id, serie = inv.invoice_serie } equals new { cfdi = dts.cfdi_id, serie = dts.cfdi_serie }
                         where inv.cfdi_id == Number && dts.sat_document_code == VoucherType.EXPENSES

                         select new SendDocumentModel
                         {
                             Customer = inv.CUSTOMERS.customer_name,
                             EmailCustomerSend = inv.CUSTOMERS.customer_email,
                             FolioFiscal = dts.UUID.Substring(dts.UUID.Length - 10, dts.UUID.Length),
                             Date = inv.cn_date.ToString(),
                             EmailCustomer = inv.CUSTOMERS.customer_email,
                             Number = inv.cfdi_id.ToString(),
                             Invoice = inv.invoice_serie + inv.cfdi_id,
                             Serie = inv.invoice_serie,
                             Total = inv.cn_total.ToString(),
                             DocumentType = dts.sat_document_code
                         })).FirstOrDefault();

            return response;
        }

        public SendDocumentModel GetCreditBon(long Number)
        {
            var Document = new SendDocumentModel();
            Document = (from cn in _context.CREDIT_NOTES
                        where cn.cn_id == Number
                        select new
                        {
                            Invoice = cn.cn_id,
                            Serie = cn.cn_serie,
                            Customer = cn.CUSTOMERS.customer_name,
                            EmailCustomer = cn.CUSTOMERS.customer_email,
                            Date = cn.cn_date,
                            DocumentType = VoucherType.EXPENSES,
                            Number = cn.cn_id,
                            Total = cn.cn_total,
                            cn.xml
                        }).AsEnumerable().Select(c => new SendDocumentModel
                        {
                            Invoice = c.Invoice.ToString(),
                            Serie = c.Serie,
                            Customer = c.Customer,
                            EmailCustomer = c.EmailCustomer,
                            Date = c.Date.ToString(DateFormat.INT_DATE),
                            DocumentType = c.DocumentType,
                            Number = c.Number.ToString(),
                            Total = c.Total.ToString(),
                            Document = Documents.CNOT,
                            //IsStamped = string.IsNullOrEmpty(c.xml) ? false : true
                        }).FirstOrDefault();

            Document.IsStamped = _DigitalTaxStampRepository.IsCreditNoteBonStamped(Number);
            return Document;
        }

        public bool CheckEmployeeBonificationByDate(string DateValue)
        {
            DateTime Date = DateTime.ParseExact(DateValue, DateFormat.INT_DATE, CultureInfo.InvariantCulture);
            var Exist = (from cn in _context.CREDIT_NOTES
                         join i in _context.INVOICES on new { Invoice = cn.invoice_no.Value, cn.invoice_serie } equals new { Invoice = i.invoice_no, i.invoice_serie }
                         join cndt in _context.CREDIT_NOTE_DETAIL on cn.cn_id equals cndt.cn_id
                         where DbFunctions.TruncateTime(cn.cn_date) == Date
                               && i.customer_code == GenericCustomer.CODE
                               && cn.is_employee_bon
                               && cn.cn_status
                         select cn).ToList();

            if (Exist.Any())
                return true;

            return false;
        }

        public bool DeleteCreditNoteByNumber(long CreditNoteNumber, string CreditNoteSerie)
        {
            try
            {
                var CreditNote = _context.CREDIT_NOTES.Where(c => c.cn_id == CreditNoteNumber && c.cn_serie == CreditNoteSerie).FirstOrDefault();

                if (CreditNote != null)
                {
                    _context.CREDIT_NOTES.Remove(CreditNote);
                    _context.SaveChanges();
                }

                return true;
            }
            catch (Exception ex)
            {
                string msg = ex.Message;
                return false;
            }
        }

        public bool DeleteCreditNoteByInvoice(long InvoiceNumber, string InvoiceSerie)
        {
            try
            {
                var CreditNote = _context.CREDIT_NOTES.Where(c => c.invoice_no.Value == InvoiceNumber && c.invoice_serie == InvoiceSerie).FirstOrDefault();

                if (CreditNote != null)
                {
                    _context.CREDIT_NOTES.Remove(CreditNote);
                    _context.SaveChanges();
                }

                return true;
            }
            catch (Exception ex)
            {
                string msg = ex.Message;
                return false;
            }
        }


        public bool CanEditDate(bool Status, DateTime DocDate)
        {
            bool CanEditDate = false;
            if(Status)
            {
                var Date = DateTime.Now.Date;
                var firstDayOfMonth = new DateTime(Date.Year, Date.Month, 1);
                var lastDayOfMonth = firstDayOfMonth.AddMonths(1).AddDays(-1);
                CanEditDate = DocDate >= firstDayOfMonth && DocDate <= lastDayOfMonth;
            }

            return CanEditDate;
        }

        public bool SprocUpdateCreditNoteNumber()
        {
            DFL_SAIEntities context = new DFL_SAIEntities();
            context.Configuration.AutoDetectChangesEnabled = false;
            try
            {                
                //Update Internal Number and Number in DB from Reference Number Table.
                ObjectParameter Reference = new ObjectParameter("Reference", typeof(string));
                _context.sp_Get_Reference_Number("CREDIT_NOTES", "ALL", Reference);
                return true;
            }
            catch (Exception ex)
            {
                string msg = ex.Message;
                return false;
            }
            finally
            {
                if (context != null)
                {
                    context.Configuration.AutoDetectChangesEnabled = true;
                    context.Dispose();
                }
            }

        }

        public bool DocumentExist(long Number)
        {
            return _context.CREDIT_NOTES.Where(c => c.cn_id == Number).Any();
        }
    }
}
