﻿using App.Entities;
using App.Entities.ViewModels.CreditNotes;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using static App.Entities.ViewModels.Common.Constants;

namespace App.DAL.CreditNotes
{
    public class CreditNotesDetailRepository
    {
        private DFL_SAIEntities _context;

        public CreditNotesDetailRepository()
        {
            _context = new DFL_SAIEntities();
        }

        public bool CreateCreditNoteDetail(CREDIT_NOTE_DETAIL Item)
        {
            try
            {
                _context.CREDIT_NOTE_DETAIL.Add(Item);
                _context.SaveChanges();
                return true;
            }
            catch(Exception ex)
            {
                string msg = ex.Message;
                return false;
            }
        }

        public long GetCreditNoteDetailId(string PartNumber, string CreditNoteSerie, long CreditNoteNumber, long? SaleDetailId, long? InvoiceDetailId, string PartNumberSale)
        {
            long DetailId = 0;
            if (SaleDetailId.HasValue)
                DetailId = _context.CREDIT_NOTE_DETAIL.First(c => c.sale_detail_id == SaleDetailId.Value && c.part_number == PartNumber && c.part_number_sale == PartNumberSale).detail_id;
            else if (InvoiceDetailId.HasValue)
                DetailId = _context.CREDIT_NOTE_DETAIL.First(c => c.invoice_detail_id == InvoiceDetailId.Value && c.cn_id == CreditNoteNumber && c.cn_serie == CreditNoteSerie).detail_id;
            else
                DetailId = _context.CREDIT_NOTE_DETAIL.First(c => c.part_number == PartNumber && c.cn_id == CreditNoteNumber && c.cn_serie == CreditNoteSerie).detail_id;
            
            return DetailId;
        }

        public List<CreditNoteDetailsProducts> GetCreditNoteProductsDetailsByNumber(long CreditNoteNumber)
        {
            var ProductDetails = new List<CreditNoteDetailsProducts>();
            ProductDetails = (from cn in _context.CREDIT_NOTES
                                  join cnd in _context.CREDIT_NOTE_DETAIL on new { cn.cn_id, cn.cn_serie } equals new { cnd.cn_id, cnd.cn_serie }
                                  where cn.cn_id == CreditNoteNumber
                                  select new CreditNoteDetailsProducts
                                  {
                                      ItemNumber = cnd.part_number,
                                      ItemName = cnd.description,
                                      Quantity = cnd.detail_qty,
                                      ItemPrice = cnd.detail_qty > 0 ? cnd.detail_price / cnd.detail_qty : 0,
                                      TotalItemPrice = cnd.detail_price,
                                      InoviceDetailId = cnd.invoice_detail_id.HasValue ? cnd.invoice_detail_id.Value : 0
                                  }).ToList();

            return ProductDetails;
        }

        public bool BonificationExist(long BonificationNumber)
        {
            var exist = _context.CREDIT_NOTE_DETAIL.Where(c => c.cn_id == BonificationNumber && c.part_number == GenericItem.ITEM_NUMBER).FirstOrDefault();
            if (exist != null)
                return true;

            return false;
        }

        public CREDIT_NOTE_DETAIL GetBonificationByNumber(long BonificationNumber)
        {
            var Bonification = _context.CREDIT_NOTE_DETAIL.Where(c => c.cn_id == BonificationNumber && c.part_number == GenericItem.ITEM_NUMBER).FirstOrDefault();
            return Bonification;
        }

        public bool DeleteCreditNoteDetailsByInvoice(long InvoiceNumber, string InvoiceSerie)
        {
            try
            {
                var CreditNoteDetail = _context.CREDIT_NOTE_DETAIL.Where(c => c.CREDIT_NOTES.invoice_no == InvoiceNumber && c.CREDIT_NOTES.invoice_serie == InvoiceSerie).FirstOrDefault();

                if (CreditNoteDetail != null)
                {
                    _context.CREDIT_NOTE_DETAIL.Remove(CreditNoteDetail);
                    _context.SaveChanges();                    
                }

                return true;
            }
            catch (Exception ex)
            {
                string msg = ex.Message;
                return false;
            }
        }

        public bool DeleteCreditNoteDetails(long Number, string Serie)
        {
            try
            {
                _context.Database.ExecuteSqlCommand("DELETE FROM CREDIT_NOTE_DETAIL WHERE cn_id = @Number AND cn_serie = @Serie",
                    new SqlParameter("Number", Number),
                    new SqlParameter("Serie", Serie));

                return true;
            }
            catch (Exception ex)
            {
                string msg = ex.Message;
                return false;
            }
        }
    }
}
