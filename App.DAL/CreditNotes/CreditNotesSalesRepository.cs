﻿using App.Entities;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App.DAL.CreditNotes
{
    public class CreditNotesSalesRepository
    {
        private DFL_SAIEntities _context;

        public CreditNotesSalesRepository()
        {
            _context = new DFL_SAIEntities();
        }

        public bool CreateCreditNoteSales(CREDIT_NOTES_SALES Item)
        {
            try
            {
                _context.CREDIT_NOTES_SALES.Add(Item);
                _context.SaveChanges();

                return true;
            }
            catch(Exception ex)
            {
                string msg = ex.Message;
                return false;
            }
        }

        public bool DeleteCreditNoteSalesDetails(long SaleNumber)
        {
            try
            {
                _context.Database.ExecuteSqlCommand("DELETE FROM CREDIT_NOTES_SALES WHERE sale_id = @Number",
                    new SqlParameter("Number", SaleNumber));

                return true;
            }
            catch (Exception ex)
            {
                string msg = ex.Message;
                return false;
            }
        }
    }
}
