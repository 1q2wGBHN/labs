﻿using App.DAL.Item;
using App.DAL.Transfer;
using App.Entities;
using App.Entities.ViewModels.Transfer;
using App.GlobalEntities;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.SqlClient;
using System.Linq;

namespace App.DAL.Tranfer
{
    public class TransferHeaderRepository
    {
        private readonly DFL_SAIEntities _context;
        private readonly GlobalERPEntities _contextGlobal;
        private readonly ItemRepository _itemRepository;
        private readonly ItemSupplierRepository _itemSupplierRepository;

        public TransferHeaderRepository()
        {
            _itemRepository = new ItemRepository();
            _context = new DFL_SAIEntities();
            _contextGlobal = new GlobalERPEntities();
            _itemSupplierRepository = new ItemSupplierRepository();
        }

        private string sc;
        private string thisSiteCode
        {
            get
            {
                if (sc != null) return sc;
                sc = _context.SITE_CONFIG.Select(e => e.site_code).First();
                return sc;
            }
        }

        public bool AddTransfer_G(TRANSFER_HEADER_G transfer, List<TRANSFER_DETAIL_G> details, bool new_erp)
        {
            foreach (var d in details)
            {
                d.transfer_document = transfer.transfer_document;
                d.transfer_site_code = transfer.transfer_site_code;
                d.destination_site_code = transfer.destination_site_code;
                d.is_sync = true;
            }

            transfer.is_sync = true;
            transfer.new_transfer_type = true;
            transfer.TRANSFER_DETAIL = details;
            try
            {
                _contextGlobal.TRANSFER_HEADER.Add(transfer);
                _contextGlobal.SaveChanges();
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public List<TransferHeaderModel> GetAllTransferHeader(DateTime DateInit, DateTime DateFin)
        {
            var global = _contextGlobal.TRANSFER_HEADER
                .Where(e => e.transfer_status == 0 
                            && e.transfer_site_code == thisSiteCode 
                            && DbFunctions.TruncateTime(e.transfer_date) >= DbFunctions.TruncateTime(DateInit) 
                            && DbFunctions.TruncateTime(e.transfer_date) <= DbFunctions.TruncateTime(DateFin))
                .Select(e => new TransferHeaderModel
                {
                    Comment = e.comment,
                    DriverCode = e.driver_code,
                    TransferDateD = e.transfer_date,
                    TransferDocument = e.transfer_document,
                    TransferSiteCode = e.transfer_site_code,
                    TransferStatus = e.transfer_status,
                    TransferSiteName = e.SITES.site_name,
                    PrintStatus = e.print_status,
                    TransferSiteAddress = e.SITES.address,
                    DestinationSiteAddress = e.SITES1.address,
                    DestinationSiteCode = e.SITES1.site_code,
                    DestinationSiteName = e.SITES1.site_name
                }).ToList();

            foreach (var t in global)
                t.TransferDate = t.TransferDateD.ToString("MM/dd/yyyy");

            return global.OrderBy(e => e.TransferDocument).ToList();
        }

        public TransferHeaderModel GetTransferHeaderDocumentGI(string Document, string TransferSiteCode)
        {
            var transferHeader = _contextGlobal.TRANSFER_HEADER
                .Join(_contextGlobal.SITES, site => site.transfer_site_code, siteName => siteName.site_code, (site, siteName) => new { site, siteName })
                .Join(_contextGlobal.SITES, site2 => site2.site.destination_site_code, siteName2 => siteName2.site_code, (site2, siteName2) => new { site2, siteName2 })
                .Where(elements => elements.site2.site.transfer_document == Document && elements.site2.site.transfer_site_code == TransferSiteCode)
                .Select(elements => new TransferHeaderModel
                {
                    Comment = elements.site2.site.comment,
                    DriverCode = elements.site2.site.driver_code,
                    TransferDate = DbFunctions.Right("00" + elements.site2.site.transfer_date.Day, 2) + "/" + DbFunctions.Right("00" + elements.site2.site.transfer_date.Month, 2) + "/" + DbFunctions.Right("00" + elements.site2.site.transfer_date.Year, 4),
                    TransferDocument = elements.site2.site.transfer_document,
                    TransferSiteCode = elements.site2.site.transfer_site_code,
                    TransferStatus = elements.site2.site.transfer_status,
                    TransferSiteName = elements.site2.siteName.site_name,
                    PrintStatus = elements.site2.site.print_status,
                    TransferSiteAddress = elements.site2.siteName.address,
                    CUser = elements.site2.site.cuser,
                    UUser = elements.site2.site.authorized_by,
                    DestinationSiteAddress = elements.siteName2.address,
                    DestinationSiteCode = elements.siteName2.site_code,
                    DestinationSiteName = elements.siteName2.site_name
                }).SingleOrDefault();

            if (transferHeader.DriverCode != null)
                transferHeader.DriverName = _contextGlobal.Database.SqlQuery<string>(@"select full_name from USER_SPECS where emp_no = @EmpoNO", new SqlParameter("@EmpoNO", transferHeader.DriverCode)).SingleOrDefault();
            else
                transferHeader.DriverName = "N/A";

            return transferHeader;
        }
        public TransferHeaderModel GetTransferHeaderDocumentGR(string Document, string destinationSite)
        {
            var transferHeader = _contextGlobal.TRANSFER_HEADER
                .Join(_contextGlobal.SITES, site => site.transfer_site_code, siteName => siteName.site_code, (site, siteName) => new { site, siteName })
                .Join(_contextGlobal.SITES, site2 => site2.site.destination_site_code, siteName2 => siteName2.site_code, (site2, siteName2) => new { site2, siteName2 })
                .Where(elements => elements.site2.site.transfer_document == Document && elements.site2.site.destination_site_code == destinationSite)
                .Select(elements => new TransferHeaderModel
                {
                    Comment = elements.site2.site.comment,
                    DriverCode = elements.site2.site.driver_code,
                    TransferDate = DbFunctions.Right("00" + elements.site2.site.transfer_date.Day, 2) + "/" + DbFunctions.Right("00" + elements.site2.site.transfer_date.Month, 2) + "/" + DbFunctions.Right("00" + elements.site2.site.transfer_date.Year, 4),
                    TransferDocument = elements.site2.site.transfer_document,
                    TransferSiteCode = elements.site2.site.transfer_site_code,
                    TransferStatus = elements.site2.site.transfer_status,
                    TransferSiteName = elements.site2.siteName.site_name,
                    PrintStatus = elements.site2.site.print_status,
                    TransferSiteAddress = elements.site2.siteName.address,
                    CUser = elements.site2.site.cuser,
                    UUser = elements.site2.site.authorized_by,
                    DestinationSiteAddress = elements.siteName2.address,
                    DestinationSiteCode = elements.siteName2.site_code,
                    DestinationSiteName = elements.siteName2.site_name
                }).SingleOrDefault();

            if (transferHeader.DriverCode != null)
                transferHeader.DriverName = _contextGlobal.Database.SqlQuery<string>(@"select full_name from USER_SPECS where emp_no = @EmpoNO", new SqlParameter("@EmpoNO", transferHeader.DriverCode)).SingleOrDefault();
            else
                transferHeader.DriverName = "N/A";

            return transferHeader;
        }
        public TransferHeaderModel GetTransferHeaderDocument(string Document, string TransferSiteCode,string DestinataionSiteCode)
        {
            var transferHeader = _contextGlobal.TRANSFER_HEADER
                .Join(_contextGlobal.SITES, site => site.transfer_site_code, siteName => siteName.site_code, (site, siteName) => new { site, siteName })
                .Join(_contextGlobal.SITES, site2 => site2.site.destination_site_code, siteName2 => siteName2.site_code, (site2, siteName2) => new { site2, siteName2 })
                .Where(elements => elements.site2.site.transfer_document == Document && elements.site2.site.transfer_site_code == TransferSiteCode && elements.site2.site.destination_site_code == DestinataionSiteCode)
                .Select(elements => new TransferHeaderModel
                {
                    Comment = elements.site2.site.comment,
                    DriverCode = elements.site2.site.driver_code,
                    TransferDate = DbFunctions.Right("00" + elements.site2.site.transfer_date.Day, 2) + "/" + DbFunctions.Right("00" + elements.site2.site.transfer_date.Month, 2) + "/" + DbFunctions.Right("00" + elements.site2.site.transfer_date.Year, 4),
                    TransferDocument = elements.site2.site.transfer_document,
                    TransferSiteCode = elements.site2.site.transfer_site_code,
                    TransferStatus = elements.site2.site.transfer_status,
                    TransferSiteName = elements.site2.siteName.site_name,
                    PrintStatus = elements.site2.site.print_status,
                    TransferSiteAddress = elements.site2.siteName.address,
                    CUser = elements.site2.site.cuser,
                    UUser = elements.site2.site.authorized_by,
                    DestinationSiteAddress = elements.siteName2.address,
                    DestinationSiteCode = elements.siteName2.site_code,
                    DestinationSiteName = elements.siteName2.site_name
                }).SingleOrDefault();

            if (transferHeader.DriverCode != null)
                transferHeader.DriverName = _contextGlobal.Database.SqlQuery<string>(@"select full_name from USER_SPECS where emp_no = @EmpoNO", new SqlParameter("@EmpoNO", transferHeader.DriverCode)).SingleOrDefault();
            else
                transferHeader.DriverName = "N/A";

            return transferHeader;
        }

        public TranferHeaderAndDetail TranferByDoc(string doc, string site_code)
        {
            var ret = new TranferHeaderAndDetail();
            var g = _contextGlobal.TRANSFER_HEADER
                .Include(e => e.TRANSFER_DETAIL)
                .SingleOrDefault(e => e.transfer_document == doc && e.transfer_site_code == site_code);
            if (g != null)
            {
                ret.TransferHeader = TransferConvert.ToLocalHeader(g);
                ret.TransferDetails = ret.TransferHeader.TRANSFER_DETAIL.ToList();
                ret.TransferHeader.TRANSFER_DETAIL = null;
                return ret;
            }

            return null;
        }

        public bool TransferHeaderEditStatus(List<TransferDetailModel> model, string Document, int Status, string User, string Comment, string driverCode)
        {
            try
            {
                var ids = model.Select(e => e.PartNumber).ToList();
                var headerG = _contextGlobal.TRANSFER_HEADER
                    .Include(e => e.TRANSFER_DETAIL)
                    .SingleOrDefault(elements => elements.transfer_document == Document 
                                                 && elements.transfer_site_code == thisSiteCode);

                StoreProcedureResult result;
                if (headerG != null)
                {
                    headerG.program_id = "AGT001.cshtml";
                    headerG.uuser = User;
                    headerG.udate = DateTime.Now;
                    headerG.transfer_date = DateTime.Now;
                    headerG.driver_code = driverCode;
                    headerG.comment = Comment;
                    headerG.authorized_by = User;
                    headerG.is_sync = true;
                    if (headerG.transfer_status >= 0 && headerG.transfer_status <= 2)
                    {
                        headerG.transfer_status = Status;
                        

                        foreach (var detail in headerG.TRANSFER_DETAIL)
                        {
                            if (Status == 1 && ids.Contains(detail.part_number))
                            {
                                detail.transfer_status = Status;
                                detail.uuser = User;
                                detail.udate = DateTime.Now;
                                // details
                                var Tax = _itemRepository.StoreProcedureIvaIeps(detail.part_number);
                                var Ieps = Tax.Ieps;
                                var Iva = Tax.Iva;
                                var price = _itemRepository.GetMapPrice(detail.part_number);
                                detail.cost = price;
                                detail.ieps = detail.cost * (Ieps / 100);
                                detail.iva = (detail.cost + detail.ieps) * (Iva / 100);
                                detail.is_sync = true;
                            }
                            else if (Status == 2 || detail.transfer_status != 1)
                            {
                                detail.transfer_status = 8;
                                detail.uuser = User;
                                detail.udate = DateTime.Now;
                                detail.is_sync = true;
                            }
                        }

                        _contextGlobal.SaveChanges();
                        if (Status == 2) return true;

                        result = TransferHeaderStore_G(Document, User, headerG.transfer_site_code, headerG.destination_site_code);
                        if (result == null || result.ReturnValue != 0)
                            return false;

                        if (Status == 1)
                        {
                            headerG.transfer_status = 3;
                            foreach (var t in headerG.TRANSFER_DETAIL)
                            {
                                if (Status == 1 && ids.Contains(t.part_number))
                                    t.transfer_status = 3;
                            }

                            _contextGlobal.SaveChanges();
                        }
                    }

                    result = TransferHeaderStoreReal_G(Document, User, headerG.transfer_site_code,headerG.destination_site_code);
                    if (result == null || result.ReturnValue != 0)
                        return false;

                    if (Status == 1)
                    {
                        headerG.transfer_status = 4;
                        headerG.udate = DateTime.Now;
                        headerG.uuser = User;
                        foreach (var t in headerG.TRANSFER_DETAIL)
                        {
                            if (Status == 1 && ids.Contains(t.part_number))
                                t.transfer_status = 4;
                                t.udate = DateTime.Now;
                                t.uuser = User;
                        }
                        _contextGlobal.SaveChanges();
                    }

                    // TRANSFERENCIA A PROCESADORA 0011 (MES_PRT)
                    if (Status == 1 && headerG.destination_site_code == "0011")
                    {
                        headerG.transfer_status = 9;
                        headerG.udate = DateTime.Now;
                        headerG.uuser = User;
                        foreach (var t in headerG.TRANSFER_DETAIL)
                        {
                            if (Status == 1 && ids.Contains(t.part_number))
                                t.transfer_status = 9;
                            t.udate = DateTime.Now;
                            t.uuser = User;
                        }
                        _contextGlobal.SaveChanges();

                        //GENERAR REGISTROS EN EL MES.
                        var DateReturn = _contextGlobal
                            .Database
                            .SqlQuery<StoreProcedureResult>(

                            @"DECLARE @return_value int

                            EXEC  @return_value = [dbo].[sp_Transfer_MES]
                            @transfer_document = @td,
                            @user = @u",
                            new SqlParameter("@td", headerG.transfer_document),
                            new SqlParameter("@u", User)).SingleOrDefault();
                    }
                    

                    return true;
                }

                return false;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public void UpdateLocal(string TransferDoc)
        {
            var global = _contextGlobal.TRANSFER_HEADER.Include(e => e.TRANSFER_DETAIL).FirstOrDefault(e => e.transfer_document == TransferDoc && e.transfer_site_code == thisSiteCode);

            if (global == null)
                return;

            var globalDet = global.TRANSFER_DETAIL;
            var oldH = _context.TRANSFER_HEADER.Include(e => e.TRANSFER_DETAIL).FirstOrDefault(e => e.transfer_document == TransferDoc && e.transfer_site_code == thisSiteCode);

            var det = globalDet.Select(TransferConvert.ToLocalDetail);
            using (var trans = _context.Database.BeginTransaction())
            {
                try
                {
                    TransferConvert.EditLocalHeader(global, ref oldH);
                    _context.TRANSFER_DETAIL.RemoveRange(oldH.TRANSFER_DETAIL);
                    _context.TRANSFER_DETAIL.AddRange(det);
                    _context.SaveChanges();
                    trans.Commit();
                }
                catch (Exception)
                {
                    trans.Rollback();
                }
            }
        }

        public bool TransferHeaderEditStatusPrint(string Document, int Status, string User)
        {
            try
            {
                var global = _contextGlobal.TRANSFER_HEADER.SingleOrDefault(e => e.transfer_document == Document && e.transfer_site_code == thisSiteCode);
                if (global != null)
                {
                    global.program_id = "AGT001.cshtml+Print";
                    global.print_status = Status;
                    _contextGlobal.SaveChanges();
                    return true;
                }

                return false;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public StoreProcedureResult TransferHeaderStore(string Document, string User)
        {
            var transfer = _context.TRANSFER_HEADER.Where(elements => elements.transfer_document == Document).SingleOrDefault();
            var DateReturn = _context.Database.SqlQuery<StoreProcedureResult>("DECLARE	@return_value int, @document nvarchar(10) EXEC    @return_value = [dbo].[sproc_ST_Transit] @site_code = N'" + transfer.transfer_site_code + "', @transfer_document = N'" + transfer.transfer_document + "', @cuser = N'" + User + "', @program_id = N'AGT001A.cshtml', @document = @document OUTPUT SELECT 'Document'= @Document, 'ReturnValue' = @return_value").ToList();
            return DateReturn[0];
        }

        public StoreProcedureResult TransferHeaderStore_G(string Document, string User, string transfer_site_code, string destination_site_code)
        {
            var DateReturn = _context.Database.SqlQuery<StoreProcedureResult>("DECLARE	@return_value int, @document nvarchar(10) EXEC    @return_value = [dbo].[sproc_ST_Transit_G] @site_code = N'" + transfer_site_code + "', @destination_site_code = N'" + destination_site_code + "', @transfer_document = N'" + Document + "', @cuser = N'" + User + "', @program_id = N'AGT001A.cshtml', @document = @document OUTPUT SELECT 'Document'= @Document, 'ReturnValue' = @return_value").ToList();
            return DateReturn[0];
        }

        public StoreProcedureResult TransferHeaderStoreReal(string Document, string User)
        {
            var transfer = _context.TRANSFER_HEADER.Where(elements => elements.transfer_document == Document).SingleOrDefault();
            var DateReturn = _context.Database.SqlQuery<StoreProcedureResult>("DECLARE	@return_value int, @document nvarchar(10) EXEC    @return_value = [dbo].[sproc_GI_From_Transfer] @transfer_document = N'" + transfer.transfer_document + "', @site_code = N'" + transfer.transfer_site_code + "', @cuser = N'" + User + "', @program_id = N'AGT001B.cshtml', @document = @document OUTPUT SELECT 'Document'= @Document, 'ReturnValue' = @return_value").ToList();
            return DateReturn[0];
        }

        public StoreProcedureResult TransferHeaderStoreReal_G(string Document, string User, string transfer_site_code, string destination_site_code)
        {
            var DateReturn = _context.Database.SqlQuery<StoreProcedureResult>("DECLARE	@return_value int, @document nvarchar(10) EXEC    @return_value = [dbo].[sproc_GI_From_Transfer_G] @transfer_document = N'" + Document + "', @site_code = N'" + transfer_site_code + "', @destination_site_code = N'" + destination_site_code +  "', @cuser = N'" + User + "', @program_id = N'AGT001B.cshtml', @document = @document OUTPUT SELECT 'Document'= @Document, 'ReturnValue' = @return_value").ToList();
            return DateReturn[0];
        }

        public List<TransferHeaderModel> GetAllTransferHeaderWithoutStatus(DateTime DateInit, DateTime DateFin)
        {
            var global = _contextGlobal.TRANSFER_HEADER
                .Where(e => e.transfer_site_code == thisSiteCode && DbFunctions.TruncateTime(e.transfer_date) >= DbFunctions.TruncateTime(DateInit) && DbFunctions.TruncateTime(e.transfer_date) <= DbFunctions.TruncateTime(DateFin))
                .Select(e => new TransferHeaderModel
                {
                    Comment = e.comment,
                    DriverCode = e.driver_code,
                    TransferDateD = e.transfer_date,
                    TransferDocument = e.transfer_document,
                    TransferSiteCode = e.transfer_site_code,
                    TransferStatus = e.transfer_status,
                    TransferSiteName = e.SITES.site_name,
                    PrintStatus = e.print_status,
                    TransferSiteAddress = e.SITES.address,
                    DestinationSiteAddress = e.SITES1.address,
                    DestinationSiteCode = e.SITES1.site_code,
                    DestinationSiteName = e.SITES1.site_name
                }).ToList();

            foreach (var t in global)
                t.TransferDate = t.TransferDateD.ToString("MM/dd/yyyy");

            return global.OrderBy(e => e.TransferDocument).ToList();
        }

        public List<TransferHeaderModel> GetAllTransferHeaderActualMonthStatus6or7(int month, int year)
        {
            var siteConfig = _context.SITE_CONFIG.Select(x => x.site_code).FirstOrDefault();
            var model = _contextGlobal.TRANSFER_HEADER
                .Join(_contextGlobal.SITES, site => site.destination_site_code, siteName => siteName.site_code, (site, siteName) => new { site, siteName })
                .Where(x => (x.site.transfer_status == 6 && x.site.transfer_date.Month == month && x.site.transfer_date.Year == year && x.site.destination_site_code == siteConfig) || (x.site.transfer_status == 7 && x.site.transfer_date.Month == month && x.site.transfer_date.Year == year && x.site.destination_site_code == siteConfig))
                .Select(p => new TransferHeaderModel
                {
                    TransferDocument = p.site.transfer_document,
                    DestinationSiteName = p.siteName.site_name,
                    TransferSiteCode = p.site.transfer_site_code,
                    TransferDate = DbFunctions.Right("00" + p.site.transfer_date.Day, 2) + "/" + DbFunctions.Right("00" + p.site.transfer_date.Month, 2) + "/" + DbFunctions.Right("00" + p.site.transfer_date.Year, 4),
                    TransferStatus = p.site.transfer_status,
                    Comment = p.site.comment
                }).ToList();
            return model;
        }

        //TRANSFERENCIAS YA ENVIADAS
        public List<TransferHeaderModel> GetAllTransferHeaderStatus4()
        {
            int lastMonth = DateTime.Now.Month - 1;
            int actualYear = DateTime.Now.Year;
            var siteConfig = _context.SITE_CONFIG.Select(x => x).FirstOrDefault();
            var model = _contextGlobal.TRANSFER_HEADER
                .Join(_contextGlobal.SITES, site => site.destination_site_code, siteName => siteName.site_code, (site, siteName) => new { site, siteName })
                .Where(x => x.site.transfer_status == 4 && x.site.transfer_site_code == siteConfig.site_code && x.site.transfer_date.Month >= lastMonth && x.site.transfer_date.Year == actualYear)
                .Select(p => new TransferHeaderModel
                {
                    TransferDocument = p.site.transfer_document,
                    DestinationSiteCode = p.site.destination_site_code,
                    DestinationSiteName = p.siteName.site_name,
                    TransferSiteCode = siteConfig.site_code,
                    TransferSiteName = siteConfig.site_name,
                    TransferDate = DbFunctions.Right("00" + p.site.transfer_date.Day, 2) + "/" + DbFunctions.Right("00" + p.site.transfer_date.Month, 2) + "/" + DbFunctions.Right("00" + p.site.transfer_date.Year, 4),
                    TransferStatus = p.site.transfer_status,
                    Comment = p.site.comment
                }).ToList();
            return model;
        }
        //Transferencias Enviadas con solicitud de Cancelacion
        public List<TransferHeaderModel> GetAllTransferHeaderStatus6()
        {
            var siteConfig = _context.SITE_CONFIG.Select(x => x).FirstOrDefault();
            var model = _contextGlobal.TRANSFER_HEADER
                .Join(_contextGlobal.SITES, site => site.destination_site_code, siteName => siteName.site_code, (site, siteName) => new { site, siteName })
                .Where(x => x.site.transfer_status == 6 && x.site.transfer_site_code == siteConfig.site_code)
                .Select(p => new TransferHeaderModel
                {
                    TransferDocument = p.site.transfer_document,
                    DestinationSiteCode = p.site.destination_site_code,
                    DestinationSiteName = p.siteName.site_name,
                    TransferSiteCode = siteConfig.site_code,
                    TransferSiteName = siteConfig.site_name,
                    TransferDate = DbFunctions.Right("00" + p.site.transfer_date.Day, 2) + "/" + DbFunctions.Right("00" + p.site.transfer_date.Month, 2) + "/" + DbFunctions.Right("00" + p.site.transfer_date.Year, 4),
                    TransferStatus = p.site.transfer_status,
                    Comment = p.site.comment
                }).ToList();
            return model;
        }

        //TRANSFERENCIAS RECIBIDAS
        public List<TransferHeaderModel> GetAllTransferHeaderStatus9()
        {
            int lastMonth = DateTime.Now.Month - 1;
            int actualYear = DateTime.Now.Year;
            var siteConfig = _context.SITE_CONFIG.Select(x => x).FirstOrDefault();
            var model = _contextGlobal.TRANSFER_HEADER
                .Join(_contextGlobal.SITES, site => site.transfer_site_code, siteName => siteName.site_code, (site, siteName) => new { site, siteName })
                .Where(x => x.site.transfer_status == 9 && x.site.destination_site_code == siteConfig.site_code && x.site.transfer_date.Month >= lastMonth && x.site.transfer_date.Year == actualYear)
                .Select(p => new TransferHeaderModel
                {
                    TransferDocument = p.site.transfer_document,
                    DestinationSiteCode = siteConfig.site_code,
                    DestinationSiteName = siteConfig.site_name,
                    TransferSiteCode = p.site.transfer_site_code,
                    TransferSiteName = p.siteName.site_name,
                    TransferDate = DbFunctions.Right("00" + p.site.transfer_date.Day, 2) + "/" + DbFunctions.Right("00" + p.site.transfer_date.Month, 2) + "/" + DbFunctions.Right("00" + p.site.transfer_date.Year, 4),
                    TransferStatus = p.site.transfer_status,
                    Comment = p.site.comment
                }).ToList();
            return model;
        }
        //Tranferencias Recibidas con solicitud de Cancelacion
        public List<TransferHeaderModel> GetAllTransferHeaderStatus7()
        {
            var siteConfig = _context.SITE_CONFIG.Select(x => x).FirstOrDefault();
            var model = _contextGlobal.TRANSFER_HEADER
                .Join(_contextGlobal.SITES, site => site.transfer_site_code, siteName => siteName.site_code, (site, siteName) => new { site, siteName })
                .Where(x => x.site.transfer_status == 7 && x.site.destination_site_code == siteConfig.site_code)
                .Select(p => new TransferHeaderModel
                {
                    TransferDocument = p.site.transfer_document,
                    DestinationSiteCode = siteConfig.site_code,
                    DestinationSiteName = siteConfig.site_name,
                    TransferSiteCode = p.site.transfer_site_code,
                    TransferSiteName = p.siteName.site_name,
                    TransferDate = DbFunctions.Right("00" + p.site.transfer_date.Day, 2) + "/" + DbFunctions.Right("00" + p.site.transfer_date.Month, 2) + "/" + DbFunctions.Right("00" + p.site.transfer_date.Year, 4),
                    TransferStatus = p.site.transfer_status,
                    Comment = p.site.comment
                }).ToList();
            return model;
        }

        public TransferHeaderModel GetTransferHeaderByDocument(string Document)
        {
            var model = _contextGlobal.TRANSFER_HEADER
                .Where(x => x.transfer_document == Document)
                .Select(x => new TransferHeaderModel
                {
                    TransferDocument = x.transfer_document,
                    TransferSiteCode = x.transfer_site_code,
                    DestinationSiteCode = x.destination_site_code,
                    PrintStatus = x.print_status,
                    TransferDate = DbFunctions.Right("00" + x.transfer_date.Day, 2) + "/" + DbFunctions.Right("00" + x.transfer_date.Month, 2) + "/" + DbFunctions.Right("00" + x.transfer_date.Year, 4),
                    TransferStatus = x.transfer_status,
                    DriverCode = x.driver_code,
                    Comment = x.comment,
                    CUser = x.cuser,
                    Items = _contextGlobal.TRANSFER_DETAIL
                    .Where(y => y.transfer_document == x.transfer_document)
                    .Select(y => new TransferDetailModel
                    {
                        TransferDocument = y.transfer_document,
                        PartNumber = y.part_number,
                        Description = y.ITEM.part_description,
                        Quantity = y.quantity,
                        UnitCost = y.cost ?? 0,
                        IEPS = y.ieps ?? 0,
                        Iva = y.iva ?? 0,
                        GrQuantity = y.gr_quantity ?? 0
                    }).ToList()
                }).FirstOrDefault();

            return model;
        }

        public TransferHeaderModel GetTransferHeaderByDocumentAndSites(string Document, string transferSite, string destinationSite)
        {
            var model = _contextGlobal.TRANSFER_HEADER
                .Where(x => x.transfer_document == Document && x.transfer_site_code == transferSite && x.destination_site_code == destinationSite)
                .Select(x => new TransferHeaderModel
                {
                    TransferDocument = x.transfer_document,
                    TransferSiteCode = x.transfer_site_code,
                    DestinationSiteCode = x.destination_site_code,
                    PrintStatus = x.print_status,
                    TransferDate = DbFunctions.Right("00" + x.transfer_date.Day, 2) + "/" + DbFunctions.Right("00" + x.transfer_date.Month, 2) + "/" + DbFunctions.Right("00" + x.transfer_date.Year, 4),
                    TransferStatus = x.transfer_status,
                    DriverCode = x.driver_code,
                    Comment = x.comment,
                    CUser = x.cuser,
                    Items = x.TRANSFER_DETAIL
                    .Where(y => y.transfer_document == x.transfer_document && x.transfer_site_code == transferSite && x.destination_site_code == destinationSite)
                    .Select(y => new TransferDetailModel
                    {
                        TransferDocument = y.transfer_document,
                        PartNumber = y.part_number,
                        Description = y.ITEM.part_description,
                        Quantity = y.quantity,
                        UnitCost = y.cost ?? 0,
                        IEPS = y.ieps ?? 0,
                        Iva = y.iva ?? 0,
                        GrQuantity = y.gr_quantity ?? 0
                    }).ToList()
                    /*Items = _contextGlobal.TRANSFER_DETAIL
                    .Where(y => y.transfer_document == x.transfer_document && x.transfer_site_code == transferSite && x.destination_site_code == destinationSite)
                    .Select(y => new TransferDetailModel
                    {
                        TransferDocument = y.transfer_document,
                        PartNumber = y.part_number,
                        Description = y.ITEM.part_description,
                        Quantity = y.quantity,
                        UnitCost = y.cost ?? 0,
                        IEPS = y.ieps ?? 0,
                        Iva = y.iva ?? 0,
                        GrQuantity = y.gr_quantity ?? 0
                    }).ToList()*/
                }).FirstOrDefault();

            return model;
        }

        public TransferHeaderModel GetTransferHeaderByDocumentSite(string Document)
        {
            var siteConfig = _context.SITE_CONFIG.Select(x => x.site_code).FirstOrDefault();
            var model = _contextGlobal.TRANSFER_HEADER.Where(x => x.transfer_document == Document && x.destination_site_code == siteConfig)
                .Select(x => new TransferHeaderModel
                {
                    TransferDocument = x.transfer_document,
                    TransferSiteCode = x.transfer_site_code,
                    DestinationSiteCode = x.destination_site_code,
                    PrintStatus = x.print_status,
                    TransferDate = DbFunctions.Right("00" + x.transfer_date.Day, 2) + "/" + DbFunctions.Right("00" + x.transfer_date.Month, 2) + "/" + DbFunctions.Right("00" + x.transfer_date.Year, 4),
                    TransferStatus = x.transfer_status,
                    DriverCode = x.driver_code,
                    Comment = x.comment,
                    CUser = x.cuser,
                    Items = _contextGlobal.TRANSFER_DETAIL
                    .Where(y => y.transfer_document == x.transfer_document)
                    .Select(y => new TransferDetailModel
                    {
                        TransferDocument = y.transfer_document,
                        PartNumber = y.part_number,
                        Description = y.ITEM.part_description,
                        Quantity = y.quantity,
                        UnitCost = y.cost ?? 0,
                        IEPS = y.ieps ?? 0,
                        Iva = y.iva ?? 0,
                        GrQuantity = y.gr_quantity ?? 0
                    }).ToList()
                }).FirstOrDefault();

            return model;
        }

        public bool CancelRequestTransferHeader(string TransferDocument, string TransferSiteCode, string User, string Reason)
        {
            try
            {
                var transfer = _contextGlobal.TRANSFER_HEADER.Where(x => x.transfer_document == TransferDocument && x.transfer_site_code == TransferSiteCode).FirstOrDefault();

                if (transfer.transfer_status == 4)
                    transfer.transfer_status = 6;
                else if (transfer.transfer_status == 9)
                    transfer.transfer_status = 7;
                else
                    return false;

                transfer.uuser = User;
                transfer.udate = DateTime.Now;
                transfer.program_id = "CT001.cshtml";
                transfer.comment = Reason;

                var items = _contextGlobal.TRANSFER_DETAIL.Where(x => x.transfer_document == TransferDocument && x.transfer_site_code == TransferSiteCode).ToList();
                foreach (var item in items)
                {
                    if (item.transfer_status == 4)
                        item.transfer_status = 6;
                    else if (item.transfer_status == 9)
                        item.transfer_status = 7;

                    item.uuser = User;
                    item.udate = DateTime.Now;
                    item.program_id = "CT001.cshtml";
                }

                _contextGlobal.SaveChanges();

                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public bool CancelRequestTransferHeaderNew(string TransferDocument, string TransferSiteCode, string User, string Reason, string destinationSite)
        {
            try
            {
                var transfer = _contextGlobal.TRANSFER_HEADER.Where(x => x.transfer_document == TransferDocument && x.transfer_site_code == TransferSiteCode && x.destination_site_code == destinationSite).FirstOrDefault();

                if (transfer.transfer_status == 4)
                    transfer.transfer_status = 6;
                else if (transfer.transfer_status == 9)
                    transfer.transfer_status = 7;
                else
                    return false;

                transfer.uuser = User;
                transfer.udate = DateTime.Now;
                transfer.program_id = "CT001.cshtml";
                transfer.comment = Reason;

                var items = _contextGlobal.TRANSFER_DETAIL.Where(x => x.transfer_document == TransferDocument && x.transfer_site_code == TransferSiteCode && x.destination_site_code == destinationSite).ToList();
                foreach (var item in items)
                {
                    if (item.transfer_status == 4)
                        item.transfer_status = 6;
                    else if (item.transfer_status == 9)
                        item.transfer_status = 7;

                    item.uuser = User;
                    item.udate = DateTime.Now;
                    item.program_id = "CT001.cshtml";
                }

                _contextGlobal.SaveChanges();

                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public List<TransferHeaderModel> GetAllTransferHeaderByMonth(int month, int year)
        {
            var model = _contextGlobal.TRANSFER_HEADER
                .Join(_contextGlobal.SITES, h => h.destination_site_code, s => s.site_code, (h, s) => new { h, s })
                .Join(_contextGlobal.SITES, hh => hh.h.transfer_site_code, ss => ss.site_code, (hh, ss) => new { hh, ss })
                .Where(x => x.hh.h.transfer_date.Month == month && x.hh.h.transfer_date.Year == year && (x.hh.h.transfer_site_code == thisSiteCode || x.hh.h.destination_site_code == thisSiteCode) && (x.hh.h.transfer_status == 4 || x.hh.h.transfer_status == 9))
                .Select(x => new TransferHeaderModel
                {
                    TransferDocument = x.hh.h.transfer_document,
                    TransferSiteCode = x.hh.h.transfer_site_code,
                    TransferSiteName = x.ss.site_name,
                    DestinationSiteName = x.hh.s.site_name,
                    Status = x.hh.h.transfer_status == 0 ? "Creado" : x.hh.h.transfer_status == 1 ? "Aprobado" : x.hh.h.transfer_status == 2 ? "Rechazado" : x.hh.h.transfer_status == 4 ? "Transito" : x.hh.h.transfer_status == 8 ? "Cancelado" : x.hh.h.transfer_status == 9 ? "Terminado" : "",
                    TransferDate = DbFunctions.Right("00" + x.hh.h.transfer_date.Day, 2) + "/" + DbFunctions.Right("00" + x.hh.h.transfer_date.Month, 2) + "/" + DbFunctions.Right("00" + x.hh.h.transfer_date.Year, 4),
                    TransferStatus = x.hh.h.transfer_status,
                    Comment = x.hh.h.comment
                }).ToList();

            return model;
        }

        public List<TransferHeaderModel> GetAllTransferByDates(DateTime BeginDate, DateTime EndDate)
        {
            var model = _contextGlobal.TRANSFER_HEADER
                .Join(_contextGlobal.SITES, h => h.destination_site_code, s => s.site_code, (h, s) => new { h, s })
                .Join(_contextGlobal.SITES, hh => hh.h.transfer_site_code, ss => ss.site_code, (hh, ss) => new { hh, ss })
                .Where(x => DbFunctions.TruncateTime(x.hh.h.transfer_date) >= DbFunctions.TruncateTime(BeginDate) && DbFunctions.TruncateTime(x.hh.h.transfer_date) <= DbFunctions.TruncateTime(EndDate) && (x.hh.h.transfer_site_code == thisSiteCode || x.hh.h.destination_site_code == thisSiteCode) && (x.hh.h.transfer_status == 4 || x.hh.h.transfer_status == 9))
                .Select(x => new TransferHeaderModel
                {
                    TransferDocument = x.hh.h.transfer_document,
                    TransferSiteCode = x.hh.h.transfer_site_code,
                    TransferSiteName = x.ss.site_name,
                    DestinationSiteName = x.hh.s.site_name,
                    Status = x.hh.h.transfer_status == 0 ? "Creado" : x.hh.h.transfer_status == 1 ? "Aprobado" : x.hh.h.transfer_status == 2 ? "Rechazado" : x.hh.h.transfer_status == 4 ? "Transito" : x.hh.h.transfer_status == 8 ? "Cancelado" : x.hh.h.transfer_status == 9 ? "Terminado" : "",
                    TransferDate = DbFunctions.Right("00" + x.hh.h.transfer_date.Day, 2) + "/" + DbFunctions.Right("00" + x.hh.h.transfer_date.Month, 2) + "/" + DbFunctions.Right("00" + x.hh.h.transfer_date.Year, 4),
                    TransferStatus = x.hh.h.transfer_status,
                    Comment = x.hh.h.comment
                }).ToList();

            return model;
        }

        public TRANSFER_HEADER_G GetTransferHeader(string doc, string transfer_site, string destination_site)
        {
            return _contextGlobal.TRANSFER_HEADER
                .Where(s => s.transfer_document == doc && s.transfer_site_code == transfer_site && s.destination_site_code == destination_site)
                .FirstOrDefault();
        }

        //UPDATE TRANSFER STATUS
        public int UpdateTransferHeader(TRANSFER_HEADER_G transfer)
        {
            try
            {
                _contextGlobal.Entry(transfer).State = System.Data.Entity.EntityState.Modified;
                return _contextGlobal.SaveChanges();
            }
            catch (Exception ex)
            {
                var msg = ex.Message;
                return 0;
            }
        }

        //REVERSA TRANSFERENCIA STATUS 4
        public StoreProcedureResult sproc_ST_Transit_Reverse_G(string Document, string User, string transfer_site_code, string destination_site_code)
        {
            /*var DateReturn = _context.Database.SqlQuery<StoreProcedureResult>(@"DECLARE	@return_value int, @document nvarchar(10) 
                            EXEC    @return_value = [dbo].[sproc_ST_Transit_Reverse_G] @site_code = N'" + transfer_site_code + "', " +
                            "@destination_site_code = N'" + destination_site_code + "', @transfer_document = N'" + Document + "', @cuser = N'" + User + "', " +
                            "@program_id = N'AGT001A.cshtml', @document = @document OUTPUT SELECT 'Document'= @Document, 'ReturnValue' = @return_value").ToList();
           
            return DateReturn[0]; */
            var DateReturn = _context.Database.SqlQuery<StoreProcedureResult>(@"DECLARE	@return_value int, @document nvarchar(10)
                        EXEC	@return_value = [dbo].[sproc_ST_Transit_Reverse_G]
		                @site_code = N'"+transfer_site_code+"', @destination_site = N'"+destination_site_code+"'," +
                        "@transfer_document = N'"+Document+"', @cuser = N'"+User+"',@program_id = N'CT003.cshtml'," +
                        "@document = @document OUTPUT SELECT	@document as N'Document', 'Return Value' = @return_value").ToList();
            return DateReturn[0];
        }

        public int ValidateDestinationCode(string partNumber, string destinationCode)
        {
            int returnValue = 1000;
            // 1000 - Todo correcto
            // 1001 - Este producto no es valido para trasferir a esta locación.
            // 1002 - La sucursal ACTUAL tiene el producto bloqueado
            // 1003 - La sucursal DESTINO tiene el producto bloqueado

            if (new[] {"10473", "10476", "10475"}.Contains(partNumber)
                && new[] {"0093", "0011"}.Contains(destinationCode))
                return 1001;
            if (partNumber == "9899" 
                && new[] { "0093", "0094" }.Contains(destinationCode))
                return 1001;

            if (!_itemSupplierRepository.ValidatePartNumberBySite(partNumber, ""))
                return 1002;

            if (!_itemSupplierRepository.ValidatePartNumberBySite(partNumber, destinationCode))
                return 1003;

            switch (destinationCode)
            {
                case "0094":
                {
                    var b = _contextGlobal.Database.SqlQuery<bool>(
                        @"select top 1 CAST(1 AS BIT) from [10.11.0.10].CDSERP.dbo.IF_Barcodes a
                            JOIN [10.11.0.10].CDSERP.dbo.ITEM b ON (a.part_barcode = b.product_id Collate SQL_Latin1_General_CP1_CI_AS)
                            where  b.active_flag=1 and a.part_number = @partNumber ",new SqlParameter("partNumber", partNumber)).FirstOrDefault();
                        if (!b)
                            return 1001;
                        else
                            return returnValue;
                }
                case "0093":
                {
                    var b = _contextGlobal.Database.SqlQuery<bool>(
                        @"select top 1 CAST(1 AS BIT) from [10.11.0.11].CDSERP.dbo.IF_Barcodes a
                            JOIN [10.11.0.11].CDSERP.dbo.ITEM b ON (a.part_barcode = b.product_id Collate SQL_Latin1_General_CP1_CI_AS)
                            where  b.active_flag=1 and a.part_number = @partNumber ",new SqlParameter("partNumber", partNumber)).FirstOrDefault();
                        if (!b)
                            return 1001;
                        else
                            return returnValue;
                    }
                case "0011":
                {
                    var b = _contextGlobal.Database.SqlQuery<bool>(
                        "select top 1 CAST(1 AS BIT) from MES_PRT.dbo.PART_MASTER where part_number = @partNumber and flag_active=1",new SqlParameter("partNumber", partNumber)).FirstOrDefault();
                        if (!b)
                            return 1001;
                        else
                            return returnValue;
                    }
                default:
                    {
                        if (!_context.SITES.Any(e => e.site_code == destinationCode))
                            return 1001;
                        else
                            return returnValue;
                    }
            }
        }
    }
}