﻿using App.Entities;
using App.Entities.ViewModels.Android;
using App.Entities.ViewModels.Transfer;
using App.GlobalEntities;
using System;
using System.Collections.Generic;
using System.Linq;

namespace App.DAL.Tranfer
{
    public class TransferDetailRepository
    {
        private readonly DFL_SAIEntities _context;
        private readonly GlobalERPEntities _globalContext;

        public TransferDetailRepository()
        {
            _context = new DFL_SAIEntities();
            _globalContext = new GlobalERPEntities();
        }

        private string sc;
        private string thisSiteCode
        {
            get
            {
                if (sc != null) return sc;
                sc = _context.SITE_CONFIG.Select(e => e.site_code).First();
                return sc;
            }
        }

        public List<TransferDetailModel> GetAllTransferDeatil(string Document)
        {
            var list = _globalContext.TRANSFER_HEADER.Select(e => new
            {
                header = e,
                details = e.TRANSFER_DETAIL.Select(de => new TransferDetailModel
                {
                    TransferDate = de.TRANSFER_HEADER.transfer_date,
                    Description = de.ITEM.part_description,
                    DestinationSiteCode = e.destination_site_code,
                    DestinationSiteAddress = e.SITES1.address,
                    DestinationSiteName = e.SITES1.site_name,
                    Quantity = de.quantity,
                    GrQuantity = de.gr_quantity,
                    ScanStatus = de.scan_status ? 1 : 0,
                    TransferStatus = de.TRANSFER_HEADER.transfer_status,
                    PartNumber = de.part_number,
                    TransferDocument = de.transfer_document,
                    UnitCost = de.cost ?? 0,
                    IEPS = de.ieps ?? 0,
                    Iva = de.iva ?? 0,
                    transfer_status = de.transfer_status,
                    TransferSiteCode = de.transfer_site_code
                })
            }).FirstOrDefault(e => e.header.transfer_document == Document && e.header.transfer_site_code == thisSiteCode);

            return list?.details?.ToList() ?? new List<TransferDetailModel>();
        }
        public List<TransferDetailModel> GetAllTransferDeatilReportIn(string Document, string TransferSiteCode, string DestinationsiteCode)
        {
            var list = _globalContext.TRANSFER_HEADER.Select(e => new
            {
                header = e,
                details = e.TRANSFER_DETAIL.Select(de => new TransferDetailModel
                {
                    TransferDate = de.TRANSFER_HEADER.transfer_date,
                    Description = de.ITEM.part_description,
                    DestinationSiteCode = e.destination_site_code,
                    DestinationSiteAddress = e.SITES1.address,
                    DestinationSiteName = e.SITES1.site_name,
                    Quantity = de.quantity,
                    GrQuantity = de.gr_quantity,
                    ScanStatus = de.scan_status ? 1 : 0,
                    TransferStatus = de.TRANSFER_HEADER.transfer_status,
                    PartNumber = de.part_number,
                    TransferDocument = de.transfer_document,
                    UnitCost = de.cost ?? 0,
                    IEPS = de.ieps ?? 0,
                    Iva = de.iva ?? 0,
                    transfer_status = de.transfer_status
                })
            }).FirstOrDefault(e => e.header.transfer_document == Document && e.header.transfer_site_code == TransferSiteCode && e.header.destination_site_code == DestinationsiteCode);

            return list?.details?.ToList() ?? new List<TransferDetailModel>();
        }

        public List<TransferDetailModel> GetAllTransfeDetailsExist(string Document, string TransferSiteCode)
        {
            var model = _globalContext.TRANSFER_DETAIL
               .Join(_globalContext.ITEM, transfer => transfer.part_number, item => item.part_number, (transfer, item) => new { transfer, item })
               .Join(_globalContext.TRANSFER_HEADER, detail => detail.transfer.transfer_document, header => header.transfer_document, (detail, header) => new { detail, header })
               .Join(_globalContext.SITES, transferSite => transferSite.header.destination_site_code, siteName => siteName.site_code, (transferSite, siteName) => new { transferSite, siteName })
               .Where(x => x.transferSite.detail.transfer.transfer_document == Document && x.transferSite.header.transfer_site_code == TransferSiteCode)
               .Select(x => new TransferDetailModel
               {
                   TransferSiteCode = x.transferSite.detail.transfer.transfer_site_code,
                   Description = x.transferSite.detail.item.part_description,
                   DestinationSiteCode = x.transferSite.header.destination_site_code,
                   DestinationSiteAddress = x.siteName.address,
                   DestinationSiteName = x.siteName.site_name,
                   Quantity = x.transferSite.detail.transfer.quantity,
                   PartNumber = x.transferSite.detail.transfer.part_number,
                   TransferDocument = x.transferSite.detail.transfer.transfer_document,
                   UnitCost = x.transferSite.detail.transfer.cost ?? 0,
                   IEPS = x.transferSite.detail.transfer.ieps ?? 0,
                   Iva = x.transferSite.detail.transfer.iva ?? 0
               }).ToList();

            return model;
        }

        public List<TransferDetailModel> GetAllTransfeDetailsExistNew(string Document, string TransferSiteCode, string destinationSite)
        {
            var model = _globalContext.TRANSFER_DETAIL
               .Join(_globalContext.ITEM, transfer => transfer.part_number, item => item.part_number, (transfer, item) => new { transfer, item })
               .Join(_globalContext.TRANSFER_HEADER, detail => new { detail.transfer.transfer_document, detail.transfer.destination_site_code, detail.transfer.transfer_site_code }, header => new { header.transfer_document, header.destination_site_code, header.transfer_site_code }, (detail, header) => new { detail, header })
               .Join(_globalContext.SITES, transferSite => transferSite.header.destination_site_code, siteName => siteName.site_code, (transferSite, siteName) => new { transferSite, siteName })
               .Where(x => x.transferSite.detail.transfer.transfer_document == Document && x.transferSite.header.transfer_site_code == TransferSiteCode && x.transferSite.header.destination_site_code == destinationSite)
               .GroupBy(g=> new { g.transferSite.detail.transfer.transfer_site_code, g.transferSite.detail.item.part_description, g.transferSite.header.destination_site_code , g.siteName.address, g.siteName.site_name, g.transferSite.detail.transfer.quantity, g.transferSite.detail.transfer.part_number, g.transferSite.detail.transfer.transfer_document,g.transferSite.detail.transfer.cost, g.transferSite.detail.transfer.ieps, g.transferSite.detail.transfer.iva } )
               .Select(x => new TransferDetailModel
               {
                   TransferSiteCode = x.Key.transfer_site_code,
                   Description = x.Key.part_description,
                   DestinationSiteCode = x.Key.destination_site_code,
                   DestinationSiteAddress = x.Key.address,
                   DestinationSiteName = x.Key.site_name,
                   Quantity = x.Key.quantity,
                   PartNumber = x.Key.part_number,
                   TransferDocument = x.Key.transfer_document,
                   UnitCost = x.Key.cost ?? 0,
                   IEPS = x.Key.ieps ?? 0,
                   Iva = x.Key.iva ?? 0
               }).ToList();

            return model;
        }

        public List<TransferDetailModel> NewGetAllTransfeDetailsExistFree(string Document)
        {
            var l = GetAllTransferDeatil(Document);
            foreach (var i in l)
            {
                i.IEPS = i.Quantity * i.IEPS;
                i.Iva = i.Quantity * i.Iva;
            }
            return l.ToList();
        }

        public List<TransferHeaderAndroidModel> GetAllTransferDetailsItems(string transferDoc, string destination_site, string transferSiteCode)
        {
            return _globalContext.TRANSFER_DETAIL.Where(s => s.transfer_document == transferDoc && s.destination_site_code == destination_site && s.transfer_site_code == transferSiteCode)
                .Select(q => new TransferHeaderAndroidModel
                {
                    part_number = q.part_number,
                    quantity = q.quantity.ToString(),
                    part_description = q.ITEM.part_description
                }).ToList();
        }

        //UPDATE 
        public int UpdateTransferDetail(TRANSFER_DETAIL_G transfer)
        {
            try
            {
                _globalContext.Entry(transfer).State = System.Data.Entity.EntityState.Modified;
                return _globalContext.SaveChanges();
            }
            catch (Exception ex)
            {
                var msg = ex.Message;
                return 0;
            }
        }

        public List<TRANSFER_DETAIL_G> GetAllTransferDetail(string transferDoc, string destination_site, string transferSiteCode)
        {
            return _globalContext.TRANSFER_DETAIL
                .Where(s => s.transfer_document == transferDoc && s.destination_site_code == destination_site && s.transfer_site_code == transferSiteCode)
                .ToList();
        }
    }
}