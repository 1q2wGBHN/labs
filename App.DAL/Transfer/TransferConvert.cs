﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using App.Entities;

namespace App.DAL.Transfer
{
    public class TransferConvert
    {

        public static Entities.TRANSFER_HEADER ToLocalHeader(GlobalEntities.TRANSFER_HEADER_G transfer)
        {
            return new Entities.TRANSFER_HEADER
            {
                transfer_document = transfer.transfer_document,
                transfer_site_code = transfer.transfer_site_code,
                destination_site_code = transfer.destination_site_code,
                transfer_status = transfer.transfer_status,
                transfer_date = transfer.transfer_date,
                transfer_date_in = transfer.transfer_date_in,
                delivery_order = transfer.delivery_order,
                authorized_by = transfer.authorized_by,
                driver_code = transfer.driver_code,
                print_status = transfer.print_status,
                comment = transfer.comment,
                comment_in = transfer.comment_in,
                cuser = transfer.cuser,
                cdate = transfer.cdate,
                uuser = transfer.uuser,
                udate = transfer.udate,
                program_id = transfer.program_id,
                file_transfer = transfer.file_transfer,
                TRANSFER_DETAIL = transfer.TRANSFER_DETAIL?.Select(ToLocalDetail).ToList(),
                new_transfer_type = transfer.new_transfer_type,
            };
        }
        public static void  EditLocalHeader(GlobalEntities.TRANSFER_HEADER_G transfer,ref Entities.TRANSFER_HEADER h)
        {
            if(h == null) h = new TRANSFER_HEADER();
                //h.transfer_document = transfer.transfer_document;
            h.transfer_site_code = transfer.transfer_site_code;
            h.destination_site_code = transfer.destination_site_code;
            h.print_status = transfer.print_status;
            h.transfer_status = transfer.transfer_status;
            h.transfer_date = transfer.transfer_date;
            h.transfer_date_in = transfer.transfer_date_in;
            h.delivery_order = transfer.delivery_order;
            h.authorized_by = transfer.authorized_by;
            h.driver_code = transfer.driver_code;
            h.new_transfer_type = transfer.new_transfer_type;
            h.comment = transfer.comment;
            h.comment_in = transfer.comment_in;
            h.cuser = transfer.cuser;
            h.cdate = transfer.cdate;
            h.uuser = transfer.uuser;
            h.udate = transfer.udate;
            h.program_id = transfer.program_id;
            h.file_transfer = transfer.file_transfer;
           // h.TRANSFER_DETAIL = transfer.TRANSFER_DETAIL?.Select(ToLocalDetail).ToList();


        }
        public static Entities.TRANSFER_DETAIL ToLocalDetail(GlobalEntities.TRANSFER_DETAIL_G detail)
        {
            return new Entities.TRANSFER_DETAIL
            {
                transfer_document = detail.transfer_document,
                part_number = detail.part_number,
                quantity = detail.quantity,
                cost = detail.cost,
                iva = detail.iva,
                ieps = detail.ieps,
                cuser = detail.cuser,
                cdate = detail.cdate,
                uuser = detail.uuser,
                udate = detail.udate,
                program_id = detail.program_id,
                transfer_status = detail.transfer_status
            };

        }
        
        // public static Entities.TRANSFER_PALLET_G ToLocalPallet(GlobalEntities.TRANSFER_PALLET pallet)
        // {
        //     return new TRANSFER_PALLET_G
        //     {
        //         pallet_id = pallet.pallet_id,
        //         transfer_id = pallet.transfer_id,
        //         scanned = pallet.scanned,
        //         cuser = pallet.cuser,
        //         cdate = pallet.cdate,
        //         uuser = pallet.uuser,
        //         udate = pallet.udate,
        //         program_id = pallet.program_id,
        //     };
        //
        // }
    }
}
