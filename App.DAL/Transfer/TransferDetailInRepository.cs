﻿using App.Entities;
using App.Entities.ViewModels.Android;
using App.Entities.ViewModels.PurchaseOrder;
using App.Entities.ViewModels.Transfer;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.SqlClient;
using System.Linq;
using App.DAL.Tranfer;
using App.GlobalEntities;

namespace App.DAL.Transfer
{
    public class TransferDetailInRepository
    {
        private readonly DFL_SAIEntities _context;
        private readonly GlobalERPEntities _globalContext;
        private readonly TransferHeaderInRepository _headerRepository;
        public TransferDetailInRepository()
        {
            _context = new DFL_SAIEntities();
            _globalContext = new GlobalERPEntities();
            _headerRepository = new TransferHeaderInRepository();
        }
        private string sc;
        private string thisSiteCode
        {
            get
            {
                if (sc != null) return sc;
                sc = _context.SITE_CONFIG.Select(e => e.site_code).First();
                return sc;
            }
        }
        public bool GetTransferInTransit(int transferId, string transferDocument, string transferSiteCode)
        {
            var transfer = _globalContext.TRANSFER_HEADER.Any(e => e.transfer_document == transferDocument && e.transfer_status == 4 && e.transfer_site_code == transferSiteCode && e.destination_site_code == thisSiteCode);

            return transfer;
        }

        public bool UpdateTransferHeaderIn(int transferId, string part_number, decimal newQuantity, string user, string transferDocument, string transferSiteCode)
        {
            try
            {
                var global = _globalContext.TRANSFER_DETAIL.SingleOrDefault(e => e.transfer_document == transferDocument && e.part_number == part_number && e.transfer_site_code == transferSiteCode && e.destination_site_code == thisSiteCode);
                if (global != null)
                {
                    if (global.quantity == global.gr_quantity && global.scan_status)
                        global.scan_status = false;

                    global.gr_quantity = newQuantity;

                    if (global.quantity == global.gr_quantity)
                        global.scan_status = true;

                    _globalContext.SaveChanges();
                    return true;
                }

                return false;
            }
            catch (Exception e)
            {
                var msg = e.Message;
                return false;
            }
        }

        public List<TransferReceptionAndroidModel> GetAllItemsByTransferInTransit(int transferId, string transferDocument, string transferSiteCode)
        {
            var global = _globalContext.TRANSFER_HEADER
                .Where(e => e.transfer_document == transferDocument && e.transfer_status == 4 && e.transfer_site_code == transferSiteCode && e.destination_site_code == thisSiteCode)
                .Select(e => e.TRANSFER_DETAIL.Where(e2 => e2.transfer_status == 4 && e2.destination_site_code == thisSiteCode))
                .FirstOrDefault()?
                .Select(e => new TransferReceptionAndroidModel
                {
                    transfer_id = 0,
                    part_number = e.part_number,
                    quantity = e.quantity,
                    gr_quantity = e.gr_quantity,
                    cost = e.cost,
                    iva = e.iva,
                    ieps = e.ieps,
                    scan_status = e.scan_status ? 1 : 0
                }).ToList();

            return global;
        }

        public List<TransferDetailInModel> ListTransferDetailInScanQuantity(int transferId, string transferDocument, string transferSiteCode)
        {
            return GetAllItemsByTransferInTransit(transferId, transferDocument, transferSiteCode).Where(e => e.gr_quantity > 0)
                .Select(e => new TransferDetailInModel
                {
                    Id = e.transfer_id,
                    PartNumber = e.part_number,
                    Description = _context.ITEM.Where(w => w.part_number == e.part_number).Select(s => s.part_description).FirstOrDefault(),
                    Quantity = e.quantity,
                    GRQuantity = e.gr_quantity ?? 0,
                    Cost = e.cost ?? 0,
                    Iva = e.iva ?? 0,
                    Ieps = e.ieps ?? 0,
                    ScanStatus = e.scan_status
                }).ToList();
        }

        public List<TransferDetailModel> GetTransferDetailsInReport(string document, string transferSiteCode)
        {
            var global = _globalContext.TRANSFER_HEADER
                .Where(e => e.transfer_document == document && e.transfer_site_code == transferSiteCode && e.destination_site_code == thisSiteCode)
                .Select(e => e.TRANSFER_DETAIL)
                .FirstOrDefault()?
                .Where(w =>  w.destination_site_code == thisSiteCode)
                .Select(e => new TransferDetailModel
                {
                    PartNumber = e.part_number,
                    Description = e.ITEM.part_description,
                    Quantity = e.quantity,
                    UnitCost = e.cost ?? 0,
                    Iva = e.iva ?? 0,
                    IEPS = e.ieps ?? 0,
                    GrQuantity = e.gr_quantity,
                    ScanStatus = e.scan_status ? 1 : 0,
                    TransferStatus = e.TRANSFER_HEADER.transfer_status,
                    transfer_status = e.transfer_status,
                    DestinationSiteCode = e.TRANSFER_HEADER.transfer_site_code
                }).ToList();

            return global;
        }

        public bool CheckAllDetailsIn(int id, string doc, string transferSiteCode)
        {
            var global = _globalContext.TRANSFER_DETAIL.Where(e => e.transfer_document == doc && e.transfer_status == 4 && e.transfer_site_code == transferSiteCode && e.destination_site_code == thisSiteCode).ToList();

            if (global.Any() && global.All(e => e.scan_status)) return false;

            return true;
        }

        public List<PurchaseOrderItemDetailReportGeneral> GetAllTransferDetail(DateTime date1, DateTime date2, string TransferType, string status, string originSite, string partNumber, string department, string family)
        {
            var consultaGenerico = @"
                        SELECT 'SALIDA: TRANSFERENCIA' TransationType, CASE
                        WHEN TD.transfer_status = 9 THEN 'Terminada'
                        WHEN TD.transfer_status = 8 THEN 'Cancelada'
                        WHEN TD.transfer_status = 7 THEN 'Solicitud Cancelacion'
                        WHEN TD.transfer_status = 6 THEN 'Solicitud Cancelacion'
                        WHEN TD.transfer_status = 4 AND ISNULL(TD.gr_quantity,0) = TD.quantity AND TD.scan_status = 1 THEN 'En Transito'
                        WHEN TD.transfer_status = 4 THEN 'En Transito'
                        WHEN TD.transfer_status = 3 THEN 'Camino transito'
                        WHEN TD.transfer_status = 2 THEN 'Rechazado Gerencia'
                        WHEN TD.transfer_status = 1 THEN 'Aprobada'
                        WHEN TD.transfer_status = 0 THEN 'Creada'
                        END as Item_Status, MC1.class_name Department, MC2.class_name Family,
                        TD.part_number PartNumber, I.part_description DescriptionC,
                        I.unit_size UnitSize,
                        TD.quantity Quantity, 'MXN' Currency,
                        TD.cost CostUnit,
                        CAST(TD.quantity*TD.cost AS decimal(18,4)) SubTotal,
                        M1.name TASA_IVA,
                        CAST(TD.iva* TD.quantity AS decimal(18,4)) IVA,
                        M2.name TASA_IEPS,
                        CAST(TD.ieps*TD.quantity AS decimal(18,4)) IEPS,
                        CAST(TD.ieps*TD.quantity AS decimal(18,4)) + CAST(TD.iva*TD.quantity AS decimal(18,4))+CAST(TD.cost*TD.quantity AS decimal(18,4))  Amount,
                        S.site_name Supplier, TH.transfer_document Folio, CONVERT(VARCHAR,TH.udate,101) MerchandiseEntry, CONVERT(VARCHAR,TH.cdate,101) MerchandiseEntryEta
                        FROM TRANSFER_DETAIL TD
                        JOIN TRANSFER_HEADER TH ON TH.transfer_document = TD.transfer_document and TH.transfer_site_code = TD.transfer_site_code and TH.destination_site_code = TD.destination_site_code
                        JOIN SITES S ON S.site_code = TH.destination_site_code
                        JOIN ITEM I ON I.part_number = TD.part_number
                        JOIN MA_TAX M1 ON M1.tax_code = I.part_iva_sale
                        JOIN MA_TAX M2 ON M2.tax_code = I.part_ieps
                        JOIN MA_CLASS MC1 ON MC1.class_id = I.level_header
                        JOIN MA_CLASS MC2 ON MC2.class_id = I.level1_code
                        WHERE CONVERT(date, ISNULL(TH.udate, TH.cdate),101) >= @date1 and CONVERT(date, ISNULL(TH.udate, TH.cdate),101) <= @date2 ";
            //Parametros
            List<SqlParameter> parameters = new List<SqlParameter>();
            parameters.Add(new SqlParameter("@date1", date1.Date));
            parameters.Add(new SqlParameter("@date2", date2.Date));

            if (TransferType == "salida")
            {
                consultaGenerico += " AND TH.transfer_site_code = @thisSite ";
                parameters.Add(new SqlParameter("@thisSite", thisSiteCode)); //La Tienda donde salio
                if (!string.IsNullOrWhiteSpace(originSite))
                {
                    consultaGenerico += " AND TH.destination_site_code = @originSite ";
                    parameters.Add(new SqlParameter("@originSite", originSite)); //La tienda Destino
                }
            }
            else
            {
                consultaGenerico += " AND TH.destination_site_code = @thisSite ";
                parameters.Add(new SqlParameter("@thisSite", thisSiteCode)); //La tienda Destino
                if (!string.IsNullOrWhiteSpace(originSite))
                {
                    consultaGenerico += " AND TH.transfer_site_code = @originSite ";
                    parameters.Add(new SqlParameter("@originSite", originSite)); //La Tienda donde salio
                }
            }

            if (!string.IsNullOrWhiteSpace(status))
            {
                consultaGenerico += " and TH.transfer_status = @status AND TD.transfer_status = @status ";
                parameters.Add(new SqlParameter("@status", Convert.ToInt32(status)));
            }
            if (!string.IsNullOrWhiteSpace(department))
            {
                consultaGenerico += " and I.level_header = @depart";
                parameters.Add(new SqlParameter("@depart", Convert.ToInt32(department)));
            }
            if (!string.IsNullOrWhiteSpace(family))
            {
                consultaGenerico += " and I.level1_code = @fam";
                parameters.Add(new SqlParameter("@fam", Convert.ToInt32(family)));
            }
            if (!string.IsNullOrWhiteSpace(partNumber))
            {
                consultaGenerico += " and TD.part_number = @part_number";
                parameters.Add(new SqlParameter("@part_number", partNumber));
            }
            return _globalContext.Database.SqlQuery<PurchaseOrderItemDetailReportGeneral>(consultaGenerico, parameters.ToArray()).ToList();

            //var q = _globalContext.TRANSFER_HEADER.Where(e => DbFunctions.TruncateTime(e.transfer_date) >= DbFunctions.TruncateTime(date1) && DbFunctions.TruncateTime(e.transfer_date) <= DbFunctions.TruncateTime(date2));

            //if (TransferType == "salida")
            //{
            //    q = q.Where(e => e.transfer_site_code == thisSiteCode);
            //    if (!string.IsNullOrWhiteSpace(originSite))
            //        q = q.Where(e => e.destination_site_code == originSite);
            //}
            //else //entrada
            //{
            //    q = q.Where(e => e.destination_site_code == thisSiteCode);
            //    if (!string.IsNullOrWhiteSpace(originSite))
            //        q = q.Where(e => e.transfer_site_code == originSite);
            //}

            //if (!string.IsNullOrWhiteSpace(status))
            //{
            //    var statusN = Convert.ToInt32(status);
            //    q = q.Where(e => e.transfer_status == statusN);
            //}

            //if (!string.IsNullOrWhiteSpace(department))
            //{
            //    int dep = Convert.ToInt32(department);
            //    q = q.Where(e => e.TRANSFER_DETAIL.Any(e2 => e2.ITEM.level_header == dep));
            //}

            //if (!string.IsNullOrWhiteSpace(family))
            //{
            //    int fam = Convert.ToInt32(family);
            //    q = q.Where(e => e.TRANSFER_DETAIL.Any(e2 => e2.ITEM.level1_code == fam));
            //}

            //if (!string.IsNullOrWhiteSpace(partNumber))
            //    q = q.Where(e => e.TRANSFER_DETAIL.Any(e2 => e2.part_number == partNumber));



            //    //.Select(e=> new {h=e,d= e.TRANSFER_DETAIL})
            //    var list = q.SelectMany(e => e.TRANSFER_DETAIL,
            //        (h, d) => new PurchaseOrderItemDetailReportGeneral
            //    {
            //        Department = h.TRANSFER_DETAIL.Join(_globalContext.ITEM, dd => dd.part_number, ii => ii.part_number, (dd, ii) => new {dd, ii }).Join(_globalContext.MA_CLASS, mm => mm.ii.level_header, mm1 => mm1.class_id, (mm, mm1) => new { mm, mm1 }).Where(w => w.mm1.class_id == w.mm.ii.level_header).Select(s => s.mm1.class_name).FirstOrDefault(),
            //        Family = h.TRANSFER_DETAIL.Join(_globalContext.ITEM, dd => dd.part_number, ii => ii.part_number, (dd, ii) => new { dd, ii }).Join(_globalContext.MA_CLASS, mm => mm.ii.level1_code, mm1 => mm1.class_id, (mm, mm1) => new { mm, mm1 }).Where(w => w.mm1.class_id == w.mm.ii.level1_code).Select(s => s.mm1.class_name).FirstOrDefault(),
            //            PartNumber = d.part_number,
            //        UnitSize = d.ITEM.unit_size,
            //        DescriptionC = d.ITEM.part_description,
            //        Quantity = d.quantity,
            //        CostUnit = d.cost ?? 0,
            //        SubTotal = d.quantity * (d.cost ?? 0),
            //        IVA = d.quantity *  (d.iva ?? 0),
            //        IEPS = d.quantity * (d.ieps ?? 0),
            //        Amount = d.quantity * (d.cost ?? 0),
            //        Supplier = d.SITES.site_name,
            //        Folio = d.transfer_document,
            //        MerchandiseEntry = DbFunctions.Right("00" + h.transfer_date.Day, 2) + "/" + DbFunctions.Right("00" + h.transfer_date.Month, 2) + "/" + DbFunctions.Right("00" + h.transfer_date.Year, 4),
            //        MerchandiseEntryEta = h.transfer_date_in == null ? "" : DbFunctions.Right("00" + h.transfer_date_in.Value.Day, 2) + "/" + DbFunctions.Right("00" + h.transfer_date_in.Value.Month, 2) + "/" + DbFunctions.Right("00" + h.transfer_date_in.Value.Year, 4), 
            //        Item_Status = d.transfer_status == 0 ? "Creado" :
            //            d.transfer_status == 1 ? "Aprobado" :
            //            d.transfer_status == 2 ? "Cancelado" :
            //            d.transfer_status == 3 ? "Camino transito" :
            //            d.transfer_status == 4 && h.TRANSFER_DETAIL.All(e2 => e2.quantity == e2.gr_quantity && e2.scan_status == true) ? "Recibido" :
            //            d.transfer_status == 4 ? "En Transito" :
            //            d.transfer_status == 8 ? "Cancelado" :
            //            d.transfer_status == 9 ? "Terminado" : "",
            //        Currency = "MXN",
            //        TransationType = TransferType == "salida" ? "Salida" : "Entrada"
            //    }).ToList();
            //    return list;



            //if (TransferType == "salida")
            //{
            //    var consultaGenerico = @"
            //            SELECT 'SALIDA: TRANSFERENCIA' TransationType, CASE
            //            WHEN TD.transfer_status = 9 THEN 'Terminado'
            //            WHEN TD.transfer_status = 8 THEN 'Cancelado'
            //            WHEN TD.transfer_status = 3 THEN 'Transito'
            //            WHEN TD.transfer_status = 2 THEN 'Rechazado'
            //            WHEN TD.transfer_status = 1 THEN 'Aprobado'
            //            WHEN TD.transfer_status = 0 THEN 'Creado'
            //            END as Item_Status, MC1.class_name Department, MC2.class_name Family,
            //            TD.part_number PartNumber, I.part_description DescriptionC,
            //            I.unit_size,
            //            TD.quantity Quantity, 'MXN' Currency,
            //            TD.cost CostUnit,
            //            CAST(TD.quantity*TD.cost AS decimal(18,4)) SubTotal,
            //            M1.name TASA_IVA,
            //            CAST(TD.iva* TD.quantity AS decimal(18,4)) IVA,
            //            M2.name TASA_IEPS,
            //            CAST(TD.ieps*TD.quantity AS decimal(18,4)) IEPS,
            //            CAST(TD.ieps*TD.quantity AS decimal(18,4)) + CAST(TD.iva*TD.quantity AS decimal(18,4))+CAST(TD.cost*TD.quantity AS decimal(18,4))  Amount,
            //            S.site_name Supplier, TH.transfer_document Folio, CONVERT(VARCHAR,TH.udate,101) MerchandiseEntry, CONVERT(VARCHAR,TH.cdate,101) MerchandiseEntryEta
            //            FROM TRANSFER_DETAIL TD
            //            JOIN TRANSFER_HEADER TH ON TH.transfer_document = TD.transfer_document
            //            JOIN SITES S ON S.site_code = TH.destination_site_code
            //            JOIN ITEM I ON I.part_number = TD.part_number
            //            JOIN MA_TAX M1 ON M1.tax_code = I.part_iva_sale
            //            JOIN MA_TAX M2 ON M2.tax_code = I.part_ieps
            //            JOIN MA_CLASS MC1 ON MC1.class_id = I.level_header
            //            JOIN MA_CLASS MC2 ON MC2.class_id = I.level1_code
            //            WHERE CONVERT(date, ISNULL(TH.udate, TH.cdate),101) >= @date1 and CONVERT(date, ISNULL(TH.udate, TH.cdate),101) <= @date2 ";
            //    //Parametros
            //    List<SqlParameter> parameters = new List<SqlParameter>();
            //    parameters.Add(new SqlParameter("@date1", date1.Date));
            //    parameters.Add(new SqlParameter("@date2", date2.Date));
            //    if (!string.IsNullOrWhiteSpace(originSite))
            //    {
            //        consultaGenerico += " AND TH.destination_site_code = @originSite ";
            //        parameters.Add(new SqlParameter("@originSite", originSite)); //Destino
            //    }
            //    if (!string.IsNullOrWhiteSpace(status))
            //    {
            //        consultaGenerico += " and TH.transfer_status = @status AND TD.transfer_status = @status ";
            //        parameters.Add(new SqlParameter("@status", Convert.ToInt32(status)));
            //    }
            //    if (!string.IsNullOrWhiteSpace(department))
            //    {
            //        consultaGenerico += " and I.level_header = @depart";
            //        parameters.Add(new SqlParameter("@depart", Convert.ToInt32(department)));
            //    }
            //    if (!string.IsNullOrWhiteSpace(family))
            //    {
            //        consultaGenerico += " and I.level1_code = @fam";
            //        parameters.Add(new SqlParameter("@fam", Convert.ToInt32(family)));
            //    }
            //    if (!string.IsNullOrWhiteSpace(partNumber))
            //    {
            //        consultaGenerico += " and TD.part_number = @part_number";
            //        parameters.Add(new SqlParameter("@part_number", partNumber));
            //    }
            //    return _context.Database.SqlQuery<PurchaseOrderItemDetailReportGeneral>(consultaGenerico, parameters.ToArray()).ToList();
            //}
            //else
            //{
            //    var consultaGenerico = @"SELECT 'ENTRADA: TRANSFERENCIA' TransationType, CASE
            //     WHEN TH.transfer_status = 9 THEN 'Terminado'
            //     WHEN TH.transfer_status = 8 THEN 'Cancelado'
            //     WHEN TH.transfer_status = 2 THEN 'Recibida'
            //     WHEN TH.transfer_status = 1 THEN 'Pendiente'
            //     END as Item_Status, MC1.class_name Department, MC2.class_name Family,
            //        TD.part_number PartNumber, I.part_description DescriptionC,
            //        I.unit_size,
            //        ISNULL(TD.gr_quantity, TD.quantity) Quantity, 'MXN' Currency,
            //        TD.cost CostUnit,
            //        CAST(ISNULL(TD.gr_quantity, TD.quantity)*TD.cost AS decimal(18,4)) SubTotal,
            //        M1.name TASA_IVA,
            //        CAST(TD.iva*ISNULL(TD.gr_quantity, TD.quantity) AS decimal(18,4)) IVA,
            //        M2.name TASA_IEPS,
            //        CAST(TD.ieps*ISNULL(TD.gr_quantity, TD.quantity) AS decimal(18,4)) IEPS,
            //        CAST(TD.ieps*ISNULL(TD.gr_quantity, TD.quantity) AS decimal(18,4)) + CAST(TD.iva*ISNULL(TD.gr_quantity, TD.quantity) AS decimal(18,4))+CAST(TD.cost*ISNULL(TD.gr_quantity, TD.quantity) AS decimal(18,4))  Amount,
            //        S.site_name Supplier, TH.transfer_document Folio, CONVERT(VARCHAR,TH.ata,101) MerchandiseEntry, CONVERT(VARCHAR,TH.cdate,101) MerchandiseEntryEta
            //        FROM TRANSFER_DETAIL_IN TD
            //        JOIN TRANSFER_HEADER_IN TH ON TH.transfer_id = TD.transfer_id
            //        JOIN SITES S ON S.site_code = TH.origin_site_code
            //        JOIN ITEM I ON I.part_number = TD.part_number
            //        JOIN MA_TAX M1 ON M1.tax_code = I.part_iva_sale
            //        JOIN MA_TAX M2 ON M2.tax_code = I.part_ieps
            //        JOIN MA_CLASS MC1 ON MC1.class_id = I.level_header
            //        JOIN MA_CLASS MC2 ON MC2.class_id = I.level1_code
            //        WHERE ISNULL(ata,CONVERT(date, TH.cdate,101)) >= @date1 and ISNULL(ata,CONVERT(date, TH.cdate,101)) <= @date2 ";
            //    //Parametros
            //    List<SqlParameter> parameters = new List<SqlParameter>();
            //    parameters.Add(new SqlParameter("@date1", date1.Date));
            //    parameters.Add(new SqlParameter("@date2", date2.Date));
            //    if (!string.IsNullOrWhiteSpace(originSite))
            //    {
            //        consultaGenerico += " AND TH.origin_site_code = @originSite";
            //        parameters.Add(new SqlParameter("@originSite", originSite));
            //    }
            //    if (!string.IsNullOrWhiteSpace(status))
            //    {
            //        consultaGenerico += " and TH.transfer_status = @status ";
            //        parameters.Add(new SqlParameter("@status", Convert.ToInt32(status)));
            //    }
            //    if (!string.IsNullOrWhiteSpace(department))
            //    {
            //        consultaGenerico += " and I.level_header = @depart";
            //        parameters.Add(new SqlParameter("@depart", Convert.ToInt32(department)));
            //    }
            //    if (!string.IsNullOrWhiteSpace(family))
            //    {
            //        consultaGenerico += " and I.level1_code = @fam";
            //        parameters.Add(new SqlParameter("@fam", Convert.ToInt32(family)));
            //    }
            //    if (!string.IsNullOrWhiteSpace(partNumber))
            //    {
            //        consultaGenerico += " and TD.part_number = @part_number";
            //        parameters.Add(new SqlParameter("@part_number", partNumber));
            //    }
            //    return _context.Database.SqlQuery<PurchaseOrderItemDetailReportGeneral>(consultaGenerico, parameters.ToArray()).ToList();

        }
    }
}