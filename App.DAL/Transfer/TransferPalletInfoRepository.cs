﻿using App.Entities;
using App.Entities.ViewModels.Transfer;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Runtime.CompilerServices;
using App.GlobalEntities;

namespace App.DAL.Transfer
{
    public class TransferPalletInfoRepository
    {
        private readonly DFL_SAIEntities _context;
        private readonly GlobalERPEntities _globalContext;

        public TransferPalletInfoRepository()
        {
            _context = new DFL_SAIEntities();
            _globalContext = new GlobalERPEntities();
        }

        private string sc;
        private string SiteCode
        {
            get
            {
                if (sc != null) return sc;
                sc = _context.SITE_CONFIG.Select(e => e.site_code).First();
                return sc;
            }
        }

        public bool EditStatusTransferPalletInfo(string Pallet, string User)
        {
            var det_pallet = _globalContext.TRANSFER_IN_PALLET_INFO
                .Where(e => e.pallet_sn == Pallet 
                            && !e.scan_flag 
                            && e.site_code == SiteCode).ToList();
            if (!det_pallet.Any()) return false;
            using (var trans = _globalContext.Database.BeginTransaction())
            {
                try
                {
                    var palletDoc = det_pallet[0].transfer_document;
                    var palletOriSite = det_pallet[0].origin_site_code;
                    foreach (var det in det_pallet)
                    {
                        var det_trans = _globalContext.TRANSFER_DETAIL
                            .SingleOrDefault(e => e.transfer_document == palletDoc 
                                                 && e.transfer_site_code == palletOriSite 
                                                 && e.part_number == det.part_number
                                                 && e.destination_site_code == SiteCode);

                        if (det_trans != null)
                        {
                            det_trans.gr_quantity = (det_trans.gr_quantity ?? 0) + (det.quantity ?? 0);
                            det.scan_flag = true;
                            if ((det_trans.gr_quantity ?? 0) == det_trans.quantity)
                                det_trans.scan_status = true;
                        }
                        else
                            Console.WriteLine(det.part_number);

                        _globalContext.SaveChanges();
                    }

                    var h = _globalContext.TRANSFER_HEADER
                        .FirstOrDefault(e => e.transfer_document == palletDoc 
                                             && e.transfer_site_code == palletOriSite
                                             && e.destination_site_code == SiteCode
                                             && e.TRANSFER_DETAIL.All(d => d.scan_status));
                    if (h != null)
                    {
                        h.transfer_status = 4;
                        _globalContext.SaveChanges();
                    }
                    trans.Commit();
                    return true;
                }
                catch (Exception)
                {
                    trans.Rollback();
                    return false;
                }
            }
        }

        /*Nuevo reporte*/
        public List<NewTransferReport> TransferPalletReport(DateTime date, DateTime date2, int status)
        {
            var list = _globalContext.TRANSFER_IN_PALLET_INFO
                .Join(_globalContext.TRANSFER_HEADER, g => new { g.transfer_document, site_code = g.origin_site_code, destination_site_code = g.site_code }, g => new { g.transfer_document, site_code = g.transfer_site_code,g.destination_site_code }, (p, h) => new { p, h })
                .Where(e => DbFunctions.TruncateTime(e.p.cdate) >= DbFunctions.TruncateTime(date) && DbFunctions.TruncateTime(e.p.cdate) <= DbFunctions.TruncateTime(date2) && e.p.site_code == SiteCode && e.h.transfer_site_code == "0094")
                
                .GroupBy(e => new { e.h.transfer_document, e.h.transfer_status, e.p.pallet_sn, e.h.transfer_site_code, e.h.SITES.site_name,e.p.scan_flag})

                .Select(e => new NewTransferReport
                {
                    PalletSn = e.Key.pallet_sn,
                    TransferStatus = e.Key.transfer_status,
                    Status = e.Key.transfer_status.ToString(),
                    TransferDocument = e.Key.transfer_document,
                    Site = e.Key.transfer_site_code,
                    SiteName = e.Key.site_name,
                    Amount = 0,
                    Ieps = 0,
                    Iva = 0,
                    ScanFlag = e.Key.scan_flag,
                }).ToList();
            return list;
        }

        public List<NewTransferDetailReport> TransferPalletDetailReport(string palletsn)
        {
            var list = _globalContext.TRANSFER_IN_PALLET_INFO
                .Join(_globalContext.TRANSFER_DETAIL, p => new { p.transfer_document, p.part_number, site = p.origin_site_code }, d => new { d.transfer_document, d.part_number, site = d.transfer_site_code }, (p, d) => new { p, d })
                .Where(e => e.p.pallet_sn == palletsn && e.p.site_code == SiteCode)
                .Select(e => new NewTransferDetailReport
                {
                    PartNumber = e.d.part_number,
                    PartDescription = e.d.ITEM.part_description,
                    Quantity = e.p.quantity ?? 0,
                    Cost = e.d.cost ?? 0,
                    Ieps = e.d.ieps ?? 0,
                    Iva = e.d.iva ?? 0
                }).ToList();
            return list;
        }

        public NewTransferReport GetTransferPalletReport(string palletSn)
        {
            var list = _globalContext.TRANSFER_IN_PALLET_INFO
                .Join(_globalContext.TRANSFER_HEADER, g => new { g.transfer_document, site_code = g.origin_site_code }, g => new { g.transfer_document, site_code = g.transfer_site_code }, (p, h) => new { p, h })
                .Where(e => e.p.pallet_sn == palletSn && e.p.site_code == SiteCode)
                .Select(e => new NewTransferReport
                {
                    TransferId = 0,
                    PalletSn = e.p.pallet_sn,
                    TransferStatus = e.h.transfer_status,
                    Status = e.h.transfer_status.ToString(),
                    TransferDocument = e.h.transfer_document,
                    Site = e.h.transfer_site_code,
                    SiteName = e.h.SITES.site_name,
                    Amount = 0,
                    Ieps = 0,
                    Iva = 0
                }).FirstOrDefault();
            return list;
        }

        /**/
        public int CountOtherPalletsInTransfer(string Pallet)
        {
            var p = _globalContext
                .TRANSFER_IN_PALLET_INFO
                .FirstOrDefault(e => e.pallet_sn == Pallet && e.site_code == SiteCode);
            if (p == null) return -1;
            return _globalContext.TRANSFER_IN_PALLET_INFO
                .Count(e => !e.scan_flag 
                            && e.transfer_document == p.transfer_document 
                            && e.origin_site_code == p.origin_site_code 
                            && e.site_code == SiteCode);
        }

        public List<TransferCedis> TransfersToReceive()
        {
            var list = _globalContext.TRANSFER_HEADER.Where(e => e.transfer_site_code == "0094" && e.destination_site_code == SiteCode)
                .Select(e => new TransferCedis
                {
                    TransferDocument = e.transfer_document,
                    TransferDate = e.transfer_date.ToString(),
                    PalletCount = _globalContext
                        .TRANSFER_IN_PALLET_INFO
                        .Where(e2 => e2.transfer_document == e.transfer_document)
                        .GroupBy(e2 => e2.pallet_sn)
                        .Count(),
                    PalletCountScanned = _globalContext
                        .TRANSFER_IN_PALLET_INFO
                        .Where(e2 => e2.transfer_document == e.transfer_document && _globalContext.TRANSFER_IN_PALLET_INFO.Where(e3 => e3.pallet_sn == e2.pallet_sn).All(e3 => e3.scan_flag))
                        .GroupBy(e2 => e2.pallet_sn)
                        .Count()
                }).ToList();
            return list;
        }

        public List<TransferCedisPallet> PalletsInTransfer(string transferDocument)
        {
            var list = _globalContext.TRANSFER_IN_PALLET_INFO
                .Where(e2 => e2.transfer_document == transferDocument && e2.site_code == SiteCode)
                .Select(e => new TransferCedisPallet
                {
                    PalletSN = e.pallet_sn,
                    ScanFlag = _globalContext.TRANSFER_IN_PALLET_INFO.Where(e3 => e3.pallet_sn == e.pallet_sn).All(e3 => e3.scan_flag)
                }).ToList();

            return list;
        }
    }
}