﻿using App.Entities;
using App.Entities.ViewModels.Android;
using App.Entities.ViewModels.Transfer;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using App.GlobalEntities;
using System.Data.SqlClient;

namespace App.DAL.Transfer
{
    public class TransferHeaderInRepository
    {
        private readonly DFL_SAIEntities _context;
        private readonly GlobalERPEntities _globalContext;

        public TransferHeaderInRepository()
        {
            _context = new DFL_SAIEntities();
            _globalContext = new GlobalERPEntities();
        }

        private string sc;
        private string thisSiteCode
        {
            get
            {
                if (sc != null) return sc;
                sc = _context.SITE_CONFIG.Select(e => e.site_code).First();
                return sc;
            }
        }

        public List<TransferReceptionAndroidModel> GetAllTransfersInTransitExcludeCEDIS(string store)
        {
            var global = _globalContext.TRANSFER_HEADER.Where(e => e.transfer_status == 4 && e.destination_site_code == thisSiteCode && e.SITES.site_name.Contains(store) && (e.SITES.site_code != "0094" || !_globalContext.TRANSFER_IN_PALLET_INFO.Any(p => p.transfer_document == e.transfer_document && e.transfer_site_code == p.origin_site_code && e.destination_site_code == p.site_code)))
                .Select(e => new TransferReceptionAndroidModel
                {
                    transfer_id = 0,
                    transfer_document = e.transfer_document,
                    origin_site_code = e.transfer_site_code,
                    origin_site_name = e.SITES.site_name,
                }).ToList();

            return global.OrderBy(e => e.transfer_document).ToList();
        }

        public StoreProcedureResult ProcessTransferIn(int TransferId, string User, string Program, string transferDocument, string transferSiteCode)
        {
            if (_globalContext.TRANSFER_HEADER.Any(e => e.transfer_document == transferDocument && e.destination_site_code == thisSiteCode))
                return ProcessTransferIn_G(transferDocument, User, Program, transferSiteCode);

            var DateReturn = _context.Database.SqlQuery<StoreProcedureResult>("DECLARE	@return_value int, @document nvarchar(10) EXEC    @return_value = [dbo].[sproc_GR_From_Transfer] @transfer_id = N'" + TransferId + "', @cuser = N'" + User + "', @program_id = N'" + Program + "', @document = @document OUTPUT SELECT 'Document'= @Document, 'ReturnValue' = @return_value").ToList();
            return DateReturn[0];
        }

        public StoreProcedureResult ProcessTransferIn_G(string TransferDocument, string User, string Program, string transferSiteCode)
        {
            _context.Database.CommandTimeout = 200;  ///PASILLAS-TRANSFERENCIAS
            var DateReturn = _context.Database.SqlQuery<StoreProcedureResult>("DECLARE	@return_value int, @document nvarchar(20) EXEC    @return_value = [dbo].[sproc_GR_From_Transfer_G] @transfer_document = N'" + TransferDocument + "',   @transfer_site_code = N'" + transferSiteCode + "',   @cuser = N'" + User + "', @program_id = N'" + Program + "', @document = @document OUTPUT SELECT 'Document'= @Document, 'ReturnValue' = @return_value").ToList();
            return DateReturn[0];
        }

        public List<TransferHeaderModel> GetTransfersHeaderIn(DateTime BeginDate, DateTime EndDate, string TransferType, string status, string originSite, string partNumber, string department, string family)
        {

            var consultaGenerico = $@"SELECT HEADER.transfer_document AS 'TransferDocument', ORIGINSITE.site_code AS 'TransferSiteCode', ORIGINSITE.site_name AS 'TransferSiteName', DESTINATIONSITE.site_code AS 'DestinationSiteCode', DESTINATIONSITE.site_name AS 'DestinationSiteName',             
                    HEADER.comment  AS 'Comment',
                    CASE WHEN HEADER.transfer_status = 9 THEN CONVERT(VARCHAR, HEADER.transfer_date_in, 101)
                         WHEN HEADER.transfer_status = 0 THEN CONVERT(VARCHAR, HEADER.cdate, 101)
                         ELSE CONVERT(VARCHAR, HEADER.udate, 101) END AS 'TransferDate',
                    'MXN' Currency,
                    CASE WHEN HEADER.transfer_status = 0 THEN 'Creada' 
						WHEN HEADER.transfer_status = 1 THEN 'Aprobada' 
						WHEN HEADER.transfer_status = 2 THEN 'Rechazado Gerencia'
						WHEN HEADER.transfer_status = 3 THEN 'Camino transito'
						WHEN HEADER.transfer_status = 4 THEN 
							CASE WHEN not exists(select 1 from TRANSFER_DETAIL d where d.transfer_document = header.transfer_document and d.transfer_site_code = header.transfer_site_code and d.destination_site_code = header.destination_site_code 
								and d.quantity <> isnull(d.gr_quantity,0) ) 
							THEN 'Recibido' ELSE 'En Transito' END
                        WHEN HEADER.transfer_status = 6 THEN 'Solicitud Cancelacion' 
                        WHEN HEADER.transfer_status = 7 THEN 'Solicitud Cancelacion' 
						WHEN HEADER.transfer_status = 8 THEN 'Cancelada' 
						WHEN HEADER.transfer_status = 9 THEN 'Terminada' 
					END AS 'Status', 
                    CASE 
                        WHEN DETAIL.amount IS NULL THEN 0 
                        ELSE DETAIL.amount  END AS 'TotalAmount', 
                    CASE WHEN DETAIL.iva IS NULL THEN 0 
                        ELSE DETAIL.iva END AS 'TotalIva',
                    CASE WHEN DETAIL.ieps IS NULL THEN 0 
                        ELSE DETAIL.ieps END AS 'TotalIEPS' 
                    FROM TRANSFER_HEADER HEADER 
                    INNER JOIN SITES ORIGINSITE ON HEADER.transfer_site_code = ORIGINSITE.site_code 
                    INNER JOIN SITES DESTINATIONSITE ON HEADER.destination_site_code = DESTINATIONSITE.site_code 
                    LEFT JOIN (
                        SELECT transfer_document, transfer_site_code, destination_site_code, SUM(quantity * cost) AS amount, 
                        SUM(quantity * iva) AS iva,
                        SUM(quantity * ieps)  AS ieps,
                        SUM(quantity) as Quantity_sum,
                        SUM(gr_quantity) as GR_quantity_sum
                        FROM TRANSFER_DETAIL
                        {(TransferType != "salida"? " where transfer_status not in (2) ":"")}
                        GROUP BY transfer_document, transfer_site_code, destination_site_code 
                    )  DETAIL ON DETAIL.transfer_document = HEADER.transfer_document and DETAIL.transfer_site_code = HEADER.transfer_site_code and DETAIL.destination_site_code = HEADER.destination_site_code
                    
					
					WHERE CONVERT(date, ISNULL(HEADER.udate, HEADER.cdate), 101) BETWEEN @BEGINDATE AND @ENDDATE";
            
            List<SqlParameter> parameters = new List<SqlParameter>();
            parameters.Add(new SqlParameter("@BEGINDATE", BeginDate.Date));
            parameters.Add(new SqlParameter("@ENDDATE",EndDate.Date ));

            if (TransferType == "salida")
            {
                consultaGenerico += " AND HEADER.transfer_site_code = @thisSite AND DETAIL.transfer_site_code = @thisSite";
                parameters.Add(new SqlParameter("@thisSite", thisSiteCode)); //La Tienda donde salio
                if (!string.IsNullOrWhiteSpace(originSite))
                {
                    consultaGenerico += " AND HEADER.destination_site_code = @originSite AND DETAIL.destination_site_code = @originSite";
                    parameters.Add(new SqlParameter("@originSite", originSite)); //La tienda Destino
                }
            }
            else
            {
                consultaGenerico += " AND HEADER.destination_site_code = @thisSite AND DETAIL.destination_site_code = @thisSite and header.transfer_status >=4";
                parameters.Add(new SqlParameter("@thisSite", thisSiteCode)); //La tienda Destino
                if (!string.IsNullOrWhiteSpace(originSite))
                {
                    consultaGenerico += " AND HEADER.transfer_site_code = @originSite AND DETAIL.transfer_site_code = @originSite";
                    parameters.Add(new SqlParameter("@originSite", originSite)); //La Tienda donde salio
                }
            }

            if (!string.IsNullOrWhiteSpace(status))
            {
                consultaGenerico += " and HEADER.transfer_status = @status ";
                parameters.Add(new SqlParameter("@status", Convert.ToInt32(status)));
            }
            //if (!string.IsNullOrWhiteSpace(department))
            //{
            //    consultaGenerico += " and I.level_header = @depart";
            //    parameters.Add(new SqlParameter("@depart", Convert.ToInt32(department)));
            //}
            //if (!string.IsNullOrWhiteSpace(family))
            //{
            //    consultaGenerico += " and I.level1_code = @fam";
            //    parameters.Add(new SqlParameter("@fam", Convert.ToInt32(family)));
            //}
            if (!string.IsNullOrWhiteSpace(partNumber))
            {
                consultaGenerico += " and DETAIL.part_number = @part_number";
                parameters.Add(new SqlParameter("@part_number", partNumber));
            }
            return _globalContext.Database.SqlQuery<TransferHeaderModel>(consultaGenerico, parameters.ToArray()).ToList();


        }

        public List<TransferHeaderModel> GetTransfersHeaderInView(DateTime BeginDate, DateTime EndDate, string TransferType, string status, string originSite, string partNumber, string department, string family)
        {

            var q = _globalContext.TRANSFER_HEADER.Where(e => DbFunctions.TruncateTime(e.transfer_date) >= DbFunctions.TruncateTime(BeginDate) && DbFunctions.TruncateTime(e.transfer_date) <= DbFunctions.TruncateTime(EndDate));

            if (TransferType == "salida")
            {
                q = q.Where(e => e.transfer_site_code == thisSiteCode);
                if (!string.IsNullOrWhiteSpace(originSite))
                    q = q.Where(e => e.destination_site_code == originSite);
            }
            else //entrada
            {
                q = q.Where(e => e.destination_site_code == thisSiteCode);
                if (!string.IsNullOrWhiteSpace(originSite))
                    q = q.Where(e => e.transfer_site_code == originSite);
            }

            
            var list = q.Select(e => new TransferHeaderModel
            {
                TransferDocument = e.transfer_document,
                TransferSiteCode = e.transfer_site_code,
                TransferSiteName = e.SITES.site_name,
                TransferDate = e.transfer_date.ToString(),
                TransferDateD = e.transfer_date,
                DriverCode = e.driver_code,
                Comment = e.comment,
                TransferStatus = e.transfer_status,
                PrintStatus = e.print_status,
                Status = e.transfer_status == 0 ? "Creada" :
                        e.transfer_status == 1 ? "Aprobada" :
                        e.transfer_status == 2 ? "Cancelada" :
                        e.transfer_status == 3 ? "Camino transito" :
                        e.transfer_status == 4 && _globalContext.TRANSFER_DETAIL.Where(e2 =>
                            e2.transfer_document==e.transfer_document 
                            && e2.transfer_site_code==e.transfer_site_code  
                            &&  e2.destination_site_code == e.destination_site_code )
                            .Any(e2 =>  e2.quantity != (e2.gr_quantity ?? 0)) ? "En Transito" :
                        e.transfer_status == 4 ? "Recibida" :
                        e.transfer_status == 6 ? "Solicitud Cancelacion" :
                        e.transfer_status == 7 ? "Solicitud Cancelacion" :
                        e.transfer_status == 8 ? "Cancelada" :
                        e.transfer_status == 9 ? "Terminada" : "",
                CUser = e.cuser,
                UUser = e.uuser,
                AutorizeName = e.authorized_by,
                AccomplishedName = null,
                DestinationSiteCode = e.destination_site_code,
                DestinationSiteName = e.SITES1.site_name,
                TotalAmount = e.TRANSFER_DETAIL.Any() ? e.TRANSFER_DETAIL.Sum(e2 => e2.quantity * (e2.cost ?? 0)) : 0,
                TotalIva = e.TRANSFER_DETAIL.Any() ? e.TRANSFER_DETAIL.Sum(e2 => e2.quantity * (e2.ieps ?? 0)) : 0,
                TotalIEPS = e.TRANSFER_DETAIL.Any() ? e.TRANSFER_DETAIL.Sum(e2 => e2.quantity * (e2.iva ?? 0)) : 0,
                Diferences = e.TRANSFER_DETAIL.Any(e2 => e2.quantity != e2.gr_quantity) ? e.TRANSFER_DETAIL.Sum(e2 => (e2.gr_quantity ?? 0) - e2.quantity).ToString() : "Sin diferencia"
            }).ToList();


            foreach (var e in list)
                e.TransferDate = e.TransferDateD.ToString("MM/dd/yyyy");

            return list.OrderBy(e => e.TransferDocument).ToList();
        }
    }
}