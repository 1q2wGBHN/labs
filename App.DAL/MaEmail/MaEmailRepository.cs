﻿using App.Entities.ViewModels.MaEmail;
using App.GlobalEntities;
using System.Linq;

namespace App.DAL.MaEmail
{
    public class MaEmailRepository
    {
        private readonly GlobalERPEntities _contextGlobal;

        public MaEmailRepository()
        {
            _contextGlobal = new GlobalERPEntities();
        }

        public MaEmailModel GetEmailsSiteCode(string site)
        {
            return _contextGlobal.MA_EMAIL.Where(x => x.site_code == site)
                .Select(s => new MaEmailModel
                {
                    site_code = s.site_code,
                    mesa_control_email = s.mesa_control_email,
                    gerencia_email = s.gerencia_email,
                    recibo_email = s.recibo_email,
                    cajagral_email = s.caja_gral_email
                }).FirstOrDefault();
        }
    }
}