﻿using App.Entities;
using System.Linq;
using static App.Entities.ViewModels.Common.Constants;

namespace App.DAL.DigitalTaxStamp
{
    public class DigitalTaxStampRepository
    {
        private DFL_SAIEntities _context;

        public DigitalTaxStampRepository()
        {
            _context = new DFL_SAIEntities();
        }

        public DIGITAL_TAX_STAMP GetStampInfo(long Number, string Serie, string DocumentType)
        {
            var StampInfo = new DIGITAL_TAX_STAMP();

            StampInfo = _context.DIGITAL_TAX_STAMP.Where(c => c.cfdi_serie == Serie && c.cfdi_id == Number && c.sat_document_code == DocumentType && c.pac_name == StampPac.PAC_NAME).FirstOrDefault();

            return StampInfo;
        }

        public bool IsInvoiceStamped(long Number)
        {            
            var exist = (from dts in _context.DIGITAL_TAX_STAMP
                         join i in _context.INVOICES on dts.cfdi_id equals i.cfdi_id
                         where i.invoice_no == Number
                               && dts.sat_document_code == VoucherType.INCOMES                                                           
                         select i).Any();

            return exist;
        }

        public bool IsCreditNoteBonStamped(long Number)
        {
            var exist = (from dts in _context.DIGITAL_TAX_STAMP
                         join cn in _context.CREDIT_NOTES on dts.cfdi_id equals cn.cn_id
                         where cn.cn_id == Number
                               && dts.sat_document_code == VoucherType.EXPENSES                               
                         select cn).Any();

            return exist;
        }

        public bool IsPaymentStamped(long Number)
        {
            var exist = (from dts in _context.DIGITAL_TAX_STAMP
                         join p in _context.PAYMENTS on dts.cfdi_id equals p.payment_no
                         where p.payment_no == Number
                               && dts.sat_document_code == VoucherType.PAYMENTS
                         select p).Any();

            return exist;
        }        
    }
}
