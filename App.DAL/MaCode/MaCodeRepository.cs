﻿using App.Entities;
using App.Entities.ViewModels.MaCode;
using System.Collections.Generic;
using System.Linq;

namespace App.DAL.MaCode
{
    public class MaCodeRepository
    {
        private readonly DFL_SAIEntities _context;

        public MaCodeRepository()
        {
            _context = new DFL_SAIEntities();
        }

        public List<MA_CODE> GetListCurrency()
        {
            var Currency = _context.MA_CODE.Where(x => x.code == "CURRENCY").ToList();

            return Currency;
        }

        public List<MaCodeModel> GetListTypeLocations()
        {
            var typeLocation = _context.MA_CODE.Where(x => x.code == "TYPE_LOCATION")
                .Select(x => new MaCodeModel
                {
                    VKey = x.vkey
                }).ToList();

            return typeLocation;
        }

        public List<MaCodeModel> GetListTypeDamagedMerchadiseReasons()
        {
            var typeLocation = _context.MA_CODE.Where(x => x.code == "SCRAP_REASONS")
                .Select(x => new MaCodeModel
                {
                    VKey = x.vkey
                }).ToList();

            return typeLocation;
        }

        public List<MA_CODE> GetListTypeOffer()
        {
            var typeLocation = _context.MA_CODE.Where(x => x.code == "OFERTA").ToList();
            return typeLocation;
        }

        public List<MA_CODE> GetListGeneric(string Generic)
        {
            var typeLocation = _context.MA_CODE.Where(x => x.code == Generic && x.used == "1").OrderBy(x => x.vkey_seq).ToList();
            return typeLocation;
        }
        public List<MA_CODE> GetAllCodeByCode(string code)
        {
            return _context.MA_CODE.Select(x => x).Where(x => x.code == code).ToList();
        }

        public string GetEmailDistrict()
        {
            return _context.Database.SqlQuery<string>(@"select description
                from MA_CODE
                where code = 'DISTRICT_EMAIL' and 
                vkey = (select district_code from SITES where site_code = (select site_code from SITE_CONFIG))").FirstOrDefault();
        }

        public int GetPermisionXML(string site_code, string rfc_supplier)
        {
            var sitePermission = _context.MA_CODE.Where(w => w.code == "XML_REQUIRED" && w.vkey == site_code && w.used == "1").FirstOrDefault();
            if (sitePermission != null)
            {
                var supplierPermision = _context.MA_CODE.Where(w => w.code == "XML_AVOIDABLE_SUPPLIERS" && w.vkey == rfc_supplier && w.used == "1").FirstOrDefault();
                if (supplierPermision != null)
                    return 0; //No es requerido XML especificamente con este proveedor 
                else
                    return 1; //ES OBLIGATORIO XML
            }
            return 0; //No es requerido XML (Permite entrad con remision y XML)
        }

        public bool GetWeighingMachineCAS()
        {
            var site_code = _context.SITE_CONFIG.Select(s => s.site_code).FirstOrDefault();
            var WeighingMachineCAS = _context.MA_CODE.Where(x => x.code == "WeighingMachineCAS" && x.used == "1" && x.vkey == site_code).OrderBy(x => x.vkey_seq).FirstOrDefault();
            if (WeighingMachineCAS != null)
                return true;
            return false;
        }

    }
}