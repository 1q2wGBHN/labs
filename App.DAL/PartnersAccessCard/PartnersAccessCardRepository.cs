﻿using System;
using System.Linq;
using App.Entities;
using System.Data.Entity;
using System.Data.SqlClient;
using static System.DateTime;

namespace App.DAL.PartnersAccessCard
{
    public class PartnersAccessCardRepository
    {
        private DFL_SAIEntities _context;

        public PartnersAccessCardRepository()
        {
            _context = new DFL_SAIEntities();
        }

        //CRUD
        public IQueryable<DFLPOS_USER_CARD_ACCESS> GetAll()
        {
            var partnersCardAccess = _context.DFLPOS_USER_CARD_ACCESS.Include(p => p.USER_MASTER);
            return partnersCardAccess;
        }

        // GET: DFLPOS_USER_CARD_ACCESS/Details/5
        public DFLPOS_USER_CARD_ACCESS Details(int? id)
        {
            if (id == null)
                return null;
            return _context.DFLPOS_USER_CARD_ACCESS.Find(id);
        }

       
        public DFLPOS_USER_CARD_ACCESS Create( DFLPOS_USER_CARD_ACCESS partnersCardAccess)
        {
            var cant = _context.DFLPOS_USER_CARD_ACCESS.Where(w => w.card_number == partnersCardAccess.card_number && w.emp_id != partnersCardAccess.emp_id)
                .Count();
            if (cant > 0)
            {
                throw new Exception("No. de tarjeta ya existe.");
            }
            cant = _context.DFLPOS_USER_CARD_ACCESS.Where(w => w.card_number != partnersCardAccess.card_number && w.emp_id == partnersCardAccess.emp_id)
                .Count();
            if (cant > 0)
            {
                throw new Exception("Empleado ya registrado.");
            }
            cant = _context.DFLPOS_USER_CARD_ACCESS.Where(w => w.card_number == partnersCardAccess.card_number && w.emp_id == partnersCardAccess.emp_id)
                .Count();
            if (cant > 0)
            {
                throw new Exception("No. de tarjeta y empleado ya registrados.");
            }
            _context.DFLPOS_USER_CARD_ACCESS.Add(partnersCardAccess);
                DFLPOS_USER_CARD_ACCESS_LOGS log = new DFLPOS_USER_CARD_ACCESS_LOGS
                {
                    prev_card_number= partnersCardAccess.card_number,
                    time_stamp= Now,
                    type = "Insert",
                    prev_emp_id = partnersCardAccess.emp_id,
                    modify_id = partnersCardAccess.id

                };
                _context.DFLPOS_USER_CARD_ACCESS_LOGS.Add(log);
            try
                {
                    _context.SaveChanges();
                }
                catch (Exception ex)
                {
                    throw new Exception("Este dato ya se ha registrado.");
                }
                
                return partnersCardAccess;
        }

       

        
        public DFLPOS_USER_CARD_ACCESS Edit(DFLPOS_USER_CARD_ACCESS partnersCardAccess)
        {

            var cant = _context.DFLPOS_USER_CARD_ACCESS.Where(w => w.card_number == partnersCardAccess.card_number && w.emp_id != partnersCardAccess.emp_id)
                .Count();
                if (cant>0)
                {
                    throw new Exception("No. de tarjeta ya existe.");
                }
                cant = _context.DFLPOS_USER_CARD_ACCESS.Where(w => w.card_number != partnersCardAccess.card_number && w.emp_id == partnersCardAccess.emp_id)
                    .Count();
                if (cant > 1)
                {
                    throw new Exception("Empleado ya registrado.");
                }
                cant = _context.DFLPOS_USER_CARD_ACCESS.Where(w => w.card_number == partnersCardAccess.card_number && w.emp_id == partnersCardAccess.emp_id)
                    .Count();
                if (cant > 1)
                {
                    throw new Exception("No. de tarjeta y empleado ya registrados.");
                }
            var old = _context.DFLPOS_USER_CARD_ACCESS.AsNoTracking().FirstOrDefault(f => f.id == partnersCardAccess.id);
            _context.Entry(old).State = EntityState.Detached;

                //_context.Entry(partnersCardAccess).State = EntityState.Modified;

                var userCard = _context.DFLPOS_USER_CARD_ACCESS.FirstOrDefault(f => f.id == partnersCardAccess.id);
                if (userCard != null) userCard.card_number = partnersCardAccess.card_number;
                if (old!=null)
                {
                    DFLPOS_USER_CARD_ACCESS_LOGS log = new DFLPOS_USER_CARD_ACCESS_LOGS
                    {
                        prev_card_number = old.card_number,
                        new_card_number = partnersCardAccess.card_number,
                        time_stamp = Now,
                        type = "Update",
                        prev_emp_id = old.emp_id,
                        new_emp_id = partnersCardAccess.emp_id,
                        modify_id = partnersCardAccess.id

                    };
                    _context.Entry(log).State = EntityState.Added;
                }
               
                try
                {
                    _context.SaveChanges();
                }
                catch (Exception e)
                {

                    throw new Exception("Este dato ya se ha registrado.");
                }
                
                return userCard;
        }

       

        // POST: DFLPOS_USER_CARD_ACCESS/Delete/5
        public void Delete(int id)
        {
            DFLPOS_USER_CARD_ACCESS partnersCardAccess = _context.DFLPOS_USER_CARD_ACCESS.Find(id);
            if (partnersCardAccess != null) _context.DFLPOS_USER_CARD_ACCESS.Remove(partnersCardAccess);
            _context.SaveChanges();
        }

        

    }
}
