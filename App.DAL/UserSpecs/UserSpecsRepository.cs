﻿using App.Entities;
using App.Entities.ViewModels.UserSpecs;
using App.GlobalEntities;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;

namespace App.DAL.UserSpecs
{
    public class UserSpecsRepository
    {
        private readonly DFL_SAIEntities _context;
        private readonly GlobalERPEntities _contextGlobal;

        public UserSpecsRepository()
        {
            _context = new DFL_SAIEntities();
            _contextGlobal = new GlobalERPEntities();
        }

        public List<UserSpecsModel> GetAllUserSpecs()
        {
            var SiteCode = _context.SITE_CONFIG.SingleOrDefault();
            var x =  _contextGlobal.Database.SqlQuery<UserSpecsModel>(@"  
                    select us.driver_flag as DriverFlag, 
                    us.emp_no as EmpNo, 
                    us.full_name as FullName,
                    us.mobile_tel as MobileTel,
                    si.site_code as Site,
                    si.address as SiteAddress,
                    si.site_name as SiteName from USER_SPECS us inner join  SITES si on si.site_code = us.site_code
                    where us.driver_flag = 1").ToList();
            return x;
        }

        public List<UserSpecsModel> GetAllUserSpecsAndDriveFalse()
        {
            var SiteCode = _context.SITE_CONFIG.SingleOrDefault();
            return _contextGlobal.Database.SqlQuery<UserSpecsModel>(@"  
                    select us.driver_flag as DriverFlag, 
                    us.emp_no as EmpNo, 
                    us.full_name as FullName,
                    us.mobile_tel as MobileTel,
                    si.site_code as Site,
                    si.address as SiteAddress,
                    si.site_name as SiteName from USER_SPECS us inner join  SITES si on si.site_code = us.site_code ").ToList();
        }

        public bool SaveUserSpeck(UserSpecsModel UserSpecs, string User)
        {
            try
            {
                var SiteCode = _context.SITE_CONFIG.SingleOrDefault();
                _contextGlobal.Database.ExecuteSqlCommand(@"
                    insert INTO  USER_SPECS (emp_no,full_name,site_code,driver_flag,mobile_tel,cdate,cuser,program_id)
                    values (@NumberEmployee,@FullName,@Site,@FlagDriver,@Phone,GETDATE(),@User,'DRIV001')",
                    new SqlParameter("@NumberEmployee", UserSpecs.EmpNo),
                    new SqlParameter("@FullName", UserSpecs.FullName),
                    new SqlParameter("@Site", SiteCode.site_code),
                    new SqlParameter("@FlagDriver", UserSpecs.DriverFlag),
                    new SqlParameter("@Phone", UserSpecs.MobileTel),
                    new SqlParameter("@User", User));
                return true;
            }
            catch (Exception ex)
            {
                string mdg = ex.Message;
                return false;
            }
        }

        public bool EditUserSpeck(UserSpecsModel UserSpecs, string User)
        {
            try
            {
                var SiteCode = _context.SITE_CONFIG.SingleOrDefault();
                _contextGlobal.Database.ExecuteSqlCommand(@"
                    update USER_SPECS set driver_flag = @FlagDriver, mobile_tel = @Phone,udate = GETDATE(), uuser = @User, program_id = 'DRIV001'
                    where emp_no = @NumberEmployee",
                    new SqlParameter("@NumberEmployee", UserSpecs.EmpNo),
                    new SqlParameter("@FlagDriver", UserSpecs.DriverFlag),
                    new SqlParameter("@Phone", UserSpecs.MobileTel),
                    new SqlParameter("@User", User));
                return true;
            }
            catch (Exception ex)
            {
                string msg = ex.Message;
                return false;
            }
        }
    }
}