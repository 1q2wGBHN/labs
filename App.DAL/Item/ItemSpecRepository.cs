﻿using App.Entities;
using App.Entities.ViewModels.Item;
using System.Collections.Generic;
using System.Linq;

namespace App.DAL.Item
{
    public class ItemSpecRepository
    {
        private readonly DFL_SAIEntities _context;

        public ItemSpecRepository()
        {
            _context = new DFL_SAIEntities();
        }

        public List<ItemSpeckModel> ListItemSpeck(string PartNumber)
        {
            return _context.ITEM_SPECS.Where(x => x.part_number == PartNumber).Select(x => new ItemSpeckModel
            {
                GrStorageLocation = x.gr_storage_location,
                PackingOfSize = x.packing_of_size ?? 0,
                PartNumber = x.part_number,
                PalletOfSize = x.pallet_of_size ?? 0,
                WeightOfPacking = x.weight_of_packing ?? 0
            }).ToList();
        }
    }
}