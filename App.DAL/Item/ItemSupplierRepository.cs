﻿using App.Entities;
using App.Entities.ViewModels.Android;
using App.Entities.ViewModels.Item;
using App.Entities.ViewModels.Order;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;

namespace App.DAL.Item
{
    public class ItemSupplierRepository
    {
        private readonly DFL_SAIEntities _context;

        public ItemSupplierRepository()
        {
            _context = new DFL_SAIEntities();
        }

        public List<ItemModel> GetAll(int Supplier)
        {
            return _context.ITEM_SUPPLIER.Join(_context.ITEM, its => its.part_number, it => it.part_number, (its, it) => new { its, it }).Where(x => x.its.supplier_id == Supplier && !_context.ITEM_SUPPLIER_INACTIVE.Any(y => y.supplier_part_number == x.its.supplier_part_number)).Select(x => new ItemModel
            {
                Description = x.it.part_description,
                PathNumber = x.its.part_number
            }).ToList();
        }

        public List<ItemModel> GetListItemCurrency(int Supplier, string Currency)
        {
            return _context.Database.SqlQuery<ItemModel>(@"select its.part_number as PathNumber,  it.part_description as Description from ITEM_SUPPLIER its
                    inner join PURCHASE_BASE_COST pbc on pbc.supplier_part_number = its.supplier_part_number
                    inner join ITEM it on it.part_number = its.part_number
                    where currency is not null and (it.combo_flag = 0 or it.combo_flag is null) and it.active_flag = 1 and (it.extra_items_flag = 0  or it.extra_items_flag is null) and
                    its.supplier_id = @supplier_id and currency = @currency and pbc.status= 1
                    --se agrego lo de abajo
                    and its.supplier_part_number not in (select supplier_part_number from ITEM_SUPPLIER_INACTIVE) and its.part_number not in (select part_number from ITEM_BLOCKED WHERE active_flag = 1 and site_code = (SELECT top 1 site_code FROM SITE_CONFIG))",
                    new SqlParameter("@supplier_id", Supplier),
                    new SqlParameter("@currency", Currency)).ToList();
        }

        public List<ItemSupplierBaseCost> GetListItemBaseCost(string PartNumber)
        {
            return _context.Database.SqlQuery<ItemSupplierBaseCost>(@"select ms.business_name as Supplier, part_number as PartNumber,base_cost as Cost from PURCHASE_BASE_COST pbc 
                    inner join ITEM_SUPPLIER  its on its.supplier_part_number = pbc.supplier_part_number
                    inner join MA_SUPPLIER ms on ms.supplier_id  = its.supplier_id
                    where part_number =  @partNumber and pbc.status = 1
                    and its.supplier_part_number not in (select supplier_part_number from ITEM_SUPPLIER_INACTIVE)",
                    new SqlParameter("@partNumber", PartNumber)).ToList();
        }

        public List<ItemModel> ItemSupppler(int Supplier)
        {
            return _context.ITEM_SUPPLIER.Join(_context.ITEM, its => its.part_number, it => it.part_number, (its, it) => new { its, it })
                    .Join(_context.PURCHASE_BASE_COST, x => x.its.supplier_part_number, pbc => pbc.supplier_part_number, (x, pbc) => new { x, pbc })
                    .Join(_context.MA_TAX, s => s.x.it.part_iva_purchase, mt => mt.tax_code, (s, mt) => new { s, mt })
                    .Join(_context.MA_TAX, p => p.s.x.it.part_ieps, mt2 => mt2.tax_code, (p, mt2) => new { p, mt2 })
                    .Where(x => x.p.s.x.its.supplier_id == Supplier && x.p.s.pbc.status == 1)
                    .Select(iv => new ItemModel
                    {
                        PathNumber = iv.p.s.x.it.part_number,
                        Description = iv.p.s.x.it.part_description,
                        PartIva = iv.p.mt.tax_value.Value,
                        TaxValue = iv.mt2.tax_value.Value,
                        BaseCost = iv.p.s.pbc.base_cost.Value,
                        UnitSize = iv.p.s.x.it.unit_size
                    }).Distinct().ToList();
        }

        public List<ItemModel> GetIeps(string partNumber)
        {
            return _context.ITEM.Join(_context.MA_TAX, it => it.part_ieps, mt => mt.tax_code, (it, mt) => new { it, mt })
                .Where(x => x.it.part_number == partNumber)
                 .Select(iv => new ItemModel
                 {
                     TaxValue = iv.mt.tax_value != null ? iv.mt.tax_value.Value : 0

                 }).ToList();
        }

        public List<ITEM_SPECS> GetPacking(string PathNumber)
        {
            return _context.ITEM_SPECS.Where(x => x.part_number == PathNumber).ToList();
        }

        public List<OrderListModel> GetListOrder(int Supplier, string Site, string Currency)
        {
            var OrderList = _context.Database.SqlQuery<OrderListModel>("select vw_order_format.part_number as ParthNumber, vw_order_format.part_description as [Description], vw_order_format.unrestricted as Stock,  vw_order_format.currency as Currency, vw_order_format.open_quantity as PrePruchase, vw_order_format.avg_sale as AverageSale, vw_order_format.packing_of_size as Packing, vw_order_format.family as Family, vw_order_format.department as Department, vw_order_format.dep as Dep, vw_order_format.fam as Fam from vw_order_format where supplier_id = @supplier and currency = @currency", new SqlParameter("@supplier", Supplier), new SqlParameter("@currency", Currency)).ToList();
            return OrderList;
        }

        public ItemSupplierModel GetItemSupplierByItemAndSupplier(string part_number, int supplier_id)
        {
            return _context.ITEM_SUPPLIER
                .Join(_context.ITEM, i => i.part_number, ii => ii.part_number, (i, ii) => new { i, ii })
                .Where(f => f.i.part_number == part_number && f.i.supplier_id == supplier_id
                && !_context.ITEM_SUPPLIER_INACTIVE.Any(y => y.supplier_part_number == f.i.supplier_part_number))
                .Select(x => new ItemSupplierModel
                {
                    supplier_item_id = x.i.supplier_part_number,
                    supplier_id = supplier_id,
                    part_number = x.i.part_number,
                    part_description = x.ii.part_description
                }).SingleOrDefault();
        }

        public bool ValidatePartNumberBySite(string part_number, string site_code_destination)
        {
            string site_code_current = "";
            if (site_code_destination == "")
                site_code_current = _context.SITE_CONFIG.Select(s => s.site_code).FirstOrDefault(); //Esta tienda/sucursal
            else
                site_code_current = site_code_destination;

            var part_number_site = _context.ITEM_BLOCKED.Where(w => w.part_number == part_number && w.site_code == site_code_current && w.active_flag == true).FirstOrDefault();

            if (part_number_site != null)
                return false;  //NO ESTA DISPONIBLE PARA ESTA TIENDA 
            else
                return true;//Disponible para esta tienda
        }

        public ItemSupplierModel GetEntryFree(string part_number, int supplier_id, string Type)
        {
            if (Type == "Con registro en inventario")
            {
                return _context.ITEM_SUPPLIER
                .Join(_context.ITEM, i => i.part_number, ii => ii.part_number, (i, ii) => new { i, ii })
                .Join(_context.ITEM_SPECS, isp => isp.i.part_number, iisp => iisp.part_number, (isp, iisp) => new { isp, iisp })
                .Join(_context.STORAGE_LOCATION, ist => ist.iisp.gr_storage_location, iist => iist.storage_location_name, (ist, iist) => new { ist, iist })
                .Where(f => f.ist.isp.i.part_number == part_number && f.ist.isp.i.supplier_id == supplier_id && f.iist.storage_type == "VENTA" && f.ist.iisp.gr_storage_location == f.iist.storage_location_name)
                .Select(x => new ItemSupplierModel
                {
                    supplier_item_id = x.ist.isp.i.supplier_part_number,
                    supplier_id = supplier_id,
                    part_number = x.ist.isp.i.part_number,
                    part_description = x.ist.isp.ii.part_description
                }).SingleOrDefault();
            }
            return _context.ITEM_SUPPLIER
                .Join(_context.ITEM, i => i.part_number, ii => ii.part_number, (i, ii) => new { i, ii })
                .Where(f => f.i.part_number == part_number && f.i.supplier_id == supplier_id)
                .Select(x => new ItemSupplierModel
                {
                    supplier_item_id = x.i.supplier_part_number,
                    supplier_id = supplier_id,
                    part_number = x.i.part_number,
                    part_description = x.ii.part_description
                }).SingleOrDefault();
        }

        public List<ItemStockModel> GetItemsSupplier(int Supplier)
        {
            var Items = _context.ITEM_SUPPLIER.Join(_context.ITEM, s => s.part_number,
                   ss => ss.part_number, (s, ss) => new { s, ss })
                   .Join(_context.ITEM_STOCK, it => it.s.part_number,
                   itt => itt.part_number, (it, itt) => new { it, itt })
                   .Join(_context.ITEM_VALUATION, pu => pu.it.s.part_number,
                   pur => pur.part_number, (pu, pur) => new { pu, pur })
                   .Join(_context.STORAGE_LOCATION, st => st.pu.itt.storage_location,
                   sto => sto.storage_location_name, (st, sto) => new { st, sto }).OrderBy(x => x.st.pu.it.s.part_number)
                   .Where(s => s.st.pu.it.s.supplier_id == Supplier && s.st.pu.itt.blocked > 0 && s.sto.storage_type == "MERMA" &&
                    !_context.ITEM_SUPPLIER_INACTIVE.Any(y => y.supplier_part_number == s.st.pu.it.s.supplier_part_number))
               .Select(s => new ItemStockModel
               {
                   PartNumber = s.st.pu.it.s.part_number,
                   Description = s.st.pu.it.ss.part_description,
                   Size = s.st.pu.it.ss.unit_size,
                   Block = s.st.pu.itt.blocked != null ? s.st.pu.itt.blocked.Value : 0,
                   Storage = s.st.pu.itt.storage_location,
                   Price = s.st.pur.last_purchase_price != null ? s.st.pur.last_purchase_price.Value : 0
               }).ToList();
            return Items;
        }

        public ItemStockModel GetSingleItemSupplier(int Supplier, string PartNumber)
        {
            var Items = _context.ITEM_SUPPLIER.Join(_context.ITEM, s => s.part_number,
                   ss => ss.part_number, (s, ss) => new { s, ss })
                   .Join(_context.ITEM_STOCK, it => it.s.part_number,
                   itt => itt.part_number, (it, itt) => new { it, itt })
                   .Join(_context.ITEM_VALUATION, pu => pu.it.s.part_number,
                   pur => pur.part_number, (pu, pur) => new { pu, pur })
                   .Join(_context.STORAGE_LOCATION, st => st.pu.itt.storage_location,
                   sto => sto.storage_location_name, (st, sto) => new { st, sto }).OrderBy(x => x.st.pu.it.s.part_number).Where(s => s.st.pu.it.s.supplier_id == Supplier && s.st.pu.it.s.part_number == PartNumber && s.st.pu.itt.blocked > 0 && s.sto.storage_type == "MERMA")
               .Select(s => new ItemStockModel
               {
                   PartNumber = s.st.pu.it.s.part_number,
                   Description = s.st.pu.it.ss.part_description,
                   Size = s.st.pu.it.ss.unit_size,
                   Block = s.st.pu.itt.blocked != null ? s.st.pu.itt.blocked.Value : 0,
                   Storage = s.st.pu.itt.storage_location,
                   Price = s.st.pur.map_price != null ? s.st.pur.map_price.Value : 0
               }).SingleOrDefault();
            return Items;
        }

        public ItemStockModel GetPrice(string part_number)
        {
            var Item = _context.ITEM_VALUATION.Where(x => x.part_number == part_number).Select(s => new ItemStockModel
            {
                PartNumber = s.part_number,
                Price = s.map_price != null ? s.map_price.Value : 0
            }).FirstOrDefault();
            return Item;
        }

        public decimal ItemStock(string PartNumber)
        {
            var Quantity = _context.vw_available_blocked_stock.Where(x => x.part_number == PartNumber).SingleOrDefault();
            if (Quantity != null)
                return Quantity.Disponible != null ? Quantity.Disponible.Value : 0;

            return 0;
        }

        public List<ItemSupplierModelPrice> GetItemsBySupplierCodeInSalePrice(int supplier_code)
        {
            return _context.ITEM_SUPPLIER.Join(_context.ITEM, itemSupplier => itemSupplier.part_number, item => item.part_number, (itemSupplier, item) => new { itemSupplier, item })
                .Join(_context.PURCHASE_BASE_COST, i => i.itemSupplier.supplier_part_number, pbc => pbc.supplier_part_number, (i, pbc) => new { i, pbc })
                .Join(_context.SALES_PRICE_CHANGE, items => items.i.itemSupplier.part_number, spc => spc.part_number, (items, spc) => new { items, spc })
                .Join(_context.MA_TAX, s => s.items.i.item.part_iva_purchase, iva => iva.tax_code, (s, iva) => new { s, iva })
                .Join(_context.MA_TAX, ss => ss.s.items.i.item.part_ieps, ieps => ieps.tax_code, (ss, ieps) => new { ss, ieps })
                .Where(x => x.ss.s.items.i.itemSupplier.supplier_id == supplier_code && x.ss.s.items.pbc.status == 1 && x.ss.s.spc.status == 1 && x.ss.s.items.i.item.combo_flag != true && x.ss.s.items.i.item.active_flag == true && !_context.ITEM_SUPPLIER_INACTIVE.Any(y => y.supplier_part_number == x.ss.s.items.i.itemSupplier.supplier_part_number))
                .OrderByDescending(k => k.ss.s.spc.cdate)
                .ToList()
                .Select(q => new ItemSupplierModelPrice
                {
                    item_number = q.ss.s.items.i.item.part_number,
                    item_name = q.ss.s.items.i.item.part_description,
                    item_cost = q.ss.s.items.pbc.base_cost.ToString(),
                    margen = q.ss.s.spc.margin.ToString(),
                    iva = q.ss.iva.tax_value.ToString(),
                    ieps = q.ieps.tax_value.ToString(),
                    price = q.ss.s.spc.sale_price.ToString(),
                    uDate = q.ss.s.spc.applied_date != null ? q.ss.s.spc.applied_date.ToString("MM/dd/yyyy") : ""
                }).ToList();
        }

        public List<ItemSupplierModelPrice> GetItemHistoryBaseCost(string part_number)
        {
            return _context.ITEM_SUPPLIER.Join(_context.PURCHASE_BASE_COST, IS => IS.supplier_part_number, PB => PB.supplier_part_number, (IS, PB) => new { IS, PB })
                .Join(_context.MA_SUPPLIER, PB2 => PB2.IS.supplier_id, MS => MS.supplier_id, (PB2, MS) => new { PB2, MS })
                .Where(w => w.PB2.IS.part_number == part_number)
                .OrderByDescending(o => o.PB2.PB.cdate)
                .Take(80)
                .Select(s => new ItemSupplierModelPrice
                {
                    item_number = s.PB2.IS.part_number,
                    fechainicio = s.PB2.PB.valid_from,
                    proveedor = s.MS.business_name,
                    base_cost = s.PB2.PB.base_cost ?? 0,
                    user = s.PB2.PB.uuser ?? s.PB2.PB.cuser,
                    active_flag = s.PB2.PB.status == 1 ? "Activo" : "Desactivado"
                }).ToList();
        }
    }
}