﻿using App.Entities;
using App.Entities.ViewModels.DamagedsGoods;
using App.Entities.ViewModels.Item;
using App.Entities.ViewModels.Inventory;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using static App.Entities.ViewModels.Common.Constants;

namespace App.DAL.Item
{
    public class ItemRepository
    {
        private readonly DFL_SAIEntities _context;

        public ItemRepository()
        {
            _context = new DFL_SAIEntities();
        }

        public List<ITEM> GetAll()
        {
            return _context.ITEM.ToList();
        }

        public List<ITEM> SearchItem(string part_number)
        {
            return _context.ITEM.Where(x => x.part_number.Contains(part_number) || x.part_description.Contains(part_number)).ToList();
        }
        public List<InventoryNegativeDetailModel> SearchItemNegativeDet(string part_number , string negative)
        {
            var r = _context.Database.SqlQuery<InventoryNegativeDetailModel>(@"SELECT 
            IV.part_number AS 'part_number_detail',
            i.part_description as 'part_description',
            isnull(isnull(iv.map_price,iv.last_purchase_price),0) as 'cost',
            its.unrestricted
            FROM ITEM_VALUATION IV
            JOIN ITEM_STOCK ITS ON ITS.part_number = IV.part_number
            JOIN ITEM I ON I.part_number = IV.part_number
            WHERE ITS.storage_location = 'SL01'
            AND its.unrestricted > 0
            AND (ITS.part_number like '%"+part_number+"%' or i.part_description  like '%"+part_number+ @"%')
            AND I.level_header = (select level_header from item where part_number = '" + negative + @"')").ToList();
            return r ;
        }

        public List<ItemModel> SearchItemNegative(string part_number)
        {
            //int?[] lista = new int?[] { 70, 89, 66, 95, 96, 97, 98, 99, 100, 101, 102, 103, 104, 105, 106 };
            //var r = _context.ITEM
            //    .Join(_context.ITEM_VALUATION, I => I.part_number, IS => IS.part_number, (I, IS) => new { I, IS })
            //    .Where(x => (x.I.part_number.Contains(part_number) || x.I.part_number.Contains(part_number) || x.I.part_description.Contains(part_number)) && x.I.extra_items_flag != true && x.I.combo_flag != true && x.I.active_flag == true && x.IS.total_stock < 0 && (!lista.Contains(x.I.level_header)))
            //    .Select(s => new ItemModel
            //    {
            //        Description = s.I.part_description,
            //        PathNumber = s.I.part_number
            //    }).ToList();
            //return r;
            var r = _context.Database.SqlQuery<ItemModel>(@"SELECT 
            IV.part_number AS 'PathNumber',
            i.part_description as 'Description'
            FROM ITEM_VALUATION IV
            JOIN ITEM I ON I.part_number = IV.part_number
            WHERE I.extra_items_flag != 1 and i.combo_flag != 1 and i.active_flag = 1 and iv.total_stock < 0
			and i.level_header not in ( 70, 89, 66, 95, 96, 97, 98, 99, 100, 101, 102, 103, 104, 105, 106)
            AND (I.part_number like '%"+part_number+ "%' or i.part_description  like '%" + part_number + @"%')
            and i.part_number not in (select top 1 part_number_header from INVENTORY_NEGATIVE_HEADER where part_number_header = i.part_number and negative_status = 0)").ToList();
            return r;
        }

        public List<ITEM> SearchItemValidateNo(string part_number, string not_part_number)
        {
            return _context.ITEM.Where(x => !x.part_number.Contains(not_part_number) || x.part_number.Contains(part_number) || x.part_description.Contains(part_number)).ToList();
        }

        public List<ItemModel> SearchItemBarcode(string part_number)
        {
            return _context.ITEM
                .Join(_context.BARCODES, document => document.part_number, docItem => docItem.part_number, (document, docItem) => new { document, docItem })
                .Where(x => (x.document.part_number.Contains(part_number) || x.document.part_description.Contains(part_number) || x.docItem.part_barcode.Contains(part_number))
                && x.document.extra_items_flag != true && x.document.combo_flag != true && x.document.active_flag == true)
                .Select(x => new ItemModel
                {
                    Description = x.document.part_description,
                    PathNumber = x.document.part_number
                }).Distinct().ToList();
        }

        public List<ItemModel> SearchItemBarcodePreInventory(string part_number)
        {
            return _context.ITEM
                .Join(_context.BARCODES, document => document.part_number, docItem => docItem.part_number, (document, docItem) => new { document, docItem })
                .Where(x => (x.document.part_number.Contains(part_number) || x.document.part_description.Contains(part_number) || x.docItem.part_barcode.Contains(part_number))
                && x.document.extra_items_flag != true && x.document.combo_flag != true)
                .Select(x => new ItemModel
                {
                    Description = x.document.part_description,
                    PathNumber = x.document.part_number
                }).Distinct().ToList();
        }




        public List<ItemModel> SearchItemBarcodeComboPresentation(string part_number)
        {
            return _context.ITEM
                .Join(_context.BARCODES, document => document.part_number, docItem => docItem.part_number, (document, docItem) => new { document, docItem })
                .Where(x => (x.document.part_number.Contains(part_number) || x.document.part_description.Contains(part_number) || x.docItem.part_barcode.Contains(part_number))
                && x.document.active_flag == true)
                .Select(x => new ItemModel
                {
                    Description = x.document.part_description,
                    PathNumber = x.document.part_number
                }).Distinct().ToList();
        }

        public List<ItemModel> SearchItemBarcodeSupplier(string part_number, int supplier)
        {
            return _context.ITEM_SUPPLIER
                .Join(_context.ITEM, item => item.part_number, itemSupplier => itemSupplier.part_number, (itemSupplier, item) => new { itemSupplier, item })
                .Join(_context.BARCODES, document => document.item.part_number, docItem => docItem.part_number, (document, docItem) => new { document, docItem })
                .Where(x => (x.document.item.part_number.Contains(part_number) || x.document.item.part_description.Contains(part_number) || x.docItem.part_barcode.Contains(part_number))
                && x.document.item.extra_items_flag != true && x.document.item.combo_flag != true && x.document.item.active_flag == true && x.document.itemSupplier.supplier_id == supplier
                && !_context.ITEM_SUPPLIER_INACTIVE.Any(y => y.supplier_part_number == x.document.itemSupplier.supplier_part_number))
                .Select(x => new ItemModel
                {
                    Description = x.document.item.part_description,
                    PathNumber = x.document.item.part_number
                }).Distinct().ToList();
        }
        public List<ItemModel> SearchItemBarcodeSupplierCurrency(string part_number, int supplier, string currency)
        {
            return _context.Database.SqlQuery<ItemModel>(@"
            select distinct(i.part_number), i.part_description as 'Description' , i.part_number as 'PathNumber' from ITEM_SUPPLIER its
            join ITEM i on its.part_number = i.part_number
            join BARCODES b on b.part_number = i.part_number
            join PURCHASE_BASE_COST pbc on pbc.supplier_part_number = its.supplier_part_number and pbc.status = 1
            where
            (i.part_number like '%" + part_number + "%' or i.part_description like '%" + part_number + "%' or  b.part_barcode like '%" + part_number + @"%')
            and isnull(i.extra_items_flag,0) != 1 and isnull(i.combo_flag,0) != 1 and isnull(i.active_flag,0) = 1 and its.supplier_id = '" + supplier + @"'
            and its.supplier_part_number not in
            (select supplier_part_number from ITEM_SUPPLIER_INACTIVE isi where isi.supplier_part_number = its.supplier_part_number)
            and i.part_number NOT IN (SELECT part_number FROM ITEM_BLOCKED WHERE part_number = i.part_number AND active_flag = 1 AND site_code = (SELECT TOP 1 site_code FROM SITE_CONFIG))
            and pbc.currency = '" + currency + "'").ToList();
        }

        public int GetDepartmentByProduct(string part_number)
        {
            return _context.ITEM.Where(w => w.part_number == part_number).Select(s => s.level_header ?? 0).FirstOrDefault();
        }

        public int GetFamilyByProduct(string part_number)
        {
            return _context.ITEM.Where(w => w.part_number == part_number).Select(s => s.level1_code ?? 0).FirstOrDefault();
        }

        public List<DropItemModel> GetAllActives()
        {
            return _context.ITEM.Where(x => x.active_flag == true).Select(x => new DropItemModel
            {
                PathNumber = x.part_number,
                Description = x.part_description
            }).ToList();
        }

        public List<ItemModelDropDown> GetAllProductsToDropDown()
        {
            return _context.ITEM.Where(x => x.active_flag == true && x.combo_flag != true)
                .Select(x => new ItemModelDropDown
                {
                    PartNumber = x.part_number,
                    Description = x.part_description
                }).ToList();
        }

        public List<ItemModelDropDown> GetItemsByFamily(int LevelCode)
        {
            return _context.ITEM.Where(c => c.level1_code == LevelCode).Select(x => new ItemModelDropDown { PartNumber = x.part_number, Description = x.part_description }).ToList();
        }
        public List<ItemModelDropDown> GetItemsByFamilyActive(int LevelCode)
        {
            return _context.ITEM.Where(c => c.level1_code == LevelCode).Select(x => new ItemModelDropDown { PartNumber = x.part_number, Description = x.part_description }).ToList();
        }

        public List<ItemModelDropDown> GetItemsByDepartment(int LevelCode)
        {
            return _context.ITEM.Where(c => c.level_header == LevelCode).Select(x => new ItemModelDropDown { PartNumber = x.part_number, Description = x.part_description }).ToList();
        }

        public ItemModel GetOneItem(string PartNumber)
        {
            return _context.ITEM
                .Where(x => x.part_number == PartNumber)
                .Select(x => new ItemModel
                {
                    UnitSize = x.unit_size ?? "None",
                    IVADescription = x.part_iva_purchase,
                    IEPSDescription = x.part_ieps,
                    buyerdivision = x.buyer_division,
                    PathNumber = x.part_number,
                    Description = x.part_description,
                    SalePrice = _context.SALES_PRICE_CHANGE
                    .Where(p => p.part_number == x.part_number && p.status == 1)
                    .Select(e => e.sale_price)
                    .FirstOrDefault()
                }).SingleOrDefault();
        }

        public string ParthNumber(string ParthNumber)
        {
            var Item = _context.ITEM.SingleOrDefault(x => x.part_number == ParthNumber);
            if (Item != null)
                return Item.part_number;

            return "";
        }

        public string PartDescription(string part_number)
        {
            var Item = _context.ITEM.FirstOrDefault(x => x.part_number == part_number);
            if (Item != null)
                return Item.part_description;

            return "";
        }

        public StoreProcedureIvaIeps StoreProcedureIvaIeps(string PartNumber)
        {
            var item = _context.Database.SqlQuery<StoreProcedureIvaIeps>("DECLARE	@return_value int, @iva_value_purchase decimal(18, 4), @iva_value decimal(18, 4), @ieps_value decimal(18, 4) EXEC    @return_value = [dbo].[sp_GetIvaIepsPartNumber] @part_number = N'" + PartNumber + "', @iva_value_purchase = @iva_value_purchase OUTPUT, @iva_value = @iva_value OUTPUT, @ieps_value = @ieps_value OUTPUT SELECT  'Iva' = @iva_value, 'Ieps' = @ieps_value, 'IvaPurchase' = @iva_value_purchase").ToList();
            return item[0];
        }

        public string GetItemLevelHeaderDamageds(string part_number)
        {
            var item = _context.ITEM.Where(w => (w.level_header == 53 || w.level_header == 89) && w.part_number == part_number).SingleOrDefault();
            if (item != null)
                return item.part_number;
            else
                return "";
        }

        public ItemModel GetListItemStockByPartNumber(string PartNumber)
        {
            var Item = _context.ITEM_VALUATION.Where(y => y.part_number == PartNumber)
                .Select(x => new ItemModel
                {
                    PathNumber = x.part_number,
                    Stock = x.unrestricted
                })
                .FirstOrDefault();
            return Item;
        }

        public decimal GetMapPrice(string PartNumber)
        {
            return _context.ITEM_VALUATION
                .FirstOrDefault(y => y.part_number == PartNumber)?.map_price ?? 0;
        }

        public List<DamagedsGoodsLastCost> GetLastPurchasePrice(string PartNumber, int supplier_id, int top, decimal currentCost)
        {
            try
            {
                _context.Database.CommandTimeout = 160;
                var DateReturn = _context.Database.SqlQuery<DamagedsGoodsLastCost>($"DECLARE  @return_value int EXEC	@return_value = [dbo].[sp_Get_Last_Cost_ERP_CDSERP] @part_number = N'"+PartNumber+ "', @supplier_id = "+supplier_id+ ", @top_1 = "+top+"").ToList();
                if(DateReturn.Count() == 0)
                {
                    DamagedsGoodsLastCost newObj = new DamagedsGoodsLastCost();
                    newObj.t_cost = 0;
                    DateReturn.Add(newObj);
                }
                return DateReturn;
            }
            catch (Exception)
            {
                List<DamagedsGoodsLastCost> obj = new List<DamagedsGoodsLastCost>();
                DamagedsGoodsLastCost newObj = new DamagedsGoodsLastCost();
                newObj.t_cost = currentCost;
                obj.Add(newObj);
                return obj;
            }
            
        }

        public bool InventoryCCEditStatusPrint(string Document, string part_number, int Status, string program_id)
        {
            try
            {
                var inventory = _context.INVENTORY_CC.Where(elements => elements.inventory_cc_id == Document && elements.part_number == part_number).SingleOrDefault();
                inventory.program_id = program_id + "+Print";
                inventory.print_status = Status + 1;
                _context.SaveChanges();
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public ItemKardexModel GetInformationInventoryCC(string document)
        {
            //Por Documento
            return _context.INVENTORY_CC.Join(_context.USER_MASTER, inv => inv.cuser, user => user.user_name, (inv, user) => new { inv, user })
                .Where(w => w.inv.inventory_cc_id == document)
                .Select(s => new ItemKardexModel
                {
                    CreateBy = s.user.first_name + " " + s.user.last_name,
                    MovementDocument = s.inv.inventory_cc_id,
                    DocumentStatus = s.inv.adjusted_flag == null ? "Pendiente"
                        : s.inv.adjusted_flag == false ? "Rechazado"
                        : s.inv.adjusted_flag == true ? "Aprobado"
                        : null,
                    MovementDate = s.inv.udate != null ? s.inv.udate.ToString() : s.inv.cdate.ToString()
                }).FirstOrDefault();
        }

        public ItemKardexModel GetInformationInventoryCCFinal(string document, string part_number)
        {
            //Por Item
            var x = _context.INVENTORY_CC.Join(_context.USER_MASTER, userPast => userPast.cuser, user2 => user2.user_name, (userPast, user2) => new { userPast, user2 })
                .Join(_context.ITEM, itemPast => itemPast.userPast.part_number, item => item.part_number, (itemPast, item) => new { itemPast, item })
                .Where(w => w.itemPast.userPast.inventory_cc_id == document && w.itemPast.userPast.part_number == part_number)
                .Select(s => new ItemKardexModel
                {
                    AuthorizationBy = s.itemPast.userPast.authorized_by ?? "",
                    CreateBy = s.itemPast.user2.first_name + " " + s.itemPast.user2.last_name,
                    MovementDocument = s.itemPast.userPast.adjusted_document ?? s.itemPast.userPast.inventory_cc_id,
                    DocumentStatus = s.itemPast.userPast.adjusted_flag == null ? "Pendiente"
                        : s.itemPast.userPast.adjusted_flag == false ? "Rechazado"
                        : s.itemPast.userPast.adjusted_flag == true ? "Aprobado"
                        : null,
                    DebitCredit = s.itemPast.userPast.dr_cr == "Dr" ? "DEBITO"
                    : s.itemPast.userPast.dr_cr == "Cr" ? "CREDITO"
                    : null,
                    TimeDocumentDate = s.itemPast.userPast.udate,
                    MovementQuantity = s.itemPast.userPast.cc_quantity,
                    MapPrice = s.itemPast.userPast.map_price ?? 0,
                    Reason = s.itemPast.userPast.comment,
                    MovementDate = s.itemPast.userPast.udate != null ? s.itemPast.userPast.udate.ToString() : s.itemPast.userPast.cdate.ToString(),
                    PartNumber = s.itemPast.userPast.part_number,
                    PartDescription = s.item.part_description,
                    print_status = s.itemPast.userPast.print_status ?? 0
                }).FirstOrDefault();
            return x;
        }

        public List<ItemKardexModel> GetInventoryCC(DateTime? date1, DateTime? date2, string status, string part_number, string type_movent)
        {
            DateTime today = DateTime.Today;
            var document = _context.INVENTORY_CC.Join(_context.ITEM, itemPast => itemPast.part_number, item => item.part_number, (itemPast, item) => new { itemPast, item })
                .Join(_context.USER_MASTER, userPast => userPast.itemPast.cuser, user => user.user_name, (userPast, user) => new { userPast, user });

            if (date1.HasValue && date2.HasValue)
                document = document.Where(w => w.userPast.itemPast.udate != null ? (w.userPast.itemPast.udate >= date1 && w.userPast.itemPast.udate <= date2) : (w.userPast.itemPast.cdate >= date1 && w.userPast.itemPast.cdate <= date2));
            else
                document = document.Where(w => w.userPast.itemPast.udate != null ? (w.userPast.itemPast.udate.Value.Month == today.Month && w.userPast.itemPast.udate.Value.Year == today.Year) : (w.userPast.itemPast.cdate.Value.Month == today.Month && w.userPast.itemPast.cdate.Value.Year == today.Year));

            if (!string.IsNullOrWhiteSpace(type_movent))
                document = document.Where(w => w.userPast.itemPast.dr_cr == type_movent);

            if (!string.IsNullOrWhiteSpace(status))
            {   // "" <- Todos los registros 
                if (status == "1") // "1" <-- Los aprobados
                    document = document.Where(w => w.userPast.itemPast.adjusted_flag == true);
                else if (status == "2")// "2" <-- Los rechazados
                    document = document.Where(w => w.userPast.itemPast.adjusted_flag == false);
                else if (status == "3") // "3" <-- Los Pendientes
                    document = document.Where(w => w.userPast.itemPast.adjusted_flag == null);
            }

            if (!string.IsNullOrWhiteSpace(part_number))
                document = document.Where(w => w.userPast.itemPast.part_number == part_number);

            return document.OrderBy(ob => ob.userPast.itemPast.cdate).Select(s => new ItemKardexModel
            {
                AuthorizationBy = s.userPast.itemPast.authorized_by ?? "",
                PartNumber = s.userPast.itemPast.part_number,
                CreateBy = s.user.first_name + " " + s.user.last_name,
                MovementDate = s.userPast.itemPast.udate.Value.ToString(),
                PartDescription = s.userPast.item.part_description,
                MovementDocument = s.userPast.itemPast.adjusted_document,
                MovementReference = s.userPast.itemPast.inventory_cc_id,
                StorageLocation = s.userPast.itemPast.storage_location,
                DocumentStatus = s.userPast.itemPast.adjusted_flag == null ? "Pendiente"
                        : s.userPast.itemPast.adjusted_flag == false ? "Rechazado"
                        : s.userPast.itemPast.adjusted_flag == true ? "Aprobado"
                        : null,
                DebitCredit = s.userPast.itemPast.dr_cr.ToUpper() == "DR" ? "Entrada/D"
                    : s.userPast.itemPast.dr_cr.ToUpper() == "CR" ? "Salida/C"
                    : null,
                Reason = s.userPast.itemPast.comment,
                MovementQuantity = s.userPast.itemPast.cc_quantity ?? 0,
                StorageLocationStock = s.userPast.itemPast.item_stock_current ?? 0,
                TotalStock = s.userPast.itemPast.dr_cr.ToUpper() == "DR" ? s.userPast.itemPast.item_stock_current == null ? 0 : s.userPast.itemPast.item_stock_current + s.userPast.itemPast.cc_quantity
                        : s.userPast.itemPast.dr_cr.ToUpper() == "CR" ? s.userPast.itemPast.item_stock_current == null ? 0 : s.userPast.itemPast.item_stock_current - s.userPast.itemPast.cc_quantity
                        : 0
            }).ToList();
        }

        public List<ItemKardexModel> GetInventoryCCPending(DateTime? date1, DateTime? date2, string status, string part_number, string type_movent)
        {
            DateTime today = DateTime.Today;
            var document = _context.INVENTORY_CC.Join(_context.TRANSACTION_DOCUMENT_ITEM, docTrans => docTrans.adjusted_document, docItem => docItem.document_no, (docTrans, docItem) => new { docTrans, docItem })
                .Join(_context.ITEM, itemPast => itemPast.docItem.part_number, item => item.part_number, (itemPast, item) => new { itemPast, item })
                .Join(_context.USER_MASTER, userPast => userPast.itemPast.docTrans.authorized_by, user => user.user_name, (userPast, user) => new { userPast, user })
                .Join(_context.USER_MASTER, userPast2 => userPast2.userPast.itemPast.docTrans.cuser, user => user.user_name, (userPast2, user) => new { userPast2, user });

            if (date1.HasValue && date2.HasValue)
                document = document.Where(w => w.userPast2.userPast.itemPast.docTrans.udate != null ? (w.userPast2.userPast.itemPast.docTrans.udate >= date1 && w.userPast2.userPast.itemPast.docTrans.udate <= date2) : (w.userPast2.userPast.itemPast.docTrans.cdate >= date1 && w.userPast2.userPast.itemPast.docTrans.cdate <= date2));
            else
                document = document.Where(w => w.userPast2.userPast.itemPast.docTrans.udate != null ? (w.userPast2.userPast.itemPast.docTrans.udate.Value.Month == today.Month && w.userPast2.userPast.itemPast.docTrans.udate.Value.Year == today.Year) : (w.userPast2.userPast.itemPast.docTrans.cdate.Value.Month == today.Month && w.userPast2.userPast.itemPast.docTrans.cdate.Value.Year == today.Year));

            if (!string.IsNullOrWhiteSpace(type_movent))
                document = document.Where(w => w.userPast2.userPast.itemPast.docTrans.dr_cr == type_movent);

            if (!string.IsNullOrWhiteSpace(status))
            {   // "" <- Todos los registros 
                if (status == "1") // "1" <-- Los aprobados
                    document = document.Where(w => w.userPast2.userPast.itemPast.docTrans.adjusted_flag == true);
                else if (status == "2")// "2" <-- Los rechazados
                    document = document.Where(w => w.userPast2.userPast.itemPast.docTrans.adjusted_flag == false);
                else if (status == "3") // "3" <-- Los Pendientes
                    document = document.Where(w => w.userPast2.userPast.itemPast.docTrans.adjusted_flag == null);
            }

            if (!string.IsNullOrWhiteSpace(part_number))
                document = document.Where(w => w.userPast2.userPast.itemPast.docTrans.part_number == part_number);

            return document.OrderBy(ob => ob.userPast2.userPast.itemPast.docTrans.cdate).Select(s => new ItemKardexModel
            {
                PartNumber = s.userPast2.userPast.itemPast.docTrans.part_number,
                AuthorizationBy = s.userPast2.user.first_name + " " + s.userPast2.user.last_name,
                CreateBy = s.user.first_name + " " + s.user.last_name,
                MovementDate = s.userPast2.userPast.itemPast.docTrans.udate.Value.ToString(),
                PartDescription = s.userPast2.userPast.item.part_description,
                MovementDocument = s.userPast2.userPast.itemPast.docTrans.adjusted_document,
                MovementReference = s.userPast2.userPast.itemPast.docTrans.inventory_cc_id,
                StorageLocation = s.userPast2.userPast.itemPast.docTrans.storage_location,
                DocumentStatus = s.userPast2.userPast.itemPast.docTrans.adjusted_flag == null ? "Pendiente"
                        : s.userPast2.userPast.itemPast.docTrans.adjusted_flag == false ? "Rechazado"
                        : s.userPast2.userPast.itemPast.docTrans.adjusted_flag == true ? "Aprobado"
                        : null,
                DebitCredit = s.userPast2.userPast.itemPast.docTrans.dr_cr == "Dr" ? "Entrada/D"
                    : s.userPast2.userPast.itemPast.docTrans.dr_cr == "Cr" ? "Salida/C"
                    : null,
                Reason = s.userPast2.userPast.itemPast.docTrans.comment,
                MovementQuantity = s.userPast2.userPast.itemPast.docTrans.cc_quantity ?? 0,
                StorageLocationStock = s.userPast2.userPast.itemPast.docItem.total_stock == null ? 0 : s.userPast2.userPast.itemPast.docItem.total_stock,
                TotalStock = s.userPast2.userPast.itemPast.docItem.debit_credit.ToUpper() == "DR" ? s.userPast2.userPast.itemPast.docItem.total_stock == null ? 0 : s.userPast2.userPast.itemPast.docItem.total_stock + s.userPast2.userPast.itemPast.docItem.quantity
                        : s.userPast2.userPast.itemPast.docItem.debit_credit.ToUpper() == "CR" ? s.userPast2.userPast.itemPast.docItem.total_stock == null ? 0 : s.userPast2.userPast.itemPast.docItem.total_stock - s.userPast2.userPast.itemPast.docItem.quantity
                        : 0
            }).ToList();
        }

        public List<ItemKardexModel> GetAllMovementsByProductAndDateRange(string part_number, DateTime date1, DateTime date2)
        {
            var mov = new[] { "GR", "GI", "CC" };
            _context.Database.CommandTimeout = 1000000;
            var movementsQuery = _context.TRANSACTION_DOCUMENT.Join(_context.TRANSACTION_DOCUMENT_ITEM, document => document.document_no, docItem => docItem.document_no, (document, docItem) => new { document, docItem })
                .Where(x => mov.Contains(x.docItem.document_type) && x.docItem.part_number == part_number && x.document.document_date >= date1.Date && x.document.document_date <= date2.Date)
                .OrderBy(c => c.document.cdate)
                .Select(movements => new ItemKardexModel
                {
                    MovementDate = movements.document.document_date.Value.ToString(),
                    MovementDocument = movements.document.document_no,
                    MovementReference = movements.document.reference_document,
                    MovementReferenceType = movements.document.reference_document_type,
                    DocumentType = movements.docItem.document_type,
                    MovementType = movements.docItem.movement_type.ToString(),
                    StorageLocation = movements.docItem.storage_location,
                    StockType = movements.docItem.stock_type == "0" ? "Disponible"
                        : movements.docItem.stock_type == "1" ? "Inspeccion"
                        : movements.docItem.stock_type == "2" ? "Bloqueado"
                        : movements.docItem.stock_type == "3" ? "WIP"
                        : movements.docItem.stock_type == "4" ? "Retorno"
                        : movements.docItem.stock_type == "5" ? "Transferencia Entrada"
                        : movements.docItem.stock_type == "6" ? "Transito Salida"
                        : null,
                    DebitCredit = movements.docItem.debit_credit == "DR" ? "Entrada"
                    : movements.docItem.debit_credit == "Cr" ? "Salida"
                    : null,
                    MovementQuantity = movements.docItem.quantity == null ? 0 : movements.docItem.quantity,
                    Currency = movements.docItem.currency.ToUpper() == "MX" ? "MXN"
                    : movements.docItem.currency,
                    AmountDocument = movements.docItem.amount_document == null ? 0 : movements.docItem.amount_document,
                    StorageLocationStock = movements.docItem.total_stock == null ? 0 : movements.docItem.total_stock,
                    StorageLocationAmount = movements.docItem.total_amount == null ? 0 : movements.docItem.total_amount,
                    TotalStock = movements.docItem.debit_credit.ToUpper() == "DR" ? movements.docItem.total_stock == null ? 0 : movements.docItem.total_stock + movements.docItem.quantity
                        : movements.docItem.debit_credit.ToUpper() == "Cr" ? movements.docItem.total_stock == null ? 0 : movements.docItem.total_stock - movements.docItem.quantity
                        : 0,
                    TotalAmount = movements.docItem.total_amount == null ? 0 : movements.docItem.total_amount
                }).ToList();

            if (movementsQuery.Count > 0)
                return movementsQuery;
            else
                return null;
        }

        public ItemSalesAverageModel GetSalesAverageByDate(string part_number)
        {
            DateTime date = DateTime.ParseExact("2018-10-18 00:00:00,000", "yyyy-MM-dd HH:mm:ss,fff", System.Globalization.CultureInfo.InvariantCulture);
            var date50 = date.AddDays(-50);
            var sales = _context.BI_DAILY_SALE_ITEM
                .Where(elements => elements.date_sale >= date50 && elements.date_sale <= date && elements.part_number == part_number)
                .GroupBy(x => x.part_number)
                .Select(group => new ItemSalesAverageModel
                {
                    Average = group.Sum(x => x.quantity) ?? 0
                }).FirstOrDefault();
            return sales;
        }

        public ItemEmptySpaceScanModel GetInfoItemByPartNumber(string part_number)
        {
            var items = _context.ITEM_STOCK
                .Join(_context.STORAGE_LOCATION, item_stock => item_stock.storage_location, storage_location => storage_location.storage_location_name, (item_stock, storage_location) => new { item_stock, storage_location })
                .Join(_context.ITEM, item_stock2 => item_stock2.item_stock.part_number, item => item.part_number, (item_stock2, item) => new { item_stock2, item })
                .Where(elements => elements.item_stock2.item_stock.unrestricted != 0 && elements.item_stock2.storage_location.storage_type == "VENTA" && elements.item_stock2.item_stock.part_number == part_number)
                .Select(elements => new ItemEmptySpaceScanModel
                {
                    description = elements.item.part_description,
                    part_number = elements.item.part_number,
                    planogram = elements.item_stock2.storage_location.storage_location_name,
                    stock_existing = elements.item_stock2.item_stock.unrestricted
                }).SingleOrDefault();
            return items;
        }

        public ItemEmptySpaceScanModel GetLastEntryByPartNumber(string part_number)
        {
            var item = _context.PURCHASE_ORDER_ITEM
               .Join(_context.PURCHASE_ORDER, x => x.purchase_no, xx => xx.purchase_no, (x, xx) => new { x, xx })
               .Where(w => w.x.part_number == part_number && w.xx.purchase_status == 9)
               .OrderByDescending(c => c.xx.udate)
               .Select(s => new ItemEmptySpaceScanModel
               {
                   last_entry = s.xx.udate,
                   last_entry_quantity = s.x.gr_quantity
               }).FirstOrDefault();

            return item;
        }

        public List<ItemInventoryDaysModel> GetInventoryDays(int SupplierId, int DepartmentId, int FamilyId, int Days)
        {
            var date = DateTime.Now.Date;
            var date50 = date.AddDays(-30);
            var begindate = new SqlParameter { ParameterName = "BEGINDATE", Value = date };
            var enddate = new SqlParameter { ParameterName = "ENDDATE", Value = date50 };
            var supplier = new SqlParameter { ParameterName = "SUPPLIERID", Value = SupplierId };
            var deparment = new SqlParameter { ParameterName = "DEPARMENTID", Value = DepartmentId };
            var family = new SqlParameter { ParameterName = "FAMILY", Value = FamilyId };
            var day = new SqlParameter { ParameterName = "DAYS", Value = Days };
            _context.Database.CommandTimeout = 10000;
            var model = _context.Database.SqlQuery<ItemInventoryDaysModel>("EXEC sp_Get_Inventory_Days @BEGINDATE, @ENDDATE, @SUPPLIERID, @DEPARMENTID, @FAMILY, @DAYS", begindate, enddate, supplier, deparment, family, day).ToList();

            return model;
        }

        public List<ItemSalesInventory> GetProductsWithoutSales(int SupplierId, int DepartmentId, int FamilyId, int Days)
        {
            var supplier = new SqlParameter { ParameterName = "SUPPLIERID", Value = SupplierId };
            var deparment = new SqlParameter { ParameterName = "DEPARMENTID", Value = DepartmentId };
            var family = new SqlParameter { ParameterName = "FAMILY", Value = FamilyId };
            var day = new SqlParameter { ParameterName = "DAYS", Value = Days };
            var model = _context.Database.SqlQuery<ItemSalesInventory>("EXEC sp_Get_ProductsWithoutSales @SUPPLIERID, @DEPARMENTID, @FAMILY, @DAYS", supplier, deparment, family, day).ToList<ItemSalesInventory>();

            return model;
        }

        public List<ItemHistoricInventory> GetHistoricInventory(DateTime Day)
        {
            var day = new SqlParameter { ParameterName = "Date", Value = Day };
            var model = _context.Database.SqlQuery<ItemHistoricInventory>("SELECT * FROM [fnc_Stock_History] (@Date)", day).ToList();

            return model;
        }

        public List<ItemSalesInventory> GetMissingProducts(int SupplierId, int DepartmentId, int FamilyId)
        {
            var supplier = new SqlParameter { ParameterName = "SUPPLIERID", Value = SupplierId };
            var deparment = new SqlParameter { ParameterName = "DEPARMENTID", Value = DepartmentId };
            var family = new SqlParameter { ParameterName = "FAMILY", Value = FamilyId };
            var model = _context.Database.SqlQuery<ItemSalesInventory>("EXEC sp_Get_MissingProducts @SUPPLIERID, @DEPARMENTID, @FAMILY", supplier, deparment, family).ToList<ItemSalesInventory>();

            return model;
        }

        public ItemModel GetItemPriceByPartNumber(string partNumber)
        {
            return _context.SALES_PRICE_CHANGE.Join(_context.ITEM, salePrice => salePrice.part_number, item => item.part_number, (salePrice, item) => new { salePrice, item })
                    .Where(x => x.salePrice.part_number == partNumber && x.salePrice.status == 1)
                    .Select(q => new ItemModel
                    {
                        PathNumber = q.item.part_number,
                        BaseCost = q.salePrice.sale_price
                    }).FirstOrDefault();
        }

        public ItemTaxInfo GetItemTaxInfo(string PartNumber)
        {
            var Item = (from it in _context.ITEM
                        join taxPurch in _context.MA_TAX on it.part_iva_purchase equals taxPurch.tax_code into LeftJoinTaxP
                        from taxPurch in LeftJoinTaxP.DefaultIfEmpty()
                        join taxSale in _context.MA_TAX on it.part_iva_sale equals taxSale.tax_code into LeftJoinTaxS
                        from taxSale in LeftJoinTaxS.DefaultIfEmpty()
                        join ieps in _context.MA_TAX on it.part_ieps equals ieps.tax_code into LeftJoinIEPS
                        from ieps in LeftJoinIEPS.DefaultIfEmpty()
                        where it.part_number == PartNumber
                        select new ItemTaxInfo
                        {
                            Number = it.part_number,
                            Description = it.part_description,
                            IEPS = it.part_ieps,
                            IEPSValue = ieps != null ? ieps.tax_value.Value : 0,
                            IVAPurchase = it.part_iva_purchase,
                            IVAPurchaseValue = taxPurch != null ? taxPurch.tax_value.Value : 0,
                            IVASale = it.part_iva_sale,
                            IVASaleValue = taxSale != null ? taxSale.tax_value.Value : 0
                        }).FirstOrDefault();

            return Item;
        }

        public CostInfo GetCostItem(string PartNumber, int Supplier)
        {
            return _context.Database.SqlQuery<CostInfo>(@"  SELECT base_cost as BaseCost , IEPS, IVA, it.unit_size as UnitSize ,dbo.fnc_Get_Tax( @partNumber,2) as Ieps
                    ,dbo.fnc_Get_Tax( @partNumber,1) as Iva
                    FROM dbo.fnc_Get_CostInfo( @partNumber,@Supplier)
                    inner join ITEM it on it.part_number = @partNumber"
                    , new SqlParameter("@partNumber", PartNumber)
                    , new SqlParameter("@Supplier", Supplier)).SingleOrDefault();
        }

        public List<ItemQuotation> GetItemQuotations(string Item)
        {
            var ItemList = (from it in _context.ITEM
                            join itv in _context.ITEM_VALUATION on it.part_number equals itv.part_number
                            where it.part_number.Contains(Item)
                                  || it.part_description.Contains(Item)
                            select new ItemQuotation
                            {
                                ItemNumber = it.part_number,
                                ItemDescription = it.part_description,
                                Stock = itv.total_stock ?? 0,
                                BasePrice = itv.price_sale ?? 0,
                                QuotePrice = itv.price_sale ?? 0,
                                Quantity = 1,
                                ItemTax = itv.price_sale_iva.HasValue && itv.price_sale.HasValue ? (itv.price_sale_iva.Value - itv.price_sale.Value) : 0,
                                ItemTaxPercentage = (it.part_iva_sale != null && it.part_iva_sale != TaxCodes.NOT_AVAILABLE && it.part_iva_sale != string.Empty) ? it.MA_TAX_IVA_SALE.tax_value.Value : 0,
                                IsPackage = false
                            }).ToList();

            var ItemList2 = (from it in _context.ITEM
                             join itv in _context.ITEM_VALUATION on it.part_number equals itv.part_number
                             join itPre in _context.ITEM_PRESENTATION on it.part_number equals itPre.part_number
                             join itPrePri in _context.ITEM_PRESENTATION_PRICE on itPre.presentation_id equals itPrePri.presentation_id
                             where it.part_number.Contains(Item) || it.part_description.Contains(Item)
                             select new
                             {
                                 ItemNumber = it.part_number,
                                 ItemDescription = it.part_description,
                                 Stock = itv.total_stock ?? 0,
                                 BasePrice = itPrePri.price_sale,
                                 QuotePrice = itPrePri.price_sale / itPre.quantity,
                                 Quantity = itPre.quantity,
                                 ItemTax = itv.price_sale_iva.Value - itPrePri.price_sale,
                                 ItemTaxPercentage = (it.part_iva_sale != null && it.part_iva_sale != TaxCodes.NOT_AVAILABLE && it.part_iva_sale != string.Empty) ? it.MA_TAX_IVA_SALE.tax_value.Value : 0,
                                 IsPackage = true,
                                 PreCode = itPre.presentation_code + " C"
                             }).AsEnumerable().Select(c => new ItemQuotation
                             {
                                 ItemNumber = c.ItemNumber,
                                 ItemDescription = string.Format("{0} <br><b>Compre {1:0} y el precio es ${2} c/u </b>", c.ItemDescription, c.Quantity, c.QuotePrice.ToString("0.00")),
                                 Stock = c.Stock,
                                 BasePrice = c.BasePrice,
                                 QuotePrice = c.QuotePrice,
                                 Quantity = c.Quantity,
                                 ItemTax = c.ItemTax,
                                 ItemTaxPercentage = c.ItemTaxPercentage,
                                 IsPackage = c.IsPackage,
                                 PresentationCode = c.PreCode,
                                 PresentationCodeText = string.Format("{0} <br><b>({1})</b>", c.ItemNumber, c.PreCode)
                             }).ToList();

            if (ItemList2 != null && ItemList2.Count > 0)
                ItemList.AddRange(ItemList2);

            return ItemList;
        }

        public List<ItemReportModel> GetAllItems()
        {
            var list = _context.ITEM
                .Join(_context.MA_TAX, y => y.part_iva_purchase, xx => xx.tax_code, (item, ma_tax) => new { item, ma_tax })
                .Join(_context.MA_TAX, y => y.item.part_iva_sale, xx => xx.tax_code, (item2, ma_tax2) => new { item2, ma_tax2 })
                .Join(_context.MA_TAX, y => y.item2.item.part_ieps, xx => xx.tax_code, (item3, ma_tax3) => new { item3, ma_tax3 })
                .Select(x => new ItemReportModel
                {
                    active_flag = x.item3.item2.item.active_flag,
                    buyer_division = x.item3.item2.item.buyer_division,
                    part_iva = x.item3.item2.ma_tax.name,
                    part_iva_sale = x.item3.ma_tax2.name,
                    part_ieps = x.ma_tax3.name,
                    part_description = x.item3.item2.item.part_description,
                    part_number = x.item3.item2.item.part_number,
                    part_number_sat = x.item3.item2.item.part_number_sat,
                    part_type = x.item3.item2.item.part_type,
                    unit = x.item3.item2.item.unit_size,
                    unit_sat = x.item3.item2.item.unit_sat,
                    weight_flag = x.item3.item2.item.weight_flag,
                    combo_flag = x.item3.item2.item.combo_flag,
                    extra_item_flag = x.item3.item2.item.extra_items_flag
                }).ToList();
            return list;
        }

        public List<ItemReportModel> NewGetAllItems(string part_numbers, int deparment, int family)
        {
            List<string> stringList = part_numbers.Split(',').ToList();

            if (part_numbers != "" && deparment == 0 && family == 0)
            {
                var list = _context.ITEM
                .Join(_context.MA_TAX, y => y.part_iva_purchase, xx => xx.tax_code, (item, ma_tax) => new { item, ma_tax })
                .Join(_context.MA_TAX, y => y.item.part_iva_sale, xx => xx.tax_code, (item2, ma_tax2) => new { item2, ma_tax2 })
                .Join(_context.MA_TAX, y => y.item2.item.part_ieps, xx => xx.tax_code, (item3, ma_tax3) => new { item3, ma_tax3 })
                .Join(_context.MA_CODE, y => y.item3.item2.item.buyer_division, xx => xx.vkey, (item5, ma_tax5) => new { item5, ma_tax5 })
                .Join(_context.MA_CLASS, y => y.item5.item3.item2.item.level_header, xx => xx.class_id, (item6, ma_tax6) => new { item6, ma_tax6 })
                .Join(_context.MA_CLASS, y => y.item6.item5.item3.item2.item.level1_code, xx => xx.class_id, (item7, ma_tax7) => new { item7, ma_tax7 })
                .GroupJoin(_context.ITEM_VALUATION, Item4 => Item4.item7.item6.item5.item3.item2.item.part_number, itv => itv.part_number, (x, y) => new { Item4 = x, itv = y })
                .SelectMany(x => x.itv.DefaultIfEmpty(), (x, y) => new { Item4 = x.Item4, itv = y })
                .Where(w => stringList.Any(x => w.Item4.item7.item6.item5.item3.item2.item.part_number.Contains(x)))
                .Select(x => new ItemReportModel
                {
                    division = x.Item4.item7.ma_tax6.class_name,
                    department = x.Item4.ma_tax7.class_name,
                    active_flag = x.Item4.item7.item6.item5.item3.item2.item.active_flag,
                    buyer_division = x.Item4.item7.item6.ma_tax5.description,
                    part_iva = x.Item4.item7.item6.item5.item3.item2.ma_tax.name,
                    part_iva_sale = x.Item4.item7.item6.item5.item3.ma_tax2.name,
                    part_ieps = x.Item4.item7.item6.item5.ma_tax3.name,
                    part_description = x.Item4.item7.item6.item5.item3.item2.item.part_description,
                    part_number = x.Item4.item7.item6.item5.item3.item2.item.part_number,
                    part_number_sat = x.Item4.item7.item6.item5.item3.item2.item.part_number_sat,
                    part_type = x.Item4.item7.item6.item5.item3.item2.item.part_type,
                    unit = x.Item4.item7.item6.item5.item3.item2.item.unit_size,
                    unit_sat = x.Item4.item7.item6.item5.item3.item2.item.unit_sat,
                    weight_flag = x.Item4.item7.item6.item5.item3.item2.item.weight_flag,
                    combo_flag = x.Item4.item7.item6.item5.item3.item2.item.combo_flag,
                    extra_item_flag = x.Item4.item7.item6.item5.item3.item2.item.extra_items_flag,
                    price = x.itv.price_sale ?? 0,
                    unrestricted = x.itv.unrestricted ?? 0,
                    expirations_days = x.Item4.item7.item6.item5.item3.item2.item.expiration_days ?? 0,
                    last_purchase_price = x.itv.last_purchase_price ?? 0,
                    iva_sale = x.Item4.item7.item6.item5.item3.ma_tax2.tax_value == null ? 0 : (x.itv.price_sale ?? 0) * (x.Item4.item7.item6.item5.item3.ma_tax2.tax_value.Value / 100),
                    ieps = x.Item4.item7.item6.item5.ma_tax3.tax_value == null ? 0 : (x.itv.price_sale ?? 0) * (x.Item4.item7.item6.item5.ma_tax3.tax_value.Value / 100),
                    iva = x.Item4.item7.item6.item5.item3.item2.ma_tax.tax_value == null ? 0 : ((x.itv.price_sale ?? 0) + ((x.itv.price_sale ?? 0) * (x.Item4.item7.item6.item5.ma_tax3.tax_value.Value / 100))) * (x.Item4.item7.item6.item5.item3.item2.ma_tax.tax_value.Value / 100),
                    map_price = x.itv.map_price
                }).ToList();
                return list;
            }
            else if (part_numbers != "" && deparment != 0 && family == 0)
            {
                var list = _context.ITEM
                 .Join(_context.MA_TAX, y => y.part_iva_purchase, xx => xx.tax_code, (item, ma_tax) => new { item, ma_tax })
                 .Join(_context.MA_TAX, y => y.item.part_iva_sale, xx => xx.tax_code, (item2, ma_tax2) => new { item2, ma_tax2 })
                 .Join(_context.MA_TAX, y => y.item2.item.part_ieps, xx => xx.tax_code, (item3, ma_tax3) => new { item3, ma_tax3 })
                 .Join(_context.MA_CODE, y => y.item3.item2.item.buyer_division, xx => xx.vkey, (item5, ma_tax5) => new { item5, ma_tax5 })
                 .Join(_context.MA_CLASS, y => y.item5.item3.item2.item.level_header, xx => xx.class_id, (item6, ma_tax6) => new { item6, ma_tax6 })
                 .Join(_context.MA_CLASS, y => y.item6.item5.item3.item2.item.level1_code, xx => xx.class_id, (item7, ma_tax7) => new { item7, ma_tax7 })
                 .GroupJoin(_context.ITEM_VALUATION, Item4 => Item4.item7.item6.item5.item3.item2.item.part_number, itv => itv.part_number, (x, y) => new { Item4 = x, itv = y })
                 .SelectMany(x => x.itv.DefaultIfEmpty(), (x, y) => new { Item4 = x.Item4, itv = y })
                 .Where(w => stringList.Any(x => w.Item4.item7.item6.item5.item3.item2.item.part_number.Contains(x) && w.Item4.item7.item6.item5.item3.item2.item.level_header == deparment))
                 .Select(x => new ItemReportModel
                 {
                     division = x.Item4.item7.ma_tax6.class_name,
                     department = x.Item4.ma_tax7.class_name,
                     active_flag = x.Item4.item7.item6.item5.item3.item2.item.active_flag,
                     buyer_division = x.Item4.item7.item6.ma_tax5.description,
                     part_iva = x.Item4.item7.item6.item5.item3.item2.ma_tax.name,
                     part_iva_sale = x.Item4.item7.item6.item5.item3.ma_tax2.name,
                     part_ieps = x.Item4.item7.item6.item5.ma_tax3.name,
                     part_description = x.Item4.item7.item6.item5.item3.item2.item.part_description,
                     part_number = x.Item4.item7.item6.item5.item3.item2.item.part_number,
                     part_number_sat = x.Item4.item7.item6.item5.item3.item2.item.part_number_sat,
                     part_type = x.Item4.item7.item6.item5.item3.item2.item.part_type,
                     unit = x.Item4.item7.item6.item5.item3.item2.item.unit_size,
                     unit_sat = x.Item4.item7.item6.item5.item3.item2.item.unit_sat,
                     weight_flag = x.Item4.item7.item6.item5.item3.item2.item.weight_flag,
                     combo_flag = x.Item4.item7.item6.item5.item3.item2.item.combo_flag,
                     extra_item_flag = x.Item4.item7.item6.item5.item3.item2.item.extra_items_flag,
                     price = x.itv.price_sale ?? 0,
                     unrestricted = x.itv.unrestricted ?? 0,
                     expirations_days = x.Item4.item7.item6.item5.item3.item2.item.expiration_days ?? 0,
                     last_purchase_price = x.itv.last_purchase_price ?? 0,
                     iva_sale = x.Item4.item7.item6.item5.item3.ma_tax2.tax_value == null ? 0 : (x.itv.price_sale ?? 0) * (x.Item4.item7.item6.item5.item3.ma_tax2.tax_value.Value / 100),
                     ieps = x.Item4.item7.item6.item5.ma_tax3.tax_value == null ? 0 : (x.itv.price_sale ?? 0) * (x.Item4.item7.item6.item5.ma_tax3.tax_value.Value / 100),
                     iva = x.Item4.item7.item6.item5.item3.item2.ma_tax.tax_value == null ? 0 : ((x.itv.price_sale ?? 0) + ((x.itv.price_sale ?? 0) * (x.Item4.item7.item6.item5.ma_tax3.tax_value.Value / 100))) * (x.Item4.item7.item6.item5.item3.item2.ma_tax.tax_value.Value / 100),
                     map_price = x.itv.map_price
                 }).ToList();
                return list;
            }
            else if (part_numbers != "" && deparment != 0 && family != 0)
            {
                var list = _context.ITEM
                .Join(_context.MA_TAX, y => y.part_iva_purchase, xx => xx.tax_code, (item, ma_tax) => new { item, ma_tax })
                .Join(_context.MA_TAX, y => y.item.part_iva_sale, xx => xx.tax_code, (item2, ma_tax2) => new { item2, ma_tax2 })
                .Join(_context.MA_TAX, y => y.item2.item.part_ieps, xx => xx.tax_code, (item3, ma_tax3) => new { item3, ma_tax3 })
                .Join(_context.MA_CODE, y => y.item3.item2.item.buyer_division, xx => xx.vkey, (item5, ma_tax5) => new { item5, ma_tax5 })
                .Join(_context.MA_CLASS, y => y.item5.item3.item2.item.level_header, xx => xx.class_id, (item6, ma_tax6) => new { item6, ma_tax6 })
                .Join(_context.MA_CLASS, y => y.item6.item5.item3.item2.item.level1_code, xx => xx.class_id, (item7, ma_tax7) => new { item7, ma_tax7 })
                .GroupJoin(_context.ITEM_VALUATION, Item4 => Item4.item7.item6.item5.item3.item2.item.part_number, itv => itv.part_number, (x, y) => new { Item4 = x, itv = y })
                .SelectMany(x => x.itv.DefaultIfEmpty(), (x, y) => new { Item4 = x.Item4, itv = y })
                .Where(w => stringList.Any(x => w.Item4.item7.item6.item5.item3.item2.item.part_number.Contains(x) && w.Item4.item7.item6.item5.item3.item2.item.level_header == deparment && w.Item4.item7.item6.item5.item3.item2.item.level1_code == family))
                .Select(x => new ItemReportModel
                {
                    division = x.Item4.item7.ma_tax6.class_name,
                    department = x.Item4.ma_tax7.class_name,
                    active_flag = x.Item4.item7.item6.item5.item3.item2.item.active_flag,
                    buyer_division = x.Item4.item7.item6.ma_tax5.description,
                    part_iva = x.Item4.item7.item6.item5.item3.item2.ma_tax.name,
                    part_iva_sale = x.Item4.item7.item6.item5.item3.ma_tax2.name,
                    part_ieps = x.Item4.item7.item6.item5.ma_tax3.name,
                    part_description = x.Item4.item7.item6.item5.item3.item2.item.part_description,
                    part_number = x.Item4.item7.item6.item5.item3.item2.item.part_number,
                    part_number_sat = x.Item4.item7.item6.item5.item3.item2.item.part_number_sat,
                    part_type = x.Item4.item7.item6.item5.item3.item2.item.part_type,
                    unit = x.Item4.item7.item6.item5.item3.item2.item.unit_size,
                    unit_sat = x.Item4.item7.item6.item5.item3.item2.item.unit_sat,
                    weight_flag = x.Item4.item7.item6.item5.item3.item2.item.weight_flag,
                    combo_flag = x.Item4.item7.item6.item5.item3.item2.item.combo_flag,
                    extra_item_flag = x.Item4.item7.item6.item5.item3.item2.item.extra_items_flag,
                    price = x.itv.price_sale ?? 0,
                    unrestricted = x.itv.unrestricted ?? 0,
                    expirations_days = x.Item4.item7.item6.item5.item3.item2.item.expiration_days ?? 0,
                    last_purchase_price = x.itv.last_purchase_price ?? 0,
                    iva_sale = x.Item4.item7.item6.item5.item3.ma_tax2.tax_value == null ? 0 : (x.itv.price_sale ?? 0) * (x.Item4.item7.item6.item5.item3.ma_tax2.tax_value.Value / 100),
                    ieps = x.Item4.item7.item6.item5.ma_tax3.tax_value == null ? 0 : (x.itv.price_sale ?? 0) * (x.Item4.item7.item6.item5.ma_tax3.tax_value.Value / 100),
                    iva = x.Item4.item7.item6.item5.item3.item2.ma_tax.tax_value == null ? 0 : ((x.itv.price_sale ?? 0) + ((x.itv.price_sale ?? 0) * (x.Item4.item7.item6.item5.ma_tax3.tax_value.Value / 100))) * (x.Item4.item7.item6.item5.item3.item2.ma_tax.tax_value.Value / 100),
                    map_price = x.itv.map_price
                }).ToList();
                return list;
            }
            else if (part_numbers == "" && deparment != 0 && family != 0)
            {
                var list = _context.ITEM
                .Join(_context.MA_TAX, y => y.part_iva_purchase, xx => xx.tax_code, (item, ma_tax) => new { item, ma_tax })
                .Join(_context.MA_TAX, y => y.item.part_iva_sale, xx => xx.tax_code, (item2, ma_tax2) => new { item2, ma_tax2 })
                .Join(_context.MA_TAX, y => y.item2.item.part_ieps, xx => xx.tax_code, (item3, ma_tax3) => new { item3, ma_tax3 })
                .Join(_context.MA_CODE, y => y.item3.item2.item.buyer_division, xx => xx.vkey, (item5, ma_tax5) => new { item5, ma_tax5 })
                .Join(_context.MA_CLASS, y => y.item5.item3.item2.item.level_header, xx => xx.class_id, (item6, ma_tax6) => new { item6, ma_tax6 })
                .Join(_context.MA_CLASS, y => y.item6.item5.item3.item2.item.level1_code, xx => xx.class_id, (item7, ma_tax7) => new { item7, ma_tax7 })
                .GroupJoin(_context.ITEM_VALUATION, Item4 => Item4.item7.item6.item5.item3.item2.item.part_number, itv => itv.part_number, (x, y) => new { Item4 = x, itv = y })
                .SelectMany(x => x.itv.DefaultIfEmpty(), (x, y) => new { Item4 = x.Item4, itv = y })
                .Where(w => stringList.Any(x => w.Item4.item7.item6.item5.item3.item2.item.level_header == deparment && w.Item4.item7.item6.item5.item3.item2.item.level1_code == family))
                .Select(x => new ItemReportModel
                {
                    division = x.Item4.item7.ma_tax6.class_name,
                    department = x.Item4.ma_tax7.class_name,
                    active_flag = x.Item4.item7.item6.item5.item3.item2.item.active_flag,
                    buyer_division = x.Item4.item7.item6.ma_tax5.description,
                    part_iva = x.Item4.item7.item6.item5.item3.item2.ma_tax.name,
                    part_iva_sale = x.Item4.item7.item6.item5.item3.ma_tax2.name,
                    part_ieps = x.Item4.item7.item6.item5.ma_tax3.name,
                    part_description = x.Item4.item7.item6.item5.item3.item2.item.part_description,
                    part_number = x.Item4.item7.item6.item5.item3.item2.item.part_number,
                    part_number_sat = x.Item4.item7.item6.item5.item3.item2.item.part_number_sat,
                    part_type = x.Item4.item7.item6.item5.item3.item2.item.part_type,
                    unit = x.Item4.item7.item6.item5.item3.item2.item.unit_size,
                    unit_sat = x.Item4.item7.item6.item5.item3.item2.item.unit_sat,
                    weight_flag = x.Item4.item7.item6.item5.item3.item2.item.weight_flag,
                    combo_flag = x.Item4.item7.item6.item5.item3.item2.item.combo_flag,
                    extra_item_flag = x.Item4.item7.item6.item5.item3.item2.item.extra_items_flag,
                    price = x.itv.price_sale ?? 0,
                    unrestricted = x.itv.unrestricted ?? 0,
                    expirations_days = x.Item4.item7.item6.item5.item3.item2.item.expiration_days ?? 0,
                    last_purchase_price = x.itv.last_purchase_price ?? 0,
                    iva_sale = x.Item4.item7.item6.item5.item3.ma_tax2.tax_value == null ? 0 : (x.itv.price_sale ?? 0) * (x.Item4.item7.item6.item5.item3.ma_tax2.tax_value.Value / 100),
                    ieps = x.Item4.item7.item6.item5.ma_tax3.tax_value == null ? 0 : (x.itv.price_sale ?? 0) * (x.Item4.item7.item6.item5.ma_tax3.tax_value.Value / 100),
                    iva = x.Item4.item7.item6.item5.item3.item2.ma_tax.tax_value == null ? 0 : ((x.itv.price_sale ?? 0) + ((x.itv.price_sale ?? 0) * (x.Item4.item7.item6.item5.ma_tax3.tax_value.Value / 100))) * (x.Item4.item7.item6.item5.item3.item2.ma_tax.tax_value.Value / 100),
                    map_price = x.itv.map_price
                }).ToList();
                return list;
            }
            else if (part_numbers == "" && deparment != 0 && family == 0)
            {
                var list = _context.ITEM
                .Join(_context.MA_TAX, y => y.part_iva_purchase, xx => xx.tax_code, (item, ma_tax) => new { item, ma_tax })
                .Join(_context.MA_TAX, y => y.item.part_iva_sale, xx => xx.tax_code, (item2, ma_tax2) => new { item2, ma_tax2 })
                .Join(_context.MA_TAX, y => y.item2.item.part_ieps, xx => xx.tax_code, (item3, ma_tax3) => new { item3, ma_tax3 })
                .Join(_context.MA_CODE, y => y.item3.item2.item.buyer_division, xx => xx.vkey, (item5, ma_tax5) => new { item5, ma_tax5 })
                .Join(_context.MA_CLASS, y => y.item5.item3.item2.item.level_header, xx => xx.class_id, (item6, ma_tax6) => new { item6, ma_tax6 })
                .Join(_context.MA_CLASS, y => y.item6.item5.item3.item2.item.level1_code, xx => xx.class_id, (item7, ma_tax7) => new { item7, ma_tax7 })
                .GroupJoin(_context.ITEM_VALUATION, Item4 => Item4.item7.item6.item5.item3.item2.item.part_number, itv => itv.part_number, (x, y) => new { Item4 = x, itv = y })
                .SelectMany(x => x.itv.DefaultIfEmpty(), (x, y) => new { Item4 = x.Item4, itv = y })
                .Where(w => stringList.Any(x => w.Item4.item7.item6.item5.item3.item2.item.level_header == deparment))
                .Select(x => new ItemReportModel
                {
                    division = x.Item4.item7.ma_tax6.class_name,
                    department = x.Item4.ma_tax7.class_name,
                    active_flag = x.Item4.item7.item6.item5.item3.item2.item.active_flag,
                    buyer_division = x.Item4.item7.item6.ma_tax5.description,
                    part_iva = x.Item4.item7.item6.item5.item3.item2.ma_tax.name,
                    part_iva_sale = x.Item4.item7.item6.item5.item3.ma_tax2.name,
                    part_ieps = x.Item4.item7.item6.item5.ma_tax3.name,
                    part_description = x.Item4.item7.item6.item5.item3.item2.item.part_description,
                    part_number = x.Item4.item7.item6.item5.item3.item2.item.part_number,
                    part_number_sat = x.Item4.item7.item6.item5.item3.item2.item.part_number_sat,
                    part_type = x.Item4.item7.item6.item5.item3.item2.item.part_type,
                    unit = x.Item4.item7.item6.item5.item3.item2.item.unit_size,
                    unit_sat = x.Item4.item7.item6.item5.item3.item2.item.unit_sat,
                    weight_flag = x.Item4.item7.item6.item5.item3.item2.item.weight_flag,
                    combo_flag = x.Item4.item7.item6.item5.item3.item2.item.combo_flag,
                    extra_item_flag = x.Item4.item7.item6.item5.item3.item2.item.extra_items_flag,
                    price = x.itv.price_sale ?? 0,
                    unrestricted = x.itv.unrestricted ?? 0,
                    expirations_days = x.Item4.item7.item6.item5.item3.item2.item.expiration_days ?? 0,
                    last_purchase_price = x.itv.last_purchase_price ?? 0,
                    iva_sale = x.Item4.item7.item6.item5.item3.ma_tax2.tax_value == null ? 0 : (x.itv.price_sale ?? 0) * (x.Item4.item7.item6.item5.item3.ma_tax2.tax_value.Value / 100),
                    ieps = x.Item4.item7.item6.item5.ma_tax3.tax_value == null ? 0 : (x.itv.price_sale ?? 0) * (x.Item4.item7.item6.item5.ma_tax3.tax_value.Value / 100),
                    iva = x.Item4.item7.item6.item5.item3.item2.ma_tax.tax_value == null ? 0 : ((x.itv.price_sale ?? 0) + ((x.itv.price_sale ?? 0) * (x.Item4.item7.item6.item5.ma_tax3.tax_value.Value / 100))) * (x.Item4.item7.item6.item5.item3.item2.ma_tax.tax_value.Value / 100),
                    map_price = x.itv.map_price
                }).ToList();
                return list;
            }
            return null;
        }

        public List<ItemQuickCheck> GetItemQuickCheckList(string Item)
        {
            var modelItemQuickCheck = _context.Database.SqlQuery<ItemQuickCheck>(@"SELECT DISTINCT ITEM.part_number 'PartNumber', ITEM.part_description 'Description', ISNULL(ITEM_VALUATION.unrestricted, 0) 'Stock', 
                ISNULL(ITEM_VALUATION.Total_Stock, 0) 'Total_Stock',
                ISNULL(ITEM_VALUATION.wip, 0) 'Wip',
               ISNULL(ITEM_VALUATION.blocked, 0) 'Blocked',
                ISNULL(ITEM_VALUATION.price_sale, 0) 'BasePrice', ISNULL(ITEM_VALUATION.price_sale_iva, 0) 'TaxPrice' 
                FROM ITEM 
                INNER JOIN ITEM_VALUATION ON ITEM.part_number = ITEM_VALUATION.part_number 
                INNER JOIN BARCODES ON ITEM.part_number = BARCODES.part_number 
                WHERE ITEM.part_description LIKE '%'+@item+'%' OR ITEM.part_number LIKE '%'+@item+'%' OR BARCODES.part_barcode LIKE '%'+@item+'%' ", new SqlParameter("@item", Item)).ToList();

            foreach (var item in modelItemQuickCheck)
            {
                var modelQuickPresentation = _context.Database.SqlQuery<QuickPresentation>(@"SELECT ITEM.part_number 'PartNumber', ITEM_PRESENTATION.packing_type 'PackingType', ITEM_PRESENTATION.quantity 'Quantity', 
                ITEM_PRESENTATION.factor_unit_size 'FactorUnitSize', ITEM_PRESENTATION_PRICE.price_sale_iva 'TaxPrice' FROM ITEM 
                INNER JOIN BARCODES ON ITEM.part_number = BARCODES.part_number 
                INNER JOIN ITEM_PRESENTATION ON ITEM_PRESENTATION.presentation_id = BARCODES.presentation_id 
                INNER JOIN ITEM_PRESENTATION_PRICE ON BARCODES.presentation_id = ITEM_PRESENTATION_PRICE.presentation_id 
                WHERE ITEM.part_number = @partnumber ", new SqlParameter("@partnumber", item.PartNumber)).ToList();

                item.Presentations = modelQuickPresentation;
            }

            return modelItemQuickCheck;

            /*var ItemList = new List<ItemQuickCheck>();

            ItemList = (from i in _context.ITEM
                        join iv in _context.ITEM_VALUATION on i.part_number equals iv.part_number
                        join ba in _context.BARCODES on i.part_number equals ba.part_number
                        where i.part_description.Contains(Item) || i.part_number.Contains(Item) || ba.part_barcode.Contains(Item)
                        select new ItemQuickCheck
                        {
                            PartNumber = i.part_number,
                            Description = i.part_description,
                            Stock = iv.unrestricted ?? 0,
                            BasePrice = iv.price_sale ?? 0,
                            TaxPrice = iv.price_sale_iva ?? 0,
                            Presentations = _context.ITEM_PRESENTATION_PRICE
                                .Join(_context.ITEM_PRESENTATION, ipp => ipp.presentation_id, ip => ip.presentation_id, (ipp, ip) => new { ipp, ip })
                                .Where(e => e.ipp.presentation_id == ba.presentation_id)
                                .Select(e => new QuickPresentation
                                {
                                    PackingType = e.ip.packing_type,
                                    Quantity = e.ip.quantity,
                                    FactorUnitSize = e.ip.factor_unit_size,
                                    TaxPrice = e.ipp.price_sale_iva
                                }).ToList()

                        }).ToList();

            return ItemList;*/
        }

        public List<SearchCodeRes> SearchCode(string search, bool exact, bool barcode_b, bool presentation, bool planogram)
        {
            List<SearchCodeRes> list = new List<SearchCodeRes>();
            if (search.Length == 0) return list;
            if (barcode_b)
            {
                var r = _context.ITEM
                    .Join(_context.BARCODES, item => item.part_number, barcodes => barcodes.part_number, (item, barcode) => new { item, barcode })
                    .Join(_context.ITEM_VALUATION, e => e.item.part_number, valuation => valuation.part_number, (e, valuation) => new { e.item, e.barcode, valuation })
                    .Where(e => (exact ?
                        e.barcode.part_barcode == search || e.barcode.part_number == search :
                        e.barcode.part_barcode.StartsWith(search) || e.item.part_number.StartsWith(search)
                        ) && e.item.active_flag == true && e.barcode.presentation_id == null).Select(e => new SearchCodeRes
                        {
                            ItemBarcode = new ItemBarcodeModelMin
                            {
                                Barcode = e.barcode.part_barcode,
                                PartNumber = e.item.part_number,
                                Description = e.item.part_description,
                                SalePrice = e.valuation.price_sale_iva ?? 0,
                            },
                            Type = SearchCodeType.Barcode
                        });
                var r2 = _context.ITEM
                    .Join(_context.BARCODES, item => item.part_number, barcodes => barcodes.part_number, (item, barcode) => new { item, barcode })
                    .Join(_context.ITEM_VALUATION, e => e.item.part_number, valuation => valuation.part_number, (e, valuation) => new { e.item, e.barcode, valuation })
                    .Join(_context.ITEM_PRESENTATION_PRICE, e => e.barcode.presentation_id, valuation => valuation.presentation_id, (e, valuation) => new { e.item, e.barcode, valuation })
                    .Join(_context.ITEM_PRESENTATION, e => e.barcode.presentation_id, pre => pre.presentation_id, (e, pre) => new { e.item, e.barcode, e.valuation, pre })
                    .Where(e => (exact ?
                            e.barcode.part_barcode == search || e.barcode.part_number == search :
                            e.barcode.part_barcode.StartsWith(search) || e.item.part_number.StartsWith(search)
                        ) && e.item.active_flag == true && e.barcode.presentation_id != null).Select(e => new SearchCodeRes
                        {
                            ItemBarcode = new ItemBarcodeModelMin
                            {
                                Barcode = e.barcode.part_barcode,
                                PartNumber = e.item.part_number,
                                Description = e.pre.presentation_name,
                                SalePrice = e.valuation.price_sale_iva,
                            },
                            Type = SearchCodeType.Barcode
                        });
                list.AddRange(r);
                list.AddRange(r2);

            }

            if (presentation)
            {
                var r = _context.ITEM_PRESENTATION
                    .Join(_context.BARCODES, pre => pre.presentation_id, barcodes => barcodes.presentation_id, (pre, barcode) => new { pre, barcode })
                    .Join(_context.ITEM_PRESENTATION_PRICE, e => e.pre.presentation_id, valuation => valuation.presentation_id, (e, valuation) => new { e.pre, e.barcode, valuation })
                    .Join(_context.ITEM, e => e.pre.part_number, item => item.part_number, (e, item) => new { e.pre, e.barcode, e.valuation, item })
                    .Join(_context.ITEM_VALUATION, e => e.item.part_number, ivaluation => ivaluation.part_number, (e, ivaluation) => new { e.pre, e.barcode, e.valuation, e.item, ivaluation })
                    .Where(e => (exact ?
                        e.barcode.part_barcode == search || e.item.part_number == search :
                        e.barcode.part_barcode.StartsWith(search) || e.item.part_number.StartsWith(search)
                        ) && e.item.active_flag == true && e.pre.active_flag && e.barcode.presentation_id != null)
                    .Select(e => new SearchCodeRes
                    {
                        ItemBarcode = new ItemBarcodeModelMin
                        {
                            Barcode = e.barcode.part_barcode,
                            PartNumber = e.item.part_number,
                            Description = e.item.part_description,
                            SalePrice = e.ivaluation.price_sale_iva ?? 0,
                        },
                        ItemPresentation = new ItemPresentationModelMin
                        {
                            Count = e.pre.quantity,
                            UnitSize = e.pre.factor_unit_size,
                            SalePrice = e.valuation.price_sale_iva,
                            Barcode = e.barcode.part_barcode,
                        },
                        Type = SearchCodeType.Presentation
                    });
                list.AddRange(r);
            }
            if (planogram)
            {
                var r = _context.PLANOGRAM_LOCATION
                    .Join(_context.ITEM, e => e.part_number, plan => plan.part_number, (plan, item) => new { plan, item })
                    .Join(_context.ITEM_VALUATION, e => e.item.part_number, ivaluation => ivaluation.part_number, (e, ivaluation) => new { e.item, e.plan, ivaluation })
                    .Join(_context.BARCODES, e => e.plan.part_number, barcodes => barcodes.part_number, (e, barcode) => new { e.item, e.plan, e.ivaluation, barcode })
                    .Where(e => (exact ?
                        (e.plan.planogram_location1 == search || e.item.part_number == search) :
                        (e.plan.planogram_location1.StartsWith(search) || e.item.part_number.StartsWith(search)))
                        && e.item.active_flag == true)
                    .Select(e => new SearchCodeRes
                    {
                        ItemBarcode = new ItemBarcodeModelMin
                        {
                            Barcode = e.barcode.part_barcode,
                            PartNumber = e.barcode.part_number,
                            Description = e.item.part_description,
                            SalePrice = e.ivaluation.price_sale_iva ?? 0,
                        },
                        Planogram = new PlanogramLocationModelMin
                        {
                            planogram_location = e.plan.planogram_location1,
                            part_number = e.plan.part_number,
                            part_description = e.item.part_description,
                            bin1 = e.plan.bin1,
                            bin2 = e.plan.bin2,
                            bin3 = e.plan.bin3,
                            quantity = e.plan.quantity ?? 0,
                            planogram_description = e.plan.planogram_description,
                        },
                        Type = SearchCodeType.Planogram
                    });
                list.AddRange(r);
            }
            return list;
        }

        public List<ItemPresentationModelMin> GetItemPresentationWithBarcode(string part_number)
        {
            var item = _context.BARCODES.Join(_context.ITEM_PRESENTATION, B => B.presentation_id, I => I.presentation_id, (B, I) => new { B, I })
                .Join(_context.ITEM_PRESENTATION_PRICE, II => II.B.presentation_id, IP => IP.presentation_id, (II, IP) => new { II, IP })
                .Where(w => w.II.B.part_number == part_number)
                .Select(s => new ItemPresentationModelMin
                {
                    PartNumber = s.II.B.part_number,
                    Barcode = s.II.I.presentation_code,
                    Description = s.II.I.presentation_name,
                    Count = s.II.I.quantity,
                    UnitSize = s.II.I.packing_type,
                    SalePrice = s.IP.price_sale_iva,
                    SalePriceUnit = s.IP.price_sale_iva / s.II.I.quantity,
                    Active_Flag = s.II.I.active_flag == true ? "Presentación Activa" : "Desactivada"
                }).ToList();
            var barcodes = _context.BARCODES.Join(_context.ITEM_VALUATION, B => B.part_number, IV => IV.part_number, (B, IV) => new { B, IV })
                .Join(_context.ITEM, IV2 => IV2.IV.part_number, I => I.part_number, (IV2, I) => new { IV2, I })
                .Where(w => w.IV2.B.part_number == part_number && w.IV2.B.presentation_id == null)
                .Select(s => new ItemPresentationModelMin
                {
                    PartNumber = s.IV2.B.part_number,
                    Barcode = s.IV2.B.part_barcode,
                    Description = "Código ligado",
                    Count = 1,
                    UnitSize = "-",
                    SalePrice = s.IV2.IV.price_sale_iva ?? 0,
                    SalePriceUnit = s.IV2.IV.price_sale_iva ?? 0,
                    Active_Flag = "Activo"
                }).ToList();
            item.AddRange(barcodes);
            return item;
        }

        public string GetOnlyBarcodes(string part_number)
        {
            var barcodes = _context.BARCODES.Where(w => w.part_number == part_number && w.presentation_id == null)
                .Select(s => s).ToList();

            if (barcodes != null)
            {
                var barcodeComma = "";
                if (barcodes.Count() == 1)
                {
                    barcodeComma = barcodes[0].part_barcode;
                }
                else
                {
                    for (int i = 0; i < barcodes.Count(); i++)
                    {
                        if (barcodes.Count() - 1 == i)
                        {
                            barcodeComma += barcodes[i].part_barcode;
                            break;
                        }
                        barcodeComma += $"{barcodes[i].part_barcode}, ";
                    }
                }

                return barcodeComma;
            }
            return "";
        }

        public List<ItemKardexHorModel> GetItemsKardexHor(DateTime dateInitial, DateTime dateFinish, string department, string part_number)
        {
            _context.Database.CommandTimeout = 1000000;
            var r = _context.Database.SqlQuery<ItemKardexHorModel>(@"DECLARE	@return_value int
            EXEC	@return_value = [dbo].[sp_Get_Kardex_Horizontal]
		    @dateInitial = '" + dateInitial.Date + @"',
		    @dateFinish = '" + dateFinish.Date + @"',
            @department = '" + department + @"',
            @part_number = '" + part_number + @"'").ToList();
            return r;
        }

        

        public List<ItemModel> SearchItemBarcodeNoFlags(string part_number)
        {
            return _context.ITEM
                .Join(_context.BARCODES, document => document.part_number, docItem => docItem.part_number, (document, docItem) => new { document, docItem })
                .Where(x => (x.document.part_number.Contains(part_number) || x.document.part_description.Contains(part_number) || x.docItem.part_barcode.Contains(part_number))
                )
                .Select(x => new ItemModel
                {
                    Description = x.document.part_description,
                    PathNumber = x.document.part_number
                }).Distinct().ToList();
        }

    }

    

    public class SearchCodeRes
    {
        public ItemBarcodeModelMin ItemBarcode { get; set; }
        public ItemPresentationModelMin ItemPresentation { get; set; }
        public PlanogramLocationModelMin Planogram { get; set; }
        public SearchCodeType Type { get; set; }
    }

    public class ItemBarcodeModelMin
    {
        public string PartNumber { get; set; }
        public string Description { get; set; }
        public decimal SalePrice { get; set; }
        public string Barcode { get; set; }
    }

    public class ItemPresentationModelMin
    {
        public string PartNumber { get; set; }
        public string Description { get; set; }
        public decimal Count { get; set; }
        public string UnitSize { get; set; }
        public decimal SalePriceUnit { get; set; }
        public decimal SalePrice { get; set; }
        public string Barcode { get; set; }
        public string Active_Flag { get; set; }
    }

    public class PlanogramLocationModelMin
    {
        public string planogram_location { get; set; }
        public string part_number { get; set; }
        public string part_description { get; set; }
        public string bin1 { get; set; }
        public string bin2 { get; set; }
        public string bin3 { get; set; }
        public decimal quantity { get; set; }
        public string planogram_description { get; set; }
    }

    public enum SearchCodeType
    {
        Barcode = 1,
        Presentation = 2,
        Planogram = 3
    }
}