﻿using App.DAL.DamagedsGoods;
using App.DAL.Site;
using App.Entities;
using App.Entities.ViewModels.DamagedsGoods;
using App.Entities.ViewModels.Item;
using App.Entities.ViewModels.Inventory;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Core.Objects;
using System.Data.SqlClient;
using System.Linq;

namespace App.DAL.Item
{
    public class ItemStockRepository
    {
        private readonly DFL_SAIEntities _context;
        private readonly SiteConfigRepository _SiteConfigRepo;
        private readonly DamagedsGoodsItemRepository _DamagedsGoodsItemRepository;
        private readonly DamagedsGoodsRestrictedRepository _damagedsGoodsRestrictedRepository;

        public ItemStockRepository()
        {
            _context = new DFL_SAIEntities();
            _SiteConfigRepo = new SiteConfigRepository();
            _DamagedsGoodsItemRepository = new DamagedsGoodsItemRepository();
            _damagedsGoodsRestrictedRepository = new DamagedsGoodsRestrictedRepository();
        }

        private string sc;
        private string thisSiteCode
        {
            get
            {
                if (sc != null) return sc;
                sc = _context.SITE_CONFIG.Select(e => e.site_code).First();
                return sc;
            }
        }
        public ItemStockModel ItemIsBlocked(string PartNumber)
        {
            return _context.ITEM_STOCK.Where(x => x.part_number == PartNumber).Select(x => new ItemStockModel
            {
                Block = x.blocked != null ? x.blocked.Value : 0,
                Storage = x.storage_location
            }).FirstOrDefault();
        }

        public List<DamagedGoodsBanatiModel> GetItemsBlocked()
        {
            var items = _context.ITEM.Join(_context.ITEM_STOCK, it => it.part_number, mt => mt.part_number, (it, mt) => new { it, mt })
                .Where(x => x.mt.blocked > 0)
                .Select(iv => new DamagedGoodsBanatiModel
                {
                    PartNumber = iv.it.part_number,
                    Description = iv.it.part_description,
                    Stock = iv.mt.blocked
                }).ToList();

            var available = _context.vw_available_blocked_stock;
            foreach (var stock in available)
            {
                var item = items.Where(x => x.PartNumber == stock.part_number).FirstOrDefault();
                if (item != null)
                {
                    if (_damagedsGoodsRestrictedRepository.GetKnowDamagedsGoodsRestricted("REMISION", item.PartNumber))
                    {
                        if (stock.Disponible > 0)
                            items.Where(x => x.PartNumber == stock.part_number).FirstOrDefault().Stock = stock.Disponible;
                        else
                            items.Remove(item);
                    }
                    else
                    {
                        items.Remove(item);
                    }
                }
            }

            return items;
        }

        public DamagedGoodsViewModel GetBanatiItemsBlocked()
        {
            DateTime date1 = DateTime.Now.AddDays(-7);
            DateTime date2 = DateTime.Now;
            DamagedGoodsViewModel model = new DamagedGoodsViewModel
            {
                DamagedGoodsHeadListModel = _context.DAMAGEDS_GOODS.Join(_context.MA_SUPPLIER, a => a.supplier_id, b => b.supplier_id, (a, b) => new { a, b })
               .Where(o => DbFunctions.TruncateTime(o.a.cdate) >= DbFunctions.TruncateTime(date1) && DbFunctions.TruncateTime(o.a.cdate) <= DbFunctions.TruncateTime(date2) && o.a.process_type == "Remision" && o.a.process_status == 0)
               .Select(p => new DamagedGoodsHeadModel
               {
                   DamagedGoodsDoc = p.a.damaged_goods_doc,
                   Site = p.a.site_code,
                   ProcessType = p.a.process_type,
                   Supplier = p.b.business_name,
                   ProcessStatusDescription = p.a.process_status == 0 ? "Inicial" : p.a.process_status == 1 ? "Pendiente" : p.a.process_status == 2 ? "Aprobado" : p.a.process_status == 3 ? "Rechazado" : p.a.process_status == 4 ? "Solicitud de Cancelación" : p.a.process_status == 5 ? "Aprobación Cancelada (Cancelado)" : p.a.process_status == 8 ? "Cancelado" : p.a.process_status == 9 ? "Terminado" : "",
                   Comment = p.a.comment,
                   SupplierId = p.b.supplier_id
               }).ToList(),

                DamagedGoodsBanatiModel = _context.ITEM.Join(_context.ITEM_STOCK, it => it.part_number, mt => mt.part_number, (it, mt) => new { it, mt })
                .Where(x => x.mt.blocked > 0 && (x.it.level_header != 109 && x.it.level_header != 62) && (x.it.level1_code != 82 && x.it.level1_code != 29)) //Restringiendo: 109: Basicos del botiquin, 62: Vinos y Licores,  29 Cigarros(6abarrotes no comestibles), 82 Farmacia(mercancia generales)
                .Select(iv => new DamagedGoodsBanatiModel
                {
                    PartNumber = iv.it.part_number,
                    Description = iv.it.part_description,
                    Stock = iv.mt.blocked
                }).ToList()
            };

            var available = _context.vw_available_blocked_stock;
            foreach (var stock in available)
            {
                var item = model.DamagedGoodsBanatiModel.Where(x => x.PartNumber == stock.part_number).FirstOrDefault();
                if (item != null)
                {
                    if (stock.Disponible > 0)
                        model.DamagedGoodsBanatiModel.Where(x => x.PartNumber == stock.part_number).FirstOrDefault().Stock = stock.Disponible;
                    else
                        model.DamagedGoodsBanatiModel.Remove(item);
                }
            }

            return model;
        }

        public ItemRmaBanati GetItemStockByPartNumber(string partNumber)
        {
            var itemstock = _context.ITEM
                .Join(_context.ITEM_STOCK, it => it.part_number, mt => mt.part_number, (it, mt) => new { it, mt })
                .Join(_context.STORAGE_LOCATION, sl => sl.mt.storage_location, sll => sll.storage_location_name, (sl, sll) => new { sl, sll })
                .Where(x => x.sl.mt.part_number == partNumber && x.sll.storage_type == "MERMA")
                .Select(iv => new ItemRmaBanati
                {
                    PartNumber = iv.sl.it.part_number,
                    Description = iv.sl.it.part_description,
                    Storage = iv.sl.mt.storage_location,
                    Quantity = iv.sl.mt.blocked
                }).FirstOrDefault();

            var supplier = _context.ITEM_SUPPLIER.Include(y => y.MA_SUPPLIER).Where(x => x.part_number == partNumber).FirstOrDefault();
            if (supplier != null)
            {
                itemstock.SupplierId = supplier.supplier_id;
                itemstock.Supplier = supplier.MA_SUPPLIER.commercial_name;
            }
            else
            {
                int remision = 0;

                if (thisSiteCode == "0008")
                {
                    remision = _DamagedsGoodsItemRepository.BanatiIdSupplier("DE LA FAMILIA");//SISTEMA PARA EL DESARROLLO INTEGRAL DE LA FAMILIA 13328
                }
                else
                {
                    remision = _DamagedsGoodsItemRepository.BanatiIdSupplier("BANATI");//Banco de Alimentos de Tijuana, A.C (BANATI)
                }

                itemstock.SupplierId = remision;
                itemstock.Supplier = "BANATI";
            }

            return itemstock;
        }

        //Restringiendo: 109: Basicos del botiquin, 62: Vinos y Licores,  29 Cigarros(6abarrotes no comestibles), 82 Farmacia(mercancia generales)
        public bool GetValidationItemByRemisionDepartmentFamily(string part_number)
        {
            var item = _context.ITEM.Join(_context.ITEM_STOCK, it => it.part_number, mt => mt.part_number, (it, mt) => new { it, mt })
                .Where(x => x.mt.blocked > 0 && (x.it.level_header != 109 && x.it.level_header != 62) && (x.it.level1_code != 82 && x.it.level1_code != 29) && x.it.part_number == part_number) //Restringiendo: 109: Basicos del botiquin, 62: Vinos y Licores,  29 Cigarros(6abarrotes no comestibles), 82 Farmacia(mercancia generales)
                .FirstOrDefault();
            if (item != null)
                return true;
            else
                return false;
        }

        public bool GetValidationItemByRemisionBloqued(string part_number)
        {
            var item = _context.ITEM.Join(_context.ITEM_STOCK, it => it.part_number, mt => mt.part_number, (it, mt) => new { it, mt })
                .Where(x => x.mt.blocked > 0 && x.it.part_number == part_number) //Restringiendo: 109: Basicos del botiquin, 62: Vinos y Licores,  29 Cigarros(6abarrotes no comestibles), 82 Farmacia(mercancia generales)
                .FirstOrDefault();
            if (item != null)
                return true;
            else
                return false;
        }

        public bool GetValidationItemByBlocked(string part_number)
        {
            var item = _context.ITEM.Join(_context.ITEM_STOCK, it => it.part_number, mt => mt.part_number, (it, mt) => new { it, mt })
                 .Where(x => x.it.part_number == part_number)
                 .FirstOrDefault();
            if (item != null)
                return true;
            else
                return false;
        }

        public bool GetValidationItemByBlockedDeparmentFamily(string part_number)
        {
            //Familias bloqueadas para el pase a bloqueo

            //SELECT* FROM MA_CLASS WHERE class_id = 36--Carnes********
            //SELECT* FROM MA_CLASS WHERE class_id = 37--AVES
            //SELECT* FROM MA_CLASS WHERE class_id = 38--CARNES VARIAS
            //SELECT* FROM MA_CLASS WHERE class_id = 39--CHIVO Y BORREGO
            //SELECT* FROM MA_CLASS WHERE class_id = 40-- PUERCO
            //SELECT* FROM MA_CLASS WHERE class_id = 41-- RES
            //SELECT* FROM MA_CLASS WHERE class_id = 42-- CAMARON Y PESCAD

            //SELECT* FROM MA_CLASS WHERE class_id = 43-- CARNES FRIAS******
            //SELECT * FROM MA_CLASS WHERE class_id = 44-- CHORIZOS
            //SELECT* FROM MA_CLASS WHERE class_id = 45-- JAMONES
            //SELECT* FROM MA_CLASS WHERE class_id = 46-- SALCHICHAS Y BOLONIAS
            //SELECT* FROM MA_CLASS WHERE class_id = 47-- TOCINOS Y PEPERONI

            //SELECT* FROM MA_CLASS WHERE class_id = 48-- Congelados********
            //SELECT* FROM MA_CLASS WHERE class_id = 49-- COMIDAS PREPARADAS

            //SELECT* FROM MA_CLASS WHERE class_id = 70-- COMIDAS PREPARADAS********
            //SELECT * FROM MA_CLASS WHERE class_id = 72-- CARNITAS
            //SELECT* FROM MA_CLASS WHERE class_id = 71-- POLLO ROSTIZADO
            //SELECT* FROM MA_CLASS WHERE class_id = 73-- SALSAS PROCESADAS

            //SELECT* FROM MA_CLASS WHERE class_id = 56-- LACTEOS********
            //SELECT* FROM MA_CLASS WHERE class_id = 57-- CREMAS
            //SELECT* FROM MA_CLASS WHERE class_id = 61-- YOGHURT Y FLANES FRIOS
            //SELECT* FROM MA_CLASS WHERE class_id = 59-- MARGARINAS
            //SELECT* FROM MA_CLASS WHERE class_id = 60-- QUESOS

            //SELECT* FROM MA_CLASS WHERE class_id = 53-- FRUTAS Y VERDURAS*********
            //SELECT* FROM MA_CLASS WHERE class_id = 54-- FRUTAS

            //SELECT* FROM MA_CLASS WHERE class_id = 48-- CONGELADOS*******
            //SELECT* FROM MA_CLASS WHERE class_id = 52-- HIELO
            //SELECT* FROM MA_CLASS WHERE class_id = 51-- VERDUDAS CONGELADAS

            //SELECT* FROM MA_CLASS WHERE class_id = 107-- HUEVO Y DERIVADOS*******
            //SELECT* FROM MA_CLASS WHERE class_id = 108-- HUEVOS

            //SELECT* FROM MA_CLASS WHERE class_id = 66-- TORTILLERIA*******
            //SELECT* FROM MA_CLASS WHERE class_id = 67-- TORTILLAS DE MAIZ
            //SELECT* FROM MA_CLASS WHERE class_id = 68-- MASA
            //SELECT* FROM MA_CLASS WHERE class_id = 69-- TORTILLAS DE TRIGO

            //SELECT* FROM MA_CLASS WHERE class_id = 94-- MATERIA PRIMA*********
            //SELECT * FROM MA_CLASS WHERE class_id = 98-- Materia Alimentos Preparados
            //SELECT* FROM MA_CLASS WHERE class_id = 95-- Materia Prima Pan
            //SELECT* FROM MA_CLASS WHERE class_id = 96-- Materia Prima Tortilleria
            //SELECT* FROM MA_CLASS WHERE class_id = 97-- Materia Prima Carniceria
            //SELECT* FROM MA_CLASS WHERE class_id = 98-- Materia Alimentos Preparados

            //SELECT* FROM MA_CLASS WHERE class_id = 101-- Materia Alimentos Preparados*****
            //SELECT* FROM MA_CLASS WHERE class_id = 102-- Materia Alimentos Preparados

            //SELECT* FROM MA_CLASS WHERE class_id = 76-- MERCANCIAS GENERALES******
            //SELECT * FROM MA_CLASS WHERE class_id = 88-- MINAS DE GAS

            //SELECT* FROM MA_CLASS WHERE class_id = 89-- PANADERIA*******
            //SELECT* FROM MA_CLASS WHERE class_id = 93-- PASTELES GELATINAS Y POSTRES

            //SELECT* FROM MA_CLASS WHERE class_id = 103-- COMIDAS PREPARADAS******
            //SELECT * FROM MA_CLASS WHERE class_id = 105-- Tarjetas de Regalo

            var item = _context.ITEM.Join(_context.ITEM_STOCK, it => it.part_number, mt => mt.part_number, (it, mt) => new { it, mt })
                 .Where(x => (x.it.level1_code != 37 && x.it.level1_code != 38 && x.it.level1_code != 39 && x.it.level1_code != 40 && x.it.level1_code != 41
                 && x.it.level1_code != 43 && x.it.level1_code != 44 && x.it.level1_code != 45 && x.it.level1_code != 46 && x.it.level1_code != 47 && x.it.level1_code != 49
                 && x.it.level1_code != 72 && x.it.level1_code != 71 && x.it.level1_code != 73 && x.it.level1_code != 57 && x.it.level1_code != 59 && x.it.level1_code != 61
                 && x.it.level1_code != 60 && x.it.level1_code != 52 && x.it.level1_code != 51 && x.it.level1_code != 108 && x.it.level1_code != 54 && x.it.level1_code != 52
                 && x.it.level1_code != 67 && x.it.level1_code != 68 && x.it.level1_code != 69 && x.it.level1_code != 96 && x.it.level1_code != 97 && x.it.level1_code != 98
                 && x.it.level1_code != 88 && x.it.level1_code != 102 && x.it.level1_code != 93 && x.it.level1_code != 105 && x.it.level1_code != 95 && x.it.level1_code != 42) && x.it.part_number == part_number)
                 .FirstOrDefault();
            if (item != null)
                return true;
            else
                return false;
        }

        public int GetItemInStorageTypeMerma(string part_number, decimal cantidad)
        {
            var result = _context.vw_available_blocked_stock.Where(x => x.part_number == part_number && cantidad <= x.Disponible).FirstOrDefault();
            if (result != null)
                return 1;
            else
                return 0;
        }

        public decimal GetItemInStorageTypeMermaDifference(string part_number, decimal cantidad)
        {
            var result = _context.vw_available_blocked_stock.Where(x => x.part_number == part_number).FirstOrDefault();
            return cantidad - result.Disponible ?? 0;
        }

        public decimal GetItemInStorageTypeMermaDifferenceReal(string part_number, decimal cantidad)
        {
            var result = _context.ITEM_STOCK.Where(x => x.part_number == part_number && x.storage_location == "MRM01").FirstOrDefault();
            if (result != null)
                return cantidad - result.blocked ?? 0;
            else
                return cantidad;

        }

        public int GetItemStock(string part_number, decimal cantidad)
        {
            var result = _context.ITEM_STOCK.Where(x => x.part_number == part_number && x.storage_location != "MERMA").FirstOrDefault();
            if (result != null)
                return 1;
            else
                return 0;
        }

        public string GetNextDocument(string ProcessName)
        {
            ObjectParameter document = new ObjectParameter("Document", typeof(int));
            _context.sp_Get_Next_Document(ProcessName, document);
            var documentNumber = Convert.ToString(document.Value);

            return documentNumber;
        }

        public StoreProcedureResult CC_Adjustment(string ProcessName, string partNumber, decimal Quantity, string Type, string StorageLocation, string User, string Document, string Comments, decimal? stockReal)
        {
            try
            {
                string UserRequest = _context.INVENTORY_CC.Where(c => c.part_number == partNumber & c.inventory_cc_id == Document).FirstOrDefault().cuser;
                string SiteCode = _SiteConfigRepo.GetSiteCode();
                bool MovementType = Type.Contains("true") ? true : false;

                var DateReturn = _context.Database.SqlQuery<StoreProcedureResult>("DECLARE	@return_value int," +
               "@document nvarchar(10)" +
               "EXEC @return_value = [dbo].[sproc_CC_Adjustment] " +
               "@site_code = N'" + SiteCode + "'," +
               "@inventory_cc_id = N'" + Document + "', " +
               "@storage_location = N'" + StorageLocation + "', " +
               "@part_number = N'" + partNumber + "', " +
               "@quantity = " + Quantity + ",  " +
               "@cc_type = " + MovementType + ", " +
               "@user_authorized = N'" + User + "', " +
               "@user = N'" + UserRequest + "',  " +
               "@remark = N'" + Comments + "', " +
               "@program_id = N'AI002.cshtml', " +
               "@document = @document OUTPUT" +
               " SELECT 'Document'= @Document, 'ReturnValue' = @return_value").SingleOrDefault();

                if (DateReturn.Document != "" && DateReturn.ReturnValue == 0)
                {
                    var InventoryCC = _context.INVENTORY_CC.Where(x => x.part_number == partNumber & x.inventory_cc_id == Document).FirstOrDefault();
                    InventoryCC.uuser = User;
                    InventoryCC.item_stock_current = stockReal;
                    InventoryCC.udate = DateTime.Now;
                    InventoryCC.adjusted_flag = true;
                    InventoryCC.adjusted_document = DateReturn.Document;
                    InventoryCC.authorized_by = User;
                    InventoryCC.program_id = "AI002.cshtml";
                    InventoryCC.comment = Comments;
                    _context.SaveChanges();
                }

                return DateReturn;
            }
            catch (Exception ex)
            {
                string msg = ex.Message;
                return new StoreProcedureResult();
            }
        }

        public bool CancelInventoryAdjustment(string partNumber, string Document, string Comments, string User, decimal? stockReal)
        {
            try
            {
                var InventoryCC = _context.INVENTORY_CC.Where(x => x.part_number == partNumber & x.inventory_cc_id == Document).FirstOrDefault();
                InventoryCC.authorized_by = User;
                InventoryCC.uuser = User;
                InventoryCC.item_stock_current = stockReal;
                InventoryCC.udate = DateTime.Now;
                InventoryCC.adjusted_flag = false;
                InventoryCC.comment = Comments;
                _context.SaveChanges();

                return true;
            }
            catch (Exception ex)
            {
                string msg = ex.Message;
                return false;
            }
        }

        public bool SetItemStock(ItemModel model, string User, string documentNumber, decimal map_price, decimal? stockReal)
        {
            try
            {
                var Inventory_CC = new INVENTORY_CC
                {
                    inventory_cc_id = documentNumber,
                    part_number = model.PathNumber,
                    storage_location = model.StorageLocation,
                    cc_quantity = model.Stock,
                    dr_cr = model.MovementType ? "Cr" : "Dr",
                    comment = model.Comment,
                    cuser = User,
                    cdate = DateTime.Now,
                    program_id = "AI001.cshtml",
                    map_price = map_price,
                    item_stock_current = stockReal
                };

                _context.INVENTORY_CC.Add(Inventory_CC);
                _context.SaveChanges();

                return true;
            }
            catch (Exception ex)
            {
                string msg = ex.Message;
                return false;
            }
        }

        public ItemModel GetItemStockSelectStock(string part_number)
        {
            var result = _context.ITEM_STOCK.Where(x => x.part_number == part_number && x.unrestricted > 0 && x.storage_location == "SL01")
                .Select(y => new ItemModel
                {
                    PathNumber = y.part_number,
                    Stock = y.unrestricted
                }).FirstOrDefault();
            return result;
        }

        public ItemModel GetItemStockSelectStockNegative(string part_number)
        {
            var result = _context.ITEM_STOCK.Where(x => x.part_number == part_number && x.storage_location == "SL01")
                .Select(y => new ItemModel
                {
                    PathNumber = y.part_number,
                    Stock = y.unrestricted ?? 0
                }).FirstOrDefault();
            return result;
        }

        public ItemModel GetItemStockSelectStockBlock(string part_number)
        {
            var result = _context.ITEM_STOCK.Where(x => x.part_number == part_number && x.storage_location == "MRM01")
                .Select(y => new ItemModel
                {
                    PathNumber = y.part_number,
                    Stock = y.blocked ?? 0
                }).FirstOrDefault();
            return result;
        }

        public ItemModel GetItemStock(string part_number)
        {
            var result = _context.ITEM_STOCK
                .Join(_context.ITEM, its => its.part_number, it => it.part_number, (its, it) => new { its, it })
                .Where(x => x.it.part_number == part_number && x.its.storage_location == "SL01")
                .Select(y => new ItemModel
                {
                    PathNumber = y.it.part_number,
                    Description = y.it.part_description,
                    Stock = y.its.unrestricted,
                    StorageLocation = y.it.ITEM_SPECS.gr_storage_location
                }).FirstOrDefault();
            return result;
        }

        public List<ItemModel> InventoryAdjustmentRequests()
        {
            var model = _context.INVENTORY_CC
                .Join(_context.ITEM, inv => inv.part_number, it => it.part_number, (inv, it) => new { inv, it })
                .Join(_context.ITEM_STOCK, cc => cc.inv.part_number, its => its.part_number, (cc, its) => new { cc, its })
                .Where(x => x.cc.inv.adjusted_flag == null && x.its.storage_location == "SL01")
                .Select(y => new ItemModel
                {
                    PathNumber = y.cc.inv.part_number,
                    Description = y.cc.it.part_description,
                    StorageLocation = y.cc.it.ITEM_SPECS.gr_storage_location,
                    Stock = y.its.unrestricted,
                    RequestedStock = y.cc.inv.cc_quantity,
                    MovementType = y.cc.inv.dr_cr.Contains("Cr") ? true : false,
                    Comment = y.cc.inv.comment,
                    Document = y.cc.inv.inventory_cc_id
                }).ToList();
            return model;
        }

        public List<ItemModel> InventoryAdjustmentRequestsByDate(DateTime beginDate, DateTime endDate)
        {
            endDate = endDate.AddDays(1);
            var model = _context.INVENTORY_CC
                .Join(_context.ITEM, inv => inv.part_number, it => it.part_number, (inv, it) => new { inv, it })
                .Join(_context.ITEM_STOCK, cc => cc.inv.part_number, its => its.part_number, (cc, its) => new { cc, its })
                .Where(x => x.cc.inv.adjusted_flag == null && x.its.storage_location != "MRM01" && x.cc.inv.cdate >= beginDate && x.cc.inv.cdate <= endDate)
                .Select(y => new ItemModel
                {
                    PathNumber = y.cc.inv.part_number,
                    Description = y.cc.it.part_description,
                    StorageLocation = y.cc.it.ITEM_SPECS.gr_storage_location,
                    Stock = y.its.unrestricted,
                    RequestedStock = y.cc.inv.cc_quantity,
                    MovementType = y.cc.inv.dr_cr.Contains("Cr") ? true : false,
                    Comment = y.cc.inv.comment,
                    Document = y.cc.inv.inventory_cc_id
                }).ToList();
            return model;
        }

        public List<ItemModel> InventoryAdjustmentRequestsById(string reference)
        {
            var model = _context.INVENTORY_CC
                .Join(_context.ITEM, inv => inv.part_number, it => it.part_number, (inv, it) => new { inv, it })
                .Join(_context.ITEM_STOCK, cc => cc.inv.part_number, its => its.part_number, (cc, its) => new { cc, its })
                .Where(x => x.cc.inv.adjusted_flag == true && x.its.storage_location == "MRM01" && x.cc.inv.inventory_cc_id == reference)
                .Select(y => new ItemModel
                {
                    Document = y.cc.inv.inventory_cc_id,
                    PathNumber = y.cc.inv.part_number,
                    Description = y.cc.it.part_description,
                    StorageLocation = y.cc.it.ITEM_SPECS.gr_storage_location,
                    Stock = y.its.unrestricted,
                    SMovementType = y.cc.inv.dr_cr.ToUpper() == "DR" ? "Debito"
                    : y.cc.inv.dr_cr.ToUpper() == "CR" ? "Credito"
                    : null,
                    Quantity = y.cc.inv.cc_quantity.Value,
                    Comment = y.cc.inv.comment,
                    FStock = y.cc.inv.dr_cr.ToUpper() == "DR" ? y.its.unrestricted.Value - y.cc.inv.cc_quantity.Value
                        : y.cc.inv.dr_cr.ToUpper() == "Cr" ? y.its.unrestricted.Value + y.cc.inv.cc_quantity.Value
                        : 0,
                    User = y.cc.inv.authorized_by
                }).ToList();
            return model;
        }

        public List<NegativeInventoryBalancesTemp> NegativeInventoryBalance()
        {
            int?[] lista = new int?[] { 70, 89, 66, 95, 96, 97, 98, 99, 100, 101, 102, 103, 104, 105, 106 };
            //return _context.ITEM_STOCK.Join(_context.ITEM_VALUATION, IS => IS.part_number, IV => IV.part_number, (IS, IV) => new { IS, IV })
            //    .Join(_context.ITEM, IZ => IZ.IV.part_number, I => I.part_number, (IZ, I) => new { IZ, I })
            //    .Where(w => w.IZ.IS.storage_location == "SL01" && w.I.extra_items_flag != true && w.I.combo_flag != true && w.I.active_flag == true && w.IZ.IV.total_stock < 0 && (!lista.Contains(w.I.level_header)))
            //    .Select(s => new NegativeInventoryBalancesTemp
            //    {
            //        part_number_origin = s.IZ.IS.part_number,
            //        part_number_origin_description = s.I.part_description,
            //        part_number_origin_stock_unrestricted = s.IZ.IV.total_stock ?? 0,
            //        part_number_origin_last_purchase_price = s.IZ.IV.last_purchase_price ?? 0,
            //        part_number_origin_amount = s.IZ.IV.last_purchase_price == 0 ? 0 : (-1 * s.IZ.IV.total_stock ?? 0) * s.IZ.IV.last_purchase_price ?? 0,
            //    }).ToList();
            return _context.Database.SqlQuery<NegativeInventoryBalancesTemp>(@"
            SELECT its.part_number as 'part_number_origin' , i.part_description as 'part_number_origin_description' ,
            isnull(iv.unrestricted,0) as 'part_number_origin_stock_unrestricted' , isnull(iv.map_price,0) as 'part_number_origin_last_purchase_price',
            isnull(iv.unrestricted,0) * isnull(iv.map_price,0) as 'part_number_origin_amount'
            FROM ITEM_STOCK ITS
            JOIN ITEM_VALUATION IV ON IV.part_number = ITS.part_number 
            JOIN ITEM I ON I.part_number = ITS.part_number
            WHERE 
            I.extra_items_flag != 1 and i.combo_flag != 1 and i.active_flag = 1 and iv.unrestricted < 0 AND ITS.storage_location = 'SL01'
            and i.level_header not in ( 70, 89, 66, 95, 96, 97, 98, 99, 100, 101, 102, 103, 104, 105, 106)
            and i.part_number not in (select top 1 part_number_header from INVENTORY_NEGATIVE_HEADER where part_number_header = i.part_number and negative_status = 0)
            order by iv.unrestricted * iv.map_price
").ToList();
        }

        public InventoryNegativeHeaderModel NegativeInventoryBalanceByPartNumber(string part_number)
        {
            var r =_context.Database.SqlQuery<InventoryNegativeHeaderModel>(@"SELECT 
            iv.part_number as 'part_number_header',
            i.part_description as 'part_description',
            isnull(isnull(iv.map_price,iv.last_purchase_price),0) as 'cost_header',
            case when exists(select top 1 id_codes_packages from CODES_PACKAGES cp where cp.part_number_origin = iv.part_number and active_flag = 1)
            then (select top 1 quantity from CODES_PACKAGES cp where cp.part_number_origin = iv.part_number and active_flag = 1)
            else 1 end as 'packing_size',
            its.unrestricted as 'unrestricted_header'
            FROM ITEM_VALUATION IV
            JOIN ITEM_STOCK ITS ON ITS.part_number = IV.part_number
            JOIN ITEM I ON I.part_number = IV.part_number
            WHERE ITS.storage_location = 'SL01'
            AND ITS.part_number = '"+part_number+"'").FirstOrDefault();
            return r;
        }

        public List<NegativeInventoryBalancesTemp> GetAllInvetoriesNegative()
        {
            //return _context.INVENTORY_NEGATIVE_TEMP.Where(w => w.active_flag == true && w.cr_document == null && w.dr_document == null)
            //    .Join(_context.ITEM_STOCK, ist => ist..part_number, I => I.part_number, (IZ, I) => new { IZ, I })
            //    .Select(s => new NegativeInventoryBalancesTemp
            //    {
            //        id_inventory_negative = s.id_inventory_negative,
            //        part_number_origin = s.part_number_origin,
            //        part_number_origin_description = s.part_number_origin_description,
            //        part_number_origin_last_purchase_price = s.part_number_origin_last_purchase_price,
            //        part_number_origin_stock_unrestricted = s.part_number_origin_unrestricted,
            //        part_number_origin_amount = s.part_number_origin_amount,
            //        part_number_destination = s.part_number_destination,
            //        part_number_destination_description = s.part_number_destination_description,
            //        part_number_destination_last_purchase_price = s.part_number_destination_last_purchase_price,
            //        part_number_destination_amount = s.part_number_destination_amount,
            //        part_number_destination_stock_unrestricted = s.part_number_destination_unrestricted,
            //        move_quantity = s.move_quantity,
            //        reason = s.reason
            //    }).ToList();
            return _context.Database.SqlQuery<NegativeInventoryBalancesTemp>(@"select * , (select unrestricted from ITEM_STOCK where part_number = n.part_number_origin and storage_location = 'SL01') stock_origin,
(select unrestricted from ITEM_STOCK where part_number = n.part_number_destination and storage_location = 'SL01') stock_destination
from INVENTORY_NEGATIVE_TEMP n
where n.active_flag = 1 and cr_document is null and dr_document is null").ToList();
        }

        public bool UpdateItemInventoryNegative(int id_inventory_negative, string user)
        {
            try
            {
                var r = _context.Database.ExecuteSqlCommand(@"
                UPDATE INVENTORY_NEGATIVE_HEADER SET negative_status = 8 ,uuser = '"+user+"' , udate = GETDATE() where id_inventory_negative = " + id_inventory_negative);
                if (r == 1)
                    return true;
                else
                {
                    return false;
                }
            }
            catch (Exception e)
            {
                var msg = e.Message;
                return false;
            }
        }

        public bool AddItemInventoryNegative(NegativeInventoryBalancesTemp obj, string user)
        {
            try
            {
                INVENTORY_NEGATIVE_TEMP New = new INVENTORY_NEGATIVE_TEMP
                {
                    part_number_origin = obj.part_number_origin,
                    part_number_origin_description = obj.part_number_origin_description,
                    part_number_origin_last_purchase_price = obj.part_number_origin_last_purchase_price,
                    part_number_origin_amount = obj.part_number_origin_amount,
                    part_number_origin_unrestricted = obj.part_number_origin_stock_unrestricted,
                    part_number_destination = obj.part_number_destination,
                    part_number_destination_description = obj.part_number_destination_description,
                    part_number_destination_unrestricted = obj.part_number_destination_stock_unrestricted,
                    part_number_destination_amount = obj.part_number_destination_amount,
                    part_number_destination_last_purchase_price = obj.part_number_destination_last_purchase_price,
                    active_flag = obj.active_flag,
                    move_quantity = obj.move_quantity,
                    reason = obj.reason,
                    cuser = user,
                    cdate = DateTime.Now,
                    program_id = "INVE013.cshtml"
                };
                _context.INVENTORY_NEGATIVE_TEMP.Add(New);
                _context.SaveChanges();
                return true;
            }
            catch (Exception e)
            {
                var msg = e.Message;
                return false;
            }
        }

        public List<ItemModel> GetStockBySupplier(int supplier_id, int class_id, string category, int family_id)
        {
            try
            {
                var sup_id = new SqlParameter { ParameterName = "SUPPLIERID", Value = supplier_id };
                var cla_id = new SqlParameter { ParameterName = "CLASSID", Value = class_id };
                var fam_id = new SqlParameter { ParameterName = "FAMILYID", Value = family_id };
                var categ = new SqlParameter { ParameterName = "CATEGORYID", Value = category };
                var model = _context.Database.SqlQuery<ItemModel>("sp_Get_ExistenseBySupplierReport @CLASSID, @SUPPLIERID, @FAMILYID, @CATEGORYID ", sup_id, cla_id, fam_id, categ).ToList();
                return model;
            }
            catch (Exception ex)
            {
                var msg = ex.Message;
                return null;
            }
        }
    }
}