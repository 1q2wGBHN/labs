﻿using App.Entities;
using App.Entities.ViewModels.ItemPresentation;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using static System.Linq.Enumerable;

namespace App.DAL.ItemPresentation
{
    public class ItemPresentationRepository
    {
        private readonly DFL_SAIEntities _context;

        public ItemPresentationRepository()
        {
            _context = new DFL_SAIEntities();
        }

        public List<ItemPresentationModel> GetAllActive()
        {
            var r = _context.Database.SqlQuery<ItemPresentationModel>(@"
                       select p.presentation_id as PresentationId,
                        p.presentation_name as Name,
                        p.part_number as PartNumber,
                        i.part_description as PartDescription,
                        p.quantity as Quantity,
                        ISNULL(p.factor_unit_size,'') as FactorUnitSize,
                        p.active_flag as ActiveFlag,
                        ISNULL(p.presentation_code,'') as  PresentationCode,
						(pp.price_sale_iva / (1 + ISNULL(iva.tax_value,0) /100)) as priceIva,

                        pp.price_sale_iva - (pp.price_sale_iva / (1 + ISNULL(iva.tax_value,0) /100)) as  IVA,
						pp.price_sale as Price,
                        ((pp.price_sale_iva / (1 + ISNULL(iva.tax_value,0) /100)) - (pp.price_sale_iva / (1 + ISNULL(iva.tax_value,0) /100)) / (1 + ISNULL(ieps.tax_value,0) /100)) as IEPS,
		                pp.price_sale_iva as TotalPrice
                        from ITEM_PRESENTATION  p
                        join ITEM i on p.part_number = i.part_number
                        join MA_TAX  iva on i.part_iva_purchase = iva.tax_code
                        join MA_TAX  ieps on i.part_ieps = ieps.tax_code
                        join ITEM_PRESENTATION_PRICE pp on p.presentation_id = pp.presentation_id
                        where p.active_flag = 1").ToList();

            return r;
        }

        public List<ItemPresentationModel> GetPresentationByPartNumber(string partNumber)
        {
            var r = _context.Database.SqlQuery<ItemPresentationModel>(@"
                        select p.presentation_id as PresentationId,
                        p.presentation_name as Name,
                        p.part_number as PartNumber,
                        i.part_description as PartDescription,
                        p.quantity as Quantity,
                        ISNULL(p.factor_unit_size,'') as FactorUnitSize,
                        p.active_flag as ActiveFlag,
                        ISNULL(p.presentation_code,'') as  PresentationCode,
						(pp.price_sale_iva / (1 + ISNULL(iva.tax_value,0) /100)) as priceIva,

                        pp.price_sale_iva - (pp.price_sale_iva / (1 + ISNULL(iva.tax_value,0) /100)) as  IVA,
						pp.price_sale as Price,
                        ((pp.price_sale_iva / (1 + ISNULL(iva.tax_value,0) /100)) - (pp.price_sale_iva / (1 + ISNULL(iva.tax_value,0) /100)) / (1 + ISNULL(ieps.tax_value,0) /100)) as IEPS,
		                pp.price_sale_iva as TotalPrice
                        from ITEM_PRESENTATION  p
                        join ITEM i on p.part_number = i.part_number
                        join MA_TAX  iva on i.part_iva_purchase = iva.tax_code
                        join MA_TAX  ieps on i.part_ieps = ieps.tax_code
                        join ITEM_PRESENTATION_PRICE pp on p.presentation_id = pp.presentation_id
                        where i.part_number = @partNumber", new SqlParameter("@partNumber", partNumber)).ToList();

            return r;
        }

        public bool PresentationExist(int id)
        {
            return _context.ITEM_PRESENTATION.Count(p => p.presentation_id == id) > 0;
        }

        public bool PresentationCodeExist(string presentationCode)
        {
            return _context.ITEM_PRESENTATION.Count(p => p.presentation_code == presentationCode) > 0;
        }

        public bool PresentationCodeExistForEdit(string presentationCode, int presentationId)
        {
            return _context.ITEM_PRESENTATION.Count(p => p.presentation_code == presentationCode && p.presentation_id != presentationId) > 0;
        }

        public void Dispose()
        {
            _context.Dispose();
        }
    }
}