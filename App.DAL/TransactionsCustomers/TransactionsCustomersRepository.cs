﻿using App.Entities;
using App.Entities.ViewModels.Common;
using App.Entities.ViewModels.TransactionsCustomers;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Globalization;
using System.Linq;
using static App.Entities.ViewModels.Common.Constants;

namespace App.DAL.TransactionsCustomers
{
    public class TransactionsCustomersRepository
    {
        private DFL_SAIEntities _context;

        public TransactionsCustomersRepository()
        {
            _context = new DFL_SAIEntities();
        }

        public bool CreateTransactions(TRANSACTIONS_CUSTOMERS Item)
        {
            try
            {
                _context.TRANSACTIONS_CUSTOMERS.Add(Item);
                _context.SaveChanges();

                return true;
            }
            catch (Exception ex)
            {
                string msg = ex.Message;
                return false;
            }
        }

        public List<TransactionsCustomersItems> GetReportData(TransactionsCustomersReport Model)
        {
            DateTime DateStart = DateTime.ParseExact(Model.StartDate, DateFormat.INT_DATE, CultureInfo.InvariantCulture);
            DateTime DateEnd = DateTime.ParseExact(Model.EndDate, DateFormat.INT_DATE, CultureInfo.InvariantCulture);
            var ListReport = new List<TransactionsCustomersItems>();            

            ListReport = (from tc in _context.TRANSACTIONS_CUSTOMERS
                          join inv in _context.INVOICES on tc.invoice_no equals inv.invoice_no
                          where DbFunctions.TruncateTime(tc.trans_date) >= DateStart
                                && DbFunctions.TruncateTime(tc.trans_date) <= DateEnd
                                && tc.cur_code == Model.Currency && inv.invoice_status==true
                                && (string.IsNullOrEmpty(Model.CustomerCode) || tc.customer_code == Model.CustomerCode)
                          select new
                          {
                              Number = tc.CUSTOMERS.customer_code,
                              Name = tc.CUSTOMERS.customer_name,
                              Date = tc.trans_date,
                              InvoiceNo = tc.invoice_no,
                              CreditNoteNo = tc.cn_id,
                              ReceiptNo = tc.recibo_no,
                              Amount = tc.trans_amount,
                              TransType = tc.trans_type,
                              Reference = tc.trans_reference
                          }).AsEnumerable().Select(c => new TransactionsCustomersItems
                          {
                              ClientNumber = c.Number.Replace("T", ""),
                              ClientName = c.Name,
                              Date = c.Date.Value.ToString(DateFormat.INT_DATE),
                              Reference = c.Reference,
                              ChargeText = c.TransType.Replace(" ", "") == "C" ? string.Format("${0}", c.Amount.Value.ToString("0.00")) : "-",
                              PaymentText = c.TransType.Replace(" ","") == "A" ? string.Format("${0}", c.Amount.Value.ToString("0.00")) : "-",
                              Charge = c.TransType.Replace(" ","") == "C" ? c.Amount.Value : 0,
                              Payment = c.TransType.Replace(" ","") == "A" ? c.Amount.Value : 0,
                          }).ToList();
           
            return ListReport;
        }

        public bool DeleteTransactions(DeleteModel Model)
        {
            var Item = new TRANSACTIONS_CUSTOMERS();            
            try
            {
                switch (Model.DocumentType)
                {
                    case VoucherType.INCOMES:
                        {
                            Item = _context.TRANSACTIONS_CUSTOMERS.Where(c => c.invoice_no == Model.InvoiceNumber && c.invoice_serie == Model.InvoiceSerie).FirstOrDefault();
                        }
                        break;

                    case VoucherType.EXPENSES:
                        {
                            Item = _context.TRANSACTIONS_CUSTOMERS.Where(c => c.invoice_no == Model.InvoiceNumber && c.cn_id == Model.DocumentNumber).FirstOrDefault();
                        }
                        break;

                    case VoucherType.PAYMENTS:
                        {
                            Item = _context.TRANSACTIONS_CUSTOMERS.Where(c => c.invoice_no == Model.InvoiceNumber && c.recibo_no == Model.DocumentNumber).FirstOrDefault();
                        }
                        break;
                }

                if (Item != null)
                {
                    _context.TRANSACTIONS_CUSTOMERS.Remove(Item);
                    _context.SaveChanges();
                }
                return true;
            }
            catch (Exception ex)
            {
                string msg = ex.Message;
                return false;
            }
        }        
    }
}
