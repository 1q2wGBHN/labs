﻿using App.Entities;
using App.Entities.ViewModels.ItemPresentation;
using App.Entities.ViewModels.PriceTag;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.SqlServer;
using System.Globalization;
using System.Linq;
using App.Entities.ViewModels.SalesPriceChange;

namespace App.DAL.PriceTagPrints
{
    public class PriceTagPrintsRepository
    {
        private readonly DFL_SAIEntities _context;

        public PriceTagPrintsRepository()
        {
            _context = new DFL_SAIEntities();
        }

        public bool CreatePriceTagPrint(List<PrintTagReq> products, string username, string program_id, string name, bool fav)
        {
            using (var trans = _context.Database.BeginTransaction())
            {
                try
                {
                    var e = new PRICE_TAG_PRINTS
                    {
                        printed = false,
                        cuser = username,
                        cdate = DateTime.Now,
                        program_id = program_id,
                        name = name,
                        fav = fav,
                    };
                    _context.PRICE_TAG_PRINTS.Add(e);
                    _context.SaveChanges();
                    for (var i = 0; i < products.Count; i++)
                    {
                        var d = new PRICE_TAG_PRINTS_DETAILS
                        {
                            tag_id = e.tag_id,
                            barcode = products[i].barcode,
                            sequence_num = i,
                            copies = products[i].copies

                        };
                        _context.PRICE_TAG_PRINTS_DETAILS.Add(d);
                    }
                    _context.SaveChanges();
                    trans.Commit();
                    return true;
                }
                catch (Exception)
                {
                    trans.Rollback();
                    return false;
                }
            }
        }

        public List<PriceTagPrintDetailsModel> GetTagList(int PrintId)
        {
            List<PriceTagPrintDetailsModel> Tags = _context.PRICE_TAG_PRINTS_DETAILS
                .Join(_context.BARCODES,details => details.barcode,barcodes => barcodes.part_barcode,(details, barcodes)  => new {details,barcodes} )
                .Where(t => t.details.tag_id == PrintId)
                .Select(t => new PriceTagPrintDetailsModel
                {
                    PartNumber = t.barcodes.part_number,
                    Barcode = t.details.barcode,
                    PrintId = t.details.tag_id,
                    SequenceNum = t.details.sequence_num,
                    copies = t.details.copies,
                    UnitSize = _context.BARCODES
                        .Join(_context.ITEM, barcodes => barcodes.part_number, item => item.part_number,
                            (barcodes, item) => new { item, barcodes })
                        .FirstOrDefault(e => e.barcodes.part_barcode == t.details.barcode).item.unit_size,
                    PartDescription = 
                        t.barcodes.presentation_id != null 
                        ? _context.ITEM_PRESENTATION
                            .FirstOrDefault(p => p.presentation_id == t.barcodes.presentation_id && p.active_flag)
                            .presentation_name
                        : _context.ITEM
                            .FirstOrDefault(i => i.part_number == t.barcodes.part_number)
                            .part_description,
                    PartPrice =
                        t.barcodes.presentation_id != null
                        ? _context.ITEM_PRESENTATION_PRICE
                            .FirstOrDefault(p => p.presentation_id == t.barcodes.presentation_id )
                            .price_sale_iva
                        : _context.ITEM_VALUATION
                                    .FirstOrDefault(i => i.part_number == t.barcodes.part_number).price_sale_iva ?? 0,
                    Presentation =
                        t.barcodes.presentation_id != null
                        ? default(ItemPresentationModel)
                        : _context.ITEM_PRESENTATION
                        .Join(_context.BARCODES, pre => pre.part_number, barcodes => barcodes.part_number,
                            (pre, barcodes) => new { pre, barcodes })
                        .Join(_context.ITEM_PRESENTATION_PRICE, e => e.pre.presentation_id, e => e.presentation_id,
                            (e, price) => new ItemPresentationModel
                            {
                                PresentationCode = e.pre.presentation_code,
                                PartNumber = e.pre.part_number,
                                Quantity = e.pre.quantity,
                                FactorUnitSize = e.pre.factor_unit_size,
                                TotalPrice = price.price_sale_iva,
                                ActiveFlag = e.pre.active_flag,
                                Barcode = e.barcodes.part_barcode,
                            })
                        .FirstOrDefault(e => e.PartNumber == t.barcodes.part_number && e.ActiveFlag),

                    promotion = _context.PROMOTION_DETAIL
                        .Join(_context.PROMOTION_HEADER, e => e.promotion_code, e => e.promotion_code,
                            (d, h) => new { d, h })
                        .Join(_context.BARCODES, e => e.d.part_number, e => e.part_number, (e, b) => new { e.d, e.h, b })
                        .Join(_context.ITEM, e => e.b.part_number, e => e.part_number, (e, i) => new { e.d, e.h, e.b, i })
                        .Join(_context.MA_TAX, e => e.i.part_iva_sale, e => e.tax_code,
                            (e, tax) => new { e.d, e.h, e.b, e.i, tax })

                        .Where(e => (
                                        t.barcodes.presentation_id != null
                                        ? (e.b.presentation_id == e.d.presentation_id && e.b.presentation_id == t.barcodes.presentation_id )
                                        : (e.b.part_number == t.barcodes.part_number && e.b.presentation_id == null)
                                    )
                                    && DbFunctions.TruncateTime(DateTime.Today) >=
                                    DbFunctions.TruncateTime(e.h.promotion_start_date)
                                    && DbFunctions.TruncateTime(DateTime.Today) <=
                                    DbFunctions.TruncateTime(e.h.promotion_end_date)
                                    && e.h.delete_flag==false && e.h.returned_flag ==false
                        )


                        .Select(e => new PromotionItemModel
                        {
                            promotion_price = e.d.promotion_price * (1 + (e.tax.tax_value ?? 0) / 100),
                            start_date = e.h.promotion_start_date.ToString(),
                            end_date = e.h.promotion_end_date.ToString()
                        })
                        .FirstOrDefault(),
                }).OrderBy(e=>e.SequenceNum).ToList();
            return Tags;
        }

        public List<PriceTagPrintModel> GetList(PrintOrderFilter filter)
        {
            IQueryable<PRICE_TAG_PRINTS> query = _context.PRICE_TAG_PRINTS.OrderByDescending(p => p.tag_id);
            if (filter.Printed != null)
                query = query.Where(o => o.printed == filter.Printed.Value);
            if (filter.Fav != null)
                query = query.Where(o => o.fav == filter.Fav.Value);
            DateTime? iniDate = !string.IsNullOrEmpty(filter.IniDate) ? 
                (DateTime?) DateTime.ParseExact(filter.IniDate,"dd/MM/yyyy",CultureInfo.InvariantCulture)
                : null;

            DateTime? endDate = !string.IsNullOrEmpty(filter.EndDate) ?
                (DateTime?)DateTime.ParseExact(filter.EndDate, "dd/MM/yyyy", CultureInfo.InvariantCulture)
                : null;
            if (filter.Fav != true && iniDate.HasValue) { 
                query = query.Where(o => DbFunctions.TruncateTime(o.cdate) >= DbFunctions.TruncateTime(iniDate)
                                         || DbFunctions.TruncateTime(o.udate) >= DbFunctions.TruncateTime(iniDate));
            }

            if (filter.Fav != true && endDate.HasValue)
            {
                query = query.Where(o =>
                    DbFunctions.TruncateTime(o.cdate) <= DbFunctions.TruncateTime(endDate)
                    || DbFunctions.TruncateTime(o.udate) <= DbFunctions.TruncateTime(endDate));
            }

            if (filter.Page.HasValue)
            {
                if (filter.Page < 0) filter.Page = 0;
                if (filter.PageCount == null || filter.PageCount <= 0 || filter.PageCount > 100)
                    filter.PageCount = 10;
                query = query.Skip(filter.PageCount.Value * filter.Page.Value).Take(filter.PageCount.Value);
            }

            
            var res = query.Select(p => new 
            {
                PrintId = p.tag_id,
                Printed = p.printed,
                CUser = p.cuser,
                CDate = p.cdate,
                UUser = p.uuser,
                UDate = p.udate,
                Name = p.name,
                Fav = p.fav,
                TagCount = _context.PRICE_TAG_PRINTS_DETAILS.Count(e => e.tag_id == p.tag_id),
            }).OrderByDescending(e=>e.UDate ?? e.CDate).ToList()
                .Select(p=> new PriceTagPrintModel
                {
                    PrintId = p.PrintId,
                    Printed = p.Printed,
                    CUser = p.CUser,
                    CDate = p.CDate?.ToString("dd/MM/yyyy"),
                    UUser = p.UUser,
                    UDate = p.UDate?.ToString("dd/MM/yyyy"),
                    Name = p.Name,
                    Fav = p.Fav,
                    TagCount = p.TagCount,
                }).ToList();
            
            return res;
        }

        public bool SetAsPrinted(int id)
        {
            var res = _context.PRICE_TAG_PRINTS.SingleOrDefault(p => p.tag_id == id);
            if (res == null) return false;
            res.printed = true;
            _context.SaveChanges();
            return true;
        }
        public bool SetAsFav(int id,bool fav)
        {
            var res = _context.PRICE_TAG_PRINTS.SingleOrDefault(p => p.tag_id == id);
            if (res == null) return false;
            res.fav = fav;
            _context.SaveChanges();
            return true;
        }

        public bool AddToList(int id, List<PrintTagReq> list)
        {
            var old = _context.PRICE_TAG_PRINTS_DETAILS
                .Where(e => e.tag_id == id).OrderBy(e => e.sequence_num)
                .LastOrDefault();

            var i = old?.sequence_num ?? 0 + 1;
            var l = list.Select(e => new PRICE_TAG_PRINTS_DETAILS
            {
                tag_id = id,
                barcode = e.barcode,
                sequence_num = i++,
                copies = e.copies
            }).ToList();
             _context.PRICE_TAG_PRINTS_DETAILS.AddRange(l);
            _context.SaveChanges();
            return true;
        }
        public bool RemoveFromList(int id, int sequence_num)
        {
            var old = _context.PRICE_TAG_PRINTS_DETAILS
                .Where(e => e.tag_id == id)
                .OrderBy(e => e.sequence_num)
                .ToList();
            var el =old.RemoveAll(e => e.sequence_num == sequence_num);
            var i = 0;
            foreach (var e in old)
            {
                e.sequence_num = i++;
            }

            var tag = _context.PRICE_TAG_PRINTS
                .Include(e=>e.PRICE_TAG_PRINTS_DETAILS)
                .FirstOrDefault(e => e.tag_id == id);

            _context.SaveChanges();
            return true;
        }
        public bool EditTagList(int id, PriceTagPrintModel model,string userName)
        {
            var oldh = _context.PRICE_TAG_PRINTS.FirstOrDefault(e => e.tag_id == id);
            oldh.fav = model.Fav;
            oldh.name = model.Name;
            oldh.udate = DateTime.Now;
            oldh.uuser = userName;
            var old = _context.PRICE_TAG_PRINTS_DETAILS
                .Where(e => e.tag_id == id)
                .ToList();
            _context.PRICE_TAG_PRINTS_DETAILS.RemoveRange(old);
            _context.SaveChanges();

            var tags = model.Tags.ToList();
            for (var i = 0; i < tags.Count(); i++)
            {
                var d = new PRICE_TAG_PRINTS_DETAILS
                {
                    tag_id = id,
                    barcode = tags[i].Barcode,
                    sequence_num = i,
                    copies = tags[i].copies

                };
                _context.PRICE_TAG_PRINTS_DETAILS.Add(d);
            }
            _context.SaveChanges();
            return true;
        }
        public bool HistoryCreate(HistoryModel model)
        {
            using (var trans = _context.Database.BeginTransaction())
            {
                try
                {
                    var details = model.Details.ToList();
                    var e = new PRICE_TAG_HISTORY()
                    {
                        process_type = model.process_type,
                        print_status = model.print_status,
                        reprint = model.reprint,
                        cuser = model.cuser,
                        cdate = DateTime.Now,
                        tag_count = model.tag_count,
                    };
                    _context.PRICE_TAG_HISTORY.Add(e);
                    _context.SaveChanges();
                    for (var i = 0; i < details.Count(); i++)
                    {
                        var d = new PRICE_TAG_HISTORY_DETAIL()
                        {
                            history_id = e.history_id,
                            sequence_num = details[i].sequence_num,
                            tag_content = details[i].tag_content,
                            copies = details[i].copies,
                        };
                        _context.PRICE_TAG_HISTORY_DETAIL.Add(d);
                    }
                    _context.SaveChanges();
                    trans.Commit();
                    return true;
                }
                catch (Exception)
                {
                    trans.Rollback();
                    return false;
                }
            }
        }

        public List<HistoryModel> HistoryList(HistoryFilter pub)
        {
            var q = _context.PRICE_TAG_HISTORY.AsQueryable();
            if (string.IsNullOrEmpty(pub.date))
                pub.date = DateTime.Today.ToString("MM/dd/yyyy");
            var date = DateTime.ParseExact(pub.date, "MM/dd/yyyy", CultureInfo.InvariantCulture);
            q = q.Where(e => e.cdate != null && DbFunctions.TruncateTime(e.cdate.Value) == DbFunctions.TruncateTime(date));

            if (!string.IsNullOrEmpty(pub.search))
            {

            }
            var r = q.Select(e => new HistoryModel
            {
                history_id = e.history_id,
                print_status = e.print_status,
                reprint = e.reprint,
                process_type = e.process_type,
                tag_count = e.tag_count,
                cuser = e.cuser,
                cdate = e.cdate,
                uuser = e.uuser,
                udate = e.udate,
                Details = e.PRICE_TAG_HISTORY_DETAIL.Select(em => new PRICE_TAG_HISTORY_DETAIL2
                {
                    detail_id = em.detail_id,
                    history_id = em.history_id,
                    sequence_num = em.sequence_num,
                    tag_content = em.tag_content,
                    copies = em.copies,
                }),
            }).ToList();
            return r;
        }
    }

    public class HistoryModel
    {
        public int history_id { get; set; }
        public string print_status { get; set; }
        public bool reprint { get; set; }
        public string process_type { get; set; }
        public int tag_count { get; set; }
        public string cuser { get; set; }
        public DateTime? cdate { get; set; }
        public string uuser { get; set; }
        public DateTime? udate { get; set; }
        public IEnumerable<PRICE_TAG_HISTORY_DETAIL2> Details { get; set; }
    }

    public class PRICE_TAG_HISTORY_DETAIL2
    {
        public int detail_id { get; set; }
        public int history_id { get; set; }
        public string tag_content { get; set; }
        public int sequence_num { get; set; }
        public int copies { get; set; }
    }

    public class HistoryFilter
    {
        public string search { get; set; }
        public string date { get; set; }
    }

    public class PrintTagReq
    {
        public string barcode { get; set; }
        public int copies { get; set; }
    }
}