﻿using System;

namespace App.Entities.ViewModels.PriceTag
{
    public class PrintOrderFilter
    {
        public bool? Printed { get; set; }
        public String IniDate { get; set; }
        public String EndDate { get; set; }
        public int? Page { get; set; }
        public int? PageCount { get; set; }
        public bool? Fav { get; set; }
    }
}