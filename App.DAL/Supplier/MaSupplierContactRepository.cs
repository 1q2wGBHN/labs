﻿using App.Entities;
using App.Entities.ViewModels.Creditors;
using App.Entities.ViewModels.Supplier;
using App.GlobalEntities;
using System;
using System.Collections.Generic;
using System.Data.Entity.Validation;
using System.Diagnostics;
using System.Linq;

namespace App.DAL.Supplier
{
    public class MaSupplierContactRepository
    {
        private readonly DFL_SAIEntities _context;
        private readonly GlobalERPEntities _Globalcontext;

        public MaSupplierContactRepository()
        {
            _context = new DFL_SAIEntities();
            _Globalcontext = new GlobalERPEntities();
        }

        public string Phone(int Supplier, string Departament)
        {
            var Contact = _context.MA_SUPPLIER_CONTACT.Where(x => x.supplier_id == Supplier && x.departament == Departament && x.flagActive == true).FirstOrDefault();
            if (Contact != null)
                return Contact.phone;

            return "N/A";
        }

        public string Email(int Supplier, string Departament)
        {
            var Contact = _context.MA_SUPPLIER_CONTACT.Where(x => x.supplier_id == Supplier && x.departament == Departament && x.flagActive == true).FirstOrDefault();
            if (Contact != null)
                return Contact.email;

            return "";
        }

        public string EmailsGeneric(int Supplier, string Departament)
        {
            var Contact = _context.MA_SUPPLIER_CONTACT.Where(x => x.supplier_id == Supplier && x.departament == Departament && x.flagActive == true).ToList();
            if (Contact != null)
            {
                var email = "";
                if (Contact.Count() == 1)
                {
                    email = Contact[0].email;
                }
                else {
                    for (int i = 0; i < Contact.Count(); i++)
                    {
                        if (Contact.Count()-1 == i )
                        {
                            email += Contact[i].email;
                            break;
                        }
                        email += $"{Contact[i].email},"; 
                    }
                }

                return email;
            }
            return "";

        }
        public MaSupplierModel InfoBasicContactContabilidad(int supplier_id)
        {
            var Supplier = _context.MA_SUPPLIER.Where(w => w.supplier_id == supplier_id).SingleOrDefault();
            var SupplierInfo = _context.MA_SUPPLIER_CONTACT.Where(x => x.departament == "Contabilidad" && x.supplier_id == Supplier.supplier_id && x.flagActive == true).FirstOrDefault();
            var Email = "N/A";
            var Phone = "N/A";
            var NameContact = "N/A";
            if (SupplierInfo != null)
            {
                Email = SupplierInfo.email;
                Phone = SupplierInfo.phone;
                NameContact = SupplierInfo.name + " " + SupplierInfo.last_name;
            }
            var MaSupplierM = new MaSupplierModel()
            {
                CommercialName = Supplier.commercial_name,
                SupplierAddress = Supplier.supplier_address,
                email = Email,
                name = NameContact,
                phone = Phone,
                Rfc = Supplier.rfc
            };
            return MaSupplierM;
        }
        // Global Repository

        public int AddSupplierContact(GlobalEntities.MA_SUPPLIER_CONTACT_G supplierContact)
        {
            var check = 0;
            _Globalcontext.MA_SUPPLIER_CONTACT.Add(supplierContact);
            try
            {
                check = _Globalcontext.SaveChanges();
            }
            catch (DbEntityValidationException dbEx)
            {
                foreach (var validationErrors in dbEx.EntityValidationErrors)
                {
                    foreach (var validationError in validationErrors.ValidationErrors)
                    {
                        Trace.TraceInformation("Property: {0} Error: {1}",
                                                validationError.PropertyName,
                                                validationError.ErrorMessage);
                    }
                }
            }
            catch (Exception ex)
            {
                var msg = ex.Message;
                check = 99;
            }

            return check;
        }
        public List<SupplierContactModel> GetAllContactsBySupplierId(string supplier_id)
        {
            return _Globalcontext.MA_SUPPLIER_CONTACT.Where(x => x.supplier_id.ToString() == supplier_id)
                .Select(x => new SupplierContactModel
                {
                    supplier_contact_id = x.supplier_contact_id.ToString(),
                    name = x.name,
                    last_name = x.last_name,
                    phone = x.phone,
                    email = x.email,
                    department = x.departament,
                    flagActive = x.flagActive
                })
                .ToList();
        }
        public int UpdateSupplierContact(GlobalEntities.MA_SUPPLIER_CONTACT_G supplierContact)
        {
            _Globalcontext.Entry(supplierContact).State = System.Data.Entity.EntityState.Modified;
            return _Globalcontext.SaveChanges();
        }
        public GlobalEntities.MA_SUPPLIER_CONTACT_G GetContactById(int id)
        {
            return _Globalcontext.MA_SUPPLIER_CONTACT.FirstOrDefault(x => x.supplier_contact_id == id);
        }

        public CreditorPOModel GetContactFirstByDeparment(int id_supplier, string deparment)
        {
            return _Globalcontext.MA_SUPPLIER
            .Join(_Globalcontext.MA_SUPPLIER_CONTACT, h => h.supplier_id, d => d.supplier_id, (h, d) => new { h, d })
            .Where(w => w.d.supplier_id == id_supplier && w.d.departament == deparment && w.d.flagActive == true)
            .Select(s => new CreditorPOModel
            {
                CreditorNameBusiness = s.h.business_name,
                CreditorName = s.d.name,
                CreditorPhone = s.d.phone,
                CreditorEmail = s.d.email
            })
            .FirstOrDefault();
        }

        public string GetContactAllByDeparment(int id_supplier, string deparment)
        {
            var supplierEmails = _Globalcontext.MA_SUPPLIER
            .Join(_Globalcontext.MA_SUPPLIER_CONTACT, h => h.supplier_id, d => d.supplier_id, (h, d) => new { h, d })
            .Where(w => w.d.supplier_id == id_supplier && w.d.departament == deparment && w.d.flagActive == true)
            .Select(s => new CreditorPOModel
            {
                CreditorEmail = s.d.email
            })
            .ToList();
            if (supplierEmails.Count() > 0)
            {
                string emails = "";
                if (supplierEmails.Count() == 1)
                {
                    emails = supplierEmails[0].CreditorEmail;
                }
                else
                {
                    var lastEmail = supplierEmails.Last();
                    foreach (var item in supplierEmails)
                    {

                        if (lastEmail.Equals(item))
                        {
                            emails += item.CreditorEmail;
                        }
                        else
                        {
                            if (emails == "")
                                emails = item.CreditorEmail + ",";
                            else
                                emails = emails + item.CreditorEmail + ",";
                        }
                    }
                }
                return emails;
            }
            else
                return "";
        }
        // END GLOBAL
    }
}

