﻿using App.Entities;
using App.Entities.ViewModels.Supplier;
using App.GlobalEntities;
using System;
using System.Collections.Generic;
using System.Data.Entity.Validation;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Linq;

namespace App.DAL.Supplier
{
    public class MaSupplierRepository
    {
        private readonly DFL_SAIEntities _context;
        private readonly GlobalERPEntities _contextGlobal;

        public MaSupplierRepository()
        {
            _context = new DFL_SAIEntities();
            _contextGlobal = new GlobalERPEntities();
        }

        public List<MaSupplierModel> GetAll()
        {
            return _context.MA_SUPPLIER.Where(x => x.supplier_type == "Proveedor").Select(x => new MaSupplierModel
            {
                SupplierId = x.supplier_id,
                BusinessName = x.business_name
            }).ToList();
        }
        public List<MaSupplierModel> SearchSupplier(string supplier)
        {
            return _context.MA_SUPPLIER.Where(x => x.supplier_type == "Proveedor" && (x.business_name.Contains(supplier) || x.supplier_id.ToString().Contains(supplier))).Select(x => new MaSupplierModel
            {
                SupplierId = x.supplier_id,
                BusinessName = x.business_name
            }).ToList();
        }

        public List<MaSupplierModel> GetAllOrderFormat()
        {
            var suppliers = _context.MA_SUPPLIER.Where(x => x.supplier_type == "Proveedor").Select(x => new MaSupplierModel
            {
                SupplierId = x.supplier_id,
                BusinessName = x.business_name,
                truck_food = x.truck_foot ?? 0
            }).ToList();
            return suppliers.Where(w => w.truck_food == 0).ToList();
        }

        public List<MaSupplierModel> GetAllDevolution()
        {
            return _context.MA_SUPPLIER.Where(x => x.supplier_type == "Proveedor" && x.apply_return == 1).Select(x => new MaSupplierModel
            {
                SupplierId = x.supplier_id,
                BusinessName = x.business_name
            }).ToList();
        }

        public List<Entities.MA_SUPPLIER> GetAllOnlySuppliers()
        {
            return _context.MA_SUPPLIER.Where(w => w.supplier_type != "Acreedor").ToList();
        }

        public List<MaSupplierModel> GetAllOnlyCreditors()
        {
            return _context.MA_SUPPLIER.Where(w => w.supplier_type != "Proveedor").Select(s => new MaSupplierModel
            {
                SupplierId = s.supplier_id,
                BusinessName = s.business_name
            }).ToList();
        }

        public List<MaSupplierModel> GetAllByType(string Type)
        {
            var type = new SqlParameter { ParameterName = "SUPPLIERTYPE", Value = Type };
            var model = _contextGlobal.Database.SqlQuery<MaSupplierModel>("sp_Get_SuppliersByType @SUPPLIERTYPE", type).ToList();

            return model;
        }

        public List<MaSupplierModel> GetAllMerchandiseEntry(int MerchandiseEntry)
        {
            return _context.MA_SUPPLIER.Where(x => x.merchandise_entry == 1 && x.supplier_type == "Proveedor").Select(x => new MaSupplierModel
            {
                SupplierId = x.supplier_id,
                BusinessName = x.business_name
            }).ToList();
        }

        public List<MaSupplierModel> GetAllSuppliersFootTruckInStatus1()
        {
            //return _context.MA_SUPPLIER.Where(x => x.truck_foot == 1 && x.supplier_type == "Proveedor").Select(x => new MaSupplierModel
            return _context.MA_SUPPLIER.Where(x => x.supplier_type == "Proveedor").Select(x => new MaSupplierModel
            {
                SupplierId = x.supplier_id,
                BusinessName = x.business_name
            }).ToList();
        }

        public MaSupplierModel GetSupplierDamagedsGood(int supplier)
        {
            return _context.MA_SUPPLIER.Where(w => w.supplier_id == supplier && w.purchase_applied_return == 1).Select(x => new MaSupplierModel
            {
                SupplierId = x.supplier_id,
                PurchaseAppliedReturn = x.purchase_applied_return ?? 0
            }).FirstOrDefault();
        }

        public List<MaSupplierModel> GetAllSuppliersApplyReturnInStatus1()
        {
            return _context.MA_SUPPLIER.Where(x => x.apply_return == 1 && x.supplier_type == "Proveedor")
                .Select(x => new MaSupplierModel
                {
                    SupplierId = x.supplier_id,
                    BusinessName = x.business_name
                }).ToList();
        }

        public List<MaSupplierModel> GetAllSupLike(String name)
        {
            return _context.MA_SUPPLIER
                .Join(_context.PURCHASE_ORDER, x => x.supplier_id, y => y.supplier_id, (x, y) => new { x, y })
                .Where(s => s.x.commercial_name.Contains(name) && (s.y.purchase_status == 1 || s.y.purchase_status == 5))
                .Select(s => new MaSupplierModel
                {
                    SupplierId = s.x.supplier_id,
                    BusinessName = s.x.business_name,
                    City = s.x.city
                }).Distinct().ToList();
        }

        public MaSupplierModel GetSupplierInfoById(int supplier_id)
        {
            var model = _context.MA_SUPPLIER.Where(w => w.supplier_id == supplier_id)
                .Select(s => new MaSupplierModel
                {
                    SupplierId = s.supplier_id,
                    Rfc = s.rfc,
                    CommercialName = s.commercial_name,
                    BusinessName = s.business_name,
                    SupplierAddress = s.supplier_address,
                    City = s.city,
                    ZipCode = s.zip_code,
                    SupplierType = s.supplier_type,
                    AccountingAccount = s.accounting_account
                }).FirstOrDefault();

            var contact = _context.MA_SUPPLIER_CONTACT.Where(w => w.flagActive == true && w.supplier_id == supplier_id).FirstOrDefault();

            if (contact != null)
            {
                model.supplier_contact_id = contact.supplier_contact_id;
                model.name = contact.name;
                model.last_name = contact.last_name;
                model.phone = contact.phone;
                model.email = contact.email;
                model.departament = contact.departament;
            }
            else
                model.supplier_contact_id = 0;

            return model;
        }

        public string GetNameSupplier(int Supplier)
        {
            var SupplierName = _context.MA_SUPPLIER.Where(x => x.supplier_id == Supplier).SingleOrDefault();
            return SupplierName != null ? SupplierName.commercial_name : "";
        }
        public MaSupplierModel RemisionSupplierId(int supplier_id)
        {
            var model = _context.MA_SUPPLIER.Where(w => w.supplier_id == supplier_id)
                .Select(s => new MaSupplierModel
                {
                    SupplierId = s.supplier_id,
                    Rfc = s.rfc,
                    CommercialName = s.commercial_name,
                    BusinessName = s.business_name,
                    SupplierAddress = s.supplier_address,
                    City = s.city,
                    ZipCode = s.zip_code,
                    SupplierType = s.supplier_type,
                    AccountingAccount = s.accounting_account
                }).FirstOrDefault();

            return model;
        }

        //revisar tienda o global en ma_supplier
        public int GetNameSupllierByfolio(string Folio)
        {
            var SupplierName = _context.DAMAGEDS_GOODS
                .Join(_context.MA_SUPPLIER, a => a.supplier_id, b => b.supplier_id, (a, b) => new { a, b })
                .Where(o => o.a.damaged_goods_doc == Folio)
                .Select(s => new Entities.MA_SUPPLIER
                {
                    supplier_id = s.b.supplier_id
                }).SingleOrDefault();

            return SupplierName != null ? SupplierName.supplier_id : 0;
        }

        public List<Entities.MA_SUPPLIER> GetSupplierByRFC(string rfc)
        {
            try
            {
                var x = _context.MA_SUPPLIER.Where(e => e.rfc == rfc).ToList();
                return x;
            }
            catch (Exception)
            {
                return new List<Entities.MA_SUPPLIER> { new Entities.MA_SUPPLIER { supplier_id = 0 } };
                throw;
            }
        }
        //Global Entities
        public int AddSupplier(GlobalEntities.MA_SUPPLIER_G supplier)
        {

            var check = 0;
            _contextGlobal.MA_SUPPLIER.Add(supplier);
            try
            {
                check = _contextGlobal.SaveChanges();
            }
            catch (DbEntityValidationException dbEx)
            {
                foreach (var validationErrors in dbEx.EntityValidationErrors)
                {
                    foreach (var validationError in validationErrors.ValidationErrors)
                    {
                        Trace.TraceInformation("Property: {0} Error: {1}",
                                                validationError.PropertyName,
                                                validationError.ErrorMessage);
                    }
                }
            }
            catch (Exception ex)
            {
                var msg = ex.Message;
                check = 99;
            }

            return check;
        }
        public GlobalEntities.MA_SUPPLIER_G GetByRfc(string rfc)
        {
            return _contextGlobal.MA_SUPPLIER.Where(x => x.rfc == rfc).FirstOrDefault();
        }
        public GlobalEntities.MA_SUPPLIER_G GetByRfcAndType(string rfc, string type)
        {
            return _contextGlobal.MA_SUPPLIER.Where(x => x.rfc == rfc && x.supplier_type == type).FirstOrDefault();
        }
        public GlobalEntities.MA_SUPPLIER_G GetByRFC2(string rfc)
        {
            return _contextGlobal.MA_SUPPLIER.FirstOrDefault(x => x.rfc == rfc);
        }
        public Tuple<string, int> EditSupplier(string RFC, string type, GlobalEntities.MA_SUPPLIER_G supplier)
        {
            var a = _contextGlobal.MA_SUPPLIER.Any(x => x.rfc == RFC && x.supplier_type == type);
            if (a == true)
                return new Tuple<string, int>("Ya existe el proveedor y como tipo " + type, 0);
            else
            {
                try
                {
                    var sup = _contextGlobal.MA_SUPPLIER.Add(supplier);
                    _contextGlobal.SaveChanges();
                    return new Tuple<string, int>("Guardado Correctamente", sup.supplier_id);
                }
                catch (Exception e)
                {
                    return new Tuple<string, int>("Error", 0);
                    throw;
                }
            }
        }
        public List<SupplierModel> GetAllSuppliersAndCreditorInModel()
        {
            return _contextGlobal.MA_SUPPLIER.Where(o => o.supplier_type == "Acreedor")
                .Select(x => new SupplierModel
                {
                    supplier_id = x.supplier_id,
                    rfc = x.rfc,
                    commercial_name = x.commercial_name,
                    business_name = x.business_name,
                    supplier_address = x.supplier_address,
                    city = x.city,
                    zip_code = x.zip_code,
                    supplier_state = x.supplier_state,
                    country = x.country,
                    supplier_type = x.supplier_type,
                    accounting_account = x.accounting_account,
                    cuser = x.cuser,
                    cdate = x.cdate,
                    uuser = x.uuser,
                    udate = x.udate,
                    program_id = x.program,
                    truck_foot = x.truck_foot ?? 0,
                    merchandise_entry = x.merchandise_entry ?? 0,
                    purchase_applied_return = x.purchase_applied_return ?? 0,
                    apply_return = x.apply_return ?? 0,
                    part_iva_purchase_supplier = x.part_iva_purchase_supplier ?? "N/A"
                })
                .ToList();
        }
        public GlobalEntities.MA_SUPPLIER_G GetById(int id)
        {
            return _contextGlobal.MA_SUPPLIER.FirstOrDefault(x => x.supplier_id == id);
        }
        public bool EditSupplierPartIva(int supplier, string part_iva, string uuser)
        {
            try
            {
                var ma_supplier = _contextGlobal.MA_SUPPLIER.Where(w => w.supplier_id == supplier).FirstOrDefault();

                ma_supplier.part_iva_purchase_supplier = part_iva;
                ma_supplier.uuser = uuser;
                ma_supplier.udate = DateTime.Now;

                _contextGlobal.SaveChanges();

                return true;
            }
            catch (Exception e)
            {
                var msg = e.Message;
                return false;
            }
        }
        public int UpdateItemIVABySupplier(int supplier, string uuser)
        {
            try
            {
                //Busca información del proveedor
                string part_iva_supplier = _contextGlobal.MA_SUPPLIER.Where(w => w.supplier_id == supplier).FirstOrDefault().part_iva_purchase_supplier;
                string supplier_acreedor = _contextGlobal.MA_SUPPLIER.Where(w => w.supplier_id == supplier && w.supplier_type == "Acreedor").FirstOrDefault().supplier_type;

                if (supplier_acreedor != null || supplier_acreedor != "")
                    return 4; //Retorna exitoso directamente porque acreedores no tiene productos relacionados (no deberia)

                if (part_iva_supplier == null || part_iva_supplier == "" || part_iva_supplier == "N/A")
                    return 1; //Asignar un iva Correcto

                var items = _contextGlobal.ITEM_SUPPLIER.Where(w => w.supplier_id == supplier && !_contextGlobal.ITEM_SUPPLIER_INACTIVE.Any(y => y.supplier_part_number == w.supplier_part_number)).ToList();
                if (items.Count() == 0)
                    return 2; //El proveedor no tiene productos relacionados

                var itemIVA = _contextGlobal.ITEM_SUPPLIER.Join(_contextGlobal.ITEM, IS => IS.part_number, I => I.part_number, (IS, I) => new { IS, I })
                    .Join(_contextGlobal.MA_TAX, II => II.I.part_iva_purchase, M => M.tax_code, (II, M) => new { II, M })
                    .Where(w => w.II.IS.supplier_id == supplier && w.M.tax_type == "1" && !_contextGlobal.ITEM_SUPPLIER_INACTIVE.Any(y => y.supplier_part_number == w.II.IS.supplier_part_number)).ToList();
                if (itemIVA.Count() == 0)
                    return 3; //El proveedor no tiene productos con IVA

                //Cambiar el IVA a todos los productos con IVA
                itemIVA.ForEach(f => { f.II.I.part_iva_purchase = part_iva_supplier; f.II.I.udate = DateTime.Now; f.II.I.uuser = uuser; });
                _contextGlobal.SaveChanges();

                return 4;// Exitoso

            }
            catch (Exception e)
            {
                var msg = e.Message;
                return 0; //Error
            }
        }
        public int UpdateSupplier(GlobalEntities.MA_SUPPLIER_G supplier)
        {
            _contextGlobal.Entry(supplier).State = System.Data.Entity.EntityState.Modified;
            return _contextGlobal.SaveChanges();
        }
    }
}