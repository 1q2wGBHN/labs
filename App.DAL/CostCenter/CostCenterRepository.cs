﻿using App.DAL.Item;
using App.Entities;
using App.Entities.ViewModels.CostCenter;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Xml;

namespace App.DAL.CostCenter
{
    public class CostCenterRepository
    {
        private readonly DFL_SAIEntities _context;
        private readonly ItemRepository _ItemRepository;

        public CostCenterRepository()
        {
            _context = new DFL_SAIEntities();
            _ItemRepository = new ItemRepository();
        }

        public int GetCostCenterPrintStatus(string Document)
        {
            return _context.GI_COST_CENTER.Where(w => w.gi_document == Document).Select(s => s.print_status ?? 0).FirstOrDefault();
        }

        public string GetCostCenterById(int CostCenterId)
        {
            return _context.GI_COST_CENTER.Where(x => x.gi_cost_center_id == CostCenterId).Select(x => x.cost_center).FirstOrDefault();
        }

        public bool GetCostCenterUpdatePrintStatus(string Document, int print_status, string user)
        {
            try
            {
                var GiCost = _context.GI_COST_CENTER.Where(w => w.gi_document == Document).FirstOrDefault();
                GiCost.program_id = "COST001.cshtml";
                GiCost.uuser = user;
                GiCost.print_status = print_status + 1;
                GiCost.udate = DateTime.Now;
                _context.SaveChanges();
                return true;
            }
            catch (Exception ex)
            {
                var msg = ex.Message;
                return false;
            }
        }

        public List<GICostCenterItem> GetCostCenterListById(int id)
        {
            var model = _context.Database.SqlQuery<GICostCenterItem>(@"SELECT COSTCENTER.gi_cost_center_id 'GiCostCenterId', COSTCENTER.cost_center 'CostCenter', 
                COSTCENTERITEM.part_number 'PartNumber', ITEM.part_description 'Description', COSTCENTERITEM.quantity 'Quantity'
                FROM GI_COST_CENTER COSTCENTER  
                INNER JOIN GI_COST_CENTER_ITEM COSTCENTERITEM ON COSTCENTER.gi_cost_center_id = COSTCENTERITEM.gi_cost_center_id
                INNER JOIN ITEM ON COSTCENTERITEM.part_number = ITEM.part_number
                WHERE COSTCENTER.gi_cost_center_id = @GiCostCenterId", new SqlParameter("@GiCostCenterId", id)).ToList();

            return model;
        }

        public List<GICostCenterItem> GetCostCenterDetailsById(int id)
        {
            var model = _context.Database.SqlQuery<GICostCenterItem>(@"SELECT COSTCENTER.gi_cost_center_id 'GiCostCenterId', COSTCENTER.cost_center 'CostCenter', COSTCENTERITEM.part_number 'PartNumber', 
                ITEM.part_description 'Description', DEP.class_name 'Department', FAM.class_name 'Family', COSTCENTERITEM.quantity 'Quantity', 
                COSTCENTERITEM.cost 'UnitCost', (COSTCENTERITEM.quantity * COSTCENTERITEM.cost) 'Amount', (COSTCENTERITEM.ieps * COSTCENTERITEM.quantity) 'Ieps', 
                (COSTCENTERITEM.iva * COSTCENTERITEM.quantity) 'Iva', (COSTCENTERITEM.quantity * (COSTCENTERITEM.cost + COSTCENTERITEM.ieps + COSTCENTERITEM.iva)) 'Total' 
                FROM GI_COST_CENTER COSTCENTER 
                INNER JOIN GI_COST_CENTER_ITEM COSTCENTERITEM ON COSTCENTER.gi_cost_center_id = COSTCENTERITEM.gi_cost_center_id 
                INNER JOIN ITEM ON COSTCENTERITEM.part_number = ITEM.part_number 
                INNER JOIN MA_CLASS DEP ON ITEM.level_header = DEP.class_id 
                INNER JOIN MA_CLASS FAM ON ITEM.level1_code = FAM.class_id 
                WHERE COSTCENTER.gi_cost_center_id = @GiCostCenterId", new SqlParameter("@GiCostCenterId", id)).ToList();

            return model;
        }

        public List<GICostCenterItem> GetCostCenterDetailsByDates(DateTime BeginDate, DateTime EndDate)
        {
            var model = _context.Database.SqlQuery<GICostCenterItem>(@"SELECT COSTCENTER.gi_document 'GiDocument', COSTCENTER.cost_center 'CostCenter', COSTCENTERITEM.part_number 'PartNumber', 
                ITEM.part_description 'Description', DEP.class_name 'Department', FAM.class_name 'Family', COSTCENTERITEM.quantity 'Quantity', 
                COSTCENTERITEM.cost 'UnitCost', (COSTCENTERITEM.quantity * COSTCENTERITEM.cost) 'Amount', (COSTCENTERITEM.ieps * COSTCENTERITEM.quantity) 'Ieps', 
                (COSTCENTERITEM.iva * COSTCENTERITEM.quantity) 'Iva', (COSTCENTERITEM.quantity * (COSTCENTERITEM.cost + COSTCENTERITEM.ieps + COSTCENTERITEM.iva)) 'Total' 
                FROM GI_COST_CENTER COSTCENTER 
                INNER JOIN GI_COST_CENTER_ITEM COSTCENTERITEM ON COSTCENTER.gi_cost_center_id = COSTCENTERITEM.gi_cost_center_id 
                INNER JOIN ITEM ON COSTCENTERITEM.part_number = ITEM.part_number 
                INNER JOIN MA_CLASS DEP ON ITEM.level_header = DEP.class_id 
                INNER JOIN MA_CLASS FAM ON ITEM.level1_code = FAM.class_id 
                WHERE CONVERT(DATE,COSTCENTER.cdate) BETWEEN @BEGINDATE AND @ENDDATE AND COSTCENTER.gi_status <> 3 AND COSTCENTERITEM.gi_status <> 3", new SqlParameter("@BEGINDATE", BeginDate), new SqlParameter("@ENDDATE", EndDate)).ToList();

            return model;
        }

        public List<GICostCenter> GetCostCenterRecords(DateTime BeginDate, DateTime EndDate)
        {
            var model = _context.Database.SqlQuery<GICostCenter>(@"SELECT COSTCENTER.gi_cost_center_id 'GiCostCenterId', COSTCENTER.gi_document 'GIDocument', COSTCENTER.cost_center 'CostCenter', 
                CASE WHEN COSTCENTER.gi_status = 0 THEN 'Inicial' WHEN COSTCENTER.gi_status = 1 THEN 'Pendiente' WHEN COSTCENTER.gi_status = 3 THEN 'Rechazado' WHEN COSTCENTER.gi_status = 8 THEN 'Cancelado' WHEN COSTCENTER.gi_status = 9 THEN 'Terminado' ELSE '' END 'Status', 
                CASE WHEN ITEM.amount IS NULL THEN 0 ELSE ITEM.amount END AS 'Amount', [USER].first_name + ' ' + [USER].last_name 'UserCreate', COSTCENTER.cdate 'CreateDate' 
                FROM GI_COST_CENTER COSTCENTER 
                INNER JOIN USER_MASTER [USER] ON COSTCENTER.cuser = [USER].[user_name] 
                INNER JOIN (SELECT gi_cost_center_id, SUM(quantity * (cost + ieps + iva)) AS amount FROM GI_COST_CENTER_ITEM WHERE gi_status <> 3 GROUP BY gi_cost_center_id) ITEM ON COSTCENTER.gi_cost_center_id = ITEM.gi_cost_center_id 
                WHERE CONVERT(DATE,COSTCENTER.cdate) BETWEEN @BEGINDATE AND @ENDDATE AND COSTCENTER.gi_status <> 3", new SqlParameter("@BEGINDATE", BeginDate), new SqlParameter("@ENDDATE", EndDate)).ToList();

            return model;
        }

        public List<GICostCenter> GetCostCenterRecords(DateTime BeginDate, DateTime EndDate, int Status)
        {
            var model = _context.Database.SqlQuery<GICostCenter>(@"SELECT COSTCENTER.gi_cost_center_id 'GiCostCenterId', COSTCENTER.gi_document 'GIDocument', COSTCENTER.cost_center 'CostCenter', 
                CASE WHEN COSTCENTER.gi_status = 0 THEN 'Inicial' WHEN COSTCENTER.gi_status = 1 THEN 'Pendiente' WHEN COSTCENTER.gi_status = 3 THEN 'Rechazado' WHEN COSTCENTER.gi_status = 8 THEN 'Cancelado' WHEN COSTCENTER.gi_status = 9 THEN 'Terminado' ELSE '' END 'Status', 
                CASE WHEN ITEM.amount IS NULL THEN 0 ELSE ITEM.amount END AS 'Amount', [USER].first_name + ' ' + [USER].last_name 'UserCreate', COSTCENTER.cdate 'CreateDate' 
                FROM GI_COST_CENTER COSTCENTER 
                INNER JOIN USER_MASTER [USER] ON COSTCENTER.cuser = [USER].[user_name] 
                INNER JOIN (SELECT gi_cost_center_id, SUM(quantity * (cost + ieps + iva)) AS amount FROM GI_COST_CENTER_ITEM GROUP BY gi_cost_center_id) ITEM ON COSTCENTER.gi_cost_center_id = ITEM.gi_cost_center_id 
                WHERE CONVERT(DATE,COSTCENTER.cdate) BETWEEN @BEGINDATE AND @ENDDATE AND COSTCENTER.gi_status = @STATUS", new SqlParameter("@BEGINDATE", BeginDate), new SqlParameter("@ENDDATE", EndDate), new SqlParameter("@STATUS", Status)).ToList();

            return model;
        }

        public List<GICostCenterItem> GetCostCenterRecordsDetail(DateTime BeginDate, DateTime EndDate)
        {
            var model = _context.Database.SqlQuery<GICostCenterItem>(@"SELECT COSTCENTER.gi_document 'GiDocument', COSTCENTER.gi_cost_center_id 'GiCostCenterId', COSTCENTER.cost_center 'CostCenter', 
                COSTCENTERITEM.part_number 'PartNumber', ITEM.part_description 'Description', DEP.class_name 'Department', FAM.class_name 'Family', 
                COSTCENTERITEM.quantity 'Quantity', COSTCENTERITEM.cost 'UnitCost', (COSTCENTERITEM.quantity * COSTCENTERITEM.cost) 'Amount', 
                (COSTCENTERITEM.ieps * COSTCENTERITEM.quantity) 'Ieps', (COSTCENTERITEM.iva * COSTCENTERITEM.quantity) 'Iva', 
                (COSTCENTERITEM.quantity * (COSTCENTERITEM.cost + COSTCENTERITEM.ieps + COSTCENTERITEM.iva)) 'Total' 
                FROM GI_COST_CENTER COSTCENTER 
                INNER JOIN GI_COST_CENTER_ITEM COSTCENTERITEM ON COSTCENTER.gi_cost_center_id = COSTCENTERITEM.gi_cost_center_id 
                INNER JOIN ITEM ON COSTCENTERITEM.part_number = ITEM.part_number 
                INNER JOIN MA_CLASS DEP ON ITEM.level_header = DEP.class_id 
                INNER JOIN MA_CLASS FAM ON ITEM.level1_code = FAM.class_id 
                WHERE CONVERT(DATE,COSTCENTER.cdate) BETWEEN @BEGINDATE AND @ENDDATE AND COSTCENTER.gi_status <> 3 AND COSTCENTERITEM.gi_status <> 3", new SqlParameter("@BEGINDATE", BeginDate), new SqlParameter("@ENDDATE", EndDate)).ToList();

            return model;
        }

        public GICostCenter GetCostCenterByGiDocument(string GiDocument)
        {
            var model = _context.Database.SqlQuery<GICostCenter>(@"SELECT COST_CENTER.gi_cost_center_id 'GiCostCenterId', SITES.site_name 'OriginSite', COST_CENTER.cost_center 'CostCenter', 
                USER_CRE.first_name + ' ' + USER_CRE.last_name 'UserCreate', USER_AUT.first_name + ' ' + USER_AUT.last_name 'UserAuthorize', 
                COST_CENTER.gi_document 'GIDocument', COST_CENTER.gi_date 'GIDate', 
                CASE WHEN COST_CENTER.gi_status = 0 THEN 'Inicial' WHEN COST_CENTER.gi_status = 1 THEN 'Pendiente' WHEN COST_CENTER.gi_status = 3 THEN 'Rechazado' WHEN COST_CENTER.gi_status = 8 THEN 'Cancelado' WHEN COST_CENTER.gi_status = 9 THEN 'Terminado' ELSE '' END 'Status' 
                FROM GI_COST_CENTER COST_CENTER 
                INNER JOIN USER_MASTER USER_AUT ON COST_CENTER.user_authorize = USER_AUT.[user_name] 
                INNER JOIN USER_MASTER USER_CRE ON COST_CENTER.cuser = USER_CRE.[user_name] 
                INNER JOIN SITES ON COST_CENTER.site_code = SITES.site_code 
                WHERE COST_CENTER.gi_document = @GiDocument", new SqlParameter("@GiDocument", GiDocument)).FirstOrDefault();

            return model;
        }

        public List<GICostCenterItem> GetCostCenterItemsByGiDocument(string GiDocument)
        {
            var model = _context.Database.SqlQuery<GICostCenterItem>(@"SELECT COST_CENTER_ITEM.part_number 'PartNumber', ITEM.part_description 'Description', 
                CASE WHEN COST_CENTER_ITEM.quantity IS NULL THEN 0 ELSE COST_CENTER_ITEM.quantity END 'Quantity', 
                COST_CENTER_ITEM.cost 'UnitCost', 
                (COST_CENTER_ITEM.cost * COST_CENTER_ITEM.quantity) 'Amount', 
                (COST_CENTER_ITEM.ieps * COST_CENTER_ITEM.quantity) 'Ieps', 
                (COST_CENTER_ITEM.iva * COST_CENTER_ITEM.quantity) 'Iva', 
                ((COST_CENTER_ITEM.cost * COST_CENTER_ITEM.quantity) + (COST_CENTER_ITEM.ieps * COST_CENTER_ITEM.quantity) + (COST_CENTER_ITEM.iva * COST_CENTER_ITEM.quantity)) 'Total', 
                CASE WHEN COST_CENTER_ITEM.gi_status = 0 THEN 'Inicial' WHEN COST_CENTER_ITEM.gi_status = 1 THEN 'Pendiente' WHEN COST_CENTER_ITEM.gi_status = 3 THEN 'Rechazado' WHEN COST_CENTER_ITEM.gi_status = 8 THEN 'Cancelado' WHEN COST_CENTER_ITEM.gi_status = 9 THEN 'Terminado' ELSE '' END 'Status' 
                FROM GI_COST_CENTER_ITEM COST_CENTER_ITEM 
                INNER JOIN ITEM ON COST_CENTER_ITEM.part_number = ITEM.part_number 
                WHERE COST_CENTER_ITEM.gi_document = @GiDocument", new SqlParameter("@GiDocument", GiDocument)).ToList();

            return model;
        }

        public int CreateCostCenter(string SiteCode, string CostCenter, int Status, string User, string ProgramId, List<GICostCenterItem> Items)
        {
            try
            {
                XmlDocument xDoc = new XmlDocument();
                XmlReaderSettings readerSettings = new XmlReaderSettings
                {
                    ConformanceLevel = ConformanceLevel.Fragment
                };

                StringBuilder xml = new StringBuilder();
                xml.Append("<CostCenter>");

                foreach (var item in Items)
                {
                    xml.Append("<Items>");

                    decimal basecost = _ItemRepository.GetMapPrice(item.PartNumber);
                    var values = _ItemRepository.StoreProcedureIvaIeps(item.PartNumber);
                    decimal ieps = (basecost * values.Ieps) / 100;
                    decimal iva = (basecost * values.Iva) / 100;

                    xml.Append("<part_number>" + item.PartNumber + "</part_number>");
                    xml.Append("<quantity>" + item.Quantity + "</quantity>");
                    xml.Append("<cost>" + basecost + "</cost>");
                    xml.Append("<ieps>" + ieps + "</ieps>");
                    xml.Append("<iva>" + iva + "</iva>");

                    xml.Append("</Items>");
                }

                xml.Append("</CostCenter>");
                xDoc.LoadXml(xml.ToString());

                var sitecode = new SqlParameter("@SiteCode", SiteCode);
                var costcenter = new SqlParameter("@CostCenter", CostCenter);
                var status = new SqlParameter("@Status", Status);
                var user = new SqlParameter("@User", User);
                var programid = new SqlParameter("@ProgramId", ProgramId);
                var xmlParameter = new SqlParameter("@xml", xDoc.OuterXml);

                var result = _context.Database.SqlQuery<int>("DECLARE @num INT EXEC @num = insert_cost_center @SiteCode, @CostCenter, @Status, @User, @ProgramId, @xml; SELECT @num", sitecode, costcenter, status, user, programid, xmlParameter).First();

                return result;
            }
            catch (Exception ex)
            {
                var msg = ex.Message;
                return 0;
            }
        }

        public bool UpdateCostCenter(int id, string CostCenter, int Status, string User, string ProgramId, List<GICostCenterItem> Items)
        {
            try
            {
                XmlDocument xDoc = new XmlDocument();
                XmlReaderSettings readerSettings = new XmlReaderSettings
                {
                    ConformanceLevel = ConformanceLevel.Fragment
                };

                StringBuilder xml = new StringBuilder();
                xml.Append("<CostCenter>");

                foreach (var item in Items)
                {
                    xml.Append("<Items>");

                    decimal basecost = _ItemRepository.GetMapPrice(item.PartNumber);
                    var values = _ItemRepository.StoreProcedureIvaIeps(item.PartNumber);
                    decimal ieps = (basecost * values.Ieps) / 100;
                    decimal iva = (basecost * values.Iva) / 100;

                    xml.Append("<part_number>" + item.PartNumber + "</part_number>");
                    xml.Append("<quantity>" + item.Quantity + "</quantity>");
                    xml.Append("<cost>" + basecost + "</cost>");
                    xml.Append("<ieps>" + ieps + "</ieps>");
                    xml.Append("<iva>" + iva + "</iva>");

                    xml.Append("</Items>");
                }

                xml.Append("</CostCenter>");
                xDoc.LoadXml(xml.ToString());

                var gicostcenterid = new SqlParameter("@GICostCenterId", id);
                var costcenter = new SqlParameter("@CostCenter", CostCenter);
                var status = new SqlParameter("@Status", Status);
                var user = new SqlParameter("@User", User);
                var programid = new SqlParameter("@ProgramId", ProgramId);
                var xmlParameter = new SqlParameter("@xml", xDoc.OuterXml);
                _context.Database.CommandTimeout = 6000;
                var result = _context.Database.SqlQuery<int>("DECLARE @num INT EXEC @num = update_cost_center @GICostCenterId, @CostCenter, @Status, @User, @ProgramId, @xml; SELECT @num", gicostcenterid, costcenter, status, user, programid, xmlParameter).First();

                if (result > 0)
                    return true;
                else
                    return false;
            }
            catch (Exception ex)
            {
                var msg = ex.Message;
                return false;
            }
        }

        public bool CancelCostCenter(int CostCenterId, string User)
        {
            try
            {
                var CostCenter = _context.GI_COST_CENTER.Where(x => x.gi_cost_center_id == CostCenterId).FirstOrDefault();
                CostCenter.gi_status = 3;
                CostCenter.uuser = User;
                CostCenter.udate = DateTime.Now;

                var Items = _context.GI_COST_CENTER_ITEM.Where(x => x.gi_cost_center_id == CostCenterId).ToList();
                foreach (var item in Items)
                {
                    item.gi_status = 3;
                    item.uuser = User;
                    item.udate = DateTime.Now;
                }

                _context.SaveChanges();

                return true;
            }
            catch (Exception ex)
            {
                var msg = ex.Message;
                return false;
            }
        }

        public bool ProcessCenter(int id, string User, string programid, out string GiDocument)
        {
            try
            {
                _context.Database.CommandTimeout = 6000;
                var result = _context.Database.SqlQuery<StoreProcedureResult>("DECLARE @return_value INT," +
                    "@document NVARCHAR(10)" +
                    "EXEC @return_value = [dbo].[sproc_gi_for_cost_center]" +
                    "@gi_cost_center_id = " + id + "," +
                    "@cuser = N'" + User + "'," +
                    "@program_id = N'" + programid + "'," +
                    "@document = @document OUTPUT" +
                    " SELECT 'Document'= @Document, 'ReturnValue' = @return_value").FirstOrDefault();
                if (result.ReturnValue == 0)
                {
                    GiDocument = result.Document;
                    return true;
                }
                else
                {
                    GiDocument = "";
                    return false;
                }
            }
            catch (Exception)
            {
                GiDocument = "";
                return false;
            }
        }
    }
}