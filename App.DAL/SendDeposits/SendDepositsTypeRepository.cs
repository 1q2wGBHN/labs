﻿using App.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App.DAL.SendDeposits
{
    public class SendDepositsTypeRepository
    {
        private DFL_SAIEntities _context;

        public SendDepositsTypeRepository()
        {
            _context = new DFL_SAIEntities();
        }

        public List<SEND_DEPOSITS_TYPE> TypeList()
        {
            return _context.SEND_DEPOSITS_TYPE.ToList();
        }
    }
}
