﻿using App.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App.DAL.SendDeposits
{
    public class SendDepositsOriginRepository
    {
        private DFL_SAIEntities _context;

        public SendDepositsOriginRepository()
        {
            _context = new DFL_SAIEntities();
        }

        public List<SEND_DEPOSITS_ORIGIN> OriginList()
        {
            return _context.SEND_DEPOSITS_ORIGIN.ToList();
        }
    }
}
