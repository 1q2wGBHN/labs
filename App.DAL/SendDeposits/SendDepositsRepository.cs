﻿using App.Entities;
using App.Entities.ViewModels.SendDeposits;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static App.Entities.ViewModels.Common.Constants;

namespace App.DAL.SendDeposits
{
    public class SendDepositsRepository
    {
        private DFL_SAIEntities _context;

        public SendDepositsRepository()
        {
            _context = new DFL_SAIEntities();
        }

        public bool Create(SEND_DEPOSITS SendDeposit)
        {
            try
            {
                _context.SEND_DEPOSITS.Add(SendDeposit);
                _context.SaveChanges();
                return true;
            }
            catch (Exception ex)
            {
                string msg = ex.Message;
                return false;
            }
        }

        public List<SendDepositsModel> GetDeposits(string Date)
        {
            var FormatDate = DateTime.ParseExact(Date, DateFormat.INT_DATE, CultureInfo.InvariantCulture);
            var List = new List<SendDepositsModel>();
            List = (from s in _context.SEND_DEPOSITS
                    where s.status
                    && DbFunctions.TruncateTime(s.date) == FormatDate
                    select new
                    {
                        Id = s.id,
                        Date = s.date,
                        CUser = s.cuser,
                        Type = s.SEND_DEPOSITS_TYPE.description,
                        Origin = s.SEND_DEPOSITS_ORIGIN.description,
                        Comments = s.comments,
                        Currency = s.CURRENCY.cur_name,
                        Amount = s.amount,
                        CDate = s.cdate
                    }).AsEnumerable().Select(s => new SendDepositsModel
                    {
                        Id = s.Id,
                        Date = s.Date.ToString(DateFormat.INT_DATE),
                        CUser = s.CUser,
                        Type = s.Type,
                        Origin = s.Origin,
                        Comments = s.Comments,
                        Currency = s.Currency,
                        Amount = s.Amount,
                        CDate = s.CDate
                    }).ToList();

            return List;
        }

        public bool DeleteDeposits(int DepositId, string Uuser, SYS_LOG SysLog)
        {
            var ItemEdit = _context.SEND_DEPOSITS.Find(DepositId);
            if (ItemEdit != null)
            {
                ItemEdit.status = false;
                ItemEdit.uuser = Uuser;
                ItemEdit.udate = DateTime.Now;
                ItemEdit.program_id = "SEDE002.cshtml";

                if (SysLog != null)
                    _context.SYS_LOG.Add(SysLog);

                _context.SaveChanges();
                return true;
            }
            return false;
        }

        public SendDepositsModel GetDepositById(int DepositId)
        {
            var Deposit = new SendDepositsModel();
            var Item = _context.SEND_DEPOSITS.Find(DepositId);
            if (Item != null)
            {
                Deposit.Id = Item.id;
                Deposit.Type = Item.type;
                Deposit.Origin = Item.origin;
                Deposit.Amount = Item.amount;
                Deposit.Comments = Item.comments;
                Deposit.Currency = Item.cur_code;
                Deposit.SiteCode = Item.site_code;
                Deposit.Status = Item.status;
                Deposit.Date = Item.date.ToString(DateFormat.INT_DATE);
                Deposit.CUser = Item.cuser;
                Deposit.CDate = Item.cdate;
            }
            return Deposit;
        }

        public bool UpdateDeposit(SEND_DEPOSITS Item, SYS_LOG SysLog)
        {
            var ItemEdit = _context.SEND_DEPOSITS.Find(Item.id);
            if (ItemEdit != null)
            {

                _context.Entry(ItemEdit).State = EntityState.Detached;
                _context.Entry(Item).State = EntityState.Modified;
                _context.SaveChanges();                

                if (SysLog != null)
                    _context.SYS_LOG.Add(SysLog);

                _context.SaveChanges();

                return true;
            }

            return false;
        }
    }
}
