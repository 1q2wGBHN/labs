﻿using App.Entities;
using App.Entities.ViewModels.Item;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;

namespace App.DAL.EmptySpace
{
    public class EmptySpaceScanDetailRepository
    {
        private readonly DFL_SAIEntities _context;

        public EmptySpaceScanDetailRepository()
        {
            _context = new DFL_SAIEntities();
        }

        /// <summary>
        /// Guarda un nuevo espacio vacío
        /// </summary>
        /// <param name="param">objeto Informacion de todo el producto escaneado</param>
        /// <returns>Regresa el numeero del espacio vacio si todo fue correcto pero 0 si hubo error</returns>
        public int CreateEmptySpaceScanDetail(ItemEmptySpaceScanModel EmptySpaceScanDetailModel)
        {
            try
            {
                var EmptySpaceScanDetail = new EMPTY_SPACE_SCAN_DETAIL()
                {
                    id_empty_space = EmptySpaceScanDetailModel.id_empty_space,
                    part_number = EmptySpaceScanDetailModel.part_number,
                    description = EmptySpaceScanDetailModel.description,
                    stock_existing = EmptySpaceScanDetailModel.stock_existing,
                    planogram = EmptySpaceScanDetailModel.planogram,
                    last_entry = EmptySpaceScanDetailModel.last_entry,
                    last_entry_quantity = EmptySpaceScanDetailModel.last_entry_quantity,
                    average_sale = EmptySpaceScanDetailModel.average_sale,
                    incidence = 0,
                    cuser = EmptySpaceScanDetailModel.cuser,
                    cdate = EmptySpaceScanDetailModel.cdate,
                    program_id = EmptySpaceScanDetailModel.program_id
                };

                _context.EMPTY_SPACE_SCAN_DETAIL.Add(EmptySpaceScanDetail);
                _context.SaveChanges();
                return EmptySpaceScanDetail.id_empty_space;
            }
            catch (Exception e)
            {
                var message = e.Message;
                return 0;
            }
        }

        /// <summary>
        /// Actuctualiza el espacio vacio y aumenta la incidencia
        /// </summary>
        /// <param name="param">objeto Informacion de todo el producto escaneado</param>
        /// <param name="param">string usuario que modifica</param>
        /// <returns>Regresa el numeero del espacio vacio si todo fue correcto pero 0 si hubo error</returns>
        public int UpdateEmptySpaceScanDetailIncidence(EMPTY_SPACE_SCAN_DETAIL EmptySpaceScanDetailModel, string uuser)
        {
            try
            {
                EmptySpaceScanDetailModel.uuser = uuser;
                EmptySpaceScanDetailModel.udate = DateTime.Now;
                EmptySpaceScanDetailModel.incidence += 1;
                _context.SaveChanges();
                return 1;
            }
            catch (Exception)
            {
                return 0;
            }
        }

        /// <summary>
        /// Actuctualiza el espacio vacio (no aumenta la incidencia)
        /// </summary>
        /// <param name="param">objeto Informacion de todo el producto escaneado</param>
        /// <param name="param">string usuario que modifica</param>
        /// <returns>Regresa 1 si todo fue correcto pero 0 si hubo error</returns>
        public int UpdateEmptySpaceScanDetail(EMPTY_SPACE_SCAN_DETAIL EmptySpaceScanDetailModel, string uuser)
        {
            try
            {
                EmptySpaceScanDetailModel.uuser = uuser;
                EmptySpaceScanDetailModel.udate = DateTime.Now;
                _context.SaveChanges();
                return 1;
            }
            catch (Exception)
            {
                return 0;
            }
        }

        /// <summary>
        /// Obtener si existen coincidencia con el prodcutos escaneado con los ya esceanos anteriormente (mismo día) (comparando codigo de producto y planograma)
        /// </summary>
        /// <param name="param">string part_number</param>
        /// <param name="param">string planogram</param>
        /// <returns>Regresa informacion de los codigos y planogramas que coincidieron</returns>
        public ItemEmptySpaceScanModel GetInfoEmptyByPartNumber(string part_number, string planogram)
        {
            try
            {
                ItemEmptySpaceScanModel item = _context.Database.SqlQuery<ItemEmptySpaceScanModel>(@"SELECT 
                part_number, planogram, id_empty_space FROM EMPTY_SPACE_SCAN_DETAIL 
                WHERE part_number = @part_number AND planogram = @planogram AND Convert(varchar(8),cdate,112)= Convert(varchar(8),GetDate(),112)",
                new SqlParameter("part_number", part_number),
                new SqlParameter("planogram", planogram)).FirstOrDefault();

                return item;
            }
            catch (Exception ex)
            {
                var msg = ex.Message;
                return null;
            }
        }

        /// <summary>
        /// Obtener si existen coincidencia en con uno ya ingresado (buscando por su iddentificador, codigo de productos y planograma)
        /// </summary>
        /// <param name="param">int identificador del espacio vacio header</param>
        /// <param name="param">string codigo del producto</param>
        /// <param name="param">string planograma</param>
        /// <returns>Regresa informacion de los codigos y planogramas que coincidieron</returns>
        public EMPTY_SPACE_SCAN_DETAIL GetEmptySpaceScanDetailByPartNumberId(int id_empty_space, string part_number, string planogram)
        {
            return _context.EMPTY_SPACE_SCAN_DETAIL.SingleOrDefault(x => x.id_empty_space == id_empty_space && x.part_number == part_number && x.planogram == planogram);
        }

        /// <summary>
        /// Obtener toda la informacion de los espacios vacios por producto buscando por su identificador
        /// </summary>
        /// <param name="param">int identificador del espacio vacio header</param>
        /// <returns>Regresa todos los espacios vacios de cierto identificador</returns>
        public List<ItemEmptySpaceScanModel> GetAllItemsByIdEmptySpace(int id_empty_space)
        {
            return _context.EMPTY_SPACE_SCAN_DETAIL.Where(x => x.id_empty_space == id_empty_space)
                .Select(s => new ItemEmptySpaceScanModel
                {
                    id_empty_space = s.id_empty_space,
                    part_number = s.part_number,
                    description = s.description,
                    stock_existing = s.stock_existing,
                    planogram = s.planogram,
                    last_entry = s.last_entry,
                    last_entry_quantity = s.last_entry_quantity,
                    average_sale = s.average_sale,
                    incidence = s.incidence,
                    cuser = s.cuser
                }).ToList();
        }

        /// <summary>
        /// Obtener todos los escaneos de parte del usuario usuario
        /// </summary>
        /// <param name="param">string user</param>
        /// <returns>Regresa informacion de la informacion de los escaneos que registraron</returns>
        public List<ItemEmptySpaceScanModel> GetEmptySpaceScanHistoryByUser(string user)
        {
            try
            {
                var item = _context.Database.SqlQuery<ItemEmptySpaceScanModel>(@"SELECT 
                part_number, description , planogram FROM EMPTY_SPACE_SCAN_DETAIL 
                WHERE Convert(varchar(8),cdate,112)= Convert(varchar(8),GetDate(),112) AND cuser = @userHistory or uuser = @userHistory;",
                new SqlParameter("userHistory", user)).ToList();

                return item;
            }
            catch (Exception ex)
            {
                var msg = ex.Message;
                return null;
            }
        }
    }
}