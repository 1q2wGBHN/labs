﻿using App.Entities;
using App.Entities.ViewModels.EmptySpaceScan;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;

namespace App.DAL.EmptySpace
{
    public class EmptySpaceScanRepository
    {
        private readonly DFL_SAIEntities _context;

        public EmptySpaceScanRepository()
        {
            _context = new DFL_SAIEntities();
        }

        /// <summary>
        /// Insertar un nuevo Espacio vacio header
        /// </summary>
        /// <param name="param">string cuser</param>
        /// <param name="param">string program_id</param>
        /// <returns>Regresa 0 si hubo error o 1 si todo fue correcto</returns>
        public int CreateEmptySpace(string cuser, string program_id)
        {
            try
            {
                var EmptyScape = new EMPTY_SPACE_SCAN()
                {
                    empty_space_status = 0,
                    cuser = cuser,
                    incidence = 0,
                    cdate = DateTime.Now,
                    program_id = program_id
                };

                _context.EMPTY_SPACE_SCAN.Add(EmptyScape);
                _context.SaveChanges();

                return EmptyScape.id_empty_space;
            }
            catch (Exception)
            {
                return 0;
            }
        }

        /// <summary>
        /// Actualiza un nuevo Espacio vacio header y aumenta la incidencia
        /// </summary>
        /// <param name="param">objeto del Espacio_Vacio</param>
        /// <param name="param">string uuser</param>
        /// <returns>Regresa 0 si hubo error o 1 si todo fue correcto</returns>
        public int UpdateEmptySpaceIncidence(EMPTY_SPACE_SCAN infoEmpty, string uuser)
        {
            try
            {
                infoEmpty.uuser = uuser;
                infoEmpty.udate = DateTime.Now;
                infoEmpty.incidence = infoEmpty.incidence + 1;
                _context.SaveChanges();
                return 1;
            }
            catch (Exception)
            {
                return 0;
            }
        }

        /// <summary>
        /// Actualiza un nuevo Espacio vacio header (no aumenta la incidencia)
        /// </summary>
        /// <param name="param">objeto del Espacio_Vacio</param>
        /// <param name="param">string uuser</param>
        /// <returns>Regresa 0 si hubo error o 1 si todo fue correcto</returns>
        public int UpdateEmptySpace(EMPTY_SPACE_SCAN infoEmpty, string uuser)
        {
            try
            {
                infoEmpty.uuser = uuser;
                infoEmpty.udate = DateTime.Now;
                _context.SaveChanges();
                return 1;
            }
            catch (Exception)
            {
                return 0;
            }
        }

        /// <summary>
        /// Busca un espacio vacio por su identificador
        /// </summary>
        /// <param name="param">int identificador por el espacio vacio</param>
        /// <returns>Regresa el objeto del identificador del espacio vacio</returns>
        public EMPTY_SPACE_SCAN GetEmptySpaceById(int id_empty_space)
        {
            return _context.EMPTY_SPACE_SCAN.SingleOrDefault(x => x.id_empty_space == id_empty_space);
        }

        /// <summary>
        /// Obtiene el listado del intervalo de las fechas
        /// </summary>
        /// <param name="param">datetime fecha incio</param>
        /// /// <param name="param">datetime fecha final</param>
        /// <returns>Regresa el listado de los espacios vacios de las fechas seleccionados</returns>
        public List<EmptySpaceScanModel> GetEmptySpaceByDates(DateTime fechaIni, DateTime fechaFin)
        {
            var items = _context.EMPTY_SPACE_SCAN
                .Where(x => DbFunctions.TruncateTime(x.cdate) >= DbFunctions.TruncateTime(fechaIni)
                            && DbFunctions.TruncateTime(x.cdate) <= DbFunctions.TruncateTime(fechaFin))
                .ToList()
                .Select(x => new EmptySpaceScanModel
                {
                    id_empty_space = x.id_empty_space,
                    empty_space_status = x.empty_space_status,
                    incidence = x.incidence,
                    cuser = x.cuser,
                    cdate = x.cdate.ToString("dd/MM/yyyy"),
                    uuser = x.uuser,
                    udate = x.udate != null ? x.udate.Value.ToString("dd/MM/yyyy") : "N/A"
                }).ToList();
            for (int i = 0; i < items.Count; i++)
            {
                var user_name = items[i].cuser;
                var name = _context.USER_MASTER.Where(x => x.user_name == user_name).SingleOrDefault();
                items[i].cuser = name.first_name + " " + name.last_name;
            }
            return items;
        }
    }
}