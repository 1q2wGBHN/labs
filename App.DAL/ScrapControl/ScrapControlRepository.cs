﻿using App.Entities;
using App.Entities.ViewModels.DamagedsGoods;
using System;
using System.Collections.Generic;
using System.Linq;

namespace App.DAL.ScrapControl
{
    public class ScrapControlRepository
    {
        private readonly DFL_SAIEntities _context;

        public ScrapControlRepository()
        {
            _context = new DFL_SAIEntities();
        }

        public int AddScrapControlItemAndroid(SCRAP_CONTROL scrapItem)
        {
            try
            {
                _context.SCRAP_CONTROL.Add(scrapItem);
                var save = _context.SaveChanges();
                return save;
            }
            catch (Exception ex)
            {
                var msg = ex.Message;
                return 0;
            }
        }

        public StoreProcedureResult StoreProcedureScrap(string Site, string PartNumber, decimal Quantity, string Sl, string ToSl, string ProgramId, string User)
        {
            var Scrap = _context.Database.SqlQuery<StoreProcedureResult>("DECLARE	@return_value int, @Document varchar(10) EXEC    @return_value = [dbo].[sproc_ST_UnrestrictedToBlocked] @site_code = N'" + Site + "', @part_number = N'" + PartNumber + "', @quantity = " + Quantity + ", @sl = N'" + Sl + "', @to_sl = N'" + ToSl + "', @program_id = N'" + ProgramId + "', @user = N'" + User + "', @Document = @Document OUTPUT SELECT  'Document' = @Document, 'ReturnValue' = @return_value").ToList();
            return Scrap[0];
        }

        public List<DamagedsGoodsModel> GenericGetList()
        {
            var consultaGenerico = @"
                select part_number as 'PartNumber', quantity as 'Quantity', 'reverse remision' as 'Description' from DAMAGEDS_GOODS_ITEM where damaged_goods_doc = '2000000360' and part_number in 
('7501040001246',
'7501040002144',
'7501040002441',
'7501040003011',
'7501040003417',
'7501040004896',
'7501040004988',
'7501040006197',
'7501040006385',
'7501040090950')




                ";

            var xx = _context.Database.SqlQuery<DamagedsGoodsModel>(consultaGenerico).ToList();
            return xx;
        }

        public List<DamagedsGoodsAndroid> GenericGetListBlockedtoUnrestricted()
        {
            var consultaGenerico = @"
                select part_number, blocked as 'quantityDecimal' from ITEM_STOCK WHERE storage_location = 'MRM01' and blocked > 0

                ";

            var xx = _context.Database.SqlQuery<DamagedsGoodsAndroid>(consultaGenerico).ToList();
            return xx;
        }
    }
}