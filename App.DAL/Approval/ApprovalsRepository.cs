﻿using App.Entities;
using App.GlobalEntities;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;

namespace App.DAL.Approval
{
    public class ApprovalsRepository
    {
        private readonly GlobalERPEntities _context;
        private readonly DFL_SAIEntities _storeContext;

        public ApprovalsRepository()
        {
            _context = new GlobalERPEntities();
            _storeContext = new DFL_SAIEntities();
        }

        public APPROVAL_HISTORY StartProcess(string processName, string empNo, string processGroup, string programId)
        {
            try
            {
                var store = _context.Database.SqlQuery<StoreProcedureResult>(@"
                    DECLARE	@return_value int,
		                    @Document nvarchar(10)

                    EXEC	@return_value = [dbo].[sp_Get_Next_Document]
		                    @Process_Name = N'AUTHORIZATION_CODE',
		                    @Document = @Document OUTPUT

                    SELECT	'Document' = @Document

                    SELECT	'Return Value' = @return_value").Single();

                var route = _context.APPROVAL_MASTER_ROUTE.Single(e => e.process_name == processName && e.step_level == 1);

                if (store.ReturnValue != 0)
                    return null;

                var model = new APPROVAL_HISTORY
                {
                    approval_id = store.Document,
                    process_name = processName,
                    emp_no = empNo,
                    step_level = 1,
                    approval_type = route.approval_type,
                    approval_status = ApprovalStatus.Approved,
                    remark = "",
                    approval_datetime = DateTime.Now,
                    program_id = programId,
                    process_group = processGroup
                };

                _context.APPROVAL_HISTORY.Add(model);
                _context.SaveChanges();

                return model;
            }
            catch (Exception)
            {
                return null;
            }
        }

        public APPROVAL_HISTORY CreateHistoryRecord(APPROVAL_HISTORY model)
        {
            try
            {
                _context.APPROVAL_HISTORY.Add(model);
                _context.SaveChanges();

                return model;
            }
            catch (Exception ex)
            {
                string msg = ex.Message;
                return null;
            }
        }

        public APPROVAL_HISTORY UpdateHistoryRecord(APPROVAL_HISTORY model)
        {
            try
            {
                var n = _context.APPROVAL_HISTORY.Single(e =>
                    e.approval_id == model.approval_id
                    && e.process_name == model.process_name
                    && e.step_level == model.step_level);

                n.approval_status = model.approval_status;
                n.approval_datetime = model.approval_datetime;
                n.approval_type = model.approval_type;
                n.emp_no = model.emp_no;
                n.remark = model.remark;
                n.program_id = model.program_id;
                _context.SaveChanges();

                return n;
            }
            catch (Exception)
            {
                return null;
            }
        }

        public APPROVAL_MASTER_ROUTE GetApprovalRoute(string processName, int step)
        {
            return _context.APPROVAL_MASTER_ROUTE.SingleOrDefault(e => e.process_name == processName && e.step_level == step);
        }

        public List<USER_MASTER> UsersFromProcessStep(string processName, int step, IEnumerable<string> processGroup)
        {
            var rGlobal = (from app in _context.APPROVAL_MASTER_ROUTE
                           join u in _context.APPROVAL_PROCESS_USER on new { app.process_name, app.step_level } equals new
                           { u.process_name, u.step_level }
                           where app.process_name == processName && app.step_level == step && u.active_flag == true &&
                                 processGroup.Contains(u.process_group)
                           select u.emp_no).ToList();
            var rLocal = (from user in _storeContext.USER_MASTER
                          where rGlobal.Contains(user.emp_no)
                          select user).ToList();

            return rLocal;
        }

        public List<USER_MASTER> RequestersUsersForStep(string approvalId, string processName, int step, string processGroup)
        {
            var rGlobal = (
                from routes in _context.APPROVAL_HISTORY
                join u in _context.APPROVAL_PROCESS_USER on new { routes.process_name, routes.step_level } equals new
                { u.process_name, u.step_level }
                where routes.approval_id == approvalId && routes.approval_type == ApprovalType.Request &&
                      routes.step_level < step && u.process_group == processGroup
                select routes.emp_no).ToList();

            var rLocal = (from user in _storeContext.USER_MASTER
                          where rGlobal.Contains(user.emp_no)
                          select user).ToList();

            return rLocal;
        }

        public bool ProcessIsRejected(string id)
        {
            return (from h in _context.APPROVAL_HISTORY
                    where h.approval_status == ApprovalStatus.Rejected && h.approval_id == id
                    select h).Any();
        }

        public bool ProcessIsApproved(string id)
        {
            var ret = (from h in _context.APPROVAL_HISTORY
                       join r in _context.APPROVAL_MASTER_ROUTE on new { h.process_name, h.step_level } equals new
                       { r.process_name, r.step_level }
                       where r.finish_flag == true && h.approval_status == ApprovalStatus.Approved && h.approval_id == id
                       select h).Any();

            return ret;
        }

        public bool CkPermissions(string empNo, string processName, int step, IEnumerable<string> processGroups)
        {
            return _context.APPROVAL_PROCESS_USER.Any(e => e.emp_no == empNo && e.process_name == processName
                                                                             && e.step_level == step &&
                                                                             e.active_flag == true &&
                                                                             (e.step_level == 1 ||
                                                                              processGroups.Contains(e.process_group)));
        }

        public APPROVAL_PROCESS_USER Permissions(string empNo, string processName, int step, string processGroup)
        {
            return _context.APPROVAL_PROCESS_USER.FirstOrDefault(e => e.emp_no == empNo && e.process_name == processName
                                                                                        && e.step_level == step &&
                                                                                        e.active_flag == true &&
                                                                                        (e.step_level == 1 ||
                                                                                         e.process_group == processGroup
                                                                                        ));
        }

        public List<APPROVAL_MASTER_ROUTE> RouteOfProcess(string processName)
        {
            var r = _context.APPROVAL_MASTER_ROUTE.Where(e => e.process_name == processName).Select(e => e);
            return r.ToList();
        }

        class StoreProcedureResult
        {
            public string Document { get; set; }
            public int ReturnValue { get; set; }
        }

        public List<ProcessType> ProcessTypeList()
        {
            throw new NotImplementedException();
        }

        public List<Process> ProcessList(string processType)
        {
            return _context.APPROVAL_MASTER_ROUTE.GroupBy(e => e.process_name).Select(e => new Process
            {
                process_name = e.Key,
                process_type = ""
            }).ToList();
        }

        public List<ProcessStep> StepList(string processName)
        {
            return _context.APPROVAL_MASTER_ROUTE
                .Where(e => e.process_name == processName)
                .Select(e => new ProcessStep
                {
                    process_name = e.process_name,
                    step_level = e.step_level,
                    approval_type = e.approval_type,

                }).ToList();
        }

        public List<StepUser> UserList(string processName, int stepLevel)
        {
            var r = _context.APPROVAL_PROCESS_USER
                .Where(e => e.process_name == processName && e.step_level == stepLevel && e.active_flag == true)
                .OrderBy(e => e.process_group).ThenBy(e => e.emp_no)
                .Select(e => new StepUser
                {
                    process_name = e.process_name,
                    step_level = e.step_level,
                    emp_no = e.emp_no,
                    process_group = e.process_group,

                }).ToList();

            r.ForEach(
                e => e.emp_name = _storeContext.USER_MASTER
                    .Where(u => e.emp_no == u.emp_no)
                    .Select(emp => emp.first_name + " " + emp.last_name)
                    .FirstOrDefault());
            return r;
        }

        public bool StepAdd(ProcessStep step)
        {
            try
            {
                var old = _context.APPROVAL_MASTER_ROUTE.FirstOrDefault(e => e.process_name == step.process_name && e.step_level == step.step_level);

                if (old != null)
                {
                    old.approval_type = step.approval_type;
                    old.finish_flag = step.finish_flag;
                    old.udate = DateTime.Now;
                    if (old.finish_flag == true)
                        _context.Database.ExecuteSqlCommand(@"update APPROVAL_MASTER_ROUTE set finish_flag=0 where finish_flag=1 and process_name=@process_name", new SqlParameter("@process_name", old.process_name));
                }
                else
                {
                    var m = new APPROVAL_MASTER_ROUTE
                    {
                        process_name = step.process_name,
                        step_level = step.step_level,
                        approval_type = step.approval_type,
                        finish_flag = step.finish_flag,
                        cdate = DateTime.Now,
                        cuser = null,
                        program_id = null,
                    };

                    if (m.finish_flag == true)
                        _context.Database.ExecuteSqlCommand(@"update APPROVAL_MASTER_ROUTE set finish_flag=0 where finish_flag=1 and process_name=@process_name", new SqlParameter("@process_name", m.process_name));

                    _context.APPROVAL_MASTER_ROUTE.Add(m);
                }
                _context.SaveChanges();

                return true;
            }
            catch (Exception ex)
            {
                string msg = ex.Message;
                return false;
            }
        }

        public bool UserAdd(StepUser user)
        {
            try
            {
                var old = _context.APPROVAL_PROCESS_USER.FirstOrDefault(e =>
                    e.process_name == user.process_name && e.step_level == user.step_level && e.emp_no == user.emp_no &&
                    e.process_group == user.process_group);

                if (old != null)
                {
                    old.active_flag = true;
                    old.udate = DateTime.Now;
                }
                else
                {
                    var m = new APPROVAL_PROCESS_USER
                    {
                        process_name = user.process_name,
                        step_level = user.step_level,
                        emp_no = user.emp_no,
                        process_group = user.process_group,
                        active_flag = true,
                        cdate = DateTime.Today,
                        cuser = null,
                        udate = null,
                        uuser = null,
                        program_id = null
                    };
                    _context.APPROVAL_PROCESS_USER.Add(m);
                }

                _context.SaveChanges();
                return true;

            }
            catch (Exception ex)
            {
                string msg = ex.Message;
                return false;
            }
        }

        public bool UserDel(StepUser user)
        {
            try
            {
                var old = _context.APPROVAL_PROCESS_USER.FirstOrDefault(e =>
                e.process_name == user.process_name && e.step_level == user.step_level && e.emp_no == user.emp_no &&
                e.process_group == user.process_group);

                if (old == null)
                    return false;

                old.active_flag = false;
                old.udate = DateTime.Now;
                _context.SaveChanges();
                return true;
            }
            catch (Exception ex)
            {
                string msg = ex.Message;
                return false;
            }
        }
    }

    public class ApprovalFilter
    {
        public bool? LastOnly { get; set; }
        public string UserName { get; set; }
    }

    public static class ApprovalStatus
    {
        public const string Approved = "aprobado";
        public const string Rejected = "rechazado";
        public const string OnWaiting = "en espera";
    }

    public static class ApprovalType
    {
        public const string Request = "RQ";
        public const string Review = "RV";
        public const string Approval = "AP";
        public const string Consent = "CN";

        public static bool IsValid(string s)
        {
            return s == Request || s == Review || s == Approval || s == Consent;
        }
    }

    public class ProcessType
    {
        public string process_type { get; set; }
    }

    public class Process
    {
        public string process_name { get; set; }
        public string process_type { get; set; }
    }

    public class ProcessStep
    {
        public bool? finish_flag { get; set; }
        public string process_name { get; set; }
        public int step_level { get; set; }
        public string approval_type { get; set; }
    }

    public class StepUser
    {
        public string process_name { get; set; }
        public int step_level { get; set; }
        public string emp_no { get; set; }
        public string process_group { get; set; }
        public string emp_name { get; set; }
    }
}