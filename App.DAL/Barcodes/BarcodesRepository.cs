﻿using App.Entities;
using App.Entities.ViewModels.Android;
using System.Linq;

namespace App.DAL.Barcodes
{
    public class BarcodesRepository
    {
        private readonly DFL_SAIEntities _context;

        public BarcodesRepository()
        {
            _context = new DFL_SAIEntities();
        }

        public BarcodesModel GetAllDataByBarcode(string barcode)
        {
            var value = _context.BARCODES.Where(f => f.part_barcode == barcode || f.part_number == barcode).FirstOrDefault();

            if (value != null)
            {
                BarcodesModel model = new BarcodesModel
                {
                    part_barcode = value.part_barcode,
                    part_number = value.part_number
                };
                return model;
            }
            else
                return null;
        }
    }
}