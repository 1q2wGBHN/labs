using App.DAL;
using App.Entities;
using App.Entities.ViewModels.Configuration;
using App.Entities.ViewModels.SysInfo;
using App.Entities.ViewModels.User;
using System;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;

namespace App.BLL
{
    public class UserMasterBusiness
    {
        private readonly UserMasterRepository _userMasterRepo;
        private readonly SysRoleUserRepository _sysRoleUserRepository;

        public UserMasterBusiness()
        {
            _userMasterRepo = new UserMasterRepository();
            _sysRoleUserRepository = new SysRoleUserRepository();
        }

        public USER_MASTER GetUserMasterByEmployeeNumber(string employeeNumber) => _userMasterRepo.GetUserMasterByEmployeeNumber(employeeNumber);

        public List<USER_MASTER> GetUsersMasterByRol(List<string> role_idList) => _userMasterRepo.GetUsersMasterByRol(role_idList);

        public USER_MASTER GetUserMasterByUsername(string username) => _userMasterRepo.GetUserMasterByUsername(username);

        public USER_MASTER GetUserMasterByEmail(string email) => _userMasterRepo.GetUserMasterByEmail(email);

        public bool AddUserMaster(USER_MASTER user)
        {
            var check = false;
            var flag = _userMasterRepo.AddUserMaster(user);

            if (flag > 0) check = true;

            return check;
        }

        public bool IsPasswordCorrect(USER_MASTER user_master, string password)
        {
            bool check = false;
            string sha1Pass = Common.Common.SetPassword(password);

            if (user_master.USER_PWD.password.Equals(sha1Pass))
            {
                _userMasterRepo.ReloadCountUser(user_master);
                check = true;
            }

            return check;
        }

        public bool IsAccountBlocked(USER_MASTER user_master)
        {
            bool check = false;

            if (user_master.USER_PWD.status.Equals("B"))
                check = true;

            return check;
        }
        public bool IsAccountInactive(USER_MASTER user_master)
        {
            bool check = false;

            if (user_master.USER_PWD.status.Equals("I"))
                check = true;

            return check;
        }
        public bool IsAccountPending(USER_MASTER user_master)
        {
            bool check = false;

            if (user_master.USER_PWD.status.Equals("P"))
                check = true;

            return check;
        }

        public bool UpdateFailedAttempts(USER_MASTER user_master)
        {
            bool check = false;
            var falled_attempts = user_master.USER_PWD.failed_attempts ?? 0;
            if (falled_attempts < 5)
            {
                user_master.USER_PWD.failed_attempts = falled_attempts + 1;
                user_master.USER_PWD.udate = DateTime.Now;
                UpdateUserMaster(user_master);
            }
            else if (falled_attempts >= 5 && user_master.USER_PWD.status != "B")
            {
                user_master.USER_PWD.status = "B";
                UpdateUserMaster(user_master);
                check = true;
            }
            else if (user_master.USER_PWD.status == "B")
                check = true;
            else
                check = false;

            return check;
        }

        public bool EmployeeEmailExists(string email)
        {
            var check = false;
            var u = _userMasterRepo.GetUserMasterByEmail(email);
            if (u != null)
                check = true;

            return check;
        }

        public bool UpdateUserMaster(USER_MASTER user) => _userMasterRepo.UpdateUserMaster(user) > 0;

        public bool EmployeeUsernameExists(string username)
        {
            var check = false;
            var u = _userMasterRepo.GetUserMasterByUsername(username);
            if (u != null)
                check = true;

            return check;
        }

        public bool IsValidEmailAddress(string emailaddress)
        {
            try
            {
                Regex rx = new Regex(
                    @"^[-!#$%&'*+/0-9=?A-Z^_a-z{|}~](\.?[-!#$%&'*+/0-9=?A-Z^_a-z{|}~])*@elflorido.com.mx$");
                var x = rx.IsMatch(emailaddress.ToLower());
                return x;
            }
            catch (FormatException)
            {
                return false;
            }
        }

        public bool DeleteUserMaster(USER_MASTER user)
        {
            var check = false;
            if (_userMasterRepo.DeleteUserMaster(user) > 0) check = true;
            return check;
        }

        public USER_MASTER GetUserMasterByUsernameOrEmpNo(string username) => _userMasterRepo.GetUserMasterByUsernameOrEmpNo(username);

        public USER_MASTER GetUserMasterByRecoveryPasswordCode(string code) => _userMasterRepo.GetUserMasterByRecoveryPasswordCode(code);

        public bool AddRecoveryCodePassword(USER_MASTER user)
        {
            user.USER_PWD.pass_code = RandomString(16);
            return UpdateUserMaster(user);
        }

        static readonly char[] allowedCharSet = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789-_.~".ToCharArray();

        string RandomString(int length)
        {
            if (length < 0) throw new ArgumentOutOfRangeException("length", "length cannot be less than zero.");
            const int byteSize = 0x100;

            // Guid.NewGuid and System.Random are not particularly random. By using a
            // cryptographically-secure random number generator, the caller is always
            // protected, regardless of use.
            using (var rng = System.Security.Cryptography.RandomNumberGenerator.Create())
            {
                var result = new StringBuilder();
                var buf = new byte[128];
                while (result.Length < length)
                {
                    rng.GetBytes(buf);
                    for (var i = 0; i < buf.Length && result.Length < length; ++i)
                    {
                        // Divide the byte into allowedCharSet-sized groups. If the
                        // random value falls into the last group and the last group is
                        // too small to choose from the entire allowedCharSet, ignore
                        // the value in order to avoid biasing the result.
                        var outOfRangeStart = byteSize - (byteSize % allowedCharSet.Length);
                        if (outOfRangeStart <= buf[i]) continue;
                        result.Append(allowedCharSet[buf[i] % allowedCharSet.Length]);
                    }
                }

                return result.ToString();
            }
        }

        public List<USER_MASTER> GetAllUsers() => _userMasterRepo.GetAllUsers();

        public List<UserMasterDrop> GetAllUsersByDrop() => _userMasterRepo.GetAllUsersByDrop();

        public string getCompleteName(string user_name) => _userMasterRepo.getCompleteName(user_name);

        public bool IsRolonUser(string user_name, string role) => _userMasterRepo.IsRolonUser(user_name, role);

        public List<string> GetUserRoles(string user_name) => _userMasterRepo.GetUserRoles(user_name);

        public bool DeletePicture(USER_MASTER model) => _userMasterRepo.DeletePicture(model);

        public string GetEmailByUserName(string user_name) => _userMasterRepo.getEmailByUserName(user_name);

        public List<SysPageMaster> GetPagesByUser(string emp_no) => _sysRoleUserRepository.GetPagesByUser(emp_no);

        public List<UserModelFirst> GetCashiersUsers() => _userMasterRepo.GetCashiersUsers();

        public List<UserModelFirst> GetCashiersChargeRelations() => _userMasterRepo.GetCashiersChargeRelations();

        public List<UserModelFirst> GetCashiersCancellationsSales() => _userMasterRepo.GetCashiersCancellationsSales();

        public List<UserModelFirst> GetCashiersDonations() => _userMasterRepo.GetCashiersDonations();

        public List<UserModelFirst> GetCashiersDonationsSales() => _userMasterRepo.GetCashiersDonationsSales();

        public List<UserModelFirst> GetCashierBySales() => _userMasterRepo.GetCashierBySales();

        public bool IsInRole(string user_name, string role_id) => _userMasterRepo.IsInRole(user_name, role_id);

        public string GetEmailByBuyerDivision(string buyer_division) => _userMasterRepo.GetEmailByBuyerDivision(buyer_division);
    }
}