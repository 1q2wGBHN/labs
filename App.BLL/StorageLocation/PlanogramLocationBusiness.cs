﻿using App.Common;
using App.DAL.Item;
using App.DAL.StorageLocation;
using App.DAL.Inventory;
using App.Entities;
using App.Entities.ViewModels.StorageLocation;
using System;
using System.Collections.Generic;
using System.Linq;

namespace App.BLL.StorageLocation
{
    public class PlanogramLocationBusiness
    {
        private readonly PlanogramLocationRepository _PlanogramLocationRepository;
        private readonly ItemRepository _itemRepository;
        private readonly InventoryHeader _inventoryHeaderRepository;

        public PlanogramLocationBusiness()
        {
            _PlanogramLocationRepository = new PlanogramLocationRepository();
            _itemRepository = new ItemRepository();
            _inventoryHeaderRepository = new InventoryHeader();
        }

        public List<PlanogramLocationModel> GetAllPlanogramLocations() => _PlanogramLocationRepository.GetAllPlanogramLocations();

        public bool AddPlanogramLocation(PlanogramLocationModel model)
        {
            PLANOGRAM_LOCATION location = new PLANOGRAM_LOCATION
            {
                planogram_location1 = model.planogram_location,
                part_number = model.part_number,
                bin1 = model.bin1,
                bin2 = model.bin2,
                bin3 = model.bin3,
                quantity = model.quantity,
                planogram_description = model.planogram_description,
                planogram_type = model.planogram_type,
                planogram_status = model.planogram_status,
                cdate = DateTime.Now,
                program_id = model.program_id
            };

            var flag = _PlanogramLocationRepository.AddPlanogramLocation(location);
            return flag > 0;
        }

        public Result<Empty> AddPlanogramLocationAndroid(PlanogramLocationModel model)
        {
            var item = _itemRepository.SearchCode(model.barcode, true, true, false, false);
            if (item == null || !item.Any()) return Result<Empty>.Error("No se encontro codigo");
            model.part_number = item.First().ItemBarcode.PartNumber;
            var s = _PlanogramLocationRepository.GetPlanogramLocationByLocationAndProduct(model.planogram_location,
                model.part_number);
            if (s != null) return Result<Empty>.Error("Este producto ya esta registrado en esta locación");
            var r = AddPlanogramLocation(model);
            return r ? Result<Empty>.OK() : Result<Empty>.Error();
        }

        public bool AddPlanogramLocationAndroid(PlanogramLocationModel model, string planogram_locationEdit, string itemProductEdit)
        {
            var flag = _PlanogramLocationRepository.UpdatePlanogramLocation(model, planogram_locationEdit, itemProductEdit);
            return flag > 0;
        }

        public bool UpdatePlanogramLocation(PlanogramLocationModel model, string planogram_locationEdit, string itemProductEdit)
        {
            var flag = _PlanogramLocationRepository.UpdatePlanogramLocation(model, planogram_locationEdit, itemProductEdit);
            return flag > 0;
        }

        public Result<Empty> UpdatePlanogramLocationAndroid(PlanogramLocationModel model, string planogramLocationOld, string itemProductOld)
        {
            var itemOld = _itemRepository.SearchCode(itemProductOld, true, true, false, false);
            if (itemOld == null || !itemOld.Any()) return Result<Empty>.Error("No se encontro codigo a cambiar");

            var item = _itemRepository.SearchCode(model.barcode, true, true, false, false);
            if (item == null || !item.Any()) return Result<Empty>.Error("No se encontro codigo nuevo"); ;
            model.part_number = item.First().ItemBarcode.PartNumber;
            var s = _PlanogramLocationRepository.GetPlanogramLocationByLocationAndProduct(model.planogram_location,
                model.part_number);
            if (s != null) return Result<Empty>.Error("Este producto ya esta registrado en esta locación");
            var r = UpdatePlanogramLocation(model, planogramLocationOld, itemOld.First().ItemBarcode.PartNumber);
            return r ? Result<Empty>.OK() : Result<Empty>.Error();
        }

        public string GetPlanogramByPartNumber(string part_number)
        {
            var planogramName = _PlanogramLocationRepository.getPlanogramByPartNumber(part_number);
            return planogramName.planogram_location;
        }

        public PLANOGRAM_LOCATION GetPlanogramLocationByLocationAndProduct(string planogram, string partNumber) => _PlanogramLocationRepository.GetPlanogramLocationByLocationAndProduct(planogram, partNumber);

        public List<string> PlanogramLocation1() => _PlanogramLocationRepository.PlanogramLocation1();

        public List<string> PlanogramLocation2(string loc1) => _PlanogramLocationRepository.PlanogramLocation2(loc1);

        public List<PlanogramLocationRepository.Location3> PlanogramLocation3(string loc1, string loc2) => _PlanogramLocationRepository.PlanogramLocation3(loc1, loc2);

        public PlanogramLocationRepository.Locations PlanogramLocations(string loc1, string loc2) => _PlanogramLocationRepository.PlanogramLocations(loc1, loc2);

        public PlanogramLocationModel GetLocation(string loc1, string loc2, string loc3, string part_number) => _PlanogramLocationRepository.GetLocation(loc1, loc2, loc3, part_number);

        public List<PlanogramLocationRepository.PlanogramTagViewModel> TagsWithFilter(PlanogramLocationRepository.PlanogramFilter filter) => _PlanogramLocationRepository.TagsWithFilter(filter);

        public List<PlanogramLocationModel> GetLocationsPlanogramByInventoryActive()
        {
            var id = _inventoryHeaderRepository.GetInventory(new[] { 1, 2, 3 }).FirstOrDefault();
            var r = new List<PlanogramLocationModel>();
            if (id != null)
                r = _PlanogramLocationRepository.GetLocationsPlanogramByInventoryActive(id.id_inventory_header);

            return r;
        }
        public List<PlanogramLocationModel> GetLocationsPlanogramByInventory(string id)
        {
               var r = _PlanogramLocationRepository.GetLocationsPlanogramByInventoryActive(id);

            return r;
        }
    }
}