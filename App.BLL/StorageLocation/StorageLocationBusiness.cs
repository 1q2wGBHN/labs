﻿using App.DAL.Site;
using App.DAL.StorageLocation;
using App.Entities;
using App.Entities.ViewModels.StorageLocation;
using System;
using System.Collections.Generic;

namespace App.BLL.StorageLocation
{
    public class StorageLocationBusiness
    {
        private readonly StorageLocationRepository _storageLocationRepository;
        private readonly SiteConfigRepository _siteConfigRepository;

        public StorageLocationBusiness()
        {
            _storageLocationRepository = new StorageLocationRepository();
            _siteConfigRepository = new SiteConfigRepository();
        }

        public List<StorageLocationModel> GetAllLocationsInStatusActive() => _storageLocationRepository.GetAllLocationsInStatusActive();

        public List<StorageLocationModel> GetAllLocations() => _storageLocationRepository.GetAllLocations();

        public StorageLocationModel GetLocationById(string storageId) => _storageLocationRepository.GetLocationById(storageId);

        public STORAGE_LOCATION GetLocationByNameAndSite(string storageId, string site) => _storageLocationRepository.GetLocationByNameAndSite(storageId, site);

        public bool AddLocation(StorageLocationModel model)
        {
            var check = false;
            STORAGE_LOCATION storage = new STORAGE_LOCATION
            {
                site_code = _siteConfigRepository.GetSiteCode(),
                storage_location_name = model.storage_location,
                bin1 = "01",
                bin2 = "01",
                bin3 = "MR",
                storage_type = model.storage_type,
                mrp_ind = false,
                active_flag = model.active_flag,
                cdate = DateTime.Now,
                cuser = model.cuser,
                program_id = "STGL001.cshtml"
            };
            var flag = _storageLocationRepository.AddLocation(storage);
            if (flag > 0)
                check = true;

            return check;
        }

        public bool UpdateLocation(STORAGE_LOCATION location, string type, bool flag)
        {
            var check = false;
            location.active_flag = flag;
            location.storage_type = type;

            location.udate = DateTime.Now;
            var flagg = _storageLocationRepository.UpdateLocation(location);
            if (flagg > 0)
                check = true;

            return check;
        }

        public List<StorageLocationModel> GetLocationsByType(string Type, string Site) => _storageLocationRepository.GetLocationsByType(Type, Site);

        public StorageLocationModel GetLocationaMain(string storage_type, string site_code) => _storageLocationRepository.GetLocationaMain(storage_type, site_code);
    }
}