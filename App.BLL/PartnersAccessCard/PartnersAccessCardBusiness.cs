﻿using System.Linq;
using App.DAL.PartnersAccessCard;
using App.Entities;

namespace App.BLL.PartnersAccessCard
{
    public class PartnersAccessCardBusiness
    {
        private PartnersAccessCardRepository _partnersAccessCardRepository;

        public PartnersAccessCardBusiness()
        {
            _partnersAccessCardRepository=new PartnersAccessCardRepository();
        }
        public IQueryable<DFLPOS_USER_CARD_ACCESS> GetAll()
        {
            return _partnersAccessCardRepository.GetAll();
        }

        // GET: DFLPOS_USER_CARD_ACCESS/Details/5
        public DFLPOS_USER_CARD_ACCESS Details(int? id)
        {
            return _partnersAccessCardRepository.Details(id);
        }


        public DFLPOS_USER_CARD_ACCESS Create(DFLPOS_USER_CARD_ACCESS partnersCardAccess)
        {
            return _partnersAccessCardRepository.Create(partnersCardAccess);
        }




        public DFLPOS_USER_CARD_ACCESS Edit(DFLPOS_USER_CARD_ACCESS partnersCardAccess)
        {
            return _partnersAccessCardRepository.Edit(partnersCardAccess);
        }



        // POST: DFLPOS_USER_CARD_ACCESS/Delete/5
        public void Delete(int id)
        {
            _partnersAccessCardRepository.Delete(id);
        }
    }
}
