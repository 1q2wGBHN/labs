﻿using App.Common;
using App.DAL.Item;
using App.DAL.PriceTagPrints;
using App.Entities.ViewModels.PriceTag;
using System.Collections.Generic;

namespace App.BLL.PriceTagPrints
{
    public class ExistItem { public bool Exist { get; set; } }

    public class MinItemModel
    {
        public string PartNumber { get; set; }
        public string Description { get; set; }
        public decimal SalePrice { get; set; }
    }

    public class PriceTagPrintsBusiness
    {
        private readonly PriceTagPrintsRepository _priceTagPrintsRepository;
        private readonly ItemRepository _itemRepository;

        public PriceTagPrintsBusiness()
        {
            _priceTagPrintsRepository = new PriceTagPrintsRepository();
            _itemRepository = new ItemRepository();
        }

        public Result<ExistItem> ItemExist(string PartNumber) => Result.OK(new ExistItem { Exist = _itemRepository.GetOneItem(PartNumber) != null });

        public Result<MinItemModel> ItemById(string PartNumber)
        {
            var r = _itemRepository.GetOneItem(PartNumber);
            if (r == null) return Result.Error("El codigo no fue encontrado");
            var m = new MinItemModel { PartNumber = r.PathNumber, Description = r.Description, SalePrice = r.SalePrice };
            return Result.OK(m);
        }

        public Result<Empty> CreatePriceTagPrint(List<PrintTagReq> products, string username, string program_id, string name, bool fav)
        {
            var r = _priceTagPrintsRepository.CreatePriceTagPrint(products, username, program_id, name, fav);
            return r ? Result.OK() : Result.Error();
        }

        public Result<List<PriceTagPrintModel>> GetPriceTagList(PrintOrderFilter filter) => Result.OK(_priceTagPrintsRepository.GetList(filter));

        public Result<List<PriceTagPrintDetailsModel>> GetTagList(int printId) => Result.OK(_priceTagPrintsRepository.GetTagList(printId));

        public Result<Empty> SetAsPrinted(int id)
        {
            var r = _priceTagPrintsRepository.SetAsPrinted(id);
            return r ? Result.OK() : Result.Error();
        }

        public Result<Empty> SetAsFav(int id,bool fav)
        {
            var r = _priceTagPrintsRepository.SetAsFav(id,fav);
            return r ? Result.OK() : Result.Error();
        }

        public Result<Empty> AddToList(int id, List<PrintTagReq> list)
        {
            var r = _priceTagPrintsRepository.AddToList(id, list);
            return r ? Result.OK() : Result.Error();
        }
        public Result<Empty> RemoveFromList(int id, int sequence_num)
        {
            var r = _priceTagPrintsRepository.RemoveFromList(id, sequence_num);
            return r ? Result.OK() : Result.Error();
        }
        public Result<Empty> EditList(int id, PriceTagPrintModel model, string userName)
        {
            var r = _priceTagPrintsRepository.EditTagList(id, model,userName);
            return r ? Result.OK() : Result.Error();
        }
        public Result<Empty> HistoryCreate(HistoryModel model)
        {
            var r = _priceTagPrintsRepository.HistoryCreate(model);
            return r ? Result.OK() : Result.Error();
        }

        public Result<List<HistoryModel>> HistoryList(HistoryFilter filter) => Result.OK(_priceTagPrintsRepository.HistoryList(filter));
    }
}