﻿using App.BLL.Site;
using App.DAL.CostCenter;
using App.Entities.ViewModels.CostCenter;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;

namespace App.BLL.CostCenter
{
    public class CostCenterBusiness
    {
        private readonly CostCenterRepository _CostCenterRepository;
        private readonly SiteConfigBusiness _SiteConfigBusiness;

        public CostCenterBusiness()
        {
            _CostCenterRepository = new CostCenterRepository();
            _SiteConfigBusiness = new SiteConfigBusiness();
        }

        public int GetCostCenterPrintStatus(string Document) => _CostCenterRepository.GetCostCenterPrintStatus(Document);

        public bool GetCostCenterUpdatePrintStatus(string Document, int print_status, string user) => _CostCenterRepository.GetCostCenterUpdatePrintStatus(Document, print_status, user);

        public List<GICostCenterItem> GetCostCenterById(int id) => _CostCenterRepository.GetCostCenterListById(id);

        public List<GICostCenterItem> GetCostCenterDetailsById(int id) => _CostCenterRepository.GetCostCenterDetailsById(id);

        public List<GICostCenterItem> GetCostCenterDetailsByDates(DateTime BeginDate, DateTime EndDate) => _CostCenterRepository.GetCostCenterDetailsByDates(BeginDate, EndDate);

        public List<GICostCenter> GetCostCenterRecords(DateTime BeginDate, DateTime EndDate) => _CostCenterRepository.GetCostCenterRecords(BeginDate, EndDate);

        public List<GICostCenter> GetCostCenterRecords(DateTime BeginDate, DateTime EndDate, int Status) => _CostCenterRepository.GetCostCenterRecords(BeginDate, EndDate, Status);

        public object GetCostCenterRecordsDetail(DateTime BeginDate, DateTime EndDate) => _CostCenterRepository.GetCostCenterRecordsDetail(BeginDate, EndDate);

        public int CreateCostCenter(string CostCenter, int Status, string User, List<GICostCenterItem> Items, string program_id) => _CostCenterRepository.CreateCostCenter(_SiteConfigBusiness.GetSiteCode(), CostCenter, Status, User, program_id, Items);

        public bool UpdateCostCenter(int id, string CostCenter, int Status, string User, List<GICostCenterItem> Items, string program_id) => _CostCenterRepository.UpdateCostCenter(id, CostCenter, Status, User, program_id, Items);

        public bool ProcessCostCenter(string CostCenter, int Status, string User, List<GICostCenterItem> Items, string program_id)
        {
            if (_CostCenterRepository.CreateCostCenter(_SiteConfigBusiness.GetSiteCode(), CostCenter, Status, User, program_id, Items) > 0)
                return true;
            else
                return false;
        }

        public bool UpdateProcessCostCenter(int id, string CostCenter, int Status, string User, List<GICostCenterItem> Items, string program_id) => _CostCenterRepository.UpdateCostCenter(id, CostCenter, Status, User, program_id, Items);

        public bool CancelCostCenter(int CostCenterId, string User) => _CostCenterRepository.CancelCostCenter(CostCenterId, User);

        public GICostCenter GetGICostCenterReport(string GiDocument)
        {
            var model = _CostCenterRepository.GetCostCenterByGiDocument(GiDocument);
            if (model != null)
                model.Items = _CostCenterRepository.GetCostCenterItemsByGiDocument(GiDocument);

            return model;
        }

        public bool AproveProcessCostCenter(int CostCenterId, List<GICostCenterItem> Items, string User, out string GiDocument)
        {
            GiDocument = "";
            if (Items != null)
            {
                if (_CostCenterRepository.UpdateCostCenter(CostCenterId, _CostCenterRepository.GetCostCenterById(CostCenterId), 1, User, "COST002.cshtml", Items))
                    return _CostCenterRepository.ProcessCenter(CostCenterId, User, "COST002.cshtml", out GiDocument);
                else
                    return false;
            }
            else
                return _CostCenterRepository.ProcessCenter(CostCenterId, User, "COST002.cshtml", out GiDocument);
        }

        public bool GetProcessByAndroid(string items, string costCenter, int Status, string user)
        {
            string v = "{\"Items\":" + items + "}";
            var result = JsonConvert.DeserializeObject<GICostCenter>(v);
            GICostCenter model = new GICostCenter();
            for (int i = 0; i < result.Items.Count; i++)
            {
                GICostCenterItem item = new GICostCenterItem
                {
                    PartNumber = result.Items[i].PartNumber,
                    Quantity = result.Items[i].Quantity
                };
                model.Items.Add(item);
            }

            return ProcessCostCenter(costCenter, Status, user, model.Items, "Android CostCenter");
        }
    }
}