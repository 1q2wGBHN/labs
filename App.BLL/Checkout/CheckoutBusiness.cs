﻿using App.DAL.Checkout;
using App.Entities.ViewModels.Checkout;
using System.Linq;
using static App.Entities.ViewModels.Common.Constants;

namespace App.BLL.Checkout
{
    public class CheckoutBusiness
    {
        private CheckoutRepository _CheckoutRepository;

        public CheckoutBusiness()
        {
            _CheckoutRepository = new CheckoutRepository();
        }

        public CheckoutDataList GetZCutReportData(string EmployeeNo, string Date)
        {
            var DataInfo = _CheckoutRepository.GetZCutReportData(EmployeeNo, Date);
            decimal TotalChargeUSD = 0, TotalReturnUSD = 0, TotalWithdrawalUSD = 0, TotalDonationsUSD = 0, ExchangeRate = 0;

            ExchangeRate = DataInfo.ExchangeRate.Select(c => c.cur_rate1).First();

            TotalChargeUSD = DataInfo.Charge.Where(c => c.currency == Currencies.USD).Sum(d=>d.Monto);
            DataInfo.TotalCharge = DataInfo.Charge.Where(c => c.currency != Currencies.USD).Sum(d => d.Monto) + (TotalChargeUSD * ExchangeRate);

            TotalReturnUSD = DataInfo.Returns.Where(c => c.currency == Currencies.USD).Sum(d => d.Monto);
            DataInfo.TotalReturns = DataInfo.Returns.Where(c => c.currency != Currencies.USD).Sum(d => d.Monto) + (TotalReturnUSD * ExchangeRate);

            TotalWithdrawalUSD = DataInfo.Withdrawal.Where(c => c.currency == Currencies.USD && c.pm_id != PaymentMethods.COMPENSATION).Sum(d => d.Retiro);
            DataInfo.TotalWithdrawal = DataInfo.Withdrawal.Where(c => c.currency != Currencies.USD && c.pm_id != PaymentMethods.COMPENSATION).Sum(d => d.Retiro) + (TotalWithdrawalUSD * ExchangeRate);

            TotalDonationsUSD = DataInfo.Donations.Where(c => c.currency == Currencies.USD && c.pm_id != PaymentMethods.COMPENSATION).Sum(d => d.Donativos);
            DataInfo.TotalDonations = DataInfo.Donations.Where(c => c.currency != Currencies.USD && c.pm_id != PaymentMethods.COMPENSATION).Sum(d => d.Donativos) + (TotalDonationsUSD * ExchangeRate);

            DataInfo.Balance = (DataInfo.TotalCharge + DataInfo.TotalDonations) - (DataInfo.TotalWithdrawal + DataInfo.TotalReturns);

            return DataInfo;
        }
    }
}
