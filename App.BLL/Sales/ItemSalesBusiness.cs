﻿using App.DAL.Sales;
using App.Entities.ViewModels.Sales;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App.BLL.Sales
{
    public class ItemSalesBusiness
    {
        private readonly ItemSalesRepository _ItemSalesRepository;

        public ItemSalesBusiness()
        {
            _ItemSalesRepository = new ItemSalesRepository();
        }

        public List<ItemSalesModel> GetItemSalesByCashier(DateTime BeginDate, DateTime EndDate, string PartNumber)
        {
            return _ItemSalesRepository.GetItemSalesByCashier(BeginDate, EndDate, PartNumber);
        }
    }
}
