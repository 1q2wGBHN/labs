﻿using App.DAL.Sales;
using App.Entities.ViewModels.Sales;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App.BLL.Sales
{
    public class SalesCancellationBusiness
    {
        private SalesCancellationRepository _SalesCancellationRepository;

        public SalesCancellationBusiness()
        {
            _SalesCancellationRepository = new SalesCancellationRepository();
        }

        public List<SalesCancellationData> GetSalesCancellationReportData(SalesReportIndex Model)
        {
            return _SalesCancellationRepository.GetSalesCancellationReportData(Model);
        }
        public List<CancellationResume> GetSalesCancellationResume(CancellationIndex Model)
        {
            return _SalesCancellationRepository.GetCancellationsResumen(Model);
        }
        public List<SalesCancellationData> GetSalesCancellationReporByDatesAndCashier(string date1, string date2, string cashierID)
        {
            return _SalesCancellationRepository.GetSalesCancellationReportDataByDatesAandCashier( date1,  date2,  cashierID);
        }

    }
}
