﻿using App.BLL.Site;
using App.DAL.Sales;
using App.Entities.ViewModels.Item;
using App.Entities.ViewModels.SalesPriceChange;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace App.BLL.Sales
{
    public class SalesPriceChangeBusiness
    {
        private readonly SalesPriceChangeRepository _SalesPriceChangeRepository;
        //private readonly Uri uriAddress = new Uri("file://10.11.0.15/XML_PDF_PROVEEDORES_PRUEBAS/nopuedo");
        //private readonly Uri uriAddress = new Uri("file://10.10.7.15/TOLEDO");
        private readonly SiteConfigBusiness _siteConfigBusiness;

        public SalesPriceChangeBusiness()
        {
            _SalesPriceChangeRepository = new SalesPriceChangeRepository();
            _siteConfigBusiness = new SiteConfigBusiness();
        }

        public List<SalePriceChangeModel> GetAllSalesPricesChangeByParameters(string fecha, string depto, string familia, string existencias)
        {
            if (existencias == "" && familia == "" && depto == "" && fecha != "")
            {
                return _SalesPriceChangeRepository.GetAllSalesPricesChangeByDate(DateTime.Parse(fecha));
            }
            else if (existencias != "" && familia == "" && depto == "" && fecha != "")
            {
                return _SalesPriceChangeRepository.GetAllSalesPricesChangeRemoveExist0AndDate(DateTime.Parse(fecha));
            }
            else if (existencias != "" && familia == "" && depto != "" && fecha != "")
            {
                return _SalesPriceChangeRepository.GetAllSalesPricesChangeRemoveExist0AndDeptoAndDate(DateTime.Parse(fecha), Int32.Parse(depto));
            }
            else if (existencias != "" && familia != "" && depto != "" && fecha != "")
            {
                return _SalesPriceChangeRepository.GetAllSalesPricesChangeRemoveExist0AndDeptoAndFamilyAndDate(DateTime.Parse(fecha), Int32.Parse(depto), Int32.Parse(familia));
            }
            else if (existencias == "" && familia == "" && depto != "" && fecha != "")
            {
                return _SalesPriceChangeRepository.GetAllSalesPricesChangeByDeptoAndDate(DateTime.Parse(fecha), Int32.Parse(depto));
            }
            else if (existencias == "" && familia != "" && depto != "" && fecha != "")
            {
                return _SalesPriceChangeRepository.GetAllSalesPricesChangeByDeptoAndFamilyAndDate(DateTime.Parse(fecha), Int32.Parse(depto), Int32.Parse(familia));
            }
            else
            {
                List<SalePriceChangeModel> list = new List<SalePriceChangeModel>();
                return list;
            }
        }

        public List<SalePriceChangeModel> GetAllSalesPricesWithFilter(SalePriceFilter filter)
        {
            if (filter.promotions == false)
            {
                return _SalesPriceChangeRepository.GetAllSalesPricesWithFilterSimple(filter);
            }
            if (filter.promotions == true)
            {
                return _SalesPriceChangeRepository.GetAllSalesPricesWithFilterProm(filter);
            }

            return new List<SalePriceChangeModel>();
        }

        public List<SalesPriceChangeBasculeModel> GetAllSalesPricesForBasculeToledoBPlus()
        {
            return _SalesPriceChangeRepository.GetAllSalesPricesForBasculeToledoBPlus();
        }

        public string FileCreate(int type)
        {
            List<SalesPriceChangeBasculeModel> TableList = new List<SalesPriceChangeBasculeModel>();
            if (type == 1)
                TableList = _SalesPriceChangeRepository.GetAllSalesPricesForBasculeCas();
            else if (type == 2)
                TableList = _SalesPriceChangeRepository.GetAllSalesPricesForBasculeToledo();
            else
                TableList = _SalesPriceChangeRepository.GetAllSalesPricesForBasculeToledoBPlus();
            string returnData;
            var infoSite = _siteConfigBusiness.GetSiteConfigInfo();
            string departamentoStr = "1";
            if (infoSite.Site == "0003") //Tecate Pedregal usa el departamento 2
                departamentoStr = "2";
            if (TableList.Count > 0)
            {
                try
                {
                    var txtTable = "";
                    string itemsTable = "";
                    string line = "";
                    string new_line_zero = "";
                    string new_line_one = "";
                    string new_line_two = "";
                    string new_line_three = "";
                    string FolderName = "";
                    if (type == 1)
                    {
                        //Bascula CAS
                        returnData = @"<table><tr><th>1.Departamento No</th><th>2.PLU No</th><th>4.PLU Tipo</th><th>11.Codigart</th><th>10.Nombre</th><th>30.Nombre2</th><th>31.Nombre3</th><th>9.Grupo No</th><th>80.Etiqueta No</th>
                        <th>81.Etiqueta Aux No</th><th>55.Origen No</th><th>5.Peso Unit</th><th>100.Pesofijo</th><th>3.refijo</th><th>14.Piezas</th><th>15.Cant unit No</th><th>26.Usar tipo precio prefijo</th><th>6.Precio</th>
                        <th>91.PrecioEspecial</th><th>8.Iva No</th><th>13.Tara</th><th>12.Tara No</th><th>24.%Tara</th><th>23.% limite tara</th><th>85.Barcode ID</th><th>86.Barcode ID2</th><th>20.FechaPreparacion</th><th>18.Fecha Empaque</th>
                        <th>19.Tiempo Empaque</th><th>16.Fecha de venta</th><th>17.Hora de venta</th><th>22.Fecha de Elab</th><th>25.Ingrediente No</th><th>35.Traceability No</th><th>50.Bonos</th><th>70.Infonutric No</th>
                        <th>90.LabelMsg No</th><th>71.Referencia Dept</th><th>69.Referencia PLU</th><th>64.Dept Unido</th><th>68.PLU Unido</th><th>60.# de PLU ligado</th><th>61.Ligado Dept1</th><th>65.Ligado PLU1</th><th>62.Ligado Dept2</th>
                        <th>66.Ligado PLU2</th></tr>";

                        itemsTable = "";
                        foreach (var item in TableList)
                        {

                            //Dar formato para que lo acepte la bascula
                            var priceReal = item.Price.ToString();
                            priceReal = priceReal.Remove(priceReal.Length - 2);
                            priceReal = priceReal.Replace(".", "");
                            string row = @"<tr><td>"+ departamentoStr + "</td><td>" + item.part_number + "</td><td>1</td><td>" + item.part_number + "</td><td>" + item.part_description + "</td><td></td><td></td><td>0</td><td>0</td><td>0</td>" +
                            "<td>0</td><td>1</td><td>0</td><td></td><td>1</td><td>0</td><td>0</td><td>" + priceReal + "</td><td>0</td><td></td><td>0</td><td></td><td></td><td></td><td>" + item.part_number + "</td>" +
                            "<td></td><td></td><td>0</td><td></td><td>" + item.part_caducidad + "</td><td>0</td><td>0</td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td></tr>";
                            itemsTable += row;
                        }
                        return returnData + itemsTable + "</table>";
                    }
                    else if (type == 2)
                    {
                        //Bascula TOLEDO
                        txtTable = @":201111221157PD  YMANTENI MANT.BAS" + Environment.NewLine;
                        FolderName = "TOLEDO";
                        foreach (var item in TableList)
                        {
                            line = "AI0000";
                            string PLU = item.part_number;
                            //if (PLU.Length > 10)
                            //break;
                            for (int i = 0; PLU.Length < 10; i++)
                            {
                                PLU = "0" + PLU;
                            }
                            string Department = "0000";
                            string Gruop = "000";
                            decimal iva = Convert.ToDecimal(0.16); //item.iva;
                                                                   //string Price = Convert.ToInt32(Convert.ToDecimal(item.sale_price) * 100 + Convert.ToDecimal(item.sale_price * iva)).ToString();
                            string Price = Convert.ToInt32(Convert.ToDecimal(item.Price) * 100).ToString();
                            for (int i = 0; Price.Length < 6; i++)
                            {
                                Price = "0" + Price;
                            }
                            string Price_Modifier = "000";
                            string Unused = "00";
                            string Package_Code = "";
                            if (item.Weight_flag == 1)
                                Package_Code = "01";
                            else
                                Package_Code = "00";
                            string Grade = "00";
                            string SHELFLIFE = "0000";
                            string EatBy = Convert.ToString(item.part_caducidad);
                            for (int i = 0; EatBy.Length < 3; i++)
                            {
                                EatBy = "0" + EatBy;
                            }
                            string Tare = "00000";
                            string ClassNumber = "000";
                            string Spare = "00";
                            string ActionCode = "00000";
                            string RecordStatus = "00";
                            string NewPrice = "000000";
                            string EffectiveDate = "0000";
                            string EffectiveTime = "00";
                            string TareAlternate = "00000";
                            string SpecialCodeNumber = "0001";
                            string ControlFlags = "00";
                            string PrintFieldFlags = "000";
                            string BarCodeType = "01";
                            string filler = "0";
                            for (int i = 0; filler.Length < 74; i++)
                            {
                                filler = "0" + filler;
                            }
                            string PLUDescription = item.part_description;
                            line = line + PLU + Department + PLU + Gruop + Price + Price_Modifier + Unused + Package_Code + Grade + SHELFLIFE + EatBy + Tare + ClassNumber + Spare + ActionCode + RecordStatus + NewPrice + EffectiveDate +
                            EffectiveTime + TareAlternate + SpecialCodeNumber + ControlFlags + PrintFieldFlags + BarCodeType + filler + PLUDescription;
                            itemsTable = itemsTable + line + Environment.NewLine;

                        }
                        txtTable += itemsTable;
                    }
                    else
                    {
                        //Bascula Toledo B Plus
                        FolderName = "MT";
                        foreach (var item in TableList)
                        {
                            line = "AI0000000000";
                            string part_number = item.part_number;
                            string department = "00" + item.department + "000000"; //06-panaderia//22-carniceria
                            string expiration_days = item.expiration_days;
                            string Price = Convert.ToInt32(Convert.ToDecimal(item.Price) * 100).ToString();
                            for (int i = 0; Price.Length < 9; i++)
                            {
                                Price = "0" + Price;
                            }
                            if (item.department == "06") //panaderia
                            {
                                new_line_one = "00000000000000";
                                new_line_two = "00000000000000000000000000000000000004000009900000000000000000000000000000000000000000000000000000000000000000000000000";
                            }
                            else //carniceria
                            {
                                new_line_one = "00100010000000";
                                new_line_two = "00000000000000000000000000000000000003000009900000000000000000000000000000000000000000000000000000000000000000000000000";
                            }
                            string description = item.part_description + "^A";
                            line = line + part_number + department + part_number + Price + new_line_one + expiration_days + new_line_two + description;
                            itemsTable = itemsTable + line + Environment.NewLine;
                        }
                        txtTable += itemsTable;

                    }
                    Uri uriAddress = new Uri("file://" + infoSite.IP + "/" + FolderName);
                    var byteArray = Encoding.ASCII.GetBytes(txtTable);
                    var stream = new MemoryStream(byteArray);
                    string PathFolder = "\\\\" + infoSite.IP + "\\" + FolderName;

                    //MapDrive("O", toledo, "Administrator", "L8:+gN@L3#\"AsvKA"); //No funciono en Santa Fe
                    MapDrive("O", PathFolder, infoSite.User, infoSite.pwd);
                    string UploadDirectoryFlorido = uriAddress.LocalPath;

                    if (!Directory.Exists(UploadDirectoryFlorido))
                    {
                        Directory.CreateDirectory(UploadDirectoryFlorido);
                    }
                    UploadDirectoryFlorido += "\\EAMBASCU.TST";
                    if (File.Exists(UploadDirectoryFlorido))
                    {
                        File.Delete(UploadDirectoryFlorido);
                    }
                    using (var fileStream = new FileStream(UploadDirectoryFlorido, FileMode.CreateNew, FileAccess.ReadWrite))
                    {
                        stream.CopyTo(fileStream); // fileStream is not populated
                    }
                    stream.Close();
                    DisconnectDrive("O");
                    return returnData = "Archivo generado con exito!";

                }
                catch (Exception ex)
                {
                    return ex.Message;
                }
            }
            else
                return "No se encontraron datos!";
        }

        public void CopyStream(Stream stream, string destPath)
        {
            using (var fileStream = new FileStream(destPath, FileMode.Create, FileAccess.Write))
            {
                stream.CopyTo(fileStream);
            }
        }

        public bool MapDrive(string DriveLetter, string Path, string Username, string Password)
        {
            bool ReturnValue = false;
            try
            {
                if (System.IO.Directory.Exists(DriveLetter + ":\\"))
                { DisconnectDrive(DriveLetter); }
                System.Diagnostics.Process p = new System.Diagnostics.Process();
                p.StartInfo.UseShellExecute = false;
                p.StartInfo.CreateNoWindow = true;
                p.StartInfo.RedirectStandardError = true;
                p.StartInfo.RedirectStandardOutput = true;

                //Colocar en CMD
                //net use O: \\10.11.4.15\nopuedo /user:Administrator 1q2wjmk,
                p.StartInfo.FileName = "net.exe";
                p.StartInfo.Arguments = " use " + DriveLetter + ": " + Path + " " + Password + " /user:" + Username;
                p.Start();
                p.WaitForExit();

                string ErrorMessage = p.StandardError.ReadToEnd();
                string OuputMessage = p.StandardOutput.ReadToEnd();
                if (ErrorMessage.Length > 0)
                { throw new Exception("Error:" + ErrorMessage); }
                else
                { ReturnValue = true; }
            }
            catch (Exception exp)
            { string sError = exp.Message; }
            return ReturnValue;
        }

        public bool DisconnectDrive(string DriveLetter)
        {
            bool ReturnValue = false;
            try
            {
                System.Diagnostics.Process p = new System.Diagnostics.Process();
                p.StartInfo.UseShellExecute = false;
                p.StartInfo.CreateNoWindow = true;
                p.StartInfo.RedirectStandardError = true;
                p.StartInfo.RedirectStandardOutput = true;

                p.StartInfo.FileName = "net.exe";
                p.StartInfo.Arguments = " use " + DriveLetter + ": /DELETE";
                p.Start();
                p.WaitForExit();

                string ErrorMessage = p.StandardError.ReadToEnd();
                string OuputMessage = p.StandardOutput.ReadToEnd();
                if (ErrorMessage.Length > 0)
                { throw new Exception("Error:" + ErrorMessage); }
                else
                { ReturnValue = true; }
            }
            catch (Exception exp)
            { string sError = exp.Message; }
            return ReturnValue;
        }

        public List<SalesPriceChangeDataReport> GetSalesPriceChangeReport(SalePriceChangeIndex Model) => _SalesPriceChangeRepository.GetSalesPriceChangeReport(Model);

        public int AddItemSalesPriceChange(string partNumber, decimal margin, decimal priceUser, string Sesion, string ProgramId) => _SalesPriceChangeRepository.addItemSalesPriceChange(partNumber, margin, priceUser, Sesion, ProgramId);

        public int ChangeSales() => _SalesPriceChangeRepository.ChangeSales();

        public List<ItemSupplierModelPrice> GetItemSaleHistory(string part_number) => _SalesPriceChangeRepository.GetItemSaleHistory(part_number);

        public List<ItemSupplierModelPrice> GetItemSaleForceHistory(string part_number) => _SalesPriceChangeRepository.GetItemSaleForceHistory(part_number);

        public List<ItemCurrent> GetItemPriceSaleCurrent(string department, string family, string user) => _SalesPriceChangeRepository.GetItemPriceSaleCurrent(department, family, user);

        public ItemCurrent GetSingleItemPrice(string part_number) => _SalesPriceChangeRepository.GetSingleItemPrice(part_number);

        public List<ItemCurrent> GetPresentationPriceByPartNumber(string part_number) => _SalesPriceChangeRepository.GetPresentationPriceByPartNumber(part_number);
    }
}