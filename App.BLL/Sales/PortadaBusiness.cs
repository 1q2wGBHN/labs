﻿using App.BLL.Site;
using App.DAL.Currency;
using App.DAL.Sales;
using App.Entities;
using App.Entities.ViewModels.CreditNotes;
using App.Entities.ViewModels.Donations;
using App.Entities.ViewModels.Sales;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App.BLL.Sales
{
    public class PortadaBusiness
    {
        private PortadaRepository _PortadaRepository;
        private CurrencyRepository _CurrencyRepository;
        private SiteConfigBusiness _SiteConfigBusiness;

        public PortadaBusiness()
        {
            _PortadaRepository = new PortadaRepository();
            _CurrencyRepository = new CurrencyRepository();
            _SiteConfigBusiness = new SiteConfigBusiness();
        }

        public PortadaModel PopulatePortadaModel(string Datee)
        {
            var model = new PortadaModel();
            var CurrencyRate = _CurrencyRepository.GetCurrencyRateByDate(Datee);
            model.ReportDate = Datee;

            model.TotalesDLLS = _PortadaRepository.GetTotals(Datee, "USD");// new decimal[6, 3];
            model.TotalesMXN = _PortadaRepository.GetTotals(model.ReportDate, "MXN"); //new decimal[6, 3];

            model.totalTasao = ((model.TotalesDLLS[0, 2] * CurrencyRate) + model.TotalesMXN[0, 2]);
            model.totalGravado0 = ((model.TotalesDLLS[1, 2] * CurrencyRate) + model.TotalesMXN[1, 2]);
            model.totalExento = ((model.TotalesDLLS[2, 2] * CurrencyRate) + model.TotalesMXN[2, 2]);
            model.totalIEPS0 = ((model.TotalesDLLS[3, 2] * CurrencyRate) + model.TotalesMXN[3, 2]);
            model.totalIEPS = ((model.TotalesDLLS[4, 2] * CurrencyRate) + model.TotalesMXN[4, 2]);
            model.totalIVA = ((model.TotalesDLLS[6, 2] * CurrencyRate) + model.TotalesMXN[5, 2]);
            model.totalTOTAL = ((model.TotalesDLLS[6, 2] * CurrencyRate) + model.TotalesMXN[6, 2]) ;

            model.Deposits = _PortadaRepository.GetDepositsLists(model.ReportDate);// new List<Deposits>();
            model.Currency_Rate = CurrencyRate;
            decimal _CreditNoteMXN = 0;
            decimal _CreditNoteDLLS = 0;
            decimal _BoniMXN = 0;
            decimal _BoniDLLS = 0;
           // decimal _cancTarjetas_0=0;
            foreach (CreditNotesBon crnb in _PortadaRepository.GetCreditNotesBoniList(Datee))
            {
                if (crnb.Type == "BONIFICACION")
                {
                    if (!crnb.Iscredit)
                    {
                        if (crnb.Currency == "MXN")
                          _BoniMXN += crnb.Total;
                        else
                            _BoniDLLS += crnb.Total;
                    }
                    if (crnb.Currency == "MXN")
                        model.BONIMXN += crnb.Total;
                    else
                        model.BONIDLLS += crnb.Total;
                }
                else
                {
                    if(!crnb.Iscredit)
                    {
                        if (crnb.Currency == "MXN")
                            _CreditNoteMXN += crnb.Total;
                        else
                            _CreditNoteDLLS += crnb.Total;
                    }
                    if (crnb.Currency == "MXN")
                        model.CreditNotesMXN += crnb.Total;
                    else
                        model.CreditNotesDLLS += crnb.Total;
                }
            }


            foreach (PAYMENTS pay in _PortadaRepository.GetPayments(Datee))
            {
                if (pay.cur_code == "MXN")
                {
                    model.PAYSCUSTMXN += pay.payment_total.Value;
                }
                else
                    model.PAYSCUSTDLLS += pay.payment_total.Value;
            }
            model.PAYSCUSTDLLSMXN = model.PAYSCUSTDLLS * CurrencyRate;
            model.PAYSCUSTTOTAL = model.PAYSCUSTMXN + model.PAYSCUSTDLLSMXN;

            decimal _cancenlationsCardD = 0;
            decimal _cancenlationsCardC = 0;
            decimal _cancenlationsCardTotal = 0;

            foreach (Cancellations canc in _PortadaRepository.GetCancellations(Datee))
            {
                if (canc.Type == "C")
                    model.CANC_CASH += canc.Total;
                else
                    model.CANC_ELECT += canc.Total;
                if (canc.CardC)
                    _cancenlationsCardC += canc.Total;
                if (canc.CardD)
                    _cancenlationsCardD += canc.Total;


            }
            
            model.Sanctions = _PortadaRepository.GetSantionsList(Datee);
            model.PaymentsMethodsList = _PortadaRepository.GetPaymentMethods(Datee);// new List<PaymentMethod>();
            
            foreach(PaymentMethod pm in model.PaymentsMethodsList)
            {
                if (pm.Typev == "V")
                    model.VALESDeposits += pm.Importe;
                if (pm.Typev == "E")
                    model.VALES_ELECT_Deposits += pm.Importe;
                if (pm.PMid == 1 || pm.PMid == 19)
                    model.CARDSDeposits += pm.Importe;
            }
            _cancenlationsCardTotal = _cancenlationsCardC + _cancenlationsCardD;
            model._cancTarjetaC = _cancenlationsCardC;
            model._cancTarjetaD = _cancenlationsCardD;
            model.CARDSDeposits = model.CARDSDeposits - _cancenlationsCardTotal;
            model.VALES_ELECT_Deposits = model.VALES_ELECT_Deposits - model.CANC_ELECT + _cancenlationsCardTotal;
            foreach (Deposits depo in model.Deposits)
            {
                model.MXNDeposits += depo.Importe;
                model.DLLSDeposits += depo.Importe_DLLS_MXN;
            }
            model.TotalDeposits = model.MXNDeposits + model.DLLSDeposits + model.VALESDeposits + model.VALES_ELECT_Deposits+model.CARDSDeposits;
            model.IEPS_DetaillList = _PortadaRepository.GetIeps(Datee);
            model.SaleDiscountslList = _PortadaRepository.GetDiscountsList(Datee);
            model.CASHBACK_TOTAL = _PortadaRepository.GetCashBack(Datee);
            model.ServicesDetailList = _PortadaRepository.GetServiceDetailList(Datee);
            foreach (Rent _rent in _PortadaRepository.GetRentList(Datee))
            {
                if(_rent.Currency=="USD")
                {
                    model.RENTA_DLLS += _rent.Impporte;
                    model.RENTA_IVA += (_rent.Tax * CurrencyRate);
                }
                else
                {
                    model.RENTA_IVA += _rent.Tax;
                    model.RENTA_MXN += _rent.Impporte;
                }
            }
            model.RENTA_TOTAL = (model.RENTA_DLLS * CurrencyRate) + model.RENTA_IVA + model.RENTA_MXN;
            foreach (DonationData _don in _PortadaRepository.GetDonations(Datee))
            {
                model.DON_MXN += _don.AmountMXN;
                model.DON_DLLS += _don.AmountUSD;
            }
            
            var _invoicesList = _PortadaRepository.getInvoices(Datee);
            var _InvComp_=0.0M;
            foreach(INVOICES inv in _invoicesList)
            {
                _InvComp_ += inv.cur_code == "MXN" ? inv.invoice_total : inv.invoice_total * CurrencyRate;
            }
           // model._cancTarjetas_0 = _PortadaRepository.GetCanceletedMountFromTarjetas(Datee, _SiteConfigBusiness.GetSiteCode());
            var contado = model.TotalesMXN[6, 1]- _InvComp_;
            var contadoMN = model.TotalesDLLS[6, 1];
            var Comisiones = model.COMIS_TOTAL;
            var Pagos_Total = model.PAYSCUSTTOTAL;
            var Donativos = (model.DON_DLLS * CurrencyRate) + model.DON_MXN;
            var Serivicios = model.ServicesDetailList.Sum(S => S.Importe);
            var ValesDespensa = model.PaymentsMethodsList.Where(W => W.PMid == 26).Sum(S => S.Importe);
          
            var resta = contado + contadoMN + Comisiones + Pagos_Total + Donativos + Serivicios + ValesDespensa;

            var TotalDepositos = model.TotalDeposits;

            var ValesFlorido = model.PaymentsMethodsList.Where(W => W.PMid == 17).Sum(S => S.Importe);
            var TarjetaFlorido = model.PaymentsMethodsList.Where(W => W.PMid == 18).Sum(S => S.Importe);
            var CuponDescuento = model.PaymentsMethodsList.Where(W => W.PMid == 24).Sum(S => S.Importe);
            var NotasCredito = (_CreditNoteDLLS * CurrencyRate) + _CreditNoteMXN;
            var Bonificaciones = (_BoniDLLS * CurrencyRate) + _BoniMXN;// (model.BONIDLLS * CurrencyRate) + model.BONIMXN;
            var CashBack = model.CASHBACK_TOTAL;

            var TotalSalidas = ValesFlorido + CuponDescuento + NotasCredito + Bonificaciones + CashBack;//+ TarjetaFlorido;

            model.Difference = TotalDepositos - resta + TotalSalidas;

            return model;
        }



    }
}
