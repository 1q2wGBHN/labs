﻿using App.DAL.CreditNotes;
using App.DAL.Sales;
using App.Entities;
using App.Entities.ViewModels.Invoices;
using App.Entities.ViewModels.Sales;
using System;
using System.Collections.Generic;
using System.Linq;
using static App.Entities.ViewModels.Common.Constants;

namespace App.BLL.Sales
{
    public class SalesBusiness
    {
        private SalesRepository _SalesRepository;
        private CreditNotesDetailTaxRepository _CreditNotesDetailTaxRepository;

        public SalesBusiness()
        {
            _SalesRepository = new SalesRepository();
            _CreditNotesDetailTaxRepository = new CreditNotesDetailTaxRepository();
        }

        public DFLPOS_SALES GetSaleByNumber(long SaleId) => _SalesRepository.GetSaleByNumber(SaleId);

        public List<SalesModel> GetUnInvoicedSalesByDate(string SaleDate) => _SalesRepository.GetUnInvoicedSalesByDate(SaleDate);

        public SalesModel GetGlobalInvoiceTotal(string SaleDate) => _SalesRepository.GetGlobalInvoiceTotal(SaleDate);

        public List<SalesModel> GetSaleBySaleId(long SaleId) => _SalesRepository.GetSalesBySaleId(SaleId);

        public List<CustomerSalesViewModel> SalesClientsByHourReport(SalesIndex model) => _SalesRepository.SalesClientsByHourReport(model);

        public List<SalesModel> GetSalesReport(SalesIndex Model)
        {
            if (string.IsNullOrEmpty(Model.caja))
                Model.caja = string.Empty;

            if (string.IsNullOrEmpty(Model.Cashier))
                Model.Cashier = string.Empty;

            var salesList2 = _SalesRepository.GetSalesReport(Model);

            return salesList2;
        }
        public List<DollarSalesModel> GetDollarSalesByDates(SalesIndex model) => _SalesRepository.GetDollarSalesByDates(model.StartDate, model.EndDate);

        public List<string> GetCajas() => _SalesRepository.GetCajas();

        public List<SalesModelConc> GetSalesConcretated(SalesConcIndex Model) => _SalesRepository.GetSalesConcretated(Model);  //recibe el modelo index y lo filtra en base a los valores contenidos en todos los campos        

        public bool IsSaleInvoiced(long SaleNumber) => _SalesRepository.IsSaleInvoiced(SaleNumber);

        public bool IsSaleFromToday(long SaleNumber) => _SalesRepository.IsSaleFromToday(SaleNumber);

        public bool ExistingSale(long SaleNumber) => _SalesRepository.ExistingSale(SaleNumber);

        public bool UpdateSale(DFLPOS_SALES Sale) => _SalesRepository.UpdateSale(Sale);

        public long GetInvoiceBySaleNumber(long Sale) => _SalesRepository.GetInvoiceBySaleNumber(Sale);

        public bool UpdateSalesWithInvoiceData(string CustomerCode, string InvoiceSerie, long InvoiceNumber, string SaleDate) => _SalesRepository.UpdateSalesWithInvoiceData(CustomerCode, InvoiceSerie, InvoiceNumber, SaleDate);

        public bool UpdateSalesFromCanceledlInvoice(long InvoiceNumber, string InvoiceSerie, bool IsCredit) => _SalesRepository.UpdateSalesFromCanceledlInvoice(InvoiceNumber, InvoiceSerie, IsCredit);

        public List<DFLPOS_SALES> GetSalesByInvoiceNumber(long InvoiceNumber) => _SalesRepository.GetSalesByInvoiceNumber(InvoiceNumber);

        public string GetDateLastSale(string CustomerRFC) => _SalesRepository.GetDateLastSale(CustomerRFC);

        public bool IsSaleFromGlobalInvoice(long SaleNumber) => _SalesRepository.IsSaleFromGlobalInvoice(SaleNumber);

        public bool SalePMForbidden(long SaleNumber) => _SalesRepository.SalePMForbidden(SaleNumber);

        public bool IsSaleCancelled(long SaleNumber) => _SalesRepository.IsSaleCancelled(SaleNumber);

        public decimal SaleTotal(long SaleNumber) => _SalesRepository.SaleTotal(SaleNumber);

        public List<WeighingReport> GetWeighingList(WeighingIndex Model) => _SalesRepository.GetWeighingList(Model);

        public List<MA_CLASS> GetDepartaments() => _SalesRepository.GetDepartaments();

        public List<ItemsWithOutSalesModel> GetItemsWithOutSalesList(string Date1, string Date2, string DepartmentsIds) => _SalesRepository.GetItemsWithOutSalesList(Date1, Date2, DepartmentsIds);

        public List<ItemsWithSalesModel> GetItemsWithSalesList(string Date1, string Date2) => _SalesRepository.GetItemsWithSalesList(Date1, Date2);

        public List<USER_MASTER> GetUsersCashiersList() => _SalesRepository.GetUsersCashiersList();

        public List<TransactionsCardData> GetTransactionsCardData(TransactionsCardIndex Model) => _SalesRepository.GetTransactionsCardData(Model);

        public List<TransactionsCardByCashier> GetTransactionsCardByCashier(TransactionsCardIndex Model) => _SalesRepository.GetTransactionsCardByCashier(Model);

        public decimal GetBonificationEmployeeTotalByInvoiceDate(DateTime InvoiceDate) => _SalesRepository.GetBonificationEmployeeTotalByInvoiceDate(InvoiceDate);        

        public List<AverageCustomersByHour> GetAverageCustomertByHours(string SaleDate) => _SalesRepository.GetAverageCustomertByHours(SaleDate);

        public bool RollbackUpdateSales(string InvoiceSerie, long InvoiceNumber, long? OldInvoiceNumber, bool IsSubstitution) => _SalesRepository.RollbackUpdateSales(InvoiceSerie, InvoiceNumber, OldInvoiceNumber, IsSubstitution);

        public List<TransactionsCardByCashier> TransactionCardReport(TransactionsCardIndex Model) => _SalesRepository.TransactionCardReport(Model);

        public List<Accumulated> GetAccumulatedData(string DateCheck, string EmployeeNumber) => _SalesRepository.GetAccumulatedData(DateCheck, EmployeeNumber);

        public List<DFLPOS_SERVICES> GetTipoServiciosList() => _SalesRepository.GetTipoServicios();

        public serviciosRecargas GetServicesRecargas(string startDate, string endDate, string compañia, string idServicio, bool fromMenu, int detailServ) => _SalesRepository.GetServicesModelList(startDate, endDate, compañia, idServicio, fromMenu, detailServ);

        public SalesIEPSModel GetSalesIEPSModel(SalesIndex Model)
        {
            var List = _SalesRepository.GetSalesIEPSModel(Model.StartDate, Model.EndDate, Model.Type, Model.Search);
            if (Model.SubstractIEPS)
            {
                var CreditNoteIEPS = _CreditNotesDetailTaxRepository.GetDetailTaxsByDate(Model.StartDate, Model.EndDate);
                if (CreditNoteIEPS.Count > 0)
                {
                    var HasTax = CreditNoteIEPS.Where(c => c.tasa_ieps.Contains("IVA")).Any();
                    if (HasTax)
                    {
                        var IEPSList = CreditNoteIEPS.Where(c => c.tasa_ieps.Contains(TaxTypes.IEPS)).ToList();
                        foreach (var Item in IEPSList)
                        {
                            var Element = List.SalesIepsTax.Where(l => l.tasa_ieps == Item.tasa_ieps && l.departamento.Contains("ABARROTES")).OrderByDescending(c => c.venta).FirstOrDefault();
                            var ElementRemove = List.SalesIepsTax.Where(l => l.tasa_ieps == Item.tasa_ieps && l.departamento.Contains("ABARROTES")).OrderByDescending(c => c.venta).FirstOrDefault();
                            if (Element != null)
                            {
                                Element.venta -= Item.venta;
                                Element.importe_ieps -= Item.importe_ieps;
                                Element.iva -= Item.iva;
                                Element.total -= Item.total;
                                List.SalesIepsTax.Remove(ElementRemove);
                                List.SalesIepsTax.Add(Element);
                            }
                        }
                    }
                    else
                    {
                        foreach (var Item in CreditNoteIEPS)
                        {
                            var Element = List.SalesIepsTaxZero.Where(l => l.tasa_ieps == Item.tasa_ieps && l.departamento.Contains("ABARROTES")).OrderByDescending(c => c.venta).FirstOrDefault();
                            var ElementRemove = List.SalesIepsTaxZero.Where(l => l.tasa_ieps == Item.tasa_ieps && l.departamento.Contains("ABARROTES")).OrderByDescending(c => c.venta).FirstOrDefault();
                            if (Element != null)
                            {
                                Element.venta -= Item.venta;
                                Element.importe_ieps -= Item.importe_ieps;
                                Element.iva -= Item.iva;
                                Element.total -= Item.total;
                                List.SalesIepsTaxZero.Remove(ElementRemove);
                                List.SalesIepsTaxZero.Add(Element);
                            }
                        }
                    }
                }
            }

            if(Model.GroupByItem)
            {
                var GroupedListTax = (from lt in List.SalesIepsTax
                                      group new { lt.venta, lt.importe_ieps, lt.iva, lt.total } by new { lt.codigo, lt.descripcion, lt.departamento, lt.tasa_ieps } into g
                                      select new SalesIEPS
                                      {
                                          saleDate = string.Format("{0} - {1}", Model.StartDate, Model.EndDate),
                                          codigo = g.Key.codigo,
                                          departamento = g.Key.departamento,
                                          descripcion = g.Key.descripcion,
                                          familia = string.Empty,
                                          tasa_ieps = g.Key.tasa_ieps,
                                          venta = g.Sum(c => c.venta),
                                          importe_ieps = g.Sum(c => c.importe_ieps),
                                          iva = g.Sum(c => c.iva),
                                          total = g.Sum(c => c.total)
                                      }).ToList();

                var GroupedListTaxZero = (from lt in List.SalesIepsTaxZero
                                          group new { lt.venta, lt.importe_ieps, lt.iva, lt.total } by new { lt.codigo, lt.descripcion, lt.departamento, lt.tasa_ieps } into g
                                          select new SalesIEPS
                                          {
                                              saleDate = string.Format("{0} - {1}", Model.StartDate, Model.EndDate),
                                              codigo = g.Key.codigo,
                                              departamento = g.Key.departamento,
                                              descripcion = g.Key.descripcion,
                                              familia = string.Empty,
                                              tasa_ieps = g.Key.tasa_ieps,
                                              venta = g.Sum(c => c.venta),
                                              importe_ieps = g.Sum(c => c.importe_ieps),
                                              iva = g.Sum(c => c.iva),
                                              total = g.Sum(c => c.total)
                                          }).ToList();

                List.SalesIepsTax = GroupedListTax;
                List.SalesIepsTaxZero = GroupedListTaxZero;
            }

            return List;
        }

        public bool CheckSalesWithPMEpmloyeeBon(string DateValue) => _SalesRepository.CheckSalesWithPMEpmloyeeBon(DateValue);

        public List<UtilitySales> UtilitySales(UtilitySalesIndex Model)
        {
            if (Model.PartNumber == null)
                Model.PartNumber = "";

            return _SalesRepository.UtilitySales(Model);
        }

        public List<GiftCardData> GetDataActiveCardsReport(GiftCardReportIndex Model) => _SalesRepository.GetDataActiveCardsReport(Model);

        public List<GiftCardData> GetDataGiftCardChargesReport(GiftCardReportIndex Model) => _SalesRepository.GetDataGiftCardChargesReport(Model);

        public InvoiceDetails GetSaleDetailForBonificationCreditNotes(long SaleNumber) => _SalesRepository.GetSaleDetailForBonificationCreditNotes(SaleNumber);

        public bool UpdateSalesWithSubstitutedInvoice(string Serie, long NewInvoice, long OldInvoice) => _SalesRepository.UpdateSalesWithSubstitutedInvoice(Serie, NewInvoice, OldInvoice);
    }
}
