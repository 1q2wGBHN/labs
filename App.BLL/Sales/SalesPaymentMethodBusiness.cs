﻿using App.DAL.Sales;
using App.Entities;
using App.Entities.ViewModels.Sales;
using System.Collections.Generic;

namespace App.BLL.Sales
{
    public class SalesPaymentMethodBusiness
    {
        private SalesPaymentMethodRepository _SalesPaymentMethodRepository;

        public SalesPaymentMethodBusiness()
        {
            _SalesPaymentMethodRepository = new SalesPaymentMethodRepository();
        }

        public int GetPaymentMethodBySaleId(long SaleId)
        {
            return _SalesPaymentMethodRepository.GetPaymentMethodBySaleId(SaleId);
        }        

        public bool UpdatePaymentMethodsWithInvoiceData(string InvoiceSerie, long InvoiceNumber)
        {
            return _SalesPaymentMethodRepository.UpdatePaymentMethodsWithInvoiceData(InvoiceSerie, InvoiceNumber);
        }

        public bool RollbackUpdatePaymentMethods(string InvoiceSerie, long InvoiceNumber)
        {
            return _SalesPaymentMethodRepository.RollbackUpdatePaymentMethods(InvoiceSerie, InvoiceNumber);
        }

        public List<DFLPOS_SALES_PAYMENT_METHOD> GetPaymentMethodList(string InvoSerie, long InvoNumber)
        {
            return _SalesPaymentMethodRepository.GetPaymentMethodList(InvoSerie, InvoNumber);
        }

        public List<ChargeSalesRelationInfo> GetChargeRelationSalesData(ChargeRelationSalesIndex Model)
        {
            return _SalesPaymentMethodRepository.GetChargeRelationSalesData(Model);
        }
    }
}
