﻿using App.DAL.Sales;
using App.Entities.ViewModels;
using App.Entities.ViewModels.Invoices;
using App.Entities.ViewModels.Order;
using App.Entities.ViewModels.Sales;
using System;
using System.Collections.Generic;
using System.Linq;

namespace App.BLL.Sales
{
    public class SalesDetailBusiness
    {
        private SalesDetailRepository _SalesDetailRepository;
        private SalesRepository _SalesRepository;
        public SalesDetailBusiness()
        {
            _SalesDetailRepository = new SalesDetailRepository();
            _SalesRepository = new SalesRepository();
        }

        public SalesDetailModel getAverageSale(string Code, string Site)
        {
            decimal AverageSales = 0;
            //var SalesDetail = _SalesDetailRepo.getAll()
            //    .Where(x => x.datee.Value.Date <= DateTime.Now.Date &&
            //    x.datee.Value.Date >= DateTime.Now.Date.AddDays(-49) && x.part_number == Code)
            //    .GroupBy(gb => gb.part_number)
            //    .Select(x => new SalesDetailViewModel
            //    {
            //        AverageSale = x.Sum(s => s.quantity.Value),
            //    }).SingleOrDefault();
            //if (SalesDetail != null)
            //{
            //    SalesDetail.PartNumber = Code;
            //    SalesDetail.AverageSale = Math.Round(SalesDetail.AverageSale / 49, 0);
            //    return SalesDetail;
            //}
            //return new SalesDetailViewModel();

            var SalesDetail = _SalesDetailRepository.getAll()
               .Where(x => x.trans_date.Date <= DateTime.Now.Date &&
               x.trans_date.Date >= DateTime.Now.Date.AddDays(-49) && x.part_number == Code && x.site_code == Site)
                .Select(x => new SalesDetailModel
                {
                    AverageSale = AverageSales += x.quantity,
                    Barcode = x.barcode,
                    PartNumber = x.part_number
                }).ToList();
            if (SalesDetail.Count > 0)
            {
                SalesDetail[0].AverageSale = Math.Round(AverageSales / 49, 0);
                return SalesDetail[0];
            }
            return new SalesDetailModel();
        }

        public List<OrderListModel> getAverageSales(int Supplier, string Site)
        {
            return _SalesDetailRepository.getListSales(Supplier, Site);            
        }

        public List<SalesDetailReportModel> getSalesDetailBySaleId(long SaleId)
        {
            return _SalesRepository.GetSailDetailsBySaleId(SaleId);            
        }

        public List<DetailTax> SalesDetailsWithTax(string InvoSerie, long InvoNumber, long GlobalDetailId) => _SalesDetailRepository.SalesDetailsWithTax(InvoSerie, InvoNumber, GlobalDetailId);        

        public List<InvoiceProductsDetails> InvoiceProductsList(long SaleNumber) => _SalesDetailRepository.InvoiceProductsList(SaleNumber);
        

        public bool HasSaleItemsAvailable(long SaleNumber)
        {
            return _SalesDetailRepository.HasSaleItemsAvailable(SaleNumber);
        }
        public List<SalesDetailReportModel> GetSalesDetailsBySaleId(long id)
        {
            return _SalesRepository.GetSalesDetailsBySaleId(id);
        }
        public List<SalesDetailReportModel> GetSalesDetailsBySaleIdReverse(long id)
        {
            return _SalesRepository.GetSalesDetailsBySaleIdReverse(id);
        }
    }
}
