﻿using App.DAL.MovementNumber;

namespace App.BLL.MovementNumber
{
    public class MovementNumberBusiness
    {
        private readonly MovementNumberRepository _MomentNumberRepo;

        public MovementNumberBusiness()
        {
            _MomentNumberRepo = new MovementNumberRepository();
        }

        public string ExecuteSp_Get_Next_Document() => _MomentNumberRepo.ExecuteSp_Get_Next_Document();
    }
}