﻿using App.DAL.Currency;
using App.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App.BLL.Currency
{
    public class CurrencyBusiness
    {
        private CurrencyRepository _CurrencyRepository;

        public CurrencyBusiness()
        {
            _CurrencyRepository = new CurrencyRepository();
        }

        public List<CURRENCY> CurrencyList() => _CurrencyRepository.CurrencyList();

        public decimal GetCurrencyRateByDate(string date) => _CurrencyRepository.GetCurrencyRateByDate(date);
    }
}
