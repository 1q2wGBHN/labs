﻿using App.DAL.MaTax;
using App.Entities;
using App.Entities.ViewModels.CreditNotes;
using App.Entities.ViewModels.MaTax;
using System.Collections.Generic;

namespace App.BLL.Ma_Tax
{
    public class MaTaxBusiness
    {
        private readonly MaTaxRepository _maTaxRepository;

        public MaTaxBusiness()
        {
            _maTaxRepository = new MaTaxRepository();
        }

        public MaTaxModel GetTaxValueByCode(string tax_code)
        {
            var MaTax = _maTaxRepository.GetTaxValueByCode(tax_code);
            if (MaTax != null)
                return MaTax;
            else
                return null;
        }

        public List<BonificationTax> GetTaxesForBonifications() => _maTaxRepository.GetTaxesForBonifications();

        public List<MA_TAX> GetTaxList() => _maTaxRepository.GetTaxList();

        public List<MA_TAX> GetIEPSList() => _maTaxRepository.GetIEPSList();

        public List<BonificationTax> GetTaxForGlobalInvoiceBonification(long InvoiceNo) => _maTaxRepository.GetTaxForGlobalInvoiceBonification(InvoiceNo);

        public List<MA_TAX> GetAllIVANormal()
        {
            return _maTaxRepository.GetAllIVANormal();
        }
    }
}