﻿using App.DAL.Quotes;
using App.Entities;
using App.Entities.ViewModels.Quotes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App.BLL.Quotes
{
    public class QuotesDetailsBusiness
    {
        private QuotesDetailsRepository _QuoteDetailsRepository;

        public QuotesDetailsBusiness()
        {
            _QuoteDetailsRepository = new QuotesDetailsRepository();
        }

        public bool CreateQuoteDetail (QuoteModel Model)
        {
            var QuoteDetailsList = new List<QUOTES_DETAILS>();
            foreach(var element in Model.ItemsList)
            {
                var Item = new QUOTES_DETAILS();
                Item.quote_no = Model.QuoteNo;
                Item.quote_serie = Model.QuoteSerie;
                Item.part_number = element.ItemNumber;
                Item.detail_qty = element.Quantity;
                Item.detail_price = element.QuotePrice;
                Item.detail_tax = element.ItemTax;
                Item.detail_tax_percentage = element.ItemTaxPercentage;
                Item.detail_capture = element.BasePrice;
                QuoteDetailsList.Add(Item);
            }
            return _QuoteDetailsRepository.CreateQuoteDetail(QuoteDetailsList);
        }
    }
}
