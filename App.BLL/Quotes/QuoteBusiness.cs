﻿using App.DAL.Quotes;
using App.Entities;
using App.Entities.ViewModels.Quotes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App.BLL.Quotes
{
    public class QuoteBusiness
    {
        private QuotesRepository _QuoteRepository;

        public QuoteBusiness()
        {
            _QuoteRepository = new QuotesRepository();
        }

        public bool CreateQuote(QuoteModel Model)
        {
            var Item = new QUOTES();
            Item.quote_no = Model.QuoteNo;
            Item.quote_serie = Model.QuoteSerie;
            Item.site_code = Model.SiteCode;
            Item.quote_date = DateTime.Now;
            Item.customer_code = Model.ClientCode;
            Item.quote_total = Model.Total;
            Item.quote_tax = Model.Tax;
            //Item.quote_status = ??
            //Item.quote_notes = ??
            Item.pdf = Model.PdfBase64;
            Item.cuser = Model.UserName;
            Item.cdate = DateTime.Now;
            Item.program_id = "QUOTE001.cshtml";
            return _QuoteRepository.CreateQuote(Item);
        }

        public List<QuoteReport> QuoteReport(string DateStart, string DateEnd, string CustomerCode) => _QuoteRepository.QuoteReport(DateStart, DateEnd, CustomerCode);

        public QUOTES GetQuotation(long Number, string Serie) => _QuoteRepository.GetQuotation(Number, Serie);
    }
}
