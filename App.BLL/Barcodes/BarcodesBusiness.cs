﻿using App.DAL.Barcodes;
using App.Entities.ViewModels.Android;

namespace App.BLL.Barcodes
{
    public class BarcodesBusiness
    {
        private readonly BarcodesRepository _barcodesRepository;

        public BarcodesBusiness()
        {
            _barcodesRepository = new BarcodesRepository();
        }

        public BarcodesModel GetAllDataByBarcode(string barcode) => _barcodesRepository.GetAllDataByBarcode(barcode);
    }
}