﻿using App.DAL.MaClass;
using App.Entities;
using System.Collections.Generic;
using App.Entities.ViewModels.MaClass;

namespace App.BLL.MaClass
{
    public class MaClassBusiness
    {
        private readonly MaClassRepository _maClassRepository;

        public MaClassBusiness()
        {
            _maClassRepository = new MaClassRepository();
        }

        public List<MA_CLASS> GetAllDepartments(int level) => _maClassRepository.GetAllDepartments(level);

        public List<MA_CLASS> GetAllFamilyByParentClass(int level2) => _maClassRepository.GetAllFamilyByParentClass(level2);

        public List<MaClassModel> GetAllFamilyByInventoryActive() => _maClassRepository.GetAllFamilyByAcitveInventory();

        public string GetDepartmentById(int level_header) => _maClassRepository.GetDepartmentById(level_header);

        public string GetFamilyById(int level1_code) => _maClassRepository.GetFamilyById(level1_code);
    }
}