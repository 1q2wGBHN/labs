﻿using App.DAL.FixedAsset;
using App.GlobalEntities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App.BLL.FixedAsset
{
    public class FixedAssetRequisitionBusiness
    {
        private FixedAssetRequisitionRepository _fixedAssetRequsition;
        

        public FixedAssetRequisitionBusiness()
        {
            _fixedAssetRequsition = new FixedAssetRequisitionRepository();
        }

        public int AddRequisition(string user, string remark, string site)
        {
            FIXED_ASSET_REQUISITION requisition = new FIXED_ASSET_REQUISITION();
            requisition.site_code = site;
            requisition.requisition_status = 1;
            requisition.requisition_date = DateTime.Now;
            requisition.requisition_remark = remark;
            requisition.cuser = user;
            requisition.cdate = DateTime.Now;
            requisition.program_id = "RFA001.cshtml";

            return _fixedAssetRequsition.AddRequisition(requisition);
        }

        public List<FixedAssetRequisitionModel> GetRequisitionAssetBySite(string site_code) => _fixedAssetRequsition.GetRequisitionAssetBySite(site_code);

    }
}
