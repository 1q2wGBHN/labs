﻿using App.DAL.FixedAsset;
using App.GlobalEntities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App.BLL.FixedAsset
{
    public class FixedAssetBusiness
    {
        private FixedAssetRepository _fixedAsset;

        public FixedAssetBusiness()
        {
            _fixedAsset = new FixedAssetRepository();
        }

        public List<FIXED_ASSET> GetAllFixedAsset() => _fixedAsset.GetAllFixedAsset();

        public FIXED_ASSET GetAssetById(int id) => _fixedAsset.GetAssetById(id);

    }
}
