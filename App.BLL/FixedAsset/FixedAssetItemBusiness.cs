﻿using App.DAL.FixedAsset;
using App.GlobalEntities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App.BLL.FixedAsset
{
    public class FixedAssetItemBusiness
    {
        private FixedAssetItemRepository _fixedAssetItem;

        public FixedAssetItemBusiness()
        {
            _fixedAssetItem = new FixedAssetItemRepository();
        }

        public int AddItem(string user, string site, int asset, string description, string brand, string serie, string model, int requisition)
        {
            FIXED_ASSET_ITEM item = new FIXED_ASSET_ITEM();
            item.fixed_asset_code = asset;
            item.site_code = site;
            item.description = description;
            item.brand = brand;
            item.serie = serie;
            item.model = model;
            item.status_asset = 0;
            item.requisition_flag = true;
            item.requisiton_code = requisition;
            item.cuser = user;
            item.cdate = DateTime.Now;
            item.program_id = "RFA001.cshtml";

            return _fixedAssetItem.AddItem(item);
        }

        public List<FixedAssetItemModel> GetItemsByRequisition(int id) => _fixedAssetItem.GetItemsByRequisition(id);




    }
}
