﻿using App.DAL;
using App.DAL.Item;
using App.Entities.ViewModels.Item;
using App.Entities.ViewModels.Inventory;
using System;
using System.Collections.Generic;

namespace App.BLL.Item
{
    public class ItemStockBusiness
    {
        private readonly ItemStockRepository _itemStockRepository;
        private readonly ItemRepository _itemRepository;

        public ItemStockBusiness()
        {
            _itemStockRepository = new ItemStockRepository();
            _itemRepository = new ItemRepository();
        }

        public int GetItemInStorageTypeMerma(string part_number, string quantity)
        {
            decimal cantidad = decimal.Parse(quantity);
            return _itemStockRepository.GetItemInStorageTypeMerma(part_number, cantidad);
        }

        public decimal GetItemInStorageTypeMermaDifference(string part_number, string quantity)
        {
            decimal cantidad = decimal.Parse(quantity);
            return _itemStockRepository.GetItemInStorageTypeMermaDifference(part_number, cantidad);
        }

        public decimal GetItemInStorageTypeMermaDifferenceReal(string part_number, string quantity)
        {
            decimal cantidad = decimal.Parse(quantity);
            return _itemStockRepository.GetItemInStorageTypeMermaDifferenceReal(part_number, cantidad);
        }

        public bool GetValidationItemByRemisionDepartmentFamily(string part_number) => _itemStockRepository.GetValidationItemByRemisionDepartmentFamily(part_number);

        public bool GetValidationItemByBlockedDeparmentFamily(string part_number) => _itemStockRepository.GetValidationItemByBlockedDeparmentFamily(part_number);

        public bool GetValidationItemByBlocked(string part_number) => _itemStockRepository.GetValidationItemByBlocked(part_number);

        public int GetItemStock(string part_number, string quantity)
        {
            decimal cantidad = decimal.Parse(quantity);
            return _itemStockRepository.GetItemStock(part_number, cantidad);
        }

        public ItemModel GetItemStockSelectStock(string PartNumber) => _itemStockRepository.GetItemStockSelectStock(PartNumber);

        public ItemModel GetItemStockSelectStockNegative(string PartNumber) => _itemStockRepository.GetItemStockSelectStockNegative(PartNumber);

        public ItemModel GetItemStockSelectStockBlock(string part_number) => _itemStockRepository.GetItemStockSelectStockBlock(part_number);

        public ItemModel GetItemStock(string PartNumber) => _itemStockRepository.GetItemStock(PartNumber);

        public List<ItemModel> GetInventoryAdjustmentRequests() => _itemStockRepository.InventoryAdjustmentRequests();

        public List<ItemModel> GetInventoryAdjustmentRequestsByDate(DateTime beginDate, DateTime endDate) => _itemStockRepository.InventoryAdjustmentRequestsByDate(beginDate, endDate);

        public List<ItemModel> GetInventoryAdjustmentRequestsByReference(string reference) => _itemStockRepository.InventoryAdjustmentRequestsById(reference);

        public List<NegativeInventoryBalancesTemp> NegativeInventoryBalance() => _itemStockRepository.NegativeInventoryBalance();

        public InventoryNegativeHeaderModel NegativeInventoryBalanceByPartNumber(string part_number) {
                return _itemStockRepository.NegativeInventoryBalanceByPartNumber(part_number);

        }

        public List<NegativeInventoryBalancesTemp> GetAllInvetoriesNegative() => _itemStockRepository.GetAllInvetoriesNegative();

        public bool UpdateItemInventoryNegative(int id_inventory_negative, string user) => _itemStockRepository.UpdateItemInventoryNegative(id_inventory_negative, user);

        public bool AddItemInventoryNegative(string origin, string destination, decimal quantity, string reason, string user)
        {
            //var originInfo = NegativeInventoryBalanceByPartNumber(origin);
            //if (originInfo == null)
            //    return false;

            //var destinationInfo = NegativeInventoryBalanceByPartNumber(destination);
            //if (destinationInfo == null)
            //    return false;
            //NegativeInventoryBalancesTemp obj = new NegativeInventoryBalancesTemp
            //{
            //    part_number_origin = origin,
            //    part_number_origin_amount = originInfo.part_number_destination_amount,
            //    part_number_origin_description = originInfo.part_number_destination_description,
            //    part_number_origin_stock_unrestricted = originInfo.part_number_destination_stock_unrestricted,
            //    part_number_origin_last_purchase_price = originInfo.part_number_destination_last_purchase_price,
            //    part_number_destination = destination,
            //    part_number_destination_amount = destinationInfo.part_number_destination_amount,
            //    part_number_destination_description = destinationInfo.part_number_destination_description,
            //    part_number_destination_stock_unrestricted = destinationInfo.part_number_destination_stock_unrestricted,
            //    part_number_destination_last_purchase_price = destinationInfo.part_number_destination_last_purchase_price,
            //    active_flag = true,
            //    move_quantity = quantity,
            //    reason = reason
            //};
            //return _itemStockRepository.AddItemInventoryNegative(obj, user);
            return true;
        }

        public List<ItemModel> SetItemStock(List<ItemModel> model, string User, out string documentNumber)
        {
            documentNumber = _itemStockRepository.GetNextDocument("CYCLE_COUNT");

            foreach (var item in model)
            {
                var stockReal = _itemStockRepository.GetItemStockSelectStockNegative(item.PathNumber);
                if (!_itemStockRepository.SetItemStock(item, User, documentNumber, _itemRepository.GetMapPrice(item.PathNumber), stockReal.Stock))
                    return null;

                var data = _itemRepository.StoreProcedureIvaIeps(item.PathNumber);

                item.BaseCost = _itemRepository.GetMapPrice(item.PathNumber);
                item.IEPS = (item.BaseCost * (item.Stock ?? 0)) * (data.Ieps / 100);
                item.PartIva = ((item.BaseCost * (item.Stock ?? 0)) + item.IEPS) * (data.Iva / 100);
            }
            return model;
        }

        public bool GetCreditDebit(string movement)
        {
            if (movement == "DEBITO")
                return true;
            else
                return false;
        }

        public List<ItemModel> SetIvaIepsInventoryCC(ItemKardexModel item)
        {
            var itemStock = _itemStockRepository.GetItemStockSelectStockNegative(item.PartNumber);
            var data = _itemRepository.StoreProcedureIvaIeps(item.PartNumber);
            List<ItemModel> model = new List<ItemModel>()
            {
                new ItemModel(){ PathNumber = item.PartNumber, Description = item.PartDescription, BaseCost = item.MapPrice ?? 0, Comment = item.Reason, IEPS = ( item.MapPrice ?? 0 * (item.MovementQuantity ?? 0)) * (data.Ieps / 100), PartIva = ((item.MapPrice ?? 0 * (item.MovementQuantity ?? 0)) + ( item.MapPrice ?? 0 * (item.MovementQuantity ?? 0)) * (data.Ieps / 100)) * (data.Iva / 100), Stock = item.MovementQuantity, MovementType = GetCreditDebit(item.MovementType) }
            };
            return model;
        }

        public bool CancelInventoryAdjustment(string partNumber, string Document, string Comments, string User)
        {
            var stockReal = _itemStockRepository.GetItemStockSelectStockNegative(partNumber);
            if (_itemStockRepository.CancelInventoryAdjustment(partNumber, Document, Comments, User, stockReal.Stock))
                return true;
            else
                return false;
        }

        public StoreProcedureResult SetInventoryAdjustment(string partNumber, string Document, string StorageLocation, decimal Quantity, string Type, string Comments, string User, out string documentNumber)
        {
            documentNumber = "";
            var stockReal = _itemStockRepository.GetItemStockSelectStockNegative(partNumber);
            var StoreProcedure = _itemStockRepository.CC_Adjustment("CYCLE_COUNT", partNumber, Quantity, Type, StorageLocation, User, Document, Comments, stockReal.Stock);
            return StoreProcedure;
        }

        public List<ItemModel> GetStockBySupplier(int supplier_id, int class_id, string category, int family_id) => _itemStockRepository.GetStockBySupplier(supplier_id, class_id, category, family_id);
    }
}