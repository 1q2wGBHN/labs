﻿using App.Common;
using App.DAL;
using App.DAL.Item;
using App.Entities.ViewModels.DamagedsGoods;
using App.Entities.ViewModels.Item;
using App.Entities.ViewModels.Inventory;
using System;
using System.Collections.Generic;
using System.Linq;

namespace App.BLL.Item
{
    public class ItemBusiness
    {
        private readonly ItemRepository _ItemRepo;
        private readonly UserMasterBusiness _userMasterBusiness;

        public ItemBusiness()
        {
            _ItemRepo = new ItemRepository();
            _userMasterBusiness = new UserMasterBusiness();
        }

        public List<ItemModel> GetAll()
        {
            return _ItemRepo.GetAll().Where(w => w.extra_items_flag != true && w.combo_flag != true && w.active_flag == true).Select(x => new ItemModel
            {
                Description = x.part_description,
                PathNumber = x.part_number
            }).ToList();
        }

        public List<ItemModel> SerchItem(string part_number)
        {
            return _ItemRepo.SearchItem(part_number).Where(w => w.extra_items_flag != true && w.combo_flag != true && w.active_flag == true).Select(x => new ItemModel
            {
                Description = x.part_description,
                PathNumber = x.part_number
            }).ToList();
        }
        public List<InventoryNegativeDetailModel> SerchItemNegativeDet(string part_number , string negative)
        {
            return _ItemRepo.SearchItemNegativeDet(part_number,negative);
        }

        public List<ItemModel> SerchItemNegative(string part_number) => _ItemRepo.SearchItemNegative(part_number);

        public List<ItemModel> SearchItemValidateNo(string part_number, string no_part_number)
        {
            var z = _ItemRepo.SearchItemValidateNo(part_number, no_part_number).Where(w => w.extra_items_flag != true && w.combo_flag != true && w.active_flag == true).Select(x => new ItemModel
            {
                Description = x.part_description,
                PathNumber = x.part_number
            }).ToList();
            return z;
        }

        public List<ItemModel> SerchItemBarcode(string part_number) => _ItemRepo.SearchItemBarcode(part_number);

        public List<ItemModel> SerchItemBarcodePreInventory(string part_number) => _ItemRepo.SearchItemBarcodePreInventory(part_number);


        public List<ItemModel> SearchItemBarcodeComboPresentation(string part_number) => _ItemRepo.SearchItemBarcodeComboPresentation(part_number);

        public List<ItemModel> SearchItemBarcodeSupplier(string part_number, int supplier) => _ItemRepo.SearchItemBarcodeSupplier(part_number, supplier);
        public List<ItemModel> SearchItemBarcodeSupplierCurrency(string part_number, int supplier , string currency) => _ItemRepo.SearchItemBarcodeSupplierCurrency(part_number, supplier,currency);

        public int GetDepartmentByProduct(string part_number) => _ItemRepo.GetDepartmentByProduct(part_number);

        public int GetFamilyByProduct(string part_number) => _ItemRepo.GetFamilyByProduct(part_number);

        public List<ItemModelDropDown> GetAllProductsToDropDown() => _ItemRepo.GetAllProductsToDropDown();

        public List<DropItemModel> GetAllItemsActives() => _ItemRepo.GetAllActives();

        public List<DamagedsGoodsLastCost> GetLastPurchasePrice(string PartNumber, int supplier_id, int top) => _ItemRepo.GetLastPurchasePrice(PartNumber, supplier_id, top, 0);

        public ItemKardexModel GetInformationInventoryCC(string document) => _ItemRepo.GetInformationInventoryCC(document);

        public bool InventoryCCEditStatusPrint(string Document, string part_number, int Status, string program_id) => _ItemRepo.InventoryCCEditStatusPrint(Document, part_number, Status, program_id);

        public ItemKardexModel GetInformationInventoryCCFinal(string document, string part_number)
        {
            var model = _ItemRepo.GetInformationInventoryCCFinal(document, part_number);
            model.AuthorizationBy = _userMasterBusiness.getCompleteName(model.AuthorizationBy);
            return model;
        }

        public List<ItemKardexModel> GetInventoryCC(DateTime? date1, DateTime? date2, string status, string part_number, string type_movent)
        {
            var model = _ItemRepo.GetInventoryCC(date1, date2, status, part_number, type_movent);
            foreach (var item in model)
                item.AuthorizationBy = _userMasterBusiness.getCompleteName(item.AuthorizationBy);
            return model;
        }

        public ItemModel GetOneItem(string PartNumber) => _ItemRepo.GetOneItem(PartNumber);

        public string PartDescription(string part_number) => _ItemRepo.PartDescription(part_number);

        public string ParthNumber(string ParthNumber) => _ItemRepo.ParthNumber(ParthNumber);

        public StoreProcedureIvaIeps StoreProcedureIvaIeps(string PartNumber) => _ItemRepo.StoreProcedureIvaIeps(PartNumber);

        public ItemModel GetListItemStockByPartNumber(string PartNumber) => _ItemRepo.GetListItemStockByPartNumber(PartNumber);

        public string GetItemLevelHeaderDamageds(string part_number) => _ItemRepo.GetItemLevelHeaderDamageds(part_number);

        public List<ItemKardexModel> GetAllMovementsByProduct(string part_number, string date1, string date2)
        {
            var initialDate = DateTime.Parse(date1);
            var finalDate = DateTime.Parse(date2);
            var movements = _ItemRepo.GetAllMovementsByProductAndDateRange(part_number, initialDate, finalDate);
            if (movements != null)
                return movements;
            else
                return null;
        }

        public List<ItemInventoryDaysModel> GetAllItemsInventoryDays(string proveedor, string depto, string familia, string excessDays) => _ItemRepo.GetInventoryDays(int.Parse(proveedor), int.Parse(depto), Int32.Parse(familia), Int32.Parse(excessDays));

        public ItemEmptySpaceScanModel GetInfoItemByPartNumber(string part_number) => _ItemRepo.GetInfoItemByPartNumber(part_number);

        public ItemEmptySpaceScanModel GetLastEntryByPartNumber(string part_number) => _ItemRepo.GetLastEntryByPartNumber(part_number);

        public decimal getAveraSalebyPartNumber(string part_number)
        {
            var sum = _ItemRepo.GetSalesAverageByDate(part_number);
            decimal? suma = 0;
            if (sum != null)
                suma = sum.Average;
            return Math.Round((suma / 50 ?? 0) * 7, 2);
        }

        public List<ItemSalesInventory> GetProductsWithoutSales(int SupplierId, int DepartmentId, int FamilyId, int Days) => _ItemRepo.GetProductsWithoutSales(SupplierId, DepartmentId, FamilyId, Days);

        public List<ItemHistoricInventory> GetHistoricInventory(DateTime Day) => _ItemRepo.GetHistoricInventory(Day);

        public ItemModel GetItemPriceByPartNumber(string part_number) => _ItemRepo.GetItemPriceByPartNumber(part_number);

        public List<ItemSalesInventory> GetMissingProducts(int SupplierId, int DepartmentId, int FamilyId) => _ItemRepo.GetMissingProducts(SupplierId, DepartmentId, FamilyId);

        public ItemTaxInfo GetItemTaxInfo(string PartNumber) => _ItemRepo.GetItemTaxInfo(PartNumber);

        public CostInfo GetCostItem(string PartNumber, int Supplier) => _ItemRepo.GetCostItem(PartNumber, Supplier);

        public List<ItemQuotation> GetItemQuotations(string Item) => _ItemRepo.GetItemQuotations(Item);

        public StoreProcedureIvaIeps GetItemIvaIepsValue(string part_number) => _ItemRepo.StoreProcedureIvaIeps(part_number);

        public List<ItemReportModel> GetAllItems() => _ItemRepo.GetAllItems();

        public List<ItemReportModel> NewGetAllItems(string part_numbers, int deparment, int family) => _ItemRepo.NewGetAllItems(part_numbers, deparment, family);

        public List<ItemQuickCheck> GetItemQuickCheckList(ItemQuickCheckIndex Model) => _ItemRepo.GetItemQuickCheckList(Model.ItemSearch);

        public List<ItemPresentationModelMin> GetItemPresentationWithBarcode(string part_number) => _ItemRepo.GetItemPresentationWithBarcode(part_number);

        public string GetOnlyBarcodes(string part_number) => _ItemRepo.GetOnlyBarcodes(part_number);

        public Result<List<SearchCodeRes>> SearchCode(string search, bool exact, bool barcode, bool presentation, bool planogram) => Result.OK(_ItemRepo.SearchCode(search, exact, barcode, presentation, planogram));

        public List<ItemModelDropDown> GetItemsByFamily(int LevelCode) => _ItemRepo.GetItemsByFamily(LevelCode);
        public List<ItemModelDropDown> GetItemsByFamilyActive(int LevelCode) => _ItemRepo.GetItemsByFamilyActive(LevelCode);

        public List<ItemModelDropDown> GetItemsByDepartment(int LevelCode) => _ItemRepo.GetItemsByDepartment(LevelCode);

        public List<ItemQuickCheck> GetItemQuickCheckListString(string item) => _ItemRepo.GetItemQuickCheckList(item);

        public List<ItemKardexHorModel> GetItemsKardexHorizontal(DateTime dateInitial, DateTime dateFinish, string department, string part_number)
        {
            return _ItemRepo.GetItemsKardexHor(dateInitial, dateFinish, department, part_number);
        }

        public List<ItemModel> SearchItemBarcodeNoFlags(string part_number) => _ItemRepo.SearchItemBarcodeNoFlags(part_number);
    }
}