﻿using App.BLL.DamagedsGoods;
using App.DAL.Item;
using App.Entities.ViewModels.Android;
using App.Entities.ViewModels.Item;
using App.Entities.ViewModels.Order;
using System.Collections.Generic;
using System.Linq;

namespace App.BLL.Item
{
    public class ItemSupplierBusiness
    {
        private readonly ItemRepository _ItemRepo;
        private readonly ItemSupplierRepository _ItemSupplierRepo;
        private readonly DamagedsGoodsItemRepository _DamagedsGoodsItemR;
        private readonly DamagedsGoodsRestrictedBusiness _damagedsGoodsRestrictedBusiness;

        public ItemSupplierBusiness()
        {
            _ItemRepo = new ItemRepository();
            _ItemSupplierRepo = new ItemSupplierRepository();
            _DamagedsGoodsItemR = new DamagedsGoodsItemRepository();
            _damagedsGoodsRestrictedBusiness = new DamagedsGoodsRestrictedBusiness();
        }

        public List<ItemModel> GetFindListItemSupplier(int Supplier) => _ItemSupplierRepo.GetAll(Supplier);

        public List<ItemModel> GetListItemCurrency(int Supplier, string Currency) => _ItemSupplierRepo.GetListItemCurrency(Supplier, Currency);

        public List<ItemSupplierBaseCost> GetListItemBaseCost(string PartNumber) => _ItemSupplierRepo.GetListItemBaseCost(PartNumber);

        public List<ItemModel> GetFindListItem(int Supplier) => _ItemSupplierRepo.ItemSupppler(Supplier);

        public List<ItemModel> GetIeps(string partNumber) => _ItemSupplierRepo.GetIeps(partNumber);

        public List<ItemModel> GetPacking(string PathNumber)
        {
            var packingItem = _ItemSupplierRepo.GetPacking(PathNumber)
                .Select(x => new ItemModel
                {
                    packing = x.packing_of_size.Value

                }).ToList();

            return packingItem;
        }

        public List<OrderListModel> GetListOrder(int Supplier, string Site , string Currency) => _ItemSupplierRepo.GetListOrder(Supplier, Site , Currency);
        public bool ValidatePartNumberBySite(string part_number, string site_code) => _ItemSupplierRepo.ValidatePartNumberBySite(part_number, site_code);
        public ItemSupplierModel GetItemSupplierByItemAndSupplier(string part_number, int supplier_id) => _ItemSupplierRepo.GetItemSupplierByItemAndSupplier(part_number, supplier_id);

        public ItemSupplierModel GetEntryFree(string part_number, int supplier_id, string Type) => _ItemSupplierRepo.GetEntryFree(part_number, supplier_id, Type);

        public List<ItemStockModel> GetItemsSupplier(int Supplier)
        {
            var Items = _ItemSupplierRepo.GetItemsSupplier(Supplier);
            foreach (var item in Items)
            {
                var block_now = _ItemSupplierRepo.ItemStock(item.PartNumber);
                //Bloqueo de familias de devolución
                if (_damagedsGoodsRestrictedBusiness.GetKnowDamagedsGoodsRestricted("RMA", item.PartNumber))
                    item.Block = block_now;
                else
                    item.Block = 0;
            }

            return Items.Where(x => x.Block > 0).ToList();
        }

        public List<ItemViewRma> GetRMAItemsByFolio(string Folio, string type)
        {
            var Items = _DamagedsGoodsItemR.GetRMAItemsByFolio(Folio, type);
            foreach (var GoodsItem in Items)
            {
                var Tax = _ItemRepo.StoreProcedureIvaIeps(GoodsItem.PartNumber);
                var Ieps = Tax.Ieps;
                var Iva = Tax.Iva;
                GoodsItem.Ieps = GoodsItem.Amount_Doc * (Ieps / 100);
                GoodsItem.Iva = (GoodsItem.Amount_Doc + GoodsItem.Ieps) * (Iva / 100);
            }
            return Items;
        }

        public ItemStockModel GetPriceByPartNumber(string part_number) => _ItemSupplierRepo.GetPrice(part_number);

        public List<ItemSupplierModelPrice> GetItemsBySupplierCodeInSalePrice(int sCode)
        {
            var ItemsSuppliers = _ItemSupplierRepo.GetItemsBySupplierCodeInSalePrice(sCode);
            if (ItemsSuppliers != null)
            {
                if (ItemsSuppliers.Count == 0)
                    return null;
                else
                    return ItemsSuppliers;
            }
            else
                return null;
        }

        public List<ItemSupplierModelPrice> GetItemHistoryBaseCost(string part_number) => _ItemSupplierRepo.GetItemHistoryBaseCost(part_number);
    }
}