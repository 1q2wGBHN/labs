﻿using App.DAL.Item;
using App.Entities.ViewModels.Item;
using System.Collections.Generic;

namespace App.BLL.Item
{
    public class ItemSpeckBusiness
    {
        private readonly ItemSpecRepository _ItemSpecRepo;

        public ItemSpeckBusiness()
        {
            _ItemSpecRepo = new ItemSpecRepository();
        }

        public List<ItemSpeckModel> ListItemSpeck(string PartNumber) => _ItemSpecRepo.ListItemSpeck(PartNumber);
    }
}