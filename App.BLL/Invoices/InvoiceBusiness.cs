﻿using App.DAL.Invoice;
using App.Entities;
using App.Entities.ViewModels.Invoices;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Linq;
using App.Entities.ViewModels.Sales;
using static App.Entities.ViewModels.Common.Constants;
using App.DAL.Lookups;
using App.DAL.Sales;
using App.BLL.SysLog;

namespace App.BLL.Invoices
{
    public class InvoiceBusiness
    {
        private InvoiceRepository _InvoiceRepository;
        private ReferenceNumberRepository _ReferenceNumberRepository;
        private SalesRepository _SalesRepository;
        private SalesPaymentMethodRepository _SalesPaymentMethodRepository;
        private InvoiceDetailRepository _InvoiceDetailRepository;
        private InvoiceDetailTaxRepository _InvoiceDetailTaxRepository;
        private SysLogBusiness _SysLogBusiness;

        public InvoiceBusiness()
        {
            _InvoiceRepository = new InvoiceRepository();
            _ReferenceNumberRepository = new ReferenceNumberRepository();
            _SalesRepository = new SalesRepository();
            _SalesPaymentMethodRepository = new SalesPaymentMethodRepository();
            _InvoiceDetailRepository = new InvoiceDetailRepository();
            _InvoiceDetailTaxRepository = new InvoiceDetailTaxRepository();
            _SysLogBusiness = new SysLogBusiness();
        }        

        public bool CreateInvoice(INVOICES InvoiceData) => _InvoiceRepository.CreateInvoice(InvoiceData);        

        public bool InvoiceExist(long InvoiceNumber) => _InvoiceRepository.InvoiceExist(InvoiceNumber);        

        public bool IsInvoiceActive (long InvoiceNumber) => _InvoiceRepository.IsInvoiceActive(InvoiceNumber);        

        public bool IsInvoiceOverDue(long InvoiceNumber) => _InvoiceRepository.IsInvoiceOverDue(InvoiceNumber);        

        public bool IsInvoiceDaily(long InvoiceNumber) => _InvoiceRepository.IsInvoiceDaily(InvoiceNumber);        

        public bool InvoiceHasCreditNotesOrBonifications(long InvoiceNumber) => _InvoiceRepository.InvoiceHasCreditNotesOrBonifications(InvoiceNumber);        

        public bool InvoiceHasPayments(long InvoiceNumber) => _InvoiceRepository.InvoiceHasPayments(InvoiceNumber);        

        public InvoiceDetails InvoiceDetails(long InvoiceNumber) => _InvoiceRepository.InvoiceDetails(InvoiceNumber);        

        public InvoiceDetails InvoiceDetailsByDate(string DateValue) => _InvoiceRepository.InvoiceDetailsByDate(DateValue);        
        
        public INVOICES GetInvoiceByNumber(long InvoiceNumber) => _InvoiceRepository.GetInvoiceByNumber(InvoiceNumber);
        
        public decimal InvoiceTotal(long InvoiceNumber) => _InvoiceRepository.InvoiceTotal(InvoiceNumber);        

        public bool UpdateInvoice(INVOICES Invoice, SYS_LOG SysLog) => _InvoiceRepository.UpdateInvoice(Invoice, SysLog);        
              
        public List<InvoiceModel> InvoicesDueByCustomer(string CustomerRFC) => _InvoiceRepository.InvoicesDueByCustomer(CustomerRFC);
        
        public List<INVOICES> InvoicesCreditByCustomer(string CustomerRFC, string Currency) => _InvoiceRepository.InvoicesCreditByCustomer(CustomerRFC, Currency);
        
        public List<INVOICES> GetAllHeaders(Expression<Func<INVOICES, bool>> predicate) => _InvoiceRepository.GetAllInvoiceHeaders(predicate);
        
        public List<INVOICES> GetConstructPredicate(InvoiceReportModel RM) => _InvoiceRepository.GetConstructPredicate(RM);               

        public DIGITAL_TAX_STAMP GetDigitalTaxStampByIdAndSerie(long cfdi_id, string cfdi_serie) => _InvoiceRepository.GetDigitalTaxStampByIdAndSerie(cfdi_id, cfdi_serie);        

        public List<CURRENCY> GetAllCurrency() => _InvoiceRepository.GetAllCurrency();

        public List<INVOICE_DETAIL_TAX> GetInvoiceDetailTaxByIdDetail(long detail_id) => _InvoiceRepository.GetInvoiceDetailTaxByIdDetail(detail_id);      

        public List<InvoiceOldBalance> OldBalanceReportData(string Currency, string CustomerCode) => _InvoiceRepository.OldBalanceReportDataByCustomer(Currency, CustomerCode);        

        public List<InvoiceOldBalanceReport> CustomerBalanceByCurrency(string Currency) => _InvoiceRepository.CustomerBalanceByCurrency(Currency);        

        public INVOICES GetGlobalInvoiceByDate(string DateValue) => _InvoiceRepository.GetGlobalInvoiceByDate(DateValue);        

        public bool DeleteInvoice(long Number, string Serie) => _InvoiceRepository.DeleteInvoice(Number, Serie);        

        public List<INVOICES> GetInvoicesListByFilterReport(InvoiceIndex Modelo) => _InvoiceRepository.GetInvoicesListByFilterReport(Modelo);        

        public bool InvoiceHasTax16(long InvoiceNo) => _InvoiceRepository.InvoiceHasTax16(InvoiceNo);

        public INVOICES SubstitutionInvoice(InvoicesSales Model, long InvoiceSubst) => _InvoiceRepository.SubstitutionInvoice(Model, InvoiceSubst);

        public bool CheckTotalAndSubtotalInvoice(INVOICES Invoice) => _InvoiceRepository.CheckTotalAndSubtotalInvoice(Invoice);

        public bool CheckInvoiceTax(INVOICES Invoice) => _InvoiceRepository.CheckInvoiceTax(Invoice);

        public InvoicesSales InvoiceSubstituteInfo(long InvoiceNo)
        {
            var Reference = _ReferenceNumberRepository.GetReferenceNumber(VoucherType.INCOMES);
            var InvoiceInfo = new InvoicesSales();
            var ITEM = _InvoiceRepository.GetInvoiceByNumber(InvoiceNo);
            var Sale = _SalesRepository.GetSalesByInvoiceNumber(InvoiceNo).First();

            //Client Info
            InvoiceInfo.City = ITEM.CUSTOMERS.CITIES.city_nombre;
            InvoiceInfo.State = ITEM.CUSTOMERS.STATES.state_name;
            InvoiceInfo.Address = ITEM.CUSTOMERS.customer_address;
            InvoiceInfo.PhoneNumber = ITEM.CUSTOMERS.customer_phone;
            InvoiceInfo.ZipCode = ITEM.CUSTOMERS.customer_zip;
            InvoiceInfo.FaxNumber = ITEM.CUSTOMERS.customer_phone2;
            InvoiceInfo.ClientName = ITEM.CUSTOMERS.customer_name;
            InvoiceInfo.ClientNumber = ITEM.customer_code;
            InvoiceInfo.Client = ITEM.customer_code;
            InvoiceInfo.ClientCode = ITEM.customer_code;

            //InvoiceInfo
            InvoiceInfo.InvoiceSubst = string.Format("{0}{1}", Reference.serie, InvoiceNo);
            InvoiceInfo.NextInvoice = string.Format("{0}{1}", Reference.serie, Reference.number);
            InvoiceInfo.Total = ITEM.invoice_total;
            InvoiceInfo.IVA = ITEM.invoice_tax;
            InvoiceInfo.Subtotal = ITEM.invoice_total - ITEM.invoice_tax;
            InvoiceInfo.Currency = ITEM.cur_code;
            InvoiceInfo.CFDICode = ITEM.sat_us_code;
            InvoiceInfo.PaymentTypeCode = _SalesPaymentMethodRepository.GetPaymentMethodBySaleId(Sale.sale_id);
            InvoiceInfo.IsGlobal = ITEM.is_daily;
            InvoiceInfo.Serie = Reference.serie;            
            InvoiceInfo.InvoiceNumber = InvoiceNo.ToString(); //Invoice Number that will be replaced

            return InvoiceInfo;
        }

        public bool CreateInvoiceSubtitution(InvoicesSales Model)
        {
            long InvoiceSubst = long.Parse(Model.InvoiceSubst.Replace(Model.Serie, string.Empty));
            var NewInvoice = _InvoiceRepository.SubstitutionInvoice(Model, InvoiceSubst);
            var InvoiceDetailsList = _InvoiceDetailRepository.InvoiceDetailsSubstitution(Model.Serie, InvoiceSubst, Model.Number);
            
            if (_InvoiceRepository.CreateInvoice(NewInvoice))
                if (_InvoiceDetailRepository.CreateInvoiceDetail(InvoiceDetailsList))
                {
                    var InvoiceTaxList = _InvoiceDetailTaxRepository.DetailsTaxForInvoiceSubstitute(Model.Serie, InvoiceSubst, Model.Number);
                    if (InvoiceTaxList.Count > 0)
                        if (!_InvoiceDetailTaxRepository.CreateInvoiceDetailTax(InvoiceTaxList))
                            goto Return;

                    return true;
                }

            Return:
            return false;
        }

        private List<InvoiceReport> BindIeps(List<InvoiceReport> Invoices)
        {
            List<DetailTaxes> Taxes = _InvoiceRepository.GetIepsFromInvoice(Invoices);

            foreach (InvoiceReport Invoice in Invoices)
            {
                //Recibo los IEPS por Factura.
                var FilteredTaxes = (from Tax in Taxes
                                     where Tax.invo_id == Invoice.InvoiceNumber && Tax.invo_serie == Invoice.InvoiceSerie
                                     select new DetailTaxes
                                     {
                                         id = Tax.id,
                                         detail_id = Tax.detail_id,
                                         tax_code = Tax.tax_code,
                                         tax_amount = Tax.tax_amount,
                                         tax_name = Tax.tax_name
                                     }).ToList();

                //Los IEPS repetidos los agrupo y el monto lo sumo.
                var GroupedTaxes = (from p in FilteredTaxes
                                    group p by p.tax_code into g
                                    select new DetailTaxes
                                    {
                                        id = g.First().id,
                                        detail_id = g.First().detail_id,
                                        tax_code = g.First().tax_code,
                                        tax_amount = g.Sum(p => p.tax_amount),
                                        tax_name = g.First().tax_name
                                    }).ToList();

                //Construyo el predicado para desplegar el IEPS por porcentajes.
                string ieps_predicate = null;
                decimal ieps_total_all_percentages = 0;

                foreach (DetailTaxes gTax in GroupedTaxes)
                {
                    ieps_predicate += "<td><div style ='width:60px'><font size=1><p>" + gTax.tax_name + " </br>" + gTax.tax_amount.ToString("C") + "</p></font></div></td>";
                    ieps_total_all_percentages += gTax.tax_amount;
                }

                if (ieps_predicate == null)
                    ieps_predicate = "Sin IEPS";

                //Asignar los valores del IEPS a la factura.
                ieps_predicate = "<table><tr>" + ieps_predicate + "</tr></table>";
                Invoice.IepsPredicate = ieps_predicate;
                Invoice.Ieps = ieps_total_all_percentages;
            }
            return Invoices;
        }

        public List<InvoiceReport> GetHeaders(List<INVOICES> HeadersFromDB)
        {
            var Headers = new List<InvoiceReport>();

            if (HeadersFromDB != null)
            {
                foreach (INVOICES HeaderFDB in HeadersFromDB)
                {
                    Headers.Add(ConvertToInvoice(HeaderFDB));
                }
            }

            return Headers;
        }

        public InvoiceReportModel BindInvoiceModel(InvoiceReportModel RM)
        {

            // B I N D     F I L T E R S
            List<CURRENCY> CurrencysFDB = GetAllCurrency();
            RM.CurrencyOptions = ToolsForReportInvoice.BindDDLCurrencyOptions(CurrencysFDB);
            RM.AmountOptions = ToolsForReportInvoice.BindDDLAmountOptions();
            RM.StatusOptions = ToolsForReportInvoice.BindDDLStateOptions();

            // B I N D     M O D E L
            List<INVOICES> InvoicesFDB = GetConstructPredicate(RM);

            RM.InvoicesReport = GetHeaders(InvoicesFDB);

            //List<DetailTaxes> Taxes = GetIeps(RM.InvoicesReport);
            RM.InvoicesReport = BindIeps(RM.InvoicesReport);

            RM.Totales = ToolsForReportInvoice.GetTotals(RM.InvoicesReport);

            return RM;
        }

        public InvoiceIndex GetListInvoiceModel(InvoiceIndex model)
        {
            model = _InvoiceRepository.GetInvoicesByModel(model);

            return model;
        }

        public List<SalesDetailReportModel> GetInvoiceReportDetail(long InvoiceNumber)
        {
            var CreditNotesBondeDetailList = _InvoiceRepository.GetAllInvoiceReportDetail(InvoiceNumber);
            List<SalesDetailReportModel> lisCreditNotesBondeDetailList = new List<SalesDetailReportModel>();
            SalesDetailReportModel model;
            foreach (SalesDetailReportModel sdrm in CreditNotesBondeDetailList)
            {
                decimal importe = 0;

                importe = (sdrm.part_importe - sdrm.part_ieps); // para calcular una sola vez el importe


                model = new SalesDetailReportModel();
                model.part_number = sdrm.part_number;
                model.part_name = sdrm.part_name;
                model.part_quantity = sdrm.part_quantity;
                model.part_price = sdrm.part_price;//-((decimal)(mt.tax_value / 100) * sld.detail_captured),
                model.part_importe = importe;
                model.part_iva = sdrm.part_iva;
                model.part_iva_percentage = sdrm.part_iva / (importe != 0 ? importe : 1) * 100;
                model.part_ieps = sdrm.part_ieps;// (decimal)(mt.tax_value / 100) * (decimal)sld.detail_captured,
                model.part_ieps_percentage = sdrm.part_ieps / (importe != 0 ? importe : 1) * 100;//(int)(mt.tax_value)
                model.part_total = importe + sdrm.part_ieps + sdrm.part_iva;
                lisCreditNotesBondeDetailList.Add(model);
            }
            return lisCreditNotesBondeDetailList;
        }

        public InvoiceModel GetInvoiceEditByNumber(long InvoiceNumber)
        {
            var model = new InvoiceModel();
            var invo = _InvoiceRepository.GetInvoiceByNumber(InvoiceNumber);
            model.Balance = invo.invoice_balance.Value;
            model.TotalAmount = invo.invoice_total;
            model.DueDate = invo.invoice_duedate.ToString(DateFormat.INT_DATE);
            model.InvoiceSerie = invo.invoice_serie;
            model.InvoiceNumber = invo.cfdi_id.Value;
            return model;
        }

        public InvoiceReport ConvertToInvoice(INVOICES HeaderFDB)
        {
            DIGITAL_TAX_STAMP DigitalTaxStampFDB = GetDigitalTaxStampByIdAndSerie(HeaderFDB.invoice_no, HeaderFDB.invoice_serie);
            var Header = new InvoiceReport();
            //Header.IepsPredicate = ieps_predicate;
            Header.InvoiceSerie = HeaderFDB.invoice_serie;
            Header.InvoiceNumber = HeaderFDB.invoice_no;
            Header.CreateDate = HeaderFDB.invoice_date;
            Header.CustomerCode = HeaderFDB.CUSTOMERS.customer_code;
            Header.CustomerName = HeaderFDB.CUSTOMERS.customer_name;
            Header.DueDate = HeaderFDB.invoice_duedate;
            Header.Amount = HeaderFDB.invoice_total - HeaderFDB.invoice_tax;
            //Header.Ieps = ;
            Header.CreditOrCash = HeaderFDB.is_credit ? "Crédito" : "Contado";
            Header.Balance = HeaderFDB.invoice_balance ?? 0;
            Header.CurrencyCode = HeaderFDB.cur_code != null ? HeaderFDB.cur_code : Currencies.MXN;
            if (DigitalTaxStampFDB != null && DigitalTaxStampFDB.UUID != null)
                Header.FiscalNumber = DigitalTaxStampFDB.UUID;
            else
                Header.FiscalNumber = "Sin Timbrar";
            if (!HeaderFDB.invoice_status)
                Header.Status = "Cancelada";
            else
            {
                if (HeaderFDB.is_credit)
                {
                    if (Header.Balance <= 0)
                        Header.Status = "Pagada";
                    else if (HeaderFDB.invoice_duedate.Date < DateTime.Now.Date)
                        Header.Status = "Vencida";
                }
                else
                {
                    Header.Status = "Pagada";
                }
            }
            if (Header.Status == "")
            {
                Header.Status = "Indefinida";
            }
            Header.Tax = HeaderFDB.invoice_tax;
            Header.TotalAmount = HeaderFDB.invoice_total;
            if (HeaderFDB.invoice_no == 27)
            {
                Header.InvoiceNumber = 27;
            }
            return Header;
        }
        
        public bool MatchTotalAndSubtotal(INVOICES Invoice)
        {
            try
            {
                decimal TotalDetails = 0, TotalTax = 0, DifferenceTotals = 0, CalculatedTotal = 0;
                int cont = 0; bool SumMoneyCents;
                var DetailsList = _InvoiceDetailRepository.GetDetailsByInvoice(Invoice.invoice_no, Invoice.invoice_serie);
                var DetailsTax = _InvoiceDetailTaxRepository.GetTaxesByInvoice(Invoice.invoice_no, Invoice.invoice_serie);
                var DetailsWithoutTax = _InvoiceDetailRepository.GetDetailsWithoutTaxByInvoice(Invoice.invoice_no, Invoice.invoice_serie);
                var ModifiedDetails = new List<INVOICE_DETAIL>();

                if (DetailsList.Count > 0)
                    TotalDetails = DetailsList.Sum(x => x.detail_price);

                if (DetailsTax.Count > 0)
                    TotalTax = DetailsTax.Sum(d => d.TaxAmount);

                CalculatedTotal = TotalTax + TotalDetails;

                if (Invoice.invoice_total > CalculatedTotal)
                {
                    DifferenceTotals = Invoice.invoice_total - CalculatedTotal;
                    SumMoneyCents = true;
                }
                else
                {
                    DifferenceTotals = CalculatedTotal - Invoice.invoice_total;
                    SumMoneyCents = false;
                }

                if (DifferenceTotals <= MoneyDifference.DifferenceRange)
                {
                    do
                    {
                        var Detail = DetailsList.First(c => c.detail_id == DetailsWithoutTax[cont].detail_id);

                        if (SumMoneyCents)
                            Detail.detail_price += MoneyDifference.AmountToOperate;
                        else
                            Detail.detail_price -= MoneyDifference.AmountToOperate;

                        _InvoiceDetailRepository.Update(Detail);
                        TotalDetails = DetailsList.Sum(d => d.detail_price);
                        CalculatedTotal = TotalTax + TotalDetails;
                        ModifiedDetails.Add(Detail);
                        cont++;
                    } while (Invoice.invoice_total != CalculatedTotal);
                }
                return true;
            }

            catch (Exception ex)
            {
                string msg = ex.Message;
                return false;
            }
        }        

        public bool MatchHeaderTaxDetailTax(INVOICES Invoice, InvoicesSales Model)
        {
            try
            {
                var DetailsTax = _InvoiceDetailTaxRepository.GetTaxesByInvoice(Invoice.invoice_no, Invoice.invoice_serie);
                if (DetailsTax.Count > 0)
                {                    
                    Invoice.invoice_tax = DetailsTax.Sum(d => d.TaxAmount);
                    Invoice.udate = DateTime.Now;
                    Invoice.uuser = Invoice.cuser;

                    if (_InvoiceRepository.UpdateInvoice(Invoice, null))
                        return true;
                }
                return false;
            }            
            catch (Exception ex)
            {
                string msg = ex.Message;
                return false;
            }
        }

        public bool SprocUpdateInvoiceNumber() => _InvoiceRepository.SprocUpdateInvoiceNumber();
    }
}
