﻿using App.DAL.Invoice;
using App.Entities;
using App.Entities.ViewModels;
using System.Collections.Generic;

namespace App.BLL.Invoices
{
    public class InvoiceDetailTaxBusiness
    {
        private InvoiceDetailTaxRepository _InvoiceDetailTaxRepository;

        public InvoiceDetailTaxBusiness()
        {
            _InvoiceDetailTaxRepository = new InvoiceDetailTaxRepository();
        }

        public bool CreateInvoiceDetailTax(List<INVOICE_DETAIL_TAX> InvoiceDetailTaxList)
        {
            return _InvoiceDetailTaxRepository.CreateInvoiceDetailTax(InvoiceDetailTaxList);
        }

        public bool DeleteDetailTax(long InvoiceNumber, string InvoiceSerie)
        {
            return _InvoiceDetailTaxRepository.DeleteDetailTax(InvoiceNumber, InvoiceSerie);
        }

        public List<INVOICE_DETAIL_TAX> DetailsTaxForInvoiceSubstitute(string Serie, long InvoiceNo, long NewInvoiceNo)
        {
            return _InvoiceDetailTaxRepository.DetailsTaxForInvoiceSubstitute(Serie, InvoiceNo, NewInvoiceNo);
        }

        public List<DetailTax> GetTaxesByInvoice(long Number, string Serie)
        {
            return _InvoiceDetailTaxRepository.GetTaxesByInvoice(Number, Serie);
        }

        public List<DetailTax> GetTaxesInfoByInvoiceDetailId(long InvoiceDetailid) => _InvoiceDetailTaxRepository.GetTaxesInfoByInvoiceDetailId(InvoiceDetailid);
    }
}
