﻿using App.DAL.Invoice;
using App.Entities;
using App.Entities.ViewModels;
using App.Entities.ViewModels.Common;
using App.Entities.ViewModels.Invoices;
using System.Collections.Generic;

namespace App.BLL.Invoices
{
    public class InvoiceDetailBusiness
    {
        private InvoiceDetailRepository _InvoiceDetailRepository;

        public InvoiceDetailBusiness()
        {
            _InvoiceDetailRepository = new InvoiceDetailRepository();
        }

        public bool CreateInvoiceDetail(List<InvoiceDetailsInsert> InvoiceDetailsList) => _InvoiceDetailRepository.CreateInvoiceDetail(InvoiceDetailsList);

        public INVOICE_DETAIL GetInvoiceDetailsById(long InvoiceDetailId) => _InvoiceDetailRepository.GetInvoiceDetailsById(InvoiceDetailId);

        public List<INVOICE_DETAIL> GetDetailsByInvoice(long Number, string Serie) => _InvoiceDetailRepository.GetDetailsByInvoice(Number, Serie);

        public List<InvoiceProductsDetails> InvoiceProductsList(long? InvoiceNumber, long? SaleNumber) => _InvoiceDetailRepository.InvoiceProductsList(InvoiceNumber, SaleNumber);

        public bool HasInvoiceItemsAvailable(long InvoiceNumber) => _InvoiceDetailRepository.HasInvoiceItemsAvailable(InvoiceNumber);

        public List<InvoiceDetailsInsert> GetInvoiceDetailsFromSalesDetails(string InvoSerie, long InvoNumber, long SaleNumber, bool isCurrencyUSD) => _InvoiceDetailRepository.GetInvoiceDetailsFromSalesDetails(InvoSerie, InvoNumber, SaleNumber, isCurrencyUSD);

        public List<DetailTax> InvoiceDetailsWithTax(string InvoSerie, long InvoNumber, long SaleNumber, bool isCurrencyUSD) => _InvoiceDetailRepository.InvoiceDetailsWithTax(InvoSerie, InvoNumber, SaleNumber, isCurrencyUSD);        

        public List<DetailTax> SpecialInvoiceDetails(string Serie, long Number, List<CFDIConcept> Concepts) => _InvoiceDetailRepository.SpecialInvoiceDetailsTax(Serie, Number, Concepts);        

        public bool Update(INVOICE_DETAIL InvoiceDetail) => _InvoiceDetailRepository.Update(InvoiceDetail);        

        public List<InvoiceDetailsInsert> InvoiceGlobal(string InvoSerie, long InvoNumber, decimal InvoTotal) => _InvoiceDetailRepository.InvoiceGlobal(InvoSerie, InvoNumber, InvoTotal);        

        public INVOICE_DETAIL GetDetailByInvoice(long Number, string Serie) => _InvoiceDetailRepository.GetDetailByInvoice(Number, Serie);        

        public bool DeleteInvoiceDetails(long Number, string Serie) => _InvoiceDetailRepository.DeleteInvoiceDetails(Number, Serie);        
    }
}
