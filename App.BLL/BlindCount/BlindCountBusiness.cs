﻿using App.BLL.PurchaseOrder;
using App.DAL.BlindCount;
using App.Entities;
using App.Entities.ViewModels.Android;
using App.Entities.ViewModels.Item;
using System;
using System.Collections.Generic;

namespace App.BLL.BlindCount
{
    public class BlindCountBusiness
    {
        private readonly BlindCountRepository _blindCountRepository;

        public BlindCountBusiness()
        {
            _blindCountRepository = new BlindCountRepository();
        }
        public DateTime? GetBlincCountExists(string purchase_no) => _blindCountRepository.GetBlincCountExists(purchase_no);

        public BLIND_COUNT GetBlindCountByOCItemBarcodeSite(string purchase_no, string part_number, string barcode, string site) => _blindCountRepository.GetBlindCountByOCItemBarcodeSite(purchase_no, part_number, barcode, site);

        public List<BlindCountModel> GetBlindCountByOC(string purchase_no) => _blindCountRepository.GetBlindCountByOC(purchase_no);

        public bool RemoveZeroBlindCount(string purchase_no) => _blindCountRepository.RemoveZeroBlindCount(purchase_no);

        public bool UpdateBlindCount(BLIND_COUNT blindCount, string quantityExist, string quantity, int position, string user)
        {
            blindCount.quantity = decimal.Parse(quantityExist) + Decimal.Parse(quantity);
            blindCount.udate = DateTime.Now;
            blindCount.uuser = user;
            if (position != 0) //Solo actualizar la posicon si es un nuevo registro
                blindCount.item_position = position;
            var check = false;
            if (_blindCountRepository.UpdateBlindCount(blindCount) > 0)
                check = true;
           
            return check;
        }

        public bool AddBlindCount(string purchase_no, string site, string part_number, string barcode, string cantidad, int position, string user)
        {
            BLIND_COUNT blind = new BLIND_COUNT
            {
                purchase_no = purchase_no,
                site_code = site,
                part_number = part_number,
                part_barcode = barcode,
                quantity = decimal.Parse(cantidad),
                cdate = DateTime.Now,
                cuser = user,
                udate = DateTime.Now,
                program_id = "Android"
            };
            if (position != 0) //Cuando no exita
                blind.item_position = position;
            var flag = false;
            flag = _blindCountRepository.AddBlindCount(blind);

            return flag;
        }

        public List<ItemBlindCount> GetBlindCountListview(string purchase_no, string site) => _blindCountRepository.GetBlindCountListview(purchase_no, site);
    }
}