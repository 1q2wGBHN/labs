﻿using App.BLL.Site;
using App.DAL.MaCode;
using App.Entities;
using App.Entities.ViewModels.MaCode;
using System.Collections.Generic;
using System.Linq;

namespace App.BLL.MaCode
{
    public class MaCodeBusiness
    {
        private readonly MaCodeRepository _MaCodeRepo;
        private readonly SiteConfigBusiness _siteConfigBusiness;

        public MaCodeBusiness()
        {
            _MaCodeRepo = new MaCodeRepository();
            _siteConfigBusiness = new SiteConfigBusiness();
        }

        public List<MaCodeModel> GetCurrency()
        {
            var Currency = _MaCodeRepo.GetListCurrency()
                .Select(x => new MaCodeModel
                {
                    VKey = x.vkey

                }).ToList();
            return Currency;
        }

        public List<MaCodeModel> GetListTypeLocations() => _MaCodeRepo.GetListTypeLocations();

        public List<MaCodeModel> GetListTypeDamagedMerchadiseReasons() => _MaCodeRepo.GetListTypeDamagedMerchadiseReasons();

        public List<MA_CODE> GetListTypeOffer() => _MaCodeRepo.GetListTypeOffer();

        public List<MA_CODE> GetListGeneric(string Generic) => _MaCodeRepo.GetListGeneric(Generic);

        public int GetPermisionXML(string rfc_supplier) => _MaCodeRepo.GetPermisionXML(_siteConfigBusiness.GetSiteCode(), rfc_supplier);
        public List<MA_CODE> GetAllCodeByCode(string code)=>_MaCodeRepo.GetAllCodeByCode(code);
        public string GetEmailDistrict() => _MaCodeRepo.GetEmailDistrict();

        public bool GetWeighingMachineCAS() => _MaCodeRepo.GetWeighingMachineCAS();
    }
}