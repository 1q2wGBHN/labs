﻿using App.DAL.InternalRequirement;
using App.Entities.ViewModels.InternalRequirement;
using App.GlobalEntities;
using System;
using System.Collections.Generic;
using System.Linq;

namespace App.BLL.InternalRequirement
{
    public class InternalRequirementBusiness
    {
        private readonly InternalRequirementRepository _internalRequirementRepository;

        public InternalRequirementBusiness()
        {
            _internalRequirementRepository = new InternalRequirementRepository();
        }

        public List<InternalRequirementModel> GetAllItemsCreditors() => _internalRequirementRepository.GetAllItemsCreditors();

        public List<InternalRequirementDetailPOModel> GetRequirementsAll()
        {
            var items = _internalRequirementRepository.GetRequirementsAll();
            decimal? total = 0;
            decimal quantity = 0;
            foreach (var item in items)
            {
                quantity += item.quantity_total;
                if (item.total_total != null)
                    total += item.total_total;
                else
                    total += 0;
            }
            items[0].quantity = quantity;
            items[0].total = total;
            items[0].folio = _internalRequirementRepository.GetCountRequirementFolios();

            return items;
        }

        public int GetCountRequirementFolios() => _internalRequirementRepository.GetCountRequirementFolios();

        public string AddInternalRequierement(List<InternalRequirementModel> list, string user, string site)
        {
            var listBuyerDivisión = list.Select(s => s.buyer_division).Distinct().OrderBy(x => x).ToList();
            string folios = "";

            while (listBuyerDivisión.Count() > 0)
            {
                try
                {
                    //Delimitar productos por division
                    var listProducts = list.Where(w => w.buyer_division == listBuyerDivisión[0]).ToList();

                    InternalRequirementModel header = new InternalRequirementModel
                    {
                        site = site,
                        cdate = DateTime.Now,
                        cuser = user
                    };
                    INTERNAL_REQUIREMENT_HEADER internal_header = new INTERNAL_REQUIREMENT_HEADER
                    {
                        cdate = DateTime.Now,
                        cuser = user,
                        buyer_division = listBuyerDivisión[0],
                        program_id = "PURC005.cshtml",
                        requirement_status = 0,
                        site_code = site,
                        requirement_type = "INTERNO"
                    };
                    var id = _internalRequirementRepository.AddInternalRequirementHeader(internal_header);
                    if (id != 0)
                    {
                        for (int i = 0; i < listProducts.Count; i++)
                        {
                            InternalRequirementDetailPOModel detail = new InternalRequirementDetailPOModel
                            {
                                cdate = DateTime.Now,
                                cuser = user,
                                folio = id,
                                supplier_part_number_creditors = listProducts[i].id_item_supplier,
                                quantity = listProducts[i].quantity,
                                program_id = "PURCH020.cshtml"
                            };
                            _internalRequirementRepository.AddInternalRequirementDetail(detail);
                        }
                        //Eliminar la división de comprador que se adquirio
                        listBuyerDivisión.RemoveAt(0);
                        //Verificar si existe el ultimo id para enviarlo
                        if (listBuyerDivisión.Count() > 0)
                            folios = folios + id.ToString() + ", ";
                        else
                            folios = folios + id.ToString() + " ";
                    }
                    else
                        return "0";
                }
                catch
                {
                    return "0";
                }
            }
            return folios;
        }

        public List<InternalRequirementPOModel> GetAllRequirementsHeaderBySite(string site, DateTime DateInit, DateTime DateFin)
        {
            var list = _internalRequirementRepository.GetAllRequirementsHeaderBySite(site, DateInit, DateFin);
            if (list != null)
            {
                for (int i = 0; i < list.Count; i++)
                {
                    if (list[i].requirement_status == "0")
                        list[i].requirement_status = "Inicial";
                    else if (list[i].requirement_status == "1")
                        list[i].requirement_status = "Enviada a proveedores";
                    else if (list[i].requirement_status == "8")
                        list[i].requirement_status = "Cancelada";
                }
                return list;
            }
            return new List<InternalRequirementPOModel>();
        }

        public List<InternalRequirementDetailPOModel> GetAllRequirementsDetails(string folio) => _internalRequirementRepository.GetAllRequirementsDetails(folio);

        public List<InternalRequirementDetailPOModel> GetRequirementsDetailsBySupplierBySite(int supplier_id, string site_code) => _internalRequirementRepository.GetRequirementsDetailsBySupplierBySite(supplier_id, site_code);

        public List<InternalRequirementDetailPOModel> GetRequirementsInSupplierAll() => _internalRequirementRepository.GetRequirementsInSupplierAll();

        public List<InternalRequirementPOModel> GetAllRequirementsHeaderBySiteByDate(string site, DateTime DateInit, DateTime DateFin)
        {
            var list = _internalRequirementRepository.GetAllRequirementsHeaderBySiteByDate(site, DateInit, DateFin);
            for (int i = 0; i < list.Count; i++)
            {
                if (list[i].requirement_status == "0")
                    list[i].requirement_status = "Inicial";
                else if (list[i].requirement_status == "1")
                    list[i].requirement_status = "Enviada a proveedores";
            }
            return list;
        }

        public List<InternalRequirementDetailPOModel> GetRequirementsInSiteAll() => _internalRequirementRepository.GetRequirementsInSiteAll();

        public List<InternalRequirementModel> SearchItemsCreditors(string search) => _internalRequirementRepository.SearchItemsCreditors(search);
    }
}