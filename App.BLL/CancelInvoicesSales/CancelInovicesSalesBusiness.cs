﻿using App.DAL.CancelInovicesSales;
using App.DAL.Customer;
using App.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App.BLL.CancelInvoicesSales
{
    public class CancelInovicesSalesBusiness
    {
        private CancelInovicesSalesRepository _CancelInovicesSalesRepository;
        private CustomerRepository _CustomerRepository;

        public CancelInovicesSalesBusiness()
        {
            _CancelInovicesSalesRepository = new CancelInovicesSalesRepository();
            _CustomerRepository = new CustomerRepository();
        }

        public bool CreateRecord(INVOICES Invoice, long SaleId, string RFC)
        {
            var Item = new CANCEL_INVOICES_SALES();            
            Item.sale_id = SaleId;
            Item.invoice_no = Invoice.invoice_no;
            Item.customer_code = Invoice.customer_code;
            Item.customer_rfc = RFC;

            return _CancelInovicesSalesRepository.CreateRercord(Item);
        }

        public bool HasSaleCancelInovice(long SaleId) => _CancelInovicesSalesRepository.HasSaleCancelInovice(SaleId);
    }
}
