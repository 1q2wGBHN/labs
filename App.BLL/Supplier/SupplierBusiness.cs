﻿using App.DAL.Site;
using App.DAL.Supplier;
using App.Entities;
using App.GlobalEntities;
using App.Entities.ViewModels.Supplier;
using System;
using System.Collections.Generic;
using App.GlobalEntities.ViewModels.Supplier;

namespace App.BLL.Supplier
{
    public class SupplierBusiness
    {
        private readonly MaSupplierRepository _Supplier;
        private readonly SiteConfigRepository _SiteConfigRepo;
        private MaSupplierContactRepository _supplierContactRepository;

        public SupplierBusiness()
        {
            _Supplier = new MaSupplierRepository();
            _SiteConfigRepo = new SiteConfigRepository();
            _supplierContactRepository = new MaSupplierContactRepository();

        }

        public List<MaSupplierModel> GetAll() => _Supplier.GetAll();

        public List<MaSupplierModel> SearchSupplier(string supplier) => _Supplier.SearchSupplier(supplier);

        public List<MaSupplierModel> GetAllOrderFormat() => _Supplier.GetAllOrderFormat();

        public List<MaSupplierModel> GetAllDevolution() => _Supplier.GetAllDevolution();

        public List<Entities.MA_SUPPLIER> GetAllOnlySuppliers() => _Supplier.GetAllOnlySuppliers();

        public List<MaSupplierModel> GetAllOnlyCreditors() => _Supplier.GetAllOnlyCreditors();

        public List<MaSupplierModel> GetAllByType(string Type) => _Supplier.GetAllByType(Type);

        public List<MaSupplierModel> GetAllSupLike(String name) => _Supplier.GetAllSupLike(name);

        public MaSupplierModel GetSupplierInfoById(int supplier_id) => _Supplier.GetSupplierInfoById(supplier_id);

        public List<MaSupplierModel> GetAllMerchandiseEntry(int MerchandiseEntry) => _Supplier.GetAllMerchandiseEntry(MerchandiseEntry);

        public List<MaSupplierModel> GetAllSuppliersFootTruckInStatus1() => _Supplier.GetAllSuppliersFootTruckInStatus1();

        public MaSupplierModel GetSupplierDamagedsGood(int supplier) => _Supplier.GetSupplierDamagedsGood(supplier);

        public List<MaSupplierModel> GetAllSuppliersApplyReturnInStatus1() => _Supplier.GetAllSuppliersApplyReturnInStatus1();

        public string GetNameSupplier(int Supplier) => _Supplier.GetNameSupplier(Supplier);

        public int GetNameSupllierByfolio(string Folio) => _Supplier.GetNameSupllierByfolio(Folio);


        public List<MaSupplierModel> RemisionSupplierId()
        {
            List<MaSupplierModel> modelSuppliers = new List<MaSupplierModel>();
            var site_code = _SiteConfigRepo.GetSiteCode();

            if (site_code == "0002" || site_code == "0032") //Rosaritos
            {
                MaSupplierModel m1 = new MaSupplierModel();
                MaSupplierModel m2 = new MaSupplierModel();
                m1 = _Supplier.RemisionSupplierId(12917);
                m2 = _Supplier.RemisionSupplierId(13338);
                modelSuppliers.Add(m1);
                modelSuppliers.Add(m2);
            } else if (site_code == "0008") //La Mesa
            {
                MaSupplierModel m1 = new MaSupplierModel();
                MaSupplierModel m2 = new MaSupplierModel();
                m1 = _Supplier.RemisionSupplierId(12917);
                m2 = _Supplier.RemisionSupplierId(13328);
                modelSuppliers.Add(m1);
                modelSuppliers.Add(m2);

            } else if(site_code == "0014" || site_code == "0015" || site_code == "0026" || site_code == "0029") //Ensenada
            {
                MaSupplierModel m1 = new MaSupplierModel();
                MaSupplierModel m2 = new MaSupplierModel();
                m1 = _Supplier.RemisionSupplierId(12917);
                m2 = _Supplier.RemisionSupplierId(13385);
                modelSuppliers.Add(m1);
                modelSuppliers.Add(m2);
            }
            else //Todos{
            {
                MaSupplierModel m1 = new MaSupplierModel();
                m1 = _Supplier.RemisionSupplierId(12917);
                modelSuppliers.Add(m1);
            }
            return modelSuppliers;

        }

        // Global Entities
        public bool AddSupplier(App.GlobalEntities.MA_SUPPLIER_G supplier)
        {
            bool check = false;
            if (_Supplier.AddSupplier(supplier) > 0) { check = true; }
            return check;
        }
        public GlobalEntities.MA_SUPPLIER_G RfcExists(string rfc, string type)
        {
            var x = _Supplier.GetByRfcAndType(rfc, type);
            return x;
        }
        public GlobalEntities.MA_SUPPLIER_G GetByRFC2(string rfc)
        {
            return _Supplier.GetByRFC2(rfc);
        }
        public string EditSupplier(string RFC, string type, string user, List<SupplierContactsEditModel> contacts)
        {
            var supplier = _Supplier.GetByRfc(RFC);
            if (type == "Acreedor")
            {
                supplier.truck_foot = 0;
                supplier.merchandise_entry = 0;
                supplier.apply_return = 0;
            }
            supplier.cdate = DateTime.Now;
            supplier.cuser = user;
            supplier.supplier_type = type;
            supplier.program = "PURCH002.cshtml";
            var x = _Supplier.EditSupplier(RFC, type, supplier);
            if (x.Item2 != 0)
            {
                if (contacts != null)
                {
                    foreach (var item in contacts)
                    {
                        GlobalEntities.MA_SUPPLIER_CONTACT_G newContact = new GlobalEntities.MA_SUPPLIER_CONTACT_G();
                        newContact.supplier_id = x.Item2;
                        newContact.name = item.name;
                        newContact.last_name = item.last_name;
                        newContact.phone = item.phone;
                        newContact.email = item.email;
                        newContact.departament = item.department;
                        newContact.cuser = user;
                        newContact.flagActive = true;
                        newContact.cdate = DateTime.Now;
                        newContact.program_id = "PURCH002.cshtml";
                        _supplierContactRepository.AddSupplierContact(newContact);
                    }
                }
            }
            return x.Item1;
        }
        public List<SupplierModel> GetAllSuppliersAndCreditorInModel()
        {
            return _Supplier.GetAllSuppliersAndCreditorInModel();
        }
        public GlobalEntities.MA_SUPPLIER_G GetById(int id)
        {
            return _Supplier.GetById(id);
        }
        public bool EditSupplierPartIva(int supplier, string part_iva, string uuser)
        {
            return _Supplier.EditSupplierPartIva(supplier, part_iva, uuser);
        }
        public int EditSupplierPartIvaMetohd(int supplier, string part_iva, string uuser)
        {
            if (EditSupplierPartIva(supplier, part_iva, uuser))
                return _Supplier.UpdateItemIVABySupplier(supplier, uuser);
            return 0;
        }
        public bool UpdateSupplier(GlobalEntities.MA_SUPPLIER_G supplier)
        {
            var check = false;
            if (_Supplier.UpdateSupplier(supplier) > 0) check = true;
            return check;
        }
    }
}