﻿using App.DAL.Supplier;
using App.Entities.ViewModels.Creditors;
using App.Entities.ViewModels.Supplier;
using App.GlobalEntities;
using System.Collections.Generic;

namespace App.BLL.Supplier
{
    public class MaSupplierContactBusiness
    {
        private readonly MaSupplierContactRepository _SupplierContact;

        public MaSupplierContactBusiness()
        {
            _SupplierContact = new MaSupplierContactRepository();
        }

        public string Email(int Supplier, string Departament) => _SupplierContact.Email(Supplier, Departament);

        public MaSupplierModel InfoBasicContactContabilidad(int supplier_id) => _SupplierContact.InfoBasicContactContabilidad(supplier_id);

        public string EmailsGeneric(int Supplier, string Departament) => _SupplierContact.EmailsGeneric(Supplier, Departament);

        //Global Contact Business

        public bool AddSupplierContact(MA_SUPPLIER_CONTACT_G supplierContact)
        {
            bool check = false;
            if (_SupplierContact.AddSupplierContact(supplierContact) > 0) { check = true; }
            return check;
        }
        public List<SupplierContactModel> GetAllContactsBySupplierId(string supplier_id)
        {
            return _SupplierContact.GetAllContactsBySupplierId(supplier_id);
        }
        public bool UpdateSupplierContact(MA_SUPPLIER_CONTACT_G supplierContact)
        {
            var check = false;
            if (_SupplierContact.UpdateSupplierContact(supplierContact) > 0) check = true;
            return check;
        }
        public MA_SUPPLIER_CONTACT_G GetContactById(int id)
        {
            return _SupplierContact.GetContactById(id);
        }

        public CreditorPOModel GetContactFirstByDeparment(int id_supplier, string deparment)
        {
            return _SupplierContact.GetContactFirstByDeparment(id_supplier, deparment);
        }

        public string GetContactAllByDeparment(int id_supplier, string deparment)
        {
            return _SupplierContact.GetContactAllByDeparment(id_supplier, deparment);
        }

        //End global 
    }
}