﻿using App.DAL.TransactionsCustomers;
using App.Entities;
using App.Entities.ViewModels.Common;
using App.Entities.ViewModels.Customers;
using App.Entities.ViewModels.TransactionsCustomers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static App.Entities.ViewModels.Common.Constants;

namespace App.BLL.TransactionsCustomers
{
    public class TransactionsCustomersBusiness
    {
        private TransactionsCustomersRepository _TransactionsCustomersRepository;

        public TransactionsCustomersBusiness()
        {
            _TransactionsCustomersRepository = new TransactionsCustomersRepository();
        }

        public bool CreateTransactions(INVOICES Invoice, CREDIT_NOTES CreditNote, PAYMENTS_DETAIL Payment, bool IsBonification)
        {
            var Model = new TransactionsCustomersModel();
            if (Invoice != null)
            {
                Model.CustomerCode = Invoice.customer_code;
                Model.Type = "C"; //stands for Cargo
                Model.Amount = Invoice.invoice_total;
                Model.Date = DateTime.Now;
                Model.InvoiceNo = Invoice.invoice_no;
                Model.InvoiceSerie = Invoice.invoice_serie;
                Model.UserName = Invoice.cuser;
                Model.Currency = Invoice.cur_code;
                Model.SiteCode = Invoice.site_code;
                Model.Reference = string.Format("{0}{1}", "Factura No. ", Invoice.invoice_no);

                goto ReturnMethod;
            }

            if (CreditNote != null)
            {
                Model.CustomerCode = CreditNote.customer_code;
                Model.Type = "A"; //stands for Abono
                Model.Amount = CreditNote.cn_total;
                Model.Date = DateTime.Now;
                Model.InvoiceNo = CreditNote.invoice_no;
                Model.InvoiceSerie = CreditNote.invoice_serie;
                Model.UserName = CreditNote.cuser;
                Model.Currency = CreditNote.cur_code;
                Model.SiteCode = CreditNote.site_code;
                Model.CreditNoteNo = CreditNote.cn_id;
                if (IsBonification)
                    Model.Reference = string.Format("{0}{1}", "Bonificación No. ", CreditNote.cn_id);
                else
                    Model.Reference = string.Format("{0}{1}", "Nota de Crédito No. ", CreditNote.cn_id);

                goto ReturnMethod;
            }

            if (Payment != null)
            {
                Model.CustomerCode = Payment.PAYMENTS.customer_code;
                Model.Type = "A"; //stands for Abono
                Model.Amount = Payment.amount;
                Model.Date = DateTime.Now;
                Model.InvoiceNo = Payment.invoice_no;
                Model.InvoiceSerie = Payment.invoice_serie;
                Model.UserName = Payment.PAYMENTS.cuser;
                Model.Currency = Payment.PAYMENTS.cur_code;
                Model.SiteCode = Payment.PAYMENTS.site_code;
                Model.ReceiptNo = Payment.payment_no;
                Model.Reference = string.Format("{0}{1} - {2}", "Recibo No. ", Payment.payment_no, Payment.PAYMENTS.PAYMENTS_PM.First().PAYMENT_METHOD.pm_name);

                goto ReturnMethod;
            }

        ReturnMethod:
            return _TransactionsCustomersRepository.CreateTransactions(Model.TSCUSTOMER);
        }

        public List<TransactionsCustomersItems> GetReportData(TransactionsCustomersReport Model) => _TransactionsCustomersRepository.GetReportData(Model);

        public bool DeleteTransactions(DeleteModel Model) => _TransactionsCustomersRepository.DeleteTransactions(Model);
    }
}
