﻿using App.DAL.Sanctions;
using App.Entities;
using App.Entities.ViewModels.Sanctions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App.BLL.Sanctions
{
    public class SanctionsBusiness
    {
        private SanctionsRepository _SanctionsRepository;
        public SanctionsBusiness()
        {
            _SanctionsRepository = new SanctionsRepository();
        }
        public List<USER_MASTER> GetUsers() => _SanctionsRepository.GetUsers();
        
        public List<SANCTIONS_MOTIVES> GetMotives() => _SanctionsRepository.GetMotives();
        
        public List<CURRENCY> GetCurrencys() => _SanctionsRepository.GetCurrencyList();
        
        public bool CreateSanction(SANCTIONS sanction) => _SanctionsRepository.CreateSanction(sanction);
        
        public bool UpdateSanction(SANCTIONS sanction, SYS_LOG SysLog) => _SanctionsRepository.UpdateSanction(sanction, SysLog);
        
        public List<SanctionModel> GetSantiosByModel(SanctionIndex model) => _SanctionsRepository.GetSantiosByModel(model);
        
        public SanctionModel GetSanctionById(long Id) => _SanctionsRepository.GetSanctionById(Id);

        public SANCTIONS GetSanctionEntityById(long SanctionId) => _SanctionsRepository.GetSanctionEntityById(SanctionId);
        
    }
}
