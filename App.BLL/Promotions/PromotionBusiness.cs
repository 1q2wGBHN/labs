using App.DAL.Promotions;
using App.Entities.ViewModels.Promotion;
using System;
using System.Collections.Generic;

namespace App.BLL.Promotions
{
    public class PromotionBusiness
    {
        private readonly PromotionRepository _promotionRepository;

        public PromotionBusiness()
        {
            _promotionRepository = new PromotionRepository();
        }

        public List<PromotionReportModel> GetAllPromotions(string TypePromotion, string PartNumber, DateTime? BeginDate, DateTime? EndDate) => _promotionRepository.PromotionReport(TypePromotion, PartNumber, BeginDate, EndDate);

        public List<PromotionItem> GetPromotionId(string promotion_code, string type)
        {
            if (type == "AVANZADA" || type == "Avanzada")
                return _promotionRepository.GetPromotionAdvancedByCode(promotion_code);
            else
                return _promotionRepository.GetPromotionByCode(promotion_code);
        }

        public List<PromotionItem> GetPromotionAdvancedCommonByCode(string promotion_code, string common) => _promotionRepository.GetPromotionAdvancedCommonByCode(promotion_code, common);

        public List<PromotionModel> GetPromotionHistoryByPartNmber(string part_number) => _promotionRepository.GetPromotionHistoryByPartNmber(part_number);
    }
}