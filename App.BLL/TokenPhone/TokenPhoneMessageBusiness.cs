﻿using App.DAL.TokenPhone;
using App.Entities;

namespace App.BLL.TokenPhone
{
    public class TokenPhoneMessageBusiness
    {
        private readonly TokenPhoneMessageRepository _tokenPhoneMessageRepository;

        public TokenPhoneMessageBusiness()
        {
            _tokenPhoneMessageRepository = new TokenPhoneMessageRepository();
        }

        public TOKEN_PHONE_MESSAGE SaveMessage(int id_token_send_to, string title, string message, int type_notification, string user_send_to, string cuser, string program_id) => _tokenPhoneMessageRepository.SaveMessage(id_token_send_to, title, message, type_notification, user_send_to, cuser, program_id);

        public int MessageNotSent(TOKEN_PHONE_MESSAGE message) => _tokenPhoneMessageRepository.MessegeNotSent(message);
    }
}