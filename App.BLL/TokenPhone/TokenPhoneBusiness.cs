﻿using App.DAL.TokenPhone;
using App.Entities;

namespace App.BLL.TokenPhone
{
    public class TokenPhoneBusiness
    {
        private readonly TokenPhoneRepository _tokenPhoneRepository;

        public TokenPhoneBusiness()
        {
            _tokenPhoneRepository = new TokenPhoneRepository();
        }

        public TOKEN_PHONE SearchUserByUserName(string user) => _tokenPhoneRepository.SearchUserByUserName(user);

        public int SearchTokenRepeat(int id_token) => _tokenPhoneRepository.SearchTokenRepeat(id_token);

        public int UpdateUsertoken(TOKEN_PHONE userInfo, int id_token) => _tokenPhoneRepository.UpdateUserToken(userInfo, id_token);

        public int CreationUserToken(string user, int id_token) => _tokenPhoneRepository.CreationUserToken(user, id_token);
    }
}