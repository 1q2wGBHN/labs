﻿using App.DAL.TokenPhone;
using App.Entities;
using App.Entities.ViewModels.TokenPhone;

namespace App.BLL.TokenPhone
{
    public class TokenPhoneDetailBusiness
    {
        private readonly TokenPhoneDetailRepository _tokenPhoneDetailRepository;

        public TokenPhoneDetailBusiness()
        {
            _tokenPhoneDetailRepository = new TokenPhoneDetailRepository();
        }

        public TOKEN_PHONE_DETAIL SearchToken(string token) => _tokenPhoneDetailRepository.SearchToken(token);

        public int CreationTokenPhone(string user, string token, string model, string brand, string sdk, string id_phone) => _tokenPhoneDetailRepository.CreationTokenPhone(user, token, model, brand, sdk, id_phone);

        public TokenPhoneModel SearchTokenByUser(string user) => _tokenPhoneDetailRepository.SearchTokenByUser(user);
    }
}