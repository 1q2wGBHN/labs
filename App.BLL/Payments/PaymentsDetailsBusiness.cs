﻿using App.DAL.Payments;
using App.Entities;
using App.Entities.ViewModels.Invoices;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App.BLL.Payments
{
    public class PaymentsDetailsBusiness
    {
        private PaymentsDetailsRepository _PaymentsDetailsRepository;

        public PaymentsDetailsBusiness()
        {
            _PaymentsDetailsRepository = new PaymentsDetailsRepository();
        }

        public List<InvoiceBalanceDetails> PaymentsDetailsByPaymentMethodId(long PaymentId)
        {
            return _PaymentsDetailsRepository.PaymentsDetailsByPaymentMethodId(PaymentId);
        }

        public PAYMENTS_DETAIL GetPaymentDetailById(long PaymentDetailId)
        {
            return _PaymentsDetailsRepository.GetPaymentDetailById(PaymentDetailId);
        }
    }
}
