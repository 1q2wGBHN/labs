﻿using App.BLL.TransactionsCustomers;
using App.DAL.Invoice;
using App.DAL.Lookups;
using App.DAL.Payments;
using App.Entities;
using App.Entities.ViewModels.Payments;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using static App.Entities.ViewModels.Common.Constants;

namespace App.BLL.Payments
{
    public class PaymentsPMBusiness
    {
        private PaymentsPMRepository _PaymentsPMRepository;
        private InvoiceRepository _InvoiceRepository;
        private PaymentsDetailsRepository _PaymentDetailsRepository;
        private TransactionsCustomersBusiness _TransactionsCustomersBusiness;
        private PaymentsRepository _PaymentsRepository;
        private CurrencyDetailRepository _CurrencyDetailRepository;

        public PaymentsPMBusiness()
        {
            _PaymentsPMRepository = new PaymentsPMRepository();
            _InvoiceRepository = new InvoiceRepository();
            _PaymentDetailsRepository = new PaymentsDetailsRepository();
            _TransactionsCustomersBusiness = new TransactionsCustomersBusiness();
            _PaymentsRepository = new PaymentsRepository();
            _CurrencyDetailRepository = new CurrencyDetailRepository();
        }

        public bool CreatePaymentPM(Payment Model)
        {
            bool flag = true;
            var PAYMENT = _PaymentsRepository.GetPaymentByNumber(Model.Number);
            foreach(var Item in Model.Payments)
            {
                PAYMENTS_PM PaymentPM = new PAYMENTS_PM();
                PaymentPM.amount = Item.Amount;
                PaymentPM.payment_method_id = Item.PaymentMethodId;
                PaymentPM.payment_no = Model.Number;
                PaymentPM.payment_serie = Model.Serie;
                PaymentPM.operation_number = Item.OperationNumber;                
                PaymentPM.date = DateTime.ParseExact(Item.PaymentDate, DateFormat.INT_DATE, CultureInfo.InvariantCulture);
                PaymentPM.bank = Item.Bank;
                PaymentPM.bank_reference = Item.BankReference;
                PaymentPM.cur_rate_sat = _CurrencyDetailRepository.GetCurrencySatRateByDate(Item.PaymentDate);

                flag = _PaymentsPMRepository.CreatePaymentPM(PaymentPM);
                if (flag)
                {
                    foreach (var PaymentDetailModel in Model.Invoices.Where(c => c.PaymentId == Item.ViewPaymentPMId))
                    {
                        var Invoice = _InvoiceRepository.GetInvoiceByNumber(PaymentDetailModel.Number);
                        PAYMENTS_DETAIL PaymentDetail = new PAYMENTS_DETAIL();
                        PaymentDetail.amount = PaymentDetailModel.PaymentInvoice;
                        PaymentDetail.payment_no = Model.Number;
                        PaymentDetail.payment_serie = Model.Serie;
                        PaymentDetail.invoice_no = PaymentDetailModel.Number;
                        PaymentDetail.invoice_serie = PaymentDetailModel.Serie;
                        PaymentDetail.outstanding_balance = Invoice.invoice_balance - PaymentDetail.amount;
                        PaymentDetail.payment_pm_id = PaymentPM.id;                         
                        if (_PaymentDetailsRepository.CreatePaymentDetail(PaymentDetail))
                        {
                            Invoice.invoice_balance -= PaymentDetail.amount;
                            if (!_InvoiceRepository.UpdateInvoice(Invoice, null))
                            {
                                flag = false;
                                break;
                            }

                            PaymentDetail.PAYMENTS = PAYMENT;
                            if (!_TransactionsCustomersBusiness.CreateTransactions(null, null, PaymentDetail, false))
                            {
                                flag = false;
                                break;
                            }
                        }
                        else
                        {
                            flag = false;
                            break;
                        }
                    }
                }
                else
                    break;
            }
            return flag;
        }

        public PAYMENTS_PM GetPaymentMethodById(long PaymentMethodId)
        {
            return _PaymentsPMRepository.GetPaymentMethodById(PaymentMethodId);
        }

        public List<PaymentMethod> GetPaymentMethodDetails(long PaymentMethodId)
        {
            return _PaymentsPMRepository.GetPaymentMethodDetails(PaymentMethodId);
        }
    }
}
