﻿using App.DAL.Lookups;
using App.DAL.Payments;
using App.DAL.Site;
using App.Entities;
using App.Entities.ViewModels.Common;
using App.Entities.ViewModels.Payments;
using App.Entities.ViewModels.TransactionsCustomers;
using System;
using System.Collections.Generic;
using System.Globalization;
using static App.Common.Common;
using static App.Entities.ViewModels.Common.Constants;
using System.Linq;

namespace App.BLL.Payments
{
    public class PaymentsBusiness
    {
        private PaymentsRepository _PaymentsRepository;
        private CurrencyDetailRepository _CurrencyDetailRepository;
        private SiteConfigRepository _SiteConfigRepository;

        public PaymentsBusiness()
        {
            _PaymentsRepository = new PaymentsRepository();
            _CurrencyDetailRepository = new CurrencyDetailRepository();
            _SiteConfigRepository = new SiteConfigRepository();
        }

        public bool CreatePayment(Payment Model)
        {
            PAYMENTS Payment = new PAYMENTS();
            Payment.payment_serie = Model.Serie;
            Payment.payment_no = Model.Number;
            Payment.customer_code = Model.Customer.Code;
            Payment.payment_date = DateTime.ParseExact(Model.AmountPaymentDate, DateFormat.INT_DATE, CultureInfo.InvariantCulture);
            Payment.payment_total = Model.TotalPayment;
            Payment.ttype_id = 14; //TODO: need to check what is and from where it cames.
            Payment.time_stamp = DateTime.Now;
            Payment.payment_status = true;
            Payment.cuser = Model.User;
            Payment.cdate = DateTime.Now;
            Payment.cur_code = Model.Currency;
            Payment.reference = Model.Reference;
            Payment.program_id = "PAMT002.cshtml";
            Payment.site_code = _SiteConfigRepository.GetSiteCode();

            if (!Model.IsPayment)
                Payment.payment_no_subst = Model.PreviousPaymentNumber;

            if (Model.Currency != CURRENCY_DEFAULT)            
                Payment.cur_rate = _CurrencyDetailRepository.CurrencyRate(Model.Currency);                
            
            //Payment.payment_tax = ??
            return _PaymentsRepository.CreatePayment(Payment);
        }

        public List<PaymentsCustomer> PaymentsByCustomer(string CustomerCode) => _PaymentsRepository.PaymentsByCustomer(CustomerCode);        

        public List<PaymentsCustomer> PaymentsCancelledByCustomer(string CustomerCode) => _PaymentsRepository.PaymentsCancelledByCustomer(CustomerCode);        

        public bool UpdatePayment(PAYMENTS Payment) => _PaymentsRepository.UpdatePayment(Payment);

        public PAYMENTS GetPaymentByNumber(long PaymentNumber) => _PaymentsRepository.GetPaymentByNumber(PaymentNumber);        

        public string GetDateLastPayment(string CustomerCode) => _PaymentsRepository.GetDateLastPayment(CustomerCode);

        public SendDocumentModel GetSendDocumentByNumberPayment(long Number) => _PaymentsRepository.GetSendDocumentByNumberPayment(Number);
        

        public SendDocumentModel GetPayment(long Number) => _PaymentsRepository.GetPayment(Number);        

        public List<PaymentCustomerReportModel> PaymentCustomerReport(TransactionsCustomersReport Model, bool IsXmlDownload)
        {
            var List = _PaymentsRepository.PaymentCustomerReport(Model);
            if (IsXmlDownload) 
            {
                var Grouped = (from l in List
                               group l by new { l.Serie, l.Number, l.xml } into g
                               select new PaymentCustomerReportModel
                               {
                                   Serie = g.Key.Serie,
                                   Number = g.Key.Number,
                                   xml = g.Key.xml
                               }).ToList();

                List = Grouped;
            }
            return List;
        }

        public bool SprocUpdatePaymentNumber() => _PaymentsRepository.SprocUpdatePaymentNumber();

        public bool PaymentExist(long Number) => _PaymentsRepository.PaymentExist(Number);
    }
}
