﻿using System.Collections.Generic;
using App.Common;
using App.Entities.ViewModels.ItemPresentation;
using App.DAL.ItemPresentation;

namespace App.BLL.ItemPresentationBusiness
{
    public class ItemPresentationBusiness
    {
        private readonly ItemPresentationRepository _presentations = new ItemPresentationRepository();

        public Result<List<ItemPresentationModel>> GetAll() => Result.OK(_presentations.GetAllActive());

        public Result<List<ItemPresentationModel>> GetPresentationByProductId(string part_number) => Result.OK(_presentations.GetPresentationByPartNumber(part_number));
    }
}