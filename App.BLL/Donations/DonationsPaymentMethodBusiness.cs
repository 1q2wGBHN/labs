﻿using App.DAL.Donations;
using App.Entities.ViewModels.Donations;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App.BLL.Donations
{
    public class DonationsPaymentMethodBusiness
    {
        private DonationsPaymentMethodRepository _DonationsPaymentMethodRepository;

        public DonationsPaymentMethodBusiness()
        {
            _DonationsPaymentMethodRepository = new DonationsPaymentMethodRepository();
        }

        public List<DonationDataDetail> GetDonationsDetail(string UserId, string Date) => _DonationsPaymentMethodRepository.GetDonationsDetail(UserId, Date);
    }
}
