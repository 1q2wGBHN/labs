﻿using App.DAL.Donations;
using App.Entities;
using App.Entities.ViewModels.Donations;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App.BLL.Donations
{
    public class DonationsBusiness
    {
        private DonationsRepository _DonationsRepository;

        public DonationsBusiness()
        {
            _DonationsRepository = new DonationsRepository();
        }

        public List<DFLPOS_DONATIONS> GetCampaingDonations() => _DonationsRepository.GetCampaingDonations();        

        public List<DonationData> GetDonationsReportData(DonationIndex Model) => _DonationsRepository.GetDonationsReportData(Model);        

        public DFLPOS_DONATIONS GetCampaignByDonationId(int DonationId) => _DonationsRepository.GetCampaignByDonationId(DonationId);        

        public List<DonationByCashier> GetTotalsByCashier(List<DonationData> Model)
        {
            var List = new List<DonationByCashier>();
            var DataList = new List<DonationByCashier>();

            foreach (var Item in Model)
            {
                var Element = new DonationByCashier();
                Element.Cashier = Item.Cashier;
                if (Item.AmountUSD != 0)
                    Element.Amount = (Item.AmountUSD * Item.CurrencyRate);

                Element.Amount += Item.AmountMXN;

                List.Add(Element);
            }

            DataList = (from l in List
                        group l by new { l.Cashier } into gp
                        select new DonationByCashier
                        {
                            Cashier = gp.Key.Cashier,
                            Amount = gp.Sum(x => x.Amount)
                        }).OrderByDescending(c => c.Amount).ToList();

            return DataList;
        }
    }
}
