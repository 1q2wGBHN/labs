﻿using App.DAL.Donations;
using App.Entities.ViewModels.Donations;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App.BLL.Donations
{
    public class DonationsSalesBusiness
    {
        private DonationsSalesRepository _DonationsSalesRepository;

        public DonationsSalesBusiness()
        {
            _DonationsSalesRepository = new DonationsSalesRepository();
        }

        public List<DonationsTicketsHeader> GetReportInfo(DonationsTicketsIndex Model)
        {
            return _DonationsSalesRepository.GetReportInfo(Model);
        }
    }
}
