﻿using App.DAL.Donations;
using App.Entities.ViewModels.Donations;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App.BLL.Donations
{
    public class DonationsSalesDetailsBusiness
    {
        private DonationsSalesDetailsRepository _DonationsSalesDetailsRepository;

        public DonationsSalesDetailsBusiness()
        {
            _DonationsSalesDetailsRepository = new DonationsSalesDetailsRepository();
        }

        public List<DonationsTicketsDetail> GetDonationsTicketsDetails(string DonationSaleId)
        {
            return _DonationsSalesDetailsRepository.GetDonationsTicketsDetails(DonationSaleId);
        }
    }
}
