﻿using App.DAL.Site;
using App.Entities.ViewModels.Site;

namespace App.BLL.Site
{
    public class SiteConfigBusiness
    {
        private readonly SiteConfigRepository _siteConfigRepository;

        public SiteConfigBusiness()
        {
            _siteConfigRepository = new SiteConfigRepository();
        }

        public string GetSiteCode() => _siteConfigRepository.GetSiteCode();

        public string GetSiteCodeName() => _siteConfigRepository.GetSiteCodeName();

        public SiteConfigModel GetSiteCodeAddress() => _siteConfigRepository.GetSiteCodeAddress();

        public SiteConfigModel GetSiteConfigInfo() => _siteConfigRepository.GetSiteConfigInfo();

        public SiteConfigModel GetSiteConfig() => _siteConfigRepository.GetSiteConfig();
    }
}