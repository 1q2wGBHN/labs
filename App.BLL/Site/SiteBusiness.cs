﻿using App.DAL.Site;
using App.Entities;
using App.Entities.ViewModels.Site;
using System.Collections.Generic;

namespace App.BLL.Site
{
    public class SiteBusiness
    {
        private readonly SiteRepository _siteRepository;
        private readonly SiteConfigRepository _siteConfigRepository;

        public SiteBusiness()
        {
            _siteRepository = new SiteRepository();
            _siteConfigRepository = new SiteConfigRepository();
        }

        public List<SiteModel> GetAllSitesFlorido() => _siteRepository.GetAllSitesFlorido();

        public List<SiteModel> GetAllSitesFloridoExcludeThisSite(string siteCode) => _siteRepository.GetAllSitesFloridoExcludeThisSite(siteCode);

        public List<SiteModel> GetAllSitesFloridoExcludeThisSiteNoProcesadora(string siteCode) => _siteRepository.GetAllSitesFloridoExcludeThisSiteNoProcesadora(siteCode);

        public string GetSiteCodeName(string siteDestination) => _siteRepository.GetSiteCodeName(siteDestination);

        public string GetSiteCodeAddress(string siteDestination) => _siteRepository.GetSiteCodeAddress(siteDestination);

        public SITES GetInfoSite()
        {
            var site_code = _siteConfigRepository.GetSiteCode();
            return _siteRepository.GetInfoSite(site_code);
        }
    }
}