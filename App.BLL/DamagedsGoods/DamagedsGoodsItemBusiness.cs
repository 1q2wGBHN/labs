﻿using App.DAL.Item;
using App.Entities;
using App.Entities.ViewModels.DamagedsGoods;
using App.Entities.ViewModels.Item;
using System;
using System.Collections.Generic;
using System.Linq;

namespace App.BLL.Rma
{
    public class DamagedsGoodsItemBusiness
    {
        private readonly DamagedsGoodsItemRepository _RmaListRepo;
        private readonly ItemRepository _ItemRepo;

        public DamagedsGoodsItemBusiness()
        {
            _RmaListRepo = new DamagedsGoodsItemRepository();
            _ItemRepo = new ItemRepository();
        }

        public List<DAMAGEDS_GOODS_ITEM> GetAll() => _RmaListRepo.GetAll().ToList();

        public List<DAMAGEDS_GOODS_ITEM> GetRmaListById(string statusId) => _RmaListRepo.GetRmaListById(statusId).ToList();

        public List<DAMAGEDS_GOODS_ITEM> GetRmaListByRmaHeaderId(string rmaheaderId) => _RmaListRepo.GetRmaListByRmaHeaderId(rmaheaderId).ToList();

        public bool AddDamagedsGoodsBanati(DamagedGoodsBanatiModel model, string User, string document, int Status)
        {
            try
            {
                DAMAGEDS_GOODS_ITEM DamagedGood = new DAMAGEDS_GOODS_ITEM { damaged_goods_doc = document, part_number = model.PartNumber, scrap_reason = model.Comment, quantity = model.Quantity, amount_doc = model.Amount_total, price = model.Price, cuser = User, cdate = DateTime.Now, program_id = "RMA0003.cshtml", process_status = Status };
                _RmaListRepo.AddDamagedGoodsItemBanati(DamagedGood);

                return true;
            }
            catch (Exception ex)
            {
                string msg = ex.Message;
                return false;
            }
        }

        public bool UpdateDamagedsGoodsBanati(List<DamagedGoodsBanatiModel> model, string User, string document, int Status, string ProgramId)
        {
            try
            {
                _RmaListRepo.UpdateDamagedGoodsItemBanati(model, User, document, Status, ProgramId);
                return true;
            }
            catch (Exception ex)
            {
                string msg = ex.Message;
                return false;
            }
        }

        public void AddDamagedGoods(DamagedsGoodsModel DamagedGoods, string Document, string User) => _RmaListRepo.PostDamagesGoodsItem(DamagedGoods, Document, User);

        public void AddDamagedGoods(DamagedsGoodsModel DamagedGoods, string Document, string User, int Status) => _RmaListRepo.PostDamagesGoodsItem(DamagedGoods, Document, User, Status);

        public void AddDamagedGoodsAndroid(DamagedsGoodsModel DamagedGoods, string Document, string User, int status) => _RmaListRepo.PostDamagesGoodsItemAndroid(DamagedGoods, Document, User, status);

        public List<ItemViewRma> GetRMAItemsByFolio(string Folio, string type)
        {
            var Items = _RmaListRepo.GetRMAItemsByFolio(Folio, type);
            foreach (var GoodsItem in Items)
            {
                var Tax = _ItemRepo.StoreProcedureIvaIeps(GoodsItem.PartNumber);
                var Ieps = Tax.Ieps;
                var Iva = Tax.Iva;

                if (GoodsItem.Size == null)
                    GoodsItem.Size = "N/A";

                GoodsItem.Ieps = GoodsItem.Amount_Doc * (Ieps / 100);
                GoodsItem.Iva = (GoodsItem.Amount_Doc + GoodsItem.Ieps) * (Iva / 100);
                if (GoodsItem.Amount_Doc == null)
                    GoodsItem.Amount_Doc = 0;
            }
            return Items;
        }

        public List<ItemViewRma> GetRMAItemsByFolio(string Folio, string Type, int Status)
        {
            var Items = _RmaListRepo.GetRMAItemsByFolio(Folio, Type, Status);
            foreach (var GoodsItem in Items)
            {
                //GoodsItem.listcost = _ItemRepo.GetLastPurchasePrice(GoodsItem.PartNumber, GoodsItem.Supplier_Id, 5);
                var Tax = _ItemRepo.StoreProcedureIvaIeps(GoodsItem.PartNumber);
                var Ieps = Tax.Ieps;
                var Iva = Tax.Iva;

                if (GoodsItem.Size == null)
                    GoodsItem.Size = "N/A";
                GoodsItem.IepsPorcentage = Ieps;
                GoodsItem.IvaPorcentage = Iva;
                var currentAmount = GoodsItem.Quantity * GoodsItem.Price;
                GoodsItem.Ieps = currentAmount * (Ieps / 100);
                GoodsItem.Iva = (currentAmount + GoodsItem.Ieps) * (Iva / 100);
                if (GoodsItem.Amount_Doc == null)
                    GoodsItem.Amount_Doc = 0;
            }
            return Items;
        }

        public List<ItemViewRma> GetRMAItemsByFolioLastCost(string Folio, string Type, int Status)
        {
            var Items = _RmaListRepo.GetRMAItemsByFolio(Folio, Type, Status);
            foreach (var GoodsItem in Items)
            {
                GoodsItem.listcost = _ItemRepo.GetLastPurchasePrice(GoodsItem.PartNumber, GoodsItem.Supplier_Id, 5, GoodsItem.Price ?? 0);
                var Tax = _ItemRepo.StoreProcedureIvaIeps(GoodsItem.PartNumber);
                var Ieps = Tax.Ieps;
                var Iva = Tax.Iva;

                if (GoodsItem.Size == null)
                    GoodsItem.Size = "N/A";
                GoodsItem.IepsPorcentage = Ieps;
                GoodsItem.IvaPorcentage = Iva;
                var currentAmount = GoodsItem.Quantity * GoodsItem.listcost[0].t_cost;
                GoodsItem.Ieps = currentAmount * (Ieps / 100);
                GoodsItem.Iva = (currentAmount + GoodsItem.Ieps) * (Iva / 100);
                if (GoodsItem.Amount_Doc == null)
                    GoodsItem.Amount_Doc = 0;
            }
            return Items;
        }

        public DamagedGoodsViewModel GetRMAItemsByDocument(string Folio, string Type, int Status) => _RmaListRepo.GetRMAItemsByDocument(Folio, Type, Status);

        public List<ItemViewRma> GetAllRMAItemsByFolio(string Folio, string Type, int Status)
        {
            var Items = _RmaListRepo.GetAllRMAItemsByFolio(Folio, Type, Status);
            foreach (var GoodsItem in Items)
            {
                var Tax = _ItemRepo.StoreProcedureIvaIeps(GoodsItem.PartNumber);
                var Ieps = Tax.Ieps;
                var Iva = Tax.Iva;

                if (GoodsItem.Size == null)
                    GoodsItem.Size = "N/A";

                GoodsItem.Ieps = GoodsItem.Amount_Doc * (Ieps / 100);
                GoodsItem.Iva = (GoodsItem.Amount_Doc + GoodsItem.Ieps) * (Iva / 100);
                if (GoodsItem.Amount_Doc == null)
                    GoodsItem.Amount_Doc = 0;
            }
            return Items;
        }

        public DamagedsGoodsModel GetDocumentGoods(string document) => _RmaListRepo.GetDocumentGoods(document);

        public List<DamagedsGoodsModel> GetItemsByDocument(string document)
        {
            var Items = _RmaListRepo.GetItemsByDocument(document);
            if (Items.Count > 0)
            {
                foreach (var Item in Items)
                {
                    var Tax = _ItemRepo.StoreProcedureIvaIeps(Item.PartNumber);
                    var Ieps = Tax.Ieps;
                    var Iva = Tax.Iva;
                    Item.IEPS = Item.Amount * (Ieps / 100);
                    Item.IVA = (Item.Amount + Item.IEPS) * (Iva / 100);
                }
                return Items;
            }
            else
            {
                List<DamagedsGoodsModel> lista = new List<DamagedsGoodsModel>();
                return lista;
            }
        }

        public List<DamagedsGoodsModel> GetItemsByDocument(string document, int status)
        {
            var Items = _RmaListRepo.GetItemsByDocument(document, status);
            if (Items.Count > 0)
            {
                foreach (var Item in Items)
                {
                    var Tax = _ItemRepo.StoreProcedureIvaIeps(Item.PartNumber);
                    var Ieps = Tax.Ieps;
                    var Iva = Tax.Iva;
                    Item.IEPS = Item.Amount * (Ieps / 100);
                    Item.IVA = (Item.Amount + Item.IEPS) * (Iva / 100);
                }
                return Items;
            }
            else
            {
                List<DamagedsGoodsModel> lista = new List<DamagedsGoodsModel>();
                return lista;
            }
        }

        public List<DamagedsGoodsModel> GetItemsByDocumentSingle(string document, int status)
        {
            var Items = _RmaListRepo.GetItemsByDocumentSingle(document, status);
            if (Items.Count > 0)
            {
                foreach (var Item in Items)
                {
                    var Tax = _ItemRepo.StoreProcedureIvaIeps(Item.PartNumber);
                    var Ieps = Tax.Ieps;
                    var Iva = Tax.Iva;
                    Item.IEPS = Item.Amount * (Ieps / 100);
                    Item.IVA = (Item.Amount + Item.IEPS) * (Iva / 100);
                }
                return Items;
            }
            else
            {
                List<DamagedsGoodsModel> lista = new List<DamagedsGoodsModel>();
                return lista;
            }
        }

        public List<DamagedsGoodsModel> GetItemsByDocumentReport(string document, int status)
        {
            var Items = _RmaListRepo.NewGetItemsByDocumentReport(document, status);
            if (Items.Count > 0)
            {
                foreach (var Item in Items)
                {
                    var Tax = _ItemRepo.StoreProcedureIvaIeps(Item.PartNumber);
                    var Ieps = Tax.Ieps;
                    var Iva = Tax.Iva;
                    Item.IEPS = Item.Amount * (Ieps / 100);
                    Item.IVA = (Item.Amount + Item.IEPS) * (Iva / 100);
                }
                return Items;
            }
            else
            {
                List<DamagedsGoodsModel> lista = new List<DamagedsGoodsModel>();
                return lista;
            }
        }

        public List<DamagedsGoodsModel> NewGetItemsByDocumentReport(string document, int status)
        {
            var Items = _RmaListRepo.NewGetItemsByDocumentReport(document, status);
            if (Items.Count > 0)
            {
                foreach (var Item in Items)
                {
                    var Tax = _ItemRepo.StoreProcedureIvaIeps(Item.PartNumber);
                    var Ieps = Tax.Ieps;
                    var Iva = Tax.Iva;
                    Item.IEPS = Item.Amount * (Ieps / 100);
                    Item.IVA = (Item.Amount + Item.IEPS) * (Iva / 100);
                }
                return Items;
            }
            else
            {
                List<DamagedsGoodsModel> lista = new List<DamagedsGoodsModel>();
                return lista;
            }
        }
        public List<DamagedsGoodsModel> NewGetItemsByDocumentReportStatus8or9(string document)
        {
            var Items = _RmaListRepo.NewGetItemsByDocumentReportStatus8or9(document);
            if (Items.Count > 0)
            {
                foreach (var Item in Items)
                {
                    var Tax = _ItemRepo.StoreProcedureIvaIeps(Item.PartNumber);
                    var Ieps = Tax.Ieps;
                    var Iva = Tax.Iva;
                    Item.IEPS = Item.Amount * (Ieps / 100);
                    Item.IVA = (Item.Amount + Item.IEPS) * (Iva / 100);
                }
                return Items;
            }
            else
            {
                List<DamagedsGoodsModel> lista = new List<DamagedsGoodsModel>();
                return lista;
            }
        }      

        public List<DamagedsGoodsModel> NewGetItemsByDocument(string document)
        {
            var Items = _RmaListRepo.GetItemsByDocument(document, 1);
            if (Items.Count > 0)
            {
                foreach (var Item in Items)
                {
                    var Tax = _ItemRepo.StoreProcedureIvaIeps(Item.PartNumber);
                    var Ieps = Tax.Ieps;
                    var Iva = Tax.Iva;
                    Item.IEPS = Item.Amount * (Ieps / 100);
                    Item.IVA = (Item.Amount + Item.IEPS) * (Iva / 100);
                }
                return Items;
            }
            else
            {
                List<DamagedsGoodsModel> lista = new List<DamagedsGoodsModel>();
                return lista;
            }
        }

        public List<DamagedGoodsBanatiModel> GetRmaBanatiItemsByDocument(string document) => _RmaListRepo.GetRmaBanatiItemsByDocument(document);
    }
}