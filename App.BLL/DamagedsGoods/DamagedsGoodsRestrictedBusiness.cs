﻿using App.DAL.DamagedsGoods;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App.BLL.DamagedsGoods
{
    public class DamagedsGoodsRestrictedBusiness
    {
        private readonly DamagedsGoodsRestrictedRepository _damagedsGoodsRestrictedBusiness;

        public DamagedsGoodsRestrictedBusiness()
        {
            _damagedsGoodsRestrictedBusiness = new DamagedsGoodsRestrictedRepository();
        }

        public bool GetKnowDamagedsGoodsRestricted(string damageds_goods_type, string part_number) => _damagedsGoodsRestrictedBusiness.GetKnowDamagedsGoodsRestricted(damageds_goods_type, part_number);
    }
}