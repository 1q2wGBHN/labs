﻿using App.BLL.Item;
using App.BLL.StorageLocation;
using App.BLL.Supplier;
using App.DAL;
using App.DAL.Item;
using App.DAL.MovementNumber;
using App.DAL.ScrapControl;
using App.DAL.Site;
using App.Entities;
using App.Entities.ViewModels.Android;
using App.Entities.ViewModels.DamagedsGoods;
using App.Entities.ViewModels.Item;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;

namespace App.BLL.Rma
{
    public class DamagedsGoodsBusiness
    {
        private readonly DamagedsGoodsRepository _RmaHeadRepo;
        private readonly DamagedsGoodsItemBusiness _DamagedGoodsItemBusinnes;
        private readonly DamagedsGoodsItemRepository _damagedGoodsItemRepository;
        private readonly SiteConfigRepository _SiteConfigRepo;
        private readonly SupplierBusiness _SupplierBusiness;
        private readonly ItemRepository _ItemRepo;
        private readonly ItemStockRepository _itemStockRepository;
        private readonly ItemSupplierBusiness _itemSupplierBusiness;
        private readonly MovementNumberRepository _movementNumberRepository;
        private readonly ItemRepository _itemRepository;
        private readonly ItemSupplierRepository _ItemSupplierRepo;
        private readonly ScrapControlRepository _scrapControlRepository;
        private readonly StorageLocationBusiness _StorageLocationBusiness;

        public DamagedsGoodsBusiness()
        {
            _RmaHeadRepo = new DamagedsGoodsRepository();
            _DamagedGoodsItemBusinnes = new DamagedsGoodsItemBusiness();
            _SiteConfigRepo = new SiteConfigRepository();
            _SupplierBusiness = new SupplierBusiness();
            _ItemRepo = new ItemRepository();
            _itemStockRepository = new ItemStockRepository();
            _movementNumberRepository = new MovementNumberRepository();
            _damagedGoodsItemRepository = new DamagedsGoodsItemRepository();
            _itemSupplierBusiness = new ItemSupplierBusiness();
            _itemRepository = new ItemRepository();
            _ItemSupplierRepo = new ItemSupplierRepository();
            _scrapControlRepository = new ScrapControlRepository();
            _StorageLocationBusiness = new StorageLocationBusiness();
        }

        public List<DAMAGEDS_GOODS> GetAll() => _RmaHeadRepo.GetAll().ToList();

        public List<DAMAGEDS_GOODS> GetRmaHeadByStatus(int statusId) => _RmaHeadRepo.GetRmaHeadByStatus(statusId).ToList();

        public List<DamagedGoodsHeadModel> GetAllRmaHeadByType(string Type, DateTime date1, DateTime date2)
        {
            var RMAHeader = _RmaHeadRepo.GetAllRmaHeadByType(Type, date1, date2).ToList();
            RMAHeader.ForEach(s =>
            {
                switch (s.ProcessStatus)
                {
                    case 1:
                        s.ProcessStatusDescription = "Pendiente";
                        break;
                    case 2:
                        s.ProcessStatusDescription = "Aprobada";
                        break;
                    case 3:
                        s.ProcessStatusDescription = "Rechazada";
                        break;
                    case 9:
                        s.ProcessStatusDescription = "Terminada";
                        break;
                    default:
                        s.ProcessStatusDescription = "Inicial";
                        break;
                }
            });
            return RMAHeader;
        }

        public List<DamagedGoodsHeadModel> GetAllRmaHeadStatus(string Type, DateTime date1, DateTime date2, int status)
        {
            var RMAHeader = _RmaHeadRepo.GetAllRmaHeadStatus(Type, date1, date2, status).ToList();
            RMAHeader.ForEach(s =>
            {
                switch (s.ProcessStatus)
                {
                    case 0:
                        s.ProcessStatusDescription = "Inicial";
                        break;
                    case 1:
                        s.ProcessStatusDescription = "Pendiente";
                        break;
                    case 2:
                        s.ProcessStatusDescription = "Aprobada";
                        break;
                    case 3:
                        s.ProcessStatusDescription = "Rechazada";
                        break;
                    case 4:
                        s.ProcessStatusDescription = "Terminada"; // Solicitud de cancelación
                        break;
                    case 9:
                        s.ProcessStatusDescription = "Terminada";
                        break;
                    default:
                        s.ProcessStatusDescription = "Inicial";
                        break;
                }
            });
            return RMAHeader;
        }

        public List<DamagedGoodsHeadModel> GetAllRma(string Type, DateTime? date1, DateTime? date2, int status)
        {
            var RMAHeader = _RmaHeadRepo.GetAllRma(Type, date1, date2, status).ToList();
            RMAHeader.ForEach(s =>
            {
                switch (s.ProcessStatus)
                {
                    case 1:
                        s.ProcessStatusDescription = "Pendiente";
                        break;
                    case 2:
                        s.ProcessStatusDescription = "Aprobada";
                        break;
                    case 3:
                        s.ProcessStatusDescription = "Rechazada";
                        break;
                    case 4:
                        s.ProcessStatusDescription = "Proceso Cancelacion"; // Solicitud de cancelación
                        break;
                    case 9:
                        s.ProcessStatusDescription = "Terminada";
                        break;
                    case 8:
                        s.ProcessStatusDescription = "Cancelado";
                        break;
                    default:
                        s.ProcessStatusDescription = "Inicial";
                        break;
                }
            });
            return RMAHeader;
        }

        public List<DamagedGoodsHeadModel> GetAllRmaHeadStatusToCancelRequest(string Type, int status)
        {
            DateTime date = DateTime.Now;
            var firstDayOfMonth = new DateTime(date.Year, date.Month, 1);
            var lastDayOfMonth = firstDayOfMonth.AddMonths(1).AddDays(-1);

            var RMAHeader = _RmaHeadRepo.GetAllRmaHeadStatus(Type, firstDayOfMonth, lastDayOfMonth, status).ToList();
            RMAHeader.ForEach(s =>
            {
                switch (s.ProcessStatus)
                {
                    case 1:
                        s.ProcessStatusDescription = "Pendiente";
                        break;
                    case 2:
                        s.ProcessStatusDescription = "Aprobada";
                        break;
                    case 3:
                        s.ProcessStatusDescription = "Rechazada";
                        break;
                    case 4:
                        s.ProcessStatusDescription = "Terminada"; // Solicitud de cancelación
                        break;
                    case 9:
                        s.ProcessStatusDescription = "Terminada";
                        break;
                    default:
                        s.ProcessStatusDescription = "Inicial";
                        break;
                }
            });
            return RMAHeader;
        }

        public ItemRmaBanati GetItemStockByPartNumber(string partNumber) => _itemStockRepository.GetItemStockByPartNumber(partNumber);

        public List<DamagedGoodsBanatiModel> GetItemsBloqued() => _itemStockRepository.GetItemsBlocked();

        public DamagedGoodsViewModel GetBanatiItemsBloqued() => _itemStockRepository.GetBanatiItemsBlocked();

        public string AddBanati(List<DamagedGoodsBanatiModel> model, string Comments, string user, int Status)
        {
            try
            {
                if (model != null)
                {
                    var Site = _SiteConfigRepo.GetSiteCodeAddress();
                    string document = _RmaHeadRepo.CreateDamagedsGoodsBanati(user, Comments, Status, Site.Site);
                    foreach (var item in model)
                    {
                        item.Price = _itemRepository.GetMapPrice(item.PartNumber);
                        item.Amount_total = item.Price * item.Quantity;
                        if (document != "")
                            _DamagedGoodsItemBusinnes.AddDamagedsGoodsBanati(item, user, document, Status);
                    }

                    return document;
                }
                else
                    return "";
            }
            catch (Exception ex)
            {
                string msg = ex.Message;
                return "";
            }
        }

        public bool UpdateBanati(List<DamagedGoodsBanatiModel> model, string Document, string Comments, string user, int Status, string ProgramId)
        {
            try
            {
                if (_RmaHeadRepo.UpdateDamagedsGoodsBanati(Document, Comments, user, Status, ProgramId))
                {
                    foreach (var item in model)
                    {
                        item.Price = _itemRepository.GetMapPrice(item.PartNumber);
                        item.Amount_total = item.Price * item.Quantity;
                    }
                    _DamagedGoodsItemBusinnes.UpdateDamagedsGoodsBanati(model, user, Document, Status, ProgramId);
                    return true;
                }
                else
                    return false;
            }
            catch (Exception ex)
            {
                string msg = ex.Message;
                return false;
            }
        }

        public DamagedGoodsHeadModel GetRmaHeadByDocument(string document)
        {
            try
            {
                return _RmaHeadRepo.GetRmaHeadByDocument(document);
            }
            catch (Exception ex)
            {
                string msg = ex.Message;
                return null;
            }
        }

        public DamagedGoodsHeadModel AddDamagedGoods(List<DamagedsGoodsModel> DamageGoods, int Supplier, string User)
        {
            var Document = _RmaHeadRepo.CreateDamagedGoodsHeadModel(Supplier, User, 2); //Mandar directo a proveedor
            Document.DamageGoodsItem = DamageGoods;

            foreach (var item in DamageGoods)
                _DamagedGoodsItemBusinnes.AddDamagedGoods(item, Document.DamagedGoodsDoc, User, 2);

            var Site = _SiteConfigRepo.GetSiteCodeAddress();
            Document.SiteName = Site.SiteName;
            Document.SiteAdress = Site.SiteAddress;
            var SupplierName = _SupplierBusiness.GetNameSupplier(Supplier);
            Document.Supplier = SupplierName;
            return Document;
        }

        public DamagedGoodsHeadModel AddDamagedGoodsAndroid(string DamageGoods, int Supplier, string User)
        {
            string v = "{\"Items\":" + DamageGoods + "}";
            var result = JsonConvert.DeserializeObject<DamagedsGoodsAndroid>(v);
            List<DamagedsGoodsModel> list = new List<DamagedsGoodsModel>();
            for (int i = 0; i < result.Items.Count; i++)
            {
                var Info = _ItemSupplierRepo.GetSingleItemSupplier(Supplier, result.Items[i].part_number);
                DamagedsGoodsModel model = new DamagedsGoodsModel
                {
                    PartNumber = result.Items[i].part_number,
                    Quantity = decimal.Parse(result.Items[i].quantity),
                    Description = result.Items[i].part_description,
                    Price = decimal.Parse(result.Items[i].price),
                    Amount = decimal.Parse(result.Items[i].price) * decimal.Parse(result.Items[i].quantity),
                    Block = Info.Block,
                    Storage = Info.Storage,
                    Size = Info.Size
                };
                list.Add(model);
            }
            var Document = _RmaHeadRepo.CreateDamagedGoodsHeadModelAndroid(Supplier, User, 2); //Mandar a proveedor
            Document.DamageGoodsItem = list;

            foreach (var item in list)
                _DamagedGoodsItemBusinnes.AddDamagedGoodsAndroid(item, Document.DamagedGoodsDoc, User, 2); //Mandar a proveedor

            var Site = _SiteConfigRepo.GetSiteCodeAddress();
            Document.SiteName = Site.SiteName;
            Document.SiteAdress = Site.SiteAddress;
            var SupplierName = _SupplierBusiness.GetNameSupplier(Supplier);
            Document.Supplier = SupplierName;
            return Document;
        }

        public DamagedGoodsHeadModel AddDamagedGoodsAndroid(string DamageGoods, int Supplier, string User, string comments)
        {
            string v = "{\"Items\":" + DamageGoods + "}";
            var result = JsonConvert.DeserializeObject<DamagedsGoodsAndroid>(v);
            List<DamagedsGoodsModel> list = new List<DamagedsGoodsModel>();
            for (int i = 0; i < result.Items.Count; i++)
            {
                var Info = _ItemSupplierRepo.GetSingleItemSupplier(Supplier, result.Items[i].part_number);
                DamagedsGoodsModel model = new DamagedsGoodsModel
                {
                    PartNumber = result.Items[i].part_number,
                    Quantity = decimal.Parse(result.Items[i].quantity),
                    Description = result.Items[i].part_description,
                    Price = decimal.Parse(result.Items[i].price),
                    Amount = decimal.Parse(result.Items[i].price) * decimal.Parse(result.Items[i].quantity),
                    Block = Info.Block,
                    Storage = Info.Storage,
                    Size = Info.Size
                };
                list.Add(model);
            }
            var Document = _RmaHeadRepo.CreateDamagedGoodsHeadModelAndroid(Supplier, User, 0, comments); //Mandar a proveedor
            Document.DamageGoodsItem = list;

            foreach (var item in list)
                _DamagedGoodsItemBusinnes.AddDamagedGoodsAndroid(item, Document.DamagedGoodsDoc, User, 0); //Mandar a proveedor

            var Site = _SiteConfigRepo.GetSiteCodeAddress();
            Document.SiteName = Site.SiteName;
            Document.SiteAdress = Site.SiteAddress;
            var SupplierName = _SupplierBusiness.GetNameSupplier(Supplier);
            Document.Supplier = SupplierName;
            return Document;
        }

        public int MethodReverseBlockToUnrestricted(string siteCode, string partNumber, int quantity, string sL, string to_sL, string program_id, string User) => _RmaHeadRepo.MethodReverseBlockToUnrestricted(siteCode, partNumber, quantity, sL, to_sL, program_id, User);

        public List<DamagedsGoodsAndroid> MoveBlockToUnrestricted(string list, string program_id, string User)
        {
            string l = "{\"Items\":" + list + "}";
            var r = JsonConvert.DeserializeObject<ScrapControlAndroidModel>(l);
            List<DamagedsGoodsAndroid> items = new List<DamagedsGoodsAndroid>();
            for (int i = 0; i < r.Items.Count; i++)
            {
                DamagedsGoodsAndroid model = new DamagedsGoodsAndroid
                {
                    part_number = r.Items[i].part_number,
                    quantityDecimal = decimal.Parse(r.Items[i].quantity),
                    part_description = r.Items[i].part_description
                };
                items.Add(model);
            }

            string siteCode = _SiteConfigRepo.GetSiteCode();
            string merma = _StorageLocationBusiness.GetLocationaMain("MERMA", siteCode).storage_location;
            string venta = _StorageLocationBusiness.GetLocationaMain("VENTA", siteCode).storage_location;
            foreach (var item in items)
            {
                var result = _RmaHeadRepo.MethodReverseBlockToUnrestricted(siteCode, item.part_number, Decimal.ToInt32(item.quantityDecimal), merma, venta, program_id, User);
                if (result == 0)
                    item.reason = "";
                else if (result == 1)
                    item.reason = "No existe locación MERMA";
                else if (result == 2)
                    item.reason = "No existe locación VENTA";
                else if (result == 3)
                    item.reason = "No tiene stock suficiente";
                else if (result == 5)
                    item.reason = "Desconocido";
            }
            return items.Where(w => w.reason != "").ToList(); //Regresa los item que hayan tenido problemas
        }
        public List<DamagedsGoodsAndroid> MoveBlockToUnrestrictedQuick( string program_id, string User)
        {

            List<DamagedsGoodsAndroid> items = new List<DamagedsGoodsAndroid>();
            items = _scrapControlRepository.GenericGetListBlockedtoUnrestricted();
            string siteCode = _SiteConfigRepo.GetSiteCode();
            string merma = _StorageLocationBusiness.GetLocationaMain("MERMA", siteCode).storage_location;
            string venta = _StorageLocationBusiness.GetLocationaMain("VENTA", siteCode).storage_location;
            foreach (var item in items)
            {
                var result = _RmaHeadRepo.MethodReverseBlockToUnrestricted(siteCode, item.part_number, Decimal.ToInt32(item.quantityDecimal), merma, venta, program_id, User);
                if (result == 0)
                    item.reason = "";
                else if (result == 1)
                    item.reason = "No existe locación MERMA";
                else if (result == 2)
                    item.reason = "No existe locación VENTA";
                else if (result == 3)
                    item.reason = "No tiene stock suficiente";
                else if (result == 5)
                    item.reason = "Desconocido";
            }
            return items.Where(w => w.reason != "").ToList(); //Regresa los item que hayan tenido problemas
        }

        public bool MoveBlockToUnrestrictedModelDG(List<DamagedsGoodsModel> items, string program_id, string User)
        {
            bool error = false;
            string siteCode = _SiteConfigRepo.GetSiteCode();
            string merma = _StorageLocationBusiness.GetLocationaMain("MERMA", siteCode).storage_location;
            string venta = _StorageLocationBusiness.GetLocationaMain("VENTA", siteCode).storage_location;
            foreach (var item in items)
            {
                var result = _RmaHeadRepo.MethodReverseBlockToUnrestricted(siteCode, item.PartNumber, Decimal.ToInt32(item.Quantity), merma, venta, program_id, User);
                if (result != 0)
                    error = true;
            }
            return error;
        }

        public bool MoveBlockToUnrestrictedModelDGRepo(List<DamagedsGoodsModel> items, string program_id, string User)
        {
            bool error = false;
            string siteCode = _SiteConfigRepo.GetSiteCode();
            string merma = _StorageLocationBusiness.GetLocationaMain("MERMA", siteCode).storage_location;
            string venta = _StorageLocationBusiness.GetLocationaMain("VENTA", siteCode).storage_location;
            foreach (var item in items)
            {
                var result = _RmaHeadRepo.MethodReverseBlockToUnrestricted(siteCode, item.PartNumber, decimal.ToInt32(item.Quantity), merma, venta, program_id, User);
                if (result != 0)
                    error = true;
            }
            return error;
        }

        public DamagedGoodsHeadModel AddDamagedGoodsRequest(List<DamagedsGoodsModel> DamageGoods, int Supplier, string User)
        {
            var Document = _RmaHeadRepo.CreateDamagedGoodsHeadModel(Supplier, User, 1); //Mandar a compras
            Document.DamageGoodsItem = DamageGoods;

            foreach (var item in DamageGoods)
                _DamagedGoodsItemBusinnes.AddDamagedGoods(item, Document.DamagedGoodsDoc, User, 1);

            var Site = _SiteConfigRepo.GetSiteCodeAddress();
            Document.SiteName = Site.SiteName;
            Document.SiteAdress = Site.SiteAddress;
            var SupplierName = _SupplierBusiness.GetNameSupplier(Supplier);
            Document.Supplier = SupplierName;
            return Document;
        }

        public DamagedGoodsHeadModel AddDamagedGoodsRequestAndroid(string DamageGoods, int Supplier, string User)
        {
            string v = "{\"Items\":" + DamageGoods + "}";
            var result = JsonConvert.DeserializeObject<DamagedsGoodsAndroid>(v);
            List<DamagedsGoodsModel> list = new List<DamagedsGoodsModel>();
            for (int i = 0; i < result.Items.Count; i++)
            {
                var Info = _ItemSupplierRepo.GetSingleItemSupplier(Supplier, result.Items[i].part_number);
                DamagedsGoodsModel model = new DamagedsGoodsModel
                {
                    PartNumber = result.Items[i].part_number,
                    Quantity = decimal.Parse(result.Items[i].quantity),
                    Description = result.Items[i].part_description,
                    Price = decimal.Parse(result.Items[i].price),
                    Amount = decimal.Parse(result.Items[i].price) * decimal.Parse(result.Items[i].quantity),
                    Block = Info.Block,
                    Storage = Info.Storage,
                    Size = Info.Size
                };
                list.Add(model);
            }
            var Document = _RmaHeadRepo.CreateDamagedGoodsHeadModelAndroid(Supplier, User, 1); //Mandar directo a compras android
            Document.DamageGoodsItem = list;

            foreach (var item in list)
                _DamagedGoodsItemBusinnes.AddDamagedGoodsAndroid(item, Document.DamagedGoodsDoc, User, 1);

            var Site = _SiteConfigRepo.GetSiteCodeAddress();
            Document.SiteName = Site.SiteName;
            Document.SiteAdress = Site.SiteAddress;
            var SupplierName = _SupplierBusiness.GetNameSupplier(Supplier);
            Document.Supplier = SupplierName;
            return Document;
        }

        public DamagedGoodsHeadModel AddDamagedGoodsRequestAndroid(string DamageGoods, int Supplier, string User, string comments)
        {
            string v = "{\"Items\":" + DamageGoods + "}";
            var buyer_division = "";
            var result = JsonConvert.DeserializeObject<DamagedsGoodsAndroid>(v);
            List<DamagedsGoodsModel> list = new List<DamagedsGoodsModel>();
            for (int i = 0; i < result.Items.Count; i++)
            {
                var Info = _ItemSupplierRepo.GetSingleItemSupplier(Supplier, result.Items[i].part_number);
                DamagedsGoodsModel model = new DamagedsGoodsModel
                {
                    PartNumber = result.Items[i].part_number,
                    Quantity = decimal.Parse(result.Items[i].quantity),
                    Description = result.Items[i].part_description,
                    Price = decimal.Parse(result.Items[i].price),
                    Amount = decimal.Parse(result.Items[i].price) * decimal.Parse(result.Items[i].quantity),
                    Block = Info.Block,
                    Storage = Info.Storage,
                    Size = Info.Size
                };
                list.Add(model);
            }

            foreach (var item in list)
            {
                var item_buyer = _ItemRepo.GetOneItem(item.PartNumber);
                if (item_buyer.buyerdivision != null)
                    buyer_division = item_buyer.buyerdivision;
            }
            if (buyer_division == null)
                buyer_division = "P002";

            var Document = _RmaHeadRepo.CreateDamagedGoodsHeadModelAndroid(Supplier, User, 1, comments); //Mandar directo a compras android
            Document.DamageGoodsItem = list;

            foreach (var item in list)
                _DamagedGoodsItemBusinnes.AddDamagedGoodsAndroid(item, Document.DamagedGoodsDoc, User, 1);

            var Site = _SiteConfigRepo.GetSiteCodeAddress();
            Document.SiteName = Site.SiteName;
            Document.SiteAdress = Site.SiteAddress;
            var SupplierName = _SupplierBusiness.GetNameSupplier(Supplier);
            Document.Supplier = SupplierName;
            Document.BuyerDivision = buyer_division;
            return Document;
        }

        public List<DamagedGoodsHeadModel> DamagedGoodsApproved(int Supplier, string ProcessType)
        {
            var DamagedGood = _RmaHeadRepo.DamagedGoodsApproved(Supplier, ProcessType);
            foreach (var item in DamagedGood)
            {
                var Site = _SiteConfigRepo.GetSiteCodeAddress();
                item.SiteName = Site.SiteName;
                item.SiteAdress = Site.SiteAddress;
                var SupplierName = _SupplierBusiness.GetNameSupplier(Supplier);
                item.Supplier = SupplierName;
                foreach (var GoodsItem in item.DamageGoodsItem)
                {
                    var Tax = _ItemRepo.StoreProcedureIvaIeps(GoodsItem.PartNumber);
                    var Ieps = Tax.Ieps;
                    var Iva = Tax.Iva;
                    GoodsItem.IEPS = GoodsItem.Amount * (Ieps / 100);
                    GoodsItem.IVA = (GoodsItem.Amount + GoodsItem.IEPS) * (Iva / 100);
                }
            }
            return DamagedGood;
        }

        public DamagedGoodsHeadModel DamagedGoodsApprovedPrint(int Supplier, string RMA, int status)
        {
            var buyer_division = "";
            var DamagedGood = _RmaHeadRepo.DamagedGoodsApprovedPrint(Supplier, RMA, status);
            var Site = _SiteConfigRepo.GetSiteCodeAddress();
            DamagedGood.SiteName = Site.SiteName;
            DamagedGood.SiteAdress = Site.SiteAddress;
            var SupplierName = _SupplierBusiness.GetNameSupplier(Supplier);
            DamagedGood.Supplier = SupplierName;
            foreach (var GoodsItem in DamagedGood.DamageGoodsItem)
            {
                var Tax = _ItemRepo.StoreProcedureIvaIeps(GoodsItem.PartNumber);
                var Ieps = Tax.Ieps;
                var Iva = Tax.Iva;
                GoodsItem.IEPS = GoodsItem.Amount * (Ieps / 100);
                GoodsItem.IVA = (GoodsItem.Amount + GoodsItem.IEPS) * (Iva / 100);
                GoodsItem.Storage = _itemStockRepository.ItemIsBlocked(GoodsItem.PartNumber).Storage;
            }
            foreach (var item in DamagedGood.DamageGoodsItem)
            {
                var item_buyer = _ItemRepo.GetOneItem(item.PartNumber);
                if (item_buyer.buyerdivision != null)
                    buyer_division = item_buyer.buyerdivision;
            }
            if (buyer_division == null)
                buyer_division = "P002";
            DamagedGood.BuyerDivision = buyer_division;
            return DamagedGood;
        }

        public DamagedGoodsHeadModel DamagedGoodsApprovedPrintRMA(int Supplier, string RMA)
        {
            var DamagedGood = _RmaHeadRepo.DamagedGoodsApprovedPrintRMA(Supplier, RMA);
            var Site = _SiteConfigRepo.GetSiteCodeAddress();
            DamagedGood.SiteName = Site.SiteName;
            DamagedGood.SiteAdress = Site.SiteAddress;
            var SupplierName = _SupplierBusiness.GetNameSupplier(Supplier);
            DamagedGood.Supplier = SupplierName;
            foreach (var GoodsItem in DamagedGood.DamageGoodsItem)
            {
                var Tax = _ItemRepo.StoreProcedureIvaIeps(GoodsItem.PartNumber);
                var Ieps = Tax.Ieps;
                var Iva = Tax.Iva;
                GoodsItem.IEPS = GoodsItem.Amount * (Ieps / 100);
                GoodsItem.IVA = (GoodsItem.Amount + GoodsItem.IEPS) * (Iva / 100);
                GoodsItem.Storage = _itemStockRepository.ItemIsBlocked(GoodsItem.PartNumber).Storage;
            }
            return DamagedGood;
        }

        public DamagedGoodsHeadModel DamagedGoodsPrint(string RMA)
        {
            var DamagedGood = _RmaHeadRepo.DamagedGoodsPrint(RMA);
            var Site = _SiteConfigRepo.GetSiteCodeAddress();
            DamagedGood.SiteName = Site.SiteName;
            DamagedGood.SiteAdress = Site.SiteAddress;
            var SupplierName = _SupplierBusiness.GetNameSupplier(DamagedGood.SupplierId);
            DamagedGood.Supplier = SupplierName;
            foreach (var GoodsItem in DamagedGood.DamageGoodsItem)
            {
                var Tax = _ItemRepo.StoreProcedureIvaIeps(GoodsItem.PartNumber);
                var Ieps = Tax.Ieps;
                var Iva = Tax.Iva;
                GoodsItem.IEPS = GoodsItem.Amount * (Ieps / 100);
                GoodsItem.IVA = (GoodsItem.Amount + GoodsItem.IEPS) * (Iva / 100);
                GoodsItem.Storage = _itemStockRepository.ItemIsBlocked(GoodsItem.PartNumber).Storage;
            }
            return DamagedGood;
        }

        public DamagedGoodsHeadModel DamagedGoodsApprovedPrint(string RMA, int status)
        {
            var DamagedGood = _RmaHeadRepo.DamagedGoodsApprovedPrint(RMA, status);
            if (DamagedGood != null)
            {
                if (DamagedGood.DamageGoodsItem != null)
                {
                    var Site = _SiteConfigRepo.GetSiteCodeAddress();
                    DamagedGood.SiteName = Site.SiteName;
                    DamagedGood.SiteAdress = Site.SiteAddress;
                    var SupplierName = _SupplierBusiness.GetNameSupplier(DamagedGood.SupplierId);
                    DamagedGood.Supplier = SupplierName;
                    foreach (var GoodsItem in DamagedGood.DamageGoodsItem)
                    {
                        var Tax = _ItemRepo.StoreProcedureIvaIeps(GoodsItem.PartNumber);
                        var Ieps = Tax.Ieps;
                        var Iva = Tax.Iva;
                        GoodsItem.IEPS = GoodsItem.Amount * (Ieps / 100);
                        GoodsItem.IVA = (GoodsItem.Amount + GoodsItem.IEPS) * (Iva / 100);
                    }
                }
            }
            return DamagedGood;
        }

        public List<DAMAGEDS_GOODS> GetFoliosBySupplierAndFechaRange(string supplierId, string SiteCode)
        {
            int idSup = string.IsNullOrWhiteSpace(supplierId.Trim()) ? 0 : int.Parse(supplierId);

            if (idSup > 0)
                return _RmaHeadRepo.GetFoliosBySupplierByDates(idSup, SiteCode);

            return _RmaHeadRepo.GetFoliosBySupplierByDates(SiteCode);
        }

        public bool UpdateRmaAprovalCancel(string Folio, bool Flag, string Comment, string User, string type, string program) => _RmaHeadRepo.UpdateRmaAprovalCancel(Folio, Flag, Comment, User, type, program);

        public bool UpdateRmaAprovalCancel(List<DamagedsGoodsModel> Model, string Folio, bool Flag, string Comment, string User, string type, string program, Decimal? weight) => _RmaHeadRepo.UpdateRmaAprovalCancel(Model, Folio, Flag, Comment, User, type, program, weight);

        public StoreProcedureResult StoreProcedureDamagedGoods(string Document, string User, string ProgramId) => _RmaHeadRepo.StoreProcedureDamagedGoods(Document, User, ProgramId);

        public Tuple<string, List<ScrapControlAndroidModel>> AddDamageGoods(string items, string user, string site)
        {
            string v = "{\"Items\":" + items + "}";
            var result = JsonConvert.DeserializeObject<ScrapControlAndroidModel>(v);
            List<ScrapControlAndroidModel> list = new List<ScrapControlAndroidModel>();
            for (int i = 0; i < result.Items.Count; i++)
            {
                ScrapControlAndroidModel model = new ScrapControlAndroidModel
                {
                    part_number = result.Items[i].part_number,
                    part_description = result.Items[i].part_description,
                    quantity = result.Items[i].quantity,
                    comment = result.Items[i].cause_blocking
                };
                list.Add(model);
            }

            string document = _movementNumberRepository.ExecSp_Get_Next_DocumentScrap();
            DAMAGEDS_GOODS header = new DAMAGEDS_GOODS
            {
                damaged_goods_doc = document,
                site_code = site,
                process_type = "SCRAP",
                process_status = 1,
                cuser = user,
                cdate = DateTime.Now,
                program_id = "AndroidCMR"
            };
            var saveHeader = _RmaHeadRepo.AddDamageGoodsAndroid(header);
            if (saveHeader != 0)
            {
                for (int i = 0; i < list.Count; i++)
                {
                    var price = _itemSupplierBusiness.GetPriceByPartNumber(list[i].part_number);
                    DAMAGEDS_GOODS_ITEM item = new DAMAGEDS_GOODS_ITEM
                    {
                        damaged_goods_doc = document,
                        part_number = list[i].part_number,
                        quantity = Decimal.Parse(list[i].quantity),
                        scrap_reason = list[i].comment,
                        price = price.Price,
                        amount_doc = price.Price * decimal.Parse(list[i].quantity),
                        cuser = user,
                        cdate = DateTime.Now,
                        program_id = "AndroidCMR",
                        process_status = 1
                    };
                    _damagedGoodsItemRepository.AddDamageGoodsItemAndroid(item);
                }
                return Tuple.Create(document, list);
            }
            else
            {
                List<ScrapControlAndroidModel> lista = new List<ScrapControlAndroidModel>();
                return Tuple.Create("ERROR", lista);
            }
        }

        public Tuple<string, List<ScrapControlAndroidModel>> AddDamageGoodsDirect(string items, string user, string site)
        {
            string v = "{\"Items\":" + items + "}";
            var result = JsonConvert.DeserializeObject<ScrapControlAndroidModel>(v);
            //Modelo para part_number unico con diferente cantidad
            List<ScrapControlAndroidModel> list = new List<ScrapControlAndroidModel>();
            //Modelo para part_number sumado la cantidad
            List<ScrapControlAndroidModel> listSum = new List<ScrapControlAndroidModel>();
            for (int i = 0; i < result.Items.Count; i++)
            {
                ScrapControlAndroidModel model = new ScrapControlAndroidModel
                {
                    part_number = result.Items[i].part_number,
                    part_description = result.Items[i].part_description,
                    quantity = result.Items[i].quantity,
                    comment = result.Items[i].cause_blocking
                };
                list.Add(model);

                //Busca y agrega el codigo
                var index = listSum.FindIndex(w => w.part_number == result.Items[i].part_number);
                if (index >= 0)
                {
                    listSum[index].quantity = (Convert.ToInt32(listSum[index].quantity) + Convert.ToInt32(result.Items[i].quantity)).ToString();
                }
                else
                {
                    ScrapControlAndroidModel modelSum = new ScrapControlAndroidModel
                    {
                        part_number = result.Items[i].part_number,
                        part_description = result.Items[i].part_description,
                        quantity = result.Items[i].quantity,
                        comment = result.Items[i].cause_blocking
                    };
                    listSum.Add(modelSum);
                }
            }

            string document = _movementNumberRepository.ExecSp_Get_Next_DocumentScrap();

            for (int i = 0; i < listSum.Count; i++)
            {
                SCRAP_CONTROL scrapItem = new SCRAP_CONTROL
                {
                    scrap_document = document,
                    site_code = site,
                    part_number = listSum[i].part_number,
                    quantity = Decimal.Parse(listSum[i].quantity),
                    cause_blocking = listSum[i].comment,
                    scrap_status = 0,
                    cdate = DateTime.Now,
                    cuser = user,
                    program_id = "AndroidBMM"
                };
                _scrapControlRepository.AddScrapControlItemAndroid(scrapItem);
                var doc = _scrapControlRepository.StoreProcedureScrap(site, listSum[i].part_number, Decimal.Parse(listSum[i].quantity), "SL01", "MRM01", "AndroidBMM", user);
                if (doc.Document != null) { }
                else
                {
                    List<ScrapControlAndroidModel> lista = new List<ScrapControlAndroidModel>();
                    return Tuple.Create("ERROR", lista);
                }
            }

            DAMAGEDS_GOODS header = new DAMAGEDS_GOODS
            {
                damaged_goods_doc = document,
                site_code = site,
                process_type = "SCRAP",
                process_status = 1,
                cuser = user,
                cdate = DateTime.Now,
                program_id = "AndroidCMR"
            };
            var saveHeader = _RmaHeadRepo.AddDamageGoodsAndroid(header);
            if (saveHeader != 0)
            {
                for (int i = 0; i < listSum.Count; i++)
                {
                    var price = _itemSupplierBusiness.GetPriceByPartNumber(listSum[i].part_number);
                    DAMAGEDS_GOODS_ITEM item = new DAMAGEDS_GOODS_ITEM
                    {
                        damaged_goods_doc = document,
                        part_number = listSum[i].part_number,
                        quantity = Decimal.Parse(listSum[i].quantity),
                        scrap_reason = listSum[i].comment,
                        price = price.Price,
                        amount_doc = price.Price * Decimal.Parse(listSum[i].quantity),
                        cuser = user,
                        cdate = DateTime.Now,
                        program_id = "AndroidCMR",
                        process_status = 1
                    };
                    var value = _damagedGoodsItemRepository.AddDamageGoodsItemAndroid(item);
                    if (value <= 0)
                        return Tuple.Create("ERROR", list);
                }

                for (int i = 0; i < list.Count; i++)
                {
                    var price = _itemSupplierBusiness.GetPriceByPartNumber(list[i].part_number);
                    DAMAGEDS_GOODS_ITEM_SINGLE item = new DAMAGEDS_GOODS_ITEM_SINGLE
                    {
                        damaged_goods_doc_unique = document,
                        part_number = list[i].part_number,
                        quantity = Decimal.Parse(list[i].quantity),
                        scrap_reason = list[i].comment,
                        price = price.Price,
                        amount_doc = price.Price * Decimal.Parse(list[i].quantity),
                        cuser = user,
                        cdate = DateTime.Now,
                        program_id = "AndroidCMR",
                        process_status = 1
                    };
                    var value = _damagedGoodsItemRepository.AddDamageGoodsItemAndroidSingle(item);
                    if (value <= 0)
                        return Tuple.Create("ERROR", list);
                }
                return Tuple.Create(document, list);
            }
            else
            {
                List<ScrapControlAndroidModel> lista = new List<ScrapControlAndroidModel>();
                return Tuple.Create("ERROR", lista);
            }
        }

        public DamagedGoodsHeadModel AddDamageGoodsRma(string items, string user, int Status, string Supplier)
        {
            try
            {
                string v = "{\"Items\":" + items + "}";
                var result = JsonConvert.DeserializeObject<ScrapControlAndroidModel>(v);
                List<ScrapControlAndroidModel> list = new List<ScrapControlAndroidModel>();
                foreach (var item in result.Items)
                {
                    ScrapControlAndroidModel model = new ScrapControlAndroidModel
                    {
                        part_number = item.part_number,
                        quantity = item.quantity,
                        comment = item.comment
                    };
                    list.Add(model);
                }
                string site = _SiteConfigRepo.GetSiteCode();
                string siteName = _SiteConfigRepo.GetSiteCodeName();

                string document = _movementNumberRepository.ExecSp_Get_Next_DocumentScrap();

                int remision = 0;
                remision = _damagedGoodsItemRepository.BanatiIdSupplier(Supplier);
                DAMAGEDS_GOODS header = new DAMAGEDS_GOODS
                {
                    damaged_goods_doc = document,
                    site_code = site,
                    process_type = "REMISION",
                    process_status = Status,
                    cuser = user,
                    cdate = DateTime.Now,
                    program_id = "AndroidCMR",
                    supplier_id = remision//139; // banati
                };
                var saveHeader = _RmaHeadRepo.AddDamageGoodsAndroid(header);
                if (saveHeader != 0)
                {
                    foreach (var i in list)
                    {
                        DAMAGEDS_GOODS_ITEM item = new DAMAGEDS_GOODS_ITEM
                        {
                            damaged_goods_doc = document,
                            part_number = i.part_number
                        };
                        item.price = _itemRepository.GetMapPrice(item.part_number);
                        item.quantity = decimal.Parse(i.quantity);
                        item.amount_doc = item.price * item.quantity;
                        item.scrap_reason = i.comment;
                        item.cuser = user;
                        item.cdate = DateTime.Now;
                        item.program_id = "AndroidCMR";
                        item.process_status = Status;
                        _damagedGoodsItemRepository.AddDamageGoodsItemAndroid(item);
                    }
                    return _RmaHeadRepo.GetRmaHeadByDocument(document);
                }
                else
                    return null;
            }
            catch (Exception ex)
            {
                string msg = ex.Message;
                return null;
            }
        }

        public List<DAMAGEDS_GOODS> GetFoliosByRemisionAprovalByDates(string supplierId)
        {
            int idSup = string.IsNullOrWhiteSpace(supplierId.Trim()) ? 0 : int.Parse(supplierId);

            if (idSup > 0)
                return _RmaHeadRepo.GetFoliosByRemisionAprovalByDates(idSup);

            return _RmaHeadRepo.GetFoliosByRemisionAprovalByDates();
        }

        public List<DamagedsGoodsModel> GetDocumentsByDates(string fechaIni, string fechaFin)
        {
            DateTime fechaInicial;
            DateTime fechaFinal;
            List<DamagedsGoodsModel> listHeaders;
            if (fechaIni == "" && fechaFin == "")
            {
                fechaInicial = DateTime.Parse(DateTime.Now.AddDays(-7).ToString());
                fechaFinal = DateTime.Parse(DateTime.Now.ToString());
                listHeaders = _RmaHeadRepo.GetDocumentsByDates(fechaInicial.Date, fechaFinal.Date);
            }
            else
            {
                fechaInicial = DateTime.Parse(fechaIni);
                fechaFinal = DateTime.Parse(fechaFin);
                listHeaders = _RmaHeadRepo.GetDocumentsByDates(fechaInicial.Date, fechaFinal.Date);
            }
            if (listHeaders.Count > 0)
                return listHeaders;
            else
                return listHeaders;
        }

        public List<DamagedsGoodsModel> GetItemsByDocumentReport(DateTime? fechaIni, DateTime? fechaFin, int status) => _RmaHeadRepo.GetAllDocumentsScraptReport(fechaIni, fechaFin, status);
        public List<DamagedsGoodsModel> GetItemsByDocumentApproval(DateTime? fechaIni, DateTime? fechaFin, int status) => _RmaHeadRepo.GetAllDocumentsScrapApproval(fechaIni, fechaFin, status);

        public List<DamagedsGoodsModel> GetDocumentsByDatesInStatus2(string fechaIni, string fechaFin)
        {
            DateTime fechaInicial;
            DateTime fechaFinal;
            List<DamagedsGoodsModel> listHeaders;
            if (fechaIni == "" && fechaFin == "")
            {
                fechaInicial = DateTime.Parse(DateTime.Now.AddDays(-7).ToString());
                fechaFinal = DateTime.Parse(DateTime.Now.ToString());
                listHeaders = _RmaHeadRepo.GetDocumentsByDatesInStatus2(fechaInicial.Date, fechaFinal.Date);
            }
            else
            {
                fechaInicial = DateTime.Parse(fechaIni);
                fechaFinal = DateTime.Parse(fechaFin);
                listHeaders = _RmaHeadRepo.GetDocumentsByDatesInStatus2(fechaInicial.Date, fechaFinal.Date);
            }
            if (listHeaders.Count > 0)
                return listHeaders;
            else
                return listHeaders;
        }

        public int UpdateDamageGoodsStatus(string msj, string document, string user)
        {
            var modelo = _RmaHeadRepo.GetDamageGoodsHeader(document);
            if (modelo != null)
            {
                if (msj == "APROBADO")
                {
                    modelo.process_status = 2;
                    modelo.uuser = user;
                    modelo.udate = DateTime.Now;
                    return _RmaHeadRepo.UpdateDamageGoodsStatus(modelo);
                }
                else
                {
                    modelo.process_status = 3;
                    modelo.uuser = user;
                    modelo.udate = DateTime.Now;
                    return _RmaHeadRepo.UpdateDamageGoodsStatus(modelo);
                }
            }
            else
                return 9;
        }

        public int UpdateDamageGoodsStatusSingle(string msj, string document, string user)
        {
            var modelo = _RmaHeadRepo.GetDamageGoodsHeader(document);
            if (modelo != null)
            {
                if (msj == "APROBADO")
                {
                    modelo.process_status = 2;
                    modelo.uuser = user;
                    modelo.udate = DateTime.Now;
                    return _RmaHeadRepo.UpdateDamageGoodsStatus(modelo);
                }
                else
                {
                    modelo.process_status = 3;
                    modelo.uuser = user;
                    modelo.udate = DateTime.Now;
                    return _RmaHeadRepo.UpdateDamageGoodsStatus(modelo);
                }
            }
            else
                return 9;
        }

        public bool UpdateDamageGoodsItemStatus(List<DamagedsGoodsModel> Model, string document, string msj, string user) => _RmaHeadRepo.UpdateDamageGoodsItemStatus(Model, document, msj, user);

        public bool UpdateDamageGoodsItemStatusSingle(List<DamagedsGoodsModel> Model, string document, string msj, string user) => _RmaHeadRepo.UpdateDamageGoodsItemStatusSingle(Model, document, msj, user);

        public bool UpdateDamegeGoodsConfirmacionRemision(string Folio, Decimal? Weight, string User, string program_id) => _RmaHeadRepo.UpdateDamegeGoodsConfirmacionRemision(Folio, Weight, User, program_id);

        public bool UpdateDamegeGoodsConfirmacionRemision(List<DamagedsGoodsModel> Model, string Folio, Decimal? Weight, string User, string program_id) => _RmaHeadRepo.UpdateDamegeGoodsConfirmacionRemision(Model, Folio, Weight, User, program_id);

        public bool UpdateRmaRemisionAprovalReject(string Folio, int Status, string Comment, string User, string type, string program) => _RmaHeadRepo.UpdateRmaRemisionAprovalReject(Folio, Status, Comment, User, type, program);

        public bool UpdateRmaRemisionAprovalReject(List<DamagedsGoodsModel> Model, string Folio, int Status, string Comment, string User, string type, string program) => _RmaHeadRepo.UpdateRmaRemisionAprovalReject(Model, Folio, Status, Comment, User, type, program);

        public bool UpdateRmaRemisionInitial(List<DamagedsGoodsModel> Model, string Folio, int Status, string Comment, string User, string program) => _RmaHeadRepo.UpdateRmaRemisionInitial(Model, Folio, Status, Comment, User, program);
        public bool UpdateRmaRemisionCancel(string Folio, string Comment, string User, string program) => _RmaHeadRepo.UpdateRmaRemisionCancel( Folio,  Comment,  User,  program);
      
        public bool RmaCancelRequest(string Folio, string Comment, string User) => _RmaHeadRepo.RmaCancelRequest(Folio, Comment, User);

        public int RmaRejectCancelRequest(string Folio, string Comment, string User) => _RmaHeadRepo.RmaRejectCancelRequest(Folio, Comment, User);

        public bool RmaApproveCancelRequest(string Folio, string Comment, string User) => _RmaHeadRepo.RmaApproveCancelRequest(Folio, Comment, User);

        public bool UpdateWeight(string folio, decimal? Weight) => _RmaHeadRepo.UpdateWeight(folio, Weight);

        public List<DamagedGoodsHeadModel> GetRMAHeaderDetail(string id)
        {
            var DamagedGood = _RmaHeadRepo.GetRMAHeaderDetail(id);

            foreach (var item in DamagedGood)
            {
                var Site = _SiteConfigRepo.GetSiteCodeAddress();
                item.SiteName = Site.SiteName;
                item.SiteAdress = Site.SiteAddress;
                var SupplierName = _SupplierBusiness.GetNameSupplier(item.SupplierId);
                item.Supplier = SupplierName;
                foreach (var GoodsItem in item.DamageGoodsItem)
                {
                    var Tax = _ItemRepo.StoreProcedureIvaIeps(GoodsItem.PartNumber);
                    var Ieps = Tax.Ieps;
                    var Iva = Tax.Iva;
                    GoodsItem.IEPS = GoodsItem.Amount * (Ieps / 100);
                    GoodsItem.IVA = (GoodsItem.Amount + GoodsItem.IEPS) * (Iva / 100);
                }
            }

            return DamagedGood;
        }

        public List<DamagedsGoodsModel> GetScrapItemsByFolio(string folio) => _damagedGoodsItemRepository.GetScrapItemsByFolio(folio);

        public List<DamagedsGoodsModel> GetScrapItemsDepartment(DateTime dateInitial, DateTime dateFinish, string department, string status) => _RmaHeadRepo.GetScrapItemsDepartment(dateInitial, dateFinish, department, status);

        public bool DamagedsGoodsEditStatusPrint(string Document, int Status, string program_id) => _RmaHeadRepo.DamagedsGoodsEditStatusPrint(Document, Status, program_id);

        public int DamagedsGoodsStatusPrintReference(string Reference) => _RmaHeadRepo.DamagedsGoodsStatusPrintReference(Reference);

        public int DamagedsGoodsStatusPrintDocument(string Document) => _RmaHeadRepo.DamagedsGoodsStatusPrintDocument(Document);

        public List<DamagedsGoodsModel> GetScrapDepartmentTotal(DateTime dateInitial, DateTime dateFinish, int Status, int Department, bool Detail) => _RmaHeadRepo.GetScrapDepartmentTotal(dateInitial, dateFinish, Status, Department, Detail);
        public Tuple<bool,string> ChangeStatusScrapCancelation(string doc , int new_status , int old_status, string site ,string user )
        {
            if(new_status == 8)
            {
                // SP AJUSTA INVENTARIO
                return _RmaHeadRepo.CancelScrapDocumentSP(doc, site, user, "SSM005.cshtml");
            }else
            {
                if (_RmaHeadRepo.UpdateScrapHeaderStatusCancelation(doc, new_status, user)) //Header
                    if (_RmaHeadRepo.UpdateScrapDetailStatusCancelation(doc, new_status, old_status, user)) //Details
                        return new Tuple<bool, string>(true,"");
                    else
                        return new Tuple<bool, string>(false, "Ha ocurrido un error");
                else
                    return new Tuple<bool, string>(false, "Ha ocurrido un error");
            }
            

        }
    }
}