﻿using App.BLL.Item;
using App.DAL;
using App.DAL.Item;
using App.DAL.MovementNumber;
using App.DAL.ScrapControl;
using App.Entities;
using App.Entities.ViewModels.Android;
using App.Entities.ViewModels.DamagedsGoods;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;

namespace App.BLL.ScrapControl
{
    public class ScrapControlBusiness
    {
        private readonly DamagedsGoodsRepository _RmaHeadRepo;
        private readonly ScrapControlRepository _scrapControlRepository;
        private readonly MovementNumberRepository _movementNumberRepository;
        private readonly ItemSupplierBusiness _itemSupplierBusiness;
        private readonly DamagedsGoodsItemRepository _damagedGoodsItemRepository;
        private readonly ItemStockBusiness _itemStockBusiness;

        public ScrapControlBusiness()
        {
            _scrapControlRepository = new ScrapControlRepository();
            _movementNumberRepository = new MovementNumberRepository();
            _RmaHeadRepo = new DamagedsGoodsRepository();
            _itemSupplierBusiness = new ItemSupplierBusiness();
            _damagedGoodsItemRepository = new DamagedsGoodsItemRepository();
            _itemStockBusiness = new ItemStockBusiness();
        }

        public string SendItemsToBlock(string items, string usuario, string site_code)
        {
            string v = "{\"Items\":" + items + "}";
            var result = JsonConvert.DeserializeObject<ScrapControlAndroidModel>(v);
            List<ScrapControlAndroidModel> list = new List<ScrapControlAndroidModel>();
            for (int i = 0; i < result.Items.Count; i++)
            {
                ScrapControlAndroidModel model = new ScrapControlAndroidModel();
                model.part_number = result.Items[i].part_number;
                model.quantity = result.Items[i].quantity;
                model.comment = result.Items[i].cause_blocking;
                list.Add(model);
            }
            var Documento = _movementNumberRepository.ExecSp_Get_Next_DocumentScrap();
            try
            {
                for (int i = 0; i < list.Count; i++)
                {
                    SCRAP_CONTROL scrapItem = new SCRAP_CONTROL();
                    scrapItem.scrap_document = Documento;
                    scrapItem.site_code = site_code;
                    scrapItem.part_number = list[i].part_number;
                    scrapItem.quantity = Decimal.Parse(list[i].quantity);
                    scrapItem.cause_blocking = list[i].comment;
                    scrapItem.scrap_status = 0;
                    scrapItem.cdate = DateTime.Now;
                    scrapItem.cuser = usuario;
                    scrapItem.program_id = "AndroidBMM";
                    _scrapControlRepository.AddScrapControlItemAndroid(scrapItem);
                    var doc = _scrapControlRepository.StoreProcedureScrap(site_code, list[i].part_number, Decimal.Parse(list[i].quantity), "SL01", "MRM01", "AndroidBMM", usuario);
                    if (doc.Document != null) { }
                    else { return ""; }
                }
                return "COMPLETADO";
            }
            catch (Exception ex)
            {
                var msg = ex.Message;
                return "";
            }
        }

        public bool SendItemsToBlockRemision(string items, string usuario, string site_code)
        {
            bool check = true;
            string v = "{\"Items\":" + items + "}";
            var result = JsonConvert.DeserializeObject<DamagedsGoodsAndroid>(v);
            List<DamagedsGoodsModel> list = new List<DamagedsGoodsModel>();
            for (int i = 0; i < result.Items.Count; i++)
            {
                DamagedsGoodsModel model = new DamagedsGoodsModel();
                model.PartNumber = result.Items[i].part_number;
                model.Quantity = _itemStockBusiness.GetItemInStorageTypeMermaDifferenceReal(result.Items[i].part_number, result.Items[i].quantity);
                model.Description = "REMISION_DIR";
                list.Add(model);
            }
            var Documento = _movementNumberRepository.ExecSp_Get_Next_DocumentScrap();
            try
            {
                for (int i = 0; i < list.Count; i++)
                {
                    SCRAP_CONTROL scrapItem = new SCRAP_CONTROL();
                    scrapItem.scrap_document = Documento;
                    scrapItem.site_code = site_code;
                    scrapItem.part_number = list[i].PartNumber;
                    scrapItem.quantity = list[i].Quantity;
                    scrapItem.cause_blocking = list[i].Description;
                    scrapItem.scrap_status = 0;
                    scrapItem.cdate = DateTime.Now;
                    scrapItem.cuser = usuario;
                    scrapItem.program_id = "Android_REMISION_DIR";
                    if (scrapItem.quantity > 0) //Mayor a cero para restarle la cantidad correcta
                    {
                        _scrapControlRepository.AddScrapControlItemAndroid(scrapItem);
                        var doc = _scrapControlRepository.StoreProcedureScrap(site_code, list[i].PartNumber, list[i].Quantity, "SL01", "MRM01", "Android_REMISION_DIR", usuario);
                        if (doc.Document != null) { }
                        else { check = false; }
                    }
                }
                return check;
            }
            catch (Exception ex)
            {
                var msg = ex.Message;
                return false;
            }
        }

        public bool SendItemsToBlockRMA(string items, string usuario, string site_code)
        {
            bool check = true;
            string v = "{\"Items\":" + items + "}";
            var result = JsonConvert.DeserializeObject<DamagedsGoodsAndroid>(v);
            List<DamagedsGoodsModel> list = new List<DamagedsGoodsModel>();
            for (int i = 0; i < result.Items.Count; i++)
            {
                DamagedsGoodsModel model = new DamagedsGoodsModel();
                model.PartNumber = result.Items[i].part_number;
                model.Quantity = _itemStockBusiness.GetItemInStorageTypeMermaDifferenceReal(result.Items[i].part_number, result.Items[i].quantity);
                model.Description = "RMA_DIRECT";
                list.Add(model);
            }
            var Documento = _movementNumberRepository.ExecSp_Get_Next_DocumentScrap();
            try
            {
                for (int i = 0; i < list.Count; i++)
                {
                    SCRAP_CONTROL scrapItem = new SCRAP_CONTROL();
                    scrapItem.scrap_document = Documento;
                    scrapItem.site_code = site_code;
                    scrapItem.part_number = list[i].PartNumber;
                    scrapItem.quantity = list[i].Quantity;
                    scrapItem.cause_blocking = list[i].Description;
                    scrapItem.scrap_status = 0;
                    scrapItem.cdate = DateTime.Now;
                    scrapItem.cuser = usuario;
                    scrapItem.program_id = "RMA_DIRECT";
                    if (scrapItem.quantity > 0) //Mayor a cero para restarle la cantidad correcta
                    {
                        _scrapControlRepository.AddScrapControlItemAndroid(scrapItem);
                        var doc = _scrapControlRepository.StoreProcedureScrap(site_code, list[i].PartNumber, list[i].Quantity, "SL01", "MRM01", "RMA_DIRECT", usuario);
                        if (doc.Document != null) { }
                        else { check = false; }
                    }
                }
                return check;
            }
            catch (Exception ex)
            {
                var msg = ex.Message;
                return false;
            }
        }

        public bool SendItemsToBlockFromQuery(string site_code, string usuario)
        {
            bool check = true;
            var list = _scrapControlRepository.GenericGetList();
            var Documento = _movementNumberRepository.ExecSp_Get_Next_DocumentScrap();
            try
            {
                for (int i = 0; i < list.Count; i++)
                {
                    SCRAP_CONTROL scrapItem = new SCRAP_CONTROL();
                    scrapItem.scrap_document = Documento;
                    scrapItem.site_code = site_code;
                    scrapItem.part_number = list[i].PartNumber;
                    scrapItem.quantity = list[i].Quantity;
                    scrapItem.cause_blocking = list[i].Description;
                    scrapItem.scrap_status = 0;
                    scrapItem.cdate = DateTime.Now;
                    scrapItem.cuser = "";
                    scrapItem.program_id = "RMA_DIRECT";
                    if (scrapItem.quantity > 0) //Mayor a cero para restarle la cantidad correcta
                    {
                        _scrapControlRepository.AddScrapControlItemAndroid(scrapItem);
                        var doc = _scrapControlRepository.StoreProcedureScrap(site_code, list[i].PartNumber, list[i].Quantity, "SL01", "MRM01", "RMA_DIRECT", usuario);
                        if (doc.Document != null) { }
                        else { check = false; }
                    }
                }
                return check;
            }
            catch (Exception ex)
            {
                var msg = ex.Message;
                return false;
            }
        }

        public string SendItemsToBlockDirect(string items, string usuario, string site_code)
        {
            string v = "{\"Items\":" + items + "}";
            var result = JsonConvert.DeserializeObject<ScrapControlAndroidModel>(v);
            List<ScrapControlAndroidModel> list = new List<ScrapControlAndroidModel>();
            for (int i = 0; i < result.Items.Count; i++)
            {
                ScrapControlAndroidModel model = new ScrapControlAndroidModel();
                model.part_number = result.Items[i].part_number;
                model.quantity = result.Items[i].quantity;
                model.comment = result.Items[i].cause_blocking;
                list.Add(model);
            }
            var Documento = _movementNumberRepository.ExecSp_Get_Next_DocumentScrap();
            try
            {
                for (int i = 0; i < list.Count; i++)
                {
                    SCRAP_CONTROL scrapItem = new SCRAP_CONTROL();
                    scrapItem.scrap_document = Documento;
                    scrapItem.site_code = site_code;
                    scrapItem.part_number = list[i].part_number;
                    scrapItem.quantity = Decimal.Parse(list[i].quantity);
                    scrapItem.cause_blocking = list[i].comment;
                    scrapItem.scrap_status = 0;
                    scrapItem.cdate = DateTime.Now;
                    scrapItem.cuser = usuario;
                    scrapItem.program_id = "AndroidBMM";
                    _scrapControlRepository.AddScrapControlItemAndroid(scrapItem);
                    var doc = _scrapControlRepository.StoreProcedureScrap(site_code, list[i].part_number, Decimal.Parse(list[i].quantity), "SL01", "MRM01", "AndroidBMM", usuario);
                    if (doc.Document != null) { }
                    else { return ""; }
                }
                string document = _movementNumberRepository.ExecSp_Get_Next_DocumentScrap();
                DAMAGEDS_GOODS header = new DAMAGEDS_GOODS();
                header.damaged_goods_doc = document;
                header.site_code = site_code;
                header.process_type = "SCRAP";
                header.process_status = 1;
                header.cuser = usuario;
                header.cdate = DateTime.Now;
                header.program_id = "AndroidCMR";
                var saveHeader = _RmaHeadRepo.AddDamageGoodsAndroid(header);
                if (saveHeader != 0)
                {
                    for (int i = 0; i < list.Count; i++)
                    {
                        var price = _itemSupplierBusiness.GetPriceByPartNumber(list[i].part_number);
                        DAMAGEDS_GOODS_ITEM item = new DAMAGEDS_GOODS_ITEM();
                        item.damaged_goods_doc = document;
                        item.part_number = list[i].part_number;
                        item.quantity = Decimal.Parse(list[i].quantity);
                        item.scrap_reason = list[i].comment;
                        item.price = price.Price;
                        item.amount_doc = price.Price * Decimal.Parse(list[i].quantity);
                        item.cuser = usuario;
                        item.cdate = DateTime.Now;
                        item.program_id = "AndroidCMR";
                        item.process_status = 1;
                        _damagedGoodsItemRepository.AddDamageGoodsItemAndroid(item);
                    }
                }
                return "COMPLETADO";
            }
            catch (Exception ex)
            {
                var msg = ex.Message;
                return "";
            }
        }

        public StoreProcedureResult StoreProcedureScrap(string Site, string PartNumber, decimal Quantity, string Sl, string ToSl, string ProgramId, string User)
        {
            return _scrapControlRepository.StoreProcedureScrap(Site, PartNumber, Quantity, Sl, ToSl, ProgramId, User);
        }
    }
}