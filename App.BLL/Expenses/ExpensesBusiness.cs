﻿using App.BLL.Site;
using App.DAL.Expenses;
using App.Entities.ViewModels.Creditors;
using App.Entities.ViewModels.Expenses;
using System;
using System.Collections.Generic;

namespace App.BLL.Expenses
{
    public class ExpensesBusiness
    {
        private readonly ExpensesRepository _ExpensesRepository;
        private readonly SiteConfigBusiness _siteConfigBusiness;
        private readonly UserMasterBusiness _userMasterBusiness;

        public ExpensesBusiness()
        {
            _ExpensesRepository = new ExpensesRepository();
            _siteConfigBusiness = new SiteConfigBusiness();
            _userMasterBusiness = new UserMasterBusiness();
        }

        public int ExpenseCancelPendingStore(int xml_id, string user ) => _ExpensesRepository.ExpenseCancelPendingStore(xml_id, user, _siteConfigBusiness.GetSiteCode());
        public int ExpenseReverseStatus(int xml_id, string comment, int old_tatus, string program_id, string user) => _ExpensesRepository.ExpenseReverseStatus(xml_id, comment, old_tatus, program_id, user, _siteConfigBusiness.GetSiteCode());
        public int ExpenseCancelFinal(int xml_id, string comment, string user) => _ExpensesRepository.ExpenseCancelFinal(xml_id, comment, user, _siteConfigBusiness.GetSiteCode());
        public int ExpenseCancelAproval(int xml_id, string comment, bool expense_month, string user) => _ExpensesRepository.ExpenseCancelAproval(xml_id, comment, user, expense_month, _siteConfigBusiness.GetSiteCode());
        public int ExpenseCancelAuthorization(int xml_id, bool flag_month, bool flag_today, string comment, string user) => _ExpensesRepository.ExpenseCancelAuthorization(xml_id,  flag_month,  flag_today, comment, user, _siteConfigBusiness.GetSiteCode());
        public List<ExpensesReportModel> ExpensesReport(string SupplierId, string Category, DateTime? BeginDate, DateTime? EndDate, string Currency, string typeExpense, string Status) => _ExpensesRepository.ExpensesReportNew(_siteConfigBusiness.GetSiteCode(), SupplierId, Category, BeginDate, EndDate, Currency, typeExpense, Status);

        public List<ItemCreditorsCategoryModel> AllCreditorsCategories() => _ExpensesRepository.AllCreditorsCategories();

        public List<ExpensesChart> CreditorsExpensive() => _ExpensesRepository.CreditorsExpensive(_siteConfigBusiness.GetSiteCode());

        public List<ExpensesChart> CreditorsExpensiveQuantity() => _ExpensesRepository.CreditorsExpensiveQuantity(_siteConfigBusiness.GetSiteCode());

        public List<ExpensesChart> CreditorsExpensiveReferenceType() => _ExpensesRepository.CreditorsExpensiveReferenceType(_siteConfigBusiness.GetSiteCode());

        public List<ExpensesReportModel> ExpensesReportDetail(string SupplierId, string Category, DateTime? BeginDate, DateTime? EndDate, string Currency, string typeExpense, string Status) => _ExpensesRepository.ExpensesReportDetailNew(_siteConfigBusiness.GetSiteCode(), SupplierId, Category, BeginDate, EndDate, Currency, typeExpense, Status);

        public List<ExpensesReportModel> ExpensesReport(string Site_code, string SupplierId, string Category, DateTime? BeginDate, DateTime? EndDate, string Currency, string typeExpense, string Status) => _ExpensesRepository.ExpensesReportNew(Site_code, SupplierId, Category, BeginDate, EndDate, Currency, typeExpense, Status);

        public List<ItemXMLDetail> getAllItemsById(int xml_id) => _ExpensesRepository.GetAllItemsById(xml_id);

        public ItemXMLDetail GetAllItemHeaderById(int xml_id)
        {
            var header = _ExpensesRepository.GetAllItemHeaderById(xml_id);
            var nameUser = _userMasterBusiness.getCompleteName(header.cuser);
            if (nameUser != "")
                header.cuser = nameUser;
            return header;
        }
        public bool UpdatePrintXMLHeader(int xml_id) => _ExpensesRepository.UpdatePrintXMLHeader(xml_id);
    }
}