﻿using App.BLL.Approvals;
using App.BLL.Site;
using App.Common;
using App.DAL.Approval;
using App.DAL.Expenses;
using App.DAL.Site;
using App.DAL.Supplier;
using App.Entities.ViewModels.Approvals;
using App.Entities.ViewModels.InternalRequirement;
using App.GlobalEntities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Linq;

namespace App.BLL.Expenses
{
    public class ExpenseStartReq
    {
        public string Username { get; set; }
        public string ProgramId { get; set; }
        public string SiteCode { get; set; }
        public List<InternalRequirementModel> Products { get; set; }
        public bool ToNewProjects { get; set; }
    }

    public class ExpenseUpdateReq
    {
        public string approval_id { get; set; }
        public string process_name { get; set; }
        public int step_level { get; set; }
        public string emp_no { get; set; }
        public string process_group { get; set; }
        public string approval_type { get; set; }
        public string approval_status { get; set; }
        public string remark { get; set; }
        public DateTime? approval_datetime { get; set; }
        public string new_approval_status { get; set; }
        public string new_remark { get; set; }
        public string user_name { get; set; }
        public string program_id { get; set; }
    }

    public class ExpenseRequisitionsBussines
    {
        private readonly ExpenseRequisitionsRepository _ExpenseRequisitionsRepository = new ExpenseRequisitionsRepository();
        private readonly UserMasterRepository _userMasterRepository = new UserMasterRepository();
        private readonly ApprovalsBusiness _approvalBusiness = new ApprovalsBusiness();
        private readonly ApprovalsRepository _approvalsRepository = new ApprovalsRepository();
        private readonly MaSupplierRepository _maSupplierRepository = new MaSupplierRepository();
        private readonly SiteConfigBusiness _siteConfigBusiness = new SiteConfigBusiness();
        private readonly ExpensesRepository _expensesRepository = new ExpensesRepository();
        private readonly SiteConfigRepository _siteConfigRepository = new SiteConfigRepository();

        public Result<List<INTERNAL_REQUIREMENT_HEADER>> GetList()
        {
            throw new NotImplementedException();
        }

        public Result<List<ExpenseRequirementModel>> ApprovalHistoryList(ExpenseRequisitionFilter filter) => Result.OK(_ExpenseRequisitionsRepository.ApprovalHistory(filter));

        public Result<List<ExpenseRequirementModel>> RequestsHistoryList(ExpenseRequisitionFilter filter) => Result.OK(_ExpenseRequisitionsRepository.RequestsHistory(filter));

        public Result<Empty> ExpenseStart(ExpenseStartReq data)
        {
            var model = new INTERNAL_REQUIREMENT_HEADER
            {
                requirement_type = "gasto",
                cuser = data.Username,
                cdate = DateTime.Now,
                requirement_status = 0,
                program_id = data.ProgramId,
                site_code = _siteConfigRepository.GetSiteCode()
            };

            if (data.Products == null || data.Products.Count == 0)
                return Result<Empty>.Error("Se necesita agregar por lo menos un producto");

            var details = data.Products
                .Select(e => new INTERNAL_REQUIREMENT_DETAIL
                {
                    supplier_part_number_creditors = e.id_item_supplier,
                    quantity = e.quantity,
                    estimated_price = e.estimate_price,
                    cuser = data.Username,
                    cdate = DateTime.Now,
                    program_id = data.ProgramId
                }).ToList();

            if (details.Any(e => e.estimated_price <= 0 || e.quantity <= 0))
                return Result<Empty>.Error("Todos los productos deben tener un precio estimado y al menos un producto");

            var estimate_price = data.Products.Select(e => e.estimate_price * e.quantity).Sum();

            var processName = "gastos_simple";

            if (estimate_price >= 10000 || data.ToNewProjects)
                processName = "gastos_proyectos";
            else if (estimate_price >= 5000)
                processName = "gastos_distrital";

            var emp = _userMasterRepository.GetUserMasterByUsername(data.Username);

            if (emp == null)
                return Result.Error("No se cuenta con un usuario valido para realizar esta acción");

            if (!_approvalsRepository.CkPermissions(emp.emp_no, processName, 1, new string[] { }))
                return Result.Error("No se cuenta con los permisos necesarios para realizar esta acción");

            var result = _ExpenseRequisitionsRepository.ExpenseStart(model, details, processName);
            if (!result)
                return Result.Error();

            var r = _approvalBusiness.StartProcess(processName, emp.emp_no, data.ProgramId);
            if (!r.Ok)
                return Result.Error(r.Msg);

            var approved = _approvalsRepository.ProcessIsApproved(r.Data.approval_id);
            var b = _ExpenseRequisitionsRepository.SetApprovalInfo(model.folio, r.Data.approval_id, r.Data.step_level, approved ? 6 : 5);
            return b ? Result.OK() : Result.Error();
        }

        public Result<Empty> ExpenseUpdate(ExpenseUpdateReq data)
        {
            var route = _approvalsRepository.GetApprovalRoute(data.process_name, data.step_level);
            var emp = _userMasterRepository.GetUserMasterByUsername(data.user_name);

            if (emp == null)
                return Result.Error("No se cuenta con un usuario valido para realizar esta acción");

            if (!_approvalsRepository.CkPermissions(emp.emp_no, data.process_name, data.step_level, new[] { data.process_group, "all" })) // este valor hay que cambiarlo
                return Result.Error("No se cuenta con los permisos necesarios para realizar esta acción");

            var model = new APPROVAL_HISTORY
            {
                step_level = data.step_level,
                approval_id = data.approval_id,
                process_name = data.process_name,
                program_id = data.program_id,
                approval_datetime = DateTime.Now,
                approval_status = data.new_approval_status,
                emp_no = emp.emp_no,
                remark = data.new_remark,
                approval_type = route.approval_type,
                process_group = data.process_group
            };

            var r = _approvalBusiness.UpdateProcess(model);
            if (!r.Ok)
                return Result.Error();

            var header = _ExpenseRequisitionsRepository.GetByApprovalId(data.approval_id);
            if (header == null)
                return Result.Error();

            if (_approvalsRepository.ProcessIsRejected(data.approval_id))
            {
                var b = _ExpenseRequisitionsRepository.SetApprovalInfo(header.folio, r.Data.approval_id, r.Data.step_level, 7);
                return b ? Result.OK() : Result.Error();
            }
            else
            {
                var approved = _approvalsRepository.ProcessIsApproved(data.approval_id);

                var b = _ExpenseRequisitionsRepository.SetApprovalInfo(header.folio, r.Data.approval_id, r.Data.step_level, approved ? 6 : 5);
                return b ? Result.OK() : Result.Error();
            }
        }

        public Result<Empty> ActionAddXml(XmlReq data)
        {
            var sitecode = _siteConfigBusiness.GetSiteCode();
            var expense = new EXPENSE
            {
                authorization_code = data.history.approval_id,
                site_code = sitecode,
                remark = "",
                reference_type = "gastos",
                cdate = DateTime.Now,
                cuser = data.user_name,
                program_id = data.program_id
            };
            var xml_header_list = new List<XML_HEADER>();
            foreach (var file in data.files)
            {
                try
                {
                    var xml = Encoding.UTF8.GetString(Convert.FromBase64String(file.xml));
                    var doc = XDocument.Parse(xml);
                    var root = doc.Root;
                    var space = root?.Name.NamespaceName;
                    var rfc = (string)root?.Element(XName.Get("Emisor", space))?.Attribute("Rfc");
                    var sup = _maSupplierRepository.GetSupplierByRFC(rfc);
                    if (!sup.Any())
                    {
                        var name = (string)root?.Element(XName.Get("Emisor", space))?.Attribute("Nombre");
                        return Result<Empty>.Error($"El proveedor {name} no está registrado como acreedor");
                    }
                    int flag = 0;
                    var index = 0;
                    for (var i = 0; i < sup.Count; i++)
                    {
                        index = i;
                        if (sup[i].supplier_id == 0) return Result<Empty>.Error("No esta dado de alta el provedor del XML");
                        if (sup[i].supplier_type != "Acreedor")
                            flag++;
                    }
                    if (flag == sup.Count)
                        return Result<Empty>.Error("El proveedor " + (sup.Any() ? sup[index].commercial_name : "") + " no está como acreedor");

                    var rfcSpace = XNamespace.Get("http://www.sat.gob.mx/TimbreFiscalDigital").NamespaceName;
                    var uuid = root?
                        .Descendants(XName.Get("TimbreFiscalDigital", rfcSpace)).FirstOrDefault()?
                        .Attribute("UUID");
                    var currency = (string)root?.Attribute("Moneda") ?? "";
                    var iepsTotal = 0M;
                    var ivaTotal = 0M;
                    var isr_retainedTotal = 0M;
                    var iva_retainedTotal = 0M;
                    try
                    {
                        iepsTotal = (decimal?)root.Element(XName.Get("Impuestos", space)).Element(XName.Get("Traslados", space)).Descendants(XName.Get("Traslado", space)).Where(a => (int)a.Attribute("Impuesto") == 3).Select(e => (decimal?)e.Attribute("Importe") ?? 0).FirstOrDefault() ?? 0;
                    }
                    catch (Exception ex)
                    {
                        string msg = ex.Message;
                        iepsTotal = 0;
                    }
                    try
                    {
                        isr_retainedTotal = (decimal?)root.Element(XName.Get("Impuestos", space)).Element(XName.Get("Retenciones", space)).Descendants(XName.Get("Retencion", space)).Where(a => (int)a.Attribute("Impuesto") == 1).Select(e => (decimal?)e.Attribute("Importe") ?? 0).FirstOrDefault() ?? 0;
                    }
                    catch (Exception ex)
                    {
                        string msg = ex.Message;
                        isr_retainedTotal = 0;
                    }
                    try
                    {
                        iva_retainedTotal = (decimal?)root.Element(XName.Get("Impuestos", space)).Element(XName.Get("Retenciones", space)).Descendants(XName.Get("Retencion", space)).Where(a => (int)a.Attribute("Impuesto") == 2).Select(e => (decimal?)e.Attribute("Importe") ?? 0).FirstOrDefault() ?? 0;
                    }
                    catch (Exception ex)
                    {
                        string msg = ex.Message;
                        iva_retainedTotal = 0;
                    }
                    try
                    {
                        ivaTotal = (decimal?)root.Element(XName.Get("Impuestos", space)).Element(XName.Get("Traslados", space)).Descendants(XName.Get("Traslado", space)).Where(a => (int)a.Attribute("Impuesto") == 2).Select(e => (decimal?)e.Attribute("Importe") ?? 0).FirstOrDefault() ?? 0;
                    }
                    catch (Exception ex)
                    {
                        string msg = ex.Message;
                        ivaTotal = 0;
                    }
                    var xml_header = new XML_HEADER
                    {
                        uuid_invoice = (string)uuid,
                        invoice_no = (string)root?.Attribute("Folio"),
                        invoice_date = (DateTime?)root?.Attribute("Fecha"),
                        supplier_id = sup[index].supplier_id,
                        currency = currency,
                        iva = ivaTotal,
                        ieps = iepsTotal,
                        isr_retained = isr_retainedTotal,
                        iva_retained = iva_retainedTotal,
                        subtotal = (decimal?)root?.Attribute("SubTotal") ?? 0,
                        discount = (decimal?)root?.Attribute("Descuento") ?? 0,
                        total_amount = (decimal?)root?.Attribute("Total") ?? 0,
                        xml = file.xml,
                        xml_name = file.xml_name,
                        cdate = DateTime.Now,
                        cuser = data.user_name,
                        program_id = data.program_id,
                        invoice_type = (string)root?.Attribute("TipoDeComprobante"),
                        exchange_currency = (decimal?)root?.Attribute("TipoCambio"),
                        payment_method = (string)root?.Attribute("MetodoPago"),
                        payment_way = (string)root?.Attribute("FormaPago"),
                        version = (string)root?.Attribute("Version")
                    };
                    xml_header.XML_DETAIL = root?.Descendants(XName.Get("Concepto", space)).Select(e =>
                    {
                        var iva_amount = (decimal?)e.Descendants(XName.Get("Traslado", space)).Where(a => (int)a.Attribute("Impuesto") == 2).Select(a => a.Attribute("Importe")).FirstOrDefault() ?? 0;
                        var ieps_amount = (decimal?)e.Descendants(XName.Get("Traslado", space)).Where(a => (int)a.Attribute("Impuesto") == 3).Select(a => a.Attribute("Importe")).FirstOrDefault() ?? 0;
                        var isr_retained = (decimal?)e.Descendants(XName.Get("Retencion", space)).Where(a => (int)a.Attribute("Impuesto") == 1).Select(a => a.Attribute("Importe")).FirstOrDefault() ?? 0;
                        var iva_retained = (decimal?)e.Descendants(XName.Get("Retencion", space)).Where(a => (int)a.Attribute("Impuesto") == 2).Select(a => a.Attribute("Importe")).FirstOrDefault() ?? 0;
                        return new XML_DETAIL
                        {
                            iva_amount = iva_amount,
                            ieps_amount = ieps_amount,
                            isr_retained = isr_retained,
                            iva_retained = iva_retained,
                            item_no = (string)e.Attribute("NoIdentificacion") ?? "",
                            item_description = (string)e.Attribute("Descripcion") ?? "",
                            quantity = (decimal?)e.Attribute("Cantidad") ?? 0,
                            unit_cost = (decimal?)e.Attribute("ValorUnitario") ?? 0,
                            item_amount = (decimal?)e.Attribute("Importe") ?? 0,
                            discount = (decimal?)e.Attribute("Descuento") ?? 0,
                        };
                    }).ToList();
                    xml_header_list.Add(xml_header);
                }
                catch (Exception ex)
                {
                    string msg = ex.Message;
                    return Result.Error($"Error leer datos de xml: \n{file.xml_name}");
                }
            }

            var b = _expensesRepository.CreateExpense(expense, xml_header_list);
            if (b == null)
                return Result.Error("Error al guardar datos"); //error 

            var b2 = _ExpenseRequisitionsRepository.SetExpense(data.history.folio, b.expense_id);
            if (b2 == null)
                return Result.Error("Error al guardar datos"); //error 

            _ExpenseRequisitionsRepository.SetApprovalInfo(data.history.folio, data.history.approval_id, data.history.step_level, 9);

            return Result.OK();
        }

        public List<InternalRequirementModel> GetDetailsByFolio(int id) => _ExpenseRequisitionsRepository.GetDetailsByFolio(id);

        public Result<Empty> ConfirmCancel(int folio) => Result<Empty>.Check(_ExpenseRequisitionsRepository.ConfirmCancel(folio));
    }

    public class XMLFile
    {
        public string xml { get; set; }
        public string xml_name { get; set; }
    }

    public class XmlReq
    {
        public List<XMLFile> files { get; set; }
        public string user_name { get; set; }
        public string program_id { get; set; }
        public ExpenseRequirementModel history { get; set; }
    }
}