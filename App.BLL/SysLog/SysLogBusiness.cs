﻿using App.DAL.SysLog;
using App.Entities;
using System;
using static App.Entities.ViewModels.Common.Constants;

namespace App.BLL.SysLog
{
    public class SysLogBusiness
    {
        private SysLogRepository _SysLogRepository;

        public SysLogBusiness()
        {
            _SysLogRepository = new SysLogRepository();
        }

        public bool CreateLogStampDocuments(object Model, string ErrorMessage, string ErrorNumber, bool IsCancel, bool IsReStamped)
        {
            var ItemLog = new SYS_LOG();
            var Type = Model.GetType().GetProperty("DocumentType").GetValue(Model, null).ToString();
            var date = DateTime.Now;
            string word = IsCancel ? "cancelar" : (!IsReStamped ? "crear" : "timbrar");
            string menu = !IsReStamped ? "VENTAS" : "CLIENTES";
            switch (Type)
            {
                case VoucherType.INCOMES:
                    {
                        ItemLog.Action = string.Format("{0} FACTURA", word.ToUpper());
                        ItemLog.Description = string.Format("Error al {0} factura: Error {1} - {2}", word, ErrorNumber, ErrorMessage);
                        ItemLog.TableName = "INVOICES";
                        ItemLog.Menu = menu;
                    }
                    break;

                case VoucherType.EXPENSES:
                    {
                        ItemLog.Action = string.Format("{0} BONIFICACIÓN - NOTA DE CRÉDITO", word.ToUpper());
                        ItemLog.Description = string.Format("Error al {0} documento: Error {1} - {2}", word, ErrorNumber, ErrorMessage);
                        ItemLog.TableName = "CREDIT_NOTES";
                        ItemLog.Menu = menu;
                    }
                    break;

                case VoucherType.PAYMENTS:
                    {
                        ItemLog.Action = string.Format("{0} PAGO", word.ToUpper());
                        ItemLog.Description = string.Format("Error al {0} pago: Error {1} - {2}", word, ErrorNumber, ErrorMessage);
                        ItemLog.TableName = "PAYMENTS";
                        ItemLog.Menu = menu;
                    }
                    break;
            }
                        
            ItemLog.Date = DateTime.Now.Date;
            ItemLog.time = date.ToLongTimeString();
            ItemLog.idAfect = long.Parse(Model.GetType().GetProperty("Number").GetValue(Model, null).ToString());
            ItemLog.uuser = Model.GetType().GetProperty("User").GetValue(Model, null).ToString();

            return _SysLogRepository.CreateLogStampDocuments(ItemLog);
        }

        public bool CreateLogRecord(SYS_LOG Item) => _SysLogRepository.CreateLogStampDocuments(Item);
    }
}
