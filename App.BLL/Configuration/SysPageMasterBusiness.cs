﻿using App.DAL;
using App.Entities;
using App.Entities.ViewModels.Android;
using System;
using System.Collections.Generic;
using System.Data;

namespace App.BLL.Configuration
{
    public class SysPageMasterBusiness
    {
        private readonly SysPageMasterRepository _PageMasterRepo;

        public SysPageMasterBusiness()
        {
            _PageMasterRepo = new SysPageMasterRepository();
        }

        public List<SYS_PAGE_MASTER> GetAllPagesAdmin() => _PageMasterRepo.GetAllPagesAdmin();

        public SYS_PAGE_MASTER GetPageByID(string page_id) => _PageMasterRepo.GetPageByID(page_id);

        public bool AddPagesMaster(SYS_PAGE_MASTER Page)
        {
            var check = false;
            var flag = _PageMasterRepo.AddPagesMaster(Page);

            if (flag > 0)
                check = true;

            return check;
        }

        public SYS_PAGE_MASTER SearchPage(string id_page) => _PageMasterRepo.SearchPage(id_page);

        public bool UpdatePagesMaster(SYS_PAGE_MASTER Page)
        {
            var check = false;
            if (_PageMasterRepo.UpdatePagesMaster(Page) > 0)
                check = true;

            return check;
        }

        public ICollection<SYS_PAGE_MASTER> GetAllPagesOfRole(string rol) => _PageMasterRepo.GetAllPagesOfRole(rol);

        public ICollection<SYS_PAGE_MASTER> GetAllPagesAvailable(string rol) => _PageMasterRepo.GetAllPagesAvailable(rol);

        public List<MenuModel> GetAllLevel1PagesByUserAndroid(string employeeNumber) => _PageMasterRepo.GetAllLevel1PagesByUserAndroid(employeeNumber);

        public List<MenuModel> GetAllLevel2PagesByUserAndLevel1Android(string employeeNumber, string level1) => _PageMasterRepo.GetAllLevel2PagesByUserAndLevel1Android(employeeNumber, level1);

        public Tuple<string, string> GetMenuAndroidByEmployeeNumber(string employeeNumber)
        {
            var level1 = _PageMasterRepo.GetAllLevel1PagesByUserAndroid(employeeNumber);
            if (level1 != null)
            {
                string nivel_1 = "";
                string nivel_2 = "";
                int l = level1.Count - 1;
                for (int i = 0; i <= l; i++)
                {
                    nivel_1 = nivel_1 + level1[i].menu_sequence.ToString() + ",";
                    var level2 = _PageMasterRepo.GetAllLevel2PagesByUserAndLevel1Android(employeeNumber, level1[i].level_1_menu);
                    if (level2 != null)
                    {
                        for (int o = 0; o <= level2.Count - 1; o++)
                            nivel_2 = nivel_2 + level2[o].menu_sequence.ToString() + ",";

                        nivel_2 = nivel_2 + "9" + ",";
                    }
                }

                nivel_1 = nivel_1.Remove(nivel_1.Length - 1);
                nivel_2 = nivel_2.Remove(nivel_2.Length - 1);
                return Tuple.Create(nivel_1, nivel_2);
            }
            else
                return Tuple.Create("", "");
        }

        public DataSet GetAllPagesOfUser(string rol)
        {
            DataSet table = new DataSet();
            DataTable dt = new DataTable();
            try
            {
                var items = _PageMasterRepo.GetAllPagesOfUser(rol);
                dt.Columns.Add("page_id", typeof(string));
                dt.Columns.Add("page_name", typeof(string));
                dt.Columns.Add("description", typeof(string));
                dt.Columns.Add("url", typeof(string));
                dt.Columns.Add("active_flag", typeof(bool));
                dt.Columns.Add("level_1_menu", typeof(string));
                dt.Columns.Add("level_2_menu", typeof(string));
                dt.Columns.Add("level_3_menu", typeof(string));
                dt.Columns.Add("type", typeof(string));
                DataRow row = null;
                foreach (var item in items)
                {
                    row = dt.NewRow();
                    row["page_id"] = item.page_id;
                    row["page_name"] = item.page_name;
                    row["description"] = item.description;
                    row["url"] = item.url;
                    row["active_flag"] = item.active_flag;
                    row["level_1_menu"] = item.level_1_menu;
                    row["level_2_menu"] = item.level_2_menu;
                    row["level_3_menu"] = item.level_3_menu;
                    row["type"] = item.type;
                    dt.Rows.Add(row);
                }
                table.Tables.Add(dt);
            }
            catch (Exception e)
            {
                string g = e.Message;
                return table;
            }
            return table;
        }
    }
}