﻿using App.DAL;
using App.Entities;
using App.Entities.ViewModels.User;
using System.Collections.Generic;

namespace App.BLL.Configuration
{
    public class SysRoleMasterBusiness
    {
        private readonly SysRoleMasterRepository _RoleMasterRepo;

        public SysRoleMasterBusiness()
        {
            _RoleMasterRepo = new SysRoleMasterRepository();
        }

        public ICollection<SYS_ROLE_MASTER> GetAllRolesAdmin() => _RoleMasterRepo.GetAllRolesAdmin();

        public SYS_ROLE_MASTER GetRoleByID(string rol) => _RoleMasterRepo.getRoleByID(rol);

        public bool AddRolesMaster(SYS_ROLE_MASTER Rol)
        {
            var check = false;
            var flag = _RoleMasterRepo.AddRolesMaster(Rol);

            if (flag > 0)
                check = true;

            return check;
        }

        public SYS_ROLE_MASTER SearchRole(string id_role) => _RoleMasterRepo.SearchRole(id_role);

        public bool UpdateRoleMaster(SYS_ROLE_MASTER rol)
        {
            var check = false;
            if (_RoleMasterRepo.updateRoleMaster(rol) > 0)
                check = true;

            return check;
        }

        public string GetEmailsByRol(string rol) => _RoleMasterRepo.GetEmailsByRol(rol);

        public List<UserModelFirst> GetEmailsInfoByRol(string rol) => _RoleMasterRepo.GetEmailsInfoByRol(rol);
    }
}