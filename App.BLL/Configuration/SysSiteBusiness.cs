﻿using App.DAL.Configuration;
using System.Collections.Generic;

namespace App.BLL.Configuration
{
    public class SysSiteBusiness
    {
        private readonly SysSiteRepository _SiteRepo;

        public SysSiteBusiness()
        {
            _SiteRepo = new SysSiteRepository();
        }

        public List<SiteViewModel> GetAllSite() => _SiteRepo.GetAllSites();

        public SiteViewModel GetSite() => _SiteRepo.GetSite();
    }
}