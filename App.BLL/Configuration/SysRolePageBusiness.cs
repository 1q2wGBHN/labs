﻿using App.DAL;

namespace App.BLL.Configuration
{
    public class SysRolePageBusiness
    {
        private readonly SysRolePageRepository _RolePageRepo;
        
        public SysRolePageBusiness()
        {
            _RolePageRepo = new SysRolePageRepository();
        }

        public bool AddPagesToRoles(string rol, string pages,string user)
        {
            var check = false;
            var flag = _RolePageRepo.AddPagesToRoles(rol, pages,user);

            if (flag > 0)
                check = true;

            return check;
        }

        public bool RemovePagesFromRoles(string rol, string pages)
        {
            var check = false;
            var flag = _RolePageRepo.RemovePagesFromRoles(rol, pages);

            if (flag > 0)
                check = true;

            return check;
        }
    }
}