﻿using App.DAL;
using App.Entities;
using App.Entities.ViewModels.Configuration;
using App.Entities.ViewModels.SysInfo;
using System.Collections.Generic;

namespace App.BLL.Configuration
{
    public class SysRoleUserBusiness
    {
        private readonly SysRoleUserRepository _RoleUserRepo;

        public SysRoleUserBusiness()
        {
            _RoleUserRepo = new SysRoleUserRepository();
        }

        public ICollection<SYS_ROLE_MASTER> GetAllRolesOfUser(string EmployeeNumber) => _RoleUserRepo.GetAllRolesOfUser(EmployeeNumber);

        public ICollection<SYS_ROLE_MASTER> GetAllRolesAvailable(string EmployeeNumber) => _RoleUserRepo.GetAllRolesAvailable(EmployeeNumber);

        public bool AddRolesToUser(string EmployeeNumber, string Roles, string user)
        {
            var check = false;
            var flag = _RoleUserRepo.AddRolesToUser(EmployeeNumber, Roles, user);

            if (flag > 0)
                check = true;

            return check;
        }

        public bool RemoveRolesFromUser(string EmployeeNumber, string Roles)
        {
            var check = false;
            var flag = _RoleUserRepo.RemoveRolesFromUser(EmployeeNumber, Roles);

            if (flag > 0)
                check = true;

            return check;
        }

        public List<SysPageMaster> GetPagesByUser(string emp_no) => _RoleUserRepo.GetPagesByUser(emp_no);

        public List<UserRoleModel> GetRolByEmpNo(string emp_no) => _RoleUserRepo.GetRolByEmpNo(emp_no);
    }
}