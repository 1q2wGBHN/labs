﻿using App.DAL.Configuration;
using App.Entities.ViewModels.Site;

namespace App.BLL.Configuration
{
    public class SysSiteConfigBusiness
    {
        private readonly SysSiteConfigRepository _SiteRepo;

        public SysSiteConfigBusiness()
        {
            _SiteRepo = new SysSiteConfigRepository();
        }

        public SiteViewModel GetSite() => _SiteRepo.GetSite();

        public SiteModel GetSiteModel() => _SiteRepo.GetSiteModel();
    }
}