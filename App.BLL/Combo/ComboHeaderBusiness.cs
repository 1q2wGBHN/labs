﻿using App.DAL.Combos;
using App.Entities.ViewModels.Combo;
using System.Collections.Generic;

namespace App.BLL.Combo
{
    public class ComboHeaderBusiness
    {
        private readonly ComboHeaderRepository _ComboHeaderRepository;

        public ComboHeaderBusiness()
        {
            _ComboHeaderRepository = new ComboHeaderRepository();
        }

        public List<ComboHeaderModel> GetComboHeder() => _ComboHeaderRepository.GetComboHeder();
    }
}