﻿using App.DAL.Combos;
using App.Entities.ViewModels.Combo;
using System.Collections.Generic;

namespace App.BLL.Combo
{
    public class ComboDetailBusiness
    {
        private readonly ComboDetailRepository _ComboDetailRepository;

        public ComboDetailBusiness()
        {
            _ComboDetailRepository = new ComboDetailRepository();
        }

        public List<ComboDetailModel> GetComboDetail(string PartNumber) => _ComboDetailRepository.GetComboDetail(PartNumber);
    }
}