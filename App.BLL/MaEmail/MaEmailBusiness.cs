﻿using App.DAL.MaEmail;
using App.Entities.ViewModels.MaEmail;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App.BLL.MaEmail
{
    public class MaEmailBusiness
    {
        private readonly MaEmailRepository _maEmailRepository;

        public MaEmailBusiness()
        {
            _maEmailRepository = new MaEmailRepository();
        }

        public MaEmailModel GetEmailsSiteCode(string site) => _maEmailRepository.GetEmailsSiteCode(site);
    }
}