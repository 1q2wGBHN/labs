﻿using App.DAL.ConfigPosSales;
using App.Entities;
using App.Entities.ViewModels.ConfigPosSales;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App.BLL.ConfigPosSales
{
    public class ConfigPosSalesBusiness
    {
        private ConfigPosSalesRepository _ConfigPosSalesRepository;
        
        public ConfigPosSalesBusiness()
        {
            _ConfigPosSalesRepository = new ConfigPosSalesRepository();
        }

        public bool CreateConfigPosSale(PosSale Model)
        {
            var ITEM = new DFLPOS_CONFIG();

            ITEM.config_pos_id = Model.PosId;
            ITEM.config_key = Model.Key;
            ITEM.config_value = Model.Value;
            ITEM.config_section = Model.Section;

            return _ConfigPosSalesRepository.CreateConfigPosSale(ITEM);
        }

        public List<PosSale> GetConfigPosSalesList()
        {
            return _ConfigPosSalesRepository.GetConfigPosSalesList();
        }

        public PosSale GetConfigPosSaleById(int ConfigPosSaleId)
        {
            return _ConfigPosSalesRepository.GetConfigPosSaleById(ConfigPosSaleId);
        }

        public bool UpdateConfig(PosSale Model)
        {
            var ITEM = new DFLPOS_CONFIG();

            ITEM.config_id = Model.Id;
            ITEM.config_pos_id = Model.PosId;
            ITEM.config_key = Model.Key;
            ITEM.config_value = Model.Value;
            ITEM.config_section = Model.Section;

            return _ConfigPosSalesRepository.UpdateConfig(ITEM);
        }
    }
}
