﻿using App.DAL.CodesPackages;
using App.Entities.ViewModels.CodesPackages;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App.BLL.CodesPackages
{
    public class CodesPackagesBusiness
    {
        private readonly CodesPackageRepository _CodesPackageRepository;

        public CodesPackagesBusiness()
        {
            _CodesPackageRepository = new CodesPackageRepository();
        }

        public List<CodesPackagesModel> GetCodesPackages() => _CodesPackageRepository.GetCodesPackages();

        public List<CodesPackagesModel> GetCodePackagesByOriginPartNumber(string OriginPartNumber) => _CodesPackageRepository.GetCodePackagesByOriginPartNumber(OriginPartNumber);
    }
}