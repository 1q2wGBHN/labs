﻿using App.DAL.WithdrawalCash;
using App.Entities.ViewModels.Withdrawal;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App.BLL.WithdrawalCash
{
    public class WithdrawalCashDetailsBusiness
    {
        private WithdrawalCashDetailsRepository _WithdrawalCashDetailsRepository;

        public WithdrawalCashDetailsBusiness()
        {
            _WithdrawalCashDetailsRepository = new WithdrawalCashDetailsRepository();
        }

        public List<WithdrawalDetails> WithDrawalDetails(string Date, string Currency, string EmployeeNo)
        {
            return _WithdrawalCashDetailsRepository.WithDrawalDetails(Date, Currency, EmployeeNo);
        }

        public List<DollarsConciliation> DollarsConciliation(string DateValue)
        {
            return _WithdrawalCashDetailsRepository.DollarsConciliation(DateValue);
        }
    }
}
