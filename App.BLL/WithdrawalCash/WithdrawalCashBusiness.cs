﻿using App.DAL.WithdrawalCash;
using App.Entities.ViewModels.Withdrawal;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App.BLL.WithdrawalCash
{
    public class WithdrawalCashBusiness
    {
        private WithdrawalCashRepository _WithdrawalCashRepository;

        public WithdrawalCashBusiness()
        {
            _WithdrawalCashRepository = new WithdrawalCashRepository();
        }

        public List<WithdrawalHeader> GetReportData(string Date, string CashierNumber)
        {
            return _WithdrawalCashRepository.GetReportData(Date, CashierNumber);
        }

        public decimal GetTotalAmount(string Date, string CashierNumber)
        {
            return _WithdrawalCashRepository.GetTotalAmount(Date, CashierNumber);
        }

        public List<WithdrawalHeader> GetAcumulateData(string Date, string CashierNumber)
        {
            return _WithdrawalCashRepository.GetAcumulateData(Date, CashierNumber);
        }
    }
}
