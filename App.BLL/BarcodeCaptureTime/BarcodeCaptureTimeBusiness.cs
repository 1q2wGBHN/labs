﻿using App.DAL.BarcodeCaptureTime;
using App.Entities.ViewModels.BarcodeCaptureTime;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App.BLL.BarCodeCaptureTime
{
    public class BarcodeCaptureTimeBusiness
    {
        private BarcodeCaptureTimeRepository _BarcodeCaptureTimeRepository;

        public BarcodeCaptureTimeBusiness()
        {
            _BarcodeCaptureTimeRepository = new BarcodeCaptureTimeRepository();
        }

        public BarcodeCaptureTimeInfo GetDataReport(BarcodeCaptureTimeIndex Model)
        {
            return _BarcodeCaptureTimeRepository.GetDataReport(Model);
        }
    }
}
