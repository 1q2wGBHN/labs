﻿using App.BLL.Sales;
using App.BLL.TransactionsCustomers;
using App.DAL.CreditNotes;
using App.DAL.Invoice;
using App.DAL.Item;
using App.DAL.Site;
using App.Entities;
using App.Entities.ViewModels.Common;
using App.Entities.ViewModels.CreditNotes;
using System;
using System.Globalization;
using System.Linq;
using static App.Entities.ViewModels.Common.Constants;

namespace App.BLL.CreditNotes
{
    public class CreditNotesBusiness
    {
        private CreditNotesRepository _CreditNotesRepository;        
        private InvoiceRepository _InvoiceRepository;
        private SiteConfigRepository _SiteConfigRepository;
        private TransactionsCustomersBusiness _TransactionsCustomersBusiness;
        private CreditNotesDetailRepository _CreditNotesDetailRepository;
        private CreditNotesDetailTaxRepository _CreditNotesDetailTaxRepository;
        private SalesBusiness _SalesBusiness;

        public CreditNotesBusiness()
        {
            _CreditNotesRepository = new CreditNotesRepository();            
            _InvoiceRepository = new InvoiceRepository();
            _SiteConfigRepository = new SiteConfigRepository();
            _TransactionsCustomersBusiness = new TransactionsCustomersBusiness();
            _CreditNotesDetailRepository = new CreditNotesDetailRepository();
            _CreditNotesDetailTaxRepository = new CreditNotesDetailTaxRepository();
            _SalesBusiness = new SalesBusiness();
        }

        public bool CreateCreditNote(CreditNote Model)
        {
            try
            {
                CREDIT_NOTES Item = new CREDIT_NOTES();
                decimal Total = 0;
                decimal Tax = 0;
                if (Model.IsSaleInvoiced)
                {
                    var Invoice = _InvoiceRepository.GetInvoiceByNumber(Model.InvoiceDetails.InvoiceNumber);
                    Item.invoice_no = Invoice.invoice_no;
                    Item.invoice_serie = Invoice.invoice_serie;
                    Item.customer_code = Invoice.customer_code;
                    Item.cur_code = Invoice.cur_code;
                    Item.cur_rate = Invoice.invoice_cur_rate;
                    Item.sat_us_code = Invoice.sat_us_code;
                    Item.sat_pm_code = Invoice.sat_pm_code;
                }
                else
                {
                    //var Sale = _SalesBusiness.GetSaleByNumber(Model.InvoiceDetails.InvoiceNumber);
                                        
                    Item.sale_id = Model.InvoiceDetails.InvoiceNumber;
                    Item.customer_code = Model.InvoiceDetails.ClientNumber;
                    Item.cur_code = Model.InvoiceDetails.Currency;
                    Item.cur_rate = Model.InvoiceDetails.CurrencyRate;//Sale.DFLPOS_SALES_DETAIL.FirstOrDefault().currency_rate;
                    Item.sat_us_code = SatUseCode.TO_DEFINE;
                    Item.sat_pm_code = Model.InvoiceDetails.SatPMCode;
                }


                Item.cn_id = Model.Number;
                Item.cn_serie = Model.Serie;
                Item.cn_date = DateTime.ParseExact(Model.IssuedDate, DateFormat.INT_DATE, CultureInfo.InvariantCulture);
                Item.cfdi_id = Model.Number;
                Item.cn_cause = Model.Cause;
                Item.cn_comments = Model.Comments;
                Item.site_code = _SiteConfigRepository.GetSiteCode();
                Item.sat_reltion_code = "01"; //TODO: check from where came this info.            
                Item.cn_status = true;
                Item.cdate = DateTime.Now;
                Item.cuser = Model.User;
                Item.program_id = "CRNO002.chstml";
                Item.is_employee_bon = Model.IsEmployeeBon;

                if (Model.IsBonification)
                {
                    Tax += Model.BonificationTaxes.Where(c => c.TaxCode.Contains(TaxTypes.TAX)).Sum(c => c.Bonification);
                    Total += Model.BonificationTaxes.Where(c => c.TaxCode.Contains(TaxTypes.TAX) || c.TaxCode == TaxCodes.SUB_TOTAL).Sum(c => c.Bonification);
                }

                Item.cn_total = Math.Round(Total, 2);
                Item.cn_tax = Tax;

                //Create record for transactions customers table
                if (Item.customer_code != GenericCustomer.CODE)
                    if (!_TransactionsCustomersBusiness.CreateTransactions(null, Item, null, Model.IsBonification))
                        return false;

                return _CreditNotesRepository.CreateCreditNote(Item);
            }
            catch(Exception ex)
            {
                string msg = ex.Message;
                return false;
            }
        }

        public decimal TotalBonificationInvoice(long InvoiceNumber) => _CreditNotesRepository.TotalBonificationInvoice(InvoiceNumber);

        public decimal TotalCreditNoteInvoice(long InvoiceNumber) => _CreditNotesRepository.TotalCreditNoteInvoice(InvoiceNumber);

        public decimal TotalBonificationSales(long SaleNumber) => _CreditNotesRepository.TotalBonificationSales(SaleNumber);        

        public CreditNoteDetails GetCreditNoteDetailsByNumber(long CreditNoteNumber) => _CreditNotesRepository.GetCreditNoteDetailsByNumber(CreditNoteNumber);        

        public bool IsDocumentActive(long Number) => _CreditNotesRepository.IsDocumentActive(Number);        

        public bool CreditNoteExist(long Number) => _CreditNotesRepository.CreditNoteExist(Number);        

        public bool UpdateCreditNote(CREDIT_NOTES Document)
        {
            Document.cn_status = false;

            return _CreditNotesRepository.UpdateCreditNote(Document);
        }

        public CREDIT_NOTES GetCreditNoteByNumber(long Number) => _CreditNotesRepository.GetCreditNoteByNumber(Number);              

        public SendDocumentModel GetSendDocumentByNumberCreditNoteBoni(long Number) => _CreditNotesRepository.GetSendDocumentByNumberCreditNoteBoni(Number);        

        public SendDocumentModel GetCreditBon(long Number) => _CreditNotesRepository.GetCreditBon(Number);        

        public bool CheckEmployeeBonificationByDate(string DateValue) => _CreditNotesRepository.CheckEmployeeBonificationByDate(DateValue);        

        public bool DeleteCreditNoteByNumber(long CreditNoteNumber, string CreditNoteSerie) => _CreditNotesRepository.DeleteCreditNoteByNumber(CreditNoteNumber, CreditNoteSerie);

        public bool DeleteCreditNoteByInvoice(long InvoiceNumber, string InvoiceSerie) => _CreditNotesRepository.DeleteCreditNoteByInvoice(InvoiceNumber, InvoiceSerie);

        public bool UpdateTotals(CreditNote Model)
        {
            var ITEM = _CreditNotesRepository.GetCreditNoteByNumber(Model.Number);
            var Products = _CreditNotesDetailRepository.GetCreditNoteProductsDetailsByNumber(Model.Number);
            var Taxs = _CreditNotesDetailTaxRepository.GetDetailTaxByNumber(Model.Number);
            if (Taxs.Where(c => c.tax_code.Contains(TaxTypes.TAX)).Any())
            {
                ITEM.cn_total = Products.Sum(c => c.TotalItemPrice) + Taxs.Where(x => x.tax_code.Contains(TaxTypes.TAX)).Sum(c => c.tax_amount);
                ITEM.cn_tax = Taxs.Where(x => x.tax_code.Contains(TaxTypes.TAX)).Sum(c => c.tax_amount);

                if (Model.IsBonification)
                    ITEM.cn_total += Taxs.Where(x => x.tax_code.Contains(TaxTypes.IEPS)).Sum(d => d.tax_amount);
            }
            else
            {
                ITEM.cn_total = Products.Sum(c => c.TotalItemPrice);
                ITEM.cn_tax = 0;
            }

            if (Model.IsSaleInvoiced)
            {
                //Update Invoice Balance when is credit Invoice.
                var Invoice = _InvoiceRepository.GetInvoiceByNumber(Model.InvoiceDetails.InvoiceNumber);
                if (Invoice.is_credit)
                {
                    Invoice.invoice_balance -= ITEM.cn_total;
                    if (!_InvoiceRepository.UpdateInvoice(Invoice, null))
                        return false;
                }
            }

            return _CreditNotesRepository.UpdateCreditNote(ITEM);
        }

        public bool UpdateStock(CreditNote Model, bool IsCancel) => _CreditNotesRepository.UpdateStock(Model, IsCancel);        

        public bool UpdateDocumentDate(CreditNote Item, string User)
        {
            var Document = _CreditNotesRepository.GetCreditNoteByNumber(Item.Number);
            Document.cn_date = DateTime.ParseExact(Item.NewIssuedDate, DateFormat.INT_DATE, CultureInfo.InvariantCulture);
            Document.uuser = User;
            Document.udate = DateTime.Now.Date;
            return _CreditNotesRepository.UpdateCreditNote(Document);
        }

        public bool SprocUpdateCreditNoteNumber() => _CreditNotesRepository.SprocUpdateCreditNoteNumber();

        public bool DocumentExist(long Number) => _CreditNotesRepository.DocumentExist(Number);
    }
}
