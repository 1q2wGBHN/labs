﻿using App.DAL.CreditNotes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App.BLL.CreditNotes
{
    public class CreditNotesSalesBusiness
    {
        private CreditNotesSalesRepository _CreditNotesSalesRepository;

        public CreditNotesSalesBusiness()
        {
            _CreditNotesSalesRepository = new CreditNotesSalesRepository();
        }

        public bool DeleteCreditNoteSalesDetails(long SaleNumber) => _CreditNotesSalesRepository.DeleteCreditNoteSalesDetails(SaleNumber);        
    }
}
