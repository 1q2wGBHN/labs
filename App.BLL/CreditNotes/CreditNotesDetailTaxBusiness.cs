﻿using App.DAL.CreditNotes;
using App.DAL.Invoice;
using App.DAL.Item;
using App.Entities;
using App.Entities.ViewModels.CreditNotes;
using App.Entities.ViewModels.Sales;
using System;
using System.Collections.Generic;
using System.Linq;
using static App.Entities.ViewModels.Common.Constants;

namespace App.BLL.CreditNotes
{
    public class CreditNotesDetailTaxBusiness
    {
        private CreditNotesDetailTaxRepository _CreditNotesDetailTaxRepository;
        private ItemRepository _ItemRepository;
        private CreditNotesDetailRepository _CreditNotesDetailRepository;
        private InvoiceDetailTaxRepository _InvoiceDetailTaxRepository;

        public CreditNotesDetailTaxBusiness()
        {
            _CreditNotesDetailTaxRepository = new CreditNotesDetailTaxRepository();
            _ItemRepository = new ItemRepository();
            _CreditNotesDetailRepository = new CreditNotesDetailRepository();
            _InvoiceDetailTaxRepository = new InvoiceDetailTaxRepository();
        }

        public bool CreateCreditNoteTax(CreditNote Model)
        {            
            CREDIT_NOTE_DETAIL_TAX Item = new CREDIT_NOTE_DETAIL_TAX();
            bool SaveFlag = true;
            try
            {
                if (!Model.IsBonification)
                {
                    foreach (var Element in Model.InvoiceDetails.Products)
                    {
                        if (Model.SelectAll && Model.IsSaleInvoiced && !Model.IsSaleFromGlobal)
                        {
                            Item.detail_id = _CreditNotesDetailRepository.GetCreditNoteDetailId(Element.ItemNumber, Model.Serie, Model.Number, null, Element.DetailId, Element.SalePartNumber);
                            var TaxesInfo = _InvoiceDetailTaxRepository.GetTaxesInfoByInvoiceDetailId(Element.DetailId);
                            foreach(var tax in TaxesInfo)
                            {
                                Item.tax_amount = tax.TaxAmount;
                                Item.tax_code = tax.TaxCode;

                                SaveFlag = _CreditNotesDetailTaxRepository.CreateCreditNoteTax(Item);

                                if (!SaveFlag)
                                    break;
                            }
                        }
                        else
                        {
                            if (Model.IsSaleFromGlobal || !Model.IsSaleInvoiced)
                                Item.detail_id = _CreditNotesDetailRepository.GetCreditNoteDetailId(Element.ItemNumber, Model.Serie, Model.Number, Element.DetailId, null, Element.SalePartNumber);
                            else
                                Item.detail_id = _CreditNotesDetailRepository.GetCreditNoteDetailId(Element.ItemNumber, Model.Serie, Model.Number, null, Element.DetailId, Element.SalePartNumber);

                            var ProductTax = _ItemRepository.GetItemTaxInfo(Element.ItemNumber);

                            if (ProductTax != null)
                            {
                                if (!string.IsNullOrEmpty(ProductTax.IVASale) && ProductTax.IVASale != TaxCodes.NOT_AVAILABLE)
                                {
                                    Item.tax_code = ProductTax.IVASale;
                                    Item.tax_amount = Math.Round(((Element.ItemPriceNoTax * Element.QuantityReturn) * ProductTax.IVASaleValue) / 100, 2);

                                    SaveFlag = _CreditNotesDetailTaxRepository.CreateCreditNoteTax(Item);

                                    if (!SaveFlag)
                                        break;
                                }

                                if (!string.IsNullOrEmpty(ProductTax.IEPS) && ProductTax.IEPS != TaxCodes.NOT_AVAILABLE)
                                {
                                    decimal IepsValue = 1 + (Element.IEPSValue / 100);
                                    decimal ItemPriceNoIeps = (Element.ItemPriceNoTax / IepsValue) * Element.QuantityReturn;
                                    Item.tax_code = ProductTax.IEPS;
                                    Item.tax_amount = Math.Round((ItemPriceNoIeps * ProductTax.IEPSValue) / 100, 2);

                                    SaveFlag = _CreditNotesDetailTaxRepository.CreateCreditNoteTax(Item);

                                    if (!SaveFlag)
                                        break;
                                }
                            }
                        }
                    }
                }
                else
                {
                    foreach (var Element in Model.BonificationTaxes.Where(c => c.TaxCode != TaxCodes.SUB_TOTAL || c.TaxName != TaxTypes.SUBTOTAL))
                    {
                        Item.detail_id = _CreditNotesDetailRepository.GetCreditNoteDetailId(GenericItem.ITEM_NUMBER, Model.Serie, Model.Number, null, null, string.Empty);
                        Item.tax_code = Element.TaxCode;
                        Item.tax_amount = Element.Bonification;

                        SaveFlag = _CreditNotesDetailTaxRepository.CreateCreditNoteTax(Item);
                    }
                }
                return SaveFlag;
            }
            catch (Exception ex)
            {
                string msg = ex.Message;
                return false;
            }
        }

        public bool DeleteDetailTaxByInvoice(long InvoiceNumber, string InvoiceSerie) => _CreditNotesDetailTaxRepository.DeleteDetailTaxByInvoices(InvoiceNumber, InvoiceSerie);        

        public bool DeleteCreditNoteDetailTax(long Number, string Serie) => _CreditNotesDetailTaxRepository.DeleteCreditNoteDetailTax(Number, Serie);        

        public List<CREDIT_NOTE_DETAIL_TAX> GetDetailTaxByNumber(long Number) => _CreditNotesDetailTaxRepository.GetDetailTaxByNumber(Number);        

        public List<SalesIEPS> GetDetailTaxsByDate(string Date1, string Date2) => _CreditNotesDetailTaxRepository.GetDetailTaxsByDate(Date1, Date2);        
    }
}
