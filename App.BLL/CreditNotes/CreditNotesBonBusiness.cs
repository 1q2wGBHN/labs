﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using App.DAL.Currency;
using App.DAL.CreditNotes;
using App.Entities.ViewModels.Currency;
using App.Entities.ViewModels.CreditNotes;
using App.Entities.ViewModels.Sales;


namespace App.BLL.CreditNotes
{
    public class CreditNotesBonBusiness
    {
        private CurrencyRepository _currencyRepository;
        private CreditNotesRepository _creditNotesBonRepository;
        public CreditNotesBonBusiness()
        {
            _currencyRepository = new CurrencyRepository();
            _creditNotesBonRepository = new CreditNotesRepository();
        }


        public List<CreditNotesBon> GetCreditNotesBonLis(CreditNotesBonIndex Model) => _creditNotesBonRepository.GetCreNotesBonPre(Model);
        
        public List<SalesDetailReportModel> GetCreditNotesBondeDetailList(long crediNoteId, decimal importeCn)
        {
            var CreditNotesBondeDetailList = _creditNotesBonRepository.GetCreditNotesBonDetailsByCrediNoteId(crediNoteId);
            List<SalesDetailReportModel> lisCreditNotesBondeDetailList = new List<SalesDetailReportModel>();
            SalesDetailReportModel model;
            foreach (SalesDetailReportModel sdrm in CreditNotesBondeDetailList)
            {
                decimal importe = 0;
                if (sdrm.part_number != "0")
                {
                    importe = (sdrm.part_price - sdrm.part_ieps - sdrm.part_iva) ;
                }
                else
                    importe = importeCn; //cuando es bonificacion le paso el mismo importe al articulo que esta registrado como bonificacion

                model = new SalesDetailReportModel();
                model.part_number = sdrm.part_number;
                model.part_name = sdrm.part_name;
                model.part_quantity = sdrm.part_quantity;
                model.part_price = importe/ sdrm.part_quantity;// sdrm.part_number != "0" ? (sdrm.part_price - sdrm.part_ieps - sdrm.part_iva)/ sdrm.part_quantity : 0;//-((decimal)(mt.tax_value / 100) * sld.detail_captured),
                model.part_importe = sdrm.part_number != "0" ? (sdrm.part_price - sdrm.part_ieps - sdrm.part_iva)  : 0; ;
                model.part_iva = sdrm.part_iva;
                model.part_iva_percentage = sdrm.part_iva / (importe != 0 ? importe : 1) * 100;
                model.part_ieps = sdrm.part_ieps;// (decimal)(mt.tax_value / 100) * (decimal)sld.detail_captured,
                model.part_ieps_percentage = sdrm.part_ieps / (importe != 0 ? importe : 1) * 100;//(int)(mt.tax_value)
                model.part_total = importe + sdrm.part_ieps + sdrm.part_iva;
                lisCreditNotesBondeDetailList.Add(model);
            }
            return lisCreditNotesBondeDetailList;
        }
    }
}
