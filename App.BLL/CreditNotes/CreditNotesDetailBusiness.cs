﻿using App.DAL.CreditNotes;
using App.DAL.Invoice;
using App.DAL.Sales;
using App.DAL.Site;
using App.Entities;
using App.Entities.ViewModels.CreditNotes;
using System;
using System.Collections.Generic;
using System.Linq;
using static App.Entities.ViewModels.Common.Constants;

namespace App.BLL.CreditNotes
{
    public class CreditNotesDetailBusiness
    {
        private CreditNotesDetailRepository _CreditNotesDetailRepository;
        private InvoiceRepository _InvoiceRepository;
        private InvoiceDetailRepository _InvoiceDetailRepository;
        private SalesDetailRepository _SalesDetailRepository;
        private CreditNotesSalesRepository _CreditNotesSalesRepository;
        private SiteConfigRepository _SiteConfigRepository;

        public CreditNotesDetailBusiness()
        {
            _CreditNotesDetailRepository = new CreditNotesDetailRepository();
            _InvoiceRepository = new InvoiceRepository();
            _InvoiceDetailRepository = new InvoiceDetailRepository();
            _SalesDetailRepository = new SalesDetailRepository();
            _CreditNotesSalesRepository = new CreditNotesSalesRepository();
            _SiteConfigRepository = new SiteConfigRepository();
        }

        public bool CreateCreditNoteDetail(CreditNote Model)
        {
            CREDIT_NOTE_DETAIL Item = new CREDIT_NOTE_DETAIL();
            bool SaveFlag = true;
            string site_code = _SiteConfigRepository.GetSiteCode();
            try
            {
                if (!Model.IsBonification)
                {
                    if (Model.IsSaleFromGlobal || !Model.IsSaleInvoiced)
                    {
                        foreach (var Object in Model.InvoiceDetails.Products)
                        {
                            var SaleDetails = _SalesDetailRepository.GetSaleDetailById(Object.DetailId);

                            Item.cn_id = Model.Number;
                            Item.cn_serie = Model.Serie;
                            Item.part_number = Object.ItemNumber;
                            Item.description = Object.ItemName;
                            Item.detail_qty = Object.QuantityReturn;
                            Item.detail_price = Object.QuantityReturn * Object.ItemPriceNoTax;//Object.QuantityReturn * Object.ItemPrice;
                            Item.site_code = site_code;
                            Item.sat_prodct_code = SaleDetails.ITEM.part_number_sat;
                            Item.sat_unt_code = SaleDetails.ITEM.unit_sat;
                            Item.sale_detail_id = SaleDetails.detail_id;
                            Item.part_number_sale = Object.SalePartNumber;

                            SaveFlag = _CreditNotesDetailRepository.CreateCreditNoteDetail(Item);

                            if (!SaveFlag)
                                break;
                            else
                            {
                                var CreditNoteSale = new CREDIT_NOTES_SALES();

                                CreditNoteSale.part_number = Object.ItemNumber;
                                CreditNoteSale.sale_detail_id = Object.DetailId;
                                CreditNoteSale.sale_id = Model.SaleNumber;
                                CreditNoteSale.detail_qty = Object.QuantityReturn;
                                CreditNoteSale.cn_id = Model.Number;
                                CreditNoteSale.cn_serie = Model.Serie;

                                SaveFlag = _CreditNotesSalesRepository.CreateCreditNoteSales(CreditNoteSale);

                                if (!SaveFlag)
                                    break;
                            }
                        }
                    }
                    else
                    {
                        if (Model.SelectAll && Model.IsSaleInvoiced) //Checkbox SelecAll is checked
                        {
                            var InvoiceDetails = _InvoiceDetailRepository.GetDetailsByInvoice(Model.InvoiceNumber.Value, Model.Serie);
                            foreach(var Element in InvoiceDetails)
                            {
                                Item.cn_id = Model.Number;
                                Item.cn_serie = Model.Serie;
                                Item.part_number = Element.part_number;
                                Item.description = Element.description;
                                Item.detail_qty = Element.detail_qty;
                                Item.detail_price = Element.detail_price;
                                Item.site_code = site_code;
                                Item.sat_prodct_code = Element.sat_prodct_code;
                                Item.sat_unt_code = Element.sat_unt_code;
                                Item.invoice_detail_id = Element.detail_id;

                                SaveFlag = _CreditNotesDetailRepository.CreateCreditNoteDetail(Item);

                                if (!SaveFlag)
                                    break;
                                else
                                {
                                    //Here we update the quantity available for return on each item.
                                    Element.detail_qty_available -= Element.detail_qty;
                                    _InvoiceDetailRepository.Update(Element);
                                }
                            }
                        }
                        else {
                            foreach (var Element in Model.InvoiceDetails.Products)
                            {
                                var InvoiceDetails = _InvoiceDetailRepository.GetInvoiceDetailsById(Element.DetailId);
                                Item.cn_id = Model.Number;
                                Item.cn_serie = Model.Serie;
                                Item.part_number = Element.ItemNumber;
                                Item.description = Element.ItemName;
                                Item.detail_qty = Element.QuantityReturn;
                                Item.detail_price = Math.Round(Element.QuantityReturn * Element.ItemPriceNoTax, 2);
                                Item.site_code = site_code;
                                Item.sat_prodct_code = InvoiceDetails.sat_prodct_code;
                                Item.sat_unt_code = InvoiceDetails.sat_unt_code;
                                Item.invoice_detail_id = Element.DetailId;

                                SaveFlag = _CreditNotesDetailRepository.CreateCreditNoteDetail(Item);

                                if (!SaveFlag)
                                    break;
                                else
                                {
                                    //Here we update the quantity available for return on each item.
                                    InvoiceDetails.detail_qty_available -= Element.QuantityReturn;
                                    _InvoiceDetailRepository.Update(InvoiceDetails);
                                }
                            }
                        }
                    }
                }
                else
                {
                    Item.cn_id = Model.Number;
                    Item.cn_serie = Model.Serie;
                    Item.part_number = GenericItem.ITEM_NUMBER;
                    Item.description = Model.Cause;
                    Item.detail_qty = 1;
                    Item.detail_price = Model.BonificationTaxes.Where(d => d.TaxCode == TaxCodes.SUB_TOTAL || d.TaxName == TaxTypes.SUBTOTAL).Sum(c => c.Bonification);
                    Item.site_code = _SiteConfigRepository.GetSiteCode();
                    Item.sat_prodct_code = "84111506";
                    Item.sat_unt_code = "ACT";

                    SaveFlag = _CreditNotesDetailRepository.CreateCreditNoteDetail(Item);
                }

                return SaveFlag;
            }
            catch(Exception ex)
            {
                string msg = ex.Message;
                return false;
            }
        }

        public long GetCreditNoteDetailId(string PartNumber, string CreditNoteSerie, long CreditNoteNumber, long SaleDetailId, long? InvoiceDetailId, string PartNumberSale) => _CreditNotesDetailRepository.GetCreditNoteDetailId(PartNumber, CreditNoteSerie, CreditNoteNumber, SaleDetailId, InvoiceDetailId, PartNumberSale);        

        public List<CreditNoteDetailsProducts> GetCreditNoteProductsDetailsByNumber(long CreditNoteNumber) => _CreditNotesDetailRepository.GetCreditNoteProductsDetailsByNumber(CreditNoteNumber);        

        public bool BonificationExist(long BonificationNumber) => _CreditNotesDetailRepository.BonificationExist(BonificationNumber);        

        public CREDIT_NOTE_DETAIL GetBonificationByNumber(long BonificationNumber) => _CreditNotesDetailRepository.GetBonificationByNumber(BonificationNumber);        

        public bool DeleteCreditNoteDetailsByInvoice(long InvoiceNumber, string InvoiceSerie) => _CreditNotesDetailRepository.DeleteCreditNoteDetailsByInvoice(InvoiceNumber, InvoiceSerie);

        public bool DeleteCreditNoteDetails(long Number, string Serie) => _CreditNotesDetailRepository.DeleteCreditNoteDetails(Number, Serie);        
    }
}
