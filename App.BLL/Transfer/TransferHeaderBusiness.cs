﻿using App.DAL.Item;
using App.DAL.MovementNumber;
using App.DAL.Tranfer;
using App.Entities.ViewModels.Android;
using App.Entities.ViewModels.Transfer;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using App.DAL.Site;
using App.DAL;

namespace App.BLL.Transfer
{
    public class TransferHeaderBusiness
    {
        private readonly TransferHeaderRepository _transferHeaderRepository;
        private readonly MovementNumberRepository _movementNumberRepository;
        private readonly TransferDetailRepository _transferDetailRepository;
        private readonly ItemRepository _ItemRepo;
        private readonly UserMasterBusiness _userMasterBusiness;
        private readonly SiteRepository _siteRepository;

        public TransferHeaderBusiness()
        {
            _transferHeaderRepository = new TransferHeaderRepository();
            _movementNumberRepository = new MovementNumberRepository();
            _transferDetailRepository = new TransferDetailRepository();
            _siteRepository = new SiteRepository();
            _ItemRepo = new ItemRepository();
            _userMasterBusiness = new UserMasterBusiness();
        }

        public List<TransferHeaderModel> GetAllTransferHeaderInit(DateTime DateInit, DateTime DateFin) => _transferHeaderRepository.GetAllTransferHeader(DateInit, DateFin);

        public string SendItemsToTransferWeb(List<TransferHeaderAndroidModel> items, string usuario, string site_code, string siteDestination) => SendItemsToTransferCommon(usuario, site_code, siteDestination, items, "CT002.CSHTML").Item1;

        public Tuple<string, List<TransferHeaderAndroidModel>> SendItemsToTransfer(string items, string usuario, string site_code, string siteDestination)
        {
            var list = TransferHeaderExtract(items);
            return SendItemsToTransferCommon(usuario, site_code, siteDestination, list, "AndroidTRT");
        }

        private Tuple<string, List<TransferHeaderAndroidModel>> SendItemsToTransferCommon(string usuario, string site_code, string siteDestination, List<TransferHeaderAndroidModel> list, string program_id)
        {
            try
            
            {
                var Documento = _movementNumberRepository.ExecSp_Get_Next_Document_Transfer();
                if (Documento == null)
                    return new Tuple<string, List<TransferHeaderAndroidModel>>(null, new List<TransferHeaderAndroidModel>());

                var header = new GlobalEntities.TRANSFER_HEADER_G
                {
                    transfer_document = Documento,
                    transfer_site_code = site_code,
                    destination_site_code = siteDestination,
                    transfer_status = 0,
                    transfer_date = DateTime.Now,
                    transfer_date_in = null,
                    delivery_order = null,
                    authorized_by = null,
                    driver_code = null,
                    print_status = 0,
                    comment = "",
                    comment_in = "",
                    cuser = usuario,
                    cdate = DateTime.Now,
                    program_id = program_id
                };

                var detail = list.Select(e =>
                {
                    var d = new GlobalEntities.TRANSFER_DETAIL_G
                    {
                        transfer_document = Documento,
                        part_number = e.part_number,
                        quantity = decimal.Parse(e.quantity),
                        cuser = usuario,
                        cdate = DateTime.Now,
                        program_id = program_id,
                        transfer_status = 0,
                    };
                    var Tax = _ItemRepo.StoreProcedureIvaIeps(e.part_number);
                    var Ieps = Tax.Ieps;
                    var Iva = Tax.Iva;
                    var price = _ItemRepo.GetMapPrice(e.part_number);
                    d.cost = price;
                    d.ieps = d.cost * (Ieps / 100);
                    d.iva = (d.cost + d.ieps) * (Iva / 100);
                    return d;
                }).ToList();

                if (_transferHeaderRepository.AddTransfer_G(header, detail, _siteRepository.SiteHasNewErp(siteDestination)))
                    return Tuple.Create(Documento, list);
                else
                    return Tuple.Create("ERROR HEADER", new List<TransferHeaderAndroidModel>());
            }
            catch (Exception ex)
            {
                List<TransferHeaderAndroidModel> listaError = new List<TransferHeaderAndroidModel>();
                var msg = ex.Message;
                return Tuple.Create("", listaError);
            }
        }

        private static List<TransferHeaderAndroidModel> TransferHeaderExtract(string items)
        {
            string v = "{\"Items\":" + items + "}";
            var result = JsonConvert.DeserializeObject<TransferHeaderAndroidModel>(v);
            List<TransferHeaderAndroidModel> list = new List<TransferHeaderAndroidModel>();
            for (int i = 0; i < result.Items.Count; i++)
            {
                TransferHeaderAndroidModel model = new TransferHeaderAndroidModel
                {
                    part_number = result.Items[i].part_number,
                    quantity = result.Items[i].quantity,
                    part_description = result.Items[i].part_description
                };
                list.Add(model);
            }

            return list;
        }

        public bool TransferHeaderEditStatus(List<TransferDetailModel> model, string Document, int Status, string User, string Comment, string driverCode) => _transferHeaderRepository.TransferHeaderEditStatus(model, Document, Status, User, Comment, driverCode);

        public List<TransferDetailModel> GetAllTransferDeatil(string Document)
        {
            var transfer = _transferDetailRepository.GetAllTransferDeatil(Document);
            foreach (var item in transfer)
            {
                var Tax = _ItemRepo.StoreProcedureIvaIeps(item.PartNumber);
                var Ieps = Tax.Ieps;
                var Iva = Tax.Iva;
                item.IEPS = item.SubTotal * (Ieps / 100);
                item.Iva = (item.SubTotal + item.IEPS) * (Iva / 100);
            }
            return transfer;
        }

        public List<TransferDetailModel> GetAllTransferDeatilsExist(string Document, string TransferSiteCode) => _transferDetailRepository.GetAllTransfeDetailsExist(Document, TransferSiteCode);
        public List<TransferDetailModel> GetAllTransferDetail(string Document, string TransferSiteCode, string destinationSiteName)
        {
           return _transferDetailRepository.GetAllTransfeDetailsExistNew(Document, TransferSiteCode, _siteRepository.GetSiteCodeByName(destinationSiteName));
        }
        public List<TransferDetailModel> GetAllTransferDetailNew(string Document, string TransferSiteCode, string DestinationSite)
        {
            return _transferDetailRepository.GetAllTransfeDetailsExistNew(Document, TransferSiteCode, DestinationSite);
        }
        public List<TransferDetailModel> NewGetAllTransfeDetailsExistFree(string Document) => _transferDetailRepository.NewGetAllTransfeDetailsExistFree(Document);

        public TransferHeaderModel GetTransferHeaderDocumentGI(string Document, string siteT)
        {
            var transfer = _transferHeaderRepository.GetTransferHeaderDocumentGI(Document, siteT);
            if (transfer != null)
            {
                var name = _userMasterBusiness.GetUserMasterByUsernameOrEmpNo(transfer.UUser);
                if (name != null)
                    transfer.AutorizeName = name.first_name + " " + name.last_name;

                name = _userMasterBusiness.GetUserMasterByUsernameOrEmpNo(transfer.CUser);
                if (name != null)
                    transfer.AccomplishedName = name.first_name + " " + name.last_name;
            }
            return transfer;
        }
        public TransferHeaderModel GetTransferHeaderDocumentGR(string Document, string destinationSite)
        {
            var transfer = _transferHeaderRepository.GetTransferHeaderDocumentGR(Document, destinationSite);
            if (transfer != null)
            {
                var name = _userMasterBusiness.GetUserMasterByUsernameOrEmpNo(transfer.UUser);
                if (name != null)
                    transfer.AutorizeName = name.first_name + " " + name.last_name;

                name = _userMasterBusiness.GetUserMasterByUsernameOrEmpNo(transfer.CUser);
                if (name != null)
                    transfer.AccomplishedName = name.first_name + " " + name.last_name;
            }
            return transfer;
        }
        public TransferHeaderModel GetTransferHeaderDocumentReport(string Document, string TransferSiteCode,string DestinationSiteCode)
        {
            var transfer = _transferHeaderRepository.GetTransferHeaderDocument(Document, TransferSiteCode, DestinationSiteCode);
            if (transfer != null)
            {
                var name = _userMasterBusiness.GetUserMasterByUsernameOrEmpNo(transfer.UUser);
                if (name != null)
                    transfer.AutorizeName = name.first_name + " " + name.last_name;

                name = _userMasterBusiness.GetUserMasterByUsernameOrEmpNo(transfer.CUser);
                if (name != null)
                    transfer.AccomplishedName = name.first_name + " " + name.last_name;
            }
            return transfer;
        }

        


        public bool TransferHeaderEditStatusPrint(string Document, int Status, string User) => _transferHeaderRepository.TransferHeaderEditStatusPrint(Document, Status, User);

        public List<TransferHeaderModel> GetAllTransferHeaderWithoutStatus(DateTime DateInit, DateTime DateFin) => _transferHeaderRepository.GetAllTransferHeaderWithoutStatus(DateInit, DateFin);

        public List<TransferDetailModel> GetAllTransferDetailWithoutStatus(string Document) => _transferDetailRepository.GetAllTransferDeatil(Document);

        public List<TransferDetailModel> GetAllTransferDetailWithoutCancelled(string Document, string TransferSiteCode, string DestinationSiteCode)
        {
            return _transferDetailRepository.GetAllTransferDeatilReportIn(Document, TransferSiteCode, DestinationSiteCode)
                .Where(e=>   e.transfer_status != 2 && e.transfer_status != 8)
                .ToList() ;
        }

        public TranferHeaderAndDetail TranferByDoc(string doc, string site_code) => _transferHeaderRepository.TranferByDoc(doc, site_code);

        public List<TransferHeaderModel> GetAllTransferByMonthStatus6or7() => _transferHeaderRepository.GetAllTransferHeaderActualMonthStatus6or7(DateTime.Now.Month, DateTime.Now.Year);
        public List<TransferHeaderModel> GetAllTransferHeaderStatus4() => _transferHeaderRepository.GetAllTransferHeaderStatus4();
        public List<TransferHeaderModel> GetAllTransferHeaderStatus9() => _transferHeaderRepository.GetAllTransferHeaderStatus9();
        public List<TransferHeaderModel> GetAllTransferHeaderStatus6() => _transferHeaderRepository.GetAllTransferHeaderStatus6();
        public List<TransferHeaderModel> GetAllTransferHeaderStatus7() => _transferHeaderRepository.GetAllTransferHeaderStatus7();
        public TransferHeaderModel GetTransferHeaderByDocument(string Document) => _transferHeaderRepository.GetTransferHeaderByDocument(Document);
        public TransferHeaderModel GetTransferHeaderByDocumentAndSites(string Document, string transferSite, string destinationSite) => _transferHeaderRepository.GetTransferHeaderByDocumentAndSites(Document, transferSite, _siteRepository.GetSiteCodeByName(destinationSite));

        public bool CancelRequestTransferHeader(string TransferDocument, string TransferSiteCode, string User, string Reason) => _transferHeaderRepository.CancelRequestTransferHeader(TransferDocument, TransferSiteCode, User, Reason);
        public bool CancelRequestTransferHeaderNew(string TransferDocument, string TransferSiteCode, string User, string Reason, string DestinationName) => _transferHeaderRepository.CancelRequestTransferHeaderNew(TransferDocument, TransferSiteCode, User, Reason, _siteRepository.GetSiteCodeByName(DestinationName));

        public List<TransferHeaderModel> GetAllTransferByMonth() => _transferHeaderRepository.GetAllTransferHeaderByMonth(DateTime.Now.Month, DateTime.Now.Year).ToList();

        public List<TransferHeaderModel> GetAllTransferByDates(DateTime BeginDate, DateTime EndDate) => _transferHeaderRepository.GetAllTransferByDates(BeginDate, EndDate);

        public int CancelTransfer(string doc, string transfer_site, string destination_site, string user)
        {
            var transfer = _transferHeaderRepository.GetTransferHeader(doc, transfer_site, destination_site);
            transfer.transfer_status = 8;
            transfer.uuser = user;
            transfer.udate = DateTime.Now;
            return _transferHeaderRepository.UpdateTransferHeader(transfer);
        }

        public List<TransferHeaderAndroidModel> GetItems(string transferDoc, string destination_site, string transferSiteCode)
        {
            return _transferDetailRepository.GetAllTransferDetailsItems(transferDoc, destination_site, transferSiteCode);
        }

        public StoreProcedureResult sproc_ST_Transit_Reverse_G(string Document, string User, string transfer_site_code, string destination_site_code)
        {
            return _transferHeaderRepository.sproc_ST_Transit_Reverse_G(Document, User, transfer_site_code, destination_site_code);
        }

        public int ValidateDestinationCode(string partNumber, string destinationCode)
        {
            return _transferHeaderRepository.ValidateDestinationCode(partNumber, destinationCode);
        }
    }
}