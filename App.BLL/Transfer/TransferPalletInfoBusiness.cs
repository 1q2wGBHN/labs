﻿using App.DAL.Transfer;
using App.Entities.ViewModels.Transfer;
using System;
using System.Collections.Generic;

namespace App.BLL.Transfer
{
    public class TransferPalletInfoBusiness
    {
        private readonly TransferPalletInfoRepository _transferPalletInfoRepo;

        public TransferPalletInfoBusiness()
        {
            _transferPalletInfoRepo = new TransferPalletInfoRepository();
        }

        public Tuple<bool, string> EditStatusTransferPalletInfo(string Pallet, string User)
        {
            /*quitar el store procedure*/
            if (!string.IsNullOrWhiteSpace(Pallet.Trim()))
            {
                var ok = _transferPalletInfoRepo.EditStatusTransferPalletInfo(Pallet, User);
                if (ok)
                {
                    var count = _transferPalletInfoRepo.CountOtherPalletsInTransfer(Pallet);
                    if (count == 0)
                    {
                        return Tuple.Create(true, "Se completo la transferencia");
                    }
                    return Tuple.Create(true, "Quedan valores pendientes en otro pallet para terminar la transferencia");
                }
                return Tuple.Create(false, "Quedaron unos que no se pudieron escanear");
            }
            return Tuple.Create(false, "No se encontro");
        }

        /*Nuevo reporte*/
        public List<NewTransferReport> TransferPalletReport(DateTime date, DateTime date2, int status) => _transferPalletInfoRepo.TransferPalletReport(date, date2, status);

        public List<NewTransferDetailReport> TransferPalletDetailReport(string palletsn) => _transferPalletInfoRepo.TransferPalletDetailReport(palletsn);

        public NewTransferReport GetTransferPalletReport(string palletsn) => _transferPalletInfoRepo.GetTransferPalletReport(palletsn);

        public List<TransferCedis> TransfersToReceive() => _transferPalletInfoRepo.TransfersToReceive();

        public List<TransferCedisPallet> PalletsInTransfer(string transferDocument) => _transferPalletInfoRepo.PalletsInTransfer(transferDocument);
    }
}