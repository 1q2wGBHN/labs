﻿using App.DAL.Tranfer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App.BLL.Transfer
{
    public class TransferDetailBusiness
    {
        private readonly TransferDetailRepository _transferDetailRepository;

        public TransferDetailBusiness()
        {
            _transferDetailRepository = new TransferDetailRepository();
        }

        public int CancelDetails(string doc, string transfer_site, string destination_site, string user)
        {
            int count = 0;
            var listItems = _transferDetailRepository.GetAllTransferDetail(doc,destination_site,transfer_site);
            for (int i = 0; i< listItems.Count; i++)
            {
                listItems[i].transfer_status = 8;
                listItems[i].uuser = user;
                listItems[i].udate = DateTime.Now;
                count += _transferDetailRepository.UpdateTransferDetail(listItems[i]);
            }
            return listItems.Count - count;
        }
    }
}
