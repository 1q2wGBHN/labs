﻿using App.DAL;
using App.DAL.Transfer;
using App.Entities.ViewModels.Android;
using App.Entities.ViewModels.PurchaseOrder;
using App.Entities.ViewModels.Transfer;
using System;
using System.Collections.Generic;
using System.Linq;

namespace App.BLL.Transfer
{
    public class TransferHeaderInBusiness
    {
        private readonly TransferHeaderInRepository _transHeaderInRepository;
        private readonly TransferDetailInRepository _transDetailInRepository;

        public TransferHeaderInBusiness()
        {
            _transHeaderInRepository = new TransferHeaderInRepository();
            _transDetailInRepository = new TransferDetailInRepository();
        }

        public List<TransferReceptionAndroidModel> GetAllTransfersInTransitExcludeCEDIS(string store)
        {
            return _transHeaderInRepository.GetAllTransfersInTransitExcludeCEDIS(store);
        }

        public List<TransferDetailInModel> ListTransferDetailInScanQuantity(int transferId, string transferDocument, string transferSiteCode)
        {
            return _transDetailInRepository.ListTransferDetailInScanQuantity(transferId, transferDocument,
                transferSiteCode);
        }

        public bool GetCompareList(int transferId, string transferDocument, string transferSiteCode)
        {
            var tra = _transDetailInRepository.GetAllItemsByTransferInTransit(transferId, transferDocument, transferSiteCode);
            return tra.All(e => e.scan_status == 1);
        }

        public bool UpdateTransferHeaderIn(int transferId, string part_number, decimal newQuantity, string user, string transferDocument, string transferSiteCode)
        {
            return _transDetailInRepository.UpdateTransferHeaderIn(transferId, part_number, newQuantity, user,
                transferDocument, transferSiteCode);
        }

        public bool GetTransferInTransit(int transferId, string transferDocument, string transferSiteCode)
        {
            return _transDetailInRepository.GetTransferInTransit(transferId, transferDocument, transferSiteCode);
        }

        public List<TransferReceptionAndroidModel> GetAllItemsByTransferInTransit(int transferId, string transferDocument, string transferSiteCode)
        {
            return _transDetailInRepository.GetAllItemsByTransferInTransit(transferId, transferDocument,
                transferSiteCode);
        }

        public StoreProcedureResult ProcessTransferIn(int TransferId, string User, string Program, string transferDocument, string transferSiteCode)
        {
            var returnValues = _transHeaderInRepository.ProcessTransferIn(TransferId, User, Program, transferDocument, transferSiteCode);
            switch (returnValues.ReturnValue)
            {
                case 6001:
                    returnValues.Document = "Site code Incorrecto";
                    break;
                case 6002:
                    returnValues.Document = "Estatus no aprobado";
                    break;
                case 8001:
                    returnValues.Document = "Error! Rollback Transaction T_GR_605";
                    break;
                default:
                    if (returnValues.Document != null)
                        returnValues.Document = "Registro Exitoso!";
                    else
                        returnValues.Document = "Error Desconocido en procedimiento almacenado [sproc_GR_From_Transfer]";
                    break;
            }
            return returnValues;
        }

        public List<TransferDetailModel> GetTransferDetailsInReport(string document, string transferSiteCode)
        {
            if (transferSiteCode == null) throw new Exception("Falta valor de SiteCode");
            return _transDetailInRepository.GetTransferDetailsInReport(document, transferSiteCode);
        }

        public bool CheckAllDetailsIn(int id, string doc, string transferSiteCode)
        {
            return _transDetailInRepository.CheckAllDetailsIn(id, doc, transferSiteCode);
        }

        public List<TransferHeaderModel> GetTransfersHeaderInView(DateTime initial, DateTime finish, string TransferType, string status, string originSite, string partNumber, string department, string family)
        {
            return _transHeaderInRepository.GetTransfersHeaderInView(initial, finish, TransferType, status, originSite,
                partNumber, department, family);
        }


        public List<TransferHeaderModel> GetTransfersHeaderIn(DateTime initial, DateTime finish, string TransferType, string status, string originSite, string partNumber, string department, string family)
        {
            return _transHeaderInRepository.GetTransfersHeaderIn(initial, finish, TransferType, status, originSite,
                partNumber, department, family);
        }


        public StoreProcedureResult ActionProcessTransferHeaderIn(string TransferDocument, string User, string transferSiteCode)
        {
            return _transHeaderInRepository.ProcessTransferIn(0, User, "RECE002.cshtml", TransferDocument,
                transferSiteCode);
        }

        public List<PurchaseOrderItemDetailReportGeneral> GetAllTransferDetail(DateTime date1, DateTime date2, string TransferType, string status, string originSite, string partNumber, string department, string family)
        {
            return _transDetailInRepository.GetAllTransferDetail(date1, date2, TransferType, status, originSite,
                partNumber, department, family);
        }
    }
}