﻿using App.Entities;
using System;

namespace App.BLL
{
    public class UserPwdBusiness
    {
        private UserPwdRepository _userPwdRepo;

        public UserPwdBusiness()
        {
            _userPwdRepo = new UserPwdRepository();
        }

        public USER_PWD GetUserPwdByEmployeeNumber(string employeeNumber) => _userPwdRepo.GetUserPwdByEmployeeNumber(employeeNumber);

        public bool UpdateUserPwd(USER_PWD user)
        {
            var check = false;
            if (_userPwdRepo.UpdateUserPwd(user) > 0) check = true;
            return check;
        }

        public bool IsPasswordNeedToChange(USER_PWD user)
        {
            if (DateTime.Now > user.valid_to)
                return true;
            return false;
        }

        public int UpdateStatusUser(string user, string status) => _userPwdRepo.UpdateStatusUser(user, status);
    }
}