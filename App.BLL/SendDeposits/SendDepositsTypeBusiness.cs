﻿using App.DAL.SendDeposits;
using App.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App.BLL.SendDeposits
{
    public class SendDepositsTypeBusiness
    {
        private SendDepositsTypeRepository _SendDepositsTypeRepository;

        public SendDepositsTypeBusiness()
        {
            _SendDepositsTypeRepository = new SendDepositsTypeRepository();
        }

        public List<SEND_DEPOSITS_TYPE> TypeList()
        {
            return _SendDepositsTypeRepository.TypeList();
        }
    }
}
