﻿using App.DAL.SendDeposits;
using App.Entities;
using App.Entities.ViewModels.SendDeposits;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static App.Entities.ViewModels.Common.Constants;

namespace App.BLL.SendDeposits
{
    public class SendDepositsBusiness
    {
        private SendDepositsRepository _SendDepositsRepository;

        public SendDepositsBusiness()
        {
            _SendDepositsRepository = new SendDepositsRepository();
        }

        public bool Create(SendDepositsModel Model)
        {
            Model.ProgramId = "SEDE001.cshtml";
            Model.AplicationDate = DateTime.ParseExact(Model.Date, DateFormat.INT_DATE, CultureInfo.InvariantCulture);
            return _SendDepositsRepository.Create(Model.SEND_DEPOSIT);
        }

        public List<SendDepositsModel> GetDeposits(string Date)
        {
            return _SendDepositsRepository.GetDeposits(Date);
        }

        public bool DeleteDeposits(int DepositId, string Uuser)
        {
            var SysLog = new SYS_LOG();
            SysLog.Action = "BORRRAR";
            SysLog.Date = DateTime.Now.Date;
            SysLog.Description = "BORRAR ENVÍO DE RETIRO";
            SysLog.idAfect = DepositId;
            SysLog.TableName = "DEPOSITOS";
            SysLog.Menu = "CAJAS";
            SysLog.time = DateTime.Now.ToString("HH:mm");
            SysLog.uuser = Uuser;

            return _SendDepositsRepository.DeleteDeposits(DepositId, Uuser, SysLog);
        }

        public SendDepositsModel GetDepositById(int DepositId)
        {
            return _SendDepositsRepository.GetDepositById(DepositId);
        }

        public bool UpdateDeposit(SendDepositsModel Model)
        {
            var SysLog = new SYS_LOG();                        
            SysLog.Action = "ACTUALIZAR";
            SysLog.Date = DateTime.Now.Date;
            SysLog.Description = "ACTUALIZACIÓN DE ENVÍO DE RETIRO";
            SysLog.idAfect = Model.Id;
            SysLog.TableName = "DEPOSITOS";
            SysLog.Menu = "CAJAS";
            SysLog.time = DateTime.Now.ToString("HH:mm");
            SysLog.uuser = Model.CUser;

            Model.AplicationDate = DateTime.ParseExact(Model.Date, DateFormat.INT_DATE, CultureInfo.InvariantCulture);
            var Deposit = Model.SEND_DEPOSIT;
            Deposit.program_id = "SEDE002.cshtml";
            Deposit.uuser = Model.CUser;
            Deposit.udate = DateTime.Now;
            Deposit.id = Model.Id;


            return _SendDepositsRepository.UpdateDeposit(Deposit, SysLog);
        }
    }
}
