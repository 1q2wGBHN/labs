﻿using App.DAL.SendDeposits;
using App.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App.BLL.SendDeposits
{
    public class SendDepositsOriginBusiness
    {
        private SendDepositsOriginRepository _SendDepositsOriginRepository;

        public SendDepositsOriginBusiness()
        {
            _SendDepositsOriginRepository = new SendDepositsOriginRepository();
        }

        public List<SEND_DEPOSITS_ORIGIN> OriginList()
        {
            return _SendDepositsOriginRepository.OriginList();
        }
    }
}
