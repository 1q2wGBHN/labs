﻿using App.Common;
using App.DAL.Approval;
using App.GlobalEntities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace App.BLL.Approvals
{
    public class ApprovalsBusiness
    {
        private readonly ApprovalsRepository approvalsRepo = new ApprovalsRepository();
        private readonly UserMasterRepository userMasterRepository = new UserMasterRepository();

        public Result<APPROVAL_HISTORY> StartProcess(string processName, string empNo, string programId)
        {
            var fullRoute = approvalsRepo.RouteOfProcess(processName);
            var perm = approvalsRepo.Permissions(empNo, processName, 1, null);
            if (perm == null)
                return Result.Error("No se cuenta con los permisos necesarios para realizar esta acción");

            var r = approvalsRepo.StartProcess(processName, empNo, perm.process_group, programId);
            if (r == null) return Result.Error();
            var route = fullRoute.FirstOrDefault(e => e.step_level == r.step_level);
            if (route == null) return Result.Error();

            if (route.finish_flag == true) return Result.OK(r);
            var nextRoute = fullRoute.FirstOrDefault(e => e.step_level == r.step_level + 1);
            if (nextRoute == null) return Result<Empty>.Error();
            var next = new APPROVAL_HISTORY
            {
                step_level = nextRoute.step_level,
                process_name = nextRoute.process_name,
                approval_datetime = null,
                approval_id = r.approval_id,
                approval_status = ApprovalStatus.OnWaiting,
                approval_type = nextRoute.approval_type,
                emp_no = r.emp_no,
                remark = null,
                program_id = r.program_id,
                process_group = perm.process_group,
            };
            NotifyToNextStep(r);
            var r2 = approvalsRepo.CreateHistoryRecord(next);
            return r2 != null ? Result.OK(r2) : Result.Error();
        }

        public Result<APPROVAL_HISTORY> UpdateProcess(APPROVAL_HISTORY model)
        {
            var r = approvalsRepo.UpdateHistoryRecord(model);
            if (r == null) return Result<Empty>.Error();

            NotifyToNextStep(r);
            NotifyToRequesters(r); //todo
            var route = approvalsRepo.GetApprovalRoute(r.process_name, r.step_level);
            if (route == null) return Result.Error();
            if (route.finish_flag == true) return Result.OK(r);
            var nextRoute = approvalsRepo.GetApprovalRoute(r.process_name, r.step_level + 1);
            if (nextRoute == null) return Result.OK(r);
            var next = new APPROVAL_HISTORY
            {
                step_level = r.step_level + 1,
                process_name = r.process_name,
                approval_datetime = r.approval_datetime,
                approval_id = r.approval_id,
                approval_status = ApprovalStatus.OnWaiting,
                approval_type = nextRoute.approval_type,
                emp_no = r.emp_no,
                remark = "",
                program_id = r.program_id,
                process_group = r.process_group
            };
            var r2 = approvalsRepo.CreateHistoryRecord(next);
            return r2 != null ? Result.OK(r2) : Result.Error();
        }

        private void NotifyToNextStep(APPROVAL_HISTORY rData)
        {
            var cm = new Common.Common();
            var url = System.Web.HttpContext.Current?.Request.Url.GetLeftPart(UriPartial.Authority) + "/Purchases/PURC006";
            var user = userMasterRepository.GetUserMasterByEmployeeNumber(rData.emp_no);
            var route = approvalsRepo.GetApprovalRoute(rData.process_name, rData.step_level);
            if (route == null) return;
            if (route.finish_flag == true) return;
            var step = rData.step_level + 1;
            var list = approvalsRepo.UsersFromProcessStep(rData.process_name, step, new[] { rData.process_group, "all" });
            Task.Factory.StartNew(() =>
            {
                Parallel.ForEach(list, u =>
                {
                    cm.MailMessageHtml(new SendMail
                    {
                        from = "Distribuidora el florido",
                        body = $@"
                            <p><strong>Folio: </strong> {rData.approval_id}</p>
                            <p><strong>Pedido por: </strong> {user?.first_name} {user?.last_name}</p>
                            ",
                        buttonLink = url,
                        buttonText = "Ver",
                        date = DateTime.Now.ToString(),
                        subject = "Ha recibido una nueva petición de aprobación",
                        email = u.email,
                        title = "Ha recibido una nueva petición de aprobación"
                    });
                });
            });
        }

        private void NotifyToRequesters(APPROVAL_HISTORY rData)
        {
            var cm = new Common.Common();
            var url = System.Web.HttpContext.Current?.Request.Url.GetLeftPart(UriPartial.Authority) + "/Purchases/PURC006";
            var user = userMasterRepository.GetUserMasterByEmployeeNumber(rData.emp_no);
            var list = approvalsRepo.RequestersUsersForStep(rData.approval_id, rData.process_name, rData.step_level, rData.process_group);

            Task.Factory.StartNew(() =>
            {
                Parallel.ForEach(list, u =>
                {
                    cm.MailMessageHtml(new SendMail
                    {
                        from = "Distribuidora el florido",
                        body = $@"
                                <ul>
                                    <li><strong>Folio: </strong> {rData.approval_id}</li>
                                    <li><strong>Paso: </strong> {rData.step_level}</li>
                                    <li><strong>Nuevo estado: </strong> {rData.approval_status}</li>
                                    <li><strong>Comentario: </strong> {rData.remark}</li>
                                    <li><strong>Revisado por: </strong>{user.first_name} {user.last_name}</li>
                                </ul>",
                        buttonLink = url,
                        buttonText = "Ver",
                        date = DateTime.Now.ToString(),
                        subject = "Ha recibido una respuesta a su petición",
                        email = u.email,
                        title = "Ha recibido una respuesta a su petición"
                    });
                });
            });
        }

        public Result<List<ProcessType>> ProcessTypeList() => Result.OK(approvalsRepo.ProcessTypeList());

        public Result<List<Process>> ProcessList(string processType) => Result.OK(approvalsRepo.ProcessList(processType));

        public Result<List<ProcessStep>> StepList(string processName) => Result.OK(approvalsRepo.StepList(processName));

        public Result<List<StepUser>> UserList(string processName, int stepLevel) => Result.OK(approvalsRepo.UserList(processName, stepLevel));

        public Result<Empty> StepAdd(ProcessStep step)
        {
            if (step == null) return Result.Error("No se agrego ningun paso");
            if (string.IsNullOrWhiteSpace(step.process_name)) return Result.Error("Nombre de proceso no valido");
            step.process_name = step.process_name.Trim();
            if (step.step_level <= 0) return Result.Error("El numero de paso debe ser mayor que 0");
            if (!ApprovalType.IsValid(step.approval_type)) return Result<Empty>.Error("Tipo de aprobación no valido");
            if (approvalsRepo.StepList(step.process_name).Any(p => p.step_level == step.step_level)) return Result<Empty>.Error("Ya existe este numero de paso");

            return approvalsRepo.StepAdd(step) ? Result<Empty>.OK() : Result<Empty>.Error();
        }

        public Result<Empty> UserAdd(StepUser user)
        {
            if (user == null) return Result.Error("No se agrego ningun usuario");
            if (userMasterRepository.GetUserMasterByEmployeeNumber(user.emp_no) == null) return Result.Error("No se encontro este numero de empleado");
            if (approvalsRepo.StepList(user.process_name).All(p => p.step_level != user.step_level)) return Result<Empty>.Error("No se encontro el proceso o paso seleccionado");
            if (approvalsRepo.UserList(user.process_name, user.step_level).Any(p => p.emp_no == user.emp_no && p.process_group == user.process_group)) return Result<Empty>.Error("Ya se agrego este empleado en este proceso, paso y grupo");
            return approvalsRepo.UserAdd(user) ? Result<Empty>.OK() : Result<Empty>.Error();
        }

        public Result<Empty> StepEdit(ProcessStep step)
        {
            if (step == null) return Result.Error("No se agrego ningun paso");
            if (string.IsNullOrWhiteSpace(step.process_name)) return Result.Error("Nombre de proceso no valido");
            if (step.step_level <= 0) return Result.Error("El numero de paso debe ser mayor que 0");
            if (!ApprovalType.IsValid(step.approval_type)) return Result<Empty>.Error("Tipo de aprobación no valido");
            if (approvalsRepo.StepList(step.process_name).All(p => p.step_level != step.step_level)) return Result<Empty>.Error("No se encontro el proceso o paso seleccionado");
            return approvalsRepo.StepAdd(step) ? Result<Empty>.OK() : Result<Empty>.Error();
        }

        public Result<Empty> UserDel(StepUser user)
        {
            if (user == null) return Result.Error("No se agrego ningun usuario");
            if (approvalsRepo.StepList(user.process_name).All(p => p.step_level != user.step_level)) return Result<Empty>.Error("No se encontro el proceso o paso seleccionado");
            if (approvalsRepo.UserList(user.process_name, user.step_level).All(p => p.emp_no != user.emp_no && p.process_group != user.process_name)) return Result<Empty>.Error("No se encontro el usuario seleccionado en este proceso, paso y grupo");
            return approvalsRepo.UserDel(user) ? Result<Empty>.OK() : Result<Empty>.Error();
        }
    }
}