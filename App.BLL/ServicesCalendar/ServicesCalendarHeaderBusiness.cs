﻿using App.BLL.Site;
using App.DAL.Expenses;
using App.DAL.ServicesCalendar;
using App.DAL.Supplier;
using App.Entities.ViewModels.ServicesCalendar;
using App.GlobalEntities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Linq;

namespace App.BLL.ServicesCalendar
{
    public class errors
    {
        public string clase { get; set; }
        public bool pass { get; set; }
        public int expense_id { get; set; }
        public string supplier_name { get; set; }
        public string error { get; set; }
    }

    public class ServicesCalendarHeaderBusiness
    {
        private readonly ServicesCalendarHeaderRepository _servicesCalendarHeaderRepository;
        private readonly SiteConfigBusiness _siteConfigBusiness;
        private readonly MaSupplierRepository _maSupplierRepository;
        private readonly ExpensesRepository _expensesRepository;

        public ServicesCalendarHeaderBusiness()
        {
            _servicesCalendarHeaderRepository = new ServicesCalendarHeaderRepository();
            _siteConfigBusiness = new SiteConfigBusiness();
            _maSupplierRepository = new MaSupplierRepository();
            _expensesRepository = new ExpensesRepository();
        }

        public List<ServicesCalendarModel> GetServicesHeaderBySiteCode(string site)
        {
            var listServices = _servicesCalendarHeaderRepository.GetServicesHeaderBySiteCode(site);
            if (listServices.Count < 1) { return null; }
            else { return listServices; }
        }

        public List<ServicesCalendarDetailModel> GetServicesDetail(int id) => _servicesCalendarHeaderRepository.GetServicesDetail(id);

        public List<ServicesCalendarModel> GetServicesHeaderByDate(string site, DateTime start, DateTime finish) => _servicesCalendarHeaderRepository.GetServicesHeaderByDate(site, start, finish);

        public List<ServicesCalendarModel> GetServicesHeaderByDateAndStatus(string site, DateTime start, DateTime finish, int status) => _servicesCalendarHeaderRepository.GetServicesHeaderByDateAndStatus(site, start, finish, status);

        public List<ServicesCalendarDetailModel> GetQuotationDocument(string site, int id, int supplier, int equipment) => _servicesCalendarHeaderRepository.GetQuotationDocument(site, id, supplier, equipment);

        public bool ModifyServicesDetail(List<ServicesCalendarDetailModel> list, string user, int status, int expense_id, int service_id)
        {
            if (list == null)
            {
                var x = _servicesCalendarHeaderRepository.ModifyServicesHeader(service_id, status, user, expense_id);
                return x;
            }
            else
            {
                bool x;
                foreach (var i in list)
                {
                    var id = i.equipment_id;
                    x = _servicesCalendarHeaderRepository.ModifyServicesDetail(i, id, service_id, user);
                    if (!x)
                        return x;
                }
                x = _servicesCalendarHeaderRepository.ModifyServicesHeader(service_id, status, user, expense_id);
                return x;
            }
        }

        public errors ActionAddXml(List<ServicesCalendarModel> data, string user)
        {
            try
            {
                var sitecode = _siteConfigBusiness.GetSiteCode();
                var expense = new EXPENSE
                {
                    site_code = sitecode,
                    remark = "",
                    reference_type = "MAQUINARIA",
                    cdate = DateTime.Now,
                    cuser = user,
                    program_id = "MRO001.cshtml"
                };
                var xml_header_list = new List<XML_HEADER>();
                var RFC = _servicesCalendarHeaderRepository.RFCOptions(data[0].service_id);
                foreach (var file in data)
                {
                    var index = 0;
                    var flag = 0;
                    var xml = Encoding.UTF8.GetString(Convert.FromBase64String(file.xml));
                    var doc = XDocument.Parse(xml);
                    var root = doc.Root;
                    var space = root?.Name.NamespaceName;
                    var rfc = (string)root?.Element(XName.Get("Emisor", space))?.Attribute("Rfc");
                    var date = (DateTime)root?.Attribute("Fecha");
                    var sup = _maSupplierRepository.GetSupplierByRFC(rfc);
                    if (RFC.Where(x => x == (string)rfc).Count() == 0)
                        return new errors() { pass = false, error = "No existe el proveedor en el XML: " + file.xml_name, clase = "Proveedor" };
                    if (sup.Count == 0)
                        return new errors() { pass = false, error = "No existe el proveedor en el XML: " + file.xml_name, clase = "Proveedor" };
                    else
                    {
                        for (var i = 0; i < sup.Count; i++)
                        {
                            index = i;
                            if (sup[i].supplier_id == 0)
                                return new errors() { pass = false, error = "No existe el proveedor en el XML: " + file.xml_name, clase = "Proveedor" };

                            if (sup[i].supplier_type != "Acreedor")
                                flag++;
                        }
                        if (flag == sup.Count)
                            return new errors() { pass = false, error = "El proveedor " + (sup.Any() ? sup[index].commercial_name : "") + " no esta como acreedor en el archivo " + file.xml_name, supplier_name = sup[index].commercial_name, clase = "Acreedor" };
                    }

                    var rfcSpace = XNamespace.Get("http://www.sat.gob.mx/TimbreFiscalDigital").NamespaceName;
                    var uuid = root?
                        .Descendants(XName.Get("TimbreFiscalDigital", rfcSpace)).FirstOrDefault()?
                        .Attribute("UUID");
                    var uuidExist = _expensesRepository.ExistUuid((string)uuid);

                    if (uuidExist > 0)
                        return new errors() { pass = false, error = "Ya existe el XML: " + file.xml_name, clase = "Uuid" };

                    var currency = (string)root?.Attribute("Moneda") ?? "";
                    var iepsTotal = 0M;
                    var ivaTotal = 0M;
                    var isr_retainedTotal = 0M;
                    var iva_retainedTotal = 0M;
                    try
                    {
                        iepsTotal = (decimal?)root.Element(XName.Get("Impuestos", space)).Element(XName.Get("Traslados", space)).Descendants(XName.Get("Traslado", space)).Where(a => (int)a.Attribute("Impuesto") == 3).Select(e => (decimal?)e.Attribute("Importe") ?? 0).FirstOrDefault() ?? 0;
                    }
                    catch (Exception ex)
                    {
                        string msg = ex.Message;
                        iepsTotal = 0;
                    }
                    try
                    {
                        isr_retainedTotal = (decimal?)root.Element(XName.Get("Impuestos", space)).Element(XName.Get("Retenciones", space)).Descendants(XName.Get("Retencion", space)).Where(a => (int)a.Attribute("Impuesto") == 1).Select(e => (decimal?)e.Attribute("Importe") ?? 0).FirstOrDefault() ?? 0;
                    }
                    catch (Exception ex)
                    {
                        string msg = ex.Message;
                        isr_retainedTotal = 0;
                    }
                    try
                    {
                        iva_retainedTotal = (decimal?)root.Element(XName.Get("Impuestos", space)).Element(XName.Get("Retenciones", space)).Descendants(XName.Get("Retencion", space)).Where(a => (int)a.Attribute("Impuesto") == 2).Select(e => (decimal?)e.Attribute("Importe") ?? 0).FirstOrDefault() ?? 0;
                    }
                    catch (Exception ex)
                    {
                        string msg = ex.Message;
                        iva_retainedTotal = 0;
                    }
                    try
                    {
                        ivaTotal = (decimal?)root.Element(XName.Get("Impuestos", space)).Element(XName.Get("Traslados", space)).Descendants(XName.Get("Traslado", space)).Where(a => (int)a.Attribute("Impuesto") == 2).Select(e => (decimal?)e.Attribute("Importe") ?? 0).FirstOrDefault() ?? 0;
                    }
                    catch (Exception ex)
                    {
                        string msg = ex.Message;
                        ivaTotal = 0;
                    }

                    var xml_header = new XML_HEADER
                    {
                        uuid_invoice = (string)uuid,
                        invoice_no = (string)root?.Attribute("Folio"),
                        invoice_date = date,
                        supplier_id = sup[index].supplier_id,
                        currency = currency,
                        iva = ivaTotal,
                        ieps = iepsTotal,
                        isr_retained = isr_retainedTotal,
                        iva_retained = iva_retainedTotal,
                        subtotal = (decimal?)root?.Attribute("SubTotal") ?? 0,
                        discount = (decimal?)root?.Attribute("Descuento") ?? 0,
                        total_amount = (decimal?)root?.Attribute("Total") ?? 0,
                        xml = file.xml,
                        xml_name = file.xml_name,
                        cdate = DateTime.Now,
                        cuser = user,
                        program_id = "MRO001.cshtml",
                        invoice_type = (string)root?.Attribute("TipoDeComprobante"),
                        exchange_currency = (decimal?)root?.Attribute("TipoCambio"),
                        payment_method = (string)root?.Attribute("MetodoPago"),
                        payment_way = (string)root?.Attribute("FormaPago"),
                        version = (string)root?.Attribute("Version")
                    };
                    xml_header.XML_DETAIL = root?.Descendants(XName.Get("Concepto", space)).Select(e =>
                    {
                        var iva_amount = (decimal?)e.Descendants(XName.Get("Traslado", space)).Where(a => (int)a.Attribute("Impuesto") == 2).Select(a => a.Attribute("Importe")).FirstOrDefault() ?? 0;
                        var ieps_amount = (decimal?)e.Descendants(XName.Get("Traslado", space)).Where(a => (int)a.Attribute("Impuesto") == 3).Select(a => a.Attribute("Importe")).FirstOrDefault() ?? 0;
                        var isr_retained = (decimal?)e.Descendants(XName.Get("Retencion", space)).Where(a => (int)a.Attribute("Impuesto") == 1).Select(a => a.Attribute("Importe")).FirstOrDefault() ?? 0;
                        var iva_retained = (decimal?)e.Descendants(XName.Get("Retencion", space)).Where(a => (int)a.Attribute("Impuesto") == 2).Select(a => a.Attribute("Importe")).FirstOrDefault() ?? 0;
                        return new XML_DETAIL
                        {
                            iva_amount = iva_amount,
                            ieps_amount = ieps_amount,
                            isr_retained = isr_retained,
                            iva_retained = iva_retained,
                            item_no = (string)e.Attribute("NoIdentificacion") ?? "",
                            item_description = (string)e.Attribute("Descripcion") ?? "",
                            quantity = (decimal?)e.Attribute("Cantidad") ?? 0,
                            unit_cost = (decimal?)e.Attribute("ValorUnitario") ?? 0,
                            item_amount = (decimal?)e.Attribute("Importe") ?? 0,
                            discount = (decimal?)e.Attribute("Descuento") ?? 0,
                        };
                    }).ToList();
                    xml_header_list.Add(xml_header);
                }
                var b = _expensesRepository.CreateExpense(expense, xml_header_list);
                if (b == null)
                    return new errors() { pass = false, error = "Error al leer el XML", clase = "xml" };  //error 

                return new errors() { pass = true, expense_id = b.expense_id };
            }
            catch (Exception ex)
            {
                string msg = ex.Message;
                return new errors() { pass = false, error = "Error al leer el XML", clase = "xml" };
            }
        }
    }
}