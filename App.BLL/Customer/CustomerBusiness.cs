﻿using App.DAL.CreditNotes;
using App.DAL.Customer;
using App.DAL.Invoice;
using App.DAL.Payments;
using App.Entities;
using App.Entities.ViewModels.Common;
using App.Entities.ViewModels.Customers;
using System.Collections.Generic;

namespace App.BLL.Customer
{
    public class CustomerBusiness
    {
        private CustomerRepository _CustomerRepository;
        private InvoiceRepository _InvoiceRepository;
        private PaymentsRepository _PaymentsRepository;
        private CreditNotesRepository _CreditNotesRepository;

        public CustomerBusiness()
        {
            _CustomerRepository = new CustomerRepository();
            _InvoiceRepository = new InvoiceRepository();
            _PaymentsRepository = new PaymentsRepository();
            _CreditNotesRepository = new CreditNotesRepository();
        }

        public CustomerViewModel GetCustomer(string CustomerCode) => _CustomerRepository.GetCustomer(CustomerCode);

        public CustomerViewModel GetCustomerByCode(string ClientNumber) => _CustomerRepository.GetCustomerByCode(ClientNumber);

        public CUSTOMERS GetByName(string CustomerName) => _CustomerRepository.GetByName(CustomerName);

        public bool Update(CUSTOMERS Item, SYS_LOG SysLog) => _CustomerRepository.Update(Item, SysLog);

        public bool Create(CUSTOMERS Item) => _CustomerRepository.Create(Item);

        public bool ExisitingRFC(string CustomerRFC) => _CustomerRepository.ExisitingRFC(CustomerRFC);

        public bool ExisitingCode(string CustomerCode) => _CustomerRepository.ExisitingCode(CustomerCode);

        public SendDocumentModel GetDocumentByNumberForSend(SendDocumentModel model)
        {
            if (model.Document == Documents.BON || model.Document == Documents.CNOT)
                return _CreditNotesRepository.GetSendDocumentByNumberCreditNoteBoni(long.Parse(model.Number));
            if (model.Document == Documents.INV)
                return _InvoiceRepository.GetSendDocumentByNumberInvoice(long.Parse(model.Number));
            else
                return _PaymentsRepository.GetSendDocumentByNumberPayment(long.Parse(model.Number));
        }

        public SendDocumentModel GetDocumentInfo (SendDocumentModel Model)
        {
            if (Model.Document == Documents.INV)
                return _InvoiceRepository.GetInvoice(long.Parse(Model.Number));

            if (Model.Document == Documents.BON || Model.Document == Documents.CNOT)
                return _CreditNotesRepository.GetCreditBon(long.Parse(Model.Number));

            if (Model.Document == Documents.PAY)
                return _PaymentsRepository.GetPayment(long.Parse(Model.Number));

            return new SendDocumentModel();
        }

        public object GetEntityValue (SendDocumentModel Model)
        {
            var PdfEncoded = string.Empty;
            var Entity = new object();

            if (Model.Document == Documents.INV)
                Entity = _InvoiceRepository.GetInvoiceByNumber(long.Parse(Model.Number));

            if (Model.Document == Documents.BON || Model.Document == Documents.CNOT)
                Entity = _CreditNotesRepository.GetCreditNoteByNumber(long.Parse(Model.Number));

            if (Model.Document == Documents.PAY)
                Entity = _PaymentsRepository.GetPaymentByNumber(long.Parse(Model.Number));            

            return Entity;
        }        

        public List<CustomersDDL> FilterCustomers(bool AllCostumers) => _CustomerRepository.FilterCustomers(AllCostumers);        

        public List<CustomersDDL> GetCustomers(CustomersLookup Option) => _CustomerRepository.GetCustomers(Option);

    }
}
