﻿using App.DAL.Lookups;
using App.Entities.ViewModels.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App.BLL.Lookups
{
    public class SatProductCodeBusiness
    {
        private SatProductCodeRepository _SatProductCodeRepository;

        public SatProductCodeBusiness()
        {
            _SatProductCodeRepository = new SatProductCodeRepository();
        }

        public List<SatLookup> SatProductCodeListSPROC()
        {
            return _SatProductCodeRepository.SatProductCodeListSPROC();
        }
    }
}
