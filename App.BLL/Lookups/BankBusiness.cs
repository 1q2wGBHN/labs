﻿using App.DAL.Lookups;
using App.Entities;
using System.Collections.Generic;

namespace App.BLL.Lookups
{
    public class BankBusiness
    {
        private BankRepository _BankRepository;

        public BankBusiness()
        {
            _BankRepository = new BankRepository();
        }

        public List<BANKS> BanksList() => _BankRepository.BanksList();
    }
}
