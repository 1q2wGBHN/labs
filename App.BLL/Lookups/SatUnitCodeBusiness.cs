﻿using App.DAL.Lookups;
using App.Entities;
using App.Entities.ViewModels.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App.BLL.Lookups
{
    public class SatUnitCodeBusiness
    {
        private SatUnitCodeRepository _SatUnitCodeRepository;

        public SatUnitCodeBusiness()
        {
            _SatUnitCodeRepository = new SatUnitCodeRepository();
        }

        public List<SAT_UNIT_CODE> SatUnitCodeList()
        {
            return _SatUnitCodeRepository.SatUnitCodeList();
        }

        public List<SatLookup> SatUseCodeListSPROC()
        {
            return _SatUnitCodeRepository.SatUseCodeListSPROC();
        }
    }
}
