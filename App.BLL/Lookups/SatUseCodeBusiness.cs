﻿using App.DAL.Lookups;
using App.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App.BLL.Lookups
{
    public class SatUseCodeBusiness
    {
        private SatUseCodeRepository _SatUseCodeRepository;

        public SatUseCodeBusiness()
        {
            _SatUseCodeRepository = new SatUseCodeRepository();
        }

        public string GetSatUseCodeByDescription(string Description)
        {
            return _SatUseCodeRepository.GetSatUseCodeByDescription(Description);
        }

        public List<SAT_USE_CODE> SatUseCodeList()
        {
            return _SatUseCodeRepository.SatUseCodeList();
        }
    }
}
