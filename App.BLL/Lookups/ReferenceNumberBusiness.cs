﻿using App.DAL.Lookups;
using App.Entities;

namespace App.BLL.Lookups
{
    public class ReferenceNumberBusiness
    {
        private ReferenceNumberRepository _ReferenceNumberRepository;

        public ReferenceNumberBusiness()
        {
            _ReferenceNumberRepository = new ReferenceNumberRepository();
        }

        public bool UpdateReferenceNumber(long Number, string Document)
        {
            return _ReferenceNumberRepository.UpdateReferenceNumber(Number, Document);
        }

        public REFERENCE_NUMBER GetReferenceNumber(string Document)
        {
            return _ReferenceNumberRepository.GetReferenceNumber(Document);
        }
    }
}
