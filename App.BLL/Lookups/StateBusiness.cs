﻿using App.DAL.Lookups;
using App.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App.BLL.Lookups
{
    public class StateBusiness
    {
        private StateRepository _StateRepository;

        public StateBusiness()
        {
            _StateRepository = new StateRepository();
        }

        public List<STATES> StatesList()
        {
            return _StateRepository.StatesList();
        }
    }
}
