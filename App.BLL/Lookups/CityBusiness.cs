﻿using App.DAL.Lookups;
using App.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App.BLL.Lookups
{
    public class CityBusiness
    {
        private CityRepository _CityRepository;

        public CityBusiness()
        {
            _CityRepository = new CityRepository();
        }

        public List<CITIES> CitiesByStateId(int StateId)
        {
            return _CityRepository.CitiesByStateId(StateId);
        }
    }
}
