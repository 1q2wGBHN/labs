﻿using App.DAL.Lookups;
using App.Entities;
using App.Entities.ViewModels.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static App.Entities.ViewModels.Common.Constants;

namespace App.BLL.Lookups
{
    public class PaymentMethodBusiness
    {
        private PaymentMethodRepository _PaymentMethodRepository;

        public PaymentMethodBusiness()
        {
            _PaymentMethodRepository = new PaymentMethodRepository();
        }

        public List<PAYMENT_METHOD> PaymentMethodList(bool isCredit = false, bool isFromInvoices = false)            
        {
            var PaymentList = _PaymentMethodRepository.PaymentMethodList();

            if (isCredit && isFromInvoices)
                PaymentList = PaymentList.Where(c => c.pm_id == PaymentMethods.CREDIT || c.pm_id == PaymentMethods.COMPENSATION).ToList();
            else if (!isCredit && isFromInvoices)
                PaymentList = PaymentList.Where(c => c.pm_id != PaymentMethods.CREDIT).ToList();

            return PaymentList;
        }

        public PAYMENT_METHOD GetPaymentMethodById(int PaymentMethodId)
        {
            return _PaymentMethodRepository.GetPaymentMethodById(PaymentMethodId);
        }

        public List<PaymentsMethods> PaymentMethodModelList() => _PaymentMethodRepository.PaymentMethodModelList();
        
        public List<PAYMENT_METHOD> PaymentMethodListSpecialInvoices() => _PaymentMethodRepository.PaymentMethodList();
    }
}
