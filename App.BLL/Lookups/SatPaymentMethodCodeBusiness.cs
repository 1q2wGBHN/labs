﻿using App.DAL.Lookups;
using App.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App.BLL.Lookups
{
    public class SatPaymentMethodCodeBusiness
    {
        private SatPaymentMethodCodeRepository _SatPaymentMethodCodeRepository;

        public SatPaymentMethodCodeBusiness()
        {
            _SatPaymentMethodCodeRepository = new SatPaymentMethodCodeRepository();
        }

        public string GetSatPaymentMethodCodeByDescription(string Description)
        {
            return _SatPaymentMethodCodeRepository.GetSatPaymentMethodCodeByDescription(Description);
        }

        public List<SAT_PAYMENT_METHOD_CODE> SatPaymentMethodCodeList()
        {
            return _SatPaymentMethodCodeRepository.SatPaymentMethodCodeList();
        }
    }
}
