﻿using App.DAL.Lookups;
using App.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App.BLL.Lookups
{
    public class ConfigPosSectionBusiness
    {
        private ConfigPosSectionRepository _ConfigPosSectionRepository;

        public ConfigPosSectionBusiness()
        {
            _ConfigPosSectionRepository = new ConfigPosSectionRepository();
        }

        public List<DFLPOS_CONFIG_SECTION> GetConfigSectionList()
        {
            return _ConfigPosSectionRepository.GetConfigSectionList();
        }

    }
}
