﻿using App.DAL.Lookups;

namespace App.BLL.Lookups
{
    public class CurrencyDetailBusiness
    {
        private CurrencyDetailRepository _CurrencyDetailRepository;

        public CurrencyDetailBusiness()
        {
            _CurrencyDetailRepository = new CurrencyDetailRepository();
        }

        public decimal CurrencyRate(string CurrencyCode) => _CurrencyDetailRepository.CurrencyRate(CurrencyCode);        

        public decimal GetCurrencyRateByDate(string date) => _CurrencyDetailRepository.GetCurrencyRateByDate(date);        

        public decimal GetCurrencySatRateByDate(string date) => _CurrencyDetailRepository.GetCurrencySatRateByDate(date);
    }
}
