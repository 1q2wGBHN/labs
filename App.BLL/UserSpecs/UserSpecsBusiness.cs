﻿using App.DAL.UserSpecs;
using App.Entities.ViewModels.UserSpecs;
using System.Collections.Generic;

namespace App.BLL.UserSpecs
{
    public class UserSpecsBusiness
    {
        private readonly UserSpecsRepository _UserSpecsRepo;

        public UserSpecsBusiness()
        {
            _UserSpecsRepo = new UserSpecsRepository();
        }

        public List<UserSpecsModel> GetAllUserSpecs() => _UserSpecsRepo.GetAllUserSpecs();

        public bool SaveUserSpeck(UserSpecsModel UserSpecs, string User) => _UserSpecsRepo.SaveUserSpeck(UserSpecs, User);

        public bool EditUserSpeck(UserSpecsModel UserSpecs, string User) => _UserSpecsRepo.EditUserSpeck(UserSpecs, User);

        public List<UserSpecsModel> GetAllUserSpecsAndDriveFalse() => _UserSpecsRepo.GetAllUserSpecsAndDriveFalse();
    }
}