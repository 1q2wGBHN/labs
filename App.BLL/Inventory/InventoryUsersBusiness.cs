﻿using App.DAL.Inventory;
using App.Entities.ViewModels.Inventory;
using System.Collections.Generic;
using static App.DAL.Inventory.InventoryUsersRepository;

namespace App.BLL.Inventory
{
    public class InventoryUsersBusiness
    {
        private readonly InventoryUsersRepository _inventoryUsersRepository;

        public InventoryUsersBusiness()
        {
            _inventoryUsersRepository = new InventoryUsersRepository();
        }

        public string EditInventoryUser(string id, int user_no, string user_name, string emp_no, string user, int status)
        {
            var checkBool = _inventoryUsersRepository.CheckEmpNo(id, emp_no, user_no, status);
            if (checkBool == false)
            {
                if (status == 0)
                {
                    var edit = _inventoryUsersRepository.EditInventoryUser(id, user_no, user_name, emp_no, user);
                    return edit;
                }
                else
                {
                    var pallet = _inventoryUsersRepository.CheckUserPalletHeader(user_no, id);
                    var detail = _inventoryUsersRepository.CheckUserInventoryDetail(user_no, id);
                    if (detail == true && pallet == true)
                        return _inventoryUsersRepository.EditInventoryUser(id, user_no, user_name, emp_no, user);
                    else
                        return "errorUsuario";
                }
            }
            else
                return "errorEmp_no";
        }

        public InventoryActive GetInventoryActive() => _inventoryUsersRepository.GetInventoryActive();

        public InventoryActive GetInventoryCountActive() => _inventoryUsersRepository.GetInventoryCountActive();

        public List<InventoryUsersModel> GetInventoryUsers(string id) => _inventoryUsersRepository.GetInventoryUsers(id);

        public List<InventoryUsersModel> GetInventoryUsersCountPallets(string inventory_id) => _inventoryUsersRepository.GetInventoryUsersCountPallets(inventory_id);

        public List<InventoryUsersModel> GetInventoryTopUsers(string inventory_id) => _inventoryUsersRepository.GetInventoryTopUsers(inventory_id);
    }
}