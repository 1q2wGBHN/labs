﻿using App.Common;
using App.DAL.Inventory;
using App.Entities.ViewModels.Inventory;
using System;
using System.Collections.Generic;

namespace App.BLL.Inventory
{
    public class InventoryHeaderBusiness
    {
        private readonly InventoryHeader _repoInvetoryHeader;
        private readonly InventoryLabelBusiness _inventoryLabel;
        private readonly InventoryPalletsBusiness _inventoryPalletsBusiness;
        private readonly InventoryUsersBusiness _inventoryUsersBusiness;
        private readonly InventoryDetailBusiness _inventoryDetailBusiness;

        public InventoryHeaderBusiness()
        {
            _repoInvetoryHeader = new InventoryHeader();
            _inventoryLabel = new InventoryLabelBusiness();
            _inventoryPalletsBusiness = new InventoryPalletsBusiness();
            _inventoryUsersBusiness = new InventoryUsersBusiness();
            _inventoryDetailBusiness = new InventoryDetailBusiness();
        }

        public List<InventoryHeaderModel> GetInventorysByDate(DateTime startDate, DateTime endDate)
        {
            var items = _repoInvetoryHeader.GetInventorysByDate(startDate, endDate);
            foreach (var item in items)
                item.countPallets = _inventoryPalletsBusiness.GetInventoryPreInventoried(item.id_inventory_header);

            return items;
        }

        public List<InventoryHeaderModel> GetInventorysByDatesNoPallets(DateTime startDate, DateTime endDate)
        {
            var items = _repoInvetoryHeader.GetInventorysByDate(startDate, endDate);
            return items;
        }

        public Result<List<InventoryHeaderModel>> GetInventoryTagManager() => Result.OK(_repoInvetoryHeader.TagGetInventory(new List<int> { 0, 1 }));

        public Result<int> InventoryStatus(string idInventary)
        {
            var r = _repoInvetoryHeader.InventoryStatus(idInventary);
            return r.HasValue ? Result.OK(r.Value) : Result.Error("No se encontro inventario");
        }

        public InventoryInformation GetInvenotoryInfo(string inventory_id)
        {
            var status = _repoInvetoryHeader.InventoryStatus(inventory_id);
            InventoryInformation II = new InventoryInformation
            {
                GetInventoryPalletsCancelled = _inventoryPalletsBusiness.GetInventoryCancelled(inventory_id),
                GetInventoryPalletsFinished = _inventoryPalletsBusiness.GetInventoryFinished(inventory_id),
                GetInventoryPalletsPreInventoried = _inventoryPalletsBusiness.GetInventoryPreInventoried(inventory_id),
                LastLabelInventory = _inventoryLabel.GetLastLabelInventory(inventory_id),
                InventoryPallets = _inventoryUsersBusiness.GetInventoryTopUsers(inventory_id)
            };
            if (status.Value == 1)
            {
                II.GetInventoryDetailCountItems = _inventoryDetailBusiness.GetAllItemsCount(inventory_id);
                II.InventoryTotalPriceAmount = _inventoryDetailBusiness.GetTotalPriceAmountById(inventory_id);
            }
            else
            {
                II.InventoryTotalPriceAmount = _inventoryDetailBusiness.GetTotalPriceAmountAdjustmentById(inventory_id);
                II.GetInventoryDetailCountItems = _inventoryDetailBusiness.GetAllItemsAdjustmentCount(inventory_id);
            }
            II.InventoryTotalPriceAmountTeoric = _inventoryPalletsBusiness.GetInventoryTotalTeoric(inventory_id);
            return II;
        }

        public List<InventoryHeaderModel> GetAllInventoryHeader() => _repoInvetoryHeader.GetInventory(9);

        public List<InventoryHeaderModel> GetInventory(IEnumerable<int> statusList) => _repoInvetoryHeader.GetInventory(statusList);
        public List<InventoryHeaderModel> GetInventoryOrderByStart(IEnumerable<int> statusList) => _repoInvetoryHeader.GetInventoryOrderByStart(statusList);

        public InventoryHeaderModel GetInventoryById(string id) => _repoInvetoryHeader.GetInventoryById(id);
        public bool CheckCyclicInventory(string id) => _repoInvetoryHeader.CheckCyclicInventory(id);
        public List<InventoryType> GetInventoryTypeActive()
        {
            return _repoInvetoryHeader.GetInventoryTypeActive();
        }
        }
}