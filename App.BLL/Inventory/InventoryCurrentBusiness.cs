﻿using App.DAL.Inventory;
using App.Entities.ViewModels.Inventory;
using System;
using System.Collections.Generic;
using System.Globalization;

namespace App.BLL.Inventory
{
    public class InventoryCurrentBusiness
    {
        private readonly InventoryHeader _InventoryHeader;
        private readonly InventoryCurrentRepository _InventoryCurrentRepo;

        public InventoryCurrentBusiness()
        {
            _InventoryHeader = new InventoryHeader();
            _InventoryCurrentRepo = new InventoryCurrentRepository();
        }

        public List<InventoryHeaderModel> GetInventory(int Status) => _InventoryHeader.GetInventory(Status);

        public List<InventoryHeaderModel> GetInventoryExist() => _InventoryHeader.GetInventoryExist();

        public bool EditStatusInventory(string IdInventoryHeader, int Status, string User)
        {
            if (IdInventoryHeader != null && !string.IsNullOrWhiteSpace(User.Trim()))
                return _InventoryHeader.EditStatusInventory(IdInventoryHeader, Status, User);

            return false;
        }

        public void UpdateJobCloseInventory(int status) => _InventoryHeader.UpdateJobCloseInventory(status);

        public List<InventoryCurrentModel> GetInventoryDetail(int IdInventoryHeader)
        {
            if (IdInventoryHeader > 0)
                return _InventoryCurrentRepo.GetInventoryDetail(IdInventoryHeader);

            return new List<InventoryCurrentModel>();
        }

        public bool SaveInventoryCurrentModel(string IdInventoryHeader, string User)
        {
            var InventoryList = _InventoryCurrentRepo.GetPriceItem(IdInventoryHeader, "VENTA", User, "INVE003.cshtml");
            if (InventoryList == 1)
                return false;
            else
                return true;
        }

        public bool ExistInventory(string IdInventoryHeader)
        {
            var InventoryList = _InventoryCurrentRepo.ExistInventory(IdInventoryHeader, "VENTA");
            if (InventoryList > 0)
                return true;
            else
                return false;
        }

        public bool AvailableInventory(string IdInventoryHeader)
        {
            var InventoryList = _InventoryCurrentRepo.AvailableInventory(IdInventoryHeader);
            if (InventoryList > 0)
                return true;
            else
                return false;
        }

        public int UpdateInventoryQuantity(string IdInventoryHeader, string PartNumber, int Quantity, string User, string location) => _InventoryCurrentRepo.PostInveryDetailHistory(IdInventoryHeader, PartNumber, Quantity, User, location);

        public int NewUpdateInventoryQuantity(int inventory, string IdInventoryHeader, string PartNumber, decimal Quantity, string User, string location) => _InventoryCurrentRepo.NewPostInveryDetailHistory(inventory, IdInventoryHeader, PartNumber, Quantity, User, location);

        public List<InventoryDetailHistoryModel> InventoryHistoryDetailAll(int id) => _InventoryCurrentRepo.InventoryHistoryDetailAll(id);

        public InventoryReportModel GetInventoryDiferentDetailItem(string IdInventoryHeader, string PartNumber, string location) => _InventoryCurrentRepo.GetInventoryDiferentDetailItem(IdInventoryHeader, PartNumber, location);

        public bool IsDateInventory(string IdInventoryHeader)
        {
            var Inventory = _InventoryCurrentRepo.DateInventory(IdInventoryHeader);
            DateTime dateValue = new DateTime(Inventory.start_date.Year, Inventory.start_date.Month, 1);
            if (Inventory.type_review.ToUpper() == "ANUAL")
            {
                for (int i = 0; i <= 8; i++)
                {
                    if (dateValue.AddYears(1).AddMonths(1).AddDays(-i).ToString("dddd", new CultureInfo("es-ES")).ToUpper() == Inventory.day.ToUpper())
                    {
                        if (DateTime.Now.Date == dateValue.AddYears(1).AddMonths(1).AddDays(-i).Date)
                            return true;
                    }
                }
            }
            if (Inventory.type_review.ToUpper() == "MENSUAL")
            {
                for (int i = 1; i <= 8; i++)
                {
                    if (dateValue.AddMonths(1).AddDays(-i).ToString("dddd", new CultureInfo("es-ES")).ToUpper() == Inventory.day.ToUpper())
                    {
                        if (DateTime.Now.Date == dateValue.AddMonths(1).AddDays(-i).Date)
                            return true;
                    }
                }
            }
            if (Inventory.type_review.ToUpper() == "SEMANAL")
            {
                for (int i = 0; i <= 7; i++)
                {
                    if (dateValue.AddDays(7).AddDays(-i).ToString("dddd", new CultureInfo("es-ES")).ToUpper() == Inventory.day.ToUpper())
                    {
                        if (DateTime.Now.Date == dateValue.AddDays(7).AddDays(-i).Date)
                            return true;
                    }
                }
            }
            if (Inventory.type_review.ToUpper() == "SEMESTRAL")
            {
                for (int i = 1; i <= 8; i++)
                {
                    if (dateValue.AddMonths(7).AddDays(-i).ToString("dddd", new CultureInfo("es-ES")).ToUpper() == Inventory.day.ToUpper())
                    {
                        if (DateTime.Now.Date == dateValue.AddMonths(7).AddDays(-i).Date)
                            return true;
                    }
                }
            }
            if (Inventory.type_review.ToUpper() == "INDEFINIDO")
                return true;

            return false;
        }

        //Filtra los datos del inventario att. OCD
        public List<InventoryReportModel> GetInventoryDiferentDetail(bool PartNumberDiferent, string Location, int Departament, bool NotCount, string id)
        {
            var type = _InventoryHeader.GetInventoryById(id).inventory_type;
            return _InventoryCurrentRepo.GetInventoryDiferentDetail(PartNumberDiferent, Location, Departament, NotCount, type, id);
        }

        //trae los datos del pallet att. OCD
        public List<InventoryReportModel> GetInventoryDiferentPallet() => _InventoryCurrentRepo.GetInventoryDiferentPallet();

        public List<InventoryReportModel> GetInventoryDiferentDetailOption(string PartNumber , string location) => _InventoryCurrentRepo.GetInventoryDiferentDetailOption(PartNumber , location);

        public bool SyncInventory(string idInventory, string user)
        {
            var inventory = _InventoryHeader.GetInventoryExistInCount();
            //VALIDAR SOLO ROL INVENTARIOS Y SISTEMAS (
            if (inventory != null)
            {
                return _InventoryCurrentRepo.SyncInventory(idInventory, user);
            }
            else
            {
                return false;
            }
        }

        public List<List<InventoryReportModel>> GetInventoryDiferentDetailByDepartment(string id)
        {
            var r = _InventoryCurrentRepo.GetInventoryDiferentDetailByDepartment(id);
            return r;
        }

        public string GetLastSync(string id)
        {
            var sync = _InventoryCurrentRepo.GetLastSync(id);
            string r;
            if (sync != null)
                r = sync.dateSync + " " + sync.userName;
            else
                r = "No se ha sincronizado";

            return r;
        }

        public string GetScrapDatesActive(string id)
        {
            var dates = _InventoryCurrentRepo.GetScrapDatesActive(id);
            var r = "";
            if (dates != null)
                r = dates.startDate + " - " + dates.endDate;
            return r;
        }
    }
}