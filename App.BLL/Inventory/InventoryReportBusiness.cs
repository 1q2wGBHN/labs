﻿using App.DAL.Inventory;
using App.Entities.ViewModels.Inventory;
using System.Collections.Generic;

namespace App.BLL.Inventory
{
    public class InventoryReportBusiness
    {
        private readonly InventoryReportRepository _inventoryReportRepository = new InventoryReportRepository();
        private readonly InventoryPalletsRepository _inventoryPalletRepository = new InventoryPalletsRepository();

        public List<InventoryHeaderModel> GetInventoryActive() => _inventoryReportRepository.GetInventoryActive();

        public List<InventoryAdjustmentModel> GetInventoryAdjustementAll() => _inventoryReportRepository.GetInventoryAdjustementAll();

        public List<InventoryReportModel> GetInventoryReportDetail(string id) => _inventoryReportRepository.GetInventoryReportDetail(id);

        public string UpdateInventoryStatus(string id, string user) => _inventoryReportRepository.UpdateInventoryStatus(id, user);

        public bool FinishInventory(string id) => _inventoryPalletRepository.FinishInventory(id);

        public List<InventoryReportModel> GetInventoryById(string id)
        {
            var x = _inventoryReportRepository.GetInventoryById(id);
            return x;
        }

        public InventoryHeaderModel GetInventoryHeader(string id) => _inventoryReportRepository.getInventoryHeader(id);

        public string GetScrapDates(string id)
        {
            var dates = _inventoryReportRepository.GetScrapDates(id);
            var r = dates.startDate + " - " + dates.endDate;
            return r;
        }
    }
}