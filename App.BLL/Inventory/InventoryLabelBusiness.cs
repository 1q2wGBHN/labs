﻿using App.BLL.Configuration;
using App.Common;
using App.DAL.Inventory;
using App.Entities.ViewModels.Inventory;
using System;
using System.Collections.Generic;

namespace App.BLL.Inventory
{
    public class InventoryLabelBusiness
    {
        private readonly InventoryLabel _repoInventoryLabel;
        private readonly SysSiteConfigBusiness _SiteConfig;

        public InventoryLabelBusiness()
        {
            _repoInventoryLabel = new InventoryLabel();
            _SiteConfig = new SysSiteConfigBusiness();
        }

        public bool CreateLabels(string id, int quantity, string user)
        {
            int digitos = 0;
            string label = "";
            List<InventoryLabelModel> labels = new List<InventoryLabelModel>();

            var last_count = _repoInventoryLabel.GetLastLabel(id);
            int last_count_temp = last_count;
            string myId = id.Substring(3, 7);

            for (int i = 0; i < quantity; i++)
            {
                last_count_temp++;
                if (last_count_temp == 0)
                    digitos = 1;
                else
                    digitos = (int)Math.Floor(Math.Log10(last_count_temp) + 1);

                if (digitos == 1)
                    label = myId.ToString() + "N00000" + last_count_temp.ToString();
                else if (digitos == 2)
                    label = myId.ToString() + "N0000" + last_count_temp.ToString();
                else if (digitos == 3)
                    label = myId.ToString() + "N000" + last_count_temp.ToString();
                else if (digitos == 4)
                    label = myId.ToString() + "N00" + last_count_temp.ToString();
                else if (digitos == 5)
                    label = myId.ToString() + "N0" + last_count_temp.ToString();
                else if (digitos == 6)
                    label = myId.ToString() + "N" + last_count_temp.ToString();

                var modelLabel = new InventoryLabelModel
                {
                    id_inventory = id,
                    last_number = last_count_temp,
                    serial_partnumer = label,
                    cuser = user,
                };

                labels.Add(modelLabel);
            }

            foreach (var item in labels)
            {
                _repoInventoryLabel.SendInventoryLabels(item);
            }

            return true;
        }


        public Result<InventoryLabelTagModel> GetLastLabel(string id_inventory)
        {
            var modelSite = _SiteConfig.GetSite();
            InventoryLabelTagModel modelTag = new InventoryLabelTagModel
            {
                last_number = _repoInventoryLabel.GetLastLabel(id_inventory),
                site_code = modelSite.site_code
            };
            return Result.OK(modelTag);
        }

        public Result<int> SendInventoryLabels(List<InventoryLabelModel> labels)
        {
            foreach (var label in labels)
            {
                int r = _repoInventoryLabel.SendInventoryLabels(label);
                if (r == 0)
                    return Result.OK(r);
            }
            return Result.OK(1);
        }

        public int GetLastLabelInventory(string id_inventory) => _repoInventoryLabel.GetLastLabel(id_inventory);

        public List<string> GetEmptyLabels(string id) => _repoInventoryLabel.GetEmptyLabels(id);

        public List<string> GetAllLabels(string id) => _repoInventoryLabel.GetAllLabels(id);

        public PalletModel GetPalletHeaderByLabel(string serial_number)
        {
            var r = _repoInventoryLabel.GetPalletHeaderByLabel(serial_number);
            if (r == null)
                return new PalletModel();
            return r;
        }
    }
}