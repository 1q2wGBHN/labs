﻿using App.DAL.Inventory;
using App.Entities;
using App.Entities.ViewModels.Inventory;
using App.Entities.ViewModels.Item;
using System;
using System.Collections.Generic;
using System.Linq;

namespace App.BLL.Inventory
{
    public class InventoryDetailBusiness
    {
        private readonly InventoryDetailRepository _InventoryDetailRepository;
        private readonly InventoryHeader _inventoryHeaderRepository;
        private readonly InventoryPalletsRepository _inventoryPalletRepository;
        private readonly InventoryLocationRepository _inventoryLocationRepository;
       

        public InventoryDetailBusiness()
        {
            _InventoryDetailRepository = new InventoryDetailRepository();
            _inventoryHeaderRepository = new InventoryHeader();
            _inventoryPalletRepository = new InventoryPalletsRepository();
            _inventoryLocationRepository = new InventoryLocationRepository();
        }

        public bool ValidateBarcodeInventory(string barcode, string inventoryId) => _InventoryDetailRepository.ValidateBarcodeInventory(barcode, inventoryId);

        public List<ItemModel> GetInventoryItems(int genericuser, string inventoryid) => _InventoryDetailRepository.GetInventoryItems(genericuser, inventoryid);

        public bool SetInventoryCount(string inventoryid, string barcode, string quantity, string location, string user, int genericuser)
        {
            try
            {
                var model = new INVENTORY_DETAIL
                {
                    id_inventory_header = inventoryid,
                    part_number = barcode,
                    quantity = Convert.ToDecimal(quantity),
                    storage_location = location,
                    user_no = genericuser,
                    cdate = DateTime.Now,
                    cuser = user,
                    program_id = "AndroidCMR",
                };

                return _InventoryDetailRepository.SetInventoryCount(model, user);
            }
            catch (Exception ex)
            {
                string msg = ex.Message;
                return false;
            }
        }

        public List<InventoryFinalModel> GetAllItemsDetailById(string inventoryId) => _InventoryDetailRepository.GetAllItemsDetailById(inventoryId);

        public decimal GetTotalPriceAmountById(string inventoryId)
        {
            var items = _InventoryDetailRepository.GetAllItemsDetailById(inventoryId);
            return items.Sum(s => s.total_amount);
        }

        public List<InventoryFinalModel> GetAllItemsDetailAdjustmentById(string inventoryId) => _InventoryDetailRepository.GetAllItemsDetailAdjustmentById(inventoryId);

        public decimal GetTotalPriceAmountAdjustmentById(string inventoryId)
        {
            var items = _InventoryDetailRepository.GetAllItemsDetailAdjustmentById(inventoryId);
            if (items != null)
                return items.Sum(s => s.total_amount);
            else
                return 0;
        }

        public int GetAllItemsCount(string inventoryId) => _InventoryDetailRepository.GetAllItemsCount(inventoryId);

        public decimal GetAllItemsAdjustmentCount(string inventoryId)
        {
            var items = _InventoryDetailRepository.GetAllItemsDetailAdjustmentById(inventoryId);
            if (items != null)
                return items.Sum(s => s.counted);
            else
                return 0;
        }

        public Tuple<bool, string> SetInventoryCountWeb(string inventoryid, string barcode, decimal quantity, string location, string user)
        {
            if (_inventoryHeaderRepository.InventoryStatus(inventoryid) == 1)
            {
                if (_inventoryPalletRepository.CkItemInventoryTypeCyclic(inventoryid, barcode))
                {
                    if (!_inventoryHeaderRepository.CheckCyclicInventory(inventoryid))
                    {
                        if (_inventoryLocationRepository.GetCountInventoryLocation() != 0)
                        {
                            if (!_inventoryLocationRepository.CheckInventoryLocation(location, inventoryid))
                                return new Tuple<bool, string>(false, "Locacion no valida para el inventario");
                        }
                    }
                    try
                    {
                        var model = new INVENTORY_DETAIL
                        {
                            id_inventory_header = inventoryid,
                            part_number = barcode,
                            quantity = quantity,
                            storage_location = location,
                            user_no = 0,
                            cdate = DateTime.Now,
                            cuser = user,
                            program_id = "INVE011",
                        };
                        if (_InventoryDetailRepository.SetInventoryCount(model, user))
                        {
                            return new Tuple<bool, string>(true, "");
                        }
                        else
                        {
                            return new Tuple<bool, string>(false, "Ha ocurrido un error al guardar");
                        }
                    }
                    catch (Exception ex)
                    {
                        string msg = ex.Message;
                        return new Tuple<bool, string>(false, "Ha ocurrido un error al guardar");
                    }
                }
                else
                {
                    return new Tuple<bool, string>(false, "El producto no pertence al inventario");
                }
            }
            else
                return new Tuple<bool, string>(false, "El inventario no está activo");
        }
        public List<InventoryItemLocationModel> GetInventoryItemsByLocation(string id, List<string> location, string type, List<string> items)
        {
            var l = "";
            var i = "";
            if (location[0].Length != 0 || items[0].Length != 0)
            {
                if (type == "location")
                {
                    foreach (var loc in location) { l += " '" + loc + "' ,"; }
                    l = l.Remove(l.Length - 1);
                }
                else
                {
                    foreach (var it in items) { i += " '" + it + "' ,"; }
                    i = i.Remove(i.Length - 1);
                }
            }
       
            return _InventoryDetailRepository.GetInventoryItemsByLocation(id, l,type,i);
        }
    }
}