﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using App.DAL.Inventory;
using App.Entities.ViewModels.Inventory;

namespace App.BLL.Inventory
{
    public class InventoryLocationBusiness
    {
        private readonly InventoryLocationRepository _inventoryLocationRepo = new InventoryLocationRepository();

        public bool AddInventoryLocation(InventoryLocationModel model)
        {
            //Agregar header
            var id_inventory = _inventoryLocationRepo.AddInventoryLocation(model);
            if (id_inventory != 0)
            {
                //Agregar tipos de inventario
                foreach(var m in model.id_inventory_type)
                {
                    var d = _inventoryLocationRepo.AddInventoryLocationType(id_inventory, m);
                    if (!d) { return false; }
                }
            }else
            {
                return false;
            }
            return true;
        }
        public bool UpdateInventoryLocation(InventoryLocationModel model)
        {
            //Elminar tipo inventario para agregar nueva
            if (_inventoryLocationRepo.DeleteInventoryLocationsType(model.id_location))
            {
                //Modificar Inventory Location
                if (_inventoryLocationRepo.UpdateInventoryLocation(model))
                {
                    //Agregar tipos de inventario
                    foreach (var m in model.id_inventory_type)
                    {
                        var d = _inventoryLocationRepo.AddInventoryLocationType(model.id_location, m);
                        if (!d) { return false; }
                    }
                    return true;
                }
                else
                    return false;
            }
            else
                return false;
        }
        public List<InventoryLocationModel> GetAllInventoryLocations()
        {
            return _inventoryLocationRepo.GetAllInventoryLocations();
        }
        public int GetCountInventoryLocation()
        {
            return _inventoryLocationRepo.GetCountInventoryLocation();
        }
        public bool CheckInventoryLocation(string part_number , string id)
        {
            return _inventoryLocationRepo.CheckInventoryLocation(part_number, id);
        }
        public List<InventoryItemLocationModel> GetProductsNotExhibited(string id)
        {
            return _inventoryLocationRepo.GetProductsNotExhibited(id);
        }
        public List<InventoryLocationModel> GetLocationsNotCounted(string id)
        {
            return _inventoryLocationRepo.GetLocationsNotCounted(id);
        }
        public List<InventoryItemLocationModel> GetInventoryFormat(string location)
        {
            return _inventoryLocationRepo.GetInventoryFormat(location);
        }
        public bool AddInventoryFormat(string location, string part_number , string cuser , string program)
        {
            return _inventoryLocationRepo.AddInventoryFormat(location,part_number , cuser , program);
        }
        public bool DeleteInventoryFormat(string location, string part_number)
        {
            return _inventoryLocationRepo.DeleteInventoryFormat(location, part_number);
        }
        }
}
