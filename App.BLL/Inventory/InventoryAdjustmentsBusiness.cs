﻿using App.DAL.Inventory;
using App.Entities;
using App.Entities.ViewModels.Inventory;
using App.Entities.ViewModels.Item;
using System;
using System.Collections.Generic;
using System.Configuration;

namespace App.BLL.Inventory
{
    public class InventoryAdjustmentsBusiness
    {
        private readonly InventoryAdjustmentsRepository _InventoryAdjustmentsRepo;
        private readonly UserMasterBusiness _userMasterBusiness;

        public InventoryAdjustmentsBusiness()
        {
            _InventoryAdjustmentsRepo = new InventoryAdjustmentsRepository();
            _userMasterBusiness = new UserMasterBusiness();
        }

        public List<DropItemModel> GetInventoryProductsDetailDrop(string InventoryNo) => _InventoryAdjustmentsRepo.GetInventoryProductsDetailDrop(InventoryNo);

        public InventoryAdjustmentModel GetInventoryItemQuantity(string InventoryNo, string partNumber) => _InventoryAdjustmentsRepo.GetInventoryItemQuantity(InventoryNo, partNumber);

        public bool AddAdjusment(InventoryAdjustmentModel Obj)
        {
            var item = _InventoryAdjustmentsRepo.GetInventoryAdjustmentItemByInventoryAndPartNumber(Obj.id_inventory_header, Obj.part_number);
            var Produc = new INVENTORY_ADJUSTMENT_HISTORY
            {
                id_inventory_header = Obj.id_inventory_header,
                part_number = Obj.part_number,
                previous_quantity = Obj.previous_quantity,
                new_quantity = Obj.new_quantity,
                justification = Obj.justification,
                evidence_document_base64 = Obj.evidence_document_base64,
                evidence_document_ext = Obj.evidence_document_ext,
                adjustment_status = 0,
                cuser = item == null ? Obj.cuser : item.cuser,
                cdate = item == null ? DateTime.Now : item.cdate,
                uuser = item == null ? Obj.uuser : item.cuser,
                udate = item == null ? Obj.udate : DateTime.Now,
                program_id = Obj.program_id
            };
            //if (item == null)
            return _InventoryAdjustmentsRepo.AddAdjusment(Produc);
            //else
            //return _InventoryAdjustmentsRepo.UpdateAdjusment(Produc);
        }

        public bool UpdateAdjusment(InventoryAdjustmentModel Obj)
        {
            var item = _InventoryAdjustmentsRepo.GetInventoryAdjustmentItemByInventoryAndPartNumber(Obj.id_inventory_header, Obj.part_number);
            var Produc = new INVENTORY_ADJUSTMENT_HISTORY
            {
                id_inventory_header = Obj.id_inventory_header,
                part_number = Obj.part_number,
                previous_quantity = Obj.previous_quantity,
                new_quantity = Obj.new_quantity,
                justification = Obj.justification,
                evidence_document_base64 = Obj.evidence_document_base64,
                evidence_document_ext = Obj.evidence_document_ext,
                adjustment_status = 0,
                cuser = item == null ? Obj.cuser : item.cuser,
                cdate = item == null ? DateTime.Now : item.cdate,
                uuser = item == null ? Obj.uuser : item.cuser,
                udate = item == null ? Obj.udate : DateTime.Now,
                program_id = Obj.program_id
            };
            return _InventoryAdjustmentsRepo.UpdateAdjusment(Produc);
        }


        public List<AdjustmentRequestModel> GetAdjustmentRequests() => _InventoryAdjustmentsRepo.GetAdjustmentRequests();

        public bool UpdateAdjustment(string InventoryHeaderId, string PartNumber, int AdjustmentStatus, string User, out List<AdjustmentRequestModel> model) => _InventoryAdjustmentsRepo.UpdateAdjustment(InventoryHeaderId, PartNumber, AdjustmentStatus, User, "INVE006.cshtml", out model);

        public AdjustmentRequestModel GetFile(string InventoryHeaderId, string PartNumber) => _InventoryAdjustmentsRepo.GetFile(InventoryHeaderId, PartNumber);

        public bool FillInventoryFinal(string InventoryHeaderId, string User, int typeAj) => _InventoryAdjustmentsRepo.fillInventoryFinal(InventoryHeaderId, User, typeAj);

        //OCD
        public List<AdjustmentRequestModel> HistoryInventoryAdjustment(int status) => _InventoryAdjustmentsRepo.historyInventoryAdjustment(status);

        public int ExistInventoryAdjustment(string id, string part_number)
        {
            var r = _InventoryAdjustmentsRepo.existInventoryAdjustment(id, part_number);
            if (r == null)
                return 5;
            return r.adjustment_status ?? 5;
        }

        public bool ExistInventoryAdjustmentInPending() => _InventoryAdjustmentsRepo.existInventoryAdjustmentInPending();
        public string AdjustNegatives(string user)
        {
            if (_userMasterBusiness.IsRolonUser(user, ConfigurationManager.AppSettings["DepartamentoInventarios"].ToString()) ||
                _userMasterBusiness.IsRolonUser(user, ConfigurationManager.AppSettings["Sistemas"].ToString()))
                return _InventoryAdjustmentsRepo.AdjustNegatives(user);
            else
                return "No tiene privilegios para realizar la accion";
        }
        public bool InsertNewNegative( InventoryNegativeHeaderModel modelHeader, List<InventoryNegativeDetailModel> modelDetail , string user)
        {
            var header = _InventoryAdjustmentsRepo.InsertNegativeHeader(modelHeader , user);
            if(header != 0)
            {
                foreach(var detail in modelDetail)
                {
                    detail.id_inventory_negative = header;
                    var insDet = _InventoryAdjustmentsRepo.InsertNegativeDetail(detail , user);
                }
                return true;
            }else
            {
                return false;
            }
        }
        public List<InventoryNegativeHeaderModel> GetAllNegativeHeader()
        {
            return _InventoryAdjustmentsRepo.GetAllNegativeHeader();
        }
        public List<InventoryNegativeDetailModel> GetNegativeDetail(int id)
        {
            return _InventoryAdjustmentsRepo.GetNegativeDetail(id);
        }
        public List<InventoryNegativeHeaderModel> GetAllNegativeHeaderByDateAndStatus(DateTime dateInit, DateTime dateFin, int status)
        {
            return _InventoryAdjustmentsRepo.GetAllNegativeHeaderByDateAndStatus(dateInit, dateFin, status);
        }

        }
}