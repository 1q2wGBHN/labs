﻿using App.Common;
using App.DAL.Inventory;
using App.DAL.Item;
using App.Entities.ViewModels.Inventory;
using FloridoERPTX.Controllers.Inventory;
using System;
using System.Collections.Generic;
using System.Linq;

namespace App.BLL.Inventory
{
    public class InventoryPalletsBusiness
    {
        private readonly InventoryPalletsRepository inventoryPalletsRepository = new InventoryPalletsRepository();
        private readonly InventoryLabel inventoryLabelsRepository = new InventoryLabel();
        private readonly ItemRepository itemRepository = new ItemRepository();
        private readonly InventoryHeader InventoryHeaderRepository = new InventoryHeader();
        private readonly InventoryUsersRepository inventoryUsersRepository = new InventoryUsersRepository();
        private readonly InventoryLocationRepository inventoryLocationRepository = new InventoryLocationRepository();

        public Result<List<PalletModel>> PalletList(PalletListFilter filter) => Result.OK(inventoryPalletsRepository.PalletList(filter));

        public Result<PalletModel> CreatePallet(PalletModel model, List<PalletDetailModel> details, string username, string programId)
        {
            var inventaries = InventoryHeaderRepository.GetInventory(new[] { InventoryStatus.initial, InventoryStatus.active });

            model.storage_location_name = model.storage_location_name.Trim();

            //REVISAR SI TIENE LOCACIONES 

            if(inventoryLocationRepository.GetCountInventoryLocation() != 0)
            {
                // REVISAR SI LA LOCACION ES VALIDA PARA EL INVENTARIO
                if(!inventoryLocationRepository.CheckInventoryLocation(model.storage_location_name  , model.id_inventory))
                {
                    return Result.Error("Locacion no valida para inventario");
                }
            }            


            if (inventaries.Count == 0)
                return Result.Error("No hay inventarios abiertos o activos");

            var label = inventoryLabelsRepository.LabelById(model.serial_number);

            if (label == null)
                return Result.Error<PalletModel>("No existe este codigo de etiqueta");
            if (label.id_inventory != model.id_inventory)
                return Result.Error<PalletModel>("Esta etiqueta no pertence a este inventario");
            if (inventaries.Find(e => e.id_inventory_header == model.id_inventory) == null)
                return Result.Error("Este inventario no esta abierto o activo");
            if (inventoryPalletsRepository.PalletById(model.serial_number) != null)
                return Result<PalletModel>.Error("Ya existe un pallet con este codigo");
            //if (!inventoryUsersRepository.CheckUserIsValid(model.id_inventory, model.user_no) && model.user_no != 0)
            //    return Result<PalletModel>.Error("El usuario no es valido");

            return Result.From(inventoryPalletsRepository.CreatePallet(model, details, username, programId));
        }

        public Result<Empty> DeletePallet(string serialNumber)
        {
            var p = inventoryPalletsRepository.PalletById(serialNumber);

            if (p == null)
                return Result<PalletModel>.Error("No existe un pallet con este codigo");
            if (new[] { PalletStatus.Cancelled, PalletStatus.Finished }.Contains(p.pallet_status))
                return Result<Empty>.Error("No se puede eliminar un pallet que ya ha sido inventariado o terminado");

            return Result.Check(inventoryPalletsRepository.DeletePallet(serialNumber));
        }

        public Result<List<PalletDetailModel>> PalletItemList(PalletItemListFilter filter) => Result.OK(inventoryPalletsRepository.PalletItemList(filter));

        public Result<PalletDetailModel> AddToPallet(string serialNumber, PalletDetailModel detail, string username, string program_id)
        {
            var p = inventoryPalletsRepository.PalletById(serialNumber);

            if (p == null)
                return Result.Error("No existe un pallet con este codigo");

            var inventaries = InventoryHeaderRepository.GetInventory(new[] { InventoryStatus.initial, InventoryStatus.active });

            if (inventaries.Count == 0)
                return Result.Error("No hay inventarios abiertos o activos");
            if (inventaries.Find(e => e.id_inventory_header == p.id_inventory) == null)
                return Result.Error("Este inventario no esta abierto o activo");

            var item = itemRepository.SearchCode(detail.barcode, true, true, false, false);

            if (item == null || !item.Any())
                return Result.Error("Codigo de producto inexistente");

            detail.part_number = item.First().ItemBarcode.PartNumber;

            if (new[] { PalletStatus.Cancelled, PalletStatus.Finished }.Contains(p.pallet_status))
                return Result.Error("No se puede agregar a un pallet que ya ha sido inventariado o esta terminado");
            if (detail.quantity <= 0)
                return Result<Empty>.Error("Por lo menos se tiene que agregar un producto");
            if (!inventoryPalletsRepository.CkItemInventoryType(p.id_inventory, detail.part_number))
                return Result.Error("Este producto no es valido para este inventario");

            var r = inventoryPalletsRepository.AddOrReplaceToPallet(serialNumber, detail, username, program_id);
            return r != null ? Result.OK(r) : Result<PalletDetailModel>.Error();
        }

        public Result<Empty> DeleteFromPallet(string serialNumber, List<int> idDetailList)
        {
            var p = inventoryPalletsRepository.PalletById(serialNumber);

            if (p == null)
                return Result.Error("No existe un pallet con este codigo");

            var inventaries = InventoryHeaderRepository.GetInventory(new[] { InventoryStatus.initial, InventoryStatus.active });

            if (inventaries.Count == 0)
                return Result.Error("No hay inventarios abiertos o activos");
            if (inventaries.Find(e => e.id_inventory_header == p.id_inventory) == null)
                return Result.Error("Este inventario no esta abierto o activo");
            if (inventoryPalletsRepository.DetailsByIds(idDetailList).Count != idDetailList.Count)
                return Result.Error("Codigo de producto inexistente");
            if (new[] { PalletStatus.Cancelled, PalletStatus.Finished }.Contains(p.pallet_status))
                return Result.Error("No se puede eliminar de un pallet que ya ha sido inventariado o esta terminado");

            return Result.Check(inventoryPalletsRepository.DeleteFromPallet(serialNumber, idDetailList));
        }

        public Result<Empty> AddPalletToInventory(string serialNumber, string username, string program_id, int userNo)
        {
            var inv = InventoryHeaderRepository.GetInventory(InventoryStatus.active);

            if (inv == null || inv.Count == 0)
                return Result.Error("No hay inventario activo");
            if (inv.Count > 1)
                return Result.Error("Hay mas de un inventario activo, por favor contacte a sistemas");

            var p = inventoryPalletsRepository.PalletById(serialNumber);

            if (p == null)
                return Result.Error("No existe un pallet con este codigo");
            if (p.pallet_status == PalletStatus.Cancelled || p.pallet_status == PalletStatus.Finished)
                return Result<Empty>.Error("Este pallet a ha sido inventariado o cancelado");
            var inventaries =
                InventoryHeaderRepository.GetInventory(new[] { InventoryStatus.initial, InventoryStatus.active });
            if (inventaries.Count == 0)
                return Result.Error("No hay inventarios abiertos o activos");
            if (inventaries.Find(e => e.id_inventory_header == p.id_inventory) == null)
                return Result.Error("Este inventario no esta abierto o activo");
            if (inv.All(e => e.id_inventory_header != p.id_inventory))
                return Result.Error("Inventario no activo, no se pueden agregar mas productos");
            //if (!inventoryUsersRepository.CheckUserIsValid(inv[0].id_inventory_header, userNo))
            //    return Result<PalletModel>.Error("El usuario no es valido");

            return Result.Check(inventoryPalletsRepository.AddPalletToInventory(serialNumber, username, program_id, userNo));
        }

        public Result<PalletModel> PalletById(string serial_number)
        {
            var r = inventoryPalletsRepository.PalletById(serial_number);
            return r != null ? Result.OK(r) : Result<PalletModel>.Error("No se encontro este pallet");
        }

        public List<PalletModel> GetInventoryUsersCountSerialNumber(string inventory_id, int user_no)
        {
            var items = inventoryPalletsRepository.GetInventoryUsersCountSerialNumber(inventory_id, user_no);
            foreach (var item in items)
            {
                var itemPrice = inventoryPalletsRepository.GetInventoryPalletsDetail(item.serial_number);
                var price_amount = itemPrice.Sum(s => s.price_amount);
                item.price_amount = price_amount;
            }
            return items;
        }

        public int GetInventoryPreInventoried(string id_inventory) => inventoryPalletsRepository.GetInventoryPreInventoried(id_inventory);

        public decimal GetInventoryTotalTeoric(string id_inventory) => inventoryPalletsRepository.GetInventoryTotalTeoric(id_inventory);

        public int GetInventoryCancelled(string id_inventory) => inventoryPalletsRepository.GetInventoryCancelled(id_inventory);

        public int GetInventoryFinished(string id_inventory) => inventoryPalletsRepository.GetInventoryFinished(id_inventory);

        public bool CkItemInventoryType(string id_inventory, string part_number) => inventoryPalletsRepository.CkItemInventoryType(id_inventory, part_number);
        public bool CkItemInventoryTypeCyclic(string id_inventory, string part_number) => inventoryPalletsRepository.CkItemInventoryTypeCyclic(id_inventory, part_number);

        public List<PalletModel> GetInventoryPalletsById(string inventory_id) => inventoryPalletsRepository.GetInventoryPalletsById(inventory_id);

        public List<PalletDetailItem> GetInventoryPalletsDetail(string serial_number) => inventoryPalletsRepository.GetInventoryPalletsDetail(serial_number);

        public Result<PalletDetailItem> ExistInPallet(string serialNumber, string barcode)
        {
            var r = inventoryPalletsRepository.ExistInPallet(serialNumber, barcode);
            return r != null ? Result.OK(r) : Result<PalletDetailItem>.Error("No se encontro este producto", 10);
        }

        public Tuple<bool, string> AddItemToPallet(string id, string part_number, string serial_number, string user, decimal quantity)
        {
            if (inventoryPalletsRepository.CkItemInventoryType(id, part_number))
            {
                PalletDetailModel model = new PalletDetailModel
                {
                    serial_number = serial_number,
                    part_number = part_number,
                    cuser = user,
                    cdate = DateTime.Now,
                    quantity = quantity
                };
                if (inventoryPalletsRepository.AddOrReplaceToPallet(serial_number, model, user, "INVE009") != null)
                {
                    return new Tuple<bool, string>(true, "");
                }
                else
                {
                    return new Tuple<bool, string>(false, "Ha ocurrido un error al insertar el registro");
                }
            }
            else
            {
                return new Tuple<bool, string>(false, "El producto no corresponde al tipo de inventario");
            }
        }
    }
}