﻿using App.DAL.Item;
using App.DAL.PurchaseBaseCost;
using App.Entities.ViewModels.PurchaseBaseCost;
using System.Collections.Generic;

namespace App.BLL.PurchaseBaseCost
{
    public class PurchaseBaseCostBusiness
    {
        private readonly PurchaseBaseCostRepository _purchaseBaseCostRepository;
        private readonly ItemRepository _itemRepository;

        public PurchaseBaseCostBusiness()
        {
            _purchaseBaseCostRepository = new PurchaseBaseCostRepository();
        }

        public PurchaseBaseCostModel getPurchaseBaseCostBySupplierPartNumber(int supplier_part_number)
        {
            var MaTax = _purchaseBaseCostRepository.GetPurchaseBaseCostBySupplierPartNumber(supplier_part_number);
            if (MaTax != null)
                return MaTax;
            else
                return null;
        }
        public List<string> GetSupplierCurrency(int supplier_id)
        {
            return _purchaseBaseCostRepository.GetSupplierCurrency(supplier_id);
        }
    }
}