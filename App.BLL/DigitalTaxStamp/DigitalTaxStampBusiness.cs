﻿using App.DAL.DigitalTaxStamp;
using App.Entities;

namespace App.BLL.DigitalTaxStamp
{
    public class DigitalTaxStampBusiness
    {
        private readonly DigitalTaxStampRepository _DigitalTaxStampRepository;

        public DigitalTaxStampBusiness()
        {
            _DigitalTaxStampRepository = new DigitalTaxStampRepository();
        }

        public DIGITAL_TAX_STAMP GetStampInfo(long Number, string Serie, string DocumentType) => _DigitalTaxStampRepository.GetStampInfo(Number, Serie, DocumentType);        

        public bool IsInvoiceStamped(long Number) => _DigitalTaxStampRepository.IsInvoiceStamped(Number);        

        public bool IsCreditNoteBonStamped(long Number) => _DigitalTaxStampRepository.IsCreditNoteBonStamped(Number);        

        public bool IsPayment(long Number) => _DigitalTaxStampRepository.IsPaymentStamped(Number);        
    }
}