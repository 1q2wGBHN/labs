﻿using App.BLL.PurchaseBaseCost;
using App.DAL.EntryFree;
using App.DAL.Item;
using App.DAL.MaTax;
using App.Entities.ViewModels.Android;
using System.Collections.Generic;

namespace App.BLL.EntryFree
{
    public class EntryFreeListBusiness
    {
        private readonly EntryFreeListRepository _EntryFreeListRepo;
        private readonly ItemSupplierRepository _ItemSupplierRepo;
        private readonly PurchaseBaseCostBusiness _PurchaseBAseCostBusiness;
        private readonly MaTaxRepository _MaTaxRepo;
        private readonly ItemRepository _ItemRepo;

        public EntryFreeListBusiness()
        {
            _EntryFreeListRepo = new EntryFreeListRepository();
            _ItemSupplierRepo = new ItemSupplierRepository();
            _PurchaseBAseCostBusiness = new PurchaseBaseCostBusiness();
            _MaTaxRepo = new MaTaxRepository();
            _ItemRepo = new ItemRepository();
        }

        public void PostEntryFreeList(List<EntryFreeListModel> EntryFree, int EntryFreeId, string User, int supplier)
        {
            int position = 0;
            foreach (var item in EntryFree)
            {
                var Price = _ItemSupplierRepo.GetItemSupplierByItemAndSupplier(item.Parth_Number, supplier);
                var PurchaseBaseCost = _PurchaseBAseCostBusiness.getPurchaseBaseCostBySupplierPartNumber(Price.supplier_item_id);
                item.base_cost = PurchaseBaseCost.base_cost;
                var Tax = _ItemRepo.GetOneItem(item.Parth_Number);
                var Ieps = _MaTaxRepo.GetTaxValue(Tax.IEPSDescription);
                var Iva = _MaTaxRepo.GetTaxValue(Tax.IVADescription);

                if (Ieps != null)
                    item.IEPS = item.base_cost * (Ieps.tax_value / 100);
                else
                    item.IEPS = 0;

                if (Iva != null)
                    item.IVA = (item.base_cost + item.IEPS) * (Iva.tax_value / 100);
                else
                    item.IVA = 0;
                _EntryFreeListRepo.PostEntryFreeList(item, EntryFreeId, position, User);
                position++;
            }
        }
    }
}