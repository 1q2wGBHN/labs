﻿using App.DAL.EntryFree;
using App.DAL.Site;
using App.DAL.Supplier;
using App.Entities.ViewModels.EntryFree;
using App.Entities.ViewModels.PurchaseOrder;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;

namespace App.BLL.EntryFree
{
    public class EntryFreeHeadBusiness
    {
        private readonly EntryFreeHeadRepository _EntryFreeHeadRepo;
        private readonly MaSupplierContactRepository _MaSupplierContactRepo;
        private readonly UserMasterBusiness _UserMaterBusiness;
        private readonly SiteConfigRepository _SiteConfigRepo;

        public EntryFreeHeadBusiness()
        {
            _EntryFreeHeadRepo = new EntryFreeHeadRepository();
            _MaSupplierContactRepo = new MaSupplierContactRepository();
            _UserMaterBusiness = new UserMasterBusiness();
            _SiteConfigRepo = new SiteConfigRepository();
        }

        public Tuple<int, List<Entities.ViewModels.Android.EntryFreeListModel>> PostEntryFreeHead(int Supplier, string Type, string SubType, string Reference, string Commentary, string User, string EntryFree)
        {
            if (Supplier > 0 && !string.IsNullOrWhiteSpace(Type) && !string.IsNullOrWhiteSpace(SubType) && !string.IsNullOrWhiteSpace(User))
            {
                string v = "{\"Entry\":" + EntryFree + "}";
                var result = JsonConvert.DeserializeObject<Entities.ViewModels.Android.EntryFreeListModel>(v);
                List<Entities.ViewModels.Android.EntryFreeListModel> list = new List<Entities.ViewModels.Android.EntryFreeListModel>();
                for (int i = 0; i < result.Entry.Count; i++)
                {
                    Entities.ViewModels.Android.EntryFreeListModel model = new Entities.ViewModels.Android.EntryFreeListModel
                    {
                        Parth_Number = result.Entry[i].Parth_Number.ToString(),
                        Quantity = result.Entry[i].Quantity
                    };
                    list.Add(model);
                }
                return Tuple.Create(_EntryFreeHeadRepo.PostEntryFreeHead(Supplier, Type, SubType, Reference, Commentary, User), list);
            }
            else
            {
                List<Entities.ViewModels.Android.EntryFreeListModel> listaError = new List<Entities.ViewModels.Android.EntryFreeListModel>();
                return Tuple.Create(0, listaError);
            }
        }

        public EntryFreeModel GetEntryFree(int EntryFreeId)
        {
            var EntryFree = _EntryFreeHeadRepo.GetEntryFree(EntryFreeId);
            if (EntryFree != null)
            {
                EntryFree.Phone = _MaSupplierContactRepo.Phone(EntryFree.SupplierId, "Compras");
                var user = _UserMaterBusiness.GetUserMasterByUsername(EntryFree.User);

                if (user != null)
                    EntryFree.UserName = user.first_name + " " + user.last_name;

                EntryFree.Emailpurchases = _MaSupplierContactRepo.Email(EntryFree.SupplierId, "Compras");
                EntryFree.EmailSale = _MaSupplierContactRepo.Email(EntryFree.SupplierId, "Ventas");
                EntryFree.EmailSale = _MaSupplierContactRepo.EmailsGeneric(EntryFree.SupplierId, "Contabilidad");
                if (EntryFree.EmailSale == "")
                    EntryFree.EmailSale = _MaSupplierContactRepo.EmailsGeneric(EntryFree.SupplierId, "Ventas");
                var Site = _SiteConfigRepo.GetSiteCodeAddress();
                EntryFree.Site = Site.SiteName;
                EntryFree.AddressSite = Site.SiteAddress;
            }
            return EntryFree;
        }

        public EntryFreeModel NewGetEntryFree(int EntryFreeId)
        {
            var EntryFree = _EntryFreeHeadRepo.NewGetEntryFree(EntryFreeId);
            if (EntryFree != null)
            {
                EntryFree.Phone = _MaSupplierContactRepo.Phone(EntryFree.SupplierId, "Compras");
                var user = _UserMaterBusiness.GetUserMasterByUsername(EntryFree.User);

                if (user != null)
                    EntryFree.UserName = user.first_name + " " + user.last_name;

                EntryFree.Emailpurchases = _MaSupplierContactRepo.Email(EntryFree.SupplierId, "Compras");
                EntryFree.EmailSale = _MaSupplierContactRepo.Email(EntryFree.SupplierId, "Ventas");
                var Site = _SiteConfigRepo.GetSiteCodeAddress();
                EntryFree.Site = Site.SiteName;
                EntryFree.AddressSite = Site.SiteAddress;
            }
            return EntryFree;
        }

        public Tuple<int, EntryFreeModel> NewgetEntryFree(int EntryFreeId)
        {
            var EntryFree = _EntryFreeHeadRepo.GetEntryFree(EntryFreeId);
            if (EntryFree != null)
            {
                EntryFree.Phone = _MaSupplierContactRepo.Phone(EntryFree.SupplierId, "Compras");
                var user = _UserMaterBusiness.GetUserMasterByUsername(EntryFree.User);

                if (user != null)
                    EntryFree.UserName = user.first_name + " " + user.last_name;

                EntryFree.Emailpurchases = _MaSupplierContactRepo.Email(EntryFree.SupplierId, "Compras");
                EntryFree.EmailSale = _MaSupplierContactRepo.Email(EntryFree.SupplierId, "Ventas");
                var Site = _SiteConfigRepo.GetSiteCodeAddress();
                EntryFree.Site = Site.SiteName;
                EntryFree.AddressSite = Site.SiteAddress;
            }
            var StoreProcedure = _EntryFreeHeadRepo.StoreProcedureEntryFree(EntryFreeId, EntryFree.User);
            return new Tuple<int, EntryFreeModel>(StoreProcedure.ReturnValue, EntryFree);
        }

        public List<EntryFreeListModel> GetEntryFreeReport(int EntryFreeId, int supplier) => _EntryFreeHeadRepo.GetEntryFreeReport(EntryFreeId);

        public bool PutEntryFree(int EntryFreeId, string User) => _EntryFreeHeadRepo.PutEntryFree(EntryFreeId, User);

        public bool NewPutEntryFree(int EntryFreeId, string User) => _EntryFreeHeadRepo.NewPutEntryFree(EntryFreeId, User);

        public List<EntryFreeModel> GetFoliosBySupplierAndFechaRange(string supplierId, string fechaIni, string fechaFin)
        {
            int idSup = string.IsNullOrWhiteSpace(supplierId.Trim()) ? 0 : int.Parse(supplierId);
            DateTime fechaInicial = DateTime.Parse(fechaIni);
            DateTime fechaFinal = DateTime.Parse(fechaFin);
            if (idSup > 0)
                return _EntryFreeHeadRepo.GetFoliosBySupplierAndFechaRange(idSup, fechaInicial.Date, fechaFinal.Date);

            return _EntryFreeHeadRepo.GetFoliosBySupplierAndFechaRange(fechaInicial.Date, fechaFinal.Date);
        }

        public List<EntryFreeModel> GetAllEntryFree(DateTime initial, DateTime final) => _EntryFreeHeadRepo.GetAllEntryFree(initial, final);

        public List<EntryFreeModel> GetAllEntryFreeOptions(DateTime? initial, DateTime? final, int supplier, int status, string deparment, string family, string currency, string part_number) => _EntryFreeHeadRepo.GetAllEntryFreeOptions(initial, final, supplier, status, deparment, family, currency, part_number);

        public List<PurchaseOrderItemDetailReportGeneral> EntryFreeListDetail(DateTime? date1, DateTime? date2, int status, int supp, string partNumber, string department, string family, string currency)
        {
            DateTime date = DateTime.Today;
            var firstDayOfMonth = new DateTime(date.Year, date.Month, 1);
            var lastDayOfMonth = firstDayOfMonth.AddMonths(1).AddDays(-1);
            DateTime BeginDate2 = date1 ?? firstDayOfMonth;
            DateTime EndDate2 = date2 ?? lastDayOfMonth;
            return _EntryFreeHeadRepo.EntryFreeListDetail(BeginDate2, EndDate2, status, supp, partNumber, department, family, currency);
        }

        public int UpdateEntryFree(int EntryFreeId, string User)
        {
            var EntryNumber = _EntryFreeHeadRepo.GetTypeEntryFree(EntryFreeId);
            if (EntryNumber == 0)
            {
                var StoreProcedure = _EntryFreeHeadRepo.StoreProcedureEntryFree(EntryFreeId, User);
                return StoreProcedure.ReturnValue;
            }
            else if (EntryNumber == 1)
                return _EntryFreeHeadRepo.UpdateEntryFree(EntryFreeId, User);
            else if (EntryNumber == 1002)
                return 1002;

            return 80010;
        }

        public int NewUpdateEntryFree(int EntryFreeId, string User)
        {
            var EntryNumber = _EntryFreeHeadRepo.GetTypeEntryFree(EntryFreeId);
            if (EntryNumber == 0)
            {
                var StoreProcedure = _EntryFreeHeadRepo.StoreProcedureEntryFree(EntryFreeId, User);
                return StoreProcedure.ReturnValue;
            }
            else if (EntryNumber == 1)
                return _EntryFreeHeadRepo.NewUpdateEntryFree(EntryFreeId, User);
            else if (EntryNumber == 1002)
                return 1002;

            return 80010;
        }

        public EntryFreeModel GetEntryFreeReport(int EntryFreeId)
        {
            var EntryFree = _EntryFreeHeadRepo.GetEntryFreeReport2(EntryFreeId);
            if (EntryFree != null)
            {
                EntryFree.Phone = _MaSupplierContactRepo.Phone(EntryFree.SupplierId, "Compras");
                var user = _UserMaterBusiness.GetUserMasterByUsername(EntryFree.User);

                if (user != null)
                    EntryFree.UserName = user.first_name + " " + user.last_name;

                EntryFree.Emailpurchases = _MaSupplierContactRepo.Email(EntryFree.SupplierId, "Compras");
                EntryFree.EmailSale = _MaSupplierContactRepo.Email(EntryFree.SupplierId, "Ventas");
                var Site = _SiteConfigRepo.GetSiteCodeAddress();
                EntryFree.Site = Site.SiteName;
                EntryFree.AddressSite = Site.SiteAddress;
            }
            return EntryFree;
        }

        public bool EntryFreeEditStatusPrint(int Document, int Status, string program_id) => _EntryFreeHeadRepo.EntryFreeEditStatusPrint(Document, Status, program_id);
    }
}