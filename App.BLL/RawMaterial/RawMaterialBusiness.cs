﻿using App.DAL.Item;
using App.DAL.RawMaterial;
using App.DAL.Site;
using App.Entities;
using App.Entities.ViewModels.Android;
using App.Entities.ViewModels.RawMaterial;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;

namespace App.BLL.RawMaterial
{
    public class RawMaterialBusiness
    {
        private readonly RawMaterialRepository _rawMaterialRepository;
        private readonly ItemRepository _ItemRepository;
        private readonly SiteConfigRepository _siteConfigRepository;

        public RawMaterialBusiness()
        {
            _rawMaterialRepository = new RawMaterialRepository();
            _ItemRepository = new ItemRepository();
            _siteConfigRepository = new SiteConfigRepository();
        }

        public Tuple<string, List<RawMaterialHeaderAndroid>> SendItemsToRawMaterial(string items, string usuario, string site_code)
        {
            string v = "{\"Items\":" + items + "}";
            var result = JsonConvert.DeserializeObject<RawMaterialHeaderAndroid>(v);
            var gr_storage_location = "";
            List<RawMaterialHeaderAndroid> list = new List<RawMaterialHeaderAndroid>();
            for (int i = 0; i < result.Items.Count; i++)
            {
                RawMaterialHeaderAndroid model = new RawMaterialHeaderAndroid
                {
                    part_number = result.Items[i].part_number,
                    quantity = result.Items[i].quantity,
                    part_description = result.Items[i].part_description
                };
                gr_storage_location = result.Items[i].gr_storage_location;
                list.Add(model);
            }
            try
            {
                RAW_MATERIAL_HEADER header = new RAW_MATERIAL_HEADER
                {
                    site_code = site_code,
                    transfer_date = DateTime.Now,
                    transfer_status = 0,
                    cuser = usuario,
                    cdate = DateTime.Now,
                    program_id = "AndroidRM",
                    comment = "",
                    gr_storage_location = gr_storage_location
                };
                var id = _rawMaterialRepository.AddRawMaterialHeader(header);
                if (id != 0)
                {
                    for (int i = 0; i < list.Count; i++)
                    {
                        RAW_MATERIAL_DETAIL rawMaterialItem = new RAW_MATERIAL_DETAIL();
                        var baseCost = _ItemRepository.GetMapPrice(list[i].part_number);
                        decimal? subTotal;
                        rawMaterialItem.folio = id;
                        rawMaterialItem.part_number = list[i].part_number;
                        rawMaterialItem.quantity = list[i].quantity;
                        rawMaterialItem.cuser = usuario;
                        rawMaterialItem.cdate = DateTime.Now;
                        rawMaterialItem.program_id = "AndroidRM";
                        rawMaterialItem.cost = baseCost;
                        subTotal = rawMaterialItem.quantity * rawMaterialItem.cost;
                        var Tax = _ItemRepository.StoreProcedureIvaIeps(list[i].part_number);
                        var Ieps = Tax.Ieps;
                        var Iva = Tax.Iva;
                        rawMaterialItem.ieps = subTotal * (Ieps / 100);
                        rawMaterialItem.iva = (subTotal + rawMaterialItem.ieps) * (Iva / 100);
                        rawMaterialItem.transfer_status = 0;
                        _rawMaterialRepository.AddRawMaterialDetail(rawMaterialItem);
                    }
                    return Tuple.Create(id.ToString(), list);
                }
                else
                {
                    List<RawMaterialHeaderAndroid> listaError = new List<RawMaterialHeaderAndroid>();
                    return Tuple.Create("ERROR HEADER", listaError);
                }
            }
            catch (Exception ex)
            {
                List<RawMaterialHeaderAndroid> listaError = new List<RawMaterialHeaderAndroid>();
                var msg = ex.Message;
                return Tuple.Create("", listaError);
            }
        }

        public List<RawMaterialHeader> GetAllRawMaterialHeader(int Status, DateTime DateInit, DateTime DateFin) => _rawMaterialRepository.GetAllRawMaterialHeader(Status, DateInit, DateFin);

        public List<RawMaterialDetail> GetAllRawMaterialDetail(int folio)
        {
            var rawMaterial = _rawMaterialRepository.GetAllRawMaterialDetail(folio);
            foreach (var item in rawMaterial)
            {
                var Tax = _ItemRepository.StoreProcedureIvaIeps(item.Part_Number);
                var Ieps = Tax.Ieps;
                var Iva = Tax.Iva;
                item.IEPS = item.SubTotal * (Ieps / 100);
                item.Iva = (item.SubTotal + item.IEPS) * (Iva / 100);
            }
            return rawMaterial;
        }

        public List<RawMaterialDetail> GetAllRawMaterialDetail(int folio, int status)
        {
            var rawMaterial = _rawMaterialRepository.GetAllRawMaterialDetail(folio, status);
            foreach (var item in rawMaterial)
            {
                var Tax = _ItemRepository.StoreProcedureIvaIeps(item.Part_Number);
                var Ieps = Tax.Ieps;
                var Iva = Tax.Iva;
                item.IEPS = item.SubTotal * (Ieps / 100);
                item.Iva = (item.SubTotal + item.IEPS) * (Iva / 100);
            }
            return rawMaterial;
        }

        public List<RawMaterialDetail> GetAllRawMaterialDetailByDates(DateTime BeginDate, DateTime EndDate, string status, string department, string family, string location) => _rawMaterialRepository.GetAllRawMaterialDetailByDates(BeginDate, EndDate, status, department, family, location);

        //RA = Receipt Aproval / Aprobacion recibo
        public bool RawMaterialEditStatusRA(int folio, string User, string comment) => _rawMaterialRepository.RawMaterialEditStatusRA(folio, User, comment);

        public bool RawMaterialEditStatusRA(List<RawMaterialDetail> model, int folio, string User, string comment) => _rawMaterialRepository.RawMaterialEditStatusRA(model, folio, User, comment);

        //RC = Receipt Cancel / Cancelacion recibo
        public bool RawMaterialEditStatusRC(int folio, string User, string comment) => _rawMaterialRepository.RawMaterialEditStatusRC(folio, User, comment);

        //MC = Management Cancel / Cancelacion gerencia
        public bool RawMaterialEditStatusMC(int folio, string User, string comment) => _rawMaterialRepository.RawMaterialEditStatusMC(folio, User, comment);

        public int RawMaterialEditStatusMA(int folio, string User, string comment)
        {
            var returnValue = _rawMaterialRepository.RawMaterialProcedure(folio, User).ReturnValue;
            if (returnValue == 0)
            {
                if (_rawMaterialRepository.RawMaterialEditStatusMA(folio, User, comment))
                    return returnValue;
                else
                    return 1000;
            }
            else
                return returnValue;
        }

        public int RawMaterialEditStatusMA(List<RawMaterialDetail> model, int folio, string User, string comment)
        {
            if (_rawMaterialRepository.RawMaterialEditStatusMA(model, folio, User, comment))
            {
                var returnValue = _rawMaterialRepository.RawMaterialProcedure(folio, User).ReturnValue;
                if (returnValue == 0)
                {
                    var siteConfig = _siteConfigRepository.GetSiteCode();

                    var returnTrue = _rawMaterialRepository.RawMaterialGIProcedure(folio, User);

                    if (returnTrue != null)
                    {
                        return 0; // Si todo bien regresa 0
                    }
                    else
                    {
                        return 1;
                    }
                }
                else
                {
                    return 1;
                }
            }
            else
                return 1000;
        }

        public List<RawMaterialHeader> GetAllTransferToRaw(DateTime initial, DateTime finish, string status, string location) => _rawMaterialRepository.GetAllTransfersToRaw(initial, finish, status, location);

        public RawMaterialHeader GetRawByFolio(int folio)
        {
            RawMaterialHeader raw = _rawMaterialRepository.GetRawHeaderByFolio(folio);
            raw.listRaw = _rawMaterialRepository.GetAllRawMaterialDetail(folio);
            return raw;
        }

        public RawMaterialHeader GetRawByFolioByStatus(int folio, int status)
        {
            RawMaterialHeader raw = _rawMaterialRepository.GetRawHeaderByFolio(folio);
            raw.listRaw = _rawMaterialRepository.GetAllRawMaterialDetail(folio, status);
            return raw;
        }

        public RawMaterialHeader NewGetRawByFolio(int folio)
        {
            RawMaterialHeader raw = _rawMaterialRepository.GetRawHeaderByFolio(folio);
            raw.listRaw = _rawMaterialRepository.GetAllRawMaterialDetail(folio, raw.Status);
            foreach (var item in raw.listRaw)
            {
                var Tax = _ItemRepository.StoreProcedureIvaIeps(item.Part_Number);
                var Ieps = Tax.Ieps;
                var Iva = Tax.Iva;
                item.IEPS = item.SubTotal * (Ieps / 100);
                item.Iva = (item.SubTotal + item.IEPS) * (Iva / 100);
            }
            return raw;
        }

        public bool DamagedsGoodsEditStatusPrint(int folio, int Status, string program_id) => _rawMaterialRepository.DamagedsGoodsEditStatusPrint(folio, Status, program_id);
    }
}