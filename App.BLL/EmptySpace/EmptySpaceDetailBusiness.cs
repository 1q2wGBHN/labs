﻿using App.BLL.Item;
using App.DAL.EmptySpace;
using App.Entities;
using App.Entities.ViewModels.Android;
using App.Entities.ViewModels.Item;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;

namespace App.BLL.EmptySpace
{
    public class EmptySpaceDetailBusiness
    {
        private readonly EmptySpaceScanDetailRepository _emptySpaceScanDetailRespository;
        private readonly EmptySpaceBusiness _emptySpaceBusiness;
        private readonly ItemBusiness _itemBusiness;

        public EmptySpaceDetailBusiness()
        {
            _emptySpaceScanDetailRespository = new EmptySpaceScanDetailRepository();
            _emptySpaceBusiness = new EmptySpaceBusiness();
            _itemBusiness = new ItemBusiness();
        }

        public List<int> CreateEmptySpaceScanDetail(string items, string username)
        {
            int incidence;
            int incidenceDetial;
            int id_empty_space_scan_new = 0;
            int id_empty_space_scan_old = 0;
            List<int> list = new List<int>();
            string v = "{\"Items\":" + items + "}";
            var result = JsonConvert.DeserializeObject<EmptySpaceScanModel>(v);

            foreach (var item in result.Items)
            {
                var newItems = _itemBusiness.GetInfoItemByPartNumber(item.part_number);

                if (item.id_empty_space != 0)
                {

                    if (id_empty_space_scan_old != item.id_empty_space * -1)
                    {
                        id_empty_space_scan_old = item.id_empty_space * -1;
                        list.Add(id_empty_space_scan_old);
                    }
                    var info = _emptySpaceBusiness.GetEmptySpaceById(item.id_empty_space);
                    var infoDetail = _emptySpaceScanDetailRespository.GetEmptySpaceScanDetailByPartNumberId(item.id_empty_space, item.part_number, item.planogram);
                    incidence = _emptySpaceBusiness.UpdateEmptySpaceIncidence(info, username);
                    incidenceDetial = _emptySpaceScanDetailRespository.UpdateEmptySpaceScanDetailIncidence(infoDetail, username);
                    if (incidence == 0 || incidenceDetial == 0)
                        return null;
                }
                else
                {
                    if (id_empty_space_scan_new == 0)
                    {
                        id_empty_space_scan_new = _emptySpaceBusiness.CreateEmptySpace(username, "AndroidESS");
                        list.Add(id_empty_space_scan_new);
                    }
                    string part_desc = _itemBusiness.PartDescription(item.part_number);
                    var last_e = _itemBusiness.GetLastEntryByPartNumber(item.part_number);
                    ItemEmptySpaceScanModel Items = new ItemEmptySpaceScanModel
                    {
                        id_empty_space = id_empty_space_scan_new,
                        part_number = item.part_number,
                        description = part_desc,
                        planogram = item.planogram
                    };

                    if (newItems != null)
                        Items.stock_existing = newItems.stock_existing;
                    else
                        Items.stock_existing = 0;

                    if (last_e != null)
                    {
                        Items.last_entry = last_e.last_entry;
                        Items.last_entry_quantity = last_e.last_entry_quantity;
                    }
                    else
                    {
                        Items.last_entry = null;
                        Items.last_entry_quantity = 0;
                    }

                    Items.average_sale = _itemBusiness.getAveraSalebyPartNumber(item.part_number);
                    Items.cuser = username;
                    Items.cdate = DateTime.Now;
                    Items.program_id = "AndroidESS";
                    int res = _emptySpaceScanDetailRespository.CreateEmptySpaceScanDetail(Items);
                    if (res == 0)
                        return null;
                }
            }
            return list;
        }

        public int UpdateEmptySpaceScanDetail(EMPTY_SPACE_SCAN_DETAIL EmptySpaceScanDetailModel, string uuser) => _emptySpaceScanDetailRespository.UpdateEmptySpaceScanDetail(EmptySpaceScanDetailModel, uuser);

        public ItemEmptySpaceScanModel GetInfoEmptyByPartNumber(string part_number, string planogram) => _emptySpaceScanDetailRespository.GetInfoEmptyByPartNumber(part_number, planogram);

        public int UpdateEmptySpaceScanDetailIncidence(EMPTY_SPACE_SCAN_DETAIL EmptySpaceScanDetailModel, string uuser) => _emptySpaceScanDetailRespository.UpdateEmptySpaceScanDetailIncidence(EmptySpaceScanDetailModel, uuser);

        public EMPTY_SPACE_SCAN_DETAIL GetEmptySpaceScanDetailByPartNumberId(int id_empty_space, string part_number, string planogram) => _emptySpaceScanDetailRespository.GetEmptySpaceScanDetailByPartNumberId(id_empty_space, part_number, planogram);

        public List<ItemEmptySpaceScanModel> GetAllItemsByIdEmptySpace(int id_empty_space) => _emptySpaceScanDetailRespository.GetAllItemsByIdEmptySpace(id_empty_space);

        public List<ItemEmptySpaceScanModel> GetEmptySpaceScanHistoryByUser(string user) => _emptySpaceScanDetailRespository.GetEmptySpaceScanHistoryByUser(user);
    }
}