﻿using App.DAL.EmptySpace;
using App.Entities;
using App.Entities.ViewModels.EmptySpaceScan;
using System;
using System.Collections.Generic;

namespace App.BLL.EmptySpace
{
    public class EmptySpaceBusiness
    {
        private readonly EmptySpaceScanRepository _emptySpaceRepo;

        public EmptySpaceBusiness()
        {
            _emptySpaceRepo = new EmptySpaceScanRepository();
        }

        public int CreateEmptySpace(string cuser, string program_id) => _emptySpaceRepo.CreateEmptySpace(cuser, program_id);

        public int UpdateEmptySpaceIncidence(EMPTY_SPACE_SCAN infoEmpty, string uuser) => _emptySpaceRepo.UpdateEmptySpaceIncidence(infoEmpty, uuser);

        public EMPTY_SPACE_SCAN GetEmptySpaceById(int id_empty_space) => _emptySpaceRepo.GetEmptySpaceById(id_empty_space);

        public int UpdateEmptySpace(EMPTY_SPACE_SCAN infoEmpty, string uuser) => _emptySpaceRepo.UpdateEmptySpace(infoEmpty, uuser);

        public List<EmptySpaceScanModel> GetEmptySpaceByDates(string fechaIni, string fechaFin)
        {
            DateTime fechaInicial;
            DateTime fechaFinal;
            List<EmptySpaceScanModel> listHeaders;
            if (fechaIni == "" && fechaFin == "")
            {
                fechaInicial = DateTime.Parse(DateTime.Now.AddDays(-7).ToString());
                fechaFinal = DateTime.Parse(DateTime.Now.ToString());
                listHeaders = _emptySpaceRepo.GetEmptySpaceByDates(fechaInicial.Date, fechaFinal.Date);
            }
            else
            {
                fechaInicial = DateTime.Parse(fechaIni);
                fechaFinal = DateTime.Parse(fechaFin);
                listHeaders = _emptySpaceRepo.GetEmptySpaceByDates(fechaInicial.Date, fechaFinal.Date);
            }
            if (listHeaders.Count > 0)
                return listHeaders;
            else
                return listHeaders;
        }
    }
}