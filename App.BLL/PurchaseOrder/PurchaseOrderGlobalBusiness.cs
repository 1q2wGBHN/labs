﻿using App.DAL.Expenses;
using App.DAL.PurchaseOrder;
using App.DAL.Site;
using App.DAL.Supplier;
using App.Entities.ViewModels.Expenses;
using App.Entities.ViewModels.PurchaseOrder;
using App.GlobalEntities;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Xml.Linq;

namespace App.BLL.PurchaseOrder
{
    public class errors
    {
        public string clase { get; set; }
        public bool pass { get; set; }
        public int expense_id { get; set; }
        public string supplier_name { get; set; }
        public string error { get; set; }
    }

    public class PurchaseOrderGlobalBusiness
    {
        private readonly PurchaseOrderGlobalRepository _purchaseOrderGlobalRepository;
        private readonly SiteConfigRepository _siteConfigRepository;
        private readonly MaSupplierRepository _maSupplierRepository;
        private readonly ExpensesRepository _expensesRepository;

        public PurchaseOrderGlobalBusiness()
        {
            _purchaseOrderGlobalRepository = new PurchaseOrderGlobalRepository();
            _siteConfigRepository = new SiteConfigRepository();
            _maSupplierRepository = new MaSupplierRepository();
            _expensesRepository = new ExpensesRepository();
        }

        public List<PurchaseOrderGlobalModel> GetPurchaseOrder(DateTime start, DateTime finish, string site) => _purchaseOrderGlobalRepository.GetPurchaseOrder(start, finish, site);

        public List<PurchaseOrderGlobalModel> GetPurchaseOrderByStatus(DateTime start, DateTime finish, int status, string site) => _purchaseOrderGlobalRepository.GetPurchaseOrderByStatus(start, finish, status, site);

        public List<PurchaseOrderGlobalModel> GetDetailOrder(int purchase_no) => _purchaseOrderGlobalRepository.GetDetailOrder(purchase_no);

        public errors ActionAddXml(List<PurchaseOrderGlobalModel> listXML, string user, int id)
        {
            try
            {

                var sitecode = _siteConfigRepository.GetSiteCode();
                var expense = new EXPENSE
                {
                    site_code = sitecode,
                    remark = "",
                    reference_type = "INTERNO",
                    cdate = DateTime.Now,
                    cuser = user,
                    program_id = "PURC008.cshtml"
                };
                var xml_header_list = new List<XML_HEADER>();
                foreach (var file in listXML)
                {
                    var index = 0;
                    var flag = 0;
                    var xml = Encoding.UTF8.GetString(Convert.FromBase64String(file.xml));
                    var doc = XDocument.Parse(xml);
                    var root = doc.Root;
                    var space = root?.Name.NamespaceName;
                    var rfc = (string)root?.Element(XName.Get("Emisor", space))?.Attribute("Rfc");
                    if (rfc == null)
                    {
                        rfc = (string)root?.Element(XName.Get("Emisor", space))?.Attribute("rfc");
                        if (rfc == null)
                        {
                            rfc = (string)root?.Element(XName.Get("Emisor", space))?.Attribute("RFC");
                        }
                    }

                    var date = (DateTime?)root?.Attribute("Fecha");
                    if (date == null)
                    {
                        date = (DateTime?)root?.Attribute("fecha");
                        if (date == null)
                        {
                            date = (DateTime?)root?.Attribute("FECHA");
                        }
                    }
                    var sup = _maSupplierRepository.GetSupplierByRFC(rfc);

                    if (sup.Count == 0)
                        return new errors() { pass = false, error = "No existe el proveedor en el XML: " + file.xml_name, clase = "Proveedor" };
                    else
                    {
                        for (var i = 0; i < sup.Count; i++)
                        {
                            index = i;

                            if (sup[i].supplier_id == 0)
                                return new errors() { pass = false, error = "No existe el proveedor en el XML: " + file.xml_name, clase = "Proveedor" };

                            if (sup[i].supplier_type != "Acreedor")
                                flag++;

                            if (sup[i].supplier_type == "Acreedor")
                                if (!_expensesRepository.ValidateSupplier(id, sup[i].supplier_id))
                                    return new errors() { pass = false, error = "El proveedor " + (sup.Any() ? sup[index].commercial_name : "") + " no corresponde a la requisición " + file.xml_name, supplier_name = sup[index].commercial_name, clase = "Acreedor" };
                        }
                        if (flag == sup.Count)
                            return new errors() { pass = false, error = "El proveedor " + (sup.Any() ? sup[index].commercial_name : "") + " no esta como acreedor en el archivo " + file.xml_name, supplier_name = sup[index].commercial_name, clase = "Acreedor" };
                    }

                    var rfcSpace = XNamespace.Get("http://www.sat.gob.mx/TimbreFiscalDigital").NamespaceName;
                    var uuid = root?
                        .Descendants(XName.Get("TimbreFiscalDigital", rfcSpace)).FirstOrDefault()?
                        .Attribute("UUID");
                    var currency = (string)root?.Attribute("Moneda") ?? "";
                    var iepsTotal = 0M;
                    var ivaTotal = 0M;
                    var isr_retainedTotal = 0M;
                    var iva_retainedTotal = 0M;


                    if (_expensesRepository.ValidateInvoiceExpense((string)root?.Attribute("Folio"), sup[index].supplier_id))
                        return new errors() { pass = false, error = "El folio " + (string)root?.Attribute("Folio") + " ya se encuentra registrado con este mismo proveedor.", clase = "Acreedor" };


                    if (!_expensesRepository.ValidateFile(uuid.Value)) //Validar si ya fue registrado
                        return new errors() { pass = false, error = "El archivo xml " + file.xml_name + " ya fue registrado anteriormente.", clase = "Acreedor" };

                    try
                    {
                        iepsTotal = (decimal?)root.Element(XName.Get("Impuestos", space)).Element(XName.Get("Traslados", space)).Descendants(XName.Get("Traslado", space)).Where(a => (int)a.Attribute("Impuesto") == 3).Select(e => (decimal?)e.Attribute("Importe") ?? 0).FirstOrDefault() ?? 0;
                    }
                    catch (Exception ex)
                    {
                        string msg = ex.Message;
                        iepsTotal = 0;
                    }
                    try
                    {
                        isr_retainedTotal = (decimal?)root.Element(XName.Get("Impuestos", space)).Element(XName.Get("Retenciones", space)).Descendants(XName.Get("Retencion", space)).Where(a => (int)a.Attribute("Impuesto") == 1).Select(e => (decimal?)e.Attribute("Importe") ?? 0).FirstOrDefault() ?? 0;
                    }
                    catch (Exception ex)
                    {
                        string msg = ex.Message;
                        isr_retainedTotal = 0;
                    }
                    try
                    {
                        iva_retainedTotal = (decimal?)root.Element(XName.Get("Impuestos", space)).Element(XName.Get("Retenciones", space)).Descendants(XName.Get("Retencion", space)).Where(a => (int)a.Attribute("Impuesto") == 2).Select(e => (decimal?)e.Attribute("Importe") ?? 0).FirstOrDefault() ?? 0;
                    }
                    catch (Exception ex)
                    {
                        string msg = ex.Message;
                        iva_retainedTotal = 0;
                    }
                    try
                    {
                        ivaTotal = (decimal?)root.Element(XName.Get("Impuestos", space)).Element(XName.Get("Traslados", space)).Descendants(XName.Get("Traslado", space)).Where(a => (int)a.Attribute("Impuesto") == 2).Select(e => (decimal?)e.Attribute("Importe") ?? 0).FirstOrDefault() ?? 0;
                    }
                    catch (Exception ex)
                    {
                        string msg = ex.Message;
                        ivaTotal = 0;
                    }
                    var xml_header = new XML_HEADER
                    {
                        uuid_invoice = (string)uuid,
                        invoice_no = (string)root?.Attribute("Folio"),
                        invoice_date = date,
                        supplier_id = sup[index].supplier_id,
                        currency = currency,
                        iva = ivaTotal,
                        ieps = iepsTotal,
                        isr_retained = isr_retainedTotal,
                        iva_retained = iva_retainedTotal,
                        subtotal = (decimal?)root?.Attribute("SubTotal") ?? 0,
                        discount = (decimal?)root?.Attribute("Descuento") ?? 0,
                        total_amount = (decimal?)root?.Attribute("Total") ?? 0,
                        xml = file.xml,
                        xml_name = file.xml_name,
                        cdate = DateTime.Now,
                        cuser = user,
                        program_id = "MRO001.cshtml",
                        invoice_type = (string)root?.Attribute("TipoDeComprobante"),
                        exchange_currency = (decimal?)root?.Attribute("TipoCambio"),
                        payment_method = (string)root?.Attribute("MetodoPago"),
                        payment_way = (string)root?.Attribute("FormaPago"),
                        version = (string)root?.Attribute("Version")
                    };


                    xml_header.XML_DETAIL = root?.Descendants(XName.Get("Concepto", space)).Select(e =>
                    {
                        var iva_amount = (decimal?)e.Descendants(XName.Get("Traslado", space)).Where(a => (int)a.Attribute("Impuesto") == 2).Select(a => a.Attribute("Importe")).FirstOrDefault() ?? 0;
                        var ieps_amount = (decimal?)e.Descendants(XName.Get("Traslado", space)).Where(a => (int)a.Attribute("Impuesto") == 3).Select(a => a.Attribute("Importe")).FirstOrDefault() ?? 0;
                        var isr_retained = (decimal?)e.Descendants(XName.Get("Retencion", space)).Where(a => (int)a.Attribute("Impuesto") == 1).Select(a => a.Attribute("Importe")).FirstOrDefault() ?? 0;
                        var iva_retained = (decimal?)e.Descendants(XName.Get("Retencion", space)).Where(a => (int)a.Attribute("Impuesto") == 2).Select(a => a.Attribute("Importe")).FirstOrDefault() ?? 0;
                        return new XML_DETAIL
                        {
                            iva_amount = iva_amount,
                            ieps_amount = ieps_amount,
                            isr_retained = isr_retained,
                            iva_retained = iva_retained,
                            item_no = (string)e.Attribute("NoIdentificacion") ?? "",
                            item_description = (string)e.Attribute("Descripcion") ?? "",
                            quantity = (decimal?)e.Attribute("Cantidad") ?? 0,
                            unit_cost = (decimal?)e.Attribute("ValorUnitario") ?? 0,
                            item_amount = (decimal?)e.Attribute("Importe") ?? 0,
                            discount = (decimal?)e.Attribute("Descuento") ?? 0,
                        };
                    }).ToList();
                    xml_header_list.Add(xml_header);
                }

                var b = _expensesRepository.CreateExpense(expense, xml_header_list);
                if (b == null)
                    return new errors() { pass = false, error = "Error al leer el XML", clase = "xml" };  //error 

                return new errors() { pass = true, expense_id = b.expense_id };
            }
            catch (Exception ex)
            {
                string msg = ex.Message;
                return new errors() { pass = false, error = "Error al leer el XML", clase = "xml" };
            }
        }

        public bool UpdatePurchaseOrder(string user, int expense_id, int id) => _purchaseOrderGlobalRepository.UpdatePurchaseOrder(user, expense_id, id);

        public errors ActionFixedExpenseXmlOnlyInvoice(ExpenseModelFolio model, string user, int id_supplier, string type_expense)
        {
            try
            {

                if (_expensesRepository.ValidateInvoiceExpense(model.invoice.ToUpper(), id_supplier))
                    return new errors() { pass = false, error = "El folio " + model.invoice + " ya se encuentra registrado con este mismo proveedor.", clase = "Acreedor" };


                string typeExpenseOriginal = "";
                string item_no = "";
                if (type_expense == "VARIADO")
                {
                    typeExpenseOriginal = "VARIADO";
                    item_no = "VARIABLE";
                }
                else
                {
                    typeExpenseOriginal = "FIJO";
                    item_no = "FIJO";
                }

                var sitecode = _siteConfigRepository.GetSiteCode();
                var expense = new EXPENSE
                {
                    expense_category = model.area,
                    site_code = sitecode,
                    remark = item_no,
                    reference_type = typeExpenseOriginal,
                    cdate = DateTime.Now,
                    cuser = user,
                    expense_status = 9,
                    program_id = "PURC014.cshtml"
                };

                var xml_header = new XML_HEADER
                {
                    uuid_invoice = "",
                    invoice_no = model.invoice.ToUpper(),
                    invoice_date = DateTime.Today,
                    supplier_id = id_supplier,
                    currency = model.currency,
                    iva = model.iva,
                    ieps = model.ieps,
                    isr_retained = model.isrRetenidos,
                    iva_retained = model.ivaRetenido,
                    subtotal = model.subtotal,
                    discount = model.discount,
                    total_amount = model.total,
                    xml = "",
                    xml_name = "",
                    cdate = DateTime.Now,
                    cuser = user,
                    xml_header_status = 9,
                    program_id = "PURC014.cshtml"
                };

                var xml_detail = new XML_DETAIL
                {
                    item_no = item_no,
                    item_description = typeExpenseOriginal + " " + model.comment,
                    quantity = model.quantity,
                    unit_cost = model.subtotal / model.quantity,
                    item_amount = model.subtotal,
                    discount = model.discount,
                    iva_amount = model.iva,
                    ieps_amount = model.ieps,
                    iva_retained = model.ivaRetenido,
                    isr_retained = model.isrRetenidos
                };

                var result = _expensesRepository.CreateExpenseOneDetail(expense, xml_header, xml_detail);
                if (!result) return new errors() { pass = false, error = "Error al guardar la información.", clase = "xml" };  //error 
                return new errors() { pass = true, expense_id = 0 };
            }
            catch (Exception ex)
            {
                var x = ex.Message;
                return new errors() { pass = false, error = "Error al leer el XML", clase = "xml" };
            }
        }


        public int EditExpenseOneDetail(ExpenseModelFolio model, string user)
        {
            return _expensesRepository.EditExpenseOneDetail(model, user);
        }

        public errors ActionFixedExpenseXml(List<PurchaseOrderGlobalModel> listXML, string user, int id_supplier, string type_expense, string comment)
        {
            try
            {

                string typeExpenseOriginal = "";
                string item_no = "";
                if (type_expense == "VARIADO")
                {
                    typeExpenseOriginal = "VARIADO";
                    item_no = "VARIABLE";
                }
                else
                {
                    typeExpenseOriginal = "FIJO";
                    item_no = "FIJO";
                }

                var sitecode = _siteConfigRepository.GetSiteCode();
                var expense = new EXPENSE
                {
                    expense_category = type_expense,
                    site_code = sitecode,
                    remark = item_no,
                    reference_type = typeExpenseOriginal,
                    cdate = DateTime.Now,
                    cuser = user,
                    program_id = "PURC014.cshtml",
                    expense_status = 9
                };
                var xml_header_list = new List<XML_HEADER>();
                foreach (var file in listXML)
                {
                    var index = 0;
                    var flag = 0;
                    var xml = Encoding.UTF8.GetString(Convert.FromBase64String(file.xml));
                    var doc = XDocument.Parse(xml);
                    var root = doc.Root;
                    var space = root?.Name.NamespaceName;
                    var rfc = (string)root?.Element(XName.Get("Emisor", space))?.Attribute("Rfc");
                    var rfcFlorido = (string)root?.Element(XName.Get("Receptor", space))?.Attribute("Rfc");
                    var date = (DateTime)root?.Attribute("Fecha");
                    var sup = _maSupplierRepository.GetSupplierByRFC(rfc);
                    var info = _maSupplierRepository.GetSupplierInfoById(id_supplier);
                    var Serie = "";
                    if (rfcFlorido.Trim().ToUpper() != ConfigurationManager.AppSettings["RfcFlorido"].ToString())
                        return new errors() { pass = false, error = "Contacte al proveedor, debido a que este archivo XML no va dirigo a DISTRIBUIDORA EL FLORIDO, S.A. DE C.V.", clase = "Proveedor" };

                    if (info.Rfc != rfc)
                        return new errors() { pass = false, error = "El proveedor: '" + info.BusinessName + "' no corresponde al XML subido.", clase = "Proveedor" };

                    if (sup.Count == 0)
                        return new errors() { pass = false, error = "No existe el proveedor en el XML: " + file.xml_name, clase = "Proveedor" };
                    else
                    {
                        for (var i = 0; i < sup.Count; i++)
                        {
                            index = i;
                            if (sup[i].supplier_id == 0) return new errors() { pass = false, error = "No existe el proveedor en el XML: " + file.xml_name, clase = "Proveedor" };
                            if (sup[i].supplier_type != "Acreedor")
                                flag++;
                        }
                        if (flag == sup.Count)
                            return new errors() { pass = false, error = "El proveedor " + (sup.Any() ? sup[index].commercial_name : "") + " no esta como acreedor en el archivo " + file.xml_name, supplier_name = sup[index].commercial_name, clase = "Acreedor" };
                    }
                    string Folio = "";
                    var rfcSpace = XNamespace.Get("http://www.sat.gob.mx/TimbreFiscalDigital").NamespaceName;
                    var FolioSpace = XNamespace.Get("http://www.sat.gob.mx/registrofiscal").NamespaceName;
                    if ((string)root?.Attribute("Serie") != null)
                        Serie = (string)root?.Attribute("Serie");
                    else
                        Serie = (string)root?.Descendants(XName.Get("CFDIRegistroFiscal", FolioSpace)).FirstOrDefault()?.Attribute("Serie") ?? "";

                    var uuid = root?.Descendants(XName.Get("TimbreFiscalDigital", rfcSpace)).FirstOrDefault()?.Attribute("UUID").Value.ToString().ToUpper();
                    if ((string)root?.Attribute("Folio") != null)
                        Folio = (string)root?.Attribute("Folio");
                    else if ((string)root?.Descendants(XName.Get("CFDIRegistroFiscal", FolioSpace)).FirstOrDefault()?.Attribute("Folio") != null)
                        Folio = (string)root?.Descendants(XName.Get("CFDIRegistroFiscal", FolioSpace)).FirstOrDefault()?.Attribute("Folio");
                    else
                        Folio = uuid.Substring(uuid.Length - 6); //Tome lo ultimos digitos del UUID
                    var currency = (string)root?.Attribute("Moneda") ?? "";
                    var iepsTotal = 0M;
                    var ivaTotal = 0M;
                    var isr_retainedTotal = 0M;
                    var iva_retainedTotal = 0M;

                    if (_expensesRepository.ValidateInvoiceExpense(Folio, sup[index].supplier_id))
                            return new errors() { pass = false, error = "El folio " + (string)root?.Attribute("Folio") + " ya se encuentra registrado con este mismo proveedor.", clase = "Acreedor" };


                    if (!_expensesRepository.ValidateFile(uuid))
                        return new errors() { pass = false, error = "El archivo xml " + file.xml_name + " ya fue registrado anteriormente.", clase = "Acreedor" };

                    try
                    {
                        iepsTotal = root.Element(XName.Get("Impuestos", space)).Element(XName.Get("Traslados", space)).Descendants(XName.Get("Traslado", space)).Where(a => (int)a.Attribute("Impuesto") == 3).Select(e => (decimal?)e.Attribute("Importe") ?? 0).ToList().Sum();
                    }
                    catch (Exception ex)
                    {
                        string msg = ex.Message;
                        iepsTotal = 0;
                    }
                    try
                    {
                        isr_retainedTotal = root.Element(XName.Get("Impuestos", space)).Element(XName.Get("Retenciones", space)).Descendants(XName.Get("Retencion", space)).Where(a => (int)a.Attribute("Impuesto") == 1).Select(e => (decimal?)e.Attribute("Importe") ?? 0).ToList().Sum();
                    }
                    catch (Exception ex)
                    {
                        string msg = ex.Message;
                        isr_retainedTotal = 0;
                    }
                    try
                    {
                        iva_retainedTotal = root.Element(XName.Get("Impuestos", space)).Element(XName.Get("Retenciones", space)).Descendants(XName.Get("Retencion", space)).Where(a => (int)a.Attribute("Impuesto") == 2).Select(e => (decimal?)e.Attribute("Importe") ?? 0).ToList().Sum();
                    }
                    catch (Exception ex)
                    {
                        string msg = ex.Message;
                        iva_retainedTotal = 0;
                    }
                    try
                    {
                        ivaTotal = root.Element(XName.Get("Impuestos", space)).Element(XName.Get("Traslados", space)).Descendants(XName.Get("Traslado", space)).Where(a => (int)a.Attribute("Impuesto") == 2).Select(e => (decimal?)e.Attribute("Importe") ?? 0).ToList().Sum();
                    }
                    catch (Exception ex)
                    {
                        string msg = ex.Message;
                        ivaTotal = 0;
                    }

                    var xml_header = new XML_HEADER
                    {
                        xml_header_status = 9,
                        uuid_invoice = (string)uuid,
                        invoice_no = Serie+Folio,
                        invoice_date = date,
                        supplier_id = sup.Where(s=>s.supplier_type == "Acreedor").FirstOrDefault().supplier_id,//sup[index].supplier_id,
                        currency = currency,
                        iva = ivaTotal,
                        ieps = iepsTotal,
                        isr_retained = isr_retainedTotal,
                        iva_retained = iva_retainedTotal,
                        subtotal = (decimal?)root?.Attribute("SubTotal") ?? 0,
                        discount = (decimal?)root?.Attribute("Descuento") ?? 0,
                        total_amount = (decimal?)root?.Attribute("Total") ?? 0,
                        xml = file.xml,
                        xml_name = file.xml_name,
                        cdate = DateTime.Now,
                        cuser = user,
                        program_id = "PURC014.cshtml",
                        invoice_type = (string)root?.Attribute("TipoDeComprobante"),
                        exchange_currency = (decimal?)root?.Attribute("TipoCambio"),
                        payment_method = (string)root?.Attribute("MetodoPago"),
                        payment_way = (string)root?.Attribute("FormaPago"),
                        version = (string)root?.Attribute("Version")
                    };

                    xml_header.XML_DETAIL = root?.Descendants(XName.Get("Concepto", space)).Select(e =>
                    {
                        var iva_amount = (decimal?)e.Descendants(XName.Get("Traslado", space)).Where(a => (int)a.Attribute("Impuesto") == 2).Select(a => a.Attribute("Importe")).FirstOrDefault() ?? 0;
                        var ieps_amount = (decimal?)e.Descendants(XName.Get("Traslado", space)).Where(a => (int)a.Attribute("Impuesto") == 3).Select(a => a.Attribute("Importe")).FirstOrDefault() ?? 0;
                        var isr_retained = (decimal?)e.Descendants(XName.Get("Retencion", space)).Where(a => (int)a.Attribute("Impuesto") == 1).Select(a => a.Attribute("Importe")).FirstOrDefault() ?? 0;
                        var iva_retained = (decimal?)e.Descendants(XName.Get("Retencion", space)).Where(a => (int)a.Attribute("Impuesto") == 2).Select(a => a.Attribute("Importe")).FirstOrDefault() ?? 0;
                        return new XML_DETAIL
                        {
                            iva_amount = iva_amount,
                            ieps_amount = ieps_amount,
                            isr_retained = isr_retained,
                            iva_retained = iva_retained,
                            item_no = (string)e.Attribute("NoIdentificacion") ?? item_no,
                            item_description = comment != "" ? (string)e.Attribute("Descripcion") ?? "" + " (" + comment + ")" : (string)e.Attribute("Descripcion") ?? "",
                            quantity = (decimal?)e.Attribute("Cantidad") ?? 0,
                            unit_cost = (decimal?)e.Attribute("ValorUnitario") ?? 0,
                            item_amount = (decimal?)e.Attribute("Importe") ?? 0,
                            discount = (decimal?)e.Attribute("Descuento") ?? 0,
                        };
                    }).ToList();
                    xml_header_list.Add(xml_header);
                }
                var b = _expensesRepository.CreateExpense(expense, xml_header_list);
                if (b == null) return new errors() { pass = false, error = "Error al leer el XML", clase = "xml" };  //error 
                return new errors() { pass = true, expense_id = b.expense_id };
            }
            catch (Exception ex)
            {
                var x = ex.Message;
                return new errors() { pass = false, error = "Error al leer el XML", clase = "xml" };
            }
        }
    }
}