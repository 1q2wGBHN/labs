﻿using App.BLL.BlindCount;
using App.BLL.Item;
using App.DAL;
using App.DAL.PurchaseBaseCost;
using App.DAL.PurchaseOrder;
using App.Entities;
using App.Entities.ViewModels.PurchaseOrder;
using System;
using System.Collections.Generic;

namespace App.BLL.PurchaseOrder
{
    public class PurchaseOrderItemBusiness
    {
        private readonly PurchaseOrderItemRepository _PurchaseOrderItemRepo;
        private readonly ItemBusiness _itemBusiness;
        private readonly PurchaseBaseCostRepository _purchaseBaseCostRepository;
        private readonly BlindCountBusiness _blindCountBusiness;

        public PurchaseOrderItemBusiness()
        {
            _PurchaseOrderItemRepo = new PurchaseOrderItemRepository();
            _itemBusiness = new ItemBusiness();
            _purchaseBaseCostRepository = new PurchaseBaseCostRepository();
            _blindCountBusiness = new BlindCountBusiness();
        }

        public List<PurchaseOrderItemModel> GetPurchaseOrderItem(string PurchaseNo) => _PurchaseOrderItemRepo.GetPurchaseOrderItem(PurchaseNo);

        public PurchaseOrderModel GetInfoByPurchaseOrderBasic(string PurchaseNo) => _PurchaseOrderItemRepo.GetInfoByPurchaseOrderBasic(PurchaseNo);

        public PurchaseOrderItemOCModel GetPurchaseOrderItemList(string PurchaseNo) => _PurchaseOrderItemRepo.GetPurchaseOrderItemList(PurchaseNo);

        public Tuple<int, string> SupplierInfo(string PurchaseNo) => _PurchaseOrderItemRepo.SupplierInfo(PurchaseNo);

        public StoreProcedureResult StoreProcedureOCRG(string PurchaseNo, string User) => _PurchaseOrderItemRepo.StoreProcedureOCRG(PurchaseNo, User);
        public bool SearchFolioBySupplier(string purchase_no, string invoice) => _PurchaseOrderItemRepo.SearchFolioBySupplier(purchase_no, invoice);
        public int ExistUuid(string Uuid) => _PurchaseOrderItemRepo.ExistUuid(Uuid);

        public bool SaveExchangeRateDate(string PurchaseNo, DateTime Fecha, string User)
        {
            if (Fecha.Month < DateTime.Now.Month && Fecha.Year == DateTime.Now.Year)
                return _PurchaseOrderItemRepo.SaveExchangeRateDate(PurchaseNo, DateTime.Now, User);
            if (Fecha.Month < DateTime.Now.Month && Fecha.Year > DateTime.Now.Year)
                return _PurchaseOrderItemRepo.SaveExchangeRateDate(PurchaseNo, Fecha, User);
            else
                return _PurchaseOrderItemRepo.SaveExchangeRateDate(PurchaseNo, Fecha, User);
        }

        public bool AddXMLPurchaseOrderItem(string PurchaseNo, string Base64, string Uuid, string User, string Invoice, string Comment) => _PurchaseOrderItemRepo.AddXMLPurchaseOrderItem(PurchaseNo, Base64, Uuid, User, Invoice, Comment);

        public bool UpdatePurchaseOrderItem(string PurchaseNo, string Invoice, string User, string Comment) => _PurchaseOrderItemRepo.UpdatePurchaseOrderItem(PurchaseNo, Invoice, User, Comment);

        public bool UpdatePurchaseOrderItemAndTotal(string PurchaseNo, string Invoice, string User, string Comment, decimal Total) => _PurchaseOrderItemRepo.UpdatePurchaseOrderItemAndTotal(PurchaseNo, Invoice, User, Comment, Total);

        public PurchaseOrderItemModel GetPurchaseOrderItemByOCAndPartNumber(string oc, string part) => _PurchaseOrderItemRepo.GetPurchaseOrderItemByOCAndPartNumber(oc, part);

        public int GetItemPositionByOc(string oc) => _PurchaseOrderItemRepo.GetItemPositionByOc(oc);

        public int GetItemPositionByOcAndPartNumber(string oc, string part_number) => _PurchaseOrderItemRepo.GetItemPositionByOcAndPartNumber(oc, part_number);

        public bool UpdateItemPositionByOcAndPartNumber(string oc, string part_number, int position) => _PurchaseOrderItemRepo.UpdateItemPositionByOcAndPartNumber(oc, part_number, position);

        public bool UpdatePurchaseOrderItemTime(string purchase_no, string part_number, string cuser) => _PurchaseOrderItemRepo.UpdatePurchaseOrderItemTime(purchase_no, part_number, cuser);

        public bool CreatePurchaseNoItem(string purchase_order, string site, decimal quantity, string part_number, string part_barcode, int supplier_part_number, int position, string user)
        {
            try
            {
                var itemUnit = _itemBusiness.GetOneItem(part_number);
                var itemSupplier = _purchaseBaseCostRepository.GetPurchaseBaseCostBySupplierPartNumber(supplier_part_number);
                var itemTax = _itemBusiness.StoreProcedureIvaIeps(part_number);
                PURCHASE_ORDER_ITEM item = new PURCHASE_ORDER_ITEM
                {
                    purchase_no = purchase_order,
                    part_number = part_number,
                    quantity = quantity,
                    original_quantity = quantity,
                    item_position = position,
                    unit_size = itemUnit.UnitSize,
                    purchase_price = itemSupplier.base_cost,
                    packing_of_size = 1,
                    item_amount = quantity * itemSupplier.base_cost,
                    ieps = ((quantity * itemSupplier.base_cost) * (itemTax.Ieps / 100))
                };
                item.iva = (((quantity * itemSupplier.base_cost) + item.ieps) * (itemTax.Iva / 100));
                item.item_total_amount = item.item_amount + item.ieps + item.iva;
                item.item_status = 1;
                item.cuser = user;
                item.cdate = DateTime.Now;
                item.program_id = "AndroidRPC";

                var purchase = _PurchaseOrderItemRepo.CreatePurchaseNoItem(item);
                if (purchase)
                    return _blindCountBusiness.AddBlindCount(purchase_order, site, part_number, part_barcode, quantity.ToString(), position, user);
                else
                    return false;
            }
            catch (Exception err)
            {
                var msg = err.Message;
                return false;
            }
        }

        public bool UpdatePurchaseNoItem(BLIND_COUNT blindExists, string purchase_order, decimal quantityReal, decimal quantity, string part_number, int supplier_part_number, string user)
        {
            try
            {
                var itemSupplier = _purchaseBaseCostRepository.GetPurchaseBaseCostBySupplierPartNumber(supplier_part_number);
                var itemTax = _itemBusiness.StoreProcedureIvaIeps(part_number);
                return _PurchaseOrderItemRepo.UpdatePuchaseNoItem(purchase_order, quantityReal, quantity, part_number, supplier_part_number, user, itemSupplier, itemTax);
            }
            catch (Exception err)
            {
                var msg = err.Message;
                return false;
            }
        }

        public bool UpdateOC(List<PurchaseOrderItemModel> OCRC, string User) => _PurchaseOrderItemRepo.UpdateOC(OCRC, User);

        public Tuple<bool, decimal> NewUpdateOC(List<PurchaseOrderItemModel> OCRC, string User, string Comment) => _PurchaseOrderItemRepo.NewUpdateOC(OCRC, User, Comment);

        public bool CreateCreditNote(string purcheNo, string User, decimal diference) => _PurchaseOrderItemRepo.CreateCreditNote(purcheNo, User, diference);

        public int RemovePurchaseOrder(string Purchase, string User) => _PurchaseOrderItemRepo.RemovePurchaseOrder(Purchase, User);

        public decimal DiferenceOCRG(string Purchase) => _PurchaseOrderItemRepo.DiferenceOCRG(Purchase);
    }
}