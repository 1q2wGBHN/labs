﻿using App.BLL.Barcodes;
using App.BLL.BlindCount;
using App.BLL.Configuration;
using App.BLL.Item;
using App.BLL.Ma_Tax;
using App.BLL.Site;
using App.BLL.Supplier;
using App.Common;
using App.DAL.Item;
using App.DAL.MovementNumber;
using App.DAL.PurchaseOrder;
using App.Entities;
using App.Entities.ViewModels.Android;
using App.Entities.ViewModels.Item;
using App.Entities.ViewModels.PurchaseOrder;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;

namespace App.BLL.PurchaseOrder
{
    public class PurchaseOrderBusiness
    {
        private readonly PurchaseOrderRepository _PurchaseOrderRepo;
        private readonly PurchaseOrderItemRepository _PurchaseOrderItemRepo;
        private readonly MovementNumberRepository _MovementNumberRepository;
        private readonly ItemBusiness _itemBusiness;
        private readonly MaTaxBusiness _maTaxBusiness;
        private readonly SysSiteBusiness _SysSiteBusiness;
        private readonly ItemRepository _ItemRepo;
        private readonly SiteBusiness _SiteBusiness;
        private readonly Common.Common common;
        private readonly UserMasterBusiness _userMasterBusiness;
        private readonly BarcodesBusiness _barcodesBusiness;
        private readonly BlindCountBusiness _blindCountBusiness;
        private readonly UserMasterBusiness _UserMasterBusiness;
        private readonly SendMail inforamtion = new SendMail();
        private readonly MaSupplierContactBusiness _MaSupplierContactBusiness;

        public PurchaseOrderBusiness()
        {
            _PurchaseOrderRepo = new PurchaseOrderRepository();
            _PurchaseOrderItemRepo = new PurchaseOrderItemRepository();
            _MovementNumberRepository = new MovementNumberRepository();
            common = new Common.Common();
            _itemBusiness = new ItemBusiness();
            _maTaxBusiness = new MaTaxBusiness();
            _SysSiteBusiness = new SysSiteBusiness();
            _ItemRepo = new ItemRepository();
            _SiteBusiness = new SiteBusiness();
            _userMasterBusiness = new UserMasterBusiness();
            _barcodesBusiness = new BarcodesBusiness();
            _blindCountBusiness = new BlindCountBusiness();
            _UserMasterBusiness = new UserMasterBusiness();
            _MaSupplierContactBusiness = new MaSupplierContactBusiness();
        }

        public int AddOrderPurchase(string numberFolio, int folio, string SiteCode, int supplier, DateTime orderDate, string currency, string comment, string sesion, string program_id) => _PurchaseOrderRepo.AddOrderPurchase(numberFolio, folio, SiteCode, supplier, orderDate, currency, comment, sesion, program_id);

        public bool AddNewOrderPurchase(PurchaseHeader PurchaseHeaderOrder, string Site, string Document, string User, string ProgramId)
        {
            var PurchaseOrder = new PURCHASE_ORDER()
            {
                folio = 0,
                purchase_no = Document,
                site_code = Site,
                purchase_date = DateTime.Now,
                eta = PurchaseHeaderOrder.PurchaseDate,
                currency = PurchaseHeaderOrder.Currency,
                purchase_status = 1,
                program_id = ProgramId,
                supplier_id = PurchaseHeaderOrder.SupplierId,
                purchase_remark = PurchaseHeaderOrder.PurchaseRemark,
                cuser = User,
                cdate = DateTime.Now
            };
            return _PurchaseOrderRepo.AddNewPurchaseOrder(PurchaseOrder);
        }

        public bool AddNewOrderItemPurchase(List<ItemPurchase> ItemPurchase, string Document, string User, string ProgramId)
        {
            var order = ItemPurchase.Select(x => new PURCHASE_ORDER_ITEM
            {
                cdate = DateTime.Now,
                cuser = User,
                program_id = ProgramId,
                purchase_no = Document,
                quantity = x.Quantity,
                original_quantity = x.Quantity,
                unit_size = x.UnitSize,
                purchase_price = x.PurchasePrice,
                packing_of_size = x.PackingOfSize,
                item_amount = x.ItemAmount,
                iva = x.IvaTotal,
                ieps = x.IepsTotal,
                item_total_amount = x.ItemTotalAmount,
                item_status = 1,
                part_number = x.PartNumber,
            }).ToList();
            var save = _PurchaseOrderRepo.AddNewPurchaseOrderItem(order);
            return save;
        }

        public bool RemovePurchaseOrder(string Document) => _PurchaseOrderRepo.RemovePurchaseOrder(Document);

        public int AddOrderItemPurchase(string NumberFolio, string PartNumber, decimal Quantity, string UnitSize, decimal PurchasePrice, int PackingOfSize, decimal ItemAmount, decimal Iva, decimal Ieps, decimal ItemTotalAmount, string sesion) => _PurchaseOrderRepo.AddPurchaseOrderItem(NumberFolio, PartNumber, Quantity, UnitSize, PurchasePrice, PackingOfSize, ItemAmount, Iva, Ieps, ItemTotalAmount, sesion);

        public void SendEmailSupplier(int Supplier, string NumberFolio)
        {
            try
            {
                List<Entities.ViewModels.PurchaseOrder.PurchaseOrderModel> InformationPurchase = _PurchaseOrderRepo.GetPurchaseOrder(NumberFolio);
                var body = common.generateTable(InformationPurchase);

                var Email = _PurchaseOrderRepo.GetContactSupplier(Supplier);

                inforamtion.folio = InformationPurchase[0].PurchaseNo;
                inforamtion.store = InformationPurchase[0].SiteCode;
                inforamtion.date = Convert.ToString(InformationPurchase[0].PurchaseDate.Date);
                inforamtion.currecy = InformationPurchase[0].Currency;
                inforamtion.body = body;
                inforamtion.subject = "Orden de compra";
                inforamtion.from = "El florido";
                inforamtion.email = Email;

                common.MailMessagePurchase(inforamtion);
            }
            catch (Exception ex)
            {
                ex.ToString();
            }
        }

        public Tuple<List<Entities.ViewModels.PurchaseOrder.PurchaseOrderModel>, string, string> SendEmailSupplier2(int Supplier, string NumberFolio)
        {
            try
            {
                List<Entities.ViewModels.PurchaseOrder.PurchaseOrderModel> InformationPurchase = _PurchaseOrderRepo.GetPurchaseOrder(NumberFolio);

                var Email = _PurchaseOrderRepo.GetContactSupplier(Supplier);

                foreach (var item in InformationPurchase)
                {
                    var Tax = _ItemRepo.StoreProcedureIvaIeps(item.PartNumber);
                    var Ieps = Tax.Ieps;
                    var Iva = Tax.IvaPurchase;
                    var SubTotal = item.Quantity * item.PurchasePrice;
                    item.Ieps = SubTotal * (Ieps / 100);
                    item.Iva = (SubTotal + item.Ieps) * (Iva / 100);
                }

                var site = _SiteBusiness.GetSiteCodeName(InformationPurchase[0].SiteCode);

                return Tuple.Create(InformationPurchase, Email ?? "", site);
            }
            catch (Exception ex)
            {
                ex.ToString();
                return Tuple.Create(new List<Entities.ViewModels.PurchaseOrder.PurchaseOrderModel>(), "", "");
            }
        }

        public Tuple<List<Entities.ViewModels.PurchaseOrder.PurchaseOrderModel>, string, string> EmailPurchaseOrder(int Supplier, string NumberFolio)
        {
            try
            {
                List<Entities.ViewModels.PurchaseOrder.PurchaseOrderModel> InformationPurchase = _PurchaseOrderRepo.GetPurchaseOrder(NumberFolio);

                var Email = _MaSupplierContactBusiness.EmailsGeneric(Supplier, "Ventas");
                if (Email == "")
                    Email = _MaSupplierContactBusiness.EmailsGeneric(Supplier, "Contabilidad");
                var site = _SiteBusiness.GetSiteCodeName(InformationPurchase[0].SiteCode);
                return Tuple.Create(InformationPurchase, Email ?? "", site);
            }
            catch (Exception ex)
            {
                ex.ToString();
                return Tuple.Create(new List<Entities.ViewModels.PurchaseOrder.PurchaseOrderModel>(), "", "");
            }
        }

        public List<Entities.ViewModels.PurchaseOrder.PurchaseOrderModel> OrderListEmail(string NumberFolio) => _PurchaseOrderRepo.GetPurchaseOrder(NumberFolio);

        public List<Entities.ViewModels.Android.PurchaseOrderModel> GetAllOCInStatus1() => _PurchaseOrderRepo.GetAllOCInStatus1();

        public List<Entities.ViewModels.Android.PurchaseOrderModel> GetAllOCInStatus1BySupplier(string supplier) => _PurchaseOrderRepo.GetAllOCInStatus1BySupplier(supplier);

        public List<PurchaseOrderRCModel> GetAllOC() => _PurchaseOrderRepo.GetAllOC();

        public string GetOCRC(string PurchaseNo) => _PurchaseOrderRepo.GetOCRC(PurchaseNo);

        // Retorna orden de compra por numero de orden con status 1
        public PURCHASE_ORDER GetPurchaseOrderByNo(string purchase_no) => _PurchaseOrderRepo.GetPurchaseOrderByNo(purchase_no);

        // Retorna orden de compra por numero de orden sin estatus
        public PURCHASE_ORDER GetPurchaseOrderByPo(string purchase_no)
        {
            var completeName = "";
            var name = _userMasterBusiness.GetUserMasterByUsername(_PurchaseOrderRepo.GetPurchaseOrderByPo(purchase_no).cuser);
            if (name != null)
                completeName = name.first_name + " " + name.last_name;
            var purchase = _PurchaseOrderRepo.GetPurchaseOrderByPo(purchase_no);
            purchase.cuser = completeName;
            if (purchase.uuser != null)
            {
                var nameUdate = _userMasterBusiness.GetUserMasterByUsername(_PurchaseOrderRepo.GetPurchaseOrderByPo(purchase_no).uuser);
                var completeNameUdate = nameUdate.first_name + " " + nameUdate.last_name;
                purchase.uuser = completeNameUdate;
            }
            else
                purchase.uuser = "-";
            return purchase;
        }

        public bool UpdatePurchaseOrderFromAndroid(PURCHASE_ORDER purchase, string user)
        {
            if (purchase.purchase_status == 5)// Rescaneo a Pie de Camión
                purchase.purchase_status = 4; //Pie de Camión
            else
                purchase.purchase_status = 3; //Pedido normal
            purchase.uuser = user;
            purchase.udate = DateTime.Now;

            var check = false;
            if (_PurchaseOrderRepo.UpdatePurchaseOrderFromAndroid(purchase) > 0) check = true;
            return check;
        }

        public bool StatusPurchase(string PurchaseNomber, List<PurchaseOrderItemModel> OCRC, string User) => _PurchaseOrderRepo.StatusPurchase(PurchaseNomber, OCRC, User);

        public PURCHASE_ORDER GetPurchaseOrderByNoAllStatus(string purchase_no) => _PurchaseOrderRepo.GetPurchaseOrderByNoAllStatus(purchase_no);

        public int GetUpdatePurchasePrintStatus(string purchase_no) => _PurchaseOrderRepo.GetUpdatePurchasePrintStatus(purchase_no);

        public List<Entities.ViewModels.PurchaseOrder.PurchaseOrderModel> GetAllPurchasesOrders(DateTime date1, DateTime date2)
        {
            var InformationPurchase = _PurchaseOrderRepo.GetAllPurchasesOrders(date1, date2);
            InformationPurchase.ForEach(s =>
            {
                switch (s.purchase_status_value)
                {
                    case 1:
                        s.purchase_status_description = "Confirmada";
                        break;
                    case 8:
                        s.purchase_status_description = "Cancelada";
                        break;
                    case 9:
                        s.purchase_status_description = "Terminada";
                        break;
                    default:
                        s.purchase_status_description = "Escaneada";
                        break;
                }
            });
            return InformationPurchase;
        }

        public List<Entities.ViewModels.PurchaseOrder.PurchaseOrderModel> NewGetAllPurchasesOrders(DateTime date1, DateTime date2)
        {
            var InformationPurchase = _PurchaseOrderRepo.NewGetAllPurchasesOrders(date1, date2);
            InformationPurchase.ForEach(s =>
            {
                switch (s.purchase_status_value)
                {
                    case 1:
                        s.purchase_status_description = "Confirmada";
                        break;
                    case 8:
                        s.purchase_status_description = "Cancelada";
                        break;
                    case 9:
                        s.purchase_status_description = "Terminada";
                        break;
                    default:
                        s.purchase_status_description = "Escaneada";
                        break;
                }
            });
            return InformationPurchase;
        }

        public List<Entities.ViewModels.PurchaseOrder.PurchaseOrderModel> NewGetAllPurchasesOrdersXML(DateTime date1, DateTime date2)
        {
            var InformationPurchase = _PurchaseOrderRepo.NewGetAllPurchasesOrdersXML(date1, date2);
            InformationPurchase.ForEach(s =>
            {
                switch (s.purchase_status_value)
                {
                    case 1:
                        s.purchase_status_description = "Confirmada";
                        break;
                    case 8:
                        s.purchase_status_description = "Cancelada";
                        break;
                    case 9:
                        s.purchase_status_description = "Terminada";
                        break;
                    default:
                        s.purchase_status_description = "Escaneada";
                        break;
                }
            });
            return InformationPurchase;
        }

        public List<Entities.ViewModels.PurchaseOrder.PurchaseOrderModel> GetAllPurchasesOrdersStatusSupplier(DateTime? date1, DateTime? date2, int status, int supplier, string partNumber, string department, string family, string currency)
        {
            DateTime date = DateTime.Today;
            var firstDayOfMonth = new DateTime(date.Year, date.Month, 1);
            var lastDayOfMonth = firstDayOfMonth.AddMonths(1).AddDays(-1);
            DateTime BeginDate2 = date1 ?? firstDayOfMonth;
            DateTime EndDate2 = date2 ?? lastDayOfMonth;
            /*Nuevp*/
            var InformationPurchase = _PurchaseOrderRepo.GetAllGenericPurchasesOrders(date1, date2, status, supplier, partNumber, department, family, currency);
            InformationPurchase.ForEach(s =>
            {
                switch (s.purchase_status_value)
                {
                    case 1:
                        s.purchase_status_description = "Confirmada";
                        break;
                    case 8:
                        s.purchase_status_description = "Cancelada";
                        break;
                    case 9:
                        s.purchase_status_description = "Terminada";
                        break;
                    default:
                        s.purchase_status_description = "Escaneada";
                        break;
                }
            });
            return InformationPurchase;
            /*Nuevo*/
            //if (status == 0 && supplier == 0)
            //{
            //    var InformationPurchase = _PurchaseOrderRepo.GetAllPurchasesOrders(BeginDate2, EndDate2);
            //    InformationPurchase.ForEach(s =>
            //    {
            //        switch (s.purchase_status_value)
            //        {
            //            case 1:
            //                s.purchase_status_description = "Confirmada";
            //                break;
            //            case 8:
            //                s.purchase_status_description = "Cancelada";
            //                break;
            //            case 9:
            //                s.purchase_status_description = "Terminada";
            //                break;
            //            default:
            //                s.purchase_status_description = "Escaneada";
            //                break;
            //        }
            //    });
            //    return InformationPurchase;
            //}
            //else if (status != 0 && supplier == 0)
            //{
            //    var InformationPurchase = _PurchaseOrderRepo.GetAllPurchasesOrdersStatus(BeginDate2, EndDate2, status);
            //    InformationPurchase.ForEach(s =>
            //    {
            //        switch (s.purchase_status_value)
            //        {
            //            case 1:
            //                s.purchase_status_description = "Confirmada";
            //                break;
            //            case 8:
            //                s.purchase_status_description = "Cancelada";
            //                break;
            //            case 9:
            //                s.purchase_status_description = "Terminada";
            //                break;
            //            default:
            //                s.purchase_status_description = "Escaneada";
            //                break;
            //        }
            //    });
            //    return InformationPurchase;
            //}
            //else if (status == 0 && supplier != 0)
            //{
            //    var InformationPurchase = _PurchaseOrderRepo.GetAllPurchasesOrdersSupplier(BeginDate2, EndDate2, supplier);
            //    InformationPurchase.ForEach(s =>
            //    {
            //        switch (s.purchase_status_value)
            //        {
            //            case 1:
            //                s.purchase_status_description = "Confirmada";
            //                break;
            //            case 8:
            //                s.purchase_status_description = "Cancelada";
            //                break;
            //            case 9:
            //                s.purchase_status_description = "Terminada";
            //                break;
            //            default:
            //                s.purchase_status_description = "Escaneada";
            //                break;
            //        }
            //    });
            //    return InformationPurchase;
            //}
            //else if (status != 0 && supplier != 0)
            //{
            //    var InformationPurchase = _PurchaseOrderRepo.GetAllPurchasesOrdersStatusSupplier(BeginDate2, EndDate2, status, supplier);
            //    InformationPurchase.ForEach(s =>
            //    {
            //        switch (s.purchase_status_value)
            //        {
            //            case 1:
            //                s.purchase_status_description = "Confirmada";
            //                break;
            //            case 8:
            //                s.purchase_status_description = "Cancelada";
            //                break;
            //            case 9:
            //                s.purchase_status_description = "Terminada";
            //                break;
            //            default:
            //                s.purchase_status_description = "Escaneada";
            //                break;
            //        }
            //    });
            //    return InformationPurchase;
            //}
            //else
            //    return null;
        }

        public List<Entities.ViewModels.PurchaseOrder.PurchaseOrderModel> GetAllPurchasesOrdersXML(DateTime? date1, DateTime? date2, int supplier, string partNumber, string department, string family, string currency)
        {
            var InformationPurchase = _PurchaseOrderRepo.GetAllGenericPurchasesOrdersXML(date1, date2, supplier, partNumber, department, family, currency);
            InformationPurchase.ForEach(s =>
            {
                switch (s.purchase_status_value)
                {
                    case 1:
                        s.purchase_status_description = "Confirmada";
                        break;
                    case 8:
                        s.purchase_status_description = "Cancelada";
                        break;
                    case 9:
                        s.purchase_status_description = "Terminada";
                        break;
                    default:
                        s.purchase_status_description = "Escaneada";
                        break;
                }
            });
            return InformationPurchase;
        }

        public List<PurchaseOrderItemDetailReportGeneral> GetAllPurchasesOrdersStatusSupplierDetail(DateTime? date1, DateTime? date2, int status, int supplier, string partNumber, string department, string family, string currency)
        {
            DateTime date = DateTime.Today;
            var firstDayOfMonth = new DateTime(date.Year, date.Month, 1);
            var lastDayOfMonth = firstDayOfMonth.AddMonths(1).AddDays(-1);
            DateTime BeginDate2 = date1 ?? firstDayOfMonth;
            DateTime EndDate2 = date2 ?? lastDayOfMonth;

            var InformationPurchase = _PurchaseOrderRepo.GetAllGenericPurchasesOrdersDetail(BeginDate2, EndDate2, status, supplier, partNumber, department, family, currency);
            return InformationPurchase;
        }

        public List<PurchaseOrderItemDetailReportGeneral> GetAllPurchasesOrdersStatusSupplierDetailXML(DateTime date1, DateTime date2, int supplier, string partNumber, string department, string family, string currency)
        {
            var InformationPurchase = _PurchaseOrderRepo.GetAllGenericPurchasesOrdersDetailXML(date1, date2, supplier, partNumber, department, family, currency);
            return InformationPurchase;
        }

        public List<Entities.ViewModels.PurchaseOrder.PurchaseOrderModel> GetFinishedPurchasesOrders(DateTime BeginDate, DateTime EndDate)
        {
            var InformationPurchase = _PurchaseOrderRepo.GetFinishedPurchasesOrders(BeginDate, EndDate);
            InformationPurchase.ForEach(s =>
            {
                s.purchase_status_description = "Terminada";
            });
            return InformationPurchase;
        }

        public List<Entities.ViewModels.PurchaseOrder.PurchaseOrderModel> GetAllFinishedPurchasesOrders()
        {
            DateTime date = DateTime.Now;
            var firstDayOfMonth = new DateTime(date.Year, date.Month, 1);
            var lastDayOfMonth = firstDayOfMonth.AddMonths(1).AddDays(-1);

            var InformationPurchase = _PurchaseOrderRepo.GetAllFinishedPurchasesOrders(firstDayOfMonth, lastDayOfMonth);
            InformationPurchase.ForEach(s =>
            {
                s.purchase_status_description = "Terminada";
            });
            return InformationPurchase;
        }

        public bool CancelFinishedPurchaseOrder(string PurchaseOrder, string User, out string Document, out int Response)
        {
            try
            {
                string Site = _SysSiteBusiness.GetSite().site_code;
                string ProgramId = "PURC004.cshtml";

                return _PurchaseOrderRepo.CancelFinishedPurchaseOrder(PurchaseOrder, Site, User, ProgramId, out Document, out Response);
            }
            catch (Exception ex)
            {
                string msg = ex.Message;
                Document = "";
                Response = 1;
                return false;
            }
        }

        public bool UpdateCommentInCancelPO(string purchase_order, string comment, string user) => _PurchaseOrderRepo.UpdateCommentInCancelPO(purchase_order, comment, user);

        public List<Entities.ViewModels.PurchaseOrder.PurchaseOrderModel> GetCanceledPurchasesOrders(DateTime begindate, DateTime enddate)
        {
            var InformationPurchase = _PurchaseOrderRepo.GetCanceledPurchasesOrders(begindate, enddate);
            InformationPurchase.ForEach(s =>
            {
                s.purchase_status_description = "Cancelada";
            });
            return InformationPurchase;
        }

        public List<PurchaseOrderItemListModel> GetAllPurchasesDetail(string poNumber) => _PurchaseOrderItemRepo.GetAllPurchasesDetail(poNumber);

        public List<PurchaseItemReportModel> GetAllProducts(string part_number, DateTime date, DateTime date2) => _PurchaseOrderItemRepo.GetAllProducts(part_number, date, date2);

        public List<PurchaseItemReportModel> GetAllProductsSupplier(int supplier, DateTime date, DateTime date2) => _PurchaseOrderItemRepo.GetAllProductsSupplier(supplier, date, date2);

        public List<PurchaseOrderItemListModel> NewGetAllPurchasesDetail(string poNumber) => _PurchaseOrderItemRepo.NewGetAllPurchasesDetail(poNumber);

        public List<PurchaseOrderItemListModel> NewGetAllPurchasesDetailStatus(string poNumber, int status) => _PurchaseOrderItemRepo.NewGetAllPurchasesDetailStatus(poNumber, status);

        public List<PurchaseOrderItemListModel> NewGetAllPurchasesDetailStatusFormatView(string poNumber, string purchase_status_value)
        {
            int status;
            switch (purchase_status_value)
            {
                case "Confirmada":
                    status = 1;
                    break;
                case "Cancelada":
                    status = 8;
                    break;
                case "Terminada":
                    status = 1;
                    break;
                default:
                    status = 1;
                    break;
            }
            return _PurchaseOrderItemRepo.NewGetAllPurchasesDetailStatus(poNumber, status);
        }

        public List<ItemPurchase> GetAllPurchasesDetailMod(string poNumber) => _PurchaseOrderItemRepo.GetAllPurchasesDetailMod(poNumber);

        public Tuple<List<Entities.ViewModels.PurchaseOrder.PurchaseOrderModel>, List<PurchaseOrderItemListModel>> GetAllListFillRate(DateTime date1, DateTime date2, string supplierId)
        {
            int sup = int.Parse(supplierId);
            var listOrders = _PurchaseOrderRepo.GetAllPurchasesOrdersBySupplier(date1, date2, sup);
            if (listOrders.Count > 0)
            {
                var listItems = new List<PurchaseOrderItemListModel>();
                for (int i = 0; i < listOrders.Count; i++)
                {
                    listItems = _PurchaseOrderItemRepo.GetAllPurchasesDetailByPurchaseNo(listOrders[i].PurchaseNo);
                    decimal t = 0;
                    for (int e = 0; e < listItems.Count; e++)
                    {
                        var ivaValue = _maTaxBusiness.GetTaxValueByCode(listItems[e].iva_text);
                        var iepsValue = _maTaxBusiness.GetTaxValueByCode(listItems[e].ieps_text);

                        var amount = listItems[e].requestedQuantity * listItems[e].PurchasePrice;
                        var TotalIEPS = amount * (iepsValue.tax_value / 100);
                        var TotalIVA = (amount + TotalIEPS) * (ivaValue.tax_value / 100);
                        var Total = amount + TotalIEPS + TotalIVA;
                        t += decimal.Parse((Total).ToString(string.Format("0.{0}", new string('0', 4))));

                        listOrders[i].assortedQuantity = listOrders[i].assortedQuantity + listItems[e].assortedQuantity;
                        listOrders[i].assortedAmount = listOrders[i].assortedAmount + listItems[e].assortedAmount;
                        listOrders[i].requestedQuantity = listOrders[i].requestedQuantity + listItems[e].requestedQuantity;
                        listOrders[i].requestedAmount = t;

                        listItems[e].requestedAmount = decimal.Parse(Total.ToString(string.Format("0.{0}", new string('0', 4))));
                        listItems[e].efficiencyInQuantity = decimal.Parse(((listItems[e].assortedQuantity * 100) / listItems[e].requestedQuantity).ToString(string.Format("0.{0}", new string('0', 4))));
                        if (listItems[e].requestedAmount != 0)
                            listItems[e].efficiencyInAmount = decimal.Parse(((listItems[e].assortedAmount * 100) / listItems[e].requestedAmount).ToString(string.Format("0.{0}", new string('0', 4))));
                        else
                            listItems[e].efficiencyInAmount = 0; //Este problema llevaba a pasar cuando el precio llegaba en 0
                    }
                    listOrders[i].efficiencyInQuantity = decimal.Parse(((listOrders[i].assortedQuantity * 100) / listOrders[i].requestedQuantity).ToString(string.Format("0.{0}", new string('0', 4))));
                    listOrders[i].efficiencyInAmount = decimal.Parse(((listOrders[i].assortedAmount * 100) / listOrders[i].requestedAmount).ToString(string.Format("0.{0}", new string('0', 4))));
                }
                return Tuple.Create(listOrders, listItems);
            }
            else
            {
                var listItems = new List<PurchaseOrderItemListModel>();
                return Tuple.Create(listOrders, listItems);
            }
        }

        public string GeneratePOAndroid(string items, string usuario, string proveedor, string moneda, string site_code)
        {
            string v = "{\"Items\":" + items + "}";
            var result = JsonConvert.DeserializeObject<ItemsPurchaseOrderModel>(v);
            List<ItemsPurchaseOrderModel> list = new List<ItemsPurchaseOrderModel>();
            for (int i = 0; i < result.Items.Count; i++)
            {
                ItemsPurchaseOrderModel model = new ItemsPurchaseOrderModel
                {
                    part_number = result.Items[i].part_number,
                    quantity = result.Items[i].quantity,
                    cost = result.Items[i].cost
                };
                list.Add(model);
            }
            var NumberPurchase = _MovementNumberRepository.ExecuteSp_Get_Next_Document();
            PURCHASE_ORDER order = new PURCHASE_ORDER
            {
                purchase_no = NumberPurchase,
                site_code = site_code,
                supplier_id = int.Parse(proveedor),
                purchase_date = DateTime.Now,
                eta = DateTime.Now,
                currency = moneda,
                purchase_status = 4,
                purchase_remark = "Recibo a Pie de Camion",
                cuser = usuario,
                cdate = DateTime.Now,
                program_id = "AndroidRPC"
            };
            int position = 1;
            var Purchase = _PurchaseOrderRepo.AddPurchaseOrderAndroid(order);
            if (Purchase != 0)
            {
                for (int i = 0; i < list.Count; i++)
                {
                    var itemBD = _itemBusiness.GetOneItem(list[i].part_number);
                    if (itemBD != null)
                    {
                        var Tax = _ItemRepo.StoreProcedureIvaIeps(list[i].part_number);
                        var ivaValue = Tax.IvaPurchase;
                        var iepsValue = Tax.Ieps;
                        var amount = decimal.Parse(list[i].quantity) * decimal.Parse(list[i].cost);
                        var TotalIEPS = amount * (iepsValue / 100);
                        var TotalIVA = (amount + TotalIEPS) * (ivaValue / 100);
                        var Total = amount + TotalIEPS + TotalIVA;

                        PURCHASE_ORDER_ITEM item = new PURCHASE_ORDER_ITEM
                        {
                            purchase_no = NumberPurchase,
                            part_number = list[i].part_number,
                            quantity = decimal.Parse(list[i].quantity),
                            unit_size = itemBD.UnitSize,
                            purchase_price = decimal.Parse(list[i].cost),
                            packing_of_size = 1,
                            item_position = position,
                            item_amount = amount,
                            iva = TotalIVA,
                            ieps = TotalIEPS,
                            item_total_amount = Total,
                            item_status = 1,
                            original_quantity = decimal.Parse(list[i].quantity),
                            cuser = usuario,
                            cdate = DateTime.Now,
                            program_id = "AndroidRPC"
                        };
                        int resultPurch = _PurchaseOrderItemRepo.AddPurchaseOrderItem(item);
                        if (resultPurch == 0)
                            return "";
                        //busca barcode
                        var barcodes = _barcodesBusiness.GetAllDataByBarcode(list[i].part_number);

                        if (barcodes != null)
                        {
                            //Si hubo error regr
                            if (!_blindCountBusiness.AddBlindCount(NumberPurchase, site_code, list[i].part_number, barcodes.part_barcode, list[i].quantity, position, usuario))
                                return "";
                        }
                        else
                            return "";
                    }
                    position++;
                }
                return NumberPurchase;
            }
            else
                return "";
        }

        public List<Entities.ViewModels.PurchaseOrder.PurchaseOrderModel> GetAllOrders() => _PurchaseOrderRepo.GetAllOrders();

        public List<Entities.ViewModels.PurchaseOrder.PurchaseOrderModel> GetAllOrdersWithoutXML() => _PurchaseOrderRepo.GetAllOrdersWithoutXML();

        public List<Entities.ViewModels.PurchaseOrder.PurchaseOrderModel> GetAllOrdersEdits() => _PurchaseOrderRepo.GetAllOrdersEdit();

        public List<PurchaseOrderItemListModel> GetAllPurchasesDetailByPurchaseNo(string poNumber) => _PurchaseOrderItemRepo.GetAllPurchasesDetailByPurchaseNo(poNumber);

        public Tuple<ItemPurchase, int> EditPurchaseOrder(string purchaseNo, decimal price, string partNumber, string user)
        {
            var validation = _PurchaseOrderRepo.ValidationPurchaseOrder(purchaseNo, partNumber);
            var userInfo = _UserMasterBusiness.GetUserMasterByUsername(user);
            if ((userInfo.buyer_division == null && price <= validation.BaseCost) || (userInfo.buyer_division != null))
                return _PurchaseOrderRepo.EditPurchaseOrder(purchaseNo, price, partNumber, user);

            return new Tuple<ItemPurchase, int>(new ItemPurchase(), 4);
        }

        public int DeleteProductPurchaseOrder(string purchaseNo, string partNumber, string user)
        {
            if (_userMasterBusiness.IsRolonUser(user, ConfigurationManager.AppSettings["Compras"].ToString()) ||
                _userMasterBusiness.IsRolonUser(user, ConfigurationManager.AppSettings["Gerente"].ToString()) ||
                _userMasterBusiness.IsRolonUser(user, ConfigurationManager.AppSettings["AuxCompras"].ToString()) ||
                _userMasterBusiness.IsRolonUser(user, ConfigurationManager.AppSettings["Sistemas"].ToString()))
                return _PurchaseOrderRepo.DeleteProductPurchaseOrder(purchaseNo, partNumber, user);
            return 3;
        }

        public int SupplierPurchaseOrder(string purchaseNo, string user)
        {
            if (_userMasterBusiness.IsRolonUser(user, ConfigurationManager.AppSettings["Compras"].ToString()) ||
                _userMasterBusiness.IsRolonUser(user, ConfigurationManager.AppSettings["Gerente"].ToString()) ||
                _userMasterBusiness.IsRolonUser(user, ConfigurationManager.AppSettings["AuxCompras"].ToString()) ||
                _userMasterBusiness.IsRolonUser(user, ConfigurationManager.AppSettings["Sistemas"].ToString()))
                return _PurchaseOrderRepo.SupplierPurchaseOrder(purchaseNo);
            return 3;
        }

        public int PurchaseOrderAddPartNumber(string purchaseNo, string PartNumber, decimal Quantity, int Supplier, string user)
        {
            if (_userMasterBusiness.IsRolonUser(user, ConfigurationManager.AppSettings["Compras"].ToString()) ||
                _userMasterBusiness.IsRolonUser(user, ConfigurationManager.AppSettings["Gerente"].ToString()) ||
                _userMasterBusiness.IsRolonUser(user, ConfigurationManager.AppSettings["AuxCompras"].ToString()) ||
                _userMasterBusiness.IsRolonUser(user, ConfigurationManager.AppSettings["Sistemas"].ToString()))
                return _PurchaseOrderRepo.PurchaseOrderAddPartNumber(purchaseNo, PartNumber, Quantity, user, _ItemRepo.GetCostItem(PartNumber, Supplier));
            return 3;
        }

        public int UpdatePurchaseOrder(string purchaseNo, int Supplier, string user)
        {
            if (_userMasterBusiness.IsRolonUser(user, ConfigurationManager.AppSettings["Compras"].ToString()) ||
                _userMasterBusiness.IsRolonUser(user, ConfigurationManager.AppSettings["Gerente"].ToString()) ||
                _userMasterBusiness.IsRolonUser(user, ConfigurationManager.AppSettings["AuxCompras"].ToString()) ||
                _userMasterBusiness.IsRolonUser(user, ConfigurationManager.AppSettings["Sistemas"].ToString()))
                return _PurchaseOrderRepo.UpdatePurchaseOrder(purchaseNo, Supplier, user);
            return 3;
        }

        public int GetSupplierIdByPurchase(string purchase_order) => _PurchaseOrderRepo.GetSupplierIdByPurchase(purchase_order);
        public bool UpdatePurchaseOrderInvoice(string PurchaseOrder, string InvoiceNo, string Comments, string User) => _PurchaseOrderRepo.UpdatePurchaseOrderInvoice(PurchaseOrder, InvoiceNo, Comments, User);
        public string GetCurrencyPO(string folio)
        {
            return _PurchaseOrderRepo.GetCurrencyPO(folio);
        }
    }
}