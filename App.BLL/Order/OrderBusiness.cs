﻿using App.DAL;
using App.DAL.MovementNumber;
using App.DAL.Order;
using App.DAL.PurchaseOrder;
using App.Entities;
using App.Entities.ViewModels.Order;
using System;
using System.Collections.Generic;

namespace App.BLL.Order
{
    public class OrderBusiness
    {
        private readonly OrderRepository _OrderRepo;
        private readonly PurchaseOrderRepository _purchaseOrderRepository;
        private readonly MovementNumberRepository _MovementNumberRepository;

        public OrderBusiness()
        {
            _OrderRepo = new OrderRepository();
            _purchaseOrderRepository = new PurchaseOrderRepository();
            _MovementNumberRepository = new MovementNumberRepository();
        }

        public List<OrderModel> GetAll() => _OrderRepo.GetAll();

        public OrderModel Get(int Folio)
        {
            var Order = _OrderRepo.Get(Folio);
            if (Order != null)
                return Order;

            return null;
        }

        public int Post(ORDER_HEAD Order)
        {
            if (Order.supplier_id != 0 && !string.IsNullOrWhiteSpace(Order.site_id))
            {
                int Folio = _OrderRepo.Post(Order);
                return Folio;
            }
            return 0;
        }

        public bool RemoveOrderHeader(ORDER_HEAD Order) => _OrderRepo.RemoveOrderHeader(Order);

        public StoreProcedureResult Update(int Folio, DateTime Date, string Comment, string Currency, string User, string Site, int Supplier)
        {
            StoreProcedureResult NumberPurchaseOrder = new StoreProcedureResult();

            var UpdateOrderRepo = _OrderRepo.Update(Folio, Date, Comment, User);

            if (UpdateOrderRepo)
            {
                var NumberPurchase = _MovementNumberRepository.ExecuteSp_Get_Next_Document();
                var Purchase = _purchaseOrderRepository.AddOrderPurchase(NumberPurchase, Folio, Site, Supplier, Date, Currency, Comment, User, "ORDE002.cshtml");
                var Dato = _purchaseOrderRepository.ExecuteSprocPurchaseOrderAddItem(Folio, NumberPurchase, User);
                NumberPurchaseOrder.Document = NumberPurchase;
                NumberPurchaseOrder.ReturnValue = Dato;
            }

            return NumberPurchaseOrder;
        }

        public bool UpdateOrder(int Folio, DateTime? Date, string Comment, string User) => _OrderRepo.UpdateOrder(Folio, Date, Comment, User);

        public bool UpdateOrderAndOrderList(int Folio, DateTime? Date, string Comment, string User, int status) => _OrderRepo.UpdateOrderAndOrderList(Folio, Date, Comment, User, status);

        public bool Cancel(int Folio, string User) => _OrderRepo.Cancel(Folio, User);

        public int Status(int Folio) => _OrderRepo.Status(Folio);

        public List<OrderModel> Filter() => _OrderRepo.Filter();

        public int GeneratePurchaseOrder(int Folio, string Document, int Supplier) => _purchaseOrderRepository.ExecuteSpCreatePurchaseOrder(Folio, Document, Supplier);
    }
}