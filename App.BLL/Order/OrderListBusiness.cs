﻿using App.DAL.Order;
using App.Entities;
using App.Entities.ViewModels.Order;
using System;
using System.Collections.Generic;
using System.Linq;

namespace App.BLL.Order
{
    public class OrderListBusiness
    {
        private readonly OrderListRepository _OrderListRepo;

        public OrderListBusiness()
        {
            _OrderListRepo = new OrderListRepository();
        }

        public List<OrderListModel> GetAll(int Folio) => _OrderListRepo.GetAll(Folio);

        public bool Post(List<ORDER_LIST> orderList, string User)
        {
            bool IsCorrect = true;
            if (orderList.Where(x => x.folio == 0 || string.IsNullOrWhiteSpace(x.part_number)).Count() == 0)
            {
                var OrderList = orderList.Select(x => new ORDER_LIST
                {
                    authorized = x.authorized,
                    average_sale = x.average_sale,
                    cdate = DateTime.Now,
                    cuser = User,
                    folio = x.folio,
                    packing = x.packing,
                    part_number = x.part_number,
                    pre_purchase = x.pre_purchase,
                    program_id = "ORDE001.cshtml",
                    stock = x.stock,
                    suggested_supplier = x.suggested_supplier,
                    suggested_systems = x.suggested_systems
                }).ToList();
                var NewOrderList = _OrderListRepo.Post(OrderList);
            }
            else
                IsCorrect = false;

            return IsCorrect;
        }

        public bool NewGenerateOrderList(List<ORDER_LIST> orderList, int Folio, string User)
        {
            if (orderList.ToArray().Where(x => string.IsNullOrWhiteSpace(x.part_number)).Count() == 0 && Folio > 0)
            {
                var OrderList = orderList.Select(x => new ORDER_LIST
                {
                    authorized = 0,
                    average_sale = x.average_sale,
                    cdate = DateTime.Now,
                    cuser = User,
                    folio = Folio,
                    packing = x.packing,
                    part_number = x.part_number,
                    pre_purchase = Math.Round(x.pre_purchase, 3),
                    program_id = "ORDE001.cshtml",
                    stock = x.stock,
                    suggested_supplier = 0,
                    suggested_systems = Math.Round(x.suggested_systems, 3)
                }).ToList();
                return _OrderListRepo.NewPost(OrderList);
            }
            else
                return false;
        }

        public bool Put(List<OrderListDatesViewModel> OrderList, string User) => _OrderListRepo.UpdateOrderList(OrderList, User);
    }
}