using App.Entities.ViewModels.DamagedsGoods;
using App.Entities.ViewModels.Quotes;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net.Mail;
using System.Net.Mime;
using System.Security.Cryptography;
using System.Text;
using System.Web;

namespace App.Common
{
    public class Common
    {
        public static string CURRENCY_DEFAULT = "MXN";
        readonly string urlFile = HttpContext.Current.Server.MapPath(@"\Emails\email_template.html");
        public static string USER = ConfigurationManager.AppSettings["userEmail"].ToString();
        public static string HOST = ConfigurationManager.AppSettings["hostEmail"].ToString();
        public static int PORT = int.Parse(ConfigurationManager.AppSettings["portEmail"].ToString());

        public static string SetPassword(string password)
        {
            return string.Join("", new SHA1Managed().ComputeHash(Encoding.UTF8.GetBytes(password)).Select(x => x.ToString("X2")).ToArray());
        }

        public string MailMessageHtml(SendMail information)
        {
            string Return = string.Empty;
            try
            {
                var user = ConfigurationManager.AppSettings["userEmail"].ToString();
                var host = ConfigurationManager.AppSettings["hostEmail"].ToString();
                var port = int.Parse(ConfigurationManager.AppSettings["portEmail"].ToString());
                using (var message = new MailMessage())
                {
                    string text = File.ReadAllText(urlFile);

                    text = text.Replace("{Title}", information.title);
                    text = text.Replace("{Body}", information.body);
                    if (information.buttonText != "" && information.buttonLink != "")
                        text = text.Replace("<a href=>{Buton}</a>", "<a href=" + information.buttonLink + ">" + information.buttonText + "</a>");
                    else
                        text = text.Replace("<a href=>{Buton}</a>", "");

                    information.body = HttpUtility.HtmlDecode(text);

                    message.To.Add(information.email);
                    message.From = new MailAddress(user, information.from);
                    message.Subject = information.subject;
                    message.IsBodyHtml = true;
                    message.Priority = MailPriority.High;

                    var htmlBody = AlternateView.CreateAlternateViewFromString(
                       information.body, Encoding.UTF8, "text/html");

                    message.AlternateViews.Add(
                        AlternateView.CreateAlternateViewFromString(string.Empty, new ContentType("text/plain")));

                    message.AlternateViews.Add(htmlBody);

                    using (var smtp = new SmtpClient())
                    {
                        var credential = new System.Net.NetworkCredential();
                        smtp.UseDefaultCredentials = false;
                        smtp.Credentials = credential;
                        smtp.Host = host;
                        smtp.Port = port;
                        smtp.EnableSsl = true;
                        smtp.Send(message);
                    }
                    Return = "success";
                }
            }
            catch (Exception ex)
            {
                Return = ex.Message;
                return Return;
            }
            return Return;
        }

        public string MailMessagePurchase(SendMail information)
        {
            string Return;
            try
            {
                var user = ConfigurationManager.AppSettings["userEmail"].ToString();
                var host = ConfigurationManager.AppSettings["hostEmail"].ToString();
                var port = int.Parse(ConfigurationManager.AppSettings["portEmail"].ToString());
                var message = new MailMessage();

                var urlFile = HttpContext.Current.Server.MapPath(@"\Emails\email_Purchase.html");
                string text = File.ReadAllText(urlFile);
                //string text = System.IO.File.ReadAllText("C:\\inetpub\\FloridoERPTX\\FloridoERPTX\\Emails\\email_Purchase.html");

                text = text.Replace("{folio}", information.folio);
                text = text.Replace("{store}", information.store);
                text = text.Replace("{date}", information.date);
                text = text.Replace("{currency}", information.currecy);
                text = text.Replace("{Body}", information.body);

                information.body = HttpUtility.HtmlDecode(text);

                message.To.Add(information.email);
                message.From = new MailAddress(user, information.from);
                message.Subject = information.subject;
                message.IsBodyHtml = true;
                message.Priority = MailPriority.High;

                var htmlBody = AlternateView.CreateAlternateViewFromString(
                   information.body, Encoding.UTF8, "text/html");

                message.AlternateViews.Add(
                    AlternateView.CreateAlternateViewFromString(string.Empty, new ContentType("text/plain")));

                message.AlternateViews.Add(htmlBody);

                using (var smtp = new SmtpClient())
                {
                    var credential = new System.Net.NetworkCredential();
                    smtp.UseDefaultCredentials = false;
                    smtp.Credentials = credential;
                    smtp.Host = host;
                    smtp.Port = port;
                    smtp.EnableSsl = false;
                    smtp.Send(message);
                    message.Dispose();
                }
                Return = "success";
            }
            catch (Exception ex)
            {
                Return = ex.Message;
                return Return;
            }
            return Return;
        }

        public string MailMessageEntryFree(SendMail information, Stream stream, string storeFlorido)
        {
            string Return = string.Empty;
            try
            {
                var user = ConfigurationManager.AppSettings["userEmail"].ToString();
                var host = ConfigurationManager.AppSettings["hostEmail"].ToString();
                var port = int.Parse(ConfigurationManager.AppSettings["portEmail"].ToString());
                var message = new MailMessage();

                //  string text = System.IO.File.ReadAllText("C:\\inetpub\\ERP2017\\saidfl_erp\\FloridoERP\\Emails\\email_template.html");
                //string currentDirectory = Directory.GetCurrentDirectory();
                //string no = Path.Combine(Path.Combine(Path.GetDirectoryName(Assembly.GetExkecutingAssembly().Location)), @"Emails\\email_entryFree.html");

                var urlFile = System.Web.Hosting.HostingEnvironment.MapPath(@"\Emails\email_entryFree.html");
                string text = File.ReadAllText(urlFile);
                //string text = System.IO.File.ReadAllText(/*"C:\\proyects\\erp_florido_tienda\\FloridoERPTX\\Emails\\email_entryFree.html"*/"C:\\inetpub\\FloridoERPTX\\FloridoERPTX\\Emails\\email_entryFree.html");//Path.Combine(@"~/Emails//email_entryFree.html");// System.IO.File.ReadAllText("C:\\proyects\\erp_florido_tienda\\FloridoERPTX\\Emails\\email_entryFree.html");

                storeFlorido = storeFlorido.Replace(" ", "_");
                text = text.Replace("{Folio}", information.folio);
                text = text.Replace("{Proveedor}", information.store);

                information.body = HttpUtility.HtmlDecode(text);

                message.To.Add(information.email);
                message.From = new MailAddress(user, information.from);
                message.Subject = information.subject;
                message.IsBodyHtml = true;
                message.Priority = MailPriority.High;

                //message.Attachments.Add(new Attachment(n, Path.GetFileName("prueba.pdf"), "application/pdf"));

                var htmlBody = AlternateView.CreateAlternateViewFromString(
                   information.body, Encoding.UTF8, "text/html");

                message.AlternateViews.Add(
                    AlternateView.CreateAlternateViewFromString(string.Empty, new ContentType("text/plain")));

                message.AlternateViews.Add(htmlBody);

                var ct = new ContentType
                {
                    MediaType = MediaTypeNames.Application.Pdf,
                    Name = storeFlorido + "_MercanciaSinCargo_" + information.folio + ".pdf"
                };
                stream.Position = 0;
                Attachment pdfb64 = new Attachment(stream, ct);
                //pdfb64.TransferEncoding = TransferEncoding.Base64;
                pdfb64.ContentType.MediaType = MediaTypeNames.Application.Pdf;
                message.Attachments.Add(pdfb64);

                using (var smtp = new SmtpClient())
                {
                    var credential = new System.Net.NetworkCredential();
                    smtp.UseDefaultCredentials = false;
                    smtp.Credentials = credential;
                    smtp.Host = host;
                    smtp.Port = port;
                    smtp.EnableSsl = false;
                    smtp.Send(message);
                    message.Dispose();
                }
                Return = "success";
            }
            catch (Exception ex)
            {
                Return = ex.Message;
                return Return;
            }
            return Return;
        }

        public string MailMessageRMA(SendMail information, Stream stream)
        {
            string Return;
            try
            {
                var user = ConfigurationManager.AppSettings["userEmail"].ToString();
                var host = ConfigurationManager.AppSettings["hostEmail"].ToString();
                var port = int.Parse(ConfigurationManager.AppSettings["portEmail"].ToString());
                var message = new MailMessage();

                var urlFile = System.Web.Hosting.HostingEnvironment.MapPath(@"\Emails\email_rma.html");
                //var urlFile = HttpContext.Current.Server.MapPath(@"\Emails\email_rma.html");
                string text = File.ReadAllText(urlFile);
                //string text = System.IO.File.ReadAllText("C:\\inetpub\\FloridoERPTX\\FloridoERPTX\\Emails\\email_rma.html");
                text = text.Replace("{folio}", information.folio);
                text = text.Replace("{proveedor}", information.store);

                information.body = HttpUtility.HtmlDecode(text);

                message.To.Add(information.email);
                message.From = new MailAddress(user, information.from);
                message.Subject = information.subject;
                message.IsBodyHtml = true;
                message.Priority = MailPriority.High;

                var htmlBody = AlternateView.CreateAlternateViewFromString(
                   information.body, Encoding.UTF8, "text/html");

                message.AlternateViews.Add(
                    AlternateView.CreateAlternateViewFromString(string.Empty, new ContentType("text/plain")));

                message.AlternateViews.Add(htmlBody);

                var ct = new ContentType
                {
                    MediaType = MediaTypeNames.Application.Pdf,
                    Name = "RMA.pdf"
                };
                stream.Position = 0;
                Attachment pdfb64 = new Attachment(stream, ct);
                pdfb64.ContentType.MediaType = MediaTypeNames.Application.Pdf;
                message.Attachments.Add(pdfb64);

                using (var smtp = new SmtpClient())
                {
                    var credential = new System.Net.NetworkCredential();
                    smtp.UseDefaultCredentials = false;
                    smtp.Credentials = credential;
                    smtp.Host = host;
                    smtp.Port = port;
                    smtp.EnableSsl = false;
                    smtp.Send(message);
                    message.Dispose();
                }
                Return = "success";
            }
            catch (Exception ex)
            {
                Return = ex.Message;
                return Return;
            }
            return Return;
        }

        public string MailMessageRMACancelRequestRMA007(SendMail information, Stream stream)
        {
            string Return;
            try
            {
                var user = ConfigurationManager.AppSettings["userEmail"].ToString();
                var host = ConfigurationManager.AppSettings["hostEmail"].ToString();
                var port = int.Parse(ConfigurationManager.AppSettings["portEmail"].ToString());
                var message = new MailMessage();

                var urlFile = System.Web.Hosting.HostingEnvironment.MapPath(@"\Emails\email_rma_request_cancel_request.html");

                //var urlFile = @"C:\inetpub\FloridoERPTX\FloridoERPTX\Emails\email_rma_approve_cancel_request.html";                
                //if (HttpContext.Current != null)
                //    urlFile = System.Web.Hosting.HostingEnvironment.MapPath(@"\Emails\email_rma_approve_cancel_request.html");
                //urlFile = HttpContext.Current.Server.MapPath(@"\Emails\email_rma_approve_cancel_request.html");

                string text = File.ReadAllText(urlFile);
                text = text.Replace("{folio}", information.folio);
                text = text.Replace("{Supplier}", information.Supplier);
                text = text.Replace("{store}", information.store);

                information.body = HttpUtility.HtmlDecode(text);

                message.To.Add(information.email);
                message.From = new MailAddress(user, information.from);
                message.Subject = information.subject;
                message.IsBodyHtml = true;
                message.Priority = MailPriority.High;

                var htmlBody = AlternateView.CreateAlternateViewFromString(
                   information.body, Encoding.UTF8, "text/html");

                message.AlternateViews.Add(
                    AlternateView.CreateAlternateViewFromString(string.Empty, new ContentType("text/plain")));

                message.AlternateViews.Add(htmlBody);

                var ct = new ContentType
                {
                    MediaType = MediaTypeNames.Application.Pdf,
                    Name = "RMAApproveCancelRequest.pdf"
                };
                stream.Position = 0;
                Attachment pdfb64 = new Attachment(stream, ct);
                pdfb64.ContentType.MediaType = MediaTypeNames.Application.Pdf;
                message.Attachments.Add(pdfb64);

                using (var smtp = new SmtpClient())
                {
                    var credential = new System.Net.NetworkCredential();
                    smtp.UseDefaultCredentials = false;
                    smtp.Credentials = credential;
                    smtp.Host = host;
                    smtp.Port = port;
                    smtp.EnableSsl = false;
                    smtp.Send(message);
                    message.Dispose();
                }
                Return = "success";
            }
            catch (Exception ex)
            {
                Return = ex.Message;
                return Return;
            }
            return Return;
        }

        public string MailMessageRMACancelRequestRMA008(SendMail information, Stream stream)
        {
            string Return;
            try
            {
                var user = ConfigurationManager.AppSettings["userEmail"].ToString();
                var host = ConfigurationManager.AppSettings["hostEmail"].ToString();
                var port = int.Parse(ConfigurationManager.AppSettings["portEmail"].ToString());
                var message = new MailMessage();

                var urlFile = System.Web.Hosting.HostingEnvironment.MapPath(@"\Emails\email_rma_approve_cancel_request.html");

                //var urlFile = @"C:\inetpub\FloridoERPTX\FloridoERPTX\Emails\email_rma_approve_cancel_request.html";                
                //if (HttpContext.Current != null)
                //    urlFile = System.Web.Hosting.HostingEnvironment.MapPath(@"\Emails\email_rma_approve_cancel_request.html");
                //urlFile = HttpContext.Current.Server.MapPath(@"\Emails\email_rma_approve_cancel_request.html");

                string text = File.ReadAllText(urlFile);
                text = text.Replace("{store}", information.store);
                text = text.Replace("{folio}", information.folio);
                text = text.Replace("{Supplier}", information.Supplier);

                information.body = HttpUtility.HtmlDecode(text);

                message.To.Add(information.email);
                message.From = new MailAddress(user, information.from);
                message.Subject = information.subject;
                message.IsBodyHtml = true;
                message.Priority = MailPriority.High;

                var htmlBody = AlternateView.CreateAlternateViewFromString(
                   information.body, Encoding.UTF8, "text/html");

                message.AlternateViews.Add(
                    AlternateView.CreateAlternateViewFromString(string.Empty, new ContentType("text/plain")));

                message.AlternateViews.Add(htmlBody);

                var ct = new ContentType
                {
                    MediaType = MediaTypeNames.Application.Pdf,
                    Name = "RMAApproveCancelRequest.pdf"
                };
                stream.Position = 0;
                Attachment pdfb64 = new Attachment(stream, ct);
                pdfb64.ContentType.MediaType = MediaTypeNames.Application.Pdf;
                message.Attachments.Add(pdfb64);

                using (var smtp = new SmtpClient())
                {
                    var credential = new System.Net.NetworkCredential();
                    smtp.UseDefaultCredentials = false;
                    smtp.Credentials = credential;
                    smtp.Host = host;
                    smtp.Port = port;
                    smtp.EnableSsl = false;
                    smtp.Send(message);
                    message.Dispose();
                }
                Return = "success";
            }
            catch (Exception ex)
            {
                Return = ex.Message;
                return Return;
            }
            return Return;
        }

        public string MailMessageRMARejectionRequestRMA008(SendMail information, Stream stream)
        {
            string Return;
            try
            {
                var user = ConfigurationManager.AppSettings["userEmail"].ToString();
                var host = ConfigurationManager.AppSettings["hostEmail"].ToString();
                var port = int.Parse(ConfigurationManager.AppSettings["portEmail"].ToString());
                var message = new MailMessage();

                var urlFile = System.Web.Hosting.HostingEnvironment.MapPath(@"\Emails\email_rma008_approve_cancel_request.html");

                string text = File.ReadAllText(urlFile);
                text = text.Replace("{store}", information.store);
                text = text.Replace("{folio}", information.folio);
                text = text.Replace("{Supplier}", information.Supplier);

                information.body = HttpUtility.HtmlDecode(text);

                message.To.Add(information.email);
                message.From = new MailAddress(user, information.from);
                message.Subject = information.subject;
                message.IsBodyHtml = true;
                message.Priority = MailPriority.High;

                var htmlBody = AlternateView.CreateAlternateViewFromString(
                   information.body, Encoding.UTF8, "text/html");

                message.AlternateViews.Add(
                    AlternateView.CreateAlternateViewFromString(string.Empty, new ContentType("text/plain")));

                message.AlternateViews.Add(htmlBody);

                var ct = new ContentType
                {
                    MediaType = MediaTypeNames.Application.Pdf,
                    Name = "RMAApproveCancelRequest.pdf"
                };
                stream.Position = 0;
                Attachment pdfb64 = new Attachment(stream, ct);
                pdfb64.ContentType.MediaType = MediaTypeNames.Application.Pdf;
                message.Attachments.Add(pdfb64);

                using (var smtp = new SmtpClient())
                {
                    var credential = new System.Net.NetworkCredential();
                    smtp.UseDefaultCredentials = false;
                    smtp.Credentials = credential;
                    smtp.Host = host;
                    smtp.Port = port;
                    smtp.EnableSsl = false;
                    smtp.Send(message);
                    message.Dispose();
                }
                Return = "success";
            }
            catch (Exception ex)
            {
                Return = ex.Message;
                return Return;
            }
            return Return;
        }

        public string MailMessageCanceledPurchase(SendMail information, Stream stream)
        {
            string Return;
            try
            {
                var user = ConfigurationManager.AppSettings["userEmail"].ToString();
                var host = ConfigurationManager.AppSettings["hostEmail"].ToString();
                var port = int.Parse(ConfigurationManager.AppSettings["portEmail"].ToString());
                var message = new MailMessage();

                var urlFile = System.Web.Hosting.HostingEnvironment.MapPath(@"\Emails\email_canceled_purchase.html");
                //var urlFile = HttpContext.Current.Server.MapPath(@"\Emails\email_canceled_purchase.html");
                string text = File.ReadAllText(urlFile);
                //string text = System.IO.File.ReadAllText("C:\\inetpub\\FloridoERPTX\\FloridoERPTX\\Emails\\email_canceled_purchase.html");

                information.body = HttpUtility.HtmlDecode(text);

                message.To.Add(information.email);
                message.From = new MailAddress(user, information.from);
                message.Subject = information.subject;
                message.IsBodyHtml = true;
                message.Priority = MailPriority.High;

                var htmlBody = AlternateView.CreateAlternateViewFromString(
                   information.body, Encoding.UTF8, "text/html");

                message.AlternateViews.Add(
                    AlternateView.CreateAlternateViewFromString(string.Empty, new ContentType("text/plain")));

                message.AlternateViews.Add(htmlBody);

                var ct = new ContentType
                {
                    MediaType = MediaTypeNames.Application.Pdf,
                    Name = "CanceledPurchaseOrder.pdf"
                };
                stream.Position = 0;
                Attachment pdfb64 = new Attachment(stream, ct);
                pdfb64.ContentType.MediaType = MediaTypeNames.Application.Pdf;
                message.Attachments.Add(pdfb64);

                using (var smtp = new SmtpClient())
                {
                    var credential = new System.Net.NetworkCredential();
                    smtp.UseDefaultCredentials = false;
                    smtp.Credentials = credential;
                    smtp.Host = host;
                    smtp.Port = port;
                    smtp.EnableSsl = false;
                    smtp.Send(message);
                    message.Dispose();
                }
                Return = "success";
            }
            catch (Exception ex)
            {
                Return = ex.Message;
                return Return;
            }
            return Return;
        }

        public string MailMessageRMA005(SendMail information, List<EmailDamagedGoodsModel> stream)
        {
            string Return;
            try
            {
                var user = ConfigurationManager.AppSettings["userEmail"].ToString();
                var host = ConfigurationManager.AppSettings["hostEmail"].ToString();
                var port = int.Parse(ConfigurationManager.AppSettings["portEmail"].ToString());
                var message = new MailMessage();

                //var urlFile = System.Web.Hosting.HostingEnvironment.MapPath(@"\Emails\email_banati_aprobacion.html"); //Cambio aqui el coreo para acelarar el proceso (se elimino aplicar Remision)
                var urlFile = System.Web.Hosting.HostingEnvironment.MapPath(@"\Emails\email_confirmacion_remision.html"); //Cambio aqui el coreo
                string text = File.ReadAllText(urlFile);
                text = text.Replace("{Supplier}", information.Supplier);
                text = text.Replace("{folio}", information.folio);
                text = text.Replace("{store}", information.store);
                information.body = HttpUtility.HtmlDecode(text);

                message.To.Add(information.email);
                message.From = new MailAddress(user, information.from);
                message.Subject = information.subject;
                message.IsBodyHtml = true;
                message.Priority = MailPriority.High;

                var htmlBody = AlternateView.CreateAlternateViewFromString(
                   information.body, Encoding.UTF8, "text/html");

                message.AlternateViews.Add(
                    AlternateView.CreateAlternateViewFromString(string.Empty, new ContentType("text/plain")));

                message.AlternateViews.Add(htmlBody);

                int count = 0;
                List<Attachment> Attachments = new List<Attachment>();
                foreach (var item in stream)
                {
                    var ct = new ContentType
                    {
                        MediaType = MediaTypeNames.Application.Pdf,
                        Name = "Remision_" + item.RMA + ".pdf"
                    };
                    item.Rerpote.Position = 0;
                    Attachment pdfb64 = new Attachment(item.Rerpote, ct);
                    pdfb64.ContentType.MediaType = MediaTypeNames.Application.Pdf;
                    Attachments.Add(pdfb64);
                    count++;
                }
                foreach (Attachment a in Attachments)
                    message.Attachments.Add(a);
                using (var smtp = new SmtpClient())
                {
                    var credential = new System.Net.NetworkCredential();
                    smtp.UseDefaultCredentials = false;
                    smtp.Credentials = credential;
                    smtp.Host = host;
                    smtp.Port = port;
                    smtp.EnableSsl = false;
                    smtp.Send(message);
                    message.Dispose();
                }
                Return = "success";
            }
            catch (Exception ex)
            {
                Return = ex.Message;
                return Return;
            }
            return Return;
        }

        public string MailMessageRMA005Cancel(SendMail information, List<EmailDamagedGoodsModel> stream)
        {
            string Return;
            try
            {
                var user = ConfigurationManager.AppSettings["userEmail"].ToString();
                var host = ConfigurationManager.AppSettings["hostEmail"].ToString();
                var port = int.Parse(ConfigurationManager.AppSettings["portEmail"].ToString());
                var message = new MailMessage();
                //var urlFile = HttpContext.Current.Server.MapPath(@"\Emails\email_confirmacion_remision.html");
                var urlFile = System.Web.Hosting.HostingEnvironment.MapPath(@"\Emails\email_banati_cancelacion.html");
                string text = File.ReadAllText(urlFile);
                text = text.Replace("{folio}", information.folio);
                text = text.Replace("{store}", information.store);
                information.body = HttpUtility.HtmlDecode(text);

                message.To.Add(information.email);
                message.From = new MailAddress(user, information.from);
                message.Subject = information.subject;
                message.IsBodyHtml = true;
                message.Priority = MailPriority.High;

                var htmlBody = AlternateView.CreateAlternateViewFromString(
                   information.body, Encoding.UTF8, "text/html");

                message.AlternateViews.Add(
                    AlternateView.CreateAlternateViewFromString(string.Empty, new ContentType("text/plain")));

                message.AlternateViews.Add(htmlBody);

                int count = 0;
                List<Attachment> Attachments = new List<Attachment>();
                foreach (var item in stream)
                {
                    var ct = new ContentType
                    {
                        MediaType = MediaTypeNames.Application.Pdf,
                        Name = "Remision_" + item.RMA + ".pdf"
                    };
                    item.Rerpote.Position = 0;
                    Attachment pdfb64 = new Attachment(item.Rerpote, ct);
                    pdfb64.ContentType.MediaType = MediaTypeNames.Application.Pdf;
                    Attachments.Add(pdfb64);
                    count++;
                }
                foreach (Attachment a in Attachments)
                    message.Attachments.Add(a);
                using (var smtp = new SmtpClient())
                {
                    var credential = new System.Net.NetworkCredential();
                    smtp.UseDefaultCredentials = false;
                    smtp.Credentials = credential;
                    smtp.Host = host;
                    smtp.Port = port;
                    smtp.EnableSsl = false;
                    smtp.Send(message);
                    message.Dispose();
                }
                Return = "success";
            }
            catch (Exception ex)
            {
                Return = ex.Message;
                return Return;
            }
            return Return;
        }

        public string MailMessageRMASupplier(SendMail information, List<EmailDamagedGoodsModel> stream)
        {
            string Return;
            try
            {
                var user = ConfigurationManager.AppSettings["userEmail"].ToString();
                var host = ConfigurationManager.AppSettings["hostEmail"].ToString();
                var port = int.Parse(ConfigurationManager.AppSettings["portEmail"].ToString());
                var message = new MailMessage();
                //var urlFile = HttpContext.Current.Server.MapPath(@"\Emails\email_confirmacion_remision.html");
                var urlFile = System.Web.Hosting.HostingEnvironment.MapPath(@"\Emails\email_confirmacion_remision.html");
                string text = File.ReadAllText(urlFile);
                //string text = System.IO.File.ReadAllText("C:\\inetpub\\FloridoERPTX\\FloridoERPTX\\Emails\\email_confirmacion_remision.html");

                text = text.Replace("{Supplier}", information.Supplier);
                text = text.Replace("{folio}", information.folio);
                text = text.Replace("{store}", information.store);
                information.body = HttpUtility.HtmlDecode(text);

                message.To.Add(information.email);
                message.From = new MailAddress(user, information.from);
                message.Subject = information.subject;
                message.IsBodyHtml = true;
                message.Priority = MailPriority.High;

                var htmlBody = AlternateView.CreateAlternateViewFromString(
                   information.body, Encoding.UTF8, "text/html");

                message.AlternateViews.Add(
                    AlternateView.CreateAlternateViewFromString(string.Empty, new ContentType("text/plain")));

                message.AlternateViews.Add(htmlBody);

                int count = 0;
                List<Attachment> Attachments = new List<Attachment>();
                foreach (var item in stream)
                {
                    var ct = new ContentType
                    {
                        MediaType = MediaTypeNames.Application.Pdf,
                        Name = "Devolucion_" + item.RMA + ".pdf"
                    };
                    item.Rerpote.Position = 0;
                    Attachment pdfb64 = new Attachment(item.Rerpote, ct);
                    pdfb64.ContentType.MediaType = MediaTypeNames.Application.Pdf;
                    Attachments.Add(pdfb64);
                    count++;
                }
                foreach (Attachment a in Attachments)
                    message.Attachments.Add(a);
                using (var smtp = new SmtpClient())
                {
                    var credential = new System.Net.NetworkCredential();
                    smtp.UseDefaultCredentials = false;
                    smtp.Credentials = credential;
                    smtp.Host = host;
                    smtp.Port = port;
                    smtp.EnableSsl = false;
                    smtp.Send(message);
                    message.Dispose();
                }
                Return = "success";
            }
            catch (Exception ex)
            {
                Return = ex.Message;
                return Return;
            }
            return Return;
        }

        public string MailMessageEntryFreeSupplier(SendMail information, Stream stream)
        {
            string Return;
            try
            {
                var user = ConfigurationManager.AppSettings["userEmail"].ToString();
                var host = ConfigurationManager.AppSettings["hostEmail"].ToString();
                var port = int.Parse(ConfigurationManager.AppSettings["portEmail"].ToString());
                var message = new MailMessage();

                //  string text = System.IO.File.ReadAllText("C:\\inetpub\\ERP2017\\saidfl_erp\\FloridoERP\\Emails\\email_template.html");
                //string currentDirectory = Directory.GetCurrentDirectory();
                //string no = Path.Combine(Path.Combine(Path.GetDirectoryName(Assembly.GetExkecutingAssembly().Location)), @"Emails\\email_entryFree.html");
                var urlFile = System.Web.Hosting.HostingEnvironment.MapPath(@"\Emails\email_supplier.html");
                string text = File.ReadAllText(urlFile);
                //string text = System.IO.File.ReadAllText("C:\\inetpub\\FloridoERPTX\\FloridoERPTX\\Emails\\email_supplier.html"/*"C:\\inetpub\\FloridoERPTX\\FloridoERPTX\\Emails\\email_supplier.html"*/);//Path.Combine(@"~/Emails//email_entryFree.html");// System.IO.File.ReadAllText("C:\\proyects\\erp_florido_tienda\\FloridoERPTX\\Emails\\email_entryFree.html");

                text = text.Replace("{Folio}", information.folio);
                text = text.Replace("{Proveedor}", information.store);

                information.body = HttpUtility.HtmlDecode(text);

                message.To.Add(information.email);
                message.From = new MailAddress(user, information.from);
                message.Subject = information.subject;
                message.IsBodyHtml = true;
                message.Priority = MailPriority.High;

                //message.Attachments.Add(new Attachment(n, Path.GetFileName("prueba.pdf"), "application/pdf"));

                var htmlBody = AlternateView.CreateAlternateViewFromString(
                   information.body, Encoding.UTF8, "text/html");

                message.AlternateViews.Add(
                    AlternateView.CreateAlternateViewFromString(string.Empty, new ContentType("text/plain")));

                message.AlternateViews.Add(htmlBody);

                var ct = new ContentType
                {
                    MediaType = MediaTypeNames.Application.Pdf,
                    Name = "EntradaDeMercancia.pdf"
                };
                stream.Position = 0;
                Attachment pdfb64 = new Attachment(stream, ct);
                //pdfb64.TransferEncoding = TransferEncoding.Base64;
                pdfb64.ContentType.MediaType = MediaTypeNames.Application.Pdf;
                message.Attachments.Add(pdfb64);

                using (var smtp = new SmtpClient())
                {
                    var credential = new System.Net.NetworkCredential();
                    smtp.UseDefaultCredentials = false;
                    smtp.Credentials = credential;
                    smtp.Host = host;
                    smtp.Port = port;
                    smtp.EnableSsl = false;
                    smtp.Send(message);
                    message.Dispose();
                }
                Return = "success";
            }
            catch (Exception ex)
            {
                Return = ex.Message;
                return Return;
            }
            return Return;
        }

        public string generateTable(List<Entities.ViewModels.PurchaseOrder.PurchaseOrderModel> InformationPurchase)
        {
            var strBody = "";

            foreach (var item in InformationPurchase)
            {
                strBody += "<tr><td>" + item.PartNumber + "</td>";
                strBody += "<td>" + item.Description + "</td>";
                strBody += "<td>" + item.Quantity + "</td>";
                strBody += "<td>" + item.UnitSize + "</td>";
                strBody += "<td>$ " + item.PurchasePrice + "</td>";
                strBody += "<td>$ " + item.Iva + "</td>";
                strBody += "<td>$ " + item.Ieps + "</td>";
                strBody += "<td>$ " + item.ItemAmount + "</td>";
                strBody += "<td>$ " + item.ItemTotalAmount + "</td></tr>";
            }
            return strBody;
        }

        public string MailMessageSalidaMerma(SendMail information, Stream stream)
        {
            string Return;
            try
            {
                var user = ConfigurationManager.AppSettings["userEmail"].ToString();
                var host = ConfigurationManager.AppSettings["hostEmail"].ToString();
                var port = int.Parse(ConfigurationManager.AppSettings["portEmail"].ToString());
                var message = new MailMessage();
                var urlFile = System.Web.Hosting.HostingEnvironment.MapPath(@"\Emails\email_solicitud_salida_mercancia_merma.html");
                string text = File.ReadAllText(urlFile);
                //string text = System.IO.File.ReadAllText("C:\\inetpub\\FloridoERPTX\\FloridoERPTX\\Emails\\email_solicitud_salida_mercancia_merma.html");
                text = text.Replace("{folio}", information.folio);

                information.body = HttpUtility.HtmlDecode(text);

                message.To.Add(information.email);
                message.From = new MailAddress(user, information.from);
                message.Subject = information.subject;
                message.IsBodyHtml = true;
                message.Priority = MailPriority.High;

                var htmlBody = AlternateView.CreateAlternateViewFromString(
                   information.body, Encoding.UTF8, "text/html");

                message.AlternateViews.Add(
                    AlternateView.CreateAlternateViewFromString(string.Empty, new ContentType("text/plain")));

                message.AlternateViews.Add(htmlBody);

                var ct = new ContentType
                {
                    MediaType = MediaTypeNames.Application.Pdf,
                    Name = "solicitud_salidamerma.pdf"
                };
                stream.Position = 0;
                Attachment pdfb64 = new Attachment(stream, ct);
                pdfb64.ContentType.MediaType = MediaTypeNames.Application.Pdf;
                message.Attachments.Add(pdfb64);

                using (var smtp = new SmtpClient())
                {
                    var credential = new System.Net.NetworkCredential();
                    smtp.UseDefaultCredentials = false;
                    smtp.Credentials = credential;
                    smtp.Host = host;
                    smtp.Port = port;
                    smtp.EnableSsl = true;
                    smtp.Send(message);
                    message.Dispose();
                }
                Return = "success";
            }
            catch (Exception ex)
            {
                Return = ex.Message;
                return Return;
            }
            return Return;
        }

        public string MailMessageSolicitudAjusteInventario(SendMail information, Stream stream)
        {
            string Return;
            try
            {
                var user = ConfigurationManager.AppSettings["userEmail"].ToString();
                var host = ConfigurationManager.AppSettings["hostEmail"].ToString();
                var port = int.Parse(ConfigurationManager.AppSettings["portEmail"].ToString());
                var message = new MailMessage();

                var urlFile = System.Web.Hosting.HostingEnvironment.MapPath(@"\Emails\email_solicitud_ajuste_inventario.html");
                string text = File.ReadAllText(urlFile);
                //string text = System.IO.File.ReadAllText("C:\\inetpub\\FloridoERPTX\\FloridoERPTX\\Emails\\email_solicitud_ajuste_inventario.html");
                text = text.Replace("{folio}", information.folio);

                information.body = HttpUtility.HtmlDecode(text);

                message.To.Add(information.email);
                message.From = new MailAddress(user, information.from);
                message.Subject = information.subject;
                message.IsBodyHtml = true;
                message.Priority = MailPriority.High;

                var htmlBody = AlternateView.CreateAlternateViewFromString(
                   information.body, Encoding.UTF8, "text/html");

                message.AlternateViews.Add(
                    AlternateView.CreateAlternateViewFromString(string.Empty, new ContentType("text/plain")));

                message.AlternateViews.Add(htmlBody);

                var ct = new ContentType
                {
                    MediaType = MediaTypeNames.Application.Pdf,
                    Name = "solicitud_ajuste_inventario.pdf"
                };
                stream.Position = 0;
                Attachment pdfb64 = new Attachment(stream, ct);
                pdfb64.ContentType.MediaType = MediaTypeNames.Application.Pdf;
                message.Attachments.Add(pdfb64);

                using (var smtp = new SmtpClient())
                {
                    var credential = new System.Net.NetworkCredential();
                    smtp.UseDefaultCredentials = false;
                    smtp.Credentials = credential;
                    smtp.Host = host;
                    smtp.Port = port;
                    smtp.EnableSsl = true;
                    smtp.Send(message);
                    message.Dispose();
                }
                Return = "success";
            }
            catch (Exception ex)
            {
                Return = ex.Message;
                return Return;
            }
            return Return;
        }

        public string MailMessageSalidaMermaAprobada(SendMail information)
        {
            string Return;
            try
            {
                var user = ConfigurationManager.AppSettings["userEmail"].ToString();
                var host = ConfigurationManager.AppSettings["hostEmail"].ToString();
                var port = int.Parse(ConfigurationManager.AppSettings["portEmail"].ToString());
                var message = new MailMessage();

                var urlFile = System.Web.Hosting.HostingEnvironment.MapPath(@"\Emails\email_aprobacion_salida_merma.html");
                string text = File.ReadAllText(urlFile);
                //string text = System.IO.File.ReadAllText("C:\\Users\\Sistemas\\Desktop\\erp_florido_tienda\\FloridoERPTX\\Emails\\email_aprobacion_salida_merma.html");
                text = text.Replace("{folio}", information.folio);

                information.body = HttpUtility.HtmlDecode(text);

                message.To.Add(information.email);
                message.From = new MailAddress(user, information.from);
                message.Subject = information.subject;
                message.IsBodyHtml = true;
                message.Priority = MailPriority.High;

                var htmlBody = AlternateView.CreateAlternateViewFromString(
                   information.body, Encoding.UTF8, "text/html");

                message.AlternateViews.Add(
                    AlternateView.CreateAlternateViewFromString(string.Empty, new ContentType("text/plain")));

                message.AlternateViews.Add(htmlBody);

                using (var smtp = new SmtpClient())
                {
                    var credential = new System.Net.NetworkCredential();
                    smtp.UseDefaultCredentials = false;
                    smtp.Credentials = credential;
                    smtp.Host = host;
                    smtp.Port = port;
                    smtp.EnableSsl = true;
                    smtp.Send(message);
                    message.Dispose();
                }
                Return = "success";
            }
            catch (Exception ex)
            {
                Return = ex.Message;
                return Return;
            }
            return Return;
        }

        public string MailMessageSalidaMermaRechazada(SendMail information)
        {
            string Return;
            try
            {
                var user = ConfigurationManager.AppSettings["userEmail"].ToString();
                var host = ConfigurationManager.AppSettings["hostEmail"].ToString();
                var port = int.Parse(ConfigurationManager.AppSettings["portEmail"].ToString());
                var message = new MailMessage();

                var urlFile = System.Web.Hosting.HostingEnvironment.MapPath(@"\Emails\email_rechazo_salida_merma.html");
                string text = File.ReadAllText(urlFile);
                //string text = System.IO.File.ReadAllText("C:\\Users\\Sistemas\\Desktop\\erp_florido_tienda\\FloridoERPTX\\Emails\\email_rechazo_salida_merma.html");
                text = text.Replace("{folio}", information.folio);

                information.body = HttpUtility.HtmlDecode(text);

                message.To.Add(information.email);
                message.From = new MailAddress(user, information.from);
                message.Subject = information.subject;
                message.IsBodyHtml = true;
                message.Priority = MailPriority.High;

                var htmlBody = AlternateView.CreateAlternateViewFromString(
                   information.body, Encoding.UTF8, "text/html");

                message.AlternateViews.Add(
                    AlternateView.CreateAlternateViewFromString(string.Empty, new ContentType("text/plain")));

                message.AlternateViews.Add(htmlBody);

                using (var smtp = new SmtpClient())
                {
                    var credential = new System.Net.NetworkCredential();
                    smtp.UseDefaultCredentials = false;
                    smtp.Credentials = credential;
                    smtp.Host = host;
                    smtp.Port = port;
                    smtp.EnableSsl = true;
                    smtp.Send(message);
                    message.Dispose();
                }
                Return = "success";
            }
            catch (Exception ex)
            {
                Return = ex.Message;
                return Return;
            }
            return Return;
        }

        public string MailMessageSalidaMermaAplicada(SendMail information, Stream stream)
        {
            string Return;
            try
            {
                var user = ConfigurationManager.AppSettings["userEmail"].ToString();
                var host = ConfigurationManager.AppSettings["hostEmail"].ToString();
                var port = int.Parse(ConfigurationManager.AppSettings["portEmail"].ToString());

                var message = new MailMessage();

                var urlFile = System.Web.Hosting.HostingEnvironment.MapPath(@"\Emails\email_aplicacion_salida_merma.html");
                string text = File.ReadAllText(urlFile);
                //string text = System.IO.File.ReadAllText("C:\\Users\\Sistemas\\Desktop\\erp_florido_tienda\\FloridoERPTX\\Emails\\email_aplicacion_salida_merma.html");
                text = text.Replace("{folio}", information.folio);

                information.body = HttpUtility.HtmlDecode(text);

                message.To.Add(information.email);
                message.From = new MailAddress(user, information.from);
                message.Subject = information.subject;
                message.IsBodyHtml = true;
                message.Priority = MailPriority.High;

                var htmlBody = AlternateView.CreateAlternateViewFromString(
                   information.body, Encoding.UTF8, "text/html");

                message.AlternateViews.Add(
                    AlternateView.CreateAlternateViewFromString(string.Empty, new ContentType("text/plain")));

                message.AlternateViews.Add(htmlBody);

                var ct = new ContentType
                {
                    MediaType = MediaTypeNames.Application.Pdf,
                    Name = "aplicacion_salida_merma.pdf"
                };
                stream.Position = 0;
                Attachment pdfb64 = new Attachment(stream, ct);
                pdfb64.ContentType.MediaType = MediaTypeNames.Application.Pdf;
                message.Attachments.Add(pdfb64);

                using (var smtp = new SmtpClient())
                {
                    var credential = new System.Net.NetworkCredential();
                    smtp.UseDefaultCredentials = false;
                    smtp.Credentials = credential;
                    smtp.Host = host;
                    smtp.Port = port;
                    smtp.EnableSsl = true;
                    smtp.Send(message);
                    message.Dispose();
                }
                Return = "success";
            }
            catch (Exception ex)
            {
                Return = ex.Message;
                return Return;
            }
            return Return;
        }

        public string MailMessageRemisionConfirmacion(SendMail information, List<EmailDamagedGoodsModel> stream)
        {
            string Return;
            try
            {
                var user = ConfigurationManager.AppSettings["userEmail"].ToString();
                var host = ConfigurationManager.AppSettings["hostEmail"].ToString();
                var port = int.Parse(ConfigurationManager.AppSettings["portEmail"].ToString());
                var message = new MailMessage();

                var urlFile = HttpContext.Current.Server.MapPath(@"\Emails\email_confirmacion_remision.html");
                string text = File.ReadAllText(urlFile);
                //string text = System.IO.File.ReadAllText("C:\\inetpub\\FloridoERPTX\\FloridoERPTX\\Emails\\email_confirmacion_remision.html");
                text = text.Replace("{proveedor}", information.store);

                information.body = HttpUtility.HtmlDecode(text);

                message.To.Add(information.email);
                message.From = new MailAddress(user, information.from);
                message.Subject = information.subject;
                message.IsBodyHtml = true;
                message.Priority = MailPriority.High;

                var htmlBody = AlternateView.CreateAlternateViewFromString(
                   information.body, Encoding.UTF8, "text/html");

                message.AlternateViews.Add(
                    AlternateView.CreateAlternateViewFromString(string.Empty, new ContentType("text/plain")));

                message.AlternateViews.Add(htmlBody);

                int count = 0;
                List<Attachment> Attachments = new List<Attachment>();
                foreach (var item in stream)
                {
                    var ct = new ContentType();
                    ct.MediaType = MediaTypeNames.Application.Pdf;
                    ct.Name = "Devolucion_" + item.RMA + ".pdf";
                    item.Rerpote.Position = 0;
                    Attachment pdfb64 = new Attachment(item.Rerpote, ct);
                    pdfb64.ContentType.MediaType = MediaTypeNames.Application.Pdf;
                    Attachments.Add(pdfb64);
                    count++;
                }
                foreach (Attachment a in Attachments)
                    message.Attachments.Add(a);
                using (var smtp = new SmtpClient())
                {
                    var credential = new System.Net.NetworkCredential();
                    smtp.UseDefaultCredentials = false;
                    smtp.Credentials = credential;
                    smtp.Host = host;
                    smtp.Port = port;
                    smtp.EnableSsl = false;
                    smtp.Send(message);
                    message.Dispose();
                }
                Return = "success";
            }
            catch (Exception ex)
            {
                Return = ex.Message;
                return Return;
            }
            return Return;
        }

        public string MailMessageRemisionConfirmacionSingleRM002(SendMail information, EmailDamagedGoodsModel stream)
        {
            string Return;
            try
            {
                var user = ConfigurationManager.AppSettings["userEmail"].ToString();
                var host = ConfigurationManager.AppSettings["hostEmail"].ToString();
                var port = int.Parse(ConfigurationManager.AppSettings["portEmail"].ToString());
                var message = new MailMessage();

                var urlFile = System.Web.Hosting.HostingEnvironment.MapPath(@"\Emails\email_rma_aprobado.html");
                string text = File.ReadAllText(urlFile);
                //string text = System.IO.File.ReadAllText("C:\\inetpub\\FloridoERPTX\\FloridoERPTX\\Emails\\email_rmasupplier.html");

                text = text.Replace("{Supplier}", information.Supplier);
                text = text.Replace("{folio}", information.folio);
                text = text.Replace("{store}", information.store);

                information.body = HttpUtility.HtmlDecode(text);

                message.To.Add(information.email);
                message.From = new MailAddress(user, information.from);
                message.Subject = information.subject;
                message.IsBodyHtml = true;
                message.Priority = MailPriority.High;

                var htmlBody = AlternateView.CreateAlternateViewFromString(
                   information.body, Encoding.UTF8, "text/html");

                message.AlternateViews.Add(
                    AlternateView.CreateAlternateViewFromString(string.Empty, new ContentType("text/plain")));

                message.AlternateViews.Add(htmlBody);

                List<Attachment> Attachments = new List<Attachment>();
                var ct = new ContentType
                {
                    MediaType = MediaTypeNames.Application.Pdf,
                    Name = "Devolucion_" + stream.RMA + ".pdf"
                };
                stream.Rerpote.Position = 0;
                Attachment pdfb64 = new Attachment(stream.Rerpote, ct);
                pdfb64.ContentType.MediaType = MediaTypeNames.Application.Pdf;
                message.Attachments.Add(pdfb64);
                using (var smtp = new SmtpClient())
                {
                    var credential = new System.Net.NetworkCredential();
                    smtp.UseDefaultCredentials = false;
                    smtp.Credentials = credential;
                    smtp.Host = host;
                    smtp.Port = port;
                    smtp.EnableSsl = false;
                    smtp.Send(message);
                    message.Dispose();
                }
                Return = "success";
            }
            catch (Exception ex)
            {
                Return = ex.Message;
                return Return;
            }
            return Return;
        }

        public string MailMessageRemisionCancelacionSingleRM002(SendMail information, EmailDamagedGoodsModel stream)
        {
            string Return;
            try
            {
                var user = ConfigurationManager.AppSettings["userEmail"].ToString();
                var host = ConfigurationManager.AppSettings["hostEmail"].ToString();
                var port = int.Parse(ConfigurationManager.AppSettings["portEmail"].ToString());
                var message = new MailMessage();

                var urlFile = System.Web.Hosting.HostingEnvironment.MapPath(@"\Emails\email_rma_rma002_cancel.html");
                string text = File.ReadAllText(urlFile);
                //string text = System.IO.File.ReadAllText("C:\\inetpub\\FloridoERPTX\\FloridoERPTX\\Emails\\email_rma_rma002_cancel.html");

                text = text.Replace("{folio}", information.folio);

                information.body = HttpUtility.HtmlDecode(text);

                message.To.Add(information.email);
                message.From = new MailAddress(user, information.from);
                message.Subject = information.subject;
                message.IsBodyHtml = true;
                message.Priority = MailPriority.High;

                var htmlBody = AlternateView.CreateAlternateViewFromString(
                   information.body, Encoding.UTF8, "text/html");

                message.AlternateViews.Add(
                    AlternateView.CreateAlternateViewFromString(string.Empty, new ContentType("text/plain")));

                message.AlternateViews.Add(htmlBody);

                List<Attachment> Attachments = new List<Attachment>();
                var ct = new ContentType
                {
                    MediaType = MediaTypeNames.Application.Pdf,
                    Name = "Devolucion_" + stream.RMA + ".pdf"
                };
                stream.Rerpote.Position = 0;
                Attachment pdfb64 = new Attachment(stream.Rerpote, ct);
                pdfb64.ContentType.MediaType = MediaTypeNames.Application.Pdf;
                message.Attachments.Add(pdfb64);
                using (var smtp = new SmtpClient())
                {
                    var credential = new System.Net.NetworkCredential();
                    smtp.UseDefaultCredentials = false;
                    smtp.Credentials = credential;
                    smtp.Host = host;
                    smtp.Port = port;
                    smtp.EnableSsl = false;
                    smtp.Send(message);
                    message.Dispose();
                }
                Return = "success";
            }
            catch (Exception ex)
            {
                Return = ex.Message;
                return Return;
            }
            return Return;
        }

        public string MailMessageRemisionConfirmacionSingle(SendMail information, EmailDamagedGoodsModel stream)
        {
            string Return = string.Empty;
            try
            {
                var user = ConfigurationManager.AppSettings["userEmail"].ToString();
                var host = ConfigurationManager.AppSettings["hostEmail"].ToString();
                var port = int.Parse(ConfigurationManager.AppSettings["portEmail"].ToString());
                var message = new MailMessage();

                var urlFile = System.Web.Hosting.HostingEnvironment.MapPath(@"\Emails\email_rmasupplier.html");
                string text = File.ReadAllText(urlFile);
                //string text = System.IO.File.ReadAllText("C:\\inetpub\\FloridoERPTX\\FloridoERPTX\\Emails\\email_rmasupplier.html");

                text = text.Replace("{Supplier}", information.Supplier);
                text = text.Replace("{store}", information.store);
                text = text.Replace("{folio}", information.folio);

                information.body = HttpUtility.HtmlDecode(text);

                message.To.Add(information.email);
                message.From = new MailAddress(user, information.from);
                message.Subject = information.subject;
                message.IsBodyHtml = true;
                message.Priority = MailPriority.High;

                var htmlBody = AlternateView.CreateAlternateViewFromString(
                   information.body, Encoding.UTF8, "text/html");

                message.AlternateViews.Add(
                    AlternateView.CreateAlternateViewFromString(string.Empty, new ContentType("text/plain")));

                message.AlternateViews.Add(htmlBody);

                List<Attachment> Attachments = new List<Attachment>();
                var ct = new ContentType
                {
                    MediaType = MediaTypeNames.Application.Pdf,
                    Name = "Devolucion_" + stream.RMA + ".pdf"
                };
                stream.Rerpote.Position = 0;
                Attachment pdfb64 = new Attachment(stream.Rerpote, ct);
                pdfb64.ContentType.MediaType = MediaTypeNames.Application.Pdf;
                message.Attachments.Add(pdfb64);
                using (var smtp = new SmtpClient())
                {
                    var credential = new System.Net.NetworkCredential();
                    smtp.UseDefaultCredentials = false;
                    smtp.Credentials = credential;
                    smtp.Host = host;
                    smtp.Port = port;
                    smtp.EnableSsl = false;
                    smtp.Send(message);
                    message.Dispose();
                }
                Return = "success";
            }
            catch (Exception ex)
            {
                Return = ex.Message;
                return Return;
            }
            return Return;
        }

        public string MailTransferSite(SendMail information, Stream stream, Stream extraFile =null)
        {
            string Return;
            try
            {
                var user = ConfigurationManager.AppSettings["userEmail"].ToString();
                var host = ConfigurationManager.AppSettings["hostEmail"].ToString();
                var port = int.Parse(ConfigurationManager.AppSettings["portEmail"].ToString());
                var message = new MailMessage();

                var urlFile = System.Web.Hosting.HostingEnvironment.MapPath(@"\Emails\email_transfer_site.html");
                string text = File.ReadAllText(urlFile);
                //string text = System.IO.File.ReadAllText("C:\\inetpub\\FloridoERPTX\\FloridoERPTX\\Emails\\email_transfer_site.html");

                text = text.Replace("{folio}", information.folio);

                information.body = HttpUtility.HtmlDecode(text);

                //message.To.Add(new MailAddress(information.email));
                message.To.Add(information.email);
                message.From = new MailAddress(user, information.from);
                message.Subject = information.subject;
                message.IsBodyHtml = true;
                message.Priority = MailPriority.High;


                var htmlBody = AlternateView.CreateAlternateViewFromString(
                   information.body, Encoding.UTF8, "text/html");

                message.AlternateViews.Add(
                    AlternateView.CreateAlternateViewFromString(string.Empty, new ContentType("text/plain")));

                message.AlternateViews.Add(htmlBody);

                var ct = new ContentType
                {
                    MediaType = MediaTypeNames.Application.Pdf,
                    Name = "notificacion_transferencia.pdf"
                };
                stream.Position = 0;
                Attachment pdfb64 = new Attachment(stream, ct);
                pdfb64.ContentType.MediaType = MediaTypeNames.Application.Pdf;
                message.Attachments.Add(pdfb64);
                if (extraFile != null)
                {
                    var ct2 = new ContentType();
                    ct2.MediaType = MediaTypeNames.Text.Plain;
                    ct2.Name = "transfer_data";
                    extraFile.Position = 0;
                    Attachment pdfb642 = new Attachment(extraFile, ct2);
                    pdfb642.ContentType.MediaType = MediaTypeNames.Text.Plain;
                    message.Attachments.Add(pdfb642);
                }
                using (var smtp = new SmtpClient())
                {
                    var credential = new System.Net.NetworkCredential();
                    smtp.UseDefaultCredentials = false;
                    smtp.Credentials = credential;
                    smtp.Host = host;
                    smtp.Port = port;
                    smtp.EnableSsl = true;
                    smtp.Send(message);
                    message.Dispose();
                }
                Return = "success";
            }
            catch (Exception ex)
            {
                Return = ex.Message;
                return Return;
            }
            return Return;
        }

        public string MailTransferCancelRequest(SendMail information, Stream stream)
        {
            try
            {
                var user = ConfigurationManager.AppSettings["userEmail"].ToString();
                var host = ConfigurationManager.AppSettings["hostEmail"].ToString();
                var port = int.Parse(ConfigurationManager.AppSettings["portEmail"].ToString());
                var message = new MailMessage();

                var urlFile = System.Web.Hosting.HostingEnvironment.MapPath(@"\Emails\email_transfer_cancel_request.html");
                string text = File.ReadAllText(urlFile);
                //string text = System.IO.File.ReadAllText("C:\\inetpub\\FloridoERPTX\\FloridoERPTX\\Emails\\email_transfer_site.html");

                text = text.Replace("{folio}", information.folio);

                information.body = HttpUtility.HtmlDecode(text);

                message.To.Add(information.email);
                message.From = new MailAddress(user, information.from);
                message.Subject = information.subject;
                message.IsBodyHtml = true;
                message.Priority = MailPriority.High;

                var htmlBody = AlternateView.CreateAlternateViewFromString(
                   information.body, Encoding.UTF8, "text/html");

                message.AlternateViews.Add(AlternateView.CreateAlternateViewFromString(string.Empty, new ContentType("text/plain")));
                message.AlternateViews.Add(htmlBody);

                var ct = new ContentType
                {
                    MediaType = MediaTypeNames.Application.Pdf,
                    Name = "cancelacion_transferencia.pdf"
                };

                stream.Position = 0;
                Attachment pdfb64 = new Attachment(stream, ct);
                pdfb64.ContentType.MediaType = MediaTypeNames.Application.Pdf;
                message.Attachments.Add(pdfb64);

                using (var smtp = new SmtpClient())
                {
                    var credential = new System.Net.NetworkCredential();
                    smtp.UseDefaultCredentials = false;
                    smtp.Credentials = credential;
                    smtp.Host = host;
                    smtp.Port = port;
                    smtp.EnableSsl = true;
                    smtp.Send(message);
                    message.Dispose();
                }

                return "success";
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }

        private static string SplitEncodedAttachmentName(string encoded)
        {
            const string encodingtoken = "=?UTF-8?B?";
            const string softbreak = "?=";
            const int maxChunkLength = 30;
            int splitLength = maxChunkLength - encodingtoken.Length - (softbreak.Length * 2);
            IEnumerable<string> parts = SplitByLength(encoded, splitLength);
            string encodedAttachmentName = encodingtoken;
            foreach (var part in parts)
            {
                encodedAttachmentName += part + softbreak + encodingtoken;
            }
            encodedAttachmentName = encodedAttachmentName.Remove(encodedAttachmentName.Length - encodingtoken.Length, encodingtoken.Length);
            return encodedAttachmentName;
        }

        private static IEnumerable<string> SplitByLength(string stringToSplit, int length)
        {
            while (stringToSplit.Length > length)
            {
                yield return stringToSplit.Substring(0, length);
                stringToSplit = stringToSplit.Substring(length);
            }
            if (stringToSplit.Length > 0)
            {
                yield return stringToSplit;
            }
        }

        /*correcion de lo de francisco*/
        public string MailMessageOrderSingle(SendMail information, EmailDamagedGoodsModel stream, string storeFlorido)
        {
            string Return;
            try
            {
                var user = ConfigurationManager.AppSettings["userEmail"].ToString();
                var host = ConfigurationManager.AppSettings["hostEmail"].ToString();
                var port = int.Parse(ConfigurationManager.AppSettings["portEmail"].ToString());
                var message = new MailMessage();
                var urlFile = System.Web.Hosting.HostingEnvironment.MapPath(@"\Emails\email_order.html");
                string text = File.ReadAllText(urlFile);
                //string text = System.IO.File.ReadAllText("C:\\inetpub\\FloridoERPTX\\FloridoERPTX\\Emails\\email_order.html");

                storeFlorido = storeFlorido.Replace(" ", "_");
                text = text.Replace("{folio}", information.folio);
                text = text.Replace("{date}", information.date);
                text = text.Replace("{currency}", information.currecy);

                information.body = HttpUtility.HtmlDecode(text);

                message.To.Add(information.email);
                message.From = new MailAddress(user, information.from);
                message.Subject = information.subject;
                message.IsBodyHtml = true;
                message.Priority = MailPriority.High;


                var htmlBody = AlternateView.CreateAlternateViewFromString(
                   information.body, Encoding.UTF8, "text/html");

                message.AlternateViews.Add(
                    AlternateView.CreateAlternateViewFromString(string.Empty, new ContentType("text/plain")));

                message.AlternateViews.Add(htmlBody);


                List<Attachment> Attachments = new List<Attachment>();
                var ct = new ContentType
                {
                    MediaType = MediaTypeNames.Application.Pdf,
                    Name = storeFlorido + "_PreOrden_" + stream.RMA + ".pdf"
                };
                stream.Rerpote.Position = 0;
                Attachment pdfb64 = new Attachment(stream.Rerpote, ct);
                pdfb64.ContentType.MediaType = MediaTypeNames.Application.Pdf;
                message.Attachments.Add(pdfb64);
                //var ct2 = new ContentType
                //{
                //    MediaType = MediaTypeNames.Application.Octet,
                //    Name = storeFlorido + "_PreOrden_" + stream.RMA + ".xlsx"
                //};
                //stream.ReporteExcel.Position = 1;
                //Attachment pdfb64Excel = new Attachment(stream.ReporteExcel, ct2);
                //pdfb64Excel.ContentType.MediaType = MediaTypeNames.Application.Octet;
                //message.Attachments.Add(pdfb64Excel);
                ////Opcion 1
                //var ctExcel = new ContentType();
                //ctExcel.MediaType = "application/excel";//"application/vnd.ms-excel"//application/excel
                //ctExcel.Name = storeFlorido + "_PreOrden_" + stream.RMA + ".xlsx";
                //stream.ReporteExcel.Position = 1;
                //Attachment excelb64 = new Attachment(stream.ReporteExcel, ctExcel);
                //excelb64.ContentType.MediaType = "application/vnd.ms-excel";
                //message.Attachments.Add(excelb64);
                ////Opcion 2
                //var attachment = new Attachment(stream.ReporteExcel, storeFlorido + "_PreOrden_" + stream.RMA + ".xlsx");
                //attachment.ContentType = new ContentType("application/excel");
                //attachment.TransferEncoding = TransferEncoding.Base64;
                //attachment.NameEncoding = Encoding.UTF8;
                //stream.ReporteExcel.Position = 1;
                ////Opcion 3
                //string encodedAttachmentName = Convert.ToBase64String(Encoding.UTF8.GetBytes(storeFlorido + "_PreOrden_" + stream.RMA + ".xlsx"));
                //encodedAttachmentName = SplitEncodedAttachmentName(encodedAttachmentName);
                //attachment.Name = encodedAttachmentName;
                //message.Attachments.Add(attachment);
                //// Opcion 4
                //stream.ReporteExcel.Position = 1;
                //Attachment attachment = new Attachment(stream.ReporteExcel, "fileName.xlss");
                //attachment.ContentType = new ContentType("application/vnd.ms-excel");
                //message.Attachments.Add(attachment);
                //////Opcion 5
                //stream.ReporteExcel.Position = 1;
                //message.Attachments.Add(new Attachment(stream.ReporteExcel, storeFlorido + "_PreOrden_" + stream.RMA + ".xlsx", "application/ms-excel"));
                //message.Attachments.Add(new Attachment(stream.ReporteExcel, storeFlorido + "_PreOrden_" + stream.RMA + ".xlsx", "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"));
                //message.Attachments.Add(new Attachment(stream.ReporteExcel, storeFlorido + "_PreOrden_" + stream.RMA + ".xlsx", "application/vnd.ms-excel"));

                using (var smtp = new SmtpClient())
                {
                    var credential = new System.Net.NetworkCredential();
                    smtp.UseDefaultCredentials = false;
                    smtp.Credentials = credential;
                    smtp.Host = host;
                    smtp.Port = port;
                    smtp.EnableSsl = false;
                    smtp.Send(message);
                    message.Dispose();
                }
                Return = "success";
            }
            catch (Exception ex)
            {
                Return = ex.Message;
                return Return;
            }
            return Return;
        }

        public string MailMessageOrderSingleOrdered(SendMail information, EmailDamagedGoodsModel stream, string storeFlorido)
        {
            string Return;
            try
            {
                var user = ConfigurationManager.AppSettings["userEmail"].ToString();
                var host = ConfigurationManager.AppSettings["hostEmail"].ToString();
                var port = int.Parse(ConfigurationManager.AppSettings["portEmail"].ToString());
                var message = new MailMessage();
                var urlFile = System.Web.Hosting.HostingEnvironment.MapPath(@"\Emails\email_ordered.html");
                string text = File.ReadAllText(urlFile);

                storeFlorido = storeFlorido.Replace(" ", "_");
                text = text.Replace("{folio}", information.folio);
                text = text.Replace("{date}", information.date);
                text = text.Replace("{currency}", information.currecy);

                information.body = HttpUtility.HtmlDecode(text);

                message.To.Add(information.email);
                message.From = new MailAddress(user, information.from);
                message.Subject = information.subject;
                message.IsBodyHtml = true;
                message.Priority = MailPriority.High;


                var htmlBody = AlternateView.CreateAlternateViewFromString(
                   information.body, Encoding.UTF8, "text/html");

                message.AlternateViews.Add(
                    AlternateView.CreateAlternateViewFromString(string.Empty, new ContentType("text/plain")));

                message.AlternateViews.Add(htmlBody);


                List<Attachment> Attachments = new List<Attachment>();
                var ct = new ContentType
                {
                    MediaType = MediaTypeNames.Application.Pdf,
                    Name = storeFlorido + "_Entrada_" + stream.RMA + ".pdf"
                };
                stream.Rerpote.Position = 0;
                Attachment pdfb64 = new Attachment(stream.Rerpote, ct);
                pdfb64.ContentType.MediaType = MediaTypeNames.Application.Pdf;
                message.Attachments.Add(pdfb64);
                using (var smtp = new SmtpClient())
                {
                    var credential = new System.Net.NetworkCredential();
                    smtp.UseDefaultCredentials = false;
                    smtp.Credentials = credential;
                    smtp.Host = host;
                    smtp.Port = port;
                    smtp.EnableSsl = false;
                    smtp.Send(message);
                    message.Dispose();
                }
                Return = "success";
            }
            catch (Exception ex)
            {
                Return = ex.Message;
                return Return;
            }
            return Return;
        }

        public string MailMessageRawMaterial(SendMail information, Stream stream)
        {
            string Return;
            try
            {
                var user = ConfigurationManager.AppSettings["userEmail"].ToString();
                var host = ConfigurationManager.AppSettings["hostEmail"].ToString();
                var port = int.Parse(ConfigurationManager.AppSettings["portEmail"].ToString());
                var message = new MailMessage();

                var urlFile = System.Web.Hosting.HostingEnvironment.MapPath(@"\Emails\email_raw_material.html");
                //string text = System.IO.File.ReadAllText("C:\\inetpub\\FloridoERPTX\\FloridoERPTX\\Emails\\email_raw_material.html");
                string text = File.ReadAllText(urlFile);


                text = text.Replace("{Title}", information.title);
                text = text.Replace("{Body}", information.body);
                if (information.buttonText != "" && information.buttonLink != "")
                    text = text.Replace("<a href=>{Buton}</a>", "<a href=" + information.buttonLink + ">" + information.buttonText + "</a>");
                else
                    text = text.Replace("<a href=>{Buton}</a>", "");

                information.body = HttpUtility.HtmlDecode(text);

                message.To.Add(information.email);
                message.From = new MailAddress(user, information.from);
                message.Subject = information.subject;
                message.IsBodyHtml = true;
                message.Priority = MailPriority.High;

                var ct = new ContentType
                {
                    MediaType = MediaTypeNames.Application.Pdf,
                    Name = "notificacion_materia_prima.pdf"
                };
                stream.Position = 0;
                Attachment pdfb64 = new Attachment(stream, ct);
                pdfb64.ContentType.MediaType = MediaTypeNames.Application.Pdf;
                message.Attachments.Add(pdfb64);

                var htmlBody = AlternateView.CreateAlternateViewFromString(
                   information.body, Encoding.UTF8, "text/html");

                message.AlternateViews.Add(
                    AlternateView.CreateAlternateViewFromString(string.Empty, new ContentType("text/plain")));

                message.AlternateViews.Add(htmlBody);


                using (var smtp = new SmtpClient())
                {
                    var credential = new System.Net.NetworkCredential();
                    smtp.UseDefaultCredentials = false;
                    smtp.Credentials = credential;
                    smtp.Host = host;
                    smtp.Port = port;
                    smtp.EnableSsl = true;
                    smtp.Send(message);
                    message.Dispose();
                }
                Return = "success";
            }
            catch (Exception ex)
            {
                Return = ex.Message;
                return Return;
            }
            return Return;
        }

        public string MailMessageEmptySpaceScan(SendMail information, Stream stream)
        {
            string Return;
            try
            {
                var user = ConfigurationManager.AppSettings["userEmail"].ToString();
                var host = ConfigurationManager.AppSettings["hostEmail"].ToString();
                var port = int.Parse(ConfigurationManager.AppSettings["portEmail"].ToString());
                var message = new MailMessage();
                var urlFile = System.Web.Hosting.HostingEnvironment.MapPath(@"\Emails\email_report_empty_scan.html");
                string text = File.ReadAllText(urlFile);

                text = text.Replace("{folio}", information.folio);

                information.body = HttpUtility.HtmlDecode(text);

                message.To.Add(information.email);
                message.From = new MailAddress(user, information.from);
                message.Subject = information.subject;
                message.IsBodyHtml = true;
                message.Priority = MailPriority.High;


                var htmlBody = AlternateView.CreateAlternateViewFromString(
                   information.body, Encoding.UTF8, "text/html");

                message.AlternateViews.Add(
                    AlternateView.CreateAlternateViewFromString(string.Empty, new ContentType("text/plain")));

                message.AlternateViews.Add(htmlBody);

                var ct = new ContentType
                {
                    MediaType = MediaTypeNames.Application.Pdf,
                    Name = "repote_espacio_vacios.pdf"
                };
                stream.Position = 0;
                Attachment pdfb64 = new Attachment(stream, ct);
                pdfb64.ContentType.MediaType = MediaTypeNames.Application.Pdf;
                message.Attachments.Add(pdfb64);

                using (var smtp = new SmtpClient())
                {
                    var credential = new System.Net.NetworkCredential();
                    smtp.UseDefaultCredentials = false;
                    smtp.Credentials = credential;
                    smtp.Host = host;
                    smtp.Port = port;
                    smtp.EnableSsl = true;
                    smtp.Send(message);
                    message.Dispose();
                }
                Return = "success";
            }
            catch (Exception ex)
            {
                Return = ex.Message;
                return Return;
            }
            return Return;
        }

        //Cierre de conteo
        public string MailMessageInventoryClose(SendMail information, Stream stream)
        {
            string Return;
            try
            {
                var user = ConfigurationManager.AppSettings["userEmail"].ToString();
                var host = ConfigurationManager.AppSettings["hostEmail"].ToString();
                var port = int.Parse(ConfigurationManager.AppSettings["portEmail"].ToString());
                var message = new MailMessage();
                var urlFile = System.Web.Hosting.HostingEnvironment.MapPath(@"\Emails\email_inventory_close.html");
                string text = File.ReadAllText(urlFile);
                //string text = System.IO.File.ReadAllText("C:\\inetpub\\FloridoERPTX\\FloridoERPTX\\Emails\\email_inventory_close.html");

                text = text.Replace("{id_inventory}", information.folio);
                information.body = HttpUtility.HtmlDecode(text);

                message.To.Add(information.email);
                message.From = new MailAddress(user, information.from);
                message.Subject = information.subject;
                message.IsBodyHtml = true;
                message.Priority = MailPriority.High;

                var htmlBody = AlternateView.CreateAlternateViewFromString(
                   information.body, Encoding.UTF8, "text/html");

                message.AlternateViews.Add(
                    AlternateView.CreateAlternateViewFromString(string.Empty, new ContentType("text/plain")));

                message.AlternateViews.Add(htmlBody);

                var ct = new ContentType
                {
                    MediaType = MediaTypeNames.Application.Pdf,
                    Name = "InventoryClose" + information.folio + ".pdf"
                };
                stream.Position = 0;
                Attachment pdfb64 = new Attachment(stream, ct);
                pdfb64.ContentType.MediaType = MediaTypeNames.Application.Pdf;
                message.Attachments.Add(pdfb64);

                using (var smtp = new SmtpClient())
                {
                    var credential = new System.Net.NetworkCredential();
                    smtp.UseDefaultCredentials = false;
                    smtp.Credentials = credential;
                    smtp.Host = host;
                    smtp.Port = port;
                    smtp.EnableSsl = false;
                    smtp.Send(message);
                    message.Dispose();
                }
                Return = "success";
            }
            catch (Exception ex)
            {
                Return = ex.Message;
                return Return;
            }
            return Return;
        }

        public bool SendMailQuotes(QuoteModel Model, bool SendToSeller)
        {
            try
            {
                var message = new MailMessage();
                var template = System.Web.Hosting.HostingEnvironment.MapPath(@"\Emails\email_quote.html");

                string text = File.ReadAllText(template);
                string Title = string.Format("{0}{1}{2}", "Cotización ", Model.QuoteSerie, Model.QuoteNo);

                text = text.Replace("{Customer}", Model.ClientName);
                text = text.Replace("{Document}", Model.QuoteSerie + Model.QuoteNo);

                if (SendToSeller)
                    message.To.Add(Model.SellerEmail);
                else
                    message.To.Add(Model.Email);

                message.From = new MailAddress(USER, Model.Seller);
                message.Subject = Title;
                message.IsBodyHtml = true;
                message.Priority = MailPriority.High;

                var htmlBody = AlternateView.CreateAlternateViewFromString(HttpUtility.HtmlDecode(text), Encoding.UTF8, "text/html");
                message.AlternateViews.Add(AlternateView.CreateAlternateViewFromString(string.Empty, new ContentType("text/plain")));
                message.AlternateViews.Add(htmlBody);

                var ct = new ContentType();
                ct.MediaType = MediaTypeNames.Application.Pdf;
                ct.Name = string.Format("{0}{1}", Title, ".pdf");

                byte[] bytes = Convert.FromBase64String(Model.PdfBase64);
                MemoryStream stream = new MemoryStream();
                stream.Write(bytes, 0, bytes.Length);
                stream.Position = 0;
                Attachment pdfb64 = new Attachment(stream, ct);
                pdfb64.ContentType.MediaType = MediaTypeNames.Application.Pdf;
                message.Attachments.Add(pdfb64);

                using (var smtp = new SmtpClient())
                {
                    var credential = new System.Net.NetworkCredential();
                    smtp.UseDefaultCredentials = false;
                    smtp.Credentials = credential;
                    smtp.Host = HOST;
                    smtp.Port = PORT;
                    smtp.EnableSsl = true;
                    smtp.Send(message);
                    message.Dispose();
                }

                return true;
            }
            catch (Exception ex)
            {
                string msg = ex.Message;
                return false;
            }
        }
        public string MailMessageExpensiveGeneric(SendMail information, Stream stream)
        {
            string Return;
            try
            {
                var user = ConfigurationManager.AppSettings["userEmail"].ToString();
                var host = ConfigurationManager.AppSettings["hostEmail"].ToString();
                var port = int.Parse(ConfigurationManager.AppSettings["portEmail"].ToString());
                var message = new MailMessage();
                var urlFile = System.Web.Hosting.HostingEnvironment.MapPath(@"\Emails\email_expense_generic.html");
                string text = File.ReadAllText(urlFile);
                //string text = System.IO.File.ReadAllText("C:\\inetpub\\FloridoERPTX\\FloridoERPTX\\Emails\\email_inventory_close.html");

                text = text.Replace("{Body}", information.body);
                text = text.Replace("{Title}", information.subject);
                text = text.Replace("{Button}", information.buttonLink);
                information.body = HttpUtility.HtmlDecode(text);

                message.To.Add(information.email);
                message.From = new MailAddress(user, information.from);
                message.Subject = information.subject;
                message.IsBodyHtml = true;
                message.Priority = MailPriority.High;

                var htmlBody = AlternateView.CreateAlternateViewFromString(
                   information.body, Encoding.UTF8, "text/html");

                message.AlternateViews.Add(
                    AlternateView.CreateAlternateViewFromString(string.Empty, new ContentType("text/plain")));

                message.AlternateViews.Add(htmlBody);

                var ct = new ContentType
                {
                    MediaType = MediaTypeNames.Application.Pdf,
                    Name = "Gastos_" + information.folio + ".pdf"
                };
                stream.Position = 0;
                Attachment pdfb64 = new Attachment(stream, ct);
                pdfb64.ContentType.MediaType = MediaTypeNames.Application.Pdf;
                message.Attachments.Add(pdfb64);

                using (var smtp = new SmtpClient())
                {
                    var credential = new System.Net.NetworkCredential();
                    smtp.UseDefaultCredentials = false;
                    smtp.Credentials = credential;
                    smtp.Host = host;
                    smtp.Port = port;
                    smtp.EnableSsl = false;
                    smtp.Send(message);
                    message.Dispose();
                }
                Return = "success";
            }
            catch (Exception ex)
            {
                Return = ex.Message;
                return Return;
            }
            return Return;
        }
        public string PurchaseEmail(SendMail information, Stream stream, string storeFlorido)
        {
            string Return;
            try
            {
                var user = ConfigurationManager.AppSettings["userEmail"].ToString();
                var host = ConfigurationManager.AppSettings["hostEmail"].ToString();
                var port = int.Parse(ConfigurationManager.AppSettings["portEmail"].ToString());
                var message = new MailMessage();

                var urlFile = System.Web.Hosting.HostingEnvironment.MapPath(@"\Emails\email_entryFree.html");
                string text = File.ReadAllText(urlFile);

                storeFlorido = storeFlorido.Replace(" ", "_");
                text = text.Replace("{Folio}", information.folio);
                text = text.Replace("{Proveedor}", information.store);

                information.body = HttpUtility.HtmlDecode(text);

                message.To.Add(information.email);
                message.From = new MailAddress(user, information.from);
                message.Subject = information.subject;
                message.IsBodyHtml = true;
                message.Priority = MailPriority.High;


                var htmlBody = AlternateView.CreateAlternateViewFromString(
                   information.body, Encoding.UTF8, "text/html");

                message.AlternateViews.Add(
                    AlternateView.CreateAlternateViewFromString(string.Empty, new ContentType("text/plain")));

                message.AlternateViews.Add(htmlBody);

                var ct = new ContentType
                {
                    MediaType = MediaTypeNames.Application.Pdf,
                    Name = storeFlorido + "_OC_" + information.folio + ".pdf"
                };
                stream.Position = 0;
                Attachment pdfb64 = new Attachment(stream, ct);
                pdfb64.ContentType.MediaType = MediaTypeNames.Application.Pdf;
                message.Attachments.Add(pdfb64);

                using (var smtp = new SmtpClient())
                {
                    var credential = new System.Net.NetworkCredential();
                    smtp.UseDefaultCredentials = false;
                    smtp.Credentials = credential;
                    smtp.Host = host;
                    smtp.Port = port;
                    smtp.EnableSsl = false;
                    smtp.Send(message);
                    message.Dispose();
                }
                Return = "success";
            }
            catch (Exception ex)
            {
                Return = ex.Message;
                return Return;
            }
            return Return;
        }

        //Correccion de inventario
        public string MailMessageAdjustmentInventory(SendMail information, Stream stream)
        {
            string Return;
            try
            {
                var user = ConfigurationManager.AppSettings["userEmail"].ToString();
                var host = ConfigurationManager.AppSettings["hostEmail"].ToString();
                var port = int.Parse(ConfigurationManager.AppSettings["portEmail"].ToString());
                var message = new MailMessage();
                var urlFile = System.Web.Hosting.HostingEnvironment.MapPath(@"\Emails\email_adjustment_inventory.html");
                string text = File.ReadAllText(urlFile);
                //string text = System.IO.File.ReadAllText("C:\\inetpub\\FloridoERPTX\\FloridoERPTX\\Emails\\email_inventory_close.html");

                text = text.Replace("{Body}", information.body);
                text = text.Replace("{Title}", information.subject);
                information.body = HttpUtility.HtmlDecode(text);

                message.To.Add(information.email);
                message.From = new MailAddress(user, information.from);
                message.Subject = information.subject;
                message.IsBodyHtml = true;
                message.Priority = MailPriority.High;

                var htmlBody = AlternateView.CreateAlternateViewFromString(
                   information.body, Encoding.UTF8, "text/html");

                message.AlternateViews.Add(
                    AlternateView.CreateAlternateViewFromString(string.Empty, new ContentType("text/plain")));

                message.AlternateViews.Add(htmlBody);

                var ct = new ContentType
                {
                    MediaType = MediaTypeNames.Application.Pdf,
                    Name = "AjusteDeInventario" + information.folio + ".pdf"
                };
                stream.Position = 0;
                Attachment pdfb64 = new Attachment(stream, ct);
                pdfb64.ContentType.MediaType = MediaTypeNames.Application.Pdf;
                message.Attachments.Add(pdfb64);

                using (var smtp = new SmtpClient())
                {
                    var credential = new System.Net.NetworkCredential();
                    smtp.UseDefaultCredentials = false;
                    smtp.Credentials = credential;
                    smtp.Host = host;
                    smtp.Port = port;
                    smtp.EnableSsl = false;
                    smtp.Send(message);
                    message.Dispose();
                }
                Return = "success";
            }
            catch (Exception ex)
            {
                Return = ex.Message;
                return Return;
            }
            return Return;
        }

        //Respuesta de correccion de inventario
        public string MailMessageAdjustmentInventoryResponse(SendMail information)
        {
            string Return;
            try
            {
                var user = ConfigurationManager.AppSettings["userEmail"].ToString();
                var host = ConfigurationManager.AppSettings["hostEmail"].ToString();
                var port = int.Parse(ConfigurationManager.AppSettings["portEmail"].ToString());
                var message = new MailMessage();
                var urlFile = System.Web.Hosting.HostingEnvironment.MapPath(@"\Emails\email_adjustment_inventory_response.html");
                string text = File.ReadAllText(urlFile);
                //string text = System.IO.File.ReadAllText("C:\\inetpub\\FloridoERPTX\\FloridoERPTX\\Emails\\email_inventory_close.html");

                text = text.Replace("{Body}", information.body);
                text = text.Replace("{Title}", information.subject);
                text = text.Replace("{Button}", information.buttonLink);
                information.body = HttpUtility.HtmlDecode(text);

                message.To.Add(information.email);
                message.From = new MailAddress(user, information.from);
                message.Subject = information.subject;
                message.IsBodyHtml = true;
                message.Priority = MailPriority.High;

                var htmlBody = AlternateView.CreateAlternateViewFromString(
                   information.body, Encoding.UTF8, "text/html");

                message.AlternateViews.Add(
                    AlternateView.CreateAlternateViewFromString(string.Empty, new ContentType("text/plain")));

                message.AlternateViews.Add(htmlBody);

                //var ct = new ContentType();
                //ct.MediaType = MediaTypeNames.Application.Pdf;
                //ct.Name = "AjusteDeInventario" + information.folio + ".pdf";
                //stream.Position = 0;
                //Attachment pdfb64 = new Attachment(stream, ct);
                //pdfb64.ContentType.MediaType = System.Net.Mime.MediaTypeNames.Application.Pdf;
                //message.Attachments.Add(pdfb64);

                using (var smtp = new SmtpClient())
                {
                    var credential = new System.Net.NetworkCredential();
                    smtp.UseDefaultCredentials = false;
                    smtp.Credentials = credential;
                    smtp.Host = host;
                    smtp.Port = port;
                    smtp.EnableSsl = false;
                    smtp.Send(message);
                    message.Dispose();
                }
                Return = "success";
            }
            catch (Exception ex)
            {
                Return = ex.Message;
                return Return;
            }
            return Return;
        }

        //Rechazo Solicitud de Cancelacion de Transferencias
        public string MailMessageRechazoCancelacionTransferencias(SendMail information)
        {
            string Return;
            try
            {
                var user = ConfigurationManager.AppSettings["userEmail"].ToString();
                var host = ConfigurationManager.AppSettings["hostEmail"].ToString();
                var port = int.Parse(ConfigurationManager.AppSettings["portEmail"].ToString());
                var message = new MailMessage();

                var urlFile = System.Web.Hosting.HostingEnvironment.MapPath(@"\Emails\email_rechazo_cancel_transfer.html");
                string text = File.ReadAllText(urlFile);
                text = text.Replace("{folio}", information.folio);

                information.body = HttpUtility.HtmlDecode(text);

                message.To.Add(information.email);
                message.From = new MailAddress(user, information.from);
                message.Subject = information.subject;
                message.IsBodyHtml = true;
                message.Priority = MailPriority.High;

                var htmlBody = AlternateView.CreateAlternateViewFromString(
                   information.body, Encoding.UTF8, "text/html");

                message.AlternateViews.Add(
                    AlternateView.CreateAlternateViewFromString(string.Empty, new ContentType("text/plain")));

                message.AlternateViews.Add(htmlBody);

                using (var smtp = new SmtpClient())
                {
                    var credential = new System.Net.NetworkCredential();
                    smtp.UseDefaultCredentials = false;
                    smtp.Credentials = credential;
                    smtp.Host = host;
                    smtp.Port = port;
                    smtp.EnableSsl = true;
                    smtp.Send(message);
                    message.Dispose();
                }
                Return = "success";
            }
            catch (Exception ex)
            {
                Return = ex.Message;
                return Return;
            }
            return Return;
        }
        public string MailMessageCancellationScrap(SendMail information, Stream stream)
        {
            string Return;
            try
            {
                var user = ConfigurationManager.AppSettings["userEmail"].ToString();
                var host = ConfigurationManager.AppSettings["hostEmail"].ToString();
                var port = int.Parse(ConfigurationManager.AppSettings["portEmail"].ToString());

                var message = new MailMessage();

                var urlFile = System.Web.Hosting.HostingEnvironment.MapPath(@"\Emails\email_cancellation_scrap.html");
                string text = File.ReadAllText(urlFile);
                text = text.Replace("{folio}", information.folio);
                text = text.Replace("{buttonERP}", "<a href= http://"+information.buttonLink+ ":35/Warehouse/SSM004>Ir a ERP</a>");

                information.body = HttpUtility.HtmlDecode(text);

                message.To.Add(information.email);
                message.From = new MailAddress(user, information.from);
                message.Subject = information.subject;
                message.IsBodyHtml = true;
                message.Priority = MailPriority.High;

                var htmlBody = AlternateView.CreateAlternateViewFromString(
                   information.body, Encoding.UTF8, "text/html");

                message.AlternateViews.Add(
                    AlternateView.CreateAlternateViewFromString(string.Empty, new ContentType("text/plain")));

                message.AlternateViews.Add(htmlBody);

                var ct = new ContentType
                {
                    MediaType = MediaTypeNames.Application.Pdf,
                    Name = "Cancelacion Merma " + information.folio +".pdf"
                };
                stream.Position = 0;
                Attachment pdfb64 = new Attachment(stream, ct);
                pdfb64.ContentType.MediaType = MediaTypeNames.Application.Pdf;
                message.Attachments.Add(pdfb64);

                using (var smtp = new SmtpClient())
                {
                    var credential = new System.Net.NetworkCredential();
                    smtp.UseDefaultCredentials = false;
                    smtp.Credentials = credential;
                    smtp.Host = host;
                    smtp.Port = port;
                    smtp.EnableSsl = true;
                    smtp.Send(message);
                    message.Dispose();
                }
                Return = "success";
            }
            catch (Exception ex)
            {
                Return = ex.Message;
                return Return;
            }
            return Return;
        }
    }
    
}