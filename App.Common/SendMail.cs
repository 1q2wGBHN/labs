﻿namespace App.Common
{
    public class SendMail
    {
        public string title { get; set; }
        public string body { get; set; }
        public string buttonLink { get; set; }
        public string buttonText { get; set; }
        public string subject { get; set; }
        public string from { get; set; }
        public string email { get; set; }
        public string folio { get; set; }
        public string store { get; set; }
        public string date { get; set; }
        public string currecy { get; set; }
        public string Supplier { get; set; }
    }
}