﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace App.Common
{
    public class Requests
    {
        private static readonly HttpClient client = new HttpClient();
        private string url;
        private Method method;
        private dynamic urlParams;
        private dynamic data;

        public static Requests Get(string url, dynamic urlParams = default(dynamic))
        {
            return new Requests().Method(App.Common.Method.Get).Url(url).UrlParams(urlParams);
        }

        public static Requests Post(string url, dynamic urlParams = default(dynamic))
        {
            return new Requests().Method(App.Common.Method.Post).Url(url).UrlParams(urlParams);
        }

        public Requests Url(string url)
        {
            this.url = url;
            return this;
        }

        public Requests Method(Method method)
        {
            this.method = method;
            return this;
        }

        public Requests UrlParams(dynamic urlParams)
        {
            this.urlParams = urlParams;
            return this;
        }

        public Requests Data(dynamic data)
        {
            this.data = data;
            return this;
        }

        public async Task<Result<TRes>> Send<TRes>()
        {
            switch (method)
            {
                case App.Common.Method.Get:
                    return await Get<TRes>();
                case App.Common.Method.Post:
                    return await Post<TRes>();
                default:
                    throw new ArgumentOutOfRangeException();
            }
        }

        public async Task<Result<TRes>> SendResult<TRes>()
        {
            var res = await Send<Result<TRes>>();
            return res.Ok ? res.Data : Result.Error(res.Msg, res.Code);
        }

        public async Task<Result<TResult>> SendAsResult<TRes, TResult>() where TRes : IAsResult<TResult>
        {
            var res = await Send<TRes>();
            return res.Ok ? res.Data.ToResult() : Result.Error(res.Msg, res.Code);
        }

        private async Task<Result<TRes>> Post<TRes>()
        {
            try
            {
                var json = JsonConvert.SerializeObject(data);
                HttpResponseMessage res = await client.PostAsync(url + Util.UrlSerialize(urlParams),
                    new StringContent(json, Encoding.UTF8, "application/json"));
                return await Util.HttpResult<TRes>(res);
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                return Result.Error("Error al realizar petición al servidor");
            }
        }

        private async Task<Result<TRes>> Get<TRes>()
        {
            try
            {
                HttpResponseMessage res = await client.GetAsync(url + Util.UrlSerialize(urlParams));
                return await Util.HttpResult<TRes>(res);
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                return Result.Error("Error al realizar petición al servidor");
            }
        }

        private static class Util
        {
            public static string UrlSerialize(dynamic obj)
            {
                var str = "";
                var o = obj;
                if (o == null) return str;
                var prop = obj.GetType().GetProperties();
                var list = new List<string>();
                foreach (var p in prop)
                {
                    if (p.GetValue(obj) != null)
                        list.Add(Uri.EscapeUriString($"{p.Name}={p.GetValue(obj)}"));
                }
                if (list.Count > 0)
                    str = "?" + string.Join("&", list);

                return str;
            }

            public static async Task<Result<TRes>> HttpResult<TRes>(HttpResponseMessage res)
            {
                try
                {
                    if (res.IsSuccessStatusCode)
                    {
                        var resStr = await res.Content.ReadAsStringAsync();
                        var obj = JsonConvert.DeserializeObject<TRes>(resStr);
                        return Result.OK(obj);
                    }

                    return Result.Error($"{res.StatusCode} Error al realizar petición", (int)res.StatusCode);
                }
                catch (JsonException e)
                {
                    Console.WriteLine(e);
                    return Result.Error("Error al procesar respuesta del servidor");
                }
            }
        }
    }

    public interface IAsResult<T>
    {
        Result<T> ToResult();
    }

    public enum Method
    {
        Get, Post
    }
}