﻿using App.DAL.TokenPhone;
using System;
using System.IO;
using System.Net;
using System.Text;
using System.Web.Script.Serialization;

namespace App.Common
{
    public class Notification
    {
        private readonly TokenPhoneMessageRepository _tokenPhoneMessageRepository;
        private readonly TokenPhoneDetailRepository _tokenPhoneDetailRepository;
        private readonly TokenPhoneRepository _tokenPhoneRepository;

        //Titles
        public string titleEmptySpace = "Códigos con Espacios Vacíos Folio. ";
        public string titleEmptySpaceIncidence = "Códigos con Espacios Vacíos con Incidencias Folio. ";

        //Messages
        public string messsageEmptySpace = "Le ha sido mandado a su correo los códigos con espacios vacios que fueron encontrados, favor de revisar.";
        public string messsageEmptySpaceIncidence = "Le ha sido mandado a su correo los códigos con espacios vacios con incidencias que fueron encontrados, favor de darle seguimiento.";

        public Notification()
        {
            _tokenPhoneMessageRepository = new TokenPhoneMessageRepository();
            _tokenPhoneDetailRepository = new TokenPhoneDetailRepository();
            _tokenPhoneRepository = new TokenPhoneRepository();
        }

        public int NotificationOne(string title, string message, string user_send_to, string cuser, string program_id)
        {
            var TokenId = _tokenPhoneRepository.SearchUserByUserName(user_send_to);
            if (TokenId != null)
            {
                var resp_message = _tokenPhoneMessageRepository.SaveMessage(TokenId.id_token, title, message, 1, user_send_to, cuser, program_id);

                if (resp_message != null)
                {
                    var tokenResp = _tokenPhoneDetailRepository.SearchTokenByUser(user_send_to);

                    try
                    {
                        var info = new
                        {
                            to = tokenResp.token,
                            data = new
                            {
                                idNotification = resp_message.id_notification,
                                type_notification = 1,
                                title = title,
                                message = message
                            },
                        };
                        return SendNotification(info);
                    }
                    catch (Exception)
                    {
                        _tokenPhoneMessageRepository.MessegeNotSent(resp_message);
                        return 0;
                    }
                }
                _tokenPhoneMessageRepository.MessegeNotSent(resp_message);
                return 0;
            }
            return 0;
        }

        public int NotificationTwo(string title, string message, string user_send_to, string cuser, string program_id)
        {
            var TokenId = _tokenPhoneRepository.SearchUserByUserName(user_send_to);
            if (TokenId != null)
            {
                var resp_message = _tokenPhoneMessageRepository.SaveMessage(TokenId.id_token, title, message, 2, user_send_to, cuser, program_id);

                if (resp_message != null)
                {
                    var tokenResp = _tokenPhoneDetailRepository.SearchTokenByUser(user_send_to);

                    try
                    {
                        var info = new
                        {
                            to = tokenResp.token,
                            data = new
                            {
                                idNotification = resp_message.id_notification,
                                type_notification = 2,
                                title = title,
                                message = message
                            },
                        };
                        return SendNotification(info);
                    }
                    catch (Exception)
                    {
                        _tokenPhoneMessageRepository.MessegeNotSent(resp_message);
                        return 0;
                    }
                }
                _tokenPhoneMessageRepository.MessegeNotSent(resp_message);
                return 0;
            }
            return 0;
        }

        public int NotificationThree(string title, string message, string user_send_to, string cuser, string program_id)
        {
            var TokenId = _tokenPhoneRepository.SearchUserByUserName(user_send_to);
            if (TokenId != null)
            {
                var resp_message = _tokenPhoneMessageRepository.SaveMessage(TokenId.id_token, title, message, 3, user_send_to, cuser, program_id);

                if (resp_message != null)
                {
                    var tokenResp = _tokenPhoneDetailRepository.SearchTokenByUser(user_send_to);

                    try
                    {
                        var info = new
                        {
                            to = tokenResp.token,
                            data = new
                            {
                                idNotification = resp_message.id_notification,
                                type_notification = 3,
                                title = title,
                                message = message
                            },
                        };
                        return SendNotification(info);
                    }
                    catch (Exception)
                    {
                        _tokenPhoneMessageRepository.MessegeNotSent(resp_message);
                        return 0;
                    }
                }
                _tokenPhoneMessageRepository.MessegeNotSent(resp_message);
                return 0;
            }
            return 0;
        }

        public int NotificationFour(string title, string message, string user_send_to, string cuser, string program_id, string urlImageBig)
        {
            var TokenId = _tokenPhoneRepository.SearchUserByUserName(user_send_to);
            if (TokenId != null)
            {
                var resp_message = _tokenPhoneMessageRepository.SaveMessage(TokenId.id_token, title, message, 4, user_send_to, cuser, program_id);

                if (resp_message != null)
                {
                    var tokenResp = _tokenPhoneDetailRepository.SearchTokenByUser(user_send_to);

                    try
                    {
                        var info = new
                        {
                            to = tokenResp.token,
                            data = new
                            {
                                idNotification = resp_message.id_notification,
                                type_notification = 4,
                                title = title,
                                message = message,
                                urlImageBig = urlImageBig
                            },
                        };
                        return SendNotification(info);
                    }
                    catch (Exception)
                    {
                        _tokenPhoneMessageRepository.MessegeNotSent(resp_message);
                        return 0;
                    }
                }
                _tokenPhoneMessageRepository.MessegeNotSent(resp_message);
                return 0;
            }
            return 0;
        }

        public int SendNotification(object data)
        {
            var seralizer = new JavaScriptSerializer();
            var json = seralizer.Serialize(data);
            Byte[] byteArray = Encoding.UTF8.GetBytes(json);

            try
            {
                //string server_api_key = ConfigurationManager.AppSettings["SENDER_API_KEY"];
                string server_api_key = "AIzaSyCRm95JAjr1AF2LzY7WPx_Gh63Mzy6pJuE";
                //string sender_id = ConfigurationManager.AppSettings["SENDER_ID"];
                string sender_id = "854122722936";

                WebRequest tRequest = WebRequest.Create("https://fcm.googleapis.com/fcm/send");
                tRequest.Method = "post";
                tRequest.ContentType = "application/json";
                tRequest.Headers.Add($"Authorization: key={server_api_key}");
                tRequest.Headers.Add($"Sender: id={sender_id}");

                tRequest.ContentLength = byteArray.Length;
                Stream dataStream = tRequest.GetRequestStream();
                dataStream.Write(byteArray, 0, byteArray.Length);
                dataStream.Close();

                WebResponse tResponse = tRequest.GetResponse();
                dataStream = tResponse.GetResponseStream();
                StreamReader tReader = new StreamReader(dataStream);

                string sResponseFromServer = tReader.ReadToEnd();

                tReader.Close();
                dataStream.Close();
                tResponse.Close();

                return 1;
            }
            catch (Exception e)
            {
                System.Diagnostics.Debug.WriteLine("*Error: " + e);
                return 0;
            }
        }
    }
}